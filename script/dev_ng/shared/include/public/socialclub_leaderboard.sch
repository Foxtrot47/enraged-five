//USING "leader_board_common.sch"
USING "commands_stats.sch"
USING "net_common_functions.sch"
USING "socialclub_leaderboard_hud.sch"
USING "net_leaderboards.sch"
USING "commands_socialclub.sch"
USING "net_stat_system.sch"
USING "PS_MP_LESSON_ENUM.sch"

// Navigation
CONST_INT SC_LB_PRESSED_UP			0
CONST_INT SC_LB_PRESSED_DOWN		1
CONST_INT SC_LB_PRESSED_SELECT		2
CONST_INT SC_LB_PRESSED_Y			3
CONST_INT SC_LB_PRESSED_ACCEPT		4

//General read stats
CONST_INT SC_LB_START_READ			0
CONST_INT SC_LB_READ_PENDING		1
CONST_INT SC_LB_READING				2
CONST_INT SC_LB_READ_FINISED		3

//Used by freemode for XP board.
CONST_INT FREEMODE_DATA_LB_STAT_TYPE_XP			0
CONST_INT FREEMODE_DATA_LB_STAT_TYPE_TIME		1

CONST_INT FREEMODE_DATA_LB_MAX_STAT_TYPES		2

CONST_INT MAX_STORED_CREWS 5

CONST_INT FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT			0
CONST_INT FREEMODE_DATA_LB_MAINTAIN_STAGE_UPDATE_WRITE	1
CONST_INT FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS		2
CONST_INT FREEMODE_DATA_LB_MAINTAIN_STAGE_WRITE			3

CONST_INT FREEMODE_DATA_LB_BS_UPDATE_STAT	0


CONST_INT FREEMODE_DATA_LB_COLUMN_XP		0
CONST_INT FREEMODE_DATA_LB_COLUMN_TIME		1	
CONST_INT FREEMODE_DATA_LB_COLUMN_SESSIONS	2

CONST_INT RACE_LEADERBOARD_NO_DATA_RESULT 	-2

STRUCT RACE_CHECKPOINTS_LOAD_STRUCT
	INT iCheckPointType
	INT m_ClanId
	INT iFriendLoopCounter
	BOOL bInitComplete
	INT iBestLapTime
ENDSTRUCT

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(STRING ProcName,Leaderboard2ReadData& lbReadData)
	INT i
	println("PRINT_LEADERBOARD_READ_DATA_STRUCT started in ",": ", ProcName)
	println("leaderboard ID = ",lbReadData.m_LeaderboardID)
	println("leaderboard TYPE = ",ENUM_TO_INT(lbReadData.m_type))
	println("leaderboard CLAN ID = ",lbReadData.m_ClanID)
	println("leaderboard NUM GROUPS = ", lbReadData.m_GroupSelector.m_NumGroups)
	REPEAT lbReadData.m_GroupSelector.m_NumGroups i
		println("leaderboard CATEGORY ",i," = ",lbReadData.m_GroupSelector.m_Group[i].m_Category)
		println("leaderboard NAME ID ",i," = ",lbReadData.m_GroupSelector.m_Group[i].m_Id)
	ENDREPEAT
ENDPROC

PROC DEBUG_PRINT_LEADERBOARD_GROUP_HANDLE_STRUCT(STRING ProcName,Leaderboard2GroupHandle& lbGroup[])
	INT i
	println("PRINT_LEADERBOARD_GROUP_HANDLE_STRUCT started in ",": ", ProcName)
	println("leaderboard NUM GROUPS = ", lbGroup[0].m_NumGroups)
	REPEAT lbGroup[0].m_NumGroups i
		println("leaderboard CATEGORY ",i," = ",lbGroup[0].m_Group[i].m_Category)
		println("leaderboard NAME ID ",i," = ",lbGroup[0].m_Group[i].m_Id)
	ENDREPEAT
ENDPROC

PROC DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(Leaderboard2ReadData& lbReadData)
	println("--CURRENT READ DETAILS--")
	println("leaderboard ID = ",lbReadData.m_LeaderboardID)
	println("eaderboard TYPE = ",ENUM_TO_INT(lbReadData.m_type))
	
	println("--OUTSTANDING READ DETAILS--")
	println("leaderboard ID = ",ongoingSCLB_readInfo.m_LeaderboardID)
	println("leaderboard TYPE = ",ENUM_TO_INT(ongoingSCLB_readInfo.m_type))
ENDPROC

PROC DEBUG_PRINT_LB_READ_PROGRESS(STRING ProcName,INT iLoadStage)
	println(ProcName," moving to load stage ", iLoadStage)
ENDPROC

PROC DEBUG_PRINT_READ_RESULT_STATUS(STRING ProcName,Leaderboard2ReadData& lbReadData, BOOL bSuccessful)
	INt i
	IF bSuccessful
		println(ProcName, " READ SUCCESSFUL")
	ENDIF
	println("leaderboard ID = ",lbReadData.m_LeaderboardID)
	println("leaderboard TYPE = ",ENUM_TO_INT(lbReadData.m_type))
	println("leaderboard NUM GROUPS = ", lbReadData.m_GroupSelector.m_NumGroups)
	REPEAT lbReadData.m_GroupSelector.m_NumGroups i
		println("leaderboard CATEGORY ",i," = ",lbReadData.m_GroupSelector.m_Group[i].m_Category)
		println("leaderboard NAME ID ",i," = ",lbReadData.m_GroupSelector.m_Group[i].m_Id)
	ENDREPEAT
ENDPROC

PROC DEBUG_START_LEADERBOARD_PRINT_RESULTS(STRING ProcName,Leaderboard2ReadData& lbReadData)
	INT i
	println(ProcName, " leaderboard ID = ",lbReadData.m_LeaderboardID)
	println("leaderboard TYPE = ",ENUM_TO_INT(lbReadData.m_type))
	println("leaderboard NUM GROUPS = ", lbReadData.m_GroupSelector.m_NumGroups)
	REPEAT lbReadData.m_GroupSelector.m_NumGroups i
		println("leaderboard CATEGORY ",i," = ",lbReadData.m_GroupSelector.m_Group[i].m_Category)
		println("leaderboard NAME ID ",i," = ",lbReadData.m_GroupSelector.m_Group[i].m_Id)
	ENDREPEAT
ENDPROC

PROC DEBUG_PRINT_SC_LB_RETURNED_VALUES(STRING ProcName,STRING Section, INT iRow, INT iColumn, INT iValue, FLOAT fValue, BOOL bFloat, BOOL bForcePrint = FALSE)
	IF g_bOutputSCLeaderboardData
	OR bForcePrint
		IF bFloat
			println(ProcName, ": ",Section,": Row # ",iRow ,": Column # ",iColumn," - fValue ", fValue)
		ELSE
			println(ProcName, ": ",Section,": Row # ",iRow , ": Column # ",iColumn," - iValue ", iValue)
		ENDIF
	ENDIF
ENDPROC

PROC DEBUG_PRINT_READ_INFO(STRING ProcName,LeaderboardReadInfo readInfo)
	println(ProcName, ": READ INFO- m_LeaderboardId : ", readINfo.m_LeaderboardId)
	println(ProcName, ": READ INFO- m_leaderboardType : ", readINfo.m_leaderboardType)
	println(ProcName, ": READ INFO- m_LeaderboardIndex : ", readINfo.m_LeaderboardIndex)
	println(ProcName, ": READ INFO- m_ReturnedRows : ", readINfo.m_ReturnedRows)
	println(ProcName, ": READ INFO- m_TotalRows: ", readINfo.m_TotalRows)
	println(ProcName, ": READ INFO- m_LocalGamerRowNumber : ", readINfo.m_LocalGamerRowNumber)
ENDPROC
#ENDIF

PROC START_LEADERBOARD_READ(Leaderboard2ReadData lbReadData)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	println("--START_LEADERBOARD_READ-- leaderboard ID = ",lbReadData.m_LeaderboardID)
	println("leaderboard TYPE = ",ENUM_TO_INT(lbReadData.m_type))
	
	ongoingSCLB_readInfo.bProcessingLBRead = TRUE
	REINIT_NET_TIMER(ongoingSCLB_readInfo.failSafeTimer,TRUE)
	ongoingSCLB_readInfo.m_LeaderboardID = lbReadData.m_LeaderboardID
	ongoingSCLB_readInfo.m_type = lbReadData.m_type
ENDPROC

PROC END_LEADERBOARD_READ(INT& iLoadStage,BOOL& readResult,Leaderboard2ReadData& lbReadData)
	println("--END_LEADERBOARD_READ-- leaderboard ID = ",lbReadData.m_LeaderboardID)
	println("leaderboard TYPE = ",ENUM_TO_INT(lbReadData.m_type))
	iLoadStage = 0
	readResult = FALSE
	ongoingSCLB_readInfo.bProcessingLBRead = FALSE
	RESET_NET_TIMER(ongoingSCLB_readInfo.failSafeTimer)
	LEADERBOARDS_READ_CLEAR(lbReadData.m_LeaderboardId,lbReadData.m_Type)
ENDPROC

PROC CHECK_LEADERBOARD_READ_SHOULD_HAVE_TERMINATED()
	
	IF HAS_NET_TIMER_EXPIRED(ongoingSCLB_readInfo.failSafeTimer,LEADERBOARD_READ_FAILSAFE_TIMER,TRUE)
		println("--TERMINATING_LEADERBOARD_READ (TIMEOUT)-- leaderboard ID = ",ongoingSCLB_readInfo.m_LeaderboardID)
		println("leaderboard TYPE = ",ENUM_TO_INT(ongoingSCLB_readInfo.m_type))
		LEADERBOARDS_READ_CLEAR(ongoingSCLB_readInfo.m_LeaderboardId,ongoingSCLB_readInfo.m_Type)
		ongoingSCLB_readInfo.bProcessingLBRead = FALSE
		RESET_NET_TIMER(ongoingSCLB_readInfo.failSafeTimer)
	ENDIF
ENDPROC


FUNC BOOL SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
	IF IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PLAYER_ACTIVE_IN_CORONA()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()
	IF LEADERBOARDS_READ_ANY_PENDING()
	OR ongoingSCLB_readInfo.bProcessingLBRead
		CHECK_LEADERBOARD_READ_SHOULD_HAVE_TERMINATED()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SCLB_CHECK_HANDLES_ARE_THE_SAME(GAMER_HANDLE &gamerHandle1, GAMER_HANDLE &gamerHandle2)
	IF NOT IS_GAMER_HANDLE_VALID(gamerHandle1)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_GAMER_HANDLE_VALID(gamerHandle2)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_ARE_HANDLES_THE_SAME(gamerHandle1, gamerHandle2)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL START_SC_LEADERBOARD_READ_SESSION_PLAYERS_BY_ROW(INT& iLoadStage,BOOL &bSuccessful,Leaderboard2ReadData& lbReadData, Leaderboard2GroupHandle& lbGroups, INT numGroups)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_SESSION_PLAYERS_BY_ROW"
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
			//LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				DEBUG_PRINT_LEADERBOARD_GROUP_HANDLE_STRUCT(procName,lbGroups)
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)
				IF LEADERBOARDS2_READ_SESSION_PLAYERS_BY_ROW(lbReadData, lbGroups,numGroups)
					iLoadStage++
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					println("LEADERBOARDS2_READ_SESSION_PLAYERS_BY_ROW!! start See Conor")
					println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
					//SCRIPT_ASSERT("LEADERBOARDS2_READ_SESSION_PLAYERS_BY_ROW FAILED!! See Conor")
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
					#ENDIF
					bSuccessful = FALSE
					iLoadStage = 3
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(procName,lbReadData)
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW(INT& iLoadStage,BOOL &bSuccessful,Leaderboard2ReadData& lbReadData, Leaderboard2GroupHandle& lbGroups[], INT numGroups, BOOL includeLocalPlayer,INT iStartIndex, INT iNumFriendsRead)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW"
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()//LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				DEBUG_PRINT_LEADERBOARD_GROUP_HANDLE_STRUCT(procName,lbGroups)
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)
				println("LEADERBOARDS2_READ_FRIENDS_BY_ROW is being called with start index: ",iStartIndex, " includeLocalPlayer: ", includeLocalPlayer)
				IF LEADERBOARDS2_READ_FRIENDS_BY_ROW(lbReadData, lbGroups,numGroups,includeLocalPlayer,iStartIndex,iNumFriendsRead)
					iLoadStage++
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					//println()
					println("LEADERBOARDS2_READ_FRIENDS_BY_ROW FAILED!! start See Conor")
					println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
					//println()
					//SCRIPT_ASSERT("LEADERBOARDS2_READ_FRIENDS_BY_ROW FAILED!! See Conor")
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
					#ENDIF
					bSuccessful = FALSE
					iLoadStage=3
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(lbReadData)//"START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW",
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,2)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				//println()
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				///println()
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL START_SC_LEADERBOARD_READ_BY_ROW(INT& iLoadStage
											,BOOL &bSuccessful
											,Leaderboard2ReadData& lbReadData
											,GAMER_HANDLE& lbGamerHandleData[]
											,INT nGamerHandles
											,LeaderboardClanIds& pArray_LBClanIds[]
											,INT nClanIds
											,Leaderboard2GroupHandle& lbGroups[]
											,INT numGroups)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_BY_ROW"
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED() //LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				DEBUG_PRINT_LEADERBOARD_GROUP_HANDLE_STRUCT(procName,lbGroups)
				
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)

				IF LEADERBOARDS2_READ_BY_ROW(lbReadData, lbGamerHandleData, nGamerHandles, pArray_LBClanIds, nClanIds, lbGroups, numGroups)
					iLoadStage++
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					//println()
					println("LEADERBOARDS2_READ_BY_ROW FAILED!! start See Conor")
					println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
					//println()
					//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_ROW FAILED!! See Conor")
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
					#ENDIF
					bSuccessful = FALSE
					iLoadStage=3
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(lbReadData)//"START_SC_LEADERBOARD_READ_BY_ROW",
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				//println()
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				//println()
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL START_SC_LEADERBOARD_READ_BY_HANDLE(INT& iLoadStage,BOOL &bSuccessful, Leaderboard2ReadData& lbReadData, GAMER_HANDLE& handleData)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_BY_HANDLE"
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()//LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)
				IF LEADERBOARDS2_READ_BY_HANDLE(lbReadData, handleData)
					iLoadStage++
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					//println()
					println("LEADERBOARDS2_READ_BY_HANDLE FAILED!! start See Conor")
					println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
					//println()
					//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_HANDLE FAILED!! See Conor")
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
					#ENDIF
					bSuccessful = FALSE
					iLoadStage=3
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(lbReadData)//"START_SC_LEADERBOARD_READ_BY_HANDLE",
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,2)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				//println()
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				//println()
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL START_SC_LEADERBOARD_READ_BY_RANK(INT& iLoadStage,BOOL &bSuccessful,Leaderboard2ReadData& lbReadData, INT rankStart, INT numRows)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_BY_RANK"
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()// LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
				#IF IS_DEBUG_BUILD
				println(procName," Rank Start ",RankStart, " numRows",numRows)
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)
				IF LEADERBOARDS2_READ_BY_RANK(lbReadData, rankStart, numRows)
					iLoadStage++
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					//println()
					println("LEADERBOARDS2_READ_BY_RANK FAILED!! start See Conor")
					println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
					//println()
					//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_RANK FAILED!! See Conor")
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
					#ENDIF
					bSuccessful = FALSE
					iLoadStage=3
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(lbReadData)//"START_SC_LEADERBOARD_READ_BY_RANK",
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,2)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				//println()
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				//println()
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL START_SC_LEADERBOARD_READ_BY_RADIUS(INT& iLoadStage,BOOL &bSuccessful,Leaderboard2ReadData& lbReadData, INT radius, GAMER_HANDLE& pivotGamerHandle,BOOL bRankPrediction = FALSE, BOOL bUseRankPredictOverride = FALSE,INT iRPOverride = 0, FLOAT fRPOverride = 0.0)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_BY_RADIUS"
	#ENDIF
	INT iScore = sclb_rank_predict.combinedResult.m_iColumnData[0]
	FLOAT fScore = sclb_rank_predict.combinedResult.m_fColumnData[0]
	IF bUseRankPredictOverride
		iScore = iRPOverride
		fScore = fRPOverride
	ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()//LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)
				IF bRankPrediction
					IF IS_BIT_SET(sclb_rank_predict.currentResult.m_ColumnsBitSets, 0)
						println("LEADERBOARDS2_READ_BY_SCORE_INT: reading for int: ",iScore )
						IF LEADERBOARDS2_READ_BY_SCORE_INT(lbReadData, iScore, radius)
							iLoadStage++
							#IF IS_DEBUG_BUILD
							DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							//println()
							println("LEADERBOARDS2_READ_BY_RADIUS PREDICTION (INT) FAILED!! start See Conor")
							println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
							//println()
							//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_RADIUS PREDICTION (INT) FAILED!! See Conor")
							DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
							DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
							#ENDIF
							bSuccessful = FALSE
							iLoadStage=3
						ENDIF
					ELSE
						println("LEADERBOARDS2_READ_BY_SCORE_INT: reading for float: ",fScore )
						IF LEADERBOARDS2_READ_BY_SCORE_FLOAT(lbReadData, fScore , radius)
							iLoadStage++
							#IF IS_DEBUG_BUILD
							DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							//println()
							println("LEADERBOARDS2_READ_BY_RADIUS PREDICTION (FLOAT)  FAILED!! start See Conor")
							println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
							//println()
							//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_RADIUS PREDICTION (FLOAT)  FAILED!! See Conor")
							DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
							DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
							#ENDIF
							bSuccessful = FALSE
							iLoadStage=3
						ENDIF
					ENDIF
				ELSE
					IF LEADERBOARDS2_READ_BY_RADIUS(lbReadData, radius, pivotGamerHandle)
						iLoadStage++
						#IF IS_DEBUG_BUILD
						DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						//println()
						println("LEADERBOARDS2_READ_BY_RADIUS FAILED!! start See Conor")
						println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
						//println()
						//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_RADIUS FAILED!! See Conor")
						DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
						DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
						#ENDIF
						bSuccessful = FALSE
						iLoadStage=3
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(lbReadData)//"START_SC_LEADERBOARD_READ_BY_RADIUS",
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				//println()
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				//println()
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_ROW_DATA(SC_LEADERBOARD_DISPLAY_ROW_STRUCT rowData)
	INT j
	IF g_bOutputSCLeaderboardData
		//println()
		//println("PRINT_ROW_DATA")
		println("row data gamer name: ",rowData.tl23_ParticipantName)
		println("row data rank: ",rowData.iRank)
		REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS j
			println("FLOAT Column # ", j, " = ", rowData.fColumnData[j])
		ENDREPEAT
		REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS j
			println("INT Column # ", j, " = ", rowData.iColumnData[j])
		ENDREPEAT
		println("row data is valid : ", rowData.bValidData)
		println("END - PRINT_ROW_DATA")	
	ENDIF
ENDPROC
#ENDIF

PROC CLEAR_FINAL_DATA_STRUCT(SC_FINAL_DATA &finalData[])
	GAMER_HANDLE emptyGamer
	INT i, j
	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
		finalData[i].rowData.tl23_ParticipantName = ""
		finalData[i].rowData.tl31_CoDriver = ""
		finalData[i].rowData.playerGamerHandle = emptyGamer
		finalData[i].rowData.coDriverGamerHandle = emptyGamer
		finalData[i].rowData.bCustomVehicle = FALSE
		finalData[i].rowData.iRank = 0
		REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS j
			finalData[i].rowData.fColumnData[j] = 0
			finalData[i].rowData.iColumnData[j] = 0	
		ENDREPEAT
		finalData[i].iSection = 0
		finalData[i].rowData.bValidData = FALSE
		finalData[i].iCrewTagLoadStage = 0
		finalData[i].iCoDriverNameLoadStage = 0
		finalData[i].iRunUpdateGamerTagsLoadStage = 0
		finalData[i].iDisplayNameRequestID = 0
		finalData[i].crewTag = ""
	ENDREPEAT
	RESET_NET_TIMER(scLB_DisplayedData.failSafeTimer)
	PRINTLN("CLEAR_FINAL_DATA_STRUCT- Called")
ENDPROC

FUNC BOOL IS_SC_LEADERBOARD_A_RACE(INT iLeaderboardID)
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES_LAPS)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES_LAPS)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY_LAPS)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_ON_FOOT_RACE)
	//OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_ON_FOOT_LAPS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SC_LEADERBOARD_A_RALLY(INT iLeaderboardID)
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY_LAPS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT SC_LC_GENERATE_ID_FOR_CACHE(TEXT_LABEL_31 tl_UGC_name, INT iLeaderboard,INT iMissionType, INT iSubType = -1, INT iLaps = -1, BOOL bCoDriver = FALSE)
	TEXT_LABEL_63 tempString
	tempString = tl_UGC_name
	tempString += iLeaderBoard
	tempString += "_"
	IF IS_SC_LEADERBOARD_A_RALLY(iLeaderboard)
		IF bCoDriver 
			tempString += "CoDri"
		ELSE
			tempString += "Dri"
		ENDIF
		tempString += "_"
	ENDIF
	tempString += iMissionType
	tempString += "_"
	tempString += iSubType
	tempString += "_"
	tempString += ilaps
	RETURN GET_HASH_KEY(tempString)
ENDFUNC

FUNC BOOL IS_SC_LB_VALUE_INVALID(INT iValue, FLOAT fValue)
	IF iValue = HIGHEST_INT
	OR iValue = -HIGHEST_INT
		RETURN TRUE
	ENDIF
	IF fValue = TO_FLOAT(HIGHEST_INT)
	OR fValue = TO_FLOAT(-HIGHEST_INT)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PROC CLEAR_ROW_DATA_STRUCT(LeaderboardRowData& tempRowData[])
//	LeaderboardRowData clearData
//	tempRowData[0] = clearData
//ENDPROC

PROC CLEAR_ROW_DATA_INFO_STRUCT(LeaderboardRowData& tempRowData)
	tempRowData.m_GamerHandle.Data1 = 0
	tempRowData.m_GamerHandle.Data2 = 0
	tempRowData.m_GamerHandle.Data3 = 0
	tempRowData.m_GamerHandle.Data4 = 0
	tempRowData.m_GamerHandle.Data5 = 0
	tempRowData.m_GamerHandle.Data6 = 0
	tempRowData.m_GamerHandle.Data7 = 0
	tempRowData.m_GamerHandle.Data8 = 0
	tempRowData.m_GamerHandle.Data9 = 0
	tempRowData.m_GamerHandle.Data10 = 0
	tempRowData.m_GamerHandle.Data11 = 0
	tempRowData.m_GamerHandle.Data12 = 0
	tempRowData.m_GamerHandle.Data13 = 0
	tempRowData.m_GamerName = ""
	tempRowData.m_GroupSelector.m_NumGroups = 0
	INT i
	REPEAT LEADERBOARD_MAX_GROUPS i
		tempRowData.m_GroupSelector.m_Group[i].m_Category = ""
		tempRowData.m_GroupSelector.m_Group[i].m_Id = ""
	ENDREPEAT
	tempRowData.m_ClanId = 0
	tempRowData.m_ClanName = ""
	tempRowData.m_ClanTag = ""
	tempRowData.m_Rank = 0
	tempRowData.m_NumColumnValues = 0
ENDPROC

PROC FILL_READ_INFO_STRUCT(LeaderboardReadInfo& readInfo,Leaderboard2ReadData &ReadDataStruct)
	readInfo.m_LeaderboardId = ReadDataStruct.m_LeaderboardId
	readInfo.m_LeaderboardType = ENUM_TO_INT(ReadDataStruct.m_Type)
	readInfo.m_LeaderboardIndex = 0
ENDPROC



FUNC BOOL IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
	IF sclb_useRankPrediction
	AND sclb_rankPredictionDataValid
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control, SC_LEADERBOARD_DISPLAY_ROW_STRUCT &rowData, INT iRank #IF IS_DEBUG_BUILD, STRING funcName,INT iSection #endif )
	//println("FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT: filling in data for a row from the combined rank prediction result")
	INT iCustomCar
	INT i
	IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
		rowData.tl23_ParticipantName = sclb_rank_predict.DriverName
		rowData.playerGamerHandle = sclb_rank_predict.driverHandle
		println("CODRIVER NAME =  ", sclb_rank_predict.coDriverName)
		rowData.tl31_CoDriver = sclb_rank_predict.coDriverName
	ELSE
		rowData.tl23_ParticipantName = GET_PLAYER_NAME(PLAYER_ID())
		rowData.playerGamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	ENDIF
	rowData.iRank = iRank
	IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
		println("COMBINED_PREDICTION_RESULT: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
		iCustomCar =  sclb_rank_predict.combinedResult.m_iColumnData[scLB_DisplayedData.iCustomVehicleColumn]
		IF iCustomCar = 1
			rowData.bCustomVehicle = TRUE
		ELSE
			rowData.bCustomVehicle = FALSE
		ENDIF
	ENDIF
	rowData.bValidData = TRUE
	REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  i
		println("COMBINED_PREDICTION_RESULT: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iReadColumns[i])
		//IF IS_BIT_SET(sclb_rank_predict.currentResult.m_ColumnsBitSets,scLB_DisplayedData.iReadColumns[i])
		IF IS_BIT_SET(sclb_rank_predict.currentResult.m_ColumnsBitSets,scLB_DisplayedData.iReadColumns[i])
		//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
			rowData.iColumnData[i] = sclb_rank_predict.combinedResult.m_iColumnData[scLB_DisplayedData.iReadColumns[i]]
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Row: ",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[i],
												rowData.iColumnData[i],0.0,FALSE,TRUE )
			#ENDIF
		ELSE
			rowData.fColumnData[i] = sclb_rank_predict.combinedResult.m_fColumnData[scLB_DisplayedData.iReadColumns[i]]
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Row: ",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[i],
												0,rowData.fColumnData[i],TRUE,TRUE )
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//FUNC INT GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(INT iSection, INT iPlayerInitialRank, INT iCurrentRowRank)
//	IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
//	AND scLB_DisplayedData.iSectionEntries[iSection] > scLB_DisplayedData.iPlayerIndexForSection[iSection]
//	AND scLB_DisplayedData.iPlayerIndexForSection[iSection] >= 0
//		IF iPlayerInitialRank != scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection]].iRank
//			IF iCurrentRowRank >= scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection]].iRank
//				RETURN 1
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN 0
//ENDFUNC

PROC CALCULATE_OFFSETS_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(INT iSection, INT iPlayerInitialRank)
	INT i
	println("CALCULATE_OFFSETS_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION")
	IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
	AND scLB_DisplayedData.iSectionEntries[iSection] > scLB_DisplayedData.iPlayerIndexForSection[iSection]
	AND scLB_DisplayedData.iPlayerIndexForSection[iSection] >= 0
		IF iPlayerInitialRank != scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection]].iRank
			REPEAT scLB_DisplayedData.iSectionEntries[iSection] i
				IF i != scLB_DisplayedData.iPlayerIndexForSection[iSection]
					//if row 
					IF scLB_DisplayedData.rowData[iSection][i].iRank >= scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection]].iRank
						IF scLB_DisplayedData.rowData[iSection][i].iRank < iPlayerInitialRank OR iPlayerInitialRank = -1
							println("offseting Rank for iSection: ",iSection,"iRow :",i)
							println("OLD RANK: ",scLB_DisplayedData.rowData[iSection][i].iRank)
							scLB_DisplayedData.rowData[iSection][i].iRank = scLB_DisplayedData.rowData[iSection][i].iRank + 1
							println("NEW RANK: ",scLB_DisplayedData.rowData[iSection][i].iRank)
						#IF IS_DEBUG_BUILD
						ELSE
							println("scLB_DisplayedData.rowData[",iSection,"][",i,"].iRank = ",scLB_DisplayedData.rowData[iSection][i].iRank)
							println("iPlayerInitialRank = ",iPlayerInitialRank)
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						println("scLB_DisplayedData.rowData[",iSection,"][",i,"].iRank = ", scLB_DisplayedData.rowData[iSection][i].iRank)
						println("scLB_DisplayedData.rowData[",iSection,"][",scLB_DisplayedData.iPlayerIndexForSection[iSection],"].iRank = ",scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection]].iRank)
					#ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			println("iPlayerInitialRank = ",iPlayerInitialRank)
		ENDIF
	ELSE
		IF NOT IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
			println("Rank prediction NOT valid")
		ENDIF
		IF scLB_DisplayedData.iSectionEntries[iSection] <= scLB_DisplayedData.iPlayerIndexForSection[iSection]
			println("scLB_DisplayedData.iSectionEntries[iSection] = ", scLB_DisplayedData.iSectionEntries[iSection])
			println("scLB_DisplayedData.iPlayerIndexForSection[iSection] = ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
		ENDIF
		IF scLB_DisplayedData.iPlayerIndexForSection[iSection] < 0
			println("scLB_DisplayedData.iPlayerIndexForSection[iSection] < 0")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_WE_HAVE_STORED_RANK_PREDICTED_DATA_FOR_THIS_READ(Leaderboard2ReadData readData)
	INT i
	IF sclb_useRankPrediction
		println("DO_WE_HAVE_STORED_RANK_PREDICTED_DATA_FOR_THIS_READ")
		IF sclb_rank_predict.LBWriteDetails.m_LeaderboardId != 0
			IF sclb_rank_predict.LBWriteDetails.m_LeaderboardId = readData.m_LeaderboardId
				IF sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_NumGroups = readData.m_GroupSelector.m_NumGroups
					REPEAT readData.m_GroupSelector.m_NumGroups i
						IF NOT ARE_STRINGS_EQUAL(sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[i].m_Category, readData.m_GroupSelector.m_Group[i].m_Category)
						OR NOT ARE_STRINGS_EQUAL(sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[i].m_Id, readData.m_GroupSelector.m_Group[i].m_Id)
							println("FALSE - sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[",i,"].m_Category = ", sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[i].m_Category)
							println("readData.m_GroupSelector.m_Group[",i,"].m_Category = ", readData.m_GroupSelector.m_Group[i].m_Category)
							println("sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[",i,"].m_Id = ", sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_Group[i].m_Id)
							println("readData.m_GroupSelector.m_Group[",i,"].m_Id = ",  readData.m_GroupSelector.m_Group[i].m_Id)
							RETURN FALSE
						ENDIF
					ENDREPEAT
					println("TRUE")
					RETURN TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					println("FALSE -sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_NumGroups = ",sclb_rank_predict.LBWriteDetails.m_GroupSelector.m_NumGroups)
					println(" readData.m_GroupSelector.m_NumGroups= ", readData.m_GroupSelector.m_NumGroups)
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				println("FALSE -sclb_rank_predict.LBWriteDetails.m_LeaderboardId = ",sclb_rank_predict.LBWriteDetails.m_LeaderboardId)
				println("readData.m_LeaderboardId = ",readData.m_LeaderboardId)
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			println("FALSE -sclb_rank_predict.LBWriteDetails.m_LeaderboardId = ",sclb_rank_predict.LBWriteDetails.m_LeaderboardId)
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		println("sclb_useRankPrediction = FALSE")
	#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/*
Global OR Crew read

11 Radius around player

If 0 rows are returned 
	Read rank 1-11
Else
	if radius !include rank 1
		read rank 1
	endif
endif

Friend read
Get all friend data including player - READ 1

Loop through all friend data.
	Store rank 1
	
	Loop through until you find player then get rows above/below
	(there is probably a nicer way to do this but just to get things working again)

*/
FUNC BOOL SOCIAL_CLUB_GET_GLOBAL_LB_DATA(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control )
	GAMER_HANDLE localPlayerHandle
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo

	//INT iRankStart
	INT i, j
	INT iCustomCar
	
	INT iPlayerRow = -1
	INT iRowStart
	INT iIndex
	
	BOOL bRankOneInRadius
	#IF IS_DEBUG_BUILD
	STRING funcName = "SOCIAL_CLUB_GET_GLOBAL_LB_DATA"
	#ENDIF
	INT iSection = SC_LB_SECTION_GLOBAL
	
	BOOL bReplaceFirstSlot
	
	INT iRankRowsToRead
	//INT iOffsetRank
	localPlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	SWITCH scLB_control.iLoadStage[iSection]
		//radius around player
		CASE 0 
			scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
			scLB_DisplayedData.iSectionEntries[iSection] = 0
			sclb_rank_predict.iRankBeforePrediction = -1
			sclb_rankPredictionDataValid = FALSE
			IF DO_WE_HAVE_STORED_RANK_PREDICTED_DATA_FOR_THIS_READ(scLB_control.ReadDataStruct)
				IF NOT sclb_rankPredictionDataValid
					println("DO_WE_HAVE_STORED_RANK_PREDICTED_DATA_FOR_THIS_READ = TRUE setting flag to valid")
					sclb_rankPredictionDataValid = TRUE
				ENDIF
			ELSE
				IF sclb_rankPredictionDataValid
					println("DO_WE_HAVE_STORED_RANK_PREDICTED_DATA_FOR_THIS_READ = FALSE setting flag to NOT valid")
					sclb_rankPredictionDataValid = FALSE
				ENDIF
			ENDIF
			IF NOT sclb_rankPredictionDataValid
				println("DO_WE_HAVE_STORED_RANK_PREDICTED_DATA_FOR_THIS_READ = FALSE")
			ENDIF
			
			IF START_SC_LEADERBOARD_READ_BY_RADIUS(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct,11,localPlayerHandle,IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ())
				#IF IS_DEBUG_BUILD
				DEBUG_START_LEADERBOARD_PRINT_RESULTS(funcName,scLB_control.ReadDataStruct)
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
				IF scLB_control.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					println(funcName, ":readInfo.m_LocalGamerRowNumber = ",readInfo.m_LocalGamerRowNumber)
					IF readInfo.m_ReturnedRows > 0
						iIndex = 0
						i = 0
						//RANK PREDICTION
						IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							REPEAT readInfo.m_ReturnedRows iIndex
								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
								IF sclb_rank_predict.iRankBeforePrediction < 0
									//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
									//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName)
									IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
									OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle)
										sclb_rank_predict.iRankBeforePrediction =initRowData.m_Rank
										println(funcName, " setting players rank before prediction = ",sclb_rank_predict.iRankBeforePrediction) 
									ENDIF
								ENDIF
								IF iPlayerRow < 0
									IF IS_BIT_SET(sclb_rank_predict.currentResult.m_ColumnsBitSets,0)
										println("Players score: ", sclb_rank_predict.combinedResult.m_iColumnData[0])
										println("Returned Row# ",iIndex, " score: ", LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0))
										IF sclb_rank_predict.combinedResult.m_iColumnData[0] >= LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0) 
											iPlayerRow = iIndex
											println(funcName, " Players score is higher than returned result setting player row to ", iPlayerRow)
											
										ENDIF
									ELSE
										println("Players score: ", sclb_rank_predict.combinedResult.m_fColumnData[0])
										println("Returned Row# ",iIndex, " score: ", LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, 0))
										IF sclb_rank_predict.combinedResult.m_fColumnData[0] >= LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, 0)
											iPlayerRow = iIndex
											println(funcName, " Players score is higher than returned result setting player row to ", iPlayerRow)
											
										ENDIF
									ENDIF
								ENDIF
								CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							ENDREPEAT
							IF iPlayerRow < 0
								iPlayerRow = readInfo.m_ReturnedRows
							ENDIF
						ENDIF
						iIndex = 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						IF initRowData.m_Rank <= 1
						//OR (iPlayerRow = 0 AND initRowData.m_Rank <= 1)
							println(funcName, ": Rank 1 included in radius search grabbing now")
							//1st Place
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								AND iPlayerRow = 0
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],initRowData.m_Rank #IF IS_DEBUG_BUILD ,funcName,iSection#endif )
									//iPlayerRow = 0
									scLB_DisplayedData.iPlayerIndexForSection[iSection] = 0
									#IF IS_DEBUG_BUILD
									PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
									#ENDIF
									scLB_DisplayedData.iSectionEntries[iSection]++
									println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
								ENDIF
							
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
								OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
								//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
								
								ELSE
									
									//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
									IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
										iPlayerRow = 0
										scLB_DisplayedData.iPlayerIndexForSection[iSection] = 0
									ENDIF
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = initRowData.m_Rank
									IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
										println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
										iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,scLB_DisplayedData.iCustomVehicleColumn)
										IF iCustomCar = 1
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
										ELSE
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
										ENDIF
									ENDIF
									IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
										println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
										scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
									ENDIF
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
									REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  i
										println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iReadColumns[i])
				      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[i])
										//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, scLB_DisplayedData.iReadColumns[i])
											#IF IS_DEBUG_BUILD
											DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Rank 1",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[i],
																				scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i],0.0,FALSE )
											#ENDIF
										ELSE
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(0, scLB_DisplayedData.iReadColumns[i])
											#IF IS_DEBUG_BUILD
											DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Rank 1",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[i],
																				0,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i],TRUE )
											#ENDIF
										ENDIF
									ENDREPEAT
									//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
									//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
									//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
									#IF IS_DEBUG_BUILD
									PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
									#ENDIF
									scLB_DisplayedData.iSectionEntries[iSection]++
									println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
								ENDIF
								bRankOneInRadius = TRUE
							ENDIF
						ENDIF
						IF NOT bRankOneInRadius
							scLB_DisplayedData.iSectionEntries[iSection]++
							println(funcName,": adding data to section number of entries now (FAKE TO ALLOW 1st PLACE LATER) : ",scLB_DisplayedData.iSectionEntries[iSection] )
						ENDIF
						IF NOT IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							iPlayerRow = readInfo.m_LocalGamerRowNumber
							println(funcName,": NOT IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ() iPlayerRow: ",iPlayerRow )
						ENDIF
						IF iPlayerRow > 6
							iRowStart = iPlayerRow - 6
						ELSE
							IF bRankOneInRadius
								iRowStart = 1
							ELSE
								iRowStart = 0
							ENDIF
						ENDIF
						iIndex = iRowStart 
						CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
						FOR iIndex = iRowStart TO readInfo.m_ReturnedRows-1

							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
							//Get data about row outside of getting data for each column?
							println(funcName,": initRowData.iRank = ", initRowData.m_Rank)
							println(funcName,": scLB_DisplayedData.iSectionEntries[iSection] = ", scLB_DisplayedData.iSectionEntries[iSection])
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
							AND initRowData.m_Rank > 1
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								AND iPlayerRow = iIndex	
									//IF NOT ARE_STRINGS_EQUAL(scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName,GET_PLAYER_NAME(PLAYER_ID()))
									IF NOT SCLB_CHECK_HANDLES_ARE_THE_SAME(scLB_DisplayedData.rowData[iSection][0].playerGamerHandle,initRowData.m_GamerHandle)
										FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],initRowData.m_Rank#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
										scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
										println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
										
										#IF IS_DEBUG_BUILD
										PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
										#ENDIF
										scLB_DisplayedData.iSectionEntries[iSection]++
										
										println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
									ENDIF
								ENDIF  //if we are using rank prediction there is no record for local player so its an extra one in the middle
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
								AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
								OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
									println(funcName,": NOT adding data to section as it's equal to prediction handle") 
								ELSE
									IF scLB_DisplayedData.iSectionEntries[iSection] < 11
										//IF NOT ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
										//AND NOT ARE_STRINGS_EQUAL(scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName,initRowData.m_GamerName)
											//IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(PLAYER_ID()),initRowData.m_GamerName)
										IF IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
										AND NOT SCLB_CHECK_HANDLES_ARE_THE_SAME(scLB_DisplayedData.rowData[iSection][0].playerGamerHandle,initRowData.m_GamerHandle)
											IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
												IF scLB_DisplayedData.iPlayerIndexForSection[iSection] < 0
													scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
													println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
												ENDIF
											ENDIF
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = initRowData.m_Rank
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
											IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
												iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, scLB_DisplayedData.iCustomVehicleColumn)
												IF iCustomCar = 1
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
												ENDIF
											ENDIF
											IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
												scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
											ENDIF
											i = 0
											REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  i
												IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[i])
												//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, scLB_DisplayedData.iReadColumns[i])
													println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iReadColumns[i], " Value = ",scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",iIndex,scLB_DisplayedData.iReadColumns[i],
																						scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i],0.0,FALSE )
													#ENDIF
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, scLB_DisplayedData.iReadColumns[i])
													println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iReadColumns[i], " Value = ",scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i])	
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",iIndex,scLB_DisplayedData.iReadColumns[i],
																						0,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i],TRUE )
													#ENDIF
												ENDIF
											ENDREPEAT
											//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
											//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
											//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
											#IF IS_DEBUG_BUILD
											PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
											#ENDIF
											scLB_DisplayedData.iSectionEntries[iSection]++
											println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
										ELSE
											println(funcName, " gamer hande is not valid or matches slot 0 ")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
						ENDFOR
						
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						
						IF bRankOneInRadius
							println(funcName," --Rank 1 included in radius-- moving to stage 2 grabbed ") 
							IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
							AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																					scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1 #IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								ELSE
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																					1 #IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								ENDIF
								scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
								println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
								
								#IF IS_DEBUG_BUILD
								IF scLB_DisplayedData.iSectionEntries[iSection] < 11
									PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
								ENDIF
								#ENDIF
								scLB_DisplayedData.iSectionEntries[iSection]++
								
								println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
							ENDIF
							scLB_control.iLoadStage[iSection] = 2
						ELSE
							println(funcName," -- moving to stage 1 grabbed ") 
							scLB_control.iLoadStage[iSection] = 1	
						ENDIF
					ELSE
						IF NOT bRankOneInRadius
							scLB_DisplayedData.iSectionEntries[iSection]++
							println(funcName,": adding data to section number of entries now (FAKE TO ALLOW 1st PLACE LATER) : ",scLB_DisplayedData.iSectionEntries[iSection] )
						ENDIF
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
						scLB_control.iLoadStage[iSection] = 1
						println(funcName," -- no data available for local player moving to stage 1(returned rows is 0)") 
					ENDIF
				ELSE
					//LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
					scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
					scLB_control.iLoadStage[iSection] = 1
					SET_BIT(scLB_DisplayedData.iReadFailedBS,iSection)
					println(funcName," -- no data available for local player moving to stage 1 (READ FAILED) ") 
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			//ENDIF
			IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
				iRankRowsToRead = 11
			ELSE
				iRankRowsToRead = 1
			ENDIF
			IF START_SC_LEADERBOARD_READ_BY_RANK(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct, 1,iRankRowsToRead)
				#IF IS_DEBUG_BUILD
				DEBUG_START_LEADERBOARD_PRINT_RESULTS(funcName,scLB_control.ReadDataStruct)
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
				IF scLB_control.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						REPEAT readInfo.m_ReturnedRows i
							println(funcName," start loop counter: ", i) 
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(i, initRowData)
							IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
							//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
							AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
							OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
								println(funcName," data already in stored via rank prediction by passing")
							ELSE
								bReplaceFirstSlot = FALSE
								IF scLB_DisplayedData.rowData[iSection][0].iRank > 1
								OR scLB_DisplayedData.rowData[iSection][0].iRank <= 0
									bReplaceFirstSlot = TRUE
									println(funcName," replacing slot 0 as rank 1 is not in")
								ENDIF
								IF scLB_DisplayedData.iSectionEntries[iSection] < 11
								OR bReplaceFirstSlot
									IF i = 0
									OR bReplaceFirstSlot
										scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName = initRowData.m_GamerName
										scLB_DisplayedData.rowData[iSection][0].playerGamerHandle = initRowData.m_GamerHandle
										scLB_DisplayedData.rowData[iSection][0].iRank = initRowData.m_Rank
										IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
											println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
											iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,scLB_DisplayedData.iCustomVehicleColumn)
											IF iCustomCar = 1
												scLB_DisplayedData.rowData[iSection][0].bCustomVehicle = TRUE
											ELSE
												scLB_DisplayedData.rowData[iSection][0].bCustomVehicle = FALSE
											ENDIF
										ENDIF
										IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
											println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
											scLB_DisplayedData.rowData[iSection][0].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
										ENDIF
										scLB_DisplayedData.rowData[iSection][0].bValidData = TRUE
										j = 0
										REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  j
											println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", i, " Column: ", scLB_DisplayedData.iReadColumns[j])
					      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[j])
											//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
												scLB_DisplayedData.rowData[iSection][i].iColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(i, scLB_DisplayedData.iReadColumns[j])
												#IF IS_DEBUG_BUILD
												DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Rank 1",i,scLB_DisplayedData.iReadColumns[j],
																					scLB_DisplayedData.rowData[iSection][i].iColumnData[j],0.0,FALSE )
												#ENDIF
											ELSE
												scLB_DisplayedData.rowData[iSection][i].fColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(i, scLB_DisplayedData.iReadColumns[j])
												#IF IS_DEBUG_BUILD
												DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Rank 1",i,scLB_DisplayedData.iReadColumns[j],
																					0,scLB_DisplayedData.rowData[iSection][i].fColumnData[j],TRUE )
												#ENDIF
											ENDIF
										ENDREPEAT
										IF scLB_DisplayedData.iSectionEntries[iSection] = 0
											//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
											//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
											//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
											#IF IS_DEBUG_BUILD
											PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
											#ENDIF
											IF bReplaceFirstSlot
												println(funcName,": Replacing first entry with rank 1 player as it was not included")
											ELSE
												scLB_DisplayedData.iSectionEntries[iSection]++
											ENDIF
											println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
										ELSE
											//1943783
											IF scLB_DisplayedData.iSectionEntries[iSection] = 1
											AND scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
												#IF IS_DEBUG_BUILD
												PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
												#ENDIF

												scLB_DisplayedData.iSectionEntries[iSection]++
												println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
											ENDIF
										ENDIF
									ELSE
										IF scLB_DisplayedData.iSectionEntries[iSection] < 11
											//Get data about row outside of getting data for each column?
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
											IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INT: Row: ", i, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
												iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(i,scLB_DisplayedData.iCustomVehicleColumn)
												IF iCustomCar = 1
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
												ENDIF
											ENDIF
											IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
												scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
											ENDIF
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = initRowData.m_Rank
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
											j = 0
											REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay j
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", i, " Column: ", scLB_DisplayedData.iReadColumns[j])
						      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[j])
												//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[j])
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(i, scLB_DisplayedData.iReadColumns[j])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",i,scLB_DisplayedData.iReadColumns[j],
																						scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[j],0.0,FALSE )
													#ENDIF
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(i, scLB_DisplayedData.iReadColumns[j])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",i,scLB_DisplayedData.iReadColumns[j],
																						0,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[j],TRUE )
													#ENDIF
												ENDIF
											ENDREPEAT
											IF i != 0
												//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
												//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
												//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
												#IF IS_DEBUG_BUILD
												PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
												#ENDIF
												scLB_DisplayedData.iSectionEntries[iSection]++
												println(funcName,": adding data to global section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							//println("Looping through global data end loop counter: ", i) 
						ENDREPEAT
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
					println(funcName," -- moving to stage 2 grabbed ") 
					scLB_control.iLoadStage[iSection] = 2		
				ELSE
					SET_BIT(scLB_DisplayedData.iReadFailedBS,iSection)
					println(funcName," -- moving to end no one in first so no data at all (READ FAILED) ") 
					//LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
					scLB_DisplayedData.iSectionEntries[iSection] = 0
					scLB_control.iLoadStage[iSection] = 2
					scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] = 0
					scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
					scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_CREW] = 0
					scLB_control.iLoadStage[SC_LB_SECTION_CREW] = 3
				ENDIF
			ENDIF
			IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
			AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
				IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
					FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																	scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
				ELSE
					FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																	1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
				ENDIF
				scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
				println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
				
				#IF IS_DEBUG_BUILD
				PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
				#ENDIF
				scLB_DisplayedData.iSectionEntries[iSection]++
				
				println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
			ENDIF
		BREAK
		//Finished
		CASE 2
			println(funcName,": doing rank offset for prediction. Player's rank before prediction =  ",sclb_rank_predict.iRankBeforePrediction ) 
			CALCULATE_OFFSETS_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
			scLB_control.iLoadStage[iSection] = 3
		BREAK
		
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//Friend read
//Get all friend data including player - READ 1
//
//Loop through all friend data.
//	Store rank 1
//	
//	Loop through until you find player then get rows above/below
//	(there is probably a nicer way to do this but just to get things working again)
FUNC BOOL SOCIAL_CLUB_GET_FRIEND_LB_DATA(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control )
	//Leaderboard2ReadData emptyStruct
	//LeaderboardRowData tempRowData[1]
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	INT i//, j
	INT iNumOfFriends
	INT iPlayerRow = -1
	//INT iTotalRows
	INT iRowStart
	INT iIndex
	INT iCustomCar
	BOOL bIncludeLocalPlayer
	#IF IS_DEBUG_BUILD
	STRING funcName = "SOCIAL_CLUB_GET_FRIEND_LB_DATA"
	#ENDIF
	INT iSection = SC_LB_SECTION_FRIEND
	//INT iOffsetRank
	GAMER_HANDLE localPlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	SWITCH scLB_control.iLoadStage[SC_LB_SECTION_FRIEND]
		CASE 0
			//getting information about local player
			iNumOfFriends = NETWORK_GET_FRIEND_COUNT()
			//inited to -1
			scLB_DisplayedData.iPlayerIndexForSection[SC_LB_SECTION_FRIEND] = -1
			sclb_rank_predict.iRankBeforePrediction = -1
			scLB_DisplayedData.iSectionEntries[iSection] = 0
			IF iNumOfFriends  > 0
				IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
					bIncludeLocalPlayer= FALSE
				ELSE
					bIncludeLocalPlayer = TRUE
				ENDIF
				IF START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct,scLB_control.groupHandle,scLB_control.groupHandle[0].m_NumGroups,bIncludeLocalPlayer,0,100)
					#IF IS_DEBUG_BUILD
					DEBUG_START_LEADERBOARD_PRINT_RESULTS("SOCIAL_CLUB_GET_FRIEND_LB_DATA",scLB_control.ReadDataStruct)
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
					IF scLB_control.bTempReadResult
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						println("SOCIAL_CLUB_GET_FRIEND_LB_DATA returned ",readInfo.m_ReturnedRows ," rows. local player row: ",readInfo.m_LocalGamerRowNumber)
						
						//RANK PREDICTION
						IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							iIndex = 0
							REPEAT readInfo.m_ReturnedRows iIndex
								IF iPlayerRow < 0
									LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
									IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
										IF sclb_rank_predict.iRankBeforePrediction < 0
											///IF ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName)
											IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle)
												sclb_rank_predict.iRankBeforePrediction = initRowData.m_Rank
												println(funcName, " setting players rank before prediction = ",sclb_rank_predict.iRankBeforePrediction) 
											ENDIF
										ENDIF
									ENDIF
									IF IS_BIT_SET(sclb_rank_predict.currentResult.m_ColumnsBitSets,0)
										println("Players score: ", sclb_rank_predict.combinedResult.m_iColumnData[0])
										println("Returned Row# ",iIndex, " score: ", LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0))
										IF sclb_rank_predict.combinedResult.m_iColumnData[0] >= LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0) 
											iPlayerRow = iIndex
											println(funcName, " Players score is higher than returned result setting player row to ", iPlayerRow)
											
										ENDIF
									ELSE
										println("Players score: ", sclb_rank_predict.combinedResult.m_fColumnData[0])
										println("Returned Row# ",iIndex, " score: ", LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, 0))
										IF sclb_rank_predict.combinedResult.m_fColumnData[0] >= LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, 0)
											iPlayerRow = iIndex
											println(funcName, " Players score is higher than returned result setting player row to ", iPlayerRow)
											
										ENDIF
									ENDIF
									CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
								ENDIF
							ENDREPEAT
							IF iPlayerRow < 0
								iPlayerRow = readInfo.m_ReturnedRows
							ENDIF
						ENDIF
						iIndex = 0
						//1st Place
						IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
						AND iPlayerRow = 0
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
								FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								//iPlayerRow = 0
								scLB_DisplayedData.iPlayerIndexForSection[iSection] = 0
								#IF IS_DEBUG_BUILD
								PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
								#ENDIF
								scLB_DisplayedData.iSectionEntries[iSection]++
								println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
							ENDIF
						ENDIF
							
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
							//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
							AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
							OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
							
							ELSE
								//1st Place
								//Get data about row outside of getting data for each column?
								
								//IF NOT ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
								//AND scLB_DisplayedData.iSectionEntries[iSection] < 12
									//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								IF IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
								AND scLB_DisplayedData.iSectionEntries[iSection] < 11
									IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
										iPlayerRow = 0
										scLB_DisplayedData.iPlayerIndexForSection[SC_LB_SECTION_FRIEND] = 0
									ENDIF
									scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
									scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
									scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].iRank = 1
									IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
										println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
										iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,scLB_DisplayedData.iCustomVehicleColumn)
										IF iCustomCar = 1
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
										ELSE
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
										ENDIF
									ENDIF
									IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
										println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
										scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
									ENDIF
									scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
									REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  i
										println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iReadColumns[i])
				      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[i])
										//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, scLB_DisplayedData.iReadColumns[i])
											#IF IS_DEBUG_BUILD
											DEBUG_PRINT_SC_LB_RETURNED_VALUES("SOCIAL_CLUB_GET_FRIEND_LB_DATA","row: ",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[i],
																				scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i],0.0,FALSE )
											#ENDIF
										ELSE
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(0, scLB_DisplayedData.iReadColumns[i])
											#IF IS_DEBUG_BUILD
											DEBUG_PRINT_SC_LB_RETURNED_VALUES("SOCIAL_CLUB_GET_FRIEND_LB_DATA","row: ",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[i],
																				0,scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i],TRUE )
											#ENDIF
										ENDIF
									ENDREPEAT
									//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
									//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
									//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
									#IF IS_DEBUG_BUILD
									PRINT_ROW_DATA(scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[iSection]]) 
									#ENDIF
									scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]++
									println("SOCIAL_CLUB_GET_FRIEND_LB_DATA: adding data to section number of entries now (FIRST)  : ",scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] ) 
								ELSE
									CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
									LEADERBOARDS2_READ_GET_ROW_DATA_END( )
									END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
									println("SOCIAL_CLUB_GET_FRIEND_LB_DATA  no friends data skiping to end stage 1 (no one in 1st)") 
									scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] = 0
									scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
									IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
									AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
										IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
											FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																							scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
										ELSE
											FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																							1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
										ENDIF
										scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
										println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
										
										#IF IS_DEBUG_BUILD
										PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
										#ENDIF
										scLB_DisplayedData.iSectionEntries[iSection]++
										
										println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
									ENDIF
									RETURN FALSE
								ENDIF
							ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
						ELSE
							//IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							// 	FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(funcName,scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],1)
							//	scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
							//	println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])		
							//	#IF IS_DEBUG_BUILD
							//	PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
							//	#ENDIF
							//	scLB_DisplayedData.iSectionEntries[iSection]++	
							//	println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
							//ELSE
								scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] = 0
								println("SOCIAL_CLUB_GET_FRIEND_LB_DATA  no friends data skiping to end stage 1 (no one in 1st no row returned)") 
							//ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
							IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
							AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																					scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								ELSE
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																					1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								ENDIF
								scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
								println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
								
								#IF IS_DEBUG_BUILD
								PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
								#ENDIF
								scLB_DisplayedData.iSectionEntries[iSection]++
								
								println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
							ENDIF
							scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
							RETURN FALSE
						ENDIF
						
						IF NOT IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							iPlayerRow = readInfo.m_LocalGamerRowNumber
						ENDIF
						
						
						//IF iPlayerRow = -1
						//OR iPlayerRow < 7
						//	iRankStart = 2
						//ELSE
							IF iPlayerRow > 6
								iRowStart = iPlayerRow - 6
							ELSE
								iRowStart = 1
							ENDIF
						//ENDIF
						iIndex = iRowStart 
						println(funcName, " iRowStart = ", iRowStart)
						FOR iIndex = iRowStart TO readInfo.m_ReturnedRows-1
							println(funcName, ": looking at details for returned row of index: ",iIndex, " columnBS: ",scLB_DisplayedData.iReadColumnIsIntBS)
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
							
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
							AND initRowData.m_Rank > 1
								//Get data about row outside of getting data for each column?
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								AND iPlayerRow = iIndex	
									//IF NOT ARE_STRINGS_EQUAL(scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName,GET_PLAYER_NAME(PLAYER_ID()))
									IF NOT SCLB_CHECK_HANDLES_ARE_THE_SAME(scLB_DisplayedData.rowData[iSection][0].playerGamerHandle,localPlayerHandle)
										FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],initRowData.m_Rank #IF IS_DEBUG_BUILD ,funcName,iSection #endif )
										scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].iRank = iIndex +1 
										scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
										println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
										
										#IF IS_DEBUG_BUILD
										PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
										#ENDIF
										scLB_DisplayedData.iSectionEntries[iSection]++
										
										println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
									ENDIF
								ENDIF 
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
								AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
								OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
								
									
								ELSE
									IF scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] < 12
										//IF NOT ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
										//AND NOT ARE_STRINGS_EQUAL(scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][0].tl23_ParticipantName,initRowData.m_GamerName)
											//IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(PLAYER_ID()),initRowData.m_GamerName)
										IF IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
										AND NOT SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,scLB_DisplayedData.rowData[iSection][0].playerGamerHandle)
											IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
												IF scLB_DisplayedData.iPlayerIndexForSection[SC_LB_SECTION_FRIEND] < 0
													scLB_DisplayedData.iPlayerIndexForSection[SC_LB_SECTION_FRIEND] = scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]
												ENDIF
											ENDIF
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].tl23_ParticipantName = initRowData.m_GamerName
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].playerGamerHandle = initRowData.m_GamerHandle
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].iRank = iIndex +1 
											scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].bValidData = TRUE
											IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
												iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, scLB_DisplayedData.iCustomVehicleColumn)
												IF iCustomCar = 1
													scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].bCustomVehicle = TRUE
												ELSE
													scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].bCustomVehicle = FALSE
												ENDIF
											ENDIF
											IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("CODRIVER- initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
												scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
											ENDIF
											REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  i
						      					println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iReadColumns[i])
												IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[i])
												//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
													scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].iColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, scLB_DisplayedData.iReadColumns[i])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES("SOCIAL_CLUB_GET_FRIEND_LB_DATA","Radius around player",iIndex,scLB_DisplayedData.iReadColumns[i],
																						scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][0].iColumnData[i],0.0,FALSE )
													#ENDIF
												ELSE
													scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]].fColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, scLB_DisplayedData.iReadColumns[i])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES("SOCIAL_CLUB_GET_FRIEND_LB_DATA","Radius around player",iIndex,scLB_DisplayedData.iReadColumns[i],
																						0,scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][0].fColumnData[i],TRUE )
													#ENDIF
												ENDIF
											ENDREPEAT
											//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
											//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
											//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
											#IF IS_DEBUG_BUILD
											PRINT_ROW_DATA(scLB_DisplayedData.rowData[SC_LB_SECTION_FRIEND][scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]]) 
											#ENDIF
											scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND]++
											println("SOCIAL_CLUB_GET_FRIEND_LB_DATA: (IN LOOP) adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] ) 
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
						ENDFOR
						
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						println("SOCIAL_CLUB_GET_FRIEND_LB_DATA -- moving to stage 1") 
						scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1	
						IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
						AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
								FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																				scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
							ELSE
								FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																				1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
							ENDIF
							scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
							println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
							
							#IF IS_DEBUG_BUILD
							PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
							#ENDIF
							scLB_DisplayedData.iSectionEntries[iSection]++
							
							println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
						ENDIF
						RETURN FALSE
					ELSE
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						scLB_DisplayedData.iSectionEntries[1] = 0
						scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
						SET_BIT(scLB_DisplayedData.iReadFailedBS,SC_LB_SECTION_FRIEND)
						println("SOCIAL_CLUB_GET_FRIEND_LB_DATA"," -- moving to end no one in first so no data at all (READ FAILED) ") 
						RETURN FALSE
					ENDIF
				ENDIF
			ELSE
				println("SOCIAL_CLUB_GET_FRIEND_LB_DATA  no friends data skiping to end stage 1 (friend count is 0)") 
				END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
				scLB_DisplayedData.iSectionEntries[SC_LB_SECTION_FRIEND] = 0
				scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE 1
			println(funcName,": doing rank offset for prediction" ) 
			CALCULATE_OFFSETS_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
			scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 2
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SOCIAL_CLUB_GET_CREW_LB_DATA(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control )

	//GAMER_HANDLE gamerHandle
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo

	//INT iRankStart
	INT i, j
	INT iCustomCar
	
	INT iPlayerRow = -1
	INT iRowStart
	INT iIndex
//	
	BOOL bRankOneInRadius
	#IF IS_DEBUG_BUILD
	STRING funcName = "SOCIAL_CLUB_GET_CREW_LB_DATA"
	#ENDIF
	INT iSection = SC_LB_SECTION_CREW
	
	BOOL bReplaceFirstSlot
	
	INT iRankRowsToRead
	//INT iOffsetRank
	GAMER_HANDLE localPlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	SWITCH scLB_control.iLoadStage[iSection]
		CASE 0
			scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
			scLB_DisplayedData.iSectionEntries[iSection] = 0
			sclb_rank_predict.iRankBeforePrediction = -1
			IF NETWORK_CLAN_SERVICE_IS_VALID()
				//gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(localPlayerHandle)
					IF NETWORK_CLAN_PLAYER_GET_DESC(scLB_control.localPlayerClanInfo, SIZE_OF(scLB_control.localPlayerClanInfo), localPlayerHandle)
						println(funcName," moving to stage 1 ") 
						scLB_control.ReadDataStruct.m_ClanId = scLB_control.localPlayerClanInfo.Id
						scLB_control.ReadDataStruct.m_type = LEADERBOARD2_TYPE_CLAN_MEMBER
						scLB_control.iLoadStage[iSection] = 1
						RETURN FALSE
					ENDIF
				ELSE
					println(funcName," moving to stage 3  NETWORK_CLAN_PLAYER_IS_ACTIVE is false") 
					scLB_control.iLoadStage[iSection] = 3
					RETURN TRUE
				ENDIF
			ELSE
				println(funcName," moving to stage 3 NETWORK_CLAN_SERVICE_IS_VALID is false") 
				scLB_control.iLoadStage[iSection] = 3
				RETURN TRUE
			ENDIF
		BREAK
		//radius around player
		CASE 1
			//gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			scLB_control.ReadDataStruct.m_type = LEADERBOARD2_TYPE_CLAN_MEMBER
			readInfo.m_LeaderboardId = 	scLB_control.ReadDataStruct.m_LeaderboardId
			readInfo.m_LeaderboardType = ENUM_TO_INT(scLB_control.ReadDataStruct.m_type)
			IF START_SC_LEADERBOARD_READ_BY_RADIUS(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct,11,localPlayerHandle,IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ())
				#IF IS_DEBUG_BUILD
				DEBUG_START_LEADERBOARD_PRINT_RESULTS(funcName,scLB_control.ReadDataStruct)
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
				IF scLB_control.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					println(funcName, ":readInfo.m_LocalGamerRowNumber = ",readInfo.m_LocalGamerRowNumber)
					IF readInfo.m_ReturnedRows > 0
						iIndex = 0
						i = 0
						//RANK PREDICTION
						IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							REPEAT readInfo.m_ReturnedRows iIndex
								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
								IF sclb_rank_predict.iRankBeforePrediction < 0
									//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
									//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName)
									IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
									OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle)
										sclb_rank_predict.iRankBeforePrediction = initRowData.m_Rank
										println(funcName, " setting players rank before prediction = ",sclb_rank_predict.iRankBeforePrediction) 
									ENDIF
								ENDIF
								IF iPlayerRow < 0
									IF IS_BIT_SET(sclb_rank_predict.currentResult.m_ColumnsBitSets,0)
										println("Players score: ", sclb_rank_predict.combinedResult.m_iColumnData[0])
										println("Returned Row# ",iIndex, " score: ", LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0))
										IF sclb_rank_predict.combinedResult.m_iColumnData[0] >= LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0) 
											iPlayerRow = iIndex
											println(funcName, " Players score is higher than returned result setting player row to ", iPlayerRow)
											
										ENDIF
									ELSE
										println("Players score: ", sclb_rank_predict.combinedResult.m_fColumnData[0])
										println("Returned Row# ",iIndex, " score: ", LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, 0))
										IF sclb_rank_predict.combinedResult.m_fColumnData[0] >= LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, 0)
											iPlayerRow = iIndex
											println(funcName, " Players score is higher than returned result setting player row to ", iPlayerRow)
											
										ENDIF
									ENDIF
								ENDIF
								CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							ENDREPEAT
							IF iPlayerRow < 0
								iPlayerRow = readInfo.m_ReturnedRows
							ENDIF
						ENDIF
						iIndex = 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						IF initRowData.m_Rank <= 1
						//OR (iPlayerRow = 0 AND initRowData.m_Rank <= 2)
							println(funcName, ": Rank 1 included in radius search grabbing now")
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								AND iPlayerRow = 0
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],initRowData.m_Rank#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
									//iPlayerRow = 0
									scLB_DisplayedData.iPlayerIndexForSection[iSection] = 0
									#IF IS_DEBUG_BUILD
									PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
									#ENDIF
									println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
									scLB_DisplayedData.iSectionEntries[iSection]++
									println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
								ENDIF
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
								AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
								OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
								
								ELSE
									//1st Place
									//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
									IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
										iPlayerRow = 0
										scLB_DisplayedData.iPlayerIndexForSection[iSection] = 0
									ENDIF
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = initRowData.m_Rank
									IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
										println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
										iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,scLB_DisplayedData.iCustomVehicleColumn)
										IF iCustomCar = 1
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
										ELSE
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
										ENDIF
									ENDIF
									IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
										println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
										scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
									ENDIF
									scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
									j = 0
									REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  j
										println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", scLB_DisplayedData.iSectionEntries[iSection], " Column: ", scLB_DisplayedData.iReadColumns[j])
				      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[j])
										//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, scLB_DisplayedData.iReadColumns[j])
											#IF IS_DEBUG_BUILD
											DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Row: ",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[j],
																				scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[j],0.0,FALSE )
											#ENDIF
										ELSE
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(0, scLB_DisplayedData.iReadColumns[j])
											#IF IS_DEBUG_BUILD
											DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Row: ",scLB_DisplayedData.iSectionEntries[iSection],scLB_DisplayedData.iReadColumns[j],
																				0,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[j],TRUE )
											#ENDIF
										ENDIF
									ENDREPEAT
									//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
									//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
									//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
									#IF IS_DEBUG_BUILD
									PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
									#ENDIF
									scLB_DisplayedData.iSectionEntries[iSection]++
									println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
								ENDIF
								bRankOneInRadius = TRUE
							ENDIF
						ENDIF
						IF NOT bRankOneInRadius
							scLB_DisplayedData.iSectionEntries[iSection]++
							println(funcName,": adding data to section number of entries now (FAKE TO ALLOW 1st PLACE LATER) : ",scLB_DisplayedData.iSectionEntries[iSection] )
						ENDIF
						
						IF NOT IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
							iPlayerRow = readInfo.m_LocalGamerRowNumber
						ENDIF
					
						IF iPlayerRow > 6
							iRowStart = iPlayerRow - 6
						ELSE
							IF bRankOneInRadius
								iRowStart = 1
							ELSE
								iRowStart = 0
							ENDIF
						ENDIF
						iIndex = iRowStart 
						CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
						FOR iIndex = iRowStart TO readInfo.m_ReturnedRows-1
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
							//Get data about row outside of getting data for each column?
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
							AND initRowData.m_Rank > 1
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								AND iPlayerRow = iIndex	
									//IF NOT ARE_STRINGS_EQUAL(scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName,GET_PLAYER_NAME(PLAYER_ID()))
									IF NOT SCLB_CHECK_HANDLES_ARE_THE_SAME(scLB_DisplayedData.rowData[iSection][0].playerGamerHandle,localPlayerHandle)
										FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],initRowData.m_Rank #IF IS_DEBUG_BUILD ,funcName,iSection #endif )
										scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
										println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
										
										#IF IS_DEBUG_BUILD
										PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
										#ENDIF
										scLB_DisplayedData.iSectionEntries[iSection]++
										
										println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
									ENDIF
								ENDIF  //if we are using rank prediction there is no record for local player so its an extra one in the middle
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
								AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
								OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
									
								ELSE
									IF scLB_DisplayedData.iSectionEntries[iSection] < 11
										//IF NOT ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
										//AND NOT ARE_STRINGS_EQUAL(scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName,initRowData.m_GamerName)
											//IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(PLAYER_ID()),initRowData.m_GamerName)
										IF IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
										AND NOT SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,scLB_DisplayedData.rowData[iSection][0].playerGamerHandle)
											IF SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
												IF scLB_DisplayedData.iPlayerIndexForSection[iSection] < 0
													scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
													println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
												ENDIF
											ENDIF
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = initRowData.m_Rank
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
											IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
												iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, scLB_DisplayedData.iCustomVehicleColumn)
												IF iCustomCar = 1
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
												ENDIF
											ENDIF
											IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
												scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
											ENDIF
											i = 0
											REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  i
						      					println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", iIndex, " Column: ", scLB_DisplayedData.iReadColumns[i])
												IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[i])
												//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, scLB_DisplayedData.iReadColumns[i])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",iIndex,scLB_DisplayedData.iReadColumns[i],
																						scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[i],0.0,FALSE )
													#ENDIF
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(iIndex, scLB_DisplayedData.iReadColumns[i])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",iIndex,scLB_DisplayedData.iReadColumns[i],
																						0,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[i],TRUE )
													#ENDIF
												ENDIF
											ENDREPEAT
											//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
											//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
											//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
											#IF IS_DEBUG_BUILD
											PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
											#ENDIF
											scLB_DisplayedData.iSectionEntries[iSection]++
											println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
						ENDFOR
						
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						
						IF bRankOneInRadius
							println(funcName," --Rank 1 included in radius-- moving to stage 3 grabbed ") 
							IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
							AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																					scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								ELSE
									FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																					1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
								ENDIF
								scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
								println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
								
								#IF IS_DEBUG_BUILD
								PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
								#ENDIF
								scLB_DisplayedData.iSectionEntries[iSection]++
								
								println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
							ENDIF
							scLB_control.iLoadStage[iSection] = 3
						ELSE
							println(funcName," -- moving to stage 2 grabbed ") 
							scLB_control.iLoadStage[iSection] = 2	
						ENDIF
					ELSE
						IF NOT bRankOneInRadius
							scLB_DisplayedData.iSectionEntries[iSection]++
							println(funcName,": adding data to section number of entries now (FAKE TO ALLOW 1st PLACE LATER) : ",scLB_DisplayedData.iSectionEntries[iSection] )
						ENDIF
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
						scLB_control.iLoadStage[iSection] = 2
						println(funcName," -- no data available for local player moving to stage 2(returned rows is 0)") 
					ENDIF
				ELSE
					//LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
					scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
					scLB_control.iLoadStage[iSection] = 2
					SET_BIT(scLB_DisplayedData.iReadFailedBS,iSection)
					println(funcName," -- no data available for local player moving to stage 2 (read failed)") 
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			//ENDIF
			IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
				iRankRowsToRead = 11
			ELSE
				iRankRowsToRead = 1
			ENDIF
			//gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			scLB_control.ReadDataStruct.m_type = LEADERBOARD2_TYPE_CLAN_MEMBER
			readInfo.m_LeaderboardId = 	scLB_control.ReadDataStruct.m_LeaderboardId
			readInfo.m_LeaderboardType = ENUM_TO_INT(scLB_control.ReadDataStruct.m_type)
			IF START_SC_LEADERBOARD_READ_BY_RANK(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct, 1,iRankRowsToRead)
				#IF IS_DEBUG_BUILD
				DEBUG_START_LEADERBOARD_PRINT_RESULTS(funcName,scLB_control.ReadDataStruct)
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
				IF scLB_control.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						REPEAT readInfo.m_ReturnedRows i
							println(funcName," start loop counter: ", i) 
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(i, initRowData)
							bReplaceFirstSlot = FALSE
							IF scLB_DisplayedData.rowData[iSection][0].iRank > 1
							OR scLB_DisplayedData.rowData[iSection][0].iRank <= 0
								bReplaceFirstSlot = TRUE
								println(funcName," replacing slot 0 as rank 1 is not in")
							ENDIF
							IF scLB_DisplayedData.iSectionEntries[iSection] < 11
							OR bReplaceFirstSlot
								IF IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
								//AND (ARE_STRINGS_EQUAL(initRowData.m_GamerName, GET_PLAYER_NAME(PLAYER_ID()))
								//OR ARE_STRINGS_EQUAL(initRowData.m_GamerName,sclb_rank_predict.driverName))
								AND (SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,localPlayerHandle)
								OR SCLB_CHECK_HANDLES_ARE_THE_SAME(initRowData.m_GamerHandle,sclb_rank_predict.driverHandle))
									
								ELSE
									IF i = 0
									OR bReplaceFirstSlot
										scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName = initRowData.m_GamerName
										scLB_DisplayedData.rowData[iSection][0].playerGamerHandle = initRowData.m_GamerHandle
										scLB_DisplayedData.rowData[iSection][0].iRank = initRowData.m_Rank
										IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
											println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
											iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,scLB_DisplayedData.iCustomVehicleColumn)
											IF iCustomCar = 1
												scLB_DisplayedData.rowData[iSection][0].bCustomVehicle = TRUE
											ELSE
												scLB_DisplayedData.rowData[iSection][0].bCustomVehicle = FALSE
											ENDIF
										ENDIF
										IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
											println("CODRIVER - initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
											scLB_DisplayedData.rowData[iSection][0].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
										ENDIF
										scLB_DisplayedData.rowData[iSection][0].bValidData = TRUE
										j = 0
										REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay  j
											println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", scLB_DisplayedData.iReadColumns[j])
					      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[j])
											//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[i])
												scLB_DisplayedData.rowData[iSection][0].iColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, scLB_DisplayedData.iReadColumns[j])
												#IF IS_DEBUG_BUILD
												DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Rank 1",0,scLB_DisplayedData.iReadColumns[j],
																					scLB_DisplayedData.rowData[iSection][0].iColumnData[j],0.0,FALSE )
												#ENDIF
											ELSE
												scLB_DisplayedData.rowData[iSection][0].fColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(0, scLB_DisplayedData.iReadColumns[j])
												#IF IS_DEBUG_BUILD
												DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Row: ",0,scLB_DisplayedData.iReadColumns[j],
																					0,scLB_DisplayedData.rowData[iSection][0].fColumnData[j],TRUE )
												#ENDIF
											ENDIF
										ENDREPEAT
										IF scLB_DisplayedData.iSectionEntries[iSection] = 0
											//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
											//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
											//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
											#IF IS_DEBUG_BUILD
											PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
											#ENDIF
											IF bReplaceFirstSlot
												println(funcName,": Replacing first entry with rank 1 player as it was not included")
											ELSE
												scLB_DisplayedData.iSectionEntries[iSection]++
											ENDIF
											println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
										ENDIF
									ELSE
										IF scLB_DisplayedData.iSectionEntries[iSection] < 11
											//Get data about row outside of getting data for each column?
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl23_ParticipantName = initRowData.m_GamerName
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].playerGamerHandle = initRowData.m_GamerHandle
											IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INT: Row: ", i, " Column: ", scLB_DisplayedData.iCustomVehicleColumn)
												iCustomCar = LEADERBOARDS2_READ_GET_ROW_DATA_INT(i,scLB_DisplayedData.iCustomVehicleColumn)
												IF iCustomCar = 1
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = TRUE
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bCustomVehicle = FALSE
												ENDIF
											ENDIF
											IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
												println("CODRIVER- initRowData.m_GroupSelector.m_Group[1].m_Id= ", initRowData.m_GroupSelector.m_Group[1].m_Id)
												scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].tl31_CoDriver = initRowData.m_GroupSelector.m_Group[1].m_Id
											ENDIF
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = initRowData.m_Rank
											scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].bValidData = TRUE
											j = 0
											REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay j
												println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", i, " Column: ", scLB_DisplayedData.iReadColumns[j])
						      					IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[j])
												//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[j])
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(i, scLB_DisplayedData.iReadColumns[j])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",i,scLB_DisplayedData.iReadColumns[j],
																						scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iColumnData[j],0.0,FALSE )
													#ENDIF
												ELSE
													scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[j] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(i, scLB_DisplayedData.iReadColumns[j])
													#IF IS_DEBUG_BUILD
													DEBUG_PRINT_SC_LB_RETURNED_VALUES(funcName,"Radius around player",i,scLB_DisplayedData.iReadColumns[j],
																						0,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].fColumnData[j],TRUE )
													#ENDIF
												ENDIF
											ENDREPEAT
											IF i != 0
												//iOffsetRank = GET_OFFSET_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
												//scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]].iRank + iOffsetRank
												//println(funcName,": rank prediction offseting rank by: ", iOffsetRank)
												#IF IS_DEBUG_BUILD
												PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
												#ENDIF
												scLB_DisplayedData.iSectionEntries[iSection]++
												println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] )
											ENDIF
										ENDIF
									ENDIF
								ENDIF	
							ENDIF
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							//println("Looping through data end loop counter: ", i) 
						ENDREPEAT
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
					println(funcName," -- moving to stage 3 grabbed ") 
					scLB_control.iLoadStage[iSection] = 3		
				ELSE
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
					SET_BIT(scLB_DisplayedData.iReadFailedBS,iSection)
					println(funcName," -- no data available for local player moving to stage 3 (read failed)") 
					scLB_DisplayedData.iSectionEntries[iSection] = 0
					scLB_control.iLoadStage[iSection] = 3
				ENDIF
				IF scLB_DisplayedData.iPlayerIndexForSection[iSection] = -1
				AND IS_RANK_PREDICTION_VALID_FOR_CURRENT_READ()
					IF scLB_DisplayedData.iSectionEntries[iSection] >= 1
						FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																		scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]-1].iRank+1 #IF IS_DEBUG_BUILD ,funcName,iSection #endif )
					ELSE
						FILL_LEADERBOARD_ROW_WITH_COMBINED_PREDICTION_RESULT(scLB_control,scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]],
																		1#IF IS_DEBUG_BUILD ,funcName,iSection #endif )
					ENDIF
					scLB_DisplayedData.iPlayerIndexForSection[iSection] = scLB_DisplayedData.iSectionEntries[iSection]
					println(funcName, "Adding player to index ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
					
					#IF IS_DEBUG_BUILD
					PRINT_ROW_DATA(scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iSectionEntries[iSection]]) 
					#ENDIF
					scLB_DisplayedData.iSectionEntries[iSection]++
					
					println(funcName,": adding data to section number of entries now : ",scLB_DisplayedData.iSectionEntries[iSection] ) 
				ENDIF
			ENDIF
		BREAK
		//Finished
		CASE 3
			println(funcName,": doing rank offset for prediction" ) 
			CALCULATE_OFFSETS_FOR_RANK_BASED_ON_PREDICTED_PLAYER_POSITION(iSection,sclb_rank_predict.iRankBeforePrediction)
			scLB_control.iLoadStage[iSection] = 4
		BREAK
		
		CASE 4
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SECTION_ROW(SC_FINAL_DATA& finalData[],INT iTotalCounter, INT &ISectionRowCounter, INT iSection)
	INT iOffset
	#IF IS_DEBUG_BUILD
	STRING sProcName = "GET_SECTION_ROW: "
	#ENDIF
	println(sProcName,"Section Row Counter: ", ISectionRowCounter, " Section: ", iSection)
	//first element is always player in first place
	IF ISectionRowCounter = 0
		IF scLB_DisplayedData.rowData[iSection][0].iRank > 0
			finalData[iTotalCounter].rowData = scLB_DisplayedData.rowData[iSection][0]
			coronaLB_Display.playerNames[iSection] = scLB_DisplayedData.rowData[iSection][0].tl23_ParticipantName
			coronaLB_Display.icolumnValue[iSection] = scLB_DisplayedData.rowData[iSection][0].iColumnData[scLB_DisplayedData.iCoronaBoxColumn]
			coronaLB_Display.fcolumnValue[iSection] = scLB_DisplayedData.rowData[iSection][0].fColumnData[scLB_DisplayedData.iCoronaBoxColumn]
			ISectionRowCounter++
			
			println(sProcName,"Added first place section ", iSection,  " data to final array: ITOTALCOUNTER = ",iTotalCounter ) 
			#IF IS_DEBUG_BUILD
			PRINT_ROW_DATA(finalData[iTotalCounter].rowData)
			#ENDIF
			RETURN TRUE
		ELSE
			println(sProcName,"scLB_DisplayedData.rowData[",iSection,"][0].iRank = 0 not adding section ", iSection,  " data") 
		ENDIF
	//second element is always local player data	
	ELIF ISectionRowCounter = 1
		println(sProcName,"Player index for section ", iSection, " : ", scLB_DisplayedData.iPlayerIndexForSection[iSection])
		IF scLB_DisplayedData.iPlayerIndexForSection[iSection] > 0
			finalData[iTotalCounter].rowData = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection]]
			ISectionRowCounter++
			
			println(sProcName,"Added player section ", iSection,  "  to final array: ITOTALCOUNTER = ",iTotalCounter ) 
			#IF IS_DEBUG_BUILD
			PRINT_ROW_DATA(finalData[iTotalCounter].rowData)
			#ENDIF
			RETURN TRUE
		ELSE
			IF scLB_DisplayedData.iPlayerIndexForSection[iSection] < 0
				finalData[iTotalCounter].rowData.tl23_ParticipantName = GET_PLAYER_NAME(PLAYER_ID())
				finalData[iTotalCounter].rowData.playerGamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				finalData[iTotalCounter].rowData.iRank = -1
				finalData[iTotalCounter].rowData.icolumnData[0] = -1
				finalData[iTotalCounter].rowData.icolumnData[1] = -1
				finalData[iTotalCounter].rowData.icolumnData[2] = -1
				finalData[iTotalCounter].rowData.icolumnData[3] = -1
				finalData[iTotalCounter].rowData.fcolumnData[0] = -1.0
				finalData[iTotalCounter].rowData.fcolumnData[1] = -1.0
				finalData[iTotalCounter].rowData.fcolumnData[2] = -1.0
				finalData[iTotalCounter].rowData.fcolumnData[3] = -1.0
				//finalData[iTotalCounter].rowData.iCarEnum = 0
				ISectionRowCounter++
				
				println(sProcName,"Added player section ", iSection,  " to final array (empty data): ITOTALCOUNTER = ",iTotalCounter )
				#IF IS_DEBUG_BUILD
				PRINT_ROW_DATA(finalData[iTotalCounter].rowData)
				#ENDIF
				RETURN TRUE
			ELSE
				println(sProcName,"Not adding player data to section ", iSection,  " data as player index is :", scLB_DisplayedData.iPlayerIndexForSection[iSection]) 
			ENDIF
			ISectionRowCounter++
		ENDIF
	ELSE
		//if even
		IF ISectionRowCounter%2 = 0
			iOffset = ISectionRowCounter/2
			IF scLB_DisplayedData.iPlayerIndexForSection[iSection] - iOffset >= 1
				IF scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection] - iOffset].iRank > 0
					finalData[iTotalCounter].rowData = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection] - iOffset]
					ISectionRowCounter++
					
					println(sProcName,"Added section ", iSection,  " data to final array (even): ITOTALCOUNTER = ",iTotalCounter ) 
					#IF IS_DEBUG_BUILD
					PRINT_ROW_DATA(finalData[iTotalCounter].rowData)
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
		//if odd
			iOffset = FLOOR(TO_FLOAT(ISectionRowCounter/2))
			IF scLB_DisplayedData.iPlayerIndexForSection[iSection] + iOffset < 12
			AND scLB_DisplayedData.iPlayerIndexForSection[iSection] + iOffset > 0
				IF scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection] + iOffset].iRank > 1
					finalData[iTotalCounter].rowData = scLB_DisplayedData.rowData[iSection][scLB_DisplayedData.iPlayerIndexForSection[iSection] + iOffset]
					ISectionRowCounter++
					
					println(sProcName,"Added section ", iSection,  " data to final array (odd): ITOTALCOUNTER = ",iTotalCounter ) 
					#IF IS_DEBUG_BUILD
					PRINT_ROW_DATA(finalData[iTotalCounter].rowData)
					#ENDIF
					RETURN TRUE
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	ISectionRowCounter++
	RETURN FALSE
ENDFUNC

FUNC INT SC_LEADERBOARD_CACHE_GET_FREE_SLOT(INT iCacheID)
	INT i
	
	INT iOldestSlot
	INT iOldestTime
	INT iCurrentTime
	
	REPEAT MAX_SOCIAL_CLUB_LEADERBOARD_CACHES i
		IF scLB_cache.iCacheIds[i] = iCacheID
			RETURN i	
			//println("SC_LEADERBOARD_CACHE_GET_FREE_SLOT: player already has this data cached but re-caching??")
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT MAX_SOCIAL_CLUB_LEADERBOARD_CACHES i
		IF scLB_cache.iCacheIds[i] = 0
			RETURN i
		ELSE
			IF LEADERBOARDS_GET_CACHE_EXISTS(scLB_cache.iCacheIds[i])
				iCurrentTime = LEADERBOARDS_GET_CACHE_TIME(scLB_cache.iCacheIds[i])
				IF iCurrentTime > iOldestTime
					iOldestSlot = i
					iOldestTime = iCurrentTime
				ENDIF
			ELSE
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iOldestSlot
ENDFUNC

PROC SC_LEADERBOARD_CACHE_CLEAR_SLOT(INT iCacheID = -1,INT iSlotID=-1)
	INT i
	
	IF iSlotID != -1
		println("SC_LEADERBOARD_CACHE_CLEAR_SLOT: Clearing slot ID: ", iSlotID)
		IF LEADERBOARDS_GET_CACHE_EXISTS(scLB_cache.iCacheIds[iSlotID])
			LEADERBOARDS_CLEAR_CACHE_DATA_ID(scLB_cache.iCacheIds[iSlotID])
			println("SC_LEADERBOARD_CACHE_CLEAR_SLOT: slot have cache ID: ", iCacheID)
		ENDIF
		scLB_cache.iCacheIds[iSlotID] = 0
	ELSE
		IF iCacheID != -1
			println("SC_LEADERBOARD_CACHE_CLEAR_SLOT: Clearing cache ID: ", iCacheID)
			IF LEADERBOARDS_GET_CACHE_EXISTS(iCacheID)
				LEADERBOARDS_CLEAR_CACHE_DATA_ID(iCacheID)
			ENDIF
			REPEAT MAX_SOCIAL_CLUB_LEADERBOARD_CACHES i
				IF scLB_cache.iCacheIds[i] = iCacheID
					scLB_cache.iCacheIds[i] = 0
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

PROC SC_LEADERBOARD_CACHE_CLEAR_ALL()
	INT i
	println("SC_LEADERBOARD_CACHE_CLEAR_ALL: Called from ", GET_THIS_SCRIPT_NAME())
	LEADERBOARDS_CLEAR_CACHE_DATA()
	REPEAT MAX_SOCIAL_CLUB_LEADERBOARD_CACHES i
		scLB_cache.iCacheIds[i] = 0
	ENDREPEAT
ENDPROC

PROC SC_LB_CHECK_IS_DATA_CACHED(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	INT iCacheTime
	IF LEADERBOARDS_GET_CACHE_EXISTS(scLB_DisplayedData.iCacheHashID)
		iCacheTime = LEADERBOARDS_GET_CACHE_TIME(scLB_DisplayedData.iCacheHashID)
		
		println("SC_LB_CHECK_IS_DATA_CACHED: cache does exists for : ",scLB_DisplayedData.iCacheHashID, " time: ",iCacheTime)
		IF iCacheTime < SC_LEADERBOARD_CACHE_DUMP_TIME
			SET_BIT(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_USING_CACHE)
		ELSE
			CLEAR_BIT(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_USING_CACHE)
			println("SC_LB_CHECK_IS_DATA_CACHED: time is up clearing cache for : ",scLB_DisplayedData.iCacheHashID)
			SC_LEADERBOARD_CACHE_CLEAR_SLOT(scLB_DisplayedData.iCacheHashID)
		ENDIF
	ELSE
		println("SC_LB_CHECK_IS_DATA_CACHED: cache does NOT exist for : ",scLB_DisplayedData.iCacheHashID)
	ENDIF
ENDPROC

PROC TRIGGER_SC_LB_CACHE_CHECK()
	SET_BIT(scLB_cache.iBS,SC_LB_CACHE_CHECK_STATUS)
	scLB_cache.iSlowCounter = 0
	RESET_NET_TIMER(scLB_cache.checkTimer)
ENDPROC

PROC MAINTAIN_CACHE_CLEARING()
	IF IS_BIT_SET(scLB_cache.iBS,SC_LB_CACHE_CHECK_STATUS)
		IF scLB_cache.iCacheIds[scLB_cache.iSlowCounter] != 0
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),scLB_cache.vCacheLocation[scLB_cache.iSlowCounter]) > SC_LEADERBOARD_CACHE_DUMP_RANGE
					AND NOT IS_PLAYER_ACTIVE_IN_CORONA()
						println("Clearing cache out of range of cache area.") 
						SC_LEADERBOARD_CACHE_CLEAR_SLOT(-1,scLB_cache.iSlowCounter)
					ELSE
						INT iCacheTime
						IF LEADERBOARDS_GET_CACHE_EXISTS(scLB_DisplayedData.iCacheHashID)
							iCacheTime = LEADERBOARDS_GET_CACHE_TIME(scLB_DisplayedData.iCacheHashID)
					
							println("MAINTAIN_CACHE_CLEARING: cache does exists for : ",scLB_DisplayedData.iCacheHashID, " time: ",iCacheTime)
							IF iCacheTime > SC_LEADERBOARD_CACHE_DUMP_TIME
								println("MAINTAIN_CACHE_CLEARING: time is up clearing cache for : ",scLB_DisplayedData.iCacheHashID)
								SC_LEADERBOARD_CACHE_CLEAR_SLOT(scLB_DisplayedData.iCacheHashID)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),scLB_cache.vCacheLocation[scLB_cache.iSlowCounter]) > SC_LEADERBOARD_CACHE_DUMP_RANGE
						println("Clearing cache out of range of cache area.") 
						SC_LEADERBOARD_CACHE_CLEAR_SLOT(-1,scLB_cache.iSlowCounter)
					ELSE
						INT iCacheTime
						IF LEADERBOARDS_GET_CACHE_EXISTS(scLB_DisplayedData.iCacheHashID)
							iCacheTime = LEADERBOARDS_GET_CACHE_TIME(scLB_DisplayedData.iCacheHashID)
					
							println("MAINTAIN_CACHE_CLEARING: cache does exists for : ",scLB_DisplayedData.iCacheHashID, " time: ",iCacheTime)
							IF iCacheTime > SC_LEADERBOARD_CACHE_DUMP_TIME
								println("MAINTAIN_CACHE_CLEARING: time is up clearing cache for : ",scLB_DisplayedData.iCacheHashID)
								SC_LEADERBOARD_CACHE_CLEAR_SLOT(scLB_DisplayedData.iCacheHashID)
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		scLB_cache.iSlowCounter++
		IF scLB_cache.iSlowCounter >= MAX_SOCIAL_CLUB_LEADERBOARD_CACHES
			CLEAR_BIT(scLB_cache.iBS,SC_LB_CACHE_CHECK_STATUS)
		ENDIF
	ELSE
		
		IF IS_PAUSE_MENU_ACTIVE()
			IF NOT IS_BIT_SET(scLB_cache.iBS,SC_LB_CACHE_PAUSE_ACTIVATED)
				TRIGGER_SC_LB_CACHE_CHECK()
				SET_BIT(scLB_cache.iBS,SC_LB_CACHE_PAUSE_ACTIVATED)
			ENDIF
		ELSE
			CLEAR_BIT(scLB_cache.iBS,SC_LB_CACHE_PAUSE_ACTIVATED)
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(scLB_cache.checkTimer)
			START_NET_TIMER(scLB_cache.checkTimer,TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(scLB_cache.checkTimer,SC_LEADERBOARD_CHECK_TIME,TRUE)
				TRIGGER_SC_LB_CACHE_CHECK()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEAR_CACHE_DISPLAY_DATA_STRUCT(LeaderboardCachedDisplayData& toClear)
	GAMER_HANDLE emptyHandle
	INT i
	toClear.m_Id = 0
	toClear.m_GamerName = ""
	toClear.m_CoDriverName = ""
	toClear.m_GamerHandle = emptyHandle
	toClear.m_CoDriverHandle = emptyHandle
	toClear.m_CustomVehicle = FALSE
	toClear.m_Rank = 0
	toClear.m_RowFlags = 0
	toClear.m_NumColumns = 0
	toClear.m_ColumnsBitSets = 0
	REPEAT MAX_COLUMNS i
		toClear.m_fColumnData[i] = 0
		toClear.m_iColumnData[i] = 0
	ENDREPEAT
ENDPROC

PROC CACHE_FINAL_DATA_LIST(SC_FINAL_DATA& finalData[])
	INT i,j
	//println("CACHE_FINAL_DATA_LIST: STARTING CACHE of ID: ", scLB_DisplayedData.iCacheHashID)
	INT iCacheSlot
	iCacheSlot = SC_LEADERBOARD_CACHE_GET_FREE_SLOT(scLB_DisplayedData.iCacheHashID)
	IF scLB_cache.iCacheIds[iCacheSlot] != 0
		println("CACHE_FINAL_DATA_LIST: clearing oldest slot to make room")
		SC_LEADERBOARD_CACHE_CLEAR_SLOT(-1,iCacheSlot)
	ENDIF
	
	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
		CLEAR_CACHE_DISPLAY_DATA_STRUCT(tempSCLB_CacheData)
		IF finalData[i].iSection != 0
			tempSCLB_CacheData.m_Id = scLB_DisplayedData.iCacheHashID
			println("---START ROW--- Hash ID: ",tempSCLB_CacheData.m_Id)
			tempSCLB_CacheData.m_GamerName = finalData[i].rowData.tl23_ParticipantName
			println("Gamer Name: ",tempSCLB_CacheData.m_GamerName )
			IF ARE_STRINGS_EQUAL(finalData[i].rowData.tl31_CoDriver,"")
				tempSCLB_CacheData.m_CoDriverName = finalData[i].rowData.tl23_ParticipantName
			ELSE
				tempSCLB_CacheData.m_CoDriverName = finalData[i].rowData.tl31_CoDriver
			ENDIF
			println("Co-Driver Name: ",tempSCLB_CacheData.m_CoDriverName)
			tempSCLB_CacheData.m_GamerHandle = finalData[i].rowData.playerGamerHandle
			//println("Gamer Handle: ", tempSCLB_CacheData.m_GamerHandle)
			IF IS_GAMER_HANDLE_VALID(finalData[i].rowData.coDriverGamerHandle)
				tempSCLB_CacheData.m_CoDriverHandle = finalData[i].rowData.coDriverGamerHandle
			ELSE
				tempSCLB_CacheData.m_CoDriverHandle = finalData[i].rowData.playerGamerHandle
			ENDIF
			//println("Co-Driver Handle: ",tempSCLB_CacheData.m_CoDriverHandle)
			tempSCLB_CacheData.m_CustomVehicle = finalData[i].rowData.bCustomVehicle
			println("Custom Vehicle: ",tempSCLB_CacheData.m_CustomVehicle)
			tempSCLB_CacheData.m_Rank = finalData[i].rowData.iRank
			println("Rank: ",tempSCLB_CacheData.m_Rank)
			tempSCLB_CacheData.m_NumColumns = scLB_DisplayedData.iNumNonRankColumnsToDisplay
			//IF tempSCLB_CacheData.m_NumColumns > 5
			//	println("More than 5 columns capping at 5 for testing")
			//	tempSCLB_CacheData.m_NumColumns = 5
			//ENDIF
			println("Number of Columns: ",tempSCLB_CacheData.m_NumColumns)
			tempSCLB_CacheData.m_ColumnsBitSets = scLB_DisplayedData.iDisplayColumnIsIntBS
			println("Column Bitset: ",tempSCLB_CacheData.m_ColumnsBitSets)
			println("Row Flags BEFORE: ",tempSCLB_CacheData.m_RowFlags)
			println("Row is: ",finalData[i].rowData.bValidData)
			IF finalData[i].rowData.bValidData
				SET_BIT(tempSCLB_CacheData.m_RowFlags,SC_LEADERBOARD_ROW_FLAG_VALID)
			ELSE
				CLEAR_BIT(tempSCLB_CacheData.m_RowFlags,SC_LEADERBOARD_ROW_FLAG_VALID)
			ENDIF
			
			println("Row is in section: ",finalData[i].iSection)
			SET_BIT(tempSCLB_CacheData.m_RowFlags,finalData[i].iSection)
			println("Row Flags: ",tempSCLB_CacheData.m_RowFlags)
			
			REPEAT tempSCLB_CacheData.m_NumColumns j
				//IF j < 5
					//if an INT
					IF IS_BIT_SET(tempSCLB_CacheData.m_ColumnsBitSets,j)
						tempSCLB_CacheData.m_iColumnData[j] = finalData[i].rowData.iColumnData[j]
						println("Adding INT for column ", j, " value: ",tempSCLB_CacheData.m_iColumnData[j])
					ELSE
						tempSCLB_CacheData.m_fColumnData[j] = finalData[i].rowData.fColumnData[j]
						println("Adding FLOAT for column ", j, " value: ",tempSCLB_CacheData.m_fColumnData[j] )
					ENDIF
				//ENDIF
			ENDREPEAT
			scLB_cache.iCacheIds[iCacheSlot] = scLB_DisplayedData.iCacheHashID
			LEADERBOARDS_CACHE_DATA_ROW(tempSCLB_CacheData)
		ENDIF
	ENDREPEAT
	scLB_cache.vCacheLocation[iCacheSlot]  = GET_PLAYER_COORDS(PLAYER_ID())
	println("CACHE_FINAL_DATA_LIST: FINISHED CACHE of ID: ", scLB_DisplayedData.iCacheHashID)
ENDPROC

PROC ASSIGN_FINAL_DATA_FROM_CACHE(SC_FINAL_DATA& finalData[])
	INT i,j
	println("ASSIGN_FINAL_DATA_FROM_CACHE: RETRIEVE DATA for cache ID: ",scLB_DisplayedData.iCacheHashID)
	INT iSection
	REPEAT 3 i
		scLB_DisplayedData.iSectionEntries[i] = 0
		scLB_DisplayedData.iPlayerIndexForSection[i] = -1
	ENDREPEAT
	GAMER_HANDLE localPlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	IF LEADERBOARDS_GET_CACHE_EXISTS(scLB_DisplayedData.iCacheHashID)
		INT iRows
		iRows = LEADERBOARDS_GET_CACHE_NUMBER_OF_ROWS(scLB_DisplayedData.iCacheHashID)
		println("RETRIEVE DATA num rows : ", iRows)
		#IF IS_DEBUG_BUILD
		IF iRows > SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
			println("ASSIGN_FINAL_DATA_FROM_CACHE: more rows cached than should be used. Previous cache NOT cleared!")
			SCRIPT_ASSERT("ASSIGN_FINAL_DATA_FROM_CACHE: more rows cached than should be used. Previous cache NOT cleared!- See Conor.")
		ENDIF
		#ENDIF
		i = 0
		REPEAT iRows i
			IF i< SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
				CLEAR_CACHE_DISPLAY_DATA_STRUCT(tempSCLB_CacheData)
				iSection = 0
				println("-START RETRIEVE CACHE ROW-Row ID: ", i)
				LEADERBOARDS_GET_CACHE_DATA_ROW(scLB_DisplayedData.iCacheHashID,i,tempSCLB_CacheData)
				//IF finalData[i].iSection != 0
				//need to get the iBS miguel added
				finalData[i].rowData.tl23_ParticipantName = tempSCLB_CacheData.m_GamerName
				println("Participant Name: ", finalData[i].rowData.tl23_ParticipantName)
				finalData[i].rowData.tl31_CoDriver = tempSCLB_CacheData.m_CoDriverName
				println("Co-Driver Name: ", finalData[i].rowData.tl31_CoDriver)
				finalData[i].rowData.playerGamerHandle = tempSCLB_CacheData.m_GamerHandle
				//println("Player gamer handle: ", finalData[i].rowData.playerGamerHandle)
				finalData[i].rowData.coDriverGamerHandle = tempSCLB_CacheData.m_CoDriverHandle
				//println("Co-Driver gamer handle: ", finalData[i].rowData.coDriverGamerHandle)
				finalData[i].rowData.bCustomVehicle = tempSCLB_CacheData.m_CustomVehicle
				println("Custom vehicle: ", finalData[i].rowData.bCustomVehicle)
				finalData[i].rowData.iRank = tempSCLB_CacheData.m_Rank
				println("Rank: ", finalData[i].rowData.iRank)
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = tempSCLB_CacheData.m_NumColumns
				println("Number of columns: ", scLB_DisplayedData.iNumNonRankColumnsToDisplay)
				scLB_DisplayedData.iReadColumnIsIntBS = tempSCLB_CacheData.m_ColumnsBitSets
				println("Column bitset: ", scLB_DisplayedData.iReadColumnIsIntBS)
				//
				iSection = 0
				println("Row Flags: ", tempSCLB_CacheData.m_RowFlags)
				IF IS_BIT_SET(tempSCLB_CacheData.m_RowFlags,1)
					iSection = 1
				ELIF IS_BIT_SET(tempSCLB_CacheData.m_RowFlags,2)
					iSection = 2
				ELIF IS_BIT_SET(tempSCLB_CacheData.m_RowFlags,3)
					iSection = 3
				ENDIF
				println("Row is in section: ", iSection)
				scLB_DisplayedData.iSectionEntries[iSection-1]++
				finalData[i].iSection = iSection
				IF finalData[i].rowData.iRank != -1
					IF IS_BIT_SET(tempSCLB_CacheData.m_RowFlags,SC_LEADERBOARD_ROW_FLAG_VALID)
						finalData[i].rowData.bValidData = TRUE
						println("Row is valid ")
					ELSE
						finalData[i].rowData.bValidData = FALSE
						println("Row is NOT valid ")
					ENDIF
					//IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(PLAYER_ID()),finalData[i].rowData.tl23_ParticipantName)
					IF SCLB_CHECK_HANDLES_ARE_THE_SAME(finalData[i].rowData.playerGamerHandle,localPlayerHandle)
						scLB_DisplayedData.iPlayerIndexForSection[iSection-1] = 0
						println("scLB_DisplayedData.iPlayerIndexForSection[",iSection-1, "] = 0")
					ENDIF
				ENDIF
				
				//ELIF IS_BIT_SET(tempSCLB_CacheData.m_RowFlags,1)
				//	scLB_DisplayedData.iSectionEntries[1]++
				//	finalData[i].iSection = 2
				//	IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(PLAYER_ID()),finalData[i].rowData.tl23_ParticipantName)
				//		IF finalData[i].rowData.iRank != -1
				//			scLB_DisplayedData.iPlayerIndexForSection[1] = 0
				//		ENDIF
				//	ENDIF
				//ELIF IS_BIT_SET(tempSCLB_CacheData.m_RowFlags,2)
				// finalData[i].iSection = 3
				//	scLB_DisplayedData.iSectionEntries[2]++
				//	IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(PLAYER_ID()),finalData[i].rowData.tl23_ParticipantName)
				//		IF finalData[i].rowData.iRank != -1
				//			scLB_DisplayedData.iPlayerIndexForSection[2] = 0
				//		ENDIF
				//	ENDIF
				//ENDIF
				
				
				REPEAT tempSCLB_CacheData.m_NumColumns j
					//IF j < 5
						//if an INT
						//println("INT for column ", j, " Value: ", tempSCLB_CacheData.m_iColumnData[j])
						//println("FLOAT for column ", j, " Value: ", tempSCLB_CacheData.m_fColumnData[j])
						IF IS_BIT_SET(tempSCLB_CacheData.m_ColumnsBitSets,j)
							finalData[i].rowData.iColumnData[j] = tempSCLB_CacheData.m_iColumnData[j]
							println("INT for column ", j, " Value: ", finalData[i].rowData.iColumnData[j])
						ELSE
							finalData[i].rowData.fColumnData[j] = tempSCLB_CacheData.m_fColumnData[j] 
							println("FLOAT for column ", j, " Value: ", finalData[i].rowData.fColumnData[j])
						ENDIF
					//ENDIF
				ENDREPEAT
				//ENDIF
				println("---END RETRIEVE CACHE ROW---")
			ENDIF
		ENDREPEAT
		#IF IS_DEBUG_BUILD
		IF g_bOutputSCLeaderboardData
			REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i 
				println("AFTER ASSIGNING CACHE - SC FINAL leaderboard row : ", i) 
				println("SC FINAL leaderboard row data gamer name: ",finalData[i].rowData.tl23_ParticipantName," rank: ",finalData[i].rowData.iRank, " section: ",finalData[i].iSection) 
				REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS j
					println(j,"- float column Value ",finalData[i].rowData.fcolumnData[j]) 
					println(j,"-int column Value ",finalData[i].rowData.icolumnData[j]) 
				ENDREPEAT
					
			ENDREPEAT
		ENDIF
		#ENDIF
	ELSE
		println("ASSIGN_FINAL_DATA_FROM_CACHE: CACHE DOESN'T EXIST!! ID: ",scLB_DisplayedData.iCacheHashID)
		SCRIPT_ASSERT("ASSIGN_FINAL_DATA_FROM_CACHE: trying to assign cache but it doesn't exist! - See Conor")
	ENDIF

ENDPROC

PROC ORDER_FINAL_LIST(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control,SC_FINAL_DATA& finalData[])
	INT i, j
	SC_LEADERBOARD_DISPLAY_ROW_STRUCT tempRow
	INT itempSection
	IF NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_USING_CACHE)
	AND NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_GENERATED_FINAL_LIST)
		#IF IS_DEBUG_BUILD
		IF g_bOutputSCLeaderboardData
			REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
				
				println("BEFORE SORT: Row: ",i) 
				println("SC FINAL leaderboard row data gamer name: ",finalData[i].rowData.tl23_ParticipantName," co-driver: ",finalData[i].rowData.tl31_CoDriver," rank: ",finalData[i].rowData.iRank, " section: ",finalData[i].iSection) 
				REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS j
					println("Column # ", j) 
					println(j, "- float column Value ",finalData[i].rowData.fcolumnData[j]) 
					println(j, "- int column Value ",finalData[i].rowData.icolumnData[j]) 
				ENDREPEAT
					
			ENDREPEAT
		ENDIF
		#ENDIF
		REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
			IF finalData[i].iSection != 0
				FOR j=i+1 TO 11
					IF finalData[j].iSection != 0
						IF finalData[j].iSection < finalData[i].iSection
							itempSection = finalData[j].iSection
							finalData[j].iSection = finalData[i].iSection
							finalData[i].iSection = itempSection
							tempRow = finalData[j].rowData
							finalData[j].rowData = finalData[i].rowData
							finalData[i].rowData = tempRow
						ELIF finalData[j].iSection = finalData[i].iSection
							//If less than j = i
							IF finalData[j].rowData.iRank != -1
								IF finalData[j].rowData.iRank < finalData[i].rowData.iRank
								OR finalData[i].rowData.iRank = -1
									itempSection = finalData[j].iSection
									finalData[j].iSection = finalData[i].iSection
									finalData[i].iSection = itempSection
									tempRow = finalData[j].rowData
									finalData[j].rowData= finalData[i].rowData
									finalData[i].rowData = tempRow
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDREPEAT
		#IF IS_DEBUG_BUILD
		IF g_bOutputSCLeaderboardData
			REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
				
				println("AFTER SORT Row: ",i) 
				println("SC FINAL leaderboard row data gamer name: ",finalData[i].rowData.tl23_ParticipantName," rank: ",finalData[i].rowData.iRank, " section: ",finalData[i].iSection) 
				REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS j
					println(j,"- float column Value ",finalData[i].rowData.fcolumnData[j]) 
					println(j,"- int column Value ",finalData[i].rowData.icolumnData[j]) 
				ENDREPEAT
					
			ENDREPEAT
		ENDIF
		#ENDIF
		
		
	ENDIF
	
ENDPROC

PROC GET_FINAL_DATA_LIST(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control,SC_FINAL_DATA& finalData[])
	INT iGlobalRowCounter
	INT iFriendRowCounter
	INT iCrewRowCounter
	INT iTotalCounter
	//INT iFriendAboveCounter, iFriendBelowCounter
	INT i
	IF NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_USING_CACHE)
	AND NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_GENERATED_FINAL_LIST)
		REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
			IF GET_SECTION_ROW(finalData,iTotalCounter,iGlobalRowCounter,SC_LB_SECTION_GLOBAL)
				finalData[iTotalCounter].iSection = 1
				iTotalCounter++
				IF iTotalCounter >= SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
					EXIT
				ENDIF
			ENDIF
			//IF GET_SECTION_ROW(finalData,scLB_control,iTotalCounter,iFriendRowCounter,iFriendAboveCounter, iFriendBelowCounter)
			IF GET_SECTION_ROW(finalData,iTotalCounter,iFRiendRowCounter,SC_LB_SECTION_FRIEND)
				finalData[iTotalCounter].iSection = 2
				iTotalCounter++
				IF iTotalCounter >= SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
					EXIT
				ENDIF
			ENDIF
			IF GET_SECTION_ROW(finalData,iTotalCounter,iCrewRowCounter,SC_LB_SECTION_CREW)
				finalData[iTotalCounter].iSection = 3
				iTotalCounter++
				IF iTotalCounter >= SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		ASSIGN_FINAL_DATA_FROM_CACHE(finalData)	
	ENDIF
ENDPROC


FUNC BOOL IS_GAMER_HANDLE_ALREADY_IN_ARRAY(GAMER_HANDLE& gamerToCheck,GAMERS_HANDLES& gamerHandles[])
	INT i
	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
		IF IS_GAMER_HANDLE_VALID(gamerHandles[i].m_GamerHandle)
			IF NETWORK_ARE_HANDLES_THE_SAME(gamerToCheck,gamerHandles[i].m_GamerHandle)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC CLEAR_CREW_TAGS_FOR_LEADERBOARD(SC_FINAL_DATA& finalData[])
	INT i
	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
		CLEAR_GAMER_HANDLE_STRUCT(tempGlobalLeaderboardCrewData.gamerHandles[i].m_GamerHandle)
		tempGlobalLeaderboardCrewData.crewTags[i] = ""
	ENDREPEAT
	IF finalData[0].iCrewTagLoadStage > 0
		finalData[0].iCrewTagLoadStage = 0
		finalData[1].iCrewTagLoadStage = 0
		IF NOT NETWORK_GET_PRIMARY_CLAN_DATA_PENDING()
			NETWORK_GET_PRIMARY_CLAN_DATA_CLEAR()
		ENDIF
	ENDIF
	PRINTLN("CLEAR_GLOBAL_CREW_DATA_FOR_LEADERBOARD called this frame")
	IF NETWORK_GET_PRIMARY_CLAN_DATA_PENDING()
		NETWORK_GET_PRIMARY_CLAN_DATA_CANCEL()
	ENDIF
	tempGlobalLeaderboardCrewData.iNumGamers = 0
ENDPROC

FUNC BOOL GET_CREW_TAGS_FOR_LEADERBOARD(SC_FINAL_DATA& finalData[])
	INT i,j
//	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
//		IF NOT GET_CREW_TAG_FOR_GAMER(finalData[i].iCrewTagLoadStage,finalData[i].rowData.playerGamerHandle,finalData[i].crewTag)
//			RETURN FALSE
//		ENDIF
//	ENDREPEAT

	//RETURN TRUE
	SWITCH finalData[0].iCrewTagLoadStage
		CASE 0
			CLEAR_CREW_TAGS_FOR_LEADERBOARD(finalData)
			IF (IS_PLAYSTATION_PLATFORM() OR IS_XBOX_PLATFORM())
			AND NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
				PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: Moving to stage 3 (user content priv off)")
				finalData[0].iCrewTagLoadStage= 3
				RETURN FALSE
			ENDIF
			REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
				IF IS_GAMER_HANDLE_VALID(finalData[i].rowData.playerGamerHandle)
					IF NOT IS_GAMER_HANDLE_ALREADY_IN_ARRAY(finalData[i].rowData.playerGamerHandle,tempGlobalLeaderboardCrewData.gamerHandles)
						tempGlobalLeaderboardCrewData.gamerHandles[tempGlobalLeaderboardCrewData.iNumGamers].m_GamerHandle = finalData[i].rowData.playerGamerHandle
						#IF IS_DEBUG_BUILD
						DEBUG_PRINT_GAMER_HANDLE(tempGlobalLeaderboardCrewData.gamerHandles[tempGlobalLeaderboardCrewData.iNumGamers].m_GamerHandle)
						#ENDIF
						tempGlobalLeaderboardCrewData.iNumGamers++
						PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD:valid handle added total now: ",tempGlobalLeaderboardCrewData.iNumGamers)
					ENDIF
				ENDIF
			ENDREPEAT
			IF tempGlobalLeaderboardCrewData.iNumGamers > 0
				PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: Moving to stage 1")
				finalData[0].iCrewTagLoadStage= 1
			ELSE
				PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: Moving to stage 3 (no gamers to query)")
				finalData[0].iCrewTagLoadStage= 3
			ENDIF
		BREAK
		CASE 1
			IF GET_CREW_TAGS_FOR_GAMERS(finalData[1].iCrewTagLoadStage,tempGlobalLeaderboardCrewData.iNumGamers,tempGlobalLeaderboardCrewData.gamerHandles,tempGlobalLeaderboardCrewData.crewTags)
				finalData[0].iCrewTagLoadStage = 2
				PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: Moving to stage 2")
			ENDIF
		BREAK
		CASE 2
			IF tempGlobalLeaderboardCrewData.iNumGamers > SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
				tempGlobalLeaderboardCrewData.iNumGamers = SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS
			ENDIF
			REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
				REPEAT tempGlobalLeaderboardCrewData.iNumGamers j
					IF IS_GAMER_HANDLE_VALID(finalData[i].rowData.playerGamerHandle)
					AND IS_GAMER_HANDLE_VALID(tempGlobalLeaderboardCrewData.gamerHandles[j].m_GamerHandle)
						IF NETWORK_ARE_HANDLES_THE_SAME(finalData[i].rowData.playerGamerHandle,tempGlobalLeaderboardCrewData.gamerHandles[j].m_GamerHandle)
							finalData[i].crewTag = tempGlobalLeaderboardCrewData.crewTags[j]
							println("GET_CREW_TAGS_FOR_LEADERBOARD: player ",finalData[i].rowData.tl23_ParticipantName, " crew Tag = ",tempGlobalLeaderboardCrewData.crewTags[j] )
						ELSE
							PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: gamer handles not the same I: ", i, " j: ",j)	
						ENDIF
					ELSE
						PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: gamer handles not valid")
					ENDIF
				ENDREPEAT
			ENDREPEAT
			PRINTLN("GET_CREW_TAGS_FOR_LEADERBOARD: Moving to stage 3")
			finalData[0].iCrewTagLoadStage= 3
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_CO_DRIVER_DISPLAY_NAMES(SC_FINAL_DATA& finalData[])
	INT i
	IF IS_XBOX_PLATFORM()
	OR IS_PLAYSTATION_PLATFORM()
		REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
			IF NOT GET_DISPLAY_NAME_FROM_USERID(finalData[i].iCoDriverNameLoadStage,finalData[i].rowData.tl31_CoDriver,finalData[i].coDriverDisplayName,scLB_DisplayedData.tempDisplayName,finalData[i].iDisplayNameRequestID)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
			IF NOT GET_GAMER_NAME_FROM_USER_ID(finalData[i].iCoDriverNameLoadStage,finalData[i].rowData.tl31_CoDriver,finalData[i].coDriverDisplayName)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC RESET_SCLB_DEBUG_METRICS_DATA()
	sclbDebugMetrics.iTotalFrames = 0
	sclbDebugMetrics.iGlobalDataFrames = 0
	sclbDebugMetrics.iCrewDataFrames = 0
	sclbDebugMetrics.iFriendDataFrames = 0
	sclbDebugMetrics.iTagsFrames = 0
	sclbDebugMetrics.iCoDriverNameFrames = 0
	sclbDebugMetrics.iUpdatedGamerTagFrames = 0
	sclbDebugMetrics.iFlagsBS = 0
	sclbDebugMetrics.bReset = FALSE
	sclbDebugMetrics.iTotalTime = 0
	RESET_NET_TIMER(sclbDebugMetrics.totalTimer)
ENDPROC

PROC PRINT_SCLB_DEBUG_METRICS_DATA()
	PRINTLN("SCLB_DEBUG_METRICS: iTotalFrames = ", sclbDebugMetrics.iTotalFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iGlobalDataFrames = ", sclbDebugMetrics.iGlobalDataFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iFriendDataFrames = ", sclbDebugMetrics.iFriendDataFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iCrewDataFrames  = ", sclbDebugMetrics.iCrewDataFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iCoDriverNameFrames = ", sclbDebugMetrics.iCoDriverNameFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iUpdatedGamerTagFrames = ", sclbDebugMetrics.iUpdatedGamerTagFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iTagsFrames = ", sclbDebugMetrics.iTagsFrames)
	PRINTLN("SCLB_DEBUG_METRICS: iTotalTime = ", sclbDebugMetrics.iTotalTime)
ENDPROC
#ENDIF

/// PURPOSE:
///    Durango Only: gets the display name from handle
FUNC BOOL GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB(SC_FINAL_DATA& finalData[])
	IF NOT IS_XBOX_PLATFORM()
	AND NOT IS_PLAYSTATION_PLATFORM()
		SCRIPT_ASSERT("GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB- is only supported on Durango- See Conor")
		DEBUG_PRINTCALLSTACK()
		PRINTLN("GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB- is only supported on Durango")
		RETURN TRUE
	ENDIF
	IF finalData[0].iRunUpdateGamerTagsLoadStage = 2
		RETURN TRUE
	ENDIF
	INT iResult
	INT i
	INT iCounter
	SWITCH finalData[0].iRunUpdateGamerTagsLoadStage
		CASE 0
			scLB_DisplayedData.iNumTempHandles = 0
			
			REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
				scLB_DisplayedData.tempDisplayName[i] = ""
				CLEAR_GAMER_HANDLE_STRUCT(scLB_DisplayedData.tempHandleList[i])
				IF IS_GAMER_HANDLE_VALID(finalData[i].rowData.playerGamerHandle)
					scLB_DisplayedData.tempHandleList[scLB_DisplayedData.iNumTempHandles] = finalData[i].rowData.playerGamerHandle
					scLB_DisplayedData.iNumTempHandles++
				ENDIF
			ENDREPEAT
			IF scLB_DisplayedData.iNumTempHandles > 0
				finalData[0].iDisplayNameRequestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(scLB_DisplayedData.tempHandleList,scLB_DisplayedData.iNumTempHandles)	
				finalData[0].iRunUpdateGamerTagsLoadStage = 1
			ELSE
				finalData[0].iRunUpdateGamerTagsLoadStage = 2
			ENDIF
		BREAK

		CASE 1
			iResult = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(finalData[0].iDisplayNameRequestID,scLB_DisplayedData.tempDisplayName,scLB_DisplayedData.iNumTempHandles)
			IF iResult = 0
				PRINTLN("GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB- SUCCEEDED!")
				REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
					IF IS_GAMER_HANDLE_VALID(finalData[i].rowData.playerGamerHandle)
						finalData[i].rowData.tl23_ParticipantName = scLB_DisplayedData.tempDisplayName[iCounter]
						PRINTLN("GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB PlayerName = ",finalData[i].rowData.tl23_ParticipantName )
						iCounter++
					ENDIF
				ENDREPEAT
				finalData[0].iRunUpdateGamerTagsLoadStage = 2
			ELIF iResult = -1
				PRINTLN("GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB- FAILED!")
				finalData[0].iRunUpdateGamerTagsLoadStage = 2
			ELSE
				 PRINTLN("GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB: waiting for display name request: iResult = ",iResult)
				 RETURN FALSE
			ENDIF
		BREAK

		CASE 2
			finalData[0].iDisplayNameRequestID = -1
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_UPDATED_GAMERTAGS(SC_FINAL_DATA& finalData[])
	INT i
	IF IS_PC_VERSION()
		PRINTLN("GET_UPDATED_GAMERTAGS: bypassing for PC build.")
		RETURN TRUE
	ELIF IS_XBOX_PLATFORM()
	OR IS_PLAYSTATION_PLATFORM()
		IF NOT GET_DISPLAY_NAMES_FROM_HANDLES_FOR_SCLB(finalData)
			RETURN FALSE
		ENDIF
	ELSE
		REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
			IF NOT GET_GAMER_NAME_FROM_HANDLE(finalData[i].iRunUpdateGamerTagsLoadStage,finalData[i].rowData.playerGamerHandle,finalData[i].rowData.tl23_ParticipantName)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Loads the social club data from global players, friends and crew
/// PARAMS:
///    scLB_control - The control struct for loading the data
/// RETURNS:
///    true when all the data has been grabbed
FUNC BOOL LOAD_SOCIAL_CLUB_LEADERBOARD_DATA(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_FINISHED)
		IF NOT HAS_NET_TIMER_STARTED(sclbDebugMetrics.totalTimer)
			START_NET_TIMER(sclbDebugMetrics.totalTimer,TRUE)
		ENDIF
	ENDIF
	#ENDIF
	IF NOT sclb_useRankPrediction
		IF NOT HAS_NET_TIMER_STARTED(scLB_DisplayedData.optionChangeDelay)
			START_NET_TIMER(scLB_DisplayedData.optionChangeDelay,TRUE)
			PRINTLN("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA- option delay timer -1 ")
			RETURN FALSE
		ELSE
			IF NOT HAS_NET_TIMER_EXPIRED(scLB_DisplayedData.optionChangeDelay,1000,TRUE)
				PRINTLN("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA- option delay timer -2 ")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_FINISHED)
		sclbDebugMetrics.iTotalFrames++
	ENDIF
	#ENDIF
	
	//failed to connect for some reason 
	IF NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()
	OR NOT IS_PLAYER_ONLINE()
	OR (NOT NETWORK_HAVE_ONLINE_PRIVILEGES() AND NETWORK_HAS_AGE_RESTRICTIONS())
	OR scLB_DisplayedData.iReadFailedBS != 0  //a read failed the values will be invalid display error message
		CLEAR_BIT(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_CHECK_CACHE)
		PRINTLN("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA- failed to connect ")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_CHECK_CACHE)
		SC_LB_CHECK_IS_DATA_CACHED(scLB_control)
		SET_BIT(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_CHECK_CACHE)
		#IF IS_DEBUG_BUILD
		//IF g_bOutputSCLeaderboardData
			println("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA: checked for cached data") 
		//ENDIF
		#ENDIF
		RETURN FALSE
	ELSE
		IF IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_SETUP_USING_CACHE)
			#IF IS_DEBUG_BUILD
			IF scLB_control.iLoadStage[SC_LB_SECTION_CREW] != 3
				println("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA: using cached data")
			ENDIF
			#ENDIF
			scLB_control.iLoadStage[SC_LB_SECTION_GLOBAL] = 2
			scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
			scLB_control.iLoadStage[SC_LB_SECTION_CREW] = 3
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_GLOBAL)
		sclbDebugMetrics.iGlobalDataFrames++
	ENDIF
	#ENDIF																										
	IF NOT SOCIAL_CLUB_GET_GLOBAL_LB_DATA(scLB_control)
		#IF IS_DEBUG_BUILD
		IF g_bOutputSCLeaderboardData
			println("Waiting for SOCIAL_CLUB_GET_GLOBAL_LB_DATA") 
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	SET_BIT(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_GLOBAL)
	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_FRIENDS)
		sclbDebugMetrics.iFriendDataFrames++
	ENDIF
	#ENDIF
	IF NOT SOCIAL_CLUB_GET_FRIEND_LB_DATA(scLB_control)
		#IF IS_DEBUG_BUILD
		IF g_bOutputSCLeaderboardData
			println("Waiting for SOCIAL_CLUB_GET_FRIEND_LB_DATA") 
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	SET_BIT(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_FRIENDS)
	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_CREW)
		sclbDebugMetrics.iCrewDataFrames++
	ENDIF
	#ENDIF
	IF NOT SOCIAL_CLUB_GET_CREW_LB_DATA(scLB_control)
		#IF IS_DEBUG_BUILD
		IF g_bOutputSCLeaderboardData
			println("Waiting for SOCIAL_CLUB_GET_CREW_LB_DATA") 
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_GENERATED_FINAL_LIST)
		CLEAR_FINAL_DATA_STRUCT(tempFinalData)
		GET_FINAL_DATA_LIST(scLB_control,tempFinalData)
		ORDER_FINAL_LIST(scLB_control,tempFinalData)
		SET_BIT(scLB_control.iBS,SC_LEADERBOARD_CONTROL_GENERATED_FINAL_LIST)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	SET_BIT(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_CREW)
//	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_TAGS)
//		sclbDebugMetrics.iTagsFrames++
//	ENDIF
	#ENDIF
	IF NOT IS_BIT_SET(scLB_control.iBS, SC_LEADERBOARD_CONTROL_GENERATED_TAGS)
		IF NOT HAS_NET_TIMER_STARTED(scLB_DisplayedData.failSafeTimer)
			START_NET_TIMER(scLB_DisplayedData.failSafeTimer,TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(scLB_DisplayedData.failSafeTimer,30000,TRUE)
				println("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA: hit failsafe timer for tag generation skipping)") 
				SET_BIT(scLB_control.iBS, SC_LEADERBOARD_CONTROL_GENERATED_TAGS)
			ENDIF
		ENDIF
		IF GET_CO_DRIVER_DISPLAY_NAMES(tempFinalData)
		
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_bOutputSCLeaderboardData
				println("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA: Waiting for GET_CO_DRIVER_DISPLAY_NAMES()") 
			ENDIF
			sclbDebugMetrics.iCoDriverNameFrames++
			#ENDIF
			
			RETURN FALSE
		ENDIF
		IF GET_UPDATED_GAMERTAGS(tempFinalData)
		
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_bOutputSCLeaderboardData
				println("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA: Waiting for GET_UPDATED_GAMERTAGS()") 
			ENDIF
			sclbDebugMetrics.iUpdatedGamerTagFrames++
			#ENDIF
			RETURN FALSE
		ENDIF
		IF GET_CREW_TAGS_FOR_LEADERBOARD(tempFinalData)
			CACHE_FINAL_DATA_LIST(tempFinalData)
			SET_BIT(scLB_control.iBS, SC_LEADERBOARD_CONTROL_GENERATED_TAGS)
			CACHE_FINAL_DATA_LIST(tempFinalData)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_bOutputSCLeaderboardData
				println("LOAD_SOCIAL_CLUB_LEADERBOARD_DATA: Waiting for GET_CREW_TAGS_FOR_LEADERBOARD()") 
			ENDIF
			sclbDebugMetrics.iTagsFrames++
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	SET_BIT(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_GOT_TAGS)
	IF NOT IS_BIT_SET(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_FINISHED)
		IF HAS_NET_TIMER_STARTED(sclbDebugMetrics.totalTimer)
			sclbDebugMetrics.iTotalTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sclbDebugMetrics.totalTimer,TRUE)
		ENDIF
		PRINT_SCLB_DEBUG_METRICS_DATA()
		SET_BIT(sclbDebugMetrics.iFlagsBS,SCLB_DEBUG_METRICS_FINISHED)
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Clears the control struct for the social club board.
///    This MUST be cleared if you are using common Social Club functions
/// PARAMS:
///    scLB_control - control struct to clear
PROC SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	INT i
	NETWORK_CLAN_DESC emptyClanDes
	//GAMER_HANDLE emptyHandle
	//SC_LEADERBOARD_DISPLAY_ROW_STRUCT tempRow
	scLB_control.iLoadStage[0] = 0
	scLB_control.iLoadStage[1] = 0
	scLB_control.iLoadStage[2] = 0
	scLB_control.iTempLoadStage = 0
	
	#IF IS_DEBUG_BUILD
	RESET_SCLB_DEBUG_METRICS_DATA()
	#ENDIF
	
	scLB_control.iLoopCounter = 0
	//scLB_control.iRowsThisLoop = 0 
	scLB_control.localPlayerClanInfo = emptyClanDes

	scLB_control.iBS = 0
	scLB_control.iDataCounter = 0
	
	scLB_control.ReadDataStruct.m_LeaderboardId = 0
	scLB_control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_INVALID
	scLB_control.ReadDataStruct.m_ClanId = 0
	scLB_control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
	REPEAT LEADERBOARD_MAX_GROUPS i
		scLB_control.ReadDataStruct.m_GroupSelector.m_Group[i].m_Category = ""
		scLB_control.ReadDataStruct.m_GroupSelector.m_Group[i].m_Id = ""
	ENDREPEAT
	scLB_control.groupHandle[0].m_NumGroups = 0
	REPEAT LEADERBOARD_MAX_GROUPS i
		scLB_control.groupHandle[0].m_Group[i].m_Category = ""
		scLB_control.groupHandle[0].m_Group[i].m_Id = ""
	ENDREPEAT
	DEBUG_PRINTCALLSTACK()
	println("SOCIAL_CLUB_CLEAR_CONTROL_STRUCT- called this frame")
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_LEADERBOARD_SETUP_DETAILS(SC_LEADERBOARD_CONTROL_STRUCT &scLB_control,TEXT_LABEL_31 UniqueLBGroup,INT iMissionType,INT iSubType,INT iLaps,BOOL bCoDriver)
	IF g_bOutputSCLeaderboardSetupThisFrame
		println("PRINT_LEADERBOARD_SETUP_DETAILS- START")
		PRINTLN("UniqueLBGroup = ",UniqueLBGroup)
		PRINTLN("iMissionType = ",iMissionType)
		PRINTLN("iSubType = ",iSubType)
		PRINTLN("iLaps = ",iLaps)
		PRINTLN("bCoDriver = ",bCoDriver)
		println("scLB_control.ReadDataStruct.m_LeaderboardId = ",scLB_control.ReadDataStruct.m_LeaderboardId)
		println("scLB_Control.ReadDataStruct.m_Type = ", scLB_Control.ReadDataStruct.m_Type)
		println("scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = ",scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups)
		println("scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ",scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category)
		println("scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ",scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id)
		println("scLB_DisplayedData.displaySetup.iDisplayType = ", scLB_DisplayedData.displaySetup.iDisplayType)
		println("scLB_DisplayedData.displaySetup.sLeaderboardType = ", scLB_DisplayedData.displaySetup.sLeaderboardType)
		println("scLB_DisplayedData.displaySetup.sLeaderboardName = ",scLB_DisplayedData.displaySetup.sLeaderboardName)			
		println("scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = ",scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)		
		println("scLB_DisplayedData.sColumnTextName[0] = ",scLB_DisplayedData.sColumnTextName[0])
		println("scLB_DisplayedData.sColumnTextName[1] = ",scLB_DisplayedData.sColumnTextName[1])
		println("scLB_DisplayedData.sColumnTextName[2] = ",scLB_DisplayedData.sColumnTextName[2])
		println("scLB_DisplayedData.sColumnTextName[3] = ",scLB_DisplayedData.sColumnTextName[3])
		println("scLB_DisplayedData.iReadColumns[0] = ",scLB_DisplayedData.iReadColumns[0])
		println("scLB_DisplayedData.iReadColumns[1] = ",scLB_DisplayedData.iReadColumns[1])
		println("scLB_DisplayedData.iReadColumns[2] = ",scLB_DisplayedData.iReadColumns[2])
		println("scLB_DisplayedData.iReadColumns[3] = ",scLB_DisplayedData.iReadColumns[3])
		println("scLB_DisplayedData.iNumNonRankColumnsToDisplay = ", scLB_DisplayedData.iNumNonRankColumnsToDisplay)
//		println("IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0]) = ",IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0]))	
//		println("IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1]) = ",IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1]))
//		println("IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2]) = ",IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2]))
//		println("IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3]) = ",IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3]))
		println("scLB_DisplayedData.displaySetup.iLayout = ",scLB_DisplayedData.displaySetup.iLayout)
//		println("scLB_DisplayedData.displaySetup.iIcons[0] = ",scLB_DisplayedData.displaySetup.iIcons[0])
//		println("scLB_DisplayedData.displaySetup.iIcons[1] = ",scLB_DisplayedData.displaySetup.iIcons[1])
//		println("scLB_DisplayedData.displaySetup.iIcons[2] = ",scLB_DisplayedData.displaySetup.iIcons[2])
//		println("scLB_DisplayedData.displaySetup.iIcons[3] = ",scLB_DisplayedData.displaySetup.iIcons[3])
		println("scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ",scLB_DisplayedData.displaySetup.iColumnDisplayType[0])
		println("scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ",scLB_DisplayedData.displaySetup.iColumnDisplayType[1])
		println("scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ",scLB_DisplayedData.displaySetup.iColumnDisplayType[2])
		println("scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ",scLB_DisplayedData.displaySetup.iColumnDisplayType[3])
		g_bOutputSCLeaderboardSetupThisFrame = FALSE
		println("PRINT_LEADERBOARD_SETUP_DETAILS- END")
	ENDIF
ENDPROC
#ENDIF
FUNC TEXT_LABEL_23 GET_SHOOTING_RANGE_CHALLENGE_NAME(INT iType)
	TEXT_LABEL_23 tempText
	SWITCH iType
		CASE FMMC_TYPE_SP_RANGE_PISTOL_1
			tempText = "HUD_MG_RANGE0"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_PISTOL_2
			tempText = "HUD_MG_RANGE1"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_PISTOL_3
			tempText = "HUD_MG_RANGE2"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_SMG_1
			tempText = "HUD_MG_RANGE3"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_SMG_2
			tempText = "HUD_MG_RANGE4"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_SMG_3
			tempText = "HUD_MG_RANGE5"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_SHOTGUN_1
			tempText = "HUD_MG_RANGE6"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_SHOTGUN_2
			tempText = "HUD_MG_RANGE7"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_SHOTGUN_3
			tempText = "HUD_MG_RANGE8"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_AR_1
			tempText =  "HUD_MG_RANGE9"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_AR_2
			tempText = "HUD_MG_RANGE10"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_AR_3
			tempText =  "HUD_MG_RANGE11"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_LMG_1
			tempText =  "HUD_MG_RANGE12"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_LMG_2
			tempText =  "HUD_MG_RANGE13"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_LMG_3
			tempText =  "HUD_MG_RANGE14"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_HEAVY_1
			tempText = "HUD_MG_RANGE15"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_HEAVY_2
			tempText =  "HUD_MG_RANGE16"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_HEAVY_3
			tempText = "HUD_MG_RANGE17"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_1
			tempText = "HUD_MG_RANGE18"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_2
			tempText = "HUD_MG_RANGE19"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_3
			tempText = "HUD_MG_RANGE20"
		BREAK
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_4
			tempText = "HUD_MG_RANGE21"
		BREAK
	ENDSWITCH
	RETURN tempText
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_WARS_MODE_NAME(INT iType)
	TEXT_LABEL_23 tempText

	SWITCH iType
		CASE SCLB_TYPE_ARENA_MODE_BUZZER_BEATER
			tempText = "BuzzerBeater" 
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_CARNAGE
			tempText = "Carnage" 
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_FLAG_WAR
			tempText ="FlagWar" 
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_WRECK_IT
			tempText = "WreckIt" 
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_BOMB_BALL
			tempText = "BombBall"
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_GAMES_MASTERS
			tempText = "GamesMasters"
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_MONSTERS
			tempText = "Monsters"
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_HOT_BOMB
			tempText = "HotBomb"
		BREAK
		CASE SCLB_TYPE_ARENA_MODE_TAG_TEAM
			tempText = "TagTeam"
		BREAK
		DEFAULT
			PRINTLN("GET_ARENA_WARS_MODE_NAME: invalid ID# ",iType," passed in! See Conor")
			SCRIPT_ASSERT("GET_ARENA_WARS_MODE_NAME: invalid ID# passed in! See Conor")
		BREAK
	ENDSWITCH
	RETURN tempText
ENDFUNC


/// PURPOSE:
///    To setup common leaderboard read parameters
/// PARAMS:
///    scLB_control - struct used to control leaderboard read progress
///    iMissionType - used to indicate what setup is used
///    UniqueLBGroupingIdentifier - Explaination below*
///    stMissionName - Only used for User Generated Content (UGC)
///    iSubType - subtype of mission type
///    iLaps - used only for races
//   	UniqueLBGroupingIdentifier - THIS IS ONLY NEEDED IF THE LEADERBOARD YOU ARE WRITING TO HAS GROUPS. 
//		Because we only have a single leaderboard we need a way to group together data this is based on a unique ID. 
//		For UGC this identifier is automatically created when a file is saved.
//		If the leaderboard you are using is NOT reling upon UGC you can make up whatever identifier you want.
PROC SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control, INT iMissionType, STRING UniqueLBGroupingIdentifier,STRING stMissionName, INT iSubType = -1, INT iLaps = -1, BOOL bCoDriver = FALSE, BOOL bMinigameDisplay = FALSE)
	TEXT_LABEL_31 UniqueLBGroup
	IF NOT IS_STRING_NULL_OR_EMPTY(UniqueLBGroupingIdentifier)
	//AND NOT IS_STRING_NULL_OR_EMPTY(creatorID)
		UniqueLBGroup = UniqueLBGroupingIdentifier
	ENDIF
	IF bMinigameDisplay
	ENDIF
	///Leaderboard2ReadData ReadDataStruct
	TEXT_LABEL_31 lapText
	INT i
	scLB_DisplayedData.iReadColumnIsIntBS = 0
	scLB_DisplayedData.iDisplayColumnIsIntBS = 0
	scLB_DisplayedData.iInvalidDataSetBS = 0
	
	GAMER_USERID localUserID
	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
			IF iSubType = ciRACE_SUB_TYPE_STANDARD
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES_LAPS)
				ELSE
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES)
				ENDIF
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Laps"
					lapText = ""
					lapText += iLaps
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = lapText
					scLB_DisplayedData.displaySetup.iLaps = iLaps
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_L1"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_LM"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ENDIF
					ELSE
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_NN_L1"
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_NN_LM"
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_NN"
					ENDIF
					scLB_DisplayedData.displaySetup.iLaps = -1
				ENDIF
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				
				//scLB_DisplayedData.iReadColumns[0] = 5 //best lap first
				IF iLaps <= 0
				OR IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.sColumnTextName[3] = ""
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 3
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 4
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELSE
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 2
					scLB_DisplayedData.iReadColumns[1] = 1
					scLB_DisplayedData.iReadColumns[2] = 3
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 4
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ENDIF
				scLB_DisplayedData.iCoronaBoxColumn = 0
				
			ELIF iSubType = ciRACE_SUB_TYPE_GTA
				//*SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
			    //BEST_LAP_TIME( AGG_LAST ) : COLUMN_ID_LB_BEST_TIME - LBCOLUMNTYPE_INT64
			   // TOTAL_TIME( AGG_LAST ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
			   // KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			   // DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //VEHICLE_ID( AGG_LAST ) : COLUMN_ID_LB_VEHICLE_ID - LBCOLUMNTYPE_INT32
			   //VEHICLE_COLOR( AGG_LAST ) : COLUMN_ID_LB_VEHICLE_COLOR - LBCOLUMNTYPE_INT32
			   IF iLaps > 0
			   AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES_LAPS)
				ELSE
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES)
				ENDIF
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Laps"
					lapText = ""
					lapText += iLaps
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = lapText
					scLB_DisplayedData.displaySetup.iLaps = iLaps
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE_L1"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE_LM"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ENDIF
					ELSE
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE_NN_L1"
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE_NN_LM"
						ENDIF
					ENDIF
				ELSE
					scLB_DisplayedData.displaySetup.iLaps = -1
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE_NN"
					ENDIF
				ENDIF
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				
				IF iLaps <= 0
				OR IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.sColumnTextName[3] = ""
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 5
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 6
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELSE
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 2
					scLB_DisplayedData.iReadColumns[1] = 1
					scLB_DisplayedData.iReadColumns[2] = 5
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
					scLB_DisplayedData.iCustomVehicleColumn = 6
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					//println("SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA: Reading RACE GTA leaderboard") 
				ENDIF
				scLB_DisplayedData.iCoronaBoxColumn = 0
			ELIF iSubType = ciRACE_SUB_TYPE_RALLY
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY_LAPS)
				ELSE
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY)
				ENDIF
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				IF NOT bCoDriver
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "CoDriver"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "" //"RSN_ConorM_2"
				ELSE
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "CoDriver"
					NETWORK_PLAYER_GET_USERID(PLAYER_ID(),localUserID)
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = localUserID.Name
					scLB_DisplayedData.displaySetup.bCoDriver = TRUE
				ENDIF
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups =3
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Category = "Laps"
					lapText = ""
					lapText += iLaps
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = lapText
					scLB_DisplayedData.displaySetup.iLaps = iLaps
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RRCE_L1"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RRCE_LM"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ENDIF
					ELSE
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RRCE_NN_L1"
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RRCE_NN_LM"
						ENDIF
					ENDIF
				ELSE
					scLB_DisplayedData.displaySetup.iLaps = -1
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RRCE"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RRCE_NN"
					ENDIF
				ENDIF
				IF iLaps <= 0
				OR IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.sColumnTextName[3] = ""
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 3
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 4
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELSE
					//scLB_DisplayedData.iReadColumns[0] = 5 //best lap first
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 2
					scLB_DisplayedData.iReadColumns[1] = 1
					scLB_DisplayedData.iReadColumns[2] = 3
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
					scLB_DisplayedData.iCustomVehicleColumn = 4
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					//println("SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA: Reading RACE RALLY leaderboard") 
				ENDIF
				//scLB_DisplayedData.iCacheHashID = SC_LC_GENERATE_ID_FOR_CACHE(scLB_control.ReadDataStruct.m_LeaderboardId,iMissionType,iSubType,iLaps,bCoDriver)
				scLB_DisplayedData.iCoronaBoxColumn = 0
			ELIF iSubType = FMMC_RACE_TYPE_ON_FOOT
			OR iSubType = FMMC_RACE_TYPE_ON_FOOT_P2P
				//println("Setup on foot race LB!")
				//*SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
			    //BEST_LAP_TIME( AGG_LAST ) : COLUMN_ID_LB_BEST_TIME - LBCOLUMNTYPE_INT64
			   // TOTAL_TIME( AGG_LAST ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
			   // KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			   // DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //VEHICLE_ID( AGG_LAST ) : COLUMN_ID_LB_VEHICLE_ID - LBCOLUMNTYPE_INT32
			   //VEHICLE_COLOR( AGG_LAST ) : COLUMN_ID_LB_VEHICLE_COLOR - LBCOLUMNTYPE_INT32
			   IF iLaps > 0
			   AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_ON_FOOT_RACE_LAPS)
				ELSE
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_ON_FOOT_RACE)
				ENDIF
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Laps"
					lapText = ""
					lapText += iLaps
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = lapText
					scLB_DisplayedData.displaySetup.iLaps = iLaps
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_FRCE_L1"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_FRCE_LM"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ENDIF
					ELSE
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_FRCE_NN_L1"
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_FRCE_NN_LM"
						ENDIF
					ENDIF
				ELSE
					scLB_DisplayedData.displaySetup.iLaps = -1
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_FRCE"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_FRCE_NN"
					ENDIF
				ENDIF
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				
				IF iLaps <= 0
				OR IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.sColumnTextName[3] = ""
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 0
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 1
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELSE
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 2
					scLB_DisplayedData.iReadColumns[1] = 1
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					//println("SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA: Reading RACE GTA leaderboard") 
				ENDIF
				scLB_DisplayedData.iCoronaBoxColumn = 0
			ELIF iSubType = ciRACE_SUB_TYPE_NON_CONTACT
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES_1_LAPS) +(iLaps-1)
				ELSE
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES)
				ENDIF
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				IF iLaps > 0
				AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_DisplayedData.displaySetup.iLaps = iLaps
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_L1"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_LM"
							scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
						ENDIF
					ELSE
						IF iLaps = 1
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_NN_L1"
						ELSE
							scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_NN_LM"
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_RCE_NN"
					ENDIF
					scLB_DisplayedData.displaySetup.iLaps = -1
				ENDIF
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				
				//scLB_DisplayedData.iReadColumns[0] = 5 //best lap first
				IF iLaps <= 0
				OR IS_THIS_RACE_A_POINT_2_POINT_RACE()
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.sColumnTextName[3] = ""
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 3
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 4
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELSE
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_BL"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 2
					scLB_DisplayedData.iReadColumns[1] = 1
					scLB_DisplayedData.iReadColumns[2] = 3
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iCustomVehicleColumn = 4
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
					////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_LAP)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_VEHICLE)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ENDIF
				scLB_DisplayedData.iCoronaBoxColumn = 0
			ENDIF
		BREAK
		CASE FMMC_TYPE_DEATHMATCH
		//KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - CLAN_LBCOLUMNTYPE_INT32
	   // DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - CLAN_LBCOLUMNTYPE_INT32
	   // KILL_DEATH_RATIO( AGG_SUM ) : COLUMN_ID_LB_DEATH_RATIO - CLAN_LBCOLUMNTYPE_INT32
            IF iSubType = FMMC_DM_TYPE_NORMAL
			OR iSubType = FMMC_DM_TYPE_KOTH
				scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_DEATHMATCH)
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				IF iSubType = FMMC_DM_TYPE_KOTH
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_KOTH"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_KOTH_NN"
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_DM"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_DM_NN"
					ENDIF
				ENDIF
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WLRAT"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WINS"
				scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_LOSES" 
				scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_KD"
				scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_KILLS"
				scLB_DisplayedData.sColumnTextName[5] = "SCLB_C_DEATH" 
				scLB_DisplayedData.iReadColumns[0] = 0
				scLB_DisplayedData.iReadColumns[1] = 4
				scLB_DisplayedData.iReadColumns[2] = 6
				scLB_DisplayedData.iReadColumns[3] = 3
				scLB_DisplayedData.iReadColumns[4] = 1
				scLB_DisplayedData.iReadColumns[5] = 2
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 6
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[5])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[5] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				//println("SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA: Reading DM NORMAL leaderboard") 
			ELIF iSubType = FMMC_DM_TYPE_TEAM
			OR iSubType = FMMC_DM_TYPE_TEAM_KOTH
				scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_TEAM_DEATHMATCH)
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				IF iSubType = FMMC_DM_TYPE_TEAM_KOTH
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_TKOTH"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_TKOTH_NN"
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_TDM"
						scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_TDM_NN"
					ENDIF
				ENDIF
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WLRAT"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WINS"
				scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_LOSES" 
				scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_KD"
				scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_KILLS"
				scLB_DisplayedData.sColumnTextName[5] = "SCLB_C_DEATH" 
				scLB_DisplayedData.iReadColumns[0] = 0
				scLB_DisplayedData.iReadColumns[1] = 4
				scLB_DisplayedData.iReadColumns[2] = 6
				scLB_DisplayedData.iReadColumns[3] = 3
				scLB_DisplayedData.iReadColumns[4] = 1
				scLB_DisplayedData.iReadColumns[5] = 2
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 6
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[5])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[5] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			ELIF iSubType = FMMC_DM_TYPE_VEHICLE
				scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH)
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_VEHDM"
					scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
				ELSE
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_VEHDM_NN"
				ENDIF
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WLRAT"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WINS"
				scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_LOSES" 
				scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_KD"
				scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_KILLS"
				scLB_DisplayedData.sColumnTextName[5] = "SCLB_C_DEATH" 
				scLB_DisplayedData.iReadColumns[0] = 0
				scLB_DisplayedData.iReadColumns[1] = 4
				scLB_DisplayedData.iReadColumns[2] = 6
				scLB_DisplayedData.iReadColumns[3] = 3
				scLB_DisplayedData.iReadColumns[4] = 1
				scLB_DisplayedData.iReadColumns[5] = 2
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 6
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[5])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[5] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			ENDIF
		BREAK
		CASE FMMC_TYPE_MG_GOLF
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_GOLF)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "MP"
			
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_GOLF"
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_GOLF0"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_GOLF1"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_GAMES" 
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
			scLB_DisplayedData.iReadColumns[0] = 0 //best round
			scLB_DisplayedData.iReadColumns[1] = 1 //longest drive
			scLB_DisplayedData.iReadColumns[2] = 3 //num matches
			scLB_DisplayedData.iReadColumns[3] = 0
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROWN)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GOLF_CLUB)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_GOLF)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT_NEG_B4_DIS) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_MG_GOLF_SP
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_GOLF)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "SP"
			
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_GOLF"
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_GOLF0"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_GOLF1"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_GAMES" 
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
			scLB_DisplayedData.iReadColumns[0] = 0 //best round
			scLB_DisplayedData.iReadColumns[1] = 1 //longest drive
			scLB_DisplayedData.iReadColumns[2] = 3 //num matches
			scLB_DisplayedData.iReadColumns[3] = 0
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROWN)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GOLF_CLUB)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_GOLF)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT_NEG_B4_DIS) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 		
		BREAK
		CASE FMMC_TYPE_MG_HUNTING
			// Want score, Time to Gold, Kills, Heart shots, and Photos sent
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_HUNTING"
			scLB_DisplayedData.displaySetup.sLeaderboardName = "CMSW"
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MINIGAME)
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_HSCORE"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_TIMEHUNT"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_EKILLS"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_PHOTOS"
			scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_MONEY"
			//scLB_DisplayedData.sColumnTextName[5] = ""
			
			scLB_DisplayedData.iReadColumns[0] = 0 // Score
			scLB_DisplayedData.iReadColumns[1] = 5 // wins
			scLB_DisplayedData.iReadColumns[2] = 2 // LOSES
			scLB_DisplayedData.iReadColumns[3] = 4
			scLB_DisplayedData.iReadColumns[4] = 6
			//scLB_DisplayedData.iReadColumns[5] = 0
			
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 5
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_5_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_POINT)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_TIME)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_KILLS)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_CROSSHAIR)
			//scLB_DisplayedData.displaySetup.iIcons[4] = ENUM_TO_INT(ICON_DOLLAR)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_GOLD_TIME) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
		BREAK
		CASE FMMC_TYPE_MG_ARM_WRESTLING
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_ARM_WRESTLING)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_ARM"
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_LOSES"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_WLRAT" 
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
			scLB_DisplayedData.iReadColumns[0] = 2 //wins
			scLB_DisplayedData.iReadColumns[1] = 5 //loses
			scLB_DisplayedData.iReadColumns[2] = 0 //ratio
			scLB_DisplayedData.iReadColumns[3] = 0
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_WINNER)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_ARM)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_SCALE)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			//println("Setup arm wrestling leaderboard read data") 
		BREAK
		CASE FMMC_TYPE_MG_DARTS
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_DARTS)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
			IF iSubType = -1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "MP"
			ELSE
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "SP"
			ENDIF
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_DARTS"
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WLRAT"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_LOSES"
			
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
			scLB_DisplayedData.iReadColumns[0] = 0 //ratio
			scLB_DisplayedData.iReadColumns[1] = 7 //wins
			scLB_DisplayedData.iReadColumns[2] = 5 //LOSES
			scLB_DisplayedData.iReadColumns[3] = 0 
			 
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_SCALE)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_WINNER)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_DART)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_MG_TENNIS
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_TENNIS)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "MP"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Location"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "ElBarto"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Category = "Type"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "????"
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_TENNIS"
			scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			IF IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_TENNIS"
				scLB_DisplayedData.displaySetup.sLeaderboardName += (iSubType+1)
			ENDIF
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_LOSES"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_TEN1"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TEN2"
			scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_TEN0"
			scLB_DisplayedData.iReadColumns[0] = 0 //wins
			scLB_DisplayedData.iReadColumns[1] = 9 //loses
			scLB_DisplayedData.iReadColumns[2] = 7 //sets won
			scLB_DisplayedData.iReadColumns[3] = 5 //games won
			scLB_DisplayedData.iReadColumns[4] = 2 //aces
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 5
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TENNIS_BALL)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_WINNER)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_LOSER)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_MG_TENNIS_SP
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_TENNIS)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "SP"
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_TENNIS"
			scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_LOSES"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_TEN1"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TEN2"
			scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_TEN0"
			scLB_DisplayedData.iReadColumns[0] = 0 //wins
			scLB_DisplayedData.iReadColumns[1] = 9 //loses
			scLB_DisplayedData.iReadColumns[2] = 7 //sets won
			scLB_DisplayedData.iReadColumns[3] = 5 //games won
			scLB_DisplayedData.iReadColumns[4] = 2 //aces
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 5
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TENNIS_BALL)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_WINNER)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_LOSER)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
			
			SWITCH iSubType
				CASE 0
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Type"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "RANDOM"
					scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGEa"
				BREAK 
				CASE 1
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Type"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "GRID"
					scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGEb"
				BREAK
				CASE 2
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Type"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "COVERED"
					scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGEc"
				BREAK
				DEFAULT
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Type"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "RANDOM"
					scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGEa"
					#IF IS_DEBUG_BUILD
					println(iSubType," FMMC_TYPE_MG_SHOOTING_RANGE: FAILED- incorrect arguments passed to setup! called from: ", GET_THIS_SCRIPT_NAME())
					SCRIPT_ASSERT("SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA: Shooting range leaderboard failed to pass in correct setup. See Conor.")
					#ENDIF
				BREAK
			ENDSWITCH 
			
			SWITCH iLaps
		        CASE 0 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId" 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "Pistols" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_PISTOL"
				BREAK
		        CASE 1 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "SMGs" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_SMG"
				BREAK
		        CASE 2 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "AssaultRifles" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_ASSAULT"
				BREAK
		        CASE 3 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "Shotguns" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_SHOTGUN"
				BREAK
		        CASE 4 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "LMGs" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_LMG"
				BREAK
		        CASE 5 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "Heavies" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_HEAVY"
				
				BREAK
				DEFAULT
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "WeaponId" 
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "Pistols" 
					scLB_DisplayedData.displaySetup.sLeaderboardName = "HUD_MG_PISTOL"
					#IF IS_DEBUG_BUILD
					println(iSubType, " FMMC_TYPE_MG_SHOOTING_RANGE: FAILED- incorrect arguments passed to setup! called from: ", GET_THIS_SCRIPT_NAME())
					SCRIPT_ASSERT("SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA: Shooting range leaderboard failed to pass in correct setup. See Conor.")
					#ENDIF
				BREAK
			ENDSWITCH
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)

			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WLRAT"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_HITS"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_SHOTS" 
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_ACC"
			scLB_DisplayedData.iReadColumns[0] = 0 //score
			scLB_DisplayedData.iReadColumns[1] = 2 //hit
			scLB_DisplayedData.iReadColumns[2] = 1 //fired
			scLB_DisplayedData.iReadColumns[3] = 3 //accuracy
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROSSHAIR)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GUN)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_NONE)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
		BREAK
//		CASE FMMC_TYPE_MG_RANGE_GRID
//			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE)
//			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "MP"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Type"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "GRID"
//			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MINIGAME)
//			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGE"
//			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
//			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WEAP"
//			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK" 
//			scLB_DisplayedData.sColumnTextName[3] = ""
//			scLB_DisplayedData.iReadColumns[0] = 0 //score
//			scLB_DisplayedData.iReadColumns[1] = 1 //weapon
//			scLB_DisplayedData.iReadColumns[2] = 0
//			scLB_DisplayedData.iReadColumns[3] = 0
//			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
//			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
//			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
//			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
//			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROSSHAIR)
//			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GUN)
//			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
//			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_NONE)
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_GUN) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_NONE) 
//			RETURN TRUE
//		BREAK
//		CASE FMMC_TYPE_MG_RANGE_COVERED
//			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE)
//			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "MP"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Type"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "COVERED"
//			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MINIGAME)
//			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGE"
//			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
//			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WEAP"
//			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK" 
//			scLB_DisplayedData.sColumnTextName[3] = ""
//			scLB_DisplayedData.iReadColumns[0] = 0 //score
//			scLB_DisplayedData.iReadColumns[1] = 1 //weapon
//			scLB_DisplayedData.iReadColumns[2] = 0
//			scLB_DisplayedData.iReadColumns[3] = 0
//			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
//			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
//			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
//			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
//			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROSSHAIR)
//			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GUN)
//			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
//			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_NONE)
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_GUN) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_NONE) 
//			RETURN TRUE
//		BREAK
//		CASE FMMC_TYPE_MG_RANGE_RANDOM
//			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE)
//			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType" 
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "MP"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Type"
//			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "RANDOM"
//			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MINIGAME)
//			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGE"
//			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
//			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WEAP"
//			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK" 
//			scLB_DisplayedData.sColumnTextName[3] = ""
//			scLB_DisplayedData.iReadColumns[0] = 0 //score
//			scLB_DisplayedData.iReadColumns[1] = 1 //weapon
//			scLB_DisplayedData.iReadColumns[2] = 0
//			scLB_DisplayedData.iReadColumns[3] = 0
//			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
//			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
//			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
//			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
//			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROSSHAIR)
//			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GUN)
//			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
//			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_NONE)
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_GUN) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_NONE) 
//			RETURN TRUE
//		BREAK
		CASE FMMC_TYPE_SP_RANGE_PISTOL_1
		CASE FMMC_TYPE_SP_RANGE_PISTOL_2
		CASE FMMC_TYPE_SP_RANGE_PISTOL_3
		CASE FMMC_TYPE_SP_RANGE_SMG_1
		CASE FMMC_TYPE_SP_RANGE_SMG_2
		CASE FMMC_TYPE_SP_RANGE_SMG_3
		CASE FMMC_TYPE_SP_RANGE_SHOTGUN_1
		CASE FMMC_TYPE_SP_RANGE_SHOTGUN_2
		CASE FMMC_TYPE_SP_RANGE_SHOTGUN_3
		CASE FMMC_TYPE_SP_RANGE_AR_1
		CASE FMMC_TYPE_SP_RANGE_AR_2
		CASE FMMC_TYPE_SP_RANGE_AR_3
		CASE FMMC_TYPE_SP_RANGE_LMG_1
		CASE FMMC_TYPE_SP_RANGE_LMG_2
		CASE FMMC_TYPE_SP_RANGE_LMG_3
		CASE FMMC_TYPE_SP_RANGE_HEAVY_1
		CASE FMMC_TYPE_SP_RANGE_HEAVY_2
		CASE FMMC_TYPE_SP_RANGE_HEAVY_3
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_1
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_2
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_3
		CASE FMMC_TYPE_SP_RANGE_RAILGUN_4
		
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Type"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_RANGE"
			scLB_DisplayedData.displaySetup.sLeaderboardName = GET_SHOOTING_RANGE_CHALLENGE_NAME(iMissionType)
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WEAP"
//			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK" 
//			scLB_DisplayedData.sColumnTextName[3] = ""
			scLB_DisplayedData.iReadColumns[0] = 0 //score
			scLB_DisplayedData.iReadColumns[1] = 7 //weapon
//			scLB_DisplayedData.iReadColumns[2] = 0
//			scLB_DisplayedData.iReadColumns[3] = 0
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROSSHAIR)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_GUN)
//			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
//			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_NONE)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_GUN) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
//			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_NONE) 
		BREAK
		CASE FMMC_TYPE_SP_TRI_ALAMO
		CASE FMMC_TYPE_SP_TRI_LOSSANTOS
		CASE FMMC_TYPE_SP_TRI_VESPUCCI
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_TRIATHLON)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Location" 
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.sLeaderboardType = "HUD_MG_TRI"
			scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_TIME"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_RANK"
			scLB_DisplayedData.sColumnTextName[2] = "" 
			scLB_DisplayedData.sColumnTextName[3] = ""
			scLB_DisplayedData.iReadColumns[0] = 0 //time
			scLB_DisplayedData.iReadColumns[1] = 0
			scLB_DisplayedData.iReadColumns[2] = 0
			scLB_DisplayedData.iReadColumns[3] = 0
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 1
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_POSITION)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_NONE)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_NONE)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NEG_B4_DIS)//SCLB_DATA_TYPE_TIME) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_NONE) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_NONE) 
		BREAK
		CASE FMMC_TYPE_SP_OFFROAD_RACES
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 3
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "SP"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Location"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = UniqueLBGroup
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Category = "Type"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.sLeaderboardType = "OFFR_TITLE"
			scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_TIME"
			//scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_RANK"
			scLB_DisplayedData.iReadColumns[0] = 3 //time
			//scLB_DisplayedData.iReadColumns[1] = 1 //race rank (only takes 1 2 or 3)
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 1
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
//			scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
//			scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
			//scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
		BREAK
		CASE FMMC_TYPE_SURVIVAL
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_HRD"
				scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_HRD_NN"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WAVE"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_TKILLS"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TDEATH" 
			scLB_DisplayedData.iReadColumns[0] = 0
			scLB_DisplayedData.iReadColumns[1] = 1 //waves
			scLB_DisplayedData.iReadColumns[2] = 2 //kills
			scLB_DisplayedData.iReadColumns[3] = 3 // deaths
			
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_KILLS)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_CROSSHAIR)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_MISSION
			//1955210		
			IF iSubType = FMMC_MISSION_TYPE_PLANNING
			OR iSubType = FMMC_MISSION_TYPE_HEIST
				scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION)
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				
				
				IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
					IF iSubType = FMMC_MISSION_TYPE_HEIST
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_HEIST"
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_HEISTP"
					ENDIF
					scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
				ELSE
					IF iSubType = FMMC_MISSION_TYPE_HEIST
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_HEIST_NN"
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_HEISTPNN"
					ENDIF
				ENDIF
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_TIME"
				scLB_DisplayedData.sColumnTextName[2] = "SCLB_ACC"
				scLB_DisplayedData.sColumnTextName[3] = "SCLB_HEADSHOT"
				scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_KILLS"
				scLB_DisplayedData.iReadColumns[0] = 0 
				scLB_DisplayedData.iReadColumns[1] = 1 
				scLB_DisplayedData.iReadColumns[2] = 4 
				scLB_DisplayedData.iReadColumns[3] = 5 
				scLB_DisplayedData.iReadColumns[4] = 6
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_5_STATS)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_POSITIVE) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			ELIF g_FMMC_STRUCT.iMissionEndType =  ciMISSION_SCORING_TYPE_TIME
				// *SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
			   	// KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			   	// DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			   	// KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
			    //TOTAL_TIME( AGG_SUM ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
			    //TOTAL_KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			    //TOTAL_DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //TOTAL_KILL_DEATH_RATIO( AGG_SUM ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
				scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_TIME)
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				
				
				IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
					IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS 
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_LTS"
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_MIS"
					ENDIF
					scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
				ELSE
					IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS 
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_LTS_NN"
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_MIS_NN"
					ENDIF
				ENDIF
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_TIME"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
				scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_DEATH" 
				scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
				scLB_DisplayedData.iReadColumns[0] = 0 //time
				scLB_DisplayedData.iReadColumns[1] = 1 //kills
				scLB_DisplayedData.iReadColumns[2] = 2 //deaths
				scLB_DisplayedData.iReadColumns[3] = 0 
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
				//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
				//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_KILLS)
				//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_DEATHS)
				//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NEG_B4_DIS) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			ELSE
				// *SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
			    //KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			    //DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
				scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE)
				scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
				scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
				IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
					IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS 
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_LTS"
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_MIS"
					ENDIF
					scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
				ELSE
					IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS 
						scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_LTS_NN"
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_MIS_NN"
					ENDIF
				ENDIF
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType) 
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType) 
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_DEATH" 
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 0 //Score
					scLB_DisplayedData.iReadColumns[1] = 2 // deaths
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROWN)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_KILLS)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_DEATHS)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_DEATH" 
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 0 //Score
					scLB_DisplayedData.iReadColumns[1] = 2 // deaths
					scLB_DisplayedData.iReadColumns[2] = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROWN)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_KILLS)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_DEATHS)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ELSE
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_DEATH" 
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
					scLB_DisplayedData.iReadColumns[0] = 0 //Score
					scLB_DisplayedData.iReadColumns[1] = 1 //kills
					scLB_DisplayedData.iReadColumns[2] = 2 // deaths
					scLB_DisplayedData.iReadColumns[3] = 0
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
					//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROWN)
					//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_KILLS)
					//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_DEATHS)
					//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				ENDIF
			ENDIF
		BREAK
		CASE FMMC_TYPE_BASE_JUMP
			//  *SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
		   	// SCORE( AGG_LAST ) : COLUMN_ID_LB_JUMP_SCORE - LBCOLUMNTYPE_INT32
		  	//  TIME( AGG_LAST ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
		  	//  JUMP_COUNT( AGG_SUM ) : COLUMN_ID_LB_NUM_POINTS - LBCOLUMNTYPE_INT32
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_BASEJUMPS)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Mission"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_BJ"
				scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_BJ_NN"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_TIME"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_JUMPS" 
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_RANK"
			scLB_DisplayedData.iReadColumns[0] = 0 //Score
			//setting up invalid time data
			scLB_DisplayedData.iReadColumns[1] = 1 //time
			SET_BIT(scLB_DisplayedData.iInvalidDataSetBS,1)
			scLB_DisplayedData.iInvalidDataInt[1] = -1
			
			scLB_DisplayedData.iReadColumns[2] = 2 // # jumps
			scLB_DisplayedData.iReadColumns[3] = 0
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_CROWN)
			//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_TIME)
			//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_CROWN)
			//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_POSITIVE) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_SP_BASEJUMP
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_BASE_JUMPING)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Location"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType =	"SCLB_BJ"
				scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_BJ_NN"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_CASH"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_TOTCASH"
			scLB_DisplayedData.iReadColumns[0] = 0 //cash
			scLB_DisplayedData.iReadColumns[1] = 3 //total cash
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		//pilot school
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
			SWITCH INT_TO_ENUM(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,iSubType)
				CASE PSCD_DLC_OutsideLoop
				CASE PSCD_DLC_FollowLeader
				CASE PSCD_DLC_VehicleLanding
				CASE PSCD_DLC_CollectFlags
					SWITCH INT_TO_ENUM(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,iSubType)
						CASE PSCD_DLC_OutsideLoop scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_OUTSIDE_LOOP) BREAK
						CASE PSCD_DLC_FollowLeader scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FOLLOW_LEADER) BREAK
						CASE PSCD_DLC_VehicleLanding scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_MOVING_LANDING) BREAK
						CASE PSCD_DLC_CollectFlags scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_COLLECT_FLAGS) BREAK
					ENDSWITCH
					scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ""
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ""
					scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType =	stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "PS_TITLE"
					ENDIF
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_TIME"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_MEDAL1"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MEDAL2"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL3"
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 4 
					scLB_DisplayedData.iReadColumns[2] = 3
					scLB_DisplayedData.iReadColumns[3] = 2
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
					//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
				BREAK
				CASE PSCD_DLC_Engine_failure
				CASE PSCD_DLC_ChaseParachute
				CASE PSCD_DLC_CityLanding
					SWITCH INT_TO_ENUM(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,iSubType)
						CASE PSCD_DLC_Engine_failure scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_ENGINE_FAILURE) BREAK
						CASE PSCD_DLC_ChaseParachute scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CHASE_PARACHUTE) BREAK
						CASE PSCD_DLC_CityLanding scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CITY_LANDING) BREAK
					ENDSWITCH
					scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ""
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ""
					scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType =	stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "PS_TITLE"
					ENDIF
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_DIST"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_MEDAL1"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MEDAL2"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL3"
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 4 
					scLB_DisplayedData.iReadColumns[2] = 3
					scLB_DisplayedData.iReadColumns[3] = 2
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
					CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
					//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
				BREAK

				CASE PSCD_DLC_FlyLow
					scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_GROUND_LEVEL)
					scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ""
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ""
					scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType =	stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "PS_TITLE"
					ENDIF
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_AVG_HEI"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_MEDAL1"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MEDAL2"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL3"
					scLB_DisplayedData.iReadColumns[0] = 1
					scLB_DisplayedData.iReadColumns[1] = 4 
					scLB_DisplayedData.iReadColumns[2] = 3
					scLB_DisplayedData.iReadColumns[3] = 2
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
					CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
					//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
				BREAK
				CASE PSCD_DLC_ShootingRange
				CASE PSCD_DLC_Formation
					SWITCH INT_TO_ENUM(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,iSubType)
						CASE PSCD_DLC_ShootingRange scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_SHOOTING_RANGE) BREAK
						CASE PSCD_DLC_Formation scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FORMATION_FLIGHT) BREAK
					ENDSWITCH
					scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
					scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ""
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ""
					scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
					IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
						scLB_DisplayedData.displaySetup.sLeaderboardType =	stMissionName
					ELSE
						scLB_DisplayedData.displaySetup.sLeaderboardType = "PS_TITLE"
					ENDIF
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_MEDAL1"
					scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MEDAL2"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL3"
					scLB_DisplayedData.iReadColumns[0] = 0
					scLB_DisplayedData.iReadColumns[1] = 3
					scLB_DisplayedData.iReadColumns[2] = 2 
					scLB_DisplayedData.iReadColumns[3] = 1
					//scLB_DisplayedData.iReadColumns[3] = 2
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
					CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
					SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
					//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT_TO_INT)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
				BREAK		
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			/*SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
		    BEST_TIME( AGG_MAX ) : COLUMN_ID_LB_BEST_TIME - LBCOLUMNTYPE_INT64
		    BEST_LANDING_ACC( AGG_MAX ) : COLUMN_ID_LB_LAND_ACCURACY - LBCOLUMNTYPE_INT32
		    NUMBER_GOLD_MEDALS( AGG_SUM ) : COLUMN_ID_LB_GOLD_MEDALS - LBCOLUMNTYPE_INT32*/
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Location" //not sure it needed this??
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType =	stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "PS_TITLE"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_DIST"
			//scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_TIME"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_MEDAL1"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MEDAL2"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL3"
			scLB_DisplayedData.iReadColumns[0] = 2 
			//scLB_DisplayedData.iReadColumns[1] = 1 
			scLB_DisplayedData.iReadColumns[1] = 5
			scLB_DisplayedData.iReadColumns[2] = 4
			scLB_DisplayedData.iReadColumns[3] = 3
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_FT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			/*SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
		    BEST_TIME( AGG_MAX ) : COLUMN_ID_LB_BEST_TIME - LBCOLUMNTYPE_INT64
		    BEST_LANDING_ACC( AGG_MAX ) : COLUMN_ID_LB_LAND_ACCURACY - LBCOLUMNTYPE_INT32
		    NUMBER_GOLD_MEDALS( AGG_SUM ) : COLUMN_ID_LB_GOLD_MEDALS - LBCOLUMNTYPE_INT32*/
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Location" //not sure it needed this??
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType =	stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_MIS_NN"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_TIME"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_MEDAL1"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MEDAL2"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL3"
			scLB_DisplayedData.iReadColumns[0] = 0
			scLB_DisplayedData.iReadColumns[1] = 4 
			scLB_DisplayedData.iReadColumns[2] = 3
			scLB_DisplayedData.iReadColumns[3] = 2
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NEG_B4_DIS_NO_MS) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT)
		BREAK
		CASE FMMC_TYPE_SP_FLIGHT_SCHOOL_RATING
			/*SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
		    BEST_TIME( AGG_MAX ) : COLUMN_ID_LB_BEST_TIME - LBCOLUMNTYPE_INT64
		    BEST_LANDING_ACC( AGG_MAX ) : COLUMN_ID_LB_LAND_ACCURACY - LBCOLUMNTYPE_INT32
		    NUMBER_GOLD_MEDALS( AGG_SUM ) : COLUMN_ID_LB_GOLD_MEDALS - LBCOLUMNTYPE_INT32*/
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_RATING)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "Location" //not sure it needed this??
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType =	"PS_TITLE"
				scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_MIS_NN"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_SCORE"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_TIME"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_ACC"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_MEDAL1"
			scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_MEDAL2"
			scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_MEDAL3"
			scLB_DisplayedData.iReadColumns[0] = 0
			scLB_DisplayedData.iReadColumns[1] = 1 
			scLB_DisplayedData.iReadColumns[2] = 2
			scLB_DisplayedData.iReadColumns[3] = 5
			scLB_DisplayedData.iReadColumns[4] = 4
			scLB_DisplayedData.iReadColumns[5] = 3
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 6
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[5])
			//scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NO_MS) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[5] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		CASE FMMC_TYPE_SP_STUNT_PLANE_RACES
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 3
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "SP"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Location"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = UniqueLBGroup
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Category = "Type"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "StuntPlaneRace"
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)
			scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral = FALSE
			IF NOT IS_STRING_NULL_OR_EMPTY(stMissionName)
				scLB_DisplayedData.displaySetup.sLeaderboardType = stMissionName
			ELSE
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SPR_TITLE"
			ENDIF
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_TIME"
			scLB_DisplayedData.iReadColumns[0] = 2 //time
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 1
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME)
		BREAK
		CASE FMMC_TYPE_SP_STREET_RACE
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 3
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Location"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Category = "Type"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "SP"
			//scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_GRCE_NN"
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER) 
			IF iLaps <= 0
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_VEH"
				scLB_DisplayedData.sColumnTextName[2] = ""//SCLB_C_RANK"
				scLB_DisplayedData.sColumnTextName[3] = ""
				scLB_DisplayedData.iReadColumns[0] = 3
				scLB_DisplayedData.iReadColumns[1] = 4
				scLB_DisplayedData.iReadColumns[2] = 1
				scLB_DisplayedData.iReadColumns[3] = 0
				scLB_DisplayedData.iCustomVehicleColumn = 6
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2//3
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
				////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
				//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_LAP)
				//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_VEHICLE)
				//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_POSITION)
				//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
				//scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			ELSE
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_RT"
				scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_BL"
				scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
				scLB_DisplayedData.sColumnTextName[3] = ""//SCLB_C_RANK"
				scLB_DisplayedData.iReadColumns[0] = 3
				scLB_DisplayedData.iReadColumns[1] = 2
				scLB_DisplayedData.iReadColumns[2] = 4
				scLB_DisplayedData.iReadColumns[3] = 1
				scLB_DisplayedData.iCustomVehicleColumn = 4
				scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3//4
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
				SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
				//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
				scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
				////scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_VEHICLE)
				//scLB_DisplayedData.displaySetup.iIcons[0] = ENUM_TO_INT(ICON_TIME)
				//scLB_DisplayedData.displaySetup.iIcons[1] = ENUM_TO_INT(ICON_LAP)
				//scLB_DisplayedData.displaySetup.iIcons[2] = ENUM_TO_INT(ICON_VEHICLE)
				//scLB_DisplayedData.displaySetup.iIcons[3] = ENUM_TO_INT(ICON_POSITION)
				//scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME) 
				scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
				//scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			ENDIF
			scLB_DisplayedData.iCoronaBoxColumn = 0
			
			SWITCH iSubType
				CASE 0
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGCR_1"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGCR_1"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
				BREAK
				CASE 1
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGCR_2"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGCR_2"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
				BREAK
				CASE 2
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGCR_4"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGCR_4"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
				BREAK
				CASE 3
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGCR_5"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGCR_5"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
				BREAK
				CASE 4
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGCR_6"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGCR_6"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
				BREAK
				CASE 5
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGSR_1"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGSR_1"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
				BREAK
				CASE 6
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGSR_2"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGSR_2"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
				BREAK
				CASE 7
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGSR_3"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGSR_3"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
				BREAK
				CASE 8
					scLB_DisplayedData.displaySetup.sLeaderboardType = "MGSR_4"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = "MGSR_4"
					scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
				BREAK
				DEFAULT
					SCRIPT_ASSERT("FMMC_TYPE_SP_STREET_RACE iSubType was invalid")
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SCLB_TYPE_ARENA_WARS_MODES
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_ARENA)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "ArenaMode"
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_GAMES"
			scLB_DisplayedData.iReadColumns[0] = 0 //wins
			scLB_DisplayedData.iReadColumns[1] = 1 //Points (kills/flags/checkpints/passes)
			scLB_DisplayedData.iReadColumns[2] = 2 //games played
			scLB_DisplayedData.iReadColumns[3] = 3 //points2 (time survived/kills/damage/goals etc)
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = GET_ARENA_WARS_MODE_NAME(iSubType)
			
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			SWITCH iSubType
				CASE SCLB_TYPE_ARENA_MODE_BUZZER_BEATER
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_BUZZ"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TIMESUR" 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NO_MS) 
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_CARNAGE
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_CARN"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_DAMDEALT" 
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_FLAG_WAR
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_FLAGW"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_FLAGS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_KILLS" 
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_WRECK_IT
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_WRECK"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_DAMDEALT" 
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_BOMB_BALL 
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_BOMBB"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_POINTS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_GOALS"
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_GAMES_MASTERS
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_GAMEM"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_CPOINTS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_KILLS"
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_MONSTERS
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_MNSTR"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_DAMDEALT"
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_HOT_BOMB
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_HOTB"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_PASSES"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TIMESUR"
					scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NO_MS) 
				BREAK
				CASE SCLB_TYPE_ARENA_MODE_TAG_TEAM
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_TAGT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_KILLS"
					scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TAGS" 
				BREAK
			ENDSWITCH
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)	
			
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
		BREAK
		
		CASE SCLB_TYPE_ARENA_WARS_CAREER
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_ARENA_CAREER)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ""
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ""
			scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_ARN_CAREER"
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_CARPT"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WLRAT"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_MATCHES"
			scLB_DisplayedData.sColumnTextName[3] = "SCLB_C_TITLE"
			//scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_VEH"
			scLB_DisplayedData.iReadColumns[0] = 0 //score
			scLB_DisplayedData.iReadColumns[1] = 1 //wins ratio
			scLB_DisplayedData.iReadColumns[2] = 3 //# games
			scLB_DisplayedData.iReadColumns[3] = 4 //title
			//scLB_DisplayedData.iReadColumns[4] = 5 //vehicle
			
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)	
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 4
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_4_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_ARENA_TITLE) 
			//scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE)
		BREAK
		
		CASE SCLB_TYPE_CAR_MEET_MEMBERSHIP
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_TUNER_CAR_CLUB)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_PLAYER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 0
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = ""
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = ""
			scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_CCM_CAREER"
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_CCMLVL"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WLRAT"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_RACES"
			//scLB_DisplayedData.sColumnTextName[4] = "SCLB_C_VEH"
			scLB_DisplayedData.iReadColumns[0] = 0 //score
			scLB_DisplayedData.iReadColumns[1] = 4 //wins ratio
			scLB_DisplayedData.iReadColumns[2] = 3 //# races
			//scLB_DisplayedData.iReadColumns[3] = 4 //title
			//scLB_DisplayedData.iReadColumns[4] = 5 //vehicle
			
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)	
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
			
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			CLEAR_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[4])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_CC_MEMBER_RANK) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_FLOAT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			//scLB_DisplayedData.displaySetup.iColumnDisplayType[3] = ENUM_TO_INT(SCLB_DATA_TYPE_ARENA_TITLE) 
			//scLB_DisplayedData.displaySetup.iColumnDisplayType[4] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE)
		BREAK
		
		CASE FMMC_TYPE_SANDBOX_ACTIVITY
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_TUNER_RACES)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 1
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType"
			UniqueLBGroup = "TUNER_SANDB_SVAR"
			IF iSubType = ENUM_TO_INT(SAS_HH_LOCATION_1)
			OR iSubType = ENUM_TO_INT(SAS_HH_LOCATION_2)
				UniqueLBGroup += ENUM_TO_INT(SAS_HH_LOCATION_1)
			ELSE
				UniqueLBGroup += iSubType
			ENDIF
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = UniqueLBGroup
			scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BESTT"
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
			scLB_DisplayedData.iReadColumns[0] = 0 //fastest time
			scLB_DisplayedData.iReadColumns[1] = 2 //wins
			scLB_DisplayedData.iReadColumns[2] = 3 //vehicle
			scLB_DisplayedData.iCustomVehicleColumn = 4	
			
			
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			//SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[3])
			
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NEG_B4_DIS) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
			
			
			SWITCH INT_TO_ENUM(SANDBOX_ACTIVITY_SUBVARIATION,iSubType)
				CASE SAS_TT_LOCATION_1
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_T_TT"
					scLB_DisplayedData.iReadColumns[0] = 0 //fastest time
					scLB_DisplayedData.iReadColumns[1] = 3 //vehicle
					scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BESTT"
					scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_VEH"
					scLB_DisplayedData.sColumnTextName[2] = ""
					scLB_DisplayedData.iNumNonRankColumnsToDisplay = 2
					scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_2_STATS)
					scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NEG_B4_DIS) 
					scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE) 
				BREAK
				CASE SAS_CD_LOCATION_1
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_T_CD"
				BREAK
				CASE SAS_HH_LOCATION_1
				CASE SAS_HH_LOCATION_2
					scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_T_HH1"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_STREET_RACE_SERIES
		CASE FMMC_TYPE_PURSUIT_SERIES
			scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_TUNER_RACES)
			scLB_Control.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			scLB_Control.ReadDataStruct.m_GroupSelector.m_NumGroups = 2
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Category = "Mission"
			scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id = UniqueLBGroup
			
			
			IF iMissionType = FMMC_TYPE_STREET_RACE_SERIES
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "TUNER_SRS"
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_T_SRS"
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BL"
			ELSE
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Category = "GameType"
				scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id = "TUNER_PS"
				scLB_DisplayedData.displaySetup.sLeaderboardType = "SCLB_T_PURS"
				scLB_DisplayedData.sColumnTextName[0] = "SCLB_C_BESTT"
			ENDIF
			scLB_DisplayedData.displaySetup.sLeaderboardName = stMissionName
			scLB_DisplayedData.sColumnTextName[1] = "SCLB_C_WINS"
			scLB_DisplayedData.sColumnTextName[2] = "SCLB_C_VEH"
			scLB_DisplayedData.iReadColumns[0] = 0 //fastest time
			scLB_DisplayedData.iReadColumns[1] = 2 //wins
			scLB_DisplayedData.iReadColumns[2] = 3 //vehicle
			scLB_DisplayedData.iCustomVehicleColumn = 4	
				
			scLB_DisplayedData.iNumNonRankColumnsToDisplay = 3
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[0])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[1])
			SET_BIT(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[2])
			scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_3_STATS)
			scLB_DisplayedData.displaySetup.iColumnDisplayType[0] = ENUM_TO_INT(SCLB_DATA_TYPE_TIME_NEG_B4_DIS) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[1] = ENUM_TO_INT(SCLB_DATA_TYPE_INT) 
			scLB_DisplayedData.displaySetup.iColumnDisplayType[2] = ENUM_TO_INT(SCLB_DATA_TYPE_VEHICLE)  
			
			scLB_DisplayedData.displaySetup.iDisplayType = ENUM_TO_INT(DISPLAY_TYPE_MULTIPLAYER)	
		BREAK
		
	ENDSWITCH
	i = 0
	REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay i
		//IF i < 5
			IF IS_BIT_SET(scLB_DisplayedData.iReadColumnIsIntBS,scLB_DisplayedData.iReadColumns[i])
				SET_BIT(scLB_DisplayedData.iDisplayColumnIsIntBS,i)
				//println("scLB_DisplayedData.iDisplayColumnIsIntBS setting bit, ", i)
			ENDIF
			
		//ENDIF
	ENDREPEAT
	
	scLB_DisplayedData.iCacheHashID = SC_LC_GENERATE_ID_FOR_CACHE(UniqueLBGroup,scLB_control.ReadDataStruct.m_LeaderboardId,iMissionType,iSubType,iLaps,bCoDriver)
	#IF IS_DEBUG_BUILD
	PRINT_LEADERBOARD_SETUP_DETAILS(scLB_control,UniqueLBGroup,iMissionType,iSubType,iLaps,bCoDriver)
	#ENDIF
ENDPROC

PROC SOCIAL_CLUB_CLEAR_NAVIGATION_STRUCT(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	scLB_control.navigation.iButtonBS = 0
	scLB_control.navigation.iCurrentVertSel = -1
	scLB_control.navigation.iMaxVertSel = 0
	RESET_NET_TIMER(scLB_control.navigation.menuNavDelay)
	scLB_control.navigation.bShowingAProfile = FALSE
	INT i
	GAMER_HANDLE emptyHandle
	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
		scLB_control.navigation.slotData[i].iSlot = -1
		scLB_control.navigation.slotData[i].iFinalState = 0
		scLB_control.navigation.slotData[i].playerGamerHandle = emptyHandle
	ENDREPEAT
	REPEAT 3 i
		scLB_control.navigation.iSectionCounter[i] = 0
	ENDREPEAT
	println("SOCIAL_CLUB_CLEAR_NAVIGATION_STRUCT called")  
ENDPROC

PROC SOCIAL_CLUB_CLEAR_DISPLAY_SETUP_STRUCT(SC_LEADERBOARD_DISPLAY_SETUP  &displaySetup)
	displaySetup.iDisplayType = 0
	displaySetup.sLeaderboardType = ""
	displaySetup.sLeaderboardName = ""
	displaySetup.b_sLeaderboardNameLiteral = TRUE
	displaySetup.iLaps = 0
	displaySetup.bCoDriver = FALSE
	displaySetup.iLayout = 0
	INT i
	REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS i 
		displaySetup.iColumnDisplayType[i] = 0
		displaySetup.iValidType[i] = 0
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Clears the global SC leaderboard display struct
PROC SOCIAL_CLUB_CLEAR_DISPLAY_STRUCT()
	INT i, j
	SC_LEADERBOARD_DISPLAY_ROW_STRUCT tempRow
	REPEAT 3 i
		REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS j
			scLB_DisplayedData.rowData[i][j] = tempRow
		ENDREPEAT
		scLB_DisplayedData.iSectionEntries[i] = 0
	ENDREPEAT
	scLB_DisplayedData.iNumNonRankColumnsToDisplay = 0
	scLB_DisplayedData.iCustomVehicleColumn = 0
	i = 0
	REPEAT SC_LEADERBOARD_MAX_DISPLAYED_STATS i
		scLB_DisplayedData.iReadColumns[i] = 0
		scLB_DisplayedData.sColumnTextName[i] = ""
		scLB_DisplayedData.iInvalidDataInt[i] = 0
		scLB_DisplayedData.iInvalidDataFloat[i] = 0
	ENDREPEAT
	scLB_DisplayedData.iInvalidDataSetBS = 0
	
	scLB_DisplayedData.iReadColumnIsIntBS = 0
	scLB_DisplayedData.iDisplayColumnIsIntBS = 0
	i= 0
	REPEAT 3 i
		scLB_DisplayedData.iPlayersRankForSection[i] = 0
		scLB_DisplayedData.iPlayerIndexForSection[i] = 0
	ENDREPEAT
	scLB_DisplayedData.iCoronaBoxColumn = 0
	SOCIAL_CLUB_CLEAR_DISPLAY_SETUP_STRUCT(scLB_DisplayedData.displaySetup)
	RESET_NET_TIMER(scLB_DisplayedData.downloadingTimer)
	scLB_DisplayedData.iLoadLoopCounter = -1
	scLB_DisplayedData.iCacheHashID = 0
	RESET_NET_TIMER(scLB_DisplayedData.optionChangeDelay)
	scLB_DisplayedData.iCurrentViewID = 0
	RESET_NET_TIMER(scLB_DisplayedData.failSafeTimer)
	scLB_DisplayedData.iReadFailedBS = 0
	

	scLB_DisplayedData.displaySetup.iLayout = ENUM_TO_INT(SLOT_LAYOUT_LABEL_NO_ICON)
	scLB_DisplayedData.displaySetup.bCoDriver = FALSE
	scLB_DisplayedData.bResetTriggered = FALSE
	println("Clearing SC display struct")  
ENDPROC

PROC CLEAR_RANK_PREDICTION_ROW(LeaderboardPredictionRow &Row)
	INT i
	Row.m_Id = 0
	Row.m_NumColumns = 0
	Row.m_ColumnsBitSets = 0
	REPEAT MAX_COLUMNS i
		Row.m_fColumnData[i] = 0
		Row.m_iColumnData[i] = 0
	ENDREPEAT
ENDPROC

PROC CLEAR_RANK_REDICTION_DETAILS()
	sclb_rank_predict.iReadStage = 0
	sclb_rank_predict.bFinishedRead = FALSE
	sclb_rank_predict.bFinishedWrite = FALSE
	sclb_rank_predict.bAssignedValuesInWrite = FALSE
	sclb_rank_predict.bNoPreviousValue = FALSE
	CLEAR_RANK_PREDICTION_ROW(scLB_rank_predict.readResult)
	CLEAR_RANK_PREDICTION_ROW(sclb_rank_predict.currentResult)
	CLEAR_RANK_PREDICTION_ROW(sclb_rank_predict.combinedResult)
	CLEAR_RANK_PREDICTION_ROW(sclb_rank_predict.DifficultyCombinedResult)
	sclb_rank_predict.coDriverName = ""
	sclb_rank_predict.driverName = ""
	CLEAR_GAMER_HANDLE_STRUCT(sclb_rank_predict.driverHandle)
	sclb_rank_predict.iRankBeforePrediction = -1
	sclb_useRankPrediction = FALSE
	sclb_rankPredictionDataValid = FALSE
	
	LeaderboardUpdateData emptyData
	sclb_rank_predict.LBWriteDetails = emptyData
	println("CLEAR_RANK_PREDICTION_DETAILS: Called by ", GET_THIS_SCRIPT_NAME())
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
	#ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the social club leaderboard display struct and control struct. Also ends any on going reads.
/// PARAMS:
///    scLB_control - The control struct for loading the data
PROC CLEANUP_SOCIAL_CLUB_LEADERBOARD(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control, BOOL bForceCleanup = FALSE)
	IF scLB_control.iTempLoadStage != 0
		println("Ending read in CLEANUP_SOCIAL_CLUB_LEADERBOARD in  ", GET_THIS_SCRIPT_NAME() ) 
		END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
	ENDIF
	IF scLB_control.iLoadStage[0] != 0
	OR scLB_control.iLoadStage[1] != 0
	OR scLB_control.iLoadStage[2] != 0
	OR bForceCleanup = TRUE
		SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(scLB_control)
		SOCIAL_CLUB_CLEAR_NAVIGATION_STRUCT(scLB_control)
		CLEAR_FINAL_DATA_STRUCT(tempFinalData)
		//println("CLEANUP_SOCIAL_CLUB_LEADERBOARD called")  
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		PRINTLN("CLEANUP_SOCIAL_CLUB_LEADERBOARD - called this frame")
	ENDIF
	IF scLB_DisplayedData.iNumNonRankColumnsToDisplay != 0
	OR scLB_DisplayedData.bResetTriggered
		SOCIAL_CLUB_CLEAR_DISPLAY_STRUCT()
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("LEADERBOARD_SCENE")
		STOP_AUDIO_SCENE("LEADERBOARD_SCENE") 
	ENDIF
	IF NETWORK_IS_SIGNED_ONLINE()
		RESET_NET_TIMER(scLBSignInWarn.signedOutTimer)
	ENDIF
	MPGlobalsAmbience.bPlayerViewingLeaderboard = FALSE
ENDPROC

PROC CLEANUP_CORONA_LEADERBOARD(BOOL& bDrawingMenu,BOOL& bMenuActive,SC_LEADERBOARD_CONTROL_STRUCT& scLB_control,SCALEFORM_INDEX& scaleformIndex[],SCALEFORM_INSTRUCTIONAL_BUTTONS& scaleformStruct, BOOL bInCorona = FALSE)
	IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
	OR IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
		IF HAS_SCALEFORM_MOVIE_LOADED(scaleformIndex[0])
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformIndex[0])
			//println("CLEANUP_CORONA_LEADERBOARD called setting SC scaleform movie 0 as no longer needed") 
		ENDIF
		//CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
		//CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
		scLB_Control.ibs = 0
		IF HAS_SCALEFORM_MOVIE_LOADED(scaleformIndex[1])
			BEGIN_SCALEFORM_MOVIE_METHOD(scaleformIndex[1], "CLEAR_ALL")
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF bInCorona = FALSE
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(scaleformStruct)
		ENDIF
		println("CLEANUP_CORONA_LEADERBOARD called") 
	ENDIF
	SOCIAL_CLUB_CLEAR_NAVIGATION_STRUCT(scLB_control)
	MPGlobalsAmbience.bPlayerViewingLeaderboard = FALSE
	bMenuActive = FALSE
	bDrawingMenu = FALSE
ENDPROC

FUNC BOOL SC_LB_OK_TO_ALLOW_MENU_NAVIGATION_AGAIN(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control,CONTROL_ACTION theInput)
	IF theInput = INPUT_FRONTEND_UP 
	OR theInput = INPUT_FRONTEND_DOWN 
		INT iL_LR, iL_UD, iR_LR, iR_UD
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iL_LR, iL_UD, iR_LR, iR_UD)
		
		IF ((NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
		AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)) AND (iL_UD < 75 and iL_UD > -75))
		OR HAS_NET_TIMER_EXPIRED(scLB_control.navigation.menuNavDelay,250)
			//#IF IS_DEBUG_BUILD
				//IF theInput = INPUT_FRONTEND_DOWN 
//					println("OK to allow input: iL_UD = ", iL_UD)
//					IF HAS_NET_TIMER_EXPIRED(scLB_control.navigation.menuNavDelay,250)
//						println("OK to allow input: timer expired ")
//					ENDIF
				//ENDIF
			//#ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
		AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,theInput))
		OR HAS_NET_TIMER_EXPIRED(scLB_control.navigation.menuNavDelay,250)
//			IF HAS_NET_TIMER_EXPIRED(scLB_control.navigation.menuNavDelay,250)
//				println("OK to allow input: timer expired ")
//			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC INT FIND_NO_ZERO_SLOT(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
//	REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS 
//		IF scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iSlot <= 0
//			scLB_control.navigation.iCurrentVertSel++
//		ENDIF
//	ENDREPEAT
//ENDFUNC

PROC HANDLE_SELECTION_OF_PLAYERS_LISTED(SCALEFORM_INDEX& scaleFormID, SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	//SC_LEADERBOARD_CONTROL_STRUCT scLB_control
	//SC_FINAL_DATA finaldata[]
	
	BOOL bChanged
	INT iTempVertSelection = scLB_control.navigation.iCurrentVertSel
	
	// PC mouse support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		SET_MOUSE_CURSOR_THIS_FRAME()
		
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_X)
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_Y)
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		// Scroll up
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_UP, 1)
		ENDIF
		
		// Scroll down
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN, 1)
		ENDIF
				
		eMOUSE_EVT eMouseEvent
		INT iUID
		INT iContext
		
		// Check mouse events
		IF GET_MOUSE_EVENT(scaleFormID, eMouseEvent, iUID, iContext )
			IF eMouseEvent = EVENT_TYPE_MOUSE_PRESS
							
				IF iUID > scLB_control.navigation.iSectionCounter[0] // The iUID isn't in section 0 (global)
					IF iUID <= scLB_control.navigation.iSectionCounter[0] + scLB_control.navigation.iSectionCounter[1] + 2 // The iUID is in section 1 (friends)
						iUID -= 2 // Reduce the iUID by 2 blank rows
					ELSE // The iUID is in section 2 (crew)
						iUID -= 4 // Reduce the iUID by 4 blank rows
					ENDIF
				ENDIF
				
				iUID -= 1 // Reduce the iUID by 1 so it syncs with scLB_control.navigation.iCurrentVertSel
				
				println("HANDLE_SELECTION_OF_PLAYERS_LISTED iSectionCounter 0 = ", scLB_control.navigation.iSectionCounter[0], " iSectionCounter 1 = ", scLB_control.navigation.iSectionCounter[1], " iSectionCounter 2 = ", scLB_control.navigation.iSectionCounter[2], " iUID = ", iUID) 
																
				// Select new item
				IF scLB_control.navigation.iCurrentVertSel != iUID
					scLB_control.navigation.iCurrentVertSel = iUID
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					bChanged = TRUE
				ELSE
				// Confirm current item to bring up social club profile
					SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, 1)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
					
	INT iL_LR, iL_UD, iR_LR, iR_UD
	
	// Don't want these getting values when using keyboard and mouse
	// as they can cause conflicts with the frontend controls. - Steve R LDS
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iL_LR, iL_UD, iR_LR, iR_UD)
	ENDIF
	
	
	IF scLB_control.navigation.iMaxVertSel > 0
		IF NOT IS_BIT_SET(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_UP)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
			OR iL_UD < -100
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				scLB_control.navigation.iCurrentVertSel += -1
				SET_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_UP)
				RESET_NET_TIMER(scLB_control.navigation.menuNavDelay)
				bChanged = TRUE
			ENDIF
		ELSE
			IF SC_LB_OK_TO_ALLOW_MENU_NAVIGATION_AGAIN(scLB_control,INPUT_FRONTEND_UP)
				CLEAR_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_UP)	
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_DOWN)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
			OR  iL_UD > 100
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				scLB_control.navigation.iCurrentVertSel += 1
				SET_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_DOWN)
				RESET_NET_TIMER(scLB_control.navigation.menuNavDelay)
				bChanged = TRUE
				//println("Player pressed down on the social club leaderboard.")
			ENDIF
		ELSE
			IF SC_LB_OK_TO_ALLOW_MENU_NAVIGATION_AGAIN(scLB_control,INPUT_FRONTEND_DOWN)
				CLEAR_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_DOWN)	
			ENDIF
		ENDIF
	ENDIF
	IF bChanged
		IF scLB_control.navigation.iCurrentVertSel < 0
			scLB_control.navigation.iCurrentVertSel	= scLB_control.navigation.iMaxVertSel-1
		ENDIF
		IF scLB_control.navigation.iCurrentVertSel >= scLB_control.navigation.iMaxVertSel
			scLB_control.navigation.iCurrentVertSel = 0
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_Y)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			SET_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_Y)
			RESET_NET_TIMER(scLB_control.navigation.menuNavDelay)
			bChanged = TRUE
		ENDIF
	ELSE
		IF SC_LB_OK_TO_ALLOW_MENU_NAVIGATION_AGAIN(scLB_control,INPUT_FRONTEND_Y)
			CLEAR_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_Y)	
		ENDIF
	ENDIF
	
	IF scLB_control.navigation.iCurrentVertSel >= 0				
		IF scLB_control.navigation.iCurrentVertSel != iTempVertSelection
			println("iTempVertSelection = ", iTempVertSelection," iCurrentVertSel = ", scLB_control.navigation.iCurrentVertSel," iSlot = ", scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iSlot)
			//highlight appropriate slot/unhighlight last slot
			IF iTempVertSelection >= 0
				CLEAR_BIT(scLB_control.navigation.slotData[iTempVertSelection].iFinalState,SCLB_SLOT_STATE_BIT_SELECTED)
				SET_SC_LEADERBOARD_SLOT_FINAL_STATE(scaleFormID,scLB_control.navigation.slotData[iTempVertSelection].iSlot,scLB_control.navigation.slotData[iTempVertSelection].iFinalState)
			ENDIF
			SET_BIT(scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iFinalState,SCLB_SLOT_STATE_BIT_SELECTED)
			SET_SC_LEADERBOARD_SLOT_FINAL_STATE(scaleFormID,scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iSlot,scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iFinalState)
			
			CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
		ENDIF
		IF IS_GAMER_HANDLE_VALID(scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].playerGamerHandle)
			IF NOT IS_BIT_SET(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_SELECT)
				
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_SELECT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_SELECT)
					IF NOT IS_SYSTEM_UI_BEING_DISPLAYED()
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_MP_SOUNDSET")
						SET_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_SELECT)
						NETWORK_SHOW_PROFILE_UI(scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].playerGamerHandle)
						PRINTLN("CDM: Activating profile UI for selected player")
					ELSE
						PRINTLN("CDM: System UI active not displaying profile UI")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_SELECT)
					CLEAR_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_SELECT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	BOOL scButtonActive
//	IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
//		scButtonActive = TRUE
//	ELSE
//		IF NOT NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()
//			scButtonActive = TRUE
//		ENDIF
//	ENDIF
//	IF scButtonActive
//		IF NOT IS_BIT_SET(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_ACCEPT)
//			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)	
//			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//				#IF IS_DEBUG_BUILD
//					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)	
//						println("HANDLE_SELECTION_OF_PLAYERS_LISTED: IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)")
//					ENDIF
//					IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//						println("HANDLE_SELECTION_OF_PLAYERS_LISTED:IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)")
//					ENDIF
//				#ENDIF
//				PLAY_SOUND_FRONTEND(-1, "ACCEPT","HUD_FRONTEND_MP_SOUNDSET")
//				SET_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_ACCEPT)
//				println("HANDLE_SELECTION_OF_PLAYERS_LISTED player pressed cancel when on leaderboard") 
//				SET_SOCIAL_CLUB_TOUR("General")
//				OPEN_SOCIAL_CLUB_MENU()
//				SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SOCIAL_CLUB_TOUR)
//				exit
//			ENDIF
//		ELSE
//			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//			AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//				CLEAR_BIT(scLB_control.navigation.iButtonBS,SC_LB_PRESSED_ACCEPT)
//			ENDIF
//		ENDIF
//	ENDIF
			
ENDPROC

/// PURPOSE:
///    Function to indicate whether it is ok to show profile option or not.
/// PARAMS:
///    scLB_control - leaderboard control struct.
/// RETURNS:
///    True if it is ok to show the button
FUNC BOOL SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
	AND scLB_DisplayedData.iSectionEntries[0] > 0
	AND scLB_control.navigation.iCurrentVertSel >= 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Draws all the components of the SC board.
/// PARAMS:
///    scaleFormID - scaleform index of the SC leaderboard
///    scLB_control - control struct handling loading the data if needed
PROC DRAW_SC_SCALEFORM_LEADERBOARD(SCALEFORM_INDEX& scaleFormID,SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	INT i
	INT iDisplayLoop
	//BOOL highlightGlobalTop
	//BOOl highlightFriendTop
	//BOOL highlightCrewTop
	INT iSectionCounter[3]
	//
	INT iSlotCounter
	REINIT_NET_TIMER(scLBSignInWarn.signedOutTimer,TRUE)
	BOOL bHighlightedARow
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
	IF NOT IS_PLAYER_ACTIVE_IN_CORONA()
		IF NOT IS_AUDIO_SCENE_ACTIVE("LEADERBOARD_SCENE")
			START_AUDIO_SCENE("LEADERBOARD_SCENE") 
		ENDIF
	ENDIF
	BOOL bInValidDataForThisColumn
	
	#IF IS_DEBUG_BUILD
	BOOL bDisplayedUIThisFrame = FALSE
	#ENDIF
//	IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SOCIAL_CLUB_TOUR)
//		IF NOT IS_SOCIAL_CLUB_ACTIVE()
//		//AND NOT IS_PAUSE_MENU_ACTIVE()
//			println("NOT IS_SOCIAL_CLUB_ACTIVE() clearing flag") 
//			CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SOCIAL_CLUB_TOUR)
//		ENDIF
//	ELSE
		TEXT_LABEL_63 tempText
		TEXT_LABEL_7 emptyText
		IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_REQUESTED_MOVIE)
			scaleFormID = REQUEST_SC_LEADERBOARD_UI()
			SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_REQUESTED_MOVIE)
		ENDIF
		BOOL bAddedSectionHeader[3]
		
		//TEXT_LABEL_15 tempCrewTag[2]
		
	
		
		INT iFinalState
		GAMER_HANDLE localPlayerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
		GAMER_USERID localUserID
		
		IF HAS_SCALEFORM_MOVIE_LOADED(scaleFormID)
			IF NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()
			OR NOT IS_PLAYER_ONLINE()
			OR (NOT NETWORK_HAVE_ONLINE_PRIVILEGES() AND NETWORK_HAS_AGE_RESTRICTIONS())
			OR scLB_DisplayedData.iReadFailedBS != 0
//				IF NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()
//					PRINTLN("CDM: 1951069- NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()")
//				ENDIF
//				IF NOT IS_PLAYER_ONLINE()
//					PRINTLN("CDM: 1951069- NOT IS_PLAYER_ONLINE()")
//				ENDIF
//				IF NOT NETWORK_HAVE_ONLINE_PRIVILEGES()
//					PRINTLN("CDM: 1951069-  NOT NETWORK_HAVE_ONLINE_PRIVILEGES()")
//				ENDIF
				

				IF NOT IS_PLAYER_ONLINE()
					IF scLB_DisplayedData.iCurrentViewID != SC_LB_VIEW_ID_NOT_ONLINE
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
						scLB_DisplayedData.iCurrentViewID = SC_LB_VIEW_ID_NOT_ONLINE
					ENDIF
				ELSE
					IF (NOT NETWORK_HAVE_ONLINE_PRIVILEGES() AND NETWORK_HAS_AGE_RESTRICTIONS())
						IF scLB_DisplayedData.iCurrentViewID != SC_LB_VIEW_ID_NO_PERMISSION
							CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
							scLB_DisplayedData.iCurrentViewID = SC_LB_VIEW_ID_NO_PERMISSION
						ENDIF
					ELSE
						IF NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()
							IF scLB_DisplayedData.iCurrentViewID != SC_LB_VIEW_ID_NO_ROS
								CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
								scLB_DisplayedData.iCurrentViewID = SC_LB_VIEW_ID_NO_ROS
							ENDIF
						ELSE
							IF scLB_DisplayedData.iReadFailedBS != 0
								IF scLB_DisplayedData.iCurrentViewID != SC_LB_VIEW_ID_READ_FAILED
									CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
									scLB_DisplayedData.iCurrentViewID = SC_LB_VIEW_ID_READ_FAILED
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
				//println("Player cannot view leaderboard.")
				IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
//					IF scLB_DisplayedData.iCurrentViewID = SC_LB_VIEW_ID_NOT_ONLINE
//						IF IS_SYSTEM_UI_BEING_DISPLAYED()
//							EXIT
//						ELSE
//							IF HAS_NET_TIMER_EXPIRED()
//								
//							ENDIF
//						ENDIF
//					ENDIF
					BEGIN_SCALEFORM_MOVIE_METHOD(scaleFormID,"CLEAR_ALL_SLOTS")
					END_SCALEFORM_MOVIE_METHOD()
					SET_SC_LEADERBOARD_DISPLAY_TYPE(scaleFormID,INT_TO_ENUM(DISPLAY_TYPES,scLB_DisplayedData.displaySetup.iDisplayType))
					println("Setting LB display type = ",scLB_DisplayedData.displaySetup.iDisplayType )
					IF DOES_TEXT_LABEL_EXIST(scLB_DisplayedData.displaySetup.sLeaderboardType)
						IF NOT IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
							IF scLB_DisplayedData.displaySetup.iLaps > 0
								tempText = scLB_DisplayedData.displaySetup.sLeaderboardName
								//tempText += "- "
								//temptext += scLB_DisplayedData.displaySetup.iLaps
								SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,temptext,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
							ELSE
								SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
							ENDIF 
						ELSE
							IF NOT scLB_DisplayedData.displaySetup.bCoDriver
								tempText = "FMMC_COR_SCLB5"
								IF scLB_DisplayedData.displaySetup.iLaps > 0
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
								ELSE
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
								ENDIF
							ELSE
								tempText = "FMMC_COR_SCLB6"
								IF scLB_DisplayedData.displaySetup.iLaps > 0
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
								ELSE
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
								ENDIF
							ENDIF
						ENDIF
						SET_SC_LEADERBOARD_COLUMN_TITLES(scaleFormID,"SCLB_C_RANK",scLB_DisplayedData.sColumnTextName,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
					ENDIF
					iFinalState = 0 
					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
					//iSlotCounter++
					iFinalState = 0 
					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
					//iSlotCounter++
					iFinalState = 0 
					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
					SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
					CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
					CLEAR_HELP()
				ELSE
					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
				ENDIF
//			IF NOT IS_PLAYER_ONLINE()
//				IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
//					BEGIN_SCALEFORM_MOVIE_METHOD(scaleFormID,"CLEAR_ALL_SLOTS")
//					END_SCALEFORM_MOVIE_METHOD()
//					SET_SC_LEADERBOARD_DISPLAY_TYPE(scaleFormID,INT_TO_ENUM(DISPLAY_TYPES,scLB_DisplayedData.displaySetup.iDisplayType))
//					println("Setting LB display type = ",scLB_DisplayedData.displaySetup.iDisplayType )
//					IF DOES_TEXT_LABEL_EXIST(scLB_DisplayedData.displaySetup.sLeaderboardType)
//						IF NOT IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
//							IF scLB_DisplayedData.displaySetup.iLaps > 0
//								tempText = scLB_DisplayedData.displaySetup.sLeaderboardName
//								//tempText += "- "
//								//temptext += scLB_DisplayedData.displaySetup.iLaps
//								SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,temptext,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
//							ELSE
//								SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
//							ENDIF 
//						ELSE
//							IF NOT scLB_DisplayedData.displaySetup.bCoDriver
//								tempText = "FMMC_COR_SCLB5"
//								//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,temptext,FALSE)
//								IF scLB_DisplayedData.displaySetup.iLaps > 0
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
//								ELSE
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
//								ENDIF
//							ELSE
//								tempText = "FMMC_COR_SCLB6"
//								//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,FALSE)
//								IF scLB_DisplayedData.displaySetup.iLaps > 0
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
//								ELSE
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
//								ENDIF
//							ENDIF
//						ENDIF
//						SET_SC_LEADERBOARD_COLUMN_TITLES(scaleFormID,"SCLB_C_RANK",scLB_DisplayedData.sColumnTextName,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
//					ENDIF
//					iFinalState = 0 
//					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
//					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
//					//iSlotCounter++
//					iFinalState = 0 
//					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
//					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
//					//iSlotCounter++
//					iFinalState = 0 
//					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
//					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
//					SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
//					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
//					CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
//					CLEAR_HELP()
//				ELSE
//					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
//				ENDIF
//			ELIF NOT NETWORK_HAVE_ONLINE_PRIVILEGES()
//				IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
//					BEGIN_SCALEFORM_MOVIE_METHOD(scaleFormID,"CLEAR_ALL_SLOTS")
//					END_SCALEFORM_MOVIE_METHOD()
//					SET_SC_LEADERBOARD_DISPLAY_TYPE(scaleFormID,INT_TO_ENUM(DISPLAY_TYPES,scLB_DisplayedData.displaySetup.iDisplayType))
//					println("Setting LB display type = ",scLB_DisplayedData.displaySetup.iDisplayType )
//					IF DOES_TEXT_LABEL_EXIST(scLB_DisplayedData.displaySetup.sLeaderboardType)
//						IF NOT IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
//							IF scLB_DisplayedData.displaySetup.iLaps > 0
//								tempText = scLB_DisplayedData.displaySetup.sLeaderboardName
//								//tempText += "- "
//								//temptext += scLB_DisplayedData.displaySetup.iLaps
//								SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,temptext,TRUE,scLB_DisplayedData.displaySetup.iLaps)
//							ELSE
//								SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
//							ENDIF
//						ELSE
//							IF NOT scLB_DisplayedData.displaySetup.bCoDriver
//								tempText = "FMMC_COR_SCLB5"
//								//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,temptext,FALSE)
//								IF scLB_DisplayedData.displaySetup.iLaps > 0
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
//								ELSE
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
//								ENDIF
//							ELSE
//								tempText = "FMMC_COR_SCLB6"
//								//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,FALSE)
//								IF scLB_DisplayedData.displaySetup.iLaps > 0
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
//								ELSE
//									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
//								ENDIF
//							ENDIF
//						ENDIF
//						SET_SC_LEADERBOARD_COLUMN_TITLES(scaleFormID,"SCLB_C_RANK",scLB_DisplayedData.sColumnTextName,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
//					ENDIF
//					iFinalState = 0 
//					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
//					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
//					//iSlotCounter++
//					iFinalState = 0 
//					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
//					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
//					//iSlotCounter++
//					iFinalState = 0 
//					SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
//					SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE,TRUE)
//					SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
//					CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
//					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
//					CLEAR_HELP()
//				ELSE
//					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
//				ENDIF
			ELSE
				IF scLB_DisplayedData.iCurrentViewID != SC_LB_VIEW_ID_NORMAL
					CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					scLB_DisplayedData.iCurrentViewID = SC_LB_VIEW_ID_NORMAL
				ENDIF
				IF NOT LOAD_SOCIAL_CLUB_LEADERBOARD_DATA(scLB_control)
					scLB_control.navigation.iCurrentVertSel = -1
					CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					
					IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
						BEGIN_SCALEFORM_MOVIE_METHOD(scaleFormID,"CLEAR_ALL_SLOTS")
						END_SCALEFORM_MOVIE_METHOD()
						SET_SC_LEADERBOARD_DISPLAY_TYPE(scaleFormID,INT_TO_ENUM(DISPLAY_TYPES,scLB_DisplayedData.displaySetup.iDisplayType))
						println("Setting LB display type = ",scLB_DisplayedData.displaySetup.iDisplayType )
						IF DOES_TEXT_LABEL_EXIST(scLB_DisplayedData.displaySetup.sLeaderboardType)
							IF NOT IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
								IF scLB_DisplayedData.displaySetup.iLaps > 0
									tempText = scLB_DisplayedData.displaySetup.sLeaderboardName
									//tempText += "- "
									//temptext += scLB_DisplayedData.displaySetup.iLaps
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,temptext,TRUE,scLB_DisplayedData.displaySetup.iLaps)
								ELSE
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
								ENDIF
							ELSE
								IF NOT scLB_DisplayedData.displaySetup.bCoDriver
									tempText = "FMMC_COR_SCLB5"
									//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,temptext,FALSE)
									IF scLB_DisplayedData.displaySetup.iLaps > 0
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
									ELSE
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
									ENDIF
								ELSE
									tempText = "FMMC_COR_SCLB6"
									//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,FALSE)
									IF scLB_DisplayedData.displaySetup.iLaps > 0
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
									ELSE
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
									ENDIF
								ENDIF
							ENDIF
							SET_SC_LEADERBOARD_COLUMN_TITLES(scaleFormID,"SCLB_C_RANK",scLB_DisplayedData.sColumnTextName,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
						ENDIF
						SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
					ENDIF
						
					iSlotCounter = 0
					i = 0
					IF scLB_DisplayedData.iLoadLoopCounter = -1
						TEXT_LABEL_23 textLabel = "SC_LB_DL0"
						
						REPEAT 3 i
							IF i = 0
								iFinalState = 0 
								SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
								SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
							ELIF i = 1
								iFinalState = 0 
								SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
								SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
							ELIF i = 2
								iFinalState = 0 
								SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
								SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
							ENDIF	
							//iSlotCounter++
							iFinalState = 0 
							SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_DESCRIPTION)
							SET_SC_LEADERBOARD_SLOT_TEXT_DESCRIPTION_ONLY(scaleFormID, iSlotCounter,iFinalState,textLabel)//,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
							iSlotCounter++
						ENDREPEAT
						scLB_DisplayedData.iLoadLoopCounter = 1
						RESET_NET_TIMER(scLB_DisplayedData.downloadingTimer)
						
					ELSE
						IF HAS_NET_TIMER_EXPIRED(scLB_DisplayedData.downloadingTimer,300)
							TEXT_LABEL_23 textLabel = "SC_LB_DL"
							textLabel += scLB_DisplayedData.iLoadLoopCounter
							REPEAT 3 i
								IF i = 0
									iFinalState = 0 
									SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
									SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
								ELIF i = 1
									iFinalState = 0 
									SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
									SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
								ELIF i = 2
									iFinalState = 0 
									SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
									SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
								ENDIF	
								//iSlotCounter++
								iFinalState = 0 
								SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_DESCRIPTION)
								SET_SC_LEADERBOARD_SLOT_TEXT_DESCRIPTION_ONLY(scaleFormID, iSlotCounter,iFinalState,textLabel)//,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
								iSlotCounter++
							ENDREPEAT
							scLB_DisplayedData.iLoadLoopCounter++
							IF scLB_DisplayedData.iLoadLoopCounter > 3
								scLB_DisplayedData.iLoadLoopCounter = 0
							ENDIF
							RESET_NET_TIMER(scLB_DisplayedData.downloadingTimer)
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					bDisplayedUIThisFrame = TRUE
					#ENDIF
					DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
				ELSE
					CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
//					bAddedSectionHeader[0] = FALSE
//					bAddedSectionHeader[1] = FALSE
//					bAddedSectionHeader[2] = FALSE
					//scLB_control.navigation.iCurrentVertSel = -1
					IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
						REPEAT 3 i
							scLB_control.navigation.iSectionCounter[i] = 0
						ENDREPEAT
						BEGIN_SCALEFORM_MOVIE_METHOD(scaleFormID,"CLEAR_ALL_SLOTS")
						END_SCALEFORM_MOVIE_METHOD()
						SET_SC_LEADERBOARD_DISPLAY_TYPE(scaleFormID,INT_TO_ENUM(DISPLAY_TYPES,scLB_DisplayedData.displaySetup.iDisplayType))
						println("Setting LB display type = ",scLB_DisplayedData.displaySetup.iDisplayType )
						IF DOES_TEXT_LABEL_EXIST(scLB_DisplayedData.displaySetup.sLeaderboardType)
							IF NOT IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
								IF scLB_DisplayedData.displaySetup.iLaps > 0
									tempText = scLB_DisplayedData.displaySetup.sLeaderboardName
									//tempText += "- "
									//temptext += scLB_DisplayedData.displaySetup.iLaps
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,temptext,TRUE,scLB_DisplayedData.displaySetup.iLaps)
								ELSE
									SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,emptyText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
								ENDIF
							ELSE
								IF NOT scLB_DisplayedData.displaySetup.bCoDriver
									tempText = "FMMC_COR_SCLB5"
									//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,temptext,FALSE)
									IF scLB_DisplayedData.displaySetup.iLaps > 0
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
									ELSE
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
									ENDIF
								ELSE
									tempText = "FMMC_COR_SCLB6"
									//SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,FALSE)
									IF scLB_DisplayedData.displaySetup.iLaps > 0
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral,scLB_DisplayedData.displaySetup.iLaps)
									ELSE
										SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(scaleFormID,scLB_DisplayedData.displaySetup.sLeaderboardType,tempText,scLB_DisplayedData.displaySetup.sLeaderboardName,scLB_DisplayedData.displaySetup.b_sLeaderboardNameLiteral)
									ENDIF
								ENDIF
							ENDIF
							SET_SC_LEADERBOARD_COLUMN_TITLES(scaleFormID,"SCLB_C_RANK",scLB_DisplayedData.sColumnTextName,scLB_DisplayedData.iNumNonRankColumnsToDisplay)
						ENDIF
						//INT iGlobalCounter,iFriendCounter, iCrewCounter
						IF NOT IS_BIT_SET(scLB_control.iBS,SC_LEADERBOARD_CONTROL_GENERATED_FINAL_LIST)
							CLEAR_FINAL_DATA_STRUCT(tempFinalData)
							GET_FINAL_DATA_LIST(scLB_control,tempFinalData)
							ORDER_FINAL_LIST(scLB_control,tempFinalData)
						ENDIF
						iSlotCounter = 0
						scLB_control.navigation.iMaxVertSel = 0
						IF scLB_DisplayedData.iSectionEntries[0] > 1
						OR (scLB_DisplayedData.iSectionEntries[0] > 0 AND scLB_DisplayedData.iPlayerIndexForSection[0] != -1)
						OR (scLB_DisplayedData.iSectionEntries[0] > 0 AND scLB_DisplayedData.displaySetup.bCoDriver AND IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId) AND scLB_DisplayedData.iPlayerIndexForSection[0] != -1)
							scLB_control.navigation.iCurrentVertSel = -1
							i = 0
							REPEAT SC_LEADERBOARD_MAX_NUM_DISPLAYED_ROWS i
								BOOL bSkipRow = FALSE
								IF tempFinalData[i].iSection = 1
									IF NOT bAddedSectionHeader[0]
										iFinalState = 0 
										
										SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
										SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
										//iSlotCounter++
										bAddedSectionHeader[0] = TRUE
									ENDIF
								ELIF tempFinalData[i].iSection = 2
									IF NOT bAddedSectionHeader[1]
										iFinalState = 0 
										SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
										IF scLB_DisplayedData.iSectionEntries[1] < 1
										AND scLB_DisplayedData.iPlayerIndexForSection[1] = -1
										AND NOT (scLB_DisplayedData.iSectionEntries[1] > 0 AND scLB_DisplayedData.displaySetup.bCoDriver AND IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId) AND scLB_DisplayedData.iPlayerIndexForSection[1] != -1)
											SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE)
											bSkipRow = TRUE
											//bSkipRow = TRUE
										ELSE
											SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
										ENDIF
										bAddedSectionHeader[1] = TRUE
									ENDIF
								ELIF tempFinalData[i].iSection = 3
									IF NOT bAddedSectionHeader[2]
										iFinalState = 0 
										SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
										IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
											bSkipRow = TRUE
										ELSE
											IF NOT NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()
												bSkipRow = TRUE
											ENDIF
										ENDIF
										IF scLB_DisplayedData.iSectionEntries[2] < 2
										AND scLB_DisplayedData.iPlayerIndexForSection[2] = -1
										AND NOT (scLB_DisplayedData.iSectionEntries[2] > 0 AND scLB_DisplayedData.displaySetup.bCoDriver AND IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId) AND scLB_DisplayedData.iPlayerIndexForSection[2] != -1)
											bSkipRow = TRUE
										ENDIF
										IF bSkipRow 
											SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE)
										ELSE
											SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,FALSE)
										ENDIF
										bAddedSectionHeader[2] = TRUE
									ENDIF
								ENDIF
								
								//IF NOT ARE_STRINGS_EQUAL(tempFinalData[i].rowData.tl23_ParticipantName,"")
								IF IS_GAMER_HANDLE_VALID(tempFinalData[i].rowData.playerGamerHandle)
									IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
										NETWORK_PLAYER_GET_USERID(PLAYER_ID(),localUserID)
										IF NOT tempFinalData[i].rowData.bValidData 
										AND ARE_STRINGS_EQUAL(scLB_Control.ReadDataStruct.m_GroupSelector.m_Group[1].m_Id,localUserID.Name)
											bSkipRow = TRUE
										ENDIF
									ENDIF
									IF NOT bSkipRow
										iFinalState = 0 
										IF NOT scLB_DisplayedData.displaySetup.bCoDriver
											//IF ARE_STRINGS_EQUAL(tempFinalData[i].rowData.tl23_ParticipantName,GET_PLAYER_NAME(PLAYER_ID()))
											IF SCLB_CHECK_HANDLES_ARE_THE_SAME(tempFinalData[i].rowData.playerGamerHandle,localPlayerHandle)
												SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_IS_PLAYER)
												IF scLB_control.navigation.iCurrentVertSel = -1
													bHighlightedARow = TRUE
													scLB_control.navigation.iCurrentVertSel = i
													SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_SELECTED)
													//SET_SC_LEADERBOARD_SLOT_FINAL_STATE(scaleFormID,scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iSlot,scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iFinalState)
												ENDIF
											ENDIF
										ENDIF
										IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
											tempText = tempFinalData[i].rowData.tl23_ParticipantName
											IF NOT IS_STRING_NULL_OR_EMPTY(tempFinalData[i].coDriverDisplayName)
											AND NOT ARE_STRINGS_EQUAL(tempFinalData[i].coDriverDisplayName,"")
												temptext += "/"
												tempText += tempFinalData[i].coDriverDisplayName
											ENDIF
													
											//GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(tempFinalData[i].rowData.playerGamerHandle,tempCrewTag[0])
											println("Entry for slot : ",iSlotCounter, " CrewID = ",tempFinalData[i].crewTag)
											//GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(GET_GAMER_HANDLE_STRING(finalData[i].rowData.tl31_CoDriver),tempCrewTag[1])
											START_SC_LEADERBOARD_SLOT(scaleFormID,iSlotCounter,iFinalState,tempFinalData[i].rowData.iRank,
																	tempText,tempFinalData[i].crewTag)//tempCrewTag[0])
											scLB_control.navigation.slotData[i].iSlot = iSlotCounter
											println("scLB_control.navigation.slotData[",i,"].iSlot = ",scLB_control.navigation.slotData[i].iSlot)
											scLB_control.navigation.slotData[i].iFinalState = iFinalState
											scLB_control.navigation.slotData[i].playerGamerHandle = tempFinalData[i].rowData.playerGamerHandle
											scLB_control.navigation.iMaxVertSel++
										ELSE
											//GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(tempFinalData[i].rowData.playerGamerHandle,tempFinalData[i].crewTag)//tempCrewTag[0])
											println("Entry for slot : ",iSlotCounter, " CrewID = ",tempFinalData[i].crewTag)//tempCrewTag[0])
											START_SC_LEADERBOARD_SLOT(scaleFormID,iSlotCounter,iFinalState,tempFinalData[i].rowData.iRank,
																	tempFinalData[i].rowData.tl23_ParticipantName,tempFinalData[i].crewTag)//tempCrewTag[0])
											scLB_control.navigation.slotData[i].iSlot = iSlotCounter
											println("scLB_control.navigation.slotData[",i,"].iSlot = ",scLB_control.navigation.slotData[i].iSlot)
											scLB_control.navigation.slotData[i].iFinalState = iFinalState
											scLB_control.navigation.slotData[i].playerGamerHandle = tempFinalData[i].rowData.playerGamerHandle
											scLB_control.navigation.iMaxVertSel++
										ENDIF
										
										REPEAT scLB_DisplayedData.iNumNonRankColumnsToDisplay iDisplayLoop
											//IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,scLB_Control.ReadDataStruct.m_Type,scLB_DisplayedData.iReadColumns[iDisplayLoop])
											bInValidDataForThisColumn = FALSE
											IF IS_BIT_SET(scLB_DisplayedData.iDisplayColumnIsIntBS,iDisplayLoop)
												IF IS_BIT_SET(scLB_DisplayedData.iInvalidDataSetBS,iDisplayLoop)
													IF scLB_DisplayedData.iInvalidDataInt[iDisplayLoop] = tempFinalData[i].rowData.icolumnData[iDisplayLoop]
														bInValidDataForThisColumn = TRUE
													ENDIF
//													println("CDM: Latest scLB_DisplayedData.iInvalidDataInt[iDisplayLoop] = ",scLB_DisplayedData.iInvalidDataInt[iDisplayLoop])
//													println("CDM: Latest tempFinalData[i].rowData.icolumnData[iDisplayLoop] = ",tempFinalData[i].rowData.icolumnData[iDisplayLoop])
												ENDIF
												IF bInValidDataForThisColumn
													ADD_SC_LEADERBOARD_COLUMN_DATA_INT(scLB_DisplayedData.displaySetup,iDisplayLoop,tempFinalData[i].rowData.icolumnData[iDisplayLoop],FALSE,tempFinalData[i].rowData.bCustomVehicle)
												ELSE
													ADD_SC_LEADERBOARD_COLUMN_DATA_INT(scLB_DisplayedData.displaySetup,iDisplayLoop,tempFinalData[i].rowData.icolumnData[iDisplayLoop],tempFinalData[i].rowData.bValidData,tempFinalData[i].rowData.bCustomVehicle)
												ENDIF
												
												println("ADD_SC_LEADERBOARD_COLUMN_DATA_INT: ", tempFinalData[i].rowData.icolumnData[iDisplayLoop])
											ELSE
												IF IS_BIT_SET(scLB_DisplayedData.iInvalidDataSetBS,iDisplayLoop)
													IF scLB_DisplayedData.iInvalidDataFloat[iDisplayLoop] = tempFinalData[i].rowData.icolumnData[iDisplayLoop]
														bInValidDataForThisColumn = TRUE
													ENDIF
												ENDIF
												IF bInValidDataForThisColumn
													ADD_SC_LEADERBOARD_COLUMN_DATA_FLOAT(scLB_DisplayedData.displaySetup,iDisplayLoop,tempFinalData[i].rowData.fcolumnData[iDisplayLoop],FALSE)
												ELSE
													ADD_SC_LEADERBOARD_COLUMN_DATA_FLOAT(scLB_DisplayedData.displaySetup,iDisplayLoop,tempFinalData[i].rowData.fcolumnData[iDisplayLoop],tempFinalData[i].rowData.bValidData)
												ENDIF
												
												println("ADD_SC_LEADERBOARD_COLUMN_DATA_FLOAT: ", tempFinalData[i].rowData.fcolumnData[iDisplayLoop])
											ENDIF
										ENDREPEAT
										STOP_SC_LEADERBOARD_SLOT()
										iSectionCounter[tempFinalData[i].iSection-1]++
										IF iSectionCounter[tempFinalData[i].iSection-1] = 2
											IF tempFinalData[i].rowData.iRank > 2
												SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_OUTLINE)
												SET_BIT(scLB_control.navigation.slotData[0].iFinalState,SCLB_SLOT_STATE_BIT_OUTLINE)
												SET_SC_LEADERBOARD_SLOT_FINAL_STATE(scaleFormID,iSlotCounter-1,iFinalState)
											ENDIF
										ENDIF
										iSlotCounter++
									ENDIF
								ENDIF
							ENDREPEAT
							i =0
							REPEAT 3 i
								scLB_control.navigation.iSectionCounter[i] = iSectionCounter[i]
							ENDREPEAT
						ELSE

							iFinalState = 0 
							SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_WORLD)
							SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE)
							//iSlotCounter++
							iFinalState = 0 
							SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_FRIENDS)
							SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE)
							//iSlotCounter++
							iFinalState = 0 
							SET_BIT(iFinalState,SCLB_SLOT_STATE_BIT_CREW)
							SET_SC_LEADERBOARD_SECTION_HEADER(scaleFormID,iSlotCounter,iFinalState,TRUE)
							//iSlotCounter++
							
							//println("DRAW_SC_SCALEFORM_LEADERBOARD: empty leaderboard")
						ENDIF
						SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						#IF IS_DEBUG_BUILD
						bDisplayedUIThisFrame = TRUE
						#ENDIF
						DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
						CLEAR_HELP()
						//println("Setup social club leaderboard")
						IF scLB_control.navigation.iCurrentVertSel = -1
						AND NOT bHighlightedARow = TRUE
							IF scLB_DisplayedData.iSectionEntries[0] > 1
								scLB_control.navigation.iCurrentVertSel = 0
								SET_BIT(scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iFinalState,SCLB_SLOT_STATE_BIT_SELECTED)
								SET_SC_LEADERBOARD_SLOT_FINAL_STATE(scaleFormID,scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iSlot,scLB_control.navigation.slotData[scLB_control.navigation.iCurrentVertSel].iFinalState)
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						bDisplayedUIThisFrame = TRUE
						#ENDIF
						DISPLAY_SC_LEADERBOARD_UI(scaleFormID)	
						HANDLE_SELECTION_OF_PLAYERS_LISTED(scaleformID, scLB_control)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
	
	
	#IF IS_DEBUG_BUILD
	IF bDisplayedUIThisFrame
		//println("DRAW_SC_SCALEFORM_LEADERBOARD: called this frame but bDisplayedUIThisFrame = FALSE")
		//IF SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(scLB_control)
		//	println("SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD = TRUE")
		//ENDIF
	ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Draws the social club leaderboards and disables control actions 
/// PARAMS:
///    scLB_control - The control struct for the leaderboard
///    scaleformIndex - The scaleform index for the leaderboard movie
PROC DRAW_SOCIAL_CLUB_LEADERBOARD(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control, SCALEFORM_INDEX& scaleformIndex[])
	IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
	AND NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	ENDIF			
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)			
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE) // B*2223500
	ENDIF
	MPGlobalsAmbience.bPlayerViewingLeaderboard = TRUE		
	g_bDisableVoteThisFrame = TRUE
	
	DRAW_SC_SCALEFORM_LEADERBOARD(scaleformIndex[0],scLB_control)	
ENDPROC

FUNC BOOL SAFE_TO_SC_LEADERBOARD()
	IF IS_CUSTOM_MENU_SAFE_TO_DRAW()
		RETURN TRUE
	ELSE
		IF IS_PLAYER_ACTIVE_IN_CORONA()
			IF NOT IS_SCREEN_FADED_IN()
			//OR (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL)
			OR (IS_PAUSE_MENU_ACTIVE()) 
			//OR (IS_PLAYER_SWITCH_IN_PROGRESS())  //MP Player Switch is happening.
			OR (IS_COMMERCE_STORE_OPEN())
			OR (g_bResultScreenDisplaying)
			OR (g_sMenuData.bDisableMenu)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA(INT& iButtonBS,BOOL& bDrawingMenu,BOOL& bMenuActive,BOOL& bDrawGlobalBoard,BOOL& bDrawCoDriverBoard, INT& iState, INT &iRangeWeap, SC_LEADERBOARD_CONTROL_STRUCT& scLB_control, SCALEFORM_INDEX& scaleformIndex[],SCALEFORM_INSTRUCTIONAL_BUTTONS& scaleformStruct, BOOL bInCorona = FALSE)
//	IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SOCIAL_CLUB_TOUR)
//		IF NOT IS_SOCIAL_CLUB_ACTIVE()
//		//AND NOT IS_PAUSE_MENU_ACTIVE()
//			println("NOT IS_SOCIAL_CLUB_ACTIVE() clearing flag") 
//			CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SOCIAL_CLUB_TOUR)
//		ENDIF
//	ELSE
		IF bDrawingMenu
			IF SAFE_TO_SC_LEADERBOARD()
				
				DRAW_SOCIAL_CLUB_LEADERBOARD(scLB_control,scaleformIndex)
				
				IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
				OR IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					//Deal with the buttons
					IF bInCorona = FALSE
						SPRITE_PLACEMENT ScaleformSprite
						ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
						IF NOT IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
							REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(scaleformStruct)
							SET_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						
							
//							IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
//								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT),"IB_SOCIAL_CLUB",scaleformStruct)
//							ELSE
//								IF NOT NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()
//									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT),"SCLB_SIGNIN",scaleformStruct)
//								ENDIF
//							ENDIF
							IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
							AND scLB_DisplayedData.iSectionEntries[0] > 0
							AND scLB_control.navigation.iCurrentVertSel >= 0
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT,"SCLB_PROFILE",scaleformStruct, TRUE)
							ENDIF
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL,"BB_BACK",scaleformStruct, TRUE)
						
							IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
							AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
								IF bDrawGlobalBoard
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB2",scaleformStruct)
								ELSE
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB1",scaleformStruct)
								ENDIF
							ENDIF
							IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
								IF bDrawCoDriverBoard
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"FMMC_COR_SCLB5",scaleformStruct)
								ELSE
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"FMMC_COR_SCLB6",scaleformStruct)
								ENDIF
							ENDIF
							IF scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE)
								SWITCH iState
									CASE 0 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB7a",scaleformStruct)
									BREAK
									CASE 1 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB7b",scaleformStruct)
									BREAK
									CASE 2 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB7c",scaleformStruct)
									BREAK
								ENDSWITCH
								SWITCH iRangeWeap
									CASE 0 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_SMG",scaleformStruct) //off by one as this is for NEXT selection not current
									BREAK
							        CASE 1 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_ASSAULT",scaleformStruct) //off by one as this is for NEXT selection not current
									BREAK
							        CASE 2 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_SHOTGUN",scaleformStruct) //off by one as this is for NEXT selection not current
									BREAK
							        CASE 3 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_LMG",scaleformStruct) //off by one as this is for NEXT selection not current
									BREAK
							        CASE 4 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_HEAVY",scaleformStruct) //off by one as this is for NEXT selection not current
									BREAK
							        CASE 5 
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_PISTOL",scaleformStruct) //off by one as this is for NEXT selection not current
									BREAK
								ENDSWiTCH
							ENDIF
							
//							IF IS_BIT_SET(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
//							AND scLB_DisplayedData.iSectionEntries[0] > 0
//								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(ICON_DPAD_UPDOWN,"SCLB_NAVIGATE",scaleformStruct)
//							ENDIF
							
							
						ENDIF
						
						
						//println("Setup the button options.")
						
						RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(scaleformIndex[1], ScaleformSprite, scaleformStruct)
						//ELSE
						//	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(scaleformIndex[1], ScaleformSprite, scaleformStruct)
						//ENDIF
					ENDIF
				ELSE
					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: waiting for menu to be setup before drawing buttons.")
				ENDIF
				
//			#IF IS_DEBUG_BUILD
//			ELSE
//				println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: can't draw IS_CUSTOM_MENU_SAFE_TO_DRAW() = FALSE!!!")
//				IF NOT IS_SCREEN_FADED_IN()
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because screen is not faded in")
//				ENDIF
//				IF (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL)
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL)")
//				ENDIF 
//				IF (IS_PAUSE_MENU_ACTIVE()) 
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because IS_PAUSE_MENU_ACTIVE())")
//				ENDIF
//				IF (IS_COMMERCE_STORE_OPEN())
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because IS_COMMERCE_STORE_OPEN())")
//				ENDIF
//				IF (g_bResultScreenDisplaying)
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because g_bResultScreenDisplaying")
//				ENDIF
//				IF (IS_PLAYER_SWITCH_IN_PROGRESS())  //MP Player Switch is happening.
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because IS_PLAYER_SWITCH_IN_PROGRESS()")
//				ENDIF
//				IF (g_sMenuData.bDisableMenu)
//					println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: because g_sMenuData.bDisableMenu)")
//				ENDIF
//			#ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iButtonBS,FM_PRESSED_BS_CIRCLE)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)	
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//					#IF IS_DEBUG_BUILD
//						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)	
//							println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)")
//						ENDIF
//						IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//							println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA:IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)")
//						ENDIF
//					#ENDIF
					PLAY_SOUND_FRONTEND(-1, "BACK","HUD_FRONTEND_MP_SOUNDSET")
					SET_BIT(iButtonBS,FM_PRESSED_BS_CIRCLE)
					println("CLEANUP_CORONA_LEADERBOARD: player pressed cancel when on leaderboard") 
					CLEANUP_CORONA_LEADERBOARD(bDrawingMenu,bMenuActive,scLB_Control,scaleformIndex,scaleformStruct)
					exit
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					CLEAR_BIT(iButtonBS,FM_PRESSED_BS_CIRCLE)
				ENDIF
			ENDIF
			
			IF IS_SC_LEADERBOARD_A_RACE(scLB_control.ReadDataStruct.m_LeaderboardId)
			AND NOT IS_THIS_RACE_A_POINT_2_POINT_RACE()
				IF NOT IS_BIT_SET(iButtonBS,FM_PRESSED_BS_X)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)	
					OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
						PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_MP_SOUNDSET")
						SET_BIT(iButtonBS,FM_PRESSED_BS_X)
						IF bDrawGlobalBoard
							bDrawGlobalBoard = FALSE
						ELSE
							bDrawGlobalBoard = TRUE
						ENDIF
						println("global board = ",bDrawGlobalBoard)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
						CLEAR_BIT(iButtonBS,FM_PRESSED_BS_X)
					ENDIF
				ENDIF
			ENDIF
			
			IF scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE)
				IF NOT IS_BIT_SET(iButtonBS,FM_PRESSED_BS_X)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)	
					OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
						PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_MP_SOUNDSET")
						SET_BIT(iButtonBS,FM_PRESSED_BS_X)
						iState++
						IF iState > 2
							iState = 0
						ENDIF
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: shooting range moving to state: ", iState) 
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
						CLEAR_BIT(iButtonBS,FM_PRESSED_BS_X)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iButtonBS,FM_PRESSED_BS_Y)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)	
					OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
						PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_MP_SOUNDSET")
						SET_BIT(iButtonBS,FM_PRESSED_BS_Y)
						iRangeWeap++
						IF iRangeWeap > 5
							iRangeWeap = 0
						ENDIF
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						println("HANDLE_SOCIAL_CLUB_LEADERBOARD_CORONA: shooting range moving to weapon: ", iRangeWeap) 
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
					AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
						CLEAR_BIT(iButtonBS,FM_PRESSED_BS_Y)
					ENDIF
				ENDIF
			ENDIF
			
			//BOOL bTemp 
			//IF bTemp = TRUE
			IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
				IF NOT IS_BIT_SET(iButtonBS,FM_PRESSED_BS_Y)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)	
					OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
						PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_MP_SOUNDSET")
						SET_BIT(iButtonBS,FM_PRESSED_BS_Y)
						IF bDrawCoDriverBoard
							bDrawCoDriverBoard = FALSE
						ELSE
							bDrawCoDriverBoard = TRUE
						ENDIF
						println("bDrawCoDriverBoard = ",bDrawCoDriverBoard ) 
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
						CLEAR_BIT(scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
					AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
						CLEAR_BIT(iButtonBS,FM_PRESSED_BS_y)
					ENDIF
				ENDIF
			//ENDIF
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

FUNC STRING CONVERT_TEXT_LABEL_TO_LITERAL_STRING(STRING theString)
	RETURN theString
ENDFUNC

STRUCT BEST_LAPS_DISPLAY_NAME_STRUCT
	TEXT_LABEL_63 tl63_names[2]
	GAMER_HANDLE gh_handles[2]
	
	INT iQueryProgress[2]
	
	//for the request on durango
	TEXT_LABEL_63 tl63_namesTemp[2]
	GAMER_HANDLE gh_handlesTemp[2]
	INT iNumHandles
	INT iRequestID
ENDSTRUCT

FUNC BOOL GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES(BEST_LAPS_DISPLAY_NAME_STRUCT &theStruct)
	INT i
	INT iResult
	INT iCounter
	IF IS_PC_VERSION()
		PRINTLN("GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES: bypassing for PC build.")
		RETURN TRUE
	ELIF IS_XBOX_PLATFORM()
	OR IS_PLAYSTATION_PLATFORM()
		SWITCH theStruct.iQueryProgress[0]
			CASE 0
				theStruct.iNumHandles = 0
				theStruct.iRequestID = -1
				REPEAT 2 i
					theStruct.tl63_namesTemp[i] = ""
					CLEAR_GAMER_HANDLE_STRUCT(theStruct.gh_handlesTemp[i])
					IF IS_GAMER_HANDLE_VALID(theStruct.gh_handles[i])
						theStruct.gh_handlesTemp[theStruct.iNumHandles] = theStruct.gh_handles[i]
						theStruct.iNumHandles++
					ENDIF
				ENDREPEAT
				IF theStruct.iNumHandles > 0
					theStruct.iRequestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(theStruct.gh_handlesTemp,theStruct.iNumHandles)	
					theStruct.iQueryProgress[0] = 1
				ELSE
					theStruct.iQueryProgress[0] = 2
				ENDIF
			BREAK

			CASE 1
				iResult = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(theStruct.iRequestID,theStruct.tl63_namesTemp,theStruct.iNumHandles)
				IF iResult = 0
					PRINTLN("GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES- SUCCEEDED!")
					REPEAT 2 i
						IF IS_GAMER_HANDLE_VALID(theStruct.gh_handles[i])
							theStruct.tl63_names[i] = theStruct.tl63_namesTemp[iCounter]
							PRINTLN("GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES PlayerName = ",theStruct.tl63_names[i] )
							iCounter++
						ENDIF
					ENDREPEAT
					theStruct.iQueryProgress[0] = 2
				ELIF iResult = -1
					PRINTLN("GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES- FAILED!")
					theStruct.iQueryProgress[0] = 2
				ELSE
					 PRINTLN("GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES: waiting for display name request: iResult = ",iResult)
					 RETURN FALSE
				ENDIF
			BREAK

			CASE 2
				theStruct.iRequestID = -1
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		REPEAT 2 i
			IF NOT GET_GAMER_NAME_FROM_HANDLE(theStruct.iQueryProgress[i],theStruct.gh_handles[i],theStruct.tl63_names[i])
				RETURN FALSE
			ENDIF
		ENDREPEAT
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the personal/global/crew best lap times  
FUNC BOOL GET_RACE_BEST_LAP_TIMES(INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful, INT iRaceType,INT& iGlobalBest, INT& iPersonalBest,INT& iComparisonBest,RACE_CHECKPOINTS_LOAD_STRUCT &raceStoredDataStruct,TEXT_LABEL_31& worldRecordHolderName, TEXT_LABEL_31& compareRecordHolderName,BEST_LAPS_DISPLAY_NAME_STRUCT &displayNamesQuery)
	IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl31LoadedContentID)	
		println("GET_RACE_BEST_LAP_TIMES: exited as current file name is empty") 
		RETURN TRUE
	ENDIF
	Leaderboard2ReadData lbReadData
	Leaderboard2ReadData emptyData
	//LeaderboardRowData tempRowData[1]
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	
	INT iLoopRowMax = 50
	INT iNumOfFriends
	INT iRowIndex
	
	GAMER_HANDLE gamerHandle
	gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	
	NETWORK_CLAN_DESC clanDesc
	
	Leaderboard2GroupHandle groupHandleData[1]
	INT iIndex = 0
	
	TEXT_LABEL_31 createdGroupText		
	createdGroupText = g_FMMC_STRUCT.tl31LoadedContentID
	
	SWITCH iRaceType
		CASE ciRACE_SUB_TYPE_STANDARD
		CASE ciRACE_SUB_TYPE_GTA
		CASE ciRACE_SUB_TYPE_NON_CONTACT
			IF GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_STANDARD
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES)
			ENDIF
			IF GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_GTA
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES)
			ENDIF
			IF GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_NON_CONTACT
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES)
			ENDIF
			lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "Mission"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = createdGroupText
		BREAK
		CASE ciRACE_SUB_TYPE_RALLY
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY)
			lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
			lbReadData.m_GroupSelector.m_NumGroups = 2
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "Mission"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = createdGroupText
			lbReadData.m_GroupSelector.m_Group[1].m_Category = "CoDriver"
			lbReadData.m_GroupSelector.m_Group[1].m_Id = ""
		BREAK
	ENDSWITCH
	
//	IF iRaceType = ciRACE_SUB_TYPE_STANDARD
//		lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES)
//	ELIF iRaceType = ciRACE_SUB_TYPE_GTA
//		lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES)
//	ELIF iRaceType = ciRACE_SUB_TYPE_RALLY
//		lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY)
//	ENDIF
//	lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
//	lbReadData.m_GroupSelector.m_NumGroups = 1
//	lbReadData.m_GroupSelector.m_Group[0].m_Category = "Mission"
//	lbReadData.m_GroupSelector.m_Group[0].m_Id = createdGroupText
//	
	SWITCH iReadStage
		CASE 0
			displayNamesQuery.iQueryProgress[0] = 0
			displayNamesQuery.iQueryProgress[1] = 0
			IF START_SC_LEADERBOARD_READ_BY_RANK(iLoadStage,bSuccessful,lbReadData,1,1)
				#IF IS_DEBUG_BUILD
				println("GET_RACE_BEST_LAP_TIMES: Global best  LEADERBOARD ID = ", lbReadData.m_LeaderboardId, " groupHandle = ",lbReadData.m_GroupSelector.m_Group[0].m_Id) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
				IF bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						
						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							println("GET_RACE_BEST_LAP_TIMES: Global best  -- moving to stage 99 no global data - A") 
							iGlobalBest = RACE_LEADERBOARD_NO_DATA_RESULT
							iReadStage = 99
						ELSE
							iGlobalBest = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0)
							println("GET_RACE_BEST_LAP_TIMES: Global best Before= ",iGlobalBest)
							IF iGlobalBest < 0
								iGlobalBest = -iGlobalBest
							ENDIF
							displayNamesQuery.gh_handles[0] = initRowData.m_GamerHandle
							println("GET_RACE_BEST_LAP_TIMES: Global best AFTER= ",iGlobalBest)
							//worldRecordHolderName = CONVERT_TEXT_LABEL_TO_LITERAL_STRING(initRowData.m_GamerName)
							worldRecordHolderName = initRowData.m_GamerName
							//stoopid compiler!
							IF IS_STRING_NULL(worldRecordHolderName)
								///
							ENDIF
							println("GET_RACE_BEST_LAP_TIMES: Global best --moving to stage 1  world record holder name = ", worldRecordHolderName)
							iReadStage = 1
						ENDIF
						//println("Ending read in READ_BY_RANK in  ", GET_THIS_SCRIPT_NAME() ) 
					ELSE
						println("GET_RACE_BEST_LAP_TIMES: Global best  -- moving to stage 99 no global data - B") 
						iGlobalBest = -1
						iReadStage = 99
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					iGlobalBest = -1
					println("GET_RACE_BEST_LAP_TIMES: Global best  -- moving to stage 99 no global data - C") 
					iReadStage = 99
				ENDIF
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				lbReadData = emptyData
			ENDIF
		BREAK
		CASE 1
			IF START_SC_LEADERBOARD_READ_BY_HANDLE(iLoadStage,bSuccessful,lbReadData,gamerHandle)
				#IF IS_DEBUG_BUILD
				println("GET_RACE_BEST_LAP_TIMES: Personal best LEADERBOARD ID = ", lbReadData.m_LeaderboardId," groupHandle = ",lbReadData.m_GroupSelector.m_Group[0].m_Id) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
				IF bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						
						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							println("GET_RACE_BEST_LAP_TIMES: Personal best -- moving to stage 2 no data for local player -A") 
							iPersonalBest = RACE_LEADERBOARD_NO_DATA_RESULT
							iReadStage = 2
						ELSE
							iPersonalBest = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0)
							displayNamesQuery.gh_handles[1] = initRowData.m_GamerHandle
							println("GET_RACE_BEST_LAP_TIMES: Personal best- Before= ",iPersonalBest)
							IF iPersonalBest < 0
								iPersonalBest = -iPersonalBest
							ENDIF
							println("GET_RACE_BEST_LAP_TIMES: Personal best-AFTERe= ",iPersonalBest)
							println("GET_RACE_BEST_LAP_TIMES: Personal best-- moving to stage 2 got data for local player -B") 
							iReadStage = 2
						ENDIF
					ELSE
						iPersonalBest = -1
						println("GET_RACE_BEST_LAP_TIMES: Personal best -- moving to stage 2 no data for local player -C") 
						iReadStage = 2
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					iPersonalBest = -1
					println("GET_RACE_BEST_LAP_TIMES: Personal best -- moving to stage 2 no data for local player -D") 
					iReadStage = 2
					
				ENDIF
				//println("Ending read in READ_BY_HANDLE in  ", GET_THIS_SCRIPT_NAME() ) 
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				lbReadData = emptyData
			ENDIF
		BREAK
		CASE 2
			println("GET_RACE_BEST_LAP_TIMES: Checkpoints type: ",raceStoredDataStruct.iCheckPointType) 
			SWITCH raceStoredDataStruct.iCheckPointType
				CASE RACE_SPLIT_COMPARISON_CREW
					iReadStage = 3
					println("GET_RACE_BEST_LAP_TIMES: moving to stage 3 GET crew time") 
				BREAK
				CASE RACE_SPLIT_COMPARISON_WORLD
					compareRecordHolderName = worldRecordHolderName
					iComparisonBest = iGlobalBest
					iReadStage = 5
					println("GET_RACE_BEST_LAP_TIMES: moving to stage 5 already got global best") 
				BREAK
				CASE RACE_SPLIT_COMPARISON_PERSONAL
					compareRecordHolderName = GET_PLAYER_NAME(PLAYER_ID())
					iComparisonBest = iPersonalBest
					iReadStage = 5
					println("GET_RACE_BEST_LAP_TIMES: moving to stage 5 already got personal") 
				BREAK
				CASE RACE_SPLIT_COMPARISON_FRIEND
					iReadStage = 4
					println("GET_RACE_BEST_LAP_TIMES: moving to stage 4 GET friend time ") 
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF NETWORK_CLAN_SERVICE_IS_VALID()
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
					IF raceStoredDataStruct.m_ClanId  = 0
						IF NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), gamerHandle)
							//println("GET_RACE_CHECKPOINT_DATA got players active crew details. moving to stage 1 ") 
							raceStoredDataStruct.m_ClanId = clanDesc.Id
						ENDIF
						println("GET_RACE_BEST_LAP_TIMES: Crew best waiting raceStoredDataStruct.m_ClanId = 0")
						RETURN FALSE
					ENDIF
				ELSE
					println("GET_RACE_BEST_LAP_TIMES: Crew best exiting NETWORK_CLAN_PLAYER_IS_ACTIVE is false") 
					RETURN TRUE
				ENDIF
			ELSE
				println("GET_RACE_BEST_LAP_TIMES: Crew best exiting N NETWORK_CLAN_SERVICE_IS_VALID is false") 
				RETURN TRUE
			ENDIF
			lbReadData.m_ClanId = raceStoredDataStruct.m_ClanId
			lbReadData.m_Type = LEADERBOARD2_TYPE_CLAN_MEMBER
			IF START_SC_LEADERBOARD_READ_BY_RANK(iLoadStage,bSuccessful,lbReadData,1,1)
				#IF IS_DEBUG_BUILD
				println("GET_RACE_BEST_LAP_TIMES: Crew best  LEADERBOARD ID = ", lbReadData.m_LeaderboardId," groupHandle = ",lbReadData.m_GroupSelector.m_Group[0].m_Id) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
				IF bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						
						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							println("GET_RACE_BEST_LAP_TIMES: Crew best -- moving to stage 5 no data  -A") 
							iComparisonBest = RACE_LEADERBOARD_NO_DATA_RESULT
							iReadStage = 5
						ELSE
							iComparisonBest = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0)
							displayNamesQuery.gh_handles[1] = initRowData.m_GamerHandle
							println("GET_RACE_BEST_LAP_TIMES: Crew best Before= ",iComparisonBest)
							IF iComparisonBest < 0 
								iComparisonBest = -iComparisonBest
							ENDIF
							println("GET_RACE_BEST_LAP_TIMES: Crew best AFTER= ",iComparisonBest)
							println("GET_RACE_BEST_LAP_TIMES: Crew best-- moving to stage 5 got data  -B") 
							iReadStage = 5
						ENDIF
					ELSE
						iComparisonBest = -1
						println("GET_RACE_BEST_LAP_TIMES: Crew best -- moving to stage 5 no data  -C") 
						iReadStage = 5
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					iComparisonBest = -1
					println("GET_RACE_BEST_LAP_TIMES: Crew best -- moving to stage 5 no data -D") 
					iReadStage = 5
					
				ENDIF
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				lbReadData = emptyData
			ENDIF
		BREAK
		CASE 4
			//getting information about local player
			iNumOfFriends = NETWORK_GET_FRIEND_COUNT()
			IF iNumOfFriends  > 0
				iRowIndex = raceStoredDataStruct.iFriendLoopCounter*iLoopRowMax
				IF START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW(iLoadStage,bSuccessful,
															lbReadData,groupHandleData,
															groupHandleData[0].m_NumGroups,TRUE,iRowIndex,iLoopRowMax)
					#IF IS_DEBUG_BUILD
					DEBUG_START_LEADERBOARD_PRINT_RESULTS("GET_RACE_BEST_LAP_TIMES: Friend best Getinng Friend Data",lbReadData)
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
					IF bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							iIndex = 0
							//repeat through all row before player
							println("readInfo.m_ReturnedRows:  ", readInfo.m_ReturnedRows)
							REPEAT readInfo.m_ReturnedRows iIndex
								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
								//IF readInfo.m_LocalGamerRowNumber != iIndex
									IF initRowData.m_Rank > 0
										IF raceStoredDataStruct.iBestLapTime = 0
										OR raceStoredDataStruct.iBestLapTime < LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0)
											compareRecordHolderName = initRowData.m_GamerName
											displayNamesQuery.gh_handles[1] = initRowData.m_GamerHandle
											raceStoredDataStruct.iBestLapTime = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0)
											iComparisonBest = 	LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,0)
											IF iComparisonBest< 0
												iComparisonBest = -(iComparisonBest)
											ENDIF
											println("Setting current best lap for Checkspoint to ", raceStoredDataStruct.iBestLapTime)
										ENDIF
									ENDIF
								//ELSE
								//	println("This is the local player's row ignoring")
								//ENDIF
							ENDREPEAT
							
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
							raceStoredDataStruct.iFriendLoopCounter++
							IF iNumOfFriends > raceStoredDataStruct.iFriendLoopCounter*iLoopRowMax
								iReadStage = 4
								println("GET_RACE_BEST_LAP_TIMES: Friend best friend data moving to ",raceStoredDataStruct.iFriendLoopCounter," loop") 
							ELSE
								println("GET_RACE_BEST_LAP_TIMES: Friend best friend data  finished looping returning true")
								iReadStage = 5
							ENDIF
						ELSE
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
							raceStoredDataStruct.iFriendLoopCounter++
							IF iNumOfFriends > raceStoredDataStruct.iFriendLoopCounter*iLoopRowMax
								iReadStage = 4
								println("GET_RACE_BEST_LAP_TIMES: Friend best friend data no friend in first group of friends moving to ",raceStoredDataStruct.iFriendLoopCounter ," loop") 
							ELSE
								iReadStage = 5
							ENDIF
						ENDIF
					ELSE
						println("GET_RACE_BEST_LAP_TIMES: Friend best friend data no friends data skiping to end stage 1 (no row data) ") 
						END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
						iReadStage = 5
					ENDIF
				ENDIF
			ELSE
				println("GET_RACE_BEST_LAP_TIMES: Friend best friend data  no friends data skiping to end stage 1 (friend count is 0)") 
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				iReadStage = 5
			ENDIF		
		BREAK
		CASE 5
			IF GET_DISPLAY_NAMES_FOR_BEST_LAP_TIMES(displayNamesQuery)
				IF NOT IS_STRING_NULL_OR_EMPTY(displayNamesQuery.tl63_names[0])
					worldRecordHolderName = displayNamesQuery.tl63_names[0]
					println("GET_RACE_BEST_LAP_TIMES: world record = ",worldRecordHolderName)
					IF raceStoredDataStruct.iCheckPointType = RACE_SPLIT_COMPARISON_WORLD
						compareRecordHolderName = displayNamesQuery.tl63_names[0]
						println("GET_RACE_BEST_LAP_TIMES: compare record = ",compareRecordHolderName)
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(displayNamesQuery.tl63_names[1])
							compareRecordHolderName = displayNamesQuery.tl63_names[1]
							println("GET_RACE_BEST_LAP_TIMES: compare record = ",compareRecordHolderName)
						ENDIF
					ENDIF
				ENDIF
				iReadStage = 99
			ENDIF
		BREAK

		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CLEANUP_RACE_BEST_LAP_TIMES(INT& iReadStage, INT& iLoadStage, BOOL& bResult, INT& iGlobalBest, INT& iPersonalBest,INT& iComparisonBest,Leaderboard2ReadData lbReadData)
	iReadStage = 0
	iGlobalBest = 0 
	iPersonalBest = 0
	iComparisonBest = 0
	IF iLoadStage != 0
		println("Ending read in CLEANUP_RACE_BEST_LAP_TIMES in  ", GET_THIS_SCRIPT_NAME() ) 
		END_LEADERBOARD_READ(iLoadStage,bResult, lbReadData)
	ENDIF
	iLoadStage = 0
ENDPROC

//FUNC BOOL GET_RACE_CHECKPOINT_DATA(INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful, INT iRaceType,INT iCheckpoints,INT &iCheckpointTimes[],RACE_CHECKPOINTS_LOAD_STRUCT &raceStoredDataStruct, INT &iComparisonTimeTotal, TEXT_LABEL_31& checkpointRecordHolderName)
//	
//	//Remove this and put in actual time
//	UNUSED_PARAMETER(iComparisonTimeTotal)
//	
//	//println("GET_RACE_CHECKPOINT_DATA - Called this frame")
//	//println("GET_RACE_CHECKPOINT_DATA -iRaceType = ",iRaceType)
//	//println("GET_RACE_CHECKPOINT_DATA -iCheckpoints = ",iCheckpoints)
//	//println("GET_RACE_CHECKPOINT_DATA -iComparisonTimeTotal = ",iComparisonTimeTotal)
//	Leaderboard2ReadData lbReadData
//	Leaderboard2ReadData emptyData
//	LeaderboardRowData initRowData
//	LeaderboardReadInfo readInfo
//	INT i
//	
//	INT iLoopRowMax = 50
//	INT iNumOfFriends
//	INT iRowIndex
//	
//	GAMER_HANDLE gamerHandle
//	NETWORK_CLAN_DESC clanDesc
//	
//	Leaderboard2GroupHandle groupHandleData[1] 
//	
//	INT iIndex = 0
//	
//	TEXT_LABEL_31 createdGroupText		
//	createdGroupText = g_FMMC_STRUCT.tl31LoadedContentID
//	IF iCheckpoints = 0
//	ENDIF
//	SWITCH iRaceType
//		CASE ciRACE_SUB_TYPE_STANDARD
//		CASE ciRACE_SUB_TYPE_GTA
//		CASE ciRACE_SUB_TYPE_RALLY
//			IF GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_STANDARD
//				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RACE_CHECKPOINTS)
//			ENDIF
//			IF GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_GTA
//				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACE_CHECKPOINTS)
//			ENDIF
//			IF GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_RALLY
//				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY_CHECKPOINTS)
//			ENDIF
//			lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
//			lbReadData.m_GroupSelector.m_NumGroups = 1
//			lbReadData.m_GroupSelector.m_Group[0].m_Category = "Mission"
//			lbReadData.m_GroupSelector.m_Group[0].m_Id = createdGroupText
//		BREAK
//		DEFAULT 
//			IF iReadStage > 0
//				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//			ENDIF
//			RETURN FALSE
//		BREAK
//	ENDSWITCH
//	
//	IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl31LoadedContentID)	
//		//println("GET_RACE_CHECKPOINT_DATA: exited as current file name is empty") 
//		IF iReadStage > 0
//			END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//		ENDIF
//		RETURN TRUE
//	ENDIF
//	
//	gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
//	IF raceStoredDataStruct.iCheckPointType =  RACE_SPLIT_COMPARISON_CREW
//		lbReadData.m_Type = LEADERBOARD2_TYPE_CLAN_MEMBER
//		lbReadData.m_ClanId = raceStoredDataStruct.m_ClanId 
//	ENDIF
//	IF IS_STRING_NULL(checkpointRecordHolderName)
//		//compiler doesn't like it without this
//	ENDIF
//	IF NOT raceStoredDataStruct.bInitComplete
//		IF raceStoredDataStruct.iCheckPointType =  RACE_SPLIT_COMPARISON_CREW
//		
//			IF NETWORK_CLAN_SERVICE_IS_VALID()
//				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
//					IF raceStoredDataStruct.m_ClanId  = 0
//						IF NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), gamerHandle)
//							//println("GET_RACE_CHECKPOINT_DATA got players active crew details. moving to stage 1 ") 
//							lbReadData.m_ClanId = clanDesc.Id
//							raceStoredDataStruct.m_ClanId = clanDesc.Id
//						ENDIF
//						println("GET_RACE_CHECKPOINT_DATA waiting raceStoredDataStruct.m_ClanId = 0")
//						RETURN FALSE
//					ENDIF
//				ELSE
//					println("GET_RACE_CHECKPOINT_DATA exiting NETWORK_CLAN_PLAYER_IS_ACTIVE is false") 
//					RETURN TRUE
//				ENDIF
//			ELSE
//				println("GET_RACE_CHECKPOINT_DATA exiting NETWORK_CLAN_SERVICE_IS_VALID is false") 
//				RETURN TRUE
//			ENDIF
//		ENDIF
//		SWITCH raceStoredDataStruct.iCheckPointType
//			CASE RACE_SPLIT_COMPARISON_CREW
//			CASE RACE_SPLIT_COMPARISON_WORLD
//				iReadStage = 1 
//				println("GET_RACE_CHECKPOINT_DATA: setting read stage = 1  Checkpoints type: ",raceStoredDataStruct.iCheckPointType) 
//			BREAK
//			CASE RACE_SPLIT_COMPARISON_PERSONAL
//				iReadStage = 2
//				println("GET_RACE_CHECKPOINT_DATA: setting read stage = 4  Checkpoints type: ",raceStoredDataStruct.iCheckPointType) 
//			BREAK
//			CASE RACE_SPLIT_COMPARISON_FRIEND
//				iReadStage = 3
//				println("GET_RACE_CHECKPOINT_DATA: setting read stage = 2  Checkpoints type: ",raceStoredDataStruct.iCheckPointType) 
//			BREAK
//		ENDSWITCH
//		raceStoredDataStruct.bInitComplete = TRUE
//	ENDIF
//	SWITCH iReadStage
//		CASE 1
//			IF START_SC_LEADERBOARD_READ_BY_RANK(iLoadStage,bSuccessful,lbReadData,1,1)
//				#IF IS_DEBUG_BUILD
//				println("GET_RACE_CHECKPOINT_DATA:  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
//				println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD Category 1 = ",lbReadData.m_GroupSelector.m_Group[0].m_Category) 
//				println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD groupHandle 1 = ",lbReadData.m_GroupSelector.m_Group[0].m_Id)
//				println("GET_RACE_CHECKPOINT_DATA: iCheckpoints = ",iCheckPoints)
//				//println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD Category 2 = ",lbReadData.m_GroupSelector.m_Group[1].m_Category) 
//				//println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD groupHandle 2 = ",lbReadData.m_GroupSelector.m_Group[1].m_Id) 
//				#ENDIF
//				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
//				IF bSuccessful
//				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
//					IF readInfo.m_ReturnedRows > 0
//						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
//						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
//						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
//							println("GET_RACE_CHECKPOINT_DATA -- moving to stage 99 no data (name invalid) Checkpoint Type: ",raceStoredDataStruct.iCheckPointType ) 
//							iReadStage = 99
//						ELSE
//							iComparisonTimeTotal = 	LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,0)
//							checkpointRecordHolderName = initRowData.m_GamerName
//							IF iComparisonTimeTotal< 0
//								iComparisonTimeTotal = -(iComparisonTimeTotal)
//							ENDIF
//							#IF IS_DEBUG_BUILD
//							IF initRowData.m_NumColumnValues-1 != iCheckpoints
//								println("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues != iCheckpoints")
//								println("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues (including total time 0) = ",initRowData.m_NumColumnValues )
//								println("GET_RACE_CHECKPOINT_DATA: iCheckpoints = ", iCheckpoints)
//								SCRIPT_ASSERT("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues != iCheckpoints")
//							ENDIF
//							#ENDIF
//							REPEAT initRowData.m_NumColumnValues-1 i
//								iCheckpointTimes[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, i+1)
//								println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", i+1, " Time: ", iCheckpointTimes[i])
//							ENDREPEAT
//							println("GET_RACE_CHECKPOINT_DATA-- moving to stage 99 got best  Checkpoint Type: ",raceStoredDataStruct.iCheckPointType )
//							iReadStage = 99
//						ENDIF
//						println("Ending read in READ_BY_RANK in  ", GET_THIS_SCRIPT_NAME() ) 
//					ELSE
//						println("GET_RACE_CHECKPOINT_DATA -- moving to stage 99 no data (no rows returned) Checkpoint Type: ",raceStoredDataStruct.iCheckPointType ) 
//						iReadStage = 99
//					ENDIF
//					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
//				ELSE
//					println("GET_RACE_CHECKPOINT_DATA -- moving to stage 99 no data (read failed)  Checkpoint Type: ",raceStoredDataStruct.iCheckPointType ) 
//					iReadStage = 99
//				ENDIF
//				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//				lbReadData = emptyData
//			ENDIF
//		BREAK
//		
//		CASE 2
//			IF START_SC_LEADERBOARD_READ_BY_HANDLE(iLoadStage,bSuccessful,lbReadData,gamerHandle)
//				#IF IS_DEBUG_BUILD
//				println("GET_RACE_CHECKPOINT_DATA:  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
//				println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD Category 1 = ",lbReadData.m_GroupSelector.m_Group[0].m_Category) 
//				println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD groupHandle 1 = ",lbReadData.m_GroupSelector.m_Group[0].m_Id) 
//				println("GET_RACE_CHECKPOINT_DATA: iCheckpoints = ",iCheckPoints)
//				//println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD Category 2 = ",lbReadData.m_GroupSelector.m_Group[1].m_Category) 
//				//println("GET_RACE_CHECKPOINT_DATA: LEADERBOARD groupHandle 2 = ",lbReadData.m_GroupSelector.m_Group[1].m_Id) 
//				#ENDIF
//				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
//				IF bSuccessful
//				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
//					IF readInfo.m_ReturnedRows > 0
//						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
//						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
//						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
//							println("GET_RACE_CHECKPOINT_DATA -- moving to stage 99 no data (name invalid) Checkpoint Type: ",raceStoredDataStruct.iCheckPointType ) 
//							iReadStage = 99
//						ELSE
//							checkpointRecordHolderName = initRowData.m_GamerName
//							iComparisonTimeTotal = 	LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,0)
//							IF iComparisonTimeTotal< 0
//								iComparisonTimeTotal = -(iComparisonTimeTotal)
//							ENDIF
//							#IF IS_DEBUG_BUILD
//							IF initRowData.m_NumColumnValues-1 != iCheckpoints
//								println("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues != iCheckpoints")
//								println("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues (including total time 0) = ",initRowData.m_NumColumnValues )
//								println("GET_RACE_CHECKPOINT_DATA: iCheckpoints = ", iCheckpoints)
//								SCRIPT_ASSERT("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues != iCheckpoints")
//							ENDIF
//							#ENDIF
//							REPEAT initRowData.m_NumColumnValues-1 i
//								iCheckpointTimes[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, i+1)
//								println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", i+1, " Time: ", iCheckpointTimes[i])
//								
//							ENDREPEAT
//							iReadStage = 99
//							println("GET_RACE_CHECKPOINT_DATA-- moving to stage 99 got best  Checkpoint Type: ",raceStoredDataStruct.iCheckPointType )
//						ENDIF
//						println("Ending read in READ_BY_RANK in  ", GET_THIS_SCRIPT_NAME() ) 
//					ELSE
//						println("GET_RACE_CHECKPOINT_DATA -- moving to stage 99 no data (no rows returned) Checkpoint Type: ",raceStoredDataStruct.iCheckPointType ) 
//						iReadStage = 99
//					ENDIF
//					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
//				ELSE
//					println("GET_RACE_CHECKPOINT_DATA -- moving to stage 99 no data (read failed)  Checkpoint Type: ",raceStoredDataStruct.iCheckPointType ) 
//					iReadStage = 99
//				ENDIF
//				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//				lbReadData = emptyData
//			ENDIF
//		BREAK
//		
//		CASE 3
//			//getting information about local player
//			iNumOfFriends = NETWORK_GET_FRIEND_COUNT()
//			IF iNumOfFriends  > 0
//				iRowIndex = raceStoredDataStruct.iFriendLoopCounter*iLoopRowMax
//				IF START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW(iLoadStage,bSuccessful,
//															lbReadData,groupHandleData,
//															groupHandleData[0].m_NumGroups,TRUE,iRowIndex,iLoopRowMax)
//					#IF IS_DEBUG_BUILD
//					DEBUG_START_LEADERBOARD_PRINT_RESULTS("GET_RACE_CHECKPOINT_DATA: Getinng Friend Data",lbReadData)
//					#ENDIF
//					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
//					println("GET_RACE_CHECKPOINT_DATA: iCheckpoints = ",iCheckPoints)
//					IF bSuccessful
//					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
//						IF readInfo.m_ReturnedRows > 0
//							iIndex = 0
//							//repeat through all row before player
//							println("readInfo.m_ReturnedRows:  ", readInfo.m_ReturnedRows)
//							REPEAT readInfo.m_ReturnedRows iIndex
//								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
//								//IF readInfo.m_LocalGamerRowNumber != iIndex
//									IF initRowData.m_Rank > 0
//										checkpointRecordHolderName = initRowData.m_GamerName
//										IF raceStoredDataStruct.iBestLapTime = 0
//										OR raceStoredDataStruct.iBestLapTime < LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0)
//											raceStoredDataStruct.iBestLapTime = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0)
//											iComparisonTimeTotal = 	LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,0)
//											IF iComparisonTimeTotal< 0
//												iComparisonTimeTotal = -(iComparisonTimeTotal)
//											ENDIF
//											println("Setting current best lap for Checkspoint to ", raceStoredDataStruct.iBestLapTime)
//											#IF IS_DEBUG_BUILD
//											IF initRowData.m_NumColumnValues-1 != iCheckpoints
//												println("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues != iCheckpoints")
//												println("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues (including total time 0) = ",initRowData.m_NumColumnValues )
//												println("GET_RACE_CHECKPOINT_DATA: iCheckpoints = ", iCheckpoints)
//												SCRIPT_ASSERT("GET_RACE_CHECKPOINT_DATA: initRowData.m_NumColumnValues != iCheckpoints")
//											ENDIF
//											#ENDIF
//											REPEAT initRowData.m_NumColumnValues-1 i
//												iCheckpointTimes[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, i+1)
//												println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", i+1, " Time: ", iCheckpointTimes[i])
//												println("GET_RACE_CHECKPOINT_DATA-- friend data filling best lap time: ",raceStoredDataStruct.iCheckPointType )
//											ENDREPEAT
//										ENDIF
//									ENDIF
//								//ELSE
//								//	println("This is the local player's row ignoring")
//								//ENDIF
//							ENDREPEAT
//							
//							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
//							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
//							END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//							raceStoredDataStruct.iFriendLoopCounter++
//							IF iNumOfFriends > raceStoredDataStruct.iFriendLoopCounter*iLoopRowMax
//								iReadStage = 3
//								println("GET_RACE_CHECKPOINT_DATA friend data moving to ",raceStoredDataStruct.iFriendLoopCounter," loop") 
//								RETURN FALSE
//							ELSE
//								println("GET_RACE_CHECKPOINT_DATA friend data  finished looping returning true")
//								iReadStage = 99
//								RETURN FALSE
//							ENDIF
//						ELSE
//							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
//							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
//							END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//							raceStoredDataStruct.iFriendLoopCounter++
//							IF iNumOfFriends > raceStoredDataStruct.iFriendLoopCounter*iLoopRowMax
//								iReadStage = 3
//								println("GET_RACE_CHECKPOINT_DATA friend data no friend in first group of friends moving to ",raceStoredDataStruct.iFriendLoopCounter ," loop") 
//							ENDIF
//							RETURN TRUE
//						ENDIF
//					ELSE
//						println("GET_RACE_CHECKPOINT_DATA friend data no friends data skiping to end stage 1 (no row data) ") 
//						END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//						iReadStage = 99
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ELSE
//				println("GET_RACE_CHECKPOINT_DATA friend data  no friends data skiping to end stage 1 (friend count is 0)") 
//				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//				iReadStage = 99
//				RETURN TRUE
//			ENDIF		
//		BREAK
//		
//		CASE 99
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	RETURN FALSE
//ENDFUNC
//
//PROC CLEANUP_GET_RACE_CHECKPOINT_DATA(INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful,Leaderboard2ReadData lbReadData)
//	iReadStage = 0
//	IF iLoadStage != 0
//		println("Ending read in CLEANUP_GET_RACE_CHECKPOINT_DATA in  ", GET_THIS_SCRIPT_NAME() ) 
//		END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
//	ENDIF
//	iLoadStage = 0
//ENDPROC


FUNC BOOL GET_FREEMODE_DATA_LEADERBOARD_VALUES()
	LeaderboardReadInfo readInfo
	LeaderboardRowData initRowData
	
	GAMER_HANDLE gamerHandle

	INT i
	SWITCH freemodeDataLB.iLoadStage
		//get TOTAL XP value on leaderboard
		CASE 0
			freemodeDataLB.iTotalLB_XP		= 0
			freemodeDataLB.iTotalLB_Time 	= 0
			REPEAT MAX_STORED_CREWS i
				freemodeDataLB.iCrewLB_XP[i] = 0
				freemodeDataLB.iCrewLB_Time[i] = 0
			ENDREPEAT
			freemodeDataLB.iLoadStage = 1
		BREAK
		
		CASE 1
			
			gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			
			freemodeDataLB.ReadDataStruct.m_LeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_PLAYER_XP)
			freemodeDataLB.ReadDataStruct.m_type = LEADERBOARD2_TYPE_PLAYER
			readInfo.m_LeaderboardId = 	ENUM_TO_INT(LEADERBOARD_FREEMODE_PLAYER_XP)
			readInfo.m_LeaderboardType = ENUM_TO_INT(LEADERBOARD2_TYPE_PLAYER)
			
			IF START_SC_LEADERBOARD_READ_BY_HANDLE(freemodeDataLB.iTempLoadStage,freemodeDataLB.bTempReadResult ,freemodeDataLB.ReadDataStruct,gamerHandle)
				#IF IS_DEBUG_BUILD
				DEBUG_START_LEADERBOARD_PRINT_RESULTS("GET_FREEMODE_DATA_LEADERBOARD_VALUES (crew) ",freemodeDataLB.ReadDataStruct)
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,freemodeDataLB.ReadDataStruct)
				IF freemodeDataLB.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						//XP
						//println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", FREEMODE_DATA_LB_COLUMN_XP)
						freemodeDataLB.iTotalLB_XP = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,FREEMODE_DATA_LB_COLUMN_XP)
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- total XP = ",freemodeDataLB.iTotalLB_XP," moving to stage 2 ") 	
						IF freemodeDataLB.iTotalLB_XP < 0
							freemodeDataLB.iTotalLB_XP = 0
						ENDIF
						//TIME
						//println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", FREEMODE_DATA_LB_COLUMN_TIME)
						freemodeDataLB.iTotalLB_Time = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,FREEMODE_DATA_LB_COLUMN_TIME)
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- total TIME = ",freemodeDataLB.iTotalLB_Time," moving to stage 3 ") 
						IF freemodeDataLB.iTotalLB_Time < 0
							freemodeDataLB.iTotalLB_Time = 0
						ENDIF
					ELSE
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- no data available for local player moving to stage 2 (no rows returned)") 
					ENDIF
				ELSE
					println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- no data available for local player moving to stage 2 (read failed)") 
				ENDIF
				freemodeDataLB.iLoadStage = 2
				CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
				IF freemodeDataLB.bTempReadResult
				LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ENDIF
				END_LEADERBOARD_READ(freemodeDataLB.iTempLoadStage,freemodeDataLB.bTempReadResult,freemodeDataLB.ReadDataStruct)
			ENDIF
		BREAK
		
		CASE 2
			IF NETWORK_CLAN_SERVICE_IS_VALID()
				freemodeDataLB.ReadDataStruct.m_ClanId = GET_CREW_ID_FROM_SLOT(freemodeDataLB.iCrewSlotForLoop)
				IF freemodeDataLB.ReadDataStruct.m_ClanId != 0
				AND freemodeDataLB.ReadDataStruct.m_ClanId != -1
					println("GET_FREEMODE_DATA_LEADERBOARD_VALUES got crew ID for slot: ",freemodeDataLB.iCrewSlotForLoop," Value of: ",freemodeDataLB.ReadDataStruct.m_ClanId ) 
					freemodeDataLB.iLoadStage = 3
				ELSE
					println("GET_FREEMODE_DATA_LEADERBOARD_VALUES crew ID for slot: ",freemodeDataLB.iCrewSlotForLoop, " is 0 or -1 looping to next crew ID") 
					freemodeDataLB.iCrewSlotForLoop++
					IF freemodeDataLB.iCrewSlotForLoop >= MAX_STORED_CREWS
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES freemodeDataLB.iCrewSlotForLoop >= MAX_STORED_CREWS moving to stage 4")
						freemodeDataLB.iLoadStage = 4
					ENDIF
				ENDIF
			ELSE
				println("GET_FREEMODE_DATA_LEADERBOARD_VALUES exiting NETWORK_CLAN_SERVICE_IS_VALID is false") 
				RETURN TRUE
			ENDIF
		BREAK

		//get CREW XP value on leaderboard
		CASE 3
			gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			
			freemodeDataLB.ReadDataStruct.m_LeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_PLAYER_XP)
			freemodeDataLB.ReadDataStruct.m_type = LEADERBOARD2_TYPE_CLAN_MEMBER
			readInfo.m_LeaderboardId = 	freemodeDataLB.ReadDataStruct.m_LeaderboardId
			readInfo.m_LeaderboardType = ENUM_TO_INT(LEADERBOARD2_TYPE_CLAN_MEMBER)
			
			IF START_SC_LEADERBOARD_READ_BY_HANDLE(freemodeDataLB.iTempLoadStage,freemodeDataLB.bTempReadResult ,freemodeDataLB.ReadDataStruct,gamerHandle)
				#IF IS_DEBUG_BUILD
				DEBUG_START_LEADERBOARD_PRINT_RESULTS("GET_FREEMODE_DATA_LEADERBOARD_VALUES (crew) ",freemodeDataLB.ReadDataStruct)
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,freemodeDataLB.ReadDataStruct)
				IF freemodeDataLB.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						//XP
						//println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", FREEMODE_DATA_LB_COLUMN_XP)
						freemodeDataLB.iCrewLB_XP[freemodeDataLB.iCrewSlotForLoop] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,FREEMODE_DATA_LB_COLUMN_XP)
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- freemodeDataLB.iCrewLB_XP[",freemodeDataLB.iCrewSlotForLoop,"] = ",freemodeDataLB.iCrewLB_XP[freemodeDataLB.iCrewSlotForLoop]) 	
						IF freemodeDataLB.iCrewLB_XP[freemodeDataLB.iCrewSlotForLoop] < 0
							freemodeDataLB.iCrewLB_XP[freemodeDataLB.iCrewSlotForLoop] = 0
						ENDIF
						//TIME
						//println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ", FREEMODE_DATA_LB_COLUMN_TIME)
						freemodeDataLB.iCrewLB_Time[freemodeDataLB.iCrewSlotForLoop] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0,FREEMODE_DATA_LB_COLUMN_TIME)
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- freemodeDataLB.iCrewLB_Time[",freemodeDataLB.iCrewSlotForLoop,"] = ",freemodeDataLB.iCrewLB_Time[freemodeDataLB.iCrewSlotForLoop]) 	
						IF freemodeDataLB.iCrewLB_Time[freemodeDataLB.iCrewSlotForLoop] < 0
							freemodeDataLB.iCrewLB_Time[freemodeDataLB.iCrewSlotForLoop] = 0
						ENDIF
					ELSE
						println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- no data available for local player moving to stage 2 (no rows returned)") 
					ENDIF
				ELSE
					println("GET_FREEMODE_DATA_LEADERBOARD_VALUES-- no data available for local player moving to stage 2 (read failed)") 
				ENDIF
				
				freemodeDataLB.iLoadStage = 2
				CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
				IF freemodeDataLB.bTempReadResult
				LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ENDIF
				END_LEADERBOARD_READ(freemodeDataLB.iTempLoadStage,freemodeDataLB.bTempReadResult,freemodeDataLB.ReadDataStruct)
				freemodeDataLB.iCrewSlotForLoop++
				
				IF freemodeDataLB.iCrewSlotForLoop >= MAX_STORED_CREWS
					println("GET_FREEMODE_DATA_LEADERBOARD_VALUES freemodeDataLB.iCrewSlotForLoop >= MAX_STORED_CREWS moving to stage 4")
					freemodeDataLB.iLoadStage = 4
				ENDIF
			ENDIF
		BREAK
		CASE 4
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC WRITE_FREEMODE_DATA_LEADERBOARD_MAINTAINENCE_UPDATE()
	TEXT_LABEL_31 categoryNames[1]
	TEXT_LABEL_23 uniqueIdentifiers[1]
	INT i
	REPEAT MAX_STORED_CREWS i
		IF freemodeDataLB.iCrewLB_Time[i] != 0
		OR freemodeDataLB.iCrewLB_XP[i] > 0
			INT iCrewID = GET_CREW_ID_FROM_SLOT(i)
			IF iCrewID != 0
			AND iCrewID != -1
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_PLAYER_XP,uniqueIdentifiers,categoryNames,0,iCrewID)
					// Write XP (score)
					println("WRITE_FREEMODE_DATA_LEADERBOARD_MAINTAINENCE_UPDATE: writing UPDATE crew XP= ", freemodeDataLB.iCrewLB_XP[i],"time = ",freemodeDataLB.iCrewLB_Time[i])
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,freemodeDataLB.iCrewLB_XP[i],0)
					
					// Write time
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME, freemodeDataLB.iCrewLB_Time[i],0)
					
					// Write session		
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 0,0)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF freemodeDataLB.iTotalLB_XP > 0
	OR freemodeDataLB.iTotalLB_Time != 0
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_PLAYER_XP,uniqueIdentifiers,categoryNames,0,0)
			// Write XP (score)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,freemodeDataLB.iTotalLB_XP,0)
			
			// Write time
			println("WRITE_FREEMODE_DATA_LEADERBOARD_MAINTAINENCE_UPDATE: writing UPDATE TOTAL XP = ",freemodeDataLB.iTotalLB_XP," TIME  =  ",freemodeDataLB.iTotalLB_Time)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME, freemodeDataLB.iTotalLB_Time,0)
			
			// Write session		
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 0,0)
		ENDIF
	ENDIF
ENDPROC

PROC WRITE_TO_FREEMODE_DATA_LEADERBOARD()
	TEXT_LABEL_31 categoryNames[1]
	TEXT_LABEL_23 uniqueIdentifiers[1]

	INT iXPdifference
	INT iTimeDifference
		
	INT iCurXP = GET_MP_INT_PLAYER_STAT(MPPLY_GLOBALXP)
	INT iCurTime = GET_TOTAL_NUMBER_OF_MINUTES_FOR_FREEMODE_PLAY_TIME()
	//1199053
	//Freemode XP
	iXPdifference = iCurXP - freemodeDataLB.lastWrite.iXpLast
	iTimeDifference = iCurTime - freemodeDataLB.lastWrite.iTimeLAst
	
	//PLAYER
	IF iXPdifference > 0
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_PLAYER_XP,uniqueIdentifiers,categoryNames,0)
		
			// Write XP (score)
			println("WRITE_TO_GLOBAL_XP_LEADERBOARD: stat = ", iCurXP," iXPlast = ",freemodeDataLB.lastWrite.iXpLast," difference = ", iXPdifference)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iXPdifference,0)
			
			// Write time
			println("WRITE_TO_GLOBAL_XP_LEADERBOARD: stat = ", iCurTime," iTimeLAst = ",freemodeDataLB.lastWrite.iTimeLAst," difference = ", iTimeDifference)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME, iTimeDifference,0)
			
			// Write session		
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 0,0)
			freemodeDataLB.lastWrite.iXpLast = iCurXP
			freemodeDataLB.lastWrite.iTimeLAst = iCurTime
		ENDIF
//	ELSE
//		freemodeDataLB.lastWrite.iXpLast = iCurXP
//		freemodeDataLB.lastWrite.iTimeLAst = iCurTime
	ENDIF
ENDPROC

//Stick validation of the local data versus board here.
FUNC BOOL GLOBAL_XP_IS_LOCAL_VALUE_VALID(INT iLocal_Value, INT iLB_Value)
	IF ABSI(iLocal_Value - iLB_Value) > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC INT CREW_FREEMODE_DATA_LB_GLOBAL_VALUE_FOR_SLOT(INT iSlot)
	SWITCH iSlot
		CASE 0
			RETURN g_MPPLY_CREW_LOCAL_XP_0
		BREAK
		
		CASE 1
			RETURN g_MPPLY_CREW_LOCAL_XP_1
		BREAK
		
		CASE 2
			RETURN g_MPPLY_CREW_LOCAL_XP_2
		BREAK
		
		CASE 3
			RETURN g_MPPLY_CREW_LOCAL_XP_3
		BREAK
		
		CASE 4
			RETURN g_MPPLY_CREW_LOCAL_XP_4
		BREAK
	ENDSWITCH
	RETURN g_MPPLY_CREW_LOCAL_XP_0
ENDFUNC

FUNC MPPLY_INT_STATS CREW_FREEMODE_DATA_LB_STAT_ENUM_FOR_SLOT(INT iSlot, BOOL bXP)
	SWITCH iSlot
		CASE 0
			IF bXP
				RETURN MPPLY_CREW_LOCAL_XP_0
			ELSE
				RETURN MPPLY_CREW_LOCAL_TIME_0
			ENDIF
		BREAK
		
		CASE 1
			IF bXP
				RETURN MPPLY_CREW_LOCAL_XP_1
			ELSE
				RETURN MPPLY_CREW_LOCAL_TIME_1
			ENDIF
		BREAK
		
		CASE 2
			IF bXP
				RETURN MPPLY_CREW_LOCAL_XP_2
			ELSE
				RETURN MPPLY_CREW_LOCAL_TIME_2
			ENDIF
		BREAK
		
		CASE 3
			IF bXP
				RETURN MPPLY_CREW_LOCAL_XP_3
			ELSE
				RETURN MPPLY_CREW_LOCAL_TIME_3
			ENDIF
		BREAK
		
		CASE 4
			IF bXP
				RETURN MPPLY_CREW_LOCAL_XP_4
			ELSE
				RETURN MPPLY_CREW_LOCAL_TIME_4
			ENDIF
		BREAK
	ENDSWITCH
	RETURN MPPLY_CREW_LOCAL_XP_0
ENDFUNC

/// PURPOSE:
///    Initialises crew stats slots
PROC INITIALISE_PLAYERS_CREW_STAT_SLOTS()
	GAMER_HANDLE playersgamerhandle= GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	GET_PLAYER_CLAN_ID(playersgamerhandle)
	
	
	NETWORK_CLAN_DESC PlayerClan
    INT iLoop = NETWORK_CLAN_GET_LOCAL_MEMBERSHIPS_COUNT()
    INT i
    MPPLY_INT_STATS playerstat
	
	NET_PRINT("INITIALISE_PLAYERS_CREW_STAT_SLOTS: ")
    IF iLoop > 0
        REPEAT iLoop i
    	    NETWORK_CLAN_GET_MEMBERSHIP_DESC(PlayerClan, i)
			IF IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan.Id)
				IF iLoop = 0
					playerstat = MPPLY_CREW_0_ID
					SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(playerstat, PlayerClan.Id)
					//NET_PRINT("SLOT 0 = : ")
					//NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(playerstat))
				ELIF iLoop = 1
					playerstat = MPPLY_CREW_1_ID
					SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(playerstat, PlayerClan.Id)
					//	NET_PRINT("SLOT 1 = : ")
					//NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(playerstat))
					
				ELIF iLoop = 2
					playerstat = MPPLY_CREW_2_ID
					SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(playerstat, PlayerClan.Id)
					//	NET_PRINT("SLOT 2 = : ")
					//NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(playerstat))
				ELIF iLoop = 3
					playerstat = MPPLY_CREW_3_ID
					SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(playerstat, PlayerClan.Id)
					//	NET_PRINT("SLOT 3 = : ")
					//NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(playerstat))
				ELIF iLoop = 4
					playerstat = MPPLY_CREW_4_ID
					SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(playerstat, PlayerClan.Id)
					//	NET_PRINT("SLOT 4 = : ")
					//NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(playerstat))
					
				ELSE
					//NET_PRINT("iloop is out of range ")
					
				ENDIF
				
			ELSE
				//NET_PRINT("IS_PLAYER_ALREADY_IN_THIS_CLAN = FALSE ")

			ENDIF
		ENDREPEAT
	ENDIF
	
	
ENDPROC

PROC REWARD_PLAYER_HAS_LINKED_ACCOUNT_TO_SOCIAL_CLUB()
	IF NOT GET_MP_BOOL_PLAYER_STAT(MPPLY_XP_REWARD_PLAYER_IN_CREW)
		 GAMER_HANDLE player_handle 
	      player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	     IF NETWORK_CLAN_SERVICE_IS_VALID() 
	        IF NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle)   

			
				SET_MP_BOOL_PLAYER_STAT(MPPLY_XP_REWARD_PLAYER_IN_CREW,TRUE)
				GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWLINKCRE", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_JOINED_CREW, 2000, 1)
				NET_PRINT("\n REWARD_PLAYER_HAS_LINKED_ACCOUNT_TO_SOCIAL_CLUB Player joined a crew REWARD: 2000\n ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_STATS_BE_MAINTAINED()
	//IF GET_MILLISECONDS_IN_DAYS(ABSI(GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastFreemodeDataLBMaintenance)) >= 1
	IF GET_MILLISECONDS_IN_DAYS(ABSI(GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_MAINTAIN_XP_WITH_LEADB))) >= g_sMPTunables.iFREEMODE_XP_MAINTAIN_TIME_IN_DAYS
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_FREEMODE_DATA_LB_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT
			println("SET_FREEMODE_DATA_LB_STAGE- FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT")
		BREAK
			
		CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_UPDATE_WRITE
			println("SET_FREEMODE_DATA_LB_STAGE- FREEMODE_DATA_LB_MAINTAIN_STAGE_UPDATE_WRITEE")
		BREAK
			
		CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS
			println("SET_FREEMODE_DATA_LB_STAGE- FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS")
		BREAK
			
		CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_WRITE	
			println("SET_FREEMODE_DATA_LB_STAGE- FREEMODE_DATA_LB_MAINTAIN_STAGE_WRITE")
		BREAK
	ENDSWITCH
	#ENDIF
	freemodeDataLB.iMaintainStage = iStage
ENDPROC

PROC INIT_WRITE_VALUES()
	freemodeDataLB.lastWrite.iXpLast = GET_MP_INT_PLAYER_STAT(MPPLY_GLOBALXP)
	freemodeDataLB.lastWrite.iTimeLast = GET_TOTAL_NUMBER_OF_MINUTES_FOR_FREEMODE_PLAY_TIME()
	println("INIT_WRITE_VALUES: iXpLast = ",freemodeDataLB.lastWrite.iXpLast," iTimeLast = ",freemodeDataLB.lastWrite.iTimeLast)
ENDPROC 

/// PURPOSE:
///    Used in freemode to maintain read/writes to XP board
/// PARAMS:
///    freemodeDataLB - control/data struct
PROC MAINTAIN_FREEMODE_DATA_LEADERBOARD_READ_WRITE()
	INT i
	INT iCurrentStatValue
	
	BOOl bUpdatedStats
	BOOL bUpdateLB
	#IF IS_DEBUG_BUILD
	STRING state
	#ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT freemodeDataLB.bFilledCrewSlots
			GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			INT iCurrentCrew = GET_PLAYER_CLAN_ID(gamerHandle)
			INT iCurrentCrewSlot = GET_CURRENT_CREW_SLOT_FROM_ID(iCurrentCrew)
			IF iCurrentCrewSlot = -1 // will have to assign this. Talk to Brenda, Conor
				
				IF NETWORK_CLAN_SERVICE_IS_VALID()
					IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)   
						IF iCurrentCrew != -1
							SET_CREW_ID_IN_VALID_CREW_STAT(iCurrentCrew)
							REWARD_PLAYER_HAS_LINKED_ACCOUNT_TO_SOCIAL_CLUB()
							freemodeDataLB.bFilledCrewSlots = TRUE
						ENDIF
					ENDIF
				ELSE
					EXIT
				ENDIF
			ELSE
				freemodeDataLB.bFilledCrewSlots = TRUE
			ENDIF
		ENDIF
		SWITCH freemodeDataLB.iMaintainStage
			CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT
				//println("---FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT--- running.")
				IF NOT SHOULD_STATS_BE_MAINTAINED()
					INIT_WRITE_VALUES()
					println("---FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT--- NOT SHOULD_STATS_BE_MAINTAINED()")
					SET_FREEMODE_DATA_LB_STAGE(FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS)
					EXIT
				ENDIF
				IF NOT NETWORK_CLAN_SERVICE_IS_VALID() 
					//println("---FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT--- clan service not valid")
					EXIT
				ENDIF
				
				IF NOT GET_FREEMODE_DATA_LEADERBOARD_VALUES()
					//println("---FREEMODE_DATA_LB_MAINTAIN_STAGE_INIT--- waiting for GET_FREEMODE_DATA_LEADERBOARD_VALUES()")
					EXIT
				ENDIF
				
				freemodeDataLB.iUpdateTotalLB_XP = 0
				freemodeDataLB.iUpdateTotalLB_Time = 0
				REPEAT MAX_STORED_CREWS i
					freemodeDataLB.iUpdateCrewLB_XP[i] = 0
					freemodeDataLB.iUpdateCrewLB_Time[i] = 0
				ENDREPEAT
				

				iCurrentStatValue = GET_MP_INT_PLAYER_STAT(MPPLY_GLOBALXP)
				//IF LB TOTAL = STAT TOTAL
				IF freemodeDataLB.iTotalLB_XP = iCurrentStatValue
					#IF IS_DEBUG_BUILD
					state = "total XP Value on LB matches"
					println("CDM---FREEMODE DATA LB--- ",state)
					#ENDIF
				//IF LB TOTAL > STAT TOTAL	
				ELIF freemodeDataLB.iTotalLB_XP > iCurrentStatValue
				
					//UPDATE STAT
					SET_MP_INT_PLAYER_STAT(MPPLY_GLOBALXP,freemodeDataLB.iTotalLB_XP)
					#IF IS_DEBUG_BUILD
					state = "total XP Value on LB is greater"
					println("CDM---FREEMODE DATA LB--- ",state,"updating total STAT to = ", freemodeDataLB.iTotalLB_XP)
					#ENDIF
					bUpdatedStats = TRUE
				//LB TOTAL < STAT TOTAL
				ELSE
					//UPDATE LB
					freemodeDataLB.iUpdateTotalLB_XP = iCurrentStatValue - freemodeDataLB.iTotalLB_XP
					#IF IS_DEBUG_BUILD
					state = "total XP Value on LB is less"
					println("CDM---FREEMODE DATA LB--- ",state,"updating total LB by = ", freemodeDataLB.iUpdateTotalLB_XP)
					#ENDIF
					bUpdateLB = TRUE
				ENDIF
				
				//CHECK CREW VALUES
				REPEAT MAX_STORED_CREWS i
					iCurrentStatValue = CREW_FREEMODE_DATA_LB_GLOBAL_VALUE_FOR_SLOT(i)
					//IF LB IS HIGHER
					IF freemodeDataLB.iCrewLB_XP[i] > iCurrentStatValue 
						//CREW STAT = LB
						SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(CREW_FREEMODE_DATA_LB_STAT_ENUM_FOR_SLOT(i,TRUE),freemodeDataLB.iCrewLB_XP[i])
						println("CDM---FREEMODE DATA LB---",state," for slot",  i," the XP STAT is less than LB setting to = ", freemodeDataLB.iCrewLB_XP[i])
						bUpdatedStats = TRUE
					//LB IS LOWER
					ELIF freemodeDataLB.iCrewLB_XP[i] < iCurrentStatValue
						//WRITE TO LB WITH THAT CREW
						freemodeDataLB.iUpdateCrewLB_XP[i] = iCurrentStatValue - freemodeDataLB.iCrewLB_XP[i]
						println("CDM---FREEMODE DATA LB---",state," for slot",i," the XP STAT is greater than LB writing new value to LB = ", freemodeDataLB.iUpdateCrewLB_XP[i])
						//MINUS WRITTEN VALUE FROM TOTAL LB VALUE
						freemodeDataLB.iUpdateTotalLB_XP += -freemodeDataLB.iUpdateCrewLB_XP[i]
						println("CDM---FREEMODE DATA LB---",state," for slot",i," the XP STAT is greater than LB incrementing LB update value by = ", -freemodeDataLB.iUpdateCrewLB_XP[i])
						println("CDM---FREEMODE DATA LB--- total  XP Value update is now ",freemodeDataLB.iUpdateTotalLB_XP)
						bUpdateLB = TRUE
					ENDIF
				ENDREPEAT
				
			
				iCurrentStatValue = GET_TOTAL_NUMBER_OF_MINUTES_FOR_FREEMODE_PLAY_TIME()
				//IF LB TOTAL = STAT TOTAL
				IF freemodeDataLB.iTotalLB_Time = iCurrentStatValue
					#IF IS_DEBUG_BUILD
					state = "total TIME Value on LB matches"
					println("CDM---FREEMODE DATA LB--- ",state)
					#ENDIF
				//IF LB TOTAL > STAT TOTAL	
				ELIF freemodeDataLB.iTotalLB_Time > iCurrentStatValue
				
					//UPDATE STAT
					//SET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_SPENT_FREEMODE,freemodeDataLB.iTotalLB_Time*60*1000) //we store minutes
					#IF IS_DEBUG_BUILD
					state = "total TIME Value on LB is greater"
					println("CDM---FREEMODE DATA LB--- ",state,"updating total STAT to = ", freemodeDataLB.iTotalLB_Time*60*1000) //we store minutes
					#ENDIF
					bUpdatedStats = TRUE
				//LB TOTAL < STAT TOTAL
				ELSE
					//UPDATE LB
					freemodeDataLB.iUpdateTotalLB_Time = iCurrentStatValue - freemodeDataLB.iTotalLB_Time
					#IF IS_DEBUG_BUILD
					state = "total TIME Value on LB is less"
					println("CDM---FREEMODE DATA LB--- ",state,"updating total LB by = ", freemodeDataLB.iUpdateTotalLB_Time)
					#ENDIF
					bUpdateLB = TRUE
				ENDIF
				
				//CHECK CREW VALUES
				REPEAT MAX_STORED_CREWS i
					iCurrentStatValue = GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_PLAYER_STAT(CREW_FREEMODE_DATA_LB_STAT_ENUM_FOR_SLOT(i,FALSE))
					//IF LB IS HIGHER
					IF freemodeDataLB.iCrewLB_Time[i] > iCurrentStatValue 
						//CREW STAT = LB
						SET_MP_INT_PLAYER_STAT(CREW_FREEMODE_DATA_LB_STAT_ENUM_FOR_SLOT(i,FALSE),freemodeDataLB.iCrewLB_Time[i])
						println("CDM---FREEMODE DATA LB---",state," for slot",  i," the TIME STAT is less than LB setting to = ", freemodeDataLB.iCrewLB_Time[i])
						bUpdatedStats = TRUE
					//LB IS LOWER
					ELIF freemodeDataLB.iCrewLB_Time[i] < iCurrentStatValue
						//WRITE TO LB WITH THAT CREW
						freemodeDataLB.iUpdateCrewLB_Time[i] = iCurrentStatValue - freemodeDataLB.iCrewLB_Time[i]
						println("CDM---FREEMODE DATA LB---",state," for slot",i," the TIME STAT is greater than LB writing new value to LB = ", freemodeDataLB.iUpdateCrewLB_Time[i])
						//MINUS WRITTEN VALUE FROM TOTAL LB VALUE
						freemodeDataLB.iUpdateTotalLB_Time += -freemodeDataLB.iUpdateCrewLB_Time[i]
						println("CDM---FREEMODE DATA LB---",state," for slot",i," the TIME STAT is greater than LB incrementing LB update value by = ", -freemodeDataLB.iUpdateCrewLB_Time[i])
						println("CDM---FREEMODE DATA LB--- total  TIME Value update is now ",freemodeDataLB.iUpdateTotalLB_Time)
						bUpdateLB = TRUE
					ENDIF
				ENDREPEAT
				
				IF bUpdateLB 
					IF bUpdatedStats
						SET_BIT(freemodeDataLB.iBS, FREEMODE_DATA_LB_BS_UPDATE_STAT)
					ENDIF
					SET_FREEMODE_DATA_LB_STAGE(FREEMODE_DATA_LB_MAINTAIN_STAGE_UPDATE_WRITE)
				ELSE
					INIT_WRITE_VALUES()
					//g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastFreemodeDataLBMaintenance = 
					SET_MP_INT_CHARACTER_STAT(MP_STAT_MAINTAIN_XP_WITH_LEADB,GET_CLOUD_TIME_AS_INT())
					//REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
					SET_FREEMODE_DATA_LB_STAGE(FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS)
				ENDIF
				
				
				//PSEUDOCODE FOR REFERENCE
				//IF LB TOTAL = STAT TOTAL
					//CHECK CREW VALUES
						//IF THEY MATCH--- DONE
							//EXIT
						//ELSE THEY DONT MATCH
							//IF LB IS HIGHER
								//CREW STAT = LB
							//ELSE LB IS LOWER
								//WRITE TO LB WITH THAT CREW
								//MINUS WRITTEN VALUE FROM TOTAL LB VALUE
							//END
						//END
					//END
				//ELSE LB TOTAL != STAT TOTAL
					//IF LB TOTAL > STAT TOTAL
						//UPDATE STAT VALUE
						//CHECK CREW VALUES
							//UPDATE MIS-MATCHED CREW VALUES
						//END
					//ELSE LB TOTAL < STAT TOTAL
						//CHECK CREW VALUES
							//IF THEY MATCH
								//UPDATE TOTAL VALUE ON LB
							//ELSE
								//UPDATE MIS-MATCHED CREW VALUES ANY OVER TOTAL WRITE MINUS
							//END
						//END
					//END
				//END		
			BREAK
			
			CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_UPDATE_WRITE
				IF NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
					WRITE_FREEMODE_DATA_LEADERBOARD_MAINTAINENCE_UPDATE()
					INIT_WRITE_VALUES()
					SET_MP_INT_CHARACTER_STAT(MP_STAT_MAINTAIN_XP_WITH_LEADB,GET_CLOUD_TIME_AS_INT())
					//g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastFreemodeDataLBMaintenance = GET_CLOUD_TIME_AS_INT()
					//REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
					SET_FREEMODE_DATA_LB_STAGE(FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS)
				ENDIF
			BREAK
			
			CASE FREEMODE_DATA_LB_MAINTAIN_STAGE_CHECKS
				INT iXPdifference
				IF NOT freemodeDataLB.bFinishedInitialLeaderboardCheck
					println("MAINTAIN_FREEMODE_DATA_LEADERBOARD_READ_WRITE: freemodeDataLB.bFinishedInitialLeaderboardCheck = TRUE")
					freemodeDataLB.bFinishedInitialLeaderboardCheck = TRUE
				ENDIF
				
				
				IF NOT HAS_NET_TIMER_STARTED(freemodeDataLB.updateTimer) 
					START_NET_TIMER(freemodeDataLB.updateTimer,TRUE)
					// record players Crew XP at start

				ELSE
					IF HAS_NET_TIMER_EXPIRED(freemodeDataLB.updateTimer,30000,TRUE) //at least 30 seconds between writes no matter what.
						iXPdifference = GET_MP_INT_PLAYER_STAT(MPPLY_GLOBALXP) - freemodeDataLB.lastWrite.iXpLast
						IF HAS_NET_TIMER_EXPIRED(freemodeDataLB.updateTimer,g_sMPTunables.iFREEMODE_DATA_LB_PERIODIC_WRITE_TIME,TRUE)
							println("MAINTAIN_FREEMODE_DATA_LEADERBOARD_READ_WRITE writing stats as timer expired")
							WRITE_TO_FREEMODE_DATA_LEADERBOARD()
							RESET_NET_TIMER(freemodeDataLB.updateTimer)
							EXIT
						ENDIF
						
						IF iXPdifference > g_sMPTunables.iFREEMODE_DATA_LB_WRITE_XP_THRESHOLD
							println("MAINTAIN_FREEMODE_DATA_LEADERBOARD_READ_WRITE xp difference is > than threshold: ", iXPdifference)
							WRITE_TO_FREEMODE_DATA_LEADERBOARD()
							RESET_NET_TIMER(freemodeDataLB.updateTimer)
						ENDIF
					ENDIF
				ENDIF
			BREAK	
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_EXCLUDED_LIST(GAMER_HANDLE gamerHandle, GAMER_HANDLE &excludeGamer[])
	INT i
	IF IS_GAMER_HANDLE_VALID(gamerHandle)
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_GAMER_HANDLE_VALID(excludeGamer[i])
				IF NETWORK_ARE_HANDLES_THE_SAME(gamerHandle,excludeGamer[i])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC 

//Example use of below function
//SC_LEADERBOARD_CONTROL_STRUCT scLB_control
//SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,FMMC_TYPE_RACE,"RaceName","DisplayName",ciRACE_SUB_TYPE_STANDARD,5,FALSE)
//IF SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE(scLB_control)
//		//finished
//ENDIF
//once finished SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(scLB_control)
/// PURPOSE:
///    To compare friends and send message if you have beaten them
/// PARAMS:
///    scLB_control - 
FUNC BOOL SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE(SC_LEADERBOARD_CONTROL_STRUCT &scLB_control, UGCStateUpdate_Data& ugcdata, BOOL &bHaveFriendsToSendTo, INT iMyPreviousScore, INT iMyCurrentScore, INT iMissionType, INT iMissionSubType, INT iLaps, STRING stMissionName, GAMER_HANDLE &excludeGamer[])
	
	IF IS_GAMER_HANDLE_VALID(excludeGamer[0])
		// Added so compiles, Conor remove this once you've used this data properly.
	ENDIF
	
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	//INT i
	INT iNumOfFriends
	//INT iPlayerRow = -1
	INT iRowIndex
	INT iIndex
	INT iLoopRowMax = 100
	INT iFriendScore
	SWITCH scLB_control.iLoadStage[SC_LB_SECTION_FRIEND]
		CASE 0
			//getting information about local player
			iNumOfFriends = NETWORK_GET_FRIEND_COUNT()
			
			IF iNumOfFriends  > 0
				iRowIndex = scLB_control.iLoopCounter*iLoopRowMax
				IF START_SC_LEADERBOARD_READ_FRIENDS_BY_ROW(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct,scLB_control.groupHandle,scLB_control.groupHandle[0].m_NumGroups,TRUE,iRowIndex,iLoopRowMax)
					#IF IS_DEBUG_BUILD
					DEBUG_START_LEADERBOARD_PRINT_RESULTS("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE",scLB_control.ReadDataStruct)
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
					IF scLB_control.bTempReadResult
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						println("readInfo.m_ReturnedRows:  ", readInfo.m_ReturnedRows)
						IF readInfo.m_ReturnedRows > 0
							iIndex = 0
							//repeat through all row before player
							REPEAT readInfo.m_ReturnedRows iIndex
								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(iIndex, initRowData)
								
								IF readInfo.m_LocalGamerRowNumber != iIndex
									
									IF initRowData.m_Rank > 0
										IF NOT IS_PLAYER_IN_EXCLUDED_LIST(initRowData.m_GamerHandle,excludeGamer) 
											iFriendScore = LEADERBOARDS2_READ_GET_ROW_DATA_INT(iIndex, 0)
											println("Previous Score:  ", iMyPreviousScore," Current Score:  ", iMyCurrentScore,"Their Score: ", iFriendScore)
											IF iMyPreviousScore < 0
												iMyPreviousScore = iMyPreviousScore*-1
												//println("Your Previous Score (UPDATED):  ", iMyPreviousScore)	
											ENDIF
											IF iMyCurrentScore < 0
												iMyCurrentScore = iMyCurrentScore*-1
												//println("Your Current Score (UPDATED):  ", iMyCurrentScore)
											ENDIF
											IF iFriendScore  < 0
												iFriendScore  = iFriendScore *-1
												//println("Their Score (UPDATED): ", iFriendScore)
											ENDIF
											
											IF iFriendScore > iMyCurrentScore
											AND ((iMyPreviousScore > iFriendScore) OR (iMyPreviousScore = 0))
												
												//IF NOT ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
												IF IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
													
													SC_INBOX_MESSAGE_PUSH_GAMER_T0_RECIP_LIST(initRowData.m_GamerHandle)
													bHaveFriendsToSendTo = TRUE
													
													println("You placed higher than a friend named ",initRowData.m_GamerName, "congrats!")
													
												ENDIF
											ENDIF
										ELSE
											println("Gamer is in exlcuded list ignoring for messaging name = ",initRowData.m_GamerName)
										ENDIF
									ENDIF
								ELSE
									println("This is the local player's row ignoring")
								ENDIF
							ENDREPEAT
							
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
							scLB_control.iLoopCounter++
							IF iNumOfFriends > scLB_control.iLoopCounter*iLoopRowMax
								scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 0
								println("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE  moving to ",scLB_control.iLoopCounter ," loop") 
								RETURN FALSE
							ELSE
								println("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE  finished looping returning true")
								scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
								RETURN FALSE
							ENDIF
						ELSE
							CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
							scLB_control.iLoopCounter++
							IF iNumOfFriends > scLB_control.iLoopCounter*iLoopRowMax
								scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 0
								println("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE no friend in first group of friends moving to ",scLB_control.iLoopCounter ," loop") 
							ENDIF
							println("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE no rows returned ") 
							RETURN TRUE
						ENDIF
					ELSE
						println("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE  no friends data skiping to end stage 1 (no row data) ") 
						END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
						scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				println("SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE  no friends data skiping to end stage 1 (friend count is 0)") 
				END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
				scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 1
				RETURN TRUE
			ENDIF		
		BREAK
		
		CASE 1
			
			IF bHaveFriendsToSendTo
				
				NET_PRINT("[WJK] - SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE - bHaveFriendsToSendTo = TRUE")NET_NL()
				
				ugcdata.tl31MissionContentId		= scLB_control.ReadDataStruct.m_GroupSelector.m_Group[0].m_Id
				ugcdata.Score 						= iMyCurrentScore
				ugcdata.tl31MissionName				= stMissionName
				ugcdata.tl31CoPlayerName			= " "
				ugcdata.MissionType					= iMissionType
				ugcdata.MissionSubType				= iMissionSubType
				ugcdata.Laps						= iLaps
				ugcdata.bSwapSenderWithCoPlayer		= FALSE
				
				NET_PRINT("[WJK] - ugcdata data:")NET_NL()
				NET_PRINT("[WJK] - tl31MissionContentId:") NET_PRINT(ugcdata.tl31MissionContentId) NET_NL()
				NET_PRINT("[WJK] - Score:") NET_PRINT_INT(ugcdata.Score) NET_NL()
				NET_PRINT("[WJK] - tl31MissionName:") NET_PRINT(ugcdata.tl31MissionName) NET_NL()
				NET_PRINT("[WJK] - tl31CoPlayerName:") NET_PRINT(ugcdata.tl31CoPlayerName) NET_NL()
				NET_PRINT("[WJK] - MissionType:") NET_PRINT_INT(ugcdata.MissionType) NET_NL()
				NET_PRINT("[WJK] - MissionSubType:") NET_PRINT_INT(ugcdata.MissionSubType) NET_NL()
				NET_PRINT("[WJK] - Laps:") NET_PRINT_INT(ugcdata.Laps) NET_NL()
				NET_PRINT("[WJK] - bSwapSenderWithCoPlayer:")
				IF ugcdata.bSwapSenderWithCoPlayer
					NET_PRINT("TRUE")
				ELSE
					NET_PRINT("FALSE")
				ENDIF
				NET_NL()
				
				SC_INBOX_SEND_UGCSTATUPDATE_TO_RECIP_LIST(ugcdata)
				
			ELSE
			
				NET_PRINT("[WJK] - SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE - bHaveFriendsToSendTo = FALSE")NET_NL()
			
			ENDIF
			
			scLB_control.iLoadStage[SC_LB_SECTION_FRIEND] = 2
			
			RETURN TRUE
			
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC INT SC_LB_GET_GLOBAL_LEADERBOARD_FROM_MODE_LB(INT iCurrentModeLB)
	SWITCH INT_TO_ENUM(LEADERBOARDS_ENUM,iCurrentModeLB)
		CASE LEADERBOARD_MINI_GAMES_DARTS
		CASE LEADERBOARD_MINI_GAMES_GOLF
		CASE LEADERBOARD_MINI_GAMES_MP_SRANGE
		CASE LEADERBOARD_MINI_GAMES_TENNIS
		CASE LEADERBOARD_MINI_GAMES_ARM_WRESTLING
		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_TIME
//		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_SCORE
		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE
		CASE LEADERBOARD_FREEMODE_SURVIVAL
		CASE LEADERBOARD_FREEMODE_RACE_TOURNAMENT_QUALIFICATION
			RETURN iCurrentModeLB
		BREAK
		CASE LEADERBOARD_FREEMODE_DEATHMATCH
		CASE LEADERBOARD_FREEMODE_DEATHMATCH_OVERALL
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_DEATHMATCH_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_TEAM_DEATHMATCH
		CASE LEADERBOARD_FREEMODE_TEAM_DEATHMATCH_OVERALL
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_TEAM_DEATHMATCH_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH
		CASE LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH_OVERALL
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_BASEJUMPS
		CASE LEADERBOARD_FREEMODE_BASEJUMPS_OVERALL
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_BASEJUMPS_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_RACES
		CASE LEADERBOARD_FREEMODE_RACES_OVERALL
		CASE LEADERBOARD_FREEMODE_RACES_LAPS
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_RACES_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_GTA_RACES
		CASE LEADERBOARD_FREEMODE_GTA_RACES_OVERALL
		CASE LEADERBOARD_FREEMODE_GTA_RACES_LAPS
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_GTA_RACES_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_RALLY
		CASE LEADERBOARD_FREEMODE_RALLY_LAPS
		CASE LEADERBOARD_FREEMODE_RALLY_OVERALL
		CASE LEADERBOARD_FREEMODE_RALLY_CODRIVER_OVERALL
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_RALLY_OVERALL)
		BREAK
		CASE LEADERBOARD_FREEMODE_ON_FOOT_RACE
		CASE LEADERBOARD_FREEMODE_ON_FOOT_RACE_OVERALL
		CASE LEADERBOARD_FREEMODE_ON_FOOT_RACE_LAPS
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_ON_FOOT_RACE_OVERALL)
		BREAK
//		CASE LEADERBOARD_FREEMODE_CREW_CHALLENGES
//		CASE LEADERBOARD_FREEMODE_CREW_CHALLENGES_H2H 
//			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_CREW_CHALLENGES_OVERALL)
//		BREAK
		CASE LEADERBOARD_FREEMODE_NON_CONTACT_RACES
		CASE LEADERBOARD_FREEMODE_NON_CONTACT_RACES_OVERALL
			RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES_OVERALL)
		BREAK
		
	ENDSWITCH
	IF iCurrentModeLB >= ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES_1_LAPS)
	AND iCurrentModeLB <= ENUM_TO_INT(LEADERBOARD_FREEMODE_ELO_NON_CONTACT_RACES_OVERALL_SEASON50)
		RETURN ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES_OVERALL)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	println("SC_LB_GET_GLOBAL_LEADERBOARD_FROM_MODE_LB: trying to get global LB from an invalid mode ID. Mode ID = ", iCurrentModeLB)
	SCRIPT_ASSERT("SC_LB_GET_GLOBAL_LEADERBOARD_FROM_MODE_LB: trying to get global LB from an invalid mode ID. See Conor")
	#ENDIF
	RETURN -1
ENDFUNC

PROC RESET_MATCHED_CREWS(SC_MATCHED_CREWS &matchedCrew)
	INT i
	matchedCrew.iLoadStage = 0
	matchedCrew.iTempLoadStage = 0
	matchedCrew.bFinished = FALSE
	REPEAT MAX_SC_MATCHED_CREWS i
		matchedCrew.CrewData[i].Name = ""
		matchedCrew.CrewData[i].Tag = ""
		matchedCrew.CrewData[i].iID = 0
	ENDREPEAT
ENDPROC

FUNC BOOL GET_MATCHED_CREWS_FOR_GAME_MODE(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails,SC_LEADERBOARD_CONTROL_STRUCT &scLB_control, SC_MATCHED_CREWS &matchedCrew)
	GAMER_HANDLE gamerHandle
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	
	NETWORK_CLAN_DESC clanDesc
	IF sLaunchMissionDetails.iCreatorID = FMMC_ROCKSTAR_CREATOR_ID
    OR sLaunchMissionDetails.iCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
    OR sLaunchMissionDetails.iCreatorID = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
	OR sLaunchMissionDetails.iCreatorID = FMMC_MINI_GAME_CREATOR_ID
		
		
	ELSE
		matchedCrew.iLoadStage = 0
		matchedCrew.bFinished = TRUE
		RETURN TRUE
	ENDIF
	
	INT i
	INT iCounter
	#IF IS_DEBUG_BUILD
	STRING funcName = "GET_MATCHED_CREWS_FOR_GAME_MODE"
	//DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(funcName,scLB_control.ReadDataStruct)
	#ENDIF
	IF NOT matchedCrew.bFinished
		SWITCH matchedCrew.iLoadStage
			CASE 0
				IF NETWORK_CLAN_SERVICE_IS_VALID()
					gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
					IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
						IF NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), gamerHandle)
							println(funcName," moving to stage 1 ") 
							matchedCrew.ReadDataStruct.m_LeaderboardId = SC_LB_GET_GLOBAL_LEADERBOARD_FROM_MODE_LB(scLB_control.ReadDataStruct.m_LeaderboardId)
							matchedCrew.ReadDataStruct.m_ClanId = clanDesc.Id
							matchedCrew.ReadDataStruct.m_type = LEADERBOARD2_TYPE_CLAN
							matchedCrew.iLoadStage = 1
							RETURN FALSE
						ENDIF
					ELSE
						println(funcName," moving to stage 2  NETWORK_CLAN_PLAYER_IS_ACTIVE is false") 
						matchedCrew.iLoadStage = 2
						RETURN TRUE
					ENDIF
				ELSE
					println(funcName," moving to stage 2 NETWORK_CLAN_SERVICE_IS_VALID is false") 
					matchedCrew.iLoadStage = 2
					RETURN TRUE
				ENDIF
			BREAK
			CASE 1
				matchedCrew.ReadDataStruct.m_Type = LEADERBOARD2_TYPE_CLAN
				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				iCounter = 0
				IF START_SC_LEADERBOARD_READ_BY_RADIUS(matchedCrew.iTempLoadStage,matchedCrew.bTempReadResult,matchedCrew.ReadDataStruct,8,gamerHandle)
					#IF IS_DEBUG_BUILD
					DEBUG_START_LEADERBOARD_PRINT_RESULTS(funcName,matchedCrew.ReadDataStruct)
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,matchedCrew.ReadDataStruct)
					IF matchedCrew.bTempReadResult
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							REPEAT readInfo.m_ReturnedRows i
								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(i, initRowData)
								IF initRowData.m_ClanId != matchedCrew.ReadDataStruct.m_ClanId
									IF iCounter < MAX_SC_MATCHED_CREWS
										matchedCrew.CrewData[iCounter].iID  = initRowData.m_ClanId
										matchedCrew.CrewData[iCounter].Name  = initRowData.m_ClanName
										matchedCrew.CrewData[iCounter].Tag  = initRowData.m_ClanTag
										println(funcName, "-- matchedCrew.CrewData[",iCounter,"].iID = ", matchedCrew.CrewData[iCounter].iID,
														" Name = ", matchedCrew.CrewData[iCounter].Name,
														" Tag = ",matchedCrew.CrewData[iCounter].Tag)
										iCounter++
									ENDIF
								ENDIF
								CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
							ENDREPEAT
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(matchedCrew.iTempLoadStage,matchedCrew.bTempReadResult,matchedCrew.ReadDataStruct)
							matchedCrew.iLoadStage = 2
							println(funcName," -- found matched crews moving to stage 2") 
						ELSE	
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
							END_LEADERBOARD_READ(matchedCrew.iTempLoadStage,matchedCrew.bTempReadResult,matchedCrew.ReadDataStruct)
							matchedCrew.iLoadStage = 2
							println(funcName," -- no rows returned moving to stage 2") 
						ENDIF
					ELSE
						END_LEADERBOARD_READ(matchedCrew.iTempLoadStage,matchedCrew.bTempReadResult,matchedCrew.ReadDataStruct)
						matchedCrew.iLoadStage = 2
						println(funcName," -- no matched crews found moving to stage 2") 
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				println(funcName," finishing generating matched crew list")
				matchedCrew.iLoadStage = 0
				matchedCrew.bFinished = TRUE
			BREAK
		ENDSWITCH
	ELSE
		RETURN TRUE
	ENDIF

	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    To get the global and personal best for a given race in SP.
/// PARAMS:
///    iReadStage - this is just needs to be an int defined in the script that will retain its value over more than a frame (i.e. dont just define it before calling this)
///    iLoadStage - this is just needs to be an int defined in the script that will retain its value over more than a frame (i.e. dont just define it before calling this)
///    bSuccessful - this is just needs to be an bool defined in the script that will retain its value over more than a frame (i.e. dont just define it before calling this)
///    iRaceType - the number indicating the race (NOTE: OFFROAD RACES offset is 100)
///    iGlobalBest - when function returns true this will be the global best
///    iPersonalBest - when function returns true this will be the personal best
/// RETURNS:
///    True when it's finished. Note this should always return true even if it finds no records and the values remain 0.
FUNC BOOL GET_SP_RACE_PERSONAL_GLOBAL_BEST(INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful, INT iRaceID,INT& iGlobalBest, INT& iPersonalBest)
	
	Leaderboard2ReadData lbReadData
	Leaderboard2ReadData emptyData
	//LeaderboardRowData tempRowData[1]
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	
	GAMER_HANDLE gamerHandle
	gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES)
	lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
	lbReadData.m_GroupSelector.m_NumGroups = 3
	lbReadData.m_GroupSelector.m_Group[0].m_Category = "GameType"
	lbReadData.m_GroupSelector.m_Group[1].m_Category = "Location"
	lbReadData.m_GroupSelector.m_Group[2].m_Category = "Type"
	lbReadData.m_GroupSelector.m_Group[0].m_Id = "SP"
	SWITCH iRaceID
		CASE 0
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGCR_1"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
		BREAK
		CASE 1
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGCR_2"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
		BREAK
		CASE 2
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGCR_4"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
		BREAK
		CASE 3
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGCR_5"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
		BREAK
		CASE 4
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGCR_6"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "StreetRace"
		BREAK
		CASE 5
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGSR_1"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
		BREAK
		CASE 6
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGSR_2"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
		BREAK
		CASE 7
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGSR_3"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
		BREAK
		CASE 8
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "MGSR_4"
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "SeaRace"
		BREAK
		CASE 100
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "OR_RACE_01" //Canyon Cliffs
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
		BREAK
		CASE 101
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "OR_RACE_02" //Ridge Run
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
		BREAK
		CASE 102
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "OR_RACE_03" //Valley Trail
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
		BREAK
		CASE 103
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "OR_RACE_04" //Lakeside Splash
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
		BREAK
		CASE 104
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "OR_RACE_05" //Eco Friendly
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
		BREAK
		CASE 105
			lbReadData.m_GroupSelector.m_Group[1].m_Id = "OR_RACE_06" //Mineward Spiral
			lbReadData.m_GroupSelector.m_Group[2].m_Id = "OffroadRace"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("GET_SP_RACE_PERSONAL_GLOBAL_BEST iRaceType was invalid")
		BREAK
	ENDSWITCH
//	
	SWITCH iReadStage
		CASE 0
			IF START_SC_LEADERBOARD_READ_BY_RANK(iLoadStage,bSuccessful,lbReadData,1,1)
				#IF IS_DEBUG_BUILD
				println("GET_SP_RACE_GLOBAL_BEST:  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
				println("groupHandle[0] = ",lbReadData.m_GroupSelector.m_Group[0].m_Id
						," groupHandle[1] = ",lbReadData.m_GroupSelector.m_Group[1].m_Id
						," groupHandle[2] = ",lbReadData.m_GroupSelector.m_Group[2].m_Id) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
				IF bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						
						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							println("GET_SP_RACE_GLOBAL_BEST -- moving to stage 2 no global data ") 
							iReadStage = 2
						ELSE
							iGlobalBest = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 3)
							println("Global best is for player name: ", initRowData.m_GamerName, " is - ",iGlobalBest)
							iReadStage = 1
						ENDIF
						//println("Ending read in READ_BY_RANK in  ", GET_THIS_SCRIPT_NAME() ) 
					ELSE
						println("GET_SP_RACE_GLOBAL_BEST -- moving to stage 2 no global data ") 
						iReadStage = 2
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					println("GET_SP_RACE_GLOBAL_BEST -- moving to stage 2 no global data ") 
					iReadStage = 2
				ENDIF
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				lbReadData = emptyData
			ENDIF
		BREAK
		CASE 1
			IF START_SC_LEADERBOARD_READ_BY_HANDLE(iLoadStage,bSuccessful,lbReadData,gamerHandle)
				#IF IS_DEBUG_BUILD
				println("GET_SP_RACE_PERSONAL_BEST:  LEADERBOARD ID = ", lbReadData.m_LeaderboardId, " groupHandle = ",lbReadData.m_GroupSelector.m_Group[0].m_Id) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
				IF bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						
						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							println("GET_SP_RACE_PERSONAL_BEST -- moving to stage 2 no data for local player ") 
							iReadStage = 2
						ELSE
							iPersonalBest = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 3)
							println("Personal best is for player name: ", initRowData.m_GamerName, " is - ",iPersonalBest )
							iReadStage = 2
						ENDIF
					ELSE
						println("GET_SP_RACE_PERSONAL_BEST -- moving to stage 2 no data for local player ") 
						iReadStage = 2
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					println("GET_SP_RACE_PERSONAL_BEST -- moving to stage 2 no data for local player ") 
					iReadStage = 2
					
				ENDIF
				println("Ending read in READ_BY_HANDLE in  ", GET_THIS_SCRIPT_NAME() ) 
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				lbReadData = emptyData
			ENDIF
		BREAK
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CLEANUP_SP_RACE_PERSONAL_GLOBAL_BEST(INT& iReadStage, INT& iLoadStage, BOOL& bResult, INT& iGlobalBest, INT& iPersonalBest)
	iReadStage = 0
	iLoadStage = 0
	iGlobalBest = 0 
	bResult = FALSE
	iPersonalBest = 0
ENDPROC

FUNC BOOL DO_SIGNED_OUT_WARNING(INT& iBS, BOOL bStoreMessage = FALSE)
	FE_WARNING_FLAGS iButtonBS = FE_WARNING_OK
	IF scLBSignInWarn.iFrameLastCalled + 5 < GET_FRAME_COUNT()
	AND scLBSignInWarn.iFrameLastCalled > 0
		RESET_NET_TIMER(scLBSignInWarn.signInDelay)
		RESET_NET_TIMER(scLBSignInWarn.signedOutTimer)
		iBS = 0
		scLBSignInWarn.iFrameLastCalled = 0
		SET_LOADING_ICON_INACTIVE()
		println("DO_SIGNED_OUT_WARNING: reseting more than 5 frames since last called")
	ENDIF
	scLBSignInWarn.iFrameLastCalled = GET_FRAME_COUNT()
	NP_UNAVAILABILITY_REASON reason = REASON_INVALID
	IF IS_PLAYSTATION_PLATFORM()
		IF NETWORK_IS_NP_AVAILABLE() = FALSE
			reason = NETWORK_GET_NP_UNAVAILABLE_REASON()
		ENDIF
	ENDIF
	IF (IS_PLAYSTATION_PLATFORM() AND 
	(reason = REASON_AGE OR reason = REASON_GAME_UPDATE OR reason = REASON_SYSTEM_UPDATE OR reason = REASON_CONNECTION))
	OR (NOT SCRIPT_IS_CLOUD_AVAILABLE() AND NETWORK_IS_SIGNED_ONLINE())
		
		// wait for ros refresh
		IF NETWORK_IS_REFRESHING_ROS_CREDENTIALS()
			println("DO_SIGNED_OUT_WARNING:  refreshing ROS credentials")
			RUN_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon))
			IF NOT IS_BIT_SET(iBs,4)
				SET_BIT(iBs,4)
				scLBSignInWarn.loadingIcon.sMainStringSlot = ""
				REFRESH_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon)
			ENDIF
		ELSE
			IF reason = REASON_AGE
				SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "HUD_PROFILECHNG",iButtonBS)
			ELIF reason = REASON_GAME_UPDATE
				SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "HUD_GAMEUPD_G",iButtonBS)
			ELIF reason= REASON_SYSTEM_UPDATE
				SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "HUD_SYSTUPD_G",iButtonBS)
			ELIF reason= REASON_CONNECTION
				SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_NO_INT",iButtonBS)
			ELIF NOT SCRIPT_IS_CLOUD_AVAILABLE()
				SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_NO_ROS",iButtonBS)
			ENDIF

			println("DO_SIGNED_OUT_WARNING:  warning signed in no cloud")
			IF NOT IS_BIT_SET(iBS, 0)
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					SET_BIT(iBS, 0)
				ENDIF
			ELSE
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					RESET_NET_TIMER(scLBSignInWarn.signedOutTimer)
					RESET_NET_TIMER(scLBSignInWarn.signInDelay)
					iBS = 0
					scLBSignInWarn.iFrameLastCalled = 0
					SET_LOADING_ICON_INACTIVE()
					println("DO_SIGNED_OUT_WARNING: pressed accept killing warning")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RUN_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon)) 	
		IF (HAS_NET_TIMER_STARTED(scLBSignInWarn.signedOutTimer) AND NOT HAS_NET_TIMER_EXPIRED(scLBSignInWarn.signedOutTimer,2000,TRUE))
		AND NOT NETWORK_IS_SIGNED_ONLINE()
			SET_BIT(iBS,3)
			scLBSignInWarn.loadingIcon.sMainStringSlot = ""
			REFRESH_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon)
			println("DO_SIGNED_OUT_WARNING: not signed in doing loading icon")
		ELSE
			IF NOT IS_BIT_SET(iBS,3)
				IF NOT IS_BIT_SET(iBS,1)
					DISPLAY_SYSTEM_SIGNIN_UI()
					SET_BIT(iBS,1)
					scLBSignInWarn.loadingIcon.sMainStringSlot = ""
					REFRESH_SCALEFORM_LOADING_ICON(scLBSignInWarn.loadingIcon)
					println("DO_SIGNED_OUT_WARNING: showing sign in warning")
				ELSE
//					IF NOT IS_BIT_SET(iBS,2)
//						REINIT_NET_TIMER(scLBSignInWarn.signInDelay,TRUE)
//						IF NOT IS_SYSTEM_UI_BEING_DISPLAYED()
//							SET_BIT(iBS,2)
//							println("DO_SIGNED_OUT_WARNING: UI no longer being displayed allow timer to progress")
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
		BOOL bDisplaySpinner
		IF HAS_NET_TIMER_STARTED(scLBSignInWarn.signInDelay)
			IF NOT HAS_NET_TIMER_EXPIRED(scLBSignInWarn.signInDelay,4000,TRUE)
				bDisplaySpinner = TRUE
			ENDIF
		ENDIF
		IF NOT bDisplaySpinner
			IF bStoreMessage
				IF NOT NETWORK_IS_SIGNED_ONLINE()
					IF NETWORK_IS_CABLE_CONNECTED()
						SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "STORE_NOT_ONL",iButtonBS)
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_NO_INT",iButtonBS)
					ENDIF
					println("DO_SIGNED_OUT_WARNING: store warning not signed in online")
					IF NOT IS_SYSTEM_UI_BEING_DISPLAYED()
						IF NOT IS_BIT_SET(iBS, 0)
							IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
								SET_BIT(iBS, 0)
							ENDIF
						ELSE
							IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
								RESET_NET_TIMER(scLBSignInWarn.signInDelay)
								iBS = 0
								scLBSignInWarn.iFrameLastCalled = 0
								SET_LOADING_ICON_INACTIVE()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(scLBSignInWarn.signInDelay)
					iBS = 0
					scLBSignInWarn.iFrameLastCalled = 0
					SET_LOADING_ICON_INACTIVE()
					RETURN TRUE
				ENDIF	
			ELSE
				IF IS_BIT_SET(iBS,3)
					IF NETWORK_IS_CABLE_CONNECTED()
						SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_SIGN_OUT",iButtonBS)
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_NO_INT",iButtonBS)
					ENDIF
					println("DO_SIGNED_OUT_WARNING: non store warning signed out")
					IF NOT IS_BIT_SET(iBS, 0)
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
							SET_BIT(iBS, 0)
						ENDIF
					ELSE
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
							RESET_NET_TIMER(scLBSignInWarn.signedOutTimer)
							RESET_NET_TIMER(scLBSignInWarn.signInDelay)
							iBS = 0
							scLBSignInWarn.iFrameLastCalled = 0
							SET_LOADING_ICON_INACTIVE()
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_IS_CABLE_CONNECTED()
						SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_NOT_ONL",iButtonBS)
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "SCLB_NO_INT",iButtonBS)
					ENDIF
					println("DO_SIGNED_OUT_WARNING: non store warning not online")
					IF NOT IS_SYSTEM_UI_BEING_DISPLAYED()
						IF NOT IS_BIT_SET(iBS, 0)
							IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
								SET_BIT(iBS, 0)
							ENDIF
						ELSE
							IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
								RESET_NET_TIMER(scLBSignInWarn.signedOutTimer)
								RESET_NET_TIMER(scLBSignInWarn.signInDelay)
								iBS = 0
								scLBSignInWarn.iFrameLastCalled = 0
								SET_LOADING_ICON_INACTIVE()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		//ELSE
			
			//display spinner
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC INIT_RANK_PREDICTION_COMMON_VALUES()
	sclb_rank_predict.readResult.m_Id = sclb_rank_predict.currentResult.m_Id
	sclb_rank_predict.readResult.m_NumColumns = sclb_rank_predict.currentResult.m_NumColumns
	sclb_rank_predict.readResult.m_ColumnsBitSets = sclb_rank_predict.currentResult.m_ColumnsBitSets
	sclb_rank_predict.combinedResult.m_Id = sclb_rank_predict.currentResult.m_Id
	sclb_rank_predict.combinedResult.m_NumColumns = sclb_rank_predict.currentResult.m_NumColumns
	sclb_rank_predict.combinedResult.m_ColumnsBitSets = sclb_rank_predict.currentResult.m_ColumnsBitSets
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_RANK_PREDICTION_STRUCT(LeaderboardPredictionRow &Row)
	INT i
	println("Leaderboard ID: ",Row.m_Id)
	println("Num columns: ",Row.m_NumColumns)
	REPEAT 32 i
		println("Row.m_ColumnsBitSets BIT[",i,"] = ", IS_BIT_SET(Row.m_ColumnsBitSets,i))
	ENDREPEAT
	REPEAT MAX_COLUMNS i
		println("Row.m_fColumnData[",i,"] = ", Row.m_fColumnData[i])
		println("Row.m_iColumnData[",i,"] = ", Row.m_iColumnData[i])
	ENDREPEAT
	println("--END PRINT_RANK_PREDICTION_STRUCT---")
ENDPROC
#ENDIF

PROC FIX_PREDICTED_RATIO_VALUES_AS_NEEDED(INT iLeaderboardID,LeaderboardPredictionRow &combinedResult)
	FLOAT fDivisor
	//Special Cases
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_DARTS)
		IF combinedResult.m_fColumnData[0] >= HIGHEST_INT
			fDivisor = TO_FLOAT(combinedResult.m_iColumnData[5])
			IF fDivisor <= 0
				fDivisor = 1
			ENDIF
			combinedResult.m_fColumnData[0] = TO_FLOAT(combinedResult.m_iColumnData[7])/fDivisor
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RANK_PREDICTION_SKIP_MERGE_IS_NO_PREVIOUS_DATA(INT iLeaderboardID)
	SWITCH INT_TO_ENUM(LEADERBOARDS_ENUM, iLeaderboardID)
		CASE LEADERBOARD_MINI_GAMES_RACES
		CASE LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME
		CASE LEADERBOARD_FREEMODE_GTA_RACES
		CASE LEADERBOARD_FREEMODE_GTA_RACES_LAPS
		CASE LEADERBOARD_FREEMODE_RACES
		CASE LEADERBOARD_FREEMODE_RACES_LAPS
		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_TIME
		CASE LEADERBOARD_FREEMODE_RALLY
		CASE LEADERBOARD_FREEMODE_RALLY_LAPS
		CASE LEADERBOARD_FREEMODE_ON_FOOT_RACE
		CASE LEADERBOARD_FREEMODE_ON_FOOT_RACE_LAPS
		CASE LEADERBOARD_FREEMODE_BASEJUMPS
		CASE LEADERBOARD_MINI_GAMES_TRIATHLON
		CASE LEADERBOARD_MINI_GAMES_HUNTING
		CASE LEADERBOARD_MINI_GAMES_GOLF
		CASE LEADERBOARD_FREEMODE_NON_CONTACT_RACES
		CASE LEADERBOARD_FREEMODE_NON_CONTACT_RACES_OVERALL
		CASE LEADERBOARD_FLIGHT_SCHOOL_CHASE_PARACHUTE
		CASE LEADERBOARD_FLIGHT_SCHOOL_CITY_LANDING
		CASE LEADERBOARD_FLIGHT_SCHOOL_COLLECT_FLAGS
		CASE LEADERBOARD_FLIGHT_SCHOOL_ENGINE_FAILURE
		CASE LEADERBOARD_FLIGHT_SCHOOL_FOLLOW_LEADER
		CASE LEADERBOARD_FLIGHT_SCHOOL_FORMATION_FLIGHT
		CASE LEADERBOARD_FLIGHT_SCHOOL_GROUND_LEVEL
		CASE LEADERBOARD_FLIGHT_SCHOOL_MOVING_LANDING
		CASE LEADERBOARD_FLIGHT_SCHOOL_OUTSIDE_LOOP
		CASE LEADERBOARD_FLIGHT_SCHOOL_SHOOTING_RANGE
			RETURN TRUE
		BREAK
	ENDSWITCH
	IF iLeaderboardID >= ENUM_TO_INT(LEADERBOARD_FREEMODE_NON_CONTACT_RACES_1_LAPS)
	AND iLeaderboardID <= ENUM_TO_INT(LEADERBOARD_FREEMODE_ELO_NON_CONTACT_RACES_OVERALL_SEASON50)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RANK_PREDICTION_SKIP_READ_COLUMN(INT iLeaderboardID, INT iCol)
	IF LEADERBOARDS_GET_COLUMN_ID(iLeaderboardID,LEADERBOARD2_TYPE_GROUP,iCol) = ENUM_TO_INT(LB_INPUT_COL_MATCH_ID)
		RETURN TRUE
	ENDIF
	SWITCH INT_TO_ENUM(LEADERBOARDS_ENUM, iLeaderboardID)
		CASE LEADERBOARD_MINI_GAMES_MP_SRANGE
			IF iCol = 5
				RETURN TRUE
			ENDIF
		BREAK
		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE
			IF iCol = 4
				RETURN TRUE
			ENDIF
		BREAK
		CASE LEADERBOARD_FREEMODE_HEIST_MISSION
			IF iCol = 5
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_RANK_PREDICTION_DETAILS(SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	//Leaderboard2ReadData lbReadData
	//Leaderboard2ReadData emptyData
	//LeaderboardRowData tempRowData[1]
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	LeaderboardPredictionRow emptyRow
	
	GAMER_HANDLE gamerHandle
	IF IS_SC_LEADERBOARD_A_RALLY(scLB_control.ReadDataStruct.m_LeaderboardId)
		gamerHandle = sclb_rank_predict.driverHandle
	ELSE
		gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	ENDIF
	
	INT i
	println("sclb_rank_predict.currentResult.m_Id = ", sclb_rank_predict.currentResult.m_Id)
	SWITCH sclb_rank_predict.iReadStage
		CASE 0
			IF START_SC_LEADERBOARD_READ_BY_HANDLE(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct,gamerHandle)
				println("GET_RANK_PREDICTION_DETAILS: Called by ", GET_THIS_SCRIPT_NAME())
//				#IF IS_DEBUG_BUILD
//				println("GET_RANK_PREDICTION_DETAILS:  LEADERBOARD ID = ", scLB_control.ReadDataStruct.m_LeaderboardId) 
//				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
				sclb_rank_predict.currentResult.m_Id = scLB_control.ReadDataStruct.m_LeaderboardId
				PRINTLN("GET_RANK_PREDICTION_DETAILS: sclb_rank_predict.currentResult.m_Id = ",sclb_rank_predict.currentResult.m_Id)
				sclb_rank_predict.currentResult.m_NumColumns = LEADERBOARDS_GET_NUMBER_OF_COLUMNS(scLB_control.ReadDataStruct.m_LeaderboardId,LEADERBOARD_TYPE_GROUP)
				REPEAT sclb_rank_predict.currentResult.m_NumColumns i
					IF NOT SHOULD_RANK_PREDICTION_SKIP_READ_COLUMN(scLB_control.ReadDataStruct.m_LeaderboardId,i)
						IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,LEADERBOARD2_TYPE_GROUP,i)
							SET_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,i)
						ELSE
							CLEAR_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,i)
						ENDIF
					ENDIF
				ENDREPEAT
				PRINTLN("GET_RANK_PREDICTION_DETAILS: m_ColumnsBitSets = ",sclb_rank_predict.currentResult.m_ColumnsBitSets)
				IF scLB_control.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						println("GET_RANK_PREDICTION_DETAILS: ",sclb_rank_predict.currentResult.m_NumColumns)
						//IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
						IF initRowData.m_NumColumnValues != sclb_rank_predict.currentResult.m_NumColumns 
							#IF IS_DEBUG_BUILD
							println("GET_RANK_PREDICTION_DETAILS: sclb_rank_predict.currentResult.m_NumColumns  = ",sclb_rank_predict.currentResult.m_NumColumns," initRowData.m_NumColumnValues  = ",initRowData.m_NumColumnValues)
							//SCRIPT_ASSERT("GET_RANK_PREDICTION_DETAILS: columns returned != num columns setup")
							#ENDIF
						ENDIF
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							scLB_rank_predict.bNoPreviousValue = TRUE
							println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 no data for local player ") 
						ELSE
							REPEAT sclb_rank_predict.currentResult.m_NumColumns i
								IF NOT SHOULD_RANK_PREDICTION_SKIP_READ_COLUMN(scLB_control.ReadDataStruct.m_LeaderboardId,i)
									println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", 0, " Column: ",i)
			      					IF IS_LEADERBOARD_COLUMN_INT(scLB_control.ReadDataStruct.m_LeaderboardId,LEADERBOARD2_TYPE_GROUP,i)
										scLB_rank_predict.readResult.m_iColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, i)
										IF scLB_rank_predict.readResult.m_iColumnData[i] = -1
											IF i > initRowData.m_NumColumnValues
												println("GET_RANK_PREDICTION_DETAILS: hit failsafe setting column (INT): ",i,"to 0")
												scLB_rank_predict.readResult.m_iColumnData[i] = 0
											ENDIF
										ENDIF
										#IF IS_DEBUG_BUILD
										DEBUG_PRINT_SC_LB_RETURNED_VALUES("GET_RANK_PREDICTION_DETAILS","Current Run",0,i,
																			scLB_rank_predict.readResult.m_iColumnData[i],0.0,FALSE,TRUE )
										#ENDIF
										
									ELSE
										scLB_rank_predict.readResult.m_fColumnData[i] = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(0, i)
										IF scLB_rank_predict.readResult.m_fColumnData[i] = -1.0
											IF i > initRowData.m_NumColumnValues
												println("GET_RANK_PREDICTION_DETAILS: hit failsafe setting column (FLOAT): ",i,"to 0")
												scLB_rank_predict.readResult.m_fColumnData[i] = 0
											ENDIF
										ENDIF
										#IF IS_DEBUG_BUILD
										DEBUG_PRINT_SC_LB_RETURNED_VALUES("GET_RANK_PREDICTION_DETAILS","Current Run",0,i,
																			0,scLB_rank_predict.readResult.m_fColumnData[i],TRUE,TRUE )
										#ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 got data for local player ") 
						ENDIF
					ELSE
						scLB_rank_predict.bNoPreviousValue = TRUE
						println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 no data for local player ") 
					ENDIF
					
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					scLB_rank_predict.bNoPreviousValue = TRUE
					println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 no data for local player ") 
				ENDIF
				sclb_rank_predict.iReadStage = 1
				println("Ending read in READ_BY_HANDLE in  ", GET_THIS_SCRIPT_NAME() ) 
				END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
			ENDIF
		BREAK
		
		CASE 1
			//Wait for the rank prediction write
			scLB_rank_predict.bFinishedRead = TRUE
			IF scLB_rank_predict.bFinishedWrite
				INIT_RANK_PREDICTION_COMMON_VALUES()
				
				#IF IS_DEBUG_BUILD
				println("GET_RANK_PREDICTION_DETAILS finished write for current run ", GET_THIS_SCRIPT_NAME() ) 
				println("READ RESULT" ) 
				PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.readResult)
				println("CURRENT RUN" ) 
				PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.currentResult)
				#ENDIF
				IF scLB_rank_predict.bNoPreviousValue
					IF SHOULD_RANK_PREDICTION_SKIP_MERGE_IS_NO_PREVIOUS_DATA(scLB_control.ReadDataStruct.m_LeaderboardId)
						sclb_rank_predict.iReadStage = 3
						println("GET_RANK_PREDICTION_DETAILS -- moving to stage 3 no previous run data SHOULD_RANK_PREDICTION_SKIP_MERGE_IS_NO_PREVIOUS_DATA = TRUE") 
					ELSE
						sclb_rank_predict.iReadStage = 2
						println("GET_RANK_PREDICTION_DETAILS -- moving to stage 2 no previous run data ") 
					ENDIF
				ELSE
					sclb_rank_predict.iReadStage = 2
					println("GET_RANK_PREDICTION_DETAILS -- moving to stage 2  previous run data ") 
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			LEADERBOARDS2_READ_RANK_PREDICTION(scLB_rank_predict.readResult,scLB_rank_predict.currentResult,scLB_rank_predict.combinedResult)
			//println("GET_RANK_PREDICTION_DETAILS -- combined the result moving to stage 4") 
			
			//FIX_PREDICTED_RATIO_VALUES_AS_NEEDED(sclb_rank_predict.currentResult.m_Id,scLB_rank_predict.readResult)
			sclb_rank_predict.iReadStage = 4
			#IF IS_DEBUG_BUILD
			println("GET_RANK_PREDICTION_DETAILS COMBINED values " ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.combinedResult)
			#ENDIF
			scLB_control.iTempLoadStage = 0
			scLB_control.bTempReadResult = FALSE
		BREAK
		
		CASE 3
			scLB_rank_predict.combinedResult = scLB_rank_predict.currentResult
			//println("GET_RANK_PREDICTION_DETAILS -- combined the result moving to stage 4 (NO PREVIOUS DATA!)") 
			sclb_rank_predict.iReadStage = 4
			#IF IS_DEBUG_BUILD
			println("GET_RANK_PREDICTION_DETAILS COMBINED values (NO PREVIOUS DATA!)" ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.combinedResult)
			#ENDIF
			scLB_control.iTempLoadStage = 0
			scLB_control.bTempReadResult = FALSE
		BREAK
		
		CASE 4
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND HAS_UGC_GLOBAL_BLOCK_LOADED()
				IF IS_PLAYLIST_SETTING_CHALLENGE_TIME()
					sclb_rank_predict.iReadStage = 99
					println("GET_RANK_PREDICTION_DETAILS: IS_PLAYLIST_SETTING_CHALLENGE_TIME: retrieving LB data" ) 
				ELSE
					println("GET_RANK_PREDICTION_DETAILS: NOT IS_PLAYLIST_SETTING_CHALLENGE_TIME() proceeding normally" ) 
					sclb_rank_predict.iReadStage = 999
					RETURN TRUE
				ENDIF
			ELSE
				println("GET_RANK_PREDICTION_DETAILS: NOT NETWORK_IS_GAME_IN_PROGRESS() proceeding normally" ) 
				sclb_rank_predict.iReadStage = 999
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 99
			IF SHOULD_RANK_PREDICTION_SKIP_MERGE_IS_NO_PREVIOUS_DATA(scLB_control.ReadDataStruct.m_LeaderboardId)
				scLB_rank_predict.DifficultyCombinedResult = scLB_rank_predict.currentResult
				println("GET_RANK_PREDICTION_DETAILS -- combined the result moving to stage 4 (NOT COMBINING DATA!)") 
			ELSE
				emptyRow.m_Id = scLB_rank_predict.currentResult.m_Id
				emptyRow.m_NumColumns = scLB_rank_predict.currentResult.m_NumColumns
				emptyRow.m_ColumnsBitSets = scLB_rank_predict.currentResult.m_ColumnsBitSets
				LEADERBOARDS2_READ_RANK_PREDICTION(emptyRow,scLB_rank_predict.currentResult,scLB_rank_predict.DifficultyCombinedResult)
				println("GET_RANK_PREDICTION_DETAILS -- combined the result moving to stage 100") 
			ENDIF	
			sclb_rank_predict.iReadStage = 100
			#IF IS_DEBUG_BUILD
			println("GET_RANK_PREDICTION_DETAILS COMBINED values " ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.combinedResult)
			#ENDIF
			scLB_control.iTempLoadStage = 0
			scLB_control.bTempReadResult = FALSE
		BREAK

		CASE 100
			IF START_SC_LEADERBOARD_READ_BY_RADIUS(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct,1,gamerHandle,TRUE,
													TRUE,scLB_rank_predict.DifficultyCombinedResult.m_iColumnData[0],scLB_rank_predict.DifficultyCombinedResult.m_fColumnData[0])
				println("GET_RANK_PREDICTION_DETAILS: Called by ", GET_THIS_SCRIPT_NAME())
				#IF IS_DEBUG_BUILD
				println("GET_RANK_PREDICTION_DETAILS:  LEADERBOARD ID = ", scLB_control.ReadDataStruct.m_LeaderboardId) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,scLB_control.ReadDataStruct)
				IF scLB_control.bTempReadResult
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
							g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = -1
							println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 no data for local player g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = ",g_sCHALLENGE_DIFFICULTY.iMyRankPrediction) 
						ELSE
							g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = initRowData.m_Rank 
							println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 got data for local player g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = ",g_sCHALLENGE_DIFFICULTY.iMyRankPrediction) 
						ENDIF
					ELSE
						g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = -1
						println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 no data for local player g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = ",g_sCHALLENGE_DIFFICULTY.iMyRankPrediction) 
					ENDIF
					
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = -1
					println("GET_RANK_PREDICTION_DETAILS -- moving to stage 1 no data for local player g_sCHALLENGE_DIFFICULTY.iMyRankPrediction = ",g_sCHALLENGE_DIFFICULTY.iMyRankPrediction) 
				ENDIF
				sclb_rank_predict.iReadStage = 999
				println("Ending read in READ_BY_RADIUS in  ", GET_THIS_SCRIPT_NAME() ) 
				END_LEADERBOARD_READ(scLB_control.iTempLoadStage,scLB_control.bTempReadResult,scLB_control.ReadDataStruct)
			ENDIF
		BREAK
		
		CASE 999
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STORED_ELO_ENUM GET_STORED_ELO_ENUM_FOR_LEADERBOARD(LEADERBOARDS_ENUM leaderboard)
	SWITCH  leaderboard
		CASE LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL			RETURN	SCLB_ELO_DM
		CASE LEADERBOARD_FREEMODE_ELO_TEAM_DEATHMATCH_OVERALL		RETURN	SCLB_ELO_TEAM_DM
		CASE LEADERBOARD_FREEMODE_ELO_VEHICLE_DEATHMATCH_OVERALL	RETURN	SCLB_ELO_VEH_DM
		CASE LEADERBOARD_FREEMODE_ELO_BASEJUMPS_OVERALL				RETURN	SCLB_ELO_BASEJ
		CASE LEADERBOARD_FREEMODE_ELO_RACES_OVERALL					RETURN	SCLB_ELO_RACE
		CASE LEADERBOARD_FREEMODE_ELO_GTA_RACES_OVERALL				RETURN	SCLB_ELO_GTA_RACE
		CASE LEADERBOARD_FREEMODE_ELO_RALLY_OVERALL					RETURN	SCLB_ELO_RALLY
		CASE LEADERBOARD_FREEMODE_ELO_RALLY_CODRIVER_OVERALL		RETURN	SCLB_ELO_RALLY_CODRIVER
		CASE LEADERBOARD_FREEMODE_ELO_ON_FOOT_RACE_OVERALL			RETURN	SCLB_ELO_ONFOOT_RACE
	ENDSWITCH
	IF ENUM_TO_INT(leaderboard) >= ENUM_TO_INT(LEADERBOARD_FREEMODE_ELO_NON_CONTACT_RACES_OVERALL_SEASON1)
	AND ENUM_TO_INT(leaderboard) <= ENUM_TO_INT(LEADERBOARD_FREEMODE_ELO_NON_CONTACT_RACES_OVERALL_SEASON50)
		RETURN	SCLB_ELO_NONCONTACT_RACE
	ENDIF		
	RETURN SCLB_ELO_INVALID
ENDFUNC

PROC CLEAR_GET_PLAYER_ELO_FOR_MODE_FLAGS(INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful)
	iReadStage = 0
	iLoadStage = 0
	bSuccessful = FALSE
ENDPROC

FUNC BOOL UPDATE_STORED_ELO(LEADERBOARDS_ENUM leaderboard,INT &iELOUpdate)
	STORED_ELO_ENUM eloEnum
	eloEnum = GET_STORED_ELO_ENUM_FOR_LEADERBOARD(leaderboard)
	IF NOT storedELOData.bReadFinished[eloEnum]
		#IF IS_DEBUG_BUILD
		println("UPDATE_STORED_ELO: ELO READ NOT FINSIHED!(",iELOUpdate,") for board ",leaderboard)
		SCRIPT_ASSERT("UPDATE_STORED_ELO: player has attempted to update elo but read not finished. Not storing!")
		#ENDIF
		RETURN FALSE
	ELIF storedELOData.bReadFailed[eloEnum]
		#IF IS_DEBUG_BUILD
		println("UPDATE_STORED_ELO: ELO READ FAILED!(",iELOUpdate,") for board ",leaderboard)
		SCRIPT_ASSERT("UPDATE_STORED_ELO: player has attempted to update elo but read failed. Not storing!")
		#ENDIF
		RETURN FALSE
	ELIF storedELOData.iLastReadSeason[eloEnum] != g_sMPTunables.iEloSeason 
		#IF IS_DEBUG_BUILD
		println("UPDATE_STORED_ELO: for board ",leaderboard,"storedELOData.iLastReadSeason[",eloEnum,"] = ",storedELOData.iLastReadSeason[eloEnum]," and g_sMPTunables.iEloSeason = ",g_sMPTunables.iEloSeason)
		SCRIPT_ASSERT("UPDATE_STORED_ELO: player has attempted to update elo but season has changed. Not storing!")
		#ENDIF
		RETURN FALSE
//		ELIF storedELOData.bIsELO_ActuallyZero[eloEnum] = TRUE
//			println("UPDATE_STORED_ELO: ELO WAS ZERO BEFORE UPDATE")
//			println("UPDATE_STORED_ELO: ELO WAS UPDATED BY MORE THAN 1000!! (",iLocalUpdate,") for board ",leaderboard)
//			storedELOData.iELO[eloEnum] = iLocalUpdate
//			RETURN TRUE
	ELSE
		storedELOData.iELO[eloEnum] += iELOUpdate
		println("UPDATE_STORED_ELO: LB ",leaderboard," stored ELO now = ",storedELOData.iELO[eloEnum])
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PLAYER_ELO_FOR_MODE(LEADERBOARDS_ENUM leaderboard, INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful, INT &iCurrentELO)
	STORED_ELO_ENUM eloEnum
	eloEnum = GET_STORED_ELO_ENUM_FOR_LEADERBOARD(leaderboard)
	//BOOL bNoData
	IF eloEnum = SCLB_ELO_INVALID
		#IF IS_DEBUG_BUILD
			println("GET_PLAYER_ELO_FOR_MODE: trying to get elo for invalid leaderboard! Leaderboard = ", leaderboard)
			SCRIPT_ASSERT("GET_PLAYER_ELO_FOR_MODE: trying to get elo for invalid leaderboard! See Conor!")
		#ENDIF
	ELSE
		IF NOT IS_THIS_A_RSTAR_ACTIVITY()
			storedELOData.bReadFailed[eloEnum] = TRUE
			storedELOData.bReadFinished[eloEnum] = FALSE
			println("GET_PLAYER_ELO_FOR_MODE: NOT IS_THIS_A_RSTAR_ACTIVITY EXITING!")
			RETURN TRUE
		ENDIF
		IF NOT storedELOData.bReadFailed[eloEnum] AND storedELOData.bReadFinished[eloEnum]
		AND storedELOData.iLastReadSeason[eloEnum] = g_sMPTunables.iEloSeason 
			iCurrentELO = storedELOData.iELO[eloEnum]
			println("GET_PLAYER_ELO_FOR_MODE: player has stored ELO value for leaderboard #",ENUM_TO_INT(leaderboard)," returning that value: ",iCurrentELO)
			RETURN TRUE
		ELSE
			Leaderboard2ReadData lbReadData
			Leaderboard2ReadData emptyData
			LeaderboardRowData initRowData
			LeaderboardReadInfo readInfo
			GAMER_HANDLE gamerHandle
			gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			lbReadData.m_LeaderboardId = ENUM_TO_INT(leaderboard)
			
			IF lbReadData.m_LeaderboardId >= ENUM_TO_INT(LEADERBOARD_FREEMODE_ELO_NON_CONTACT_RACES_OVERALL_SEASON1)
			AND lbReadData.m_LeaderboardId <= ENUM_TO_INT(LEADERBOARD_FREEMODE_ELO_NON_CONTACT_RACES_OVERALL_SEASON50)
				lbReadData.m_Type = LEADERBOARD2_TYPE_PLAYER
				lbReadData.m_GroupSelector.m_NumGroups = 0
			ELSE
				lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP_MEMBER
				lbReadData.m_GroupSelector.m_NumGroups = 1
				lbReadData.m_GroupSelector.m_Group[0].m_Category = "SeasonId"
				lbReadData.m_GroupSelector.m_Group[0].m_Id = ""
				lbReadData.m_GroupSelector.m_Group[0].m_Id += g_sMPTunables.iEloSeason
			ENDIF
			
			SWITCH iReadStage
				CASE 0
					println("GET_PLAYER_ELO_FOR_MODE: getting ELO for leaderboardd #",ENUM_TO_INT(leaderboard)," season: ",lbReadData.m_GroupSelector.m_Group[0].m_Id)
					IF START_SC_LEADERBOARD_READ_BY_HANDLE(iLoadStage,bSuccessful,lbReadData,gamerHandle)
						#IF IS_DEBUG_BUILD
						println("GET_PLAYER_ELO_FOR_MODE:  LEADERBOARD ID = ", lbReadData.m_LeaderboardId," groupHandle = ",lbReadData.m_GroupSelector.m_Group[0].m_Id)
						#ENDIF
						FILL_READ_INFO_STRUCT(readInfo,lbReadData)
						IF bSuccessful
						AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
							IF readInfo.m_ReturnedRows > 0
								LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
								IF ARE_STRINGS_EQUAL(initRowData.m_GamerName, "")
									println("GET_PLAYER_ELO_FOR_MODE:-- moving to stage 1 no data for local player -A ") 
									iReadStage = 1
									iCurrentELO = 0
								ELSE
									iCurrentELO = LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0)
									println("GET_PLAYER_ELO_FOR_MODE: player ", initRowData.m_GamerName," ELO = ",iCurrentELO)
									iReadStage = 1
								ENDIF
							ELSE
								println("GET_PLAYER_ELO_FOR_MODE: -- moving to stage 1 no data for local player  - B") 
								iReadStage = 1
								iCurrentELO = 0
							ENDIF
							storedELOData.iELO[eloEnum] = iCurrentELO
							storedELOData.iLastReadSeason[eloEnum] = g_sMPTunables.iEloSeason 
							storedELOData.bReadFailed[eloEnum] = FALSE
							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
						ELSE
							println("GET_PLAYER_ELO_FOR_MODE: -- moving to stage 1 no data for local player (READ FAILED) ") 
							iReadStage = 1
							//IF storedELOData.iELO[eloEnum] = 0
							storedELOData.bReadFailed[eloEnum] = TRUE
						ENDIF
						storedELOData.bReadFinished[eloEnum] = TRUE
						println("Ending read in READ_BY_HANDLE in  ", GET_THIS_SCRIPT_NAME() ) 
						END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
						lbReadData = emptyData
					ENDIF
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    To get the global best for a flight school lesson
/// PARAMS:
///    iReadStage - this is just needs to be an int defined in the script that will retain its value over more than a frame (i.e. dont just define it before calling this)
///    iLoadStage - this is just needs to be an int defined in the script that will retain its value over more than a frame (i.e. dont just define it before calling this)
///    bSuccessful - this is just needs to be an bool defined in the script that will retain its value over more than a frame (i.e. dont just define it before calling this)
///    iLesson - the number indicating the lession IS
///    fGlobalBest - when function returns true this will be the global best
///    bGotResultFromLB - true if read was successful. Read may not be successful
/// RETURNS:
///    True when it's finished. Note this should always return true even if it finds no records and the values remain 0.
FUNC BOOL GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST(INT& iReadStage, INT& iLoadStage,BOOL& bSuccessful, INT iLesson,FLOAT& fGlobalBest, BOOL &bGotResultFromLB)
	
	Leaderboard2ReadData lbReadData
	Leaderboard2ReadData emptyData
	//LeaderboardRowData tempRowData[1]
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	BOOL bInt = FALSE
	SWITCH INT_TO_ENUM(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,iLesson)
		CASE PSCD_DLC_OutsideLoop
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_OUTSIDE_LOOP) 
		BREAK
		CASE PSCD_DLC_FollowLeader
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FOLLOW_LEADER) 
		BREAK
		CASE PSCD_DLC_VehicleLanding
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_MOVING_LANDING) 
		BREAK
		CASE PSCD_DLC_CollectFlags
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_COLLECT_FLAGS) 
		BREAK
		CASE PSCD_DLC_Engine_failure
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_ENGINE_FAILURE) 
		BREAK
		CASE PSCD_DLC_ChaseParachute
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CHASE_PARACHUTE) 
		BREAK
		CASE PSCD_DLC_CityLanding
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CITY_LANDING) 
		BREAK
		CASE PSCD_DLC_FlyLow
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_GROUND_LEVEL) 
		BREAK
		CASE PSCD_DLC_ShootingRange
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_SHOOTING_RANGE)
		BREAK
		CASE PSCD_DLC_Formation
			lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FORMATION_FLIGHT) 
		BREAK
		DEFAULT
			PRINTLN("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST- passed in invalid lesson ID! ID: ",iLesson)
			SCRIPT_ASSERT("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST- passed in invalid lesson ID!")
		BREAK
	ENDSWITCH
	lbReadData.m_Type = LEADERBOARD2_TYPE_PLAYER
	lbReadData.m_GroupSelector.m_NumGroups = 0
	lbReadData.m_GroupSelector.m_Group[0].m_Category = ""
	lbReadData.m_GroupSelector.m_Group[0].m_Id = ""
	
	SWITCH iReadStage
		CASE 0
			IF START_SC_LEADERBOARD_READ_BY_RANK(iLoadStage,bSuccessful,lbReadData,1,1)
				#IF IS_DEBUG_BUILD
				println("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST:  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
				IF bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
						IF NOT IS_GAMER_HANDLE_VALID(initRowData.m_GamerHandle)
							println("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST -- no global data C") 
						ELSE
							IF bInt
								fGlobalBest = TO_FLOAT(LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0))
							ELSE
								fGlobalBest = LEADERBOARDS2_READ_GET_ROW_DATA_FLOAT(0, 0)
							ENDIF
							println("Global best is for player name: ", initRowData.m_GamerName, " is - ",fGlobalBest)
						ENDIF
					ELSE
						println("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST -- no global data A ") 
					ENDIF
					LEADERBOARDS2_READ_GET_ROW_DATA_END()
				ELSE
					println("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST -- no global data B ") 
				ENDIF
				bGotResultFromLB = bSuccessful
				iReadStage = 2
				END_LEADERBOARD_READ(iLoadStage,bSuccessful,lbReadData)
				lbReadData = emptyData
			ENDIF
		BREAK
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CLEANUP_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST(INT iLesson,INT& iReadStage, INT& iLoadStage, BOOL& bResult)
	IF iLoadStage != 0
		Leaderboard2ReadData lbReadData
		SWITCH INT_TO_ENUM(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,iLesson)
			CASE PSCD_DLC_OutsideLoop
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_OUTSIDE_LOOP) 
			BREAK
			CASE PSCD_DLC_FollowLeader
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FOLLOW_LEADER) 
			BREAK
			CASE PSCD_DLC_VehicleLanding
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_MOVING_LANDING) 
			BREAK
			CASE PSCD_DLC_CollectFlags
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_COLLECT_FLAGS) 
			BREAK
			CASE PSCD_DLC_Engine_failure
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_ENGINE_FAILURE) 
			BREAK
			CASE PSCD_DLC_ChaseParachute
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CHASE_PARACHUTE) 
			BREAK
			CASE PSCD_DLC_CityLanding
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CITY_LANDING) 
			BREAK
			CASE PSCD_DLC_FlyLow
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_GROUND_LEVEL) 
			BREAK
			CASE PSCD_DLC_ShootingRange
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_SHOOTING_RANGE)
			BREAK
			CASE PSCD_DLC_Formation
				lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FORMATION_FLIGHT) 
			BREAK
			DEFAULT
				PRINTLN("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST- passed in invalid lesson ID! ID: ",iLesson)
				SCRIPT_ASSERT("GET_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST- passed in invalid lesson ID!")
			BREAK
		ENDSWITCH
		lbReadData.m_Type = LEADERBOARD2_TYPE_PLAYER
		lbReadData.m_GroupSelector.m_NumGroups = 0
		lbReadData.m_GroupSelector.m_Group[0].m_Category = ""
		lbReadData.m_GroupSelector.m_Group[0].m_Id = ""
		println("Ending read in CLEANUP_RACE_BEST_LAP_TIMES in  ", GET_THIS_SCRIPT_NAME() ) 
		END_LEADERBOARD_READ(iLoadStage,bResult, lbReadData)
	ENDIF
	iReadStage = 0
	iLoadStage = 0
	bResult = FALSE
	PRINTLN("CLEANUP_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST- Called")
ENDPROC

PROC CLEAR_LIMITED_EDITION_ITEM_LB_DETAILS(LIMITED_EDITION_ITEM_LB_DETAILS &itemDetails)
	INT i
	itemDetails.iTotalStage = 0
	itemDetails.iReadStage = 0
	itemDetails.bSuccessful = FALSE
	REPEAT MAX_LIMITED_EDITION_ITEMS i
		itemDetails.iID[i] = 0
		itemDetails.iStockSold[i] = 0
		itemDetails.iTotalStock[i] = 0
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_LIMITED_EDITION_INFORMATION(LIMITED_EDITION_ITEM_LB_DETAILS &itemDetails)

	println("READ- ",itemDetails.bSuccessful)
	INT i
	REPEAT MAX_LIMITED_EDITION_ITEMS i
		println("itemDetails.iID[",i,"] = ",itemDetails.iID[i], "iStockSold = ",itemDetails.iStockSold[i], "iTotalStock = ",itemDetails.iTotalStock[i])
	ENDREPEAT
	println("----------------END DEBUG_PRINT_LIMITED_EDITION_INFORMATION------------------------")
ENDPROC
#ENDIF

FUNC INT GET_LIMITED_EDITION_ITEM_INDEX(TEXT_LABEL_31 &tl_31)
	TEXT_LABEL_31 tempLabel
	INT i = 0
	REPEAT MAX_LIMITED_EDITION_ITEMS i
		tempLabel = ""
		tempLabel += g_sMPTunables.ilimitededitionitemid[i] //i
		println("GET_LIMITED_EDITION_ITEM_INDEX: tl_31:",tl_31, " tempLabel: ",tempLabel)
		IF ARE_STRINGS_EQUAL(tl_31,tempLabel)
			println("GET_LIMITED_EDITION_ITEM_INDEX: ",tl_31, " at Index = ",i)
			RETURN i
		ENDIF
		
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	println("GET_LIMITED_EDITION_ITEM_INDEX: trying to get index of invalid item! Item ID = ",tl_31)
	#ENDIF
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns the details about the limited edition items
/// PARAMS:
///    itemDetails - all the details you need as well as control for the leaderboard read
/// RETURNS:
///    TRUE when finished (this will always return true even if LB read fails
FUNC BOOL GET_LIMITED_EDITION_ITEMS(LIMITED_EDITION_ITEM_LB_DETAILS &itemDetails)
	TEXT_LABEL_31 categoryNames[MAX_LIMITED_EDITION_ITEMS]
	TEXT_LABEL_23 uniqueIdentifiers[MAX_LIMITED_EDITION_ITEMS]
	
	INT iCategoriesToCheck
	INT i
	REPEAT MAX_LIMITED_EDITION_ITEMS i
		itemDetails.iID[i] = g_sMPTunables.ilimitededitionitemid[i] //i
		itemDetails.iStockSold[i] = 0
		itemDetails.iTotalStock[i] = ROUND(g_sMPTunables.flimitededitiontunableamount[i]) //50
		IF itemDetails.iTotalStock[i] != -1
		AND g_sMPTunables.ilimitededitionitemid[i] != 0
			categoryNames[iCategoriesToCheck] = "ItemId"
			uniqueIdentifiers[iCategoriesToCheck] = ""
			uniqueIdentifiers[iCategoriesToCheck] += g_sMPTunables.ilimitededitionitemid[i] //i
			iCategoriesToCheck++
		ENDIF
	ENDREPEAT
	
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	
	Leaderboard2ReadData lbReadData
	
	lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_FREEMODE_LIMITED_EDITION)
	lbReadData.m_Type = LEADERBOARD2_TYPE_GROUP
	lbReadData.m_GroupSelector.m_NumGroups = 1
	lbReadData.m_GroupSelector.m_Group[0].m_Category = ""
	lbReadData.m_GroupSelector.m_Group[0].m_Id = ""
	i = 0
	GAMER_HANDLE noGamer[1]
	LeaderboardClanIds clanIDs[1]
	Leaderboard2GroupHandle groupHandle[MAX_LIMITED_EDITION_ITEMS]
	REPEAT iCategoriesToCheck i
		groupHandle[i].m_Group[0].m_Category = categoryNames[i]
		groupHandle[i].m_Group[0].m_Id = uniqueIdentifiers[i]
		groupHandle[i].m_NumGroups = 1
	ENDREPEAT



	INT iIndex
	SWITCH itemDetails.iTotalStage
		CASE 0
			IF START_SC_LEADERBOARD_READ_BY_ROW(itemDetails.iReadStage,itemDetails.bSuccessful,lbReadData,
								noGamer,0,
								clanIDs,0,
								groupHandle, iCategoriesToCheck)
				//println("GET_LIMITED_EDITION_ITEMS: Called by ", GET_THIS_SCRIPT_NAME())
				#IF IS_DEBUG_BUILD
				println("GET_LIMITED_EDITION_ITEMS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
				#ENDIF
				FILL_READ_INFO_STRUCT(readInfo,lbReadData)
		
				IF itemDetails.bSuccessful
				AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
					IF readInfo.m_ReturnedRows > 0
						i = 0
						iIndex = 0
						REPEAT readInfo.m_ReturnedRows i
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(i, initRowData)
							iIndex = GET_LIMITED_EDITION_ITEM_INDEX(initRowData.m_GroupSelector.m_Group[0].m_Id)
							IF iIndex >= 0
								itemDetails.iStockSold[iIndex] = LEADERBOARDS2_READ_GET_ROW_DATA_INT(i, 0)
								println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", i, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("TEST_LIMITED_SHOP_ITEMS","Current Run",i,0,
																		itemDetails.iStockSold[iIndex],0.0,TRUE )
								#ENDIF
							ENDIF
						ENDREPEAT
					ELSE
						println("GET_LIMITED_EDITION_ITEMS -- moving to stage 1 no data ") 
					ENDIF
					
					LEADERBOARDS2_READ_GET_ROW_DATA_END( )
				ELSE
					println("GET_LIMITED_EDITION_ITEMS -- moving to stage 1 no data ") 
				ENDIF
				itemDetails.iTotalStage = 1
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LIMITED_EDITION_INFORMATION(itemDetails)
				#ENDIF
				END_LEADERBOARD_READ(itemDetails.iReadStage,itemDetails.bSuccessful,lbReadData)
			ENDIF
		BREAK

		CASE 1
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Writes to the leaderboard for limited edition items
/// PARAMS:
///    iID - the id of the items you are purchasing
///    iAmount - the amount of items you are purchasing
PROC WRITE_PURCHASE_OF_LIMITED_EDITION_ITEMS(INT iID, INT iAmount = 1)
	TEXT_LABEL_31 categoryNames[1]
	TEXT_LABEL_23 uniqueIdentifiers[1]
	categoryNames[0] = "ItemId"
	uniqueIdentifiers[0] = ""
	uniqueIdentifiers[0] += iID
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_LIMITED_EDITION,uniqueIdentifiers,categoryNames,1)
		println(" WRITE_PURCHASE_OF_LIMITED_EDITION_ITEMS: purchasing item ID: ", iID, " and Amount: ",iAmount)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iAmount,0)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC TEST_ELO_LEADERBOARDS()
	IF debugELOLeaderboard.bReadData
		IF GET_PLAYER_ELO_FOR_MODE(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL, debugELOLeaderboard.iReadStage, debugELOLeaderboard.iTempLoadStage,debugELOLeaderboard.bTempReadResult, debugELOLeaderboard.iReturnedELO)
			CLEAR_GET_PLAYER_ELO_FOR_MODE_FLAGS(debugELOLeaderboard.iReadStage, debugELOLeaderboard.iTempLoadStage,debugELOLeaderboard.bTempReadResult)
			debugELOLeaderboard.bReadData = FALSE
		ENDIF
	ENDIF
	TEXT_LABEL_31 categoryNames[1]
	TEXT_LABEL_23 uniqueIdentifiers[1]
	INT iELOUpdate = debugELOLeaderboard.iWriteNewEloValue
	IF debugELOLeaderboard.bWriteData
		categoryNames[0] = "SeasonId"
		uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
			IF UPDATE_STORED_ELO(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL,iELOUpdate)
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iELOUpdate,0)
				println("LEADERBOARDS_WRITE_ADD_COLUMN writing ELO update to LB ",iELOUpdate)
			ELSE
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,0,0)
				//println("LEADERBOARDS_WRITE_ADD_COLUMN: UPDATE_STORED_ELO failed not updating LB!")
			ENDIF
				
			// Write kills
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,0,0)
			//println("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",0 )

			// Write deaths			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,0,0)
			//println("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",0)

			// Write wins		
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_WINS,0,0)
			//println("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_WINS ",0)
			
			// Write wins		
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES,0,0)
			//println("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_MATCHES ",0)
			
			// Write wins		
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES_LOST,0,0)
			//println("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_LOSES ",0)
			
			// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_TOTAL_TIME_ELO_RACE,0,0)
			
		ENDIF
		debugELOLeaderboard.bWriteData = FALSE
	ENDIF
ENDPROC


PROC CLEAR_TEST_LIMITED_SHOP_ITEMS_LEADERBOARD_READ_STRUCT()
	debugLimitedShopItems.bReadLeaderboardValues = FALSE
	debugLimitedShopItems.iReadStage = 0
	debugLimitedShopItems.iTempLoadStage = 0
	debugLimitedShopItems.bTempReadResult = FALSE
ENDPROC

PROC TEST_LIMITED_SHOP_ITEMS() //1445116
//	INT iItemID[10]
//	INT iItemAmount[10]
//	INT i
	//TEXT_LABEL_31 categoryNames[1]
	//TEXT_LABEL_23 uniqueIdentifiers[0]
//	REPEAT 10 i
//		iItemID[i] = i
//		iItemAmount[i] = 10*i
//		categoryNames[i] = "ItemId"
//		uniqueIdentifiers[i] = ""
//		uniqueIdentifiers[i] += i
//	ENDREPEAT
//	
//	LeaderboardRowData initRowData
//	LeaderboardReadInfo readInfo
//	
//	Leaderboard2ReadData lbReadData
//	
//	
	IF debugLimitedShopItems.bTriggerPurchase
		WRITE_PURCHASE_OF_LIMITED_EDITION_ITEMS(debugLimitedShopItems.iPurchaseItemID,debugLimitedShopItems.iPurchaseAmount)
		debugLimitedShopItems.bTriggerPurchase = FALSE
	ELIF debugLimitedShopItems.bReadLeaderboardValues
		IF GET_LIMITED_EDITION_ITEMS(debugTextLimEdItems)
			CLEAR_LIMITED_EDITION_ITEM_LB_DETAILS(debugTextLimEdItems)
			debugLimitedShopItems.bReadLeaderboardValues = FALSE
		ENDIF
	ENDIF
	
//		lbReadData.m_LeaderboardId = LEADERBOARD_FREEMODE_LIMITED_EDITION
//		lbReadData.m_Type = LEADERBOARD_TYPE_GROUP
//		lbReadData.m_GroupSelector.m_NumGroups = 4
//		i = 0
//		GAMER_HANDLE noGamer[1]
//		LeaderboardClanIds clanIDs[1]
//		Leaderboard2GroupHandle groupHandle
//		
//		REPEAT lbReadData.m_GroupSelector.m_NumGroups i
//			lbReadData.m_GroupSelector.m_Group[i].m_Category = categoryNames[i]
//			lbReadData.m_GroupSelector.m_Group[i].m_Id = uniqueIdentifiers[i]
//			groupHandle.m_Group[i].m_Category = categoryNames[i]
//			groupHandle.m_Group[i].m_Id = uniqueIdentifiers[i]
//		ENDREPEAT
//		groupHandle.m_NumGroups = lbReadData.m_GroupSelector.m_NumGroups
//		
//		IF debugLimitedShopItems.bReadLeaderboardValues
//			SWITCH debugLimitedShopItems.iReadStage
//				CASE 0
//					IF START_SC_LEADERBOARD_READ_BY_ROW(debugLimitedShopItems.iTempLoadStage,debugLimitedShopItems.bTempReadResult,lbReadData,noGamer,0,clanIDs,0,groupHandle,groupHandle.m_NumGroups)
//						println("TEST_LIMITED_SHOP_ITEMS: Called by ", GET_THIS_SCRIPT_NAME())
//						#IF IS_DEBUG_BUILD
//						println("TEST_LIMITED_SHOP_ITEMS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
//						#ENDIF
//						FILL_READ_INFO_STRUCT(readInfo,lbReadData)
//				
//						IF debugLimitedShopItems.bTempReadResult
//						AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
//							IF readInfo.m_ReturnedRows > 0
//								i = 0
//								REPEAT readInfo.m_ReturnedRows i
//									LEADERBOARDS2_READ_GET_ROW_DATA_INFO(i, initRowData)
//									println("LEADERBOARDS2_READ_GET_ROW_DATA_INFO: Row: ", i, " Column: ",0)
//									#IF IS_DEBUG_BUILD
//									DEBUG_PRINT_SC_LB_RETURNED_VALUES("TEST_LIMITED_SHOP_ITEMS","Current Run",i,0,
//																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(i, 0),0.0,TRUE )
//									#ENDIF
//								ENDREPEAT
//							ELSE
//								println("TEST_LIMITED_SHOP_ITEMS -- moving to stage 1 no data for local player ") 
//							ENDIF
//							
//							LEADERBOARDS2_READ_GET_ROW_DATA_END( )
//						ELSE
//							println("TEST_LIMITED_SHOP_ITEMS -- moving to stage 1 no data for local player ") 
//						ENDIF
//						debugLimitedShopItems.iReadStage = 1
//						END_LEADERBOARD_READ(debugLimitedShopItems.iTempLoadStage,debugLimitedShopItems.bTempReadResult,lbReadData)
//					ENDIF
//				BREAK
//		
//				CASE 1
//					CLEAR_TEST_LIMITED_SHOP_ITEMS_LEADERBOARD_READ_STRUCT()
//				BREAK
//			ENDSWITCH
//		ENDIF
//	ENDIF
ENDPROC
#ENDIF

//PURPOSE: Checks if a Lottery Leaderboard Read is successful. Making sure the Leaderboard is okay to be used.
FUNC INT HAS_LOTTERY_LEADERBOARD_TEST_PASSED(INT &iTempLoadStage, BOOL &bTempReadResult)
	LeaderboardReadInfo readInfo
	LeaderboardRowData initRowData
	Leaderboard2ReadData ReadDataStruct
	GAMER_HANDLE gamerHandle

	gamerHandle 					= GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	ReadDataStruct.m_LeaderboardID 	= ENUM_TO_INT(LEADERBOARD_FREEMODE_LOTTERY_TICKET)
	ReadDataStruct.m_type 			= LEADERBOARD2_TYPE_PLAYER
	readInfo.m_LeaderboardId 		= ENUM_TO_INT(LEADERBOARD_FREEMODE_LOTTERY_TICKET)
	readInfo.m_LeaderboardType 		= ENUM_TO_INT(LEADERBOARD2_TYPE_PLAYER)
	
	IF START_SC_LEADERBOARD_READ_BY_HANDLE(iTempLoadStage, bTempReadResult, ReadDataStruct, gamerHandle)
		#IF IS_DEBUG_BUILD
		DEBUG_START_LEADERBOARD_PRINT_RESULTS("HAS_LOTTERY_LEADERBOARD_TEST_PASSED (ReadDataStruct) ", ReadDataStruct)
		#ENDIF
		
		FILL_READ_INFO_STRUCT(readInfo, ReadDataStruct)
		CLEAR_ROW_DATA_INFO_STRUCT(initRowData)
		END_LEADERBOARD_READ(iTempLoadStage, bTempReadResult, ReadDataStruct)
		
		IF bTempReadResult = TRUE
			PRINTLN(" LOTTERY - HAS_LOTTERY_LEADERBOARD_TEST_PASSED = TRUE")
			RETURN 1
		ELSE
			PRINTLN(" LOTTERY - HAS_LOTTERY_LEADERBOARD_TEST_PASSED = FALSE")
			RETURN -1
		ENDIF
	ENDIF
	
	//PRINTLN(" LOTTERY - HAS_LOTTERY_LEADERBOARD_TEST_PASSED = PENDING")
	RETURN 0
ENDFUNC


FUNC BOOL START_SC_LEADERBOARD_READ_BY_PLATFORM(INT &iLoadStage,BOOL &bSuccessful, Leaderboard2ReadData &lbReadData, STRING handleData,STRING platform)
	#IF IS_DEBUG_BUILD
	STRING procName = "START_SC_LEADERBOARD_READ_BY_PLATFORM"
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NOT IS_ANY_LEADERBOARD_READ_BEING_PROCESSED()//LEADERBOARDS_READ_EXISTS(lbReadData.m_LeaderboardId, lbReadData.m_Type)
			AND NOT SHOULD_ALL_SCRIPT_LEADERBOARD_READS_TERMINATE()
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_LEADERBOARD_READ_DATA_STRUCT(procName,lbReadData)
				#ENDIF
				START_LEADERBOARD_READ(lbReadData)
				IF LEADERBOARDS2_READ_BY_PLAFORM(lbReadData, handleData, platform)
					iLoadStage++
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName, 1)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					//println()
					println("LEADERBOARDS2_READ_BY_PLAFORM FAILED!! start See Conor")
					println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
					//println()
					//SCRIPT_ASSERT("LEADERBOARDS2_READ_BY_HANDLE FAILED!! See Conor")
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
					#ENDIF
					bSuccessful = FALSE
					iLoadStage=3
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_DBb_OutputLeaderboardReadErrors
					DEBUG_PRINT_WAITING_FOR_READ_TO_FINISH(lbReadData)//"START_SC_LEADERBOARD_READ_BY_HANDLE",
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT LEADERBOARDS_READ_PENDING(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				iLoadStage++
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_LB_READ_PROGRESS(procName,2)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF LEADERBOARDS_READ_SUCCESSFUL(lbReadData.m_LeaderboardId, lbReadData.m_Type)
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,TRUE)
					DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = TRUE
				iLoadStage++
			ELSE
				#IF IS_DEBUG_BUILD
				//println()
				println("LEADERBOARD READ FAILED!! See Conor")
				println(procname, ":  ID :", lbReadData.m_LeaderboardId, ": Type: ",lbReadData.m_Type)
				//println()
				//SCRIPT_ASSERT("LEADERBOARD READ FAILED!! See Conor")
				DEBUG_PRINT_READ_RESULT_STATUS(procName,lbReadData,FALSE)
				DEBUG_PRINT_LB_READ_PROGRESS(procName,3)
				#ENDIF
				bSuccessful = FALSE
				iLoadStage++
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


PROC RESET_PLATFORM_UPGRADE_LB_CHECK_STRUCT(PLATFORM_UPGRADE_LB_CHECKS &sPlatformUpdateLBData)
	PRINTLN("Resetting PLATFORM_UPGRADE_LB_CHECKS struct.")
	Leaderboard2ReadData lbReadData
	lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_COMMERCE)
	lbReadData.m_Type = LEADERBOARD2_TYPE_PLAYER
	END_LEADERBOARD_READ(sPlatformUpdateLBData.iReadStage,sPlatformUpdateLBData.bSuccessful,lbReadData)
	
	sPlatformUpdateLBData.iTotalStage = 0
	sPlatformUpdateLBData.iReadStage = 0
	sPlatformUpdateLBData.bSuccessful = FALSE
	sPlatformUpdateLBData.bComplete = FALSE
ENDPROC



FUNC BOOL RUN_PLATFORM_UPGRADE_LB_CHECKS(PLATFORM_UPGRADE_LB_CHECKS &iPlatformUpdateLBData, BOOL bSPCheck = FALSE)
	
	IF iPlatformUpdateLBData.iFrameCheck+10 < GET_FRAME_COUNT()
		RESET_PLATFORM_UPGRADE_LB_CHECK_STRUCT(iPlatformUpdateLBData)
		PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Resetting due to inactivity.")
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Resetting due to inactivity.")
		#ENDIF
	ENDIF
	iPlatformUpdateLBData.iFrameCheck = GET_FRAME_COUNT()
	
	#IF IS_DEBUG_BUILD
		IF NOT bSPCheck
			PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Running MP check variation.")
		ELSE
			PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Running SP check variation.")
		ENDIF
	#ENDIF
	
	#IF USE_FINAL_PRINTS
		IF NOT bSPCheck
			PRINTLN_FINAL("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Running MP check variation.")
		ELSE
			PRINTLN_FINAL("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Running SP check variation.")
		ENDIF
	#ENDIF
	
	IF (g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp <= 0
	OR IS_STRING_NULL_OR_EMPTY(g_struct_Save_transfer_data_PS3.m_gamerHandle))
	AND (g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp <= 0
	OR IS_STRING_NULL_OR_EMPTY(g_struct_Save_transfer_data_XBOX360.m_gamerHandle))
		PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS- no valid migration data returning true")
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No valid migration data returning true.")
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	LeaderboardRowData initRowData
	LeaderboardReadInfo readInfo
	Leaderboard2ReadData lbReadData
	TEXT_LABEL_63 gamerName
	STRING platform
	
	lbReadData.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_COMMERCE)
	lbReadData.m_Type = LEADERBOARD2_TYPE_PLAYER
	
	INT iChecksStatus
	IF NOT bSPCheck
		iChecksStatus = GET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK)
	ELSE
		iChecksStatus = 0 //Whenever we rerun this function in SP we want to rerun all tests.
		
		//Pull the current profile settings so we don't lose any values as we update bits.
		IF ARE_PROFILE_SETTINGS_VALID()
			CDEBUG1LN(DEBUG_LASTGEN, "<LastGen-SE-CE> Loaded current profile settings.")
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] Loaded current profile settings.")
			#ENDIF
			
			g_iPlayerHasLastGenSpecialContentBitset = GET_PROFILE_SETTING(GAMER_HAS_SPECIALEDITION_CONTENT)
		ENDIF
	ENDIF
	
	//platform matches STAT_MIGRATE_SAVEGAME_START()
	IF IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_BLIMP))
	AND IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_COLLECT))
	AND IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_SPECIAL))
	AND IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_BLIMP))
	AND IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_COLLECT))
	AND IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_SPECIAL))
		PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS- all checks complete returning true")
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - All checks complete returning true.")
		#ENDIF
		
		RETURN TRUE
	ENDIF

	SWITCH iPlatformUpdateLBData.iTotalStage
		CASE 0
			IF g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp <= 0
			OR IS_STRING_NULL_OR_EMPTY(g_struct_Save_transfer_data_PS3.m_gamerHandle)
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - PS3 SP progress made: ", g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp)
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - PS3 gamer handle: ", g_struct_Save_transfer_data_PS3.m_gamerHandle)
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - PS3 gamer-handle invalid. Skipping PS3 checks.")
				
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][SE-CE] PS3 SP progress made: ", g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp)
					PRINTLN_FINAL("[LASTGEN][SE-CE] PS3 gamer handle: ", g_struct_Save_transfer_data_PS3.m_gamerHandle)
					PRINTLN_FINAL("[LASTGEN][SE-CE] PS3 gamer-handle invalid. Skipping PS3 checks.")
				#ENDIF
				
				iPlatformUpdateLBData.iTotalStage = 3
				RETURN FALSE
			ENDIF
			
			gamerName = g_struct_Save_transfer_data_PS3.m_gamerHandle
			platform = ("ps3")
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "ProductId"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = "preorder_pack"
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] ------- Leaderboard Query -------")
				PRINTLN_FINAL("[LASTGEN][SE-CE] GamerHandle: ", gamerName)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Platform: ", platform)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Id: ", lbReadData.m_GroupSelector.m_Group[0].m_Id)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Category: ", lbReadData.m_GroupSelector.m_Group[0].m_Category)
				PRINTLN_FINAL("[LASTGEN][SE-CE] NumbeOfGroups: ", lbReadData.m_GroupSelector.m_NumGroups)
				PRINTLN_FINAL("[LASTGEN][SE-CE] ---------------------------------")
			#ENDIF
			
			IF NOT IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_BLIMP))
				IF START_SC_LEADERBOARD_READ_BY_PLATFORM(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData,
									gamerName,platform)
									
					PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#ENDIF
					
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
			
					IF iPlatformUpdateLBData.bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0) > 0
								PRINTLN("START_SC_LEADERBOARD_READ_BY_PLATFORM: Row: ",0, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("RUN_PLATFORM_UPGRADE_LB_CHECKS","Current Run",0,0,
																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0),0.0,TRUE )
								#ENDIF
								
								PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has PS3 BLIMP.") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has PS3 BLIMP.")
								#ENDIF
								
								SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_HAS_BLIMP))
								
								IF NOT bSPCheck
									SET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK,iChecksStatus)
								ENDIF
							ELSE
								PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 BLIMP. (C)") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 BLIMP. (C)")
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 BLIMP. (B)") 
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 BLIMP. (B)")
							#ENDIF
						ENDIF
						SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_BLIMP))
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					ELSE
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 BLIMP. (A)")
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 BLIMP. (A)")
							PRINTLN_FINAL("[LASTGEN][SE-CE] iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
							PRINTLN_FINAL("[LASTGEN][SE-CE] LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#ENDIF
					ENDIF
					iPlatformUpdateLBData.bSuccessful = FALSE
					iPlatformUpdateLBData.iTotalStage = 1
					END_LEADERBOARD_READ(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData)
				ENDIF
			ELSE
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Already run check for PS3 BLIMP. Moving to next check.")
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][SE-CE] Already run check for PS3 BLIMP. Moving to next check.")
				#ENDIF
			
				iPlatformUpdateLBData.bSuccessful = FALSE
				iPlatformUpdateLBData.iTotalStage = 1
			ENDIF
		BREAK

		CASE 1
			gamerName = g_struct_Save_transfer_data_PS3.m_gamerHandle
			platform = ("ps3")
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "ProductId"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = "collectors_ed"
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] ------- Leaderboard Query -------")
				PRINTLN_FINAL("[LASTGEN][SE-CE] GamerHandle: ", gamerName)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Platform: ", platform)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Id: ", lbReadData.m_GroupSelector.m_Group[0].m_Id)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Category: ", lbReadData.m_GroupSelector.m_Group[0].m_Category)
				PRINTLN_FINAL("[LASTGEN][SE-CE] NumbeOfGroups: ", lbReadData.m_GroupSelector.m_NumGroups)
				PRINTLN_FINAL("[LASTGEN][SE-CE] ---------------------------------")
			#ENDIF
			
			CLEAR_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_PS3_COLLECTORS)
			
			IF NOT IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_COLLECT))
				IF START_SC_LEADERBOARD_READ_BY_PLATFORM(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData,
									gamerName,platform)
					#IF IS_DEBUG_BUILD
					println("RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
					
					PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS readInfo.m_LeaderboardId = ", readInfo.m_LeaderboardId)
					PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS readInfo.m_LeaderboardType = ", readInfo.m_LeaderboardType)
					
					IF iPlatformUpdateLBData.bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0) > 0
								println("START_SC_LEADERBOARD_READ_BY_PLATFORM: Row: ", 0, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("RUN_PLATFORM_UPGRADE_LB_CHECKS","Current Run",0,0,
																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0),0.0,TRUE )
								#ENDIF
								
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has PS3 COLLECTORS.") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has PS3 COLLECTORS.")
								#ENDIF
								
								SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_HAS_COLLECT))
								SET_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_PS3_COLLECTORS)
								
								IF NOT bSPCheck
									SET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK,iChecksStatus)
								ELSE
									CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Flagging that player's last gen account had PS3 COLLECTORS in SP stat.")
									#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("[LASTGEN][SE-CE] Flagging that player's last gen account had PS3 COLLECTORS in SP stat.")
									#ENDIF
									
									
									IF ARE_PROFILE_SETTINGS_VALID()
										CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Updated profile settings.")
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][SE-CE] Updated profile settings.")
										#ENDIF
										
										SET_HAS_SPECIALEDITION_CONTENT(g_iPlayerHasLastGenSpecialContentBitset)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 COLLECTORS. (C)") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 COLLECTORS. (C)")
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 COLLECTORS. (B)") 
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 COLLECTORS. (B)")
							#ENDIF
						ENDIF
						
						SET_BIT(g_iPlayerHasLastGenSpecialContentBitset,BIT_LAST_GEN_SPECIAL_CONTENT_STAT_STORED_COLLECTOR)
						STAT_SET_INT(SP_UNLOCK_EXCLUS_CONTENT, g_iPlayerHasLastGenSpecialContentBitset)
						CPRINTLN(DEBUG_LASTGEN, "[LASTGEN][SE-CE] PS3 STAT_STORED_COLLECTOR Set g_iPlayerHasLastGenSpecialContentBitset = ", g_iPlayerHasLastGenSpecialContentBitset, " " )

						
						
						SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_COLLECT))
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					ELSE
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 COLLECTORS. (A)")
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 COLLECTORS. (A)")
							PRINTLN_FINAL("[LASTGEN][SE-CE] iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
							PRINTLN_FINAL("[LASTGEN][SE-CE] LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#ENDIF
					ENDIF
					iPlatformUpdateLBData.bSuccessful = FALSE
					iPlatformUpdateLBData.iTotalStage = 2
					END_LEADERBOARD_READ(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData)
				ENDIF
			ELSE
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Already run check for PS3 COLLECTORS. Moving to next check.") 
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][SE-CE] Already run check for PS3 COLLECTORS. Moving to next check.")
				#ENDIF
				
				iPlatformUpdateLBData.bSuccessful = FALSE
				iPlatformUpdateLBData.iTotalStage = 2
			ENDIF
		BREAK
		
		CASE 2
			gamerName = g_struct_Save_transfer_data_PS3.m_gamerHandle
			platform = ("ps3")
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "ProductId"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = "special_ed"
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] ------- Leaderboard Query -------")
				PRINTLN_FINAL("[LASTGEN][SE-CE] GamerHandle: ", gamerName)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Platform: ", platform)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Id: ", lbReadData.m_GroupSelector.m_Group[0].m_Id)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Category: ", lbReadData.m_GroupSelector.m_Group[0].m_Category)
				PRINTLN_FINAL("[LASTGEN][SE-CE] NumbeOfGroups: ", lbReadData.m_GroupSelector.m_NumGroups)
				PRINTLN_FINAL("[LASTGEN][SE-CE] ---------------------------------")
			#ENDIF
			
			CLEAR_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_PS3_SPECIAL)
			
			IF NOT IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_SPECIAL))
				IF START_SC_LEADERBOARD_READ_BY_PLATFORM(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData,
									gamerName,platform)
					#IF IS_DEBUG_BUILD
					println("RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
			
					IF iPlatformUpdateLBData.bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0) > 0
								println("START_SC_LEADERBOARD_READ_BY_PLATFORM: Row: ", 0, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("RUN_PLATFORM_UPGRADE_LB_CHECKS","Current Run",0,0,
																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0),0.0,TRUE )
								#ENDIF
								
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has PS3 SPECIAL.") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has PS3 SPECIAL.")
								#ENDIF
								
								SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_HAS_SPECIAL))
								SET_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_PS3_SPECIAL)

								
								IF NOT bSPCheck
									SET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK,iChecksStatus)
								ELSE
									CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Flagging that player's last gen account had PS3 SPECIAL in SP stat.")
									#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("[LASTGEN][SE-CE] Flagging that player's last gen account had PS3 SPECIAL in SP stat.")
									#ENDIF
									

									IF ARE_PROFILE_SETTINGS_VALID()
										CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Updated profile settings.")
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][SE-CE] Updated profile settings.")
										#ENDIF
										
										SET_HAS_SPECIALEDITION_CONTENT(g_iPlayerHasLastGenSpecialContentBitset)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 SPECIAL. (C)") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 SPECIAL. (C)")
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 SPECIAL. (B)") 
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 SPECIAL. (B)")
							#ENDIF
						ENDIF
						
						SET_BIT(g_iPlayerHasLastGenSpecialContentBitset,BIT_LAST_GEN_SPECIAL_CONTENT_STAT_STORED_SPECIAL)
						STAT_SET_INT(SP_UNLOCK_EXCLUS_CONTENT, g_iPlayerHasLastGenSpecialContentBitset)
						CPRINTLN(DEBUG_LASTGEN, "[LASTGEN][SE-CE] PS3 STAT_STORED_SPECIAL Set g_iPlayerHasLastGenSpecialContentBitset = ", g_iPlayerHasLastGenSpecialContentBitset, " " )
						
						
						SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_PS3_SPECIAL))
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					ELSE
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for PS3 SPECIAL. (A)")
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for PS3 SPECIAL. (A)")
							PRINTLN_FINAL("[LASTGEN][SE-CE] iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
							PRINTLN_FINAL("[LASTGEN][SE-CE] LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#ENDIF 
					ENDIF
					iPlatformUpdateLBData.bSuccessful = FALSE
					iPlatformUpdateLBData.iTotalStage = 3
					END_LEADERBOARD_READ(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData)
				ENDIF
			ELSE
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Already run check for PS3 SPECIAL. Moving to next check.") 
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][SE-CE] Already run check for PS3 SPECIAL. Moving to next check.")
				#ENDIF
			
				iPlatformUpdateLBData.bSuccessful = FALSE
				iPlatformUpdateLBData.iTotalStage = 3 
			ENDIF
		BREAK
		
		CASE 3
			IF g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp <= 0
			OR IS_STRING_NULL_OR_EMPTY(g_struct_Save_transfer_data_XBOX360.m_gamerHandle)
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - 360 SP progress made: ", g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp)
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - 360 gamer handle: ", g_struct_Save_transfer_data_XBOX360.m_gamerHandle)
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Xbox360 gamer-handle invalid. Skipping 360 checks.") 
				
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][SE-CE] 360 SP progress made: ", g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp)
					PRINTLN_FINAL("[LASTGEN][SE-CE] 360 gamer handle: ", g_struct_Save_transfer_data_XBOX360.m_gamerHandle)
					PRINTLN_FINAL("[LASTGEN][SE-CE] Xbox360 gamer-handle invalid. Skipping 360 checks.")
				#ENDIF
				
				iPlatformUpdateLBData.iTotalStage = 6
				RETURN FALSE
			ENDIF
			gamerName = g_struct_Save_transfer_data_XBOX360.m_gamerHandle
			platform = ("xbox360")
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "ProductId"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = "atomic_blimp"
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] ------- Leaderboard Query -------")
				PRINTLN_FINAL("[LASTGEN][SE-CE] GamerHandle: ", gamerName)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Platform: ", platform)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Id: ", lbReadData.m_GroupSelector.m_Group[0].m_Id)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Category: ", lbReadData.m_GroupSelector.m_Group[0].m_Category)
				PRINTLN_FINAL("[LASTGEN][SE-CE] NumbeOfGroups: ", lbReadData.m_GroupSelector.m_NumGroups)
				PRINTLN_FINAL("[LASTGEN][SE-CE] ---------------------------------")
			#ENDIF
			
			IF NOT IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_BLIMP))
				IF START_SC_LEADERBOARD_READ_BY_PLATFORM(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData,
									gamerName,platform)
					#IF IS_DEBUG_BUILD
					println("RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
			
					IF iPlatformUpdateLBData.bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0) > 0
								println("START_SC_LEADERBOARD_READ_BY_PLATFORM: Row: ", 0, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("RUN_PLATFORM_UPGRADE_LB_CHECKS","Current Run",0,0,
																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0),0.0,TRUE )
								#ENDIF
								
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has 360 BLIMP.") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has 360 BLIMP.")
								#ENDIF
								
								SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_HAS_BLIMP))
								
								IF NOT bSPCheck
									SET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK,iChecksStatus)
								ENDIF
							ELSE
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 BLIMP. (C)") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 BLIMP. (C)")
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 BLIMP. (B)") 
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 BLIMP. (B)")
							#ENDIF
						ENDIF
						SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_BLIMP))
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					ELSE
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 BLIMP. (A)")
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 BLIMP. (A)")
							PRINTLN_FINAL("[LASTGEN][SE-CE] iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
							PRINTLN_FINAL("[LASTGEN][SE-CE] LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#ENDIF
					ENDIF
					iPlatformUpdateLBData.bSuccessful = FALSE
					iPlatformUpdateLBData.iTotalStage = 4
					END_LEADERBOARD_READ(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData)
				ENDIF
			ELSE
				PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Already run check for 360 BLIMP. Moving to next check.") 
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][SE-CE] Already run check for 360 BLIMP. Moving to next check.")
				#ENDIF
			
				iPlatformUpdateLBData.bSuccessful = FALSE
				iPlatformUpdateLBData.iTotalStage = 4
			ENDIF
		BREAK

		CASE 4
			gamerName = g_struct_Save_transfer_data_XBOX360.m_gamerHandle
			platform = ("xbox360")
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "ProductId"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = "collectors_ed"
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] ------- Leaderboard Query -------")
				PRINTLN_FINAL("[LASTGEN][SE-CE] GamerHandle: ", gamerName)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Platform: ", platform)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Id: ", lbReadData.m_GroupSelector.m_Group[0].m_Id)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Category: ", lbReadData.m_GroupSelector.m_Group[0].m_Category)
				PRINTLN_FINAL("[LASTGEN][SE-CE] NumbeOfGroups: ", lbReadData.m_GroupSelector.m_NumGroups)
				PRINTLN_FINAL("[LASTGEN][SE-CE] ---------------------------------")
			#ENDIF
			
			CLEAR_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_360_COLLECTORS)
			
			IF NOT IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_COLLECT))
				IF START_SC_LEADERBOARD_READ_BY_PLATFORM(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData,
									gamerName,platform)
					#IF IS_DEBUG_BUILD
					println("RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
			
					IF iPlatformUpdateLBData.bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0) > 0
								println("START_SC_LEADERBOARD_READ_BY_PLATFORM: Row: ", 0, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("RUN_PLATFORM_UPGRADE_LB_CHECKS","Current Run",0,0,
																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0),0.0,TRUE )
								#ENDIF
								
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has 360 COLLECTORS.") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has 360 COLLECTORS.")
								#ENDIF
								
								SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_HAS_COLLECT))
								SET_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_360_COLLECTORS)

								
								IF NOT bSPCheck
									SET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK,iChecksStatus)
								ELSE
									PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has 360 COLLECTORS.") 
									#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has 360 COLLECTORS.")
									#ENDIF
									
									
									IF ARE_PROFILE_SETTINGS_VALID()
										CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Updated profile settings.")
										
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][SE-CE] Updated profile settings.")
										#ENDIF
										
										SET_HAS_SPECIALEDITION_CONTENT(g_iPlayerHasLastGenSpecialContentBitset)
									ENDIF
								ENDIF 
							ELSE
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 COLLECTORS. (C)") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 COLLECTORS. (C)")
								#ENDIF
							ENDIF
	
						ELSE
							PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 COLLECTORS. (B)") 
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 COLLECTORS. (B)")
							#ENDIF
						ENDIF
						
						SET_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_STAT_STORED_COLLECTOR)
						STAT_SET_INT(SP_UNLOCK_EXCLUS_CONTENT, g_iPlayerHasLastGenSpecialContentBitset)
						CPRINTLN(DEBUG_LASTGEN, "[LASTGEN][SE-CE] 360 STAT_STORED_COLLECTOR Set g_iPlayerHasLastGenSpecialContentBitset = ", g_iPlayerHasLastGenSpecialContentBitset, " " )
						
						SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_COLLECT))
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					ELSE
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 COLLECTORS. (A)")
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 COLLECTORS. (A)")
							PRINTLN_FINAL("[LASTGEN][SE-CE] iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
							PRINTLN_FINAL("[LASTGEN][SE-CE] LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#ENDIF
					ENDIF
					iPlatformUpdateLBData.bSuccessful = FALSE
					iPlatformUpdateLBData.iTotalStage = 5
					END_LEADERBOARD_READ(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData)
				ENDIF
			ELSE
				iPlatformUpdateLBData.bSuccessful = FALSE
				iPlatformUpdateLBData.iTotalStage = 5
				println("RUN_PLATFORM_UPGRADE_LB_CHECKS -- already checked blimp moving to stage 5 ") 
			ENDIF
		BREAK
		
		CASE 5
			gamerName = g_struct_Save_transfer_data_XBOX360.m_gamerHandle
			platform = ("xbox360")
			lbReadData.m_GroupSelector.m_NumGroups = 1
			lbReadData.m_GroupSelector.m_Group[0].m_Category = "ProductId"
			lbReadData.m_GroupSelector.m_Group[0].m_Id = "special_ed"
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] ------- Leaderboard Query -------")
				PRINTLN_FINAL("[LASTGEN][SE-CE] GamerHandle: ", gamerName)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Platform: ", platform)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Id: ", lbReadData.m_GroupSelector.m_Group[0].m_Id)
				PRINTLN_FINAL("[LASTGEN][SE-CE] Category: ", lbReadData.m_GroupSelector.m_Group[0].m_Category)
				PRINTLN_FINAL("[LASTGEN][SE-CE] NumbeOfGroups: ", lbReadData.m_GroupSelector.m_NumGroups)
				PRINTLN_FINAL("[LASTGEN][SE-CE] ---------------------------------")
			#ENDIF
			
			CLEAR_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_360_SPECIAL)
			
			IF NOT IS_BIT_SET(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_SPECIAL))
				IF START_SC_LEADERBOARD_READ_BY_PLATFORM(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData,
									gamerName,platform)
					#IF IS_DEBUG_BUILD
					println("RUN_PLATFORM_UPGRADE_LB_CHECKS  LEADERBOARD ID = ", lbReadData.m_LeaderboardId) 
					#ENDIF
					FILL_READ_INFO_STRUCT(readInfo,lbReadData)
			
					IF iPlatformUpdateLBData.bSuccessful
					AND LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo)
						IF readInfo.m_ReturnedRows > 0
							LEADERBOARDS2_READ_GET_ROW_DATA_INFO(0, initRowData)
							IF LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0) > 0
								println("START_SC_LEADERBOARD_READ_BY_PLATFORM: Row: ", 0, " Column: ",0)
								#IF IS_DEBUG_BUILD
								DEBUG_PRINT_SC_LB_RETURNED_VALUES("RUN_PLATFORM_UPGRADE_LB_CHECKS","Current Run",0,0,
																			LEADERBOARDS2_READ_GET_ROW_DATA_INT(0, 0),0.0,TRUE )
								#ENDIF
								
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - Setting player has 360 SPECIAL.") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] Setting player has 360 SPECIAL.")
								#ENDIF
								
								SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_HAS_SPECIAL))
								SET_BIT(g_iPlayerHasLastGenSpecialContentBitset, BIT_LAST_GEN_SPECIAL_CONTENT_360_SPECIAL)
								
								IF NOT bSPCheck
									SET_MP_INT_PLAYER_STAT(MPPLY_PLAT_UP_LB_CHECK,iChecksStatus)
								ELSE
									CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Flagging that player's last gen account had 360 SPECIAL in SP stat.")
									#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("[LASTGEN][SE-CE] Flagging that player's last gen account had 360 SPECIAL in SP stat.")
									#ENDIF
	
									IF ARE_PROFILE_SETTINGS_VALID()
										CPRINTLN(DEBUG_LASTGEN, "<LastGen-SE-CE> Updated profile settings.")
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][SE-CE] Updated profile settings.")
										#ENDIF
										
										SET_HAS_SPECIALEDITION_CONTENT(g_iPlayerHasLastGenSpecialContentBitset)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 SPECIAL. (C)") 
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 SPECIAL. (C)")
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("[LASTGEN][SE-CE] RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 SPECIAL. (B)") 
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 SPECIAL. (B)")
							#ENDIF
						ENDIF
						SET_BIT(g_iPlayerHasLastGenSpecialContentBitset,BIT_LAST_GEN_SPECIAL_CONTENT_STAT_STORED_SPECIAL)
						STAT_SET_INT(SP_UNLOCK_EXCLUS_CONTENT, g_iPlayerHasLastGenSpecialContentBitset)
						CPRINTLN(DEBUG_LASTGEN, "[LASTGEN][SE-CE] 360 STAT_STORED_SPECIAL Set g_iPlayerHasLastGenSpecialContentBitset = ", g_iPlayerHasLastGenSpecialContentBitset, " " )

						SET_BIT(iChecksStatus,ENUM_TO_INT(PLATFORM_UPGRADE_LB_CHECK_XBOX360_SPECIAL))
						LEADERBOARDS2_READ_GET_ROW_DATA_END( )
					ELSE
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - No data found for 360 SPECIAL. (A)")
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
						PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN][SE-CE] No data found for 360 SPECIAL. (A)")
							PRINTLN_FINAL("[LASTGEN][SE-CE] iPlatformUpdateLBData.bSuccessful = ", iPlatformUpdateLBData.bSuccessful)
							PRINTLN_FINAL("[LASTGEN][SE-CE] LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo) = ", LEADERBOARDS2_READ_GET_ROW_DATA_START(readInfo))
						#ENDIF
					ENDIF
					iPlatformUpdateLBData.bSuccessful = FALSE
					iPlatformUpdateLBData.iTotalStage = 6
					END_LEADERBOARD_READ(iPlatformUpdateLBData.iReadStage,iPlatformUpdateLBData.bSuccessful,lbReadData)
					
					IF NOT bSPCheck
						REQUEST_SAVE(SSR_REASON_LEADERBOARD)
					ENDIF
				ENDIF
			ELSE
				iPlatformUpdateLBData.bSuccessful = FALSE
				iPlatformUpdateLBData.iTotalStage = 6
				
				IF NOT bSPCheck
					PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Requesting save in final stage.")
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[LASTGEN][SE-CE] Requesting save in final stage.")
					#ENDIF
				
					REQUEST_SAVE(SSR_REASON_LEADERBOARD)
				ENDIF

			ENDIF
		BREAK
		CASE 6
			PRINTLN("RUN_PLATFORM_UPGRADE_LB_CHECKS - Check ran to completion. Finished.")
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][SE-CE] Check ran to completion. Finished.")
			#ENDIF
		
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

#IF FEATURE_TUNER
STRUCT CAR_MEET_LEADERBOARD_WRITE_DATA
	INT iFMMCType
	INT iSubType
	INT iBestTime 	//fastest lap/best time
	INT iWins 		//# of wins (generally will be 1 if you won, 0 if you lost)
	INT iLoses 		//# of loses (generally will be 0 if you won, 1 if you lost)
	INT iMatches	//# of matches (generally will be 1)
	INT iVehModel	//Int value of the vehicle model used
	BOOL bCustomVeh //Was custom or stock vehicle used TRUE = custom
	INT iClubXP		//Club XP after event
	TEXT_LABEL_31  tlOverwriteUGCID //USED FOR TESTING/OVERWRITING IF NEEDED ONLY
ENDSTRUCT

/// PURPOSE:
///    Write to the car meet membership leaderboard
/// PARAMS:
PROC WRITE_TO_CAR_MEET_LEADERBOARDS(CAR_MEET_LEADERBOARD_WRITE_DATA &data #IF IS_DEBUG_BUILD, BOOL bBypassXPSafetyCheck = FALSE #endif )
	
	//EXIT //exitng LBS dont exist yet.
	TEXT_LABEL_31 categoryNames[2] 
   	TEXT_LABEL_23 uniqueIdentifiers[2]
	INT iNumCategories
	IF g_bAllowWriteToCarMeetMembershipLB //so we can disable this in BG script if needed
		#IF IS_DEBUG_BUILD
		IF NOT bBypassXPSafetyCheck
		#ENDIF
		IF data.iClubXP < GET_PLAYERS_CAR_CLUB_REP(PLAYER_ID())
			PRINTLN("WRITE_TO_CAR_MEET_LEADERBOARDS: INVALID XP USED!! See Conor  stored XP: ",GET_PLAYERS_CAR_CLUB_REP(PLAYER_ID())," written XP: ",data.iClubXP)
			SCRIPT_ASSERT("WRITE_TO_CAR_MEET_LEADERBOARDS: INVALID XP USED!! See Conor")
		ELSE
			PRINTLN("WRITE_TO_CAR_MEET_LEADERBOARDS: stored XP: ",GET_PLAYERS_CAR_CLUB_REP(PLAYER_ID())," written XP: ",data.iClubXP)
		ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		categoryNames[0] = "" 
		uniqueIdentifiers[0] = ""
		IF data.iFMMCType = FMMC_TYPE_SANDBOX_ACTIVITY
		AND data.iSubType = ENUM_TO_INT(SAS_TT_LOCATION_1)
			//Time trials don't "win" or "lose"
			data.iWins = 0
			data.iLoses = 0
			data.iMatches = 0
		ENDIF
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_TUNER_CAR_CLUB,uniqueIdentifiers,categoryNames,0) //LEADERBOARD_FREEMODE_TUNER_CAR_CLUB
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD Writing to SC Leaderboard for CAR MEET MEMBERSHIP")
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, data.iClubXP, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD XP value = ", data.iClubXP)
			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_WINS, data.iWins, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD wins incremented by ",data.iWins)
			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES_LOST, data.iLoses, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD loses incremented by ",data.iLoses)
			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, data.iMatches, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD matches incremented by ", data.iMatches)
			
		ENDIF
	ENDIF
	IF g_bAllowWriteToTunerRaceLB
	AND data.iFMMCType != 0
		categoryNames[0] = "GameType" 
		SWITCH data.iFMMCType
			CASE FMMC_TYPE_SANDBOX_ACTIVITY
				iNumCategories = 1
				uniqueIdentifiers[0] = "TUNER_SANDB_SVAR"
				IF data.iSubType = ENUM_TO_INT(SAS_HH_LOCATION_1)
				OR data.iSubType = ENUM_TO_INT(SAS_HH_LOCATION_2)
					uniqueIdentifiers[0] += ENUM_TO_INT(SAS_HH_LOCATION_1)
				ELSE
					uniqueIdentifiers[0] += data.iSubType
				ENDIF
				PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD Writing to SC Leaderboard for Sandbox Activity: ",uniqueIdentifiers[0])	
			BREAK	
			CASE FMMC_TYPE_STREET_RACE_SERIES
			CASE FMMC_TYPE_PURSUIT_SERIES
				iNumCategories = 2
				categoryNames[1] = "Mission" 
				IF data.iFMMCType = FMMC_TYPE_STREET_RACE_SERIES
					uniqueIdentifiers[0] = "TUNER_SRS"
				ELSE
					uniqueIdentifiers[0] = "TUNER_PS"
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(data.tlOverwriteUGCID)
					PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD: Overwriting UGC content ID to: ",data.tlOverwriteUGCID)
					uniqueIdentifiers[1] = data.tlOverwriteUGCID
				ELSE
					uniqueIdentifiers[1] = g_FMMC_STRUCT.tl31LoadedContentID
				ENDIF
				PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD Writing to SC Leaderboard for Tuner Race type: ",data.iFMMCType," content ID: ",uniqueIdentifiers[0])	
			BREAK
		ENDSWITCH
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_TUNER_RACES,uniqueIdentifiers,categoryNames,iNumCategories) //LEADERBOARD_FREEMODE_TUNER_CAR_CLUB
			//TUNER_SANDB_SVAR2
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, -data.iBestTime, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD score (neg best time) = ", -data.iBestTime)
			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, data.iBestTime, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD best time = ",data.iBestTime)
			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_WINS, data.iWins, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD wins =  ",data.iWins) 
			
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_ID, data.iVehModel, 0)
			PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD data.iVehModel = ", data.iVehModel)
			
			IF data.bCustomVeh
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CUSTOM_VEHICLE, 1, 0)
				PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD custom vehicle ")
			ELSE
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CUSTOM_VEHICLE, 0, 0)
				PRINTLN("WRITE_TO_CAR_MEET_MEMBERSHIP_LEADERBOARD NOT custom vehicle ")
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

CONST_INT CM_SCLB_BS_SETUP_READ 			0
CONST_INT CM_SCLB_BS_RUNNING_LB 			1
CONST_INT CM_SCLB_BS_NOT_SAFE_TO_DISPLAY	2
CONST_INT CM_SCLB_BS_STARTED_RUNNING		3

CONST_INT CM_SCLB_TRIGGER_CAREER 	0
CONST_INT CM_SCLB_TRIGGER_SANDBOX	1
CONST_INT CM_SCLB_TRIGGER_SERIES 	2

CONST_INT CM_SCLB_TIME_TRIAL		0
CONST_INT CM_SCLB_CHECKPOINT_DASH	1
CONST_INT CM_SCLB_HEAD_TO_HEAD1		2
//CONST_INT CM_SCLB_HEAD_TO_HEAD2		3

CONST_INT CM_SCLB_SERIES_STREET_RACES		0
CONST_INT CM_SCLB_SERIES_PURSUIT			1

STRUCT CAR_MEET_SCLB_DATA
	INT iBS
	SC_LEADERBOARD_CONTROL_STRUCT scLB_control
	SCALEFORM_INDEX scLB_ScaleformID[2]
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformStruct
	INT iFMMCType
	INT iSubType
	TEXT_LABEL_31 FileName
	TEXT_LABEL_63 MissionName
	INT iTriggerType
	INT iCurrentSelection
ENDSTRUCT

FUNC BOOL IS_SAFE_TO_DISPLAY_SCLB_IN_CAR_MEET(CAR_MEET_SCLB_DATA &cmSCLBData)
	IF g_BG_ForceCleanupCarMeetLB
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(cmSCLBData.iBS,CM_SCLB_BS_NOT_SAFE_TO_DISPLAY)
		PRINTLN("IS_SAFE_TO_DISPLAY_SCLB_IN_CAR_MEET: FALSE cmSCLBData.iBS,CM_SCLB_BS_NOT_SAFE_TO_DISPLAY ")
		RETURN FALSE
	ENDIF
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("IS_SAFE_TO_DISPLAY_SCLB_IN_CAR_MEET: FALSE NOT IS_SKYSWOOP_AT_GROUND() ")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("IS_SAFE_TO_DISPLAY_SCLB_IN_CAR_MEET: FALSE IS_PLAYER_SWITCH_IN_PROGRESS")
		RETURN FALSE
	ENDIF
	IF IS_PAUSE_MENU_ACTIVE_EX()
		PRINTLN("IS_SAFE_TO_DISPLAY_SCLB_IN_CAR_MEET: FALSE IS_PAUSE_MENU_ACTIVE_EX")
		RETURN FALSE
	ENDIF
	//add any conditions here
	RETURN TRUE
ENDFUNC

PROC GET_CAR_MEET_SC_LEADERBOARD_UGC_DATA(INT iFMMCType,INT iArrayID,TEXT_LABEL_31 &fileName,TEXT_LABEL_63 &missionName)
	INT iArray
	INT iContentID
	
	IF iFMMCType = FMMC_TYPE_PURSUIT_SERIES
		iContentID = g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[ciCV2_SERIES_PURSUIT][iArrayID]
	ELIF iFMMCType = FMMC_TYPE_STREET_RACE_SERIES
		iContentID = g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[ciCV2_SERIES_STREET_RACE][iArrayID]
	ELSE
		fileName = ""
		missionName = ""
		EXIT
	ENDIF
	iArray = GET_CONTENT_ID_INDEX(iContentID)
	IF iArray != -1
		fileName = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlName
		missionName = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlMissionName
		EXIT
	ENDIF
ENDPROC

CONST_INT CAR_MEET_SCLB_RESET_NEW_BOARD 100

PROC CLEANUP_CAR_MEET_SC_LEADERBOARD(CAR_MEET_SCLB_DATA &cmSCLBData,INT iReason = 0)
	PRINTLN("CLEANUP_CAR_MEET_SC_LEADERBOARD: called for iReason = ",iReason)
	IF iReason = CAR_MEET_SCLB_RESET_NEW_BOARD
		scLB_DisplayedData.bResetTriggered = TRUE
		CLEAR_BIT(cmSCLBData.scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
		CLEAR_BIT(cmSCLBData.scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
		CLEAR_BIT(cmSCLBData.scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
		scLB_DisplayedData.iReadFailedBS = 0
		SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(cmSCLBData.scLB_Control)
		//CLEANUP_SOCIAL_CLUB_LEADERBOARD(cmSCLBData.scLB_control,TRUE)
		CLEAR_BIT(cmSCLBData.iBS,CM_SCLB_BS_SETUP_READ)
	ELSE
		CLEAR_BIT(cmSCLBData.iBS, CM_SCLB_BS_RUNNING_LB)
		scLB_DisplayedData.bResetTriggered = TRUE
		IF IS_BIT_SET(cmSCLBData.scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
		OR IS_BIT_SET(cmSCLBData.scLB_Control.ibs,SC_LEADERBOARD_CONTROL_SETUP_LOADING_MENU)
			IF HAS_SCALEFORM_MOVIE_LOADED(cmSCLBData.scLB_ScaleformID[0])
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(cmSCLBData.scLB_ScaleformID[0])
			ENDIF
			IF HAS_SCALEFORM_MOVIE_LOADED(cmSCLBData.scLB_ScaleformID[1])
				BEGIN_SCALEFORM_MOVIE_METHOD(cmSCLBData.scLB_ScaleformID[1], "CLEAR_ALL")
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(cmSCLBData.scaleformStruct)
		ENDIF
		CLEANUP_SOCIAL_CLUB_LEADERBOARD(cmSCLBData.scLB_control,TRUE)
		cmSCLBData.iBS = 0
	ENDIF
	
ENDPROC

PROC RUN_CAR_MEET_SC_LEADERBOARD(CAR_MEET_SCLB_DATA &cmSCLBData)

	IF IS_BIT_SET(cmSCLBData.iBS, CM_SCLB_BS_RUNNING_LB)
		IF IS_SAFE_TO_DISPLAY_SCLB_IN_CAR_MEET(cmSCLBData)
			// Do any leaderboard per-frame processing
			IF NOT IS_BIT_SET(cmSCLBData.iBS,CM_SCLB_BS_SETUP_READ)
				SWITCH cmSCLBData.iTriggerType
					CASE CM_SCLB_TRIGGER_CAREER
						cmSCLBData.iFMMCType = SCLB_TYPE_CAR_MEET_MEMBERSHIP
					BREAK
					CASE CM_SCLB_TRIGGER_SANDBOX
						cmSCLBData.iFMMCType = FMMC_TYPE_SANDBOX_ACTIVITY
						SWITCH cmSCLBData.iCurrentSelection
							CASE CM_SCLB_TIME_TRIAL
								cmSCLBData.iSubType = ENUM_TO_INT(SAS_TT_LOCATION_1)
							BREAK
							CASE CM_SCLB_CHECKPOINT_DASH
								cmSCLBData.iSubType = ENUM_TO_INT(SAS_CD_LOCATION_1)
							BREAK
							CASE CM_SCLB_HEAD_TO_HEAD1
								cmSCLBData.iSubType = ENUM_TO_INT(SAS_HH_LOCATION_1) //just one?
								//SAS_HH_LOCATION_2
							BREAK
//							CASE CM_SCLB_HEAD_TO_HEAD2
//								cmSCLBData.iSubType = ENUM_TO_INT(SAS_HH_LOCATION_2)
//								//SAS_HH_LOCATION_2
//							BREAK
						ENDSWITCH
					BREAK
					CASE CM_SCLB_TRIGGER_SERIES
						SWITCH cmSCLBData.iCurrentSelection
							CASE CM_SCLB_SERIES_STREET_RACES
								cmSCLBData.iFMMCType = FMMC_TYPE_STREET_RACE_SERIES
							BREAK
							CASE CM_SCLB_SERIES_PURSUIT
								cmSCLBData.iFMMCType = FMMC_TYPE_PURSUIT_SERIES
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
				
				GET_CAR_MEET_SC_LEADERBOARD_UGC_DATA(cmSCLBData.iFMMCType,cmSCLBData.iSubType,cmSCLBData.FileName,cmSCLBData.MissionName)
				PRINTLN("RUN_CAR_MEET_SC_LEADERBOARD: cmSCLBData.iFMMCType = ",cmSCLBData.iFMMCType," cmSCLBData.iSubType = ",cmSCLBData.iSubType,
												" cmSCLBData.FileName = ",cmSCLBData.FileName, " cmSCLBData.MissionName = ",cmSCLBData.MissionName)
				SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(cmSCLBData.scLB_control, cmSCLBData.iFMMCType,cmSCLBData.FileName,cmSCLBData.MissionName,cmSCLBData.iSubType)
				SET_BIT(cmSCLBData.iBS,CM_SCLB_BS_SETUP_READ)
				DRAW_SOCIAL_CLUB_LEADERBOARD(cmSCLBData.scLB_control,cmSCLBData.scLB_ScaleformID) 
			ELSE
				DRAW_SOCIAL_CLUB_LEADERBOARD(cmSCLBData.scLB_control,cmSCLBData.scLB_ScaleformID) 
				SPRITE_PLACEMENT ScaleformSprite
				ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
				IF NOT IS_BIT_SET(cmSCLBData.scLB_control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(cmSCLBData.scaleformStruct)
					SET_BIT(cmSCLBData.scLB_control.ibs,SC_LEADERBOARD_CONTROL_SETUP_BUTTONS)
						
					IF IS_BIT_SET(cmSCLBData.scLB_control.ibs,SC_LEADERBOARD_CONTROL_SETUP_DATA_MENU)
					AND scLB_DisplayedData.iSectionEntries[0] > 0
					AND cmSCLBData.scLB_control.navigation.iCurrentVertSel >= 0
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT,"SCLB_PROFILE",cmSCLBData.scaleformStruct, TRUE)
					ENDIF
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL,"BB_BACK",cmSCLBData.scaleformStruct, TRUE)
					IF cmSCLBData.iTriggerType = CM_SCLB_TRIGGER_SANDBOX
						SWITCH cmSCLBData.iCurrentSelection
							CASE CM_SCLB_TIME_TRIAL
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"SCLB_T_CD",cmSCLBData.scaleformStruct)
							BREAK
							CASE CM_SCLB_CHECKPOINT_DASH
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"SCLB_T_HH1",cmSCLBData.scaleformStruct)
							BREAK
							CASE CM_SCLB_HEAD_TO_HEAD1
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"SCLB_T_TT",cmSCLBData.scaleformStruct)
							BREAK
						ENDSWITCH
					ELIF cmSCLBData.iTriggerType = CM_SCLB_TRIGGER_SERIES
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"SCLB_B_NEXT",cmSCLBData.scaleformStruct)
						SWITCH cmSCLBData.iCurrentSelection
							CASE CM_SCLB_SERIES_STREET_RACES
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"SCLB_T_PURSA",cmSCLBData.scaleformStruct)
							BREAK
							CASE CM_SCLB_SERIES_PURSUIT
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"SCLB_T_SRSA",cmSCLBData.scaleformStruct)
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(cmSCLBData.scLB_ScaleformID[1], ScaleformSprite, cmSCLBData.scaleformStruct)
			ENDIF
//							IF scLB_control.ReadDataStruct.m_LeaderboardId = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE)
//								SWITCH iState
//									CASE 0 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB7a",scaleformStruct)
//									BREAK
//									CASE 1 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB7b",scaleformStruct)
//									BREAK
//									CASE 2 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X),"FMMC_COR_SCLB7c",scaleformStruct)
//									BREAK
//								ENDSWITCH
//								SWITCH iRangeWeap
//									CASE 0 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_SMG",scaleformStruct) //off by one as this is for NEXT selection not current
//									BREAK
//							        CASE 1 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_ASSAULT",scaleformStruct) //off by one as this is for NEXT selection not current
//									BREAK
//							        CASE 2 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_SHOTGUN",scaleformStruct) //off by one as this is for NEXT selection not current
//									BREAK
//							        CASE 3 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_LMG",scaleformStruct) //off by one as this is for NEXT selection not current
//									BREAK
//							        CASE 4 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_HEAVY",scaleformStruct) //off by one as this is for NEXT selection not current
//									BREAK
//							        CASE 5 
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y),"HUD_MG_PISTOL",scaleformStruct) //off by one as this is for NEXT selection not current
//									BREAK
//								ENDSWiTCH
//							ENDIF

			IF cmSCLBData.iTriggerType = CM_SCLB_TRIGGER_SANDBOX
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					cmSCLBData.iCurrentSelection++
					IF cmSCLBData.iCurrentSelection > CM_SCLB_HEAD_TO_HEAD1
						cmSCLBData.iCurrentSelection = CM_SCLB_TIME_TRIAL
					ENDIF
					
					CLEANUP_CAR_MEET_SC_LEADERBOARD(cmSCLBData,2)//,CAR_MEET_SCLB_RESET_NEW_BOARD)
				ENDIF
			ELIF cmSCLBData.iTriggerType = CM_SCLB_TRIGGER_SERIES
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					cmSCLBData.iSubType++
					IF cmSCLBData.iCurrentSelection = CM_SCLB_SERIES_STREET_RACES
						IF cmSCLBData.iSubType > g_sMPTunables.iFmCoronaPlaylistProffesionalLength[ciCV2_SERIES_STREET_RACE]
							cmSCLBData.iSubType = 0
						ENDIF
					ELIF cmSCLBData.iCurrentSelection = CM_SCLB_SERIES_PURSUIT
						IF cmSCLBData.iSubType > g_sMPTunables.iFmCoronaPlaylistProffesionalLength[ciCV2_SERIES_PURSUIT]
							cmSCLBData.iSubType = 0
						ENDIF
					ENDIF
					CLEANUP_CAR_MEET_SC_LEADERBOARD(cmSCLBData,3)//,CAR_MEET_SCLB_RESET_NEW_BOARD)
				ENDIF
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
					cmSCLBData.iCurrentSelection++
					IF cmSCLBData.iCurrentSelection > CM_SCLB_SERIES_PURSUIT
						cmSCLBData.iCurrentSelection = CM_SCLB_SERIES_STREET_RACES
					ENDIF
					cmSCLBData.iSubType = 0
					CLEANUP_CAR_MEET_SC_LEADERBOARD(cmSCLBData,4)//,CAR_MEET_SCLB_RESET_NEW_BOARD)
				ENDIF
			ENDIF
			// XBOX B to go back to wall control
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				CLEANUP_CAR_MEET_SC_LEADERBOARD(cmSCLBData,0)
			ENDIF 
		ELSE
			CLEANUP_CAR_MEET_SC_LEADERBOARD(cmSCLBData,1)
		ENDIF
	ENDIF
ENDPROC
#ENDIF
//

//EOF
