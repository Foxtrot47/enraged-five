//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        socialclub_leaderboard_hud.sch																	//
// Description: Controls opening/closing of garage and warping player out									//
// Written by:  Conor McGuire																				//
// Date: 2013-03-05 (ISO 8601) 																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
USING "leader_board_common.sch"
USING "commands_stats.sch"
USING "net_common_functions.sch"
USING "script_conversion.sch"
USING "net_car_club_rep.sch"

ENUM DISPLAY_TYPES
	DISPLAY_TYPE_MINIGAME = 0,
	DISPLAY_TYPE_MULTIPLAYER
ENDENUM

ENUM SCLB_SECTIONS
	SECTION_WORLD = 0,
	SECTION_FRIEND,
	SECTION_CREW
ENDENUM

CONST_INT SCLB_SLOT_STATE_BIT_NONE			0
CONST_INT SCLB_SLOT_STATE_BIT_IS_PLAYER		1
CONST_INT SCLB_SLOT_STATE_BIT_OUTLINE		2
CONST_INT SCLB_SLOT_STATE_BIT_SELECTED		3
CONST_INT SCLB_SLOT_STATE_BIT_WORLD			4
CONST_INT SCLB_SLOT_STATE_BIT_FRIENDS		5
CONST_INT SCLB_SLOT_STATE_BIT_CREW			6
CONST_INT SCLB_SLOT_STATE_BIT_DESCRIPTION	7

ENUM SCLB_SLOT_LAYOUTS
	SLOT_LAYOUT_LABEL_NO_ICON = 0,	// Just a label, no icon
	SLOT_LAYOUT_LABEL,				// Label and Icon, no columns
	SLOT_LAYOUT_1_STATS,			// Label and Icon, one column with label and icon
	SLOT_LAYOUT_2_STATS,			// Label and Icon, two columns with labels and icons
	SLOT_LAYOUT_3_STATS,			// Label and Icon, three columns with labels and icons
	SLOT_LAYOUT_4_STATS,				// Label and Icon, four columns with labels and icons
	SLOT_LAYOUT_5_STATS				// Label and Icon, five columns with labels and icons
ENDENUM

ENUM SCLB_ICONS
	ICON_NONE = -1,
	ICON_WORLD_RANKING = 0,	// 0
	ICON_FRIENDS,
	ICON_CREW,
	ICON_TIME,
	ICON_LAP,
	ICON_POSITION,		// 5
	ICON_KILLS,
	ICON_DEATHS,
	ICON_GUN,
	ICON_VEHICLE,
	ICON_TARGET,		// 10
	ICON_CROSSHAIR,
	ICON_DOLLAR,
	ICON_POINT,
	ICON_CROWN,
	ICON_TROPHY,		// 15
	ICON_WINNER			= 15,
	ICON_ARM,
	ICON_DART,
	ICON_GOLF_CLUB,
	ICON_GOLF,
	ICON_HOOKER,		//20
	ICON_HEART,
	ICON_HEART_BROKEN,
	ICON_TENNIS_RACKET,
	ICON_TENNIS_BALL,
	ICON_ROBBER_MASK,	//25
	ICON_LOSER,
	ICON_SCALE
ENDENUM

ENUM SCLB_DATA_TYPE
	SCLB_DATA_TYPE_NONE = 0,
	SCLB_DATA_TYPE_TIME,
	SCLB_DATA_TYPE_TIME_NEG_B4_DIS,
	SCLB_DATA_TYPE_VEHICLE,
	SCLB_DATA_TYPE_FLOAT,
	SCLB_DATA_TYPE_INT,
	SCLB_DATA_TYPE_GOLD_TIME,
	SCLB_DATA_TYPE_INT_NEG_B4_DIS,
	SCLB_DATA_TYPE_GUN,
	SCLB_DATA_TYPE_TIME_NO_MS,
	SCLB_DATA_TYPE_TIME_NEG_B4_DIS_NO_MS,
	SCLB_DATA_TYPE_TIME_POSITIVE,
	SCLB_DATA_TYPE_FLOAT_TO_INT,
	SCLB_DATA_TYPE_INT_DIST_WITH_CONV_FT,
	SCLB_DATA_TYPE_INT_DIST_WITH_CONV_FT_NEG_B4_DIS,
	SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE,
	SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE_NEG_B4_DIS,
	SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_FT,
	SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_FT_NEG_B4_DIS,
	SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE,
	SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE_NEG_B4_DIS,
	SCLB_DATA_TYPE_ARENA_TITLE,
	SCLB_DATA_TYPE_CC_MEMBER_RANK
ENDENUM

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////   IN-VALID COMMANDS BELOW DO NOT USE THESE THEY ARE OLD COMMANDS !////////////////////////////////////////////////

//INVALID NOW DO NOT USE!!!
FUNC TEXT_LABEL_15	SC_LEADERBOARD_MAKE_INT_PRETTY(INT iUglyNumber)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	TEXT_LABEL_15 sPrettyNumber
	INT millions, thousands, hundreds
	
	millions = iUglyNumber / 1000000
	thousands = iUglyNumber % 1000000 / 1000
	hundreds = iUglyNumber % 1000
	
	// Set Millions and add padding to Thousands if needed
	IF millions > 0
		sPrettyNumber += millions
		sPrettyNumber += ","
		IF thousands < 100
			sPrettyNumber += "0"
		ENDIF
		IF thousands < 10
			sPrettyNumber += "0"
		ENDIF
	ENDIF
	// Set Thousands and add padding to Hundreds if needed
	IF thousands > 0
		sPrettyNumber += thousands
		sPrettyNumber += ","
		IF hundreds < 100
			sPrettyNumber += "0"
		ENDIF
		IF hundreds < 10
			sPrettyNumber += "0"
		ENDIF
	ENDIF
	// Set Hundreds
	sPrettyNumber += hundreds
	
	// TODO: Add support for ordinals
	
	RETURN (sPrettyNumber)
ENDFUNC

//INVALID NOW DO NOT USE!!!
FUNC TEXT_LABEL_15 SC_LEADERBOARD_MAKE_TIME_LABEL(INT iLeftNum, INT iRightNum, BOOL bDispColon=TRUE, BOOL bDispMinSec=TRUE)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	TEXT_LABEL_15 texLabel
	texLabel = ""
	
	// Add minutes to the label
	IF iLeftNum < 10
		texLabel += "0"
	ENDIF
	texLabel += iLeftNum
	
	// Add middle demarcation, a colon or an 'm'
	IF bDispColon
		texLabel += ":"
	ELIF bDispMinSec
		texLabel += "m "
	ELSE
		texLabel += "h "
	ENDIF
	
	// Add seconds to the label
	IF iRightNum < 10
		texLabel += "0"
	ENDIF
	texLabel += iRightNum
	
	// Add final demarcation if needed
	IF NOT bDispColon AND bDispMinSec
		texLabel += "s"
	ELIF NOT bDispColon AND NOT bDispMinSec
		texLabel += "m"
	ENDIF
	
	RETURN texLabel
ENDFUNC

//INVALID NOW DO NOT USE!!!
FUNC TEXT_LABEL_15 SC_LEADERBOARD_MAKE_TIME_LABEL_BETTER(FLOAT fMS, BOOL bDispColon=TRUE, BOOL bDispMinSec=TRUE)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	TEXT_LABEL_15 texLabel
	texLabel = ""
	INT iHours
	INT iMinutes
	INT iSec
	INT iMilSec
	IF fMS > 0
		INT iMSLeft = ROUND(fMS)
		PRINTLN("SC_LEADERBOARD_MAKE_TIME_LABEL_BETTER	starting MS :", iMSLeft)
		iHours = FLOOR(TO_FLOAT(iMSLeft/(1000*60*60)))
		PRINTLN("SC_LEADERBOARD_MAKE_TIME_LABEL_BETTER	Hours :", iHours)
		
		iMSLeft = iMSLeft- (iHours*(1000*60*60))
		iMinutes = FLOOR(TO_FLOAT(iMSLeft/(1000*60)))
		PRINTLN("SC_LEADERBOARD_MAKE_TIME_LABEL_BETTER	minutes :", iMinutes)
		
		iMSLeft = iMSLeft- (iMinutes*(1000*60))
		iSec = FLOOR(TO_FLOAT(iMSLeft/1000))
		PRINTLN("SC_LEADERBOARD_MAKE_TIME_LABEL_BETTER	second :", iSec)
		
		iMSLeft = iMSLeft- (iSec*1000)
		iMilSec = iMSLeft
		PRINTLN("SC_LEADERBOARD_MAKE_TIME_LABEL_BETTER	ms :", iMilSec)
		
		//using text labels for translations
		IF bDispColon
			IF iHours > 0
				texLabel += iHours
				texLabel += ":"
				IF iMinutes < 10
					texLabel += "0"
					texLabel += iMinutes
				ELSE
					texLabel += iMinutes
				ENDIF
				texLabel += ":"
				IF iSec < 10
					texLabel += "0"
					texLabel += iSec
				ELSE
					texLabel += iSec
				ENDIF
			ELSE
				IF iMinutes < 10
					texLabel += "0"
					texLabel += iMinutes
				ELSE
					texLabel += iMinutes
				ENDIF
				texLabel += ":"
				IF iSec < 10
					texLabel += "0"
					texLabel += iSec
				ELSE
					texLabel += iSec
				ENDIF
				texLabel += ":"	
				IF iMilSec > 100
					texLabel += iMilSec
				ELSE	
					IF iMilSec > 10
						texLabel += "0"
						texLabel += iMilSec
					ELSE
						texLabel += "00"
						texLabel += iMilSec
					ENDIF
				ENDIF
			ENDIF
		ELIF bDispMinSec
			IF fMS >= 1000*60*60
				texLabel += iHours
				texLabel += "H"//GET_STRING_FROM_TEXT_FILE("TIME_H_LC")
				texLabel += " "
				texLabel += iMinutes
				texLabel += "M"//GET_STRING_FROM_TEXT_FILE("TIME_M_LC")
				texLabel += " "
				texLabel += iSec
				texLabel += "S"//GET_STRING_FROM_TEXT_FILE("TIME_S_LC")
			ELSE
				texLabel += iMinutes
				texLabel += "m"//GET_STRING_FROM_TEXT_FILE("TIME_M_LC")
				texLabel += " "
				texLabel += iSec
				texLabel += "s"//GET_STRING_FROM_TEXT_FILE("TIME_S_LC")
				texLabel += " "
				texLabel += iMilSec
				texLabel += "ms"// GET_STRING_FROM_TEXT_FILE("TIME_MS_LC")
			ENDIF
		ELSE
			texLabel += ROUND(fMS)
		ENDIF
	ELSE
		texLabel = "---"
	ENDIF
	
	RETURN texLabel
ENDFUNC

//INVALID NOW DO NOT USE!!!
PROC CLEAR_SC_LEADERBOARD_SLOT_ENTRIES(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "CLEAR_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC REMOVE_ALL_SC_LEADERBOARD_HEADERS_AND_SLOTS(SCALEFORM_INDEX uiLeaderboard)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "REMOVE_ALL")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC OUTLINE_SC_LEADERBOARD_SLOT(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, BOOL bIsOutlined)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "OUTLINE_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsOutlined)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC HIGHLIGHT_SC_LEADERBOARD_SLOT(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, BOOL bIsHighlighted)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "HIGHLIGHT_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsHighlighted)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_SLOT_AS_SELECTED(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, BOOL bIsHighlighted)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "HIGHLIGHT_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsHighlighted)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_HEADER(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iHeaderID, SCLB_SLOT_LAYOUTS eLayout, STRING sCol1, STRING sCol2, STRING sCol3, STRING sCol4, SCLB_ICONS eIcon1=ICON_NONE, SCLB_ICONS eIcon2=ICON_NONE, SCLB_ICONS eIcon3=ICON_NONE, SCLB_ICONS eIcon4=ICON_NONE)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	STRING sHeader = ""
	SCLB_ICONS eHeaderIcon
	NETWORK_CLAN_DESC clanDesc
	TEXT_LABEL_63 sLiteralHeader
	BOOL bLiteralHeader
	IF eSection = SECTION_WORLD
		IF eLayout > SLOT_LAYOUT_LABEL_NO_ICON
			sHeader = "SCLB_GLOBAL"
		ELSE
			sHeader = "SCLB_NO_GLOBAL"
		ENDIF
		eHeaderIcon = ICON_WORLD_RANKING
	ELIF eSection = SECTION_FRIEND
		IF eLayout > SLOT_LAYOUT_LABEL_NO_ICON
			sHeader = "SCLB_FRIENDS"
		ELSE
			sHeader = "SCLB_NO_FRNDS"
		ENDIF
		eHeaderIcon = ICON_FRIENDS
	ELIF eSection = SECTION_CREW
		IF eLayout > SLOT_LAYOUT_LABEL_NO_ICON
			IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_CLAN_SERVICE_IS_VALID()
				GAMER_HANDLE gamerHandle
				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
					NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),gamerHandle)
					IF NOT IS_STRING_NULL_OR_EMPTY(clanDesc.ClanName)
						sHeader = "STRING"
						sLiteralHeader = clanDesc.ClanName
						bLiteralHeader = TRUE
					ELSE
						sHeader = "SCLB_CREW"
					ENDIF
				ELSE
					sHeader = "SCLB_CREW"
				ENDIF
			ELSE
				sHeader = "SCLB_CREW"
			ENDIF
		ELSE
			IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_CLAN_SERVICE_IS_VALID()
				GAMER_HANDLE gamerHandle
				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
					NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),gamerHandle)
					IF NOT IS_STRING_NULL_OR_EMPTY(clanDesc.ClanName)
						sHeader = "SCLB_NO_CREWb"
						sLiteralHeader = clanDesc.ClanName
						bLiteralHeader = TRUE
					ELSE
						sHeader = "SCLB_NO_CREWc"
					ENDIF
				ELSE
					sHeader = "SCLB_NO_CREWa"
				ENDIF
			ELSE
				sHeader = "SCLB_NO_CREWa"
			ENDIF
		ENDIF
		eHeaderIcon = ICON_CREW
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_HEADER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))	// Tell the SF which kind of header this is
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeaderID)			// Tell it which slot it appears in
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eLayout))	// Tell the SF how many columns we have
		IF bLiteralHeader
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sLiteralHeader)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Begin sending overload variables
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eHeaderIcon))
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sCol1)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eIcon1))
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sCol2)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eIcon2))
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sCol3)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eIcon3))
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sCol4)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eIcon4))
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_SLOT_0_COLUMNS(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, STRING sPlayer)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))	// Tell the SF what the section is
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)				// Tell the SF which slot to put the values in
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SLOT_LAYOUT_LABEL))	// Tell the SF which layout to use
		
		// Begin sending overload variables
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_SLOT_1_COLUMNS(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, STRING sPlayer, TEXT_LABEL_15 texRank)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))	// Tell the SF what the section is
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)				// Tell the SF which slot to put the values in
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SLOT_LAYOUT_1_STATS))	// Tell the SF which layout to use
		
		// Begin sending overload variables
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRank)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_SLOT_2_COLUMNS(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, STRING sPlayer, TEXT_LABEL_15 texRankingVal, TEXT_LABEL_15 texRank)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))	// Tell the SF what the section is
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)				// Tell the SF which slot to put the values in
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SLOT_LAYOUT_2_STATS))	// Tell the SF which layout to use
		
		// Begin sending overload variables
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRankingVal)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRank)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_SLOT_3_COLUMNS(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, STRING sPlayer, TEXT_LABEL_15 texRankingVal, TEXT_LABEL_15 texMiddleText, TEXT_LABEL_15 texRank)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))	// Tell the SF what the section is
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)				// Tell the SF which slot to put the values in
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SLOT_LAYOUT_3_STATS))	// Tell the SF which layout to use
		
		// Begin sending overload variables
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRankingVal)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texMiddleText)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRank)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//INVALID NOW DO NOT USE!!!
PROC SET_SC_LEADERBOARD_SLOT_4_COLUMNS(SCALEFORM_INDEX uiLeaderboard, SCLB_SECTIONS eSection, INT iSlotID, STRING sPlayer, TEXT_LABEL_15 texRankingVal, TEXT_LABEL_15 texMiddleText1, TEXT_LABEL_15 texMiddleText2, TEXT_LABEL_15 texRank)
	#IF IS_DEBUG_BUILD
	PRINTLN("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	SCRIPT_ASSERT("THIS COMMAND IS NOT VALID DO NOT USE IT! SEE CONOR FOR INFORMATION ABOUT COMMANDS TO USE.")
	#ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSection))	// Tell the SF what the section is
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)				// Tell the SF which slot to put the values in
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SLOT_LAYOUT_4_STATS))	// Tell the SF which layout to use
		
		// Begin sending overload variables
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRankingVal)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texMiddleText1)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texMiddleText2)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(texRank)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////    VALID COMMANDS BELOW //////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    For displaying SP Shooting Range weapon names
/// PARAMS:
///    iDataInt - Integer off the SP Range leaderboards, Range uploads a specific INT for each weapon
/// RETURNS:
///    WEAPON_TYPE based off the integer passed in
FUNC WEAPON_TYPE GET_RANGE_GUN_NAME_FROM_INT(INT iDataInt)
	//CGtoNG only
	IF iDataInt = 600
		RETURN WEAPONTYPE_DLC_RAILGUN
		
	ELIF iDataInt = 500
		RETURN WEAPONTYPE_MINIGUN	
		
	ELIF iDataInt = 400
		RETURN WEAPONTYPE_MG
	ELIF iDataInt = 401
		RETURN WEAPONTYPE_COMBATMG
	ELIF iDataInt = 402
		RETURN WEAPONTYPE_DLC_ASSAULTMG
		
	ELIF iDataInt = 300
		RETURN WEAPONTYPE_ASSAULTRIFLE
	ELIF iDataInt = 301
		RETURN WEAPONTYPE_CARBINERIFLE
	ELIF iDataInt = 302
		RETURN WEAPONTYPE_ADVANCEDRIFLE
	ELIF iDataInt = 303
		RETURN WEAPONTYPE_DLC_HEAVYRIFLE
		
	ELIF iDataInt = 200
		RETURN WEAPONTYPE_PUMPSHOTGUN
	ELIF iDataInt = 201
		RETURN WEAPONTYPE_SAWNOFFSHOTGUN
	ELIF iDataInt = 202
		RETURN WEAPONTYPE_ASSAULTSHOTGUN
	ELIF iDataInt = 203
		RETURN WEAPONTYPE_DLC_BULLPUPSHOTGUN
		
	ELIF iDataInt = 100
		RETURN WEAPONTYPE_MICROSMG
	ELIF iDataInt = 101
		RETURN WEAPONTYPE_SMG
	ELIF iDataInt = 102
		RETURN WEAPONTYPE_DLC_ASSAULTSMG
		
	ELIF iDataInt = 0
		RETURN WEAPONTYPE_PISTOL
	ELIF iDataInt = 1
		RETURN WEAPONTYPE_COMBATPISTOL
	ELIF iDataInt = 2
		RETURN WEAPONTYPE_APPISTOL
	ELIF iDataInt = 3
		RETURN WEAPONTYPE_DLC_PISTOL50
	ENDIF
	
//	Do I need to deal with WEAPONTYPE_DLC_SPECIALCARBINE in this function?

	RETURN WEAPONTYPE_INVALID
ENDFUNC


/// PURPOSE:
///    Request the scaleform movie for drawing the SC leaderboard
/// RETURNS:
///    Scaleform index of the SC leaderboard movie
FUNC SCALEFORM_INDEX REQUEST_SC_LEADERBOARD_UI()
	RETURN REQUEST_SCALEFORM_MOVIE("SC_LEADERBOARD")
ENDFUNC

/// PURPOSE:
///    Cleans up the SC leaderboard scaleform movie
/// PARAMS:
///    uiLeaderboard - 
PROC CLEANUP_SC_LEADERBOARD_UI(SCALEFORM_INDEX uiLeaderboard)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiLeaderboard)
ENDPROC

/// PURPOSE:
///    Draws the SC leaderboard movie to the screen
/// PARAMS:
///    uiLeaderboard - 
PROC DISPLAY_SC_LEADERBOARD_UI(SCALEFORM_INDEX uiLeaderboard)
	IF HAS_SCALEFORM_MOVIE_LOADED(uiLeaderboard)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiLeaderboard,255, 255, 255, 0)
	ELSE
		PRINTLN("Unable to draw scaleform! It hasn't loaded into memory yet!")
	ENDIF
ENDPROC


///----------------------------------------------------
///  generic commands below so values don't have be structured first i.e. time values can be passed as ints
///----------------------------------------------------

/// PURPOSE:
///    	Set the SC leaderboard title
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    sTitle - Pass "SCLB_TITLE" for default string of "Social Club Leaderboard"
PROC SET_SC_LEADERBOARD_TITLE(SCALEFORM_INDEX uiLeaderboard, STRING sTitle)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_TITLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sTitle)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the SC leaderboard display type
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    eDisplayType - Sets display type between Minigame (2/3 of Script UI grid) and Multiplayer (Full Screen)
PROC SET_SC_LEADERBOARD_DISPLAY_TYPE(SCALEFORM_INDEX uiLeaderboard, DISPLAY_TYPES eDisplayType = DISPLAY_TYPE_MINIGAME)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_DISPLAY_TYPE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eDisplayType))
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Used for the large panel title for Multiplayer Leaderboards
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    sTitle - No default values.
PROC SET_SC_LEADERBOARD_MULTIPLAYER_TITLE(SCALEFORM_INDEX uiLeaderboard, STRING sTitle,STRING sDriverCoDriverString = NULL, STRING sSubString = NULL, BOOL bSubStringLiteral = TRUE, INT iLapParameter = -1)
	PRINTLN("sTitle= ",sTitle)
	PRINTLN("sDriverCoDriverString= ",sDriverCoDriverString)
	PRINTLN("sSubString= ",sSubString)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_MULTIPLAYER_TITLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sTitle)
		IF NOT IS_STRING_NULL_OR_EMPTY(sDriverCoDriverString)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sDriverCoDriverString)
		ENDIF
		IF iLapParameter = -1
			IF bSubStringLiteral
				IF NOT IS_STRING_NULL_OR_EMPTY(sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sSubString)
				ENDIF
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sSubString)
				ENDIF
			ENDIF
		ELSE
			IF bSubStringLiteral
				IF NOT IS_STRING_NULL_OR_EMPTY(sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sSubString)
				ENDIF
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sSubString)
				ENDIF
			ENDIF
			ADD_TEXT_COMPONENT_INTEGER(iLapParameter)
		ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Set the titles of the columns
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    tl23_Position - Position title 
///    ColumnTitles - Title for each column
///    iColumns - Number of columns
PROC SET_SC_LEADERBOARD_COLUMN_TITLES(SCALEFORM_INDEX uiLeaderboard,STRING tl23_Position, TEXT_LABEL_23 &ColumnTitles[], INT iColumns)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_TITLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tl23_Position)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		INT i
		REPEAT iColumns i
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(ColumnTitles[i])
			END_TEXT_COMMAND_SCALEFORM_STRING()	
		ENDREPEAT
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the header of a give section
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    iSlotID - overall row on board
///    iSlotState - type of row
///    bEmpty - to indicate this section is empty and needs a text description for next row
PROC SET_SC_LEADERBOARD_SECTION_HEADER(SCALEFORM_INDEX uiLeaderboard,INT& iSlotID, INT iSlotStateValue, BOOL bEmpty, BOOL bNoOnlinePrivileges = FALSE)
	STRING sHeader = ""
	NETWORK_CLAN_DESC clanDesc
	TEXT_LABEL_63 sLiteralHeader
	BOOL bLiteralHeader
	PRINTLN("SET_SC_LEADERBOARD_SECTION_HEADER: iSlotID: ", iSlotID)
	PRINTLN("SET_SC_LEADERBOARD_SECTION_HEADER: iSlotStateValue: ", iSlotStateValue)
	PRINTLN("SET_SC_LEADERBOARD_SECTION_HEADER: bEmpty: ", bEmpty)
	PRINTLN("SET_SC_LEADERBOARD_SECTION_HEADER: bNoOnlinePrivileges: ",bNoOnlinePrivileges)
	IF IS_BIT_SET(iSlotStateValue, SCLB_SLOT_STATE_BIT_WORLD)
		//IF NOT bEmpty
			sHeader = "SCLB_GLOBAL"
		//ELSE
		//	sHeader = "SCLB_NO_GLOBAL"
		//ENDIF
	ELIF IS_BIT_SET(iSlotStateValue, SCLB_SLOT_STATE_BIT_FRIENDS)
		iSlotID++
		//IF NOT bEmpty
			sHeader = "SCLB_FRIENDS"
		//ELSE
		//	sHeader = "SCLB_NO_FRNDS"
		//ENDIF
	ELIF IS_BIT_SET(iSlotStateValue, SCLB_SLOT_STATE_BIT_CREW)
		iSlotID++
		//IF NOT bEmpty
			IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_CLAN_SERVICE_IS_VALID()
				IF (IS_PLAYSTATION_PLATFORM() OR IS_XBOX_PLATFORM())
				AND NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
					sHeader = "SCLB_CREW"
				ELSE
					GAMER_HANDLE gamerHandle
					gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
					IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
						NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),gamerHandle)
						IF NOT IS_STRING_NULL_OR_EMPTY(clanDesc.ClanName)
							sHeader = "STRING"
							sLiteralHeader = clanDesc.ClanName
							bLiteralHeader = TRUE
						ELSE
							sHeader = "SCLB_CREW"
						ENDIF
					ELSE
						sHeader = "SCLB_CREW"
					ENDIF
				ENDIF
			ELSE
				sHeader = "SCLB_CREW"
			ENDIF
//		ELSE
//			IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_CLAN_SERVICE_IS_VALID()
//				GAMER_HANDLE gamerHandle
//				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
//				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
//					NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),gamerHandle)
//					IF NOT IS_STRING_NULL_OR_EMPTY(clanDesc.ClanName)
//						sHeader = "SCLB_NO_CREWb"
//						sLiteralHeader = clanDesc.ClanName
//						bLiteralHeader = TRUE
//					ELSE
//						sHeader = "SCLB_NO_CREWc"
//					ENDIF
//				ELSE
//					sHeader = "SCLB_NO_CREWa"
//				ENDIF
//			ELSE
//				sHeader = "SCLB_NO_CREWa"
//			ENDIF
//		ENDIF
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotStateValue)
		IF bLiteralHeader
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sLiteralHeader)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	iSlotID++
	IF bNoOnlinePrivileges
		INT iTempSlotValue = 0
		SET_BIT(iTempSlotValue ,SCLB_SLOT_STATE_BIT_DESCRIPTION )
		BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTempSlotValue )
			IF NOT NETWORK_IS_SIGNED_ONLINE()
				
				//IF IS_XBOX360_VERSION()
					sHeader = "SCLB_NOT_ONL"
				//ELIF IS_PS3_VERSION()
				//OR IS_PLAYSTATION_PLATFORM()
				//	sHeader = "SCLB_NOT_ONLPS3"
				//ENDIF
			ELSE
				IF NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()
					sHeader = "SCLB_NO_ROS"
				ELSE
					IF scLB_DisplayedData.iReadFailedBS != 0
						sHeader = "SCLB_READ_FAIL"
					ELSE
						sHeader = "HUD_PERM"
					ENDIF
				ENDIF
			ENDIF
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
			END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		iSlotID++
	ELSE
		IF bEmpty
			IF IS_BIT_SET(iSlotStateValue, SCLB_SLOT_STATE_BIT_WORLD)
				//IF NOT bEmpty
				//	sHeader = "SCLB_GLOBAL"
				//ELSE
					sHeader = "SCLB_NO_GLOBAL"
				//ENDIF
			ELIF IS_BIT_SET(iSlotStateValue, SCLB_SLOT_STATE_BIT_FRIENDS)
			//iSlotState = SCLB_SLOT_STATE_FRIENDS
				//IF NOT bEmpty
				//	sHeader = "SCLB_FRIENDS"
				//ELSE
				IF NETWORK_GET_FRIEND_COUNT() > 0
					sHeader = "SCLB_NO_FRNDS"
				ELSE
					sHeader = "SCLB_NO_FRNDSb"
				ENDIF
				//ENDIF
			ELIF IS_BIT_SET(iSlotStateValue, SCLB_SLOT_STATE_BIT_CREW)
			//iSlotState = SCLB_SLOT_STATE_CREW
	//			IF NOT bEmpty
	//				IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_CLAN_SERVICE_IS_VALID()
	//					GAMER_HANDLE gamerHandle
	//					gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
	//					IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
	//						NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),gamerHandle)
	//						IF NOT IS_STRING_NULL_OR_EMPTY(clanDesc.ClanName)
	//							sHeader = "STRING"
	//							sLiteralHeader = clanDesc.ClanName
	//							bLiteralHeader = TRUE
	//						ELSE
	//							sHeader = "SCLB_CREW"
	//						ENDIF
	//					ELSE
	//						sHeader = "SCLB_CREW"
	//					ENDIF
	//				ELSE
	//					sHeader = "SCLB_CREW"
	//				ENDIF
	//			ELSE
				IF NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
					IF NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()
						IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_CLAN_SERVICE_IS_VALID()
							IF (IS_PLAYSTATION_PLATFORM() OR IS_XBOX_PLATFORM())
							AND NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
								sHeader = "SCLB_NO_CREWc"
							ELSE
								GAMER_HANDLE gamerHandle
								gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())	
								IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
									NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),gamerHandle)
									IF NOT IS_STRING_NULL_OR_EMPTY(clanDesc.ClanName)
										sHeader = "SCLB_NO_CREWb"
										sLiteralHeader = clanDesc.ClanName
										bLiteralHeader = TRUE
									ELSE
										sHeader = "SCLB_NO_CREWc"
									ENDIF
								ELSE
									sHeader = "SCLB_NO_CREWa"
								ENDIF
							ENDIF
						ELSE
							sHeader = "SCLB_NO_CREWa"
						ENDIF
					ELSE
						sHeader = "SCLB_NO_CREWe"
					ENDIF
				ELSE
					sHeader = "SCLB_NO_CREWd"
				ENDIF
				//ENDIF
			ENDIF
			INT iTempSlotValue = 0
			SET_BIT(iTempSlotValue ,SCLB_SLOT_STATE_BIT_DESCRIPTION )
			BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTempSlotValue )
				IF bLiteralHeader
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sLiteralHeader)
					END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sHeader)				// What the name of the header is
					END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
				ENDIF
			END_SCALEFORM_MOVIE_METHOD()
			iSlotID++
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set a text only line on leaderboard  
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    iSlotID - overall row on board
///    iSlotState - bitfield defining the type of row
///    sDescription - the text labelf of the text to display
PROC SET_SC_LEADERBOARD_SLOT_TEXT_DESCRIPTION_ONLY(SCALEFORM_INDEX uiLeaderboard,INT iSlotID, INT iCurrentSlotState,STRING sDescription)//,INT iColumns)
	SET_BIT(iCurrentSlotState ,SCLB_SLOT_STATE_BIT_DESCRIPTION )
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentSlotState)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sDescription)
		END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    To override the state of a particular slot i.e. hightlight a slow without changing data
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    iSlotID - overall row on board
///    iSlotState - bitfield defining the type of row
PROC SET_SC_LEADERBOARD_SLOT_FINAL_STATE(SCALEFORM_INDEX uiLeaderboard,INT iSlotID, INT iSlotStateValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT_STATE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotStateValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Starts a row on a leaderboard
/// PARAMS:
///    uiLeaderboard - scaleform index of the SC leaderboard
///    iSlotID - overall row on board
///    iSlotState - bitfield defining the type of row
///    iPosition - the rank of this p
///    playerName - the name of the player for this row
///    crewTag - the crew tag of the player in this row
PROC START_SC_LEADERBOARD_SLOT(SCALEFORM_INDEX uiLeaderboard,INT iSlotID, INT iSlotStateValue, INT iPosition,
							STRING playerName,STRING crewTag)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiLeaderboard, "SET_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotStateValue)
		IF iPosition > 0
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
				ADD_TEXT_COMPONENT_INTEGER(iPosition)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(playerName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(crewTag)
	//END_SCALEFORM_MOVIE_METHOD()
ENDPROC
/// PURPOSE:
///    Adds a float to the row begun with START_SC_LEADERBOARD_SLO
/// PARAMS:
///    displaySetup - the display setup details
///    iIndex - index of value in the display setup
///    fDataFloat - the float value
///    bValidData - true if this is a valid row versus empty data (local player always has a row it is filled with invalid data if there is no data for them)
PROC ADD_SC_LEADERBOARD_COLUMN_DATA_FLOAT(SC_LEADERBOARD_DISPLAY_SETUP displaySetup, INT iIndex, FLOAT fDataFloat, BOOL bValidData)
//	IF fDataFloat = HIGHEST_FLOAT
//		PRINTLN("ADD_SC_LEADERBOARD_COLUMN_DATA_FLOAT- Highest int setting invalid data")
//		bValidData = FALSE
//	ENDIF
	SWITCH INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex])
		CASE SCLB_DATA_TYPE_FLOAT
			IF bValidData
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_FLOAT(fDataFloat,2)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_FLOAT_TO_INT
			IF bValidData
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_INTEGER(FLOOR(fDataFloat))
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_FT
		CASE SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE
		CASE SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_FT_NEG_B4_DIS
		CASE SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE_NEG_B4_DIS
			IF bValidData
				IF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_FT_NEG_B4_DIS
				OR INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE_NEG_B4_DIS
					fDataFloat = fDataFloat*-1
				ENDIF
				IF NOT SHOULD_USE_METRIC_MEASUREMENTS()
					IF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE
					OR INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_FLOAT_DIST_WITH_CONV_MILE_NEG_B4_DIS
						fDataFloat = CONVERT_METERS_TO_MILES(fDataFloat)
					ELSE
						fDataFloat = CONVERT_METERS_TO_FEET(fDataFloat)
					ENDIF
				ENDIF
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_FLOAT(fDataFloat,2)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		
		CASE SCLB_DATA_TYPE_NONE
			//nothing
		BREAK
	ENDSWITCH
ENDPROC

FUNC TEXT_LABEL_15 GET_ARENA_SKILL_LEVEL_TITLE_FOR_SCLB(INT iLevel)
	TEXT_LABEL_15 tlReturn = "ARENA_C_SL"
	
	IF iLevel > ciARENA_SKILL_LEVEL_MAX
		tlReturn = "**INVALID**"
		RETURN tlReturn
	ENDIF
	
	IF iLevel < 0
		iLevel = 0
	ENDIF
		
	tlReturn += iLevel
	
	RETURN tlReturn
ENDFUNC

/// PURPOSE:
///    Adds a int to the row begun with START_SC_LEADERBOARD_SLO
/// PARAMS:
///    displaySetup - the display setup details
///    iIndex - index of value in the display setup
///    iDataInt - the value of the int
///    bValidData - true if this is a valid row versus empty data (local player always has a row it is filled with invalid data if there is no data for them)
///    bCustomCar - true if the data repersents a custom car
PROC ADD_SC_LEADERBOARD_COLUMN_DATA_INT(SC_LEADERBOARD_DISPLAY_SETUP displaySetup, INT iIndex,INT iDataInt, BOOL bValidData, BOOL bCustomCar = FALSE)
	IF iDataInt = HIGHEST_INT
	OR iDataInt = -HIGHEST_INT
		PRINTLN("ADD_SC_LEADERBOARD_COLUMN_DATA_INT- Highest int setting invalid data")
		bValidData = FALSE
	ENDIF
	TEXT_LABEL_15 tlReturn = ""
	
	SWITCH INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex])
		CASE SCLB_DATA_TYPE_INT
			IF bValidData
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_INTEGER(iDataInt)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_INT_NEG_B4_DIS
			IF bValidData
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_INTEGER(-iDataInt)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		
		CASE SCLB_DATA_TYPE_TIME
		CASE SCLB_DATA_TYPE_TIME_NO_MS
		CASE SCLB_DATA_TYPE_TIME_POSITIVE
			IF bValidData
				IF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_TIME_POSITIVE
				AND iDataInt < 0
					iDataInt = iDataInt*-1
				ENDIF
				IF iDataInt >= 1000*60*60 //if time is over an hr only should minutes/sec
				OR iDataInt <= -1000*60*60
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_TIME_NO_MS
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		
		CASE SCLB_DATA_TYPE_GOLD_TIME
			IF bValidData
				IF (iDataInt = HIGHEST_INT)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF iDataInt >= 1000*60*60 //if time is over an hr only should minutes/sec
				OR iDataInt <= -1000*60*60
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		
		CASE SCLB_DATA_TYPE_TIME_NEG_B4_DIS
		CASE SCLB_DATA_TYPE_TIME_NEG_B4_DIS_NO_MS
			IF bValidData
				iDataInt = iDataInt*-1
				IF iDataInt >= 1000*60*60 //if time is over an hr only should minutes/sec
				OR iDataInt <= -1000*60*60
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_TIME_NEG_B4_DIS_NO_MS
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME(iDataInt,TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_VEHICLE
			//PRINTLN("Car ID 1 = ",iDataInt)
			IF bCustomCar
				IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES,iDataInt))
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SCLB_VEH_CUST")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES,iDataInt)))
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES,iDataInt))
					//PRINTLN("Car Name = ", GET_STRING_FROM_TEXT_FILE(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES,iDataInt))) )
					//PRINTLN("Car ID = ",iDataInt)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES,iDataInt)))
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_GUN
			IF GET_RANGE_GUN_NAME_FROM_INT(iDataInt) != WEAPONTYPE_INVALID
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_WEAPON_NAME(GET_RANGE_GUN_NAME_FROM_INT(iDataInt)))
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		
		CASE SCLB_DATA_TYPE_INT_DIST_WITH_CONV_FT
		CASE SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE
		CASE SCLB_DATA_TYPE_INT_DIST_WITH_CONV_FT_NEG_B4_DIS
		CASE SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE_NEG_B4_DIS
			IF bValidData
				IF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_INT_DIST_WITH_CONV_FT_NEG_B4_DIS
				OR INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE_NEG_B4_DIS
					iDataInt = iDataInt*-1
				ENDIF
				IF NOT SHOULD_USE_METRIC_MEASUREMENTS()
					IF INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE
					OR INT_TO_ENUM(SCLB_DATA_TYPE,displaySetup.iColumnDisplayType[iIndex]) = SCLB_DATA_TYPE_INT_DIST_WITH_CONV_MILE_NEG_B4_DIS
						iDataInt = FLOOR(CONVERT_METERS_TO_MILES(TO_FLOAT(iDataInt)))
					ELSE
						iDataInt = FLOOR(CONVERT_METERS_TO_FEET(TO_FLOAT(iDataInt)))
					ENDIF
				ENDIF
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_INTEGER(iDataInt)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK	
		CASE SCLB_DATA_TYPE_ARENA_TITLE
			IF iDataInt > ciARENA_SKILL_LEVEL_MAX
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				tlReturn = GET_ARENA_SKILL_LEVEL_TITLE_FOR_SCLB(iDataInt)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlReturn)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_CC_MEMBER_RANK
			IF bValidData
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_INTEGER(GET_CAR_CLUB_REP_TIER(iDataInt))
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("SC_LB_EMPTY")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		BREAK
		CASE SCLB_DATA_TYPE_NONE
			//nothing
		BREAK
	ENDSWITCH
ENDPROC
//// PURPOSE:
 ///    Indicates you have written all data you need to slot started with START_SC_LEADERBOARD_SLOT
PROC STOP_SC_LEADERBOARD_SLOT()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


//EOF
