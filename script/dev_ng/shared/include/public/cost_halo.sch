// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	cost_halo.sch
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Cost Halo
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"
USING "menu_public.sch"

#IF IS_DEBUG_BUILD 
	USING "shared_debug.sch"
#ENDIF

//----------------------
//	ENUM
//----------------------
ENUM eHALO_SELECTION
	APPROACH_HALO,
	CANTAFFORD_HALO,
	CUSTOMCHECK_HALO,	// if the param in bCustomCheck in the handler update is true go to this
	DENIED_HALO,		// for doing certain checks
	TRIGGER_HALO,		// pressing right with enough
	LAUNCH_HALO,
	CLOSED_HALO,
	DAMAGED_HALO,		// if the car is too broken to go in
	WANTED_HALO,
	WRONGWAY_HALO
ENDENUM

ENUM eHALO_TYPE
	HALO_PED,
	HALO_VEHICLE,
	HALO_ALL
ENDENUM

//----------------------
//	STRUCT
//----------------------
STRUCT COSTHALO_LAUNCH_STRUCT
	INT iCandidateID = NO_CANDIDATE_ID
	VECTOR vec_coord
ENDSTRUCT

STRUCT COST_HALO
	BOOL bIsActive = FALSE
	VECTOR vTriggerPoint
	STRING sNameHelpText
	STRING sScriptName
	
	eHALO_TYPE haloType = HALO_PED
	BOOL bClosed = FALSE
	BOOL bCustomCheck = FALSE
	INT iCost = 5
	
	FLOAT fTriggerDist = LOCATE_SIZE_MISSION_TRIGGER 
	BOOL bUseAngledArea = FALSE
	
	VECTOR vDesiredDirection
	FLOAT fHeadingTolerance = 360.0
	ANGLED_AREA triggerArea
ENDSTRUCT

STRUCT COSTHALO_HANDLER
	STRING sActivateString	// help string for activation - has to have a substring and number
	STRING sBrokenString	// help string if activity is broken
	STRING sPoorString		// help string if player is too broke for activity - has to have a substring and number
	STRING sNoWayString		// help string if player is denied (eg wrong vehicle type)
	STRING sWantedString	// help string if player is wanted
	STRING sDamagedString	// help string if player is too damaged
	STRING sCustomString	// has to have a substring
	STRING sWrongWayString  // help string if player is pointing the wrong way
	STRING sUpsideDownString  // help string if player is pointing the wrong way
	
	BOOL bTriggerHelpShown = FALSE
	BOOL bShowMarkers = FALSE
	BOOL bNoWayCheck = FALSE
	INT iSelectedHalo = -1
	eHALO_SELECTION selectState
	
	STRING sLastDisplayedString
	INT iLastDisplayedNumber = 0
	BOOL bAllowOnMission = FALSE
ENDSTRUCT

CONST_FLOAT CARWASH_HEAD_TOLERANCE 15.0

//----------------------
//	FUNCTIONS
//----------------------

FUNC BOOL IS_COSTHALO_HANDLER_HELP_MESSAGE_BEING_DISPLAYED(COSTHALO_HANDLER& hndlr)
	IF IS_STRING_NULL_OR_EMPTY(hndlr.sLastDisplayedString)	
		RETURN FALSE
	ENDIF

	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	// if we are displaying cost halo
	IF ARE_STRINGS_EQUAL(hndlr.sActivateString, hndlr.sLastDisplayedString) OR ARE_STRINGS_EQUAL(hndlr.sPoorString, hndlr.sLastDisplayedString)
		RETURN IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(hndlr.sLastDisplayedString, hndlr.iLastDisplayedNumber)
	ENDIF
	
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hndlr.sLastDisplayedString)		
ENDFUNC

/// PURPOSE:
///    Clear Help
/// PARAMS:
///    hndlr - 
PROC CLEAR_COSTHALO_HANDLER_HELP(COSTHALO_HANDLER& hndlr)
	IF IS_COSTHALO_HANDLER_HELP_MESSAGE_BEING_DISPLAYED(hndlr)
		CPRINTLN(DEBUG_AMBIENT, "[COST_HALO]:Help String being Reset:", hndlr.sLastDisplayedString)
		CLEAR_HELP()
	ENDIF
	
	hndlr.sLastDisplayedString = NULL
	hndlr.iLastDisplayedNumber = 0
	
	IF hndlr.bTriggerHelpShown = TRUE
		CPRINTLN(DEBUG_AMBIENT, "[COST_HALO]:Help Shown Variable being Cleared")
		hndlr.bTriggerHelpShown = FALSE
	ENDIF
ENDPROC

FUNC BOOL IS_STRING_OKAY_FOR_HELP(STRING str)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	RETURN NOT IS_STRING_NULL_OR_EMPTY(str)
ENDFUNC

FUNC BOOL PRINT_COSTHALO_HELP(COSTHALO_HANDLER& hndlr, STRING textString)
	IF IS_STRING_NULL_OR_EMPTY(textString)
		RETURN FALSE
	ENDIF
	
	IF IS_COSTHALO_HANDLER_HELP_MESSAGE_BEING_DISPLAYED(hndlr) OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
		PRINT_HELP_FOREVER(textString)			
		hndlr.sLastDisplayedString = textString
		hndlr.iLastDisplayedNumber = 0
		hndlr.bTriggerHelpShown = TRUE
		
		CPRINTLN(DEBUG_AMBIENT, "[COST_HALO]:Help String being displayed:", hndlr.sLastDisplayedString)
		RETURN TRUE			
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRINT_COSTHALO_HELP_WITH_NUMBER(COSTHALO_HANDLER& hndlr, STRING textString, INT number)
	IF IS_STRING_NULL_OR_EMPTY(textString) 
		RETURN FALSE
	ENDIF
	
	IF IS_COSTHALO_HANDLER_HELP_MESSAGE_BEING_DISPLAYED(hndlr) OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
		PRINT_HELP_FOREVER_WITH_NUMBER(textString, number)			
		hndlr.sLastDisplayedString = textString
		hndlr.iLastDisplayedNumber = number
		hndlr.bTriggerHelpShown = TRUE
		
		CPRINTLN(DEBUG_AMBIENT, "[COST_HALO]:Help String being displayed:", hndlr.sLastDisplayedString)
		RETURN TRUE			
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Doesn't allow launching if mission isn't allowed to launch
/// RETURNS:
///    
FUNC BOOL IS_COSTHALO_ALLOWED_TO_LAUNCH(BOOL bAllowOnMission = FALSE)
//	#IF FEATURE_BUSINESS_BATTLES
//	IF GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) != MBV_COLLECT_DJ
//	#ENDIF
		IF (bAllowOnMission = FALSE)
			IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
				RETURN FALSE
			ENDIF
		ENDIF
//	#IF FEATURE_BUSINESS_BATTLES
//	ENDIF
//	#ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_SYSTEM_UI_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF g_bMissionOverStatTrigger
		RETURN FALSE
	ENDIF

	IF IS_SELECTOR_ONSCREEN(FALSE)
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
//	#IF FEATURE_BUSINESS_BATTLES
//	IF GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) != MBV_COLLECT_DJ
//	#ENDIF
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			RETURN FALSE
		ENDIF
//	#IF FEATURE_BUSINESS_BATTLES
//	ENDIF
//	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Setups Cost Halo
/// PARAMS:
///    r - halo reference 
///    haloType - halo type
///    scrpt - the script the halo will launch
///    pos - halo position
///    cost - cost of the halo
///    hlp - name of the object (this goes in the help text)
///    closed - does it start off closed
///    haloOverride - use this size of the area instead
PROC SETUP_COST_HALO(COST_HALO &r, eHALO_TYPE haloType, STRING scrpt, VECTOR pos, INT cost, STRING hlp, BOOL closed = TRUE, FLOAT haloOverride = 0.0)
	r.sScriptName = scrpt
	r.vTriggerPoint = pos 
	r.sNameHelpText = hlp
	r.iCost = cost
	r.bClosed = closed
	r.haloType = haloType
	r.bUseAngledArea = false
	r.bIsActive = TRUE
	r.vDesiredDirection = <<0, 0, 0>>
	r.fHeadingTolerance = 360.0
	IF (haloType = HALO_PED)
		r.fTriggerDist = LOCATE_SIZE_MISSION_TRIGGER
	ELSE
		r.fTriggerDist = LOCATE_SIZE_MISSION_TRIGGER * 2.0
	ENDIF

	IF (haloOverride > 0.0)
		r.fTriggerDist = haloOverride
	ENDIF
ENDPROC

/// PURPOSE:
///    Setups Cost Halo but using an angled area instead
/// PARAMS:
///    r - halo reference 
///    haloType - halo type
///    scrpt - the script the halo will launch
///    pos1 - position 1 of area
///    pos2 - position 2 of area
///    width - area width
///    cost - 
///    hlp - 
///    closed - 
PROC SETUP_COST_AREA(COST_HALO &r, eHALO_TYPE haloType, STRING scrpt, VECTOR pos1, VECTOR pos2, FLOAT width, INT cost, STRING hlp, BOOL closed = TRUE)
	r.sScriptName = scrpt
	r.sNameHelpText = hlp
	r.iCost = cost
	r.bClosed = closed
	r.haloType = haloType
	r.bUseAngledArea = TRUE
	r.fTriggerDist = 0.0
	r.bIsActive = TRUE
	r.vDesiredDirection = <<0, 0, 0>>
	r.fHeadingTolerance = 360.0
	SET_ANGLED_AREA(r.triggerArea, pos1, pos2, width)
	r.vTriggerPoint = GET_ANGLED_AREA_CENTER(r.triggerArea)
ENDPROC

/// PURPOSE:
///    Setup tolerance for halo, object has to be pointing within tol degrees of this direction
/// PARAMS:
///    r - halo
///    dir - direction as vector as rotation
///    tol - angle tolerance
PROC SETUP_COST_HALO_DIRECTION_TOLERANCE(COST_HALO &r, VECTOR dir, FLOAT tol = 360.0)
	r.vDesiredDirection = NORMALISE_VECTOR(dir * 5.0)
	r.fHeadingTolerance = CLAMP(tol, 0, 360.0)
ENDPROC

/// PURPOSE:
///    	Shutdown halos - this used to turn of the blips but i'm keeping this function just in case 
/// PARAMS:
///   	halo - halo ref
PROC SHUTDOWN_COST_HALO(COST_HALO &r)
	r.bIsActive = FALSE
ENDPROC

/// PURPOSE:
///    	Shutdown all halos in array
/// PARAMS:
///   	halo - array ref
PROC SHUTDOWN_COST_HALOS(COST_HALO& halo[])
	INT i = 0
	REPEAT COUNT_OF(halo) i 
		SHUTDOWN_COST_HALO(halo[i])
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Draws the marker
/// PARAMS:
///    halo - halo ref
PROC DRAW_COST_HALO(COST_HALO& halo)
	IF (halo.bUseAngledArea = TRUE)
		DEBUG_DRAW_ANGLED_AREA_EX(halo.triggerArea, 255, 100, 0, 100)
	ELSE
		DRAW_DEBUG_CIRCLE(halo.vTriggerPoint, halo.fTriggerDist)
	ENDIF
ENDPROC


/// PURPOSE:
///    Returns True if we in cost halo
/// PARAMS:
///    halo - halo ref
FUNC BOOL IS_PLAYER_IN_COST_HALO(COST_HALO& halo)
	IS_ENTITY_OK(PLAYER_PED_ID())
	
	IF halo.bIsActive = FALSE
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN(FALSE)
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF (halo.haloType = HALO_PED)	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ELIF (halo.haloType = HALO_VEHICLE)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
			RETURN FALSE
		ENDIF
		
		IF (GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) <> VS_DRIVER)
			RETURN FALSE
		ENDIF	
	ENDIF
	
	IF (halo.bUseAngledArea = TRUE)
		//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IS_PLAYER_IN_COST_HALO halo.triggerArea.vPosition[0]: ",halo.triggerArea.vPosition[0])
		//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IS_PLAYER_IN_COST_HALO halo.triggerArea.vPosition[1]: ",halo.triggerArea.vPosition[1])
		//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IS_PLAYER_IN_COST_HALO halo.triggerArea.fWidth: ",halo.triggerArea.fWidth)
		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), halo.triggerArea.vPosition[0], halo.triggerArea.vPosition[1], halo.triggerArea.fWidth)
	ENDIF
	
	//VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IS_PLAYER_IN_COST_HALO halo.vTriggerPoint: ",halo.vTriggerPoint)
	//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IS_PLAYER_IN_COST_HALO halo.fTriggerDist: ",halo.fTriggerDist)
	//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: player coords", vPlayerPos)
	//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: in range: ",IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), halo.vTriggerPoint, halo.fTriggerDist))
	RETURN IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), halo.vTriggerPoint, halo.fTriggerDist)
ENDFUNC

/// PURPOSE:
///    Returns True if we aren't in cost halo
/// PARAMS:
///    halo - halo ref
FUNC BOOL IS_PLAYER_OUT_OF_COST_HALO(COST_HALO& halo)
	ANGLED_AREA ex 
	
	IF (halo.haloType = HALO_PED)	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ELIF (halo.haloType = HALO_VEHICLE)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
			RETURN TRUE
		ENDIF
		
		IF (GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) <> VS_DRIVER)
			RETURN TRUE
		ENDIF	
	ENDIF
	
	IF (halo.bUseAngledArea = FALSE)
		RETURN NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), halo.vTriggerPoint, halo.fTriggerDist + 2.0)
	ENDIF
	
	ex = EXPAND_ANGLED_AREA(halo.triggerArea, 2.0)
	RETURN NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), ex.vPosition[0], ex.vPosition[1], ex.fWidth)
ENDFUNC


/// PURPOSE:
///    Test if given vehicle has x amount of tyres burst
/// PARAMS:
///    vehIndex - vehicle to check
///    iNumTyresBurstToReturnTrue - num tyres to be burst inorder to return true
/// RETURNS:
///    TRUE - if num of burst tyres >= iNumTyresBurstToReturnTrue
FUNC BOOL ARE_VEHICLE_TYRES_BURST(VEHICLE_INDEX vehIndex, INT iNumTyresBurstToReturnTrue = 1)
	IF IS_VEHICLE_OK(vehIndex)
		INT iNumBurstTyres = 0
		
		// B*1744975
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_LEFT) AND IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_RIGHT)
			RETURN TRUE
		ENDIF

		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_LEFT) AND IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_RIGHT)
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_LEFT)
			iNumBurstTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_FRONT_RIGHT)
			iNumBurstTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_LEFT)
			iNumBurstTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehIndex, SC_WHEEL_CAR_REAR_RIGHT)
			iNumBurstTyres++
		ENDIF
		IF iNumBurstTyres >= iNumTyresBurstToReturnTrue
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

 
/// PURPOSE:
///    Returns true if the player is in a bust vehicle
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_IN_A_BUST_VEHICLE()
	VEHICLE_INDEX veh
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
		RETURN FALSE 
	ENDIF
	
	veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) 
	IF IS_ENTITY_ON_FIRE(veh)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_FUCKED(veh)
		RETURN TRUE
	ENDIF
	
	IF ARE_VEHICLE_TYRES_BURST(veh, 3)
		RETURN TRUE
	ENDIF
	
	RETURN NOT IS_VEHICLE_DRIVEABLE(veh)
ENDFUNC

PROC RESET_COST_HALO(COST_HALO& halo)
	halo.bIsActive = TRUE
ENDPROC

PROC SET_COSTHALO_HANDLER_STATE(COSTHALO_HANDLER &hndlr, eHALO_SELECTION state)
	hndlr.selectState = state
	CLEAR_COSTHALO_HANDLER_HELP(hndlr)
	CPRINTLN(DEBUG_AMBIENT, "[COST_HALO]:Changing State To:", ENUM_TO_INT(hndlr.selectState))
ENDPROC
	
FUNC BOOL IS_PLAYER_WITHIN_HALO_TOLERANCE(COST_HALO& halo)
	VECTOR fwd 
	VEHICLE_INDEX veh
	ENTITY_INDEX ent = PLAYER_PED_ID()
	
	IF (halo.fHeadingTolerance = 360.0) 
		RETURN TRUE
	ENDIF
	
	IF IS_VECTOR_ZERO(halo.vDesiredDirection) 
		RETURN TRUE
	ENDIF
	
	// check vehicle is upright
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
		veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_ENTITY_OK(veh)
			fwd = GET_ENTITY_ROTATION(veh)
			IF (fwd.y > 45) OR (fwd.y < -45)
				RETURN FALSE
			ENDIF
			
			ent = veh
		ENDIF	
	ENDIF
	
	fwd = GET_ENTITY_FORWARD_VECTOR(ent)
	IF DOT_PRODUCT_XY(halo.vDesiredDirection, fwd) > COS(halo.fHeadingTolerance)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the cost halos
/// PARAMS:
///    halo - array of halos
///    hndlr - handler reference (string and other stuff)
///    bNoWay - if we've passed the no way check
///    bFadeLaunch - do we fade when we launch
/// RETURNS:
///    Returns true if we have launched the script
FUNC BOOL UPDATE_COSTHALO_HANDLER(COST_HALO& halo[], COSTHALO_HANDLER& hndlr, BOOL bNoWay = FALSE, BOOL bFadeLaunch = FALSE, BOOL bAutoLaunch = TRUE, INT iStack = DEFAULT_STACK_SIZE)
	INT i 
	
	IF (hndlr.bShowMarkers)
		i = 0
		REPEAT COUNT_OF(halo) i 
			DRAW_COST_HALO(halo[i])		
		ENDREPEAT		
	ENDIF
	
	SWITCH (hndlr.selectState)
		CASE APPROACH_HALO
			i = 0
			IF IS_COSTHALO_ALLOWED_TO_LAUNCH(hndlr.bAllowOnMission) 
				REPEAT COUNT_OF(halo) i 	
					IF IS_PLAYER_IN_COST_HALO(halo[i]) AND (halo[i].bIsActive)
						hndlr.iSelectedHalo = i 
						
						IF (halo[i].bClosed) OR (GET_MISSION_FLAG() AND (hndlr.bAllowOnMission = FALSE))
							SET_COSTHALO_HANDLER_STATE(hndlr, CLOSED_HALO)
						ELIF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
							SET_COSTHALO_HANDLER_STATE(hndlr, WANTED_HALO)
						ELIF (bNoWay)
							SET_COSTHALO_HANDLER_STATE(hndlr, DENIED_HALO)
						ELIF (halo[i].bCustomCheck)
							SET_COSTHALO_HANDLER_STATE(hndlr, CUSTOMCHECK_HALO)
						ELIF  (GET_CURRENT_PLAYER_PED_ACCOUNT_BALANCE() < halo[hndlr.iSelectedHalo].iCost)	
							SET_COSTHALO_HANDLER_STATE(hndlr, CANTAFFORD_HALO)
						ELIF IS_PLAYER_IN_A_BUST_VEHICLE()
							SET_COSTHALO_HANDLER_STATE(hndlr, DAMAGED_HALO)
						ELSE
							SET_COSTHALO_HANDLER_STATE(hndlr, TRIGGER_HALO)
						ENDIF		

					ENDIF	
				ENDREPEAT
			ELSE
				CLEAR_COSTHALO_HANDLER_HELP(hndlr)			
			ENDIF
		BREAK
		CASE CUSTOMCHECK_HALO
			IF NOT (hndlr.bTriggerHelpShown)	
				PRINT_COSTHALO_HELP(hndlr, hndlr.sCustomString)	
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR (halo[i].bCustomCheck = FALSE)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF				
		BREAK
		CASE CANTAFFORD_HALO
			IF NOT (hndlr.bTriggerHelpShown)
				PRINT_COSTHALO_HELP_WITH_NUMBER(hndlr, hndlr.sPoorString, halo[hndlr.iSelectedHalo].iCost)
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR (GET_CURRENT_PLAYER_PED_ACCOUNT_BALANCE() >= halo[hndlr.iSelectedHalo].iCost)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF			
		BREAK
		CASE CLOSED_HALO
			IF NOT (hndlr.bTriggerHelpShown)	
				PRINT_COSTHALO_HELP(hndlr, hndlr.sBrokenString)	
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR NOT IS_COSTHALO_ALLOWED_TO_LAUNCH(hndlr.bAllowOnMission)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF	
		BREAK
		CASE DENIED_HALO
			IF NOT (hndlr.bTriggerHelpShown)	
				PRINT_COSTHALO_HELP(hndlr, hndlr.sNoWayString)		
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR (bNoWay = FALSE) OR NOT IS_COSTHALO_ALLOWED_TO_LAUNCH(hndlr.bAllowOnMission)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF	
		BREAK
		CASE DAMAGED_HALO
			IF NOT (hndlr.bTriggerHelpShown)	
				PRINT_COSTHALO_HELP(hndlr, hndlr.sDamagedString)			
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR NOT IS_COSTHALO_ALLOWED_TO_LAUNCH(hndlr.bAllowOnMission)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF	
		BREAK
		CASE WANTED_HALO
			IF NOT (hndlr.bTriggerHelpShown)	
				PRINT_COSTHALO_HELP(hndlr, hndlr.sWantedString)	
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF
		BREAK
		CASE WRONGWAY_HALO
			IF NOT (hndlr.bTriggerHelpShown)	
				IF IS_PLAYER_UPRIGHT()
					PRINT_COSTHALO_HELP(hndlr, hndlr.sWrongWayString)	
				ELSE
					PRINT_COSTHALO_HELP(hndlr, hndlr.sUpsideDownString)	
				ENDIF
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR IS_PLAYER_WITHIN_HALO_TOLERANCE(halo[hndlr.iSelectedHalo])
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF
		BREAK
		CASE TRIGGER_HALO
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CONTEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
			DISABLE_SELECTOR_THIS_FRAME()
							
			IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
				SET_COSTHALO_HANDLER_STATE(hndlr, WANTED_HALO)
				RETURN FALSE
			ENDIF
	
			IF (halo[hndlr.iSelectedHalo].bCustomCheck)
				SET_COSTHALO_HANDLER_STATE(hndlr, CUSTOMCHECK_HALO)
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_OUT_OF_COST_HALO(halo[hndlr.iSelectedHalo]) OR NOT IS_COSTHALO_ALLOWED_TO_LAUNCH(hndlr.bAllowOnMission)
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN FALSE
			ENDIF
			
			// don't launch if browser is running
			IF (g_bBrowserVisible)
				RETURN FALSE
			ENDIF
			
#IF FEATURE_SP_DLC_DIRECTOR_MODE
			//Don't launch if launching director mode
			IF IS_DIRECTOR_MODE_RUNNING(TRUE)
				RETURN FALSE
			ENDIF
#ENDIF
			// need to add wrong type check here
			IF (halo[hndlr.iSelectedHalo].haloType = HALO_VEHICLE) 
				IF (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())  OR (GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) <> VS_DRIVER))
					SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
					RETURN FALSE
				ENDIF
			ENDIF
	
			IF (halo[hndlr.iSelectedHalo].bClosed) OR (GET_MISSION_FLAG() AND (hndlr.bAllowOnMission = FALSE))
				SET_COSTHALO_HANDLER_STATE(hndlr, CLOSED_HALO)
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_IN_A_BUST_VEHICLE()
				SET_COSTHALO_HANDLER_STATE(hndlr, DAMAGED_HALO)
				RETURN FALSE
			ENDIF
			
			IF (GET_CURRENT_PLAYER_PED_ACCOUNT_BALANCE() < halo[hndlr.iSelectedHalo].iCost)
				SET_COSTHALO_HANDLER_STATE(hndlr, CANTAFFORD_HALO)
				RETURN FALSE
			ENDIF
					
			IF NOT (hndlr.bTriggerHelpShown)
				PRINT_COSTHALO_HELP_WITH_NUMBER(hndlr, hndlr.sActivateString, halo[hndlr.iSelectedHalo].iCost)
			ENDIF		
			
			// don't let player trigger if they are driving straight through it
			// cant use a vehicle stuck on roof check as i need to add it so check for roll instead
			IF (halo[hndlr.iSelectedHalo].haloType = HALO_VEHICLE)
			
				/*
				IF (GET_ENTITY_SPEED(PLAYER_PED_ID()) > 5.0)
					RETURN FALSE
				ENDIF
				*/
				
				// if ped is getting out of vehicle don't allow launch
				IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					RETURN FALSE
				ENDIF				
			ENDIF
			
			IF IS_PAUSE_MENU_ACTIVE() OR IS_SYSTEM_UI_BEING_DISPLAYED()
				RETURN FALSE
			ENDIF
				
			// don't let a ped this if the player is on foot but on with both feet on the ground
			IF IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF IS_PED_FALLING(PLAYER_PED_ID()) OR IS_PED_JUMPING(PLAYER_PED_ID()) OR IS_PED_JUMPING_OUT_OF_VEHICLE(PLAYER_PED_ID())
					RETURN FALSE
				ENDIF
				
				IF IS_PED_RUNNING_RAGDOLL_TASK(PLAYER_PED_ID()) OR IS_PED_RAGDOLL(PLAYER_PED_ID()) OR IS_PED_GETTING_UP(PLAYER_PED_ID())
					RETURN FALSE
				ENDIF
				
				IF IS_PED_CLIMBING(PLAYER_PED_ID())
					RETURN FALSE
				ENDIF
				
				IF IS_PED_DUCKING(PLAYER_PED_ID()) OR IS_PED_SWIMMING(PLAYER_PED_ID())
					RETURN FALSE
				ENDIF
				
				IF (GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.05)
					RETURN FALSE
				ENDIF
			ENDIF
			
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT) AND (hndlr.bTriggerHelpShown = TRUE) 
				IF NOT IS_PLAYER_WITHIN_HALO_TOLERANCE(halo[hndlr.iSelectedHalo])
					SET_COSTHALO_HANDLER_STATE(hndlr, WRONGWAY_HALO)
				ELSE
					SET_COSTHALO_HANDLER_STATE(hndlr, LAUNCH_HALO)
				ENDIF
			ENDIF		
		BREAK  
		CASE LAUNCH_HALO
			CLEAR_COSTHALO_HANDLER_HELP(hndlr)		
			halo[hndlr.iSelectedHalo].bCustomCheck = FALSE
			IF NOT bAutoLaunch
				SET_COSTHALO_HANDLER_STATE(hndlr, APPROACH_HALO)
				RETURN TRUE
			ENDIF
			
			CLEAR_HELP()
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0)
			IF NOT IS_STRING_NULL_OR_EMPTY(halo[hndlr.iSelectedHalo].sScriptName)
			
				REQUEST_SCRIPT(halo[hndlr.iSelectedHalo].sScriptName)
				WHILE NOT HAS_SCRIPT_LOADED(halo[hndlr.iSelectedHalo].sScriptName)
					DISABLE_SELECTOR_THIS_FRAME()
					
					IF IS_ENTITY_OK(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT g_bInMultiplayer
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 2)
							SET_VEHICLE_BRAKE_LIGHTS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
						ENDIF
					ENDIF
					
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CONTEXT)
					REQUEST_SCRIPT(halo[hndlr.iSelectedHalo].sScriptName)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
					WAIT(0)
				ENDWHILE
				
				IF (bFadeLaunch)
					DO_SCREEN_FADE_OUT(250)
				ENDIF
				
				IF HAS_SCRIPT_LOADED(halo[hndlr.iSelectedHalo].sScriptName)
				
					IF (bFadeLaunch)
						WHILE NOT IS_SCREEN_FADED_OUT()
						
							IF IS_ENTITY_OK(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT g_bInMultiplayer
									BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 2)
									SET_VEHICLE_BRAKE_LIGHTS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
								ENDIF
							ENDIF
					
							SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CONTEXT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
							DISABLE_SELECTOR_THIS_FRAME()
							WAIT(0)
						ENDWHILE
					ENDIF
					
					START_NEW_SCRIPT(halo[hndlr.iSelectedHalo].sScriptName, iStack)
					SET_SCRIPT_AS_NO_LONGER_NEEDED(halo[hndlr.iSelectedHalo].sScriptName)
					RETURN TRUE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Set up default string for halo handler
/// PARAMS:
///    hndlr - halo handler
///    onMission - on mission flag
PROC SET_STRINGS_FOR_CARWASH_HALO_HANDLER(COSTHALO_HANDLER &hndlr, BOOL onMission = FALSE)
	hndlr.sActivateString = "CWASH_RIDEHLP"
	hndlr.sBrokenString = ""
	
	hndlr.sNoWayString = "CWASH_NOWAY"
	hndlr.sWantedString = "CWASH_WANTED"
	hndlr.sDamagedString = "CWASH_CARBROKE"
	hndlr.sCustomString = "CWASH_BLOCKED"
	hndlr.sWrongWayString = "CWASH_WRONGWAY"
	hndlr.sUpsideDownString = "CWASH_UPSIDEDO"
	hndlr.bAllowOnMission = onMission
	
	IF NOT g_bInMultiplayer
		hndlr.sPoorString = "CWASH_NOMONEY"
		EXIT
	ENDIF
	
	IF IS_PS3_VERSION() 
	OR IS_PLAYSTATION_PLATFORM()
		hndlr.sPoorString = "CWASH_POOR_PSN"
	ELIF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		hndlr.sPoorString = "CWASH_POOR_XBX"
	ELSE
		hndlr.sPoorString = "CWASH_POOR_STD"
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if car is allowed in car wash
/// PARAMS:
///    veh - 
/// RETURNS:
///    
FUNC BOOL IS_VEHICLE_MODEL_BLATANTLY_STUPID_FOR_CARWASH(MODEL_NAMES mdl) 
	IF NOT IS_THIS_MODEL_A_CAR(mdl)
		RETURN TRUE
	ENDIF
	
	SWITCH (mdl)
		CASE AMBULANCE
		CASE ARMYTRAILER
		CASE AIRTUG
		CASE BIFTA
		CASE BISON2
		CASE BISON3
		CASE BODHI2
		CASE BOXVILLE
		CASE BOXVILLE2
		CASE BOXVILLE3
		CASE BULLDOZER
		CASE CADDY
		CASE CADDY2
		CASE CADDY3
		CASE CUTTER
		CASE DILETTANTE2
		CASE DUBSTA3
		CASE DUMP
		CASE DUNE
		CASE DLOADER
		CASE FORKLIFT
		CASE GUARDIAN
		CASE GRAINTRAILER
		CASE HANDLER
		CASE INSURGENT
		CASE INSURGENT2
		CASE KALAHARI
		CASE MESA3
		CASE MIXER
		CASE MIXER2
		CASE MOWER
		CASE PACKER
		CASE PBUS
		CASE RATLOADER
		CASE RENTALBUS
		CASE RHINO
		CASE RIPLEY
		CASE RIOT
		CASE SADLER
		CASE SADLER2
		CASE SANDKING
		CASE SANDKING2
		CASE SCRAP
		CASE STOCKADE
		CASE STOCKADE3
		CASE TECHNICAL
		CASE TRACTOR
		CASE TRACTOR2
		CASE TORNADO4
		CASE TOURBUS
		CASE LIMO2
		CASE RAPTOR
		CASE RATLOADER2
		CASE DUNE3
		CASE DUNE4
		CASE DUNE5
		CASE VIGILANTE
		CASE DELUXO
		CASE CARACARA
		CASE SCRAMJET
		CASE MENACER
		CASE CARACARA2
		CASE LOCUST
		CASE JUGULAR
		CASE ZORRUSSO
		CASE FORMULA
		CASE EVERON
		CASE ZHABA
		CASE OUTLAW
		CASE VAGRANT
		CASE FORMULA2
		CASE MINITANK
		CASE PEYOTE3
		CASE YOUGA3
		CASE OPENWHEEL1
		CASE OPENWHEEL2
		CASE WINKY
		CASE SLAMTRUCK
		CASE VETO
		CASE VETO2
		CASE SQUADDIE
		#IF FEATURE_FIXER
		CASE COMET7
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE SM722
		CASE DRAUGUR
		#ENDIF
			RETURN TRUE	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_BLATANTLY_STUPID_FOR_CARWASH(VEHICLE_INDEX veh)
	MODEL_NAMES mdl = GET_ENTITY_MODEL(veh)
	
	IF IS_VEHICLE_MODEL_BLATANTLY_STUPID_FOR_CARWASH(mdl)
		RETURN TRUE
	ENDIF
	

	IF (mdl = REGINA)
		IF IS_VEHICLE_EXTRA_TURNED_ON(veh, 2)	
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_EXTRA_TURNED_ON(veh, 3) AND IS_VEHICLE_EXTRA_TURNED_ON(veh, 4)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (mdl = BANSHEE) AND IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = PEYOTE) AND IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = COQUETTE) AND NOT DOES_VEHICLE_HAVE_ROOF(veh) //IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = MANANA) AND IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = MESA) AND NOT IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = VOLTIC) AND IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = BOBCATXL) AND NOT IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	
	IF (mdl = RUINER)
		IF IS_VEHICLE_EXTRA_TURNED_ON(veh, 3) AND NOT IS_VEHICLE_EXTRA_TURNED_ON(veh, 7)
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF (mdl = YOSEMITE3)
		IF GET_VEHICLE_MOD(veh, MOD_CHASSIS) = 4
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF (mdl = COQUETTE4) AND NOT DOES_VEHICLE_HAVE_ROOF(veh) //IS_VEHICLE_EXTRA_TURNED_ON(veh, 1)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_TUNER
	IF (mdl = RT3000) AND  GET_VEHICLE_MOD(veh, MOD_ROOF) = 3
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF (mdl = BRIOSO3)
		SWITCH GET_VEHICLE_MOD(veh, MOD_ROOF)
			CASE 1
			CASE 4
			CASE 7
			CASE 10
			RETURN TRUE
		ENDSWITCH
	ENDIF
	#ENDIF
	
	IF mdl = ZR350 AND GET_VEHICLE_MOD(veh, MOD_SPOILER) > 10
		RETURN TRUE
	ENDIF
	
	IF mdl = KRIEGER AND GET_VEHICLE_MOD(veh, MOD_SPOILER) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks if car is allowed in car wash
/// PARAMS:
///    veh - 
/// RETURNS:
///    
FUNC BOOL IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH(VEHICLE_INDEX veh)
	VECTOR dMin, dMax
	
	#IF IS_DEBUG_BUILD
	BOOL bPrintReason = TRUE
	#ENDIF
	
	IF NOT IS_ENTITY_ALIVE(veh)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_VEHICLE_HAVE_ROOF(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: NOT DOES_VEHICLE_HAVE_ROOF")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_ATTACHED_TO_TRAILER(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: IS_VEHICLE_ATTACHED_TO_TRAILER")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(veh)) 
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: NOT NOT IS_THIS_MODEL_A_CAR")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: NOT IS_ENTITY_ON_FIRE()")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_VEHICLE_A_TEST_DRIVE_VEHICLE(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: IS_VEHICLE_A_TEST_DRIVE_VEHICLE")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_VEHICLE_BLATANTLY_STUPID_FOR_CARWASH(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: IS_VEHICLE_BLATANTLY_STUPID_FOR_CARWASH(")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), dMin, dMax)
//	#IF IS_DEBUG_BUILD
//	IF bPrintReason
//		PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: ABSF(dMax.x(",dMax.x,") - dMin.x(",dMin.x,")) > 3.4)")
//	ENDIF
//	#ENDIF
	IF (GET_ENTITY_MODEL(veh) != ZENO) // B*7310301 - don't check width for the Zeno only.
		IF (ABSF(dMax.x - dMin.x) > 3.4)
			#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: ABSF(dMax.x(",dMax.x,") - dMin.x(",dMin.x,")) > 3.4)")
			ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (ABSF(dMax.z - dMin.z) > 2.9)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: (ABSF(dMax.z - dMin.z) > 2.9)")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	// B*1439838 - only the machine who owns this can call this
	IF (g_bInMultiplayer)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: NOT NETWORK_HAS_CONTROL_OF_ENTITY(")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_A_CONVERTIBLE(veh, TRUE) AND NOT DOES_VEHICLE_HAVE_ROOF(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH: IS_VEHICLE_A_CONVERTIBLE(veh, TRUE) AND NOT DOES_VEHICLE_HAVE_ROOF(veh")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_A_CONVERTIBLE(veh, TRUE) AND NOT DOES_VEHICLE_HAVE_ROOF(veh)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH:NOT  IS_VEHICLE_A_CONVERTIBLE(veh, TRUE) AND NOT DOES_VEHICLE_HAVE_ROOF(veh)")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	/* special case for ROOSEVOLT */
	IF GET_ENTITY_MODEL(veh) = INT_TO_ENUM(MODEL_NAMES, HASH("btype"))
	OR GET_ENTITY_MODEL(veh) = BTYPE3
		IF IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(veh, VS_EXTRA_LEFT_1)) OR IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(veh, VS_EXTRA_RIGHT_1))
		OR  IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(veh, VS_EXTRA_LEFT_2)) OR IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(veh, VS_EXTRA_RIGHT_2))
		OR  IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(veh, VS_EXTRA_LEFT_3)) OR IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(veh, VS_EXTRA_RIGHT_3))
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC




