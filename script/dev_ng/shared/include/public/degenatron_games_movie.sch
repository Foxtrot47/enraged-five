USING "arcade_cabinet_minigame_common.sch"


PROC DG_CLEAN_UP_MOVIE(BINK_MOVIE_ID bmID)
	IF (bmID != NULL)
		STOP_BINK_MOVIE(bmID)
		RELEASE_BINK_MOVIE(bmID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the bink movie asset for the video and starts playback
PROC DG_START_MOVIE(BINK_MOVIE_ID &bmID, STRING movieName)

	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DG_START_MOVIE - Starting movie") 
	
	bmID = SET_BINK_MOVIE(movieName)
	PLAY_BINK_MOVIE(bmID)
	SET_BINK_SHOULD_SKIP(bmID, TRUE)
	SET_BINK_MOVIE_AUDIO_FRONTEND(bmID, TRUE) 
		
ENDPROC

/// PURPOSE:
///    Draws the movie and returns true when the intro movie has completed
FUNC BOOL DG_DRAW_MOVIE(BINK_MOVIE_ID bmID, FLOAT fRatioWidth, FLOAT fRatioHeight)
	
	DRAW_BINK_MOVIE(bmID, cfSCREEN_CENTER, cfSCREEN_CENTER, fRatioWidth, fRatioHeight, 0.0, 255,255,255,255)

	IF GET_BINK_MOVIE_TIME(bmID) >= 99.0
		
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DG_DRAW_MOVIE - Stopping and releasing movie") 
		
		STOP_BINK_MOVIE(bmID)
		RELEASE_BINK_MOVIE(bmID)
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DG_DRAW_MOVIE_WITH_POS(BINK_MOVIE_ID bmID, FLOAT fRatioWidth, FLOAT fRatioHeight, FLOAT xPos, FLOAT yPos,RGBA_COLOUR_STRUCT rgbaColor)
	
	DRAW_BINK_MOVIE(bmID, xPos, yPos, fRatioWidth, fRatioHeight, 0.0, rgbaColor.iR, rgbaColor.iG, rgbaColor.iB, rgbaColor.iA)

	IF GET_BINK_MOVIE_TIME(bmID) >= 99.0
		
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DG_DRAW_MOVIE - Stopping and releasing movie") 
		
		STOP_BINK_MOVIE(bmID)
		RELEASE_BINK_MOVIE(bmID)
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
