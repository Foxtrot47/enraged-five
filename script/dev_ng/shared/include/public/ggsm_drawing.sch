//-------------------------------------------------
// File: ggsm_arcade_drawing.sch
// Purpose: the game itself
//-------------------------------------------------

//-------------------------------------------------
// INCLUDES
//-------------------------------------------------
USING "ggsm_structs.sch"
USING "ggsm_helpers.sch"
USING "ggsm_data.sch"
USING "ggsm_entity.sch"
USING "arcade_games_postfx.sch"

//-------------------------------------------------
// SPECIAL DRAW FUNCTIONS
//-------------------------------------------------
PROC ARCADE_CABINET_REAL_RESOLUTION_CORRECTED(VECTOR_2D &vPos, VECTOR_2D &vScale)

	vPos = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vPos)
	vScale = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vScale)

	vPos.x = APPLY_ASPECT_MOD_TO_X(vPos.x)
	vScale.X *= fAspectRatioModifier

	vPos.x *= iScreenX
	vPos.y *= iScreenY
	
	vScale.x *= iScreenX
	vScale.y *= iScreenY
	
	vPos.x = TO_FLOAT(FLOOR(vPos.x))
	vPos.y = TO_FLOAT(FLOOR(vPos.y))
	
	vScale.x = TO_FLOAT(FLOOR(vScale.x))
	vScale.y = TO_FLOAT(FLOOR(vScale.y))

	vPos.x -= (vPos.x%4)
	vPos.y -= (vPos.y%4)
	
	vScale.x -= (vScale.x%4)
	vScale.y -= (vScale.y%4)
	
	vPos.x /= iScreenX
	vPos.y /= iScreenY
	
	vScale.x /= iScreenX
	vScale.y /= iScreenY
	
ENDPROC

/// PURPOSE:
///    Draws a sprite from screenspace values
///    Note: Use ARCADE_CABINET_DRAW_SPRITE_ROTATED for non-square rotated sprites
PROC ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(STRING stTextureDict, STRING stTexture, VECTOR_2D vCenter, VECTOR_2D vScale, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF

	

#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(vCenter.X) * g_fMinimizeArcadeCabinetCentreX, vCenter.Y * g_fMinimizeArcadeCabinetCentreY, vScale.X * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, vScale.Y * g_fMinimizeArcadeCabinetCentreScale, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	ELSE
#ENDIF
		
		ARCADE_CABINET_REAL_RESOLUTION_CORRECTED(vCenter, vScale)
		
		DRAW_SPRITE_ARX(stTextureDict, stTexture, vCenter.X, vCenter.Y, vScale.X, vScale.Y, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

//-------------------------------------------------
// HUD CHAR FUNCTIONS
//-------------------------------------------------
FUNC STRING GGSM_GET_HUD_CHAR_SPRITE(GGSM_HUD_CHAR eChar)
	SWITCH (eChar)
		CASE GGSM_HUD_CHAR_0			RETURN "HUD_NUMBER_0"
		CASE GGSM_HUD_CHAR_1			RETURN "HUD_NUMBER_1"
		CASE GGSM_HUD_CHAR_2			RETURN "HUD_NUMBER_2"
		CASE GGSM_HUD_CHAR_3			RETURN "HUD_NUMBER_3"
		CASE GGSM_HUD_CHAR_4			RETURN "HUD_NUMBER_4"
		CASE GGSM_HUD_CHAR_5			RETURN "HUD_NUMBER_5"
		CASE GGSM_HUD_CHAR_6			RETURN "HUD_NUMBER_6"
		CASE GGSM_HUD_CHAR_7			RETURN "HUD_NUMBER_7"
		CASE GGSM_HUD_CHAR_8			RETURN "HUD_NUMBER_8"
		CASE GGSM_HUD_CHAR_9			RETURN "HUD_NUMBER_9"
	
		CASE GGSM_HUD_CHAR_A			RETURN "HUD_LETTER_A"
		CASE GGSM_HUD_CHAR_B			RETURN "HUD_LETTER_B"
		CASE GGSM_HUD_CHAR_C			RETURN "HUD_LETTER_C"
		CASE GGSM_HUD_CHAR_D			RETURN "HUD_LETTER_D"
		CASE GGSM_HUD_CHAR_E			RETURN "HUD_LETTER_E"
		CASE GGSM_HUD_CHAR_F			RETURN "HUD_LETTER_F"
		CASE GGSM_HUD_CHAR_G			RETURN "HUD_LETTER_G"
		CASE GGSM_HUD_CHAR_H			RETURN "HUD_LETTER_H"
		CASE GGSM_HUD_CHAR_I			RETURN "HUD_LETTER_I"
		CASE GGSM_HUD_CHAR_J			RETURN "HUD_LETTER_J"
		CASE GGSM_HUD_CHAR_K			RETURN "HUD_LETTER_K"
		CASE GGSM_HUD_CHAR_L			RETURN "HUD_LETTER_L"
		CASE GGSM_HUD_CHAR_M			RETURN "HUD_LETTER_M"
		CASE GGSM_HUD_CHAR_N			RETURN "HUD_LETTER_N"
		CASE GGSM_HUD_CHAR_O			RETURN "HUD_LETTER_O"
		CASE GGSM_HUD_CHAR_P			RETURN "HUD_LETTER_P"
		CASE GGSM_HUD_CHAR_Q			RETURN "HUD_LETTER_Q"
		CASE GGSM_HUD_CHAR_R			RETURN "HUD_LETTER_R"
		CASE GGSM_HUD_CHAR_S			RETURN "HUD_LETTER_S"
		CASE GGSM_HUD_CHAR_T			RETURN "HUD_LETTER_T"
		CASE GGSM_HUD_CHAR_U			RETURN "HUD_LETTER_U"
		CASE GGSM_HUD_CHAR_V			RETURN "HUD_LETTER_V"
		CASE GGSM_HUD_CHAR_W			RETURN "HUD_LETTER_W"
		CASE GGSM_HUD_CHAR_X			RETURN "HUD_LETTER_X"
		CASE GGSM_HUD_CHAR_Y			RETURN "HUD_LETTER_Y"
		CASE GGSM_HUD_CHAR_Z			RETURN "HUD_LETTER_Z"
		
		CASE GGSM_HUD_CHAR_APOSTROPHE	RETURN "HUD_SYMBOL_APOSTROPHE"
		CASE GGSM_HUD_CHAR_BACKSLASH	RETURN "HUD_SYMBOL_BACKSLASH"
		CASE GGSM_HUD_CHAR_CLOSEBRACKET	RETURN "HUD_SYMBOL_CLOSE_BRACKET"
		CASE GGSM_HUD_CHAR_COLON		RETURN "HUD_SYMBOL_COLON"
		CASE GGSM_HUD_CHAR_COMMA		RETURN "HUD_SYMBOL_COMMA"
		CASE GGSM_HUD_CHAR_EXCLAMATION	RETURN "HUD_SYMBOL_EXCLAMATION"
		CASE GGSM_HUD_CHAR_FULL_STOP	RETURN "HUD_SYMBOL_FULL_STOP"
		CASE GGSM_HUD_CHAR_MINUS		RETURN "HUD_SYMBOL_MINUS"
		CASE GGSM_HUD_CHAR_OPEN_BRACKET	RETURN "HUD_SYMBOL_OPEN_BRACKET"
		CASE GGSM_HUD_CHAR_PLUS			RETURN "HUD_SYMBOL_PLUS"
		CASE GGSM_HUD_CHAR_QUESTION		RETURN "HUD_SYMBOL_QUESTION"
		CASE GGSM_HUD_CHAR_SEMI_COLON	RETURN "HUD_SYMBOL_SEMI_COLON"
		CASE GGSM_HUD_CHAR_SPEECH		RETURN "HUD_SYMBOL_SPEECH"
		CASE GGSM_HUD_CHAR_STAR			RETURN "HUD_SYMBOL_STAR"
	ENDSWITCH 
	
	RETURN ""
ENDFUNC 

FUNC VECTOR_2D GGSM_GET_HUD_CHAR_SIZE(GGSM_HUD_CHAR eChar, FLOAT sx = 1.0, FLOAT sy = 1.0)
	VECTOR_2D vScale 
	
	vScale.y = GGSM_PX_HUD_CHAR_HEIGHT * sy 
	
	SWITCH (eChar)
		CASE GGSM_HUD_CHAR_APOSTROPHE
		CASE GGSM_HUD_CHAR_COLON
		CASE GGSM_HUD_CHAR_COMMA
		CASE GGSM_HUD_CHAR_EXCLAMATION
		CASE GGSM_HUD_CHAR_FULL_STOP
		CASE GGSM_HUD_CHAR_SEMI_COLON
			vScale.x = GGSM_PX_HUD_THIN_CHAR_WIDTH * sx
		BREAK 
		
		DEFAULT 
			vScale.x = GGSM_PX_HUD_CHAR_WIDTH * sx
		BREAK
	ENDSWITCH 
	
	RETURN vScale
ENDFUNC

PROC GGSM_PACK_HUD_CHAR_TO_ARRAY(GGSM_HUD_CHAR eChar, INT iIndex, INT &iCArray[])
	INT iIntArrayIndex = iIndex / 5
	
	IF (iIntArrayIndex >= COUNT_OF(iCArray))
		EXIT 
	ENDIF 
	
	INT iBitIntIndex = (iIndex % 5) * 6
	SET_BITS_IN_RANGE(iCArray[iIntArrayIndex], iBitIntIndex, iBitIntIndex + 5, ENUM_TO_INT(eChar))
ENDPROC 

PROC GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(INT iDigit, INT iIndex, INT &iCArray[])
	INT iIntArrayIndex = iIndex / 5
	
	IF (iIntArrayIndex >= COUNT_OF(iCArray))
		EXIT 
	ENDIF 
	
	INT iBitIntIndex = (iIndex % 5) * 6
	SET_BITS_IN_RANGE(iCArray[iIntArrayIndex], iBitIntIndex, iBitIntIndex + 5, ENUM_TO_INT(GGSM_HUD_CHAR_0) + iDigit)
ENDPROC 

FUNC GGSM_HUD_CHAR GGSM_UNPACK_HUD_CHAR_FROM_ARRAY(INT &iCArray[], INT iIndex)
	INT iIntArrayIndex = iIndex / 5
	INT iBitIntIndex = (iIndex % 5) * 6
	IF (iIntArrayIndex >= COUNT_OF(iCArray))
		RETURN GGSM_HUD_CHAR_INVALID
	ENDIF 
	
	RETURN INT_TO_ENUM(GGSM_HUD_CHAR, GET_BITS_IN_RANGE(iCArray[iIntArrayIndex], iBitIntIndex, iBitIntIndex + 5))
ENDFUNC 

PROC GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT eTextPivot)
	sGGSMData.eTextPivot = eTextPivot
ENDPROC 

PROC GGSM_SET_HUD_CHAR_SCALE(FLOAT sx = 1.0, FLOAT sy = 1.0)
	sGGSMData.vTextScale.x = sx
	sGGSMData.vTextScale.y = sy
ENDPROC 

PROC GGSM_DRAW_HUD_CHAR_POSITIVE_NUMBER_AT_POSITION(FLOAT fX, FLOAT fY, INT iIntToDisplay, INT iNumLength = 8)
	INT iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iIntToDisplay)	
	IF (iMaxDigits >= GGSM_HUD_MAX_NUM_LENGTH)
		EXIT 
	ENDIF 
	
	INT i
	INT iCurValue = iIntToDisplay
	TEXT_LABEL_15 str 
	str += iIntToDisplay
	INT iStrLen = GET_LENGTH_OF_LITERAL_STRING(str)
	GGSM_HUD_CHAR mChars[GGSM_HUD_MAX_NUM_LENGTH]

	VECTOR_2D vPos = INIT_VECTOR_2D(fx, fy)
	VECTOR_2D vScale = INIT_VECTOR_2D(GGSM_PX_HUD_CHAR_WIDTH * sGGSMData.vTextScale.x, GGSM_PX_HUD_CHAR_HEIGHT * sGGSMData.vTextScale.y)
	
	IF (iStrLen < iNumLength)
		vPos.x -= ((iNumLength - iStrLen) * vScale.x)
	ENDIF 
	
	REPEAT iMaxDigits i
		mChars[i] = INT_TO_ENUM(GGSM_HUD_CHAR, ENUM_TO_INT(GGSM_HUD_CHAR_0) + (iCurValue % 10))
		iCurValue /= 10
		
		ARCADE_DRAW_PIXELSPACE_SPRITE("MpGoGoSpaceResident", GGSM_GET_HUD_CHAR_SPRITE(mChars[i]), vPos, vScale, 0, sGGSMData.rgbaWhite)
		vPos.x -= vScale.x
	ENDREPEAT 
ENDPROC 

PROC GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(FLOAT fX, FLOAT fY, INT &iCArray[], INT iNumCharactersInArray)
	INT i
	GGSM_HUD_CHAR eChar
	VECTOR_2D vPos = INIT_VECTOR_2D(fx, fy)
	VECTOR_2D vScale = INIT_VECTOR_2D(GGSM_PX_HUD_CHAR_WIDTH * sGGSMData.vTextScale.x, GGSM_PX_HUD_CHAR_HEIGHT * sGGSMData.vTextScale.y)
	FLOAT sWidth 
	
	// get string len
	REPEAT iNumCharactersInArray i
		eChar = GGSM_UNPACK_HUD_CHAR_FROM_ARRAY(iCArray, i)	
		vScale = GGSM_GET_HUD_CHAR_SIZE(eChar, sGGSMData.vTextScale.x, sGGSMData.vTextScale.y)
		sWidth += vScale.x
	ENDREPEAT 
	
	vPos.y += (vScale.y / 2)
	SWITCH (sGGSMData.eTextPivot)
		CASE GGSM_TEXTPIVOT_LEFT
			vPos.x += (vScale.x / 2)
		BREAK 
		
		CASE GGSM_TEXTPIVOT_CENTER
			vPos.x -= (sWidth / 2.0)
		BREAK 
		
		CASE GGSM_TEXTPIVOT_RIGHT
			vPos.x -= (sWidth)
		BREAK 
	ENDSWITCH
			
	REPEAT iNumCharactersInArray i
		eChar = GGSM_UNPACK_HUD_CHAR_FROM_ARRAY(iCArray, i)
		IF (eChar != GGSM_HUD_CHAR_SPACE) AND (eChar != GGSM_HUD_CHAR_INVALID)
			vScale = GGSM_GET_HUD_CHAR_SIZE(eChar, sGGSMData.vTextScale.x, sGGSMData.vTextScale.y)
			ARCADE_DRAW_PIXELSPACE_SPRITE("MpGoGoSpaceResident", GGSM_GET_HUD_CHAR_SPRITE(eChar), vPos, vScale, 0, sGGSMData.rgbaWhite)
			
			vPos.x += vScale.x
		ELSE 
			vPos.x += (GGSM_PX_HUD_CHAR_WIDTH * sGGSMData.vTextScale.x)
		ENDIF 
	ENDREPEAT 
ENDPROC 

PROC GGSM_DRAW_PLAYER_SCORE_TEXT(FLOAT fx = 680.0, FLOAT fy = GGSM_PX_HUD_SCORE_Y)
	INT iMaxDigits, i
	INT iCurValue
	
	INT iCArray[3] //SC:        0 - # Chars:12
	iCArray[0] = 17208222
	iCArray[1] = 17043521
	iCArray[2] = 129
	
	// get number bit
	INT sc = sGGSMData.sGameStats.score
	
	IF (sc > 0)
		iCurValue = sc
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(sc)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 11 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(0.7, 0.7)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_LEFT)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 12)
ENDPROC

PROC GGSM_DRAW_PLAYER_SCORE_TEXT_WITH_MULTIPLIER(FLOAT fx = 680.0, FLOAT fy = GGSM_PX_HUD_SCORE_Y)
	INT iMaxDigits, i
	INT iCurValue
	INT sc
	INT iCArray[4] //SC:         0 [X00] - # Chars:19
	iCArray[0] = 17208222
	iCArray[1] = 17043521
	iCArray[2] = 788799553
	iCArray[3] = 10494115
	
	sc = FLOOR(sGGSMData.fChainMultiplier)
	IF (sc >= GGSM_CHAIN_MAX)
		iCArray[0] = 17208222
		iCArray[1] = 17043521
		iCArray[2] = 788799553
		iCArray[3] = 10629912	
	ELSE 
		IF (sc < 1)
			sc = 1
		ENDIF 

		iCurValue = sc
		
		REPEAT 2 i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 17 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 
	
	// get number bit
	sc = sGGSMData.sGameStats.score
	
	IF (sc > 0)
		iCurValue = sc
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(sc)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 12 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(0.7, 0.7)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_LEFT)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 19)
ENDPROC

PROC GGSM_DRAW_HIGH_SCORE_TEXT(FLOAT fx = 1716.0, FLOAT fy = GGSM_PX_HUD_SCORE_Y)
	INT iMaxDigits, i
	INT iCurValue
	
	// pack score bits
	INT iCArray[4] //HI:        0 [XXX] - # Chars:18
	iCArray[0] = 17208595
	iCArray[1] = 17043521
	iCArray[2] = 599527553
	iCArray[3] = 166115
	
	INT iSc = sAGLeaderboardData.sLeaderboard[0].iScore
	
	// get number bit
	IF (sGGSMData.sGameStats.score > sAGLeaderboardData.sLeaderboard[0].iScore)
		iSc = sGGSMData.sGameStats.score
	ENDIF 
		
	IF (iSc > 0)
		iCurValue = iSc
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iSc)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 11 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(0.7, 0.7)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_RIGHT)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 12)
ENDPROC

PROC GGSM_DRAW_GAME_OVER_TEXT(FLOAT scale = 2.0)
	INT iArray[2] //GAME OVER - # Chars:9
	iArray[0] = 21070610
	iArray[1] = 7669850

	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_CENTER)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(960, 540, iArray, 9)
ENDPROC

PROC GGSM_DRAW_PRESS_START_TEXT(FLOAT fx, FLOAT fy, FLOAT scale = 1.0, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iArray[3] //PRESS START - # Chars:11
	iArray[0] = 511248219
	iArray[1] = 489813889
	iArray[2] = 31
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iArray, 11)
ENDPROC

PROC GGSM_DRAW_BOSS_TEXT(FLOAT fx = 380.0, FLOAT fy = 940.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_LEFT)
	INT iCArray[1] //BOSS: - # Chars:5
	iCArray[0] = 695854733
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 5)
ENDPROC

PROC GGSM_DRAW_SECTOR_CLEAR_TEXT(FLOAT fx = cfBASE_SCREEN_HALF_WIDTH, FLOAT fy = 300.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iCArray[7] //S E C T O R  X  C L E A R E D - # Chars:29
	INT nChar
	
	IF (sGGSMData.sGameStats.level < GGSM_SECTOR_MAX)
		//S E C T O R  X  C L E A R E D - # Chars:29
		iCArray[0] = 235208798
		iCArray[1] = 23599041
		iCArray[2] = 25956445
		iCArray[3] = 22811521
		iCArray[4] = 486850640
		iCArray[5] = 3937281
		nChar = 29
		GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(sGGSMData.sGameStats.level, 13, iCArray)
	ELSE 
		//A L L  S E C T O R S  C L E A R E D - # Chars:35
		iCArray[0] = 386232396
		iCArray[1] = 268820545
		iCArray[2] = 24908673
		iCArray[3] = 503697498
		iCArray[4] = 386195521
		iCArray[5] = 19928065
		iCArray[6] = 93
		nChar = 33
	ENDIF 
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, nChar)
ENDPROC

PROC GGSM_DRAW_SECTOR_BONUS_TEXT(INT iPts, FLOAT fx = cfBASE_SCREEN_HALF_WIDTH, FLOAT fy = 470.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iCArray[6] //SECTOR BONUS:             0 - # Chars:27
	iCArray[0] = 444392478
	iCArray[1] = 426299485
	iCArray[2] = 17209248
	iCArray[3] = 17043521
	iCArray[4] = 17043521
	iCArray[5] = 129
	
	INT iMaxDigits, i
	INT iCurValue
	
	// get number bit
	IF (iPts > 0)
		iCurValue = iPts
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iPts)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 26 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 27)
ENDPROC

PROC GGSM_DRAW_TAKEDOWN_BONUS_TEXT(INT iPts, FLOAT fx = cfBASE_SCREEN_HALF_WIDTH, FLOAT fy = 470.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iCArray[6] //TAKEDOWN BONUS:           0 - # Chars:27
	iCArray[0] = 255943455
	iCArray[1] = 218470554
	iCArray[2] = 695862874
	iCArray[3] = 17043521
	iCArray[4] = 17043521
	iCArray[5] = 129
 
	INT iMaxDigits, i
	INT iCurValue
	
	// get number bit
	IF (iPts > 0)
		iCurValue = iPts
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iPts)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 26 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 27)
ENDPROC

PROC GGSM_DRAW_NO_MISS_BONUS_TEXT(INT iPts, FLOAT fx = cfBASE_SCREEN_HALF_WIDTH, FLOAT fy = 470.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iCArray[6] //NO DEATH BONUS:           0 - # Chars:27
	iCArray[0] = 272373401
	iCArray[1] = 218445772
	iCArray[2] = 695862874
	iCArray[3] = 17043521
	iCArray[4] = 17043521
	iCArray[5] = 129
	
	INT iMaxDigits, i
	INT iCurValue
	
	// get number bit
	IF (iPts > 0)
		iCurValue = iPts
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iPts)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 26 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 27)
ENDPROC

PROC GGSM_DRAW_COMPLETE_BONUS_TEXT(INT iPts, FLOAT fx = cfBASE_SCREEN_HALF_WIDTH, FLOAT fy = 470.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iCArray[6] //ALL CLEAR BONUS:          0 - # Chars:27
	iCArray[0] = 235238860
	iCArray[1] = 24429591
	iCArray[2] = 511809165
	iCArray[3] = 17043561
	iCArray[4] = 17043521
	iCArray[5] = 129

	INT iMaxDigits, i
	INT iCurValue
	
	// get number bit
	IF (iPts > 0)
		iCurValue = iPts
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iPts)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 26 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 27)
ENDPROC

PROC GGSM_DRAW_NO_DAMAGE_BONUS_TEXT(INT iPts, FLOAT fx = cfBASE_SCREEN_HALF_WIDTH, FLOAT fy = 470.0, FLOAT scale = 0.7, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER)
	INT iCArray[6] //NO DAMAGE BONUS:          0 - # Chars:27
	iCArray[0] = 205264537
	iCArray[1] = 21046040
	iCArray[2] = 511809165
	iCArray[3] = 17043561
	iCArray[4] = 17043521
	iCArray[5] = 129

	INT iMaxDigits, i
	INT iCurValue
	
	// get number bit
	IF (iPts > 0)
		iCurValue = iPts
		iMaxDigits = GGSM_GET_NUMBER_OF_DIGITS_IN_NUMBER(iPts)
		REPEAT iMaxDigits i
			GGSM_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 26 - i, iCArray)
			iCurValue /= 10
		ENDREPEAT 
	ENDIF 

	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 27)
ENDPROC

PROC GGSM_DRAW_SECTOR_MAP_TITLE_TEXT(FLOAT fx = GGSM_PX_HUD_MIN_X, FLOAT fy = GGSM_PX_HUD_MIN_Y, FLOAT scale = 1.0)
	INT iCArray[2] //SECTOR MAP - # Chars:10
	iCArray[0] = 444392478
	iCArray[1] = 456228957
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_LEFT)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 10)
ENDPROC

PROC GGSM_DRAW_SECTOR_NAME_TEXT(INT iSector, FLOAT fx = GGSM_PX_HUD_MIN_X, FLOAT fy = GGSM_PX_HUD_MIN_Y, GGSM_TEXTPIVOT pivot = GGSM_TEXTPIVOT_CENTER, FLOAT scale = 0.6)
	INT iCArray[4] //SECTOR MAP - # Chars:10
	INT iMaxChars 
	
	SWITCH (iSector)
		CASE GGSM_SECTOR_EARTH				//EARTH - # Chars:5
			iCArray[0] = 327013136
			iMaxChars = 5
		BREAK 
		
		CASE GGSM_SECTOR_ASTEROID_BELT		//OCYANS BELT - # Chars:11
			iCArray[0] = 422724506
			iCArray[1] = 390123614
			iCArray[2] = 31
			iMaxChars = 11
		BREAK 
		
		CASE GGSM_SECTOR_PINK_RING			//THE PINK RING - # Chars:13
			iCArray[0] = 453313759
			iCArray[1] = 486893140
			iCArray[2] = 75348
			iMaxChars = 13
		BREAK
		
		CASE GGSM_SECTOR_YELLOW_CLAM 		//YELLOW CLAM - # Chars:11
			iCArray[0] = 442332196
			iCArray[1] = 207413346
			iCArray[2] = 24
			iMaxChars = 11
		BREAK 
		
		CASE GGSM_SECTOR_DOUGHBALL 		//DOUGHBALL - # Chars:9
			iCArray[0] = 323618447
			iCArray[1] = 6124301
			iMaxChars = 9
		BREAK	
		
		CASE GGSM_SECTOR_BANANASTAR 	//THAT'S NO BANANA - # Chars:16
			iCArray[0] = 511493343
			iCArray[1] = 218474049
			iCArray[2] = 207930956
			iMaxChars = 15
		BREAK	
		
		DEFAULT //UNKNOWN - # Chars:7
			iCArray[0] = 442852960
			iCArray[1] = 1634
			iMaxChars = 7
		BREAK
	ENDSWITCH
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(pivot)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, iMaxChars)
ENDPROC


PROC GGSM_DRAW_THANK_YOU_TEXT(FLOAT fx = GGSM_PX_HUD_MIN_X, FLOAT fy = GGSM_PX_HUD_MIN_Y, FLOAT scale = 1.0)
	INT iCArray[5] //THANK YOU FOR PLAYING! - # Chars:22
	iCArray[0] = 375702751
	iCArray[1] = 25274625
	iCArray[2] = 453367441
	iCArray[3] = 424821527
	iCArray[4] = 2834
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_CENTER)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 22)
ENDPROC

PROC GGSM_DRAW_CONGRATULATIONS_TEXT(FLOAT fx = GGSM_PX_HUD_MIN_X, FLOAT fy = GGSM_PX_HUD_MIN_Y, FLOAT scale = 1.0)
	INT iCArray[4] //CONGRATULATIONS! - # Chars:16
	iCArray[0] = 491361934
	iCArray[1] = 207488972
	iCArray[2] = 509977887
	iCArray[3] = 44
	
	GGSM_SET_HUD_CHAR_SCALE(scale, scale)
	GGSM_SET_HUD_CHAR_PIVOT(GGSM_TEXTPIVOT_CENTER)
	GGSM_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 16)
ENDPROC

//-------------------------------------------------
// BINK FUNCTIONS
//-------------------------------------------------	

/// PURPOSE:
///    Releases Bink Movie 
/// PARAMS:
///    bmID - bink movie id
PROC GGSM_RELEASE_BINK_MOVIE(BINK_MOVIE_ID &bmID)
	IF (bmID != NULL)
		CDEBUG1LN(DEBUG_MINIGAME, "[GGSM] GGSM_RELEASE_BINK_MOVIE - Released movie") 
		STOP_BINK_MOVIE(bmID)
		RELEASE_BINK_MOVIE(bmID)
		bmID = NULL
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the bink movie asset for the video and starts playback
FUNC BOOL GGSM_START_BINK_MOVIE(BINK_MOVIE_ID &bmID, STRING movieName)
	CDEBUG1LN(DEBUG_MINIGAME, "[GGSM] GGSM_START_BINK_MOVIE - Starting movie:", movieName) 
	
	bmID = SET_BINK_MOVIE(movieName)
	IF (bmID = NULL)
		CASSERTLN(DEBUG_MINIGAME, "GGSM_START_BINK_MOVIE - Movie:", movieName, " does not exist")
		RETURN FALSE 
	ENDIF 
	
	PLAY_BINK_MOVIE(bmID)
	SET_BINK_SHOULD_SKIP(bmID, TRUE)
	SET_BINK_MOVIE_AUDIO_FRONTEND(bmID, TRUE)
	RETURN TRUE 
ENDFUNC 

/// PURPOSE:
///    Draws the movie and returns true when the intro movie has completed
FUNC BOOL GGSM_DRAW_BINK_MOVIE(BINK_MOVIE_ID bmID)
	DRAW_BINK_MOVIE(bmID, cfSCREEN_CENTER, cfSCREEN_CENTER, 1.0 * fAspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
	IF GET_BINK_MOVIE_TIME(bmID) >= 99.0		
		CDEBUG1LN(DEBUG_MINIGAME, "[GGSM] GGSM_DRAW_BINK_MOVIE - Stopping and releasing movie") 
		
		STOP_BINK_MOVIE(bmID)
		RELEASE_BINK_MOVIE(bmID)
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE	
ENDFUNC

//-------------------------------------------------
// COLLISION FUNCTIONS
//-------------------------------------------------

/// PURPOSE:
///    Draw the collision for an object
/// PARAMS:
///    data - transformed collision data
///    col - color to draw it
PROC GGSM_DRAW_AABB(GGSM_AABB &sAABB, RGBA_COLOUR_STRUCT col)
	VECTOR_2D dim = sAABB.vHalfSize

	dim.x *= 2.0
	dim.y *= 2.0
	ARCADE_DRAW_PIXELSPACE_RECT(sAABB.vCenter, dim, col)
	ARCADE_DRAW_PIXELSPACE_RECT(sAABB.vCenter, INIT_VECTOR_2D(2, 2), sGGSMData.rgbaWhite)
ENDPROC

/// PURPOSE:
///    Draw the collision for an object
/// PARAMS:
///    data - transformed collision data
///    col - color to draw it
PROC GGSM_DRAW_COLLIDER(GGSM_COLLIDER &data, RGBA_COLOUR_STRUCT col)
	FLOAT f	
	
	IF (data.sData.eType = GGSM_COLLISION_CIRCLE)
		f = (data.sData.fColRadius * 2.0)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceResident", "debugcircle", data.sData.vCollisionVectors[0], INIT_VECTOR_2D(f, f), 0, col)
		EXIT 
	ENDIF 
	
	IF (data.sData.eType = GGSM_COLLISION_CAPSULE)
		f = (data.sData.fColRadius * 2.0)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceResident", "debugcircle", data.sData.vCollisionVectors[0], INIT_VECTOR_2D(f, f), 0, col)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceResident", "debugcircle", data.sData.vCollisionVectors[1], INIT_VECTOR_2D(f, f), 0, col)
	ENDIF			
ENDPROC 

//-------------------------------------------------
// TILE MAP FUNCTIONS
//-------------------------------------------------

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Draws Collsion For a Tile 
/// PARAMS:
///    tm - tile map 
///    data - tile map collision
///    vTileLeftCorner - left corner to draw tile at
PROC GGSM_DRAW_TILE_COLLISION(GGSM_TILEMAP &tm, INT iTileIndex, VECTOR_2D vTileLeftCorner, BOOL bHFlip = FALSE, BOOL bVFlip = FALSE)
	INT i
	RGBA_COLOUR_STRUCT col
	GGSM_TILE_COLLISION_DATA data
	VECTOR_2D vSTemp[GGSM_MAX_TILE_POLYPOINTS]
	
	INIT_RGBA_STRUCT(col, 0, 255, 0, 128)
	
	// flip the data if required
	IF (bHFlip OR bVFlip)
		GGSM_TILEMAP_FLIP_COLLISION(tm, iTileIndex, data, bHFlip, bVFlip)
	ELSE 
		data = tm.sCollisionData[iTileIndex]
	ENDIF 
	
	REPEAT data.iNumPoints i
		vSTemp[i] = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(ADD_VECTOR_2D(data.vCollisionVectors[i], vTileLeftCorner))
	ENDREPEAT 
			
	REPEAT data.iNumPoints i
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vSTemp[i], vSTemp[(i + 1) % data.iNumPoints], col)
	ENDREPEAT
ENDPROC 	

/// PURPOSE:
///    Draws Collsion For a Tile 
/// PARAMS:
///    tm - tile map 
///    data - tile map collision
///    vTileLeftCorner - left corner to draw tile at
PROC GGSM_DRAW_TILE_COLLISION_FOR_EDITING(GGSM_TILE_COLLISION_DATA &data, VECTOR_2D vTileLeftCorner)
	INT i
	RGBA_COLOUR_STRUCT col
	VECTOR_2D vSTemp[GGSM_MAX_TILE_POLYPOINTS]
	
	INIT_RGBA_STRUCT(col, 0, 255, 0, 128)
	
	REPEAT data.iNumPoints i
		vSTemp[i] = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(ADD_VECTOR_2D(data.vCollisionVectors[i], vTileLeftCorner))
	ENDREPEAT 
			
	REPEAT data.iNumPoints i
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vSTemp[i], vSTemp[(i + 1) % data.iNumPoints], col)
	ENDREPEAT
ENDPROC 	

#ENDIF

//-------------------------------------------------
// CAMERA FUNCTIONS
//-------------------------------------------------

/// PURPOSE:
///    Draws a map tile in screen space
/// PARAMS:
///    cam - camera 
///    tileMap - tilemap
///    iMapX - map coord x
///    iMapY - map coord y
///    col - color to draw
PROC GGSM_CAMERA_DRAW_MAPTILE(GGSM_CAMERA &cam, GGSM_TILEMAP &tileMap, INT iMapX, INT iMapY, RGBA_COLOUR_STRUCT col)
	
	INT iTile
	BOOL bHFlip, bVFlip
	IF NOT GGSM_TILEMAP_GET_DATA(tileMap, iMapX, iMapY, iTile, bHFlip, bVFlip)
		EXIT 
	ENDIF 
	
	IF (iTile = 0)
		EXIT 
	ENDIF 
	
	VECTOR_2D vDrawPos 
	VECTOR_2D vTileWorldPos 
	VECTOR_2D vScale 
	FLOAT fTileSizeHalf = (tileMap.fTileSize / 2.0)
	FLOAT fDrawSize = tileMap.fTileSize
	
	// work out tile pos 
	vTileWorldPos.x = (iMapX * tileMap.fTileSize)
	vTileWorldPos.y = (iMapY * tileMap.fTileSize)
	
	// drawPos 
	vDrawPos = SUBTRACT_VECTOR_2D(vTileWorldPos, cam.vWorldPosition)
	vDrawPos.x = TO_FLOAT(FLOOR(vDrawPos.x)) + fTileSizeHalf
	vDrawPos.y = TO_FLOAT(FLOOR(vDrawPos.y)) + fTileSizeHalf
	
	TEXT_LABEL_23 sSprName = tileMap.sTileSetBaseName
	sSprName += iTile 
	
	IF (tileMap.fTileDrawSize != 0.0)
		fDrawSize = tileMap.fTileDrawSize
	ENDIF
	
	vScale = INIT_VECTOR_2D(fDrawSize, fDrawSize)
	IF bHFlip
		vScale.x *= -1.0
	ENDIF 
	
	IF bVFlip
		vScale.y *= -1.0
	ENDIF 
	
	vDrawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(vDrawPos)
	ARCADE_DRAW_PIXELSPACE_SPRITE(tileMap.sTileDirectory, sSprName, vDrawPos, vScale, 0, col)
	
	#IF IS_DEBUG_BUILD
	IF (sGGSMData.bDrawCollisionDebug)
		vDrawPos.x -= fTileSizeHalf
		vDrawPos.y -= fTileSizeHalf
		GGSM_DRAW_TILE_COLLISION(tileMap, iTile, vDrawPos, bHFlip, bVFlip)
	ENDIF 
	#ENDIF
ENDPROC 

/// PURPOSE:
///    Draws a highlight over map tile - this is really only used for the editor
/// PARAMS:
///    cam - camera 
///    tileMap - tilemap
///    iMapX - x mapcoord
///    iMapY - y mapcoor
///    col - colour
PROC GGSM_CAMERA_DRAW_MAPTILE_HIGHLIGHT(GGSM_CAMERA &cam, GGSM_TILEMAP &tileMap, INT iMapX, INT iMapY, RGBA_COLOUR_STRUCT col)
	VECTOR_2D vDrawPos 
	VECTOR_2D vTileWorldPos 
	FLOAT fTileSizeHalf = (tileMap.fTileSize / 2.0)
	
	// work out tile pos 
	vTileWorldPos.x = (iMapX * tileMap.fTileSize)
	vTileWorldPos.y = (iMapY * tileMap.fTileSize)
	
	// drawPos 
	vDrawPos = SUBTRACT_VECTOR_2D(vTileWorldPos, cam.vWorldPosition)
	vDrawPos.x = TO_FLOAT(FLOOR(vDrawPos.x)) + fTileSizeHalf
	vDrawPos.y = TO_FLOAT(FLOOR(vDrawPos.y)) + fTileSizeHalf
	
	ARCADE_DRAW_PIXELSPACE_RECT(vDrawPos, INIT_VECTOR_2D(tileMap.fTileSize, tileMap.fTileSize), col)
ENDPROC 

/// PURPOSE:
///    Draws a map tile - this is really only used for the editor
/// PARAMS:
///    cam - camera 
///    tileMap - tilemap
///    iMapX - x mapcoord
///    iMapY - y mapcoor
///    iTile - tileoverride
///    col - colour
PROC GGSM_CAMERA_DRAW_MAPTILE_OVERRIDE(GGSM_CAMERA &cam, GGSM_TILEMAP &tileMap, INT iMapX, INT iMapY, INT iTile, RGBA_COLOUR_STRUCT col, BOOL bHlip, BOOL bVFlip)
	IF IS_STRING_NULL_OR_EMPTY(tileMap.sTileSetBaseName)
		EXIT 
	ENDIF
	
	VECTOR_2D vDrawPos 
	VECTOR_2D vTileWorldPos 
	VECTOR_2D vScale = INIT_VECTOR_2D(tileMap.fTileSize, tileMap.fTileSize)
	FLOAT fTileSizeHalf = (tileMap.fTileSize / 2.0)
	
	// work out tile pos 
	vTileWorldPos.x = (iMapX * tileMap.fTileSize)
	vTileWorldPos.y = (iMapY * tileMap.fTileSize)
	
	// drawPos 
	vDrawPos = SUBTRACT_VECTOR_2D(vTileWorldPos, cam.vWorldPosition)
	vDrawPos.x = TO_FLOAT(FLOOR(vDrawPos.x)) + fTileSizeHalf
	vDrawPos.y = TO_FLOAT(FLOOR(vDrawPos.y)) + fTileSizeHalf
	
	TEXT_LABEL_23 sSprName = tileMap.sTileSetBaseName
	sSprName += iTile 
	
	IF bHlip
		vScale.x *= -1.0
	ENDIF 
	
	IF bVFlip
		vScale.y *= -1.0
	ENDIF 
	
	vDrawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(vDrawPos)
	//ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(tileMap.sTileDirectory, sSprName, vDrawPos, vScale, 0, col)
	ARCADE_DRAW_PIXELSPACE_SPRITE(tileMap.sTileDirectory, sSprName, vDrawPos, vScale, 0, col)
ENDPROC 

/// PURPOSE:
///    Draws the tile map
/// PARAMS:
///    cam - camera 
///    tileMap - tilemap
PROC GGSM_CAMERA_DRAW_TILE_MAP(GGSM_CAMERA &cam, GGSM_TILEMAP &tileMap)
	#IF IS_DEBUG_BUILD
	IF (g_bBlockArcadeCabinetDrawing)
		EXIT 
	ENDIF
	#ENDIF
	
	INT x, y 
	INT tx, ty 
	
	IF IS_STRING_NULL_OR_EMPTY(tileMap.sTileSetBaseName)
		EXIT 
	ENDIF
	
	REPEAT tileMap.sVisibleTileRect.w x
		REPEAT tileMap.sVisibleTileRect.h y		
			tx = (tileMap.sVisibleTileRect.x + x)
			ty = (tileMap.sVisibleTileRect.y + y)			
			GGSM_CAMERA_DRAW_MAPTILE(cam, tileMap, tx, ty, sGGSMData.rgbaWhite)		
		ENDREPEAT
	ENDREPEAT 	
ENDPROC 

//-------------------------------------------------
// OBJECT FUNCTIONS
//-------------------------------------------------

/// PURPOSE:
///    Draws a sprite from screenspace values
///    Note: Use ARCADE_CABINET_DRAW_SPRITE_ROTATED for non-square rotated sprites
PROC ARCADE_CABINET_DRAW_SPRITE_UV(STRING stTextureDict, STRING stTexture, FLOAT fCenterX, FLOAT fCenterY, FLOAT fScaleX, FLOAT fScaleY, FLOAT fRotation, FLOAT ux, FLOAT uy, FLOAT vx, FLOAT vy, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] Invalid Dictionary string passed. SPRITE will not display")
		ASSERTLN("[GRID_ARCADE] [JS] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_SPRITE_WITH_UV(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX) * g_fMinimizeArcadeCabinetCentreX, fCenterY * g_fMinimizeArcadeCabinetCentreY, fScaleX * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, fScaleY * g_fMinimizeArcadeCabinetCentreScale, ux, uy, vx, vy,  fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	ELSE
#ENDIF
		INT iPixels = ROUND(fCenterY * iScreenY)
		fCenterY = TO_FLOAT(iPixels) * (1.0 / iScreenY)
		
		iPixels = ROUND((fScaleY * iScreenY) / 4) * 4
		fScaleY = TO_FLOAT(iPixels) * (1.0 / iScreenY)
		
		DRAW_SPRITE_WITH_UV(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX), fCenterY, fScaleX * fAspectRatioModifier, fScaleY, ux, uy, vx, vy, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

/// PURPOSE:
///    Draws a sprite from pixelspace (reference resolution: 1920x1080) values
PROC ARCADE_DRAW_PIXELSPACE_SPRITE_UV(STRING stTextureDict, STRING stTexture, VECTOR_2D vCenter, VECTOR_2D vScale, FLOAT fRotation, FLOAT ux, FLOAT uy, FLOAT vx, FLOAT vy, RGBA_COLOUR_STRUCT rgba)
	vCenter = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vCenter)
	vScale = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vScale)
	ARCADE_CABINET_DRAW_SPRITE_UV(stTextureDict, stTexture, vCenter.x, vCenter.y, vScale.x, vScale.y, ux, uy, vx, vy, fRotation, rgba)
ENDPROC

PROC GGSM_DRAW_SLICED_SPRITE(GGSM_SPRITE_TYPE eType, VECTOR_2D vPos, INT iSlices = 8)

	STRING tName = GGSM_GET_SPRITE_DICT_NAME(eType)
	STRING sName = GGSM_GET_SPRITE_NAME(eType)
	UNUSED_PARAMETER(vPos)
	
	/*
	INT i

	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(eType)
	
	vSize.y /= TO_FLOAT(iSlices)
	FLOAT uvGap = 1.0 / TO_FLOAT(iSlices)
	FLOAT uy = 0.0
	
	REPEAT iSlices i
		//ARCADE_DRAW_PIXELSPACE_SPRITE_UV(tName, sName, vPos, vSize, 0, 0.0, uy, 1.0, uy + uvGap, sGGSMData.rgbaWhite)
		ARCADE_DRAW_PIXELSPACE_RECT(vPos, vSize, sGGSMData.rgbaWhite)
		uy += uvGap
		vPos.y += vSize.y
	ENDREPEAT
	
	UNUSED_PARAMETER(uy)
	*/
	
	//vPos = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vPos)
	IF NOT IS_STRING_NULL_OR_EMPTY(tName)
		IF NOT IS_STRING_NULL_OR_EMPTY(sName)
			DRAW_SPRITE_WITH_UV(tName, sName, 0.5, 0.5, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0, 255, 255, 255, 255)
		ENDIF
	ENDIF 
	
	UNUSED_PARAMETER(iSlices)
ENDPROC 

/// PURPOSE:
///    Draw an entity
/// PARAMS:
///    ent - entity to draw
PROC GGSM_ENTITY_DRAW(GGSM_ENTITY &ent)
	GGSM_ENTITY_DATA dat = GGSM_ENTITY_DATA_GET(ent.eType)
	GGSM_SPRITE_TYPE spr = dat.eSpriteType
	INT iFlame 
	IF (spr = GGSM_SPRITE_NONE)
		EXIT 
	ENDIF 
	
	VECTOR_2D size = GGSM_GET_SPRITE_SIZE(spr) 
	RGBA_COLOUR_STRUCT col = sGGSMData.rgbaSprite
	VECTOR_2D drawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(ent.sWorld.vPosition)
	
	IF (ent.eType = GGSM_ENTITY_PLAYERSHIP) 	
		IF (GET_GAME_TIMER() < ent.iInvTimeMS)
			INIT_RGBA_STRUCT(col, 128, 128, 128, 128)
			ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_SPRITE_DICT_NAME(GGSM_SPRITE_SHIELD), GGSM_GET_SPRITE_NAME(GGSM_SPRITE_SHIELD), drawPos, GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_SHIELD), 0, col)
			
			//ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(GGSM_GET_SPRITE_DICT_NAME(GGSM_SPRITE_SHIELD), GGSM_GET_SPRITE_NAME(GGSM_SPRITE_SHIELD), drawPos, GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_SHIELD), 0, col)
		ENDIF
	ENDIF
		
	GGSM_UNPACK_RGBA_COLOUR(ent.iPackedColor, col)
	BOOL bPained = IS_BITMASK_ENUM_AS_ENUM_SET(ent.eFlags, GGSM_ENTITY_BIT_TOOK_DAMAGE_THIS_FRAME)
	BOOL bBroken = ((ent.eState = GGSM_ENTITY_STATE_UNDEAD) OR (ent.eState = GGSM_ENTITY_STATE_EXPLODING))
	
	IF (GET_GAME_TIMER() < ent.iInvTimeMS)
		
		
		IF ((GET_FRAME_COUNT() / 4) % 2 = 0)
			col.iR = 255
			col.iG = 255
			col.iB = 255
		ENDIF
		
		IF (ent.eType != GGSM_ENTITY_PLAYERSHIP)
			bPained = TRUE 
		ENDIF 
	ENDIF 
	
	IF (bPained = TRUE) AND (ent.eType = GGSM_ENTITY_PLAYERSHIP)
		col.iR = 255
		col.iG = 0
		col.iB = 0	
	ENDIF 
	
	IF (ent.eType = GGSM_ENTITY_PLAYERSHIP)
		drawPos.x -= 50.0
		drawPos.y += 14.0
		iFlame = GET_FRAME_COUNT() % 3
		ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_SPRITE_DICT_NAME(GGSM_SPRITE_FLAME), GGSM_GET_SPRITE_NAME(GGSM_SPRITE_FLAME, iFlame), drawPos, GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_FLAME), 0, sGGSMData.rgbaWhite)
	ENDIF
	
	IF (ent.eType = GGSM_ENTITY_PLAYERSHIELD) AND GGSM_DOES_PLAYER_SHIP_EXIST()
		drawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(sGGSMData.sEntities[sGGSMData.iPlayerShipIndex].sWorld.vPosition)
	ELSE 
		drawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(ent.sWorld.vPosition)
	ENDIF 
	
	size.x *= ent.sWorld.vScale.x
	size.y *= ent.sWorld.vScale.y
	ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_SPRITE_DICT_NAME(dat.eSpriteType), GGSM_GET_SPRITE_NAME(dat.eSpriteType, FLOOR(ent.fAnimFrame), bPained, bBroken), drawPos, size, ent.sWorld.fRotation + dat.fSpriteRotationFix, col)		
	//ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(GGSM_GET_SPRITE_DICT_NAME(dat.eSpriteType), GGSM_GET_SPRITE_NAME(dat.eSpriteType, FLOOR(ent.fAnimFrame), bPained), drawPos, size, ent.sWorld.fRotation + dat.fSpriteRotationFix, col)		
	
	#IF IS_DEBUG_BUILD
		IF (sGGSMData.bDrawCollisionDebug)
			GGSM_DRAW_COLLIDER(ent.sCollider, col)
			INIT_RGBA_STRUCT(col, 255, 255, 255, 64)
			GGSM_DRAW_AABB(ent.sCollider.sAABB, col)
			
			IF (ent.eType = GGSM_ENTITY_PLAYERSHIP)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceResident", "debugcircle", drawPos, INIT_VECTOR_2D(sGGSMData.fGrazeRadius * 2.0, sGGSMData.fGrazeRadius * 2.0), 0, col)
			ENDIF 
		ENDIF
	#ENDIF 
ENDPROC 

/// PURPOSE:
///    Draw a projectile array
/// PARAMS:
///    array - array to draw
PROC GGSM_PROJECTILE_ARRAY_DRAW(GGSM_PROJECTILE &array[])	
	#IF IS_DEBUG_BUILD
	RGBA_COLOUR_STRUCT col
	#ENDIF
	
	INT i
	INT cnt = COUNT_OF(array)
	GGSM_PROJECTILE_DATA dat
	GGSM_SPRITE_TYPE spr 
	FLOAT fRotation
	VECTOR_2D size
	VECTOR_2D drawPos 
	STRING str 
	
	REPEAT cnt i
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(array[i].eFlags, GGSM_PROJECTILE_BIT_ACTIVE)		
			dat = GGSM_PROJECTILE_DATA_GET(array[i].eType)

			fRotation = GET_HEADING_FROM_VECTOR_2D(array[i].vDirection.x, array[i].vDirection.y)
			size.x = (array[i].sCollider.sData.fColRadius * 2.0)
			size.y = array[i].fLength + size.x
			drawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(array[i].sCollider.sAABB.vCenter)
			spr = dat.eEnemySprite
			IF IS_BITMASK_ENUM_AS_ENUM_SET(array[i].eFlags, GGSM_PROJECTILE_BIT_IS_PLAYER_ALLIED)		
				spr = dat.eAlliedSprite
			ENDIF 
			
			IF (array[i].eType = GGSM_PROJECTILE_BOMB_EXPLOSION)
				str = GGSM_GET_SPRITE_NAME(spr, FLOOR(array[i].fTimer * 10.0))
				IF IS_STRING_NULL_OR_EMPTY(str) 
					array[i].eFlags = GGSM_PROJECTILE_BIT_NONE 
					RELOOP 
				ENDIF 
				
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceCommon", str, drawPos, size, fRotation, sGGSMData.rgbaWhite)					
			ELSE 
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceCommon", GGSM_GET_SPRITE_NAME(spr), drawPos, size, fRotation, sGGSMData.rgbaWhite)					
			ENDIF 
			
			#IF IS_DEBUG_BUILD
				IF (sGGSMData.bDrawCollisionDebug)
					IF IS_BITMASK_ENUM_AS_ENUM_SET(array[i].eFlags, GGSM_PROJECTILE_BIT_IS_PLAYER_ALLIED) 
						INIT_RGBA_STRUCT(col, 0, 0, 255, 128)
					ELSE 
						INIT_RGBA_STRUCT(col, 255, 0, 0, 128)
					ENDIF
					GGSM_DRAW_COLLIDER(array[i].sCollider, col)
					INIT_RGBA_STRUCT(col, 255, 255, 255, 32)
					GGSM_DRAW_AABB(array[i].sCollider.sAABB, col)
				ENDIF 
			#ENDIF
		ENDIF
	ENDREPEAT 	
ENDPROC

/// PURPOSE:
///    Update the projectile array
/// PARAMS:
///    array - array
/// RETURNS:
///    # of active projectiles
PROC GGSM_DRAW_FXSPRITE_ARRAY(GGSM_FX_SPRITE &array[])
	INT i
	INT cnt = COUNT_OF(array)
	VECTOR_2D size, drawPos
	RGBA_COLOUR_STRUCT col = sGGSMData.rgbaWhite

	REPEAT cnt i
		IF IS_BITMASK_ENUM_AS_ENUM_SET(array[i].eFlags, GGSM_FX_SPRITE_BIT_ACTIVE) 
			GGSM_UNPACK_RGBA_COLOUR(array[i].iPackedColor, col)
			size = GGSM_GET_SPRITE_SIZE(array[i].eSpriteType)
			size.x *= array[i].fScale
			size.y *= array[i].fScale
			
			drawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(GGSM_UNPACK_VECTOR_2D(array[i].iPackedPosition))
			ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_SPRITE_DICT_NAME(array[i].eSpriteType), GGSM_GET_SPRITE_NAME(array[i].eSpriteType, FLOOR(array[i].fAnimFrame)), drawPos, size, 0.0, col)		
		ENDIF
	ENDREPEAT 	
ENDPROC 

PROC GGSM_DRAW_POWER_UP_ARRAY(GGSM_POWER_UP &pArray[])
	INT i
	VECTOR_2D v
	VECTOR_2D size = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_POWER_UP_BEAM)
	
	REPEAT COUNT_OF(pArray) i
		IF (pArray[i].eType != GGSM_SPRITE_NONE)
			v = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(GGSM_UNPACK_VECTOR_2D(pArray[i].iPackedPosition))
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceCommon", GGSM_GET_SPRITE_NAME(pArray[i].eType), v, size, 0, sGGSMData.rgbaSprite)	
		ENDIF
	ENDREPEAT 
ENDPROC 

//-------------------------------------------------
// SCREEN FUNCTIONS
//-------------------------------------------------
PROC GGSM_DRAW_BLACK_BG()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing OR g_bMinimizeArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
ENDPROC

PROC GGSM_BGSCROLLER_DRAW(GGSM_BGSCROLLER &scr)
	INT i
	STRING sprName = GGSM_GET_SPRITE_NAME(GGSM_SPRITE_BG_SPACE)
	STRING txtDict = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_BG)
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_BG_SPACE)
	VECTOR_2D vDrawPos 
	
	// draw stars
	REPEAT GGSM_MAX_STAR_BG i
		vDrawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(scr.vSpaceBgPos[i])
		ARCADE_DRAW_PIXELSPACE_SPRITE(txtDict, sprName, vDrawPos, vSize, 0, sGGSMData.rgbaWhite)
	ENDREPEAT 
	
	// draw floor
	sprName = GGSM_GET_SPRITE_BG_LAYER_NAME_FOR_STAGE(scr.iStage)
	vSize = GGSM_GET_SPRITE_BG_LAYER_SIZE_FOR_STAGE(scr.iStage)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sprName)
		REPEAT GGSM_MAX_FLOOR_BG i
			vDrawPos = GGSM_FLOOR_VECTOR_2D_FOR_RENDERING(scr.vFloorBgPos[i])
			ARCADE_DRAW_PIXELSPACE_SPRITE(txtDict, sprName, vDrawPos, vSize, 0, sGGSMData.rgbaWhite)
		ENDREPEAT 
	ENDIF
	
	// draw bg sprites
	GGSM_DRAW_FXSPRITE_ARRAY(scr.sBgSprite)
ENDPROC 

PROC GGSM_DRAW_BG()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	GGSM_BGSCROLLER_DRAW(sGGSMData.sBGScroller)
ENDPROC 

PROC GGSM_DRAW_FACADE()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	RGBA_COLOUR_STRUCT col 
	VECTOR_2D size = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_FACADE)
	IF (sGGSMData.fWhiteOutAlpha > 0)
		INIT_RGBA_STRUCT(col, 255, 255, 255, FLOOR(255.0 * sGGSMData.fWhiteOutAlpha))
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), col)
	ENDIF
	
	IF (sGGSMData.fFadeAlpha > 0)
		INIT_RGBA_STRUCT(col, 0, 0, 0, FLOOR(255.0 * sGGSMData.fFadeAlpha))
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), col)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (sGGSMData.bDebugDisablePostFX)
			ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_RESIDENT), "FACADE", INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), size, 0.0, sGGSMData.rgbaWhite)	
			EXIT 
		ENDIF 
	#ENDIF 
		
	ARCADE_GAMES_POSTFX_DRAW()

	// Draw Facade 
	ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_RESIDENT), "FACADE", INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), size, 0.0, sGGSMData.rgbaWhite)	
ENDPROC

/// PURPOSE:
///    Draw Energy Bar
/// PARAMS:
///    px - top left corner x
///    py - top right x
///    pw - width
///    ph - height
///    pc - percentage of energy bar to fill
///    col - colour of fill
///    iBorder - Border size in pixels
PROC GGSM_DRAW_ENERGY_BAR(FLOAT px, FLOAT py, FLOAT pw, FLOAT ph, FLOAT pc, RGBA_COLOUR_STRUCT col, INT iBorder = 2)
	//RGBA_COLOUR_STRUCT backCol = col 
	VECTOR_2D center = GGSM_GET_CENTER_FROM_CORNER_RECT(px, py, pw, ph)
	VECTOR_2D size 
	
	// draw border
	ARCADE_DRAW_PIXELSPACE_RECT(center, INIT_VECTOR_2D(pw, ph), sGGSMData.rgbaWhite)
	size = INIT_VECTOR_2D(pw - (iBorder * 2), ph - (iBorder * 2))
	
	// draw back fill
	IF (pc < 1.0)
		center = GGSM_GET_CENTER_FROM_CORNER_RECT(px + iBorder, py + iBorder, size.x, size.y)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MpGoGoSpaceCommon", "HEALTH_BOX_BOSS", center, size, 0, col)
	ENDIF 
	
	// draw front bar
	IF (pc > 0.0)
		size.x *= pc
		center = GGSM_GET_CENTER_FROM_CORNER_RECT(px + iBorder, py + iBorder, size.x, size.y)	
		ARCADE_DRAW_PIXELSPACE_SPRITE("MpGoGoSpaceCommon", "HEALTH_BAR_BOSS", center, size, 0, col)
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws the game hud
PROC GGSM_DRAW_GAME_HUD()

	#IF IS_DEBUG_BUILD
		IF GGSM_IS_ANY_EDITOR_RUNNING()
		 	EXIT 
		ENDIF 
	#ENDIF 
	
	IF GGSM_IS_DIALOG_ACTIVE()
		EXIT 
	ENDIF 
	
	// Draw Hud
	FLOAT hpc = GGSM_GET_PLAYER_SHIP_HP_PERCENTAGE()
	VECTOR_2D pos 
	INT ind
		
	RGBA_COLOUR_STRUCT col 
	
	// draw player energy bar
	INIT_RGBA_STRUCT(col, 255, 0, 0, 255)
	GGSM_DRAW_ENERGY_BAR(GGSM_PX_HUD_MIN_X + 5, GGSM_PX_HUD_MIN_Y + 5, GGSM_PX_HUD_HEALTH_BAR_WIDTH, GGSM_PX_HUD_HEALTH_BAR_HEIGHT, hpc, col)
	
	// draw boss energy bar
	
	IF GGSM_IS_TEXTURE_DICT_LOADED(GGSM_TEXDICT_PORTRAITS)
		GGSM_SPRITE_TYPE spr 
		FLOAT fy = GGSM_PX_BOSS_HP_BAR_Y_POS
		REPEAT COUNT_OF(sGGSMData.sBossController) ind 
			IF (sGGSMData.sBossController[ind].iMaxTotalHP > 0)
				hpc = TO_FLOAT(sGGSMData.sBossController[ind].iTotalHP) / TO_FLOAT(sGGSMData.sBossController[ind].iMaxTotalHP)	
				pos = INIT_VECTOR_2D(GGSM_PX_BOSS_HP_BAR_X_POS - 38, fy + (GGSM_PX_BOSS_HP_BAR_HEIGHT / 2.0))
				spr = GGSM_GET_BOSS_CONTROLLER_PORTRAIT(sGGSMData.sBossController[ind].eType)
				ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_PORTRAITS), GGSM_GET_SPRITE_NAME(spr), pos, GGSM_GET_SPRITE_SIZE(spr), 0, sGGSMData.rgbaWhite)
				GGSM_DRAW_ENERGY_BAR(GGSM_PX_BOSS_HP_BAR_X_POS, fy, GGSM_PX_BOSS_HP_BAR_LENGTH, GGSM_PX_BOSS_HP_BAR_HEIGHT, hpc, col)
				fy -= GGSM_PX_BOSS_BAR_SEPERATION
			ENDIF 
		ENDREPEAT 
	ENDIF 
		
	// life icons 
	VECTOR_2D size = INIT_VECTOR_2D(GGSM_PX_HUD_LIFE_ICON_SIZE, GGSM_PX_HUD_LIFE_ICON_SIZE)
	pos = GGSM_GET_CENTER_FROM_CORNER_RECT(GGSM_PX_HUD_MIN_X + 5, GGSM_PX_HUD_MIN_Y + 15 + GGSM_PX_HUD_HEALTH_BAR_HEIGHT, size.x, size.y)
	
	REPEAT sGGSMData.iPlayerLives ind
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceCommon", "LIVES_REMAINING", pos, size, 0, sGGSMData.rgbaWhite)
		pos.x += (GGSM_PX_HUD_LIFE_ICON_SIZE + 5)
	ENDREPEAT
	
	// weapon icons
	size = INIT_VECTOR_2D(GGSM_PX_HUD_WEAPON_BOX_SIZE, GGSM_PX_HUD_WEAPON_BOX_SIZE)
	pos.x = GGSM_PX_HUD_MAX_X - (TO_FLOAT(GGSM_MAX_WEAPON_SLOTS) * (GGSM_PX_HUD_WEAPON_BOX_SIZE + 10)) + (GGSM_PX_HUD_WEAPON_BOX_SIZE / 2) 
	pos.y = GGSM_PX_HUD_MIN_Y + (GGSM_PX_HUD_WEAPON_BOX_SIZE / 2)
	
	INIT_RGBA_STRUCT(col, 0, 0, 0, 64)
	REPEAT GGSM_MAX_WEAPON_SLOTS ind
		ARCADE_DRAW_PIXELSPACE_RECT(pos, INIT_VECTOR_2D(GGSM_PX_HUD_WEAPON_BOX_SIZE - 4, GGSM_PX_HUD_WEAPON_BOX_SIZE - 4), col)
		IF (sGGSMData.eWeaponSlot[ind] != GGSM_SPRITE_NONE)
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceCommon", GGSM_GET_SPRITE_NAME(sGGSMData.eWeaponSlot[ind]), pos, INIT_VECTOR_2D(GGSM_PX_HUD_WEAPON_BOX_SIZE - 8, GGSM_PX_HUD_WEAPON_BOX_SIZE - 8), 0, sGGSMData.rgbaWhite)
		ENDIF
		
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceCommon", "WEAPON_BOX", pos, size, 0, sGGSMData.rgbaWhite)
		pos.x += (GGSM_PX_HUD_WEAPON_BOX_SIZE + 10)
	ENDREPEAT	
	
	// draw score 
	pos.x = GGSM_PX_HUD_MAX_X - (TO_FLOAT(GGSM_MAX_WEAPON_SLOTS) * (GGSM_PX_HUD_WEAPON_BOX_SIZE + 10)) + (GGSM_PX_HUD_WEAPON_BOX_SIZE / 2) 
	pos.y = GGSM_PX_HUD_MIN_Y + (GGSM_PX_HUD_WEAPON_BOX_SIZE / 2)
	pos.x -= ((GGSM_PX_HUD_WEAPON_BOX_SIZE / 2) + 15)
	GGSM_DRAW_PLAYER_SCORE_TEXT()
	//GGSM_DRAW_PLAYER_SCORE_TEXT_WITH_MULTIPLIER()
	GGSM_DRAW_HIGH_SCORE_TEXT(pos.x)//GGSM_PX_HUD_MAX_X - (5 + GGSM_PX_HUD_HEALTH_BAR_WIDTH))


ENDPROC 

PROC GGSM_DRAW_MAIN_GAME()	
	INT ind, ent 
	RGBA_COLOUR_STRUCT col 
	
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	GGSM_DRAW_BG()
				
	REPEAT COUNT_OF(sGGSMData.sEntities) ind 
		ent = sGGSMData.sZSortData[ind].iEntityIndex
		IF (ent != GGSM_INVALID_ENTITY)
			IF (sGGSMData.sEntities[ent].eState != GGSM_ENTITY_STATE_NONE)
				GGSM_ENTITY_DRAW(sGGSMData.sEntities[ent])
			ENDIF
		ENDIF
	ENDREPEAT 
	
	GGSM_DRAW_FXSPRITE_ARRAY(sGGSMData.sFXSprite)
	GGSM_PROJECTILE_ARRAY_DRAW(sGGSMData.sProjectiles)
	
	GGSM_CAMERA_DRAW_TILE_MAP(sGGSMData.sCamera, sGGSMData.sTileMap)
	
	
	GGSM_DRAW_POWER_UP_ARRAY(sGGSMData.sPowerUp)
	
	IF (sGGSMData.fFakeFadeAlpha > 0.0)
		INIT_RGBA_STRUCT(col, 0, 0, 0, FLOOR(255.0 * sGGSMData.fFakeFadeAlpha))
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), col)	
	ENDIF 
	
	GGSM_DRAW_GAME_HUD()
ENDPROC

//-------------------------------------------------
// MAIN STATES
//-------------------------------------------------

/// PURPOSE:
///    Draw the title screen
PROC GGSM_DRAW_CLIENT_STATE_TITLE_SCREEN()	
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	INT i
	RGBA_COLOUR_STRUCT str
	STRING sFrontEnd = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_FRONTEND)
	GGSM_DRAW_BG()
	
	INIT_RGBA_STRUCT(str, 255, 0, 0, 255)
	IF (sGGSMData.bHardMode)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sFrontEnd, "SPACE_MONKEY_LOGO", INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, 350.0), INIT_VECTOR_2D(1024, 512), 0, str)
	ELSE  
		ARCADE_DRAW_PIXELSPACE_SPRITE(sFrontEnd, "SPACE_MONKEY_LOGO", INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, 350.0), INIT_VECTOR_2D(1024, 512), 0, sGGSMData.rgbaWhite)	
	ENDIF 
	
	// make the text flash
	i = (FLOOR(sGGSMData.fStateTimer * 5.0) % 10)
	IF (i % 2 = 0)
		GGSM_DRAW_PRESS_START_TEXT(cfBASE_SCREEN_HALF_WIDTH, 650.0)
	ENDIF
ENDPROC	

PROC GGSM_DRAW_CLIENT_STATE_FIB_WARNING()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	RGBA_COLOUR_STRUCT bg
	STRING textDict = GGSM_GET_SPRITE_DICT_NAME(GGSM_SPRITE_FBI_WARN)
	STRING sprName = GGSM_GET_SPRITE_NAME(GGSM_SPRITE_FBI_WARN)
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_FBI_WARN)
	
	INIT_RGBA_STRUCT(bg, 115, 140, 156, 255)	
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), bg)
	ARCADE_DRAW_PIXELSPACE_SPRITE(textDict, sprName, INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), vSize, 0, sGGSMData.rgbaWhite)
ENDPROC

PROC GGSM_DRAW_CLIENT_STATE_PSA_SCREEN()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	RGBA_COLOUR_STRUCT bg
	STRING textDict = GGSM_GET_SPRITE_DICT_NAME(GGSM_SPRITE_PSA)
	STRING sprName = GGSM_GET_SPRITE_NAME(GGSM_SPRITE_PSA)
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_PSA)
	
	INIT_RGBA_STRUCT(bg, 123, 181, 247, 255)	
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), bg)
	ARCADE_DRAW_PIXELSPACE_SPRITE(textDict, sprName, INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), vSize, 0, sGGSMData.rgbaWhite)
ENDPROC

FUNC FLOAT GGSM_CALCULATE_T_FOR_STORY_POS(FLOAT fYPos)
	RETURN (fYPos - sGGSMData.fStoryFadeOutYPos) / (GGSM_STORY_MAX_Y_POS - sGGSMData.fStoryFadeOutYPos)
ENDFUNC 

PROC GGSM_DRAW_CLIENT_STATE_STORY()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	FLOAT t
	INT iSprite 
	VECTOR_2D vPos = sGGSMData.vStoryBasePos
	GGSM_SPRITE_TYPE sSprite = GGSM_SPRITE_OPENING_TEXT_1
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_BG_MAP)
	ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_BG), GGSM_GET_SPRITE_NAME(GGSM_SPRITE_BG_MAP), INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), vSize, 0, sGGSMData.rgbaWhite)
	RGBA_COLOUR_STRUCT col 
	
	WHILE (sSprite <= GGSM_SPRITE_OPENING_TEXT_9)
		vSize = GGSM_GET_SPRITE_SIZE(sSprite)
		vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_OPENING_TEXT_1)
		//
	
		col = sGGSMData.rgbaWhite
		t = GGSM_CALCULATE_T_FOR_STORY_POS(vPos.y)
		vSize.x = LERP_FLOAT(sGGSMData.fStoryMinWidth, sGGSMData.fStoryMaxWidth, t)
		
		IF (t < 0.0)
			col.iA = FLOOR((5.0 - ABSF(t * 5.0)) * 255.0)
			col.iA = CLAMP_INT(col.iA, 0, 255)
		ENDIF 
		
		vSize = GGSM_GET_SPRITE_SIZE(sSprite)
		ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_SPRITE_DICT_NAME(sSprite), GGSM_GET_SPRITE_NAME(sSprite), vPos, vSize, 0, col)
		
		vPos.y += (vSize.y * 2.0)
		
		IF (sSprite = GGSM_SPRITE_OPENING_TEXT_4) OR (sSprite = GGSM_SPRITE_OPENING_TEXT_6) OR (sSprite = GGSM_SPRITE_OPENING_TEXT_8)
			vPos.y += (vSize.y * 3.0)
		ENDIF 
		
		iSprite = ENUM_TO_INT(sSprite) + 1
		sSprite = INT_TO_ENUM(GGSM_SPRITE_TYPE, iSprite)
	ENDWHILE 
	
	//GGSM_DRAW_SLICED_SPRITE(GGSM_SPRITE_OPENING_TEXT_9, INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT))
ENDPROC 

PROC GGSM_DRAW_CLIENT_STATE_STAGE_MISSION_SELECT()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	VECTOR_2D vSpritePos
	VECTOR_2D vSpriteSize
	VECTOR_2D vTextPos
	RGBA_COLOUR_STRUCT gray 
	RGBA_COLOUR_STRUCT lightgray
	FLOAT rot 
	INT i
	
	STRING str = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_BG)
	vSpriteSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_BG_MAP)
	ARCADE_DRAW_PIXELSPACE_SPRITE(str, GGSM_GET_SPRITE_NAME(GGSM_SPRITE_BG_MAP), INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), vSpriteSize, 0, sGGSMData.rgbaWhite)
	
	INIT_RGBA_STRUCT(gray, 32, 32, 32, 255)
	INIT_RGBA_STRUCT(lightgray, 130, 130, 130, 255)
	//GGSM_DRAW_SECTOR_MAP_TITLE_TEXT()
	
	// make the text flash
	INT iFlash = (FLOOR(sGGSMData.fStateTimer * 5.0) % 10)
	IF (iFlash % 2 = 0)
		GGSM_DRAW_PRESS_START_TEXT(cfBASE_SCREEN_HALF_WIDTH, 900.0, 0.75)
	ENDIF
	
	str = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_FRONTEND)
	REPEAT 6 i
		vSpritePos = GGSM_GET_SPRITE_POSITION_FOR_STAGE_MAP(i)
		vSpriteSize = GGSM_GET_SPRITE_SIZE_FOR_STAGE_MAP(i)
		vTextPos = GGSM_GET_TEXT_POSITION_FOR_STAGE_MAP(i)
		
		GGSM_DRAW_SECTOR_NAME_TEXT(i, vTextPos.x, vTextPos.y)	
		
		IF (i = sGGSMData.sGameStats.level)	
			ARCADE_DRAW_PIXELSPACE_SPRITE(str, GGSM_GET_SPRITE_NAME_FOR_STAGE_MAP(i), vSpritePos, vSpriteSize, 0, sGGSMData.rgbaWhite)
		ELIF (i < sGGSMData.sGameStats.level)		
			ARCADE_DRAW_PIXELSPACE_SPRITE(str, GGSM_GET_SPRITE_NAME_FOR_STAGE_MAP(i), vSpritePos, vSpriteSize, 0, lightgray)
		ELSE 
			ARCADE_DRAW_PIXELSPACE_SPRITE(str, GGSM_GET_SPRITE_NAME_FOR_STAGE_MAP(i), vSpritePos, vSpriteSize, 0, gray)
		ENDIF 
	ENDREPEAT 
	
	vSpriteSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_LEVEL_SCREEN_DASH)
	REPEAT 6 i
		IF (i <= sGGSMData.sGameStats.level)
			vSpritePos = GGSM_GET_SPRITE_DASH_POSITION_FOR_STAGE_MAP(i)
			rot = GGSM_GET_SPRITE_DASH_ROTATION_FOR_STAGE_MAP(i)
			
			IF (i = sGGSMData.sGameStats.level) 
				ARCADE_DRAW_PIXELSPACE_SPRITE(str, "Dashes", vSpritePos, vSpriteSize, rot, sGGSMData.rgbaWhite)
			ELSE 
				ARCADE_DRAW_PIXELSPACE_SPRITE(str, "Dashes", vSpritePos, vSpriteSize, rot, lightgray)			
			ENDIF 			
		ENDIF
	ENDREPEAT
	
	// Draw Ship
	VECTOR_2D v1, v2 
	vSpriteSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_LEVEL_SCREEN_DASH)
	vSpritePos = GGSM_GET_SPRITE_DASH_POSITION_FOR_STAGE_MAP(sGGSMData.sGameStats.level)
	IF GGSM_IS_SPRITE_DASH_HORIZONTAL_FOR_STAGE_MAP(sGGSMData.sGameStats.level)
		v1 = INIT_VECTOR_2D(vSpritePos.x - (vSpriteSize.y / 2.0), vSpritePos.y)
		v2 = INIT_VECTOR_2D(vSpritePos.x + (vSpriteSize.y / 2.0), vSpritePos.y)
	ELSE 
		v1 = INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y - (vSpriteSize.y / 2.0))
		v2 = INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y + (vSpriteSize.y / 2.0))	
	ENDIF 
	
	IF (sGGSMData.sGameStats.level != 3)
		vSpritePos = LERP_VECTOR_2D(v1, v2, sGGSMData.fStateInterpolateValue)
	ELSE 
		vSpritePos = LERP_VECTOR_2D(v2, v1, sGGSMData.fStateInterpolateValue)
	ENDIF
	
	vSpriteSize = MULTIPLY_VECTOR_2D(GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_MONKEY), 0.5)
	str = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_GAME)
	ARCADE_DRAW_PIXELSPACE_SPRITE(str, GGSM_GET_SPRITE_NAME(GGSM_SPRITE_MONKEY), vSpritePos, vSpriteSize, 0.5, sGGSMData.rgbaWhite)
ENDPROC

PROC GGSM_DRAW_GAME_STATE_STAGE_CLEAR()	
	FLOAT fy = 420.0
	GGSM_DRAW_MAIN_GAME()
		
	IF (sGGSMData.eSubState >= GGSM_SUBSTATE_CLR_STAGE_CLEAR) AND (sGGSMData.eSubState <= GGSM_SUBSTATE_CLR_FLY_OFF_DONE)
		GGSM_DRAW_SECTOR_CLEAR_TEXT(cfBASE_SCREEN_HALF_WIDTH)
		GGSM_DRAW_SECTOR_BONUS_TEXT(sGGSMData.iSectorBonus, DEFAULT, fy) 
		
		fy += 32
		GGSM_DRAW_TAKEDOWN_BONUS_TEXT(sGGSMData.iTakedownBonus, DEFAULT, fy) 
		
		fy += 32
		GGSM_DRAW_NO_DAMAGE_BONUS_TEXT(sGGSMData.iNoDamageBonus, DEFAULT, fy)
		
		fy += 32
		GGSM_DRAW_NO_MISS_BONUS_TEXT(sGGSMData.iNoMissBonus, DEFAULT, fy)
		
		IF (sGGSMData.sGameStats.level >= GGSM_SECTOR_MAX)
			fy += 32
			GGSM_DRAW_COMPLETE_BONUS_TEXT(sGGSMData.iAllClearBonus, DEFAULT, fy)		
		ENDIF 
	ENDIF
	
	IF GGSM_DOES_PLAYER_SHIP_EXIST()
		GGSM_ENTITY_DRAW(sGGSMData.sEntities[sGGSMData.iPlayerShipIndex])
	ENDIF 
	
ENDPROC

PROC GGSM_DRAW_CLIENT_STATE_STAGE_CONGRATS()
	#IF IS_DEBUG_BUILD
		IF (g_bBlockArcadeCabinetDrawing)
			EXIT 
		ENDIF
	#ENDIF
	
	GGSM_DRAW_BG()
	VECTOR_2D vDrawPos = INIT_VECTOR_2D(960.0, 450.0)
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_MONKEY_CONGRATS)
	ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_FRONTEND), GGSM_GET_SPRITE_NAME(GGSM_SPRITE_MONKEY_CONGRATS), vDrawPos, vSize, 0, sGGSMData.rgbaWhite)
	
	GGSM_DRAW_CONGRATULATIONS_TEXT(cfBASE_SCREEN_HALF_WIDTH, 140)
	GGSM_DRAW_THANK_YOU_TEXT(cfBASE_SCREEN_HALF_WIDTH, 720)
ENDPROC

//-------------------------------------------------
// DEBUG STUFF
//-------------------------------------------------
#IF IS_DEBUG_BUILD

PROC GGSM_DRAW_POINT_ON_DEBUG_GRID(INT iX, INT iY, RGBA_COLOUR_STRUCT col, INT iSize = 4, INT iXSep = GGSM_PATH_GRID_SEPERATION, INT iYSep = GGSM_PATH_GRID_SEPERATION)
	VECTOR_2D v, s
	
	v.x = TO_FLOAT(ix) * TO_FLOAT(iXSep)
	v.y = TO_FLOAT(iy) * TO_FLOAT(iYSep)
	s.x = TO_FLOAT(iSize)
	s.y = s.x 
	
	ARCADE_DRAW_PIXELSPACE_RECT(v, s, col)
ENDPROC 

PROC GGSM_DRAW_DEBUG_GRID(RGBA_COLOUR_STRUCT col, INT iXSep = GGSM_PATH_GRID_SEPERATION, INT iYSep = GGSM_PATH_GRID_SEPERATION)
	INT gx, limit 
	VECTOR_2D v, v1, v2
	
	gx = 0
	limit = FLOOR(cfBASE_SCREEN_WIDTH)
	WHILE (gx < limit)
		v = INIT_VECTOR_2D(TO_FLOAT(gx), 0)
		v1 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v)
		
		v.y = cfBASE_SCREEN_HEIGHT
		v2 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v)
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(v1, v2, col)
		gx += iXSep
	ENDWHILE
	
	gx = 0
	limit = FLOOR(cfBASE_SCREEN_HEIGHT)
	WHILE (gx < limit)
		v = INIT_VECTOR_2D(0, TO_FLOAT(gx))
		v1 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v)
		
		v.x = cfBASE_SCREEN_WIDTH
		v2 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v)
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(v1, v2, col)
		gx += iYSep
	ENDWHILE
ENDPROC 

PROC GGSM_DRAW_DEBUG_TEXT(STRING str, FLOAT px, FLOAT py, BOOL bWhite = TRUE)
	VECTOR_2D pos = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(INIT_VECTOR_2D(px, py))
	
	SET_TEXT_SCALE(0.5, 0.5)
	
	IF (bWhite)
		SET_TEXT_COLOUR(0, 255, 255, 255)	
	ELSE 
		SET_TEXT_COLOUR(192, 192, 192, 255)		
	ENDIF 
	
	SET_TEXT_DROP_SHADOW()
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	
	SET_TEXT_FONT(FONT_STYLE_PRICEDOWN)
	DISPLAY_TEXT_WITH_LITERAL_STRING(pos.x, pos.y, "STRING", str)
ENDPROC

PROC GGSM_DRAW_POINT_ON_DEBUG_MINIMAP(VECTOR_2D vPoint, VECTOR_2D vSize, RGBA_COLOUR_STRUCT col)
	VECTOR_2D pos, size
	VECTOR_2D scaleFactor = INIT_VECTOR_2D(GGSM_PX_HUD_MINIMAP_WIDTH / cfBASE_SCREEN_WIDTH, GGSM_PX_HUD_MINIMAP_HEIGHT / cfBASE_SCREEN_HEIGHT)
	
	pos.x = (vPoint.x * scaleFactor.x) + GGSM_PX_HUD_MINIMAP_X
	pos.y = (vPoint.y * scaleFactor.y) + GGSM_PX_HUD_MINIMAP_Y
	size.x = (vSize.x * scaleFactor.x)
	size.y = (vSize.y * scaleFactor.y)
	
	ARCADE_DRAW_PIXELSPACE_RECT(pos, size, col)
ENDPROC 

PROC GGSM_DRAW_FORMATION_EXPLICT(GGSM_FORMATION_DATA &data, VECTOR_2D vOrigin, VECTOR_2D vDirection, RGBA_COLOUR_STRUCT col)
	VECTOR_2D v1, v2, v
	
	v = MULTIPLY_VECTOR_2D(vDirection, 2000.0)
	v.x += vOrigin.x
	
	v.y += vOrigin.y 
	v1 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vOrigin)
	v2 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v) 	
	DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(v1, v2, col)
	
	UNUSED_PARAMETER(data)
ENDPROC 

PROC GGSM_DRAW_FORMATION(GGSM_FORMATION_DATA &data, RGBA_COLOUR_STRUCT col)
	VECTOR_2D vOrigin = GGSM_FORMATION_GET_START_POSITION(data)
	VECTOR_2D vDirection = GGSM_FORMATION_GET_START_DIRECTION(data)
	GGSM_DRAW_FORMATION_EXPLICT(data, vOrigin, vDirection, col)
ENDPROC 

PROC GGSM_DRAW_PATH(GGSM_PATH_DATA &data, RGBA_COLOUR_STRUCT col)
	INT i, iNumPoints 
	VECTOR_2D v, v1, v2
	
	// draw points 
	iNumPoints = GGSM_PATH_DATA_GET_NUM_POINTS(data)
	REPEAT iNumPoints i
		v = GGSM_PATH_DATA_GET_POINT(data, i)
		GGSM_DRAW_POINT_ON_DEBUG_GRID(FLOOR(v.x) / GGSM_PATH_GRID_SEPERATION, FLOOR(v.y) / GGSM_PATH_GRID_SEPERATION, col, 8)
	ENDREPEAT
	
	REPEAT iNumPoints - 1 i
		v1 = GGSM_PATH_DATA_GET_POINT(data, i)
		v2 = GGSM_PATH_DATA_GET_POINT(data, (i + 1) % (iNumPoints))	
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v1), ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v2), col)
	ENDREPEAT 
ENDPROC

PROC GGSM_DRAW_RELATIVE_PATH(VECTOR_2D vOrigin, GGSM_PATH_DATA &data, RGBA_COLOUR_STRUCT col)
	INT i, iNumPoints 
	VECTOR_2D root 
	VECTOR_2D v, v1, v2
	VECTOR_2D vPoints[GGSM_MAX_PATH_POINTS]
	
	// draw points 
	iNumPoints = GGSM_PATH_DATA_GET_NUM_POINTS(data)
	root = GGSM_PATH_DATA_GET_POINT(data, 0)
	
	REPEAT iNumPoints i
		v = SUBTRACT_VECTOR_2D(GGSM_PATH_DATA_GET_POINT(data, i), root)
		vPoints[i] = ADD_VECTOR_2D(v, vOrigin)
		GGSM_DRAW_POINT_ON_DEBUG_GRID(FLOOR(vPoints[i].x) / GGSM_PATH_GRID_SEPERATION, FLOOR(vPoints[i].y) / GGSM_PATH_GRID_SEPERATION, col, 8)
	ENDREPEAT
	
	REPEAT iNumPoints - 1 i
		v1 = vPoints[i]
		v2 = vPoints[(i + 1) % (iNumPoints)]
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v1), ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(v2), col)
	ENDREPEAT 
ENDPROC

PROC GGSM_DRAW_DEBUG_MINIMAP()
	RGBA_COLOUR_STRUCT rcol
	VECTOR_2D pos = INIT_VECTOR_2D(GGSM_PX_HUD_MINIMAP_X, GGSM_PX_HUD_MINIMAP_Y)
	VECTOR_2D size = INIT_VECTOR_2D(GGSM_PX_HUD_MINIMAP_WIDTH, GGSM_PX_HUD_MINIMAP_HEIGHT)
	VECTOR_2D scaleFactor = INIT_VECTOR_2D(GGSM_PX_HUD_MINIMAP_WIDTH / cfBASE_SCREEN_WIDTH, GGSM_PX_HUD_MINIMAP_HEIGHT / cfBASE_SCREEN_HEIGHT)
	
	// draw bg
	pos = GGSM_GET_CENTER_FROM_CORNER_RECT(pos.x, pos.y, size.x, size.y)
	INIT_RGBA_STRUCT(rcol, 255, 255, 255, 32)
	ARCADE_DRAW_PIXELSPACE_RECT(pos, size, rcol)
	
	// draw safe screen area
	pos.x = (GGSM_PX_OVERSCAN_MIN_X * scaleFactor.x) + GGSM_PX_HUD_MINIMAP_X
	pos.y = (GGSM_PX_OVERSCAN_MIN_Y * scaleFactor.y) + GGSM_PX_HUD_MINIMAP_Y
	size.x = (GGSM_PX_OVERSCAN_WIDTH * scaleFactor.y)
	size.y = (GGSM_PX_OVERSCAN_HEIGHT * scaleFactor.y)
	pos = GGSM_GET_CENTER_FROM_CORNER_RECT(pos.x, pos.y, size.x, size.y)
	INIT_RGBA_STRUCT(rcol, 255, 255, 255, 64)
	ARCADE_DRAW_PIXELSPACE_RECT(pos, size, rcol)	
ENDPROC 

PROC GGSM_DRAW_ENTITY_EDITOR_OPTION(GGSM_ENTITY_EDITOR &editor, INT i)
	FLOAT fx = 160
	FLOAT fy = 100
	TEXT_LABEL_63 str 
	BOOL bSelected = FALSE 
	
	fy += (i * 32.0)
	
	IF (i = editor.sEditCore.iOption)
		bSelected = TRUE 
	ENDIF

	SWITCH (i)
		CASE GGSM_ENTITY_EDITOR_OPTION_DATA_SELECT
			str = "ENTITY: "
			str += GGSM_ENTITY_TYPE_TO_STRING(editor.eEditType)
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_ENTITY_EDITOR_OPTION_POINT_EDIT
			str = "C1:["
			str += FLOOR(editor.sEditData.sCollisionData.vCollisionVectors[0].x)
			str += ", "
			str += FLOOR(editor.sEditData.sCollisionData.vCollisionVectors[0].y)
			str += "] C2:["
			str += FLOOR(editor.sEditData.sCollisionData.vCollisionVectors[1].x)
			str += ", "
			str += FLOOR(editor.sEditData.sCollisionData.vCollisionVectors[1].y)			
			str += "]"
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_ENTITY_EDITOR_OPTION_RADIUS_SELECT
			str = "RADIUS: "
			str += FLOOR(editor.sEditData.sCollisionData.fColRadius)
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK 
		
		CASE GGSM_ENTITY_EDITOR_OPTION_GUNPOS_SELECT
			str = "GUN:["
			str += FLOOR(editor.sEditData.sGunPos.vPosition.x)
			str += ", "
			str += FLOOR(editor.sEditData.sGunPos.vPosition.y)
			str += "]"
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK 
	ENDSWITCH 
ENDPROC 

PROC GGSM_DRAW_ENTITY_EDITOR(GGSM_ENTITY_EDITOR &editor)
	INT i
	FLOAT fx = 170
	FLOAT fy = GGSM_PX_HUD_MAX_Y - 64
	RGBA_COLOUR_STRUCT col 
	
	// draw vert lines grid 
	INIT_RGBA_STRUCT(col, 0, 255, 0, 64)
	GGSM_DRAW_DEBUG_GRID(col)
	
	// draw info	
	DRAW_RECT_FROM_CORNER(0, 0, 1, 0.3, 0, 0, 0, 128)
	REPEAT GGSM_ENTITY_EDITOR_OPTION_MAX i
		GGSM_DRAW_ENTITY_EDITOR_OPTION(editor, i)
	ENDREPEAT
	
	INIT_RGBA_STRUCT(col, 255, 255, 255, 128)
	GGSM_ENTITY_DRAW(editor.sTestEntity)
	GGSM_DRAW_COLLIDER(editor.sTestEntity.sCollider, col)
	
	VECTOR_2D vGunPos = GGSM_ENTITY_GET_GUN_WORLD_POS(editor.sTestEntity)
	ARCADE_DRAW_PIXELSPACE_SPRITE("MPGoGoSpaceResident", "debugcircle", vGunPos, INIT_VECTOR_2D(8, 8), 0, sGGSMData.rgbaWhite)

	fy -= 32
	IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(editor.sEditCore.eFlags, GGSM_EDITOR_BIT_SAVED) AND NOT IS_BITMASK_ENUM_AS_ENUM_SET(editor.sEditCore.eFlags, GGSM_EDITOR_BIT_EXPORT)
		GGSM_DRAW_DEBUG_TEXT("[UNSAVED]", fx, fy)
	ENDIF
ENDPROC

PROC GGSM_DRAW_SPAWN_DEBUG()
	FLOAT fx = 160
	FLOAT fy = GGSM_PX_HUD_MAX_Y - 32
	
	TEXT_LABEL_63 str  = " SP INDEX:"
	str += sGGSMData.iSpawnIndexCounter
	
	str += " SDIST:"
	str += FLOOR(sGGSMData.sCamera.fDistTravelled)
	
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
ENDPROC 

PROC GGSM_DRAW_AWARD_DEBUG()
	FLOAT fy = GGSM_PX_HUD_MAX_Y - (32 * 12)
	FLOAT fx = 160
	GGSM_CHALLENGE_BIT cb
	INT i 
	TEXT_LABEL_63 str
	
	FOR i = 0 TO 7 STEP 1 
		cb = INT_TO_ENUM(GGSM_CHALLENGE_BIT, SHIFT_LEFT(1, i))
		str = GGSM_CHALLENGE_BIT_TO_STRING(cb)
		str += ":"
		str += GET_STRING_FROM_BOOL(IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eChallengeFlags, cb))
		GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
		fy += 32
	ENDFOR 
	
	str = "MP_AWARD_APEESCAPE:" 
	str += GET_STRING_FROM_BOOL(GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_APEESCAPE))
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
	fy += 32
		
	str = "MP_AWARD_MONKEYKIND:" 
	str += GET_STRING_FROM_BOOL(GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MONKEYKIND))
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
	fy += 32
	
	str = "MP_AWARD_ASTROCHIMP:" 
	str += GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP)
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
	fy += 32
ENDPROC 

PROC GGSM_DRAW_SPAWN_EDITOR_OPTION(GGSM_SPAWN_EDITOR &editor, INT iOption)
	FLOAT fx = 160
	FLOAT fy = 100
	TEXT_LABEL_63 str 
	BOOL bSelected = FALSE 
	GGSM_SPRITE_TYPE eSprite
	
	fy += (iOption * 32.0)
	
	IF (iOption = editor.sEditCore.iOption)
		bSelected = TRUE 
	ENDIF
	
	SWITCH (iOption)
		CASE GGSM_SPAWN_EDITOR_OPTION_DATA_SELECT
			str = "FORMATION #:"
			str += editor.sEditCore.iCurrentEditIndex
			
			IF (editor.sEditData.iFormationMembers = 0)
				str += " [EMPTY]"
			ELSE 
				str += " SD:"
				str += FLOOR(editor.sEditData.fScrollDistance)
			ENDIF 
			
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_SPAWN_EDITOR_OPTION_FORMATION_SELECT
			GGSM_DRAW_DEBUG_TEXT(GGSM_FORMATION_TYPE_TO_STRING(INT_TO_ENUM(GGSM_FORMATION_TYPE, editor.iFormationType)), fx, fy, bSelected)
		BREAK
		
		CASE GGSM_SPAWN_EDITOR_OPTION_ENTITY_SELECT
			GGSM_DRAW_DEBUG_TEXT(GGSM_ENTITY_TYPE_TO_STRING(INT_TO_ENUM(GGSM_ENTITY_TYPE, editor.iEntityType)), fx, fy, bSelected)
		BREAK 
		
		CASE GGSM_SPAWN_EDITOR_OPTION_MOVEMENT_SELECT
			GGSM_DRAW_DEBUG_TEXT(GGSM_MOVEMENT_TYPE_TO_STRING(INT_TO_ENUM(GGSM_MOVEMENT_TYPE, editor.iMoveType)), fx, fy, bSelected)
		BREAK 
		
		CASE GGSM_SPAWN_EDITOR_OPTION_WEAPON_SELECT
			GGSM_DRAW_DEBUG_TEXT(GGSM_WEAPON_TYPE_TO_STRING(INT_TO_ENUM(GGSM_WEAPON_TYPE, editor.iWeaponType)), fx, fy, bSelected)
		BREAK 
		
		CASE GGSM_SPAWN_EDITOR_OPTION_UNITCOUNT_SELECT
			str = "# UNITS:"
			str += editor.sEditData.iFormationMembers
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK 
		
		CASE GGSM_SPAWN_EDITOR_OPTION_PATH_SELECT
			IF (editor.iFormationType = ENUM_TO_INT(GGSM_FORMATION_POWER_UP))
				eSprite = INT_TO_ENUM(GGSM_SPRITE_TYPE, ENUM_TO_INT(GGSM_SPRITE_POWER_UP_BEAM) + editor.sEditData.iPathNumber)
				str = "POWER_UP:"
				str += GGSM_GET_SPRITE_NAME(eSprite)
				GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
			ELSE 
				str = "PATH #:"
				str += editor.sEditData.iPathNumber
				GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)			
			ENDIF 
		BREAK 	
		
		CASE GGSM_SPAWN_EDITOR_OPTION_XSPACE_SELECT
			str = "X SPACING:"
			str += FLOAT_TO_STRING(editor.sEditData.vSpacing.x, 2)
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)	
		BREAK 
		
		CASE GGSM_SPAWN_EDITOR_OPTION_YSPACE_SELECT
			str = "Y SPACING:"
			str += FLOAT_TO_STRING(editor.sEditData.vSpacing.y, 2)
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)	
		BREAK 	
		
		CASE GGSM_SPAWN_EDITOR_OPTION_FORMATION_SHIFT
			str = "SHIFT ALL FORMATIONS:"
			str += FLOAT_TO_STRING(editor.fFormShift, 2)
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)	
		BREAK 
	ENDSWITCH 
ENDPROC 

PROC GGSM_DRAW_SPAWN_EDITOR(GGSM_SPAWN_EDITOR &editor)
	INT i
	FLOAT fx = 170
	FLOAT fy = 100
	VECTOR_2D size, pos  
	RGBA_COLOUR_STRUCT col 
	GGSM_ENTITY_DATA entData
	GGSM_ENTITY_TYPE entType  
	GGSM_SPRITE_TYPE spr 
	GGSM_PATH_DATA pathDat 
	
	DRAW_RECT_FROM_CORNER(0, 0, 1, 0.3, 0, 0, 0, 128)
	
	// draw info	
	REPEAT GGSM_SPAWN_EDITOR_OPTION_MAX i
		GGSM_DRAW_SPAWN_EDITOR_OPTION(editor, i)
	ENDREPEAT
	
	GGSM_DRAW_DEBUG_MINIMAP()
	
	// draw selected enemy
	pos.x = 1440 
	pos.y = 190
	size.x = 160
	size.y = 160
	INIT_RGBA_STRUCT(col, 255, 255, 255, 32)
	ARCADE_DRAW_PIXELSPACE_RECT(pos, size, col)
		
	IF (editor.iFormationType != ENUM_TO_INT(GGSM_FORMATION_POWER_UP))
		entType = INT_TO_ENUM(GGSM_ENTITY_TYPE, editor.iEntityType)
		entData = GGSM_ENTITY_DATA_GET(entType)
		spr = entData.eSpriteType
	ELSE 
		spr = INT_TO_ENUM(GGSM_SPRITE_TYPE, ENUM_TO_INT(GGSM_SPRITE_POWER_UP_BEAM) + editor.sEditData.iPathNumber)
	ENDIF 
	
	INIT_RGBA_STRUCT(col, 255, 255, 255, 255)
	ARCADE_DRAW_PIXELSPACE_SPRITE(GGSM_GET_SPRITE_DICT_NAME(spr), GGSM_GET_SPRITE_NAME(spr), pos, size, 0, col)	
	
	// draw potential enemy placement
	IF (sGGSMData.iPlayerShipIndex != GGSM_INVALID_ENTITY)
		INIT_RGBA_STRUCT(col, 0, 0, 255, 255) 
		GGSM_DRAW_POINT_ON_DEBUG_MINIMAP(sGGSMData.sEntities[sGGSMData.iPlayerShipIndex].sWorld.vPosition, GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_MONKEY), col)	
	ENDIF 
	
	// draw potential enemy placement
	INIT_RGBA_STRUCT(col, 0, 255, 0, 255) 
	size = GGSM_GET_SPRITE_SIZE(spr)
	GGSM_DRAW_POINT_ON_DEBUG_MINIMAP(editor.vFormPosition, size, col)	
	
	// draw other units in formation
	INIT_RGBA_STRUCT(col, 0, 255, 0, 64)
	REPEAT editor.sEditData.iFormationMembers i
		pos = GGSM_FORMATION_GET_POSITION_FOR_UNIT(editor.sEditData, i, size)
		GGSM_DRAW_POINT_ON_DEBUG_MINIMAP(pos, size, col)
	ENDREPEAT 
		
	// draw enemy direction
	pos = GGSM_GET_OFFSET_FROM_POSITION(editor.vFormPosition, editor.fFormAngle, INIT_VECTOR_2D(0, 150))
	size.x = 32
	size.y = 32
	INIT_RGBA_STRUCT(col, 0, 255, 0, 192)
	GGSM_DRAW_POINT_ON_DEBUG_MINIMAP(pos, size, col)	
	
	IF (editor.iMoveType = ENUM_TO_INT(GGSM_MOVEMENT_FOLLOW_PATH)) AND (editor.sEditData.iPathNumber != GGSM_INVALID_PATH)
		pathDat = GGSM_PATH_DATA_GET(editor.sEditData.iPathNumber)
		GGSM_DRAW_PATH(pathDat, col)
	ELIF (editor.iMoveType = ENUM_TO_INT(GGSM_MOVEMENT_FOLLOW_REL_PATH)) AND (editor.sEditData.iPathNumber != GGSM_INVALID_PATH)
		pathDat = GGSM_PATH_DATA_GET(editor.sEditData.iPathNumber)
		GGSM_DRAW_RELATIVE_PATH(editor.vFormPosition, pathDat, col)
	ELSE
		GGSM_DRAW_FORMATION_EXPLICT(editor.sEditData, editor.vFormPosition, GGSM_GET_VECTOR_FROM_HEADING(editor.fFormAngle), col)		
	ENDIF 
	
	TEXT_LABEL_63 str = "SD:"
	str += FLOOR(sGGSMData.sCamera.fDistTravelled)
	
	IF ((sGGSMData.iSpawnIndexCounter + 1) < COUNT_OF(sGGSMData.sSpawnData))
		str += " NT:"
		str += FLOOR(sGGSMData.sSpawnData[(sGGSMData.iSpawnIndexCounter + 1)].fScrollDistance)
	ENDIF
	
	IF (editor.sEditCore.iCurrentEditIndex != -1) AND (editor.sEditCore.iCurrentEditIndex < COUNT_OF(sGGSMData.sSpawnData))
		str += " ED:"
		str += FLOOR(sGGSMData.sSpawnData[editor.sEditCore.iCurrentEditIndex].fScrollDistance)
	ENDIF 
	
	IF (sGGSMData.iSpawnsUsed < COUNT_OF(sGGSMData.sSpawnData)) AND (sGGSMData.iSpawnsUsed > 0)
		str += " END:"
		str += FLOOR(sGGSMData.sSpawnData[(sGGSMData.iSpawnsUsed - 1)].fScrollDistance)
	ENDIF
	
	fy = GGSM_PX_HUD_MAX_Y - 32
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
	
	fy -= 32
	str = "FPOS:["
	str += FLOOR(editor.vFormPosition.x)
	str += ", " 
	str += FLOOR(editor.vFormPosition.y)
	str += "] ANG:"
	str += FLOOR(editor.fFormAngle)
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
	
	fy -= 32
	str = "TX:"
	str += sGGSMData.sTileMap.sVisibleTileRect.x
	str += " TY:"
	str += sGGSMData.sTileMap.sVisibleTileRect.y
	str += " CURRENT SPAWN:"
	str += sGGSMData.iSpawnIndexCounter
	
	GGSM_DRAW_DEBUG_TEXT(str, fx, fy)
	
	IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(editor.sEditCore.eFlags, GGSM_EDITOR_BIT_SAVED) AND NOT IS_BITMASK_ENUM_AS_ENUM_SET(editor.sEditCore.eFlags, GGSM_EDITOR_BIT_EXPORT)
		fy -= 32
		GGSM_DRAW_DEBUG_TEXT("[UNSAVED]", fx, fy)
	ENDIF
ENDPROC

PROC GGSM_DRAW_TILE_MAP_EDITOR_OPTION(GGSM_TILEMAP_EDITOR &editor, GGSM_TILEMAP &tileMap, INT i)
	FLOAT fx = 160
	FLOAT fy = 100
	TEXT_LABEL_63 str 
	BOOL bSelected = FALSE 
	INT iTileIndex
	BOOL bHFlip, bVFlip
	
	fy += (i * 32.0)
	
	IF (i = editor.sEditCore.iOption)
		bSelected = TRUE 
	ENDIF

	SWITCH (i)
		CASE GGSM_TILEMAP_EDITOR_OPTION_TILE_SELECT
			str = "TILE #: "
			str += editor.sEditCore.iCurrentEditIndex
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_TILEMAP_EDITOR_OPTION_FLIP_SELECT
			str = "FLIP MODE:"
			
			IF IS_BIT_SET(editor.iFlipBits, 0)
				str += " -X"
			ELSE 
				str += " +X"
			ENDIF 
			
			IF IS_BIT_SET(editor.iFlipBits, 1)
				str += " -Y"
			ELSE 
				str += " +Y"
			ENDIF 			
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_TILEMAP_EDITOR_OPTION_MAX_X_SELECT
			str = "MAP WIDTH: "
			str += tileMap.iMapWidth
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_TILEMAP_EDITOR_OPTION_MAX_Y_SELECT
			str = "MAP HEIGHT: "
			str += tileMap.iMapHeight
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)
		BREAK
		
		CASE GGSM_TILEMAP_EDITOR_OPTION_EDIT_MAP_SELECT
			str = "X:"
			str += editor.iEditMapX
			str += " Y:"
			str += editor.iEditMapY
			
			IF GGSM_TILEMAP_GET_DATA(tileMap, editor.iEditMapX, editor.iEditMapY, iTileIndex, bHFlip, bVFlip)
				str += " TILE:"
				str += iTileIndex
			ENDIF
			GGSM_DRAW_DEBUG_TEXT(str, fx, fy, bSelected)	
		BREAK	
	ENDSWITCH 
ENDPROC 

PROC GGSM_DRAW_TILE_MAP_EDITOR(GGSM_TILEMAP_EDITOR &editor, GGSM_TILEMAP &tileMap)
	FLOAT fx = 160
	FLOAT fy = 100
	INT i
	
	DRAW_RECT_FROM_CORNER(0, 0, 1, 0.2, 0, 0, 0, 128)
	
	// draw square over active tile
	RGBA_COLOUR_STRUCT rcol 
	INIT_RGBA_STRUCT(rcol, 0, 255, 0, 128)
	
	GGSM_CAMERA_DRAW_MAPTILE_OVERRIDE(sGGSMData.sCamera, tileMap, editor.iEditMapX, editor.iEditMapY, editor.sEditCore.iCurrentEditIndex, rcol, IS_BIT_SET(editor.iFlipBits, 0), IS_BIT_SET(editor.iFlipBits, 1))
	GGSM_CAMERA_DRAW_MAPTILE_HIGHLIGHT(sGGSMData.sCamera, tileMap, editor.iEditMapX, editor.iEditMapY, rcol)
	
	// draw info	
	REPEAT GGSM_TILEMAP_EDITOR_OPTION_MAX i
		GGSM_DRAW_TILE_MAP_EDITOR_OPTION(editor, tileMap, i)
	ENDREPEAT
	
	fy = GGSM_PX_HUD_MAX_Y - 32
	IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(editor.sEditCore.eFlags, GGSM_EDITOR_BIT_SAVED)	
		GGSM_DRAW_DEBUG_TEXT("[UNSAVED]", fx, fy)
	ENDIF
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(editor.sEditCore.eFlags, GGSM_EDITOR_BIT_EDIT_MODE)	
		fy -= 32
		GGSM_DRAW_DEBUG_TEXT("[EDIT MODE]", fx, fy)
	ENDIF
	
	VECTOR_2D pos, size 
	pos.x = 1440 
	pos.y = 190
	size.x = 160
	size.y = 160
	INIT_RGBA_STRUCT(rcol, 0, 0, 0, 255)
	ARCADE_DRAW_PIXELSPACE_RECT(pos, size, rcol)
	
	TEXT_LABEL_23 sSprName = tileMap.sTileSetBaseName
	sSprName += editor.sEditCore.iCurrentEditIndex
	
	IF IS_BIT_SET(editor.iFlipBits, 0)
		size.x *= -1.0
	ENDIF 
	
	IF IS_BIT_SET(editor.iFlipBits, 1)
		size.y *= -1.0
	ENDIF 
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(tileMap.sTileDirectory, sSprName, pos, size, 0, sGGSMData.rgbaWhite)
ENDPROC
#ENDIF 

