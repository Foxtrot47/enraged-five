USING "degenatron_games_using.sch"
USING "degenatron_games_drawing.sch"
USING "arcade_games_help_text.sch"
USING "degenatron_games_animation.sch"

PROC DEGENATRON_GAMES_INIT_TELEMETRY()
	sDegenatronGamesData.sTelemetry.challenges = 0
	SWITCH sDegenatronGamesData.eGame
		CASE DEGENATRON_GAMES_DEFENDER
			sDegenatronGamesData.sTelemetry.gameType = HASH("ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH")
		BREAK
		CASE DEGENATRON_GAMES_MONKEY
			sDegenatronGamesData.sTelemetry.gameType = HASH("ARCADE_CABINET_CH_MONKEYS_PARADISE")
		BREAK
		CASE DEGENATRON_GAMES_PENETRATOR
			sDegenatronGamesData.sTelemetry.gameType = HASH("ARCADE_CABINET_CH_PENETRATOR")
		BREAK
	ENDSWITCH
	sDegenatronGamesData.sTelemetry.kills = 0
	sDegenatronGamesData.sTelemetry.level = 0
	sDegenatronGamesData.sTelemetry.matchId = -1
	sDegenatronGamesData.sTelemetry.numPlayers = 1
	sDegenatronGamesData.sTelemetry.powerUps = 0
	sDegenatronGamesData.sTelemetry.reward = 0
	sDegenatronGamesData.sTelemetry.score = 0
	sDegenatronGamesData.sTelemetry.timePlayed = GET_GAME_TIMER()
	sDegenatronGamesData.bTelemetryInit = TRUE
ENDPROC

PROC DEGENATRON_GAMES_SEND_TELEMETRY()
	IF sDegenatronGamesData.bTelemetryInit
		sDegenatronGamesData.sTelemetry.timePlayed = GET_GAME_TIMER() - sDegenatronGamesData.sTelemetry.timePlayed
		sDegenatronGamesData.sTelemetry.challenges = ENUM_TO_INT(sDegenatronGamesData.eChallengeFlags)
		IF PLAYER_ID() != INVALID_PLAYER_INDEX()
			SWITCH GET_SIMPLE_INTERIOR_TYPE_LOCAL_PLAYER_IS_IN()
			CASE SIMPLE_INTERIOR_TYPE_ARCADE
				sDegenatronGamesData.sTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_ARCADE")
			BREAK
			CASE SIMPLE_INTERIOR_TYPE_AUTO_SHOP
				sDegenatronGamesData.sTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_AUTO_SHOP")
			BREAK
			
			#IF FEATURE_FIXER
			CASE SIMPLE_INTERIOR_TYPE_FIXER_HQ
				sDegenatronGamesData.sTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_FIXER_HQ")
			BREAK						
			#ENDIF
			
			DEFAULT
				CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] [DEGENATRON_GAMES_SEND_TELEMETRY] not in valid arcade location, setting sDegenatronGamesData.sTelemetry.location = 0")
				sDegenatronGamesData.sTelemetry.location = 0
			BREAK
		ENDSWITCH
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] [DEGENATRON_GAMES_SEND_TELEMETRY] Invalid Player ID, setting sDegenatronGamesData.sTelemetry.location = 0")
			sDegenatronGamesData.sTelemetry.location = 0
		ENDIF				
		
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SEND_TELEMETRY")
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] challenges ",sDegenatronGamesData.sTelemetry.challenges)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] gameType ",sDegenatronGamesData.sTelemetry.gameType)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] kills ",sDegenatronGamesData.sTelemetry.kills)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] level ",sDegenatronGamesData.sTelemetry.level)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] matchId ",sDegenatronGamesData.sTelemetry.matchId)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] numPlayers ",sDegenatronGamesData.sTelemetry.numPlayers)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] powerUps ",sDegenatronGamesData.sTelemetry.powerUps)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] reward ",sDegenatronGamesData.sTelemetry.reward)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] score ",sDegenatronGamesData.sTelemetry.score)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] timePlayed ",sDegenatronGamesData.sTelemetry.timePlayed)
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] timePlayed ",sDegenatronGamesData.sTelemetry.location)
		
		PLAYSTATS_ARCADE_CABINET(sDegenatronGamesData.sTelemetry)

		sDegenatronGamesData.bTelemetryInit = FALSE
	ENDIF
ENDPROC

FUNC CASINO_ARCADE_GAME DEGENATRON_GAMES_GET_CASINO_ARCADE_GAME()
	SWITCH sDegenatronGamesData.eGame
		CASE DEGENATRON_GAMES_DEFENDER
			RETURN CASINO_ARCADE_GAME_DEGENATRON_DEFENDER
		CASE DEGENATRON_GAMES_MONKEY
			RETURN CASINO_ARCADE_GAME_DEGENATRON_MONKEY
		CASE DEGENATRON_GAMES_PENETRATOR
			RETURN CASINO_ARCADE_GAME_DEGENATRON_PENETRATOR
	ENDSWITCH
	RETURN CASINO_ARCADE_GAME_DEGENATRON_DEFENDER
ENDFUNC

/// PURPOSE:
///    Updates the values used for animating sprite sequences
PROC DEGENATRON_GAMES_UPDATE_ANIM_FRAMES()
	
	//Slow anim update
	sDegenatronGamesData.iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
	sDegenatronGamesData.iSlowUpdateFrames = FLOOR(sDegenatronGamesData.iSlowFrameTimeCounter / cfDEGENATRON_GAMES_SLOW_ANIM_FRAME_TIME)
	sDegenatronGamesData.iSlowFrameTimeCounter -= ROUND(cfDEGENATRON_GAMES_SLOW_ANIM_FRAME_TIME * sDegenatronGamesData.iSlowUpdateFrames)
	
	//Default anim update
	sDegenatronGamesData.iDefaultFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
	sDegenatronGamesData.iDefaultUpdateFrames = FLOOR(sDegenatronGamesData.iDefaultFrameTimeCounter / cfDEGENATRON_GAMES_DEFAULT_ANIM_FRAME_TIME)
	sDegenatronGamesData.iDefaultFrameTimeCounter -= ROUND(cfDEGENATRON_GAMES_DEFAULT_ANIM_FRAME_TIME * sDegenatronGamesData.iDefaultUpdateFrames)
	
ENDPROC

PROC DEGENATRON_GAMES_MAINTAIN_GLITCH
	sDegenatronGamesData.iMachineBashed = BOOL_TO_INT( IS_ARCADE_CABINET_GLITCHED() ) #IF IS_DEBUG_BUILD + BOOL_TO_INT( sDegenatronGamesData.bDebugGlitchOverride )*2 #ENDIF
	
	IF sDegenatronGamesData.iMachineBashed = 0
		EXIT
	ENDIF
	
	IF sDegenatronGamesData.iMachineBashed = 1
		IF NOT sDegenatronGamesData.bGlitchCrazy
			IF sDegenatronGamesData.iGlitchTime = -HIGHEST_INT
			OR sDegenatronGamesData.iGlitchTime-5000 < sDegenatronGamesData.iGameTimer AND sDegenatronGamesData.iGlitchCounter = 1
			OR sDegenatronGamesData.iGlitchTime-3500 < sDegenatronGamesData.iGameTimer AND sDegenatronGamesData.iGlitchCounter = 2
			OR sDegenatronGamesData.iGlitchTime-1500 < sDegenatronGamesData.iGameTimer AND sDegenatronGamesData.iGlitchCounter = 3
				IF sDegenatronGamesData.iGlitchTime = -HIGHEST_INT
					sDegenatronGamesData.iGlitchTime = sDegenatronGamesData.iGameTimer + GET_RANDOM_INT_IN_RANGE(ciDEGENATRON_GAMES_GLITCH_CALM_TIME_MIN,ciDEGENATRON_GAMES_GLITCH_CALM_TIME_MAX)
				ENDIF
				sDegenatronGamesData.fNoise = GET_RANDOM_FLOAT_IN_RANGE(2.0,8.0)
				sDegenatronGamesData.fYbugA = GET_RANDOM_FLOAT_IN_RANGE(-65.00,65.0)
				sDegenatronGamesData.fYbugB = GET_RANDOM_FLOAT_IN_RANGE(-0.05,0.05)
				sDegenatronGamesData.fYbugC = GET_RANDOM_FLOAT_IN_RANGE(-0.05,0.05)
				sDegenatronGamesData.fAberrationTime = (BOOL_TO_INT(GET_RANDOM_BOOL())*2-1)*GET_RANDOM_FLOAT_IN_RANGE(500.00,1000.00)
				sDegenatronGamesData.fAberrationNoiseMultiplier = (BOOL_TO_INT(GET_RANDOM_BOOL())*2-1)*GET_RANDOM_FLOAT_IN_RANGE(3.0,8.0)
				sDegenatronGamesData.fAberrationNoise = GET_RANDOM_FLOAT_IN_RANGE(2.0,4.0)
				sDegenatronGamesData.fAberrationSeparation = (BOOL_TO_INT(GET_RANDOM_BOOL())*2-1)*GET_RANDOM_FLOAT_IN_RANGE(2.0,10.0)
				sDegenatronGamesData.fAberrationYNegative = TO_FLOAT(GET_RANDOM_INT_IN_RANGE(0,3)-1)
				sDegenatronGamesData.fAberrationXNegative = TO_FLOAT(GET_RANDOM_INT_IN_RANGE(0,3)-1)
				sDegenatronGamesData.iGlitchCounter++
			ENDIF
			IF sDegenatronGamesData.iGlitchTime < sDegenatronGamesData.iGameTimer
				sDegenatronGamesData.iGlitchTime = -HIGHEST_INT
				sDegenatronGamesData.bGlitchCrazy = TRUE
				sDegenatronGamesData.iGlitchCounter = 0
			ENDIF
		ELSE
			IF sDegenatronGamesData.iGlitchTime = -HIGHEST_INT
				sDegenatronGamesData.iGlitchTime = sDegenatronGamesData.iGameTimer + GET_RANDOM_INT_IN_RANGE(ciDEGENATRON_GAMES_GLITCH_CRAZY_TIME_MIN,ciDEGENATRON_GAMES_GLITCH_CRAZY_TIME_MAX)
				sDegenatronGamesData.fNoise = GET_RANDOM_FLOAT_IN_RANGE(3.0,5.0)
				sDegenatronGamesData.fYbugA = GET_RANDOM_FLOAT_IN_RANGE(-5.0,5.0)
				sDegenatronGamesData.fYbugB = GET_RANDOM_FLOAT_IN_RANGE(0.05,0.2)
				sDegenatronGamesData.fYbugC = GET_RANDOM_FLOAT_IN_RANGE(0.5,1.2)

				sDegenatronGamesData.fAberrationTime = (BOOL_TO_INT(GET_RANDOM_BOOL())*2-1)*GET_RANDOM_FLOAT_IN_RANGE(500.00,1000.00)
				sDegenatronGamesData.fAberrationNoiseMultiplier = (BOOL_TO_INT(GET_RANDOM_BOOL())*2-1)*GET_RANDOM_FLOAT_IN_RANGE(3.0,8.0)
				sDegenatronGamesData.fAberrationSeparation = (BOOL_TO_INT(GET_RANDOM_BOOL())*2-1)*GET_RANDOM_FLOAT_IN_RANGE(2.0,10.0)
				sDegenatronGamesData.fAberrationNoise = GET_RANDOM_FLOAT_IN_RANGE(2.0,4.0)
				sDegenatronGamesData.fAberrationYNegative = TO_FLOAT(GET_RANDOM_INT_IN_RANGE(0,3)-1)
				sDegenatronGamesData.fAberrationXNegative = TO_FLOAT(GET_RANDOM_INT_IN_RANGE(0,3)-1)
			ENDIF
			IF sDegenatronGamesData.iGlitchTime < sDegenatronGamesData.iGameTimer
				sDegenatronGamesData.iGlitchTime = -HIGHEST_INT
				sDegenatronGamesData.bGlitchCrazy = FALSE
			ENDIF
		ENDIF
#IF IS_DEBUG_BUILD
	ELIF sDegenatronGamesData.iMachineBashed > 1
		sDegenatronGamesData.fNoise = sDegenatronGamesData.fDebugNoise
		sDegenatronGamesData.fYbugA = sDegenatronGamesData.fDebugYbugA
		sDegenatronGamesData.fYbugB = sDegenatronGamesData.fDebugYbugB
		sDegenatronGamesData.fYbugC = sDegenatronGamesData.fDebugYbugC

		sDegenatronGamesData.fAberrationTime = sDegenatronGamesData.fDebugAberrationTime
		sDegenatronGamesData.fAberrationNoise = sDegenatronGamesData.fDebugAberrationNoise
		sDegenatronGamesData.fAberrationNoiseMultiplier = sDegenatronGamesData.fDebugAberrationNoiseMultiplier
		sDegenatronGamesData.fAberrationSeparation = sDegenatronGamesData.fDebugAberrationSeparation
		sDegenatronGamesData.fAberrationFlooring = sDegenatronGamesData.fDebugAberrationFlooring
#ENDIF
	ENDIF
	sDegenatronGamesData.yGlitch = YBUG()
	sDegenatronGamesData.bShouldDoHeavyShake = sDegenatronGamesData.bGlitchCrazy
	sDegenatronGamesData.bShouldDoMidShake = NOT sDegenatronGamesData.bGlitchCrazy AND ABSF(sDegenatronGamesData.yGlitch) < 0.04 AND ABSF(sDegenatronGamesData.yGlitch) > 0.01
	sDegenatronGamesData.bShouldDoSoftShake = NOT sDegenatronGamesData.bGlitchCrazy AND ABSF(sDegenatronGamesData.yGlitch) > 0.04
ENDPROC

PROC DEGENATRON_GAMES_SET_CLIENT_STATE(DEGENATRON_GAMES_CLIENT_STATE eNewState)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SET_CLIENT_STATE - State changed from ", 
	DEGENATRON_GAMES_DEBUG_GET_DEGENATRON_GAMES_CLIENT_STATE_AS_STRING(sDegenatronGamesData.eClientState), " to ", DEGENATRON_GAMES_DEBUG_GET_DEGENATRON_GAMES_CLIENT_STATE_AS_STRING(eNewState))
	
	sDegenatronGamesData.eClientState = eNewState
	
ENDPROC

PROC DEGENATRON_GAMES_CLEANUP_ASSETS()
	DEGENATRON_GAMES_CLEANUP_SPRITE_DICT("MPArcadeDegenatron")
	SWITCH sDegenatronGamesData.eGame
		CASE DEGENATRON_GAMES_DEFENDER
			DEGENATRON_GAMES_CLEANUP_SPRITE_DICT("MPArcadeDegenatronFacadeDoF")
		BREAK
		CASE DEGENATRON_GAMES_MONKEY
			DEGENATRON_GAMES_CLEANUP_SPRITE_DICT("MPArcadeDegenatronFacadeMonkey")
		BREAK
		CASE DEGENATRON_GAMES_PENETRATOR
			DEGENATRON_GAMES_CLEANUP_SPRITE_DICT("MPArcadeDegenatronFacadePenetrator")
		BREAK
	ENDSWITCH

	DEGENATRON_GAMES_CLEANUP_SPRITE_DICT("MPArcadeDegenatronCharacters")
	ARCADE_GAMES_POSTFX_CLEANUP_ASSETS()
ENDPROC


PROC DEGENATRON_GAMES_CLEANUP_AND_EXIT_CLIENT()
	
	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] - DEGENATRON_GAMES_CLEANUP_AND_EXIT_CLIENT - Cleaning up and terminating the script")
	
	DEGENATRON_GAMES_SEND_TELEMETRY()
	
	ARCADE_CABINET_COMMON_CLEANUP()
	
	g_bIsPlayerPlayingDegenatron = FALSE
	
	// Release any asset preloaded
	DEGENATRON_GAMES_CLEAN_UP()
	DEGENATRON_GAMES_CLEANUP_ASSETS()
	ARCADE_GAMES_HELP_TEXT_CLEAR()
	ARCADE_GAMES_AUDIO_CLEAN_UP()
	
	sDGAnimationData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
	PLAY_ARCADE_CABINET_ANIMATION(sDGAnimationData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC DEGENATRON_GAMES_CHALLENGE_INIT	
	IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_KEEPFAITH)
		SET_BITMASK_ENUM_AS_ENUM(sDegenatronGamesData.eChallengeFlags, DEGENATRON_GAMES_CHALLENGE_BIT_KEEP_THE_FAITH)
	ENDIF
	IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_AQUAAPE)
		SET_BITMASK_ENUM_AS_ENUM(sDegenatronGamesData.eChallengeFlags, DEGENATRON_GAMES_CHALLENGE_BIT_AQUAAPE)
	ENDIF
	IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_MASTERFUL) >= 40000
		SET_BITMASK_ENUM_AS_ENUM(sDegenatronGamesData.eChallengeFlags, DEGENATRON_GAMES_CHALLENGE_BIT_MASTERFUL_PLATINUM)
	ENDIF
ENDPROC

PROC DEGENATRON_GAMES_SOUND_PRE_GAME()
	FLOAT fMachineDamaged = 0.2
	IF IS_ARCADE_CABINET_GLITCHED()
		fMachineDamaged = 1.0
	ENDIF
	INT iSound
	FOR iSound = ARCADE_GAMES_SOUND_DEGENATRON_BOOT_SCREEN TO ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_BACK
		ARCADE_GAMES_SOUND_SET_VARIABLE(INT_TO_ENUM(ARCADE_GAMES_SOUND,iSound),"MachineDamage",fMachineDamaged)
	ENDFOR
	ARCADE_GAMES_SOUND_SET_VARIABLE(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP_UW,"MachineDamage",fMachineDamaged)
ENDPROC

PROC DEGENATRON_GAMES_PROCESS_CLIENT_STATE_INIT()

	ARCADE_CABINET_COMMON_INITIALIZATION()
	
	g_bIsPlayerPlayingDegenatron = TRUE
	PRINTLN("[DEGENATRON_GAMES] [DEGENATRON_GAMES_PROCESS_CLIENT_STATE_INIT] -  MPGlobalsAmbience.iDegenatronGame ", MPGlobalsAmbience.iDegenatronGame)
	sDegenatronGamesData.eGame = INT_TO_ENUM(DEGENATRON_GAMES, MPGlobalsAmbience.iDegenatronGame)
	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_PROCESS_CLIENT_STATE_PLAYING - sDegenatronGamesData.iMachineBashed ",sDegenatronGamesData.iMachineBashed)

	sDegenatronGamesData.sControlTimer.control = FRONTEND_CONTROL
	sDegenatronGamesData.sControlTimer.action = INPUT_FRONTEND_CANCEL
	sDegenatronGamesData.vScreenSpace = INIT_VECTOR_2D(cfDEGENATRON_GAMES_SCREEN_WIDTH,cfDEGENATRON_GAMES_SCREEN_HEIGHT)
	
	DEGENATRON_GAMES_INIT_COLOUR_STRUCTS()
	ARCADE_GAMES_POSTFX_INIT(DEGENATRON_GAMES_GET_CASINO_ARCADE_GAME())
	ARCADE_GAMES_SOUND_INIT(DEGENATRON_GAMES_GET_CASINO_ARCADE_GAME())
	
	DEGENATRON_GAMES_SET_CLIENT_STATE(DEGENATRON_GAMES_CLIENT_STATE_REQUESTING_ASSETS)

//	ARCADE_GAMES_AUDIO_SCENE_START(ARCADE_GAMES_AUDIO_SCENE_MACHINE_IN_USE)
ENDPROC

FUNC BOOL DEGENATRON_GAMES_REQUESTING_ASSETS()
	BOOL bLoaded = TRUE
	bLoaded = bLoaded AND DEGENATRON_GAMES_PRELOAD_SPRITE_DICT("MPArcadeDegenatron")
	SWITCH sDegenatronGamesData.eGame
		CASE DEGENATRON_GAMES_DEFENDER
			bLoaded = bLoaded AND DEGENATRON_GAMES_PRELOAD_SPRITE_DICT("MPArcadeDegenatronFacadeDoF")
		BREAK
		CASE DEGENATRON_GAMES_MONKEY
			bLoaded = bLoaded AND DEGENATRON_GAMES_PRELOAD_SPRITE_DICT("MPArcadeDegenatronFacadeMonkey")
		BREAK
		CASE DEGENATRON_GAMES_PENETRATOR
			bLoaded = bLoaded AND DEGENATRON_GAMES_PRELOAD_SPRITE_DICT("MPArcadeDegenatronFacadePenetrator")
		BREAK
	ENDSWITCH
//#IF IS_DEBUG_BUILD
//	IF NOT bLoaded
//		sDegenatronGamesData.bDebugDrawFacade = bLoaded
//		bLoaded = TRUE
//	ENDIF
//#ENDIF
	bLoaded = bLoaded AND DEGENATRON_GAMES_PRELOAD_SPRITE_DICT("MPArcadeDegenatronCharacters")
	bLoaded = bLoaded AND ARCADE_GAMES_POSTFX_REQUESTING_ASSETS()
	RETURN bLoaded
ENDFUNC

FUNC BOOL DEGENATRON_GAMES_HAVE_ALL_ASSETS_LOADED()
	IF NOT DEGENATRON_GAMES_REQUESTING_ASSETS()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC DEGENATRON_GAMES_PROCESS_CLIENT_STATE_REQUESTING_ASSETS()
	IF NOT DEGENATRON_GAMES_HAVE_ALL_ASSETS_LOADED()
		PRINTLN("[DEGENATRON_GAMES] [DEGENATRON_GAMES_PROCESS_CLIENT_STATE_REQUESTING_ASSETS] -  Loading assets...")
		EXIT
	ENDIF
	
	//Start the intro movie
//	DG_START_MOVIE(sDegenatronGamesData.bmIDDegenatronIntro, "_1992_DegenatronLogo_720_auto")
	DG_START_MOVIE(sDegenatronGamesData.bmIDDegenatronIntro, "Degenatron_80s")

	// Preload any asset needed
	DEGENATRON_GAMES_SET_CLIENT_STATE(DEGENATRON_GAMES_CLIENT_STATE_INTRO)

ENDPROC

PROC DEGENATRON_GAMES_PROCESS_CLIENT_STATE_INTRO()

	DEGENATRON_GAMES_DRAW_BACKMASK()

	//Wait for the intro movie to finish
	BOOL bAberration = (sDegenatronGamesData.iMachineBashed > 0 AND sDegenatronGamesData.iMachineBashed <= 3)
	BOOL bGreenScale = sDegenatronGamesData.bShowFXGreenScale
	BOOL bMovieFinished
	
	RGBA_COLOUR_STRUCT color = sDegenatronGamesData.rgbaWhite
	RGBA_COLOUR_STRUCT colorCyan = sDegenatronGamesData.rgbaCyan

	IF bGreenScale
		color.iR = 0
		color.iG = ROUND(sDegenatronGamesData.rgbaWhite.iR*0.2999 + sDegenatronGamesData.rgbaWhite.iG*0.587 + sDegenatronGamesData.rgbaWhite.iB*0.114)
		color.iB = 0
		color.iA = sDegenatronGamesData.rgbaWhite.iA

		colorCyan.iR = 0
		colorCyan.iG = ROUND(sDegenatronGamesData.rgbaCyan.iR*0.2999 + sDegenatronGamesData.rgbaCyan.iG*0.587 + sDegenatronGamesData.rgbaCyan.iB*0.114)
		colorCyan.iB = 0
		colorCyan.iA = sDegenatronGamesData.rgbaCyan.iA
	ENDIF

	FLOAT xPos = cfSCREEN_CENTER
	FLOAT yPos = cfSCREEN_CENTER
	IF NOT bAberration
		bMovieFinished = DG_DRAW_MOVIE_WITH_POS(sDegenatronGamesData.bmIDDegenatronIntro,DEGENATRON_GAMES_SCREEN_HEIGHT_RATIO()*1.1,DEGENATRON_GAMES_SCREEN_HEIGHT_RATIO()*1.1,xPos,yPos,color)
	ELSE
		yPos = MOD(cfSCREEN_CENTER+sDegenatronGamesData.yGlitch, 1.0)

		FLOAT fRatio
		fRatio = (sDegenatronGamesData.iGameTimer % sDegenatronGamesData.fAberrationTime) / sDegenatronGamesData.fAberrationTime
		fRatio = RAND(fRatio,sDegenatronGamesData.fAberrationNoise)*sDegenatronGamesData.fAberrationNoiseMultiplier
		fRatio = FLOOR(fRatio*sDegenatronGamesData.fAberrationFlooring)/sDegenatronGamesData.fAberrationFlooring
		fRatio *= sDegenatronGamesData.fAberrationXNegative 
		FLOAT fRatioY
		fRatioY = (sDegenatronGamesData.iGameTimer % sDegenatronGamesData.fAberrationTime) / sDegenatronGamesData.fAberrationTime
		fRatioY = RAND(fRatioY,sDegenatronGamesData.fAberrationNoise)*sDegenatronGamesData.fAberrationNoiseMultiplier
		fRatioY = FLOOR(fRatioY*sDegenatronGamesData.fAberrationFlooring)/sDegenatronGamesData.fAberrationFlooring
		fRatioY *= sDegenatronGamesData.fAberrationYNegative 
		RGBA_COLOUR_STRUCT r = color
		r.iA = sDegenatronGamesData.rgbaWhite.iA/4*3
		RGBA_COLOUR_STRUCT b = colorCyan
		b.iA = colorCyan.iA/2
		
		xPos = cfSCREEN_CENTER - sDegenatronGamesData.fAberrationSeparation/cfBASE_SCREEN_HEIGHT*fRatio
		bMovieFinished = DG_DRAW_MOVIE_WITH_POS(sDegenatronGamesData.bmIDDegenatronIntro,DEGENATRON_GAMES_SCREEN_HEIGHT_RATIO()*1.1,DEGENATRON_GAMES_SCREEN_HEIGHT_RATIO()*1.1,xPos,yPos,b)
		xPos = cfSCREEN_CENTER + sDegenatronGamesData.fAberrationSeparation/cfBASE_SCREEN_HEIGHT*fRatio
		yPos = yPos + sDegenatronGamesData.fAberrationSeparation/cfBASE_SCREEN_HEIGHT*fRatioY
		bMovieFinished = DG_DRAW_MOVIE_WITH_POS(sDegenatronGamesData.bmIDDegenatronIntro,DEGENATRON_GAMES_SCREEN_HEIGHT_RATIO()*1.1,DEGENATRON_GAMES_SCREEN_HEIGHT_RATIO()*1.1,xPos,yPos,r)
	ENDIF
	
	ARCADE_GAMES_POSTFX_DRAW()
	DEGENATRON_GAMES_DRAW_FACADE()
	IF NOT bMovieFinished
		EXIT
	ENDIF
	DEGENATRON_GAMES_SET_CLIENT_STATE(DEGENATRON_GAMES_CLIENT_STATE_PLAYING)

ENDPROC

PROC DEGENATRON_GAMES_UPDATE_GAME_STATE()
	SWITCH sDegenatronGamesData.sGameData.eGameState
		CASE DEGENATRON_GAMES_STATE_GAME_INIT
			sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_MENU
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_MENU
			IF sDegenatronGamesData.bButtonPressed
				IF sDegenatronGamesData.iMenuSelected = 0
					sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_INTRO
				ELSE
					IF IS_ACCOUNT_OVER_17_FOR_UGC()
						sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_LEADERBOARD
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_INTRO
			IF sDegenatronGamesData.bButtonPressed
				sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_UPDATE
			ENDIF
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_UPDATE
			SWITCH sDegenatronGamesData.eGame
				CASE DEGENATRON_GAMES_DEFENDER
					IF sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_END
						sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_SCORE
					ENDIF
				BREAK
				CASE DEGENATRON_GAMES_MONKEY
					IF sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_END
						sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_SCORE
					ENDIF
				BREAK
				CASE DEGENATRON_GAMES_PENETRATOR
					IF sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_END
						sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_SCORE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_SCORE
			IF sDegenatronGamesData.bButtonPressed
				IF IS_ACCOUNT_OVER_17_FOR_UGC()
					sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_LEADERBOARD
				ELSE
					sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_END
				ENDIF
			ENDIF
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_LEADERBOARD
			IF sDegenatronGamesData.bBackButtonPressed AND NOT sAGLeaderboardData.bEditing
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_BACK)
				sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_END
			ENDIF
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_END
			sDegenatronGamesData.sGameData.eGameState = DEGENATRON_GAMES_STATE_GAME_INIT
		BREAK
	ENDSWITCH
ENDPROC
  
PROC DEGENATRON_GAMES_GAME_INIT()
	sDegenatronGamesData.sGameData.iLevel = 1
	sDegenatronGamesData.sGameData.iLifes = 3
	sDegenatronGamesData.sGameData.iScore = 0
	
ENDPROC

PROC DEGENATRON_GAMES_UPDATE_CONTROL_FEEDBACK()
	IF sDegenatronGamesData.bShouldDoHeavyShake
		SET_CONTROL_SHAKE(FRONTEND_CONTROL, 500, sDegenatronGamesData.iFreqHeavyShake)
		sDegenatronGamesData.iFeedBackTime =  NATIVE_TO_INT(GET_NETWORK_TIME()) + 1000
		sDegenatronGamesData.rgbaFeedback = sDegenatronGamesData.rgbaRed
	ELIF sDegenatronGamesData.bShouldDoMidShake
		SET_CONTROL_SHAKE(FRONTEND_CONTROL, 250, sDegenatronGamesData.iFreqMidShake)
		sDegenatronGamesData.iFeedBackTime =  NATIVE_TO_INT(GET_NETWORK_TIME()) + 1000
		sDegenatronGamesData.rgbaFeedback = sDegenatronGamesData.rgbaGreen
	ELIF sDegenatronGamesData.bShouldDoSoftShake
		SET_CONTROL_SHAKE(FRONTEND_CONTROL, 1, sDegenatronGamesData.iFreqSoftShake)
	ENDIF
	sDegenatronGamesData.bShouldDoHeavyShake = FALSE
	sDegenatronGamesData.bShouldDoMidShake = FALSE

	IF sDegenatronGamesData.iFeedBackTime > NATIVE_TO_INT(GET_NETWORK_TIME())
		BOOL bOn = INT_TO_BOOL(((sDegenatronGamesData.iFeedBackTime - NATIVE_TO_INT(GET_NETWORK_TIME()))/250) % 2)
		IF bOn
			SET_CONTROL_LIGHT_EFFECT_COLOR(FRONTEND_CONTROL,sDegenatronGamesData.rgbaFeedback.iR,sDegenatronGamesData.rgbaFeedback.iG,sDegenatronGamesData.rgbaFeedback.iB)
		ELSE
			CLEAR_CONTROL_LIGHT_EFFECT(FRONTEND_CONTROL)
		ENDIF
	ENDIF
ENDPROC

PROC DEGENATRON_GAMES_CLIENT_STATE_PLAYING_UPDATE_AUDIO
	SWITCH sDegenatronGamesData.sGameData.eGameState
		CASE DEGENATRON_GAMES_STATE_GAME_INIT
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_MENU
			ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_LOOP)
			IF sDegenatronGamesData.sControlsInput.iLeftAxisYTick != 0
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_NAV)
			ENDIF
			IF sDegenatronGamesData.bButtonPressed
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_SELECT)
			ENDIF
			
			ARCADE_GAMES_AUDIO_SCENE_START(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_MENU)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_INTRO
			ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_LOOP)
			ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
			IF sDegenatronGamesData.bButtonPressed
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_SELECT)
			ENDIF
			
			ARCADE_GAMES_AUDIO_SCENE_START(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_MENU)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_UPDATE
		
			ARCADE_GAMES_AUDIO_SCENE_STOP(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_MENU)
			ARCADE_GAMES_AUDIO_SCENE_START(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_GAMEPLAY)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_SCORE
			ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
			ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_LOOP)
			IF sDegenatronGamesData.bButtonPressed
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_SELECT)
			ENDIF
			
			ARCADE_GAMES_AUDIO_SCENE_STOP(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_GAMEPLAY)
			ARCADE_GAMES_AUDIO_SCENE_START(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_MENU)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_LEADERBOARD
			IF sDegenatronGamesData.bButtonPressed AND NOT sAGLeaderboardData.bEditing
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MENU_BACK)
			ENDIF
			
			ARCADE_GAMES_AUDIO_SCENE_START(ARCADE_GAMES_AUDIO_SCENE_DEGENATRON_IN_MENU)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_END
		BREAK
	ENDSWITCH
ENDPROC

PROC DEGENATRON_GAMES_CLIENT_STATE_PLAYING_UPDATE_GAME
	SWITCH sDegenatronGamesData.sGameData.eGameState
		CASE DEGENATRON_GAMES_STATE_GAME_INIT
			DEGENATRON_GAMES_GAME_INIT()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_IDLE)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_MENU
			DEGENATRON_GAMES_MENU()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_IDLE)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_INTRO
			DEGENATRON_GAMES_INTRO()
			DEGENATRON_GAMES_INIT_TELEMETRY()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_IDLE)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_UPDATE
				SWITCH sDegenatronGamesData.eGame
					CASE DEGENATRON_GAMES_DEFENDER
						DG_DEFENDER_CLIENT_STATE_UPDATE()
					BREAK
					CASE DEGENATRON_GAMES_MONKEY
						DG_MONKEY_CLIENT_STATE_UPDATE()
					BREAK
					CASE DEGENATRON_GAMES_PENETRATOR
						DG_PENETRATOR_CLIENT_STATE_UPDATE()
					BREAK
				ENDSWITCH
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_SCORE
			DEGENATRON_GAMES_SCORE()
			DEGENATRON_GAMES_SEND_TELEMETRY()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_IDLE)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_LEADERBOARD
			DEGENATRON_GAMES_LEADERBOARD()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_IDLE)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_END
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_IDLE)
		BREAK
	ENDSWITCH
	DEGENATRON_GAMES_UPDATE_GAME_STATE()
	DEGENATRON_GAMES_UPDATE_CONTROL_FEEDBACK()
ENDPROC

PROC DEGENATRON_GAMES_CLIENT_STATE_PLAYING_MAINTAIN_HELP_TEXT
	
	SWITCH sDegenatronGamesData.sGameData.eGameState
		CASE DEGENATRON_GAMES_STATE_GAME_INIT
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_MENU
			ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_GAME_MENU)
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_INTRO
			ARCADE_GAMES_HELP_TEXT_CLEAR()
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_UPDATE
			SWITCH sDegenatronGamesData.eGame
				CASE DEGENATRON_GAMES_DEFENDER
					IF NOT ARCADE_GAMES_HELP_TEXT_HAS_BEEN_DISPLAYED(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_DEFENDER_CONTROLS)
						ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_DEFENDER_CONTROLS)
					ELSE
						IF NOT ARCADE_GAMES_HELP_TEXT_HAS_BEEN_DISPLAYED(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_DEFENDER_HELP)
							ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_DEFENDER_HELP)
						ELSE
							IF sDegenatronGamesData.sGameData.iObjectives < sDGDefenderData.sLevelData.iObjectivesAmount
								ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_DEFENDER_HELP_2)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE DEGENATRON_GAMES_MONKEY
					ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_MONKEY_CONTROLS)
					IF sDGMonkeyData.sLevelData.iCurrentTree = 20
						IF sDGMonkeyData.sLevelData.iTimeTree = -HIGHEST_INT
							sDGMonkeyData.sLevelData.iTimeTree = NATIVE_TO_INT( GET_NETWORK_TIME() ) + 10000
						ELIF sDGMonkeyData.sLevelData.iTimeTree > NATIVE_TO_INT( GET_NETWORK_TIME() )
							ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_MONKEY_HELP)
						ENDIF
					ELSE
						sDGMonkeyData.sLevelData.iTimeTree = -HIGHEST_INT
					ENDIF
				BREAK
				CASE DEGENATRON_GAMES_PENETRATOR
					ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_DEGENATRON_PENETRATOR_CONTROLS)
				BREAK
			ENDSWITCH
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_SCORE
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_LEADERBOARD
		BREAK
		CASE DEGENATRON_GAMES_STATE_GAME_END
		BREAK
	ENDSWITCH
ENDPROC

PROC DEGENATRON_GAMES_PROCESS_CLIENT_STATE_PLAYING()
	
	sDegenatronGamesData.bButtonPressed = FALSE
	sDegenatronGamesData.bBackButtonPressed = FALSE
	DEGENATRON_GAMES_LEFT_AXIS_INPUT()
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		sDegenatronGamesData.bButtonPressed = TRUE
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
		sDegenatronGamesData.bBackButtonPressed = TRUE
	ENDIF
		
	DEGENATRON_GAMES_CLIENT_STATE_PLAYING_UPDATE_GAME()
	DEGENATRON_GAMES_CLIENT_STATE_PLAYING_DRAW()
	DEGENATRON_GAMES_CLIENT_STATE_PLAYING_UPDATE_AUDIO()
	DEGENATRON_GAMES_CLIENT_STATE_PLAYING_MAINTAIN_HELP_TEXT()
	DEGENATRON_GAMES_ANIMATION_UPDATE()
	#IF IS_DEBUG_BUILD
	IF sDegenatronGamesData.bShowHelp
		__DEGENATRON_GAMES_PRINT_DEBUG_GAME_INFO()
	ENDIF
	#ENDIF
ENDPROC

PROC DEGENATRON_GAMES_PROCESS_CLIENT_STATE_MACHINE()

	SWITCH sDegenatronGamesData.eClientState
		
		CASE DEGENATRON_GAMES_CLIENT_STATE_INIT
		
			DEGENATRON_GAMES_PROCESS_CLIENT_STATE_INIT()
		
		BREAK
		
		CASE DEGENATRON_GAMES_CLIENT_STATE_REQUESTING_ASSETS
		
			DEGENATRON_GAMES_PROCESS_CLIENT_STATE_REQUESTING_ASSETS()
		
		BREAK
		CASE DEGENATRON_GAMES_CLIENT_STATE_INTRO
		
			DEGENATRON_GAMES_PROCESS_CLIENT_STATE_INTRO()
		
		BREAK
		
		CASE DEGENATRON_GAMES_CLIENT_STATE_PLAYING
		
			DEGENATRON_GAMES_PROCESS_CLIENT_STATE_PLAYING()
		
		BREAK
		
		CASE DEGENATRON_GAMES_CLIENT_STATE_CLEANUP
		
			DEGENATRON_GAMES_CLEANUP_AND_EXIT_CLIENT()
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC DEGENATRON_GAMES_DEBUG_SET_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)

	sDegenatronGamesData.widgetGroupExample = parentWidgetGroup
	
ENDPROC
	
PROC DEGENATRON_GAMES_DEBUG_CREATE_WIDGETS()
	
	IF sDegenatronGamesData.bDebugCreatedWidgets
		EXIT
	ENDIF
	
	IF sDegenatronGamesData.widgetGroupExample = NULL
		EXIT
	ENDIF
	
	sDegenatronGamesData.widgetGroupExample = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
	SET_CURRENT_WIDGET_GROUP(sDegenatronGamesData.widgetGroupExample)

	ADD_WIDGET_FLOAT_SLIDER("Degenatron Ratio", sDegenatronGamesData.fScaleRatio,0.0,1.0,0.01)
	ADD_WIDGET_FLOAT_SLIDER("Degenatron Screen Space X", sDegenatronGamesData.vScreenSpace.x,0.0,5000.0,1.0)
	ADD_WIDGET_FLOAT_SLIDER("Degenatron Screen Space Y", sDegenatronGamesData.vScreenSpace.y,0.0,5000.0,1.0)
	ADD_WIDGET_BOOL("Draw", sDegenatronGamesData.bDebugDraw)
	ADD_WIDGET_BOOL("Draw Backmask", sDegenatronGamesData.bDebugDrawBackmask)
	ADD_WIDGET_BOOL("Draw Facade", sDegenatronGamesData.bDebugDrawFacade)
	START_WIDGET_GROUP("Glitch")
		ADD_WIDGET_BOOL("Override Glitch", sDegenatronGamesData.bDebugGlitchOverride)
		ADD_WIDGET_FLOAT_READ_ONLY("Glitch", sDegenatronGamesData.yGlitch)
		ADD_WIDGET_FLOAT_SLIDER("Noise", sDegenatronGamesData.fDebugNoise,-50.0,50.0,0.010)
		ADD_WIDGET_FLOAT_SLIDER("Ybug A", sDegenatronGamesData.fDebugYbugA,-50.0,50.0,0.010)
		ADD_WIDGET_FLOAT_SLIDER("Ybug B", sDegenatronGamesData.fDebugYbugB,-5.0,5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Ybug C", sDegenatronGamesData.fDebugYbugC,-30.0,30.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Aberration Time", sDegenatronGamesData.fDebugAberrationTime,0.0,10000.0,1.0)
		ADD_WIDGET_FLOAT_SLIDER("Aberration Noise", sDegenatronGamesData.fDebugAberrationNoise,-50.0,50.0,0.010)
		ADD_WIDGET_FLOAT_SLIDER("Aberration Mul", sDegenatronGamesData.fDebugAberrationNoiseMultiplier,0.0,30.0,1.0)
		ADD_WIDGET_FLOAT_SLIDER("Aberration Separation", sDegenatronGamesData.fDebugAberrationSeparation,0.0,100.0,1.0)
		ADD_WIDGET_FLOAT_SLIDER("Aberration Flooring", sDegenatronGamesData.fDebugAberrationFlooring,0.0,100.0,1.0)
		ADD_WIDGET_FLOAT_READ_ONLY("Noise", sDegenatronGamesData.fNoise)
		ADD_WIDGET_FLOAT_READ_ONLY("Ybug A", sDegenatronGamesData.fYbugA)
		ADD_WIDGET_FLOAT_READ_ONLY("Ybug B", sDegenatronGamesData.fYbugB)
		ADD_WIDGET_FLOAT_READ_ONLY("Ybug C", sDegenatronGamesData.fYbugC)
		ADD_WIDGET_FLOAT_READ_ONLY("Aberration Time", sDegenatronGamesData.fAberrationTime)
		ADD_WIDGET_FLOAT_READ_ONLY("Aberration Noise", sDegenatronGamesData.fAberrationNoise)
		ADD_WIDGET_FLOAT_READ_ONLY("Aberration Mul", sDegenatronGamesData.fAberrationNoiseMultiplier)
		ADD_WIDGET_FLOAT_READ_ONLY("Aberration Separation", sDegenatronGamesData.fAberrationSeparation)
		ADD_WIDGET_FLOAT_READ_ONLY("Aberration Separation", sDegenatronGamesData.fAberrationFlooring)
	STOP_WIDGET_GROUP()							

	START_WIDGET_GROUP("Game Data")
		ADD_WIDGET_INT_SLIDER("Level", sDegenatronGamesData.sGameData.iLevel,0,100,1)
		ADD_WIDGET_INT_SLIDER("Lifes", sDegenatronGamesData.sGameData.iLifes,0,100,1)
		ADD_WIDGET_INT_SLIDER("Score", sDegenatronGamesData.sGameData.iScore,0,9999999,50)
	STOP_WIDGET_GROUP()

	DG_DEFENDER_DEBUG_CREATE_WIDGETS()
	DG_MONKEY_DEBUG_CREATE_WIDGETS()
	DG_PENETRATOR_DEBUG_CREATE_WIDGETS()

	START_WIDGET_GROUP("Render")
		ADD_WIDGET_BOOL("bBlockDrawing", g_bBlockArcadeCabinetDrawing)
		ADD_WIDGET_BOOL("g_bMinimizeArcadeCabinetDrawing", g_bMinimizeArcadeCabinetDrawing)
		ADD_WIDGET_INT_READ_ONLY("iRectsDrawnThisFrame", iRectsDrawnThisFrame)
		ADD_WIDGET_INT_READ_ONLY("iSpritesDrawnThisFrame", iSpritesDrawnThisFrame)
		ADD_WIDGET_INT_READ_ONLY("iDebugLinesDrawnThisFrame", iDebugLinesDrawnThisFrame)
	STOP_WIDGET_GROUP()
	ARCADE_GAMES_POSTFX_WIDGET_CREATE()
	ARCADE_GAMES_SOUND_WIDGET_CREATE()
	ARCADE_GAMES_LEADERBOARD_WIDGET_CREATE()
	START_WIDGET_GROUP("Shake Controller")
		ADD_WIDGET_INT_SLIDER("Hard Shake", sDegenatronGamesData.iFreqHeavyShake,0,255,1)
		ADD_WIDGET_BOOL("Hard Shake", sDegenatronGamesData.bShouldDoHeavyShake)
		ADD_WIDGET_INT_SLIDER("Mid Shake", 	sDegenatronGamesData.iFreqMidShake,0,255,1)
		ADD_WIDGET_BOOL("Mid Shake", 	sDegenatronGamesData.bShouldDoMidShake)
		ADD_WIDGET_INT_SLIDER("Soft Shake", sDegenatronGamesData.iFreqSoftShake,0,255,1)
		ADD_WIDGET_BOOL("Soft Shake", sDegenatronGamesData.bShouldDoSoftShake)
	STOP_WIDGET_GROUP()
	START_WIDGET_GROUP("Challenges")
		ADD_WIDGET_BOOL("Reset All", sDegenatronGamesData.bDebugResetChallenges)
		ADD_WIDGET_BOOL("Reset Trophy", sDegenatronGamesData.bDebugResetTrophy)
		ADD_WIDGET_BOOL("Reset DoF", sDegenatronGamesData.bDebugResetDoF)
		ADD_WIDGET_BOOL("Reset Monkey", sDegenatronGamesData.bDebugResetMonkey)
		ADD_WIDGET_BOOL("Reset Penetrator", sDegenatronGamesData.bDebugResetPenetrator)
	STOP_WIDGET_GROUP()
	
	sDegenatronGamesData.bDebugCreatedWidgets = TRUE
	
	CLEAR_CURRENT_WIDGET_GROUP(sDegenatronGamesData.widgetGroupExample)
ENDPROC

PROC DEGENATRON_GAMES_DEBUG_UPDATE_WIDGETS()
	
	IF NOT sDegenatronGamesData.bDebugCreatedWidgets
		EXIT
	ENDIF
	
	IF sDegenatronGamesData.bDebugResetChallenges
		DEGENATRON_GAMES_RESET_CHALLENGES()
		sDegenatronGamesData.bDebugResetChallenges = FALSE
	ENDIF
	IF sDegenatronGamesData.bDebugResetTrophy
		ARCADE_CABINET_FLOW_CLEAR_TROPHY(ARCADE_GAME_TROPHY_DEGENATRON)
		sDegenatronGamesData.bDebugResetTrophy = FALSE
	ENDIF
	IF sDegenatronGamesData.bDebugResetDoF
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_KEEPFAITH, FALSE)
		ARCADE_CABINET_FLOW_CLEAR_TSHIRT(ARCADE_GAME_TSHIRTS_DEGENATRON_RIGHT_KIND_OF_FAITH)
		sDegenatronGamesData.bDebugResetDoF = FALSE
	ENDIF
	IF sDegenatronGamesData.bDebugResetMonkey
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_AQUAAPE, FALSE)
		ARCADE_CABINET_FLOW_CLEAR_TSHIRT(ARCADE_GAME_TSHIRTS_DEGENATRON_AQUATIC_APE)
		sDegenatronGamesData.bDebugResetMonkey = FALSE
	ENDIF
	IF sDegenatronGamesData.bDebugResetPenetrator
		SET_MP_INT_CHARACTER_AWARD(MP_AWARD_MASTERFUL, 0)
		ARCADE_CABINET_FLOW_CLEAR_TSHIRT(ARCADE_GAME_TSHIRTS_DEGENATRON_RIMMING_MASTER)
		sDegenatronGamesData.bDebugResetPenetrator = FALSE
	ENDIF
	
	DG_DEFENDER_DEBUG_UPDATE_WIDGETS()
	DG_MONKEY_DEBUG_UPDATE_WIDGETS()
	DG_PENETRATOR_DEBUG_UPDATE_WIDGETS()
	ARCADE_GAMES_POSTFX_WIDGET_UPDATE()
	ARCADE_GAMES_SOUND_WIDGET_UPDATE()
	ARCADE_GAMES_LEADERBOARD_WIDGET_UPDATE()
ENDPROC

/// PURPOSE:
///    Dumps the current state of the game to the logs
///    Automatically called when a bug is entered
PROC DEGENATRON_GAMES_DEBUG_DUMP_DATA_TO_LOGS()

	ARCADE_CABINET_DUMP_DATA_TO_CONSOLE()

	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_DEBUG_DUMP_DATA_TO_LOGS - Dumping data...")
	
	//General
	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] ----- GENERAL -----")
	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] eClientState = ", DEGENATRON_GAMES_DEBUG_GET_DEGENATRON_GAMES_CLIENT_STATE_AS_STRING(sDegenatronGamesData.eClientState))
	
ENDPROC

PROC DEGENATRON_GAMES_DEBUG_PROCESSING()

	DEGENATRON_GAMES_DEBUG_CREATE_WIDGETS()
	DEGENATRON_GAMES_DEBUG_UPDATE_WIDGETS()
	
	//If a bug is entered dump all data to the console.
	IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)
		DEGENATRON_GAMES_DEBUG_DUMP_DATA_TO_LOGS()
	ENDIF
	
ENDPROC

#ENDIF


/// PURPOSE:
///    Returns true if the game should quit
///    Processes holding a button to quit
/// RETURNS:
///    
FUNC BOOL DEGENATRON_GAMES_SHOULD_QUIT_NOW()

	IF g_bForceQuitPenthouseArcadeMachines
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SHOULD_QUIT_NOW - g_bForceQuitPenthouseArcadeMachines")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_DOING_HELI_DOCK_CUTSCENE(GET_PLAYER_INDEX())
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SHOULD_QUIT_NOW - IS_PLAYER_DOING_HELI_DOCK_CUTSCENE")
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SHOULD_QUIT_NOW - IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR")
		RETURN TRUE
	ENDIF	
	
	IF SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SHOULD_QUIT_NOW - SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME")
		RETURN TRUE
	ENDIF
	
	CONTROL_ACTION CA_Quit =  INPUT_FRONTEND_CANCEL
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_Quit = INPUT_FRONTEND_DELETE
	ENDIF

	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Quit)
	OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Quit))
		DRAW_GENERIC_METER(ciDEGENATRON_GAMES_HOLD_TO_QUIT_TIME - ABSI(NATIVE_TO_INT(GET_NETWORK_TIME()) - sDegenatronGamesData.iQuitTime), ciDEGENATRON_GAMES_HOLD_TO_QUIT_TIME, "DEG_GAME_QUIT")
	ELSE
		DRAW_GENERIC_METER(0, ciDEGENATRON_GAMES_HOLD_TO_QUIT_TIME, "DEG_GAME_QUIT")
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Quit)
		
		IF sDegenatronGamesData.iQuitTime = -HIGHEST_INT
			sDegenatronGamesData.iQuitTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + ciDEGENATRON_GAMES_HOLD_TO_QUIT_TIME
		ENDIF
				
		//DRAW_GENERIC_METER(ciIAP_HOLD_TO_QUIT_TIME - (sIAPData.iQuitTime - NATIVE_TO_INT(GET_NETWORK_TIME())), ciIAP_HOLD_TO_QUIT_TIME, "IAP_EXIT", HUD_COLOUR_RED, -1, HUDORDER_TOP)
		
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) > sDegenatronGamesData.iQuitTime
			CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_SHOULD_QUIT_NOW - Button Held")
			RETURN TRUE
		ENDIF
		
	ELIF sDegenatronGamesData.iQuitTime != -HIGHEST_INT
		sDegenatronGamesData.iQuitTime = -HIGHEST_INT
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Run every frame before the client state machine is processed
PROC DEGENATRON_GAMES_PRE_UPDATE()
	sDegenatronGamesData.iGameTimer += DEGENATRON_GAMES_FRAME_TIME()
	
	IF sDegenatronGamesData.eClientState >= DEGENATRON_GAMES_CLIENT_STATE_CLEANUP
		// Cleaning up, don't do anything
		EXIT
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		DEGENATRON_GAMES_SET_CLIENT_STATE(DEGENATRON_GAMES_CLIENT_STATE_CLEANUP)
		EXIT
	ENDIF

	IF DEGENATRON_GAMES_SHOULD_QUIT_NOW()
		DEGENATRON_GAMES_SET_CLIENT_STATE(DEGENATRON_GAMES_CLIENT_STATE_CLEANUP)
		EXIT
	ENDIF

	ARCADE_GAMES_LEADERBOARD_PROCESS_EVENTS()
	
	ARCADE_GAMES_LEADERBOARD_LOAD(DEGENATRON_GAMES_GET_CASINO_ARCADE_GAME())
	
	ARCADE_GAMES_HELP_TEXT_PRE_UPDATE()
	
	DEGENATRON_GAMES_UPDATE_ANIM_FRAMES()
	
	DEGENATRON_GAMES_MAINTAIN_GLITCH()
	
	DEGENATRON_GAMES_SOUND_PRE_GAME()

	CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DEGENATRON_GAMES_PRE_UPDATE - sDegenatronGamesData.yGlitch ",sDegenatronGamesData.yGlitch)
	
	BOOL bIsPlaying = sDegenatronGamesData.eClientState >= DEGENATRON_GAMES_CLIENT_STATE_INTRO
	ARCADE_CABINET_COMMON_EVERY_FRAME_PROCESSING(bIsPlaying)
	
ENDPROC
	
/// PURPOSE:
///    Run every frame after the client state machine is processed
PROC DEGENATRON_GAMES_POST_UPDATE()
	
	//Hide help text every frame unless this minigame wants to show some
	IF NOT ARCADE_GAMES_HELP_TEXT_SHOULD_ALLOW_THIS_FRAME()
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Call every frame to run the minigame
PROC PROCESS_DEGENATRON_GAMES()
	
	DEGENATRON_GAMES_PRE_UPDATE()

	DEGENATRON_GAMES_PROCESS_CLIENT_STATE_MACHINE()
	
	DEGENATRON_GAMES_POST_UPDATE()
#IF IS_DEBUG_BUILD
	DEGENATRON_GAMES_DEBUG_PROCESSING()
#ENDIF	
ENDPROC
