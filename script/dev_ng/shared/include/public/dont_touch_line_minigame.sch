//+-----------------------------------------------------------------------------+
//|																				|
//|						dont_touch_line_minigame.sch					        |
//|																				|
//|			Main header for "Don't Cross the Line 1989" minigame        		|
//|			                            										|
//|																				|
//+-----------------------------------------------------------------------------+

USING "timer_public.sch"

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"
USING "freemode_header.sch"
USING "net_mp_tv.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"

USING "LineActivation.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//Enums
ENUM LINE_MINIGAME_STATE
	LINE_MINIGAME_INITIALISING = 0,
	LINE_MINIGAME_REQUESTING_ASSETS,
	LINE_MINIGAME_LAUNCHING,
	LINE_MINIGAME_TITLE_SCREEN,				//Initial title screen: host presses a button to start the game.
	LINE_MINIGAME_RESHUFFLE,				//Reshuffle: Moves players to the right array indices if a player quit out
	LINE_MINIGAME_READY_SCREEN,				//Ready screen: allows players to confirm they're ready and resolves any network issues.
	LINE_MINIGAME_COUNTDOWN_SCREEN,			//Countdown screen: synchronises the start of the game.
	LINE_MINIGAME_RUNNING,					//Main game loop.
	LINE_MINIGAME_WINNER_SCREEN,			//Winner screen: Displays when there is only one player remaining.
	LINE_MINIGAME_REPLAY_SCREEN,			//Replay screen: Allows players to choose if they want to play again or quit.
	LINE_MINIGAME_GAME_OVER_SCREEN,			//Game over screen: Displays before cleanup if the last players decide to quit.
	LINE_MINIGAME_CLEANUP,
	LINE_MINIGAME_FINISHED
ENDENUM

ENUM LINE_MINIGAME_PLAYER_STATE
	LINE_MINIGAME_PLAYER_INITIALISING = 0,
	LINE_MINIGAME_PLAYER_WAITING_TO_START,
	LINE_MINIGAME_PLAYER_ACTIVE,
	LINE_MINIGAME_PLAYER_DEAD
ENDENUM

ENUM LINE_MINIGAME_RESHUFFLE_STATE
	LINE_MINIGAME_RESHUFFLE_INIT = 0,
	LINE_MINIGAME_RESHUFFLE_ASSIGN,
	LINE_MINIGAME_RESHUFFLE_CHECK,
	LINE_MINIGAME_RESHUFFLE_FINISHED
ENDENUM

ENUM LINE_MINIGAME_SPLASH_SCREEN_STATE
	LINE_MINIGAME_SPLASH_START = 0,
	LINE_MINIGAME_SPLASH_DEGEN_LOGO,
	LINE_MINIGAME_SPLASH_TWINKLE,
	LINE_MINIGAME_SPLASH_TITLES
ENDENUM

//Constants
CONST_INT FLOAT_COMPRESSION_DEFAULT_VALUE					9974 	//Used for compressing float data that describes the player's path.
CONST_INT DIRECTION_UP										0
CONST_INT DIRECTION_DOWN									1
CONST_INT DIRECTION_LEFT									2
CONST_INT DIRECTION_RIGHT									3
CONST_INT DIRECTION_UPDATE_ONLY								4 //Used to update the line without turning
CONST_INT LINE_MINIGAME_PLAYER_FLAGS_MOVEMENT_ENABLED		0
CONST_INT MAX_TAIL_SEGMENTS									60
CONST_INT MAX_NUM_PLAYERS									4
TWEAK_INT DEFAULT_PLAYER_ALPHA								255
TWEAK_INT TURN_DELAY										66
CONST_INT LINE_MINIGAME_UI_MESSAGE_EFFECT_NONE				0
CONST_INT LINE_MINIGAME_UI_MESSAGE_EFFECT_FLASH				1
CONST_INT LINE_MINIGAME_UI_MESSAGE_EFFECT_SCANLINE			2
CONST_INT LINE_MINIGAME_UI_MIC_HIDDEN						0
CONST_INT LINE_MINIGAME_UI_MIC_MUTED						1
CONST_INT LINE_MINIGAME_UI_MIC_INACTIVE						2
CONST_INT LINE_MINIGAME_UI_MIC_ACTIVE						3

//Minigame flags for the local script
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_1	0
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_2	1
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_3	2
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_MAIN_MUSIC			3
CONST_INT LINE_MINIGAME_FLAGS_COMPLETED_ONE_PLAYTHROUGH		4
CONST_INT LINE_MINIGAME_FLAGS_ALLOW_HELP_TEXT_THIS_FRAME	5
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_READY			6
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_3		7
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_2		8
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_1		9
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_GO		10
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_WINNER			11
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_GAME_OVER		12
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOADING			13
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_SPLASH			14
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES			15
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY			16
CONST_INT LINE_MINIGAME_FLAGS_DRAW_TITLES_THIS_FRAME		17
CONST_INT LINE_MINIGAME_FLAGS_TRIGGERED_SFX_READY			18

//Player specific flags for the local script
CONST_INT LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_CONFIRM	0
CONST_INT LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_REJECT	1
CONST_INT LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_CONFIRM	2
CONST_INT LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_REJECT	3

CONST_FLOAT DEFAULT_ASPECT_RATIO_MODIFIER					1.778 	//How much the X values for the minigame need to be multiplied by to work with the default aspect ratio.
TWEAK_FLOAT TAIL_THICKNESS									0.003
TWEAK_FLOAT STICK_THRESHOLD									0.65
TWEAK_FLOAT DEFAULT_SPEED									0.06
CONST_FLOAT PLAYER_1_START_X_FROM_CENTRE					-0.32	//See notes in DRAW_LINE_MINIGAME: X-positions need to be relative from the centre.
CONST_FLOAT PLAYER_1_START_Y								0.54
CONST_FLOAT PLAYER_2_START_X_FROM_CENTRE					0.321
CONST_FLOAT PLAYER_2_START_Y								0.54
CONST_FLOAT PLAYER_3_START_X_FROM_CENTRE					0.0
CONST_FLOAT PLAYER_3_START_Y								0.26
CONST_FLOAT PLAYER_4_START_X_FROM_CENTRE					0.0
CONST_FLOAT PLAYER_4_START_Y								0.822
TWEAK_FLOAT PLAYER_HEAD_WIDTH								0.0068
TWEAK_FLOAT PLAYER_HEAD_HEIGHT								0.012
TWEAK_FLOAT PLAYER_FADE_OUT_SPEED							300.0
TWEAK_FLOAT TOP_WALL_POS_Y									0.229
TWEAK_FLOAT BOTTOM_WALL_POS_Y								0.851
TWEAK_FLOAT LEFT_WALL_DIST_FROM_CENTRE						0.339
TWEAK_FLOAT RIGHT_WALL_DIST_FROM_CENTRE						0.343
TWEAK_FLOAT HEAD_COLLISION_BOX_MODIFIER						0.42

CONST_INT ciUPDATE_TIME										500
CONST_INT ciRESHUFFLE_TIME									1000
CONST_INT ciGAMEOVER_SCREEN_TIME							5000
CONST_INT ciINSERT_COINS_TIME								30000

CONST_INT ciEVENT_CACHE										32
CONST_INT ciLAG_CACHE										16

//PlayerBD Bitset
CONST_INT	ciPBD_ASSETS_LOADED		0
CONST_INT	ciPBD_READY_TO_LAUNCH	1
CONST_INT	ciPBD_NOT_CONFIRMED		2
CONST_INT	ciPBD_CONFIRM_ACCEPT	3
CONST_INT	ciPBD_CONFIRM_REJECT	4
CONST_INT	ciPBD_RESHUFFLED		5
CONST_INT	ciPBD_FINISHED_INTRO	6

SCRIPT_EVENT_DATA_DCTL_TURN LaggedTurnEventCache[ciLAG_CACHE]
INT iLastTurn[MAX_NUM_PLAYERS]

INT iLocalPlayer = -1

//Local Bitset
CONST_INT ciLineBS_UpdateDone 					0 //BOOL bUpdateDone 
CONST_INT ciLineBS_UpdateSent 					1 //BOOL bUpdateSent 
CONST_INT ciLineBS_UpdateGamertags 				2 //BOOL bUpdateGamertags 
CONST_INT ciLineBS_MovementProcessed_Part_0 	3 //BOOL bMovementProcessedThisFrame[MAX_NUM_PLAYERS]
CONST_INT ciLineBS_MovementProcessed_Part_1 	4
CONST_INT ciLineBS_MovementProcessed_Part_2 	5
CONST_INT ciLineBS_MovementProcessed_Part_3		6
CONST_INT ciLineBS_ReceivedEventsThisFrame_0 	7
CONST_INT ciLineBS_ReceivedEventsThisFrame_1 	8
CONST_INT ciLineBS_ReceivedEventsThisFrame_2 	9
CONST_INT ciLineBS_ReceivedEventsThisFrame_3 	10
CONST_INT ciLineBS_ReceivedEventsLastFrame_0 	11
CONST_INT ciLineBS_ReceivedEventsLastFrame_1 	12
CONST_INT ciLineBS_ReceivedEventsLastFrame_2 	13
CONST_INT ciLineBS_ReceivedEventsLastFrame_3 	14
CONST_INT ciLineBS_RecievedFirstSync_0			15
CONST_INT ciLineBS_RecievedFirstSync_1			16
CONST_INT ciLineBS_RecievedFirstSync_2			17
CONST_INT ciLineBS_RecievedFirstSync_3			18

INT iLocalLineBS

INT iTimeToStart = -1
SCRIPT_TIMER tdSyncEvent

TEXT_LABEL_63 strHeadshotTextures[MAX_NUM_PLAYERS]

#IF IS_DEBUG_BUILD
INT iDrawnSprites[5] //[0-3] tails [4] other
#ENDIF
//Structs
STRUCT LINE_MINIGAME_PLAYER_DATA
	LINE_MINIGAME_PLAYER_STATE eCurrentState
	
	INT iAlpha
	INT iDirection
	INT iFlags
	INT iTailIndex
	INT iPlayerMoveSound
	INT iPlayerCrashSound
	INT iPlayerTurnSound
	
	INT iPrevDirection
	INT iNextDirection
	INT iDirectionChangeHistory[2]		//Stores the last two direction changes: used to help determine if the player is turning back on themselves.
	INT iTimeOfLastTurn
	
	INT iTimeColliding = -HIGHEST_INT
	
	FLOAT fHeadX
	FLOAT fHeadY
	FLOAT fHeadRotation
	FLOAT fSpeedModifier				//Primary use is to stop remote players from moving if we detect collisions on the local machine. Default is 1.0.
	
	FLOAT fTail[MAX_TAIL_SEGMENTS]		//Packed float: contains both the X and Y positions for each segment.
ENDSTRUCT

STRUCT LINE_MINIGAME_DATA
	LINE_MINIGAME_STATE eCurrentState
	LINE_MINIGAME_SPLASH_SCREEN_STATE eSplashState
	
	INT iFlags
	INT iMusicTitleScreen
	INT iTextFlashTimer
	INT iNumPlayersThisRound
	INT iInputSoundID
	INT iGameOverSoundID
	
	FLOAT fAspectRatio
	FLOAT fAspectRatioModifier						
	
	SCRIPT_TIMER splashScreenTimer
	SCRIPT_TIMER startConfirmedTimer
	SCRIPT_TIMER insertCoinsTimer
	SCRIPT_TIMER readyTimer
	SCRIPT_TIMER countdownTimer
	SCRIPT_TIMER goTimer
	SCRIPT_TIMER winnerScreenTimer
	SCRIPT_TIMER winnerDeterminedTimer
	SCRIPT_TIMER replayScreenTimer
	SCRIPT_TIMER replayConfirmedTimer
	SCRIPT_TIMER gameOverScreenTimer
	SCRIPT_TIMER reshuffleTimer

	SCALEFORM_INDEX siMinigame

	LINE_MINIGAME_PLAYER_DATA PlayerData[MAX_NUM_PLAYERS]
ENDSTRUCT

STRUCT LINE_SERVER_DATA
	LINE_MINIGAME_STATE eCurrentState
	INT iWinningPlayer = -1
	INT iNewStartPositions[MAX_NUM_PLAYERS]
	INT iCurrentScore[MAX_NUM_PLAYERS]
	TEXT_LABEL_63 strGamerTags[MAX_NUM_PLAYERS]
	LINE_MINIGAME_RESHUFFLE_STATE eReshuffleState
ENDSTRUCT

LINE_SERVER_DATA LINE_ServerBD

PROC LINE_MINIGAME_PRE_DEFINE_SERVER_DATA()
	INT i
	FOR i = 0 TO MAX_NUM_PLAYERS - 1
		LINE_ServerBD.iNewStartPositions[i] = -1
	ENDFOR
ENDPROC

STRUCT LINE_PLAYER_DATA
	INT iStartPos = -1		//Represents the in-game position of the player. If a specific in-game player needs to be referenced (e.g "Player 1") then use this.
	INT iPlayerBDBitSet
ENDSTRUCT

LINE_PLAYER_DATA LINE_PlayerBD[MAX_NUM_PLAYERS]

//Debug only variables.
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetLineMinigame
	
	BOOL bDebugDrawTestRect = FALSE
	BOOL bDebugCreatedWidgets = FALSE
	BOOL bDebugDisplayDataOnScreen = FALSE
	BOOL bDebugDisplayCollisionInfo = FALSE
	BOOL bDebugAllowSinglePlayerTesting = FALSE
	BOOL bDebugEnableInputTest = FALSE
	BOOL bDebugEnableSplashTimer = FALSE
	BOOL bDebugOverrideMicStatus = FALSE
	BOOL bDebugHideMinigameUI = FALSE
	
	FLOAT fDebugTestRectPosX = 0.0
	FLOAT fDebugTestRectPosY = 0.0
	
	INT iDebugSplashTimer = 0
	INT iDebugMicOverride = 0
#ENDIF

/// PURPOSE:
///    Returns the given state name as a string, useful for debug prints.
FUNC STRING GET_LINE_MINIGAME_STATE_AS_STRING(LINE_MINIGAME_STATE eState)
	SWITCH eState
		CASE LINE_MINIGAME_INITIALISING 		RETURN "LINE_MINIGAME_INITIALISING"
		CASE LINE_MINIGAME_REQUESTING_ASSETS	RETURN "LINE_MINIGAME_REQUESTING_ASSETS"
		CASE LINE_MINIGAME_LAUNCHING			RETURN "LINE_MINIGAME_LAUNCHING"
		CASE LINE_MINIGAME_TITLE_SCREEN			RETURN "LINE_MINIGAME_TITLE_SCREEN"
		CASE LINE_MINIGAME_RESHUFFLE			RETURN "LINE_MINIGAME_RESHUFFLE"
		CASE LINE_MINIGAME_READY_SCREEN			RETURN "LINE_MINIGAME_READY_SCREEN"
		CASE LINE_MINIGAME_COUNTDOWN_SCREEN		RETURN "LINE_MINIGAME_COUNTDOWN_SCREEN"
		CASE LINE_MINIGAME_RUNNING				RETURN "LINE_MINIGAME_RUNNING"
		CASE LINE_MINIGAME_WINNER_SCREEN		RETURN "LINE_MINIGAME_WINNER_SCREEN"
		CASE LINE_MINIGAME_REPLAY_SCREEN		RETURN "LINE_MINIGAME_REPLAY_SCREEN"
		CASE LINE_MINIGAME_GAME_OVER_SCREEN		RETURN "LINE_MINIGAME_GAME_OVER_SCREEN"
		CASE LINE_MINIGAME_CLEANUP				RETURN "LINE_MINIGAME_CLEANUP"
		CASE LINE_MINIGAME_FINISHED				RETURN "LINE_MINIGAME_FINISHED"
	ENDSWITCH
	
	RETURN "State not recognised, GET_LINE_MINIGAME_STATE_AS_STRING needs to be updated."
ENDFUNC

/// PURPOSE:
///    Sets the minigame state to the given state, call this instead of setting directly for easy debug output.
PROC SET_LINE_MINIGAME_STATE(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_STATE eNewState)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] SET_LINE_MINIGAME_STATE - State changed from ", 
		GET_LINE_MINIGAME_STATE_AS_STRING(LINE_ServerBD.eCurrentState), " to ", GET_LINE_MINIGAME_STATE_AS_STRING(eNewState))
	
	minigameData.iTextFlashTimer = 0
	LINE_ServerBD.eCurrentState = eNewState
ENDPROC

PROC SET_LINE_MINIGAME_STATE_CLIENT(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_STATE eNewState)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] SET_LINE_MINIGAME_STATE_CLIENT - State changed from ", 
		GET_LINE_MINIGAME_STATE_AS_STRING(minigameData.eCurrentState), " to ", GET_LINE_MINIGAME_STATE_AS_STRING(eNewState))
	
	minigameData.iTextFlashTimer = 0
	minigameData.eCurrentState = eNewState
ENDPROC

/// PURPOSE:
///    Returns the given player state name as a string, useful for debug prints.
FUNC STRING GET_LINE_MINIGAME_PLAYER_STATE_AS_STRING(LINE_MINIGAME_PLAYER_STATE eState)
	SWITCH eState
		CASE LINE_MINIGAME_PLAYER_INITIALISING 			RETURN "LINE_MINIGAME_PLAYER_INITIALISING"
		CASE LINE_MINIGAME_PLAYER_WAITING_TO_START 		RETURN "LINE_MINIGAME_PLAYER_WAITING_TO_START"
		CASE LINE_MINIGAME_PLAYER_ACTIVE				RETURN "LINE_MINIGAME_PLAYER_ACTIVE"
		CASE LINE_MINIGAME_PLAYER_DEAD					RETURN "LINE_MINIGAME_PLAYER_DEAD"
	ENDSWITCH
	
	RETURN "State not recognised, GET_LINE_MINIGAME_PLAYER_STATE_AS_STRING needs to be updated."
ENDFUNC

/// PURPOSE:
///    Sets the player state to the given state, call this instead of setting directly for easy debug output.
PROC SET_LINE_MINIGAME_PLAYER_STATE(LINE_MINIGAME_PLAYER_DATA &playerData[], LINE_MINIGAME_PLAYER_STATE eNewState, INT iPlayer)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] SET_LINE_MINIGAME_PLAYER_STATE - State changed from ", 
		GET_LINE_MINIGAME_PLAYER_STATE_AS_STRING(playerData[iPlayer].eCurrentState), " to ", GET_LINE_MINIGAME_PLAYER_STATE_AS_STRING(eNewState))
	
	playerData[iPlayer].eCurrentState = eNewState
ENDPROC

/// PURPOSE:
///    Returns the given splash screen state name as a string, useful for debug prints.
FUNC STRING GET_LINE_MINIGAME_SPLASH_STATE_AS_STRING(LINE_MINIGAME_SPLASH_SCREEN_STATE eState)
	SWITCH eState
		CASE LINE_MINIGAME_SPLASH_START					RETURN "LINE_MINIGAME_SPLASH_START"
		CASE LINE_MINIGAME_SPLASH_DEGEN_LOGO			RETURN "LINE_MINIGAME_SPLASH_DEGEN_LOGO"
		CASE LINE_MINIGAME_SPLASH_TWINKLE 				RETURN "LINE_MINIGAME_SPLASH_TWINKLE"
		CASE LINE_MINIGAME_SPLASH_TITLES				RETURN "LINE_MINIGAME_SPLASH_TITLES"
	ENDSWITCH
	
	RETURN "State not recognised, GET_LINE_MINIGAME_SPLASH_STATE_AS_STRING needs to be updated."
ENDFUNC

/// PURPOSE:
///    Sets the splash screen state to the given state, call this instead of setting directly for easy debug output.
PROC SET_LINE_MINIGAME_SPLASH_STATE(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_SPLASH_SCREEN_STATE eNewState)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] SET_LINE_MINIGAME_SPLASH_STATE - State changed from ", 
		GET_LINE_MINIGAME_SPLASH_STATE_AS_STRING(minigameData.eSplashState), " to ", GET_LINE_MINIGAME_SPLASH_STATE_AS_STRING(eNewState))
	
	minigameData.eSplashState = eNewState
ENDPROC

///// PURPOSE:
/////    Returns the given player state name as a string, useful for debug prints.
FUNC STRING GET_LINE_MINIGAME_PLAYER_CONFIRM_STATE_AS_STRING(INT iState)
	SWITCH iState
		CASE ciPBD_NOT_CONFIRMED		RETURN "ciPBD_NOT_CONFIRMED"
		CASE ciPBD_CONFIRM_ACCEPT 		RETURN "ciPBD_CONFIRM_ACCEPT"
		CASE ciPBD_CONFIRM_REJECT		RETURN "ciPBD_CONFIRM_REJECT"
	ENDSWITCH
	
	RETURN "State not recognised, GET_LINE_MINIGAME_PLAYER_CONFIRM_STATE_AS_STRING needs to be updated."
ENDFUNC

///// PURPOSE:
/////    Sets the player state to the given state, call this instead of setting directly for easy debug output.
PROC SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(INT iState)
	CLEAR_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
	CLEAR_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
	CLEAR_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE - State for player ", iLocalPlayer, 
		" changed to ", GET_LINE_MINIGAME_PLAYER_CONFIRM_STATE_AS_STRING(iState))
		
	SET_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, iState)
ENDPROC


/// PURPOSE:
///    Compresses two floats into a single float value
/// PARAMS:
///    fInputX		-	Value 1 to be compressed
///    fInputY		-	Value 2 to be compressed
///    iPrecision	-	Degree of precision value
/// RETURNS:
///    FLOAT: Single float value made from compressing the two input float values together
FUNC FLOAT FLOAT_COMPRESSION_PACK(FLOAT fInputX, FLOAT fInputY, INT iPrecision)
	FLOAT fOutputX = TO_FLOAT(FLOOR(fInputX * (iPrecision - 1)))
	FLOAT fOutputY = TO_FLOAT(FLOOR(fInputY * (iPrecision - 1)))
	
	RETURN (fOutputX * iPrecision) + fOutputY
ENDFUNC

/// PURPOSE:
///    Decompresses a float variable that was originally compressed using FLOAT_COMPRESSION_PACK
/// PARAMS:
///    fCompressedInput	-	Value output by FLOAT_COMPRESSION_PACK
///   &fOutputX			-	A reference to a float value - Stores value 1 decompressed from the input value
///   &fOutputY			-	A reference to a float value - Stores value 2 decompressed from the input value
///    iPrecision		-	Degree of precision value - Use the same value used to pack input value.
PROC FLOAT_COMPRESSION_UNPACK(FLOAT fCompressedInput, FLOAT &fOutputX, FLOAT &fOutputY, INT iPrecision)
	fOutputX = TO_FLOAT(FLOOR(fCompressedInput / iPrecision))
	fOutputY = fCompressedInput % iPrecision

	fOutputX = fOutputX / (iPrecision - 1)
	fOutputY = fOutputY / (iPrecision - 1)
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Dumps data about the given player to the console.
	/// PARAMS:
	///    playerData - 
	PROC LINE_MINIGAME_DUMP_PLAYER_DATA_TO_CONSOLE(LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] iPlayer = ", iPlayer)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.eCurrentState = ", GET_LINE_MINIGAME_PLAYER_STATE_AS_STRING(playerData[iPlayer].eCurrentState))
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.iAlpha = ", playerData[iPlayer].iAlpha)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.iDirection = ", playerData[iPlayer].iDirection)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.iFlags = ", playerData[iPlayer].iFlags)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.iTailIndex = ", playerData[iPlayer].iTailIndex)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.fHeadX = ", playerData[iPlayer].fHeadX)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.fHeadY = ", playerData[iPlayer].fHeadY)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.fHeadRotation = ", playerData[iPlayer].fHeadRotation)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.iTailIndex = ", playerData[iPlayer].iTailIndex)
		
		//Broadcast data.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_ServerBD.strGamerTags[iPlayer] = ", LINE_ServerBD.strGamerTags[iPlayer])
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] strHeadshotTextures[iPlayer] = ", strHeadshotTextures[iPlayer])
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_ServerBD.iCurrentScore[iPlayer] = ", LINE_ServerBD.iCurrentScore[iPlayer])
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_PlayerBD[iPlayer].iStartPos = ", LINE_PlayerBD[iPlayer].iStartPos)
		
		INT i
		FLOAT fCurrentNodeX, fCurrentNodeY
		
		REPEAT MAX_TAIL_SEGMENTS i
			
			FLOAT_COMPRESSION_UNPACK(playerData[iPlayer].fTail[i], fCurrentNodeX, fCurrentNodeY, FLOAT_COMPRESSION_DEFAULT_VALUE)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] playerData.fTail[",i,"] = ", fCurrentNodeX ,", ",fCurrentNodeY)
		ENDREPEAT	
	ENDPROC
#ENDIF

/// PURPOSE:
///    Calculates the screen aspect ratio based on the current resolution.
FUNC FLOAT GET_CUSTOM_SCREEN_ASPECT_RATIO()
	INT iScreenX = 0
	INT iScreenY = 0

	GET_ACTUAL_SCREEN_RESOLUTION(iScreenX, iScreenY)
	
	FLOAT fAspectRatio = TO_FLOAT(iScreenX) / TO_FLOAT(iScreenY)
	
	//Fix for Eyefinity triple monitors: the resolution returns as the full three-screen width but the game is only drawn on one screen.
	IF IS_PC_VERSION()
		IF fAspectRatio >= 4.0
			fAspectRatio = fAspectRatio / 3.0
		ENDIF
	ENDIF
	
	RETURN fAspectRatio
ENDFUNC

/// PURPOSE:
///    Calculates the modifer value used for the width of sprites based on the current aspect ratio and 
///    the default aspect modifier value.
/// PARAMS:
///    fAspectRatio	- The current aspect ratio value.
/// RETURNS:
///    FLOAT: The value the sprite needs to be multiplied by to keep the correct aspect ratio. Default / current aspect value
FUNC FLOAT GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO(FLOAT fAspectRatio)
	RETURN DEFAULT_ASPECT_RATIO_MODIFIER / fAspectRatio
ENDFUNC

/// PURPOSE:
///    Updates the current aspect ratio and aspect ratio modifier values.
PROC SET_CURRENT_ASPECT_RATIO_MODIFIER(LINE_MINIGAME_DATA &minigameData)
	minigameData.fAspectRatio = GET_CUSTOM_SCREEN_ASPECT_RATIO()
	minigameData.fAspectRatioModifier = GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO(minigameData.fAspectRatio)
ENDPROC

FUNC HUD_COLOURS GET_HUD_COLOUR_FOR_PLAYER(INT iPos)
	SWITCH(iPos)
		CASE 0 		RETURN HUD_COLOUR_DEGEN_CYAN
		CASE 1		RETURN HUD_COLOUR_DEGEN_MAGENTA
		CASE 2		RETURN HUD_COLOUR_DEGEN_GREEN
		CASE 3		RETURN HUD_COLOUR_DEGEN_YELLOW
	ENDSWITCH
	
	RETURN HUD_COLOUR_PURE_WHITE
ENDFUNC

PROC GET_COLOUR_FOR_PLAYER(INT iPos, INT &iR, INT &iG, INT &iB)
	INT iA
	
	GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_PLAYER(iPos)), iR, iG, iB, iA)
ENDPROC

/// PURPOSE:
///    Checks if all the assets needed for the minigame have been loaded.
FUNC BOOL LINE_MINIGAME_HAVE_ALL_ASSETS_LOADED()
	IF REQUEST_SCRIPT_AUDIO_BANK("DLC_EXEC1/OFFICE_BOARDROOM")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("LineArcadeMinigame")
	AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the gamertag corresponding to the player at the given start position. 
FUNC TEXT_LABEL_63 LINE_MINIGAME_GET_GAMERTAG_FOR_PLAYER_POSITION(INT iStartPos)
	TEXT_LABEL_63 strGamerTag = ""
	
	INT i
	REPEAT MAX_NUM_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos = iStartPos
			strGamerTag = LINE_ServerBD.strGamerTags[i]
		ENDIF
	ENDREPEAT
	
	RETURN strGamerTag
ENDFUNC

/// PURPOSE:
///    Gets the headshot texture corresponding to the player at the given start position. 
FUNC TEXT_LABEL_63 LINE_MINIGAME_GET_HEADSHOT_FOR_PLAYER_POSITION(INT iStartPos)
	TEXT_LABEL_63 strHeadshotTexture = NULL_STRING()

	INT i
	REPEAT MAX_NUM_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos = iStartPos
			strHeadshotTexture = strHeadshotTextures[i]
		ENDIF
	ENDREPEAT
	
	RETURN strHeadshotTexture
ENDFUNC

/// PURPOSE:
///    Gets the score corresponding to the player at the given start position. 
FUNC INT LINE_MINIGAME_GET_SCORE_FOR_PLAYER_POSITION(INT iStartPos)
	INT i
	REPEAT MAX_NUM_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos = iStartPos
			RETURN LINE_ServerBD.iCurrentScore[i]
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Gets the mic status corresponding to the player at the given start position. 
FUNC INT LINE_MINIGAME_GET_MIC_STATUS_FOR_PLAYER_POSITION(INT iStartPos)
	#IF IS_DEBUG_BUILD
		IF bDebugOverrideMicStatus
			RETURN iDebugMicOverride
		ENDIF
	#ENDIF

	INT i
	REPEAT MAX_NUM_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos = iStartPos
			PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
		
			IF NOT NETWORK_PLAYER_HAS_HEADSET(player)
				RETURN LINE_MINIGAME_UI_MIC_HIDDEN
			ENDIF
		
			IF NETWORK_IS_PLAYER_MUTED_BY_ME(player)
				RETURN LINE_MINIGAME_UI_MIC_MUTED
			ENDIF
			
			IF NETWORK_IS_PLAYER_TALKING(player)
				RETURN LINE_MINIGAME_UI_MIC_ACTIVE
			ENDIF
		
			RETURN LINE_MINIGAME_UI_MIC_INACTIVE
		ENDIF
	ENDREPEAT
	
	RETURN LINE_MINIGAME_UI_MIC_HIDDEN
ENDFUNC

/// PURPOSE:
///    Turns on the Scaleform intro loading screen animation.
PROC LINE_MINIGAME_UI_SHOW_LOADING_SCREEN(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_SHOW_LOADING_SCREEN called.")

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "SHOW_LOADING_SCREEN")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Turns off the Scaleform intro loading screen animation.
PROC LINE_MINIGAME_UI_HIDE_LOADING_SCREEN(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_HIDE_LOADING_SCREEN called.")

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "HIDE_LOADING_SCREEN")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the given text label to be displayed in the centre of the screen.
PROC LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(LINE_MINIGAME_DATA &minigameData, STRING strMessageLabel, HUD_COLOURS eTextColour = HUD_COLOUR_PURE_WHITE, 
					INT iEffect = LINE_MINIGAME_UI_MESSAGE_EFFECT_SCANLINE)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE called with label ", strMessageLabel)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "SET_CENTRAL_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strMessageLabel)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eTextColour))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEffect)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the given text label and player name to be displayed in the centre of the screen.
PROC LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE_WITH_PLAYER_NAME(LINE_MINIGAME_DATA &minigameData, STRING strMessageLabel, STRING strPlayerName, 
					HUD_COLOURS eTextColour = HUD_COLOUR_PURE_WHITE, INT iEffect = LINE_MINIGAME_UI_MESSAGE_EFFECT_SCANLINE, 
					HUD_COLOURS ePlayerColour = HUD_COLOUR_PURE_WHITE)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE_WITH_PLAYER_NAME called with label ", 
		strMessageLabel, " and player name ", strPlayerName)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "SET_CENTRAL_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strMessageLabel)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eTextColour))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEffect)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strPlayerName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ePlayerColour))
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes any text displaying in the centre of the screen.
PROC LINE_MINIGAME_UI_CLEAR_CENTRAL_MESSAGE(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_CLEAR_CENTRAL_MESSAGE called.")

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "CLEAR_CENTRAL_MESSAGE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    To be used privately by LINE_MINIGAME_UI_UPDATE_LOBBY: Finds the player that corresponds to the given start position
///    and adds their confirmation state to the scaleform movie.
PROC LINE_MINIGAME_UI_UPDATE_LOBBY_ADD_PARAM_FOR_PLAYER_POSITION(INT iStartPos)
	BOOL bFoundPlayer = FALSE
	INT i
	
	REPEAT COUNT_OF(LINE_PlayerBD) i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos = iStartPos
		
			bFoundPlayer = TRUE
		
			IF IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			ELIF IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT bFoundPlayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Private function for use by LINE_MINIGAME_UI_INIT_LOBBY: Adds the player's gamertag and headshot to the lobby display.
PROC LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_LOBBY_DISPLAY(INT iStartPos)
	TEXT_LABEL_63 strGamerTag = LINE_MINIGAME_GET_GAMERTAG_FOR_PLAYER_POSITION(iStartPos)
	TEXT_LABEL_63 strHeadshotTexture = LINE_MINIGAME_GET_HEADSHOT_FOR_PLAYER_POSITION(iStartPos)
	
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTexture)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strGamerTag)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_LOBBY_DISPLAY called for start pos ", iStartPos, 
			" with gamertag ", strGamerTag, " and headshot ", strHeadshotTexture)
ENDPROC

/// PURPOSE:
///    Shows the game lobby which consists of a centred message and four boxes corresponding to each player. Each box
///    Displays a headshot of the player, their gamertag and an optional tick or cross.
PROC LINE_MINIGAME_UI_INIT_LOBBY(LINE_MINIGAME_DATA &minigameData, STRING strLobbyText, HUD_COLOURS eTextColour = HUD_COLOUR_PURE_WHITE)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_INIT_LOBBY called with label ", strLobbyText)

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "INIT_LOBBY")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strLobbyText)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eTextColour))
			
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_LOBBY_DISPLAY(0)
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_LOBBY_DISPLAY(1)
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_LOBBY_DISPLAY(2)
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_LOBBY_DISPLAY(3)
			
			IF iLocalPlayer > -1
			AND LINE_PlayerBD[iLocalPlayer].iStartPos > -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(LINE_PlayerBD[iLocalPlayer].iStartPos)
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the game lobby, showing ticks or crosses for each player.
///    0 = no decision made, 1 = tick, 2 = cross
PROC LINE_MINIGAME_UI_UPDATE_LOBBY(LINE_MINIGAME_DATA &minigameData)
	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "UPDATE_LOBBY")
			LINE_MINIGAME_UI_UPDATE_LOBBY_ADD_PARAM_FOR_PLAYER_POSITION(0)
			LINE_MINIGAME_UI_UPDATE_LOBBY_ADD_PARAM_FOR_PLAYER_POSITION(1)
			LINE_MINIGAME_UI_UPDATE_LOBBY_ADD_PARAM_FOR_PLAYER_POSITION(2)
			LINE_MINIGAME_UI_UPDATE_LOBBY_ADD_PARAM_FOR_PLAYER_POSITION(3)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Hides the game lobby.
PROC LINE_MINIGAME_UI_HIDE_LOBBY(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_HIDE_LOBBY called.")

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "HIDE_LOBBY")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the mic icons for each player on lobby and score screens.
PROC LINE_MINIGAME_UI_SET_MICS(LINE_MINIGAME_DATA &minigameData)
	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "SET_MICS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(LINE_MINIGAME_GET_MIC_STATUS_FOR_PLAYER_POSITION(0))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(LINE_MINIGAME_GET_MIC_STATUS_FOR_PLAYER_POSITION(1))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(LINE_MINIGAME_GET_MIC_STATUS_FOR_PLAYER_POSITION(2))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(LINE_MINIGAME_GET_MIC_STATUS_FOR_PLAYER_POSITION(3))
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Private function for use by LINE_MINIGAME_UI_SHOW_HUD: Adds the player's gamertag and headshot to the lobby display.
PROC LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_SCORE_HUD(INT iStartPos)
	TEXT_LABEL_63 strGamerTag = LINE_MINIGAME_GET_GAMERTAG_FOR_PLAYER_POSITION(iStartPos)
	TEXT_LABEL_63 strHeadshotTexture = LINE_MINIGAME_GET_HEADSHOT_FOR_PLAYER_POSITION(iStartPos)
	INT iScore = LINE_MINIGAME_GET_SCORE_FOR_PLAYER_POSITION(iStartPos)
	
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTexture)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strGamerTag)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_SCORE_HUD called for start pos ", iStartPos, 
			" with gamertag ", strGamerTag, " headshot ", strHeadshotTexture, " and score ", iScore)
ENDPROC

/// PURPOSE:
///    Shows the current scores of the players along with their headshots and gamertags.
PROC LINE_MINIGAME_UI_SHOW_HUD(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_SHOW_HUD called.")

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "SHOW_HUD")		
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_SCORE_HUD(0)
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_SCORE_HUD(1)
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_SCORE_HUD(2)
			LINE_MINIGAME_UI_ADD_PLAYER_PARAMS_TO_SCORE_HUD(3)
			
			IF iLocalPlayer > -1
			AND LINE_PlayerBD[iLocalPlayer].iStartPos > -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(LINE_PlayerBD[iLocalPlayer].iStartPos)
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Hides the score HUD.
PROC LINE_MINIGAME_UI_HIDE_HUD(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UI_HIDE_HUD called.")

	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		BEGIN_SCALEFORM_MOVIE_METHOD(minigameData.siMinigame, "HIDE_HUD")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Check to see if two rectangles overlap with each other
/// PARAMS:
///    fCentreX1	-	X coord center point of rectangle 1
///    fCentreY1	-	Y coord center point of rectangle 1
///    fWidth1		-	Width of rectangle 1
///    fHeight1		-	Height of rectangle 1
///    fCentreX		-	X coord center point of rectangle 2
///    fCentreY2	-	Y coord center point of rectangle 2
///    fWidth2		-	Width of rectangle 1
///    fHeight2		-	Width of rectangle 1
/// RETURNS:
///    BOOL: TRUE if the two rectangles overlap
FUNC BOOL DO_RECTANGLES_OVERLAP(FLOAT fCentreX1, FLOAT fCentreY1, FLOAT fWidth1, FLOAT fHeight1, 
								FLOAT fCentreX2, FLOAT fCentreY2, FLOAT fWidth2, FLOAT fHeight2)
	IF (fCentreX1 + (fWidth1 * 0.5)) < (fCentreX2 - (fWidth2 * 0.5))
	    RETURN FALSE
	ENDIF

	IF (fCentreX2 + (fWidth2 * 0.5)) < (fCentreX1 - (fWidth1 * 0.5))
	    RETURN FALSE
	ENDIF

	IF (fCentreY1 + (fHeight1 * 0.5)) < (fCentreY2 - (fHeight2 * 0.5))
	    RETURN FALSE
	ENDIF

	IF (fCentreY2 + (fHeight2 * 0.5)) < (fCentreY1 - (fHeight1 * 0.5))
	    RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Resets all data stored within the player's tail array.
PROC LINE_MINIGAME_RESET_TAIL_ARRAY(FLOAT &fTailArray[])
	INT i = 0
	REPEAT COUNT_OF(fTailArray) i
		fTailArray[i] = 0.0
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Updates the player's position based on their current direction and speed.
PROC LINE_MINIGAME_UPDATE_PLAYER_POSITION(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	FLOAT fSpeedX = DEFAULT_SPEED * playerData[iPlayer].fSpeedModifier
	FLOAT fSpeedY = (DEFAULT_SPEED * playerData[iPlayer].fSpeedModifier) * minigameData.fAspectRatio

	SWITCH playerData[iPlayer].iDirection
		CASE DIRECTION_UP	
			playerData[iPlayer].fHeadY = playerData[iPlayer].fHeadY -@ fSpeedY
		BREAK
		
		CASE DIRECTION_DOWN	
			playerData[iPlayer].fHeadY = playerData[iPlayer].fHeadY +@ fSpeedY
		BREAK
		
		CASE DIRECTION_LEFT	
			playerData[iPlayer].fHeadX = playerData[iPlayer].fHeadX -@ fSpeedX
		BREAK
		
		CASE DIRECTION_RIGHT
			playerData[iPlayer].fHeadX = playerData[iPlayer].fHeadX +@ fSpeedX
		BREAK
	ENDSWITCH
	
	//Reset the speed modifier.
	playerData[iPlayer].fSpeedModifier = 1.0
ENDPROC

/// PURPOSE:
///    Converts the player's current direction into a rotation for the head.
PROC LINE_MINIGAME_UPDATE_PLAYER_HEAD_ROTATION(LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	SWITCH playerData[iPlayer].iDirection
		CASE DIRECTION_UP	
			playerData[iPlayer].fHeadRotation = 0.0
		BREAK
		
		CASE DIRECTION_DOWN	
			playerData[iPlayer].fHeadRotation = 180.0
		BREAK
		
		CASE DIRECTION_LEFT	
			playerData[iPlayer].fHeadRotation = 270.0
		BREAK
		
		CASE DIRECTION_RIGHT
			playerData[iPlayer].fHeadRotation = 90.0
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Returns TRUE if the player has collided with the bounds of the player area.
FUNC BOOL HAS_PLAYER_COLLIDED_WITH_SIDE_WALLS(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	FLOAT fHeadXAdjusted = (0.5 - ((0.5 - playerData[iPlayer].fHeadX) * minigameData.fAspectRatioModifier))
	FLOAT fHeadWidthAdjusted = PLAYER_HEAD_WIDTH * minigameData.fAspectRatioModifier
	
	#IF IS_DEBUG_BUILD
		IF bDebugDisplayCollisionInfo
			DRAW_RECT(fHeadXAdjusted, playerData[iPlayer].fHeadY, 0.002, 0.002 * minigameData.fAspectRatio, 255, 255, 255, 255)
			DRAW_RECT(fHeadXAdjusted - (fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER), playerData[iPlayer].fHeadY, 0.002, 0.002 * minigameData.fAspectRatio, 255, 255, 255, 255)
			DRAW_RECT(fHeadXAdjusted + (fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER), playerData[iPlayer].fHeadY, 0.002, 0.002 * minigameData.fAspectRatio, 255, 255, 255, 255)
			DRAW_RECT(fHeadXAdjusted, playerData[iPlayer].fHeadY - (PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER), 0.002, 0.002 * minigameData.fAspectRatio, 255, 255, 255, 255)
			DRAW_RECT(fHeadXAdjusted, playerData[iPlayer].fHeadY + (PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER), 0.002, 0.002 * minigameData.fAspectRatio, 255, 255, 255, 255)

			
			DRAW_RECT(0.5, TOP_WALL_POS_Y, 0.5, 0.002 * minigameData.fAspectRatio, 255, 0, 0, 255)
			DRAW_RECT(0.5, BOTTOM_WALL_POS_Y, 0.5, 0.002 * minigameData.fAspectRatio, 255, 0, 0, 255)
			DRAW_RECT(0.5 - (LEFT_WALL_DIST_FROM_CENTRE * minigameData.fAspectRatioModifier), 0.5, 0.002, 0.5, 255, 0, 0, 255)
			DRAW_RECT(0.5 + (RIGHT_WALL_DIST_FROM_CENTRE * minigameData.fAspectRatioModifier), 0.5, 0.002, 0.5, 255, 0, 0, 255)
		ENDIF
	#ENDIF
	
	//Only check the wall that the player is currently heading towards.
	SWITCH playerData[iPlayer].iDirection
		CASE DIRECTION_UP	
			IF playerData[iPlayer].fHeadY - (PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER) <= TOP_WALL_POS_Y
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE DIRECTION_DOWN	
			IF playerData[iPlayer].fHeadY + (PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER) >= BOTTOM_WALL_POS_Y
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE DIRECTION_LEFT	
			IF fHeadXAdjusted - (fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER) <= 0.5 - (LEFT_WALL_DIST_FROM_CENTRE * minigameData.fAspectRatioModifier)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE DIRECTION_RIGHT
			IF fHeadXAdjusted + (fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER) >= 0.5 + (RIGHT_WALL_DIST_FROM_CENTRE * minigameData.fAspectRatioModifier)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the player to their death state, and runs any script that needs to occur on the moment of death.
PROC LINE_MINIGAME_KILL_PLAYER(LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_KILL_PLAYER - Killing player ", iPlayer)
	
	//Kill the movement sound.
	IF NOT HAS_SOUND_FINISHED(playerData[iPlayer].iPlayerMoveSound)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_KILL_PLAYER - Stopping move sound for player ", iPlayer)
		STOP_SOUND(playerData[iPlayer].iPlayerMoveSound)
	ENDIF
	
	//Play the death sound: different sounds depending on if this player is local or remote.
	IF iPlayer = iLocalPlayer
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_KILL_PLAYER - Playing local player death sound.")
		PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerCrashSound, "Crash", "DLC_EXEC_ARC_MAC_SOUNDS")
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_KILL_PLAYER - Playing remote player death sound.")
		PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerCrashSound, "Crash_NPC", "DLC_EXEC_ARC_MAC_SOUNDS")
	ENDIF
	
	SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerCrashSound, "X", playerData[iPlayer].fHeadX)
	SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerCrashSound, "Y", playerData[iPlayer].fHeadY)
	
	
	SET_LINE_MINIGAME_PLAYER_STATE(playerData, LINE_MINIGAME_PLAYER_DEAD, iPlayer)
ENDPROC

/// PURPOSE:
///    Call every frame to handle player movement: receives input and converts it into a speed and direction.
PROC LINE_MINIGAME_UPDATE_LOCAL_PLAYER_MOVEMENT( LINE_MINIGAME_PLAYER_DATA &playerData[])

	IF playerData[iLocalPlayer].iNextDirection < 0
		playerData[iLocalPlayer].iNextDirection = playerData[iLocalPlayer].iDirection
	ENDIF
	
	IF GET_GAME_TIMER() - playerData[iLocalPlayer].iTimeOfLastTurn >= TURN_DELAY
		FLOAT fLeftStickX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		FLOAT fLeftStickY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		
		IF fLeftStickY < -STICK_THRESHOLD
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			IF playerData[iLocalPlayer].iDirection != DIRECTION_DOWN
				playerData[iLocalPlayer].iNextDirection = DIRECTION_UP
			ENDIF
		ENDIF
		
		IF fLeftStickY > STICK_THRESHOLD
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			IF playerData[iLocalPlayer].iDirection != DIRECTION_UP
				playerData[iLocalPlayer].iNextDirection = DIRECTION_DOWN
			ENDIF
		ENDIF
		
		IF fLeftStickX < -STICK_THRESHOLD
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			IF playerData[iLocalPlayer].iDirection != DIRECTION_RIGHT
				playerData[iLocalPlayer].iNextDirection = DIRECTION_LEFT
			ENDIF
		ENDIF
		
		IF fLeftStickX > STICK_THRESHOLD
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			IF playerData[iLocalPlayer].iDirection != DIRECTION_LEFT
				playerData[iLocalPlayer].iNextDirection = DIRECTION_RIGHT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Converts the node data into a sprite position, width and height to use for drawing and/or collision detection.
PROC GET_SEGMENT_SPRITE_DIMENSIONS_FROM_NODE_DATA(LINE_MINIGAME_DATA &minigameData, 
												  FLOAT fCurrentNodeX, FLOAT fCurrentNodeY, FLOAT fNextNodeX, FLOAT fNextNodeY,
												  FLOAT &fSpriteX, FLOAT &fSpriteY, FLOAT &fSpriteWidth, FLOAT &fSpriteHeight,
												  FLOAT fCustomTailThickness = 0.0)
	
	IF fCustomTailThickness = 0.0
		fCustomTailThickness = TAIL_THICKNESS
	ENDIF
	
	FLOAT fDistToNextNodeX = fNextNodeX - fCurrentNodeX
	FLOAT fDistToNextNodeY = fNextNodeY - fCurrentNodeY
	
	fSpriteX = fCurrentNodeX + (fDistToNextNodeX * 0.5)
	fSpriteY = fCurrentNodeY + (fDistToNextNodeY * 0.5)
	
	IF ABSF(fDistToNextNodeX) > 0.001
		//These two nodes connect horizontally: adjust the sprite accordingly.
		fSpriteWidth = ABSF(fDistToNextNodeX) + (fCustomTailThickness * 0.5)
		fSpriteHeight = (fCustomTailThickness * minigameData.fAspectRatio)
	ELSE
		//These two nodes connect vertically: adjust the sprite accordingly.
		fSpriteWidth = fCustomTailThickness
		fSpriteHeight = ABSF(fDistToNextNodeY) + (fCustomTailThickness * 0.5 * minigameData.fAspectRatio)
	ENDIF
	
	//Adjust horizontal values based on the aspect ratio modifiers.
	fSpriteX = (0.5 - ((0.5 - fSpriteX) * minigameData.fAspectRatioModifier))
	fSpriteWidth = fSpriteWidth * minigameData.fAspectRatioModifier
ENDPROC

PROC PROCESS_OTHER_PLAYER_COLLISIONS(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_PLAYER_DATA &playerData[], BOOL bKillOnCollision = TRUE)
			
	FLOAT fHeadXAdjusted = (0.5 - ((0.5 - playerData[iLocalPlayer].fHeadX) * minigameData.fAspectRatioModifier))
	FLOAT fHeadWidthAdjusted = PLAYER_HEAD_WIDTH * minigameData.fAspectRatioModifier
	
	FLOAT fCurrentNodeX, fCurrentNodeY, fNextNodeX, fNextNodeY, fDistToNextNodeX, fDistToNextNodeY 
	FLOAT fSegmentSpriteX, fSegmentSpriteY, fSegmentSpriteWidth, fSegmentSpriteHeight
	FLOAT fLocalTailStartX, fLocalTailStartY, fLocalTailEndX, fLocalTailEndY
	FLOAT fLocalTailSpriteX, fLocalTailSpriteY, fLocalTailSpriteWidth, fLocalTailSpriteHeight
	
	//2848616 - Store some info about the tail segment closest to the player's head, this will be used for backup collision checks.
	//Make the dimensions for this box slightly smaller than the drawn version, as we want this to be a backup check and not interfere
	//with the regular checks.
	INT iLocalTailIndex = playerData[iLocalPlayer].iTailIndex - 1
	
	IF iLocalTailIndex > -1 AND iLocalTailIndex < MAX_TAIL_SEGMENTS - 1
		FLOAT_COMPRESSION_UNPACK(playerData[iLocalPlayer].fTail[iLocalTailIndex], fLocalTailStartX, fLocalTailStartY, FLOAT_COMPRESSION_DEFAULT_VALUE)
		
		IF playerData[iLocalPlayer].iDirection = DIRECTION_LEFT
			fLocalTailEndX = playerData[iLocalPlayer].fHeadX + (fHeadWidthAdjusted * 0.5)
			fLocalTailEndY = playerData[iLocalPlayer].fHeadY
			
			IF fLocalTailEndX > fLocalTailStartX
				fLocalTailEndX = fLocalTailStartX
			ENDIF
		ELIF playerData[iLocalPlayer].iDirection = DIRECTION_RIGHT
			fLocalTailEndX = playerData[iLocalPlayer].fHeadX - (fHeadWidthAdjusted * 0.5)
			fLocalTailEndY = playerData[iLocalPlayer].fHeadY
			
			IF fLocalTailEndX < fLocalTailStartX
				fLocalTailEndX = fLocalTailStartX
			ENDIF
		ELIF playerData[iLocalPlayer].iDirection = DIRECTION_UP
			fLocalTailEndX = playerData[iLocalPlayer].fHeadX
			fLocalTailEndY = playerData[iLocalPlayer].fHeadY + (PLAYER_HEAD_HEIGHT * 0.5)
			
			IF fLocalTailEndY > fLocalTailStartY
				fLocalTailEndY = fLocalTailStartY
			ENDIF
		ELIF playerData[iLocalPlayer].iDirection = DIRECTION_DOWN
			fLocalTailEndX = playerData[iLocalPlayer].fHeadX
			fLocalTailEndY = playerData[iLocalPlayer].fHeadY - (PLAYER_HEAD_HEIGHT * 0.5)
			
			IF fLocalTailEndY < fLocalTailStartY
				fLocalTailEndY = fLocalTailStartY
			ENDIF
		ENDIF
	ENDIF
	
	INT iPlayer, i
	FOR iPlayer = 0 TO MAX_NUM_PLAYERS - 1
		BOOL bPlayersCollidedThisFrame = FALSE
	
		IF iPlayer = iLocalPlayer
			RELOOP
		ENDIF
		
		IF playerData[iPlayer].iTimeColliding != -HIGHEST_INT
		AND NATIVE_TO_INT(GET_NETWORK_TIME()) > playerData[iPlayer].iTimeColliding + 1000
			CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_OTHER_PLAYER_COLLISIONS - Not processing player (likely DCed or suspended): ", iPlayer)
			RELOOP
		ENDIF
		
		IF playerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
		AND playerData[iLocalPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
			FOR i = 0 TO MAX_TAIL_SEGMENTS - 1
				FLOAT_COMPRESSION_UNPACK(playerData[iPlayer].fTail[i], fCurrentNodeX, fCurrentNodeY, FLOAT_COMPRESSION_DEFAULT_VALUE)
				IF fCurrentNodeX != 0.0 OR fCurrentNodeY != 0.0
					IF i + 1 >= MAX_TAIL_SEGMENTS
						//This is the last segment in the array, so this segment needs to attach to the head.
						fNextNodeX = playerData[iPlayer].fHeadX
						fNextNodeY = playerData[iPlayer].fHeadY
					ELSE
						FLOAT_COMPRESSION_UNPACK(playerData[iPlayer].fTail[i+1], fNextNodeX, fNextNodeY, FLOAT_COMPRESSION_DEFAULT_VALUE)
						
						IF fNextNodeX = 0.0 AND fNextNodeY = 0.0
							//The next segment is invalid, so this segment needs to attach to the head.
							fNextNodeX = playerData[iPlayer].fHeadX
							fNextNodeY = playerData[iPlayer].fHeadY
						ENDIF
					ENDIF
					
					//The tail array stores end-to-end node positions, so the sprite needs to be drawn in the middle of two nodes.
					fDistToNextNodeX = fNextNodeX - fCurrentNodeX
					fDistToNextNodeY = fNextNodeY - fCurrentNodeY
					fSegmentSpriteX = fCurrentNodeX + (fDistToNextNodeX * 0.5)
					fSegmentSpriteY = fCurrentNodeY + (fDistToNextNodeY * 0.5)
					
					
					IF ABSF(fDistToNextNodeX) > 0.001
						//These two nodes connect horizontally: adjust the sprite accordingly.
						fSegmentSpriteWidth = ABSF(fDistToNextNodeX) + (TAIL_THICKNESS * 0.5)
						fSegmentSpriteHeight = (TAIL_THICKNESS * minigameData.fAspectRatio)
					ELIF ABSF(fDistToNextNodeY) > 0.001
						//These two nodes connect vertically: adjust the sprite accordingly.
						fSegmentSpriteWidth = TAIL_THICKNESS
						fSegmentSpriteHeight = ABSF(fDistToNextNodeY) + (TAIL_THICKNESS * 0.5 * minigameData.fAspectRatio)
					ELSE
						//2858127 - The distance is too small to accurately confirm the direction just yet: hard-code the values to ensure nothing carries over from a previous segment.
						fSegmentSpriteWidth = 0.0001
						fSegmentSpriteHeight = 0.0001
					ENDIF
					
					FLOAT fSegmentXAdjusted = (0.5 - ((0.5 - fSegmentSpriteX) * minigameData.fAspectRatioModifier))
					FLOAT fSegmentWidthAdjusted = fSegmentSpriteWidth * minigameData.fAspectRatioModifier
					
					BOOL bSegmentIsCollidingWithHead = FALSE

					IF playerData[iLocalPlayer].iDirection = DIRECTION_LEFT OR playerData[iLocalPlayer].iDirection = DIRECTION_RIGHT
						bSegmentIsCollidingWithHead = DO_RECTANGLES_OVERLAP(fHeadXAdjusted, playerData[iLocalPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER,
																			fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted * 0.95, fSegmentSpriteHeight)
					ENDIF
					
					IF playerData[iLocalPlayer].iDirection = DIRECTION_UP OR playerData[iLocalPlayer].iDirection = DIRECTION_DOWN
						bSegmentIsCollidingWithHead = DO_RECTANGLES_OVERLAP(fHeadXAdjusted, playerData[iLocalPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER,
																			fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted, fSegmentSpriteHeight * 0.95)
					ENDIF
					
					IF bSegmentIsCollidingWithHead
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - Segment ", i, " of iPlayer: ", iPlayer, " collided with head of iLocalPlayer ", iLocalPlayer)
						CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fHeadXAdjusted =  ", fHeadXAdjusted, " playerData[iLocalPlayer].fHeadY = ", playerData[iLocalPlayer].fHeadY)
						CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fHeadWidthAdjusted =  ", fHeadWidthAdjusted, " HEAD_COLLISION_BOX_MODIFIER = ", HEAD_COLLISION_BOX_MODIFIER, " PLAYER_HEAD_HEIGHT = ", PLAYER_HEAD_HEIGHT)
						CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fSegmentXAdjusted =  ", fSegmentXAdjusted, " fSegmentSpriteY = ", fSegmentSpriteY)
						CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fSegmentWidthAdjusted =  ", fSegmentWidthAdjusted, " fSegmentSpriteHeight = ", fSegmentSpriteHeight)
						CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fDistToNextNodeX =  ", fDistToNextNodeX, " fDistToNextNodeY = ", fDistToNextNodeY)
						CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - iPlayer name =  ", LINE_ServerBD.strGamerTags[iPlayer], " iLocalPlayer name = ", LINE_ServerBD.strGamerTags[iLocalPlayer])
						
						IF bKillOnCollision
							BROADCAST_DCTL_KILL(iLocalPlayer)
						ENDIF
						
						bPlayersCollidedThisFrame = TRUE
					ELSE
						IF NOT IS_BIT_SET(iLocalLineBS, ciLineBS_ReceivedEventsThisFrame_0 + iPlayer)
						AND IS_BIT_SET(iLocalLineBS, ciLineBS_ReceivedEventsLastFrame_0 + iPlayer)
							//2848616 - If this is the first segment after the head then check if it intersects with the equivalent segment for the local player.
							//This should help in situations where two player heads miss each other due to syncing issues.
							IF i = playerData[iPlayer].iTailIndex - 1
								IF fLocalTailStartX != 0.0
								AND fLocalTailStartY != 0.0
								AND fLocalTailEndX != 0.0
								AND fLocalTailEndY != 0.0
									GET_SEGMENT_SPRITE_DIMENSIONS_FROM_NODE_DATA(minigameData, fLocalTailStartX, fLocalTailStartY, fLocalTailEndX, fLocalTailEndY,
										fLocalTailSpriteX, fLocalTailSpriteY, fLocalTailSpriteWidth, fLocalTailSpriteHeight, 0.0015)
								
									//Make the segment we're checking against a little smaller so it doesn't trigger instead of head collisions.
									/*IF playerData[iPlayer].iDirection = DIRECTION_LEFT OR playerData[iLocalPlayer].iDirection = DIRECTION_RIGHT
										fSegmentWidthAdjusted *= 0.95
										fSegmentSpriteHeight *= 0.5
									ELSE
										fSegmentWidthAdjusted *= 0.5
										fSegmentSpriteHeight *= 0.95
									ENDIF*/

									#IF IS_DEBUG_BUILD
										IF bDebugDisplayCollisionInfo
											DRAW_RECT(fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted, fSegmentSpriteHeight, 255, 120, 0, 190)
											DRAW_RECT(fLocalTailSpriteX, fLocalTailSpriteY, fLocalTailSpriteWidth, fLocalTailSpriteHeight, 255, 120, 0, 190)
										ENDIF
									#ENDIF
								
									IF DO_RECTANGLES_OVERLAP(fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted, fSegmentSpriteHeight,
															 fLocalTailSpriteX, fLocalTailSpriteY, fLocalTailSpriteWidth, fLocalTailSpriteHeight)
										CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - Segment ", i, " of iPlayer: ", iPlayer ," intersects the first tail segment of local player ", iLocalPlayer)
										CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fSegmentXAdjusted =  ", fSegmentXAdjusted, " fSegmentSpriteY = ", fSegmentSpriteY)
										CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fSegmentWidthAdjusted =  ", fSegmentWidthAdjusted, " fSegmentSpriteHeight = ", fSegmentSpriteHeight)
										CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fLocalTailSpriteX =  ", fLocalTailSpriteX, " fLocalTailSpriteY = ", fLocalTailSpriteY)
										CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fLocalTailSpriteWidth =  ", fLocalTailSpriteWidth, " fLocalTailSpriteHeight = ", fLocalTailSpriteHeight)
										CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fDistToNextNodeX =  ", fDistToNextNodeX, " fDistToNextNodeY = ", fDistToNextNodeY)
										CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - iPlayer name =  ", LINE_ServerBD.strGamerTags[iPlayer], " iLocalPlayer name = ", LINE_ServerBD.strGamerTags[iLocalPlayer])
										
										#IF IS_DEBUG_BUILD
											IF bDebugDisplayCollisionInfo
											AND NOT bKillOnCollision
												LINE_MINIGAME_DUMP_PLAYER_DATA_TO_CONSOLE(playerData, iPlayer)
											ENDIF
										#ENDIF
										
										IF bKillOnCollision
											BROADCAST_DCTL_KILL(iLocalPlayer)
										ENDIF
										
										bPlayersCollidedThisFrame = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			//Check collision between heads.
			IF NOT bPlayersCollidedThisFrame
				FLOAT fCurrentPlayerHeadXAdjusted = (0.5 - ((0.5 - playerData[iPlayer].fHeadX) * minigameData.fAspectRatioModifier))
				
				IF DO_RECTANGLES_OVERLAP(fHeadXAdjusted, playerData[iLocalPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER,
										 fCurrentPlayerHeadXAdjusted, playerData[iPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER)
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_DRAW_PLAYER - head of player ", iPlayer , " collided with head of player ", iLocalPlayer)
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fHeadXAdjusted =  ", fHeadXAdjusted, " playerData[iLocalPlayer].fHeadY = ", playerData[iLocalPlayer].fHeadY)
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fHeadWidthAdjusted =  ", fHeadWidthAdjusted, " HEAD_COLLISION_BOX_MODIFIER = ", HEAD_COLLISION_BOX_MODIFIER, " PLAYER_HEAD_HEIGHT = ", PLAYER_HEAD_HEIGHT)
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - fCurrentPlayerHeadXAdjusted =  ", fCurrentPlayerHeadXAdjusted, " playerData[iPlayer].fHeadY = ", playerData[iPlayer].fHeadY)
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] PROCESS_OTHER_PLAYER_COLLISIONS - iPlayer name =  ", LINE_ServerBD.strGamerTags[iPlayer], " iLocalPlayer name = ", LINE_ServerBD.strGamerTags[iLocalPlayer])
					
					#IF IS_DEBUG_BUILD
						IF bDebugDisplayCollisionInfo
						AND NOT bKillOnCollision
							LINE_MINIGAME_DUMP_PLAYER_DATA_TO_CONSOLE(playerData, iPlayer)
						ENDIF
					#ENDIF
					
					IF bKillOnCollision
						BROADCAST_DCTL_KILL(iLocalPlayer)
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC EXTEND_TAIL(LINE_MINIGAME_DATA &minigameData, INT iPlayer)
	//Handle extending the tail:
	//If there currently isn't a tail then begin it.
	IF minigameData.playerData[iPlayer].iTailIndex = 0
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_PLAYER_MOVEMENT - Adding first tail segment")
		minigameData.playerData[iPlayer].fTail[minigameData.playerData[iPlayer].iTailIndex] = FLOAT_COMPRESSION_PACK(minigameData.playerData[iPlayer].fHeadX, minigameData.playerData[iPlayer].fHeadY, FLOAT_COMPRESSION_DEFAULT_VALUE)
		minigameData.playerData[iPlayer].iTailIndex++
	ENDIF
	
	//If we changed direction this frame then add a new node to the tail.
	IF minigameData.playerData[iPlayer].iDirection != minigameData.playerData[iPlayer].iPrevDirection
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_PLAYER_MOVEMENT - Changed Direction this frame! Player = ", iPlayer)
		//If the array is full then remove the end of the tail and shift everything else.
		IF minigameData.playerData[iPlayer].iTailIndex >= MAX_TAIL_SEGMENTS
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_PLAYER_MOVEMENT - iTailIndex >= MAX_TAIL_SEGMENTS Player = ", iPlayer)
			INT i
			FOR i = 0 TO (MAX_TAIL_SEGMENTS - 1)
				IF i = MAX_TAIL_SEGMENTS - 1
					minigameData.playerData[iPlayer].fTail[i] = FLOAT_COMPRESSION_PACK(minigameData.playerData[iPlayer].fHeadX, minigameData.playerData[iPlayer].fHeadY, FLOAT_COMPRESSION_DEFAULT_VALUE)
				ELSE
					//Move the next segment into the current array index.
					FLOAT fSegmentCenterX = 0, fSegmentCenterY = 0
					FLOAT_COMPRESSION_UNPACK(minigameData.playerData[iPlayer].fTail[i+1], fSegmentCenterX, fSegmentCenterY, FLOAT_COMPRESSION_DEFAULT_VALUE)
					
					minigameData.playerData[iPlayer].fTail[i] = FLOAT_COMPRESSION_PACK(fSegmentCenterX, fSegmentCenterY, FLOAT_COMPRESSION_DEFAULT_VALUE)
				ENDIF
			ENDFOR
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_PLAYER_MOVEMENT - Adding another tail segment - playerData[",iPlayer,"].iTailIndex", minigameData.playerData[iPlayer].iTailIndex)
			//If there's still space in the array then add another segment node.
			minigameData.playerData[iPlayer].fTail[minigameData.playerData[iPlayer].iTailIndex] = FLOAT_COMPRESSION_PACK(minigameData.playerData[iPlayer].fHeadX, minigameData.playerData[iPlayer].fHeadY, FLOAT_COMPRESSION_DEFAULT_VALUE)
			minigameData.playerData[iPlayer].iTailIndex++
		ENDIF
		minigameData.playerData[iPlayer].iPrevDirection = minigameData.playerData[iPlayer].iDirection
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_MOVEMENT(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	//Start the movement sound if it hasn't started yet.
	//NOTE: There's a specific sound for each player, so we need to use the positional index rather than the player array index.
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
	AND playerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
		IF HAS_SOUND_FINISHED(playerData[iPlayer].iPlayerMoveSound)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - PROCESS_PLAYER_MOVEMENT - Starting move sound for player ", iPlayer)
		
			IF LINE_PlayerBD[iPlayer].iStartPos = 0
				PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerMoveSound, "Trail_1", "DLC_EXEC_ARC_MAC_SOUNDS")
			ELIF LINE_PlayerBD[iPlayer].iStartPos = 1
				PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerMoveSound, "Trail_2", "DLC_EXEC_ARC_MAC_SOUNDS")
			ELIF LINE_PlayerBD[iPlayer].iStartPos = 2
				PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerMoveSound, "Trail_3", "DLC_EXEC_ARC_MAC_SOUNDS")
			ELSE
				PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerMoveSound, "Trail_4", "DLC_EXEC_ARC_MAC_SOUNDS")
			ENDIF
		ENDIF
		
		//Update sound each frame to change based on current head position.
		IF NOT HAS_SOUND_FINISHED(playerData[iPlayer].iPlayerMoveSound)
			SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerMoveSound, "X", playerData[iPlayer].fHeadX)
			SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerMoveSound, "Y", playerData[iPlayer].fHeadY)
		ENDIF
	ENDIF

	//Update the current direction: do some additional checks first to make sure the head can't turn back on itself.
	IF playerData[iPlayer].iNextDirection != playerData[iPlayer].iDirection
	AND playerData[iPlayer].iTailIndex > 0
		FLOAT fTailCentreX, fTailCentreY, fDistanceFromTail, fDistanceThreshold
		FLOAT_COMPRESSION_UNPACK(playerData[iPlayer].fTail[playerData[iPlayer].iTailIndex - 1], fTailCentreX, fTailCentreY, FLOAT_COMPRESSION_DEFAULT_VALUE)
		
		//Check if the player is about to do a U-turn: if they are then make sure there's enough gap. If not then allow turning immediately.
		FLOAT fThresholdModifier = 0.2
		
		IF (playerData[iPlayer].iNextDirection = DIRECTION_UP AND playerData[iPlayer].iDirectionChangeHistory[1] = DIRECTION_DOWN)
		OR (playerData[iPlayer].iNextDirection = DIRECTION_DOWN AND playerData[iPlayer].iDirectionChangeHistory[1] = DIRECTION_UP)
		OR (playerData[iPlayer].iNextDirection = DIRECTION_LEFT AND playerData[iPlayer].iDirectionChangeHistory[1] = DIRECTION_RIGHT)
		OR (playerData[iPlayer].iNextDirection = DIRECTION_RIGHT AND playerData[iPlayer].iDirectionChangeHistory[1] = DIRECTION_LEFT)
			fThresholdModifier = 0.25
		ENDIF
		
		IF playerData[iPlayer].iNextDirection = DIRECTION_UP OR playerData[iPlayer].iNextDirection = DIRECTION_DOWN
			fDistanceFromTail = ABSF(playerData[iPlayer].fHeadX - fTailCentreX)
			fDistanceThreshold = PLAYER_HEAD_WIDTH * minigameData.fAspectRatio * fThresholdModifier
		ELIF playerData[iPlayer].iNextDirection = DIRECTION_LEFT OR playerData[iPlayer].iNextDirection = DIRECTION_RIGHT
			fDistanceFromTail = ABSF(playerData[iPlayer].fHeadY - fTailCentreY)
			fDistanceThreshold = (PLAYER_HEAD_WIDTH * minigameData.fAspectRatio) * (fThresholdModifier * minigameData.fAspectRatio)
		ENDIF
		
		IF fDistanceFromTail >= fDistanceThreshold
		OR (iPlayer != iLocalPlayer AND NOT IS_BIT_SET(iLocalLineBS ,ciLineBS_RecievedFirstSync_0 + iPlayer))
		
			//Broadcast that the player has turned: also handle any audio that's required at the moment of turning.
			IF iPlayer = iLocalPlayer
				CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - PROCESS_PLAYER_MOVEMENT - Local player has turned.")
				PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerTurnSound, "Turn", "DLC_EXEC_ARC_MAC_SOUNDS")
				BROADCAST_DCTL_TURN(playerData[iPlayer].iNextDirection, FLOAT_COMPRESSION_PACK(playerData[iPlayer].fHeadX, playerData[iPlayer].fHeadY, FLOAT_COMPRESSION_DEFAULT_VALUE) , iLocalPlayer, iLastTurn[iLocalPlayer])
				RESET_NET_TIMER(tdSyncEvent)
				iLastTurn[iLocalPlayer]++
			ELSE
				CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - PROCESS_PLAYER_MOVEMENT - Remote player has turned.")
				PLAY_SOUND_FRONTEND(playerData[iPlayer].iPlayerTurnSound, "Turn_NPC", "DLC_EXEC_ARC_MAC_SOUNDS")
			ENDIF
			
			SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerTurnSound, "X", playerData[iPlayer].fHeadX)
			SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerTurnSound, "Y", playerData[iPlayer].fHeadY)
			
			IF NOT HAS_SOUND_FINISHED(playerData[iPlayer].iPlayerMoveSound)
				SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerMoveSound, "Turning", 1.0)
			ENDIF
			
			playerData[iPlayer].iDirection = playerData[iPlayer].iNextDirection
			
			//Update the history.
			playerData[iPlayer].iDirectionChangeHistory[1] = playerData[iPlayer].iDirectionChangeHistory[0]
			playerData[iPlayer].iDirectionChangeHistory[0] = playerData[iPlayer].iDirection
			playerData[iPlayer].iTimeOfLastTurn = GET_GAME_TIMER()
		ENDIF
	ELSE
		//Reset the turning sound variable.
		IF NOT HAS_SOUND_FINISHED(playerData[iPlayer].iPlayerMoveSound)
			SET_VARIABLE_ON_SOUND(playerData[iPlayer].iPlayerMoveSound, "Turning", 0.0)
		ENDIF
	ENDIF
	
	EXTEND_TAIL(minigameData, iPlayer)
	
	//Update head movement and rotation based on speed and direction.
	LINE_MINIGAME_UPDATE_PLAYER_POSITION(minigameData, playerData, iPlayer)
	LINE_MINIGAME_UPDATE_PLAYER_HEAD_ROTATION(playerData, iPlayer)
	
	SET_BIT(iLocalLineBS, ciLineBS_MovementProcessed_Part_0 + iPlayer)
ENDPROC


/// PURPOSE:
///    Updates any specific events that need to occur after a player has died.
PROC LINE_MINIGAME_UPDATE_PLAYER_DEATH(LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	//Fade the player's line out.
	FLOAT fAlphaTemp = TO_FLOAT(playerData[iPlayer].iAlpha)
	fAlphaTemp = fAlphaTemp -@ PLAYER_FADE_OUT_SPEED
	
	IF fAlphaTemp < 0.0
		fAlphaTemp = 0.0
	ENDIF
	
	playerData[iPlayer].iAlpha = FLOOR(fAlphaTemp)	
ENDPROC

PROC GET_START_POSITION_DATA(INT iStartPos, INT &iDirection, FLOAT &fHeadX, FLOAT &fHeadY, INT &iPrevDirection)
	
	SWITCH(iStartPos)
		CASE 0
			fHeadX = 0.5 + PLAYER_1_START_X_FROM_CENTRE
			fHeadY = PLAYER_1_START_Y
			iDirection = DIRECTION_RIGHT
			iPrevDirection = DIRECTION_RIGHT
		BREAK
		
		CASE 1
			fHeadX = 0.5 + PLAYER_2_START_X_FROM_CENTRE
			fHeadY = PLAYER_2_START_Y
			iDirection = DIRECTION_LEFT
			iPrevDirection = DIRECTION_LEFT
		BREAK
		
		CASE 2
			fHeadX = 0.5 + PLAYER_3_START_X_FROM_CENTRE
			fHeadY = PLAYER_3_START_Y
			iDirection = DIRECTION_DOWN
			iPrevDirection = DIRECTION_DOWN
		BREAK
		
		CASE 3
			fHeadX = 0.5 + PLAYER_4_START_X_FROM_CENTRE
			fHeadY = PLAYER_4_START_Y
			iDirection = DIRECTION_UP
			iPrevDirection = DIRECTION_UP
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Returns the number of players currently alive.
FUNC INT LINE_MINIGAME_GET_NUM_PLAYERS_ALIVE(LINE_MINIGAME_DATA &minigameData)
	INT iPlayersAlive = 0
	INT i = 0
	
	FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos > -1
			IF minigameData.PlayerData[i].eCurrentState != LINE_MINIGAME_PLAYER_DEAD
				iPlayersAlive++
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		IF bDebugAllowSinglePlayerTesting
		AND iPlayersAlive != 0
			iPlayersAlive = 4
		ENDIF
	#ENDIF
	
	RETURN iPlayersAlive
ENDFUNC


/// PURPOSE:
///    Main update function for a particular player: call every frame to update their state.
PROC LINE_MINIGAME_UPDATE_PLAYER_STATE(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	SWITCH playerData[iPlayer].eCurrentState
		CASE LINE_MINIGAME_PLAYER_INITIALISING
			SWITCH(iPlayer)
				CASE 0 
					playerData[iPlayer].iTailIndex = 0
					playerData[iPlayer].iAlpha = DEFAULT_PLAYER_ALPHA
					playerData[iPlayer].iFlags = 0
					playerData[iPlayer].iNextDirection = -1
					GET_START_POSITION_DATA(LINE_PlayerBD[iPlayer].iStartPos, 
											playerData[iPlayer].iDirection, playerData[iPlayer].fHeadX, 
											playerData[iPlayer].fHeadY, playerData[iPlayer].iPrevDirection)
				BREAK
				
				CASE 1
					playerData[iPlayer].iTailIndex = 0
					playerData[iPlayer].iAlpha = DEFAULT_PLAYER_ALPHA
					playerData[iPlayer].iFlags = 0
					playerData[iPlayer].iNextDirection = -1
					GET_START_POSITION_DATA(LINE_PlayerBD[iPlayer].iStartPos, 
											playerData[iPlayer].iDirection, playerData[iPlayer].fHeadX, 
											playerData[iPlayer].fHeadY, playerData[iPlayer].iPrevDirection)
				BREAK
				
				CASE 2 
					playerData[iPlayer].iTailIndex = 0
					playerData[iPlayer].iAlpha = DEFAULT_PLAYER_ALPHA
					playerData[iPlayer].iFlags = 0
					playerData[iPlayer].iNextDirection = -1
					GET_START_POSITION_DATA(LINE_PlayerBD[iPlayer].iStartPos, 
											playerData[iPlayer].iDirection, playerData[iPlayer].fHeadX, 
											playerData[iPlayer].fHeadY, playerData[iPlayer].iPrevDirection)
				BREAK
				
				CASE 3
					playerData[iPlayer].iTailIndex = 0
					playerData[iPlayer].iAlpha = DEFAULT_PLAYER_ALPHA
					playerData[iPlayer].iFlags = 0
					playerData[iPlayer].iNextDirection = -1
					GET_START_POSITION_DATA(LINE_PlayerBD[iPlayer].iStartPos, 
											playerData[iPlayer].iDirection, playerData[iPlayer].fHeadX, 
											playerData[iPlayer].fHeadY, playerData[iPlayer].iPrevDirection)
				BREAK
			ENDSWITCH
			
			CLEAR_BIT(iLocalLineBS, ciLineBS_RecievedFirstSync_0 + iPlayer)
			
			LINE_MINIGAME_UPDATE_PLAYER_HEAD_ROTATION(playerData, iPlayer)		
			LINE_MINIGAME_RESET_TAIL_ARRAY(playerData[iPlayer].fTail)
			
			SET_LINE_MINIGAME_PLAYER_STATE(playerData, LINE_MINIGAME_PLAYER_WAITING_TO_START, iPlayer)
		BREAK
	
		CASE LINE_MINIGAME_PLAYER_WAITING_TO_START
			IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_RUNNING
				SET_LINE_MINIGAME_PLAYER_STATE(playerData, LINE_MINIGAME_PLAYER_ACTIVE, iPlayer)
			ENDIF
		BREAK
		
		CASE LINE_MINIGAME_PLAYER_ACTIVE
			//Receive input from local player.
			IF iPlayer = iLocalPlayer
				CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_PLAYER_STATE - iPlayer = iLocalPlayer")
				IF NOT IS_PAUSE_MENU_ACTIVE()
					LINE_MINIGAME_UPDATE_LOCAL_PLAYER_MOVEMENT(playerData)
				ENDIF
			ENDIF
			
			//Stop remaining players moving if we have a winner.
			IF LINE_MINIGAME_GET_NUM_PLAYERS_ALIVE(minigameData) <= 1
				playerData[iPlayer].fSpeedModifier = 0.0
				playerData[iPlayer].iNextDirection = playerData[iPlayer].iDirection
			ENDIF
			
			//Set the current position for all players.
			CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_PLAYER_STATE - Updating position")
			IF NOT IS_BIT_SET(iLocalLineBS, ciLineBS_MovementProcessed_Part_0 + iPlayer)
				PROCESS_PLAYER_MOVEMENT(minigameData, playerData, iPlayer)
			ENDIF
			CLEAR_BIT(iLocalLineBS, ciLineBS_MovementProcessed_Part_0 + iPlayer)
		
			IF iPlayer = iLocalPlayer
				//Process collisions for local player.
				PROCESS_OTHER_PLAYER_COLLISIONS(minigameData, playerData)
				IF HAS_PLAYER_COLLIDED_WITH_SIDE_WALLS(minigameData, playerData, iLocalPlayer)
					BROADCAST_DCTL_KILL(iLocalPlayer)
				ENDIF
			ELSE
				//If a remote player hits the wall then stop them moving for this frame: this prevents issues where disconnected
				//players will appear to move through the walls on other player's screens.
				//The modifier is reset in PROCESS_PLAYER_MOVEMENT
				IF HAS_PLAYER_COLLIDED_WITH_SIDE_WALLS(minigameData, playerData, iPlayer)
					playerData[iPlayer].fSpeedModifier = 0.0
					IF playerData[iPlayer].iTimeColliding = -HIGHEST_INT
						playerData[iPlayer].iTimeColliding = NATIVE_TO_INT(GET_NETWORK_TIME())
					ENDIF
				ELIF playerData[iPlayer].iTimeColliding != -HIGHEST_INT
					playerData[iPlayer].iTimeColliding = -HIGHEST_INT
				ENDIF
			ENDIF
		BREAK
		
		CASE LINE_MINIGAME_PLAYER_DEAD
			LINE_MINIGAME_UPDATE_PLAYER_DEATH(playerData, iPlayer)
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Resets the local game state ready to start a new game, call after a retry or game start has been confirmed.
PROC LINE_MINIGAME_RESET_GAME_STATE(LINE_MINIGAME_DATA &minigameData)
	RESET_NET_TIMER(minigameData.startConfirmedTimer)
	RESET_NET_TIMER(minigameData.readyTimer)
	RESET_NET_TIMER(minigameData.countdownTimer)
	RESET_NET_TIMER(minigameData.goTimer)
	RESET_NET_TIMER(minigameData.winnerScreenTimer)
	RESET_NET_TIMER(minigameData.replayScreenTimer)
	RESET_NET_TIMER(minigameData.replayConfirmedTimer)
	RESET_NET_TIMER(minigameData.gameOverScreenTimer)
	RESET_NET_TIMER(minigameData.winnerDeterminedTimer)
	
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_1)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_2)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_3)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_MAIN_MUSIC)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_1)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_2)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_3)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_GO)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_GAME_OVER)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_READY)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_WINNER)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOADING)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_SFX_READY)
	
	INT iPlayer
	FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
		//Reset any player data here that can be reset regardless of participation.
		IF iPlayer < MAX_NUM_PLAYERS
			minigameData.PlayerData[iPlayer].iFlags = 0
			minigameData.PlayerData[iPlayer].fSpeedModifier = 1.0
		ENDIF
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			SET_LINE_MINIGAME_PLAYER_STATE(minigameData.PlayerData, LINE_MINIGAME_PLAYER_INITIALISING, iPlayer)
		ELSE
			LINE_MINIGAME_RESET_TAIL_ARRAY(minigameData.PlayerData[iPlayer].fTail)
			SET_LINE_MINIGAME_PLAYER_STATE(minigameData.PlayerData, LINE_MINIGAME_PLAYER_DEAD, iPlayer)
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Stores all the info required for displaying HUD elements related to the players (e.g. gamertags).
PROC LINE_MINIGAME_STORE_PLAYERS_DISPLAY_INFO()
	//Store all the info required for the HUD.
	INT i
	FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		//Reset the data in case they're no longer in the game.
		IF i < MAX_NUM_PLAYERS
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				LINE_ServerBD.strGamerTags[i] = ""
			ENDIF
			strHeadshotTextures[i] = NULL_STRING()
		ENDIF
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos > -1
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//Assign the gamertags.
				LINE_ServerBD.strGamerTags[i] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
			ENDIF
			//Assign the textures.
			PEDHEADSHOT_ID playerHeadshot = Get_HeadshotID_For_Player(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
			
			IF playerHeadshot != NULL
				strHeadshotTextures[i] = GET_PEDHEADSHOT_TXD_STRING(playerHeadshot)
			ENDIF
			
			CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_STORE_PLAYERS_DISPLAY_INFO - Storing player array index ", i, " with gamertag ",
					LINE_ServerBD.strGamerTags[i], " and texture ", strHeadshotTextures[i])
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Call every frame to draw the minigame background.
PROC LINE_MINIGAME_DRAW_BACKGROUND(LINE_MINIGAME_DATA &minigameData)
	INT iBackgroundAlpha = 255

	#IF IS_DEBUG_BUILD
		IF bDebugHideMinigameUI
			iBackgroundAlpha = 0
		ENDIF
	#ENDIF

	//Draw a black background to potentially cover any gaps in the custom interface.
	DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, iBackgroundAlpha)
	
	//Draw the static background.
	DRAW_SPRITE("LineArcadeMinigame", "Degenatron_DontCrossTheLine_Game", 0.5, 0.5, 1.0 * minigameData.fAspectRatioModifier, 1.0, 0.0, 255, 255, 255, iBackgroundAlpha)
	#IF IS_DEBUG_BUILD
		iDrawnSprites[4] ++
	#ENDIF
ENDPROC

/// PURPOSE:
///    Call every frame to draw the minigame title screen.
PROC LINE_MINIGAME_DRAW_TITLE_SCREEN(LINE_MINIGAME_DATA &minigameData)	
	DRAW_SPRITE("LineArcadeMinigame", "Degenatron_DontCrossTheLine_Home", 0.5, 0.5, 1.0 * minigameData.fAspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
	
	#IF IS_DEBUG_BUILD
		iDrawnSprites[4] ++
	#ENDIF
ENDPROC

/// PURPOSE:
///    Draws the scores for each player.
PROC LINE_MINIGAME_DRAW_SCORES(LINE_MINIGAME_DATA &minigameData)
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
		LINE_MINIGAME_UI_SHOW_HUD(minigameData)
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
	ENDIF
	
	LINE_MINIGAME_UI_SET_MICS(minigameData)
ENDPROC

/// PURPOSE:
///    Draws the title screen, which also indicates which players have joined the game.
PROC LINE_MINIGAME_DRAW_TITLE_LOBBY(LINE_MINIGAME_DATA &minigameData)
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
	ENDIF
	
	LINE_MINIGAME_UI_INIT_LOBBY(minigameData, "DCTL_INSERT")
	LINE_MINIGAME_UI_UPDATE_LOBBY(minigameData)
	LINE_MINIGAME_UI_SET_MICS(minigameData)
ENDPROC

/// PURPOSE:
///    Draws "Loading..." text on the screen
PROC LINE_MINIGAME_DRAW_LOADING_TEXT(LINE_MINIGAME_DATA &minigameData)
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOADING)
		LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_LOADING")
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOADING)
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws "Ready?" confirmation on the screen
PROC LINE_MINIGAME_DRAW_READY_TEXT(LINE_MINIGAME_DATA &minigameData)
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_READY)
		LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_READY")
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_READY)
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws countdown on screen.
PROC LINE_MINIGAME_DRAW_COUNTDOWN_TEXT(LINE_MINIGAME_DATA &minigameData)
	IF NOT HAS_NET_TIMER_EXPIRED(minigameData.countdownTimer, 1000)
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_3)
			LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_COUNTDOWN3", DEFAULT, LINE_MINIGAME_UI_MESSAGE_EFFECT_NONE)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_3)
		ENDIF
	ELIF NOT HAS_NET_TIMER_EXPIRED(minigameData.countdownTimer, 2000)
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_2)
			LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_COUNTDOWN2", DEFAULT, LINE_MINIGAME_UI_MESSAGE_EFFECT_NONE)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_2)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_1)
			LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_COUNTDOWN1", DEFAULT, LINE_MINIGAME_UI_MESSAGE_EFFECT_NONE)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_1)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws "Go!" on the screen.
PROC LINE_MINIGAME_DRAW_GO_TEXT(LINE_MINIGAME_DATA &minigameData)
	IF NOT HAS_NET_TIMER_EXPIRED(minigameData.goTimer, 1000)
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_GO)
			LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_COUNTDOWNGO", DEFAULT, LINE_MINIGAME_UI_MESSAGE_EFFECT_NONE)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_GO)
		ENDIF
	ELSE
		IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_GO)
			LINE_MINIGAME_UI_CLEAR_CENTRAL_MESSAGE(minigameData)
			CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_COUNTDOWN_GO)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws text indicating the person who won the game.
PROC LINE_MINIGAME_DRAW_WINNER_TEXT(LINE_MINIGAME_DATA &minigameData)
	IF LINE_ServerBD.iWinningPlayer != -1
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_WINNER)
			TEXT_LABEL_63 strGamerTag = LINE_MINIGAME_GET_GAMERTAG_FOR_PLAYER_POSITION(LINE_PlayerBD[LINE_ServerBD.iWinningPlayer].iStartPos)
			HUD_COLOURS ePlayerColour = GET_HUD_COLOUR_FOR_PLAYER(LINE_PlayerBD[LINE_ServerBD.iWinningPlayer].iStartPos)
			LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE_WITH_PLAYER_NAME(minigameData, "DCTL_WINNERV2", strGamerTag, DEFAULT, 
																  LINE_MINIGAME_UI_MESSAGE_EFFECT_FLASH, ePlayerColour)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_WINNER)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_WINNER)
			LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_DRAW", DEFAULT, LINE_MINIGAME_UI_MESSAGE_EFFECT_FLASH)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_WINNER)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws the replay display, showing which players have chosen to play again.
PROC LINE_MINIGAME_DRAW_REPLAY_LOBBY(LINE_MINIGAME_DATA &minigameData)
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
		LINE_MINIGAME_UI_INIT_LOBBY(minigameData, "DCTL_REPLAY")
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
	ENDIF
	
	LINE_MINIGAME_UI_UPDATE_LOBBY(minigameData)
	LINE_MINIGAME_UI_SET_MICS(minigameData)
ENDPROC

/// PURPOSE:
///    Draws text indicating that the game has finished.
PROC LINE_MINIGAME_DRAW_GAME_OVER_TEXT(LINE_MINIGAME_DATA &minigameData)
	//Scaleform version.
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_GAME_OVER)
		LINE_MINIGAME_UI_SET_CENTRAL_MESSAGE(minigameData, "DCTL_GAMEOVER", DEFAULT, LINE_MINIGAME_UI_MESSAGE_EFFECT_FLASH)
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_GAME_OVER)
	ENDIF
ENDPROC





/// PURPOSE:
///    Call every frame to draw the player's path on the screen. NOTE: This procedure also handles collisions between the player
///    and their own tail to save on execution time.
PROC LINE_MINIGAME_DRAW_PLAYER(LINE_MINIGAME_DATA &minigameData, LINE_MINIGAME_PLAYER_DATA &playerData[], INT iPlayer)
	//Precalculate some values used in multiple parts of the drawing stage.
	
	IF playerData[iPlayer].iTimeColliding != -HIGHEST_INT
	AND NATIVE_TO_INT(GET_NETWORK_TIME()) > playerData[iPlayer].iTimeColliding + 1000
		CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_DRAW_PLAYER - Not drawing player (likely DCed or suspended): ", iPlayer)
		EXIT
	ENDIF
	
	INT iR, iG, iB
	
	GET_COLOUR_FOR_PLAYER(LINE_PlayerBD[iPlayer].iStartPos, iR, iG, iB)
	
	IF playerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_DEAD
		iR = 255
		iG = 255
		iB = 255
	ENDIF
	
	FLOAT fHeadXAdjusted = (0.5 - ((0.5 - playerData[iPlayer].fHeadX) * minigameData.fAspectRatioModifier))
	FLOAT fHeadWidthAdjusted = PLAYER_HEAD_WIDTH * minigameData.fAspectRatioModifier
	
	//Draw each tail segment in order.
	INT i = 0
	INT iSegmentDirection
	FLOAT fCurrentNodeX, fCurrentNodeY, fNextNodeX, fNextNodeY, fDistToNextNodeX, fDistToNextNodeY 
	FLOAT fSegmentSpriteX, fSegmentSpriteY, fSegmentSpriteRotation, fSegmentSpriteWidth, fSegmentSpriteHeight
	
	REPEAT MAX_TAIL_SEGMENTS i
		FLOAT_COMPRESSION_UNPACK(playerData[iPlayer].fTail[i], fCurrentNodeX, fCurrentNodeY, FLOAT_COMPRESSION_DEFAULT_VALUE)
		
		IF fCurrentNodeX != 0.0 OR fCurrentNodeY != 0.0
			IF i + 1 >= MAX_TAIL_SEGMENTS
				//This is the last segment in the array, so this segment needs to attach to the head.
				fNextNodeX = playerData[iPlayer].fHeadX
				fNextNodeY = playerData[iPlayer].fHeadY
			ELSE
				FLOAT_COMPRESSION_UNPACK(playerData[iPlayer].fTail[i+1], fNextNodeX, fNextNodeY, FLOAT_COMPRESSION_DEFAULT_VALUE)
				
				IF fNextNodeX = 0.0 AND fNextNodeY = 0.0
					//The next segment is invalid, so this segment needs to attach to the head.
					fNextNodeX = playerData[iPlayer].fHeadX
					fNextNodeY = playerData[iPlayer].fHeadY
				ENDIF
			ENDIF
			
			//The tail array stores end-to-end node positions, so the sprite needs to be drawn in the middle of two nodes.
			fDistToNextNodeX = fNextNodeX - fCurrentNodeX
			fDistToNextNodeY = fNextNodeY - fCurrentNodeY
			fSegmentSpriteX = fCurrentNodeX + (fDistToNextNodeX * 0.5)
			fSegmentSpriteY = fCurrentNodeY + (fDistToNextNodeY * 0.5)
			
			//These two nodes connect horizontally: adjust the sprite accordingly.
			IF ABSF(fDistToNextNodeX) > 0.001
				IF fDistToNextNodeX > 0.0
					iSegmentDirection = DIRECTION_RIGHT
				ELSE
					iSegmentDirection = DIRECTION_LEFT
				ENDIF
			
				fSegmentSpriteRotation = 90.0
				fSegmentSpriteWidth = ABSF(fDistToNextNodeX) + TAIL_THICKNESS //(TAIL_THICKNESS * 0.5)
				fSegmentSpriteHeight = (TAIL_THICKNESS * minigameData.fAspectRatio)
			ENDIF
			
			//These two nodes connect vertically: adjust the sprite accordingly.
			IF ABSF(fDistToNextNodeY) > 0.001
				IF fDistToNextNodeY > 0.0
					iSegmentDirection = DIRECTION_DOWN
				ELSE
					iSegmentDirection = DIRECTION_UP
				ENDIF
			
				fSegmentSpriteRotation = 0.0
				fSegmentSpriteWidth = TAIL_THICKNESS
				fSegmentSpriteHeight = ABSF(fDistToNextNodeY) + (TAIL_THICKNESS * 0.5 * minigameData.fAspectRatio)
			ENDIF
			
			FLOAT fSegmentXAdjusted = (0.5 - ((0.5 - fSegmentSpriteX) * minigameData.fAspectRatioModifier))
			FLOAT fSegmentWidthAdjusted = fSegmentSpriteWidth * minigameData.fAspectRatioModifier
			
//			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_DRAW_PLAYER - iDirection = ",playerData[iPlayer].iDirection ," fSegmentXAdjusted= ",fSegmentXAdjusted, 
//										" fSegmentSpriteY= ", fSegmentSpriteY, " fSegmentSpriteHeight= ", fSegmentSpriteHeight,
//										" fSegmentSpriteRotation= ", fSegmentSpriteRotation)
			
			DRAW_SPRITE("LineArcadeMinigame", "Tail", fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted, fSegmentSpriteHeight, 
				fSegmentSpriteRotation, iR, iG, iB, playerData[iPlayer].iAlpha)
			#IF IS_DEBUG_BUILD
			iDrawnSprites[iPlayer] ++
			#ENDIF
			IF iPlayer = iLocalPlayer
				//Handle collisions with the head here: most of the data required for handling collisions has already been calculated.
				IF playerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
					BOOL bSegmentIsCollidingWithHead = FALSE
					IF playerData[iPlayer].iDirection = DIRECTION_LEFT OR playerData[iPlayer].iDirection = DIRECTION_RIGHT
						bSegmentIsCollidingWithHead = DO_RECTANGLES_OVERLAP(fHeadXAdjusted, playerData[iPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER,
																			fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted * 0.95, fSegmentSpriteHeight)
						
						//DEBUG - Draw the collision box for this segment.
						#IF IS_DEBUG_BUILD
							IF bDebugDisplayCollisionInfo
								DRAW_RECT(fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted * 0.95, fSegmentSpriteHeight, 0, 0, 0, 160)
							ENDIF
						#ENDIF
					ENDIF
					
					IF playerData[iPlayer].iDirection = DIRECTION_UP OR playerData[iPlayer].iDirection = DIRECTION_DOWN
						bSegmentIsCollidingWithHead = DO_RECTANGLES_OVERLAP(fHeadXAdjusted, playerData[iPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER,
																			fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted, fSegmentSpriteHeight * 0.95)
						
						//DEBUG - Draw the collision box for this segment.													
						#IF IS_DEBUG_BUILD
							IF bDebugDisplayCollisionInfo
								DRAW_RECT(fSegmentXAdjusted, fSegmentSpriteY, fSegmentWidthAdjusted, fSegmentSpriteHeight * 0.95, 0, 0, 0, 160)
							ENDIF
						#ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF bDebugDisplayCollisionInfo
							//Draw head collision box.
							DRAW_RECT(fHeadXAdjusted, playerData[iPlayer].fHeadY, fHeadWidthAdjusted * HEAD_COLLISION_BOX_MODIFIER, PLAYER_HEAD_HEIGHT * HEAD_COLLISION_BOX_MODIFIER, 255, 255, 255, 160)
							
							//Mark the segments that won't be included in the final collision check below.
							IF i >= playerData[iPlayer].iTailIndex - 2
								DRAW_RECT(fSegmentXAdjusted, fSegmentSpriteY, 0.01, 0.01, 255, 255, 255, 160)
							ENDIF
						ENDIF
					#ENDIF
					
					IF bSegmentIsCollidingWithHead	
						BOOl bPlayerIsZigZagging = FALSE
						
						//Special case: if the player is zig-zagging quickly then make sure they don't collide with the third segment
						//back if it was in the same direction as their current one.
						IF i = playerData[iPlayer].iTailIndex - 3
							IF iSegmentDirection = playerData[iPlayer].iDirection
								#IF IS_DEBUG_BUILD
									IF bDebugDisplayCollisionInfo
										DRAW_RECT(fSegmentXAdjusted, fSegmentSpriteY, 0.02, 0.02, 0, 0, 255, 160)
									ENDIF
								#ENDIF
							
								bPlayerIsZigZagging = TRUE
							ENDIF
						ENDIF
					
						//Stop the player from instantly colliding with the newest tail segments.
						IF i < playerData[iPlayer].iTailIndex - 2
						AND NOT bPlayerIsZigZagging
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_DRAW_PLAYER - Segment ", i, " collided with head.")
							BROADCAST_DCTL_KILL(iPlayer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//Clear these to stop drawing issues
			fSegmentSpriteRotation = 0
			fSegmentSpriteWidth = 0
			fSegmentSpriteHeight = 0
		ENDIF
	ENDREPEAT	
	
	//Draw the head.
	DRAW_SPRITE("LineArcadeMinigame", "HeadPixel", fHeadXAdjusted, playerData[iPlayer].fHeadY, fHeadWidthAdjusted, PLAYER_HEAD_HEIGHT, 
				playerData[iPlayer].fHeadRotation, iR, iG, iB, playerData[iPlayer].iAlpha)
	#IF IS_DEBUG_BUILD
	iDrawnSprites[iPlayer] ++
	#ENDIF
ENDPROC

/// PURPOSE:
///    Call each frame to draw all the UI elements of the minigame. 
PROC DRAW_LINE_MINIGAME(LINE_MINIGAME_DATA &minigameData)
	//NOTE: The drawing functions are designed so that the game maintains the same proportion regardless of screen resolution.
	//This works as follows:
	//- The heights of sprites are scaled to fit a 0.0-1.0 screen as normal.
	//- The widths of sprites are adjusted so that they stay in proportion based on the current aspect ratio.
	//This method means that the sides of the game will be cut off in 4:3 resolutions (instead of the game being squashed). It also
	//means that X-positions cannot be calculated from the left of the screen as we no longer know how much of the game is visible,
	//instead they need to be calculated from the centre of the screen.

	LINE_MINIGAME_DRAW_BACKGROUND(minigameData)
	
	//Draw the player.
	INT iPlayer
	FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
		AND LINE_PlayerBD[iPlayer].iStartPos > -1
			IF minigameData.PlayerData[iPlayer].iAlpha != 0
				CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - DRAW_LINE_MINIGAME - Drawing player: ", iPlayer ," alpha = ", minigameData.PlayerData[iPlayer].iAlpha)
				LINE_MINIGAME_DRAW_PLAYER(minigameData, minigameData.PlayerData, iPlayer)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Plays the countdown sounds for the local player.
PROC LINE_MINIGAME_UPDATE_COUNTDOWN_SOUNDS(LINE_MINIGAME_DATA &minigameData)
	IF NOT HAS_NET_TIMER_EXPIRED(minigameData.countdownTimer, 1000)
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_3)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_COUNTDOWN_SOUNDS - Triggering '3' sound.")
			PLAY_SOUND_FRONTEND(-1, "321", "DLC_EXEC_ARC_MAC_SOUNDS")
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_3)
		ENDIF
	ELIF NOT HAS_NET_TIMER_EXPIRED(minigameData.countdownTimer, 2000)
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_2)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_COUNTDOWN_SOUNDS - Triggering '2' sound.")
			PLAY_SOUND_FRONTEND(-1, "321", "DLC_EXEC_ARC_MAC_SOUNDS")
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_2)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_1)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_COUNTDOWN_SOUNDS - Triggering '1' sound.")
			PLAY_SOUND_FRONTEND(-1, "321", "DLC_EXEC_ARC_MAC_SOUNDS")
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_COUNTDOWN_SOUND_1)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks when players have selected an option on the title screen and plays the relevant sound. For the local player
///    this is as soon as they press the button, for remote players this will be when the local player receives broadcast
///    data indicating they've chosen an option.
PROC LINE_MINIGAME_UPDATE_TITLE_SCREEN_SOUNDS(LINE_MINIGAME_DATA &minigameData)
	INT i = 0
	REPEAT MAX_NUM_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos > -1
			IF IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
			AND NOT IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_CONFIRM)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_TITLE_SCREEN_SOUNDS - Playing confirm sound for player ", i)
				PLAY_SOUND_FRONTEND(minigameData.iInputSoundID, "Insert_Coin", "DLC_EXEC_ARC_MAC_SOUNDS")
				SET_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_CONFIRM)
				IF IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_REJECT)
					CLEAR_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_REJECT)
				ENDIF
			ELIF IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
			AND NOT IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_REJECT)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_TITLE_SCREEN_SOUNDS - Playing reject sound for player ", i)
				PLAY_SOUND_FRONTEND(minigameData.iInputSoundID, "Cancel", "DLC_EXEC_ARC_MAC_SOUNDS")
				SET_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_REJECT)
				IF IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_CONFIRM)
					CLEAR_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_TITLE_SOUND_CONFIRM)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks when players have selected an option on the replay screen and plays the relevant sound. For the local player
///    this is as soon as they press the button, for remote players this will be when the local player receives broadcast
///    data indicating they've chosen an option.
PROC LINE_MINIGAME_UPDATE_REPLAY_SCREEN_SOUNDS(LINE_MINIGAME_DATA &minigameData)
	INT i = 0
	REPEAT MAX_NUM_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND LINE_PlayerBD[i].iStartPos > -1
			IF IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
			AND NOT IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_CONFIRM)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_REPLAY_SCREEN_SOUNDS - Playing confirm sound for player ", i)
				PLAY_SOUND_FRONTEND(minigameData.iInputSoundID, "Insert_Coin", "DLC_EXEC_ARC_MAC_SOUNDS")
				SET_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_CONFIRM)
				IF IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_REJECT)
					CLEAR_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_REJECT)
				ENDIF
			ELIF IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
			AND NOT IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_REJECT)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_REPLAY_SCREEN_SOUNDS - Playing reject sound for player ", i)
				PLAY_SOUND_FRONTEND(minigameData.iInputSoundID, "Cancel", "DLC_EXEC_ARC_MAC_SOUNDS")
				SET_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_REJECT)
				IF IS_BIT_SET(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_CONFIRM)
					CLEAR_BIT(minigameData.PlayerData[i].iFlags, LINE_MINIGAME_PLAYER_FLAGS_TRIGGERED_REPLAY_SOUND_CONFIRM)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Updates the music that plays during the main portion of the game: music increases in intensity the more players that have
///    died. Calling this before the main section will preload the music in advance.
PROC LINE_MINIGAME_UPDATE_MAIN_MUSIC(LINE_MINIGAME_DATA &minigameData)
	//Check how many players have died.
	INT iPlayer
	INT iNumPlayersStillAlive = 0
	
	FOR iPlayer = 0 TO MAX_NUM_PLAYERS - 1
		IF minigameData.PlayerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
			iNumPlayersStillAlive++
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		IF bDebugAllowSinglePlayerTesting
		AND iNumPlayersStillAlive > 0
			iNumPlayersStillAlive = 4
		ENDIF
	#ENDIF
	
	IF minigameData.eCurrentState < LINE_MINIGAME_RUNNING
		LOAD_STREAM("Music_Stream", "DLC_EXEC_ARC_MAC_SOUNDS")
	ELSE
		IF iNumPlayersStillAlive > 1		
			IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_MAIN_MUSIC)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_MAIN_MUSIC - Starting main music")
				PLAY_STREAM_FRONTEND()
				SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_MAIN_MUSIC)
			ENDIF
		ELSE
			//As soon as the final opponent is shown crashing locally stop the music.
			IF IS_STREAM_PLAYING()
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_MAIN_MUSIC - Stopping main music")
				STOP_STREAM()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_EXIST_WITH_THIS_START_POSITION(INT iStartPos, INT &iPlayer)
	FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
		AND LINE_PlayerBD[iPlayer].iStartPos > -1
			IF LINE_PlayerBD[iPlayer].iStartPos = iStartPos
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC LINE_MINIGAME_INITIALISE_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF iLocalPlayer = -1
		g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
		
		minigameData.fAspectRatio = GET_CUSTOM_SCREEN_ASPECT_RATIO()
		
		//Initialise audio IDs.
		minigameData.iMusicTitleScreen = GET_SOUND_ID()
		minigameData.iInputSoundID = GET_SOUND_ID()
		minigameData.iGameOverSoundID = GET_SOUND_ID()
		
		INT i
		FOR i = 0 TO MAX_NUM_PLAYERS -1
			minigameData.PlayerData[i].iPlayerMoveSound = GET_SOUND_ID()
			minigameData.PlayerData[i].iPlayerCrashSound = GET_SOUND_ID()
			minigameData.PlayerData[i].iPlayerTurnSound = GET_SOUND_ID()
		ENDFOR
		
		//Prevent notifications from appearing during the minigame.
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
		
		iLocalPlayer = PARTICIPANT_ID_TO_INT()
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_INITIALISE - iLocalPlayer = ", iLocalPlayer)
		LINE_PlayerBD[iLocalPlayer].iStartPos = -1
		minigameData.siMinigame = NULL
		SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(ciPBD_NOT_CONFIRMED)
		SET_LINE_MINIGAME_SPLASH_STATE(minigameData, LINE_MINIGAME_SPLASH_START)
	ENDIF
	
	IF LINE_PlayerBD[iLocalPlayer].iStartPos = -1
		IF LINE_ServerBD.iNewStartPositions[iLocalPlayer] > -1
			LINE_PlayerBD[iLocalPlayer].iStartPos = LINE_ServerBD.iNewStartPositions[iLocalPlayer]
		ENDIF
	ELSE
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_REQUESTING_ASSETS)
	ENDIF
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_INITIALISE - LINE_PlayerBD[iLocalPlayer].iStartPos = ", LINE_PlayerBD[iLocalPlayer].iStartPos)
	
	DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
ENDPROC


/// PURPOSE:
///    Initialises all data required to start the minigame.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_INITIALISE(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT iPlayer
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				IF LINE_PlayerBD[iPlayer].iStartPos = -1
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_INITIALISE - Active player: ", iPlayer, " does not have a start position. Waiting")
					EXIT
				ENDIF
			ENDIF
		ENDFOR
		
		SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_REQUESTING_ASSETS)
	ENDIF
ENDPROC


PROC LINE_MINIGAME_REQUEST_ASSETS_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_ASSETS_LOADED)
		
		LINE_MINIGAME_REQUEST_ALL_ASSETS()
		
		minigameData.siMinigame = REQUEST_SCALEFORM_MOVIE("dont_cross_the_line")
			
		IF LINE_MINIGAME_HAVE_ALL_ASSETS_LOADED()
		AND HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
			SET_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_ASSETS_LOADED)
			SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_LAUNCHING)
		ENDIF
	ENDIF
ENDPROC
	

/// PURPOSE:
///    Requests all assets required for the minigame.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_REQUEST_ASSETS(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		BOOL bAllLoaded = TRUE
		INT iPlayer
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND LINE_PlayerBD[iPlayer].iStartPos > -1
				IF NOT IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_ASSETS_LOADED)
					bAllLoaded = FALSE
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_REQUEST_ASSETS - PLAYER: ", iPlayer ," has not loaded")
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
		IF bAllLoaded
			SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_LAUNCHING)
		ENDIF
	ENDIF
ENDPROC

PROC LINE_MINIGAME_LAUNCH_CLIENT(LINE_MINIGAME_DATA &minigameData)
	//NOTE: This stage could be used for a custom startup screen, currently it just turns off player control and progresses.
	IF NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_READY_TO_LAUNCH)
		CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_LAUNCH_CLIENT - Local player ", iLocalPlayer ," is ready to launch.")
	
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			DISABLE_INTERACTION_MENU() 
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
		
		//Turn on blinders for multihead displays.
		SET_MULTIHEAD_SAFE(TRUE, FALSE)
		
		START_AUDIO_SCENE("DLC_Exec_Arc_Mac_Playing_Game_Scene")
		
		//Start the Scaleform splash screen.
		LINE_MINIGAME_UI_SHOW_LOADING_SCREEN(minigameData)
		START_NET_TIMER(minigamedata.splashScreenTimer)
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SPLASH)
		
		#IF IS_DEBUG_BUILD
			iDebugSplashTimer = GET_GAME_TIMER()
		#ENDIF
		
		SET_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_READY_TO_LAUNCH)
	ELSE
		#IF IS_DEBUG_BUILD
			IF bDebugEnableSplashTimer
				TEXT_LABEL strTimer = GET_GAME_TIMER() - iDebugSplashTimer
				DISPLAY_TEXT_WITH_PLAYER_NAME(0.5, 0.5, "STRING", strTimer, HUD_COLOUR_PURE_WHITE)
			ENDIF
		#ENDIF
		
		//Update the splash screen.
		SWITCH minigameData.eSplashState
			CASE LINE_MINIGAME_SPLASH_START
				PLAY_SOUND_FRONTEND(-1, "Degenatron_Logo", "DLC_EXEC_ARC_MAC_SOUNDS")
				SET_LINE_MINIGAME_SPLASH_STATE(minigameData, LINE_MINIGAME_SPLASH_DEGEN_LOGO)
			BREAK
		
			CASE LINE_MINIGAME_SPLASH_DEGEN_LOGO
				IF HAS_NET_TIMER_EXPIRED(minigamedata.splashScreenTimer, 1800)
					PLAY_SOUND_FRONTEND(-1, "Degenatron_Star", "DLC_EXEC_ARC_MAC_SOUNDS")
					SET_LINE_MINIGAME_SPLASH_STATE(minigameData, LINE_MINIGAME_SPLASH_TWINKLE)
				ENDIF
			BREAK
			
			CASE LINE_MINIGAME_SPLASH_TWINKLE
				IF HAS_NET_TIMER_EXPIRED(minigamedata.splashScreenTimer, 2250)
					//Start the title screen music.
					IF HAS_SOUND_FINISHED(minigameData.iMusicTitleScreen)
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_LAUNCH_CLIENT - Starting title music")
						PLAY_SOUND_FRONTEND(minigameData.iMusicTitleScreen, "Background", "DLC_EXEC_ARC_MAC_SOUNDS")
					ENDIF
					
					SET_LINE_MINIGAME_SPLASH_STATE(minigameData, LINE_MINIGAME_SPLASH_TITLES)
				ENDIF
			BREAK
		ENDSWITCH
	
		//Draw the title screen once the Degenatron logo has displayed.
		IF HAS_NET_TIMER_EXPIRED(minigamedata.splashScreenTimer, 2200)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_DRAW_TITLES_THIS_FRAME)
		ENDIF
	
		//Once the splash screen finishes progress if save to do so.
		IF HAS_NET_TIMER_EXPIRED(minigamedata.splashScreenTimer, 6000)
		OR (minigameData.eSplashState = LINE_MINIGAME_SPLASH_TITLES 
		AND HAS_NET_TIMER_EXPIRED(minigamedata.splashScreenTimer, 3000)
		AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			SET_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_FINISHED_INTRO)
			LINE_MINIGAME_STORE_PLAYERS_DISPLAY_INFO()
			IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_REPLAY_SCREEN
				SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_REPLAY_SCREEN)
			ELIF LINE_ServerBD.eCurrentState >= LINE_MINIGAME_TITLE_SCREEN
				SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_TITLE_SCREEN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the initial launch: removes player control and displays any launch UI.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_LAUNCH(LINE_MINIGAME_DATA &minigameData)
	//NOTE: This stage could be used for a custom startup screen, currently it just turns off player control and progresses.
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		BOOL bAllReady = TRUE
		INT iPlayer
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND LINE_PlayerBD[iPlayer].iStartPos > -1
				IF NOT IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_READY_TO_LAUNCH)
					bAllReady = FALSE
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
		IF bAllReady
			SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_TITLE_SCREEN)
		ENDIF
	ENDIF
ENDPROC

PROC LINE_MINIGAME_UPDATE_GAMERTAGS(LINE_MINIGAME_DATA &minigameData, BOOL bReplayScreen = FALSE)
	IF LINE_ServerBD.iNewStartPositions[iLocalPlayer] != LINE_PlayerBD[iLocalPlayer].iStartPos
		LINE_PlayerBD[iLocalPlayer].iStartPos = LINE_ServerBD.iNewStartPositions[iLocalPlayer]
	ENDIF
	
	INT iPlayer
	IF NOT IS_BIT_SET(iLocalLineBS, ciLineBS_UpdateGamertags)
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				IF LINE_PlayerBD[iPlayer].iStartPos > -1
				AND (IS_STRING_NULL_OR_EMPTY(LINE_ServerBD.strGamerTags[iPlayer])
					OR IS_STRING_NULL_OR_EMPTY(strHeadshotTextures[iPlayer]))
					SET_BIT(iLocalLineBS, ciLineBS_UpdateGamertags)
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - We need to update headshots and gamertags")
				ELSE
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - Player ", iPlayer ," is active. Start Pos = ", LINE_PlayerBD[iPlayer].iStartPos," Gamertag = ", LINE_ServerBD.strGamerTags[iPlayer]," Headshot = ", strHeadshotTextures[iPlayer])
				ENDIF
			ELSE
				IF NOT (IS_STRING_NULL_OR_EMPTY(LINE_ServerBD.strGamerTags[iPlayer])
					OR IS_STRING_NULL_OR_EMPTY(strHeadshotTextures[iPlayer]))
					SET_BIT(iLocalLineBS, ciLineBS_UpdateGamertags)
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - We need to update headshots and gamertags")
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	IF IS_BIT_SET(iLocalLineBS, ciLineBS_UpdateGamertags)
		LINE_MINIGAME_STORE_PLAYERS_DISPLAY_INFO()
		CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - Checking if we can update this frame")
		
		BOOL bUpdated = TRUE
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				IF LINE_PlayerBD[iPlayer].iStartPos = -1
				OR IS_STRING_NULL_OR_EMPTY(LINE_ServerBD.strGamerTags[iPlayer])
				OR IS_STRING_NULL_OR_EMPTY(strHeadshotTextures[iPlayer])
					bUpdated = FALSE
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - Waiting for player ",iPlayer," to initialise")
				ENDIF
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(LINE_ServerBD.strGamerTags[iPlayer])
				OR NOT IS_STRING_NULL_OR_EMPTY(strHeadshotTextures[iPlayer])
					bUpdated = FALSE
					CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - Waiting for player ",iPlayer,"'s strings to clear")
				ENDIF
			ENDIF
		ENDFOR
		IF bUpdated = TRUE
			IF bReplayScreen
				LINE_MINIGAME_UI_INIT_LOBBY(minigameData, "DCTL_REPLAY")
			ELSE
				LINE_MINIGAME_UI_INIT_LOBBY(minigameData, "DCTL_INSERT")
			ENDIF
			CLEAR_BIT(iLocalLineBS, ciLineBS_UpdateGamertags)
			CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_GAMERTAGS - Updated")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_JOINING_GAME(LINE_MINIGAME_DATA &minigameData)
	INT iPlayer
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayer
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			IF NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_FINISHED_INTRO)
			AND (minigameData.eCurrentState != LINE_MINIGAME_REPLAY_SCREEN
			OR minigameData.eCurrentState != LINE_MINIGAME_TITLE_SCREEN)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REMOTE_PLAYERS_BE_CALLED()
	INT iNumOfPlayersReady = 0
	INT iPlayer
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayer
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			IF IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
				iNumOfPlayersReady++
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iNumOfPlayersReady > 1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC LINE_MINIGAME_UPDATE_TITLE_SCREEN_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SPLASH)
		LINE_MINIGAME_UI_HIDE_LOADING_SCREEN(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SPLASH)
		
		//Draw title screen one last frame to hide pops.
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_DRAW_TITLES_THIS_FRAME)
	ENDIF

	LINE_MINIGAME_DRAW_BACKGROUND(minigameData)
	LINE_MINIGAME_DRAW_TITLE_LOBBY(minigameData)
	
	IF IS_PC_VERSION()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
			SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(ciPBD_CONFIRM_ACCEPT)
			CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_TITLE_SCREEN_CLIENT - Accepted")
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
			SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(ciPBD_CONFIRM_REJECT)
			CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_UPDATE_TITLE_SCREEN_CLIENT - Declined")
		ENDIF
	ENDIF
	
	IF SHOULD_REMOTE_PLAYERS_BE_CALLED()
		DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(TRUE)
	ENDIF
	
	//The first time the player plays the game display help text on how to confirm/reject.
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_COMPLETED_ONE_PLAYTHROUGH)
	AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
	AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
		DISPLAY_HELP_TEXT_THIS_FRAME("DCTL_TITLEHELP", FALSE)
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_ALLOW_HELP_TEXT_THIS_FRAME)
	ENDIF

	LINE_MINIGAME_UPDATE_TITLE_SCREEN_SOUNDS(minigameData)
	LINE_MINIGAME_UPDATE_MAIN_MUSIC(minigameData)
	
	LINE_MINIGAME_UPDATE_GAMERTAGS(minigameData)
	
	IF LINE_ServerBD.eCurrentState > LINE_MINIGAME_TITLE_SCREEN
		IF IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
		OR IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
			SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
			//DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
			//DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
		ELSE
			IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_GAME_OVER_SCREEN
				DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
				DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
				SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
				DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(TRUE)
				EXIT
			ENDIF
			LINE_MINIGAME_RESET_GAME_STATE(minigameData)
			DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
			DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
			IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_RESHUFFLE
				SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_RESHUFFLE)
			ELSE
				minigameData.iNumPlayersThisRound = NETWORK_GET_NUM_PARTICIPANTS()

				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_TITLE_SCREEN_CLIENT - Fading out title music.")
				SET_VARIABLE_ON_SOUND(minigameData.iMusicTitleScreen, "FadeOut", 7.0)

				SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_READY_SCREEN)
			ENDIF
		ENDIF
	ELSE
		//Start the title screen music.
		IF HAS_SOUND_FINISHED(minigameData.iMusicTitleScreen)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_TITLE_SCREEN_CLIENT - Starting title music")
			PLAY_SOUND_FRONTEND(minigameData.iMusicTitleScreen, "Background", "DLC_EXEC_ARC_MAC_SOUNDS")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the initial screen, and waits for the players to continue.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_UPDATE_TITLE_SCREEN(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		BOOL bAllReady = TRUE
		BOOL bStillWaitingForIntroScreens = FALSE
		INT iPlayer
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND LINE_PlayerBD[iPlayer].iStartPos > -1
				IF NOT IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_FINISHED_INTRO)
					bStillWaitingForIntroScreens = TRUE
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_TITLE_SCREEN - Someone is still on the intro screen")
				ENDIF
				IF IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
					bAllReady = FALSE
				ENDIF
			ENDIF
		ENDFOR
		
		IF NOT HAS_NET_TIMER_STARTED(minigamedata.insertCoinsTimer)
		AND NOT IS_PLAYER_JOINING_GAME(minigameData)
		AND NOT bStillWaitingForIntroScreens
			START_NET_TIMER(minigamedata.insertCoinsTimer)
		ELSE
			IF bStillWaitingForIntroScreens
			OR IS_PLAYER_JOINING_GAME(minigameData)
				RESET_NET_TIMER(minigameData.insertCoinsTimer)
				EXIT
			ENDIF
			INT iNumPlayers = NETWORK_GET_NUM_PARTICIPANTS()
			INT iInsertCoinsTime = ciINSERT_COINS_TIME
			IF iNumPlayers = 1
			AND NOT IS_PLAYER_JOINING_GAME(minigameData)
				iInsertCoinsTime /= 2
			ENDIF
			IF (bAllReady AND iNumPlayers > 1)
			OR HAS_NET_TIMER_EXPIRED(minigamedata.insertCoinsTimer, iInsertCoinsTime)
				//Once all players have confirmed add a bit of delay so they can see all the confirmation icons before continuing
				IF NOT HAS_NET_TIMER_STARTED(minigameData.startConfirmedTimer)
					START_NET_TIMER(minigameData.startConfirmedTimer)
				ELIF HAS_NET_TIMER_EXPIRED(minigameData.startConfirmedTimer, 3000)
				AND NOT IS_PLAYER_JOINING_GAME(minigameData)
					RESET_NET_TIMER(minigameData.startConfirmedTimer)
					IF iNumPlayers = 1
						SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
						EXIT
					ENDIF
					LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_INIT
					SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_RESHUFFLE)
					DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
					DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT(LINE_MINIGAME_DATA &minigameData)
	LINE_MINIGAME_DRAW_BACKGROUND(minigameData)
	LINE_MINIGAME_DRAW_LOADING_TEXT(minigameData)
	//LINE_MINIGAME_DRAW_READY_TEXT(minigameData)

	//Remove the lobby screen.
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
		LINE_MINIGAME_UI_HIDE_LOBBY(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
	ENDIF

	IF LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_FINISHED
	AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_RESHUFFLED)
		BOOL bComplete = TRUE
		INT iPlayer
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				IF LINE_ServerBD.iNewStartPositions[iPlayer] = -1
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT - Waiting for broadcast data from server")
					bComplete = FALSE
					BREAKLOOP
				ENDIF
				IF iPlayer = iLocalPlayer
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT - Setting local player data")
					LINE_PlayerBD[iLocalPlayer].iStartPos = LINE_ServerBD.iNewStartPositions[iLocalPlayer]
					GET_START_POSITION_DATA(LINE_PlayerBD[iPlayer].iStartPos, 
											minigameData.playerData[iPlayer].iDirection, minigameData.playerData[iPlayer].fHeadX, 
											minigameData.playerData[iPlayer].fHeadY, minigameData.playerData[iPlayer].iPrevDirection)
				ELSE
					IF LINE_PlayerBD[iPlayer].iStartPos = LINE_ServerBD.iNewStartPositions[iPlayer]
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT - Setting remote player data")
						GET_START_POSITION_DATA(LINE_PlayerBD[iPlayer].iStartPos, 
												minigameData.playerData[iPlayer].iDirection, minigameData.playerData[iPlayer].fHeadX, 
												minigameData.playerData[iPlayer].fHeadY, minigameData.playerData[iPlayer].iPrevDirection)
					ELSE
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT - Waiting for broadcast data from remote player")
						bComplete = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		IF bComplete
			SET_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_RESHUFFLED)
		ENDIF
	ENDIF
	
	IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_GAME_OVER_SCREEN
		//Stop the title music immediately.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT - Stopping title music.")
		STOP_SOUND(minigameData.iMusicTitleScreen)
		
		//Play the game over sound.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN_CLIENT - Playing game over sound.")
		PLAY_SOUND_FRONTEND(-1, "Music_Game_Over", "DLC_EXEC_ARC_MAC_SOUNDS")
	
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
		DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(FALSE)
	ELIF LINE_ServerBD.eCurrentState >= LINE_MINIGAME_READY_SCREEN
		minigameData.iNumPlayersThisRound = NETWORK_GET_NUM_PARTICIPANTS()

		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT - Fading out title music.")
		SET_VARIABLE_ON_SOUND(minigameData.iMusicTitleScreen, "FadeOut", 7.0)
		LINE_MINIGAME_STORE_PLAYERS_DISPLAY_INFO()
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_READY_SCREEN)
		CLEAR_BIT(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_RESHUFFLED)
	ENDIF
ENDPROC

FUNC INT FIND_NEXT_EMPTY_START_POS()
	INT i, j
	
	FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		BOOL bFound = FALSE
		//loop through start pos
		FOR j = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF LINE_ServerBD.iNewStartPositions[j] = i
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - FIND_NEXT_EMPTY_START_POS - found match ", i, " = LINE_ServerBD.iNewStartPositions[",j,"]")
				bFound = TRUE
				BREAKLOOP
			ENDIF
		ENDFOR
		IF bFound = FALSE
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - FIND_NEXT_EMPTY_START_POS - Next free pos = ", i)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC


PROC LINE_MINIGAME_RESHUFFLE_PLAYERS(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(minigameData.reshuffleTimer)
			START_NET_TIMER(minigameData.reshuffleTimer)
		ELIF HAS_NET_TIMER_EXPIRED(minigameData.reshuffleTimer, ciRESHUFFLE_TIME)
			INT iNumPlayers = NETWORK_GET_NUM_PARTICIPANTS()
			
			//Override checks if using the single player test debug.
			#IF IS_DEBUG_BUILD
				IF bDebugAllowSinglePlayerTesting
					RESET_NET_TIMER(minigameData.startConfirmedTimer)
					SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_READY_SCREEN)
					EXIT
				ENDIF
			#ENDIF
			
			IF iNumPlayers = 1
				SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
				EXIT
			ENDIF
			INT i, j, iNext
			BOOL bComplete = TRUE
			SWITCH(LINE_ServerBD.eReshuffleState)
				CASE LINE_MINIGAME_RESHUFFLE_INIT
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - RESHUFFLE_INIT")
					FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						AND LINE_PlayerBD[i].iStartPos > -1
							LINE_ServerBD.iNewStartPositions[i] = LINE_PlayerBD[i].iStartPos
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - LINE_ServerBD.iNewStartPositions[",i,"] = istartpos[",LINE_PlayerBD[i].iStartPos)
						ELSE
							LINE_ServerBD.iNewStartPositions[i] = -1
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - LINE_ServerBD.iNewStartPositions[",i,"] = -1")
						ENDIF
					ENDFOR
					LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_ASSIGN
				BREAK
				
				CASE LINE_MINIGAME_RESHUFFLE_ASSIGN
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - RESHUFFLE_ASSIGN")
					//Check if we need to assign any new positions
					FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
							IF LINE_ServerBD.iNewStartPositions[i] = -1
								CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - LINE_ServerBD.iNewStartPositions[",i,"] = next empty pos")
								LINE_ServerBD.iNewStartPositions[i] = FIND_NEXT_EMPTY_START_POS()
							ENDIF
						ENDIF
					ENDFOR
					LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_CHECK
				BREAK
				
				CASE LINE_MINIGAME_RESHUFFLE_CHECK
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - RESHUFFLE_CHECK")
					//Check no dupes
					FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
							FOR j = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
								IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(j))
									IF j != i
									AND (LINE_ServerBD.iNewStartPositions[i] = LINE_ServerBD.iNewStartPositions[j]
									OR LINE_ServerBD.iNewStartPositions[i] = -1)
										CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - LINE_ServerBD.iNewStartPositions[",i,"] = ", LINE_ServerBD.iNewStartPositions[i] ," DUPE of LINE_ServerBD.iNewStartPositions[",j,"] = ", LINE_ServerBD.iNewStartPositions[j])
										LINE_ServerBD.iNewStartPositions[i] = -1
										LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_ASSIGN
										EXIT
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDFOR
					
					//Check no empty spaces
					FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
							IF LINE_ServerBD.iNewStartPositions[i] != -1
								iNext = FIND_NEXT_EMPTY_START_POS()
								IF iNext != -1
								AND iNext < LINE_ServerBD.iNewStartPositions[i]
									CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - LINE_ServerBD.iNewStartPositions[",i,"] (",LINE_ServerBD.iNewStartPositions[i],") swapped to empty space ", iNext)
									LINE_ServerBD.iNewStartPositions[i] = iNext
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_FINISHED			
				BREAK
				
				CASE LINE_MINIGAME_RESHUFFLE_FINISHED
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_RESHUFFLE_PLAYERS - RESHUFFLE_FINISHED")
					
					FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
							IF NOT IS_BIT_SET(LINE_PlayerBD[i].iPlayerBDBitSet, ciPBD_RESHUFFLED)
								bComplete = FALSE
							ENDIF
						ENDIF
					ENDFOR
					
					IF bComplete
						RESET_NET_TIMER(minigameData.startConfirmedTimer)
						RESET_NET_TIMER(minigameData.reshuffleTimer)
						SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_READY_SCREEN)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC LINE_MINIGAME_UPDATE_READY_SCREEN_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF NOT HAS_NET_TIMER_STARTED(minigameData.readyTimer)
		START_NET_TIMER(minigameData.readyTimer)
	ENDIF
	INT iPlayer
	FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
		AND LINE_PlayerBD[iPlayer].iStartPos > -1
			LINE_MINIGAME_UPDATE_PLAYER_STATE(minigameData, minigameData.PlayerData, iPlayer)
		ENDIF
	ENDFOR

	//Remove the lobby screen.
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
		LINE_MINIGAME_UI_HIDE_LOBBY(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
	ENDIF

	//Play the ready sound effect.
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_SFX_READY)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_READY_SCREEN_CLIENT - Playing ready audio.")
		PLAY_SOUND_FRONTEND(-1, "Ready", "DLC_EXEC_ARC_MAC_SOUNDS")
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_SFX_READY)
	ENDIF

	DRAW_LINE_MINIGAME(minigameData)
	LINE_MINIGAME_DRAW_READY_TEXT(minigameData)
	LINE_MINIGAME_DRAW_SCORES(minigameData)
	LINE_MINIGAME_UPDATE_MAIN_MUSIC(minigameData)
	
	IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_GAME_OVER_SCREEN
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
		DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(TRUE)
	ELIF LINE_ServerBD.eCurrentState >= LINE_MINIGAME_COUNTDOWN_SCREEN
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_COUNTDOWN_SCREEN)
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles the ready screen: players can confirm they're ready before the countdown begins.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_UPDATE_READY_SCREEN(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT() 
		INT iNumPlayers = NETWORK_GET_NUM_PARTICIPANTS()
		IF iNumPlayers = 1
			SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
			EXIT
		ENDIF
		//Progress once the timer runs out.
		IF HAS_NET_TIMER_STARTED(minigameData.readyTimer)
			IF HAS_NET_TIMER_EXPIRED(minigameData.readyTimer, 2000)
				SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_COUNTDOWN_SCREEN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT PARTITION_ARRAY(SCRIPT_EVENT_DATA_DCTL_TURN &CacheArray[], INT iLow, INT iHigh)
	INT i = iLow
	INT j = iHigh
	SCRIPT_EVENT_DATA_DCTL_TURN sTemp 
	INT iPivot = CacheArray[(iLow + iHigh) / 2].iLastTurn
	
	WHILE(i <= j)
		WHILE(CacheArray[i].iLastTurn < iPivot)
			i++
		ENDWHILE
		WHILE (CacheArray[j].iLastTurn > iPivot)
			j--
		ENDWHILE
		IF (i <= j)
			sTemp = CacheArray[i]
			CacheArray[i] = CacheArray[j]
			CacheArray[j] = sTemp
			i++
			j--
		ENDIF
	ENDWHILE
	
	RETURN i
ENDFUNC

PROC SORT_CACHE_ARRAY(SCRIPT_EVENT_DATA_DCTL_TURN &CacheArray[], INT iLow, INT iHigh)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - SORT_CACHE_ARRAY - [SPAM] Sorting cache array")
	INT iIndex = PARTITION_ARRAY(CacheArray, iLow, iHigh)
	IF iLow < iIndex - 1
		SORT_CACHE_ARRAY(CacheArray, iLow, iIndex - 1)
	ENDIF
	IF iIndex < iHigh
		SORT_CACHE_ARRAY(CacheArray, iIndex, iHigh)
	ENDIF
ENDPROC

PROC PROCESS_TURN_EVENT(LINE_MINIGAME_DATA &minigameData, INT iPart, INT iDirection, FLOAT fNewX, FLOAT fNewY, INT &iProcessedBitSet)		
	minigameData.PlayerData[iPart].iNextDirection = iDirection
	minigameData.playerData[iPart].fHeadX = fNewX
	minigameData.playerData[iPart].fHeadY = fNewY
	
	IF IS_BIT_SET(iProcessedBitSet, iPart)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - We have already processed part: ",iPart," this frame, reseting previous direction")
		minigameData.playerData[iPart].iPrevDirection = minigameData.playerData[iPart].iDirection
	ELSE
		SET_BIT(iProcessedBitSet, iPart)
	ENDIF
ENDPROC

FUNC BOOL GET_NEXT_EMPTY_CACHE_SLOT(INT &iCachePos)
	FOR iCachePos = 0 TO ciLAG_CACHE - 1
		IF LaggedTurnEventCache[iCachePos].iLastTurn = -1
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC PROCESS_LINE_EVENTS(LINE_MINIGAME_DATA &minigameData)
	INT iCount
	SCRIPT_EVENT_DATA_DCTL_TURN EventDataTurn
	SCRIPT_EVENT_DATA_DCTL_KILL EventDataKill
	SCRIPT_EVENT_DATA_DCTL_START EventDataStart
	
	SCRIPT_EVENT_DATA_DCTL_TURN EventDataTurnCache[ciEVENT_CACHE]
	INT iCacheArrayPos = 0
	
	INT iProcessedThisFrame = 0
	
	FLOAT fNewX, fNewY
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		IF GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_SCRIPT_EVENT
			IF IS_BIT_SET(iLocalLineBS, ciLineBS_UpdateDone)
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataTurn, SIZE_OF(EventDataTurn))
					IF EventDataTurn.Details.Type = SCRIPT_EVENT_DCTL_TURN
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Found event: TURN")
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Direction = ", EventDataTurn.iDirection)
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - fPosition = ", EventDataTurn.fPosition)
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Part = ", EventDataTurn.iPart)
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Turn = ", EventDataTurn.iLastTurn)
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - iCacheArrayPos = ", iCacheArrayPos)
						IF iCacheArrayPos < ciEVENT_CACHE - ciLAG_CACHE
							EventDataTurnCache[iCacheArrayPos] = EventDataTurn
							IF NOT IS_BIT_SET(iLocalLineBS, ciLineBS_ReceivedEventsThisFrame_0 + EventDataTurn.iPart)
								SET_BIT(iLocalLineBS, ciLineBS_ReceivedEventsThisFrame_0 + EventDataTurn.iPart)
								CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Setting ciLineBS_ReceivedEventsThisFrame_0", EventDataTurn.iPart)
							ENDIF
							iCacheArrayPos++
						ELSE
							SCRIPT_ASSERT("Too many cached events this frame. Please make a bug for John Scott/Default Design(Online Tools)")
						ENDIF
					ENDIF
				ENDIF
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataKill, SIZE_OF(EventDataKill))
					IF EventDataKill.Details.Type = SCRIPT_EVENT_DCTL_KILL
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Found event: KILL")
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Part = ", EventDataKill.iKillPart)
						LINE_MINIGAME_KILL_PLAYER(minigameData.PlayerData, EventDataKill.iKillPart)
					ENDIF
				ENDIF
			ELSE
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataStart, SIZE_OF(EventDataStart))
					IF EventDataStart.Details.Type = SCRIPT_EVENT_DCTL_START
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Found event: START")
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Time = ", EventDataStart.iTime)
						iTimeToStart = EventDataStart.iTime
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Add lag cache array to the frame cache	
	INT i, j
	SCRIPT_EVENT_DATA_DCTL_TURN EmptyTurnStruct
	FOR i = iCacheArrayPos TO ciLAG_CACHE - 1
		IF LaggedTurnEventCache[i].iLastTurn < 99999
		AND LaggedTurnEventCache[i].iLastTurn > -1
			EventDataTurnCache[iCacheArrayPos + i] = LaggedTurnEventCache[i]
			LaggedTurnEventCache[i] = EmptyTurnStruct
			j++
		ENDIF
	ENDFOR
	iCacheArrayPos += j
	
	//Sort array
	IF iCacheArrayPos > 1
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Final iCacheArrayPos =  ", iCacheArrayPos)
		FOR iCount = 0 TO iCacheArrayPos - 1
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - BEFORE EventDataTurnCache[iCount].iLastTurn =  ", EventDataTurnCache[iCount].iLastTurn)
		ENDFOR
		SORT_CACHE_ARRAY(EventDataTurnCache, 0, iCacheArrayPos -1)
		FOR iCount = 0 TO iCacheArrayPos - 1
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - AFTER EventDataTurnCache[iCount].iLastTurn =  ", EventDataTurnCache[iCount].iLastTurn)
		ENDFOR
	ENDIF
	//Process array
	FOR iCount = 0 TO iCacheArrayPos - 1
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Processing event ", iCount)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Direction = ", EventDataTurnCache[iCount].iDirection)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - fPosition = ", EventDataTurnCache[iCount].fPosition)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Part = ", EventDataTurnCache[iCount].iPart)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - iLastTurn = ", EventDataTurnCache[iCount].iLastTurn)
		
		FLOAT_COMPRESSION_UNPACK(EventDataTurnCache[iCount].fPosition, fNewX, fNewY, FLOAT_COMPRESSION_DEFAULT_VALUE)
		
		IF EventDataTurnCache[iCount].iLastTurn < 99999
			//Processing a real turn not an update
			IF NOT (iLastTurn[EventDataTurnCache[iCount].iPart] = (EventDataTurnCache[iCount].iPart * 10000))
				IF EventDataTurnCache[iCount].iLastTurn > iLastTurn[EventDataTurnCache[iCount].iPart] + 2
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Recieved an event too early, caching it for now. Current turn =  ", iLastTurn[EventDataTurnCache[iCount].iPart])
					INT iCachePos = 0
					IF GET_NEXT_EMPTY_CACHE_SLOT(iCachePos)
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Adding turn", EventDataTurnCache[iCount].iLastTurn," to lag cache, index = ", iCachePos)
						LaggedTurnEventCache[iCachePos] = EventDataTurnCache[iCount]
					ELSE
						SCRIPT_ASSERT("Too many events in the lag cache. Please make a bug for John Scott/Default Design(Online Tools)")
					ENDIF
					IF EventDataTurnCache[iCount].iLastTurn > iLastTurn[EventDataTurnCache[iCount].iPart] + 4
						iLastTurn[EventDataTurnCache[iCount].iPart]++
						CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Too far behind, incrementing iLastTurn")
					ENDIF
					RELOOP
				ELIF EventDataTurnCache[iCount].iLastTurn < iLastTurn[EventDataTurnCache[iCount].iPart]
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Recieved an event too late (Ignoring it). Current turn =  ", iLastTurn[EventDataTurnCache[iCount].iPart])
					RELOOP
				ENDIF
			ENDIF
			IF EventDataTurnCache[iCount].iLastTurn = iLastTurn[EventDataTurnCache[iCount].iPart] + 2
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - We are missing an event between our current(",iLastTurn[EventDataTurnCache[iCount].iPart],") and this event (",EventDataTurnCache[iCount].iLastTurn,")")
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - We don't have the event stored, generating the coords and direction")
				FLOAT fCalculatedX = 0.0, fCalculatedY = 0.0
				FLOAT fCurrentX = 0.0, fCurrentY = 0.0
				INT iCalculatedDirection = -1
				
				IF minigameData.playerData[EventDataTurnCache[iCount].iPart].iTailIndex > 0
					FLOAT_COMPRESSION_UNPACK(minigameData.playerData[EventDataTurnCache[iCount].iPart].fTail[minigameData.playerData[EventDataTurnCache[iCount].iPart].iTailIndex - 1],
											fCurrentX, fCurrentY, FLOAT_COMPRESSION_DEFAULT_VALUE)
				ELSE
					INT iUnusedDirection = 0
					INT iUnusedPrevDirection = 0
					GET_START_POSITION_DATA(LINE_PlayerBD[EventDataTurnCache[iCount].iPart].iStartPos, iUnusedDirection, fCurrentX, fCurrentY, iUnusedPrevDirection)
				ENDIF
											 
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - CURRENT DIRECTION: ", minigameData.playerData[EventDataTurnCache[iCount].iPart].iDirection)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - CURRENT COORDS: (",fCurrentX,",",fCurrentY,")" )
				
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - NEXT DIRECTION: ",EventDataTurnCache[iCount].iDirection )
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - NEXT COORDS: (",fNewX,",",fNewY,")" )
				
				//Find missing direction
				SWITCH(minigameData.playerData[EventDataTurnCache[iCount].iPart].iDirection)
					CASE DIRECTION_UP
					CASE DIRECTION_DOWN
						IF fCurrentX > fNewX
							iCalculatedDirection = DIRECTION_RIGHT
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Missing direction is RIGHT")
						ELSE
							iCalculatedDirection = DIRECTION_LEFT
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Missing direction is LEFT")
						ENDIF
					BREAK

					CASE DIRECTION_RIGHT
					CASE DIRECTION_LEFT
						IF fCurrentY > fNewY
							iCalculatedDirection = DIRECTION_DOWN
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Missing direction is DOWN")
						ELSE
							iCalculatedDirection = DIRECTION_UP
							CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Missing direction is UP")
						ENDIF
					BREAK
				ENDSWITCH
				
				//Find missing coords
				SWITCH(iCalculatedDirection)
					CASE DIRECTION_UP
						fCalculatedX = fNewX
						fCalculatedY = fCurrentY
					BREAK
					
					CASE DIRECTION_DOWN
						fCalculatedX = fNewX
						fCalculatedY = fCurrentY
					BREAK

					CASE DIRECTION_RIGHT
						fCalculatedX = fCurrentX
						fCalculatedY = fNewY
					BREAK
					
					CASE DIRECTION_LEFT
						fCalculatedX = fCurrentX
						fCalculatedY = fNewY
					BREAK
				ENDSWITCH
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Missing coords are (",fCalculatedX,",",fCalculatedY,")")
				PROCESS_TURN_EVENT(minigameData, EventDataTurnCache[iCount].iPart, iCalculatedDirection,
								fCalculatedX, fCalculatedY, iProcessedThisFrame)
				PROCESS_PLAYER_MOVEMENT(minigameData, minigameData.playerData, EventDataTurnCache[iCount].iPart)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Missing event processing done, processing turn: ", EventDataTurnCache[iCount].iLastTurn)
				PROCESS_TURN_EVENT(minigameData, EventDataTurnCache[iCount].iPart, EventDataTurnCache[iCount].iDirection,
									fNewX, fNewY, iProcessedThisFrame)
				PROCESS_PLAYER_MOVEMENT(minigameData, minigameData.playerData, EventDataTurnCache[iCount].iPart)
			ELSE
				IF NOT (EventDataTurnCache[iCount].iLastTurn = iLastTurn[EventDataTurnCache[iCount].iPart] + 1)
				AND NOT (EventDataTurnCache[iCount].iLastTurn = (EventDataTurnCache[iCount].iPart * 10000))
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - We have lost or not recieved 2 or more events. Nothing we can do to fix the line")
				ELSE
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - Event is in the correct order")
				ENDIF
				FLOAT_COMPRESSION_UNPACK(EventDataTurnCache[iCount].fPosition, fNewX, fNewY, FLOAT_COMPRESSION_DEFAULT_VALUE)
				PROCESS_TURN_EVENT(minigameData, EventDataTurnCache[iCount].iPart, EventDataTurnCache[iCount].iDirection,
									fNewX, fNewY, iProcessedThisFrame)
				PROCESS_PLAYER_MOVEMENT(minigameData, minigameData.playerData, EventDataTurnCache[iCount].iPart)
			ENDIF
			iLastTurn[EventDataTurnCache[iCount].iPart] = EventDataTurnCache[iCount].iLastTurn
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - PROCESS_LINE_EVENTS - DIRECTION_UPDATE_ONLY")
			IF EventDataTurnCache[iCount].iDirection = DIRECTION_UPDATE_ONLY
			AND NOT IS_BIT_SET(iProcessedThisFrame, EventDataTurnCache[iCount].iPart)
				IF NOT IS_BIT_SET(iLocalLineBS ,ciLineBS_RecievedFirstSync_0 + EventDataTurnCache[iCount].iPart)
					SET_BIT(iLocalLineBS ,ciLineBS_RecievedFirstSync_0 + EventDataTurnCache[iCount].iPart)
				ENDIF
				minigameData.playerData[EventDataTurnCache[iCount].iPart].fHeadX = fNewX
				minigameData.playerData[EventDataTurnCache[iCount].iPart].fHeadY = fNewY
				PROCESS_PLAYER_MOVEMENT(minigameData, minigameData.playerData, EventDataTurnCache[iCount].iPart)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEAR_LAST_TURN_ARRAY()
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - CLEAR_LAST_TURN_ARRAY - Clearing out event arrays for the next round")
	INT i
	SCRIPT_EVENT_DATA_DCTL_TURN EmptyTurnStruct
	FOR i = 0 TO MAX_NUM_PLAYERS - 1
		iLastTurn[i] = (i * 10000)
	ENDFOR
	FOR i = 0 TO ciLAG_CACHE - 1
		LaggedTurnEventCache[i] = EmptyTurnStruct
	ENDFOR
ENDPROC

PROC LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF NOT HAS_NET_TIMER_STARTED(minigameData.countdownTimer)
		START_NET_TIMER(minigameData.countdownTimer)
	ENDIF
	INT iPlayer
	FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
		AND LINE_PlayerBD[iPlayer].iStartPos > -1
			LINE_MINIGAME_UPDATE_PLAYER_STATE(minigameData, minigameData.PlayerData, iPlayer)
		ENDIF
	ENDFOR

	DRAW_LINE_MINIGAME(minigameData)
	LINE_MINIGAME_DRAW_COUNTDOWN_TEXT(minigameData)
	LINE_MINIGAME_DRAW_SCORES(minigameData)
	PROCESS_LINE_EVENTS(minigameData)
	LINE_MINIGAME_UPDATE_COUNTDOWN_SOUNDS(minigameData)
	LINE_MINIGAME_UPDATE_MAIN_MUSIC(minigameData)
	
	IF LINE_ServerBD.eCurrentState >= LINE_MINIGAME_RUNNING
		//End the title screen music completely.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN_CLIENT - Stopping title music.")
		STOP_SOUND(minigameData.iMusicTitleScreen)
	
		//Play the 'Go' sound.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN_CLIENT - Starting go sound.")
		PLAY_SOUND_FRONTEND(-1, "Go", "DLC_EXEC_ARC_MAC_SOUNDS")
		RESET_NET_TIMER(minigameData.countdownTimer)
		CLEAR_LAST_TURN_ARRAY()
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_RUNNING)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the countdown screen: players will be shown a "3...2...1" countdown before the game starts.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT() 
		//Progress once the timer runs out.
		IF HAS_NET_TIMER_STARTED(minigameData.countdownTimer)
			IF HAS_NET_TIMER_EXPIRED(minigameData.countdownTimer, 2000)
			AND NOT IS_BIT_SET(iLocalLineBS, ciLineBS_UpdateSent)
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN - Sending update")
				BROADCAST_DCTL_START(NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) + 1250)
				SET_BIT(iLocalLineBS, ciLineBS_UpdateSent)
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(minigameData.countdownTimer, 3000)
				SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_RUNNING)
				CLEAR_BIT(iLocalLineBS, ciLineBS_UpdateSent)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LINE_MINIGAME_UPDATE_MAIN_GAME_CLIENT(LINE_MINIGAME_DATA &minigameData)
	INT iPlayer
	FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
		AND LINE_PlayerBD[iPlayer].iStartPos > -1
			minigameData.playerData[iPlayer].iPrevDirection = minigameData.playerData[iPlayer].iDirection
			minigameData.playerData[iPlayer].iNextDirection = minigameData.playerData[iPlayer].iDirection
		ENDIF
	ENDFOR

	PROCESS_LINE_EVENTS(minigameData)
	DRAW_LINE_MINIGAME(minigameData)
	LINE_MINIGAME_DRAW_GO_TEXT(minigameData)
	LINE_MINIGAME_DRAW_SCORES(minigameData)
	
	//Keep the music updating as long as enough players are alive.
	LINE_MINIGAME_UPDATE_MAIN_MUSIC(minigameData)
	
	IF IS_BIT_SET(iLocalLineBS, ciLineBS_UpdateDone)
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND LINE_PlayerBD[iPlayer].iStartPos > -1
				LINE_MINIGAME_UPDATE_PLAYER_STATE(minigameData, minigameData.PlayerData, iPlayer)
				IF IS_BIT_SET(iLocalLineBS, ciLineBS_ReceivedEventsLastFrame_0 + iPlayer)
					CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_MAIN_GAME_CLIENT - Clearing ciLineBS_ReceivedEventsLastFrame_", iPlayer)
					CLEAR_BIT(iLocalLineBS, ciLineBS_ReceivedEventsLastFrame_0 + iPlayer)
				ENDIF
				IF IS_BIT_SET(iLocalLineBS, ciLineBS_ReceivedEventsThisFrame_0 + iPlayer)
					CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_MAIN_GAME_CLIENT - Setting ciLineBS_ReceivedEventsLastFrame_", iPlayer, " and clearing ciLineBS_ReceivedEventsThisFrame_", iPlayer )
					SET_BIT(iLocalLineBS, ciLineBS_ReceivedEventsLastFrame_0 + iPlayer)
					CLEAR_BIT(iLocalLineBS, ciLineBS_ReceivedEventsThisFrame_0 + iPlayer)
				ENDIF
			ELSE
				IF minigameData.playerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
					SET_LINE_MINIGAME_PLAYER_STATE(minigameData.playerData, LINE_MINIGAME_PLAYER_DEAD, iPlayer)
					minigameData.playerData[iPlayer].iAlpha = 0
				ENDIF
			ENDIF
		ENDFOR
		
		IF NOT HAS_NET_TIMER_STARTED(tdSyncEvent)
			START_NET_TIMER(tdSyncEvent)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(tdSyncEvent, ciUPDATE_TIME)
				INT iNumPlayers = NETWORK_GET_NUM_PARTICIPANTS()
				IF iNumPlayers = 1
					//Play the game over sound.
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN_CLIENT - Playing game over sound.")
					PLAY_SOUND_FRONTEND(-1, "Music_Game_Over", "DLC_EXEC_ARC_MAC_SOUNDS")
					SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
					EXIT
				ENDIF
				BROADCAST_DCTL_TURN(DIRECTION_UPDATE_ONLY, FLOAT_COMPRESSION_PACK(minigameData.playerData[iLocalPlayer].fHeadX, minigameData.playerData[iLocalPlayer].fHeadY, FLOAT_COMPRESSION_DEFAULT_VALUE), iLocalPlayer, 99999)
				RESET_NET_TIMER(tdSyncEvent)
			ENDIF
		ENDIF
		
		//The first time the player plays the game display help text on how to move.
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_COMPLETED_ONE_PLAYTHROUGH)
		AND minigameData.playerData[iLocalPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
			DISPLAY_HELP_TEXT_THIS_FRAME("DCTL_GAMEHELP", FALSE)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_ALLOW_HELP_TEXT_THIS_FRAME)
		ENDIF
		
		IF LINE_ServerBD.eCurrentState >= LINE_MINIGAME_WINNER_SCREEN
			//Play the winner screen sound.
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_MAIN_GAME_CLIENT - Playing winner sound.")
			PLAY_SOUND_FRONTEND(-1, "Music_Win", "DLC_EXEC_ARC_MAC_SOUNDS")
		
			//Kill any remaining sounds.
			FOR iPlayer = 0 TO MAX_NUM_PLAYERS - 1
				STOP_SOUND(minigameData.PlayerData[iPlayer].iPlayerMoveSound)
			ENDFOR
			
			IF IS_STREAM_PLAYING()
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_MAIN_GAME_CLIENT - Stopping main music")
				STOP_STREAM()
			ENDIF
			CLEAR_BIT(iLocalLineBS, ciLineBS_UpdateDone)
			SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_WINNER_SCREEN)
		ENDIF
	ELSE
		IF iTimeToStart != -1
		AND NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) >= iTimeToStart
			SET_BIT(iLocalLineBS, ciLineBS_UpdateDone)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles the main game update: updates player movement and draws everything on the screen.
PROC LINE_MINIGAME_UPDATE_MAIN_GAME(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT iPlayer
		INT iPlayersRemaining = MAX_NUM_PLAYERS
		INT iWinner = -1
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND LINE_PlayerBD[iPlayer].iStartPos > -1
				IF minigameData.PlayerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_DEAD
					iPlayersRemaining--
				ELIF minigameData.PlayerData[iPlayer].eCurrentState = LINE_MINIGAME_PLAYER_ACTIVE
					iWinner = iPlayer
				ENDIF
			ELSE
				iPlayersRemaining--
			ENDIF
		ENDFOR
		
		//Debug only to allow testing with only one player: override the number of players remaining.
		#IF IS_DEBUG_BUILD
			IF bDebugAllowSinglePlayerTesting
				IF iPlayersRemaining > 0
					iPlayersRemaining = 4
				ENDIF
			ENDIF
		#ENDIF
		
		IF iPlayersRemaining <= 1
			IF NOT HAS_NET_TIMER_STARTED(minigameData.winnerDeterminedTimer)
				START_NET_TIMER(minigameData.winnerDeterminedTimer)
			ELIF HAS_NET_TIMER_EXPIRED(minigameData.winnerDeterminedTimer, 1000)
				SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_WINNER_SCREEN)
			ELIF HAS_NET_TIMER_EXPIRED(minigameData.winnerDeterminedTimer, 500)
				IF iWinner > -1
					LINE_ServerBD.iWinningPlayer = iWinner
				ENDIF
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_MAIN_GAME - Winner = ", LINE_ServerBD.iWinningPlayer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the winner screen: displays a message indicating the player that won.
PROC LINE_MINIGAME_UPDATE_WINNER_SCREEN_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
		LINE_MINIGAME_UI_HIDE_HUD(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
	ENDIF
	
	LINE_MINIGAME_DRAW_BACKGROUND(minigameData)
	LINE_MINIGAME_DRAW_WINNER_TEXT(minigameData)
	
	IF LINE_ServerBD.eCurrentState >= LINE_MINIGAME_REPLAY_SCREEN
		//DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(TRUE)
		DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(TRUE)

		IF HAS_SOUND_FINISHED(minigameData.iMusicTitleScreen)
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_WINNER_SCREEN_CLIENT - Starting replay music")
			PLAY_SOUND_FRONTEND(minigameData.iMusicTitleScreen, "Background", "DLC_EXEC_ARC_MAC_SOUNDS")
		ENDIF
		
		// Incrememnt times played
		INT iCurrentPlayStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_DCTL_PLAY_COUNT)
		iCurrentPlayStat++
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DCTL_PLAY_COUNT, iCurrentPlayStat)
		
		// Reward player a t-shirt if they win
		IF LINE_ServerBD.iWinningPlayer = iLocalPlayer
			CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_WINNER_SCREEN_CLIENT - This player is the winner")
			INT iCurrentWinsStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_DCTL_WINS)
			iCurrentWinsStat++
			SET_MP_INT_CHARACTER_STAT(MP_STAT_DCTL_WINS, iCurrentWinsStat)
			IF iCurrentWinsStat >= g_sMPTunables.iDCTL_WIN_COUNT_REWARD
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_WINNER_SCREEN_CLIENT - iCurrentWinsStat (",iCurrentWinsStat,") >= g_sMPTunables.iDCTL_WIN_COUNT_REWARD(",g_sMPTunables.iDCTL_WIN_COUNT_REWARD,")")
				SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL, TRUE)
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_015_M"), TATTOO_MP_FM), TRUE)
				ELSE
					SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_015_F"), TATTOO_MP_FM_F), TRUE)
				ENDIF
			ENDIF
		ENDIF
		LINE_MINIGAME_STORE_PLAYERS_DISPLAY_INFO()
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_REPLAY_SCREEN)
	ENDIF
	
	IF NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
		SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(ciPBD_NOT_CONFIRMED)
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles the winner screen: displays a message indicating the player that won.
PROC LINE_MINIGAME_UPDATE_WINNER_SCREEN(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT iPlayer
		IF NOT HAS_NET_TIMER_STARTED(minigameData.winnerScreenTimer)
			START_NET_TIMER(minigameData.winnerScreenTimer)
			IF LINE_ServerBD.iWinningPlayer != -1
				LINE_ServerBD.iCurrentScore[LINE_ServerBD.iWinningPlayer]++
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_WINNER_SCREEN - Incrementing player ",LINE_ServerBD.iWinningPlayer,"'s score to: ", LINE_ServerBD.iCurrentScore[LINE_ServerBD.iWinningPlayer])
			ENDIF
			FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
					LINE_ServerBD.iCurrentScore[iPlayer] = 0
				ENDIF
			ENDFOR
		ELIF HAS_NET_TIMER_EXPIRED(minigameData.winnerScreenTimer, 3000)
			BOOL bAllReady = TRUE
			FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				AND LINE_PlayerBD[iPlayer].iStartPos > -1
					IF NOT IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
						bAllReady = FALSE
						BREAKLOOP
					ENDIF
				ENDIF
			ENDFOR
			IF bAllReady
				LINE_MINIGAME_RESET_GAME_STATE(minigameData)
				DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(TRUE)
				SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_REPLAY_SCREEN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LINE_MINIGAME_UPDATE_REPLAY_SCREEN_CLIENT(LINE_MINIGAME_DATA &minigameData)
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
		LINE_MINIGAME_UI_HIDE_HUD(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_SCORES)
	ENDIF

	LINE_MINIGAME_DRAW_BACKGROUND(minigameData)
	LINE_MINIGAME_DRAW_REPLAY_LOBBY(minigameData)

	IF IS_PC_VERSION()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
			SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(ciPBD_CONFIRM_ACCEPT)
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
		AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
			SET_LINE_MINIGAME_PLAYER_CONFIRM_STATE(ciPBD_CONFIRM_REJECT)
		ENDIF
	ENDIF
	
	IF SHOULD_REMOTE_PLAYERS_BE_CALLED()
		DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(TRUE)
	ENDIF
	
	IF NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
	AND NOT IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
		IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_COMPLETED_ONE_PLAYTHROUGH)
			DISPLAY_HELP_TEXT_THIS_FRAME("DCTL_REPLAYHELP", FALSE)
			SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_ALLOW_HELP_TEXT_THIS_FRAME)
		ENDIF
	ENDIF
	LINE_MINIGAME_UPDATE_GAMERTAGS(minigameData, TRUE)
	LINE_MINIGAME_UPDATE_REPLAY_SCREEN_SOUNDS(minigameData)
	
	IF LINE_ServerBD.eCurrentState = LINE_MINIGAME_GAME_OVER_SCREEN
		//End the replay screen music (same music as the title screen).
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN_CLIENT - Stopping replay music.")
		STOP_SOUND(minigameData.iMusicTitleScreen)
	
		//Play the game over sound.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN_CLIENT - Playing game over sound.")
		PLAY_SOUND_FRONTEND(-1, "Music_Game_Over", "DLC_EXEC_ARC_MAC_SOUNDS")
	
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_COMPLETED_ONE_PLAYTHROUGH)
	
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
		DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(TRUE)
		DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
		DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
	ELIF LINE_ServerBD.eCurrentState = LINE_MINIGAME_RESHUFFLE
		IF IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
		OR IS_BIT_SET(LINE_PlayerBD[iLocalPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
			SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
			//DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
			//DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
			EXIT
		ENDIF
		CLEAR_BIT(iLocalLineBS, ciLineBS_UpdateDone)
		LINE_MINIGAME_RESET_GAME_STATE(minigameData)
		DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
		DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
		SET_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_COMPLETED_ONE_PLAYTHROUGH)

		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_RESHUFFLE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the replay screen: all players must confirm if they want to play again or quit.
PROC LINE_MINIGAME_UPDATE_REPLAY_SCREEN(LINE_MINIGAME_DATA &minigameData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		BOOL bAllReject = TRUE
		BOOL bStillWaitingForConfirmations = FALSE
		BOOL bStillWaitingForIntroScreens = FALSE
		INT iContinuingPlayers = 0
		INT iPlayer
		FOR iPlayer = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND LINE_PlayerBD[iPlayer].iStartPos > -1
				IF NOT IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_FINISHED_INTRO)
					bStillWaitingForIntroScreens = TRUE
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN - Someone is still on the intro screen")
				ENDIF
				IF IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_NOT_CONFIRMED)
					bStillWaitingForConfirmations = TRUE
				ENDIF
				IF NOT IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_REJECT)
					bAllReject = FALSE
				ENDIF
				IF IS_BIT_SET(LINE_PlayerBD[iPlayer].iPlayerBDBitSet, ciPBD_CONFIRM_ACCEPT)
					iContinuingPlayers++
				ENDIF
			ELSE
				IF LINE_ServerBD.iCurrentScore[iPlayer] != 0
					LINE_ServerBD.iCurrentScore[iPlayer] = 0
				ENDIF
			ENDIF
		ENDFOR
		
		//Once all players have confirmed then add some delay so players can see the all confirmation icons before progressing.
		IF NOT HAS_NET_TIMER_STARTED(minigameData.replayConfirmedTimer)
		AND NOT IS_PLAYER_JOINING_GAME(minigameData)
		AND NOT bStillWaitingForConfirmations
			START_NET_TIMER(minigameData.replayConfirmedTimer)
		ELSE
			IF bStillWaitingForIntroScreens
			OR bStillWaitingForConfirmations
			OR IS_PLAYER_JOINING_GAME(minigameData)
				RESET_NET_TIMER(minigameData.replayConfirmedTimer)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(minigameData.replayConfirmedTimer, 3000)
			AND HAS_NET_TIMER_STARTED(minigameData.replayConfirmedTimer)
			AND NOT IS_PLAYER_JOINING_GAME(minigameData)
			AND NOT bStillWaitingForConfirmations
				IF bAllReject
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN - replayConfirmedTimer has expired and bAllReject")
					SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
					EXIT
				ELIF NOT bStillWaitingForConfirmations
					CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN - replayConfirmedTimer has expired and bAllConfirm")
					LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_INIT
					SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_RESHUFFLE)
					LINE_ServerBD.iWinningPlayer = -1
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(minigameData.replayScreenTimer)
		AND NOT bStillWaitingForIntroScreens
			START_NET_TIMER(minigameData.replayScreenTimer)
		ELSE
			IF bStillWaitingForIntroScreens
				RESET_NET_TIMER(minigameData.replayScreenTimer)
				EXIT
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(minigameData.replayScreenTimer, 10000)
				
				CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_UPDATE_REPLAY_SCREEN - replayScreenTimer has expired")
				IF iContinuingPlayers > 1
					DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
					DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
					LINE_ServerBD.eReshuffleState = LINE_MINIGAME_RESHUFFLE_INIT
					SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_RESHUFFLE)
				ELSE
					SET_LINE_MINIGAME_STATE(minigameData, LINE_MINIGAME_GAME_OVER_SCREEN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles the game over screen: this occurs if there are not enough players to play another game.
PROC LINE_MINIGAME_UPDATE_GAME_OVER_SCREEN_CLIENT(LINE_MINIGAME_DATA &minigameData)
	LINE_MINIGAME_DRAW_BACKGROUND(minigameData)
	LINE_MINIGAME_DRAW_GAME_OVER_TEXT(minigameData)
	
	//Remove the lobby screen.
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
		LINE_MINIGAME_UI_HIDE_LOBBY(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_TRIGGERED_UI_LOBBY)
	ENDIF
	
	//Play sound effect.
	IF HAS_SOUND_FINISHED(minigameData.iGameOverSoundID)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME][AUDIO] - LINE_MINIGAME_UPDATE_GAME_OVER_SCREEN_CLIENT - Starting game over sound")
		PLAY_SOUND_FRONTEND(minigameData.iGameOverSoundID, "Game_Over_Blink", "DLC_EXEC_ARC_MAC_SOUNDS")
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(minigameData.gameOverScreenTimer)
		START_NET_TIMER(minigameData.gameOverScreenTimer)
	ELIF HAS_NET_TIMER_EXPIRED(minigameData.gameOverScreenTimer, ciGAMEOVER_SCREEN_TIME)
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
		DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(FALSE)
		//DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
		//DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
		DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(FALSE)
		//DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(FALSE)
		//DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Final cleanup procedure: removes all assets and resets player state.
/// PARAMS:
///    minigameData - 
PROC LINE_MINIGAME_CLEANUP_AND_EXIT_CLIENT(LINE_MINIGAME_DATA &minigameData)
	CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] - LINE_MINIGAME_CLEANUP_AND_EXIT_CLIENT - Cleaning up and terminating the script")
	//Stop any ongoing sounds.
	STOP_AUDIO_SCENE("DLC_Exec_Arc_Mac_Playing_Game_Scene")
	STOP_SOUND(minigameData.iMusicTitleScreen)
	STOP_SOUND(minigameData.iGameOverSoundID)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF PARTICIPANT_ID_TO_INT() != -1
			STOP_SOUND(minigameData.PlayerData[PARTICIPANT_ID_TO_INT()].iPlayerMoveSound)
		ENDIF
	ENDIF
	IF IS_STREAM_PLAYING()
		STOP_STREAM()
	ENDIF
	
	//Unpause objective text
	Unpause_Objective_Text()
	
	//Reenable phone/internet
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	MP_FORCE_TERMINATE_INTERNET_CLEAR()
	
	//Renable notifications
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	
	//Disable multiple displays/blinders.
	SET_MULTIHEAD_SAFE(FALSE, TRUE)
	
	//Re-enable the interaction menu.
	ENABLE_INTERACTION_MENU()
	
	//Remove loaded assets.
	LINE_MINIGAME_CLEANUP_ALL_ASSETS()
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(minigameData.siMinigame)
	ENDIF
	
	//Return player control.
	IF IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	DONT89_CLEAN_UP_LAUNCH_ARGS(TRUE)
	
	//Set the state so calling the main update function does nothing.
	SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_FINISHED)
	DONT89_SETIS_RUNNING_GAME_BD_STATE(FALSE)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Handles any global updates for the minigame that need to occur before the main state updates.
PROC LINE_MINIGAME_GLOBAL_PRE_UPDATE(LINE_MINIGAME_DATA &minigameData)
	//Disable any controls that shouldn't be active.
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	Pause_Objective_Text()
	DISABLE_SELECTOR_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	
	IF NOT IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("DCTL89_LaunchOutside")
		#ENDIF
			SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
	ENDIF
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	AND IS_SCREEN_FADED_OUT()
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
	ENDIF
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	AND IS_SCREEN_FADED_OUT()
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
	ENDIF
	
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	AND IS_SCREEN_FADED_OUT()
		SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
	ENDIF
	
	//Stop any replay recording from happening.
	#IF USE_REPLAY_RECORDING_TRIGGERS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
	
	REPLAY_PREVENT_RECORDING_THIS_FRAME()

	//Disable the cellphone.
	IF LINE_ServerBD.eCurrentState > LINE_MINIGAME_LAUNCHING
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	
	//Ensure the aspect ratio values are kept up to date in case players change resolution mid-game.
	SET_CURRENT_ASPECT_RATIO_MODIFIER(minigameData)
	
	//Pause menu checks.
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND LINE_ServerBD.eCurrentState > LINE_MINIGAME_LAUNCHING
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF
	
	BOOL bChangedThisFrame
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND NOT (LINE_ServerBD.eCurrentState = LINE_MINIGAME_RESHUFFLE)
		INT i, j
		FOR i = 0 TO MAX_NUM_PLAYERS - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF LINE_ServerBD.iNewStartPositions[i] = -1
					LINE_ServerBD.iNewStartPositions[i] = FIND_NEXT_EMPTY_START_POS()
					bChangedThisFrame = TRUE
				ENDIF
			ELSE
				IF LINE_ServerBD.iNewStartPositions[i] != -1
					LINE_ServerBD.iNewStartPositions[i] = -1
					bChangedThisFrame = TRUE
				ENDIF
			ENDIF
		ENDFOR
		IF bChangedThisFrame
			FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					FOR j = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(j))
							IF j != i
							AND (LINE_ServerBD.iNewStartPositions[i] = LINE_ServerBD.iNewStartPositions[j]
							OR LINE_ServerBD.iNewStartPositions[i] = -1)
								LINE_ServerBD.iNewStartPositions[i] = -1
								EXIT
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles any updates for the minigame that need to occur after the main state updates.
PROC LINE_MINIGAME_GLOBAL_POST_UPDATE(LINE_MINIGAME_DATA &minigameData)
	//Hide help text except when this script is trying to display help text.
	IF NOT IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_ALLOW_HELP_TEXT_THIS_FRAME)
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
	
	CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_ALLOW_HELP_TEXT_THIS_FRAME)
	
	//Draw the scaleform movie: this needs to be called every frame after the other UI elements, 
	//individual stages will control what actually gets drawn.
	IF minigameData.eCurrentState > LINE_MINIGAME_REQUESTING_ASSETS
	AND minigameData.eCurrentState < LINE_MINIGAME_CLEANUP
	AND HAS_SCALEFORM_MOVIE_LOADED(minigameData.siMinigame)
	AND NOT IS_PAUSE_MENU_ACTIVE()
	#IF IS_DEBUG_BUILD AND NOT bDebugHideMinigameUI #ENDIF	
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(minigameData.siMinigame, 255, 255, 255, 255)
	ENDIF
	
	//Draw the title screen over everything else if flagged to do so.
	IF IS_BIT_SET(minigameData.iFlags, LINE_MINIGAME_FLAGS_DRAW_TITLES_THIS_FRAME)
		LINE_MINIGAME_DRAW_TITLE_SCREEN(minigameData)
		CLEAR_BIT(minigameData.iFlags, LINE_MINIGAME_FLAGS_DRAW_TITLES_THIS_FRAME)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Sets the given widget group to be the parent for any widgets created by the minigame.
	PROC LINE_MINIGAME_SET_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)
		widgetLineMinigame = parentWidgetGroup
	ENDPROC

	/// PURPOSE:
	///    Creates the debug widgets for this minigame.
	PROC LINE_MINIGAME_CREATE_WIDGETS()
		IF NOT bDebugCreatedWidgets
		AND widgetLineMinigame != NULL
			SET_CURRENT_WIDGET_GROUP(widgetLineMinigame)
			
			START_WIDGET_GROUP("Line Minigame")
				ADD_WIDGET_BOOL("Hide minigame UI", bDebugHideMinigameUI)
				ADD_WIDGET_BOOL("Allow testing with one player", bDebugAllowSinglePlayerTesting)
				ADD_WIDGET_BOOL("Display debug on screen", bDebugDisplayDataOnScreen)
				ADD_WIDGET_BOOL("Enable input testing", bDebugEnableInputTest)
				ADD_WIDGET_INT_SLIDER("Turn delay", TURN_DELAY, 0, 500, 1)
				ADD_WIDGET_INT_SLIDER("Default player alpha", DEFAULT_PLAYER_ALPHA, 0, 255, 1)
				ADD_WIDGET_FLOAT_SLIDER("Player tail thickness", TAIL_THICKNESS, 0.001, 0.5, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Player speed", DEFAULT_SPEED, 0.001, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Player head width", PLAYER_HEAD_WIDTH, 0.001, 0.5, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Player head height", PLAYER_HEAD_HEIGHT, 0.001, 0.5, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Player fade out speed", PLAYER_FADE_OUT_SPEED, 0.001, 1000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Stick threshold", STICK_THRESHOLD, 0.001, 1.0, 0.001)
				ADD_WIDGET_BOOL("Display collision debug", bDebugDisplayCollisionInfo)
				ADD_WIDGET_FLOAT_SLIDER("Head collision box modifier", HEAD_COLLISION_BOX_MODIFIER, 0.001, 1.0, 0001)
				
				START_WIDGET_GROUP("Play area bounds")
					ADD_WIDGET_FLOAT_SLIDER("Top", TOP_WALL_POS_Y, 0.001, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Bottom", BOTTOM_WALL_POS_Y, 0.001, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Left", LEFT_WALL_DIST_FROM_CENTRE, 0.001, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Right", RIGHT_WALL_DIST_FROM_CENTRE, 0.001, 1.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Mic Status")
					ADD_WIDGET_BOOL("Override", bDebugOverrideMicStatus)
					ADD_WIDGET_INT_SLIDER("Override Value", iDebugMicOverride, 0, 3, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Test Rectangle")
					ADD_WIDGET_BOOL("Draw rectangle", bDebugDrawTestRect)
					ADD_WIDGET_FLOAT_SLIDER("Pos x", fDebugTestRectPosX, 0.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Pos y", fDebugTestRectPosY, 0.0, 1.0, 0.001)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			CLEAR_CURRENT_WIDGET_GROUP(widgetLineMinigame)
			
			bDebugCreatedWidgets = TRUE
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Dumps all minigame data to the console.
	/// PARAMS:
	///    minigameData - 
	PROC LINE_MINIGAME_DUMP_DATA_TO_CONSOLE(LINE_MINIGAME_DATA &minigameData)
	
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_DUMP_DATA_TO_CONSOLE - Dumping data...")
		
		//General data.
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] eCurrentState = ", GET_LINE_MINIGAME_STATE_AS_STRING(minigameData.eCurrentState))
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] iFlags = ", minigameData.iFlags)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] iMusicTitleScreen = ", minigameData.iMusicTitleScreen)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] fAspectRatio = ", minigameData.fAspectRatio)
		CDEBUG1LN(DEBUG_MINIGAME, "[LINE_MINIGAME] fAspectRatioModifier = ", minigameData.fAspectRatioModifier)
		
		//Player-specific data.
		INT iPlayer
		FOR iPlayer = 0 TO MAX_NUM_PLAYERS - 1
			LINE_MINIGAME_DUMP_PLAYER_DATA_TO_CONSOLE(minigameData.PlayerData, iPlayer)
		ENDFOR
	ENDPROC

	PROC DRAW_LITERAL_STRING_AT_POS(STRING strText, FLOAT fPosX, FLOAT fPosY)
		SET_TEXT_SCALE(0.25, 0.25)
		SET_TEXT_DROP_SHADOW()
		DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX, fPosY, "STRING", strText)
	ENDPROC

	/// PURPOSE:
	///    Draws some info about the minigame state and data on the screen.
	PROC LINE_MINIGAME_DRAW_DATA_ON_SCREEN(LINE_MINIGAME_DATA &minigameData)
		DRAW_RECT(0.175, 0.5, 0.3, 0.95, 0, 0, 0, 192)
	
		TEXT_LABEL_63 strCurrentState = "eCurrentState: "
		strCurrentState += GET_LINE_MINIGAME_STATE_AS_STRING(minigameData.eCurrentState)
		DRAW_LITERAL_STRING_AT_POS(strCurrentState, 0.05, 0.05)
		
		TEXT_LABEL_63 strAspectRatio = "fAspectRatio: "
		strAspectRatio += GET_STRING_FROM_FLOAT(minigameData.fAspectRatio)
		DRAW_LITERAL_STRING_AT_POS(strAspectRatio, 0.05, 0.075)
		
		TEXT_LABEL_63 strAspectRatioModifier = "fAspectRatioModifier: "
		strAspectRatioModifier += GET_STRING_FROM_FLOAT(minigameData.fAspectRatioModifier)
		DRAW_LITERAL_STRING_AT_POS(strAspectRatioModifier, 0.05, 0.1)
		
		FLOAT fCurrentY = 0.125
		INT iPlayer
		FOR iPlayer = 0 TO MAX_NUM_PLAYERS - 1
			TEXT_LABEL_63 strPlayerState = "playerData[iPlayer].eCurrentState: "
			strPlayerState += GET_LINE_MINIGAME_PLAYER_STATE_AS_STRING(minigameData.PlayerData[iPlayer].eCurrentState)
			DRAW_LITERAL_STRING_AT_POS(strPlayerState, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerStartPos = "LINE_PlayerBD[iPlayer].iStartPos: "
			strPlayerStartPos += LINE_PlayerBD[iPlayer].iStartPos
			DRAW_LITERAL_STRING_AT_POS(strPlayerStartPos, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerDirection = "playerData[iPlayer].iDirection: "
			strPlayerDirection += minigameData.PlayerData[iPlayer].iDirection
			DRAW_LITERAL_STRING_AT_POS(strPlayerDirection, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerNextDirection = "playerData[iPlayer].iNextDirection: "
			strPlayerNextDirection += minigameData.PlayerData[iPlayer].iNextDirection
			DRAW_LITERAL_STRING_AT_POS(strPlayerNextDirection, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerHeadX = "playerData[iPlayer].fHeadX: "
			strPlayerHeadX += GET_STRING_FROM_FLOAT(minigameData.PlayerData[iPlayer].fHeadX)
			DRAW_LITERAL_STRING_AT_POS(strPlayerHeadX, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerHeadY = "playerData[iPlayer].fHeadY: "
			strPlayerHeadY += GET_STRING_FROM_FLOAT(minigameData.PlayerData[iPlayer].fHeadY)
			DRAW_LITERAL_STRING_AT_POS(strPlayerHeadY, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerHeadRotation = "playerData[iPlayer].fHeadRotation: "
			strPlayerHeadRotation += GET_STRING_FROM_FLOAT(minigameData.PlayerData[iPlayer].fHeadRotation)
			DRAW_LITERAL_STRING_AT_POS(strPlayerHeadRotation, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerTailIndex = "playerData[iPlayer].iTailIndex: "
			strPlayerTailIndex += minigameData.PlayerData[iPlayer].iTailIndex
			DRAW_LITERAL_STRING_AT_POS(strPlayerTailIndex, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerMicStatus = "mic status: "
			strPlayerMicStatus += LINE_MINIGAME_GET_MIC_STATUS_FOR_PLAYER_POSITION(LINE_PlayerBD[iPlayer].iStartPos)
			DRAW_LITERAL_STRING_AT_POS(strPlayerMicStatus, 0.05, fCurrentY)
			fCurrentY += 0.02
			
			TEXT_LABEL_63 strPlayerFlags = "playerData[iPlayer].iFlags: "
			strPlayerFlags += minigameData.PlayerData[iPlayer].iFlags
			DRAW_LITERAL_STRING_AT_POS(strPlayerFlags, 0.05, fCurrentY)
			fCurrentY += 0.025
		ENDFOR
		
		
	ENDPROC

	/// PURPOSE:
	///    Updates all debug functionality for the minigame.
	/// PARAMS:
	///    minigameData - 
	PROC LINE_MINIGAME_UPDATE_DEBUG(LINE_MINIGAME_DATA &minigameData)
		LINE_MINIGAME_CREATE_WIDGETS()
		
		//Draw a debug rectangle: used for positioning tests.
		IF bDebugDrawTestRect
			DRAW_RECT(fDebugTestRectPosX, fDebugTEstRectPosY, 0.002, 0.002 * minigameData.fAspectRatio, 255, 255, 255, 255)
		ENDIF
		
		//Display some debug info in the top left, and allow easily toggling on/off.
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "Toggle line minigame debug")
			bDebugDisplayDataOnScreen = NOT bDebugDisplayDataOnScreen
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD2, KEYBOARD_MODIFIER_SHIFT, "Toggle collision debug")
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Toggling collision debug", 5000, 0)
			bDebugDisplayCollisionInfo = NOT bDebugDisplayCollisionInfo
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD3, KEYBOARD_MODIFIER_SHIFT, "Slow player")
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Enabling slow mode for local player.", 5000, 0)
			DEFAULT_SPEED = 0.004
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD4, KEYBOARD_MODIFIER_SHIFT, "Display splash timer")
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Splash timer enabled.", 5000, 0)
			bDebugEnableSplashTimer = NOT bDebugEnableSplashTimer
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_SHIFT, "Hide minigame UI")
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Toggled minigame UI.", 5000, 0)
			bDebugHideMinigameUI = NOT bDebugHideMinigameUI
		ENDIF
		
		IF bDebugDisplayDataOnScreen
			LINE_MINIGAME_DRAW_DATA_ON_SCREEN(minigameData)
		ENDIF
		
		IF bDebugDisplayCollisionInfo
			//The placement of this command during normal play means that the debug inside draws underneat other sprites, so just
			//call it again here.
			HAS_PLAYER_COLLIDED_WITH_SIDE_WALLS(minigameData, minigameData.PlayerData, iLocalplayer)
			PROCESS_OTHER_PLAYER_COLLISIONS(minigameData, minigameData.PlayerData, FALSE)
		ENDIF
		
		IF bDebugEnableInputTest
			FLOAT fLeftStickX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			FLOAT fLeftStickY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			
			IF fLeftStickY < -STICK_THRESHOLD
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				DRAW_RECT(0.5, 0.4, 0.05, 0.05 * minigameData.fAspectRatio, 255, 255, 255, 255)
			ENDIF
			
			IF fLeftStickY > STICK_THRESHOLD
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				DRAW_RECT(0.5, 0.6, 0.05, 0.05 * minigameData.fAspectRatio, 255, 255, 255, 255)
			ENDIF
			
			IF fLeftStickX < -STICK_THRESHOLD
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				DRAW_RECT(0.4, 0.5, 0.05, 0.05 * minigameData.fAspectRatio, 255, 255, 255, 255)
			ENDIF
			
			IF fLeftStickX > STICK_THRESHOLD
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				DRAW_RECT(0.6, 0.5, 0.05, 0.05 * minigameData.fAspectRatio, 255, 255, 255, 255)
			ENDIF
		ENDIF
		
		IF bDebugHideMinigameUI
			DEFAULT_SPEED = 0.001
		ENDIF
		
		//If a bug is entered dump all data to the console.
		IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)	
			LINE_MINIGAME_DUMP_DATA_TO_CONSOLE(minigameData)
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE:
///    Main update: call every frame to run the minigame.
PROC UPDATE_LINE_MINIGAME(LINE_MINIGAME_DATA &minigameData)
	#IF IS_DEBUG_BUILD
	INT i
	FOR i = 0 TO 4
		CDEBUG3LN(DEBUG_MINIGAME, "[LINE_MINIGAME] iDrawnSprites[",i,"] = ",iDrawnSprites[i])
		iDrawnSprites[i] = 0
	ENDFOR
	#ENDIF
	
	IF LINE_ServerBD.eCurrentState < LINE_MINIGAME_CLEANUP
		LINE_MINIGAME_GLOBAL_PRE_UPDATE(minigameData)
	ENDIF

	//Client State
	SWITCH minigameData.eCurrentState
		CASE LINE_MINIGAME_INITIALISING
			IF LINE_ServerBD.eCurrentState > LINE_MINIGAME_TITLE_SCREEN
			AND NOT (LINE_ServerBD.eCurrentState = LINE_MINIGAME_REPLAY_SCREEN)
				SET_LINE_MINIGAME_STATE_CLIENT(minigameData, LINE_MINIGAME_CLEANUP)
				EXIT
			ENDIF
			LINE_MINIGAME_INITIALISE_CLIENT(minigameData)
		BREAK
			
		CASE LINE_MINIGAME_REQUESTING_ASSETS
			LINE_MINIGAME_REQUEST_ASSETS_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_LAUNCHING
			LINE_MINIGAME_LAUNCH_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_TITLE_SCREEN
			LINE_MINIGAME_UPDATE_TITLE_SCREEN_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_RESHUFFLE
			LINE_MINIGAME_RESHUFFLE_PLAYERS_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_READY_SCREEN
			LINE_MINIGAME_UPDATE_READY_SCREEN_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_COUNTDOWN_SCREEN
			LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_RUNNING
			LINE_MINIGAME_UPDATE_MAIN_GAME_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_WINNER_SCREEN
			LINE_MINIGAME_UPDATE_WINNER_SCREEN_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_REPLAY_SCREEN
			LINE_MINIGAME_UPDATE_REPLAY_SCREEN_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_GAME_OVER_SCREEN
			LINE_MINIGAME_UPDATE_GAME_OVER_SCREEN_CLIENT(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_CLEANUP
			LINE_MINIGAME_CLEANUP_AND_EXIT_CLIENT(minigameData)
		BREAK
	
	ENDSWITCH
	
	//Server State
	SWITCH LINE_ServerBD.eCurrentState
		CASE LINE_MINIGAME_INITIALISING
			LINE_MINIGAME_INITIALISE(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_REQUESTING_ASSETS
			LINE_MINIGAME_REQUEST_ASSETS(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_LAUNCHING
			LINE_MINIGAME_LAUNCH(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_TITLE_SCREEN
			LINE_MINIGAME_UPDATE_TITLE_SCREEN(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_RESHUFFLE
			LINE_MINIGAME_RESHUFFLE_PLAYERS(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_READY_SCREEN
			LINE_MINIGAME_UPDATE_READY_SCREEN(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_COUNTDOWN_SCREEN
			LINE_MINIGAME_UPDATE_COUNTDOWN_SCREEN(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_RUNNING
			LINE_MINIGAME_UPDATE_MAIN_GAME(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_WINNER_SCREEN
			LINE_MINIGAME_UPDATE_WINNER_SCREEN(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_REPLAY_SCREEN
			LINE_MINIGAME_UPDATE_REPLAY_SCREEN(minigameData)
		BREAK
		
		CASE LINE_MINIGAME_GAME_OVER_SCREEN

		BREAK
		
		CASE LINE_MINIGAME_CLEANUP
			
		BREAK
	ENDSWITCH
	
	LINE_MINIGAME_GLOBAL_POST_UPDATE(minigameData)
	
	#IF IS_DEBUG_BUILD
		LINE_MINIGAME_UPDATE_DEBUG(minigameData)
	#ENDIF
ENDPROC
