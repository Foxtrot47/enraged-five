USING "globals.sch"
USING "commands_camera.sch"
USING "minigames_helpers.sch"
USING "script_usecontext.sch"

CONST_FLOAT MG_BIG_MESSAGE_ANIM_TIME		0.15
CONST_FLOAT MG_BIG_MESSAGE_ANIM_TIME_SLOW	0.3

ENUM MG_BIG_MESSAGE_TYPE
	MG_BIG_MESSAGE_WASTED,
	MG_BIG_MESSAGE_BUSTED,
	MG_BIG_MESSAGE_CENTERED,
	MG_BIG_MESSAGE_CENTERED_TOP,
	MG_BIG_MESSAGE_MIDSIZED,
	MG_BIG_MESSAGE_MISSION_END,
	MG_BIG_MESSAGE_MISSION_PASSED,
	MG_BIG_MESSAGE_MISSION_FAILED	
ENDENUM

/// PURPOSE: Used for the splash state
ENUM MG_FAIL_SPLASH_STATE
	FAIL_SPLASH_STATE_INVALID = -1,
	FAIL_SPLASH_STATE_INIT,
	FAIL_SPLASH_STATE_DISPLAY,
	FAIL_SPLASH_STATE_WAIT_FOR_INPUT,
	FAIL_SPLASH_STATE_TRANSITION_OUT_INIT,
	FAIL_SPLASH_STATE_TRANSITION_OUT
ENDENUM

ENUM MG_FAIL_SPLASH_FLAGS
	FAIL_SPLASH_EMPTY = 0,
	FAIL_SPLASH_ENABLE_PLAYER_CONTROL = BIT0,
	FAIL_SPLASH_ALLOW_ABANDON	= BIT1,
	FAIL_SPLASH_ENABLE_SOUNDS	= BIT2,
	FAIL_SPLASH_ENABLE_FX		= BIT3,
	FAIL_SPLASH_HIDE_BUTTONS	= BIT4,
	FAIL_SPLASH_ANIMATE_SUB		= BIT5,
	FAIL_SPLASH_FADE_ON_RETRY	= BIT6,
	
	FAIL_SPLASH_DEFAULT_FLAGS = BIT1 + BIT2 + BIT3 + BIT6// + BIT5//allow abandon, enable sounds, enable vfx, DON'T animate sub, fade out on retry. (dont hide buttons or enable player control)
ENDENUM

STRUCT MG_FAIL_SPLASH
	MG_FAIL_SPLASH_STATE eState
	structTimer	ButtonInput
	structTimer AbandonSplash
	structTimer tStraplineTimer
	SIMPLE_USE_CONTEXT	context
	FLOAT fTransitionUpTime, fButtonDelay
	BOOL bAnimStarted
	BOOL bTransOut
	BOOL bRetry
ENDSTRUCT

//RANKUP AND WEAPON PUCHASE IS NOT SUPPORTED!!! (BUT CAN BE IF NEEDED) -AsD

STRUCT SCRIPT_SCALEFORM_BIG_MESSAGE
	SCALEFORM_INDEX siMovie
	INT 			iDuration
	structTimer		movieTimer
ENDSTRUCT

FUNC SCALEFORM_INDEX REQUEST_MG_BIG_MESSAGE()
	RETURN REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
ENDFUNC

FUNC SCALEFORM_INDEX REQUEST_MIDSIZED_MESSAGE()
	RETURN REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
ENDFUNC

PROC CLEANUP_MG_BIG_MESSAGE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct)
	IF HAS_SCALEFORM_MOVIE_LOADED(scaleformStruct.siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformStruct.siMovie)
		scaleformStruct.siMovie = NULL
	ENDIF
ENDPROC

FUNC STRING GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(MG_BIG_MESSAGE_TYPE eMessageType)
	SWITCH eMessageType
		CASE MG_BIG_MESSAGE_WASTED
			RETURN "SHOW_WASTED_MP_MESSAGE"		
		CASE MG_BIG_MESSAGE_BUSTED
			RETURN "SHOW_BUSTED_MP_MESSAGE"
		CASE MG_BIG_MESSAGE_CENTERED
			RETURN "SHOW_CENTERED_MP_MESSAGE_LARGE"
		CASE MG_BIG_MESSAGE_CENTERED_TOP
			RETURN "SHOW_CENTERED_TOP_MP_MESSAGE"
		CASE MG_BIG_MESSAGE_MIDSIZED
			RETURN "SHOW_MIDSIZED_MESSAGE"
		CASE MG_BIG_MESSAGE_MISSION_END
			RETURN "SHOW_MISSION_END_MP_MESSAGE"
		CASE MG_BIG_MESSAGE_MISSION_PASSED
			RETURN "SHOW_MISSION_PASSED_MESSAGE"
		CASE MG_BIG_MESSAGE_MISSION_FAILED
			RETURN "SHOW_MISSION_FAILED_MP_MESSAGE"
		
		DEFAULT
			PRINTLN("minigame_big_message - GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM - Could't find a matching string. Returning default")
			RETURN "SHOW_CENTERED_MP_MESSAGE_LARGE"
	ENDSWITCH
ENDFUNC


/// PURPOSE:
///    Set big message for a given duration
/// PARAMS:
///    scaleformStruct - 
///    labeToDisp - big message text label
///    strapLine - byline for the big message
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_BIG_MESSAGE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, MG_BIG_MESSAGE_TYPE eMessageType = MG_BIG_MESSAGE_CENTERED, BOOL bAnimated = FALSE, FLOAT fAnimationDuration = 0.0)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(eMessageType)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF NOT IS_STRING_NULL_OR_EMPTY(strapline)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
		ENDIF
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF		
	END_SCALEFORM_MOVIE_METHOD()
		
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimationDuration)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message with a number token
///    UPDATE: this has been changed to support hud coloring.  details in the params.
/// PARAMS:
///    scaleformStruct - local big message scaleform
///    labeToDisp - main splash text string - this needs to be in this format: ~a~ ~1~
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_BIG_MESSAGE_WITH_NUMBER(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, MG_BIG_MESSAGE_TYPE eMessageType = MG_BIG_MESSAGE_CENTERED, BOOL bAnimated = FALSE, FLOAT fAnimationDuration = 0.0)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(eMessageType)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("GEN_BIGM_NUM")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF NOT IS_STRING_NULL_OR_EMPTY(strapline)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
		ENDIF
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
		
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimationDuration)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message with a number token
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, MG_BIG_MESSAGE_TYPE eMessageType = MG_BIG_MESSAGE_CENTERED, BOOL bAnimated = FALSE, FLOAT fAnimationDuration = 0.0)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(eMessageType)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
		
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimationDuration)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message with a number token
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Time in milliseconds to display in the strapline.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_BIG_MESSAGE_WITH_TIME_IN_STRAPLINE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, MG_BIG_MESSAGE_TYPE eMessageType = MG_BIG_MESSAGE_CENTERED, BOOL bAnimated = FALSE, FLOAT fAnimationDuration = 0.0)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(eMessageType)
	
	IF IS_STRING_EMPTY_HUD(strapLine)
		strapLine = "STRING"
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(iNum, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
		
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimationDuration)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message for a given duration
/// PARAMS:
///    scaleformIndex - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_BIG_MESSAGE_WITH_STRING_IN_STRAPLINE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine, STRING subString, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, MG_BIG_MESSAGE_TYPE eMessageType = MG_BIG_MESSAGE_CENTERED, BOOL bAnimated = FALSE, FLOAT fAnimationDuration = 0.0)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(eMessageType)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(subString)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
		
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimationDuration)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message for a given duration using the player's username/gamertag
///    Used mainly in MP
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine, STRING sPlayerName, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW, MG_BIG_MESSAGE_TYPE eMessageType = MG_BIG_MESSAGE_CENTERED, BOOL bAnimated = FALSE, FLOAT fAnimationDuration = 0.0)
	STRING sMethodName = GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM(eMessageType)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF bAnimated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
		
	IF bAnimated
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimationDuration)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Renders a scaleform splash movie.
/// RETURNS:
///    FALSE when the movie is done drawing and you should move on.
FUNC BOOL UPDATE_SCALEFORM_BIG_MESSAGE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct, BOOL bAllowSkip = FALSE, BOOL bDrawHigh=FALSE)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	ENDIF
	
	// Hide the reticle.
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	
	// Draw the moobie.
	//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	IF NOT bDrawHigh
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleformStruct.siMovie, 255, 255, 255, 255)
	ELIF bDrawHigh
		// Force all movies to use fullscreen now
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleformStruct.siMovie, 255, 255, 255, 255)
	ENDIF
	
	// If we can skip, detect that button press
	IF bAllowSkip
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// When it's no longer visible, return FALSE. -- Changed. Now, when its curation is up, return FALSE
	IF (scaleformStruct.iDuration = -1)
		RETURN TRUE
	ENDIF

	// Is it time to leave?
	IF (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 > TO_FLOAT(scaleformStruct.iDuration))
		CANCEL_TIMER(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if this scaleform splash would be displaying. Mainly for Golf, as golf was using an odd method to check this.
FUNC BOOL WOULD_SCALEFORM_BIG_MESSAGE_BE_VISIBLE(SCRIPT_SCALEFORM_BIG_MESSAGE & scaleformStruct)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 <= TO_FLOAT(scaleformStruct.iDuration))
ENDFUNC

/// PURPOSE:
///    Call this prior to MG_UPDATE_FAIL_SPLASH_SCREEN
/// PARAMS:
///    splash - the splash struct to be used for the splash screen
PROC MG_INIT_FAIL_SPLASH_SCREEN(MG_FAIL_SPLASH& splash, FLOAT fTransitionUpTime = 0.3, FLOAT fButtonDelay = 2.0)
	IF IS_TIMER_STARTED(splash.ButtonInput)
		CANCEL_TIMER(splash.ButtonInput)
	ENDIF
	
	IF IS_TIMER_STARTED(splash.AbandonSplash)
		CANCEL_TIMER(splash.AbandonSplash)
	ENDIF
	
	CLEANUP_SIMPLE_USE_CONTEXT(splash.context)
	splash.fTransitionUpTime = fTransitionUpTime
	splash.fButtonDelay = fButtonDelay
	splash.bTransOut = TRUE
	splash.bAnimStarted = FALSE
	splash.eState = FAIL_SPLASH_STATE_INIT
ENDPROC


/// PURPOSE:
///    Updates and runs the Fail splash screen UI for all mini games. It uses a state machine, so you will need to create an MG_FAIL_SPLASH struct inside your scripts as well as a 
///    SCRIPT_SCALEFORM_BIG_MESSAGE struct. You will need to call this until it returns true. Once it does, catch bShouldRestart which will tell you what the player has chosen.
///    Call MG_INIT_FAIL_SPLASH_SCREEN before calling this
/// PARAMS:
///    scaleformStruct - the big message struct, necessary for the scaleform movie. Make sure this persists
///    splashStruct - the MG_FAIL_SPLASH struct that is necessary for the state machine. Make sure this persists
///    strapLine - the strapline text (reserved for fail reasons)
///    bShouldRetry - this will return whether the player has chosen to retry or not. 
///    eFlags - Flags that you wish to enable for the function. Can be found in MG_FAIL_SPLASH_FLAGS
///    fAbandonTime - the amount of time before the fail screen clears and returns FALSE in the bShouldRetry. (Only applicable if you have chosen to allow abandoning)
/// RETURNS:
///    True once the player has made a choice
FUNC BOOL MG_UPDATE_FAIL_SPLASH_SCREEN(SCRIPT_SCALEFORM_BIG_MESSAGE& scaleformStruct, MG_FAIL_SPLASH& splashStruct, STRING sFailString, STRING sFailStrapline, BOOL& bShouldRetry, MG_FAIL_SPLASH_FLAGS eFlags = FAIL_SPLASH_DEFAULT_FLAGS, GFX_DRAW_ORDER drawOrder = GFX_ORDER_AFTER_FADE, BOOL bButtonsIgnoreFade = TRUE, FLOAT fAbandonTime = 15.0, BOOL bAllowSetNoLoadingScreen = TRUE)
	SWITCH splashStruct.eState
		CASE FAIL_SPLASH_STATE_INIT
			//Fade screen out, slow time
			IF NOT IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
				REPLAY_PREVENT_RECORDING_THIS_FRAME()	//B* 2447830
			ENDIF
			IF bAllowSetNoLoadingScreen
				PRINTLN("MG_UPDATE_FAIL_SPLASH_SCREEN() - Setting SET_NO_LOADING_SCREEN(TRUE)")
				SET_NO_LOADING_SCREEN(TRUE)
			ENDIF
			SET_TIME_SCALE(FAIL_EFFECT_SLO_MO)
			IF IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_SOUNDS )
				IF REQUEST_SCRIPT_AUDIO_BANK("generic_failed")
					splashStruct.eState = FAIL_SPLASH_STATE_DISPLAY
				ENDIF
			ELSE
				splashStruct.eState = FAIL_SPLASH_STATE_DISPLAY
			ENDIF
		BREAK
		
		CASE FAIL_SPLASH_STATE_DISPLAY
			BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_CENTERED_MP_MESSAGE_LARGE")
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_RED)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sFailString)
				END_TEXT_COMMAND_SCALEFORM_STRING()
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sFailStrapline)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			
			IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, FAIL_SPLASH_ANIMATE_SUB)
				IF NOT splashStruct.bAnimStarted
					BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(splashStruct.fTransitionUpTime)
					END_SCALEFORM_MOVIE_METHOD()
					splashStruct.bAnimStarted = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_PLAYER_CONTROL )
				DISABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )
			ENDIF
			
			INIT_SIMPLE_USE_CONTEXT(splashStruct.context, FALSE, TRUE, TRUE, TRUE)
			ADD_SIMPLE_USE_CONTEXT_INPUT(splashStruct.context, "IB_RETRY", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			ADD_SIMPLE_USE_CONTEXT_INPUT(splashStruct.context, "FE_HLP16", FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			
			IF IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_SOUNDS )
				PLAY_SOUND_FRONTEND(-1, "ScreenFlash", "MissionFailedSounds")
			ENDIF
			
			IF IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_FX )
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						ANIMPOSTFX_PLAY( "MinigameEndMichael", 500, FALSE )
					BREAK
					CASE CHAR_FRANKLIN
						ANIMPOSTFX_PLAY( "MinigameEndFranklin", 500, FALSE )
					BREAK
					CASE CHAR_TREVOR
						ANIMPOSTFX_PLAY( "MinigameEndTrevor", 500, FALSE )
					BREAK
				ENDSWITCH
			ENDIF
			
			IF NOT IS_TIMER_STARTED(splashStruct.ButtonInput)
				START_TIMER_NOW(splashStruct.ButtonInput)
			ENDIF
			
			IF IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ALLOW_ABANDON )
				IF NOT IS_TIMER_STARTED(splashStruct.AbandonSplash)
					START_TIMER_NOW(splashStruct.AbandonSplash)
				ENDIF
			ENDIF
			
			splashStruct.eState = FAIL_SPLASH_STATE_WAIT_FOR_INPUT
		BREAK
		
		CASE FAIL_SPLASH_STATE_WAIT_FOR_INPUT
			
			HIDE_LOADING_ON_FADE_THIS_FRAME()
			
			IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, FAIL_SPLASH_ANIMATE_SUB)
				IF NOT splashStruct.bAnimStarted
					BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_UP")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(splashStruct.fTransitionUpTime)
					END_SCALEFORM_MOVIE_METHOD()
					splashStruct.bAnimStarted = TRUE
				ENDIF
			ENDIF
			
			SET_SCRIPT_GFX_DRAW_ORDER(drawOrder)
			UPDATE_SCALEFORM_BIG_MESSAGE(scaleformStruct)
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, FAIL_SPLASH_HIDE_BUTTONS) AND 
			(GET_TIMER_IN_SECONDS(splashStruct.ButtonInput) >= splashStruct.fButtonDelay OR IS_SCREEN_FADED_OUT())
				
				UPDATE_SIMPLE_USE_CONTEXT( splashStruct.context, DEFAULT, drawOrder, bButtonsIgnoreFade)
				SET_MOUSE_CURSOR_THIS_FRAME()
				
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					splashStruct.bRetry = TRUE
					PLAY_SOUND_FRONTEND(-1, "YES", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					IF NOT IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_PLAYER_CONTROL )
						ENABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )
					ENDIF
					CLEANUP_SIMPLE_USE_CONTEXT(splashStruct.context)
					splashStruct.eState = FAIL_SPLASH_STATE_TRANSITION_OUT_INIT
					RETURN FALSE
				ELIF IS_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL )
					splashStruct.bRetry = FALSE
					PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					IF NOT IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_PLAYER_CONTROL )
						ENABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )
					ENDIF
					CLEANUP_SIMPLE_USE_CONTEXT(splashStruct.context)
					splashStruct.eState = FAIL_SPLASH_STATE_TRANSITION_OUT_INIT
					RETURN FALSE
				ENDIF
			ENDIF
			IF IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ALLOW_ABANDON )
				IF GET_TIMER_IN_SECONDS(splashStruct.AbandonSplash) >= fAbandonTime
					splashStruct.bRetry = FALSE
					PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					IF NOT IS_BITMASK_ENUM_AS_ENUM_SET( eFlags, FAIL_SPLASH_ENABLE_PLAYER_CONTROL )
						ENABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )
					ENDIF
					CLEANUP_SIMPLE_USE_CONTEXT(splashStruct.context)
					splashStruct.eState = FAIL_SPLASH_STATE_TRANSITION_OUT_INIT
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE FAIL_SPLASH_STATE_TRANSITION_OUT_INIT
			UPDATE_SCALEFORM_BIG_MESSAGE(scaleformStruct)
		
			//No longer required as minigames don't transition out of fail screen
//			BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "TRANSITION_OUT")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.5)
//			END_SCALEFORM_MOVIE_METHOD()
			
			//Set time scale back, fade-in screen
			SET_TIME_SCALE(1)
			
			//Fix for 2100094: Don't fade in after selecting to exit from the stunt plane time
			//trials or pilot school fail screen. If the player dies during the fail sequence 
			//it is possible to see code respawn the player. If we stay faded until the menu fades
			//us back in we can hide the issue. There was also no reason to fade in anyway. -BenR
			IF splashStruct.bRetry OR NOT (	ARE_STRINGS_EQUAL("stunt_plane_races", GET_THIS_SCRIPT_NAME()) OR 
											ARE_STRINGS_EQUAL("pilot_school", GET_THIS_SCRIPT_NAME()) OR
											(ARE_STRINGS_EQUAL("bj", GET_THIS_SCRIPT_NAME()) AND IS_PED_INJURED(PLAYER_PED_ID()) ))
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
			ENDIF
			
			IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, FAIL_SPLASH_FADE_ON_RETRY)
				AND splashStruct.bRetry
				DO_SCREEN_FADE_OUT(500)
			ENDIF
			
			RESTART_TIMER_NOW(splashStruct.AbandonSplash)
			IF bAllowSetNoLoadingScreen
				PRINTLN("MG_UPDATE_FAIL_SPLASH_SCREEN() - Clearing SET_NO_LOADING_SCREEN(FALSE)")
				SET_NO_LOADING_SCREEN(FALSE)
			ENDIF
			splashStruct.eState = FAIL_SPLASH_STATE_TRANSITION_OUT
		BREAK
		CASE FAIL_SPLASH_STATE_TRANSITION_OUT
			IF GET_TIMER_IN_SECONDS(splashStruct.AbandonSplash) <= 0.1
				UPDATE_SCALEFORM_BIG_MESSAGE(scaleformStruct)
			ELSE
				bShouldRetry = splashStruct.bRetry
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------
//
//	Fade out effect stuff
//
//-----------------------------------------------------------------------------------------------------------------------------
ENUM MG_FAIL_FADE_EFFECT_STATE
	MG_FAIL_FADE_EFFECT_STATE_INVALID = -1,
	MG_FAIL_FADE_EFFECT_STATE_INIT,
	MG_FAIL_FADE_EFFECT_STATE_UPDATE,
	MG_FAIL_FADE_EFFECT_STATE_WAIT_FOR_INPUT,
	MG_FAIL_FADE_EFFECT_STATE_WAIT_FOR_RESTART,
	MG_FAIL_FADE_EFFECT_IDLE
ENDENUM

ENUM MG_FAIL_FADE_EFFECT_FLAGS
	MG_FAIL_FADE_EFFECT_NONE = 0,
	MG_FAIL_FADE_TRIGGERED_TEXT_HIT_SFX = BIT1,
	MG_FAIL_FADE_TRIGGERED_BED_SFX = BIT2
ENDENUM

STRUCT MG_FAIL_FADE_EFFECT
	MG_FAIL_FADE_EFFECT_STATE efailFadeEffectState
	structTimer	tProgressTimer
	INT iFlags
	BOOL bHasMenu
ENDSTRUCT

PROC SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(BOOL bEnable)
	IF bEnable
		//setup for escaping off mission death
		SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
		PAUSE_DEATH_ARREST_RESTART(TRUE)
		SET_FADE_OUT_AFTER_DEATH(FALSE)
		SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(TRUE)
		g_bBlockWastedTitle = TRUE	
	ELSE
		//setup for off mission death
		SET_FADE_IN_AFTER_DEATH_ARREST(TRUE)
		PAUSE_DEATH_ARREST_RESTART(FALSE)
		SET_FADE_OUT_AFTER_DEATH(TRUE)
		SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)
		g_bBlockWastedTitle = FALSE	
	ENDIF

ENDPROC

PROC MG_INIT_FAIL_FADE_EFFECT(MG_FAIL_FADE_EFFECT& failFadeEffect, BOOL bHasMenu = FALSE)
	failFadeEffect.efailFadeEffectState = MG_FAIL_FADE_EFFECT_STATE_INIT
	failFadeEffect.iFlags = 0
	failFadeEffect.bHasMenu = bHasMenu
	REQUEST_SCRIPT_AUDIO_BANK("OFFMISSION_WASTED")
ENDPROC

FUNC BOOL MG_UPDATE_FAIL_FADE_EFFECT(MG_FAIL_FADE_EFFECT& failFadeEffect, MG_FAIL_SPLASH& failSplash, SCRIPT_SCALEFORM_BIG_MESSAGE& bigMessage, STRING sTitle, STRING sStrapLine, BOOL& bShouldRetry, MG_FAIL_SPLASH_FLAGS eSplashFlags = FAIL_SPLASH_DEFAULT_FLAGS)
	
	
	SWITCH failFadeEffect.efailFadeEffectState
	
		CASE MG_FAIL_FADE_EFFECT_STATE_INIT
			failFadeEffect.efailFadeEffectState = MG_FAIL_FADE_EFFECT_STATE_UPDATE
			START_AUDIO_SCENE("DEATH_SCENE")
			PLAY_SOUND_FRONTEND(-1, "ScreenFlash", "WastedSounds")
			START_TIMER_NOW_SAFE(failFadeEffect.tProgressTimer)
			MG_RESET_FAIL_EFFECT_VARS()
			MG_INIT_FAIL_SPLASH_SCREEN(failSplash, 0.15 * FAIL_OUT_EFFECT_SLO_MO, 0.5)
			SET_NO_LOADING_SCREEN(TRUE) 
			BREAK	
		
		CASE MG_FAIL_FADE_EFFECT_STATE_UPDATE	
			IF MG_DO_FAIL_BLUR_EFFECT() OR IS_SCREEN_FADED_OUT()
				
				failFadeEffect.efailFadeEffectState = MG_FAIL_FADE_EFFECT_STATE_WAIT_FOR_INPUT
			ENDIF
			IF NOT IS_BITMASK_AS_ENUM_SET(failFadeEffect.iFlags, MG_FAIL_FADE_TRIGGERED_BED_SFX)
				IF REQUEST_SCRIPT_AUDIO_BANK("OFFMISSION_WASTED")
					PLAY_SOUND_FRONTEND(-1, "Bed", "WastedSounds")
					SET_BITMASK_AS_ENUM(failFadeEffect.iFlags, MG_FAIL_FADE_TRIGGERED_BED_SFX)
				ENDIF
			ENDIF

			IF TIMERA() > 1500 * FAIL_EFFECT_SLO_MO
				PRINTLN("we should be showing the splash now!!!!")
				IF NOT IS_BITMASK_AS_ENUM_SET(failFadeEffect.iFlags, MG_FAIL_FADE_TRIGGERED_TEXT_HIT_SFX)
					PLAY_SOUND_FRONTEND(-1, "TextHit", "WastedSounds") //play this once
					SET_BITMASK_AS_ENUM(failFadeEffect.iFlags, MG_FAIL_FADE_TRIGGERED_TEXT_HIT_SFX)
				ENDIF	
				MG_UPDATE_FAIL_SPLASH_SCREEN(bigMessage, failSplash, sTitle, sStrapLine, bShouldRetry, eSplashFlags - (FAIL_SPLASH_ENABLE_SOUNDS & eSplashFlags) - (FAIL_SPLASH_ALLOW_ABANDON & eSplashFlags) /*- (FAIL_SPLASH_ANIMATE_SUB & eSplashFlags)*/| FAIL_SPLASH_HIDE_BUTTONS, GFX_ORDER_AFTER_FADE, TRUE, DEFAULT, FALSE)
			ENDIF
			BREAK
			
		CASE MG_FAIL_FADE_EFFECT_STATE_WAIT_FOR_INPUT
			IF MG_UPDATE_FAIL_SPLASH_SCREEN(bigMessage, failSplash, sTitle, sStrapLine, bShouldRetry, eSplashFlags - (FAIL_SPLASH_ENABLE_FX & eSplashFlags) - (FAIL_SPLASH_ENABLE_SOUNDS & eSplashFlags) - (FAIL_SPLASH_ALLOW_ABANDON & eSplashFlags), GFX_ORDER_AFTER_FADE, TRUE)
				MG_DO_FAIL_EFFECT(FALSE, TRUE)
				MG_DO_FAIL_OUT_EFFECT(FALSE, TRUE)
				MG_RESET_FAIL_EFFECT_VARS()
				IF bShouldRetry
					IGNORE_NEXT_RESTART(TRUE)
				ELSE
					IF NOT failFadeEffect.bHasMenu //if we have a menu, skip this.
						SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)
					ENDIF
				ENDIF
				SET_NO_LOADING_SCREEN(FALSE) 
				PAUSE_DEATH_ARREST_RESTART(FALSE)
				failFadeEffect.efailFadeEffectState = MG_FAIL_FADE_EFFECT_STATE_WAIT_FOR_RESTART
			ENDIF
			BREAK
		
		CASE MG_FAIL_FADE_EFFECT_STATE_WAIT_FOR_RESTART
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_FADE_IN_AFTER_DEATH_ARREST(TRUE)
				MG_DO_FAIL_EFFECT(FALSE, TRUE)
				MG_DO_FAIL_OUT_EFFECT(FALSE, TRUE)
				STOP_AUDIO_SCENE("DEATH_SCENE")
				SET_NO_LOADING_SCREEN(FALSE)	
				RETURN TRUE
			ELSE
				PRINTLN("player not playing")
			ENDIF
//			RETURN TRUE
			BREAK
		
		CASE MG_FAIL_FADE_EFFECT_IDLE
			PRINTLN("finished")
			SET_NO_LOADING_SCREEN(FALSE)
			RETURN TRUE
			BREAK
			
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MG_CLEANUP_FAIL_FADE_EFFECT()
	MG_RESET_FAIL_EFFECT_VARS()
	MG_DO_FAIL_EFFECT(FALSE, TRUE)
	MG_DO_FAIL_OUT_EFFECT(FALSE, TRUE)
ENDPROC


