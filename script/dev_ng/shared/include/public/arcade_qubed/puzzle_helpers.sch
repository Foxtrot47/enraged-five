USING "commands_camera.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "globals.sch"
USING "model_enums.sch"
USING "rage_builtins.sch"
USING "types.sch"
USING "cellphone_public.sch"
USING "commands_clock.sch"
USING "commands_audio.sch"
USING "arcade_cabinet_minigame_common.sch"
USING "arcade_games_leaderboard.sch"
USING "arcade_games_sound.sch"

USING "net_arcade_cabinet.sch"
USING "arcade_games_postfx.sch"
USING "arcade_games_help_text.sch"
USING "arcade_games_help_text_flow.sch"

USING "commands_stats.sch"
USING "commands_debug.sch"

USING "puzzle_world.sch"
///////////////////////////////////////////////////////////////////////
// *** DELCARE ANY VARIABLES THAT ARE TO BE GLOBAL TO THE SCRIPT *** //
///////////////////////////////////////////////////////////////////////

// Enums
ENUM MISSION_STAGE
	PIXTRO_SETUP = 0,
	PIXTRO_LOGO,
	WAITING_TO_START_STAGE,
	STARTING_NEW_GAME, 		
	GAME_IN_PROGRESS,
	HI_SCORE_ENTRY,
	HI_SCORE_DISPLAY
ENDENUM

ENUM TUTORIAL_STAGE
	TUTORIAL_INTRO = 0,
	SHOWING_GAME_OVERVIEW,
	SHOWING_ROTATION_DEMO,
	SHOWING_CUBE_CLEAR,
	SHOWING_BLOCK_REMOVAL,
	SHOWING_SPECIAL_BLOCK,
	SHOWING_SPECIAL_BLOCK_REMOVAL,
	SHOWING_POWER_UP,
	SHOWING_SLOW_POWER,
	SHOWING_BOMB_POWER,
	SHOWING_ERASER_POWER,
	SHOWING_REMOVAL_POWER
			
ENDENUM

ENUM FRONT_END_OPTIONS
	START_OPTION = 0,
	TUTORIAL_OPTION,
	HI_SCORE_OPTION,		
	QUIT_OPTION
ENDENUM

ENUM BOARD_GRID_CONTENTS
	EMPTY = -1,
	RED = 0,
	BLUE,
	GREEN,
	YELLOW,
	PURPLE,
	ORANGE_CUBE
ENDENUM

ENUM BOARD_BLOCK_STATUS
	REMAINS = 0,
	DELETE
ENDENUM	

ENUM RELATIVE_BLOCK_POS
	LEFT_BLOCK = 0,
	RIGHT_BLOCK,
	TOP_BLOCK,
	BOTTOM_BLOCK,
	UPPER_BLOCK,
	LOWER_BLOCK
ENDENUM

ENUM LARGE_BOX_POSITION
	BOTTOM_LEFT = 0,
	BOTTOM_RIGHT,
	TOP_LEFT,
	TOP_RIGHT
ENDENUM

ENUM DIRECTION_PRESSED_ENUM
	PRESSED_RIGHT = 0,
	PRESSED_LEFT,
	PRESSED_NOTHING
ENDENUM
	
ENUM PZ_CHALLENGES	
	PZ_CHALLENGE_QUBISM = 0,
	PZ_CHALLENGE_QUBITS,
	PZ_CHALLENGE_KING_OF_QUB3D,
	PZ_CHALLENGE_GOD_OF_QUB3D
ENDENUM
	
STRUCT GRID_BOX_STRUCT
	// variables that control the stash object itself
	VECTOR box_object_pos
	OBJECT_INDEX box_object
	INT position_in_col
	INT position_in_row
	BOARD_GRID_CONTENTS this_box_colour
	BOOL is_box_on_grid
	BOOL is_large_box
ENDSTRUCT

STRUCT FALLING_BOX_STRUCT
	// variables that control the stash object itself
	INT block_a_position_in_col
	INT block_a_position_in_row
	INT block_b_position_in_col
	INT block_b_position_in_row

	
	BOOL is_box_a_on_grid 
	BOOL is_box_b_on_grid
	OBJECT_INDEX falling_box_a
	OBJECT_INDEX falling_box_b
	VECTOR a_pos
	VECTOR b_pos
	BOARD_GRID_CONTENTS block_a_colour
	BOARD_GRID_CONTENTS block_b_colour		
	RELATIVE_BLOCK_POS block_a_is//	= LEFT_BLOCK
	RELATIVE_BLOCK_POS block_b_is//	= RIGHT_BLOCK
ENDSTRUCT

/// constants
CONST_INT number_of_rows 10
CONST_INT number_of_columns 6
CONST_INT number_required_for_deletion  3
CONST_INT max_number_of_colours 6
CONST_INT start_time_between_block_movement 1000	
CONST_INT time_between_presses 250
CONST_INT max_level_reachable 30
CONST_INT no_powerup_selected 9999999
CONST_INT timer_between_power_up_presses  250
CONST_INT time_for_slow_down_power  10000

CONST_FLOAT bottom_left_heading  90.0
CONST_FLOAT bottom_right_heading  180.0
CONST_FLOAT top_left_heading   0.0
CONST_FLOAT top_right_heading  270.0

CONST_INT number_to_fill_power_bar  4
CONST_INT max_number_of_power_uses  3
CONST_INT number_of_possible_power_up_types 4

CONST_INT max_number_of_digits_in_score 10
CONST_FLOAT distance_between_digits 0.3

CONST_INT total_letter_no  26

/// Flags(INT)
MISSION_STAGE current_mission_stage
TUTORIAL_STAGE current_tutorial_stage
BOARD_GRID_CONTENTS current_grid_contents	
BOARD_GRID_CONTENTS random_colour_to_be_removed 	= RED
BOARD_BLOCK_STATUS block_status[10][10]	
DIRECTION_PRESSED_ENUM direction_pressed			= PRESSED_NOTHING
DIRECTION_PRESSED_ENUM tutorial_rotation_progression[20]
FRONT_END_OPTIONS option_selected
RELATIVE_BLOCK_POS new_a_relative_pos
RELATIVE_BLOCK_POS new_b_relative_pos

TEXT_FONTS		fMainFont 							= FONT_CONDENSED_NOT_GAMERNAME

BOOL is_mission_in_progress							= FALSE
BOOL first_time_through								= TRUE
BOOL is_a_box_in_motion								= FALSE
BOOL is_game_over									= TRUE
BOOL is_in_danger_zone								= FALSE
BOOL is_button_still_pressed						= TRUE
BOOL is_player_moving_block							= FALSE
BOOL is_cross_pressed								= TRUE
BOOL allowed_two_block_of_same_colour				= FALSE
BOOL has_a_power_up_been_selected 					= FALSE

BOOL slow_down_powerup_in_effect					= FALSE
BOOL colour_removal_powerup_in_effect				= FALSE
BOOL eraser_powerup_in_effect						= FALSE
BOOL bomb_powerup_in_effect							= FALSE

BOOL colour_removal_selected						= FALSE
BOOL bomb_power_selected							= FALSE
BOOL eraser_power_selected							= FALSE

BOOL is_player_forcing_blocks_speed_up				= FALSE
BOOL ingame_music_playing							= FALSE


BOOL is_this_tutorial								= FALSE
BOOL have_blocks_been_set_up_for_demo				= FALSE
BOOL has_front_end_been_setup						= FALSE
BOOL has_front_end_grid_been_setup					 =FALSE

BOOL have_large_boxes_in_play						= TRUE
#if IS_DEBUG_BUILD
BOOL debug_text										= FALSE
BOOL outputting_variable_info						= FALSE
BOOL output_general_variable_info 					= FALSE
#endif

// structs
GRID_BOX_STRUCT grid_box[number_of_rows][number_of_columns]
FALLING_BOX_STRUCT falling_box
FALLING_BOX_STRUCT preview_box	
/// People (PED_INDEX)
	
/// Sequences (SEQUENCE_INDEX)

/// Objects (OBJECT_INDEX)
OBJECT_INDEX skybox_object
OBJECT_INDEX grid_object

//VECTOR block_pos_f
VECTOR game_grid_pos
FLOAT game_grid_heading
/// decision makers (DECISION_MAKER_INDEX)

/// other ints (INT)
INT current_column
INT current_row
INT max_row
INT min_row
INT max_col
INT min_col
INT global_alpha

INT global_text_alpha = 250
//INT loop_counter
INT number_of_blocks_created

INT a_col_shift
INT a_row_shift
//INT //b_depth_shift

/// timer
FLOAT current_time
FLOAT time_difference
FLOAT time_started
FLOAT time_between_block_movement	
FLOAT time_deduction_factor = 75.0
FLOAT blocks_fall_speed_mulitplier = 0.0
FLOAT time_last_press
FLOAT time_power_up_active
//INT time_player_started_game

//INT time_tut_text_started
FLOAT time_last_box_made = 0.0

/// model names	
MODEL_NAMES box_model
MODEL_NAMES large_box_model//[4] // [colour][pos]
MODEL_NAMES skybox_model

//// text
//TEXT_LABEL game_title_txt
TEXT_LABEL_63 start_option_txt
TEXT_LABEL_63 quit_option_txt
TEXT_LABEL_63 tutorial_option_txt
TEXT_LABEL_63 hiscore_option_txt

STRING qub3d_ptfx_assets = "scr_sum_q3"
STRING qub3d_ptfx_explosion = "scr_sum_q3_block_destroy"
STRING qub3d_ptfx_merge = "scr_sum_q3_block_destroy_charge"

/// Audio Position
INT soundId = -1
INT Qub3dMusicSoundID = -1
STRING audioPanVarX = "blockXPos"
STRING audioPanVarY = "blockYPos"
STRING audioNumBlocksCleared = "blocksCleared"
FLOAT blocks_cleared

/// audio strings
STRING exploding_block_sfx = "QUB3D_CLEAR_BLOCK"  // -  Exploding block
STRING rotating_block_sfx = "QUB3D_ROTATE_BLOCK" //  -  Rotating a block
STRING falling_block_sfx = "QUB3D_MOVE_BLOCK"  //-  Block tumbling normally
STRING merging_block_sfx = "QUB3D_GLOM_BLOCK"   //-  4 blocks joining together
STRING fixing_block_sfx = "QUB3D_LAND_BLOCK" // -  Block sticking at the bottom of the screen
STRING got_powerup_sfx = "QUB3D_GOT_POWERUP" // -  Player is awarded a powerup
STRING use_powerup_sfx = "QUB3D_USE_POWERUP" // -  Player uses a powerup
STRING game_over_sfx = "QUB3D_GAME_OVER" // -  Game Over

/// scoreing
INT total_score	
INT session_high_score
INT current_score
INT score_multiplyer
INT current_level = 1
INT score_per_block
INT score_required_to_level[max_level_reachable]

/// power up stuff
BOOL power_up_ready[number_to_fill_power_bar]
BOOL is_square_pressed
BOOL is_circle_pressed
BOOL is_triangle_pressed
INT number_of_power_uses
//INT timer_last_power_up_press
INT number_of_powerup_objs_to_display = 0
INT number_of_powerup_objs_being_displayed

// tutorial stuff
INT current_tutorial_rotation

// front end
STRING puzzle_textures
STRING logo_texture
STRING strapline_quote

STRING special_texture_1
STRING special_texture_2
STRING special_texture_3
STRING special_texture_4

STRING power_bar_texture
STRING preview_block_texture
STRING random_block_texture

// high score stuff
STRING high_score_textures

VECTOR border_rect_size = <<0.004, 0.006, 0.0>>

VECTOR skybox_pos= <<0.0, 0.0, 0.0>>
VECTOR skybox_rot= <<0.0, 0.0, 0.0>>

// Block positions
FLOAT bottom_z
FLOAT top_z


/// power 	
FLOAT ig_power_txt_pos_x 	 = 518.0
FLOAT ig_power_txt_pos_y 	 = 116
FLOAT ig_power_txt_size_x	 = 128
FLOAT ig_power_txt_size_y	 = 32
//FLOAT ig_power_txt_wrap_left = 0.0000
//FLOAT ig_power_txt_wrap_right= 1.0000
FLOAT ig_power_bar_pos_x 	 = 526.0
FLOAT ig_power_bar_pos_y 	 = 160.0
FLOAT ig_power_bar_size_x	 = 144
FLOAT ig_power_bar_size_y	 = 36
FLOAT ig_power_bar_inner_1_pos_x 	 = ig_power_bar_pos_x - 52
FLOAT ig_power_bar_inner_1_pos_y 	 = ig_power_bar_pos_y-1
FLOAT ig_power_bar_inner_1_size_x	 = 34.25
FLOAT ig_power_bar_inner_1_size_y	 = 25
FLOAT ig_power_bar_inner_2_pos_x 	 = ig_power_bar_inner_1_pos_x  + (ig_power_bar_inner_1_size_x/2)
FLOAT ig_power_bar_inner_2_pos_y 	 = ig_power_bar_inner_1_pos_y
FLOAT ig_power_bar_inner_2_size_x	 = ig_power_bar_inner_1_size_x * 2
FLOAT ig_power_bar_inner_2_size_y	 = ig_power_bar_inner_1_size_y
FLOAT ig_power_bar_inner_3_pos_x 	 = ig_power_bar_inner_1_pos_x + (ig_power_bar_inner_2_size_x/2)
FLOAT ig_power_bar_inner_3_pos_y 	 = ig_power_bar_inner_1_pos_y
FLOAT ig_power_bar_inner_3_size_x	 = ig_power_bar_inner_1_size_x * 3
FLOAT ig_power_bar_inner_3_size_y	 = ig_power_bar_inner_1_size_y
FLOAT ig_power_bar_inner_4_pos_x 	 = ig_power_bar_inner_1_pos_x + (ig_power_bar_inner_3_size_x/2)
FLOAT ig_power_bar_inner_4_pos_y 	 = ig_power_bar_inner_1_pos_y
FLOAT ig_power_bar_inner_4_size_x	 = ig_power_bar_inner_1_size_x * 4
FLOAT ig_power_bar_inner_4_size_y	 = ig_power_bar_inner_1_size_y


/// special
FLOAT ig_special_txt_pos_x 	   = 518
FLOAT ig_special_txt_pos_y 	   = 216
FLOAT ig_special_txt_size_x	   = 128
FLOAT ig_special_txt_size_y	   = 32
//FLOAT ig_special_txt_wrap_left = 0.0000
//FLOAT ig_special_txt_wrap_right= 1.0000
FLOAT ig_power_uses_txt_pos_x 	   = 568
FLOAT ig_power_uses_txt_pos_y 	   = ig_power_txt_pos_y + 4

FLOAT ig_special_1st_power_pos_x = ig_special_txt_pos_x - 42
FLOAT ig_special_1st_power_pos_y = ig_special_txt_pos_y + 44
FLOAT ig_special_1st_power_size_x= 40.0
FLOAT ig_special_1st_power_size_y= 40.0
FLOAT ig_special_2nd_power_pos_x = ig_special_1st_power_pos_x + 42.0
FLOAT ig_special_2nd_power_pos_y = ig_special_1st_power_pos_y
FLOAT ig_special_2nd_power_size_x= 40.0
FLOAT ig_special_2nd_power_size_y= 40.0
FLOAT ig_special_3rd_power_pos_x = ig_special_2nd_power_pos_x + 42.0
FLOAT ig_special_3rd_power_pos_y = ig_special_1st_power_pos_y
FLOAT ig_special_3rd_power_size_x= 40.0
FLOAT ig_special_3rd_power_size_y= 40.0
FLOAT ig_special_4th_power_pos_x = ig_special_3rd_power_pos_x + 42.0
FLOAT ig_special_4th_power_pos_y = ig_special_1st_power_pos_y
FLOAT ig_special_4th_power_size_x= 40.0
FLOAT ig_special_4th_power_size_y= 40.0
FLOAT ig_random_colour_pos_x[max_number_of_colours]// = 0.2950
FLOAT ig_random_colour_pos_y[max_number_of_colours]// = 0.1900
FLOAT ig_random_colour_size_x= 256
FLOAT ig_random_colour_size_y= 256

// next block display
FLOAT ig_next_txt_pos_x		 = 518
FLOAT ig_next_txt_pos_y		 = 328
FLOAT ig_next_txt_size_x 	 = 128
FLOAT ig_next_txt_size_y 	 = 32
FLOAT ig_next_left_block_pos_x 	 = ig_next_txt_pos_x - 40
FLOAT ig_next_left_block_pos_y 	 = ig_next_txt_pos_y + 48
FLOAT ig_next_left_block_size_x	 = 48.0
FLOAT ig_next_left_block_size_y	 = 48.0
FLOAT ig_next_right_block_pos_x  = ig_next_left_block_pos_x + 52.0
FLOAT ig_next_right_block_pos_y  = ig_next_left_block_pos_y
FLOAT ig_next_right_block_size_x = 48.0
FLOAT ig_next_right_block_size_y = 48.0
INT preview_block_alpha = 180
// score display 
FLOAT ig_score_txt_pos_x 	  = 1412
FLOAT ig_score_txt_pos_y 	  = ig_power_txt_pos_y
FLOAT ig_score_txt_size_x 	  = 128
FLOAT ig_score_txt_size_y 	  = 32
FLOAT ig_score_num_pos_x 	  = ig_score_txt_pos_x - 144
FLOAT ig_score_num_pos_y 	  = ig_score_txt_pos_y + 40

// hi-score display
FLOAT ig_hiscore_txt_pos_x 	   = ig_score_txt_pos_x
FLOAT ig_hiscore_txt_pos_y 	   = ig_special_txt_pos_y
FLOAT ig_hiscore_txt_size_x	   = 128
FLOAT ig_hiscore_txt_size_y	   = 32
FLOAT ig_hiscore_num_pos_x 	   = ig_score_num_pos_x
FLOAT ig_hiscore_num_pos_y 	   = ig_hiscore_txt_pos_y + 40

// score display 
FLOAT ig_level_txt_pos_x 	  = ig_score_txt_pos_x
FLOAT ig_level_txt_pos_y 	  = ig_next_txt_pos_y
FLOAT ig_level_txt_size_x 	  = 128
FLOAT ig_level_txt_size_y 	  = 32
FLOAT ig_level_num_pos_x 	  = ig_level_txt_pos_x - 36
FLOAT ig_level_num_pos_y 	  = ig_next_txt_pos_y + 48

FLOAT tut_offset = 36.0

FLOAT 	tut_power_bar_pos_y			  = ig_power_bar_pos_y + tut_offset
FLOAT 	tut_power_bar_inner_1_pos_y	  = ig_power_bar_inner_1_pos_y + tut_offset
FLOAT 	tut_power_txt_pos_y 		 = ig_power_txt_pos_y + tut_offset

FLOAT 	tut_special_txt_pos_y		= ig_special_txt_pos_y + tut_offset
FLOAT 	tut_special_icons_pos_y		= tut_special_txt_pos_y + 44

VECTOR_2D start_rect_pos = INIT_VECTOR_2D(0.4950, 0.540)
VECTOR_2D start_rect_size = INIT_VECTOR_2D(0.375, 0.0800)
VECTOR_2D hiscore_rect_pos = INIT_VECTOR_2D(0.4950, 0.7000)
VECTOR_2D hiscore_rect_size = INIT_VECTOR_2D(0.375, 0.0800)
VECTOR_2D tut_rect_pos = INIT_VECTOR_2D(0.4950, 0.6200)
VECTOR_2D tut_rect_size = INIT_VECTOR_2D(0.375, 0.0800)
VECTOR_2D quit_rect_pos = INIT_VECTOR_2D(0.4950, 0.7800)
VECTOR_2D quit_rect_size = INIT_VECTOR_2D(0.375, 0.0800)

FLOAT 	start_txt_x   = 0.496
FLOAT 	start_txt_y	  = 0.540
FLOAT 	tutorial_txt_x= 0.495
FLOAT 	tutorial_txt_y= 0.620
FLOAT 	hiscore_txt_x = 0.497
FLOAT 	hiscore_txt_y = 0.7
FLOAT 	quit_txt_x 	  = 0.495
FLOAT 	quit_txt_y	  = 0.780

// RGBA Colour structs
RGBA_COLOUR_STRUCT rgbaEraserActive
RGBA_COLOUR_STRUCT rgbaEraserNonActive
RGBA_COLOUR_STRUCT rgbaBombActive
RGBA_COLOUR_STRUCT rgbaBombNonActive
RGBA_COLOUR_STRUCT rgbaRandomActive
RGBA_COLOUR_STRUCT rgbaRandomNonActive
RGBA_COLOUR_STRUCT rgbaSlowActive
RGBA_COLOUR_STRUCT rgbaSlowNonActive

RGBA_COLOUR_STRUCT rgbaWhiteOut
RGBA_COLOUR_STRUCT rgbaBackdrop
RGBA_COLOUR_STRUCT rgbaDarkSeeThrough
RGBA_COLOUR_STRUCT rgbaBlackOut

RGBA_COLOUR_STRUCT rgbaNearOpaque

STRING sPrefixDebugQub3d = "[BAZ][QUB3D] "

FLOAT starting_x
FLOAT starting_y

SCRIPT_CONTROL_HOLD_TIMER schtQuitGame
SCRIPT_CONTROL_HOLD_TIMER schtQuitGamePC

// Track the time running
FLOAT fRunTimer

// Cleanup controls
ENUM DELETION_PHASES

	SAME_COLOUR,
	LARGE_BOX_CHECK,
	BOX_DELETE_TUTORIAL,
	LARGE_BOX_DELETION,
	FINISHED_DELETION

ENDENUM

DELETION_PHASES currentDeletionPhase
INT number_of_same_colour_found
INT number_flagged_for_deletion	

ARCADE_GAMES_AUDIO_SCENE qub3edAudioScene_Active = ARCADE_GAMES_AUDIO_SCENE_END

BOOL bPCScriptedControlLoaded

// TUTORIAL CONTROLS

ENUM POWER_DEMO_STATE
	PD_SETUP_GRID,
	PD_TRIGGER_POWER,
	PD_HELP_UPDATE_00,
	PD_HELP_UPDATE_01,
	PD_HELP_UPDATE_02,
	PD_NEXT_DEMO
ENDENUM

POWER_DEMO_STATE powerDemoState = PD_SETUP_GRID


///////////////////////
// 	 *** VIDEO ***   //
///////////////////////
BINK_MOVIE_ID puzzleIDPixtroIntro

PROC QUB3D_CLEAN_UP_MOVIE(BINK_MOVIE_ID bmID)
	IF (bmID != NULL)
		STOP_BINK_MOVIE(bmID)
		RELEASE_BINK_MOVIE(bmID)
	ENDIF
ENDPROC

///////////////////////
// *** CHALLENGE *** //
///////////////////////

STRUCT CHALLENGE_TELEMETRY
	INT gameType
	INT level
	INT score
	INT powerUps
	INT challenges
ENDSTRUCT

// Challenge bools
BOOL bWonTrophyThisSession
BOOL bRemovedFromPlay

STRUCT_ARCADE_CABINET sQub3dTelemetry
STRUCT_ARCADE_CABINET sTelemetryReset // Use to zero the telemetry values

PROC PZ_SEND_TELEMETRY()	
	sQub3dTelemetry.gameType = HASH("ARCADE_CABINET_SUM_QUB3D")
	
	CPRINTLN(DEBUG_MINIGAME, "PZ_SEND_TELEMETRY - Score to send: ", total_score)
	sQub3dTelemetry.score = total_score
	sQub3dTelemetry.timePlayed = ROUND(fRunTimer)

	IF bWonTrophyThisSession
		sQub3dTelemetry.reward = ARCADE_GAME_GET_TROPHY_ID_FOR_TELEMETRY(ARCADE_GAME_TROPHY_QUB3D)
	ELSE
		sQub3dTelemetry.reward = 0
	ENDIf

	IF PLAYER_ID() != INVALID_PLAYER_INDEX()	
		SWITCH GET_SIMPLE_INTERIOR_TYPE_LOCAL_PLAYER_IS_IN()
			CASE SIMPLE_INTERIOR_TYPE_ARCADE
				sQub3dTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_ARCADE")
			BREAK
			CASE SIMPLE_INTERIOR_TYPE_AUTO_SHOP
				sQub3dTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_AUTO_SHOP")
			BREAK
			
			#IF FEATURE_FIXER
			#IF FEATURE_MUSIC_STUDIO
			CASE SIMPLE_INTERIOR_TYPE_MUSIC_STUDIO
				sQub3dTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_MUSIC_STUDIO")
			BREAK						
			#ENDIF
			#ENDIF
			
			DEFAULT
				CDEBUG1LN(DEBUG_MINIGAME, "[[TLG_ARCADE] [TLG_SEND_TELEMETRY] not in valid arcade location, setting sQub3dTelemetry.location = 0")
				sQub3dTelemetry.location = 0
			BREAK
		ENDSWITCH
	ELSE
		CDEBUG2LN(DEBUG_MINIGAME, "[TLG_ARCADE] [TLG_SEND_TELEMETRY] Invalid Player ID, setting sQub3dTelemetry.location = 0")
		sQub3dTelemetry.location = 0
	ENDIF
	
	//url:bugstar:6543886
	sQub3dTelemetry.numPlayers = 1
	//CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY]")
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY] 	sQub3dTelemetry.numPlayers: ", sQub3dTelemetry.numPlayers)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY] 	sQub3dTelemetry.level: ", sQub3dTelemetry.level)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY] 	sQub3dTelemetry.score: ", sQub3dTelemetry.score)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY] 	sQub3dTelemetry.powerUps: ", sQub3dTelemetry.powerUps)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY] 	sQub3dTelemetry.challenges: ", sQub3dTelemetry.challenges)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SEND_TELEMETRY] 	sQub3dTelemetry.challenges: ", sQub3dTelemetry.location)
	
	PLAYSTATS_ARCADE_CABINET(sQub3dTelemetry)
	sQub3dTelemetry = sTelemetryReset
ENDPROC

////////////////////////////
// *** DEBUG FUNCTION *** //
////////////////////////////
#if IS_DEBUG_BUILD
WIDGET_GROUP_ID wgQubed
BOOL bDebugCreatedWidgets = FALSE
BOOL bMinimiseCams = FALSE
PROC WIDGET_UPDATE()

	IF NOT bDebugCreatedWidgets
	
	IF NOT DOES_WIDGET_GROUP_EXIST(wgQubed)
		wgQubed = START_WIDGET_GROUP("Qub3d port")
		STOP_WIDGET_GROUP()
		//PUZZLE_DEBUG_SET_PARENT_WIDGET_GROUP(wgQubed)
	ENDIF
	
	WIDGET_GROUP_ID modGroup
	modGroup = wgQubed
	
		SET_CURRENT_WIDGET_GROUP(modGroup)
		START_WIDGET_GROUP("Qub3d mods") 
			//// GENERAL WIDGETS
			ADD_WIDGET_INT_READ_ONLY("TOTAL_SCORE", total_Score) 
			ADD_WIDGET_INT_SLIDER("current level", current_level, 0, 30, 1)
			ADD_WIDGET_FLOAT_READ_ONLY("fall speed multiplier", blocks_fall_speed_mulitplier)
			
			ADD_WIDGET_BOOL("DEGUG TEXT", debug_text)
			ADD_WIDGET_BOOL("OUT PUT ALL VARIABLES TO TEMP_DEBUG", outputting_variable_info )		
			ADD_WIDGET_BOOL("OUT GENERAL VARIABLES TO TEMP_DEBUG", output_general_variable_info)		
			
			START_WIDGET_GROUP("Qub3d port - Camera controls")
				ADD_WIDGET_FLOAT_SLIDER("game_Cam_pos.x", game_Cam_pos.x, -9999.0, 9999.0, 0.01) 
				ADD_WIDGET_FLOAT_SLIDER("game_Cam_pos.y", game_Cam_pos.y, -9999.0, 9999.0, 0.01) 
				ADD_WIDGET_FLOAT_SLIDER("game_Cam_pos.z", game_Cam_pos.z, -9999.0, 9999.0, 0.01) 
				ADD_WIDGET_FLOAT_SLIDER("game_Cam_rot.x", game_Cam_rot.x, -360.0, 360.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("game_Cam_rot.y", game_Cam_rot.y, -360.0, 360.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("game_Cam_rot.z", game_Cam_rot.z, -360.0, 360.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("game_cam_fov", game_cam_fov, 1.0, 100.0, 0.1) 
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Qub3d port - SKYBOX position")
				ADD_WIDGET_FLOAT_SLIDER("skybox_pos.x", skybox_pos.x, -9999.0, 9999.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("skybox_pos.y", skybox_pos.y, -9999.0, 9999.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("skybox_pos.z", skybox_pos.z, -9999.0, 9999.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("skybox_rot.x", skybox_rot.x, -360.0, 360.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("skybox_rot.y", skybox_rot.y, -360.0, 360.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("skybox_rot.z", skybox_rot.z, -360.0, 360.0, 0.1)				
			STOP_WIDGET_GROUP()
						
			START_WIDGET_GROUP("Qub3d port - Grid position")
				ADD_WIDGET_FLOAT_SLIDER("game_grid_pos.x", game_grid_pos.x, -9999.0, 9999.0, 0.01) 
				ADD_WIDGET_FLOAT_SLIDER("game_grid_pos.y", game_grid_pos.y, -9999.0, 9999.0, 0.01) 
				ADD_WIDGET_FLOAT_SLIDER("game_grid_pos.z", game_grid_pos.z, -9999.0, 9999.0, 0.01) 
				ADD_WIDGET_FLOAT_SLIDER("game_grid_heading", game_grid_heading, -360.0, 360.0, 0.01)		
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Qub3d port - PostFX")
				ARCADE_GAMES_POSTFX_WIDGET_CREATE()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Qub3d port - Display")
				ADD_WIDGET_BOOL("Minimise the game", bMinimiseCams)
			STOP_WIDGET_GROUP()
			
			Vector boxA
			Vector boxB
			
			IF DOES_ENTITY_EXIST(grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object)
				boxA = GET_ENTITY_COORDS(grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object)
			ENDIF
			
			IF DOES_ENTITY_EXIST(grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object)
				boxB = GET_ENTITY_COORDS(grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object)
			ENDIF
			
			START_WIDGET_GROUP("Qub3d port - Falling block A position")
				ADD_WIDGET_FLOAT_READ_ONLY("Falling Block A - X:", boxA.x) 
				ADD_WIDGET_FLOAT_READ_ONLY("Falling Block A - Y:", boxA.y)
				ADD_WIDGET_FLOAT_READ_ONLY("Falling Block A - Z:", boxA.z)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Qub3d port - Falling block B position")
				ADD_WIDGET_FLOAT_READ_ONLY("Falling Block B - X:", boxB.x) 
				ADD_WIDGET_FLOAT_READ_ONLY("Falling Block B - Y:", boxB.y) 
				ADD_WIDGET_FLOAT_READ_ONLY("Falling Block B - Z:", boxB.z)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(modGroup)
	
	bDebugCreatedWidgets = TRUE
	ENDIF
	
ENDPROC
#endif

//////////////////////////////////////
// *** MISSION PASSED PROCEDURE *** //
//////////////////////////////////////
PROC PASSED_MISSION_PUZZLE()
	// display the passed text
	// players score
	// clear the wanted level?
	
ENDPROC

///////////////////////////////////////
// *** MISSION CLEANUP PROCEDURE *** //
///////////////////////////////////////
PROC QUB3D_ARCADE_RELEASE_ALL_SOUNDS(BOOL bReset = TRUE)

	ARCADE_GAMES_SOUND_RELEASE(QUB3D_AUDIO_EFFECT_SCOREBOARD_CHANGE_LETTER)
	ARCADE_GAMES_SOUND_RELEASE(QUB3D_AUDIO_EFFECT_SCOREBOARD_CONFIRM)
	ARCADE_GAMES_SOUND_RELEASE(QUB3D_AUDIO_EFFECT_SCOREBOARD_ENTER)

	IF bReset
		ARCADE_GAMES_SOUND_INIT(CASINO_ARCADE_GAME_QUB3D)
	ENDIF

ENDPROC

PROC CLEANUP_QUB3D_CAMS()

	IF DOES_CAM_EXIST(game_cam)
		IF IS_CAM_ACTIVE(game_cam)
			SET_CAM_ACTIVE(game_cam, FALSE)	
		ENDIF
		DESTROY_CAM(game_cam)
	ENDIF
		
	IF DOES_CAM_EXIST(anim_cam)
		IF IS_CAM_ACTIVE(anim_cam)
			SET_CAM_ACTIVE(anim_cam, FALSE)	
		ENDIF
		DESTROY_CAM(anim_cam)
	ENDIF
	
ENDPROC

PROC CLEANUP_MISSION_PUZZLE()	

	IF ingame_music_playing
		STOP_SOUND(Qub3dMusicSoundID)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SUM20/sum20_qub3d_sfx")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SUM20/sum20_qub3d_music")
		
		QUB3D_ARCADE_RELEASE_ALL_SOUNDS()
		
		ingame_music_playing = FALSE
	ENDIF
	
	/// cameras
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	CLEANUP_QUB3D_CAMS()
	
	OVERRIDE_FREEZE_FLAGS(FALSE)	

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	DISABLE_CELLPHONE(FALSE)
	
	// utnr on radar
	DISPLAY_RADAR (TRUE)
	DISPLAY_HUD (TRUE)
	REMOVE_NAMED_PTFX_ASSET(qub3d_ptfx_assets)
		
	IF qub3edAudioScene_Active != ARCADE_GAMES_AUDIO_SCENE_END
		ARCADE_GAMES_AUDIO_SCENE_STOP(qub3edAudioScene_Active) // Safely stop the original scene
		qub3edAudioScene_Active = ARCADE_GAMES_AUDIO_SCENE_END // Store the current scene
	ENDIF
	
	// mark chars as no longer needed
	// mark vehicles as no longer needed
	// mark objects as no longer needed
	
	FOR current_column = 0 TO (number_of_columns -1)
		// initialise the starting column
		FOR current_row = 0 TO (number_of_rows-1)
			// set the initial values in the grid			
			DELETE_OBJECT(grid_box[current_row][current_column].box_object)
		ENDFOR
	ENDFOR
	
	
	DELETE_OBJECT(skybox_object)
	DELETE_OBJECT(grid_object)
	
	IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
		DELETE_OBJECT(falling_box.falling_box_a)
	ENDIF
	
	IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
		DELETE_OBJECT(falling_box.falling_box_b)
	ENDIF

	// remove blips
	// remove decision makers
	// remove the models from memory
	SET_MODEL_AS_NO_LONGER_NEEDED (box_model)
	SET_MODEL_AS_NO_LONGER_NEEDED (large_box_model)
	SET_MODEL_AS_NO_LONGER_NEEDED (skybox_model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sum_prop_ac_qub3d_grid)

	IF DOES_ANIM_DICT_EXIST("ANIM_HEIST@ARCADE@QUB3D")
		REMOVE_ANIM_DICT("ANIM_HEIST@ARCADE@QUB3D")
	ENDIF
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(puzzle_textures)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(high_score_textures)
	
	/// end the script
	
	CLEAR_OVERRIDE_WEATHER()		
	
	pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_EXIT
	pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
	pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
	pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_EXIT	
	PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
	RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "PZ_ANIM_CLIP_EXIT - Triggering cabinet exit")
	
	ARCADE_CABINET_COMMON_CLEANUP()
	ARCADE_GAMES_HELP_TEXT_CLEAR()
	
	IF (bPCScriptedControlLoaded)
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
		bPCScriptedControlLoaded = FALSE
	ENDIF
	
	QUB3D_CLEAN_UP_MOVIE(puzzleIDPixtroIntro)
	
	is_mission_in_progress = FALSE

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

FUNC BOOL QUB3D_IS_NETWORK_DISCONNECTED()

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT()
		RETURN TRUE
	ENDIF

	IF IS_SCREEN_FADED_OUT()
		RETURN TRUE
	ENDIF

	IF IS_SCREEN_FADING_IN()
		RETURN TRUE
	ENDIF
		
	RETURN FALSE

ENDFUNC

/////////////////////////////////////
// *** MISSION START PROCEDURE *** //
/////////////////////////////////////

PROC QUB3D_INIT_VARS(ARCADE_CABINET_PROPERTY eProperty)
	
	// INITIALISE THE VARAIBLES
	max_row		= number_of_rows - 1
	min_row		= 1 
	max_col		= number_of_columns - 1
	min_col		= 0
	current_column	= 0
	current_row	= 0

	UNUSED_PARAMETER(sPrefixDebugQub3d)
	
	game_grid_heading   = 90.0//180.0
	QUB3D_GET_WORLD_COORDS(eProperty, game_Cam_pos, game_grid_pos, game_grid_heading, starting_x, starting_y)

	/// scores	
	total_score 		= 0
	session_high_score  = 0
	current_score 		= 0
	score_multiplyer 	= 0//1
	current_level 		= 1
	score_per_block		= 25
	
	// power ups
	//power_up_selected = 0
	
	/// leve req	
	score_required_to_level[0]		=  250
	score_required_to_level[1]		=  500
	score_required_to_level[2]		=  750
	score_required_to_level[3]		=  1000
	score_required_to_level[4]		=  1250
	score_required_to_level[5]		=  1500
	score_required_to_level[6]		=  2000
	score_required_to_level[7]		=  2500
	score_required_to_level[8]		=  3000
	score_required_to_level[9]		=  3500
	score_required_to_level[10]		=  4000
	score_required_to_level[11]		=  5000
	score_required_to_level[12]		=  6000
	score_required_to_level[13]		=  7000
	score_required_to_level[14]		=  8000
	score_required_to_level[15]		=  9000
	score_required_to_level[16]		=  10000
	score_required_to_level[17]		=  11000
	score_required_to_level[18]		=  12000
	score_required_to_level[19]		=  13000
	
	score_required_to_level[20]		=  15000
	score_required_to_level[21]		=  17000
	score_required_to_level[22]		=  20000
	score_required_to_level[23]		=  23000
	score_required_to_level[24]		=  27000
	score_required_to_level[25]		=  31000
	score_required_to_level[26]		=  36000
	score_required_to_level[27]		=  41000
	score_required_to_level[28]		=  45000
	score_required_to_level[29]		=  50000

	ig_random_colour_pos_x[RED] = 800
	ig_random_colour_pos_y[RED] = 212
	ig_random_colour_pos_x[BLUE] = 1184
	ig_random_colour_pos_y[BLUE] = 212
	ig_random_colour_pos_x[GREEN] = 800
	ig_random_colour_pos_y[GREEN] = 524
	ig_random_colour_pos_x[YELLOW] = 1184
	ig_random_colour_pos_y[YELLOW] = 524
	ig_random_colour_pos_x[PURPLE] = 800
	ig_random_colour_pos_y[PURPLE] = 836
	ig_random_colour_pos_x[ORANGE_CUBE] = 1184
	ig_random_colour_pos_y[ORANGE_CUBE] = 836
	/// models

	box_model 			= SUM_PROP_AC_QUB3D_CUBE_01
		
	large_box_model 	= SUM_prop_ac_qub3d_cube_02		
	
	skybox_model 		= SUM_PROP_AC_QUB3D_FLIPPEDCUBE
	
	/// text labels
	start_option_txt 	= "MENU_LABEL_PLAY"//START
	quit_option_txt 	= "MENU_LABEL_QUIT"//QUIT
	tutorial_option_txt = "MENU_LABEL_TUTORIAL"//TUTORIAL
	hiscore_option_txt  = "MENU_LABEL_HISCORES"

	strapline_quote = "MENU_LABEL_STRAPLINE"
	
	option_selected = START_OPTION/// QUIT_OPTION
	is_this_tutorial = FALSE
	is_game_over = TRUE // Starts true
	is_in_danger_zone = FALSE
	current_tutorial_stage = TUTORIAL_INTRO
	
	// texture labels
	puzzle_textures = "MPQub3d"
	
	high_score_textures = "MPQub3d_HUD"
	
	logo_texture = "QUB3D_LOGO"

	// power ups
	special_texture_1 = "RedSpecial"
	special_texture_2 = "GreenSpecial"
	special_texture_3 = "BlueSpecial"
	special_texture_4 = "YellowSpecial"
			
	// power bar texture
	power_bar_texture = "PowerUpBar"
	
	// preview block
	preview_block_texture = "NextBox"
	random_block_texture = "RandomBox"

	bottom_z = game_grid_pos.z + 0.011
	top_z = game_grid_pos.z + 0.035
	
	currentDeletionPhase = SAME_COLOUR
	
	// Initialise hold actions
	schtQuitGame.control = FRONTEND_CONTROL
	schtQuitGame.action  = INPUT_FRONTEND_CANCEL
	
	schtQuitGamePC.control = FRONTEND_CONTROL
	schtQuitGamePC.action = INPUT_FRONTEND_DELETE
	
	pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_ENTRY
	
	IF IS_PC_VERSION()
		IF (bPCScriptedControlLoaded)
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCScriptedControlLoaded = FALSE
		ENDIF
			
		bPCScriptedControlLoaded = INIT_PC_SCRIPTED_CONTROLS("QUB3D")
	ENDIF

	sQub3dTelemetry.level = 0
	sQub3dTelemetry.score = 0
	sQub3dTelemetry.powerUps = 0
	sQub3dTelemetry.challenges = 0
	sQub3dTelemetry.timePlayed = 0
		
	// Set the run timer to 0
	fRunTimer = 0
	
ENDPROC

PROC QUB3D_REQUEST_ASSETS()
	///////////////////////
	/// **** AUDIO **** ///
	///////////////////////
		
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	
	IF NOT ARCADE_GAMES_SOUND_REQUESTING_AUDIO_BANK("DLC_SUM20/sum20_qub3d_sfx")
		PRINTSTRING("ATTEMPTING TO LOAD AUDIO BANK QUB3D")
		PRINTNL()
	ENDIF
	
	IF NOT ARCADE_GAMES_SOUND_REQUESTING_AUDIO_BANK("DLC_SUM20/sum20_qub3d_music")
		PRINTSTRING("ATTEMPTING TO LOAD AUDIO BANK QUB3D MUSIC")
		PRINTNL()
	ENDIF
	
	/// background grid
	REQUEST_MODEL (sum_prop_ac_qub3d_grid)
	
	// small box models
	REQUEST_MODEL (box_model)
	
	/// large box models
	REQUEST_MODEL (large_box_model)
	
	// skybox model
	REQUEST_MODEL(skybox_model)
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX00")
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX01")
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX02")
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX03")
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX04")
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX05")
	
	REQUEST_STREAMED_TEXTURE_DICT(puzzle_textures)
	REQUEST_STREAMED_TEXTURE_DICT(high_score_textures)
	REQUEST_NAMED_PTFX_ASSET(qub3d_ptfx_assets)
	
	REQUEST_ANIM_DICT("ANIM_HEIST@ARCADE@QUB3D@")
ENDPROC

FUNC BOOL QUB3D_HAVE_REQUESTED_ASSETS_LOADED()
	
	IF NOT HAS_MODEL_LOADED (box_model)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "box_model - Returning FALSE")
		RETURN FALSE
	ENDIF
		
	IF NOT HAS_MODEL_LOADED (large_box_model)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "large_box_model - Returning FALSE")
		RETURN FALSE
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX00")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "MPArcadeGamesFX00 - Returning FALSE")	
		RETURN FALSE
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX01")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "MPArcadeGamesFX01 - Returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX02")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "MPArcadeGamesFX02 - Returning FALSE")
		RETURN FALSE	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX03")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "MPArcadeGamesFX03 - Returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX04")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "MPArcadeGamesFX04 - Returning FALSE")
		RETURN FALSE
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX05")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "MPArcadeGamesFX05 - Returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("ANIM_HEIST@ARCADE@QUB3D@")
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "ANIM_HEIST@arcade@qub3d@ - Returning FALSE")
		RETURN FALSE
	ENDIF

	ARCADE_CABINET_COMMON_INITIALIZATION()
	
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "QUB3D_HAVE_REQUESTED_ASSETS_LOADED - Returning TRUE")
	RETURN TRUE
ENDFUNC

FUNC BOOL QUB3D_CREATE_OBJECT(OBJECT_INDEX& object, MODEL_NAMES model, VECTOR vPos)

	object = CREATE_OBJECT_NO_OFFSET(model, vPos, FALSE, FALSE)

	IF NOT IS_ENTITY_A_MISSION_ENTITY(object) 
	OR NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(object)
		SET_ENTITY_AS_MISSION_ENTITY(object, FALSE, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(object)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC QUB3D_SETUP_VIEW()

	skybox_pos = game_grid_pos
	skybox_pos.z += 0.3875
	skybox_pos.x +=	0.175
	
	IF NOT DOES_ENTITY_EXIST(skybox_object)
		QUB3D_CREATE_OBJECT(skybox_object, skybox_model, skybox_pos)  //850  
		SET_ENTITY_ROTATION(skybox_object, skybox_rot)
		FREEZE_ENTITY_POSITION(skybox_object, TRUE)
		SET_ENTITY_COLLISION (skybox_object, FALSE)
		SET_ENTITY_HEADING(skybox_object, 0.0)
	ENDIF
	
	/// create the grid
	IF NOT DOES_ENTITY_EXIST(grid_object)
		QUB3d_CREATE_OBJECT(grid_object, sum_prop_ac_qub3d_grid, game_grid_pos)
		FREEZE_ENTITY_POSITION(grid_object, TRUE)
		SET_ENTITY_COLLISION(grid_object, FALSE)
		SET_ENTITY_HEADING(grid_object, game_grid_heading)
		
		// If grid goes not exist Darken the screen
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaBlackOut)
	ENDIF
		
	// store the World state
	SET_OVERRIDE_WEATHER("CLEAR")

	// Create the game camera.
	game_cam = CREATE_CAMERA(DEFAULT, true)
		
	SET_CAM_COORD(game_cam, game_Cam_pos)
	SET_CAM_ROT(game_cam,game_Cam_rot)
	SET_CAM_FOV(game_cam, game_cam_fov)
	
	SET_CAM_NEAR_CLIP(game_cam, 0.01)
	SET_CAM_FAR_CLIP(game_cam, 0.02)
	
	SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(game_cam, -1.0)
	SET_CAM_DOF_STRENGTH(game_cam, 0.0)
	SET_CAM_MOTION_BLUR_STRENGTH(game_cam, 0.0)

	// Spline cam starts active first.
	SET_CAM_ACTIVE(game_cam, TRUE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	STOP_SOUND(Qub3dMusicSoundID)
	
ENDPROC

// ------------------------- MENU SUPPORT  --------------------------------
PROC PZ_WRITE_STRING_USING_TEXT_SPRITES(STRING sTextToDisplay, VECTOR_2D vDisplayPos, INT iSizeMultiplier = 1, BOOL bCenterText = FALSE, INT iLengthOverride = 0)
	
	// Moddable variable
	VECTOR_2D vDisplayFinalPos = vDisplayPos	
	VECTOR_2D vBaseLetterScale = INIT_VECTOR_2D(28, 32)
	
	INT iLengthOfString = GET_LENGTH_OF_LITERAL_STRING(sTextToDisplay)
	
	IF iLengthOverride > 0
		iLengthOfString = iLengthOverride
	ENDIF
	
	IF bCenterText
		vDisplayFinalPos.X = (cfBASE_SCREEN_WIDTH/2.0)
		vDisplayFinalPos.X -= ROUND((28.0*iSizeMultiplier)*((iLengthOfString)/2.0))
	ENDIF
		
	FLOAT fCarryOverX = 0
		
	INT i = 0
	FOR i = 0 TO iLengthOfString-1
		
		TEXT_LABEL_63 sLetterTexture = "LET_"
		TEXT_LABEL_63 sCharacter = "FULLSTOP"

		sCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sTextToDisplay, i, i+1)	
		VECTOR_2D vScaleFinal = MULTIPLY_VECTOR_2D(vBaseLetterScale, TO_FLOAT(iSizeMultiplier))
		
		IF ARE_STRINGS_EQUAL(sCharacter, " ")
			vDisplayFinalPos.x += vBaseLetterScale.x // Add the size of a sprite Before displaying the next one.
			RELOOP
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ".")
			sCharacter = "FULLSTOP"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ",")
			sCharacter = "COMMA"
		ENDIF
				
		sLetterTexture += sCharacter

		vDisplayFinalPos.x += (fCarryOverX)
		fCarryOverX = 0
				
		ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures,
			sLetterTexture, vDisplayFinalPos, vScaleFinal, 0.0, rgbaNearOpaque)

		IF ARE_STRINGS_EQUAL(sCharacter, ".")
		OR ARE_STRINGS_EQUAL(sCharacter, ":")
		OR ARE_STRINGS_EQUAL(sCharacter, "!")
			fCarryOverX = -(vScaleFinal.x/8)
		ENDIF
		
		IF vScaleFinal.x  > (vBaseLetterScale.x*iSizeMultiplier)
			vScaleFinal.x = (vBaseLetterScale.x*iSizeMultiplier)
		ENDIF
		
		vDisplayFinalPos.x += vScaleFinal.x // Add the size of a sprite Before displaying the next one.
		
		
		IF vScaleFinal.x  > (vBaseLetterScale.x*iSizeMultiplier)
			vScaleFinal.x = (vBaseLetterScale.x*iSizeMultiplier)
		ENDIF
		
		vDisplayFinalPos.x += vScaleFinal.x // Add the size of a sprite Before displaying the next one.
		
		
	ENDFOR
	
ENDPROC

PROC PZ_ARCADE_GET_SCORE(INT &scoreArray[], INT iScore)

	FLOAT iLocalScore = TO_FLOAT(iScore)
	
	Int iHundThs = FLOOR((iLocalScore/POW(10,5)))
	iLocalScore -= (iHundThs*POW(10,5))
	scoreArray[0] = iHundThs // Add the number to the return string
	
	Int iTenThs = FLOOR((iLocalScore/POW(10,4)))
	iLocalScore -= (iTenThs*POW(10,4))
	scoreArray[1] = iTenThs // Add the number to the return string
	
	Int iThs = FLOOR((iLocalScore/POW(10,3)))
	iLocalScore -= (iThs*POW(10,3))
	scoreArray[2] = iThs // Add the number to the return string
	
	Int iHund = FLOOR((iLocalScore/POW(10,2)))
	iLocalScore -= (iHund*POW(10,2))
	scoreArray[3] = iHund // Add the number to the return string
	
	Int iTen = FLOOR((iLocalScore/10))
	iLocalScore -= (iTen*10)
	scoreArray[4] = iTen // Add the number to the return string
	
	Int iSing = FLOOR((iLocalScore))
	iLocalScore -= iSing
	scoreArray[5] = iSing // Add the number to the return string
	
ENDPROC

PROC PZ_DRAW_SCORE(INT iScore, VECTOR_2D vScoreNumStart)

	INT iScoreArray[6] 
	PZ_ARCADE_GET_SCORE(iScoreArray, iScore)
	
	vScoreNumStart.x += 56
	
	INT i	
	FOR i = 0 TO 5
	
		VECTOR_2D vPos = vScoreNumStart
		VECTOR_2D vScale = INIT_VECTOR_2D(28, 32)
		
		TEXT_LABEL_63 sScoreNumber = "NO_BIG_"
		sScoreNumber += iScoreArray[i]
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, sScoreNumber, 
				vPos, 
				vScale
				, 0.0, rgbaNearOpaque)
		
		vScoreNumStart.x += 28
	
	ENDFOR

ENDPROC

PROC PZ_DRAW_LEVEL(INT iScore, VECTOR_2D vScoreNumStart)

	INT iScoreArray[2]
	
	FLOAT iLocalScore = TO_FLOAT(iScore)
	Int iTen = FLOOR((iLocalScore/10))
	iLocalScore -= (iTen*10)
	iScoreArray[0] = iTen // Add the number to the return string
	
	Int iSing = FLOOR((iLocalScore))
	iLocalScore -= iSing
	iScoreArray[1] = iSing // Add the number to the return string
	
	vScoreNumStart.x += 56
	
	INT i	
	FOR i = 0 TO 1
	
		VECTOR_2D vPos = vScoreNumStart
		VECTOR_2D vScale = INIT_VECTOR_2D(28, 32)
		
		TEXT_LABEL_63 sScoreNumber = "NO_BIG_"
		sScoreNumber += iScoreArray[i]
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, sScoreNumber, 
				vPos, 
				vScale
				, 0.0, rgbaNearOpaque)
		
		vScoreNumStart.x += 32
	
	ENDFOR

	IF iScore > sQub3dTelemetry.level
		sQub3dTelemetry.level = iScore
	ENDIF

ENDPROC


//////////////////////////////
/// * APPLY CUBE COLOUR *  ///
//////////////////////////////
///    
///    Tint ID 
///    ID:0-Black, 
///    ID:1-Red, 
///    ID:2-Blue, 
///    ID:3-Purple, 
///    ID:4-Orange, 
///    ID:5-Green, 
///    ID:6-Yellow, 
///    ID:7-White 
PROC QUB3D_SET_CUBE_COLOR(OBJECT_INDEX &obj, BOARD_GRID_CONTENTS iColourIndex)
	
	INT iPickedColour = 0
	
	SWITCH iColourIndex		

		// Black
		CASE EMPTY
			iPickedColour =	7
		BREAK
		
		// Red
		CASE RED
			iPickedColour =	1
		BREAK
		
		// Blue
		CASE BLUE	
			iPickedColour =	2
		BREAK
		
		// Purple
		CASE PURPLE	
			iPickedColour =	3
		BREAK
	
		// Orange
		CASE ORANGE_CUBE
			iPickedColour =	4
		BREAK
		
		// Green
		CASE GREEN
			iPickedColour =	5
		BREAK
		
		// Yellow
		CASE YELLOW	
			iPickedColour =	6
		BREAK
		
	ENDSWITCH
	
	if DOES_ENTITY_EXIST(obj)
		SET_OBJECT_TINT_INDEX(obj, iPickedColour)
	ENDIF
		
ENDPROC

//////////////////////////////
// *** OTHER PROCEDURES *** //
//////////////////////////////

PROC SETUP_THE_TEXT(FLOAT this_width, FLOAT this_height, eTextJustification text_justify = FONT_LEFT)
	
	global_text_alpha = 255 - global_alpha
	IF global_text_alpha > 250
		global_text_alpha  =250
	ENDIF
	
	SET_TEXT_SCALE (this_width, this_height)
	SET_TEXT_COLOUR (255, 255, 255, global_text_alpha)	
	
	SET_TEXT_FONT(fMainFont)
	
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_WRAP(0.0, 1.0)
	
	
	IF text_justify = FONT_LEFT
		SET_TEXT_JUSTIFY(TRUE)
	ENDIF
	
	IF text_justify = FONT_RIGHT
		SET_TEXT_RIGHT_JUSTIFY(TRUE)
	ENDIF
	
	IF text_justify = FONT_CENTRE
		SET_TEXT_CENTRE(TRUE)
	ENDIF
		
ENDPROC


////////////////////////////////////////
////// **** SOUND MANAGEMENT **** //////
////////////////////////////////////////

FUNC BOOL QUB3D_AUDIO_START_AUDIO_SCENE(ARCADE_GAMES_AUDIO_SCENE scene)
	IF qub3edAudioScene_Active != scene
		ARCADE_GAMES_AUDIO_SCENE_STOP(qub3edAudioScene_Active) // Safely stop the original scene
		qub3edAudioScene_Active = scene // Store the current scene
	ENDIF

	ARCADE_GAMES_AUDIO_SCENE_START(scene)
		
	// Assess imediately to avoid 1 frame delays
	IF IS_AUDIO_SCENE_ACTIVE(ARCADE_GAMES_AUDIO_SCENE_GET_NAME(scene))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE_THE_GRID()

	FLOAT current_column_f
	FLOAT current_row_f
	
	/// ***** remove any boxes on the game grid
	IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
		DELETE_OBJECT(falling_box.falling_box_a)
	ENDIF
	falling_box.is_box_a_on_grid = FALSE
	
	IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
		DELETE_OBJECT(falling_box.falling_box_b)
	ENDIF
	falling_box.is_box_b_on_grid = FALSE
	
	FOR current_column = 0 TO (number_of_columns-1)
		// initialise the starting column
		//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 1450")
		
		FOR current_row = 0 TO (number_of_rows-1)
			//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 1453")
			// set the initial values in the grid
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				DELETE_OBJECT(grid_box[current_row][current_column].box_object)
			ENDIF
			
			// convert the int for colum and row to their world coords
			current_row_f = starting_x + (current_row/40.0)
			current_column_f = starting_y + (current_column/40.0)
			grid_box[current_row][current_column].box_object_pos = <<current_row_f, current_column_f, bottom_z>>			
			grid_box[current_row][current_column].position_in_col = current_row
			grid_box[current_row][current_column].position_in_row = current_column
			grid_box[current_row][current_column].this_box_colour = EMPTY
			grid_box[current_row][current_column].is_box_on_grid = FALSE
			
			grid_box[current_row][current_column].is_large_box = FALSE
			
			// set this grids spaces status		
			block_status[current_row][current_column] = REMAINS
		ENDFOR
	ENDFOR
	
	//set any required flags
	direction_pressed  					= PRESSED_NOTHING
	first_time_through 					= TRUE
	
	is_button_still_pressed				= TRUE
	is_player_moving_block 				= FALSE
	has_front_end_been_setup			= FALSE
	has_front_end_grid_been_setup 		= FALSE
	is_player_forcing_blocks_speed_up 	= FALSE
	slow_down_powerup_in_effect			= FALSE
	colour_removal_powerup_in_effect		= FALSE
	eraser_powerup_in_effect			= FALSE
	bomb_powerup_in_effect				= FALSE
	time_between_block_movement 		= start_time_between_block_movement//1000
	blocks_fall_speed_mulitplier 		= 0
	bRemovedFromPlay = FALSE
	
	/// initialise the scores
	total_score 		= 0
	current_score 		= 0
	score_multiplyer 	= 0//1
	current_level 		= 1
	option_selected 	= START_OPTION
	
	
	/// initialise power bars
	has_a_power_up_been_selected 			= FALSE
	slow_down_powerup_in_effect				= FALSE
	is_cross_pressed						= TRUE
	is_square_pressed						= TRUE
	is_circle_pressed						= TRUE
	is_triangle_pressed						= TRUE
	number_of_powerup_objs_to_display		= 0
	number_of_powerup_objs_being_displayed	= 0
	number_of_power_uses					= 0
	
	INT loop_counter
	FOR loop_counter = 0 TO (number_to_fill_power_bar-1)
		power_up_ready[loop_counter] = FALSE
	ENDFOR
	
	// Set gameplay music
	SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",1.0)
	QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_GAMEPLAY)
	
	// start the game loop
	current_mission_stage = GAME_IN_PROGRESS
	
ENDPROC

PROC CREATE_A_RANDOM_GRID()
	FOR current_column = min_col TO max_col
		// initialise the starting column
		FOR current_row = min_row TO (number_of_rows - 1)
			
			// set the initial values in the grid
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				DELETE_OBJECT(grid_box[current_row][current_column].box_object)
				//SET_OBJECT_AS_NO_LONGER_NEEDED(grid_box[current_row][current_column].box_object)
			ENDIF
			
			// select a random colour for the box
			grid_box[current_row][current_column].this_box_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours))
			// create the block
			QUB3d_CREATE_OBJECT(grid_box[current_row][current_column].box_object, box_model, grid_box[current_row][current_column].box_object_pos)
			
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, 0.0)
				SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
				FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
				QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, grid_box[current_row][current_column].this_box_colour)
			ENDIF
						
			/// update flags in the struct
			grid_box[current_row][current_column].is_box_on_grid = TRUE

			block_status[current_row][current_column] = REMAINS
		
		ENDFOR
		
	ENDFOR
	
	/// remove the power up icons
	INT loop_counter
	FOR loop_counter = 0 TO (number_to_fill_power_bar - 1)
		//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 1581")
		power_up_ready[loop_counter] = FALSE
	ENDFOR

ENDPROC


PROC CREATE_A_BOMBABLE_GRID()
	FOR current_column = 0 TO (number_of_columns -1)
		// initialise the starting column
		FOR current_row = min_row TO (number_of_rows - 1)
			
			// set the initial values in the grid
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				DELETE_OBJECT(grid_box[current_row][current_column].box_object)
				//SET_OBJECT_AS_NO_LONGER_NEEDED(grid_box[current_row][current_column].box_object)
			ENDIF
			
			// select a random colour for the box
			grid_box[current_row][current_column].this_box_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours))
			// create the block
			QUB3d_CREATE_OBJECT(grid_box[current_row][current_column].box_object, box_model, grid_box[current_row][current_column].box_object_pos)

			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, 0.0)
				SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
				FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
				QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, grid_box[current_row][current_column].this_box_colour)
			ENDIF
						
			/// update flags in the struct
			grid_box[current_row][current_column].is_box_on_grid = TRUE

			block_status[current_row][current_column] = REMAINS
		
		ENDFOR
		
	ENDFOR
	
ENDPROC

PROC CREATE_AN_ERASABLE_GRID()
	FOR current_column = 0 TO (number_of_columns -1)
		// initialise the starting column
		FOR current_row = (max_row - 3) TO max_row
			
			// set the initial values in the grid
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				DELETE_OBJECT(grid_box[current_row][current_column].box_object)
				//SET_OBJECT_AS_NO_LONGER_NEEDED(grid_box[current_row][current_column].box_object)
			ENDIF
			
			// select a random colour for the box
			grid_box[current_row][current_column].this_box_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours))
			// create the block
			QUB3d_CREATE_OBJECT(grid_box[current_row][current_column].box_object, box_model, grid_box[current_row][current_column].box_object_pos)
						
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "CREATE_A_RANDOM_GRID - Creating box on the grid at X: ", grid_box[current_row][current_column].box_object_pos.x)
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "CREATE_A_RANDOM_GRID - Creating box on the grid at Y: ", grid_box[current_row][current_column].box_object_pos.y)
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "CREATE_A_RANDOM_GRID - Creating box on the grid at Z: ", grid_box[current_row][current_column].box_object_pos.z)
			
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
				SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, 0.0)
				SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
				FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
				QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, grid_box[current_row][current_column].this_box_colour)
			ENDIF
						
			/// update flags in the struct
			grid_box[current_row][current_column].is_box_on_grid = TRUE

			block_status[current_row][current_column] = REMAINS
		
		ENDFOR
		
	ENDFOR
	
ENDPROC
	
////////////////////////////////////////
// *** TEXT AND GRAPHIC FUNCTIONS *** //
////////////////////////////////////////

//INFO: DISPLAY_TEXT resets all text settings after use, so make sure you call SET_TEXT_SCALE, SET_TEXT_BACKGROUND etc. each time you call DISPLAY_TEXT. 
//PARAM NOTES: Display x & y are between 0, 0 (top left) and 1, 1 (bottom right)
//PURPOSE:  Displays text to main render target. More info..
PROC QUB3D_DISPLAY_TEXT(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, INT Stereo = 0)
	//SET_TEXT_LINE_HEIGHT_MULT()
	//TEXT_FONTS
	SET_TEXT_FONT(fMainFont)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
	
	DisplayAtX *= (1 / cfBASE_SCREEN_WIDTH)
	DisplayAtY *= (1 / cfBASE_SCREEN_HEIGHT)
	
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY, Stereo)
ENDPROC
PROC SCORE_OBJECTS_UPDATER()
	/// players current score			
	ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_SCORE", 
		INIT_VECTOR_2D(ig_score_txt_pos_x, ig_score_txt_pos_y), 
		INIT_VECTOR_2D(ig_score_txt_size_x, ig_score_txt_size_y), 0.0, rgbaWhiteOut)
	
	PZ_DRAW_SCORE(total_score, INIT_VECTOR_2D(ig_score_num_pos_x, ig_score_num_pos_y))
				
	ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_HISCORE", 
		INIT_VECTOR_2D(ig_hiscore_txt_pos_x, ig_hiscore_txt_pos_y), 
		INIT_VECTOR_2D(ig_hiscore_txt_size_x, ig_hiscore_txt_size_y), 0.0, rgbaWhiteOut)
		
	PZ_DRAW_SCORE(session_high_score, INIT_VECTOR_2D(ig_hiscore_num_pos_x, ig_hiscore_num_pos_y))
		
ENDPROC

PROC LEVEL_OBJECTS_UPDATER()
	///  the high score
	ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_LEVEL",
		INIT_VECTOR_2D(ig_level_txt_pos_x, ig_level_txt_pos_y),
		INIT_VECTOR_2D(ig_level_txt_size_x, ig_level_txt_size_y),
		0.0, rgbaWhiteOut)

	PZ_DRAW_LEVEL(current_level, INIT_VECTOR_2D(ig_level_num_pos_x, ig_level_num_pos_y))
	
	IF current_level > sQub3dTelemetry.level
		sQub3dTelemetry.level = current_level
	ENDIF
ENDPROC

PROC POWER_BAR_OBJECT_UPDATER()
	
	/// PURPOSE: adds objects to the power bar as the player removes the larger blocks from the game grid
	//INT alpha_value	
	
	INT non_active_alpha_value = 150	
	INT active_alpha_value = 255
	//INT eraser
	
	
	INT eraser_r
	INT eraser_g
	INT eraser_b 
	
	INT bomb_r
	INT bomb_g 
	INT bomb_b
	
	INT random_r
	INT random_g
	INT random_b
	
	INT slow_r
	INT slow_g
	INT slow_b
	
	
	IF IS_XBOX_PLATFORM()
	OR IS_PC_VERSION()
		//		Red (B button) = 167, 4, 0
		eraser_r = 167
		eraser_g = 4
		eraser_b = 0
		//		Green (A button) = 77, 151, 5
		bomb_r = 77
		bomb_g = 151
		bomb_b = 5
		//		Blue (X button) = 24, 72, 170
		random_r = 24		
		random_g = 72
		random_b = 170
		//		Yellow (Y button) = 179, 147, 0
		slow_r = 179
		slow_g = 147
		 slow_b = 0
	ELSE // IS PS4 Version
		//		Red (circle button) = 204  43  71
		eraser_r = 204
		eraser_g = 43
		eraser_b = 71
		//		blue (cross button) = 108  130  187
		bomb_r = 108
		bomb_g = 130
		bomb_b = 187
		//		pink (square button) = 220  136  186
		random_r = 220		
		random_g = 136
		random_b = 186
		//		green (triangle button) = 134  178  135
		slow_r = 134
		slow_g = 178
		slow_b = 135
	ENDIF
	
	IF number_of_powerup_objs_to_display > 0
		// dependiing on number_of_powerup_objs_to_display draw a suitably large rectange
		IF number_of_powerup_objs_to_display = 1
			ARCADE_DRAW_PIXELSPACE_RECT (INIT_VECTOR_2D(ig_power_bar_inner_1_pos_x, ig_power_bar_inner_1_pos_y), INIT_VECTOR_2D(ig_power_bar_inner_1_size_x, ig_power_bar_inner_1_size_y), rgbaNearOpaque)
		ELSE
			IF number_of_powerup_objs_to_display = 2
				ARCADE_DRAW_PIXELSPACE_RECT (INIT_VECTOR_2D(ig_power_bar_inner_2_pos_x, ig_power_bar_inner_2_pos_y), INIT_VECTOR_2D(ig_power_bar_inner_2_size_x, ig_power_bar_inner_2_size_y), rgbaNearOpaque)
			ELSE
				IF number_of_powerup_objs_to_display = 3
					ARCADE_DRAW_PIXELSPACE_RECT (INIT_VECTOR_2D(ig_power_bar_inner_3_pos_x, ig_power_bar_inner_3_pos_y), INIT_VECTOR_2D(ig_power_bar_inner_3_size_x, ig_power_bar_inner_3_size_y), rgbaNearOpaque)
				ELSE
					ARCADE_DRAW_PIXELSPACE_RECT (INIT_VECTOR_2D(ig_power_bar_inner_4_pos_x, ig_power_bar_inner_4_pos_y), INIT_VECTOR_2D(ig_power_bar_inner_4_size_x, ig_power_bar_inner_4_size_y), rgbaNearOpaque)
				ENDIF
			ENDIF
		ENDIF
			
			
	ENDIF

	// the bar	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, power_bar_texture, INIT_VECTOR_2D(ig_power_bar_pos_x,  ig_power_bar_pos_y), INIT_VECTOR_2D(ig_power_bar_size_x, ig_power_bar_size_y), 0.0000, rgbaWhiteOut)
	ENDIF
	
	// multiplyer
	IF (number_of_power_uses > 0 )
		ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "LET_X", 
		INIT_VECTOR_2D(ig_power_uses_txt_pos_x, ig_power_uses_txt_pos_y),
		INIT_VECTOR_2D(20, 20),
		0.0, rgbaNearOpaque)
		
		IF number_of_power_uses = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "NO_BIG_1", 
				INIT_VECTOR_2D(ig_power_uses_txt_pos_x + 20, ig_power_uses_txt_pos_y),
				INIT_VECTOR_2D(20, 20),
				0.0, rgbaNearOpaque)
		ENDIF
		
		IF number_of_power_uses = 2
			ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "NO_BIG_2", 
				INIT_VECTOR_2D(ig_power_uses_txt_pos_x + 20, ig_power_uses_txt_pos_y),
				INIT_VECTOR_2D(20, 20),
				0.0, rgbaNearOpaque)
		ENDIF
		
		IF number_of_power_uses = 3
			ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "NO_BIG_3",
				INIT_VECTOR_2D(ig_power_uses_txt_pos_x + 20, ig_power_uses_txt_pos_y),
				INIT_VECTOR_2D(20, 20),
				0.0, rgbaNearOpaque)
		ENDIF
	ENDIF
		
	// sprites (apha dependant on if its avail, rgb based off of buttons
		
	INIT_RGBA_STRUCT(rgbaEraserActive, eraser_r, eraser_g, eraser_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaEraserNonActive, eraser_r, eraser_g, eraser_b, non_active_alpha_value)
	INIT_RGBA_STRUCT(rgbaBombActive, bomb_r, bomb_g, bomb_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaBombNonActive, bomb_r, bomb_g, bomb_b, non_active_alpha_value)
	INIT_RGBA_STRUCT(rgbaRandomActive, random_r, random_g, random_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaRandomNonActive, random_r, random_g, random_b, non_active_alpha_value)
	INIT_RGBA_STRUCT(rgbaSlowActive, slow_r, slow_g, slow_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaSlowNonActive, slow_r, slow_g, slow_b, non_active_alpha_value)
	
	// if any are active they are highlighted rest are dimmed
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		IF number_of_power_uses > 0
		
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_1, INIT_VECTOR_2D(ig_special_1st_power_pos_x,  ig_special_1st_power_pos_y), INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaEraserActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_2, INIT_VECTOR_2D(ig_special_2nd_power_pos_x,  ig_special_2nd_power_pos_y),  INIT_VECTOR_2D(ig_special_2nd_power_size_x ,  ig_special_2nd_power_size_y), 0.0000, rgbaBombActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_3, INIT_VECTOR_2D(ig_special_3rd_power_pos_x,  ig_special_3rd_power_pos_y) ,  INIT_VECTOR_2D(ig_special_3rd_power_size_x ,  ig_special_3rd_power_size_y), 0.0000, rgbaRandomActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_4, INIT_VECTOR_2D(ig_special_4th_power_pos_x,  ig_special_4th_power_pos_y) ,  INIT_VECTOR_2D(ig_special_4th_power_size_x ,  ig_special_4th_power_size_y), 0.0000, rgbaSlowActive)
			
		ELSE // Power ups not active
			// if none available all are dimmed
			
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_1, INIT_VECTOR_2D(ig_special_1st_power_pos_x,  ig_special_1st_power_pos_y), INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaEraserNonActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_2, INIT_VECTOR_2D(ig_special_2nd_power_pos_x,  ig_special_2nd_power_pos_y),  INIT_VECTOR_2D(ig_special_2nd_power_size_x ,  ig_special_2nd_power_size_y), 0.0000, rgbaBombNonActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_3, INIT_VECTOR_2D(ig_special_3rd_power_pos_x,  ig_special_3rd_power_pos_y) ,  INIT_VECTOR_2D(ig_special_3rd_power_size_x ,  ig_special_3rd_power_size_y), 0.0000, rgbaRandomNonActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_4, INIT_VECTOR_2D(ig_special_4th_power_pos_x,  ig_special_4th_power_pos_y) ,  INIT_VECTOR_2D(ig_special_4th_power_size_x ,  ig_special_4th_power_size_y), 0.0000, rgbaSlowNonActive)
			
		ENDIF
	ENDIF
	
	BOOL change_in_number_to_be_displayed
	INT number_already_added
	
	IF NOT (number_of_power_uses = max_number_of_power_uses)
		IF number_of_powerup_objs_to_display > number_to_fill_power_bar
			number_of_powerup_objs_to_display = number_to_fill_power_bar
		ENDIF
	ELSE
		number_of_powerup_objs_to_display = 0
	ENDIF
	
	IF number_of_powerup_objs_being_displayed < number_of_powerup_objs_to_display
		change_in_number_to_be_displayed = TRUE
	ELSE
		change_in_number_to_be_displayed = FALSE
	ENDIF
	
	/// if the bar isn't already full
	IF NOT (number_of_power_uses = max_number_of_power_uses)//power_bar_full// = FALSE
		// determine if another part of the power bar should be added		
		IF change_in_number_to_be_displayed
			number_of_powerup_objs_being_displayed = 0
			INT loop_counter
			FOR loop_counter = 0 TO (number_to_fill_power_bar - 1)
				// see if this part of the power bar is being displayed
				IF NOT power_up_ready[loop_counter]
					// if we should add another part of the bar
					IF loop_counter < number_of_powerup_objs_to_display
						// update the flags
						power_up_ready[loop_counter] = TRUE
						number_of_powerup_objs_being_displayed++
					ELSE
						// exit the loop as dont need to add anymore
						loop_counter = number_to_fill_power_bar
					ENDIF
				ELSE
					number_of_powerup_objs_being_displayed++
				ENDIF
				
			ENDFOR
		
			// is there enough in power bar to allow player to access power ups
			IF number_of_powerup_objs_being_displayed = number_to_fill_power_bar
					
				number_of_power_uses++
				IF number_of_power_uses > max_number_of_power_uses
					number_of_power_uses = max_number_of_power_uses
				ENDIF

				//determine which array index to add
				number_already_added = number_of_power_uses - 1
				
				// if not currently any on screen
				IF number_already_added <= 0
					number_already_added = 0
					// add the icons for the powerup choices here
				ENDIF

				// play audio
				PLAY_SOUND_FRONTEND(-1, got_powerup_sfx)				

				// reset the power bar
				FOR loop_counter = 0 TO (number_to_fill_power_bar - 1)
				//WHILE loop_counter >= 0
					//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 2085")
					power_up_ready[loop_counter] = FALSE
					//loop_counter--
				ENDFOR
				
				number_of_powerup_objs_to_display		= 0
				number_of_powerup_objs_being_displayed	= 0				
			ENDIF
		ENDIF

	ENDIF
ENDPROC

PROC PREVIEW_BLOCK_UPDATER()
	INT block_r[6]
	INT block_g[6]
	INT block_b[6]
	
	// need to determine the preview block colour
	block_r[RED]		= 255	 block_g[RED]		= 0			block_b[RED]		= 0
	block_r[BLUE]		= 0	 	block_g[BLUE]		= 248			block_b[BLUE]		= 255
	block_r[GREEN]		= 0	 	block_g[GREEN]		= 250		block_b[GREEN]		= 0
	block_r[YELLOW]		= 255	 block_g[YELLOW]	= 255		block_b[YELLOW]		= 0
	block_r[PURPLE]		= 255	 block_g[PURPLE]	= 0			block_b[PURPLE]		= 255
	block_r[ORANGE_CUBE]		= 255	 block_g[ORANGE_CUBE]	= 165		block_b[ORANGE_CUBE]		= 0 

	RGBA_COLOUR_STRUCT rgbaBlock
	INIT_RGBA_STRUCT(rgbaBlock, block_r[preview_box.block_a_colour], block_g[preview_box.block_a_colour], block_b[preview_box.block_a_colour], preview_block_alpha)

	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, preview_block_texture, 
			INIT_VECTOR_2D(ig_next_left_block_pos_x,  ig_next_left_block_pos_y),  
			INIT_VECTOR_2D(ig_next_left_block_size_x ,  ig_next_left_block_size_y), 
			0.0000, 
			rgbaBlock)
	ENDIF
	
	INIT_RGBA_STRUCT(rgbaBlock, block_r[preview_box.block_b_colour], block_g[preview_box.block_b_colour], block_b[preview_box.block_b_colour], preview_block_alpha)
	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, preview_block_texture, 
			INIT_VECTOR_2D(ig_next_right_block_pos_x,  ig_next_right_block_pos_y), 
			INIT_VECTOR_2D(ig_next_right_block_size_x ,  ig_next_right_block_size_y), 
			0.0000,
			rgbaBlock)
	ENDIF
	
ENDPROC


PROC ARCADE_DRAW_SPRITEANGLE_BORDER(FLOAT p_position_x, FLOAT p_position_y, FLOAT p_size_x, FLOAT p_size_y)
	
	// draws the white borders round a passed tables paramemters
	
	// variables
	FLOAT pos_x, pos_y 
	FLOAT size_x, size_y
	
	// left side
	pos_x  = p_position_x - (p_size_x / 2.0)
	pos_y  = p_position_y
	size_x = border_rect_size.x
	size_y = p_size_y + border_rect_size.y
	DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(pos_x, pos_y), INIT_VECTOR_2D(size_x, size_y), rgbaNearOpaque)

	// right side
	pos_x  = p_position_x + (p_size_x / 2.0)
	pos_y  = p_position_y
	size_x = border_rect_size.x
	size_y = p_size_y + border_rect_size.y
	DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(pos_x, pos_y), INIT_VECTOR_2D(size_x, size_y), rgbaNearOpaque)
	
	// top 
	pos_x  = p_position_x 
	pos_y  = p_position_y + (p_size_y / 2.0)
	size_x = p_size_x + border_rect_size.x
	size_y = border_rect_size.y
	DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(pos_x, pos_y), INIT_VECTOR_2D(size_x, size_y), rgbaNearOpaque)
	
	// bottom
	pos_x  = p_position_x 
	pos_y  = p_position_y - (p_size_y / 2.0)
	size_x = p_size_x + border_rect_size.x
	size_y = border_rect_size.y
	DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(pos_x, pos_y), INIT_VECTOR_2D(size_x, size_y), rgbaNearOpaque)
	
ENDPROC

PROC QUB3D_SHOW_INGAME_OBJECTS()
	
	/// make the objects visible again
	
	IF DOES_ENTITY_EXIST(grid_object)
		SET_ENTITY_VISIBLE(grid_object, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(preview_box.falling_box_a)
		SET_ENTITY_VISIBLE(preview_box.falling_box_a, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(preview_box.falling_box_b)
		SET_ENTITY_VISIBLE(preview_box.falling_box_b, TRUE)
	ENDIF

ENDPROC

PROC QUB3D_GET_STICK_INPUT(float &left_stick_x, float &left_stick_y, float &right_stick_x, float &right_stick_y)

	left_stick_x = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) 
	left_stick_y = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) 
	right_stick_x = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) 
	right_stick_y = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) 

ENDPROC

/// PURPOSE: Checks to see if player is currently pressing the menu accept
/// RETURNS: TRUE if the player is pressing the button, FALSE if they are not
FUNC BOOL QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	//OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the menu accept button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	//OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player is currently pressing the menu decline
/// RETURNS: TRUE if the player is pressing the button, FALSE if they are not
FUNC BOOL QUB3d_IS_MENU_DECLINE_BUTTON_PRESSED()

	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the menu decline button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL QUB3d_IS_MENU_DECLINE_BUTTON_JUST_PRESSED()
	
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC DISPLAY_FRONT_END_MENU(BOOL allow_player_control = TRUE)

	VECTOR title_rect_pos = <<0.4950, 0.2970, 0.0>>
	VECTOR title_rect_size = <<0.375, 0.3130, 0.0>>
	
	FLOAT logo_pos_x = 0.4950
	FLOAT logo_pos_y = 0.2640
	
	FLOAT logo_size_x = 0.3625
	FLOAT logo_size_y = 0.196

	FLOAT strap_size_x = 0.35833
	FLOAT strap_size_y = 0.06

	FLOAT strap_pos_x = 0.494
	FLOAT strap_pos_y = 0.41
	
	// OPTION VARIABLES 
	INT option_selected_int
	FLOAT left_stick_x, left_stick_y
	FLOAT right_stick_x, right_stick_y
	
	IF NOT has_front_end_grid_been_setup
		/// create a random grid
		CREATE_A_RANDOM_GRID()
		has_front_end_grid_been_setup = TRUE
	ENDIF
	
	IF allow_player_control
		IF NOT has_front_end_been_setup	
			has_front_end_been_setup = TRUE
		ENDIF
	ENDIF

	IF NOT ingame_music_playing
		current_time = TO_FLOAT(GET_GAME_TIMER())
		// time since last one added
		time_difference = current_time - time_started			
		IF time_difference >= 2000
			Qub3dMusicSoundID = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(Qub3dMusicSoundID, "Music_Dynamic_Banked", "sum20_am_Qub3d_sounds")
			SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",0.0)
			QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_MENUS)
			ingame_music_playing = TRUE
		ENDIF
	ENDIF
	
	DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(title_rect_pos.x, title_rect_pos.y), INIT_VECTOR_2D(title_rect_size.x, title_rect_size.y), rgbaDarkSeeThrough)//title
	// draw the sprites	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(high_score_textures)
		ARCADE_CABINET_DRAW_SPRITE(high_score_textures, logo_texture, logo_pos_x, logo_pos_y, logo_size_x, logo_size_y, 0.0000, rgbaWhiteout)
	ENDIF
	
	ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, strapline_quote, strap_pos_x, strap_pos_y, strap_size_x, strap_size_y, 0.0, rgbaWhiteOut)
	
	ARCADE_DRAW_SPRITEANGLE_BORDER(title_rect_pos.x, title_rect_pos.y, title_rect_size.x, title_rect_size.y)
	DRAW_RECT_FROM_VECTOR_2D(INIT_VECTOR_2D(title_rect_pos.x, 0.365), INIT_VECTOR_2D((title_rect_size.x + border_rect_size.x), border_rect_size.y), rgbaNearOpaque)// quit

	// display the list
	IF option_selected = START_OPTION
		
		DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(start_rect_pos.x, start_rect_pos.y), INIT_VECTOR_2D(start_rect_size.x, start_rect_size.y), rgbaWhiteOut)// start
		DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(tut_rect_pos.x, tut_rect_pos.y), INIT_VECTOR_2D(tut_rect_size.x, tut_rect_size.y), rgbaDarkSeeThrough)// tut
		DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(hiscore_rect_pos.x, hiscore_rect_pos.y), INIT_VECTOR_2D(hiscore_rect_size.x, hiscore_rect_size.y), rgbaDarkSeeThrough)// hiscore		
		
		IF IS_ACCOUNT_OVER_17_FOR_UGC()
			DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(quit_rect_pos.x, quit_rect_pos.y), INIT_VECTOR_2D(quit_rect_size.x, quit_rect_size.y), rgbaDarkSeeThrough)// quit
		ENDIF
		
		ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, start_option_txt, start_txt_x, start_txt_y, 0.156, 0.048, 0.0, rgbaBlackOut)
		ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, tutorial_option_txt, tutorial_txt_x, tutorial_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
		
		IF IS_ACCOUNT_OVER_17_FOR_UGC()
			ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, hiscore_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)		
			ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, quit_txt_x, quit_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
		ELSE
			ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
		ENDIF
	ELSE
		IF option_selected = TUTORIAL_OPTION
		
			DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(start_rect_pos.x, start_rect_pos.y), INIT_VECTOR_2D(start_rect_size.x, start_rect_size.y), rgbaDarkSeeThrough)// start
			DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(tut_rect_pos.x, tut_rect_pos.y), INIT_VECTOR_2D(tut_rect_size.x, tut_rect_size.y), rgbaWhiteOut)// tut
			DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(hiscore_rect_pos.x, hiscore_rect_pos.y), INIT_VECTOR_2D(hiscore_rect_size.x, hiscore_rect_size.y), rgbaDarkSeeThrough)// hiscore		
	
			IF IS_ACCOUNT_OVER_17_FOR_UGC()
				DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(quit_rect_pos.x, quit_rect_pos.y), INIT_VECTOR_2D(quit_rect_size.x, quit_rect_size.y), rgbaDarkSeeThrough)// quit
			ENDIF
			
			// quit option selected  QUIT_OPTION			
			ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, start_option_txt, start_txt_x, start_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
			ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, tutorial_option_txt, tutorial_txt_x, tutorial_txt_y, 0.156, 0.048, 0.0, rgbaBlackOut)

			IF IS_ACCOUNT_OVER_17_FOR_UGC()
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, hiscore_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)							
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, quit_txt_x, quit_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
			ELSE
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
			ENDIF
						
		ELSE
			IF option_selected = QUIT_OPTION
			
				IF IS_ACCOUNT_OVER_17_FOR_UGC()
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(start_rect_pos.x, start_rect_pos.y), INIT_VECTOR_2D(start_rect_size.x, start_rect_size.y), rgbaDarkSeeThrough)// start
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(tut_rect_pos.x, tut_rect_pos.y), INIT_VECTOR_2D(tut_rect_size.x, tut_rect_size.y), rgbaDarkSeeThrough)// tut
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(hiscore_rect_pos.x, hiscore_rect_pos.y), INIT_VECTOR_2D(hiscore_rect_size.x, hiscore_rect_size.y), rgbaDarkSeeThrough)// hiscore		
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(quit_rect_pos.x, quit_rect_pos.y), INIT_VECTOR_2D(quit_rect_size.x, quit_rect_size.y), rgbaWhiteOut)// quit
				ELSE
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(start_rect_pos.x, start_rect_pos.y), INIT_VECTOR_2D(start_rect_size.x, start_rect_size.y), rgbaDarkSeeThrough)// start
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(tut_rect_pos.x, tut_rect_pos.y), INIT_VECTOR_2D(tut_rect_size.x, tut_rect_size.y), rgbaDarkSeeThrough)// tut
					DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(hiscore_rect_pos.x, hiscore_rect_pos.y), INIT_VECTOR_2D(hiscore_rect_size.x, hiscore_rect_size.y), rgbaWhiteOut)// quit		
				ENDIF
				
				// quit option selected  QUIT_OPTION			
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, start_option_txt, start_txt_x, start_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, tutorial_option_txt, tutorial_txt_x, tutorial_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
				
				IF IS_ACCOUNT_OVER_17_FOR_UGC()
					ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, hiscore_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
					ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, quit_txt_x, quit_txt_y, 0.156, 0.048, 0.0, rgbaBlackOut)
				ELSE
					ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaBlackOut)
				ENDIF
			ELIF IS_ACCOUNT_OVER_17_FOR_UGC()
				
				// HI_SCORE_OPTION
				DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(start_rect_pos.x, start_rect_pos.y), INIT_VECTOR_2D(start_rect_size.x, start_rect_size.y), rgbaDarkSeeThrough)// start
				DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(tut_rect_pos.x, tut_rect_pos.y), INIT_VECTOR_2D(tut_rect_size.x, tut_rect_size.y), rgbaDarkSeeThrough)// tut
				DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(hiscore_rect_pos.x, hiscore_rect_pos.y), INIT_VECTOR_2D(hiscore_rect_size.x, hiscore_rect_size.y), rgbaWhiteOut)// hiscore		
				DRAW_RECT_FROM_VECTOR_2D (INIT_VECTOR_2D(quit_rect_pos.x, quit_rect_pos.y), INIT_VECTOR_2D(quit_rect_size.x, quit_rect_size.y), rgbaDarkSeeThrough)// quit
				
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, start_option_txt, start_txt_x, start_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
				
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, tutorial_option_txt, tutorial_txt_x, tutorial_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)

				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, hiscore_option_txt, hiscore_txt_x, hiscore_txt_y, 0.156, 0.048, 0.0, rgbaBlackOut)
							
				ARCADE_CABINET_DRAW_SPRITE(puzzle_textures, quit_option_txt, quit_txt_x, quit_txt_y, 0.156, 0.048, 0.0, rgbaWhiteOut)
			ENDIF
		ENDIF
	ENDIF
	
	/////  borders
	ARCADE_DRAW_SPRITEANGLE_BORDER(start_rect_pos.x, start_rect_pos.y, start_rect_size.x, start_rect_size.y)
	ARCADE_DRAW_SPRITEANGLE_BORDER(tut_rect_pos.x, tut_rect_pos.y, tut_rect_size.x, tut_rect_size.y)
	ARCADE_DRAW_SPRITEANGLE_BORDER(hiscore_rect_pos.x, hiscore_rect_pos.y, hiscore_rect_size.x, hiscore_rect_size.y)
	
	IF IS_ACCOUNT_OVER_17_FOR_UGC()
		ARCADE_DRAW_SPRITEANGLE_BORDER(quit_rect_pos.x, quit_rect_pos.y, quit_rect_size.x, quit_rect_size.y)
	ENDIF
	
	////////////////////////////////////////////////////
	//// ******** PLAYER INTERACTION ************* /////
	////////////////////////////////////////////////////
	IF allow_player_control
		IF Qub3D_IS_MENU_ACCEPT_BUTTON_PRESSED()//IS_BUTTON_PRESSED(PAD1, CROSS)
			IF NOT is_cross_pressed
				is_cross_pressed = TRUE
				PLAY_SOUND_FRONTEND( -1, "MENU_SELECT", ARCADE_GAMES_SOUND_GET_SOUNDSET(QUB3D_AUDIO_EFFECT_SCOREBOARD_ENTER))
				
				IF option_selected = HI_SCORE_OPTION
					PRINT_HELP_FOREVER("PZ_RETURN")
					current_mission_stage = HI_SCORE_DISPLAY
				ELSE			
					IF option_selected = START_OPTION
						is_this_tutorial			= FALSE
						is_game_over 		= FALSE
						is_in_danger_zone = FALSE
						// update high score?
						current_mission_stage = STARTING_NEW_GAME
						
						IF NOT HAS_NET_TIMER_STARTED(pz_AnimStruct.sAnimTimer)
							START_NET_TIMER(pz_AnimStruct.sAnimTimer)
						ENDIF
						
						IF NOT ingame_music_playing
							Qub3dMusicSoundID = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(Qub3dMusicSoundID, "Music_Dynamic_Banked", "sum20_am_Qub3d_sounds")
							SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",1.0)
							QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_GAMEPLAY)
							ingame_music_playing = TRUE
						ENDIF
						
					ELSE
						IF option_selected = TUTORIAL_OPTION
							is_this_tutorial 		= TRUE
							is_game_over 		= FALSE
							is_in_danger_zone = FALSE
							current_tutorial_stage 	= tutorial_intro
							current_mission_stage 	= STARTING_NEW_GAME
							
							IF NOT ingame_music_playing
								Qub3dMusicSoundID = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(Qub3dMusicSoundID, "Music_Dynamic_Banked", "sum20_am_Qub3d_sounds")
								SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",1.0)
								QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_GAMEPLAY)
								ingame_music_playing = TRUE
							ENDIF
							
						ELSE				
							CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "PLAYER QUITS VIA MENU")
							CLEANUP_MISSION_PUZZLE()
							
						ENDIF
					ENDIF
				ENDIF
				
				/// remove the spline cameras
				QUB3D_SHOW_INGAME_OBJECTS()
																							
			ENDIF			
		ELSE
			is_cross_pressed = FALSE
		ENDIF
		
	
		/// check analogue sticks
		QUB3D_GET_STICK_INPUT(left_stick_x, left_stick_y, right_stick_x,  right_stick_y)
				
		//// player selecting item
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
		OR left_stick_y >= 0.85 
			current_time = TO_FLOAT(GET_GAME_TIMER())
			time_difference = current_time  - time_last_press 
			IF time_difference >= 250
				time_last_press = TO_FLOAT(GET_GAME_TIMER())
				PLAY_SOUND_FRONTEND( -1, "Menu_Navigate", ARCADE_GAMES_SOUND_GET_SOUNDSET(QUB3D_AUDIO_EFFECT_SCOREBOARD_CONFIRM))
				IF option_selected < QUIT_OPTION				
					option_selected_int = ENUM_TO_INT (option_selected)
					option_selected_int++
					option_selected = INT_TO_ENUM(FRONT_END_OPTIONS, option_selected_int)	
					
					// Skip the hi score option is account isn't permitted to view user info.
					IF NOT IS_ACCOUNT_OVER_17_FOR_UGC()
					AND option_selected = HI_SCORE_OPTION
						option_selected = QUIT_OPTION
					ENDIF
					
				ELSE
					option_selected = START_OPTION
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
		OR left_stick_y <= -0.85
			current_time = TO_FLOAT(GET_GAME_TIMER())
			time_difference = current_time  - time_last_press 
			IF time_difference >= 250
				time_last_press = TO_FLOAT(GET_GAME_TIMER())
				PLAY_SOUND_FRONTEND( -1, "Menu_Navigate", ARCADE_GAMES_SOUND_GET_SOUNDSET(QUB3D_AUDIO_EFFECT_SCOREBOARD_CHANGE_LETTER))
				IF option_selected > START_OPTION
					option_selected_int = ENUM_TO_INT (option_selected)
					option_selected_int--
					option_selected = INT_TO_ENUM(FRONT_END_OPTIONS, option_selected_int)
					
					// Skip the hi score option is account isn't permitted to view user info.
					IF NOT IS_ACCOUNT_OVER_17_FOR_UGC()
					AND option_selected = HI_SCORE_OPTION
						option_selected = TUTORIAL_OPTION
					ENDIF
					
				ELSE
					option_selected = QUIT_OPTION
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DISPLAY_IN_GAME_TEXT()		
	// left hand side of screen changes based on language (for text layout issues)
	
	SCORE_OBJECTS_UPDATER()
	
	LEVEL_OBJECTS_UPDATER()

	POWER_BAR_OBJECT_UPDATER()
	
	IF !first_time_through
		PREVIEW_BLOCK_UPDATER()
	ENDIF
		
	ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_POWER", 
		INIT_VECTOR_2D(ig_power_txt_pos_x, ig_power_txt_pos_y),
		INIT_VECTOR_2D(ig_power_txt_size_x, ig_power_txt_size_y),
		0.0, rgbaWhiteOut)
		
	ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_SPECIAL", 
		INIT_VECTOR_2D(ig_special_txt_pos_x, ig_special_txt_pos_y),
		INIT_VECTOR_2D(ig_special_txt_size_x, ig_special_txt_size_y),
		0.0, rgbaWhiteOut)
		
	ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_NEXT", 
		INIT_VECTOR_2D(ig_next_txt_pos_x, ig_next_txt_pos_y),
		INIT_VECTOR_2D(ig_next_txt_size_x, ig_next_txt_size_y),
		0.0, rgbaWhiteOut)
			
ENDPROC

PROC QUIT_TUTORIAL()

	// reset falling flag
	is_a_box_in_motion 					= FALSE
	have_blocks_been_set_up_for_demo 	= FALSE
	time_started = TO_FLOAT(GET_GAME_TIMER())
	
	CLEAR_PRINTS()
	CLEAR_HELP()

	PLAY_SOUND_FRONTEND( -1, "Menu_Back", ARCADE_GAMES_SOUND_GET_SOUNDSET(QUB3D_AUDIO_EFFECT_SCOREBOARD_CHANGE_LETTER))

	INITIALISE_THE_GRID()
	is_cross_pressed = TRUE	
	is_game_over = TRUE
	is_this_tutorial = FALSE
	slow_down_powerup_in_effect = FALSE
	colour_removal_powerup_in_effect = FALSE
	eraser_powerup_in_effect = FALSE
	bomb_powerup_in_effect = FALSE
	
	// Reset sound mixes.
	SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",0.0)
	SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"Slowmo",0.0)
	
	powerDemoState = PD_SETUP_GRID
	blocks_cleared = 0
	current_tutorial_stage = TUTORIAL_INTRO

ENDPROC

PROC CHECK_FOR_PLAYER_WANTING_TO_QUIT_TUTORIAL()
	// PURPOSE: while the tutorial is in progress checks if the player wished to return to the from end
	//			via pressing the x button
	IF is_this_tutorial
	
		// draw the background
		
		// draw the on screen button			

		IF QUB3d_IS_MENU_DECLINE_BUTTON_JUST_PRESSED()
			
			QUIT_TUTORIAL()
			
		ELSE		
			is_cross_pressed = FALSE
		ENDIF	
	ENDIF
	
	IF is_game_over
		SET_CAM_ACTIVE_WITH_INTERP(anim_cam, game_cam, 4000, GRAPH_TYPE_VERY_SLOW_IN_VERY_SLOW_OUT)
		SET_CAM_ACTIVE(game_cam, FALSE)	
	ENDIF
ENDPROC

PROC CHECK_FOR_STANDARD_MINI_EXIT()	

	INT iQuitTime = 1000

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		IF IS_CONTROL_PRESSED(schtQuitGamePC.control, schtQuitGamePC.action)
		OR (IS_DISABLED_CONTROL_PRESSED(schtQuitGamePC.control, schtQuitGamePC.action))
			DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), schtQuitGamePC.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
		ELSE
			DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
		ENDIF

	ELSE
		
		IF IS_CONTROL_PRESSED(schtQuitGame.control, schtQuitGame.action)
		OR (IS_DISABLED_CONTROL_PRESSED(schtQuitGame.control, schtQuitGame.action))
			DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), schtQuitGame.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
		ELSE
			DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
		ENDIF
	
	ENDIF

	IF IS_CONTROL_HELD(schtQuitGame, DEFAULT, iQuitTime)
		CLEANUP_MISSION_PUZZLE()
	ENDIF
	
	IF IS_PC_VERSION()
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_HELD(schtQuitGamePC, DEFAULT, iQuitTime)
			CLEANUP_MISSION_PUZZLE()
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	OR SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
		CLEANUP_MISSION_PUZZLE()
	ENDIF
	
ENDPROC

////////////////////////////////////
/// ***** PARTICLE EFFECTS ***** ///
////////////////////////////////////
PROC QUB3D_TRIGGER_EFFECT(VECTOR vPos, BOARD_GRID_CONTENTS colour, STRING sEffect)

	FLOAT fRed = 0.0
	FLOAT fGreen = 0.0
	FLOAT fBlue = 0.0

	SWITCH colour
	
		CASE RED
			fRed = 255
			fGreen = 0
			fBlue = 0
		BREAK
		
		CASE BLUE
			fRed = 0
			fGreen = 0
			fBlue = 255
		BREAK
		
		CASE GREEN
			fRed = 0
			fGreen = 255
			fBlue = 0
		BREAK
		
		CASE YELLOW
			fRed = 255
			fGreen = 255
			fBlue = 0
		BREAK
		
		CASE PURPLE
			fRed = 128
			fGreen = 0
			fBlue = 128
		BREAK
				
		CASE ORANGE_CUBE
			fRed = 255
			fGreen = 165
			fBlue = 0
		BREAK
	
	ENDSWITCH

	IF HAS_NAMED_PTFX_ASSET_LOADED(qub3d_ptfx_assets)
		USE_PARTICLE_FX_ASSET(qub3d_ptfx_assets) 
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(fRed, fGreen, fBlue)
		START_PARTICLE_FX_NON_LOOPED_AT_COORD(sEffect, vPos, <<0.0, 0.0, 0.0>>) // top left
	ENDIF
		
ENDPROC

///////////////////////////////
/// ***** AUDIO HELPERS ***** ///
///////////////////////////////

FUNC FLOAT PZ_GET_AUDIO_PAN_X(INT iCurrentColumn)

	IF iCurrentColumn = 0
		RETURN -0.99 
	ENDIF

	IF iCurrentColumn = 1
		RETURN -0.66 
	ENDIF

	IF iCurrentColumn = 2
		RETURN -0.33 
	ENDIF

	IF iCurrentColumn = 3
		RETURN 0.33 
	ENDIF
	
	IF iCurrentColumn = 4
		RETURN 0.66
	ENDIF
	
	IF iCurrentColumn >= 5
		RETURN 0.99
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT PZ_GET_AUDIO_PAN_Y(INT iCurrentRow)

	IF iCurrentRow = 0
		RETURN -1.0
	ENDIF

	IF iCurrentRow = 1
		RETURN -0.8 
	ENDIF

	IF iCurrentRow = 2
		RETURN -0.6
	ENDIF

	IF iCurrentRow = 3
		RETURN -0.4
	ENDIF
	
	IF iCurrentRow = 4
		RETURN -0.2
	ENDIF
	
	IF iCurrentRow = 5
		RETURN 0.2
	ENDIF
	
	IF iCurrentRow = 6
		RETURN 0.4
	ENDIF
	
	IF iCurrentRow = 7
		RETURN 0.6
	ENDIF
	
	IF iCurrentRow = 8
		RETURN 0.8
	ENDIF
	
	IF iCurrentRow >= 9
		RETURN 1.0
	ENDIF
	
	RETURN 0.0
ENDFUNC

///////////////////////////////
/// ***** GRID UPKEEP ***** ///
///////////////////////////////

PROC CHECK_FOR_2_X_2_OF_SAME_COLOUR()
	// checks around the current block looking for the 2x2 layout required to create a 
	// bigger special block?
	//			---------
	//			I	I	I
	//			---------
	//			I 	I	I
	//			---------
	
	INT small_colour_int	
	BOOL create_large_block = FALSE

	FOR current_column = 0 TO (number_of_columns -1)
		// initialise the starting column
		
		FOR current_row = min_row TO (number_of_rows-1)
			// initialise the flags
			create_large_block = FALSE
			current_grid_contents = grid_box[current_row][current_column].this_box_colour
			
			small_colour_int = ENUM_TO_INT (current_grid_contents)
				
			// check to see if block already contains a large block and not empty
			IF (small_colour_int < max_number_of_colours)
			AND current_grid_contents != EMPTY
				// check surrounding blocks to see if in suitable pattern
				// assume we are only ever checking from bottom left corner ( S to prevent going over same stuff repeatedly)
				
				// check boundaries on grid
				IF (current_row + 1) <= max_row
				AND (current_column + 1) <= max_col
				
					// check the required boxes colour
					IF grid_box[current_row][current_column + 1].this_box_colour = current_grid_contents // bottom right
					AND grid_box[current_row + 1][(current_column)].this_box_colour = current_grid_contents // top left
					AND grid_box[(current_row + 1)][(current_column+1)].this_box_colour = current_grid_contents // top right 
						// update flag showing suitable to create large block here
						create_large_block =TRUE
					ENDIF			
				ENDIF
	
				IF create_large_block

					/// **** main block
					// remove the current boxes 
					IF IS_ENTITY_VISIBLE(grid_box[current_row][current_column].box_object)
						// add particle effect	
						SET_ENTITY_VISIBLE(grid_box[current_row][current_column].box_object, FALSE)
						
						// play audio
						soundId=GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(soundId, merging_block_sfx)
						SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(current_column))
						SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(current_row))						
						RELEASE_SOUND_ID(soundId)
						// update grid contents
						grid_box[current_row][current_column].this_box_colour  = EMPTY
					ENDIF
					
					
					// replace them with the model for the large box
					VECTOR vPos = grid_box[current_row][(current_column)].box_object_pos
					
					// Offset into position
					vPos.x += 0.0125
					vPos.y += 0.0125
					
					QUB3d_CREATE_OBJECT(grid_box[current_row][current_column].box_object,large_box_model, vPos)

					IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
						SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, bottom_left_heading)
						SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
						FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
						grid_box[current_row][current_column].this_box_colour = current_grid_contents
						QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, current_grid_contents)
						QUB3D_TRIGGER_EFFECT(grid_box[current_row][current_column].box_object_pos, grid_box[current_row][current_column].this_box_colour, qub3d_ptfx_merge)
						grid_box[current_row][current_column].is_large_box = TRUE
						
						// play audio
						soundId = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(soundId, merging_block_sfx)
						SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(current_column))
						SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(current_row))						
						RELEASE_SOUND_ID(soundId)
						
					ENDIF
										
					// *** to the right
					IF (current_column + 1 <= max_col)
						// remove the current boxes 
						IF DOES_ENTITY_EXIST(grid_box[current_row][current_column + 1].box_object)
							QUB3D_TRIGGER_EFFECT(grid_box[current_row][current_column+ 1].box_object_pos, current_grid_contents, qub3d_ptfx_merge)
							DELETE_OBJECT(grid_box[current_row][current_column + 1].box_object)
							
							// update grid contents
							grid_box[(current_row)][current_column + 1].this_box_colour  = EMPTY
						ENDIF
											
					ENDIF
					
					// *** bottom left
					IF (current_row + 1 <= max_row)
						
						// remove the current boxes 
						IF DOES_ENTITY_EXIST(grid_box[current_row + 1][(current_column)].box_object)
							QUB3D_TRIGGER_EFFECT(grid_box[current_row+ 1][current_column].box_object_pos, current_grid_contents, qub3d_ptfx_merge)
							DELETE_OBJECT(grid_box[current_row + 1][(current_column)].box_object)
							
							// update grid contents
							grid_box[current_row + 1][(current_column)].this_box_colour  = EMPTY
						ENDIF
						
					ENDIF
						
					// *** bottom right
					IF (current_row + 1 <= max_row)
					AND (current_column + 1 <= max_col)
						// remove the current boxes 
						IF DOES_ENTITY_EXIST(grid_box[(current_row + 1)][(current_column + 1)].box_object)	
							QUB3D_TRIGGER_EFFECT(grid_box[(current_row + 1)][(current_column+ 1)].box_object_pos, current_grid_contents, qub3d_ptfx_merge)
							DELETE_OBJECT(grid_box[(current_row + 1)][(current_column + 1)].box_object)
																					
							// update grid contents
							grid_box[(current_row + 1)][(current_column + 1)].this_box_colour  = EMPTY
						ENDIF
						
					ENDIF
					
				ENDIF		
			ENDIF
		
		ENDFOR
		
	ENDFOR
ENDPROC 

PROC MARK_LARGE_BLOCK_FOR_DELETION(INT this_row, INT this_col)
	
	// PURPOSE:  determines which part of the large square is being looked at (bottom/top  left/right )
	// 			 and marks the contecting ones of this square to be deleted
	
	MODEL_NAMES this_box_model
	FLOAT this_box_heading
	
	// if box exists in game world
	IF DOES_ENTITY_EXIST(grid_box[this_row][this_col].box_object)
		// find its model
		this_box_model = GET_ENTITY_MODEL(grid_box[this_row][this_col].box_object)
		UNUSED_PARAMETER(this_box_model)
		// find its rotation
		this_box_heading = GET_ENTITY_HEADING(grid_box[this_row][this_col].box_object)
		
		/// check for ranges as chance heading is not accurate to the consts for box headings
		IF this_box_heading <  (top_left_heading + 45.0) /// 45.0 mid point between the two headings
			this_box_heading = top_left_heading
		ELSE
			IF this_box_heading < (bottom_left_heading + 45.0)
				this_box_heading = bottom_left_heading
			ELSE
				IF this_box_heading < (bottom_right_heading + 45)
					this_box_heading = bottom_right_heading
				ELSE
					this_box_heading = 	top_right_heading
				ENDIF
			ENDIF
		ENDIF
			
		// mark one being looked at to be deleted
		block_status[this_row][this_col] = DELETE
		
		// depending on its model decide which surrounding blocks should contain the rest of the squares
		IF this_box_heading = bottom_left_heading
			// mark bottom right, top left/right
			IF (this_row - 1 <= min_row)
			AND (this_col + 1 <= max_col)
				block_status[this_row - 1][this_col] = DELETE
				block_status[this_row][this_col + 1] = DELETE
				block_status[this_row - 1][this_col + 1] = DELETE
			ENDIF
		ELSE 
			IF this_box_heading = bottom_right_heading
				IF (this_row - 1 >= min_row)
				AND (this_col - 1 >= min_col)
					block_status[this_row][this_col - 1] = DELETE
					block_status[this_row - 1][this_col - 1] = DELETE
					block_status[this_row - 1][this_col] = DELETE
				ENDIF
			ELSE
				IF this_box_heading = top_left_heading
					IF (this_row + 1 <= max_row)
					AND (this_col + 1 <= max_col)
						block_status[this_row + 1][this_col] = DELETE
						block_status[this_row + 1][this_col + 1] = DELETE
						block_status[this_row][this_col + 1] = DELETE
					ENDIF
				ELSE
					// ASSUME :this_box_model = large_box_model[small_colour_int][TOP_RIGHT]
					// mark bottom left/right, top left
					IF (this_row + 1 <= max_row)
					AND (this_col - 1 >= min_col)
						block_status[this_row - 1][this_col - 1] = DELETE
						block_status[this_row - 1][this_col] = DELETE
						block_status[this_row][this_col - 1] = DELETE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	/// add to the power bar?
	number_of_powerup_objs_to_display++
	
ENDPROC

FUNC INT CHECK_CONTENTS_OF_ADJACENT_SQUARES()
	INT number_of_same_colour = 0 // as we dont include starting block

	IF current_grid_contents = EMPTY
		RETURN 0
	ENDIF

	/// left
	IF (current_column - 1) >= min_col
		IF grid_box[current_row][(current_column - 1)].this_box_colour = current_grid_contents//grid_contents[current_row][(current_column - 1)] =  current_grid_contents
			number_of_same_colour++
			// go to left of this one
			IF (current_column - 2) >= min_col
				IF grid_box[current_row][(current_column - 2)].this_box_colour =  current_grid_contents
					number_of_same_colour++	
				ENDIF
			ENDIF
			
			// up from this one
			IF (current_row - 1) >= min_row
				IF grid_box[current_row - 1][(current_column - 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			
			// down from this one
			IF (current_row + 1) <= max_row
				IF grid_box[current_row + 1][(current_column - 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	/// right
	IF (current_column + 1) <= max_col
		IF grid_box[current_row][(current_column + 1)].this_box_colour =  current_grid_contents
			number_of_same_colour++
			IF (current_column + 2) <= max_col
				IF grid_box[current_row][(current_column + 2)].this_box_colour =  current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			
			// up from this one
			IF (current_row - 1) >= min_row
				IF grid_box[current_row - 1][(current_column + 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			
			// down from this one
			IF (current_row + 1) <= max_row
				IF grid_box[current_row + 1][(current_column + 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
						
		ENDIF
	ENDIF
	
	/// down
	IF (current_row + 1) <= max_row
		IF grid_box[(current_row + 1)][current_column].this_box_colour =  current_grid_contents
			number_of_same_colour++
			IF (current_row + 2) <= max_row
				IF grid_box[(current_row + 2)][current_column].this_box_colour =  current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			
			// left from this one
			IF (current_column - 1) >= min_col
				IF grid_box[current_row + 1][(current_column - 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			// right from this one
			IF (current_column + 1) <= max_col
				IF grid_box[current_row + 1][(current_column + 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
						
		ENDIF
	ENDIF
	
	/// up
	IF (current_row - 1) >= min_row
		IF grid_box[(current_row - 1)][current_column].this_box_colour =  current_grid_contents
			number_of_same_colour++
			IF (current_row - 2) >= min_row
				IF grid_box[(current_row - 2)][current_column].this_box_colour =  current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			// left from this one
			IF (current_column - 1) >= min_col
				IF grid_box[(current_row - 1)][(current_column - 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
			// right from this one
			IF (current_column + 1) <= max_col
				IF grid_box[(current_row - 1)][(current_column + 1)].this_box_colour = current_grid_contents
					number_of_same_colour++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "CHECK_CONTENTS_OF_ADJACENT_SQUARES - Number of same colour returned: ", number_of_same_colour)
	RETURN number_of_same_colour
ENDFUNC

FUNC INT MARK_BOXES_FOR_DELETION()
	/// need to scan through the adjacent blocks again but this time instead of incrementing the number found
	/// counter we mark the block to be deleted
	
	INT number_to_be_deleted = 0
	
	// current box
	block_status[(current_row)][current_column] = DELETE

	/// up
	IF (current_row - 1) >= min_row
		IF grid_box[(current_row - 1)][current_column].this_box_colour = current_grid_contents//grid_contents[(current_row - 1)][current_column] =  current_grid_contents
			block_status[(current_row - 1)][current_column] = DELETE
			number_to_be_deleted++
			
			// go to up of this one
			IF (current_row - 2) >= min_row
				IF grid_box[(current_row - 2)][current_column].this_box_colour =  current_grid_contents
					block_status[(current_row - 2)][current_column] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			// right from this one
			IF (current_column + 1) <= max_col
				IF grid_box[(current_row - 1)][(current_column + 1)].this_box_colour = current_grid_contents
					block_status[(current_row - 1)][(current_column + 1)] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			// left from this one
			IF current_column - 1 >= min_col
				IF grid_box[(current_row - 1)][current_column-1].this_box_colour = current_grid_contents
					block_status[(current_row - 1)][current_column-1] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF

	/// down
	IF (current_row + 1) <= max_row
		IF grid_box[(current_row + 1)][current_column].this_box_colour =  current_grid_contents
			block_status[(current_row + 1)][current_column] = DELETE
			number_to_be_deleted++
			
			// Down 1 from this one
			IF (current_row + 2) <= max_row
				IF grid_box[(current_row + 2)][current_column].this_box_colour =  current_grid_contents
					block_status[(current_row + 2)][current_column] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			
			// right from this one
			IF (current_column + 1) <= max_col
				IF grid_box[(current_row + 1)][(current_column + 1)].this_box_colour = current_grid_contents
					block_status[(current_row + 1)][(current_column + 1)] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			// left from this one
			IF current_column - 1 >= min_col
				IF grid_box[(current_row + 1)][current_column-1].this_box_colour = current_grid_contents
					block_status[(current_row + 1)][current_column-1] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	/// right
	IF (current_column + 1) <= max_col
		IF grid_box[current_row][(current_column + 1)].this_box_colour =  current_grid_contents
			block_status[current_row][(current_column + 1)] = DELETE
			number_to_be_deleted++
				
			// Right from this one
			IF (current_column + 2) <= max_col
				IF grid_box[current_row][(current_column + 2)].this_box_colour =  current_grid_contents
					block_status[current_row][(current_column + 2)] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			// Up from this one
			IF (current_row - 1) >= min_row
				IF grid_box[(current_row - 1)][(current_column + 1)].this_box_colour = current_grid_contents
					block_status[(current_row - 1 )][(current_column + 1)] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			// Down from this one
			IF (current_row + 1) <= max_row
				IF grid_box[(current_row + 1)][(current_column + 1)].this_box_colour = current_grid_contents
					block_status[(current_row + 1 )][(current_column + 1)] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	/// left
	IF current_column - 1 >= min_col
		IF grid_box[current_row][current_column - 1].this_box_colour =  current_grid_contents
			block_status[current_row][current_column - 1] = DELETE
			number_to_be_deleted++
			
			IF (current_column - 2) >= min_col
				IF grid_box[current_row][(current_column - 2)].this_box_colour =  current_grid_contents					
					block_status[current_row][(current_column - 2)] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF

			// left from this one
			IF (current_row - 1) >= min_row
				IF grid_box[(current_row - 1)][current_column - 1].this_box_colour = current_grid_contents
					block_status[(current_row - 1 )][current_column - 1] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF
			
			// right from this one
			IF (current_row + 1) <= max_row
				IF grid_box[(current_row + 1)][current_column - 1].this_box_colour = current_grid_contents
					block_status[(current_row + 1 )][current_column - 1] = DELETE
					number_to_be_deleted++
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	RETURN number_to_be_deleted
ENDFUNC

FUNC BOOL DELETE_MARK_GRID_BOXES()

	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "DELETE_MARK_GRID_BOXES - Running this frame. Current Row: ", current_row)
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "DELETE_MARK_GRID_BOXES - Running this frame. Current Column: ", current_column)

	IF current_row <= max_row
		IF (current_column <=  max_col)
		
			WHILE block_status[current_row][current_column] != DELETE
				current_column++
				
				IF current_column >= number_of_columns
					current_row++
					current_column = 0
				ENDIF
				
				IF current_row >= number_of_rows
					current_row = min_row
					current_column = 0
					RETURN TRUE
				ENDIF
			ENDWHILE
		
			// if block has been marked for deletion
		
			// if it exists in game world
			IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)	
				
				IF TIMERA() <= 100
					// time since block last moved
					RETURN FALSE
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "DELETE_MARK_GRID_BOXES - Running this frame. RETURNING FALSE")
				ELSE
					SETTIMERA(0)
				ENDIF

				// remove the block	
				QUB3D_TRIGGER_EFFECT(grid_box[current_row][current_column].box_object_pos,grid_box[current_row][current_column].this_box_colour, qub3d_ptfx_explosion)
				// play audio
				blocks_cleared++
				
				soundId=GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(soundId, exploding_block_sfx)
				SET_VARIABLE_ON_SOUND(soundId, audioNumBlocksCleared, blocks_cleared)
				SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(current_column))
				SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(current_row))						
				RELEASE_SOUND_ID(soundId)
				
				DELETE_OBJECT(grid_box[current_row][current_column].box_object)
				
			ENDIF
			
			// update grid contents
			grid_box[current_row][current_column].this_box_colour  = EMPTY
			grid_box[current_row][current_column].is_box_on_grid = FALSE
			
			// update grid status
			block_status[current_row][current_column] = REMAINS
		ENDIF
		
		current_column++
	ENDIF
		
	IF current_column >= number_of_columns
		current_row++
		current_column = 0
	ENDIF
	
	IF current_row >= number_of_rows
		current_row = min_row
		current_column = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
		
ENDFUNC

FUNC BOOL REMOVE_EMPTY_SPACES()
/// PURPOSE: scans through the grid and fills up any empty spaces with a block from above
// repeat until no more blocks move
// if current positions is not empty, scan directly below and if empty move the block down one row,
// and flag that a block has been removed

	BOOL have_any_boxes_been_moved = TRUE
	BOOL bEndedLoop = FALSE
	
	BOOL can_shift_large_block
	MODEL_NAMES current_box_model
	FLOAT this_box_heading
	INT time_started_local
	
	int current__col = 0
	int current__row = 0
		
	// keep moving until all blocks have been shifted
	WHILE (have_any_boxes_been_moved)
		// initialise the exit flag so that it will exit loop if none are moved
		have_any_boxes_been_moved = FALSE
		
		// Initialising to 1 as we can't assess bottom.
		FOR current__row = 0 TO max_row
			// initialise the starting column

			FOR current__col = 0 TO max_col
				// +++++++ issue is a large block is moved down as its outside the model range 
				// if block contains a small colour 
				IF (grid_box[current__row][current__col].this_box_colour != EMPTY)
				
					/// check if there is an empty space in row directly below
					IF (current__row + 1 <= max_row)
					AND (grid_box[current__row+1][current__col].this_box_colour = EMPTY)
						// if box exists in game world
						IF DOES_ENTITY_EXIST(grid_box[current__row][current__col].box_object)
							// delete the current block
							DELETE_OBJECT(grid_box[current__row][current__col].box_object)
						ENDIF
						
						// delete any posibility of having a box in this slot
						IF DOES_ENTITY_EXIST(grid_box[(current__row+1)][current__col].box_object)
							// delete the block one row down.
							DELETE_OBJECT(grid_box[(current__row+1)][current__col].box_object)
						ENDIF
						// create a copy of this box in the row below
						QUB3d_CREATE_OBJECT(grid_box[current__row+1][current__col].box_object, box_model, 
							grid_box[current__row+1][current__col].box_object_pos)
							
						grid_box[current__row+1][current__col].this_box_colour = grid_box[current__row][current__col].this_box_colour
						grid_box[current__row+1][current__col].is_box_on_grid = TRUE
						
						IF DOES_ENTITY_EXIST(grid_box[current__row+1][current__col].box_object)
							SET_ENTITY_HEADING(grid_box[current__row+1][current__col].box_object, 0.0)
							SET_ENTITY_COLLISION(grid_box[current__row+1][current__col].box_object, FALSE)
							FREEZE_ENTITY_POSITION(grid_box[current__row+1][current__col].box_object, TRUE)
							QUB3D_SET_CUBE_COLOR(grid_box[current__row+1][current__col].box_object, grid_box[current__row][current__col].this_box_colour)
						ENDIF

						// update grid contents for currently looked at grid space
						grid_box[current__row][current__col].this_box_colour  = EMPTY
						grid_box[current__row][current__col].is_box_on_grid = FALSE
						
						// update the moved flag
						have_any_boxes_been_moved = TRUE
					
					ENDIF				
				ELSE
					IF have_large_boxes_in_play
						// if block not empty must contain a large colour
						IF (grid_box[current__row][current__col].this_box_colour != EMPTY)
							/// need some rule for moving down large blocks
							can_shift_large_block = FALSE
							
							/// check if there is an empty space in rows directly below
							IF (current__row + 1) <= max_row
								IF (grid_box[current__row+1][current__col].this_box_colour = EMPTY)
									
									/// determine if its bottom left or bottom right
									IF DOES_ENTITY_EXIST(grid_box[current__row][current__col].box_object)
										this_box_heading = GET_ENTITY_HEADING (grid_box[current__row][current__col].box_object)
									ENDIF
										
									/// check for ranges as chance heading is not accurate to the consts for box headings
									IF this_box_heading <  (top_left_heading + 45.0) /// 45.0 mid point between the two headings
										this_box_heading = top_left_heading
									ELSE
										IF this_box_heading < (bottom_left_heading + 45.0)
											this_box_heading = bottom_left_heading
										ELSE
											IF this_box_heading < (bottom_right_heading + 45.0)
												this_box_heading = bottom_right_heading
											ELSE
												this_box_heading = 	top_right_heading
											ENDIF
										ENDIF
									ENDIF
									
									
									IF this_box_heading = bottom_left_heading
										/// as bottom left need to check one column along to see if its emtpy below as well
										IF (current__row + 1) <= max_row
										AND (current__row - 1) >= min_row
										AND (current__col + 1) <= max_col
										AND (current__col - 1) >= min_col
											IF (grid_box[(current__row + 1)][(current__col + 1)].this_box_colour = EMPTY)
												CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "REMOVE_EMPTY_SPACES - Colours around this space are the same check for moving down the large box.")
												can_shift_large_block = TRUE	
											ENDIF
										ENDIF
									
									ENDIF		
								ENDIF
							ENDIF
							
							IF can_shift_large_block

								// ******* BOTTOM LEFT ******** //
								
								// if box exists in game world
								IF DOES_ENTITY_EXIST(grid_box[current__row][current__col].box_object)
									// find its model
									current_box_model = GET_ENTITY_MODEL (grid_box[current__row][current__col].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[current__row][current__col].box_object)
								ENDIF
								
								// delete any posibility of having a box in this slot
								IF DOES_ENTITY_EXIST(grid_box[current__row+1][current__col].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[current__row+1][current__col].box_object)
								ENDIF
								// create a copy of this box in the row below
								QUB3D_CREATE_OBJECT(grid_box[current__row+1][current__col].box_object, current_box_model,
									grid_box[current__row+1][current__col].box_object_pos)
								
								/// update flags in the struct for newly created block
								grid_box[current__row+1][current__col].is_box_on_grid = TRUE
								grid_box[current__row+1][current__col].this_box_colour = grid_box[current__row][current__col].this_box_colour
								
								IF DOES_ENTITY_EXIST(grid_box[current__row+1][current__col].box_object)
									SET_ENTITY_HEADING(grid_box[current__row+1][current__col].box_object, bottom_left_heading)
									SET_ENTITY_COLLISION(grid_box[current__row+1][current__col].box_object, FALSE)
									FREEZE_ENTITY_POSITION(grid_box[current__row+1][current__col].box_object, TRUE)
									QUB3D_SET_CUBE_COLOR(grid_box[current__row+1][current__col].box_object, grid_box[current__row+1][current__col].this_box_colour)
								ENDIF

								// update grid contents for currently looked at grid space
								grid_box[current__row][current__col].this_box_colour  = EMPTY
								grid_box[current__row][current__col].is_box_on_grid = FALSE
																
								// ******* BOTTOM RIGHT ***** //
								
								// if box exists in game world
								IF DOES_ENTITY_EXIST(grid_box[current__row + 1][current__col].box_object)
									// find its model
									current_box_model = GET_ENTITY_MODEL (grid_box[current__row + 1][current__col].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[current__row + 1][current__col].box_object)
								ENDIF
								
								// delete any posibility of having a box in this slot
								IF DOES_ENTITY_EXIST(grid_box[current__row + 1][current__col + 1].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[(current__row + 1)][(current__col + 1)].box_object)
								ENDIF
								// create a copy of this box in the column below
								QUB3D_CREATE_OBJECT(grid_box[(current__row + 1)][(current__col + 1)].box_object, current_box_model,
									grid_box[current__row + 1][current__col + 1].box_object_pos)
												
								/// update flags in the struct for newly created block
								IF grid_box[current__row + 1][current__col + 1].this_box_colour != EMPTY
									grid_box[(current__row + 1)][current__col + 1].this_box_colour = grid_box[current__row + 1][current__col].this_box_colour
									grid_box[(current__row + 1)][current__col + 1].is_box_on_grid = TRUE
								ENDIF
									
								IF DOES_ENTITY_EXIST(grid_box[(current__row + 1)][(current__col+1)].box_object)
									SET_ENTITY_HEADING(grid_box[(current__row + 1)][(current__col+1)].box_object, bottom_right_heading)
									SET_ENTITY_COLLISION(grid_box[(current__row + 1)][(current__col+1)].box_object, FALSE)
									FREEZE_ENTITY_POSITION(grid_box[(current__row + 1)][(current__col+1)].box_object, TRUE)
									QUB3D_SET_CUBE_COLOR(grid_box[(current__row + 1)][(current__col+1)].box_object, grid_box[(current__row + 1)][(current__col+1)].this_box_colour)
								ENDIF

								// update grid contents for currently looked at grid space
								grid_box[current__row + 1][current__col].this_box_colour  = EMPTY
								grid_box[current__row + 1][current__col].is_box_on_grid = FALSE
								
								
								// ******* TOP LEFT ******** //
								
								// if box exists in game world
								IF DOES_ENTITY_EXIST(grid_box[current__row - 1][current__col].box_object)
									// find its model
									current_box_model = GET_ENTITY_MODEL (grid_box[current__row - 1][current__col].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[current__row - 1][current__col].box_object)
								ENDIF
								
								// delete any posibility of having a box in this slot
								IF DOES_ENTITY_EXIST(grid_box[current__row][(current__col)].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[current__row][(current__col)].box_object)
								ENDIF
								// create a copy of this box in the row below
								grid_box[current__row][current__col].this_box_colour = grid_box[current__row+1][current__col].this_box_colour
								QUB3D_CREATE_OBJECT(grid_box[current__row][current__col].box_object, current_box_model,
									grid_box[current__row][(current__col)].box_object_pos)
								
								IF DOES_ENTITY_EXIST(grid_box[current__row][current__col].box_object)
									SET_ENTITY_COLLISION(grid_box[current__row][(current__col)].box_object, FALSE)
									FREEZE_ENTITY_POSITION(grid_box[current__row][(current__col)].box_object, TRUE)
									QUB3D_SET_CUBE_COLOR(grid_box[current__row][current__col].box_object, grid_box[current__row][current__col].this_box_colour)
								ENDIF
											
								/// update flags in the struct for newly created block
								grid_box[current__row][current__col].this_box_colour = grid_box[current__row+1][(current__col)].this_box_colour
								grid_box[current__row][current__col].is_box_on_grid = TRUE
								// update grid contents for currently looked at grid space
								grid_box[current__row - 1][current__col].this_box_colour  = EMPTY
								grid_box[current__row - 1][current__col].is_box_on_grid = FALSE
								
								
								// ******* TOP RIGHT ******** //
								
								// if box exists in game world
								IF DOES_ENTITY_EXIST(grid_box[(current__row - 1)][(current__col + 1)].box_object)
									// find its model
									current_box_model = GET_ENTITY_MODEL (grid_box[(current__row - 1)][(current__col + 1)].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[(current__row - 1)][(current__col + 1)].box_object)
								ENDIF
								
								// delete any posibility of having a box in this slot
								IF DOES_ENTITY_EXIST(grid_box[current__row][(current__col + 1)].box_object)
									// move the block down
									DELETE_OBJECT(grid_box[current__row][(current__col + 1)].box_object)
								ENDIF
								// create a copy of this box in the column below
								QUB3D_CREATE_OBJECT(grid_box[current__row][(current__col + 1)].box_object, current_box_model,
									grid_box[(current__row)][(current__col + 1)].box_object_pos)
								
								/// update flags in the struct for newly created block
								grid_box[current__row][current__col + 1].this_box_colour = grid_box[(current__row - 1)][(current__col + 1)].this_box_colour
								grid_box[current__row][current__col + 1].is_box_on_grid = TRUE
								
								IF DOES_ENTITY_EXIST(grid_box[current__row][(current__col)].box_object)
									SET_ENTITY_HEADING(grid_box[current__row][(current__col)].box_object, top_right_heading)
									SET_ENTITY_COLLISION(grid_box[current__row][(current__col)].box_object, FALSE)
									FREEZE_ENTITY_POSITION(grid_box[current__row][(current__col)].box_object, TRUE)
									QUB3D_SET_CUBE_COLOR(grid_box[current__row][(current__col)].box_object, grid_box[current__row][current__col + 1].this_box_colour)
								ENDIF

								// update grid contents for currently looked at grid space
								grid_box[(current__row - 1)][(current__col + 1)].this_box_colour  = EMPTY
								grid_box[(current__row - 1)][(current__col + 1)].is_box_on_grid = FALSE
								
								// update the moved flag
								have_any_boxes_been_moved = TRUE
								
							ENDIF
							
						ENDIF
					ENDIF
					
				ENDIF
				// increase column counter				
			ENDFOR

			IF NOT have_any_boxes_been_moved
				IF current__col >= max_col
				AND current__row >= max_row
					bEndedLoop = TRUE
				ENDIF
			ENDIF

		ENDFOR
		
		IF have_any_boxes_been_moved
			// have a wait here?			
			time_difference += (0+@1000)
			WHILE (time_difference < 160)
				//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 4256")
				current_time = TO_FLOAT(GET_GAME_TIMER())
				// time since block last moved
				time_difference = current_time - time_started_local

			ENDWHILE
		ENDIF
		
	ENDWHILE

	IF bEndedLoop
	
		// Final game over conditions
		IF is_in_danger_zone
		
			IF grid_box[min_row][2].this_box_colour != EMPTY
			OR grid_box[min_row][3].this_box_colour != EMPTY 
				is_game_over = true
			ENDIF
		
			is_in_danger_zone = FALSE
		
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_FOR_DELETION()

	/// PURPOSE :  	scans through the grid and for each space checks the adjacent grid spaces (2 less than number required in a row for deletion 
	/// 			up down left right) looking for a run or blocks the same colour. If sufficient blocks are found it flags them for deletion
	
	SWITCH(currentDeletionPhase)
	
		CASE SAME_COLOUR
			// check of 2 x 2 square of same colour here?	
			IF have_large_boxes_in_play
				CHECK_FOR_2_X_2_OF_SAME_COLOUR()
			ENDIF
		
			number_of_same_colour_found = 0
			current_column = 0

			currentDeletionPhase = LARGE_BOX_CHECK
		BREAK
				
		CASE LARGE_BOX_CHECK
			current_row = min_row
			
			// check we have assessed all columns
			IF current_column >= number_of_columns
				current_row = min_row
				current_column = 0
				time_difference = TO_FLOAT(GET_GAME_TIMER())
				SETTIMERA(0)
				
				IF is_this_tutorial
					currentDeletionPhase = BOX_DELETE_TUTORIAL
				ELSE
					currentDeletionPhase = LARGE_BOX_DELETION
				ENDIF
				
				BREAK
			ENDIF
				
			
			// initialise the starting column	
			FOR current_row = min_row TO max_row
					
				// set the initial values in the grid									
				current_grid_contents = grid_box[current_row][current_column].this_box_colour//grid_contents[current_row][current_column]
				// if the space is not empty 
				//IF NOT (grid_box[current_row][current_column].this_box_colour = EMPTY)
				IF (!grid_box[current_row][current_column].is_large_box) // to avoid deleting the large blocks
					// find if enough of same colour to warrent deletion by checking adjacent squares to this one
					number_of_same_colour_found = CHECK_CONTENTS_OF_ADJACENT_SQUARES()
					IF number_of_same_colour_found >= number_required_for_deletion
						// mark these boxes for deletion
						number_flagged_for_deletion += MARK_BOXES_FOR_DELETION()									
					ENDIF
				ELSE								
					IF (block_status[current_row][current_column] != DELETE)
						/// and not already marked for deletion
						MARK_LARGE_BLOCK_FOR_DELETION(current_row, current_column)
						grid_box[current_row][current_column].is_large_box = FALSE
						number_flagged_for_deletion++
					ENDIF
				ENDIF
				
				// increase column counter
				IF current_row >= max_row
					current_column++
				ENDIF
				
			ENDFOR
				
		BREAK
		
		CASE BOX_DELETE_TUTORIAL
		
			/// temp stuff  remove this later ///
			INT delete_flashes_counter
			INT number_of_flashes_required
			
			// if tutorial is running have it drawn out longer and blocks flash to show	
			IF NOT is_this_tutorial
				number_of_flashes_required = 3
			ELSE
				number_of_flashes_required = 5
			ENDIF
			
			IF delete_flashes_counter < number_of_flashes_required
				CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "DELETE_MARK_GRID_BOXES - Timer loop")
				
				IF current_row <= max_row
					IF current_column <=  max_col
						// if block has been marked for deletion
						IF block_status[current_row][current_column] = DELETE
							// if it exists in game world
							IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
								IF TIMERA() >= 300
									//if tutorial is running have blocks flash to show which are to be deleted
									IF NOT IS_ENTITY_VISIBLE(grid_box[current_row][current_column].box_object)
										SET_ENTITY_VISIBLE (grid_box[current_row][current_column].box_object, TRUE)
									ELSE
										SET_ENTITY_VISIBLE (grid_box[current_row][current_column].box_object, FALSE)
									ENDIF
									SETTIMERA(0)
								ELSE
									BREAK
								ENDIF
							ENDIF
						ENDIF
						// increase column counter
						current_column++
						
						IF current_column >= number_of_columns
							// increase row counter
							current_row++
						ENDIF
						
					ENDIF
				ENDIF

			ENDIF
			
			// Reset variables and continue!
			current_row = min_row
			current_column = 0
			SETTIMERA(0)
			currentDeletionPhase = LARGE_BOX_DELETION
					
		BREAK
		
		CASE LARGE_BOX_DELETION
			/// if any boxes have been marked to be deleted
			IF number_flagged_for_deletion > 0
				
				// delete the required boxes
				IF NOT DELETE_MARK_GRID_BOXES()
					BREAK
				ENDIF
				
				// increase score muliplyer
				score_multiplyer++
				
				// reset the score for this round
				current_score = number_flagged_for_deletion
				// add possible multiplyers
				current_score *= score_per_block
				current_score *= score_multiplyer
				// add score for this round
				total_score += 	current_score
				
				number_flagged_for_deletion	= 0
				
				/// check if level should increas
				IF current_level < max_level_reachable
					IF total_score >= score_required_to_level[(current_level-1)]			
						current_level++
					ENDIF
				ENDIF
				
				REMOVE_EMPTY_SPACES()
				current_column = 0
				current_row = 0
				
				// deleted something so run again
				currentDeletionPhase = SAME_COLOUR
				BREAK
			ENDIF
			
			// sort the grid to remove any spaces
			IF REMOVE_EMPTY_SPACES()
				// did not delete anything so assume grid spaces sorted
				score_multiplyer = 0
				number_flagged_for_deletion	= 0
				currentDeletionPhase = FINISHED_DELETION
			ENDIF
			
		BREAK
		
		CASE FINISHED_DELETION // Reset any local variables here.
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "CHECK_FOR_DELETION - FINISHED_DELETION")
			
			currentDeletionPhase = SAME_COLOUR
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

/////////////////////////////////////
////// **** POWER UP STUFF **** /////
/////////////////////////////////////
PROC SLOW_DOWN_POWER_UP_ACTION()
	// PURPOSE: slows down the time it take the blocks to fall for a set period of time
	
	// determine if power up has finished
	time_power_up_active += (0+@1000)
	IF time_power_up_active >= time_for_slow_down_power
		time_power_up_active = 0
		slow_down_powerup_in_effect = FALSE
		SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"Slowmo",0.0)
	ELSE		
		// divide the time by 2 to slow down block falling
		time_difference = current_time - time_started // time since block last moved
		time_difference /= 2
	ENDIF
	
ENDPROC

PROC ERASER_POWER_UP_ACTION()
	// PURPOSE:  removes the bottom two lines from the game grid
	/// scan through the grid and mark all blocks that have this colour as to be deleted

	FLOAT this_box_heading
	
	IF NOT eraser_power_selected
		FOR current_column = 0 TO max_col
			// initialise the starting column
			
			FOR current_row = (max_row-3) TO max_row
				// if not empty and not large block			
				IF grid_box[current_row][current_column].this_box_colour != EMPTY			
					// mark this block for deletion
					block_status[current_row][current_column] = DELETE
					number_flagged_for_deletion++
				ENDIF
				
				// check if its a big block 
				IF grid_box[current_row][current_column].is_large_box
					// can only delete big block if its top left or top right of it to ensure whole of big block is removed
				
					IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
						this_box_heading = GET_ENTITY_HEADING(grid_box[current_row][current_column].box_object)
					ENDIF
							
					/// check for ranges as chance heading is not accurate to the consts for box headings
					IF this_box_heading <  (top_left_heading + 45.0) /// 45.0 mid point between the two headings
						this_box_heading = top_left_heading
					ELSE
						IF this_box_heading < (bottom_left_heading + 45.0)
							this_box_heading = bottom_left_heading
						ELSE
							IF this_box_heading < (bottom_right_heading + 45)
								this_box_heading = bottom_right_heading
							ELSE
								this_box_heading = 	top_right_heading
							ENDIF
						ENDIF
					ENDIF

				ENDIF
							
			ENDFOR
			
		ENDFOR
		
		eraser_power_selected = TRUE
	ENDIF
	
	IF eraser_power_selected
	AND CHECK_FOR_DELETION()
		eraser_powerup_in_effect = FALSE
		eraser_power_selected = FALSE
	ENDIF
	
ENDPROC

PROC BOMB_POWER_UP_ACTION()
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "BOMB_POWER_UP_ACTION - Start the bomb power up.")

	// PURPOSE: removes a 2 x 3 area from the middle of the game grid
	FLOAT this_box_heading
	
	INT starting_col_b	= min_col + 2//1
	INT end_col			= min_col + 3//5
	INT starting_row_b  	= min_row
	INT end_row			= max_row
		
	INT current__column
	INT current__row
		
	IF starting_row_b < min_row
		starting_row_b = min_row
	ENDIF
	
	IF starting_row_b < min_col
		starting_row_b = min_col
	ENDIF
		
	IF NOT bomb_power_selected
		FOR current__row = starting_row_b TO end_row

			FOR current__column = starting_col_b TO end_col

				// if not empty and not large block			
				
				// mark this block for deletion
				block_status[current__row][current__column] = DELETE
				number_flagged_for_deletion++
				
				IF grid_box[current__row][current__column].is_large_box			

					// can only delete big block if its top left or top right of it to ensure whole of big block is removed
					IF DOES_ENTITY_EXIST(grid_box[current__row][current__column].box_object)
						this_box_heading = GET_ENTITY_HEADING (grid_box[current__row][current__column].box_object)
					ENDIF
							
					/// check for ranges as chance heading is not accurate to the consts for box headings
					IF this_box_heading <  (top_left_heading + 45.0) /// 45.0 mid point between the two headings
						this_box_heading = top_left_heading
					ELSE
						IF this_box_heading < (bottom_left_heading + 45.0)
							this_box_heading = bottom_left_heading
						ELSE
							IF this_box_heading < (bottom_right_heading + 45)
								this_box_heading = bottom_right_heading
							ELSE
								this_box_heading = 	top_right_heading
							ENDIF
						ENDIF
					ENDIF
					
					/// if any part of the larger block is included delete the whole thing?
					IF this_box_heading = bottom_left_heading
						// mark bottom right, top left/right
						IF current__row + 1 <= max_row
						AND current__column + 1 <= max_col
							block_status[current__row][current__column + 1] = DELETE
							block_status[current__row + 1][current__column] = DELETE
							block_status[current__row + 1][current__column + 1] = DELETE
							number_flagged_for_deletion += 3
						ENDIF
					ELSE 
						IF this_box_heading = bottom_right_heading	
							// mark bottom left, top left/right				
							IF current__row - 1 >= min_row
							AND current__column + 1 <= max_col
								block_status[current__row - 1][current__column] = DELETE
								block_status[current__row - 1][current__column + 1] = DELETE
								block_status[current__row][current__column + 1] = DELETE
								number_flagged_for_deletion += 3
							ENDIF
						ELSE
							IF this_box_heading = top_left_heading
								// mark bottom left/right, top right
								IF current__row + 1 <= max_row
								AND current__column - 1 >= min_col
									block_status[current__row][current__column - 1] = DELETE
									block_status[current__row + 1][current__column - 1] = DELETE
									block_status[current__row + 1][current__column] = DELETE
									number_flagged_for_deletion += 3
								ENDIF
							ELSE					
								// mark bottom left/right, top left
								IF current__row - 1 >= min_row
								AND current__column - 1 >= min_col
									block_status[current__row - 1][current__column - 1] = DELETE
									block_status[current__row][current__column - 1] = DELETE
									block_status[current__row - 1][current__column] = DELETE
									number_flagged_for_deletion += 3
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDFOR
			
		ENDFOR
		
		bomb_power_selected = TRUE
	ENDIF
	
	IF bomb_power_selected
	AND CHECK_FOR_DELETION()
		bomb_powerup_in_effect = FALSE
		bomb_power_selected = FALSE
	ENDIF
ENDPROC

PROC COLOUR_REMOVAL_POWER_UP_ACTION()
	// PURPOSE: selects a colour at random from those that could possibly be on screen
	//			and removes them from the grid
	
	INT colour_int
	INT number_of_colours
	INT time_between_choice_cycles
	time_between_choice_cycles  = 750
		
	/// link the number of possible colours to the current level
	IF current_level < 5
		number_of_colours = 3		
	ELSE
		IF current_level < 10
			number_of_colours = 4			
		ELSE
			IF current_level < 15
				number_of_colours = 5				
			ELSE
				number_of_colours = max_number_of_colours //as ORANGE_CUBE block is fixed?				
			ENDIF
		ENDIF
	ENDIF
	
	// Automatic
	IF is_this_tutorial
		number_of_colours = max_number_of_colours //as ORANGE_CUBE block is fixed?				
	ENDIF
	
	// if enough time has passed 
	time_last_box_made += (0+@1000)
	IF time_last_box_made >= time_between_choice_cycles
		//cycle the colour choice			
		// convert the in to colour enum
		colour_int = ENUM_TO_INT (random_colour_to_be_removed)		
		colour_int++
			
		IF colour_int >= number_of_colours
			colour_int = 0
		ENDIF
			
		random_colour_to_be_removed = INT_TO_ENUM(BOARD_GRID_CONTENTS, colour_int)
		time_last_box_made = 0
	ENDIF
	
	// draw the individual boxes
	
	INT block_r[6]
	INT block_g[6]
	INT block_b[6]
	
	// need to determine the preview block colour
	block_r[RED]		= 255	 block_g[RED]		= 0			block_b[RED]		= 0
	block_r[BLUE]		= 0	 	block_g[BLUE]		= 248		block_b[BLUE]		= 255
	block_r[GREEN]		= 0	 	block_g[GREEN]		= 255		block_b[GREEN]		= 0
	block_r[YELLOW]		= 255	 block_g[YELLOW]	= 255		block_b[YELLOW]		= 0
	block_r[PURPLE]		= 255	 block_g[PURPLE]	= 0			block_b[PURPLE]		= 255
	block_r[ORANGE_CUBE] = 255	 block_g[ORANGE_CUBE]	= 165	block_b[ORANGE_CUBE] = 0
	
	RGBA_COLOUR_STRUCT rgbaBlock = rgbaWhiteOut
	
	IF NOT colour_removal_selected
		INT local_counter = 0
		INT square_alpha
				
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaDarkSeeThrough)
				
		FOR local_counter = 0 TO (number_of_colours	- 1)
		
			IF (local_counter = ENUM_TO_INT (random_colour_to_be_removed))
				square_alpha = 250
			ELSE
				square_alpha = 70
			ENDIF
				
			INIT_RGBA_STRUCT(rgbaBlock, block_r[local_counter], block_g[local_counter], block_b[local_counter], square_alpha)
					
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
				ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, random_block_texture, 
					INIT_VECTOR_2D(ig_random_colour_pos_x[local_counter],  ig_random_colour_pos_y[local_counter]),  
					INIT_VECTOR_2D(ig_random_colour_size_x ,  ig_random_colour_size_y),
					0.0000, rgbaDarkSeeThrough)
			ENDIF
								
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
				ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, random_block_texture, 
					INIT_VECTOR_2D(ig_random_colour_pos_x[local_counter],  ig_random_colour_pos_y[local_counter]),
					INIT_VECTOR_2D(ig_random_colour_size_x ,  ig_random_colour_size_y), 
					0.0000, rgbaBlock)
			ENDIF
				
		ENDFOR	
		
		/// scan through the grid and mark all blocks that have this colour as to be deleted
		IF QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
				
			INT current__column
			INT current__row
			
			FOR current__column = 0 TO max_col
				// initialise the starting column
				FOR current__row = min_row TO max_row
					// if block has been marked for deletion			
					IF grid_box[current__row][current__column].this_box_colour = random_colour_to_be_removed			
						block_status[current__row][current__column] = DELETE
						number_flagged_for_deletion++
					ENDIF			
					
				ENDFOR
				
			ENDFOR
			
			colour_removal_selected = TRUE
			
		ENDIF
		
		IF is_this_tutorial
			// Time difference
			IF random_colour_to_be_removed = PURPLE
				
				INT current__column
				INT current__row
				
				FOR current__column = 0 TO max_col
					// initialise the starting column
					FOR current__row = min_row TO max_row
						// if block has been marked for deletion			
						IF grid_box[current__row][current__column].this_box_colour = random_colour_to_be_removed			
							block_status[current__row][current__column] = DELETE
							number_flagged_for_deletion++
						ENDIF			
						
					ENDFOR
					
				ENDFOR
				
				colour_removal_selected = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	// Selected a colour so run the deletion process.
	IF colour_removal_selected
	AND CHECK_FOR_DELETION()
		time_last_box_made = 0
		colour_removal_powerup_in_effect = FALSE
		colour_removal_selected = FALSE
	ENDIF
		
ENDPROC

PROC POWERUP_UPDATE()
		
	// PURPOSE once the power bar has been filled checks for player pressing one of the face buttons
	//			to activate a power up

	IF bomb_powerup_in_effect
		BOMB_POWER_UP_ACTION()
	ENDIF

	IF colour_removal_powerup_in_effect
		COLOUR_REMOVAL_POWER_UP_ACTION()
	ENDIF
	
	IF eraser_powerup_in_effect
		ERASER_POWER_UP_ACTION()
	ENDIF

	IF number_of_power_uses > 0// power_bar_full
		IF NOT slow_down_powerup_in_effect
		AND NOT colour_removal_powerup_in_effect
			IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
			OR IS_DISABLED_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
				IF NOT is_cross_pressed
				AND !bomb_powerup_in_effect
					// play audio
					PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
					bomb_powerup_in_effect = TRUE
					has_a_power_up_been_selected = TRUE
				ENDIF
			ELSE
				IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
					IF NOT is_square_pressed
					AND !colour_removal_powerup_in_effect
						// play audio
						PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
						colour_removal_powerup_in_effect = TRUE
						has_a_power_up_been_selected = TRUE
					ENDIF
				ELSE
					IF IS_DISABLED_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
					OR IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
						IF NOT is_triangle_pressed
						AND !slow_down_powerup_in_effect

							// play audio
							PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
							slow_down_powerup_in_effect = TRUE
							SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"Slowmo",1.0)
							has_a_power_up_been_selected = TRUE
							time_power_up_active = 0
						ENDIF
					ELSE
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						OR IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
							IF NOT is_circle_pressed
							AND !eraser_powerup_in_effect
							
								// play audio
								PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
								CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "IS_KEYBOARD_KEY_PRESSED(KEY_J) - Picked up this frame")
								eraser_powerup_in_effect = TRUE
								has_a_power_up_been_selected = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	/// remove the objects from the power bar
	IF has_a_power_up_been_selected
		has_a_power_up_been_selected 			= FALSE
		
		number_of_power_uses --
		IF number_of_power_uses < 0
			number_of_power_uses = 0
		ENDIF

		IF current_level < 20
			sQub3dTelemetry.powerUps++
		ENDIF
		
		// set initial button states
		is_cross_pressed	= TRUE					
		is_square_pressed	= TRUE
		is_circle_pressed	= TRUE
		is_triangle_pressed	= TRUE	
	ELSE
		// check for button states	
		IF IS_DISABLED_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
		OR IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
			is_cross_pressed = TRUE
		ELSE
			is_cross_pressed = FALSE
		ENDIF
		
		IF IS_DISABLED_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
		OR IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)	
			is_square_pressed = TRUE
		ELSE
			is_square_pressed = FALSE
		ENDIF
		
		IF IS_DISABLED_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
		OR IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
			is_circle_pressed = TRUE
		ELSE
			is_circle_pressed = FALSE
		ENDIF
		
		IF IS_DISABLED_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
		OR IS_CONTROL_PRESSED (FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
			is_triangle_pressed = TRUE
		ELSE
			is_triangle_pressed = FALSE
		ENDIF
		
		
	ENDIF
ENDPROC

///////////////////////////////////////////
////// **** FALLING BLOCK STUFF **** //////
///////////////////////////////////////////

PROC CREATE_RANDOM_BLOCK()
	INT colour_selection
	INT number_of_colours
	INT previous_colour
	
	UNUSED_PARAMETER(number_of_colours)
		
	/// initialise the struct
	IF first_time_through
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "CREATE_RANDOM_BLOCK - first_time_through")
		falling_box.is_box_a_on_grid = FALSE
		falling_box.is_box_b_on_grid = FALSE
		first_time_through = FALSE
		allowed_two_block_of_same_colour = FALSE
		number_of_blocks_created = 0
		
		// set up the colour of the first block
		number_of_colours = 3
		// generate a new colour for next block
		colour_selection =  GET_RANDOM_INT_IN_RANGE(0, number_of_colours)
		// convert the in to colour enum
		preview_box.block_a_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, colour_selection)
		
		// check if this block is the same colour as the previous one
		previous_colour = ENUM_TO_INT (preview_box.block_a_colour)	
		IF (colour_selection = previous_colour)
			/// if allowed to have two the same
			IF allowed_two_block_of_same_colour
				// reset the block counter
				number_of_blocks_created = 0
				// update the flag so cant have same colour pair until required no. blocks have been made
				allowed_two_block_of_same_colour = FALSE
			ELSE
				// need to find a new colour for this block	(go up or down one on enum list?)
				// check if we can go down one
				IF colour_selection > 0
					colour_selection--
				ELSE
					// go up on on enum list
					colour_selection++
				ENDIF
				// increment block counter
				number_of_blocks_created++
			ENDIF
		ELSE
			// increment block counter
			number_of_blocks_created++
		ENDIF
		
		// convert the in to colour enum
		preview_box.block_b_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, colour_selection)
	
	ELSE
		// increment the number of blocks created flag
		IF number_of_blocks_created > 2
			allowed_two_block_of_same_colour = TRUE
		ENDIF
	ENDIF	

	/// link the number of possible colours to the current level
	IF current_level < 5
		number_of_colours = 3
	ELSE
		IF current_level < 10
			number_of_colours = 4
		ELSE
			IF current_level < 15
				number_of_colours = 5
			ELSE
				number_of_colours = max_number_of_colours //as ORANGE_CUBE block is fixed?
				/// check for speeding up the blocks
				IF current_level >= 20
					/// determine the speed up penalty
					blocks_fall_speed_mulitplier = (time_deduction_factor * (current_level - 20))
					/// keep a minimum of 250 ms
					IF blocks_fall_speed_mulitplier > (start_time_between_block_movement - 250) 
						blocks_fall_speed_mulitplier = (start_time_between_block_movement - 250)
					ENDIF					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	//////////////////////
	// *** block a **** //
	//////////////////////

	// remove any previous version
	
	/// start point
	falling_box.block_a_position_in_col = 2
	falling_box.block_a_position_in_row = 0
	
	falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
	falling_box.a_pos.z = bottom_z
	
	/// create the box
	IF NOT DOES_ENTITY_EXIST(falling_box.falling_box_a)
		QUB3D_CREATE_OBJECT(falling_box.falling_box_a, box_model, falling_box.a_pos)
	ENDIF
	
	IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
		SET_ENTITY_HEADING(falling_box.falling_box_a, 0.0)
		SET_ENTITY_COLLISION(falling_box.falling_box_a, FALSE)
		FREEZE_ENTITY_POSITION(falling_box.falling_box_a, TRUE)
	
		falling_box.block_a_colour = preview_box.block_a_colour
		QUB3D_SET_CUBE_COLOR(falling_box.falling_box_a, falling_box.block_a_colour)
	ENDIF
		
	// update the flag
	falling_box.is_box_a_on_grid = TRUE
			
	//////////////////////
	// *** block b **** //
	//////////////////////
	
	/// if block not carried over
		
	// remove any previous version
	IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
		DELETE_OBJECT(falling_box.falling_box_b)
	ENDIF
	
	/// start point
	falling_box.block_b_position_in_col = 3
	falling_box.block_b_position_in_row = 0
	falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
	falling_box.b_pos.z = bottom_z
	// create the box
	IF NOT DOES_ENTITY_EXIST(falling_box.falling_box_b)
		QUB3D_CREATE_OBJECT(falling_box.falling_box_b, box_model,falling_box.b_pos)
	ENDIF
	
	IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
		SET_ENTITY_HEADING(falling_box.falling_box_b, 0.0)
		SET_ENTITY_COLLISION(falling_box.falling_box_b, FALSE)
		FREEZE_ENTITY_POSITION(falling_box.falling_box_b, TRUE)
		
		falling_box.block_b_colour = preview_box.block_b_colour
		QUB3D_SET_CUBE_COLOR(falling_box.falling_box_b, falling_box.block_b_colour)
	ENDIF
		
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "Creating Falling Box B this frame")
	// update the flag
	falling_box.is_box_b_on_grid = TRUE
	
	/// *** attach them b to a	**** ////	
	falling_box.block_a_is	= LEFT_BLOCK
	falling_box.block_b_is	= RIGHT_BLOCK
	
	// set the initial flag
	is_a_box_in_motion = TRUE
	// set the time the block was created
	time_started = TO_FLOAT(GET_GAME_TIMER())
	
	
	/////////////////////////////
	// *** preview  BLOCKS *** //
	/////////////////////////////
	IF NOT is_this_tutorial
		// generate a new colour for next block
		colour_selection = GET_RANDOM_INT_IN_RANGE(0, number_of_colours)
		
		// convert the in to colour enum
		preview_box.block_a_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, colour_selection)
					
		// generate a new colour for next block
		colour_selection = GET_RANDOM_INT_IN_RANGE(0, number_of_colours)
						
		// check if this block is the same colour as the previous one
		previous_colour = ENUM_TO_INT (preview_box.block_a_colour)	
		IF (colour_selection = previous_colour)
			/// if allowed to have two the same
			IF allowed_two_block_of_same_colour
				// reset the block counter
				number_of_blocks_created = 0
				// update the flag so cant have same colour pair until required no. blocks have been made
				allowed_two_block_of_same_colour = FALSE
			ELSE
				// need to find a new colour for this block	(go up or down one on enum list?)
				// check if we can go down one
				IF colour_selection > 0
					colour_selection--
				ELSE
					// go up on on enum list
					colour_selection++
				ENDIF
				// increment block counter
				number_of_blocks_created++
			ENDIF
		ELSE
			// increment block counter
			number_of_blocks_created++
		ENDIF
		
		// convert the in to colour enum
		preview_box.block_b_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, colour_selection)
	
		preview_box.a_pos = SUBTRACT_TWO_VECTORS(game_grid_pos, <<-1.0, 1.5, (top_z+1)>>)//<<-3.0, 3.0, top_z>>
		preview_box.b_pos = SUBTRACT_TWO_VECTORS(game_grid_pos, <<-1.0, 1.5, top_z>>)//<<-2.13397, 3.0, (top_z - 0.5)>>
		
	ENDIF
	
	
ENDPROC

PROC DETERMINE_REALATIVE_POSITION_OF_BOXES()
	IF falling_box.a_pos.x < falling_box.b_pos.x
		// a is to left of b
		falling_box.block_a_is	= LEFT_BLOCK
 		falling_box.block_b_is	= RIGHT_BLOCK
	ELSE
		IF falling_box.a_pos.x > falling_box.b_pos.x
			// a is to right of b
			falling_box.block_a_is	= RIGHT_BLOCK
 			falling_box.block_b_is	= LEFT_BLOCK
		ELSE
			// a and b same x so either above or below or on top of each other
			IF falling_box.a_pos.z < falling_box.b_pos.z
				// a is below b
				falling_box.block_a_is	= BOTTOM_BLOCK
 				falling_box.block_b_is	= TOP_BLOCK
			ELSE
				IF falling_box.a_pos.z > falling_box.b_pos.z
					// a is above b
					falling_box.block_a_is	= TOP_BLOCK
					falling_box.block_b_is	= BOTTOM_BLOCK
				ELSE
					// share same y pos so must be on top of each other
					IF falling_box.a_pos.z < falling_box.b_pos.z
						// a is underneath b
						falling_box.block_a_is	= LOWER_BLOCK
						falling_box.block_b_is	= UPPER_BLOCK
					ELSE
						// a must be above b
						falling_box.block_a_is	= UPPER_BLOCK
						falling_box.block_b_is	= LOWER_BLOCK
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_BOX_BE_MOVED()
		
	BOOL return_value = TRUE
	INT poss_col_a
	INT poss_row_a
	INT poss_col_b
	INT poss_row_b
			
		poss_col_a = falling_box.block_a_position_in_col + a_col_shift
		poss_row_a = falling_box.block_a_position_in_row + a_row_shift
		
		SWITCH new_a_relative_pos
			CASE LEFT_BLOCK
				// b must be right block (same row, +1 col)
				poss_col_b = (poss_col_a + 1)
				poss_row_b = poss_row_a
			BREAK
			
			CASE RIGHT_BLOCK
				// b must be left block (same row, -1 col)
				poss_col_b = (poss_col_a - 1)
				poss_row_b = poss_row_a				
			BREAK
			
			CASE TOP_BLOCK
				// b must be bottom block (same col -1 row
				poss_col_b = poss_col_a 
				poss_row_b = (poss_row_a + 1)									
			BREAK

			CASE BOTTOM_BLOCK
				// b must be top block (same col + 1 row)
				poss_col_b = poss_col_a
				poss_row_b = (poss_row_a - 1)
			BREAK
			
			CASE UPPER_BLOCK
				// be must be lower block (same row and col)
				poss_col_b = poss_col_a
				poss_row_b = poss_row_a	
			BREAK
			
			CASE LOWER_BLOCK
				// must be upper block (same row and col)
				poss_col_b = poss_col_a
				poss_row_b = poss_row_a	
			BREAK
		ENDSWITCH
		
		
		/////////////////////////
		/// **** BLOCK A **** ///
		/////////////////////////
		
		/// within grid boundaries
		IF poss_col_a <= max_col
		AND poss_col_a >= min_col
		AND poss_row_a <= max_row
		AND poss_row_a >= min_row
			/// if destination contains a box already
			IF NOT (grid_box[poss_row_a][poss_col_a].this_box_colour = EMPTY)
				return_value =  FALSE
			ENDIF
		ELSE
			return_value =  FALSE
		ENDIF
		
		/////////////////////////
		/// **** BLOCK B **** ///
		/////////////////////////
		
		/// within grid boundaries
		IF poss_col_b <= max_col
		AND poss_col_b >= min_col
		AND poss_row_b <= max_row
		AND poss_row_b >= min_row
			/// if destination contains a box already
			IF NOT (grid_box[poss_row_b][poss_col_b].this_box_colour = EMPTY)
				return_value =  FALSE
			ENDIF
		ELSE
			return_value =  FALSE
		ENDIF
		
		

		IF return_value

			/// update the coords (A BLOCK)
			falling_box.block_a_position_in_col = poss_col_a
			falling_box.block_a_position_in_row = poss_row_a
			
			if falling_box.block_a_position_in_row >= number_of_rows
				CDEBUG1LN(DEBUG_MINIGAME, "Number of Rows exceeded max: ", number_of_rows)
				falling_box.block_a_position_in_row = (number_of_rows-1)
			ENDIF
			
			if falling_box.block_a_position_in_col >= number_of_columns
				CDEBUG1LN(DEBUG_MINIGAME, "Number of Cols exceeded max: ", number_of_columns)
				falling_box.block_a_position_in_col = (number_of_columns-1)
			ENDIF
			
			falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
			
			IF new_a_relative_pos = UPPER_BLOCK
				falling_box.a_pos.z = top_z
			ELSE
				falling_box.a_pos.z = bottom_z
			ENDIF
								
			/// update the coords (B BLOCK)
			falling_box.block_b_position_in_col = poss_col_b
			falling_box.block_b_position_in_row = poss_row_b
			
			falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos

			IF new_b_relative_pos = UPPER_BLOCK
				falling_box.b_pos.z = top_z
			ELSE
				falling_box.b_pos.z = bottom_z
			ENDIF

			//Play Audio
			soundId=GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(soundId, rotating_block_sfx)
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_a_position_in_col))
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_b_position_in_row))
			RELEASE_SOUND_ID(soundId)
		
			IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
				SET_ENTITY_COORDS(falling_box.falling_box_a, falling_box.a_pos)
			ENDIF
			IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
				SET_ENTITY_COORDS(falling_box.falling_box_b, falling_box.b_pos)
			ENDIF
			// find the new relative positions 					
			falling_box.block_a_is = new_a_relative_pos
			falling_box.block_b_is = new_b_relative_pos

		ENDIF
		

		RETURN return_value
		
ENDFUNC

PROC CHECK_FOR_PLAYER_INPUT()
	
	FLOAT left_stick_x, left_stick_y
	FLOAT right_stick_x, right_stick_y
	
	// if neither block has been fixed
	IF falling_box.is_box_b_on_grid
	AND falling_box.is_box_a_on_grid
		/// if player is allowed to move the block
		IF NOT is_player_moving_block
			//// check position of analogue sticks
			QUB3D_GET_STICK_INPUT(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
			
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				CDEBUG1LN(DEBUG_minigame, sPrefixDebugQub3d, "- PRESSED MOVE_LEFT")
			ENDIF
			
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				CDEBUG1LN(DEBUG_minigame, sPrefixDebugQub3d, "- PRESSED MOVE_RIGHT")
			ENDIF
			
			/// are they pressing a button
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
			OR left_stick_x >= 0.85
			OR left_stick_x <= -0.85
				IF NOT is_button_still_pressed
					/// determine which direction player presses
					IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
					OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
					OR left_stick_x <= -0.85
						direction_pressed = PRESSED_LEFT
						is_button_still_pressed = TRUE
					ENDIF
					
					IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
					OR left_stick_x >= 0.85
						direction_pressed = PRESSED_RIGHT
						is_button_still_pressed = TRUE
					ENDIF
			
					///// *******8 TEMP FOR DEBUG REMOVE IT ONCE THIS WORKS ******/////
					IF is_button_still_pressed = TRUE
						
						//DETERMINE_REALATIVE_POSITION_OF_BOXES()						  /////
						
						///// *******8 TEMP FOR DEBUG REMOVE IT ONCE THIS WORKS ******/////
						
						/// initialise the shift variables for this move
						a_col_shift 		= 0
						a_row_shift			= 0
						
						
						new_a_relative_pos = falling_box.block_a_is 
						new_b_relative_pos = falling_box.block_b_is
						
						//// calculate the shift depending on position and which directionpressed
						
						SWITCH falling_box.block_a_is
							CASE LEFT_BLOCK
								// a is 1 col to left of b
								IF direction_pressed = PRESSED_LEFT
									// a doesnt move
									a_col_shift = 0
									// shift b down 1 columns and up on z
							
									new_a_relative_pos = LOWER_BLOCK
									new_b_relative_pos = UPPER_BLOCK
									
								ELSE
									IF direction_pressed = PRESSED_RIGHT
										// shift a up 1 columns and up on z
										a_col_shift = 1
										
										new_a_relative_pos = UPPER_BLOCK
										new_b_relative_pos = LOWER_BLOCK
									ENDIF
								ENDIF
							BREAK
							
							CASE RIGHT_BLOCK
								// a is 1 col to right of b
								IF direction_pressed = PRESSED_LEFT
									// shift a down 1 columns and up on z
									a_col_shift = -1

									new_a_relative_pos = UPPER_BLOCK
									new_b_relative_pos = LOWER_BLOCK
									
								ELSE
									IF direction_pressed = PRESSED_RIGHT
										// shift a up a column
										a_col_shift = 0
										// shift b up 1 columns and up on z
										new_a_relative_pos = LOWER_BLOCK
										new_b_relative_pos = UPPER_BLOCK
									ENDIF
								ENDIF				
							BREAK
							
							CASE TOP_BLOCK
								// a is 1 row above b
								IF direction_pressed = PRESSED_LEFT
									// shift a down a column
									a_col_shift = -1
								ELSE
									IF direction_pressed = PRESSED_RIGHT
										// shift a up a column
										a_col_shift = 1
									ENDIF
								ENDIF
								
									
								new_a_relative_pos = TOP_BLOCK
								new_b_relative_pos = BOTTOM_BLOCK
								
							BREAK
							
							
							CASE BOTTOM_BLOCK
								// a is 1 row below b
								IF direction_pressed = PRESSED_LEFT
									// shift a down a column
									a_col_shift = -1
								ELSE
									IF direction_pressed = PRESSED_RIGHT
										// shift a up a column
										a_col_shift = 1
									ENDIF
								ENDIF
								
								
								new_a_relative_pos = BOTTOM_BLOCK
								new_b_relative_pos = TOP_BLOCK
							BREAK
							
							CASE UPPER_BLOCK
								// a must be above b on z
								IF direction_pressed = PRESSED_LEFT
									// shift a down 2 columns and down on z
									a_col_shift = -1

									new_a_relative_pos = LEFT_BLOCK
									new_b_relative_pos = RIGHT_BLOCK
									
								ELSE
									IF direction_pressed = PRESSED_RIGHT
										// shift a up 2 columns and down on z
										a_col_shift = 1

										new_a_relative_pos = RIGHT_BLOCK 
										new_b_relative_pos = LEFT_BLOCK
									ENDIF
								ENDIF
							BREAK
							
							CASE LOWER_BLOCK
								// a is underneath b on z
								IF direction_pressed = PRESSED_LEFT
									//  shift a down 1 column
									a_col_shift = 0

									new_a_relative_pos = RIGHT_BLOCK 
									new_b_relative_pos = LEFT_BLOCK
									
								ELSE
									IF direction_pressed = PRESSED_RIGHT
										//  shift a up 1 column
										a_col_shift = 0

										new_a_relative_pos = LEFT_BLOCK
										new_b_relative_pos = RIGHT_BLOCK
									ENDIF
								ENDIF
							BREAK
							
						ENDSWITCH
						
						is_player_moving_block = TRUE
					ENDIF
				ELSE
					// check if enough time has passed to allow it to move again
					current_time = TO_FLOAT(GET_GAME_TIMER())
					time_difference = current_time - time_last_press
					IF time_difference >= time_between_presses
						is_button_still_pressed = FALSE
						direction_pressed = PRESSED_NOTHING
					ENDIF
				ENDIF
			ELSE
				direction_pressed = PRESSED_NOTHING
				is_button_still_pressed = FALSE
				
				// check if they are pressing down to speed up falling block
				IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
				OR left_stick_y >= 0.85
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
				OR left_stick_y <= -0.85
					time_between_block_movement = (start_time_between_block_movement / 10) //1000					
					/// need to take into account the speed multiplier so it doesn't get sped up too much
					is_player_forcing_blocks_speed_up = TRUE
				ELSE
					is_player_forcing_blocks_speed_up = FALSE
					time_between_block_movement = start_time_between_block_movement				
				ENDIF
			ENDIF
			
			
		ELSE
			// determine if it should be shifted
			IF CAN_BOX_BE_MOVED()
				
				time_last_press = TO_FLOAT(GET_GAME_TIMER())

			ENDIF

			is_player_moving_block = FALSE
			
		ENDIF
	ELSE
		// both blocks arent on grid can still check for speed up though
		//// check position of analogue sticks
		QUB3D_GET_STICK_INPUT(left_stick_x, left_stick_y, right_stick_x,  right_stick_y)
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
		OR left_stick_y >= 0.85
			time_between_block_movement = (start_time_between_block_movement / 10) //1000					
			/// need to take into account the speed multiplier so it doesn't get sped up too much
			is_player_forcing_blocks_speed_up = TRUE
		ELSE
			is_player_forcing_blocks_speed_up = FALSE
			time_between_block_movement = start_time_between_block_movement				
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_FALLING_BOX_POSITION()
	
	//// basically shift the blocks down one row and flags when they have reached another block
	/// set the new position of the objects if they are not in final position
	BOOL fix_a_in_current_pos = FALSE
	BOOL fix_b_in_current_pos = FALSE
		
	INT temp_col
	INT temp_row

	//STRING ptfx_to_use
	
	new_a_relative_pos = falling_box.block_a_is 
	new_b_relative_pos = falling_box.block_b_is
		
	////////////////////
	///// BLOCK A //////
	////////////////////
				
	IF falling_box.is_box_a_on_grid
		IF falling_box.is_box_b_on_grid
								
			///////////////////////////////////////
			//// ++++ LEFT OR RIGHT BLOCK ++++ ////
			///////////////////////////////////////
			IF falling_box.block_a_is = LEFT_BLOCK /// moves down a row
			OR falling_box.block_a_is = RIGHT_BLOCK /// moves down a row
				// check if at bottom of grid 			
				IF (falling_box.block_a_position_in_row + 1) <= max_row
					// scan the grid space below and see if it is free			
					IF (grid_box[(falling_box.block_a_position_in_row + 1)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						
						// shift it down one row
						falling_box.block_a_position_in_row += 1
						falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
						
						new_a_relative_pos = falling_box.block_a_is
					ELSE
						
						IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						AND (falling_box.block_a_position_in_row ) >= min_row
							fix_a_in_current_pos = TRUE
						
							IF (falling_box.block_a_position_in_row ) = min_row
							AND falling_box.block_a_position_in_col >= 2
							AND falling_box.block_a_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
						ENDIF
								
					ENDIF
				ELSE
					// at bottom of grid
					fix_a_in_current_pos = TRUE
				ENDIF
				
			ENDIF
			
	
			/////////////////////////////
			//// ++++ TOP BLOCK ++++ ////
			/////////////////////////////
			IF falling_box.block_a_is = TOP_BLOCK /// moves down 1 rows and increases on z
				// check if at bottom of grid 			
				IF (falling_box.block_a_position_in_row + 2) <= max_row  
					// scan the grid space below and see if it is free			
					IF (grid_box[(falling_box.block_a_position_in_row + 2)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						
						// shift it down one row
						falling_box.block_a_position_in_row += 1
						
						falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
						falling_box.a_pos.z = top_z
						
						new_a_relative_pos = UPPER_BLOCK
					ELSE
					
						IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						AND (falling_box.block_a_position_in_row ) >= min_row
							fix_a_in_current_pos = TRUE
						
							IF (falling_box.block_a_position_in_row ) = min_row
							AND falling_box.block_a_position_in_col >= 2
							AND falling_box.block_a_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// at bottom of grid
					fix_a_in_current_pos = TRUE
				ENDIF
			ENDIF
			
			////////////////////////////////
			//// ++++ BOTTOM BLOCK ++++ ////
			////////////////////////////////
			IF falling_box.block_a_is = BOTTOM_BLOCK // doesnt move
				// check if at bottom of grid 			
				IF (falling_box.block_a_position_in_row + 1) <= max_row  
					// scan the grid space below and see if it is free			
					IF (grid_box[(falling_box.block_a_position_in_row + 1)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						
						// shift it down one row
						falling_box.block_a_position_in_row += 0
						//falling_box.a_pos.y = TO_FLOAT (falling_box.block_a_position_in_row)
						falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
						falling_box.a_pos.z = bottom_z
						
						new_a_relative_pos = LOWER_BLOCK
					ELSE
						IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						AND (falling_box.block_a_position_in_row ) >= min_row
							fix_a_in_current_pos = TRUE
						
							IF (falling_box.block_a_position_in_row ) = min_row
							AND falling_box.block_a_position_in_col >= 2
							AND falling_box.block_a_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// at bottom of grid
					fix_a_in_current_pos = TRUE
				ENDIF
			ENDIF
			
			///////////////////////////////
			//// ++++ UPPER BLOCK ++++ ////
			///////////////////////////////
			IF falling_box.block_a_is = UPPER_BLOCK // moves down 1 rows and decreases on z
				// check if at bottom of grid 			
				IF (falling_box.block_a_position_in_row + 1) <= max_row  
					// scan the grid space below and see if it is free			
					IF (grid_box[(falling_box.block_a_position_in_row + 1)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						
						// shift it down one row
						falling_box.block_a_position_in_row += 1
						//falling_box.a_pos.y = TO_FLOAT (falling_box.block_a_position_in_row)
						falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
						falling_box.a_pos.z = bottom_z
						
						new_a_relative_pos = BOTTOM_BLOCK
					ELSE
						IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						AND (falling_box.block_a_position_in_row ) >= min_row
							fix_a_in_current_pos = TRUE
						
							IF (falling_box.block_a_position_in_row ) = min_row
							AND falling_box.block_a_position_in_col >= 2
							AND falling_box.block_a_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// at bottom of grid
					fix_a_in_current_pos = TRUE
				ENDIF
			ENDIF
			
			
			///////////////////////////////
			//// ++++ LOWER BLOCK ++++ ////
			///////////////////////////////
			IF falling_box.block_a_is = LOWER_BLOCK // doesnt move
				// check if at bottom of grid 			
				IF (falling_box.block_a_position_in_row + 1) <= max_row  
					// scan the grid space below and see if it is free			
					IF (grid_box[(falling_box.block_a_position_in_row + 1)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						
						// shift it down one row
						falling_box.block_a_position_in_row -= 0
						//falling_box.a_pos.y = TO_FLOAT (falling_box.block_a_position_in_row)
						falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
						falling_box.a_pos.z = bottom_z
						
						new_a_relative_pos = TOP_BLOCK
					ELSE
						IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
						AND (falling_box.block_a_position_in_row ) >= min_row
							fix_a_in_current_pos = TRUE
							
							IF (falling_box.block_a_position_in_row ) = min_row
							AND falling_box.block_a_position_in_col >= 2
							AND falling_box.block_a_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// at bottom of grid
					fix_a_in_current_pos = TRUE
				ENDIF
			ENDIF
		ELSE
			// blocks just falls to lowest empty slot
			IF (falling_box.block_a_position_in_row + 1) <= max_row
				// scan the grid space below and see if it is free			
				IF (grid_box[(falling_box.block_a_position_in_row + 1)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
					
					// shift it down one row
					falling_box.block_a_position_in_row += 1
					//falling_box.a_pos.y = TO_FLOAT (falling_box.block_a_position_in_row)
					falling_box.a_pos = grid_box[falling_box.block_a_position_in_row][falling_box.block_a_position_in_col].box_object_pos
					falling_box.a_pos.z = top_z
					new_a_relative_pos = LEFT_BLOCK
				ELSE
					IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
					AND (falling_box.block_a_position_in_row ) >= min_row
						fix_a_in_current_pos = TRUE
						
						IF (falling_box.block_a_position_in_row ) = min_row
						AND falling_box.block_a_position_in_col >= 2
						AND falling_box.block_a_position_in_col <= 3
							is_in_danger_zone = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// at bottom of grid
				fix_a_in_current_pos = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	////////////////////
	///// BLOCK B //////
	////////////////////
	
	IF falling_box.is_box_b_on_grid
		IF falling_box.is_box_a_on_grid
		
			// No option to move due to restrictions.
			IF (falling_box.block_b_position_in_row) = min_row
			AND falling_box.b_pos.z = bottom_z
			AND falling_box.block_b_is != TOP_BLOCK
			AND falling_box.block_b_position_in_col >= 2
			AND falling_box.block_b_position_in_col <= 3
			AND (grid_box[(falling_box.block_b_position_in_row + 1)][falling_box.block_b_position_in_col].this_box_colour != EMPTY)
			AND (grid_box[(falling_box.block_b_position_in_row)][1].this_box_colour != EMPTY)
			AND (grid_box[(falling_box.block_b_position_in_row)][4].this_box_colour != EMPTY)
				fix_b_in_current_pos = TRUE
			ENDIF
		
			///////////////////////////////////////
			//// ++++ LEFT OR RIGHT BLOCK ++++ ////
			///////////////////////////////////////
			IF falling_box.block_b_is  = LEFT_BLOCK /// moves down a row
			OR falling_box.block_b_is = RIGHT_BLOCK /// moves down a row
				// check if at bottom of grid 			
				IF (falling_box.block_b_position_in_row + 1) <= max_row
					// scan the grid space below and see if it is free
					IF (grid_box[(falling_box.block_b_position_in_row + 1)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						// shift it down one row
						falling_box.block_b_position_in_row += 1
						//falling_box.b_pos.y = TO_FLOAT (falling_box.block_b_position_in_row)
						falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
						falling_box.b_pos.z = bottom_z
						new_b_relative_pos = falling_box.block_b_is
					
					ELSE
						IF (grid_box[(falling_box.block_b_position_in_row)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						AND (falling_box.block_b_position_in_row) >= min_row
						
							fix_b_in_current_pos = TRUE
						
							IF falling_box.block_b_position_in_row = min_row
							AND falling_box.block_b_position_in_col >= 2
							AND falling_box.block_b_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
												
						ENDIF
						
//						IF (grid_box[(falling_box.block_a_position_in_row)][falling_box.block_a_position_in_col].this_box_colour = EMPTY)
//						AND (falling_box.block_a_position_in_row ) >= min_row
//							fix_a_in_current_pos = TRUE
//						
//							IF (falling_box.block_a_position_in_row ) = min_row
//							AND falling_box.block_a_position_in_col >= 2
//							AND falling_box.block_a_position_in_col <= 3
//								is_in_danger_zone = TRUE
//							ENDIF
//						ENDIF
						
					ENDIF
					
				ELSE
					fix_b_in_current_pos = TRUE
				ENDIF
			ENDIF
			
	
			/////////////////////////////
			//// ++++ TOP BLOCK ++++ ////
			/////////////////////////////
			IF falling_box.block_b_is = TOP_BLOCK /// moves down 1 rows and increases on z
				// check if at bottom of grid 			
				IF (falling_box.block_b_position_in_row + 2) <= max_row
					// scan the grid space below and see if it is free
					IF (grid_box[(falling_box.block_b_position_in_row + 2)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						// shift it down one row
						falling_box.block_b_position_in_row += 1
												
						falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
						falling_box.b_pos.z = top_z
						
						new_b_relative_pos = UPPER_BLOCK
					ELIF (grid_box[(falling_box.block_b_position_in_row)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						IF (falling_box.block_b_position_in_row) >= min_row
						
							fix_b_in_current_pos = TRUE
						
							IF falling_box.block_b_position_in_row = min_row
							AND falling_box.block_b_position_in_col >= 2
							AND falling_box.block_b_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
												
						ENDIF
					ENDIF
				ELSE
					fix_b_in_current_pos = TRUE
				ENDIF
			ENDIF
			
			////////////////////////////////
			//// ++++ BOTTOM BLOCK ++++ ////
			////////////////////////////////
			IF falling_box.block_b_is = BOTTOM_BLOCK // doesnt move
				// check if at bottom of grid 			
				IF (falling_box.block_b_position_in_row + 1) <= max_row
					// scan the grid space below and see if it is free
					IF (grid_box[(falling_box.block_b_position_in_row + 1)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						// shift it down one row
						falling_box.block_b_position_in_row += 0
						//falling_box.b_pos.y = TO_FLOAT (falling_box.block_b_position_in_row)
						falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
						falling_box.b_pos.z = bottom_z
						new_b_relative_pos = LOWER_BLOCK
					ELIF (grid_box[(falling_box.block_b_position_in_row)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						IF (falling_box.block_b_position_in_row) >= min_row
						
							fix_b_in_current_pos = TRUE
						
							IF falling_box.block_b_position_in_row = min_row
							AND falling_box.block_b_position_in_col >= 2
							AND falling_box.block_b_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
												
						ENDIF
					ENDIF
				ELSE
					fix_b_in_current_pos = TRUE
				ENDIF
			ENDIF
		
			///////////////////////////////
			//// ++++ UPPER BLOCK ++++ ////
			///////////////////////////////
			IF falling_box.block_b_is = UPPER_BLOCK // moves down 1 rows and decreases on z
				// check if at bottom of grid 			
				IF (falling_box.block_b_position_in_row + 1) <= max_row  
					// scan the grid space below and see if it is free			
					IF (grid_box[(falling_box.block_b_position_in_row + 1)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						
						// shift it down one row
						falling_box.block_b_position_in_row += 1
						falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
						falling_box.b_pos.z = bottom_z
						
						new_b_relative_pos = BOTTOM_BLOCK
					ELIF (grid_box[(falling_box.block_b_position_in_row)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						IF (falling_box.block_b_position_in_row) >= min_row
						
							fix_b_in_current_pos = TRUE
						
							IF falling_box.block_b_position_in_row = min_row
							AND falling_box.block_b_position_in_col >= 2
							AND falling_box.block_b_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
												
						ENDIF
					ENDIF
				ELSE
					// at bottom of grid
					fix_b_in_current_pos = TRUE
				ENDIF
			ENDIF
		
		
			///////////////////////////////
			//// ++++ LOWER BLOCK ++++ ////
			///////////////////////////////
			IF falling_box.block_b_is = LOWER_BLOCK // doesn't move
				// check if at bottom of grid 			
				IF (falling_box.block_b_position_in_row + 1) <= max_row
					// scan the grid space below and see if it is free
					IF (grid_box[(falling_box.block_b_position_in_row + 1)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						// shift it down one row
						falling_box.block_b_position_in_row -= 0
						//falling_box.b_pos.y = TO_FLOAT (falling_box.block_b_position_in_row)
						falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
						new_b_relative_pos = TOP_BLOCK
					ELIF (grid_box[(falling_box.block_b_position_in_row)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
						IF (falling_box.block_b_position_in_row) >= min_row
						
							fix_b_in_current_pos = TRUE
						
							IF falling_box.block_b_position_in_row = min_row
							AND falling_box.block_b_position_in_col >= 2
							AND falling_box.block_b_position_in_col <= 3
								is_in_danger_zone = TRUE
							ENDIF
												
						ENDIF
					ENDIF
				ELSE
					fix_b_in_current_pos = TRUE
				ENDIF
			ENDIF
		ELSE
			// check if at bottom of grid 			
			IF (falling_box.block_b_position_in_row + 1) <= max_row
				// scan the grid space below and see if it is free
				IF (grid_box[(falling_box.block_b_position_in_row + 1)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
					// shift it down one row
					falling_box.block_b_position_in_row += 1
					//falling_box.b_pos.y = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos.y
					falling_box.b_pos = grid_box[falling_box.block_b_position_in_row][falling_box.block_b_position_in_col].box_object_pos
					new_b_relative_pos = LEFT_BLOCK
					// Is the block  still valid or has it been cleared?
				ELIF (grid_box[(falling_box.block_b_position_in_row)][falling_box.block_b_position_in_col].this_box_colour = EMPTY)
					IF (falling_box.block_b_position_in_row) >= min_row
					
						fix_b_in_current_pos = TRUE
					
						IF falling_box.block_b_position_in_row = min_row
						AND falling_box.block_b_position_in_col >= 2
						AND falling_box.block_b_position_in_col <= 3
							is_in_danger_zone = TRUE
						ENDIF
											
					ENDIF
				ENDIF
			ELSE
				fix_b_in_current_pos = TRUE
			ENDIF
		
		ENDIF
	ENDIF
	
	/// check for both blocks finishing moving
	IF fix_a_in_current_pos	OR fix_b_in_current_pos
		// play audio as block has become fixed
		soundId=GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(soundId, fixing_block_sfx)
		
		IF fix_a_in_current_pos
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_a_position_in_col))
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_a_position_in_row))
		ENDIF
		
		IF fix_b_in_current_pos
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_b_position_in_col))
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_b_position_in_row))
		ENDIF
		
		RELEASE_SOUND_ID(soundId)
	ELSE
		// play audio as block falls a row
		soundId=GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(soundId, falling_block_sfx)
		SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_a_position_in_col))
		SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_b_position_in_row))
		RELEASE_SOUND_ID(soundId)
	ENDIF
	
	
	IF fix_a_in_current_pos

		// check if it is on upper level
		IF (falling_box.a_pos.z != top_z)
		
			// block should stay fixed here, update this grid space with info				
			temp_row = falling_box.block_a_position_in_row
			temp_col = falling_box.block_a_position_in_col
						
			grid_box[temp_row][temp_col].box_object_pos = <<falling_box.a_pos.x, falling_box.a_pos.y, bottom_z>>
			grid_box[temp_row][temp_col].this_box_colour = falling_box.block_a_colour
			
			IF NOT DOES_ENTITY_EXIST(grid_box[temp_row][temp_col].box_object)
				QUB3d_CREATE_OBJECT(grid_box[temp_row][temp_col].box_object,box_model, grid_box[temp_row][temp_col].box_object_pos)
			ENDIF
						
			IF DOES_ENTITY_EXIST(grid_box[temp_row][temp_col].box_object)
				SET_ENTITY_HEADING(grid_box[temp_row][temp_col].box_object, 0.0)
				SET_ENTITY_COLLISION(grid_box[temp_row][temp_col].box_object, FALSE)
				FREEZE_ENTITY_POSITION(grid_box[temp_row][temp_col].box_object, TRUE)
				QUB3D_SET_CUBE_COLOR(grid_box[temp_row][temp_col].box_object, grid_box[temp_row][temp_col].this_box_colour)
			ENDIF
						
			grid_box[temp_row][temp_col].is_box_on_grid = TRUE
			
			// set this grids spaces status		
			block_status[temp_row][temp_col] = REMAINS
		ELSE
			// create a particle effect here showing block getting deleted?
			QUB3D_TRIGGER_EFFECT(falling_box.a_pos, falling_box.block_a_colour, qub3d_ptfx_explosion)
			// play audio
			soundId = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(soundId, exploding_block_sfx)
			SET_VARIABLE_ON_SOUND(soundId, audioNumBlocksCleared, blocks_cleared)
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_a_position_in_col))
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_a_position_in_row))
			RELEASE_SOUND_ID(soundId)
			
			IF !bRemovedFromPlay
			AND current_level < 20
				bRemovedFromPlay = TRUE
			ENDIF
		ENDIF
		// remove the falling block
		DELETE_OBJECT ( falling_box.falling_box_a)
		falling_box.is_box_a_on_grid = FALSE
	ENDIF

	
	IF fix_b_in_current_pos

		// check if it is on upper level
		IF NOT (falling_box.b_pos.z = top_z)
			temp_col = falling_box.block_b_position_in_col
			temp_row = falling_box.block_b_position_in_row
			
			grid_box[temp_row][temp_col].box_object_pos = <<falling_box.b_pos.x, falling_box.b_pos.y, bottom_z>>
			grid_box[temp_row][temp_col].this_box_colour = falling_box.block_b_colour
			
			IF NOT DOES_ENTITY_EXIST(grid_box[temp_row][temp_col].box_object)
				QUB3d_CREATE_OBJECT(grid_box[temp_row][temp_col].box_object, box_model, grid_box[temp_row][temp_col].box_object_pos)
			ENDIF

			IF DOES_ENTITY_EXIST(grid_box[temp_row][temp_col].box_object)
				SET_ENTITY_HEADING(grid_box[temp_row][temp_col].box_object, 0.0)
				SET_ENTITY_COLLISION(grid_box[temp_row][temp_col].box_object, FALSE)
				FREEZE_ENTITY_POSITION(grid_box[temp_row][temp_col].box_object, TRUE)
				QUB3D_SET_CUBE_COLOR(grid_box[temp_row][temp_col].box_object, grid_box[temp_row][temp_col].this_box_colour)
			ENDIF
			
			grid_box[temp_row][temp_col].is_box_on_grid = TRUE
			
			// set this grids spaces status		
			block_status[temp_row][temp_col] = REMAINS
		ELSE
			// create a particle effect here showing block getting deleted?

			QUB3D_TRIGGER_EFFECT(falling_box.b_pos, falling_box.block_b_colour, qub3d_ptfx_explosion)
			// play audio
			soundId=GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(soundId, exploding_block_sfx)
			SET_VARIABLE_ON_SOUND(soundId, audioNumBlocksCleared, blocks_cleared)
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_b_position_in_col))
			SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_b_position_in_row))
			RELEASE_SOUND_ID(soundId)
			
			IF !bRemovedFromPlay
			AND current_level < 20
				bRemovedFromPlay = TRUE
			ENDIF

		ENDIF
		// remove the falling block
		DELETE_OBJECT ( falling_box.falling_box_b)
		falling_box.is_box_b_on_grid = FALSE		
	ENDIF
	
	IF NOT is_game_over
		IF falling_box.is_box_a_on_grid
			IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
				// safety chacks on z
				IF new_a_relative_pos = UPPER_BLOCK
					falling_box.a_pos.z = top_z
				ELSE
					falling_box.a_pos.z = bottom_z
				ENDIF
				SET_ENTITY_COORDS(falling_box.falling_box_a, falling_box.a_pos)
				falling_box.block_a_is = new_a_relative_pos
				
			ENDIF
		ENDIF
		
		IF falling_box.is_box_b_on_grid
			IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
				// safety chacks on z
				IF new_b_relative_pos = UPPER_BLOCK
					falling_box.b_pos.z = top_z
				ELSE
					falling_box.b_pos.z = bottom_z
				ENDIF
				SET_ENTITY_COORDS(falling_box.falling_box_b, falling_box.b_pos)
				falling_box.block_b_is = new_b_relative_pos

			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
			DELETE_OBJECT (falling_box.falling_box_a)
		ENDIF
		falling_box.is_box_a_on_grid = FALSE
		
		IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
			DELETE_OBJECT(falling_box.falling_box_b)
		ENDIF
		falling_box.is_box_b_on_grid = FALSE	
	ENDIF
	
ENDPROC

PROC FALLING_BLOCK_CONTROLLER()
	
	// determine if enough time has past to move the blocks down	
	current_time = TO_FLOAT(GET_GAME_TIMER())
	
	IF slow_down_powerup_in_effect
		SLOW_DOWN_POWER_UP_ACTION()
	ELSE
		// time since block last moved
		time_difference = current_time - time_started
	ENDIF
	
	/// if enough time has passed to move the blocks, need to use to_float in order to have a mulitplier less than 1	
	IF NOT is_player_forcing_blocks_speed_up // flag gets set in player input checks to see if player pressing down
		IF time_difference >= (time_between_block_movement - blocks_fall_speed_mulitplier)
			/// determine the new postions for the blocks or the rotations required		
			UPDATE_FALLING_BOX_POSITION()
			time_started = TO_FLOAT(GET_GAME_TIMER())
		ENDIF
	ELSE	
	
		// Keep player responsiveness.
		INT iForced = 1
		IF slow_down_powerup_in_effect
			iForced = 2
		ENDIF
	
		///   dont use the multiplier
		IF (time_difference * iForced) >= time_between_block_movement
			/// determine the new postions for the blocks or the rotations required		
			UPDATE_FALLING_BOX_POSITION()
			time_started = TO_FLOAT(GET_GAME_TIMER())
		ENDIF
	ENDIF
		
	/// check for both blocks finishing moving
	IF NOT falling_box.is_box_a_on_grid
	AND NOT falling_box.is_box_b_on_grid
		is_a_box_in_motion = FALSE
	ELSE
		/// check for  player input if not in tutorial mode
		IF NOT is_this_tutorial
			CHECK_FOR_PLAYER_INPUT()
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL READY_TO_CREATE_NEXT_BLOCK()

	// stop the update and procede to deletion phase
	IF NOT CHECK_FOR_DELETION()
		RETURN FALSE
	ENDIF
		
	blocks_cleared = 0	//Reset the number of blocks cleared, used for audio

	RETURN TRUE
ENDFUNC

/////////////////////////////////
////// **** TUTORIAL **** ///////
/////////////////////////////////
PROC INITIALISE_THE_TUTORIAL()
	// PURPOSE: sets up the variables to be used in the tutorial
	current_tutorial_rotation = 0
	
	// left and right rots
	tutorial_rotation_progression[0] = PRESSED_NOTHING // Needs to drop down
	tutorial_rotation_progression[1] = PRESSED_LEFT
	tutorial_rotation_progression[2] = PRESSED_LEFT
	tutorial_rotation_progression[3] = PRESSED_RIGHT
	tutorial_rotation_progression[4] = PRESSED_RIGHT
	tutorial_rotation_progression[5] = PRESSED_RIGHT
	tutorial_rotation_progression[6] = PRESSED_LEFT
	tutorial_rotation_progression[7] = PRESSED_LEFT
	
	// vertical rots
	tutorial_rotation_progression[8] = PRESSED_NOTHING
	tutorial_rotation_progression[9] = PRESSED_LEFT
	tutorial_rotation_progression[10] = PRESSED_NOTHING
	tutorial_rotation_progression[11] = PRESSED_RIGHT
	tutorial_rotation_progression[12] = PRESSED_NOTHING
	tutorial_rotation_progression[13] = PRESSED_RIGHT
	tutorial_rotation_progression[14] = PRESSED_LEFT
	tutorial_rotation_progression[15] = PRESSED_LEFT
	tutorial_rotation_progression[16] = PRESSED_RIGHT
	tutorial_rotation_progression[17] = PRESSED_NOTHING	
	tutorial_rotation_progression[18] = PRESSED_LEFT
	tutorial_rotation_progression[19] = PRESSED_NOTHING
	
	is_a_box_in_motion 					= FALSE
	have_blocks_been_set_up_for_demo 	= FALSE
	direction_pressed 					= PRESSED_NOTHING
	current_tutorial_stage 				= SHOWING_GAME_OVERVIEW
	
	time_started = TO_FLOAT(GET_GAME_TIMER())
	
ENDPROC

PROC FIND_ROT_TUT_SHIFT_VARIABLES()
	a_col_shift 		= 0
	a_row_shift			= 0

	new_a_relative_pos = falling_box.block_a_is 
	new_b_relative_pos = falling_box.block_b_is
		
	//// calculate the shift depending on position
	SWITCH falling_box.block_a_is
		CASE LEFT_BLOCK
			// a is 1 col to left of b
			IF direction_pressed = PRESSED_LEFT
				// a doesnt move
				a_col_shift = 0
													
				new_a_relative_pos = LOWER_BLOCK
				new_b_relative_pos = UPPER_BLOCK
				
			ELSE
				IF direction_pressed = PRESSED_RIGHT
					// shift a up 1 columns and up on z
					a_col_shift = 1
					
					new_a_relative_pos = UPPER_BLOCK
					new_b_relative_pos = LOWER_BLOCK
				ENDIF
			ENDIF
		BREAK
		
		CASE RIGHT_BLOCK
			// a is 1 col to right of b
			IF direction_pressed = PRESSED_LEFT
				// shift a down 1 columns and up on z
				a_col_shift = -1

				new_a_relative_pos = UPPER_BLOCK
				new_b_relative_pos = LOWER_BLOCK
				
			ELSE
				IF direction_pressed = PRESSED_RIGHT
					// shift a up a column
					a_col_shift = 0
					
					new_a_relative_pos = LOWER_BLOCK
					new_b_relative_pos = UPPER_BLOCK
				ENDIF
			ENDIF				
		BREAK
		
		CASE TOP_BLOCK
			// a is 1 row above b
			IF direction_pressed = PRESSED_LEFT
				// shift a down a column
				a_col_shift = -1
				// shift b down a column
			ELSE
				IF direction_pressed = PRESSED_RIGHT
					// shift a up a column
					a_col_shift = 1
					// shift b up a column
				ENDIF
			ENDIF
			
				
			new_a_relative_pos = TOP_BLOCK
			new_b_relative_pos = BOTTOM_BLOCK
			
		BREAK
		
		
		CASE BOTTOM_BLOCK
			// a is 1 row below b
			IF direction_pressed = PRESSED_LEFT
				// shift a down a column
				a_col_shift = -1
				// shift b down a column
				//b_col_shift = -1
			ELSE
				IF direction_pressed = PRESSED_RIGHT
					// shift a up a column
					a_col_shift = 1
					// shift b up a column
					//b_col_shift = 1
				ENDIF
			ENDIF
			
			
			new_a_relative_pos = BOTTOM_BLOCK
			new_b_relative_pos = TOP_BLOCK
		BREAK
		
		CASE UPPER_BLOCK
			// a must be above b on z
			IF direction_pressed = PRESSED_LEFT
				// shift a down 2 columns and down on z
				a_col_shift = -1
				//a_depth_shift = -1
				// shift b down 1 column
				//b_col_shift = 0

				new_a_relative_pos = LEFT_BLOCK
				new_b_relative_pos = RIGHT_BLOCK
				
			ELSE
				IF direction_pressed = PRESSED_RIGHT
					// shift a up 2 columns and down on z
					a_col_shift = 1
					//a_depth_shift = -1
					// shift b up 1 column
					//b_col_shift = 0
					
					new_a_relative_pos = RIGHT_BLOCK 
					new_b_relative_pos = LEFT_BLOCK
				ENDIF
			ENDIF
		BREAK
		
		CASE LOWER_BLOCK
			// a is underneath b on z
			IF direction_pressed = PRESSED_LEFT
				//  shift a down 1 column
				a_col_shift = 0
				// shift b down 2 columns and down on z
				//b_col_shift = -1
				//b_depth_shift = -1
				
				
				new_a_relative_pos = RIGHT_BLOCK 
				new_b_relative_pos = LEFT_BLOCK
				
			ELSE
				IF direction_pressed = PRESSED_RIGHT
					//  shift a up 1 column
					a_col_shift = 0
					// shift b up 2 columns and down on z
					//b_col_shift = 1
					//b_depth_shift = -1
					
					new_a_relative_pos = LEFT_BLOCK
					new_b_relative_pos = RIGHT_BLOCK
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
						
ENDPROC 

PROC SHOW_GAME_OVERVIEW()

	
	SWITCH (powerDemoState)
	
		CASE PD_SETUP_GRID
			CREATE_A_BOMBABLE_GRID()
			
			PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
			has_a_power_up_been_selected = TRUE
			
			PRINT_HELP("PZ_TUT_01", 3500)
			
			time_started = TO_FLOAT(GET_GAME_TIMER())
			
			powerDemoState = PD_HELP_UPDATE_00
		BREAK
		
		CASE PD_HELP_UPDATE_00
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 3500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PRINT_HELP("PZ_OVR_01", 6500)
				time_started = TO_FLOAT(GET_GAME_TIMER())
				powerDemoState = PD_HELP_UPDATE_01
			ENDIF
		BREAK
		
		CASE PD_HELP_UPDATE_01
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 6500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PRINT_HELP("PZ_OVR_02", 6500)
				time_started = TO_FLOAT(GET_GAME_TIMER())
				powerDemoState = PD_HELP_UPDATE_02
			ENDIF
		BREAK
		
		CASE PD_HELP_UPDATE_02
		
			current_time = TO_FLOAT(GET_GAME_TIMER())
			time_difference = current_time - time_started
		
			IF time_difference >= 6500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				time_started = TO_FLOAT(GET_GAME_TIMER())
				
				PRINT_HELP("PZ_OVR_03", 6500)
				INITIALISE_THE_GRID()
				
				QUB3D_CREATE_OBJECT(falling_box.falling_box_a, box_model, grid_box[min_row][2].box_object_pos)
				QUB3D_SET_CUBE_COLOR(falling_box.falling_box_a, INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours)))
				QUB3D_CREATE_OBJECT(falling_box.falling_box_b, box_model, grid_box[min_row][3].box_object_pos)
				QUB3D_SET_CUBE_COLOR(falling_box.falling_box_b, INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours)))
				
				powerDemoState = PD_NEXT_DEMO
			ENDIF
		
		BREAK
		
		CASE PD_NEXT_DEMO
		
			current_time = TO_FLOAT(GET_GAME_TIMER())
			time_difference = current_time - time_started
		
			IF (time_difference%1000) >= 500
				IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
				AND IS_ENTITY_VISIBLE(falling_box.falling_box_a)
					SET_ENTITY_VISIBLE(falling_box.falling_box_a, FALSE)
					SET_ENTITY_VISIBLE(falling_box.falling_box_b, FALSE)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
				AND NOT IS_ENTITY_VISIBLE(falling_box.falling_box_a)
					SET_ENTITY_VISIBLE(falling_box.falling_box_a, TRUE)
					SET_ENTITY_VISIBLE(falling_box.falling_box_b, TRUE)
				ENDIF
			ENDIF
			
			IF time_difference >= 6500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				current_tutorial_stage = SHOWING_ROTATION_DEMO
				
				DELETE_OBJECT(falling_box.falling_box_a)
				DELETE_OBJECT(falling_box.falling_box_b)
				
				INITIALISE_THE_GRID()
				blocks_cleared = 0
				powerDemoState = PD_SETUP_GRID
			ENDIF
		BREAK
	
	ENDSWITCH



ENDPROC

PROC SHOW_ROTATION_DEMO()

	/// PURPOSE: shows the possible rotations a block can pass through
	INT number_of_rots_in_tut = 20
	INT time_between_block_movement_for_tut = 600
	
	// Skipping step
	IF QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
		// reset falling flag
		is_a_box_in_motion 					= FALSE
		have_blocks_been_set_up_for_demo 	= FALSE
		time_started = TO_FLOAT(GET_GAME_TIMER())
		// move onto next part of the tutorial
		current_tutorial_stage = SHOWING_CUBE_CLEAR
	ENDIF
	
	IF is_a_box_in_motion
		// no matter its state always three options for rotations
		// left right down
		
		current_time = TO_FLOAT(GET_GAME_TIMER())
		// time since block last moved
		time_difference = current_time - time_started			
		
		// Demo the speed up effect
		IF current_tutorial_rotation >= number_of_rots_in_tut

			IF current_tutorial_rotation = number_of_rots_in_tut
				
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					PRINT_HELP("PZ_ROT_02", 6500)
				ELSE
					PRINT_HELP("PZ_ROT_02_PC", 6500)
				ENDIF
					
				current_tutorial_rotation++
			ENDIF
		
			time_between_block_movement_for_tut = 250
		ENDIF
		
		IF time_difference >= time_between_block_movement_for_tut 
			
			IF current_tutorial_rotation < number_of_rots_in_tut
				direction_pressed = tutorial_rotation_progression[current_tutorial_rotation]
		
				IF NOT (direction_pressed = PRESSED_NOTHING)
					FIND_ROT_TUT_SHIFT_VARIABLES()					
					/// determine the new postions for the blocks or the rotations required		
					CAN_BOX_BE_MOVED()
				ELSE
					// drop it down one row
					UPDATE_FALLING_BOX_POSITION()
				ENDIF
				
				// increase current rotation
				current_tutorial_rotation++
															
				// find time of last move			
				time_started = TO_FLOAT(GET_GAME_TIMER())
			ELSE
						
				// Falling blocks are deleted once they are secured in place.
				IF DOES_ENTITY_EXIST(falling_box.falling_box_a)
				AND DOES_ENTITY_EXIST(falling_box.falling_box_b)
					// drop it down one row
					UPDATE_FALLING_BOX_POSITION()
										
					// find time of last move			
					time_started = TO_FLOAT(GET_GAME_TIMER())
				ELSE // Blocks have been fixed
					//IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF time_difference >= 6500
						// reset falling flag
						is_a_box_in_motion 					= FALSE
						have_blocks_been_set_up_for_demo 	= FALSE
						time_started = TO_FLOAT(GET_GAME_TIMER())
						// move onto next part of the tutorial
						current_tutorial_stage = SHOWING_CUBE_CLEAR
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ELSE		
		// create the box
		CREATE_RANDOM_BLOCK()
		CLEAR_HELP()
		
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			PRINT_HELP("PZ_ROT_01", 6500)
		ELSE
			PRINT_HELP("PZ_ROT_01_PC", 6500)
		ENDIF
				
	ENDIF
ENDPROC


PROC SHOW_SLOW_POWER()

	/// PURPOSE: shows the possible rotations a block can pass through
	INT number_of_rots_in_tut = 5
	INT time_between_block_movement_for_tut = 900
	
	IF is_a_box_in_motion
		// no matter its state always three options for rotations
		// left right down
		
		current_time = TO_FLOAT(GET_GAME_TIMER())
		// time since block last moved
		time_difference = current_time - time_started			
				
		IF time_difference >= time_between_block_movement_for_tut 
						
			IF current_tutorial_rotation < number_of_rots_in_tut
				direction_pressed = tutorial_rotation_progression[current_tutorial_rotation]
		
				IF current_tutorial_rotation = 0
					PRINT_HELP("PZ_SLOW_01", 6500)
				ENDIF
		
				IF current_tutorial_rotation = 3
					PRINT_HELP("PZ_SLOW_02", 6500)
				ENDIF
		
				IF NOT (direction_pressed = PRESSED_NOTHING)
					FIND_ROT_TUT_SHIFT_VARIABLES()					
					/// determine the new postions for the blocks or the rotations required		
					CAN_BOX_BE_MOVED()
				ELSE
					// drop it down one row
					UPDATE_FALLING_BOX_POSITION()
				ENDIF
				
				// increase current rotation
				current_tutorial_rotation++
											
				// find time of last move			
				time_started = TO_FLOAT(GET_GAME_TIMER())
			ELSE
				IF time_difference >= 6500
				OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
					QUIT_TUTORIAL()	
				ENDIF
			ENDIF
		ENDIF
	ELSE		
		// create the box
		CREATE_RANDOM_BLOCK()
		slow_down_powerup_in_effect = TRUE
		SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"Slowmo",1.0)
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC SHOW_CUBE_CLEAR()
	// PURPOSE: shows how the blocks get removed when placed in such a way that 4 faces 
	//			are touching. Will need to create a situation where the blocks are removed
	//			shifted down and removed again, preferably in different patterns.
	
	BOARD_GRID_CONTENTS temp_colour
	BOOL should_block_be_added

	IF NOT have_blocks_been_set_up_for_demo
		// create the starting grid here
		// block should stay fixed here, update this grid space with info	
		
		INITIALISE_THE_GRID()	
	
		CLEAR_HELP()
		PRINT_HELP("PZ_REM_0", 6000)
	
		FOR current_row = min_row TO  max_row
			//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 6781")
			// initialise the starting column
			FOR current_column = min_col TO max_col
				
				should_block_be_added = FALSE
				
				// bottom row
				IF current_row = max_row
					IF current_column = 0
						// blue
						temp_colour = BLUE
						should_block_be_added = TRUE
					ELSE
						IF current_column = 1
						OR current_column = 2
							// red
							temp_colour = RED
							should_block_be_added = TRUE
						ELSE
							IF current_column < 5
							//green
								temp_colour = GREEN
								should_block_be_added = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE	
					IF current_row = max_row - 1
						IF current_column < 2
							// blue
							temp_colour = BLUE
							should_block_be_added = TRUE
						ELSE
							IF current_column < 4						
								// red
								temp_colour = RED
								should_block_be_added = TRUE
							ELSE
								IF current_column = 4
									// green
									temp_colour = GREEN
									should_block_be_added = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF current_row = max_row - 2
							IF current_column = 2
								// green
								temp_colour = GREEN
								should_block_be_added = TRUE
							ELSE 
								IF current_column = 3
									// blue
									temp_colour = BLUE
									should_block_be_added = TRUE
								ENDIF
							ENDIF
						ELSE
							IF current_row = max_row - 3
								IF current_column = 2
									// blue
									temp_colour = BLUE
									should_block_be_added = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				
				IF should_block_be_added
					// create the block
					IF NOT DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
						QUB3D_CREATE_OBJECT(grid_box[current_row][current_column].box_object, box_model, grid_box[current_row][current_column].box_object_pos)
					ENDIF
					
					IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
						SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, 0.0)
						SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
						FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
									
						/// update flags in the struct
						grid_box[current_row][current_column].this_box_colour = temp_colour
						QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, temp_colour)
					ENDIF
					
					grid_box[current_row][current_column].is_box_on_grid = TRUE
					block_status[current_row][current_column] = REMAINS
				ENDIF
	
			ENDFOR

		ENDFOR
		
		// update the flag
		have_blocks_been_set_up_for_demo = TRUE
		
	ELSE
		// stop the update and procede to deletion phase
		current_time = TO_FLOAT(GET_GAME_TIMER())
		// time since block last moved
		time_difference = current_time - time_started
		IF 	time_difference >= 5500
			
			IF CHECK_FOR_DELETION()	
				SWITCH (powerDemoState)
				
					CASE PD_SETUP_GRID
						PRINT_HELP("PZ_REM_01", 5500)

						time_started = TO_FLOAT(GET_GAME_TIMER())
						powerDemoState = PD_HELP_UPDATE_00
					BREAK
					
					CASE PD_HELP_UPDATE_00
										
						IF time_difference >= 5500
						OR QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
							PRINT_HELP("PZ_REM_02", 5500)
							
							time_started = TO_FLOAT(GET_GAME_TIMER())
							powerDemoState = PD_NEXT_DEMO
						ENDIF
					BREAK
																	
					CASE PD_NEXT_DEMO

						IF time_difference >= 6500
						OR QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
						 	powerDemoState = PD_SETUP_GRID
							blocks_cleared = 0
							CLEAR_HELP()
							have_blocks_been_set_up_for_demo 	= FALSE
							time_started = TO_FLOAT(GET_GAME_TIMER())
							current_tutorial_stage 				= SHOWING_BLOCK_REMOVAL
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		
		ENDIF
		
		IF QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
		 	powerDemoState = PD_SETUP_GRID
			blocks_cleared = 0
			CLEAR_HELP()
			have_blocks_been_set_up_for_demo 	= FALSE
			time_started = TO_FLOAT(GET_GAME_TIMER())
			current_tutorial_stage 				= SHOWING_BLOCK_REMOVAL
		ENDIF

	ENDIF
	
ENDPROC

PROC SHOW_BLOCK_REMOVAL()

	IF NOT have_blocks_been_set_up_for_demo	
	
		// create the starting grid here
		// block should stay fixed here, update this grid space with info	
		
		INITIALISE_THE_GRID()	
	
		CLEAR_HELP()
	
		// initialise the starting column
		FOR current_column = min_col TO max_col
	
			// set the initial values in the grid
			IF DOES_ENTITY_EXIST(grid_box[max_row][current_column].box_object)
				DELETE_OBJECT(grid_box[max_row][current_column].box_object)
			ENDIF
			
			// select a random colour for the box
			grid_box[max_row][current_column].this_box_colour = INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours))
			// create the block
			QUB3D_CREATE_OBJECT(grid_box[max_row][current_column].box_object, box_model, grid_box[max_row][current_column].box_object_pos)

			IF DOES_ENTITY_EXIST(grid_box[max_row][current_column].box_object)
				SET_ENTITY_HEADING(grid_box[max_row][current_column].box_object, 0.0)
				SET_ENTITY_COLLISION(grid_box[max_row][current_column].box_object, FALSE)
				FREEZE_ENTITY_POSITION(grid_box[max_row][current_column].box_object, TRUE)
				QUB3D_SET_CUBE_COLOR(grid_box[max_row][current_column].box_object, grid_box[max_row][current_column].this_box_colour)
			ENDIF
						
			/// update flags in the struct
			grid_box[max_row][current_column].is_box_on_grid = TRUE

			block_status[max_row][current_column] = REMAINS
	
		ENDFOR
		
		falling_box.a_pos = grid_box[max_row - 1][2].box_object_pos
		falling_box.a_pos.z = bottom_z
		
		falling_box.b_pos = grid_box[max_row - 1][2].box_object_pos
		falling_box.b_pos.z = top_z
				
		/// create the box
		IF NOT DOES_ENTITY_EXIST(falling_box.falling_box_a)
			QUB3D_CREATE_OBJECT(falling_box.falling_box_a, box_model, falling_box.a_pos)
			QUB3D_SET_CUBE_COLOR(falling_box.falling_box_a, INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours)))
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(falling_box.falling_box_b)
			QUB3D_CREATE_OBJECT(falling_box.falling_box_b, box_model, falling_box.b_pos)
			QUB3D_SET_CUBE_COLOR(falling_box.falling_box_b, INT_TO_ENUM(BOARD_GRID_CONTENTS, GET_RANDOM_INT_IN_RANGE(0, max_number_of_colours)))
		ENDIF
		
		CLEAR_HELP()
		PRINT_HELP("PZ_REM_SINGLE", 6500)
		
		time_started = TO_FLOAT(GET_GAME_TIMER())
		
		have_blocks_been_set_up_for_demo = TRUE
	ELSE
	
		// have it based on a time delay
		current_time = TO_FLOAT(GET_GAME_TIMER())			
		// time since block last moved
		time_difference = current_time - time_started	
	
		IF time_difference >= 4500
		OR QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
			IF DOES_ENTITY_EXIST(falling_box.falling_box_b)
				DELETE_OBJECT(falling_box.falling_box_b)
				
				QUB3D_TRIGGER_EFFECT(falling_box.b_pos, falling_box.block_b_colour, qub3d_ptfx_explosion)
				// play audio
				soundId = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(soundId, exploding_block_sfx)
				SET_VARIABLE_ON_SOUND(soundId, audioNumBlocksCleared, blocks_cleared)
				SET_VARIABLE_ON_SOUND(soundId, audioPanVarX, PZ_GET_AUDIO_PAN_X(falling_box.block_b_position_in_col))
				SET_VARIABLE_ON_SOUND(soundId, audioPanVarY, PZ_GET_AUDIO_PAN_Y(falling_box.block_b_position_in_row))
				RELEASE_SOUND_ID(soundId)
				
				falling_box.is_box_b_on_grid = FALSE	
			ENDIF
					
			IF time_difference >= 6500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
				time_started = TO_FLOAT(GET_GAME_TIMER())
				powerDemoState = PD_SETUP_GRID
				blocks_cleared = 0
				have_blocks_been_set_up_for_demo 	= FALSE
				is_a_box_in_motion 					= FALSE
				current_tutorial_stage 				= SHOWING_SPECIAL_BLOCK
			ENDIF
			
		ENDIF
	
	ENDIF
ENDPROC

PROC SHOW_SPECIAL_BLOCK()
	
	BOARD_GRID_CONTENTS temp_colour
	BOOL should_block_be_added
	
	IF NOT have_blocks_been_set_up_for_demo
		INITIALISE_THE_GRID()
		CLEAR_PRINTS()
		PRINT_HELP("PZ_BIG_01", 6500)
		FOR current_column = 0 TO (number_of_columns -1)
			// initialise the starting column
			FOR current_row = min_row TO (number_of_rows-1)	
				
				should_block_be_added = FALSE
				
				// bottom row
				IF current_column = 0					
					IF current_row = max_row
					OR current_row = max_row -1
						// red
						temp_colour = RED
						should_block_be_added = TRUE
					ENDIF
					
				ELSE	
					IF current_column = 1
						IF current_row = max_row
						OR current_row = max_row - 1
							// red
							temp_colour = RED
							should_block_be_added = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				
				
				IF should_block_be_added
					// create the block
					IF NOT DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
						QUB3D_CREATE_OBJECT(grid_box[current_row][current_column].box_object, box_model, grid_box[current_row][current_column].box_object_pos)
					ENDIF
					
					IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
						SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, 0.0)
						SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
						FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
									
						/// update flags in the struct
						grid_box[current_row][current_column].this_box_colour = temp_colour
						QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, temp_colour)
					ENDIF
					
					grid_box[current_row][current_column].is_box_on_grid = TRUE
					block_status[current_row][current_column] = REMAINS
				ENDIF

			ENDFOR
			// increase row counter
		ENDFOR
		
		have_blocks_been_set_up_for_demo = TRUE
		
		time_started = TO_FLOAT(GET_GAME_TIMER())			
	ELSE
		// have it based on a time delay
		current_time = TO_FLOAT(GET_GAME_TIMER())			
		// time since block last moved
		time_difference = current_time - time_started			
		IF time_difference >= 6500
			PRINT_HELP("PZ_BIG_02", 4000)
		ENDIF
		
		IF time_difference >= 9000
		OR QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
			// stop the update and procede to deletion phase
			CHECK_FOR_2_X_2_OF_SAME_COLOUR()
			// move to next part of the tutorial
			current_tutorial_stage 				= SHOWING_SPECIAL_BLOCK_REMOVAL
			have_blocks_been_set_up_for_demo 	= FALSE
			//current_tut_text 					= 0
			time_started = TO_FLOAT(GET_GAME_TIMER())
		ENDIF
	ENDIF

ENDPROC


PROC SHOW_SPECIAL_BLOCK_REMOVAL()
	
	IF NOT have_blocks_been_set_up_for_demo
		/// determine if enough time has passed in order to create the other blocks
		// have it based on a time delay
		current_time = TO_FLOAT(GET_GAME_TIMER())
		// time since block last moved
		time_difference = current_time - time_started
		IF time_difference >= 1000
			current_column = 0

			// update the flag
			have_blocks_been_set_up_for_demo = TRUE
			time_started = TO_FLOAT(GET_GAME_TIMER())
		ENDIF
										
	ELSE
		// have it based on a time delay
		current_time = TO_FLOAT(GET_GAME_TIMER())			
		// time since block last moved
		time_difference = current_time - time_started			
		IF time_difference >= 1000
			// stop the update and procede to deletion phase			
		
			IF CHECK_FOR_DELETION()
				have_blocks_been_set_up_for_demo = FALSE			
				number_of_powerup_objs_to_display = 0
				number_of_power_uses = 0
				// move to next part of the tutorial
				current_tutorial_stage = SHOWING_POWER_UP
				time_started = TO_FLOAT(GET_GAME_TIMER())
			ENDIF

		ENDIF
	ENDIF
	
ENDPROC


PROC TUTORIAL_POWER_BAR_OBJECT_UPDATER()
	/// PURPOSE: adds objects to the power bar as the player removes the larger blocks from the game grid
	
	//INT alpha_value	
	INT non_active_alpha_value = 90
	INT active_alpha_value = 250
	
	//INT eraser	
	INT eraser_r
	INT eraser_g
	INT eraser_b 
	
	INT bomb_r
	INT bomb_g 
	INT bomb_b
	
	INT random_r
	INT random_g
	INT random_b
	
	INT slow_r
	INT slow_g
	INT slow_b
		
	IF IS_XBOX_PLATFORM()
	OR IS_PC_VERSION()
		//		Red (B button) = 167, 4, 0
		eraser_r = 167
		eraser_g = 4
		eraser_b = 0
		//		Green (A button) = 77, 151, 5
		bomb_r = 77
		bomb_g = 151
		bomb_b = 5
		//		Blue (X button) = 24, 72, 170
		random_r = 24		
		random_g = 72
		random_b = 170
		//		Yellow (Y button) = 179, 147, 0
		slow_r = 179
		slow_g = 147
		 slow_b = 0
	ELSE
		//		Red (circle button) = 204  43  71
		eraser_r = 204
		eraser_g = 43
		eraser_b = 71
		//		blue (cross button) = 108  130  187
		bomb_r = 108
		bomb_g = 130
		bomb_b = 187
		//		pink (square button) = 220  136  186
		random_r = 220		
		random_g = 136
		random_b = 186
		//		green (triangle button) = 134  178  135
		slow_r = 134
		slow_g = 178
		slow_b = 135

	ENDIF

	INIT_RGBA_STRUCT(rgbaEraserActive, eraser_r, eraser_g, eraser_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaEraserNonActive, eraser_r, eraser_g, eraser_b, non_active_alpha_value)
	INIT_RGBA_STRUCT(rgbaBombActive, bomb_r, bomb_g, bomb_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaBombNonActive, bomb_r, bomb_g, bomb_b, non_active_alpha_value)
	INIT_RGBA_STRUCT(rgbaRandomActive, random_r, random_g, random_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaRandomNonActive, random_r, random_g, random_b, non_active_alpha_value)
	INIT_RGBA_STRUCT(rgbaSlowActive, slow_r, slow_g, slow_b, active_alpha_value)
	INIT_RGBA_STRUCT(rgbaSlowNonActive, slow_r, slow_g, slow_b, non_active_alpha_value)

	// the bar	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, power_bar_texture, INIT_VECTOR_2D(ig_power_bar_pos_x,  tut_power_bar_pos_y ),  INIT_VECTOR_2D(ig_power_bar_size_x ,  ig_power_bar_size_y), 0.0000, rgbaWhiteOut)
	ENDIF
	
	IF number_of_power_uses > 1
		// dependiing on number_of_powerup_objs_to_display draw a suitably large rectange
		ARCADE_DRAW_PIXELSPACE_RECT (INIT_VECTOR_2D(ig_power_bar_inner_4_pos_x, tut_power_bar_inner_1_pos_y), INIT_VECTOR_2D(ig_power_bar_inner_4_size_x, ig_power_bar_inner_1_size_y), rgbaNearOpaque)	
	ENDIF
	
	
	// Display the text.
	// multiplyer
	
	// multiplyer
	IF (number_of_power_uses > 0 )
		ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "LET_X", 
		INIT_VECTOR_2D(ig_power_uses_txt_pos_x, tut_power_txt_pos_y + 4),
		INIT_VECTOR_2D(20, 20),
		0.0, rgbaNearOpaque)
		
		IF number_of_power_uses = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "NO_BIG_1", 
				INIT_VECTOR_2D(ig_power_uses_txt_pos_x + 20, tut_power_txt_pos_y + 4),
				INIT_VECTOR_2D(20, 20),
				0.0, rgbaNearOpaque)
		ENDIF
		
		IF number_of_power_uses = 2
			ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "NO_BIG_2", 
				INIT_VECTOR_2D(ig_power_uses_txt_pos_x + 20, tut_power_txt_pos_y + 4),
				INIT_VECTOR_2D(20, 20),
				0.0, rgbaNearOpaque)
		ENDIF
		
		IF number_of_power_uses = 3
			ARCADE_DRAW_PIXELSPACE_SPRITE(high_score_textures, "NO_BIG_3",
				INIT_VECTOR_2D(ig_power_uses_txt_pos_x + 20, tut_power_txt_pos_y + 4),
				INIT_VECTOR_2D(20, 20),
				0.0, rgbaNearOpaque)
		ENDIF
	ENDIF
		

	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_SPECIAL", 
			INIT_VECTOR_2D(ig_special_txt_pos_x, tut_special_txt_pos_y),
			INIT_VECTOR_2D(ig_special_txt_size_x, ig_special_txt_size_y),
			0.0, rgbaWhiteOut)
			
		ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, "LABEL_POWER", 
			INIT_VECTOR_2D(ig_power_txt_pos_x, tut_power_txt_pos_y),
			INIT_VECTOR_2D(ig_power_txt_size_x, ig_power_txt_size_y),
			0.0, rgbaWhiteOut)
	ENDIF
	
	// sprites (apha dependant on if its avail, rgb based off of buttons
	// if none available all are dimmed
	ig_special_2nd_power_pos_x = ig_special_1st_power_pos_x + ig_special_1st_power_size_x
	ig_special_3rd_power_pos_x = ig_special_2nd_power_pos_x + ig_special_1st_power_size_x
	ig_special_4th_power_pos_x = ig_special_3rd_power_pos_x + ig_special_1st_power_size_x
	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(puzzle_textures)
		IF number_of_power_uses = 0				
			ARCADE_DRAW_PIXELSPACE_SPRITE (puzzle_textures, special_texture_1, INIT_VECTOR_2D(ig_special_1st_power_pos_x, tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaEraserNonActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_2, INIT_VECTOR_2D(ig_special_2nd_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaBombNonActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_3, INIT_VECTOR_2D(ig_special_3rd_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaRandomNonActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_4, INIT_VECTOR_2D(ig_special_4th_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaSlowNonActive)
		ELSE
			// if any avilaable all are lit
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_1, INIT_VECTOR_2D(ig_special_1st_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_1st_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaEraserActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_2, INIT_VECTOR_2D(ig_special_2nd_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_2nd_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaBombActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_3, INIT_VECTOR_2D(ig_special_3rd_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_3rd_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaRandomActive)
			ARCADE_DRAW_PIXELSPACE_SPRITE(puzzle_textures, special_texture_4, INIT_VECTOR_2D(ig_special_4th_power_pos_x,  tut_special_icons_pos_y),  INIT_VECTOR_2D(ig_special_4th_power_size_x ,  ig_special_1st_power_size_y), 0.0000, rgbaSlowActive)
		ENDIF	
	ENDIF

	BOOL change_in_number_to_be_displayed
	INT number_already_added
		
	IF NOT (number_of_power_uses = max_number_of_power_uses)
		IF number_of_powerup_objs_to_display > number_to_fill_power_bar
			number_of_powerup_objs_to_display = number_to_fill_power_bar
		ENDIF
	ELSE
		number_of_powerup_objs_to_display = 0
	ENDIF
	
	IF number_of_powerup_objs_being_displayed < number_of_powerup_objs_to_display
		change_in_number_to_be_displayed = TRUE
	ELSE
		change_in_number_to_be_displayed = FALSE
	ENDIF
	
	/// if the bar isn't already full
	IF NOT (number_of_power_uses = max_number_of_power_uses)//power_bar_full// = FALSE
		// determine if another part of the power bar should be added		
		IF change_in_number_to_be_displayed
			number_of_powerup_objs_being_displayed = 0
			INT loop_counter = 0
			FOR loop_counter = 0 TO (number_to_fill_power_bar - 1)
				// see if this part of the power bar is being displayed
				IF NOT power_up_ready[loop_counter]
					// if we should add another part of the bar
					IF loop_counter < number_of_powerup_objs_to_display
						// update the flags
						power_up_ready[loop_counter] = TRUE
					ENDIF
				
				ENDIF
				
				number_of_powerup_objs_being_displayed++
			ENDFOR
		
			// is there enough in power bar to allow player to access power ups
			IF number_of_powerup_objs_being_displayed = number_to_fill_power_bar
						
				number_of_power_uses++
				IF number_of_power_uses > max_number_of_power_uses
					number_of_power_uses = max_number_of_power_uses
				ENDIF

				//determine which array index to add
				number_already_added = number_of_power_uses - 1
				
				// if not currently any on screen
				IF number_already_added <= 0
					number_already_added = 0
				ENDIF

				// play audio
				PLAY_SOUND_FRONTEND(-1, got_powerup_sfx)				

				// reset the power bar 
				FOR loop_counter = 0 TO (number_to_fill_power_bar - 1)
					power_up_ready[loop_counter] = FALSE
				ENDFOR
				
				number_of_powerup_objs_to_display		= 0
				number_of_powerup_objs_being_displayed	= 0				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SHOW_POWER_UP()
	
	TUTORIAL_POWER_BAR_OBJECT_UPDATER()
	
	IF NOT have_blocks_been_set_up_for_demo
		CLEAR_PRINTS()
		PRINT_HELP("PZ_POW_01", 6500)
		have_blocks_been_set_up_for_demo = TRUE
	ENDIF
		
	IF number_of_power_uses = 0 
		// if enough time has passed add another block to the power bar
		current_time = TO_FLOAT(GET_GAME_TIMER())
		// time since block last moved
		time_difference = current_time - time_started			
		IF time_difference >= 1500
			number_of_powerup_objs_to_display++
			// if enough to fill bar move onto next part of tut
			// reset the start timer
			time_started = TO_FLOAT(GET_GAME_TIMER())
		ENDIF
	ELSE
		IF number_of_power_uses < max_number_of_power_uses
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 4000
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				// add another stored power icon 
				
				IF number_of_power_uses = 1
					PRINT_HELP("PZ_POW_02", 4000)
				ENDIF
				
				IF number_of_power_uses = 2
					PRINT_HELP("PZ_POW_03", 4000)
				ENDIF
				
				number_of_powerup_objs_to_display ++// 4
				// reset the start timer
				time_started = TO_FLOAT(GET_GAME_TIMER())
			ENDIF		
		ELSE
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started		
			
			IF time_difference >= 6500
				 PRINT_HELP("PZ_POW_04", 6500)
			ENDIF
			
			IF time_difference >= 12500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_PRESSED()
				CLEAR_PRINTS()
				is_cross_pressed = TRUE	
				number_of_power_uses = 3
				current_tutorial_stage = SHOWING_BOMB_POWER
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SHOW_BOMB_POWER()
	TUTORIAL_POWER_BAR_OBJECT_UPDATER()
	
	IF bomb_powerup_in_effect
		BOMB_POWER_UP_ACTION()
	ENDIF
	
	SWITCH (powerDemoState)
	
		CASE PD_SETUP_GRID
			CREATE_A_BOMBABLE_GRID()
			
			PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
			has_a_power_up_been_selected = TRUE
			
			PRINT_HELP("PZ_POW_BOMB", 3700)
			
			time_started = TO_FLOAT(GET_GAME_TIMER())
			current_time = 0
			
			powerDemoState = PD_HELP_UPDATE_00
		BREAK
		
		CASE PD_HELP_UPDATE_00
		
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 4000
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PRINT_HELP("PZ_POW_BOMB_01", 4000)
				powerDemoState = PD_TRIGGER_POWER
			ENDIF
		
		BREAK
		
		CASE PD_TRIGGER_POWER
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 1000
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
				has_a_power_up_been_selected = TRUE
				time_started = TO_FLOAT(GET_GAME_TIMER())
				bomb_powerup_in_effect = TRUE
				powerDemoState = PD_NEXT_DEMO
			ENDIF
		BREAK
		
		CASE PD_NEXT_DEMO
		
			current_time = TO_FLOAT(GET_GAME_TIMER())
			time_difference = current_time - time_started
		
			IF NOT bomb_powerup_in_effect
				IF time_difference >= 6500
				OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
					blocks_cleared = 0
					INITIALISE_THE_GRID()
					number_of_power_uses = 2
					powerDemoState = PD_SETUP_GRID
					current_tutorial_stage = SHOWING_ERASER_POWER
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH


ENDPROC

PROC SHOW_ERASER_POWER()

	TUTORIAL_POWER_BAR_OBJECT_UPDATER()
	
	IF eraser_powerup_in_effect
		ERASER_POWER_UP_ACTION()
	ENDIF
	
	SWITCH (powerDemoState)
	
		CASE PD_SETUP_GRID
			CREATE_AN_ERASABLE_GRID()
						
			PRINT_HELP("PZ_POW_ERASER", 6500)
			
			time_started = TO_FLOAT(GET_GAME_TIMER())
			current_time = 0
			
			powerDemoState = PD_HELP_UPDATE_00
		BREAK
		
		CASE PD_HELP_UPDATE_00
		
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 4000
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PRINT_HELP("PZ_POW_ERASER_01", 4000)
				powerDemoState = PD_TRIGGER_POWER
			ENDIF
		
		BREAK
		
		CASE PD_TRIGGER_POWER
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 1000
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				eraser_powerup_in_effect = TRUE
				PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
				has_a_power_up_been_selected = TRUE
				time_started = TO_FLOAT(GET_GAME_TIMER())
				powerDemoState = PD_NEXT_DEMO
			ENDIF
		BREAK
		
		CASE PD_NEXT_DEMO
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			time_difference = current_time - time_started
			
			IF NOT eraser_powerup_in_effect
				IF time_difference >= 6500
				OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				//AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					powerDemoState = PD_SETUP_GRID
					current_tutorial_rotation = 0
					blocks_cleared = 0
					number_of_power_uses = 1
					time_started = TO_FLOAT(GET_GAME_TIMER())
					current_tutorial_stage = SHOWING_REMOVAL_POWER
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC


PROC SHOW_COLOUR_REMOVAL_POWER()

	BOOL should_block_be_added
	BOARD_GRID_CONTENTS temp_colour

	TUTORIAL_POWER_BAR_OBJECT_UPDATER()
	
	IF colour_removal_powerup_in_effect
		COLOUR_REMOVAL_POWER_UP_ACTION()
	ENDIF
	
	SWITCH (powerDemoState)
	
		CASE PD_SETUP_GRID
			
			PRINT_HELP("PZ_POW_REMOVAL_01", 6500)
			
			time_started = TO_FLOAT(GET_GAME_TIMER())
			current_time = 0
			
			FOR current_row = min_row TO  max_row
				//CPRINTLN(DEBUG_MINIGAME, sPrefixDebugQub3d, "While loop - Line 6781")
				// initialise the starting column
				FOR current_column = min_col TO max_col
					
					should_block_be_added = FALSE
					
					// bottom row
					IF current_row = max_row
						IF current_column = 0
							// blue
							temp_colour = ORANGE_CUBE
							should_block_be_added = TRUE
						ELSE
							IF current_column = 1
							OR current_column = 2
								// red
								temp_colour = YELLOW
								should_block_be_added = TRUE
							ELSE
								IF current_column < 5
								//green
									temp_colour = PURPLE
									should_block_be_added = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF current_row = max_row - 1
							IF current_column < 2
								// blue
								temp_colour = ORANGE_CUBE
								should_block_be_added = TRUE
							ELSE
								IF current_column < 3						
									// red
									temp_colour = YELLOW
									should_block_be_added = TRUE
								ELSE
									IF current_column = 3
										// green
										temp_colour = PURPLE
										should_block_be_added = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF current_row = max_row - 2
								IF current_column = 2
									// green
									temp_colour = PURPLE
									should_block_be_added = TRUE
								ELSE 
									IF current_column = 3
										// blue
										temp_colour = YELLOW
										should_block_be_added = TRUE
									ENDIF
								ENDIF
							ELSE
								IF current_row = max_row - 3
									IF current_column = 2
										// blue
										temp_colour = ORANGE_CUBE
										should_block_be_added = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				
					IF should_block_be_added
						// create the block
						IF NOT DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
							QUB3D_CREATE_OBJECT(grid_box[current_row][current_column].box_object, box_model, grid_box[current_row][current_column].box_object_pos)
						ENDIF
						
						IF DOES_ENTITY_EXIST(grid_box[current_row][current_column].box_object)
							SET_ENTITY_HEADING(grid_box[current_row][current_column].box_object, 0.0)
							SET_ENTITY_COLLISION(grid_box[current_row][current_column].box_object, FALSE)
							FREEZE_ENTITY_POSITION(grid_box[current_row][current_column].box_object, TRUE)
										
							/// update flags in the struct
							grid_box[current_row][current_column].this_box_colour = temp_colour
							QUB3D_SET_CUBE_COLOR(grid_box[current_row][current_column].box_object, temp_colour)
						ENDIF
						
						grid_box[current_row][current_column].is_box_on_grid = TRUE
						block_status[current_row][current_column] = REMAINS
					ENDIF
		
				ENDFOR

			ENDFOR
			
			powerDemoState = PD_HELP_UPDATE_00
		BREAK
		
		CASE PD_HELP_UPDATE_00
		
			// this part of the tutorial is finished
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started			
			IF time_difference >= 6500
			OR QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PRINT_HELP("PZ_POW_REMOVAL_02", 6500)
				powerDemoState = PD_TRIGGER_POWER
			ENDIF
		BREAK
		
		CASE PD_TRIGGER_POWER
			// this part of the tutorial is finished
			colour_removal_powerup_in_effect = TRUE
			PLAY_SOUND_FRONTEND(-1, use_powerup_sfx)
			has_a_power_up_been_selected = TRUE
			time_started = TO_FLOAT(GET_GAME_TIMER())
			powerDemoState = PD_NEXT_DEMO
		BREAK
		
		CASE PD_NEXT_DEMO
			current_time = TO_FLOAT(GET_GAME_TIMER())
			// time since last one added
			time_difference = current_time - time_started	
			
			IF NOT colour_removal_powerup_in_effect
				IF time_difference >= 6500
					powerDemoState = PD_SETUP_GRID
					blocks_cleared = 0
					current_tutorial_rotation = 0
					is_a_box_in_motion = FALSE
					
					INITIALISE_THE_GRID()
					number_of_power_uses = 1
					
					tutorial_rotation_progression[0] = PRESSED_NOTHING // Needs to drop down
					tutorial_rotation_progression[1] = PRESSED_LEFT
					tutorial_rotation_progression[2] = PRESSED_NOTHING
					tutorial_rotation_progression[3] = PRESSED_RIGHT
					tutorial_rotation_progression[4] = PRESSED_NOTHING
					
					current_tutorial_stage = SHOWING_SLOW_POWER
				ENDIF
			ENDIF
			
			// Skip the demo completely
			IF QUB3d_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				colour_removal_powerup_in_effect = FALSE
				powerDemoState = PD_SETUP_GRID
				blocks_cleared = 0
				current_tutorial_rotation = 0
				is_a_box_in_motion = FALSE
				
				INITIALISE_THE_GRID()
				number_of_power_uses = 1
				
				tutorial_rotation_progression[0] = PRESSED_NOTHING // Needs to drop down
				tutorial_rotation_progression[1] = PRESSED_LEFT
				tutorial_rotation_progression[2] = PRESSED_NOTHING
				tutorial_rotation_progression[3] = PRESSED_RIGHT
				tutorial_rotation_progression[4] = PRESSED_NOTHING
				
				current_tutorial_stage = SHOWING_SLOW_POWER
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC TUTORIAL_CONTROLLER()

	SWITCH current_tutorial_stage
		CASE TUTORIAL_INTRO	
			INITIALISE_THE_TUTORIAL()
		BREAK
		
		CASE SHOWING_GAME_OVERVIEW
			SHOW_GAME_OVERVIEW()
		BREAK
		
		CASE SHOWING_ROTATION_DEMO
			SHOW_ROTATION_DEMO()
		BREAK
		
		CASE SHOWING_CUBE_CLEAR
			SHOW_CUBE_CLEAR()
		BREAK
		
		CASE SHOWING_BLOCK_REMOVAL
			SHOW_BLOCK_REMOVAL()
		BREAK
		
		CASE SHOWING_SPECIAL_BLOCK
			SHOW_SPECIAL_BLOCK()
		BREAK
		
		CASE SHOWING_SPECIAL_BLOCK_REMOVAL
			SHOW_SPECIAL_BLOCK_REMOVAL()
		BREAK
		
		CASE SHOWING_POWER_UP
			SHOW_POWER_UP()
		BREAK
		
		CASE SHOWING_BOMB_POWER
			SHOW_BOMB_POWER()
		BREAK
		
		CASE SHOWING_ERASER_POWER
			SHOW_ERASER_POWER()
		BREAK
		
		CASE SHOWING_REMOVAL_POWER
			SHOW_COLOUR_REMOVAL_POWER()
		BREAK
		
		CASE SHOWING_SLOW_POWER
			SHOW_SLOW_POWER()
		BREAK
		
	ENDSWITCH
	
	/// check for player player exit
	CHECK_FOR_PLAYER_WANTING_TO_QUIT_TUTORIAL()
	
ENDPROC


////////////////////////////////////////
////// **** HIGH SCORE TABLE **** //////
////////////////////////////////////////

PROC RESTORE_TO_FRONT_END_STATUS()
	
	INITIALISE_THE_GRID()
	is_game_over = TRUE
	current_mission_stage = GAME_IN_PROGRESS
			
	CLEAR_HELP()
			
	// Reset gameplay music variable
	SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",0.0)
	QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_MENUS)
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugQub3d, "RESTORE_TO_FRONT_END_STATUS - Reset music stream")
	PLAY_SOUND_FRONTEND( -1, "Menu_Back", ARCADE_GAMES_SOUND_GET_SOUNDSET(QUB3D_AUDIO_EFFECT_SCOREBOARD_CHANGE_LETTER))
	
	IF NOT IS_CAM_ACTIVE(anim_cam)
		SET_CAM_ACTIVE_WITH_INTERP(anim_cam, game_cam, 4000, GRAPH_TYPE_VERY_SLOW_IN_VERY_SLOW_OUT)
		SET_CAM_ACTIVE(game_cam, FALSE)	
	ENDIF
		
ENDPROC

PROC HIGH_SCORE_TABLE_NEW()
	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(high_score_textures)
		ARCADE_GAMES_LEADERBOARD_PROCESS_ENTRY(total_Score)

		// Draw darkened background
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaDarkSeeThrough)
			
		ARCADE_GAMES_LEADERBOARD_DRAW(FALSE, TRUE)
	ENDIF
	
	// this part of the tutorial is finished
	current_time = TO_FLOAT(GET_GAME_TIMER())
	// time since last one added
	time_difference = current_time - time_started			
	IF time_difference >= 2000
	AND NOT ingame_music_playing
		Qub3dMusicSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(Qub3dMusicSoundID, "Music_Dynamic_Banked", "sum20_am_Qub3d_sounds")
		SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",0.0)
		QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_MENUS)
		ingame_music_playing = TRUE
		//ENDIF
	ENDIF
				
	IF !sAGLeaderboardData.bEditing
		is_cross_pressed = TRUE
		PLAY_SOUND_FRONTEND(-1, fixing_block_sfx)
		// if it was the last letter				
		ARCADE_GAMES_HELP_TEXT_CLEAR()	
		total_score = 0				
		
		// Ensure music has started again
		IF NOT ingame_music_playing
			Qub3dMusicSoundID = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(Qub3dMusicSoundID, "Music_Dynamic_Banked", "sum20_am_Qub3d_sounds")
			SET_VARIABLE_ON_SOUND(Qub3dMusicSoundID,"MusicMix",0.0)
			QUB3D_AUDIO_START_AUDIO_SCENE(QUB3D_ARCADE_AUDIO_SCENE_MENUS)
			ingame_music_playing = TRUE
		ENDIF
		
		PRINT_HELP_FOREVER("PZ_RETURN")
		current_mission_stage = HI_SCORE_DISPLAY	
	ENDIF
	
ENDPROC

PROC PUZZLE_INIT_COLOUR_STRUCTS()

	INIT_RGBA_STRUCT(rgbaWhiteOut, 255, 255, 255, 255)
	INIT_RGBA_STRUCT(rgbaBackdrop, 155, 155, 155, 155)
	INIT_RGBA_STRUCT(rgbaDarkSeeThrough, 0, 0, 0, 150)
	INIT_RGBA_STRUCT(rgbaBlackOut, 0, 0, 0, 255)
	INIT_RGBA_STRUCT(rgbaNearOpaque, 255, 255, 255, 190)
ENDPROC
