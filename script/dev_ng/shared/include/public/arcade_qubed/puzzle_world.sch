USING "globals.sch"
USING "model_enums.sch"
USING "rage_builtins.sch"
USING "types.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_camera.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "cellphone_public.sch"
USING "commands_clock.sch"
USING "commands_audio.sch"
USING "arcade_cabinet_minigame_common.sch"
USING "arcade_games_leaderboard.sch"
USING "arcade_games_sound.sch"
USING "commands_stats.sch"
USING "net_arcade_cabinet.sch"

// camera
VECTOR game_Cam_pos = <<-244.530, 6198.4976, 37.06>>
//VECTOR game_Cam_rot = <<-45, 0.00, 90.0>>
VECTOR game_Cam_rot = <<-67.5, 0.00, 90.0>>
FLOAT game_cam_fov = 58.0


/// spline camera stuff
CAMERA_INDEX game_cam
CAMERA_INDEX anim_cam 

PROC SETUP_THE_ANIM_CAM(VECTOR vBasePosition, FLOAT fGridRotation)
		
	IF NOT DOES_CAM_EXIST(anim_cam)
		/// create and position the cameras
		// set up the spline camera 
		anim_cam = CREATE_CAM_WITH_PARAMS("DEFAULT_ANIMATED_CAMERA",vBasePosition, <<0.0, 0.0, fGridRotation>>, game_cam_fov, TRUE)
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][QUB3D] SETUP_THE_ANIM_CAM - anim_cam - Create this frame")					
		SET_CAM_NEAR_CLIP(anim_cam, 0.01)
		SET_CAM_FAR_CLIP(anim_cam, 0.02)
		SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(anim_cam, -1.0)
		SET_CAM_DOF_STRENGTH(anim_cam, 0.0)
		PLAY_CAM_ANIM(anim_cam, "loop_cam", "ANIM_HEIST@ARCADE@QUB3D@", vBasePosition, <<0.0, 0.0, fGridRotation>>, CAF_LOOPING)
	ENDIF
		
ENDPROC

// Add two vectors together
FUNC VECTOR QUB3D_ADD_TWO_VECTORS(VECTOR vectorA, VECTOR vectorB)
	RETURN <<(vectorA.x + vectorB.x), (vectorA.y + vectorB.y), (vectorA.z + vectorB.z)>>
ENDFUNC

PROC QUB3D_GET_WORLD_COORDS(ARCADE_CABINET_PROPERTY eProperty, VECTOR& vGameCamera, VECTOR& vGridPosition, FLOAT &fGridRotation, FLOAT &startingColumn, FLOAT &startingRow)

	VECTOR vBasePosition = game_Cam_pos

	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != ARCADE_CABINET_INVALID
		INT iDisplaySlot =  GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()) 
		ARCADE_CABINETS eCabinetLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
		GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(eProperty, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), eCabinetLocatePlayerIsIn, vBasePosition, fGridRotation)
		vBasePosition.z -= 0.75
	ENDIF
	
	fGridRotation = 90.0 // Needs to be 90 degrees
	
	vGameCamera =  QUB3D_ADD_TWO_VECTORS(vBasePosition, <<-0.0075, 0, 1.16>>)
	vGridPosition = SUBTRACT_TWO_VECTORS(vBasePosition, <<0.12, 0.0, -0.92>>)

	//fCamRotation = (fHeading)

	startingColumn = (vGridPosition.x - 0.126)
	startingRow = (vGridPosition.Y - 0.0625)


ENDPROC

///////////////////////////
// *** CABINET ANIMS *** //
///////////////////////////

ENUM PZ_ANIM_CLIP

	PZ_ANIM_CLIP_INVALID = -1,
	PZ_ANIM_CLIP_ENTRY = 0,
	PZ_ANIM_CLIP_IDLE,
	PZ_ANIM_CLIP_KICK,
	PZ_ANIM_CLIP_PLAY_IDLE,
	PZ_ANIM_CLIP_LOSE,
	PZ_ANIM_CLIP_LOSE_BIG,
	PZ_ANIM_CLIP_WIN,
	PZ_ANIM_CLIP_WIN_BIG,
	PZ_ANIM_CLIP_EXIT

ENDENUM

STRUCT QUB3d_ANIM_STRUCT

	PZ_ANIM_CLIP pzAnimClipActive
	ARCADE_CABINET_ANIM_CLIPS animationToPlay
	ARCADE_CABINET_ANIM_EVENT_STRUCT sQub3dAnimEvent
	SCRIPT_TIMER sAnimTimer

ENDSTRUCT

QUB3d_ANIM_STRUCT pz_AnimStruct

PROC PUZZLE_PROCESS_PLAYER_PED_ANIMS()

	// Used to sotre a random value if a random clip needs to be picked
	INT iRandomAnimClip

	SWITCH pz_AnimStruct.pzAnimClipActive
		CASE PZ_ANIM_CLIP_ENTRY
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_ENTRY
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = TRUE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_ENTRY
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(pz_AnimStruct.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(pz_AnimStruct.animationToPlay)
				AND pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF
			ENDIF
		BREAK

		CASE PZ_ANIM_CLIP_IDLE
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
				RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
				START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)	
			ENDIF	
		BREAK
		
		CASE PZ_ANIM_CLIP_PLAY_IDLE
			
			IF HAS_NET_TIMER_STARTED(pz_AnimStruct.sAnimTimer)
				IF HAS_NET_TIMER_EXPIRED(pz_AnimStruct.sAnimTimer, 20000)
				
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
				
					CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][QUB3D] Update IDLE animation while QUB3D game is running.")
					iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRandomAnimClip
						CASE 0
							IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
								pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
								PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)					
							ENDIF
						BREAK
						CASE 1
							IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2
								pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2	
								PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
							ENDIF
						BREAK
						
					ENDSWITCH	
					
					// Restart the Net timer from the start.
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF
			ENDIF
		BREAK
			
		CASE PZ_ANIM_CLIP_LOSE
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_LOSE
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = TRUE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_LOSE				
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(pz_AnimStruct.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(pz_AnimStruct.animationToPlay)
				AND pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_IDLE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE	
					PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF	
			ENDIF	
		BREAK
			
		CASE PZ_ANIM_CLIP_LOSE_BIG
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_LOSE_BIG
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = TRUE
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_LOSE_BIG			
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(pz_AnimStruct.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(pz_AnimStruct.animationToPlay)
				AND pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_IDLE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF	
			ENDIF	
		BREAK
			
		CASE PZ_ANIM_CLIP_KICK
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_KICK
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = TRUE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_KICK			
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(pz_AnimStruct.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(pz_AnimStruct.animationToPlay)
				AND pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_IDLE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF	
			ENDIF	
		BREAK
			
		CASE PZ_ANIM_CLIP_WIN
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_WIN
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = TRUE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_WIN
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(pz_AnimStruct.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(pz_AnimStruct.animationToPlay)
				AND pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_IDLE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF	
			ENDIF	
		BREAK
			
		CASE PZ_ANIM_CLIP_WIN_BIG
			IF pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_WIN_BIG
				pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = FALSE
				pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = TRUE
				pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_WIN_BIG
				PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(pz_AnimStruct.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(pz_AnimStruct.animationToPlay)
				AND pz_AnimStruct.animationToPlay != ARCADE_CABINET_ANIM_CLIP_IDLE
					pz_AnimStruct.sQub3dAnimEvent.bLoopAnim = TRUE
					pz_AnimStruct.sQub3dAnimEvent.bHoldLastFrame = FALSE
					pz_AnimStruct.pzAnimClipActive = PZ_ANIM_CLIP_IDLE
					pz_AnimStruct.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(pz_AnimStruct.sQub3dAnimEvent, pz_AnimStruct.animationToPlay)
					RESET_NET_TIMER(pz_AnimStruct.sAnimTimer)
					START_NET_TIMER(pz_AnimStruct.sAnimTimer)
				ENDIF	
			ENDIF	
		BREAK
			
	ENDSWITCH
	
ENDPROC
