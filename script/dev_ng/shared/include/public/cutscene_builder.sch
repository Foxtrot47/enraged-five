USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_camera.sch"
using "commands_misc.sch"
USING "script_maths.sch"
USING "Script_player.sch"
USING "Overlay_Effects.sch"

USING "usefulCommands.sch"
USING "script_buttons.sch"
//using "commands_path.sch"

CONST_INT MAX_CAM_PANS 10



struct structCamData
	vector vPosa,vposb
	vector vRota,vRotb
	int iPanTime, iPauseTime
	bool bDelete, bStorePosa, bstoreposb, bPlayPan, bMoveUp, bMoveDown, bHoldLastFrame
	int graphTypeA,graphTypeB
	WIDGET_GROUP_ID widget
	int startTime, endTime, shakeStyle
	float amplitude
	vehicle_index linkedVehicle = NULL
	int specialCase //attaching to vehicles etc
	bool persistCam
ENDSTRUCT

structCamData camPans[MAX_CAM_PANS]
#IF IS_DEBUG_BUILD
	structCamData tempcampan
#ENDIF

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
#ENDIF

int iCamPanCount = 1
int iPanPlaying
int iEndPanTime
bool bPlayCutscene
bool bTriggerNextPan
bool bFullCutPlaying
bool bDisableScriptTrigger
bool bPreventScriptAddPans
int iCuttime,iCutStartTime

#IF IS_DEBUG_BUILD

bool bPause, bResetCutscene
#ENDIF


CAMERA_INDEX cutscene_cam //cama,camb
bool bCamsMade

vector vcamNull = <<0,0,0>>

proc emptyPanData(int i)
	camPans[i].vPosa = <<0,0,0>>				
	camPans[i].vRota = <<0,0,0>>
	camPans[i].vPosb = <<0,0,0>>				
	camPans[i].vRotb = <<0,0,0>>
	camPans[i].iPanTime = 0
	camPans[i].iPauseTime = 0
	camPans[i].bDelete = FALSE
	camPans[i].bPlayPan = FALSE
	camPans[i].bMoveUp = FALSE
	camPans[i].bMoveDown = FALSE
	camPans[i].bHoldLastFrame = FALSE
	camPans[i].graphTypeA = 0
	camPans[i].graphTypeB = 0
ENDPROC

#IF IS_DEBUG_BUILD



bool bWidgetMade
int iExistingCamPans
bool bExport
 
WIDGET_GROUP_ID wTemp,debugWidget



proc cutscene_widget(WIDGET_GROUP_ID parent = null)
	int i
	bool bPanDeleted = false
	
	if bWidgetMade = FALSE
		bWidgetMade = true
		iExistingCamPans = iCamPanCount
		
		if parent != NULL
			SET_CURRENT_WIDGET_GROUP(parent)
		
		
			if not DOES_WIDGET_GROUP_EXIST(debugWidget)
				debugWidget = START_WIDGET_GROUP("CUTSCENE DIRECTOR")	
				ADD_WIDGET_BOOL("Play all pans",bPlayCutscene)
				ADD_WIDGET_BOOL("Keep debug cam view",bDisableScriptTrigger)
				ADD_WIDGET_BOOL("Prevent ADD_CAM_PAN",bPreventScriptAddPans)
				ADD_WIDGET_INT_SLIDER("No of cam pans",iCamPanCount,0,MAX_CAM_PANS-1,1)
				ADD_WIDGET_INT_READ_ONLY("Cutscene Time",iCutTime)
				ADD_WIDGET_BOOL("Export Cams",bExport)
				ADD_WIDGET_BOOL("Pause",bPause)			
				ADD_WIDGET_BOOL("Restart Cut",bResetCutscene)		
				STOP_WIDGET_GROUP()
			ENDIF
			
			CLEAR_CURRENT_WIDGET_GROUP(parent)
		
			SET_CURRENT_WIDGET_GROUP(debugWidget)
			
			TEXT_LABEL_7 panWidgetName
			
			for i = 0 to MAX_CAM_PANS-1
				if not DOES_WIDGET_GROUP_EXIST(camPans[i].widget)
					if i < iCamPanCount
						panWidgetName = "Pan: "
						panWidgetName += i
						camPans[i].widget = START_WIDGET_GROUP(panWidgetName)	
						
						
							
							START_WIDGET_GROUP("Start Cam")
								ADD_WIDGET_VECTOR_SLIDER("Start cam pos",camPans[i].vPosa,-10000,10000,0.1)
								ADD_WIDGET_VECTOR_SLIDER("Start cam rot",camPans[i].vRota,-360,360,0.1)
								ADD_WIDGET_BOOL("Set cam from debug viewport",camPans[i].bStorePosa)
								START_NEW_WIDGET_COMBO()
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
								STOP_WIDGET_COMBO("Cam Graph Type",camPans[i].graphTypeA)						
							STOP_WIDGET_GROUP()
							START_WIDGET_GROUP("End Cam")
								ADD_WIDGET_VECTOR_SLIDER("end cam pos",camPans[i].vPosb,-10000,10000,0.1)
								ADD_WIDGET_VECTOR_SLIDER("end cam rot",camPans[i].vRotb,-360,360,0.1)
								ADD_WIDGET_BOOL("Set cam from debug viewport",camPans[i].bStorePosb)
								START_NEW_WIDGET_COMBO()
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
									ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
								STOP_WIDGET_COMBO("Cam Graph Type",camPans[i].graphTypeB)
							STOP_WIDGET_GROUP()
							
							START_NEW_WIDGET_COMBO()
								ADD_TO_WIDGET_COMBO("None")
								ADD_TO_WIDGET_COMBO("Hand Shake")
								ADD_TO_WIDGET_COMBO("Road Vibration Shake")
								ADD_TO_WIDGET_COMBO("Medium Explosion Shake")
								ADD_TO_WIDGET_COMBO("Vibrate Shake")
							STOP_WIDGET_COMBO("Shake type",camPans[i].shakeStyle)
							ADD_WIDGET_FLOAT_SLIDER("Shake Amplitude",camPans[i].amplitude,0,10.0,0.01)
						
							
							ADD_WIDGET_INT_READ_ONLY("Start at",camPans[i].startTime)
							ADD_WIDGET_INT_SLIDER("Cam pan duration",camPans[i].iPanTime,0,60000,1)
							ADD_WIDGET_INT_SLIDER("Pause at end duration",camPans[i].iPauseTime,0,60000,1)
							ADD_WIDGET_INT_READ_ONLY("end at",camPans[i].endTime)
							
							ADD_WIDGET_BOOL("Persist from last pan",camPans[i].persistCam)
							ADD_WIDGET_BOOL("Play this pan",camPans[i].bPlayPan)
							ADD_WIDGET_BOOL("Hold last position",camPans[i].bHoldLastFrame)
							ADD_WIDGET_BOOL("Delete pan",camPans[i].bDelete)
							ADD_WIDGET_BOOL("Shift pan up",camPans[i].bMoveUp)
							ADD_WIDGET_BOOL("Shift pan down",camPans[i].bMoveDown)
							
									
						
						STOP_WIDGET_GROUP()
					ENDIF
				ELSE
					if i >= iCamPanCount
						DELETE_WIDGET_GROUP(camPans[i].widget)
						emptyPanData(i)					
					ENDIF
				ENDIF
			ENDFOR
			

			CLEAR_CURRENT_WIDGET_GROUP(debugWidget)
		
		
	
		ENDIF
	ELSE
		if parent != NULL
			if iExistingCamPans != iCamPanCount
				bWidgetMade = FALSE
			ENDIF
			
			for i = 0 to MAX_CAM_PANS-1
				if camPans[i].bDelete = TRUE
					camPans[i].bDelete = FALSE
					bPanDeleted = true				
				ENDIF
				if bPanDeleted = true
					if DOES_WIDGET_GROUP_EXIST(camPans[i].widget)
						DELETE_WIDGET_GROUP(camPans[i].widget)
					ENDIF
					if i < MAX_CAM_PANS-1										
						wTemp = camPans[i].widget										
						camPans[i] = camPans[i+1]
						camPans[i].widget = wTemp
					ELSE
						emptyPanData(i)						
					ENDIF
				ENDIF
				
				if camPans[i].bStorePosa = TRUE
					camPans[i].bStorePosa = FALSE
					CAMERA_INDEX renderingCam = GET_RENDERING_CAM()
					if DOES_CAM_EXIST(renderingCam)
						camPans[i].vPosa = GET_CAM_COORD(renderingCam)
						camPans[i].vRota =  GET_CAM_ROT(renderingCam)
					ENDIF
				ENDIF
				
				if camPans[i].bStorePosb = TRUE
					camPans[i].bStorePosb = FALSE
					CAMERA_INDEX renderingCam = GET_RENDERING_CAM()
					if DOES_CAM_EXIST(renderingCam)
						camPans[i].vPosb = GET_CAM_COORD(renderingCam)
						camPans[i].vRotb =  GET_CAM_ROT(renderingCam)
					ENDIF
				ENDIF
				
				if camPans[i].bMoveUp = TRUE
					camPans[i].bMoveUp = FALSE
					if i != 0
						tempcampan = camPans[i-1]
						WIDGET_GROUP_ID wtemp1 = camPans[i-1].widget
						WIDGET_GROUP_ID wtemp2 = camPans[i].widget
						camPans[i-1] = camPans[i]
						camPans[i] = tempcampan
						camPans[i].widget = wtemp2
						camPans[i-1].widget = wtemp1
					ENDIF
				ENDIF
				
				if camPans[i].bMoveDown = TRUE
					camPans[i].bMoveDown = FALSE
					if i != iCamPanCount-1
						tempcampan = camPans[i+1]
						WIDGET_GROUP_ID wtemp1 = camPans[i+1].widget
						WIDGET_GROUP_ID wtemp2 = camPans[i].widget
						camPans[i+1] = camPans[i]
						camPans[i] = tempcampan
						camPans[i].widget = wtemp2
						camPans[i+1].widget = wtemp1
					ENDIF
				ENDIF
				
				if i = 0
					camPans[i].startTime = 0
				ELSE
					camPans[i].startTime = camPans[i-1].endtime
				ENDIF
				
				camPans[i].endTime = camPans[i].startTime + camPans[i].iPanTime + camPans[i].iPauseTime
				
				
				if camPans[i].bPlayPan = TRUE
					iPanPlaying = i
					
				ENDIF
				
				
				
			endfor
			if bPanDeleted = true
				iCamPanCount--
				bWidgetMade = FALSE
			ENDIF
			if bExport = TRUE
				bExport = FALSE
				
				STRING sFilePath = "C:/temp/cams"
				STRING sFileName = "cams.txt"
				
				OPEN_NAMED_DEBUG_FILE(sFilePath,sFileName)
				
				for i = 0 to iCamPanCount-1
				
				
					SAVE_STRING_TO_NAMED_DEBUG_FILE("ADD_CAM_PAN(",sFilePath,sFileName)
					SAVE_INT_TO_NAMED_DEBUG_FILE(i,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",<<",sFilePath,sFileName)	
					
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vPosa.x,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vPosa.y,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vPosa.z,sFilePath,sFileName)
					
					//SAVE_VECTOR_TO_NAMED_DEBUG_FILE(camPans[i].vPosa,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(">>,<<",sFilePath,sFileName)
					
					
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vPosb.x,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vPosb.y,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vPosb.z,sFilePath,sFileName)
					
					//SAVE_VECTOR_TO_NAMED_DEBUG_FILE(camPans[i].vPosb,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(">>,<<",sFilePath,sFileName)
					
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vRota.x,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vRota.y,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vRota.z,sFilePath,sFileName)
					
					//SAVE_VECTOR_TO_NAMED_DEBUG_FILE(camPans[i].vRota,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(">>,<<",sFilePath,sFileName)
					
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vRotb.x,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vRotb.y,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].vRotb.z,sFilePath,sFileName)
					
					//SAVE_VECTOR_TO_NAMED_DEBUG_FILE(camPans[i].vRotb,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(">>,",sFilePath,sFileName)
					SAVE_INT_TO_NAMED_DEBUG_FILE(camPans[i].iPanTime,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_INT_TO_NAMED_DEBUG_FILE(camPans[i].iPauseTime,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_INT_TO_NAMED_DEBUG_FILE(camPans[i].graphTypeA,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_INT_TO_NAMED_DEBUG_FILE(camPans[i].graphTypeB,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_INT_TO_NAMED_DEBUG_FILE(camPans[i].shakeStyle,sFilePath,sFileName)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(",",sFilePath,sFileName)
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(camPans[i].amplitude,sFilePath,sFileName)
					
					IF camPans[i].persistCam = TRUE
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",TRUE",sFilePath,sFileName)
					ELSE
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",FALSE",sFilePath,sFileName)
					ENDIF
					SAVE_STRING_TO_NAMED_DEBUG_FILE(")",sFilePath,sFileName)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath,sFileName)
					
					/*
					SAVE_STRING_TO_DEBUG_FILE("ADD_CAM_PAN(")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_VECTOR_TO_DEBUG_FILE(camPans[i].vPosa)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_VECTOR_TO_DEBUG_FILE(camPans[i].vPosb)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_VECTOR_TO_DEBUG_FILE(camPans[i].vRota)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_VECTOR_TO_DEBUG_FILE(camPans[i].vRotb)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_INT_TO_DEBUG_FILE(camPans[i].iPanTime)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_INT_TO_DEBUG_FILE(camPans[i].iPauseTime)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_INT_TO_DEBUG_FILE(camPans[i].graphTypeA)
					SAVE_STRING_TO_DEBUG_FILE(",")
					SAVE_INT_TO_DEBUG_FILE(camPans[i].graphTypeB)
					SAVE_STRING_TO_DEBUG_FILE(")")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					*/
				ENDFOR
				CLOSE_DEBUG_FILE()
			ENDIF	
		ENDIF
	ENDIF
ENDPROC
#ENDIF

proc ADD_CAM_PAN(int a, vector posStart, vector posEnd, vector rotStart, vector rotEnd, int panTime, int pauseAtEnd, int graphTypeStart, int graphTypeEnd, int shakeStyle=0, float shakeAmplitude=0.0, bool bPersistsFromPreviousCam=FALSE)
	if bPreventScriptAddPans = FALSE
		int i
		If a = 0
			for i = 0 to MAX_CAM_PANS-1
				emptyPanData(i)
			endfor
			iCamPanCount = 0
		ENDIF
		camPans[a].vPosa = posStart
		camPans[a].vPosb = posEnd
		camPans[a].vRota = rotStart
		camPans[a].vRotb = rotEnd
		camPans[a].iPanTime = panTime
		camPans[a].iPauseTime = pauseAtEnd
		camPans[a].graphTypeA = graphTypeStart
		camPans[a].graphTypeB = graphTypeEnd
		camPans[a].shakeStyle = shakeStyle
		camPans[a].amplitude = shakeAmplitude
		camPans[a].persistCam = bPersistsFromPreviousCam
		if a >= iCamPanCount
			iCamPanCount = a+1
		ENDIF	
	ENDIF
ENDPROC

PROC SET_CAM_PAN_TARGET_VEHICLE(int camPan, VEHICLE_INDEX VEH, vector vOffset)
	camPans[camPan].linkedVehicle = VEH
	camPans[camPan].vRota = vOffset
	camPans[camPan].specialCase = 1
ENDPROC

proc SET_CAM_PAN_ATTACH_VEHICLE(int camPan, VEHICLE_INDEX VEH, vector vOffset)
	camPans[camPan].linkedVehicle = VEH
	camPans[camPan].vPosa = vOffset
	camPans[camPan].specialCase = 2
ENDPROC		

proc END_CUTSCENE(bool endWithInterp = true, int iInterpTime=1000)
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF DOES_CAM_EXIST(cutscene_cam)		
			if IS_CAM_ACTIVE(cutscene_cam)
				if endWithInterp = true
					RENDER_SCRIPT_CAMS(FALSE,TRUE,iInterpTime)
				ELSE
					RENDER_SCRIPT_CAMS(FALSE,FALSE)	
				ENDIF
				SET_CAM_ACTIVE(cutscene_cam,FALSE)	
				
				camPans[iPanPlaying].bPlayPan = FALSE
			
				bPlayCutscene = FALSE
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

proc controlCameras()
	
	iCuttime=iCuttime
	if bCamsMade = FALSE
		cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		debugMessage(debug_cameras,"controlCameras():created script cam for first time")
		//CLEAR_PRINTS()
		//camb = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		bCamsMade = true
		
		
	ELSE
	
		if bPlayCutscene = TRUE
			if bFullCutPlaying = FALSE
				debugMessage(debug_cameras,"controlCameras():trigger cut playing for first time")
				iPanPlaying = 0
				bFullCutPlaying = true
				camPans[iPanPlaying].bPlayPan = TRUE	
			
				iCutStartTime = GET_GAME_TIMER()
			
			ELSE
				
				iCuttime = GET_GAME_TIMER() - iCutStartTime
			
			ENDIF
		ELSE
			if bFullCutPlaying = TRUE
				//end cutscene
				camPans[iPanPlaying].bPlayPan = FALSE
				bTriggerNextPan = FALSE
				bFullCutPlaying = FALSE
				bPlayCutscene = FALSE
			ENDIF
		ENDIF
		//added this if endif
		if bFullCutPlaying = TRUE
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			if bTriggerNextPan = true
				camPans[iPanPlaying].bPlayPan = FALSE
				iPanPlaying++
				if iPanPlaying < iCamPanCount
					camPans[iPanPlaying].bPlayPan = true
					debugMessage(debug_cameras,"controlCameras():iPanPlaying < iCamPanCount")
					STOP_CAM_POINTING(cutscene_cam)					
					DETACH_CAM(cutscene_cam)		
					IF camPans[iPanPlaying].persistCam = FALSE
						DESTROY_CAM(cutscene_cam)
						cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
					ENDIF
					debugMessage(debug_cameras,"controlCameras():iPanPlaying < iCamPanCount:create cam")
				ELSE
					bTriggerNextPan = FALSE
					bFullCutPlaying = FALSE
					bPlayCutscene = FALSE
				ENDIF
			ENDIF
				
			if camPans[iPanPlaying].bPlayPan = TRUE		
				debugMessage(debug_cameras,"controlCameras():play pan")
				if not IS_CAM_ACTIVE(cutscene_cam) or bTriggerNextPan = true
					debugMessage(debug_cameras,"controlCameras():trigger next pan")
					bTriggerNextPan = FALSE
					if IS_CAM_RENDERING(GET_DEBUG_CAM())
						if bDisableScriptTrigger = FALSE
							SET_DEBUG_CAM_ACTIVE(FALSE)
						ENDIF
					ENDIF
					if bDisableScriptTrigger = FALSE
						
						
						if camPans[iPanPlaying].specialCase = 1 //point at vehicle
							if not IS_ENTITY_DEAD(camPans[iPanPlaying].linkedVehicle)
							//	SET_CAM_COORD(cutscene_cam,camPans[iPanPlaying].vPosa)
								//SET_CAM_PARAMS(cutscene_cam,camPans[iPanPlaying].vPosa, vcamNull, 45.0)
								//POINT_CAM_AT_ENTITY(cutscene_cam,camPans[iPanPlaying].linkedVehicle,<<0,0,0>>)//camPans[iPanPlaying].vRota)
								
								//SET_CAM_PARAMS(cutscene_cam,camPans[iPanPlaying].vPosb, vcamNull, 45.0,camPans[iPanPlaying].iPanTime,INT_TO_ENUM(CAMERA_GRAPH_TYPE,camPans[iPanPlaying].graphTypeA),INT_TO_ENUM(CAMERA_GRAPH_TYPE,camPans[iPanPlaying].graphTypeB))
								debugMessage(debug_cameras,"controlCameras():point at vehicle")
								DESTROY_CAM(cutscene_cam)		
								cutscene_cam = CREATE_CAM ("DEFAULT_SPLINE_CAMERA")
							
								ADD_CAM_SPLINE_NODE(cutscene_cam,camPans[iPanPlaying].vPosa,vcamNull,0)
								ADD_CAM_SPLINE_NODE(cutscene_cam,camPans[iPanPlaying].vPosb,vcamNull,camPans[iPanPlaying].iPanTime)
								POINT_CAM_AT_ENTITY(cutscene_cam,camPans[iPanPlaying].linkedVehicle,camPans[iPanPlaying].vRota)
								SET_CAM_FOV(cutscene_cam,45.0)
								debugMessage(debug_cameras,"controlCameras():set cam active pointing at vehicle")
								SET_CAM_ACTIVE(cutscene_cam,TRUE)
								//SET_CAM_ACTIVE(cutscene_cam,TRUE)		
							ELSE
								debugMessage(debug_cameras,"controlCameras():point at vehicle is dead")
							ENDIF
						ELSE
							if camPans[iPanPlaying].specialCase = 2 // attach to vehicle
								if not IS_ENTITY_DEAD(camPans[iPanPlaying].linkedVehicle)
									//DESTROY_CAM(cutscene_cam)		
									//cutscene_cam = CREATE_CAM ("DEFAULT_SPLINE_CAMERA")
									//ADD_CAM_SPLINE_NODE(cutscene_cam,camPans[iPanPlaying].vPosa,vcamNull,0)
									//ADD_CAM_SPLINE_NODE(cutscene_cam,camPans[iPanPlaying].vPosb,vcamNull,camPans[iPanPlaying].iPanTime)
									ATTACH_CAM_TO_ENTITY(cutscene_cam,camPans[iPanPlaying].linkedVehicle,camPans[iPanPlaying].vPosa)
									SET_CAM_FOV(cutscene_cam,45.0)
									debugMessage(debug_cameras,"controlCameras():set cam active attached to vehicle")
									SET_CAM_ACTIVE(cutscene_cam,TRUE)
								ENDIF
							ELSE
								IF camPans[iPanPlaying].persistCam = FALSE
									SET_CAM_PARAMS(cutscene_cam,camPans[iPanPlaying].vPosa, camPans[iPanPlaying].vRota, 45.0)
									SET_CAM_FOV(cutscene_cam,45.0)
									debugMessage(debug_cameras,"controlCameras():set cam active")
									SET_CAM_ACTIVE(cutscene_cam,TRUE)
								ENDIF
								SET_CAM_PARAMS(cutscene_cam,camPans[iPanPlaying].vPosb, camPans[iPanPlaying].vRotb, 45.0,camPans[iPanPlaying].iPanTime,INT_TO_ENUM(CAMERA_GRAPH_TYPE,camPans[iPanPlaying].graphTypeA),INT_TO_ENUM(CAMERA_GRAPH_TYPE,camPans[iPanPlaying].graphTypeB))
							ENDIF
						ENDIF
						if camPans[iPanPlaying].shakeStyle != 0
							switch camPans[iPanPlaying].shakeStyle
								case 1
									SHAKE_CAM(cutscene_cam,"HAND_SHAKE",camPans[iPanPlaying].amplitude)
								BREAK
								case 2
									SHAKE_CAM(cutscene_cam,"ROAD_VIBRATION_SHAKE",camPans[iPanPlaying].amplitude)
								BREAK 
								case 3
									SHAKE_CAM(cutscene_cam,"MEDIUM_EXPLOSION_SHAKE",camPans[iPanPlaying].amplitude)
								BREAK
								case 4
									SHAKE_CAM(cutscene_cam,"VIBRATE_SHAKE",camPans[iPanPlaying].amplitude)
								BREAK								
							ENDSWITCH
						ELSE
						//shake
						//	SHAKE_CAM(cutscene_cam,0)
						ENDIF
						
						IF IS_CAM_ACTIVE(cutscene_cam)
							RENDER_SCRIPT_CAMS(TRUE,FALSE)	
							debugMessage(debug_cameras,"controlCameras():render script cam")
						ELSE
							debugMessage(debug_cameras,"controlCameras():script cam not active")
						ENDIF
					ENDIF
					iEndPanTime = camPans[iPanPlaying].iPanTime + camPans[iPanPlaying].iPauseTime + GET_GAME_TIMER()
				ENDIF	
			
				if not IS_CAM_INTERPOLATING(cutscene_cam)
					if (camPans[iPanPlaying].bHoldLastFrame = FALSE and  GET_GAME_TIMER() > iEndPanTime)// or (camPans[iPanPlaying].bHoldLastFrame = true and  GET_GAME_TIMER() > iEndPanTime)
						if bFullCutPlaying = FALSE
							//RENDER_SCRIPT_CAMS(FALSE,FALSE)
							//SET_CAM_ACTIVE(cutscene_cam,FALSE)	
							//camPans[iPanPlaying].bPlayPan = FALSE
							END_CUTSCENE(FALSE)
						ELSE
							bTriggerNextPan = true
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				if IS_CAM_ACTIVE(cutscene_cam)
					END_CUTSCENE(FALSE)
					//RENDER_SCRIPT_CAMS(FALSE,FALSE)										
					//SET_CAM_ACTIVE(cutscene_cam,FALSE)				
					//camPans[iPanPlaying].bPlayPan = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



FUNC BOOL PLAY_CAM_PANS()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		//debugMessage(debug_cameras,"run func: play_cam_pans()")
		CLEAR_HELP()
		CLEAR_PRINTS()
		SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
		bPlayCutscene = true
		DISPLAY_RADAR(FALSE)
		DISPLAY_HUD(FALSE)
		return true
	ENDIF
	return FALSE
ENDFUNC

func bool ARE_CAM_PANS_PLAYING()
	if bPlayCutscene = true
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

proc RESET_CAM_PANS()

	//iCamPanCount = 1
	camPans[iPanPlaying].bPlayPan = FALSE
	bTriggerNextPan = FALSE
	bFullCutPlaying = FALSE	
	bPlayCutscene = FALSE			
	//iCamPanCount = 0
	iPanPlaying = 0		
	SET_CAM_ACTIVE(cutscene_cam,FALSE)
	STOP_CAM_POINTING(cutscene_cam)
	DETACH_CAM(cutscene_cam)
ENDPROC



#IF IS_DEBUG_BUILD
	func bool RESET_CUTSCENE_CALLED()
		if bResetCutscene = TRUE
			bResetCutscene = FALSE
			bExport = true
			RESET_CAM_PANS()
			return true
		ENDIF
		return FALSE	
	ENDFUNC

	proc cleanup_cam_pans()
		if DOES_WIDGET_GROUP_EXIST(debugWidget)
			DELETE_WIDGET_GROUP(debugWidget)
		ENDIF
	ENDPROC


#ENDIF

func bool HAS_CUTSCENE_BEEN_SKIPPED()
	debugMessage(DEBUG_CAMERAS,"HAS_CUTSCENE_BEEN_SKIPPED(): START")
	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() and ARE_CAM_PANS_PLAYING()	
		debugMessage(DEBUG_CAMERAS,"if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() and ARE_CAM_PANS_PLAYING() TRUE")
		if iCuttime > 1000
			END_CUTSCENE(FALSE)
			return true			
		ENDIF
	ENDIF

	
	return FALSE
	
ENDFUNC

proc CUTSCENE_DIRECTOR(#IF IS_DEBUG_BUILD WIDGET_GROUP_ID parent=null #endif)

	controlCameras()
	#IF IS_DEBUG_BUILD
		cutscene_widget(parent)
		if bPause = TRUE
			SET_GAME_PAUSED(true)
		ELSE
			SET_GAME_PAUSED(FALSE)
		ENDIF
	#ENDIF
ENDPROC

