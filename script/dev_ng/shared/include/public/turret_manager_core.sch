//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_manager_core.sch																	    //
// Description: This header contains the core functionality for the TURRET_MANAGER system. Players can      //
//				ask the server to "lock" (/reserve) them a turre, or "unlock" a turret if already using one.// 
// 																											//
//				Supporting headers:																			//
//				* turret_manager_def.sch 	- struct, enum, look-up tables, and const defenitions.			//
//				* turret_manager_public.sch - "public" support methods.										//
//				* turret_manager_sup.sch 	- "private" support methods.									//
//																											//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		05/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "turret_manager_sup.sch"
USING "turret_manager_def.sch"

PROC _TURRET_MANAGER_MAINTAIN_LOCK_REQUEST(TURRET_MANAGER_DATA& ref_data)
	
	TURRET_MANAGER_LOCK_STATE eStateSuccess
	TURRET_MANAGER_LOCK_STATE eStateFail
	BOOL bIgnoreRequestLimit
	
	IF g_eTurretManagerState = TLLS_LOCKING
		eStateSuccess = TLLS_LOCKED
		eStateFail = TLLS_UNLOCKED
		bIgnoreRequestLimit = FALSE
	ELSE
		eStateSuccess = TLLS_UNLOCKED
		eStateFail = TLLS_UNLOCKED
		bIgnoreRequestLimit = TRUE
	ENDIF
	
	// If the request is new we need to update internal data
	// so that the request/response unique id will match up.
	IF g_bNewTurretLockRequest
		g_bNewTurretLockRequest = FALSE
		CDEBUG1LN(DEBUG_NET_TURRET, "New turret request...")
		
		RESET_NET_TIMER(ref_data.requestSentTime)
		ref_data.turretLockRequestCopy = g_turretLockRequest
	ENDIF
	
	// If local player is host we can just directly process their request...
	IF ref_data.pHost = PLAYER_ID()
		CDEBUG1LN(DEBUG_NET_TURRET, "_TURRET_MANAGER_MAINTAIN_LOCKING I am host ")
		PRINT_TURRET_TIME()
		
		INT iRequestData[ci_TURRET_RQST_COUNT]
		INT iResponseData[ci_TURRET_RQST_COUNT]
		
		// Unique Id is still important for the host because we use it to validate the running turret script is set up correctly.
		FILL_TURRET_REQUEST_DATA(g_turretLockRequest, ENUM_TO_INT(g_iTurretLockRequestUid), g_eTurretLockSearchType, g_iTurretLockBs, iRequestData)
		PROCESS_SERVER_RESPONSE_TO_TURRET_REQUEST(ref_data.pHost, iRequestData, iResponseData)
		SAVE_TURRET_LOCK_RESPONSE(iResponseData)
		
		// Do same checks as if we had just recieved the event.
		IF g_turretLockResponse.groupType = g_turretLockRequest.groupType
			_TURRET_MANAGER_SET_LOCK_STATE(eStateSuccess)	
		ELSE
			// Server said we couldn't use this turret.
			_TURRET_MANAGER_SET_LOCK_STATE(eStateFail)
		ENDIF
	ELSE
		// Already sent inital request?
		IF HAS_NET_TIMER_STARTED(ref_data.requestSentTime)
		
			// If the server as replied to our request
			IF g_iTurretLockRequestUid = g_iTurretLockResponseUid 
				// Check to make sure the group matches up.
				// Other data may vary based on special cases for the specific group
				// e.g. the sever may be permitted to allocate another turret in the
				// group if it is free.
				IF g_turretLockResponse.groupType = g_turretLockRequest.groupType
					CDEBUG1LN(DEBUG_NET_TURRET, "_TURRET_MANAGER_MAINTAIN_LOCKING **RESPONSE** Turret locked for our use.")
					PRINT_TURRET_TIME()
					PRINT_TURRET_OCC(g_turretLockResponse)
					
					_TURRET_MANAGER_SET_LOCK_STATE(eStateSuccess)	
				ELSE
					// Server said we couldn't use this turret.
					CDEBUG1LN(DEBUG_NET_TURRET, "_TURRET_MANAGER_MAINTAIN_LOCKING **RESPONSE** Turret already in use.")
					CDEBUG1LN(DEBUG_NET_TURRET, "   rqst  grp ", ENUM_TO_INT(g_turretLockRequest.groupType))
					CDEBUG1LN(DEBUG_NET_TURRET, "   rspns grp ", ENUM_TO_INT(g_turretLockResponse.groupType))
					PRINT_TURRET_TIME()

					_TURRET_MANAGER_SET_LOCK_STATE(eStateFail)
				ENDIF

				RESET_NET_TIMER(ref_data.requestSentTime)
			ELSE
				// If the timer expires...
				IF HAS_NET_TIMER_EXPIRED(ref_data.requestSentTime, ref_data.iRequestAttempts * ci_TURRET_RQST_SETTINGS_TIMEOUT_MS)
					// If we are allowed to send another request...
					IF bIgnoreRequestLimit
					OR ref_data.iRequestAttempts < ci_TURRET_RQST_SETTINGS_TRIES
						// Send another event using the same unique ID.
						SCRIPT_EVENT_REQUEST_PLAYER_DATA_ARGS eventArgs
						FILL_TURRET_REQUEST_DATA(g_turretLockRequest, ENUM_TO_INT(g_iTurretLockRequestUid), g_eTurretLockSearchType, g_iTurretLockBs, eventArgs.iArray)
						BROADCAST_REQUEST_PLAYER_DATA_ARGS(ref_data.pHost, PRDT_REQUEST_USE_OF_TURRET, eventArgs)
						ref_data.iRequestAttempts++
						
						CDEBUG1LN(DEBUG_NET_TURRET, "_TURRET_MANAGER_MAINTAIN_LOCKING ** TIMEOUT** Sending another request. Attempts = ",ref_data.iRequestAttempts, " Time (ms) = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_data.requestSentTime))
						PRINT_TURRET_TIME()
						PRINT_TURRET_RQST(eventArgs.iArray)
					ELSE
						// Failed to contact server.
						CDEBUG1LN(DEBUG_NET_TURRET, "_TURRET_MANAGER_MAINTAIN_LOCKING ** FAILED ** - Attempts = ",ref_data.iRequestAttempts, " Time (ms) = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_data.requestSentTime))
						PRINT_TURRET_TIME()
					
						_TURRET_MANAGER_SET_LOCK_STATE(eStateFail)		
						RESET_NET_TIMER(ref_data.requestSentTime)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Send a request...			
			// Save a timestamp & use it as a unique Id for the event
			REINIT_NET_TIMER(ref_data.requestSentTime, FALSE, TRUE)
			ref_data.iRequestAttempts = 1 // First attempt.
			
			// Setup & send event data
			SCRIPT_EVENT_REQUEST_PLAYER_DATA_ARGS eventArgs
			FILL_TURRET_REQUEST_DATA(g_turretLockRequest, ENUM_TO_INT(g_iTurretLockRequestUid), g_eTurretLockSearchType, g_iTurretLockBs, eventArgs.iArray)
						
			BROADCAST_REQUEST_PLAYER_DATA_ARGS(ref_data.pHost, PRDT_REQUEST_USE_OF_TURRET, eventArgs)
			
			CDEBUG1LN(DEBUG_NET_TURRET, "_TURRET_MANAGER_MAINTAIN_LOCKING  Sending initial request")
			PRINT_TURRET_TIME()
			PRINT_TURRET_RQST(eventArgs.iArray)
		ENDIF	
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC TURRET_MANAGER_CREATE_WIDGETS(TURRET_MANAGER_DATA& ref_data)
	ref_data.debug.widgetGroup = START_WIDGET_GROUP("Turret manager")
		ADD_WIDGET_BOOL("Dump server bd", ref_data.debug.bDumpServerData)		
	STOP_WIDGET_GROUP()
ENDPROC
PROC _TURRET_MANAGER_MAINTAIN_WIDGETS(TURRET_MANAGER_DATA& ref_data)
	IF ref_data.debug.bDumpServerData
		ref_data.debug.bDumpServerData = FALSE
		CDEBUG1LN(DEBUG_NET_TURRET, "*** DUMPING SERVER INFO ***")
		INT i
		REPEAT COUNT_OF(GlobalServerBD.turrets.players) i
			PRINT_PLAYER_TURRET_OCC(i)
		ENDREPEAT
	ENDIF
ENDPROC
#ENDIF //IS_DEBUG_BUILD

/// PURPOSE:
///    Per frame turret manager update. All players should be running this proc once every frame. 
PROC TURRET_MANAGER_UPDATE(TURRET_MANAGER_DATA& ref_data, PARTICIPANT_INDEX piHost)
	IF NATIVE_TO_INT(piHost) < 0
	OR NOT NETWORK_IS_PARTICIPANT_ACTIVE(piHost)
		CDEBUG1LN(DEBUG_NET_TURRET, "*** Freemode host migrating? TURRET_MANAGER_UPDATE(.., PLAYER_INDEX piHost) piHost invalid *** piHost = ", NATIVE_TO_INT(piHost))
		EXIT
	ENDIF
	
	ref_data.pHost = NETWORK_GET_PLAYER_INDEX(piHost)
	
	#IF IS_DEBUG_BUILD
	_TURRET_MANAGER_MAINTAIN_WIDGETS(ref_data)
	#ENDIF //IS_DEBUG_BUILD
	
	SWITCH g_eTurretManagerState
		CASE TLLS_UNLOCKING
			_TURRET_MANAGER_MAINTAIN_LOCK_REQUEST(ref_data)
		BREAK
			
		CASE TLLS_LOCKING
			_TURRET_MANAGER_MAINTAIN_LOCK_REQUEST(ref_data)
		BREAK	
		
		CASE TLLS_LOCKED
			// Wait for global state update...
		BREAK
		
		CASE TLLS_UNLOCKED
			// If the server seems to think we're using a turret make sure they know we are not.
			IF IS_PLAYER_USING_ANY_TURRET(PLAYER_ID())
				CDEBUG1LN(DEBUG_NET_TURRET, "********** SERVER THINKS WE'RE IN A TURRET ******************")
				_TURRET_MANAGER_SET_LOCK_STATE(TLLS_UNLOCKING)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Per frame update for turret manager server host only.
PROC TURRET_MANAGER_SERVER_UPDATE(INT iServerPlayerStagger, PLAYER_INDEX piPlayer, BOOL bActive)
	// Server needs to mark player as not using any turret if they leave etc.
	IF GlobalServerBD.turrets.players[iServerPlayerStagger].groupType <> TGT_NONE	
	AND (NOT bActive
	OR NOT IS_NET_PLAYER_OK(piPlayer))
		CDEBUG1LN(DEBUG_NET_TURRET, "[SERVER] TURRET_MANAGER_SERVER_UPDATE Setting groupType for player ", iServerPlayerStagger, " to TGT_NONE (0)")
		GlobalServerBD.turrets.players[iServerPlayerStagger].groupType = TGT_NONE
	ENDIF
ENDPROC



