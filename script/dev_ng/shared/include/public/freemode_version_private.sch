
USING "globals.sch"
USING "mp_globals.sch"
USING "freemode_version.sch"
USING "net_stat_system.sch"

#IF FEATURE_GEN9_EXCLUSIVE
USING "net_mp_intro.sch"
#ENDIF

PROC NET_SESSION_SET_GAMEMODE(MP_GAMEMODE gamemode)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NET_SESSION_SET_GAME_MODE - overriding with ARCADE")
				NETWORK_SESSION_SET_GAMEMODE(GetArcadeGamemodeIntForCodeNatives())
				EXIT
			ENDIF
		ENDIF
	#ENDIF	
		
	PRINTLN("NET_SESSION_SET_GAME_MODE - calling with ", ENUM_TO_INT(gamemode))
	NETWORK_SESSION_SET_GAMEMODE(ENUM_TO_INT(gamemode))

ENDPROC

FUNC BOOL NET_SESSION_HOST(MP_GAMEMODE gamemode, INT nMaxPlayers, BOOL bIsPrivate)
	
	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NET_SESSION_HOST - overriding with ARCADE")
				RETURN NETWORK_SESSION_HOST(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, bIsPrivate)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NET_SESSION_HOST - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_HOST(ENUM_TO_INT(gamemode), nMaxPlayers, bIsPrivate)
ENDFUNC

FUNC BOOL NET_SESSION_HOST_CLOSED(MP_GAMEMODE gamemode, INT nMaxPlayers)
	
	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NET_SESSION_HOST_CLOSED - overriding with ARCADE")
				RETURN NETWORK_SESSION_HOST_CLOSED(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NET_SESSION_HOST_CLOSED - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_HOST_CLOSED(ENUM_TO_INT(gamemode), nMaxPlayers)
ENDFUNC

#IF FEATURE_FREEMODE_ARCADE
FUNC BOOL SHOULD_LAUNCH_TRANSITION_SESSION_FOR_ARCADE_MODE()
	RETURN g_sArcadeLobbyTransitionSessionData.bTransitionSessionInitialised
ENDFUNC

INT iArcadeLaunchTransitionStage
//SCRIPT_TIMER stLaunchTimer
FUNC BOOL LAUNCH_TRANSITION_FOR_ARCADE_MODE(INT iMaxPlayers, BOOL bShouldMatchmake)
	IF g_sArcadeLobbyGroup.bIAmGroupLeader
		PRINTLN("[GROUPING] LAUNCH_TRANSITION_FOR_ARCADE_MODE - Leader")
		
		SWITCH iArcadeLaunchTransitionStage
			CASE 0	// Setup matchmaking rules
			
				UGC_SET_USING_OFFLINE_CONTENT(FALSE)
				g_b_HasJoinedSessionHappened = TRUE
				NETWORK_SESSION_SET_SCRIPT_VALIDATE_JOIN()
				NETWORK_SESSION_SET_MATCHMAKING_GROUP(MM_GROUP_FREEMODER)
				NETWORK_SESSION_ADD_ACTIVE_MATCHMAKING_GROUP(MM_GROUP_FREEMODER)		
				NETWORK_SESSION_SET_MATCHMAKING_GROUP_MAX(MM_GROUP_FREEMODER, NUM_NETWORK_PLAYERS)
				
				PRINTLN("[GROUPING] LAUNCH_TRANSITION_FOR_ARCADE_MODE - SETUP_MATCHMAKING_RULES(GAMEMODE_ARCADE)")
				iArcadeLaunchTransitionStage++
				RETURN FALSE
				
			BREAK
						
			CASE 1	// Transition to game
				IF NOT NETWORK_DO_TRANSITION_FROM_ACTIVITY(GetArcadeGamemodeIntForCodeNatives(), g_sArcadeLobbyGroup.GroupMemberGamerHandles, g_sArcadeLobbyGroup.iNumGroupMembers, bShouldMatchmake, iMaxPlayers, ENUM_TO_INT(MM_FLAG_TO_GAME_VIA_TRANSITION))
//				IF NOT NETWORK_DO_TRANSITION_TO_NEW_FREEMODE(g_sArcadeLobbyGroup.GroupMemberGamerHandles, g_sArcadeLobbyGroup.iNumGroupMembers, iMaxPlayers, TRUE, DEFAULT, ENUM_TO_INT(HOST_FLAG_VIA_TRANSITION_LOBBY))
					PRINTLN("[GROUPING] LAUNCH_TRANSITION_FOR_ARCADE_MODE - waiting for NETWORK_DO_TRANSITION_FROM_ACTIVITY...")
					RETURN FALSE
				ENDIF
			BREAK
		ENDSWITCH
	
	ELSE
		PRINTLN("[GROUPING] LAUNCH_TRANSITION_FOR_ARCADE_MODE - Member")
		
		IF !g_sTransitionSessionData.bGotToGameFinished
		
			PRINTLN("[GROUPING] LAUNCH_TRANSITION_FOR_ARCADE_MODE - Waiting for transition to game")
			RETURN FALSE
		ENDIF
				
	ENDIF
	
	PRINTLN("[GROUPING] LAUNCH_TRANSITION_FOR_ARCADE_MODE - return true")
	RETURN TRUE
ENDFUNC
#ENDIF

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL CALL_MP_INTRO_MATCHMAKING_FLAGS()

	IF IS_PLAYER_ON_MP_INTRO(TRUE) 
		PRINTLN("NET_SESSION_GET_MATCHMAKING_FLAGS - CALL_MP_INTRO_MATCHMAKING_FLAGS - IS_PLAYER_ON_MP_INTRO(TRUE)")
		RETURN TRUE
	ENDIF
	
	IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_CREATION_ENTER_SESSION
	AND GET_PENDING_CHARACTER_CAREER_FOR_NEW_CHARACTER() != CHARACTER_CAREER_NONE
		PRINTLN("NET_SESSION_GET_MATCHMAKING_FLAGS - CALL_MP_INTRO_MATCHMAKING_FLAGS - GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_CREATION_ENTER_SESSION")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF // FEATURE_GEN9_EXCLUSIVE

FUNC MM_FLAGS NET_SESSION_GET_MATCHMAKING_FLAGS(BOOL bConsiderBlacklisted)

	MM_FLAGS eFlags
	
	IF bConsiderBlacklisted
		PRINTLN("NET_SESSION_GET_MATCHMAKING_FLAGS - bConsiderBlacklisted - Adding MM_FLAG_ALLOW_BLACKLISTED")
		eFlags |= MM_FLAG_ALLOW_BLACKLISTED
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	BOOL bOnIntro = CALL_MP_INTRO_MATCHMAKING_FLAGS()	
	IF bOnIntro
		PRINTLN("NET_SESSION_GET_MATCHMAKING_FLAGS - IS_PLAYER_ON_MP_INTRO - Adding MM_FLAG_EXPANDED_INTRO_FLOW")
		eFlags |= MM_FLAG_EXPANDED_INTRO_FLOW
		PRINTLN("NET_SESSION_GET_MATCHMAKING_FLAGS - IS_PLAYER_ON_MP_INTRO - Adding MM_FLAG_IS_BOSS")
		eFlags |= MM_FLAG_IS_BOSS
	ENDIF
	#ENDIF // FEATURE_GEN9_EXCLUSIVE
	
	IF DOES_PLAYER_HAVE_BOSS_UUID()
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT bOnIntro
	#ENDIF // FEATURE_GEN9_EXCLUSIVE
		PRINTLN("NET_SESSION_GET_MATCHMAKING_FLAGS - DOES_PLAYER_HAVE_BOSS_UUID - Adding MM_FLAG_IS_BOSS")
		eFlags |= MM_FLAG_IS_BOSS
	ENDIF
	
	RETURN eFlags

ENDFUNC

FUNC BOOL NET_SESSION_ENTER(MP_GAMEMODE gamemode, INT nMaxPlayers, BOOL isPrivate, BOOL bConsiderBlacklisted = TRUE)

	UNUSED_PARAMETER(isPrivate)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				PRINTLN("NET_SESSION_ENTER - ARCADE MODE - nMaxPlayers = ", nMaxPlayers)
				PRINTLN("NET_SESSION_ENTER - ARCADE MODE - isPrivate = ", isPrivate)
				PRINTLN("NET_SESSION_ENTER - ARCADE MODE - bConsiderBlacklisted = ", bConsiderBlacklisted)
			
				IF SHOULD_LAUNCH_TRANSITION_SESSION_FOR_ARCADE_MODE()
					PRINTLN("NET_SESSION_ENTER - ARCADE MODE - calling LAUNCH_TRANSITION_FOR_ARCADE_MODE")
					RETURN LAUNCH_TRANSITION_FOR_ARCADE_MODE(nMaxPlayers, !isPrivate)
				ELSE
					DEBUG_PRINTCALLSTACK()
					PRINTLN("NET_SESSION_ENTER - ARCADE MODE - calling NETWORK_SESSION_DO_FREEROAM_QUICKMATCH")	
					NETWORK_SESSION_DO_FREEROAM_QUICKMATCH(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
				ENDIF
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_DO_FREEROAM_QUICKMATCH - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_DO_FREEROAM_QUICKMATCH(ENUM_TO_INT(gamemode), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
ENDFUNC

FUNC BOOL NET_SESSION_FRIEND_MATCHMAKING(MP_GAMEMODE gamemode, INT nMaxPlayers, BOOL bConsiderBlacklisted = TRUE)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_FRIEND_MATCHMAKING - overriding with ARCADE")
				RETURN NETWORK_SESSION_DO_FRIEND_MATCHMAKING(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_DO_FRIEND_MATCHMAKING - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_DO_FRIEND_MATCHMAKING(ENUM_TO_INT(gamemode), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
ENDFUNC

FUNC BOOL NET_SESSION_SOCIAL_MATCHMAKING(STRING szQuery, STRING szParams, MP_GAMEMODE gamemode, INT nMaxPlayers, BOOL bConsiderBlacklisted = TRUE)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_SOCIAL_MATCHMAKING - overriding with ARCADE")
				RETURN NETWORK_SESSION_DO_SOCIAL_MATCHMAKING(szQuery, szParams, GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_DO_SOCIAL_MATCHMAKING - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_DO_SOCIAL_MATCHMAKING(szQuery, szParams, ENUM_TO_INT(gamemode), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
ENDFUNC

FUNC BOOL NET_SESSION_CREW_MATCHMAKING(INT nCrewID, MP_GAMEMODE gamemode, INT nMaxPlayers, BOOL bConsiderBlacklisted = TRUE)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_CREW_MATCHMAKING - overriding with ARCADE")
				RETURN NETWORK_SESSION_DO_CREW_MATCHMAKING(nCrewID, GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_DO_CREW_MATCHMAKING - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_DO_CREW_MATCHMAKING(nCrewID, ENUM_TO_INT(gamemode), nMaxPlayers, NET_SESSION_GET_MATCHMAKING_FLAGS(bConsiderBlacklisted))
ENDFUNC

FUNC BOOL NET_SESSION_ACTIVITY_QUICKMATCH(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nActivityType, INT nActivityID)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_ACTIVITY_QUICKMATCH - overriding with ARCADE")
				RETURN NETWORK_SESSION_DO_ACTIVITY_QUICKMATCH(etArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nActivityType, nActivityID, NET_SESSION_GET_MATCHMAKING_FLAGS(FALSE))
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_DO_ACTIVITY_QUICKMATCH - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_DO_ACTIVITY_QUICKMATCH(ENUM_TO_INT(gamemode), nMaxPlayers, nActivityType, nActivityID, NET_SESSION_GET_MATCHMAKING_FLAGS(FALSE))
ENDFUNC

FUNC BOOL NET_SESSION_HOST_FRIENDS_ONLY(MP_GAMEMODE gamemode, INT nMaxPlayers)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_HOST_FRIENDS_ONLY - overriding with ARCADE")
				RETURN NETWORK_SESSION_HOST_FRIENDS_ONLY(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_HOST_FRIENDS_ONLY - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_HOST_FRIENDS_ONLY(ENUM_TO_INT(gamemode), nMaxPlayers)
ENDFUNC

FUNC BOOL NET_SESSION_HOST_CLOSED_CREW(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nUniqueCrewLimit, INT nCrewLimitMaxMembers)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_HOST_CLOSED_CREW - overriding with ARCADE")
				RETURN NETWORK_SESSION_HOST_CLOSED_CREW(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nUniqueCrewLimit, nCrewLimitMaxMembers)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_HOST_CLOSED_CREW - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_SESSION_HOST_CLOSED_CREW(ENUM_TO_INT(gamemode), nMaxPlayers, nUniqueCrewLimit, nCrewLimitMaxMembers)
ENDFUNC

PROC NET_SESSION_HOST_SINGLE_PLAYER(MP_GAMEMODE gamemode)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_SESSION_HOST_SINGLE_PLAYER - overriding with ARCADE")
				NETWORK_SESSION_HOST_SINGLE_PLAYER(GetArcadeGamemodeIntForCodeNatives())
				EXIT
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_SESSION_HOST_SINGLE_PLAYER - calling with ", ENUM_TO_INT(gamemode))
	NETWORK_SESSION_HOST_SINGLE_PLAYER(ENUM_TO_INT(gamemode))
ENDPROC

FUNC BOOL NET_HOST_TRANSITION(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nActivityType, INT nActivityID, BOOL bIsPrivate, BOOL bIsOpen, BOOL bFromMatchmaking = FALSE, ACTIVITY_ISLAND nActivityIsland = ACTIVITY_ISLAND_GENERAL, JOB_CONTENT_CREATOR nContentCreator = JOB_CONTENT_ROCKSTAR_CREATED, INT nHostFlags = 0)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_HOST_TRANSITION - overriding with ARCADE")
				RETURN NETWORK_HOST_TRANSITION(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nActivityType, nActivityID, bIsPrivate, bIsOpen, bFromMatchmaking, nActivityIsland, nContentCreator, nHostFlags)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_HOST_TRANSITION - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_HOST_TRANSITION(ENUM_TO_INT(gamemode), nMaxPlayers, nActivityType, nActivityID, bIsPrivate, bIsOpen, bFromMatchmaking, nActivityIsland, nContentCreator, nHostFlags)
ENDFUNC

FUNC BOOL NET_HOST_TRANSITION_FRIENDS_ONLY(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nActivityType, INT nActivityID, ACTIVITY_ISLAND nActivityIsland = ACTIVITY_ISLAND_GENERAL, JOB_CONTENT_CREATOR nContentCreator = JOB_CONTENT_ROCKSTAR_CREATED)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_HOST_TRANSITION_FRIENDS_ONLY - overriding with ARCADE")
				RETURN NETWORK_HOST_TRANSITION_FRIENDS_ONLY(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nActivityType, nActivityID, nActivityIsland, nContentCreator)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_HOST_TRANSITION_FRIENDS_ONLY - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_HOST_TRANSITION_FRIENDS_ONLY(ENUM_TO_INT(gamemode), nMaxPlayers, nActivityType, nActivityID, nActivityIsland, nContentCreator)
ENDFUNC

FUNC BOOL NET_HOST_TRANSITION_CLOSED_CREW(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nUniqueCrewLimit, INT nCrewLimitMaxMembers, INT nActivityType, INT nActivityID, ACTIVITY_ISLAND nActivityIsland = ACTIVITY_ISLAND_GENERAL, JOB_CONTENT_CREATOR nContentCreator = JOB_CONTENT_ROCKSTAR_CREATED)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_HOST_TRANSITION_CLOSED_CREW - overriding with ARCADE")
				RETURN NETWORK_HOST_TRANSITION_CLOSED_CREW(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nUniqueCrewLimit, nCrewLimitMaxMembers, nActivityType, nActivityID, nActivityIsland, nContentCreator)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_HOST_TRANSITION_CLOSED_CREW - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_HOST_TRANSITION_CLOSED_CREW(ENUM_TO_INT(gamemode), nMaxPlayers, nUniqueCrewLimit, nCrewLimitMaxMembers, nActivityType, nActivityID, nActivityIsland, nContentCreator)
ENDFUNC

FUNC BOOL NET_DO_TRANSITION_QUICKMATCH(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nActivityType, INT nActivityID, INT nMmFlags = 0, ACTIVITY_ISLAND nActivityIsland = ACTIVITY_ISLAND_INVALID)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_DO_TRANSITION_QUICKMATCH - overriding with ARCADE")
				RETURN NETWORK_DO_TRANSITION_QUICKMATCH(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nActivityType, nActivityID, nMmFlags, nActivityIsland)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_DO_TRANSITION_QUICKMATCH - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_DO_TRANSITION_QUICKMATCH(ENUM_TO_INT(gamemode), nMaxPlayers, nActivityType, nActivityID, nMmFlags, nActivityIsland)
ENDFUNC

FUNC BOOL NET_DO_TRANSITION_QUICKMATCH_ASYNC(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nActivityType, INT nActivityID, INT nMmFlags = 0, ACTIVITY_ISLAND nActivityIsland = ACTIVITY_ISLAND_INVALID)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_DO_TRANSITION_QUICKMATCH_ASYNC - overriding with ARCADE")
				RETURN NETWORK_DO_TRANSITION_QUICKMATCH_ASYNC(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nActivityType, nActivityID, nMmFlags, nActivityIsland)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_DO_TRANSITION_QUICKMATCH_ASYNC - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_DO_TRANSITION_QUICKMATCH_ASYNC(ENUM_TO_INT(gamemode), nMaxPlayers, nActivityType, nActivityID, nMmFlags, nActivityIsland)
ENDFUNC

FUNC BOOL NET_DO_TRANSITION_QUICKMATCH_WITH_GROUP(MP_GAMEMODE gamemode, INT nMaxPlayers, INT nActivityType, INT nActivityID, STRUCT_TO_FREEMODE_GAMERS &hGamers, INT nNumGamers, INT nMmFlags = 0, ACTIVITY_ISLAND nActivityIsland = ACTIVITY_ISLAND_INVALID)

	#IF FEATURE_FREEMODE_ARCADE
		IF (gamemode = GAMEMODE_FM)
			IF IS_FREEMODE_ARCADE()
				DEBUG_PRINTCALLSTACK()
				PRINTLN("NETWORK_DO_TRANSITION_QUICKMATCH_WITH_GROUP - overriding with ARCADE")
				RETURN NETWORK_DO_TRANSITION_QUICKMATCH_WITH_GROUP(GetArcadeGamemodeIntForCodeNatives(), nMaxPlayers, nActivityType, nActivityID, hGamers, nNumGamers, nMmFlags, nActivityIsland)
			ENDIF
		ENDIF
	#ENDIF		
	
	PRINTLN("NETWORK_DO_TRANSITION_QUICKMATCH_WITH_GROUP - calling with ", ENUM_TO_INT(gamemode))
	RETURN NETWORK_DO_TRANSITION_QUICKMATCH_WITH_GROUP(ENUM_TO_INT(gamemode), nMaxPlayers, nActivityType, nActivityID, hGamers, nNumGamers, nMmFlags, nActivityIsland)
ENDFUNC









