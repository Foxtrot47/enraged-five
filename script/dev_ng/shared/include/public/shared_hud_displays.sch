

USING "commands_graphics.sch"
USING "net_include.sch"
USING "Screen_placements.sch"
USING "Cellphone_public.sch"
USING "fm_in_Corona_header.sch"
//USING "fm_playlist_header.sch"

//USING "Net_Spectator_cam_common.sch"
//USING "net_spectator_cam.sch"

CONST_INT MAX_NUM_CHECKPOINT_SPRITE 4
CONST_INT MAX_NUM_CHECKPOINT_RECT 1
CONST_INT MAX_NUM_CHECKPOINT_TEXT 5

CONST_INT FIVE_ICON_SCORE_BAR_DELAY_TIME	3000

CONST_FLOAT PLAYERNAME_TITLE_OFFSET -0.004
CONST_FLOAT RIGHT_EDGE_CORONA_OFFSET -0.113

CONST_FLOAT TITLE_NON_ROMANIC_OFFSET -0.008
CONST_FLOAT TITLE_NON_ROMANIC_OFFSET_CHINESE -0.012



CONST_FLOAT TITLE_NON_ROMANIC_OFFSET_LITTLE -0.003
CONST_FLOAT TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE -0.009

CONST_FLOAT TITLE_NON_ROMANIC_OFFSET_LITTLE_DOTS -0.007

CONST_INT TIMER_METER_NON_SELECTED_ALPHA 51

ENUM UIELEMENTS
	UIELEMENTS_NOTSET = 0,
	UIELEMENTS_BOTTOMRIGHT,
	UIELEMENTS_MIDDLERIGHT
ENDENUM


CONST_INT NUMBER_SPACES_FOR_HUD_ELEMENTS 12

CONST_FLOAT TIMER_OVERLAY_X 0.079
CONST_FLOAT TIMER_OVERLAY_W 0.157


CONST_FLOAT TIMER_OVERLAY_X_NON_ROMANIC_OFFSET -0.025 
CONST_FLOAT TIMER_OVERLAY_W_NON_ROMANIC_OFFSET 0.050

CONST_FLOAT TIMER_OVERLAY_Y_TIME 0.008
CONST_FLOAT TIMER_OVERLAY_H_TIME 0.036


CONST_FLOAT TIMER_OVERLAY_Y_SCORE 0.008
CONST_FLOAT TIMER_OVERLAY_H_SCORE 0.036

CONST_FLOAT TIMER_OVERLAY_Y_SCORESML 0.01
CONST_FLOAT TIMER_OVERLAY_H_SCORESML 0.033

CONST_FLOAT TIMER_OVERLAY_Y_BIGNUM 0.002 //-0.003+0.005
CONST_FLOAT TIMER_OVERLAY_H_BIGNUM 0.049 //0.056-0.007

CONST_FLOAT TIMER_OVERLAY_Y_BIGNUMSML 0.009
CONST_FLOAT TIMER_OVERLAY_H_BIGNUMSML 0.035

CONST_FLOAT TIMER_OVERLAY_Y_BIGNUMPLACE 0.002 //-0.003+0.005
CONST_FLOAT TIMER_OVERLAY_H_BIGNUMPLACE 0.048 //0.058


CONST_FLOAT TIMER_OVERLAY_Y_SINGLEBIGNUM 0.002 //-0.002
CONST_FLOAT TIMER_OVERLAY_H_SINGLEBIGNUM 0.049 //0.057
CONST_FLOAT TIMER_OVERLAY_Y_SPRITES 0.012
CONST_FLOAT TIMER_OVERLAY_H_SPRITES 0.028

CONST_FLOAT TIMER_OVERLAY_Y_BIGMETER 0.008
CONST_FLOAT TIMER_OVERLAY_H_BIGMETER 0.036

CONST_FLOAT TIMER_OVERLAY_Y_SPRITEMETER 0.008
CONST_FLOAT TIMER_OVERLAY_H_SPRITEMETER 0.036

CONST_FLOAT TIMER_OVERLAY_Y_BIGTIME 0.002
CONST_FLOAT TIMER_OVERLAY_H_BIGTIME 0.048

CONST_FLOAT CHECKPOINT_DOT_SPACING -0.0094
CONST_FLOAT CHECKPOINT_DOT_W 0.012
CONST_FLOAT CHECKPOINT_DOT_H 0.023

CONST_INT TIMER_OVERLAY_ALPHA HUD_COLOUR_INGAME_BG

CONST_INT CHECKPOINT_TIMER_SWITCH 9


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// PUBLIC FUNCTIONS /////////////////////////////////////////////////////////////////////////////


FUNC BOOL IS_RANKUP_RUNNING_BAR_AND_DISPLAY()
	RETURN MPGlobalsHud.bIsaRankupRunningBarAndBanner
ENDFUNC

PROC SET_RANKUP_RUNNING_BAR_AND_DISPLAY(BOOL isRunning)

	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF G_bTurnOnAllHUDBlockers
			bPrint = TRUE
		ENDIF
		IF MPGlobalsHud.bIsaRankupRunningBarAndBanner <> MPGlobalsHud.bIsaRankupRunningBarAndBannerLastFrame
			
			bPrint = TRUE
		ENDIF
		
		IF bPrint
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("<<< SET_RANKUP_RUNNING_BAR_AND_DISPLAY called with ")NET_PRINT_BOOL(isRunning)NET_PRINT(" >>>")
		ENDIF
	#ENDIF
	
	

	MPGlobalsHud.bIsaRankupRunningBarAndBanner = isRunning
	
	#IF IS_DEBUG_BUILD
		MPGlobalsHud.bIsaRankupRunningBarAndBannerLastFrame = isRunning
	#ENDIF
ENDPROC


FUNC BOOL CAN_DISPLAY_DPADDOWN_PARTS()

//	IF ((IS_RANKUP_RUNNING_BAR_AND_DISPLAY() AND IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR) = FALSE)
//	OR IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR))
		RETURN TRUE
//	ENDIF

RETURN FALSE
ENDFUNC


FUNC GFX_DRAW_ORDER SET_GFX_TIMERS_DRAW_ORDER()
	//GFX_ORDER_AFTER_HUD - Works
	//GFX_ORDER_BEFORE_HUD - Broken Damage Meter

	GFX_DRAW_ORDER anOrder =  GFX_ORDER_BEFORE_HUD
	/*IF g_bDeathmatchPlayerDead
		anOrder = GFX_ORDER_AFTER_FADE
	ENDIF*/
	IF MPGlobalsScoreHud.bSniperScopeOn 
		anOrder = GFX_ORDER_AFTER_FADE
	ENDIF

	RETURN anOrder
ENDFUNC
PROC RESET_GFX_TIMERS_DRAW_ORDER()
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)

ENDPROC

PROC UPDATE_ALL_PROGRESSHUD_LAST_FRAME()
	INT I
	FOR I = 0 TO NUMBER_OF_PROGRESSHUD_ELEMENTS-1
		MPGlobalsScoreHud.ProgressHud_LastFrameBitset[I] = MPGlobalsScoreHud.ProgressHud_ActivationBitset[I]
	ENDFOR
ENDPROC

PROC FORCE_TIMER_ORDER_REFRESH()
	MPGlobalsScoreHud.ProgressHud_ForceReset = TRUE
ENDPROC

FUNC BOOL HAS_ANY_PROGRESSHUD_ACTIVATION_CHANGED()
	INT I
	FOR I = 0 TO NUMBER_OF_PROGRESSHUD_ELEMENTS-1
		IF MPGlobalsScoreHud.ProgressHud_LastFrameBitset[I] <> MPGlobalsScoreHud.ProgressHud_ActivationBitset[I]
			RETURN TRUE
		ENDIF
	ENDFOR
	
	IF MPGlobalsScoreHud.ProgressHud_ForceReset 
		MPGlobalsScoreHud.ProgressHud_ForceReset  = FALSE
		RETURN TRUE
	ENDIF
	

	RETURN FALSE
ENDFUNC


PROC UPDATE_PROGRESSHUD_LAST_FRAME(PROGRESSHUD_ELEMENTS aProgressHudPart)
	MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(aProgressHudPart)] = MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(aProgressHudPart)]
ENDPROC

FUNC BOOL HAS_PROGRESSHUD_ACTIVATION_CHANGED(PROGRESSHUD_ELEMENTS aProgressHudPart)
	IF MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(aProgressHudPart)] <> MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(aProgressHudPart)]
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	SET_BIT(MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDPROC

//PROC SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
//	CLEAR_BIT(MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(aProgressHudPart)], Index)
//ENDPROC

FUNC BOOL IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	RETURN IS_BIT_SET(MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDFUNC

FUNC BOOL IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELEMENTS aProgressHudPart)
	IF MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(aProgressHudPart)] > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_PROGRESSHUD_BITSET()
		
INT I

	FOR I = 0 TO NUMBER_OF_PROGRESSHUD_ELEMENTS-1
		 MPGlobalsScoreHud.ProgressHud_ActivationBitset[I] = 0	
	ENDFOR
		
ENDPROC

PROC SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	SET_BIT(MPGlobalsScoreHud.ProgressHud_InitBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDPROC

PROC SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	CLEAR_BIT(MPGlobalsScoreHud.ProgressHud_InitBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDPROC

FUNC BOOL IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	RETURN IS_BIT_SET(MPGlobalsScoreHud.ProgressHud_InitBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDFUNC

FUNC BOOL IS_PROGRESSHUD_INIT_DONE(PROGRESSHUD_ELEMENTS aProgressHudPart)
	IF MPGlobalsScoreHud.ProgressHud_InitBitset[ENUM_TO_INT(aProgressHudPart)] > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_PROGRESSHUD_INIT_BITSET()
	
INT I
	FOR I = 0 TO NUMBER_OF_PROGRESSHUD_ELEMENTS-1
		 MPGlobalsScoreHud.ProgressHud_InitBitset[I] = 0	
	ENDFOR
		
ENDPROC


PROC SET_PROGRESSHUD_EXTENDDISPLAY(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	SET_BIT(MPGlobalsScoreHud.ProgressHud_ExtendDisplayBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDPROC

PROC CLEAR_PROGRESSHUD_EXTENDDISPLAY(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	CLEAR_BIT(MPGlobalsScoreHud.ProgressHud_ExtendDisplayBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDPROC

FUNC BOOL IS_PROGRESSHUD_EXTENDDISPLAY_SET_INDEXED(PROGRESSHUD_ELEMENTS aProgressHudPart, INT Index)
	RETURN IS_BIT_SET(MPGlobalsScoreHud.ProgressHud_ExtendDisplayBitset[ENUM_TO_INT(aProgressHudPart)], Index)
ENDFUNC

FUNC BOOL IS_PROGRESSHUD_EXTENDDISPLAY_SET(PROGRESSHUD_ELEMENTS aProgressHudPart)
	IF MPGlobalsScoreHud.ProgressHud_ExtendDisplayBitset[ENUM_TO_INT(aProgressHudPart)] > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_PROGRESSHUD_EXTENDDISPLAY_BITSET()
	
INT I
	FOR I = 0 TO NUMBER_OF_PROGRESSHUD_ELEMENTS-1
		 MPGlobalsScoreHud.ProgressHud_ExtendDisplayBitset[I] = 0	
	ENDFOR
		
ENDPROC



FUNC FLOAT CAST_METRES_TO_MILES(FLOAT fMetres)
	RETURN(fMetres * 0.000621371192)
ENDFUNC


FUNC BOOL IS_BOTTOM_RIGHT_AREA_FREE()
//	IF g_sSelectorUI.bOnScreen = TRUE
//		RETURN FALSE
//	ENDIF
RETURN TRUE
ENDFUNC


FUNC TEXT_LABEL_63 GET_POSITION_STRING_WITH_NUMBER(INT Position)
	TEXT_LABEL_63 tl_63 = ""
	IF Position > 0
	
		INT Units
		
		Position = Position%100 //Shave off third column
		Units = Position%10
		

		IF Units = 1
			tl_63 += "TIMER_ST"	
		ELIF Units = 2
			tl_63 += "TIMER_ND"
		ELIF Units = 3
			tl_63 += "TIMER_RD"
		ELSE
			tl_63 += "TIMER_TH"
		ENDIF
		
		IF Position > 9
		AND Position < 21
			 tl_63 = ""
			tl_63 += "TIMER_TH"
		ENDIF
	
	ENDIF
	RETURN tl_63
ENDFUNC

FUNC TEXT_LABEL_63 GET_POSITION_STRING(INT Position)
	TEXT_LABEL_63 tl_63 = ""
	IF Position > 0
		INT Units
		
		Position = Position%100 //Shave off third column
		Units = Position%10
				
		IF Units = 1
			tl_63 += "TIMER_NST"	
		ELIF Units = 2
			tl_63 += "TIMER_NND"
		ELIF Units = 3
			tl_63 += "TIMER_NRD"
		ELSE
			tl_63 += "TIMER_NTH"
		ENDIF
		
		
		
		IF Position > 9
		AND Position < 21
			 tl_63 = ""
			tl_63 += "TIMER_NTH"
		ENDIF
	
		 
		
	ENDIF
	RETURN tl_63
ENDFUNC




///// PURPOSE:
/////     Draws a Timer hud in the bottom right of the screen to be used in races
/////   
/// PARAMS:
///    TimerRunning - The time the race will run for 
///    RacePos - The players current position
///    MaxRacePos - the number of racers in the race
///    iPlayerTeam - Pass in the players gang so the lap meter can match the gang colour.
///    LapNum - the number of laps completed
///    MaxLaps - the maximum number of laps to complete
///    ExtraTime - If the player is to be given or subtracing extra time from the TimerRunning amount, this shows up a -num or +num
///    FlashTime - how long you want the hud to flash. Only does all the hud, let Brenda know if you need this indivdual
///    Title - The text next to the TimerRunning, defaults to "TIMER" if left blank
///    FixedTitle - The title for the fixed timer, defaults to "BEST TIME" if left blank
///    Medal - adds text and colouring for the fixed timer. Used if the fixed timer is telling the player "this time will get you a silver medal"
//PROC DRAW_RACE_TIMER_HUD(INT TimerRunning = -1, INT RacePos = 0, INT MaxRacePos = 0, INT iPlayerTeam = -1, INT LapNum = 0, INT MaxLaps = 0, INT ExtraTime = 0, INT BestTime = -1, INT FlashTime = 0, STRING Title = NULL, STRING FixedTitle = NULL, PODIUMPOS Medal = PODIUMPOS_NONE )
//				
//	IF TimerRunning = 0
//	ENDIF
//	IF RacePos = 0
//	ENDIF
//	IF MaxRacePos = 0
//	ENDIF
//	IF iPlayerTeam = 9
//	ENDIF
//	IF LapNum = 9
//	ENDIF
//	IF MaxLaps = 9
//	ENDIF
//	IF BestTime = 9
//	ENDIF
//	IF FlashTime = 9
//	ENDIF
//	IF ExtraTime = 9
//	ENDIF
//	IF Medal = PODIUMPOS_NONE 
//	ENDIF
//	IF IS_STRING_EMPTY_HUD(Title)
//	ENDIF
//	IF IS_STRING_EMPTY_HUD(FixedTitle)
//	ENDIF
//	
//ENDPROC


FUNC BOOL IS_LANGUAGE_NON_ROMANIC()
	
	
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED 
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


PROC RESET_GENERIC_NUMBER_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericNumber_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericNumber_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_NUMBER_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericNumber_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericNumber_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_SCORE_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericScore_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericScore_MiniHud[Index])
ENDPROC

PROC RESET_GENERIC_SCORE_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericScore_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericScore_MiniHud[Index])
ENDPROC

PROC RESET_GENERIC_TIMER_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericTimer_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericTimer_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_TIMER_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericTimer_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericTimer_MiniHud[Index])
ENDPROC

PROC RESET_GENERIC_DOUBLE_NUMBER_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericDoubleNumber_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericDoubleNumber_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumber_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumber_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_DOUBLE_NUMBER_PLACE_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericDoubleNumberPlace_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericDoubleNumberPlace_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_DOUBLE_NUMBER_PLACE_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumberPlace_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumberPlace_MiniHud[Index])
	
ENDPROC

PROC RESET_GENERIC_CHECKPOINT_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericCheckpoint_Hud[Index] )
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericCheckpoint_MiniHud[Index] )
	
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_TitleNumber[Index] = -1
ENDPROC

PROC RESET_GENERIC_CHECKPOINT_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericCheckpoint_Hud[Index] )
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericCheckpoint_MiniHud[Index] )
	
ENDPROC

PROC RESET_GENERIC_METER_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericMeter_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericMeter_MiniHud[Index])
	
	MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_TitleNumber[Index] = -1
	
ENDPROC

PROC RESET_GENERIC_METER_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericMeter_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericMeter_MiniHud[Index])	
ENDPROC

PROC RESET_GENERIC_ELIMINATION_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericElimination_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericElimination_MiniHud[Index])

ENDPROC

PROC RESET_GENERIC_ELIMINATION_FLASHING_COLOUR(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericElimination_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iColourFlashing_GenericElimination_MiniHud[Index])

ENDPROC

PROC RESET_GENERIC_FOUR_ICON_BAR_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericFourIconBar_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericFourIconBar_MiniHud[Index])
ENDPROC

PROC RESET_GENERIC_FIVE_ICON_SCORE_BAR_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_MiniHud[Index])
ENDPROC

PROC RESET_GENERIC_FIVE_ICON_SCORE_BAR_AVATAR_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_Avatar_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_Avatar_MiniHud[Index])
ENDPROC

PROC RESET_GENERIC_SIX_ICON_BAR_FLASHING(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericSixIconBar_Hud[Index])
	RESET_NET_TIMER(MPGlobalsScoreHud.iFlashing_GenericSixIconBar_MiniHud[Index])
ENDPROC

//FLOAT TheHudOffset
//FLOAT HudOffsetArray[20]
//BOOL CreatedWidget

PROC RESET_HUDELEMENT_METERS(INT Index)

	MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_Number[Index] = 0
	MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_MaxNumber[Index] = 0
	MPGlobalsScoreHud.ElementHud_METER.sGenericMeter_Title[Index] = ""
	
	MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_Colour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[Index] = HUDORDER_DONTCARE
	
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[Index] = <<0.5, 0.5, 0.0>>
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsPlayer[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bOnlyZeroIsEmpty[Index] = FALSE
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_ExtendedStartTimer[Index] )
	MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_ExtendedTimer[Index] = -1

	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bBigMeter[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iDrawRedDangerZonePercent[Index] = 0
	MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsLiteralString[Index] = FALSE
	
	MPGlobalsScoreHud.ElementHud_METER.PulseToColour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_METER.iPulseTime[Index] = -1
	
//	RESET_GENERIC_METER_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_CHECKPOINTS(INT Index)
//	HUDELEMENT_CHECKPOINT_STUCT emptyCheckpointStruct 
//	MPGlobalsScoreHud.ElementHud_CHECKPOINT = emptyCheckpointStruct
	
	
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_Number[Index] = 0
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_MaxNumber[Index] = 0
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.sGenericCheckpoint_Title[Index] = ""
	
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_Colour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[Index]  = HUDORDER_DONTCARE
	
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[Index]  = <<0.5, 0.5, 0.0>>
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_bIsPlayer[Index]= FALSE
		
	#IF USE_TU_CHANGES	
	GenericCheckpoint_Cross0[Index]	= FALSE
	GenericCheckpoint_Cross1[Index]	= FALSE
	GenericCheckpoint_Cross2[Index]	= FALSE
	GenericCheckpoint_Cross3[Index]	= FALSE
	GenericCheckpoint_Cross4[Index]	= FALSE
	GenericCheckpoint_Cross5[Index]	= FALSE
	GenericCheckpoint_Cross6[Index]	= FALSE
	GenericCheckpoint_Cross7[Index]	= FALSE
	
	GenericElimination_Cross0[Index]	= FALSE
	GenericElimination_Cross1[Index]	= FALSE
	GenericElimination_Cross2[Index]	= FALSE
	GenericElimination_Cross3[Index]	= FALSE
	GenericElimination_Cross4[Index]	= FALSE
	GenericElimination_Cross5[Index]	= FALSE
	GenericElimination_Cross6[Index]	= FALSE
	GenericElimination_Cross7[Index]	= FALSE
	
	#ENDIF
		
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_ExtendedStartTimer[Index])
	MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_ExtendedTimer[Index] = -1
	
//	RESET_GENERIC_CHECKPOINT_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_ELIMINATION(INT Index)
//	HUDELEMENT_ELIMINATION_STUCT emptyEliminationStruct 
//	MPGlobalsScoreHud.ElementHud_ELIMINATION = emptyEliminationStruct
	
	
	MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_MaxNumber[Index] = 0
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive1[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive2[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive3[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive4[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive5[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive6[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive7[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive8[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.sGenericElimination_Title[Index] = ""
	MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_VisibleBoxes[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFirst[Index] = HUD_COLOUR_GREEN
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourSecond[Index] = HUD_COLOUR_RED
	MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_TitleNumber[Index] = -1
	
//	MPGlobalsScoreHud.ElementHud_ELIMINATION.bRunning_GenericElimination[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[Index] = HUDORDER_DONTCARE
	
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[Index] = <<0.5, 0.5, 0.0>>
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bIsPlayer[Index]  = FALSE
	
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour[Index]   = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour[Index]   = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour[Index]   = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour[Index]   = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour[Index]   = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour[Index]   = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour[Index]   = HUD_COLOUR_WHITE
	
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour_InActive[Index]  = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour_InActive[Index]  = HUD_COLOUR_WHITE
		
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedStartTimer[Index])
	MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bUseNonPlayerFont[Index] = FALSE
	
	
//	RESET_GENERIC_ELIMINATION_FLASHING(Index)
ENDPROC
PROC RESET_HUDELEMENT_SINGLENUMBER(INT Index)
//	HUDELEMENT_SINGLE_NUMBER_STUCT emptySINGLENUMStruct 
//	MPGlobalsScoreHud.ElementHud_SINGLENUMBER = emptySINGLENUMStruct
	
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_Number[Index] = 0
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberTitle[Index] = ""
	
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_Colour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[Index] = HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_bIsPlayer[Index] = FALSE
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_ExtendedStartTimer[Index])
	MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_ExtendedTimer[Index] = -1

	
//	RESET_GENERIC_NUMBER_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_DOUBLENUMBER(INT Index)
//	HUDELEMENT_DOUBLE_NUMBER_STUCT emptyDOUBLENUMStruct 
//	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER = emptyDOUBLENUMStruct
	
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_Number[Index] = -1
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.sGenericDoubleNumber_Title[Index] = ""
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_NumberTwo[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_COLOUR[Index]  =HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[Index] = HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bIsPlayer[Index] = FALSE
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_ExtendedStartTimer[Index])
	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_ExtendedTimer[Index] = -1

	
//	RESET_GENERIC_DOUBLE_NUMBER_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_DOUBLENUMPLACE(INT Index)
//	HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT emptyDOUBLENUMPLACEStruct 
//	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE = emptyDOUBLENUMPLACEStruct
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_Number[Index] = 0
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.sGenericDoubleNumberPlace_Title[Index] = ""
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_NumberTwo[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_COLOUR[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[Index] = HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bIsPlayer[Index] = FALSE
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_ExtendedStartTimer[Index] )
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_ExtendedTimer[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bCustomFont[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_eCustomFont[Index] = FONT_STANDARD
	
//	RESET_GENERIC_DOUBLE_NUMBER_PLACE_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_SCORE(INT Index)
//	HUDELEMENT_SCORE_STUCT emptyScoreStruct 
//	MPGlobalsScoreHud.ElementHud_SCORE = emptyScoreStruct
	
	MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_Number[Index] = 0
	MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_Title[Index] = ""
	
	MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Colour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[Index] = HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsPlayer[Index] = FALSE
	
	MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_NumberString[Index] = ""
	MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_IsFloat[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_FloatValue[Index] = -1
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_ExtendedStartTimer[Index])
	MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_ExtendedTimer[Index] = -1

	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Style[Index] = HUD_COUNTER_STYLE_DEFAULT
	
//	RESET_GENERIC_SCORE_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_TIMER(INT Index)
//	HUDELEMENT_TIMER_STUCT emptyTimerStruct 
//	MPGlobalsScoreHud.ElementHud_TIMER = emptyTimerStruct
	
	MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_Timer[Index] = 0
	MPGlobalsScoreHud.ElementHud_TIMER.sGenericTimer_TimerTitle[Index] = ""
	MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtraTime[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_MedalDisplay[Index] = PODIUMPOS_NONE
	MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Colour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_TIMER.bGenericTimer_TimerStyle[Index] = TIMER_STYLE_DONTUSEMILLISECONDS
	MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[Index] = HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsPlayer[Index] = FALSE
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtendedStartTimer[Index] )
	MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtendedTimer[Index] = -1

	
//	RESET_GENERIC_TIMER_FLASHING(Index)
	
ENDPROC
PROC RESET_HUDELEMENT_WINDMETER(INT idx)
	MPGlobalsScoreHud.ElementHud_WIND.fGenericMeter_Heading[idx] = 0.0
	MPGlobalsScoreHud.ElementHud_WIND.sGenericMeter_Title[idx] = ""
	
	MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_WindSpeed[idx] = 0
	MPGlobalsScoreHud.ElementHud_WIND.fGenericMeter_Heading[idx] = 0.0
	MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_RedComponent[idx] = 0
	MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_BlueComponent[idx] = 0
	MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_GreenComponent[idx] = 0
	
	MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[idx] = HUDORDER_DONTCARE
ENDPROC
PROC RESET_HUDELEMENT_BIG_RACE_POSITION(INT idx)
	MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_iRacePosition[idx] = 0
	MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_eRacePositionHUDColour[idx] = HUD_COLOUR_WHITE
	
	MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[idx] = HUDORDER_DONTCARE
ENDPROC
PROC RESET_HUDELEMENT_SPRITE_METERS(INT Index)

	MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_Number[Index] = 0
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_MaxNumber[Index] = 0
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_Title[Index] = ""
	
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_FlashTimer[Index] = -1
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_Colour[Index] = HUD_COLOUR_WHITE
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_TitleNumber[Index] = -1
	
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[Index] = HUDORDER_DONTCARE
	
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsPlayer[Index] = FALSE
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bOnlyZeroIsEmpty[Index] = FALSE
	
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_ExtendedStartTimer[Index] )
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_ExtendedTimer[Index] = -1

	MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsLiteralString[Index] = FALSE
//	RESET_GENERIC_METER_FLASHING(Index)
	
ENDPROC

PROC RESET_HUDELEMENT_FOUR_ICON_BAR(INT idx)
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_TitleColour[idx] 		= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerOne[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerTwo[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerThree[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerFour[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupOne[idx] 		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupTwo[idx]		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupThree[idx]		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupFour[idx]		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[idx] 			= HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconOne[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconTwo[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconThree[idx]	= FALSE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconFour[idx]	= FALSE
	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_iFlashTime[idx]		= -1
ENDPROC

PROC RESET_HUDELEMENT_FIVE_ICON_SCORE_BAR(INT idx)
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_Number[idx]							= 0
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_MaXNumber[idx]						= 0
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_FloatValue[idx]						= 0.0
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_NumberString[idx]					= ""
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_isFloat[idx]							= FALSE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_DrawInfinity[idx]					= FALSE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_TitleColour[idx] 					= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerOne[idx] 						= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerTwo[idx] 						= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerThree[idx] 					= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFour[idx] 					= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFive[idx] 					= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupOne[idx] 					= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupTwo[idx]						= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupThree[idx]					= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFour[idx]					= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFive[idx]					= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[idx] 						= HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerToHighlight[idx]				= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bEnablePlayerHighlight[idx] 			= FALSE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupOneColour[idx] 				= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupTwoColour[idx] 				= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupThreeColour[idx] 			= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFourColour[idx] 				= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFiveColour[idx] 				= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iInstanceToHighlight[idx]			= 0
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bPulseHighlight[idx]					= FALSE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iPulseTime[idx]						= 0
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pAvatarToFlash[idx]					= NULL
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bFlashAvatar[idx]					= FALSE
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarFlashTime[idx]				= 0
	MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarSlotToFlash[idx]				= 0
ENDPROC

PROC RESET_HUDELEMENT_SIX_ICON_BAR(INT idx)
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_TitleColour[idx] 		= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerOne[idx] 			= NULL
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerTwo[idx] 			= NULL
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerThree[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFour[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFive[idx] 		= NULL
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerSix[idx] 			= NULL
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupOne[idx] 		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupTwo[idx]			= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupThree[idx]		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFour[idx]		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFive[idx]		= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupSix[idx]			= ACTIVITY_POWERUP_NONE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[idx] 			= HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconOne[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconTwo[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconThree[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFour[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFive[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconSix[idx]		= FALSE
	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_iFlashTime[idx]			= -1
ENDPROC

PROC RESET_HUDELEMENT_DOUBLE_TEXT(INT idx)
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleLeft[idx]									= ""
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleRight[idx]									= ""
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleLeftLiteral[idx]							= FALSE
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleRightLiteral[idx]							= FALSE
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[idx]										= HUDORDER_DONTCARE
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_TitleCOLOUR[idx]									= HUD_COLOUR_PURE_WHITE
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bCustomFont[idx]									= FALSE
	MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_eCustomFont[idx]									= FONT_STANDARD
ENDPROC

PROC EXTEND_ALL_HUD_ELEMENTS()

INT I

//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_ExtendedStartTimer[I])
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_ExtendedTimer[I])	= FALSE
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SINGLE_NUMBER, I)		
//			ENDIF
//		ENDIF
//	ENDFOR
	
//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_SCORE, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SCORE, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_ExtendedStartTimer[I])
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_ExtendedTimer[I])	= FALSE
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SCORE, I)		
//			ENDIF
//		ENDIF
//	ENDFOR

//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_TIMER, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_TIMER, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtendedStartTimer[I])
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtendedTimer[I])	= FALSE
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_TIMER, I)	
//			ENDIF
//		ENDIF
//	ENDFOR

//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_ExtendedStartTimer[I])
//
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_ExtendedTimer[I])	= FALSE
//
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)	
//			ENDIF
//		ENDIF
//	ENDFOR
	
//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_ExtendedStartTimer[I])
//
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_ExtendedTimer[I])	= FALSE
//
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER, I)	
//			ENDIF
//		ENDIF
//	ENDFOR

//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_CHECKPOINT, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_CHECKPOINT, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_ExtendedStartTimer[I])
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_ExtendedTimer[I])	= FALSE
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_CHECKPOINT, I)		
//			ENDIF
//		ENDIF
//	ENDFOR

//	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
//		IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_METER, I)
//		AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_METER, I)
//			RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_ExtendedStartTimer[I])
//		ELSE
//			IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_ExtendedTimer[I])	= FALSE
//				MPGlobalsScoreHud.isSomethingDisplaying = TRUE
//				SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_METER, I)
//			ENDIF
//		ENDIF
//	ENDFOR

	
	IF IS_PROGRESSHUD_EXTENDDISPLAY_SET(PROGRESSHUD_ELIMINATION)
		
		FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
	//		IF MPGlobalsScoreHud.bInit_GenericElimination[I] = TRUE
	//			NET_NL()NET_PRINT("MPGlobalsScoreHud.bInit_GenericElimination[I] = TRUE ")NET_PRINT_INT(I)NET_NL()	
	//		ELSE
	//			NET_NL()NET_PRINT("MPGlobalsScoreHud.bInit_GenericElimination[I] = FALSE ")NET_PRINT_INT(I)NET_NL()	
	//		ENDIF
	//		IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, I) = TRUE
	//			NET_NL()NET_PRINT("PROGRESSHUD_ELIMINATION = TRUE ")NET_PRINT_INT(I)NET_NL()	
	//		ELSE
	//			NET_NL()NET_PRINT("PROGRESSHUD_ELIMINATION = FALSE ")NET_PRINT_INT(I)NET_NL()	
	//		ENDIF
	//		NET_NL()NET_PRINT("MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[I] = ")NET_PRINT_INT(MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[I])NET_PRINT(" I = ")NET_PRINT_INT(I) NET_NL()	

			IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_ELIMINATION, I)
			AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, I)
				RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedStartTimer[I])
	//			NET_NL()NET_PRINT("MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[I] = RESET_NET_TIMER")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedStartTimer[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[I])	= FALSE
					MPGlobalsScoreHud.isSomethingDisplaying = TRUE
					SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELIMINATION, I)
	//				NET_NL()NET_PRINT("MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[I] = HAS_NET_TIMER_EXPIRED TRUE")
				ELSE
					CLEAR_PROGRESSHUD_EXTENDDISPLAY(PROGRESSHUD_ELIMINATION, I)
	//				NET_NL()NET_PRINT("MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[I] = HAS_NET_TIMER_EXPIRED = FALSE ")
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

ENDPROC

//BOOL InitWidgetBaseLine
//FLOAT BaseLineWidgetOffset
FUNC FLOAT BASE_LINE_OFFSET()
	
	FLOAT result = 0.925-0.002
	
	result += -0.036*MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud
	
	IF MPGlobalsScoreHud.bGolfScoreUnderHud = TRUE
		result += -0.15
	ELIF MPGlobalsScoreHud.bCoronaUnderHud = TRUE
		result += -0.048-0.753
	ENDIF 
	IF MPGlobalsScoreHud.bTopRightHud = TRUE
		result += -0.919
	ENDIF
	IF MPGlobalsScoreHud.bSwitchWheel = TRUE
		result += -0.184
	ENDIF
	IF MPGlobalsScoreHud.bSwitchWheelAndStats = TRUE
		result += -0.522
	ENDIF
	
	
	IF MPGlobalsScoreHud.bPhoneUnderHud = TRUE
	AND MPGlobalsScoreHud.bPhoneUnderHudNoRise = FALSE 
		SWITCH get_current_player_ped_enum()
			CASE CHAR_MICHAEL 
				result += -0.405
			BREAK
			CASE CHAR_TREVOR 
				result += -0.405
			BREAK
			CASE CHAR_FRANKLIN 
				result += -0.414
			BREAK
			DEFAULT //MP
				IF NOT IS_CELLPHONE_CAMERA_IN_USE()
					result += -0.405
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	IF InitWidgetBaseLine = FALSE
//		START_WIDGET_GROUP("BOTTOM RIGHT START POSITION")
//			ADD_WIDGET_FLOAT_SLIDER("BaseLineWidgetOffset", BaseLineWidgetOffset, -1, 1, 0.001)
//			ADD_WIDGET_BOOL("bTopRightHud", MPGlobalsScoreHud.bTopRightHud)
//			ADD_WIDGET_BOOL("bPhoneUnderHud", MPGlobalsScoreHud.bPhoneUnderHud)
//		STOP_WIDGET_GROUP()
//		InitWidgetBaseLine = TRUE
//	ENDIF
//	result += BaseLineWidgetOffset
//	#ENDIF

	

	
	RETURN result
ENDFUNC

PROC RESET_ALL_HUD_ELEMENTS_PRIVATE()

	
		MPGlobalsScoreHud.iHowManyDisplays = 0
		MPGlobalsScoreHud.HudStartY = 0.725
		MPGlobalsScoreHud.BottomStartY = BASE_LINE_OFFSET()
		MPGlobalsScoreHud.isSomethingDisplaying = FALSE
		MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud = 0
		MPGlobalsScoreHud.bGolfScoreUnderHud = FALSE
		MPGlobalsScoreHud.bCoronaUnderHud = FALSE
		MPGlobalsScoreHud.bPhoneUnderHud = FALSE
		MPGlobalsScoreHud.bPhoneUnderHudNoRise = FALSE
		MPGlobalsScoreHud.bTopRightHud = FALSE
		MPGlobalsScoreHud.bSwitchWheel = FALSE
		MPGlobalsScoreHud.bSwitchWheelAndStats = FALSE
		MPGlobalsScoreHud.bSniperScopeOn = FALSE
		MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE
		MPGlobalsScoreHud.bTitleMiddleJustified = FALSE
		MPGlobalsScoreHud.bTitleExtraLeftJustified = FALSE
		MPGlobalsScoreHud.bTitleInsaneLeftJustified = FALSE
		MPGlobalsScoreHud.bTitleInsanePlusLeftJustified = FALSE
		
		
		#IF USE_TU_CHANGES	
		g_b_HoldOffDrawingTimersThisFrame = FALSE
		#ENDIF
		
		g_b_ChangePlayerNameToTeamName = FALSE
		g_b_UseBottomRightTitleColour = FALSE
		g_b_iNumberOfDecimalPlacesForScore = 1
		#IF USE_TU_CHANGES
		g_b_IsATMrenderingForTimers = FALSE
		#ENDIF

		//BC Added a reset for the Generic Number UI
		INT I
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SINGLE_NUMBER)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I)
					RESET_HUDELEMENT_SINGLENUMBER(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_SINGLE_NUMBER, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_SINGLE_NUMBER, I)
			ENDFOR
		ENDIF
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SCORE)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_SCORE, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SCORE, I)
					RESET_HUDELEMENT_SCORE(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_SCORE, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_SCORE, I)
			ENDFOR
		ENDIF
		
		//BC Added a reset for the Generic Timer UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_TIMER)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_TIMER, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_TIMER, I)
					RESET_HUDELEMENT_TIMER(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_TIMER, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_TIMER, I)
			ENDFOR
		ENDIF
		
		//BC Added a reset for the Generic Double Number UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I)
					RESET_HUDELEMENT_DOUBLENUMBER(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_DOUBLE_NUMBER, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_DOUBLE_NUMBER, I)
			ENDFOR
		ENDIF
		
		//BC Added a reset for the Generic Double with Placement Number UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER_PLACE)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
					RESET_HUDELEMENT_DOUBLENUMPLACE(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
			ENDFOR
		ENDIF
		
		//BC Added a reset for the Generic Checkpoint UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_CHECKPOINT)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_CHECKPOINT, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_CHECKPOINT, I)
					RESET_HUDELEMENT_CHECKPOINTS(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_CHECKPOINT, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_CHECKPOINT, I)
			ENDFOR
		ENDIF
		
		//BC Added a reset for the Generic Meter UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_METER)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_METER, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_METER, I)
					RESET_HUDELEMENT_METERS(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_METER, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_METER, I)
			ENDFOR
		ENDIF
		
		//BC Added a reset for the Generic Elimination UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELIMINATION)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_ELIMINATION, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, I) = FALSE
					RESET_HUDELEMENT_ELIMINATION(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_ELIMINATION, I)
					
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_ELIMINATION, I)
			ENDFOR
		ENDIF

		//RP Added a reset for Generic Wind Meter UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_WINDMETER)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_WINDMETER, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_WINDMETER, I) = FALSE
					RESET_HUDELEMENT_WINDMETER(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_WINDMETER, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_WINDMETER, I)
			ENDFOR
		ENDIF	
		
		//SR Added a reset for Generic Big Race Position UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_BIG_RACE_POSITION)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_BIG_RACE_POSITION, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_BIG_RACE_POSITION, I) = FALSE
					RESET_HUDELEMENT_BIG_RACE_POSITION(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_BIG_RACE_POSITION, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_BIG_RACE_POSITION, I)
			ENDFOR
		ENDIF	
		
		//SR Added a reset for Generic Sprite Meter UI
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SPRITE_METER)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_SPRITE_METER, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SPRITE_METER, I) = FALSE
					RESET_HUDELEMENT_SPRITE_METERS(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_SPRITE_METER, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_SPRITE_METER, I)
			ENDFOR
		ENDIF	
		
		//SR Added a reset for Generic Four Icon Bar
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FOUR_ICON_BAR)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_FOUR_ICON_BAR, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FOUR_ICON_BAR, I) = FALSE
					RESET_HUDELEMENT_FOUR_ICON_BAR(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_FOUR_ICON_BAR, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_FOUR_ICON_BAR, I)
			ENDFOR
		ENDIF
		
		//SR Added a reset for Generic Five Icon Score Bar
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FIVE_ICON_SCORE_BAR)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I) = FALSE
					RESET_HUDELEMENT_FIVE_ICON_SCORE_BAR(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I)
			ENDFOR
		ENDIF
		
		//SR Added a reset for Generic Six Icon Bar
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SIX_ICON_BAR)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT - 1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_SIX_ICON_BAR, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SIX_ICON_BAR, I) = FALSE
					RESET_HUDELEMENT_SIX_ICON_BAR(I)
					
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_SIX_ICON_BAR, I)
				ENDIF
			ENDFOR
		ENDIF
		
		//SR Added a reset for Generic Double Text
		IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_TEXT)
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				IF IS_PROGRESSHUD_INIT_DONE_INDEXED(PROGRESSHUD_DOUBLE_TEXT, I)
				AND IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_TEXT, I) = FALSE
					RESET_HUDELEMENT_DOUBLE_TEXT(I)
					SET_PROGRESSHUD_INIT_NOT_DONE(PROGRESSHUD_DOUBLE_TEXT, I)
				ENDIF
//				SET_PROGRESSHUD_ACTIVATION_OFF(PROGRESSHUD_DOUBLE_TEXT, I)
			ENDFOR
		ENDIF
ENDPROC


//BOOL RightEdgeWidgetStart
//FLOAT RightEdge_WIDGETVAlue
//FLOAT RightEdge_UI_SINGLE_NUMBER_WIDGETVAlue

PROC SET_WORD_WRAPPING_RIGHTEDGE(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF RightEdgeWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_RIGHTEDGE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_RIGHTEDGE: RightEdge_WIDGETVAlue", RightEdge_WIDGETVAlue, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_RIGHTEDGE: SINGLE NUMBER", RightEdge_UI_SINGLE_NUMBER_WIDGETVAlue, -1, 1, 0.001)
//			
//			STOP_WIDGET_GROUP()
//			RightEdgeWidgetStart = TRUE
//		ENDIF
//	#ENDIF

	FLOAT Offset = 0.95-0.047+0.001+0.047-0.002+0.001
	aTitle.WrapEndX = Offset

	IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
		SWITCH aTitle.aTextType
			CASE TEXTTYPE_TS_UI_SINGLE_NUMBER
				aTitle.WrapEndX += 0.001
//				#IF IS_DEBUG_BUILD
//					aTitle.WrapEndX += RightEdge_UI_SINGLE_NUMBER_WIDGETVAlue
//				#ENDIF
			BREAK
			CASE TEXTTYPE_TS_UI_TIMERNUMBER_TWOSETS
				aTitle.WrapEndX += 0.001
//				#IF IS_DEBUG_BUILD
//					aTitle.WrapEndX += RightEdge_UI_SINGLE_NUMBER_WIDGETVAlue
//				#ENDIF
			BREAK
			
			
			DEFAULT
				
			BREAK
		
		
		ENDSWITCH
	ENDIF


	
	IF MPGlobalsScoreHud.bCoronaUnderHud = TRUE
		aTitle.WrapEndX = Offset
		aTitle.WrapEndX += RIGHT_EDGE_CORONA_OFFSET
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	aTitle.WrapEndX += RightEdge_WIDGETVAlue
//	#ENDIF
	
ENDPROC

//BOOL RightEdgeDOTWidgetStart
//FLOAT RightEdgeDOT_WIDGETVAlue

PROC SET_WORD_WRAPPING_RIGHTEDGE_AND_DOT(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF RightEdgeDOTWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_RIGHTEDGE_AND_DOT")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_RIGHTEDGE_AND_DOT: RightEdge_WIDGETVAlue", RightEdgeDOT_WIDGETVAlue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			RightEdgeDOTWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	
	
	

	aTitle.WrapEndX = 0.95-0.047+0.001+0.047-0.002-0.013
	
//	#IF IS_DEBUG_BUILD
//	aTitle.WrapEndX += RightEdgeDOT_WIDGETVAlue
//	#ENDIF
	
ENDPROC



//BOOL RightEdgePLACEWidgetStart
//FLOAT RightEdgePLACE_WIDGETVAlue

PROC SET_WORD_WRAPPING_RIGHTEDGE_AND_PLACE(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF RightEdgePLACEWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_RIGHTEDGE_AND_PLACE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_RIGHTEDGE_AND_PLACE: RightEdge_WIDGETVAlue", RightEdgePLACE_WIDGETVAlue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			RightEdgePLACEWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	
	
	

	aTitle.WrapEndX = 0.95-0.047+0.001+0.047-0.002-0.013+0.014
	
//	#IF IS_DEBUG_BUILD
//	aTitle.WrapEndX += RightEdgePLACE_WIDGETVAlue
//	#ENDIF
	
ENDPROC

//BOOL RightEdgeAMPMWidgetStart
//FLOAT RightEdgeAMPM_WIDGETVAlue

PROC SET_WORD_WRAPPING_RIGHTEDGE_AMPM(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF RightEdgeAMPMWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_RIGHTEDGE_AND_AMPM")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_RIGHTEDGE_AND_AMPM: RightEdge_WIDGETVAlue", RightEdgeAMPM_WIDGETVAlue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			RightEdgeAMPMWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	
	
	

	aTitle.WrapEndX = 0.95-0.047+0.001+0.047-0.002-0.013+0.014-0.024+0.005
	
//	#IF IS_DEBUG_BUILD
//	aTitle.WrapEndX += RightEdgeAMPM_WIDGETVAlue
//	#ENDIF
	
ENDPROC

//BOOL RightEdgeXPWidgetStart
//FLOAT RightEdgeXP_WIDGETVAlue

PROC SET_WORD_WRAPPING_RIGHTEDGE_AND_XP(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF RightEdgeXPWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_RIGHTEDGE_AND_XP")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_RIGHTEDGE_AND_XP: RightEdge_WIDGETVAlue", RightEdgeXP_WIDGETVAlue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			RightEdgeXPWidgetStart = TRUE
//		ENDIF
//	#ENDIF

	aTitle.WrapEndX = 0.95-0.047+0.001+0.047-0.002-0.013-0.002-0.001
	
//	#IF IS_DEBUG_BUILD
//	aTitle.WrapEndX += RightEdgeXP_WIDGETVAlue
//	#ENDIF
	
ENDPROC



PROC SET_WORD_HUD_COLOUR(TEXT_STYLE& aTitle, HUD_COLOURS aColour)
	INT RED, GREEN, BLUE, AlphaValue
	GET_HUD_COLOUR(aColour, RED, GREEN, BLUE, AlphaValue)
	aTitle.r = RED
	aTitle.g = GREEN
	aTitle.b = BLUE
	aTitle.a = AlphaValue
ENDPROC






#IF USE_TU_CHANGES

FUNC BOOL DRAW_CROSSED_DOT(INT Index, BOOL CrossedDot0,BOOL CrossedDot1,BOOL CrossedDot2,BOOL CrossedDot3,BOOL CrossedDot4,BOOL CrossedDot5,BOOL CrossedDot6,BOOL CrossedDot7)
	
	IF Index = 0
	AND CrossedDot0 = TRUE
		RETURN TRUE
	ENDIF
	
	IF Index = 1
	AND CrossedDot1 = TRUE
		RETURN TRUE
	ENDIF
	
	IF Index = 2
	AND CrossedDot2 = TRUE
		RETURN TRUE
	ENDIF
	
	IF Index = 3
	AND CrossedDot3 = TRUE
		RETURN TRUE
	ENDIF
	
	IF Index = 4
	AND CrossedDot4 = TRUE
		RETURN TRUE
	ENDIF
	
	IF Index = 5
	AND CrossedDot5 = TRUE
		RETURN TRUE
	ENDIF
	
	
	IF Index = 6
	AND CrossedDot6 = TRUE
		RETURN TRUE
	ENDIF
	
	IF Index = 7
	AND CrossedDot7 = TRUE
		RETURN TRUE
	ENDIF
	

	RETURN FALSE

ENDFUNC

#ENDIF


//#IF IS_DEBUG_BUILD
//BOOL InitiMask
//SPRITE_PLACEMENT CheckpointWidget
//FLOAT CheckpointSpacingWidget
//TEXT_STYLE NumberStyle_Widget
//TEXT_PLACEMENT NumberPlacement_Widget
//#ENDIF
PROC RUN_CHECKPOINT_GUTS(INT NumCheckpointsPassed, INT MaxNumCheckpoints, SPRITE_PLACEMENT& Sprites[], TEXT_STYLE& NumberStyle, TEXT_PLACEMENT& NumberPlace, HUD_COLOURS Colour, GFX_DRAW_ORDER anOrder, BOOL DisplaySingleDot  
						#IF USE_TU_CHANGES , BOOL CrossedDot0,BOOL CrossedDot1,BOOL CrossedDot2,BOOL CrossedDot3,BOOL CrossedDot4,BOOL CrossedDot5,BOOL CrossedDot6,BOOL CrossedDot7 #ENDIF)

//	#IF IS_DEBUG_BUILD
//		IF InitiMask = FALSE
////			
//			START_WIDGET_GROUP("CHECKPOINT")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(CheckpointWidget, "CheckpointWidget")
//				ADD_WIDGET_FLOAT_SLIDER("CheckpointSpacingWidget", CheckpointSpacingWidget, -1, 1, 0.001)
//				
//				CREATE_A_TEXT_STYLE_WIGET(NumberStyle_Widget, "NumberStyle_Widget")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(NumberPlacement_Widget, "NumberPlacement_Widget")
//			STOP_WIDGET_GROUP()
////		
//			InitiMask = TRUE
//		ENDIF
//	#ENDIF
		
	
	
//	FLOAT WidthOffsetTimers = -0.002 
//	FLOAT XOffsetTimers = 0.001
//		
//	FLOAT MaskWidthOffsetTimersCheckPoint = -0.001//0.001 //0.0 if you want a sliver at 0
//	IF NumCheckpointsPassed = 0
//		MaskWidthOffsetTimersCheckPoint = 0.0
//	ENDIF
//	FLOAT MaskXOffsetTimersCheckPoint = 0.002
//	
//	FLOAT MainMaskHeight = -0.003
//	FLOAT OverlayMaskHeight = 0.004
//	FLOAT FillMaskHeight = 0.002
	
		IF MaxNumCheckpoints < CHECKPOINT_TIMER_SWITCH
	
			FLOAT SpacingofDots = CHECKPOINT_DOT_SPACING
//			#IF IS_DEBUG_BUILD
//			SpacingofDots += CheckpointSpacingWidget
//			#ENDIF

			REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
			#IF IS_DEBUG_BUILD
			REQUEST_STREAMED_TEXTURE_DICT("Cross")
			#ENDIF
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
			#IF IS_DEBUG_BUILD
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("Cross") 
			#ENDIF
			
				INT I
				FOR I = 0 TO MaxNumCheckpoints-1
//					#if IS_DEBUG_BUILD
//					
//					UPDATE_SPRITE_WIDGET_VALUE(Sprites[I], CheckpointWidget)
//					#ENDIF
					
					Sprites[I].x += SpacingofDots*I
					IF ((MaxNumCheckpoints-I) > NumCheckpointsPassed) 
//						Sprites[I].x += SpacingofDots*I
						
						

						SET_SPRITE_HUD_COLOUR(Sprites[I], Colour)
						
						Sprites[I].a = TIMER_METER_NON_SELECTED_ALPHA
						
						DRAW_2D_SPRITE("TimerBars", "Circle_checkpoints_Outline", Sprites[I], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ELSE
					
					
//						IF (MaxNumCheckpoints-I) > NumCheckpointsPassed
//							SET_SPRITE_HUD_COLOUR(Sprites[I], HUD_COLOUR_BLACK)
//						ELSE	
							SET_SPRITE_HUD_COLOUR(Sprites[I], Colour)
//						ENDIF

						DRAW_2D_SPRITE("TimerBars", "Circle_checkpoints", Sprites[I], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						
						#IF USE_TU_CHANGES
						IF DRAW_CROSSED_DOT(I, CrossedDot0, CrossedDot1, CrossedDot2, CrossedDot3, CrossedDot4, CrossedDot5, CrossedDot6, CrossedDot7)
							
							SET_SPRITE_HUD_COLOUR(Sprites[I], HUD_COLOUR_BLACK)
							DRAW_2D_SPRITE("Cross", "Circle_checkpoints_Cross", Sprites[I], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						ENDIF
						#ENDIF
						
					
					ENDIF
					

				ENDFOR
				
			
			
			
			
			
			
			
			
	//			SET_SPRITE_HUD_COLOUR(Sprites[0], OuterRingColour)
	//			
	//			DRAW_2D_SPRITE("TimerBars", "GaugeForIngameHUD_Outline", Sprites[0], FALSE)
	//
	//			Sprites[0].w += WidthOffsetTimers
	//			Sprites[0].x += XOffsetTimers
	//			Sprites[0].h += MainMaskHeight
	//
	//			GET_SPRITE_LEVEL_BAR(Sprites[0], 100,Sprites[1], FALSE)
	//
	//			
	//			SET_SPRITE_HUD_COLOUR(Sprites[1], Colour)
	//			
	//			GET_SPRITE_LEVEL_BAR(Sprites[0], 100,Sprites[3], FALSE)
	//			IF GET_BAR_CONTRAST_COLOUR(Colour) = HUD_COLOUR_BLACK
	//				Sprites[3].a = 90
	//			ENDIF
	//			Sprites[3].h += FillMaskHeight
	//			SET_SPRITE_HUD_COLOUR(Sprites[3], GET_BAR_CONTRAST_COLOUR(Colour))
	//			DRAW_2D_SPRITE("TimerBars", "GaugeForIngameHUD_BackgroundAndFill", Sprites[3], FALSE)
	//			
	//			IF NumCheckpointsPassed > 0
	//				FLOAT Percent = (TO_FLOAT(NumCheckpointsPassed)/TO_FLOAT(MaxNumCheckpoints) *100)
	//				
	//				GET_SPRITE_LEVEL_BAR(Sprites[0], Percent,Sprites[2], FALSE, FALSE, 0, FALSE)
	//				Sprites[2].w += MaskWidthOffsetTimersCheckPoint
	//				Sprites[2].x += MaskXOffsetTimersCheckPoint
	//				Sprites[2].h += OverlayMaskHeight
	//				
	//				
	//				//SET_SPRITES_DRAW_BEFORE_FADE(FALSE)
	//				
	//				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	//				SET_MASK_ATTRIBUTES(Sprites[2].x, Sprites[2].y, Sprites[2].w, Sprites[2].h)
	//				SET_MASK_ACTIVE(TRUE)
	//				DRAW_2D_SPRITE("TimerBars", "GaugeForIngameHUD_BackgroundAndFill", Sprites[1], FALSE)
	//				SET_MASK_ATTRIBUTES(0,0,0,0)
	//				SET_MASK_ACTIVE(FALSE)
	//			ENDIF
	//			
	//			DRAW_DIVIDING_LINES(Sprites[0],aRect, MaxNumCheckpoints)
			ELSE	
				NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()

			ENDIF
		
		ELSE
	
//			#IF IS_DEBUG_BUILD
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle, NumberStyle_Widget)
//			UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberPlace, NumberPlacement_Widget)
//			#ENDIF

			IF Colour = HUD_COLOUR_WHITE 
				DRAW_TEXT_WITH_2_NUMBERS(NumberPlace,NumberStyle,"TIMER_DASHES", NumCheckpointsPassed,MaxNumCheckpoints,FONT_RIGHT)
				
			ELSE
			
				SET_WORD_WRAPPING_RIGHTEDGE_AND_DOT(NumberStyle)
			
				IF DisplaySingleDot
					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
					
//						#IF IS_DEBUG_BUILD
//						UPDATE_SPRITE_WIDGET_VALUE(Sprites[0], CheckpointWidget)
//						#ENDIF
						
						Sprites[0].x += 0.058-0.060
						Sprites[0].y += -0.005
						Sprites[0].w +=  0.003-0.005+0.002
						Sprites[0].h +=  0.009-0.010
						
						SET_SPRITE_HUD_COLOUR(Sprites[0], Colour)
					
						DRAW_2D_SPRITE("TimerBars", "Circle_checkpoints_Big", Sprites[0], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ENDIF
				
				STRING Dashes = "TIMER_DASHES"
				
				IF DisplaySingleDot = TRUE
				AND NumCheckpointsPassed > 99
				AND MaxNumCheckpoints > 99
					Dashes = "TIMER_DASHES"
				ENDIF
				
				DRAW_TEXT_WITH_2_NUMBERS(NumberPlace,NumberStyle,Dashes, NumCheckpointsPassed,MaxNumCheckpoints,FONT_RIGHT)
			ENDIF
			
		ENDIF
	
ENDPROC




//BOOL DamagedWidgetINIT
//SPRITE_PLACEMENT Offset_Widget_Sprite[2]

PROC SET_SPRITE_PULSE_COLOUR(SPRITE_PLACEMENT &thisSprite, HUD_COLOURS eStartingColour, HUD_COLOURS ePulseColour, INT iPulseTime, SCRIPT_TIMER stPulseTimer)
	INT iR, iG, iB, iA, iPR, iPG, iPB, iPA
	INT iElapsedTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPulseTimer)
	FLOAT fPulseTime
	
	IF (iPulseTime / 2) > iElapsedTime
		fPulseTime = (TO_FLOAT(iElapsedTime / 2) / TO_FLOAT(iPulseTime / 2))
		GET_HUD_COLOUR(eStartingColour, iR, iG, iB, iA)
		GET_HUD_COLOUR(ePulseColour, iPR, iPG, iPB, iPA)
	ELSE
		fPulseTime = (TO_FLOAT(iElapsedTime) / TO_FLOAT(iPulseTime))
		GET_HUD_COLOUR(ePulseColour, iR, iG, iB, iA)
		GET_HUD_COLOUR(eStartingColour, iPR, iPG, iPB, iPA)
	ENDIF
	
	thisSprite.r = FLOOR(LERP_FLOAT(TO_FLOAT(iR), TO_FLOAT(iPR), fPulseTime))
	thisSprite.g = FLOOR(LERP_FLOAT(TO_FLOAT(iG), TO_FLOAT(iPG), fPulseTime))
	thisSprite.b = FLOOR(LERP_FLOAT(TO_FLOAT(iB), TO_FLOAT(iPB), fPulseTime))
	thisSprite.a = FLOOR(LERP_FLOAT(TO_FLOAT(iA), TO_FLOAT(iPA), fPulseTime))
ENDPROC

PROC RUN_METER_GUTS(INT CurrentLevel, INT MaxLevel, SPRITE_PLACEMENT& Sprites[], HUD_COLOURS Colour, BOOL OnlyZeroIsEmpty, GFX_DRAW_ORDER anOrder , SCRIPT_TIMER &stPulseTimer, SCRIPT_TIMER &stSecBarPulseTimer, INT iDrawRedDangerZonePercent = 0, PERCENTAGE_METER_LINE Percentage = PERCENTAGE_METER_LINE_NONE, BOOL MakeBarUrgent = FALSE, INT iUrgentPercentage = 0, FLOAT fCurrentLevel = -1.0, FLOAT fMaxLevel = -1.0, HUD_COLOURS PulseToColour = HUD_COLOUR_WHITE, INT iPulseTime = -1, BOOL bUseSecondaryBar = FALSE, HUD_COLOURS eSecondaryBarColour = HUD_COLOUR_WHITE, FLOAT fSecondaryBarPercentage = -1.0, BOOL bTransparentSecBarIntersectingMainBar = FALSE, HUD_COLOURS eSecBarPulseToColour = HUD_COLOUR_WHITE, INT iSecBarPulseTime = -1, FLOAT fSecBarStartPercentage = 0.0, BOOL bCapMaxPercentage = TRUE)
	
	
	CONST_FLOAT METER_WIDTH 0.007
	CONST_FLOAT METER_HEIGHT 0.004
	
	
	SPRITE_PLACEMENT Offsets[2]
	
	Offsets[0].y = 0.0
	Offsets[0].w = METER_WIDTH
	Offsets[0].h = METER_HEIGHT
	
	Offsets[1].x = 0
	Offsets[1].w = METER_WIDTH
	Offsets[1].h = METER_HEIGHT
	

	


//	#IF IS_DEBUG_BUILD		
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[0], Offset_Widget_Sprite[0])
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[1], Offset_Widget_Sprite[1])
//
//	#ENDIF

	

		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF Percentage != PERCENTAGE_METER_LINE_NONE
			REQUEST_STREAMED_TEXTURE_DICT("timerbar_lines")
		ENDIF
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
			
	//		#IF IS_DEBUG_BUILD		
	//		IF DamagedWidgetINIT = FALSE
	//			START_WIDGET_GROUP("DAMAGE METER GUTS")
	//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_Sprite[0], "Offset_Widget_Sprite[0]")
	//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_Sprite[1], "Offset_Widget_Sprite[1]")
	//				
	//
	//			STOP_WIDGET_GROUP()
	//			DamagedWidgetINIT = TRUE
	//		ENDIF
	//		#ENDIF
							
			UPDATE_SPRITE_VALUE(Sprites[0], Offsets[0])
			UPDATE_SPRITE_VALUE(Sprites[1], Offsets[1])
			
			Sprites[3] = Sprites[0]
			
			

			SET_SPRITE_HUD_COLOUR(Sprites[1], Colour)
			
			IF MakeBarUrgent
				SET_SPRITE_HUD_COLOUR(Sprites[3], HUD_COLOUR_GREY)
			ELSE
				SET_SPRITE_HUD_COLOUR(Sprites[3], Colour)
			ENDIF
			
			IF iUrgentPercentage != 0
			AND CurrentLevel < iUrgentPercentage
				SET_SPRITE_HUD_COLOUR(Sprites[1], HUD_COLOUR_RED)
			ENDIF
			
			IF iPulseTime > 0
				IF NOT HAS_NET_TIMER_STARTED(stPulseTimer)
					START_NET_TIMER(stPulseTimer)
				ELIF HAS_NET_TIMER_EXPIRED(stPulseTimer, iPulseTime)
					REINIT_NET_TIMER(stPulseTimer)
				ENDIF
				
				SET_SPRITE_PULSE_COLOUR(Sprites[1], Colour, PulseToColour, iPulseTime, stPulseTimer)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_displaytimerhudcolour")
					PRINTLN("[BCTIMER] TimerBars hud colour = ", HUD_COLOUR_AS_STRING(Colour))
				ENDIF
			#ENDIF
			
			FLOAT Percent = ((TO_FLOAT(CurrentLevel)/TO_FLOAT(MaxLevel)) *100)
			
			IF (fCurrentLevel != -1.0 AND fMaxLevel != -1.0)
				Percent = (fCurrentLevel/fMaxLevel)*100.0
			ELIF (fCurrentLevel != -1.0 AND fMaxLevel = -1.0)
				Percent = (fCurrentLevel/TO_FLOAT(MaxLevel))*100.0
			ELIF (fCurrentLevel = -1.0 AND fMaxLevel != -1.0)
				Percent = (TO_FLOAT(CurrentLevel)/fMaxLevel)*100.0
			ENDIF
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			Sprites[3].a = TIMER_METER_NON_SELECTED_ALPHA
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
			IF MakeBarUrgent
				SET_SPRITE_HUD_COLOUR(Sprites[3], HUD_COLOUR_GREY)
			ELSE
				SET_SPRITE_HUD_COLOUR(Sprites[3], Colour)
			ENDIF
			
			Sprites[3].a = TIMER_METER_NON_SELECTED_ALPHA
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			Sprites[3].a = 255
			
			GET_SPRITE_LEVEL_BAR(Sprites[0], Percent, Sprites[1], TRUE, TRUE, OnlyZeroIsEmpty, DEFAULT, bCapMaxPercentage)
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[1], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
			IF (bUseSecondaryBar)
				SPRITE_PLACEMENT SecondaryBar = Sprites[1]
				GET_SPRITE_LEVEL_BAR(Sprites[0], fSecondaryBarPercentage, SecondaryBar, TRUE, TRUE, OnlyZeroIsEmpty, fSecBarStartPercentage, bCapMaxPercentage)
				SET_SPRITE_HUD_COLOUR(SecondaryBar, eSecondaryBarColour)
				SecondaryBar.a = 255
				
				IF iSecBarPulseTime > 0
					IF NOT HAS_NET_TIMER_STARTED(stSecBarPulseTimer)
						START_NET_TIMER(stSecBarPulseTimer)
					ELIF HAS_NET_TIMER_EXPIRED(stSecBarPulseTimer, iSecBarPulseTime)
						REINIT_NET_TIMER(stSecBarPulseTimer)
					ENDIF
					SET_SPRITE_PULSE_COLOUR(SecondaryBar, eSecondaryBarColour, eSecBarPulseToColour, iSecBarPulseTime, stSecBarPulseTimer)
				ENDIF
				
				IF (bTransparentSecBarIntersectingMainBar)
					IF (Percent <= fSecondaryBarPercentage)
						SecondaryBar.a = 100
					ENDIF
				ENDIF
				
				SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
				DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", SecondaryBar, FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder, FALSE)
			ENDIF
			
//			NET_NL()NET_PRINT("Percentage = ")NET_PRINT_INT(ENUM_TO_INT(Percentage))
			IF Percentage != PERCENTAGE_METER_LINE_NONE
				REQUEST_STREAMED_TEXTURE_DICT("timerbar_lines")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("timerbar_lines") 
				
					IF Percentage != PERCENTAGE_METER_LINE_ALL_20
						STRING LineTexture = "LineMarker90_128"
						
						SWITCH Percentage
							CASE PERCENTAGE_METER_LINE_10 LineTexture = "LineMarker10_128" BREAK
							CASE PERCENTAGE_METER_LINE_20 LineTexture = "LineMarker20_128" BREAK
							CASE PERCENTAGE_METER_LINE_30 LineTexture = "LineMarker30_128" BREAK
							CASE PERCENTAGE_METER_LINE_40 LineTexture = "LineMarker40_128" BREAK
							CASE PERCENTAGE_METER_LINE_50 LineTexture = "LineMarker50_128" BREAK
							CASE PERCENTAGE_METER_LINE_60 LineTexture = "LineMarker60_128" BREAK
							CASE PERCENTAGE_METER_LINE_70 LineTexture = "LineMarker70_128" BREAK
							CASE PERCENTAGE_METER_LINE_80 LineTexture = "LineMarker80_128" BREAK
							CASE PERCENTAGE_METER_LINE_90 LineTexture = "LineMarker90_128" BREAK
						ENDSWITCH
						Sprites[3].a = 255
						SET_SPRITE_HUD_COLOUR(Sprites[3], HUD_COLOUR_BLACK)
	//					NET_NL()NET_PRINT("LineTexture = ")NET_PRINT(LineTexture)
						
						DRAW_2D_SPRITE("timerbar_lines", LineTexture, Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ELSE
						
						Sprites[3].a = 255
						SET_SPRITE_HUD_COLOUR(Sprites[3], HUD_COLOUR_BLACK)
						DRAW_2D_SPRITE("timerbar_lines", "LineMarker20_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						DRAW_2D_SPRITE("timerbar_lines", "LineMarker40_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						DRAW_2D_SPRITE("timerbar_lines", "LineMarker60_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						DRAW_2D_SPRITE("timerbar_lines", "LineMarker80_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						
						
					ENDIF
				ELSE
					NET_NL()NET_PRINT("waiting on timerbar_lines to load")NET_NL()
				ENDIF
			ENDIF
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			IF (iDrawRedDangerZonePercent > 0)
			AND (Percent >= iDrawRedDangerZonePercent)
				SET_SPRITE_HUD_COLOUR(Sprites[1], HUD_COLOUR_RED)
				GET_SPRITE_LEVEL_BAR(Sprites[0], TO_FLOAT(iDrawRedDangerZonePercent), Sprites[1], TRUE, TRUE, OnlyZeroIsEmpty, DEFAULT, bCapMaxPercentage)
				SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
				DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[1], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			ENDIF

			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
	//		DRAW_2D_SPRITE("TimerBars", "DamageBar_128", Sprites[0], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
			
		
		ELSE	
			NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()

		ENDIF
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	SPRITE_PLACEMENT Offsets[4]
//	
//	Offsets[0].y += 0.00025
//	Offsets[0].w = 0.006-0.003+0.004
//	Offsets[0].h = 0.006-0.002
//	
//	Offsets[1].x = 0.001+0.001
//	Offsets[1].y += -0.001
//	Offsets[1].w = 0.003
//	Offsets[1].h = 0.003-0.008
//	
//	Offsets[2].x = 0.002+0.001+0.001
//	
//	Offsets[2].w = 0.003+0.002
//	Offsets[2].h = 0.002+0.003
//	
//	Offsets[3].x = 0.001-0.001
//	Offsets[3].y += -0.0005-0.001
//	Offsets[3].w = 0.005-0.006
//	Offsets[3].h = 0.018-0.005-0.010-0.005
//
//	#IF IS_DEBUG_BUILD		
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[0], Offset_Widget_Sprite[0])
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[1], Offset_Widget_Sprite[1])
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[2], Offset_Widget_Sprite[2])
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[3], Offset_Widget_Sprite[3])
//	#ENDIF
//
//	REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
//		
//		#IF IS_DEBUG_BUILD		
//		IF DamagedWidgetINIT = FALSE
//			START_WIDGET_GROUP("DAMAGE METER GUTS")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_Sprite[0], "Offset_Widget_Sprite[0]")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_Sprite[1], "Offset_Widget_Sprite[1]")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_Sprite[2], "Offset_Widget_Sprite[2]")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_Sprite[3], "Offset_Widget_Sprite[3]")
//
//			STOP_WIDGET_GROUP()
//			DamagedWidgetINIT = TRUE
//		ENDIF
//		#ENDIF
//						
//		UPDATE_SPRITE_VALUE(Sprites[0], Offsets[0])
//		
//		Sprites[1] = Sprites[0]		
//		Sprites[3] = Sprites[0]		
//		
//		
//		
//		
//		
//		
//
//		
//		
//		SET_SPRITE_HUD_COLOUR(Sprites[1], Colour)
//		SET_SPRITE_HUD_COLOUR(Sprites[3], HUD_COLOUR_BLACK)// GET_BAR_CONTRAST_COLOUR(Colour))
//		
//		
//		
//		FLOAT Percent = ((TO_FLOAT(CurrentLevel)/TO_FLOAT(MaxLevel)) *100)
//
//		SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
//		UPDATE_SPRITE_VALUE(Sprites[3], Offsets[3])
//		Sprites[3].a = 128
//		DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//		SET_SPRITE_HUD_COLOUR(Sprites[3], GET_BAR_CONTRAST_COLOUR(Colour))
//		Sprites[3].a = 178
//		DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//		Sprites[3].a = 255
//
//		GET_SPRITE_LEVEL_BAR(Sprites[0], Percent,Sprites[2], FALSE, FALSE, OnlyZeroIsEmpty)
//		
//		SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
//		UPDATE_SPRITE_VALUE(Sprites[2], Offsets[2])
//		SET_MASK_ATTRIBUTES(Sprites[2].x, Sprites[2].y, Sprites[2].w, Sprites[2].h)
//		SET_MASK_ACTIVE(TRUE)
//		SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
//		UPDATE_SPRITE_VALUE(Sprites[1], Offsets[1])
//		DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[1], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//		SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
//		SET_MASK_ATTRIBUTES(0,0,0,0)
//		SET_MASK_ACTIVE(FALSE)
//
//
//
//		SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
////		DRAW_2D_SPRITE("TimerBars", "DamageBar_128", Sprites[0], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//		
//		
//	
//	ELSE	
//		NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()
//
//	ENDIF


	
ENDPROC

//BOOL DamagedBIGWidgetINIT
//SPRITE_PLACEMENT Offset_BIGWidget_Sprite[2]



PROC RUN_BIG_METER_GUTS(INT CurrentLevel, INT MaxLevel, SPRITE_PLACEMENT& Sprites[], HUD_COLOURS Colour, BOOL OnlyZeroIsEmpty, GFX_DRAW_ORDER anOrder, PERCENTAGE_METER_LINE Percentage, SCRIPT_TIMER &stPulseTimer, SCRIPT_TIMER &stSecBarPulseTimer, FLOAT fCurrentLevel = -1.0, FLOAT fMaxLevel = -1.0, HUD_COLOURS PulseToColour = HUD_COLOUR_WHITE, INT iPulseTime = -1, BOOL bUseSecondaryBar = FALSE, HUD_COLOURS eSecondaryBarColour = HUD_COLOUR_WHITE, FLOAT fSecondaryBarPercentage = 0.0, BOOL bTransparentSecBarIntersectingMainBar = FALSE, HUD_COLOURS eSecBarPulseToColour = HUD_COLOUR_WHITE, INT iSecBarPulseTime = -1, BOOL bCapMaxPercentage = TRUE)
	
	
	CONST_FLOAT METER_WIDTH 0.007
	CONST_FLOAT METER_HEIGHT 0.016
	
	
	SPRITE_PLACEMENT Offsets[2]
	Offsets[0].y = -0.004
	Offsets[0].w = METER_WIDTH
	Offsets[0].h = METER_HEIGHT
	
	Offsets[1].w = METER_WIDTH
	Offsets[1].h = METER_HEIGHT
	

	


//	#IF IS_DEBUG_BUILD		
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[0], Offset_BIGWidget_Sprite[0])
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[1], Offset_BIGWidget_Sprite[1])
//
//	#ENDIF

			
	//		#IF IS_DEBUG_BUILD		
	//		IF DamagedBIGWidgetINIT = FALSE
	//			START_WIDGET_GROUP("DAMAGE BIG METER GUTS")
	//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_BIGWidget_Sprite[0], "Offset_Widget_Sprite[0]")
	//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_BIGWidget_Sprite[1], "Offset_Widget_Sprite[1]")
	//				
	//
	//			STOP_WIDGET_GROUP()
	//			DamagedBIGWidgetINIT = TRUE
	//		ENDIF
	//		#ENDIF

	
			

	

		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF Percentage != PERCENTAGE_METER_LINE_NONE
			REQUEST_STREAMED_TEXTURE_DICT("timerbar_lines")
		ENDIF
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
			
						
							
			
			UPDATE_SPRITE_VALUE(Sprites[0], Offsets[0])
			UPDATE_SPRITE_VALUE(Sprites[1], Offsets[1])
			
			Sprites[3] = Sprites[0]
			

			SET_SPRITE_HUD_COLOUR(Sprites[1], Colour)
			SET_SPRITE_HUD_COLOUR(Sprites[3], Colour)
			
			IF iPulseTime > 0
				IF NOT HAS_NET_TIMER_STARTED(stPulseTimer)
					START_NET_TIMER(stPulseTimer)
				ELIF HAS_NET_TIMER_EXPIRED(stPulseTimer, iPulseTime)
					REINIT_NET_TIMER(stPulseTimer)
				ENDIF
				
				SET_SPRITE_PULSE_COLOUR(Sprites[1], Colour, PulseToColour, iPulseTime, stPulseTimer)
			ENDIF
			
			FLOAT Percent = ((TO_FLOAT(CurrentLevel)/TO_FLOAT(MaxLevel)) *100)
			
			IF (fCurrentLevel != -1.0 AND fMaxLevel != -1.0)
				Percent = (fCurrentLevel/fMaxLevel)*100.0
			ELIF (fCurrentLevel != -1.0 AND fMaxLevel = -1.0)
				Percent = (fCurrentLevel/TO_FLOAT(MaxLevel))*100.0
			ELIF (fCurrentLevel = -1.0 AND fMaxLevel != -1.0)
				Percent = (TO_FLOAT(CurrentLevel)/fMaxLevel)*100.0
			ENDIF
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			Sprites[3].a = TIMER_METER_NON_SELECTED_ALPHA
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			SET_SPRITE_HUD_COLOUR(Sprites[3], Colour)
			Sprites[3].a = TIMER_METER_NON_SELECTED_ALPHA
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			Sprites[3].a = 255

			GET_SPRITE_LEVEL_BAR(Sprites[0], Percent,Sprites[1], TRUE, TRUE, OnlyZeroIsEmpty, DEFAULT, bCapMaxPercentage)
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[1], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
			IF (bUseSecondaryBar)
				SPRITE_PLACEMENT SecondaryBar = Sprites[1]
				GET_SPRITE_LEVEL_BAR(Sprites[0], fSecondaryBarPercentage, SecondaryBar, TRUE, TRUE, OnlyZeroIsEmpty, DEFAULT, bCapMaxPercentage)
				SET_SPRITE_HUD_COLOUR(SecondaryBar, eSecondaryBarColour)
				SecondaryBar.a = 255
				
				IF iSecBarPulseTime > 0
					IF NOT HAS_NET_TIMER_STARTED(stSecBarPulseTimer)
						START_NET_TIMER(stSecBarPulseTimer)
					ELIF HAS_NET_TIMER_EXPIRED(stSecBarPulseTimer, iSecBarPulseTime)
						REINIT_NET_TIMER(stSecBarPulseTimer)
					ENDIF
					SET_SPRITE_PULSE_COLOUR(SecondaryBar, eSecondaryBarColour, eSecBarPulseToColour, iSecBarPulseTime, stSecBarPulseTimer)
				ENDIF
				
				IF (bTransparentSecBarIntersectingMainBar)
					IF (Percent <= fSecondaryBarPercentage)
						SecondaryBar.a = 100
					ENDIF
				ENDIF
				
				SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
				DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", SecondaryBar, FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			ENDIF
			
			IF Percentage != PERCENTAGE_METER_LINE_NONE
				REQUEST_STREAMED_TEXTURE_DICT("timerbar_lines")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("timerbar_lines") 
					STRING LineTexture = "LineMarker90_128"
					
					SWITCH Percentage
						CASE PERCENTAGE_METER_LINE_10 LineTexture = "LineMarker10_128" BREAK
						CASE PERCENTAGE_METER_LINE_20 LineTexture = "LineMarker20_128" BREAK
						CASE PERCENTAGE_METER_LINE_30 LineTexture = "LineMarker30_128" BREAK
						CASE PERCENTAGE_METER_LINE_40 LineTexture = "LineMarker40_128" BREAK
						CASE PERCENTAGE_METER_LINE_50 LineTexture = "LineMarker50_128" BREAK
						CASE PERCENTAGE_METER_LINE_60 LineTexture = "LineMarker60_128" BREAK
						CASE PERCENTAGE_METER_LINE_70 LineTexture = "LineMarker70_128" BREAK
						CASE PERCENTAGE_METER_LINE_80 LineTexture = "LineMarker80_128" BREAK
						CASE PERCENTAGE_METER_LINE_90 LineTexture = "LineMarker90_128" BREAK
					ENDSWITCH
					Sprites[1].a = 255
					SET_SPRITE_HUD_COLOUR(Sprites[1], HUD_COLOUR_BLACK)
					NET_NL()NET_PRINT("LineTexture = ")NET_PRINT(LineTexture)
					DRAW_2D_SPRITE("timerbar_lines", LineTexture, Sprites[1], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				ELSE
					NET_NL()NET_PRINT("waiting on timerbar_lines to load")NET_NL()
				ENDIF
			ENDIF
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)


			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
	//		DRAW_2D_SPRITE("TimerBars", "DamageBar_128", Sprites[0], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
		
			
		
		ELSE	
			NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()

		ENDIF
	
	

	
ENDPROC


//BOOL DamagedWidgetSpriteMeterINIT
//SPRITE_PLACEMENT Offset_Widget_SpriteMeter[2]


PROC RUN_SPRITE_METER_GUTS(INT CurrentLevel, INT MaxLevel, SPRITE_PLACEMENT& Sprites[], HUD_COLOURS Colour, BOOL OnlyZeroIsEmpty, GFX_DRAW_ORDER anOrder)
	
	
	CONST_FLOAT METER_WIDTH 0.011
	CONST_FLOAT METER_HEIGHT 0.059
	
	
	SPRITE_PLACEMENT Offsets[2]
	
	Offsets[0].x = 0.003
	Offsets[0].y = -0.004
	Offsets[0].w = METER_WIDTH
	Offsets[0].h = METER_HEIGHT
	
	Offsets[1].x = 0
	Offsets[1].w = METER_WIDTH
	Offsets[1].h = METER_HEIGHT
	

	


//	#IF IS_DEBUG_BUILD		
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[0], Offset_Widget_SpriteMeter[0])
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[1], Offset_Widget_SpriteMeter[1])
//
//	#ENDIF

	

		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
			
//			#IF IS_DEBUG_BUILD		
//			IF DamagedWidgetSpriteMeterINIT = FALSE
//				START_WIDGET_GROUP("DAMAGE SPRITE METER GUTS")
//					CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_SpriteMeter[0], "Offset_Widget_SpriteMeter[0]")
//					CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_SpriteMeter[1], "Offset_Widget_SpriteMeter[1]")
//					
//	
//				STOP_WIDGET_GROUP()
//				DamagedWidgetSpriteMeterINIT = TRUE
//			ENDIF
//			#ENDIF
							
			UPDATE_SPRITE_VALUE(Sprites[0], Offsets[0])
			UPDATE_SPRITE_VALUE(Sprites[1], Offsets[1])
			
			Sprites[3] = Sprites[0]
			

			SET_SPRITE_HUD_COLOUR(Sprites[1], Colour)
			SET_SPRITE_HUD_COLOUR(Sprites[3], Colour)
			
//			#IF IS_DEBUG_BUILD
//				IF GET_COMMANDLINE_PARAM_EXISTS("sc_displaytimerhudcolour")
//					PRINTLN("[BCTIMER] TimerBars hud colour = ", HUD_COLOUR_AS_STRING(Colour))
//				ENDIF
//			#ENDIF
			
			FLOAT Percent = ((TO_FLOAT(CurrentLevel)/TO_FLOAT(MaxLevel)) *100)

			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			Sprites[3].a = TIMER_METER_NON_SELECTED_ALPHA
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			SET_SPRITE_HUD_COLOUR(Sprites[3], Colour)
			Sprites[3].a = TIMER_METER_NON_SELECTED_ALPHA
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[3], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			Sprites[3].a = 255
			
			GET_SPRITE_LEVEL_BAR(Sprites[0], Percent, Sprites[1], TRUE, TRUE, OnlyZeroIsEmpty)
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			DRAW_2D_SPRITE("TimerBars", "DamageBarFill_128", Sprites[1], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			
			
		
		ELSE	
			NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()

		ENDIF
		
	
	
	
	


	
ENDPROC

FUNC STRING GET_MODELNAME_TEXTURE(MODEL_NAMES modelName)
	SWITCH modelname
		
		CASE HUNTER 	RETURN "hunter"
		CASE ROGUE		RETURN "rogue"
		CASE HAVOK		RETURN "havok"
		CASE MOGUL		RETURN "mogul"
		CASE STARLING	RETURN "starling"
		CASE MOLOTOK	RETURN "molotok"
		CASE NOKOTA		RETURN "nokota"
		CASE PYRO		RETURN "pyro"
		CASE LAZER		RETURN "lazer"
		CASE TULA		RETURN "tula"
		CASE MICROLIGHT	RETURN "ultralight"
		CASE HYDRA		RETURN "hydra"
		CASE ANNIHILATOR RETURN "annihl"
		CASE BUZZARD	RETURN "buzzard"
		CASE SAVAGE		RETURN "savage"
		CASE THRUSTER	RETURN "thruster"
		CASE AKULA		RETURN "akula"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_WEAPONTYPE_TEXTURE(WEAPON_TYPE aWeapon)

	SWITCH aWeapon
	
		
		CASE WEAPONTYPE_DLC_ASSAULTMG				RETURN "WEAPON_ASSAULT_MG"
		CASE WEAPONTYPE_BALL						RETURN "WEAPON_BALL"
		CASE WEAPONTYPE_BAT							RETURN "WEAPON_BAT"
		CASE WEAPONTYPE_DLC_BOTTLE					RETURN "WEAPON_BOTTLE"
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE			RETURN "WEAPON_BULLPUPRIFLE"
		CASE WEAPONTYPE_DLC_COMBATPDW				RETURN "WEAPON_COMBATPDW"
		CASE WEAPONTYPE_DLC_COMPACTRIFLE			RETURN "WEAPON_COMPACTRIFLE"
		CASE WEAPONTYPE_CROWBAR						RETURN "WEAPON_CROWBAR"
		CASE WEAPONTYPE_DLC_DAGGER					RETURN "WEAPON_DAGGER"			
		CASE WEAPONTYPE_DLC_DBSHOTGUN				RETURN "WEAPON_DBSHOTGUN"		
		CASE WEAPONTYPE_DLC_FIREWORK				RETURN "WEAPON_FIREWORK"		
		CASE WEAPONTYPE_FLARE						RETURN "WEAPON_FLARE"		
		CASE WEAPONTYPE_DLC_FLAREGUN				RETURN "WEAPON_FLARE_GUN"		
		CASE WEAPONTYPE_DLC_FLASHLIGHT				RETURN "WEAPON_FLASHLIGHT"		
		CASE WEAPONTYPE_GOLFCLUB					RETURN "WEAPON_GOLFCLUB"		
		CASE WEAPONTYPE_DLC_GUSENBERG				RETURN "WEAPON_GUSENBERG"		
		CASE WEAPONTYPE_HAMMER						RETURN "WEAPON_HAMMER"		
		CASE WEAPONTYPE_DLC_HATCHET					RETURN "WEAPON_HATCHET"		
		CASE WEAPONTYPE_GRENADELAUNCHER				RETURN "WEAPON_HEAVY_GRENADE_LAUNCHER"		
		CASE WEAPONTYPE_MINIGUN						RETURN "WEAPON_HEAVY_MINIGUN"		
		CASE WEAPONTYPE_DLC_HEAVYRIFLE				RETURN "WEAPON_HEAVY_RIFLE"		
		CASE WEAPONTYPE_RPG							RETURN "WEAPON_HEAVY_RPG"		
		CASE WEAPONTYPE_DLC_HEAVYPISTOL				RETURN "WEAPON_HEAVYPISTOL"		
		CASE WEAPONTYPE_DLC_HEAVYSHOTGUN			RETURN "WEAPON_HEAVYSHOTGUN"		
		CASE WEAPONTYPE_DLC_HOMINGLAUNCHER			RETURN "WEAPON_HOMINGLAUNCHER"		
		CASE WEAPONTYPE_PETROLCAN					RETURN "WEAPON_JERRY_CAN"		
		CASE WEAPONTYPE_KNIFE						RETURN "WEAPON_KNIFE"		
		CASE WEAPONTYPE_DLC_KNUCKLE					RETURN "WEAPON_KNUCKLE"		
		CASE WEAPONTYPE_MG							RETURN "WEAPON_LMG"		
		CASE WEAPONTYPE_COMBATMG					RETURN "WEAPON_LMG_COMBAT"		 
		CASE WEAPONTYPE_DLC_MACHETE					RETURN "WEAPON_MACHETE"		
		CASE WEAPONTYPE_DLC_MACHINEPISTOL			RETURN "WEAPON_MACHINEPISTOL"		
		CASE WEAPONTYPE_DLC_MARKSMANPISTOL			RETURN "WEAPON_MARKSMANPISTOL"		
		CASE WEAPONTYPE_DLC_MARKSMANRIFLE			RETURN "WEAPON_MARKSMANRIFLE"		
		CASE WEAPONTYPE_MOLOTOV						RETURN "WEAPON_MOLOTOV"		
		CASE WEAPONTYPE_DLC_MUSKET					RETURN "WEAPON_MUSKET"		
		CASE WEAPONTYPE_NIGHTSTICK					RETURN "WEAPON_NIGHTSTICK"		
		CASE WEAPONTYPE_PISTOL						RETURN "WEAPON_PISTOL"		
		CASE WEAPONTYPE_DLC_PISTOL50				RETURN "WEAPON_PISTOL_50"		
		CASE WEAPONTYPE_APPISTOL					RETURN "WEAPON_PISTOL_AP"		
		CASE WEAPONTYPE_COMBATPISTOL				RETURN "WEAPON_PISTOL_COMBAT"		
		CASE WEAPONTYPE_DLC_PROGRAMMABLEAR			RETURN "WEAPON_PROGRAMMABLE_AR"		
		CASE WEAPONTYPE_DLC_PROXMINE				RETURN "WEAPON_PROXIMINE"		
		CASE WEAPONTYPE_DLC_RAILGUN					RETURN "WEAPON_RAILGUN"		
		CASE WEAPONTYPE_DLC_REVOLVER				RETURN "WEAPON_REVOLVER"		
		CASE WEAPONTYPE_ADVANCEDRIFLE				RETURN "WEAPON_RIFLE_ADVANCED"		
		CASE WEAPONTYPE_ASSAULTRIFLE				RETURN "WEAPON_RIFLE_ASSAULT"		
		CASE WEAPONTYPE_CARBINERIFLE				RETURN "WEAPON_RIFLE_CARBINE"		
		CASE WEAPONTYPE_ASSAULTSHOTGUN 				RETURN "WEAPON_SHOTGUN_ASSAULT"		
		CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN			RETURN "WEAPON_SHOTGUN_BULLPUP"		
		CASE WEAPONTYPE_PUMPSHOTGUN					RETURN "WEAPON_SHOTGUN_PUMP"		
		CASE WEAPONTYPE_SAWNOFFSHOTGUN				RETURN "WEAPON_SHOTGUN_SAWNOFF"		
		CASE WEAPONTYPE_SMG							RETURN "WEAPON_SMG"		
		CASE WEAPONTYPE_DLC_ASSAULTSMG				RETURN "WEAPON_SMG_ASSAULT"		
		CASE WEAPONTYPE_MICROSMG					RETURN "WEAPON_SMG_MICRO"		
		CASE WEAPONTYPE_SNIPERRIFLE					RETURN "WEAPON_SNIPER"		
		CASE WEAPONTYPE_DLC_ASSAULTSNIPER			RETURN "WEAPON_SNIPER_ASSAULT"		
		CASE WEAPONTYPE_HEAVYSNIPER					RETURN "WEAPON_SNIPER_HEAVY"		
		CASE WEAPONTYPE_DLC_SNOWBALL				RETURN "WEAPON_SNOWBALL"		
		CASE WEAPONTYPE_DLC_SNSPISTOL				RETURN "WEAPON_SNSPISTOL"		
		CASE WEAPONTYPE_DLC_SPECIALCARBINE			RETURN "WEAPON_SPECIALCARBINE"		
		CASE WEAPONTYPE_STUNGUN						RETURN "WEAPON_STUNGUN"		
		CASE WEAPONTYPE_DLC_SWITCHBLADE				RETURN "WEAPON_SWITCHBLADE"		
		CASE WEAPONTYPE_BZGAS						RETURN "WEAPON_THROWN_BZ_GAS"		
		CASE WEAPONTYPE_GRENADE						RETURN "WEAPON_THROWN_GRENADE"		
		CASE WEAPONTYPE_STICKYBOMB					RETURN "WEAPON_THROWN_STICKY"		
		CASE WEAPONTYPE_UNARMED						RETURN "WEAPON_UNARMED"		
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL			RETURN "WEAPON_VINTAGEPISTOL"	
		
		CASE WEAPONTYPE_DLC_AUTOSHOTGUN				RETURN "WEAPON_AUTOMATIC_SHOTGUN"
		CASE WEAPONTYPE_DLC_BATTLEAXE				RETURN "WEAPON_BATTLE_AXE"
		CASE WEAPONTYPE_DLC_COMPACTLAUNCHER			RETURN "WEAPON_COMPACT_GRENADE_LAUNCHER"
		CASE WEAPONTYPE_DLC_MINISMG					RETURN "WEAPON_MINI_SMG"
		CASE WEAPONTYPE_DLC_PIPEBOMB				RETURN "WEAPON_PIPEBOMB"
		CASE WEAPONTYPE_DLC_POOLCUE					RETURN "WEAPON_POOL_CUE"
		CASE WEAPONTYPE_DLC_WRENCH					RETURN "WEAPON_WRENCH"

	ENDSWITCH


RETURN ""
ENDFUNC


//BOOL DamagedWidgetSpriteWeaponINIT
//SPRITE_PLACEMENT Offset_Widget_SpriteWeapon[2]


PROC RUN_SPRITE_METER_SPRITE(STRING spriteName, SPRITE_PLACEMENT& Sprites[], GFX_DRAW_ORDER anOrder, STRING dictionaryName)
	
	
	
	SPRITE_PLACEMENT Offsets[1]
	
	Offsets[0].x = 0
	Offsets[0].y = 0
	Offsets[0].w = -0.015
	Offsets[0].h = -0.025
	

	

	


//	#IF IS_DEBUG_BUILD		
//	UPDATE_SPRITE_WIDGET_VALUE(Offsets[0], Offset_Widget_SpriteWeapon[0])
//
//
//	#ENDIF


		REQUEST_STREAMED_TEXTURE_DICT(dictionaryName)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(dictionaryName) 
			
//			#IF IS_DEBUG_BUILD		
//			IF DamagedWidgetSpriteWeaponINIT = FALSE
//				START_WIDGET_GROUP("DAMAGE SPRITE METER WEAPONS")
//					CREATE_A_SPRITE_PLACEMENT_WIDGET(Offset_Widget_SpriteWeapon[0], "Offset_Widget_SpriteWeapon[0]")	
//				STOP_WIDGET_GROUP()
//				DamagedWidgetSpriteWeaponINIT = TRUE
//			ENDIF
//			#ENDIF
							
			UPDATE_SPRITE_VALUE(Sprites[0], Offsets[0])
			

			

			SET_SPRITE_HUD_COLOUR(Sprites[0], HUD_COLOUR_WHITE)
			

			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			Sprites[0].a = 255
			DRAW_2D_SPRITE(dictionaryName, spriteName, Sprites[0], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			
			
		
		ELSE	
			NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()

		ENDIF
		
	
	
	
	


	
ENDPROC




FUNC BOOL SHOULD_DRAW_SQUARE(INT iCurrentSquare, INT NumVisibleSquares)

	IF NumVisibleSquares = -1
		RETURN TRUE
	ENDIF	
	IF iCurrentSquare > NumVisibleSquares
		RETURN FALSE
	ENDIF
RETURN TRUE
ENDFUNC
FUNC SPRITE_PLACEMENT SET_SPRITE_COLOUR_ACTIVE(SPRITE_PLACEMENT aSprite, BOOL Active)
	SPRITE_PLACEMENT TempSprite = aSprite
	IF Active
		TempSprite.r = 20
		TempSprite.g = 120
		TempSprite.b = 20
	ELSE
		TempSprite.r = 120
		TempSprite.g = 20
		TempSprite.b = 20
	ENDIF
	RETURN TempSprite
ENDFUNC
FUNC SPRITE_PLACEMENT SET_SPRITE_COLOUR_ELIMINATION(SPRITE_PLACEMENT aSprite, BOOL Active, HUD_COLOURS ActiveColour, HUD_COLOURS inActiveColour, HUD_COLOURS BoxColour, HUD_COLOURS BoxColour_Inactive)
	SPRITE_PLACEMENT TempSprite = aSprite
	IF BoxColour = HUD_COLOUR_PURE_WHITE
//	AND BoxColour_Inactive = HUD_COLOUR_PURE_WHITE //Potential fix if your buttons are pure_white. 
		IF Active
			SET_SPRITE_HUD_COLOUR(TempSprite, ActiveColour)
		ELSE
			SET_SPRITE_HUD_COLOUR(TempSprite, inActiveColour)
		ENDIF
	ELSE
		IF Active
			SET_SPRITE_HUD_COLOUR(TempSprite,BoxColour)	
		ELSE
			SET_SPRITE_HUD_COLOUR(TempSprite,BoxColour_Inactive)	
		ENDIF
	ENDIF
	IF Active = FALSE
//		TempSprite.a = 90
	ENDIF
	RETURN TempSprite
ENDFUNC



//#IF IS_DEBUG_BUILD
//	BOOL InitiMaskElim
//	SPRITE_PLACEMENT EliminationWidget
//	FLOAT EliminationSpacingWidget
//#ENDIF
PROC RUN_ELIMINATION_GUTS(INT MaxNumCheckpoints, SPRITE_PLACEMENT& Sprites[], HUD_COLOURS FirstColour, HUD_COLOURS SecondColour, INT iVisibleBoxes, BOOL IsEliminated1, BOOL IsEliminated2, BOOL IsEliminated3, BOOL IsEliminated4, BOOL IsEliminated5, BOOL IsEliminated6, BOOL IsEliminated7, BOOL IsEliminated8,
						HUD_COLOURS Box1Colour,HUD_COLOURS Box2Colour, HUD_COLOURS Box3Colour, HUD_COLOURS Box4Colour, HUD_COLOURS Box5Colour, HUD_COLOURS Box6Colour, HUD_COLOURS Box7Colour, HUD_COLOURS Box8Colour, HUD_COLOURS Box1Colour_Inactive,HUD_COLOURS Box2Colour_Inactive, HUD_COLOURS Box3Colour_Inactive, 
						HUD_COLOURS Box4Colour_Inactive, HUD_COLOURS Box5Colour_Inactive, HUD_COLOURS Box6Colour_Inactive, HUD_COLOURS Box7Colour_Inactive, HUD_COLOURS Box8Colour_Inactive, GFX_DRAW_ORDER anOrder
						#IF USE_TU_CHANGES , BOOL CrossedDot0, BOOL CrossedDot1, BOOL CrossedDot2, BOOL CrossedDot3, BOOL CrossedDot4, BOOL CrossedDot5, BOOL CrossedDot6, BOOL CrossedDot7,
						HUD_COLOURS CrossedDot0Colour, HUD_COLOURS CrossedDot1Colour, HUD_COLOURS CrossedDot2Colour, HUD_COLOURS CrossedDot3Colour, HUD_COLOURS CrossedDot4Colour, HUD_COLOURS CrossedDot5Colour, HUD_COLOURS CrossedDot6Colour, HUD_COLOURS CrossedDot7Colour #ENDIF )
						

//	#IF IS_DEBUG_BUILD
//	IF InitiMaskElim = FALSE
//		
//		START_WIDGET_GROUP("RUN ELIMINATION GUTS")
//			CREATE_A_SPRITE_PLACEMENT_WIDGET(EliminationWidget, "EliminationWidget")
//			ADD_WIDGET_FLOAT_SLIDER("EliminationSpacingWidget", EliminationSpacingWidget, -1, 1, 0.001)
//		
//		STOP_WIDGET_GROUP()
//	
//		InitiMaskElim = TRUE
//	ENDIF
//	#ENDIF



	REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
	#IF USE_TU_CHANGES
	REQUEST_STREAMED_TEXTURE_DICT("Cross")
	#ENDIF
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
	#IF USE_TU_CHANGES
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("Cross") 
	#ENDIF


		BOOL IsActive
		HUD_COLOURS BoxColour, BoxColour_Inactive
		FLOAT SpacingofDots = CHECKPOINT_DOT_SPACING
//		#IF IS_DEBUG_BUILD
//		SpacingofDots += EliminationSpacingWidget
//		#ENDIF

		INT I
		FOR I = 0 TO MaxNumCheckpoints-1
//				#IF IS_DEBUG_BUILD
//				UPDATE_SPRITE_WIDGET_VALUE(Sprites[I], EliminationWidget)
//				#ENDIF
			Sprites[I].x += SpacingofDots*(I)
			IF SHOULD_DRAW_SQUARE(i, iVisibleBoxes)
				
				
				SWITCH I

					CASE 0
						IsActive = IsEliminated1
						BoxColour = Box1Colour
						BoxColour_Inactive = Box1Colour_Inactive
					BREAK
					CASE 1
						IsActive = IsEliminated2
						BoxColour = Box2Colour
						BoxColour_Inactive = Box2Colour_Inactive
					BREAK 
					CASE 2
						IsActive = IsEliminated3
						BoxColour = Box3Colour
						BoxColour_Inactive = Box3Colour_Inactive
					BREAK 
					CASE 3
						IsActive = IsEliminated4
						BoxColour = Box4Colour
						BoxColour_Inactive = Box4Colour_Inactive
					BREAK 
					CASE 4
						IsActive = IsEliminated5
						BoxColour = Box5Colour
						BoxColour_Inactive = Box5Colour_Inactive
					BREAK 
					CASE 5
						IsActive = IsEliminated6
						BoxColour = Box6Colour
						BoxColour_Inactive = Box6Colour_Inactive
					BREAK
					CASE 6
						IsActive = IsEliminated7
						BoxColour = Box7Colour
						BoxColour_Inactive = Box7Colour_Inactive
					BREAK 
					CASE 7
						IsActive = IsEliminated8
						BoxColour = Box8Colour
						BoxColour_Inactive = Box8Colour_Inactive
					BREAK 
				ENDSWITCH		
			
//				NET_NL()NET_PRINT("RUN_ELIMINATION_GUTS: FirstColour = ")NET_PRINT(HUD_COLOUR_AS_STRING(FirstColour))
//				NET_NL()NET_PRINT("RUN_ELIMINATION_GUTS: SecondColour = ")NET_PRINT(HUD_COLOUR_AS_STRING(SecondColour))
//				NET_NL()NET_PRINT("RUN_ELIMINATION_GUTS: BoxColour_Inactive = ")NET_PRINT(HUD_COLOUR_AS_STRING(BoxColour_Inactive))
//				NET_NL()NET_PRINT("RUN_ELIMINATION_GUTS: BoxColour = ")NET_PRINT(HUD_COLOUR_AS_STRING(BoxColour))
			
			
				
				SPRITE_PLACEMENT sTempPlacement
				BOOL reduceAlpha = FALSE
				
				IF BoxColour_Inactive = HUD_COLOUR_BLACK
					reduceAlpha = TRUE
					BoxColour_Inactive = HUD_COLOUR_WHITE
				ENDIF
				
				sTempPlacement = SET_SPRITE_COLOUR_ELIMINATION(Sprites[I], IsActive, FirstColour, SecondColour, BoxColour, BoxColour_Inactive)
			
				IF reduceAlpha 
					sTempPlacement.a = TIMER_METER_NON_SELECTED_ALPHA
				ENDIF
									
				DRAW_2D_SPRITE("TimerBars", "Circle_checkpoints", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				
				#IF USE_TU_CHANGES
				IF DRAW_CROSSED_DOT(I, CrossedDot0, CrossedDot1, CrossedDot2, CrossedDot3, CrossedDot4, CrossedDot5, CrossedDot6, CrossedDot7)
					
					SWITCH I
						CASE 0	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot0Colour)  BREAK
						CASE 1	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot1Colour)  BREAK
						CASE 2	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot2Colour)  BREAK
						CASE 3	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot3Colour)  BREAK
						CASE 4	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot4Colour)  BREAK
						CASE 5	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot5Colour)  BREAK
						CASE 6	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot6Colour)  BREAK
						CASE 7	SET_SPRITE_HUD_COLOUR(Sprites[I], CrossedDot7Colour)  BREAK
					ENDSWITCH
					
					DRAW_2D_SPRITE("Cross", "Circle_checkpoints_Cross", Sprites[I], FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				ENDIF
				#ENDIF
				
				
			ENDIF
		
		ENDFOR
		

		
	ENDIF

//
//
//		
//		SET_SPRITE_HUD_COLOUR(Sprites[0], OuterRingColour)
//		DRAW_2D_SPRITE("TimerBars", "GaugeForIngameHUD_Outline", Sprites[0], FALSE)
//		INT I
//		BOOL IsActive
//		HUD_COLOURS BoxColour, BoxColour_Inactive
//		
//		IF iVisibleBoxes > -1
//			GET_SPRITE_LEVEL_BAR(Sprites[0], 100,Sprites[2], FALSE)
//			
////			SET_SPRITE_BLACK(Sprites[2])
//			Sprites[2].a = 90
//			
//			DRAW_2D_SPRITE("TimerBars", "GaugeForIngameHUD_BackgroundAndFill", Sprites[2], FALSE)
//		ENDIF
//		
//		FOR I = 1 TO MaxNumCheckpoints
//		
//
//			SWITCH I
//				CASE 1
//					IsActive = IsEliminated1
//					BoxColour = Box1Colour
//					BoxColour_Inactive = Box1Colour_Inactive
//				BREAK
//				CASE 2
//					IsActive = IsEliminated2
//					BoxColour = Box2Colour
//					BoxColour_Inactive = Box2Colour_Inactive
//				BREAK 
//				CASE 3
//					IsActive = IsEliminated3
//					BoxColour = Box3Colour
//					BoxColour_Inactive = Box3Colour_Inactive
//				BREAK 
//				CASE 4
//					IsActive = IsEliminated4
//					BoxColour = Box4Colour
//					BoxColour_Inactive = Box4Colour_Inactive
//				BREAK 
//				CASE 5
//					IsActive = IsEliminated5
//					BoxColour = Box5Colour
//					BoxColour_Inactive = Box5Colour_Inactive
//				BREAK 
//				CASE 6
//					IsActive = IsEliminated6
//					BoxColour = Box6Colour
//					BoxColour_Inactive = Box6Colour_Inactive
//				BREAK
//				CASE 7
//					IsActive = IsEliminated7
//					BoxColour = Box7Colour
//					BoxColour_Inactive = Box7Colour_Inactive
//				BREAK 
//				CASE 8
//					IsActive = IsEliminated8
//					BoxColour = Box8Colour
//					BoxColour_Inactive = Box8Colour_Inactive
//				BREAK 
//			ENDSWITCH		
//
//			FLOAT BlockPercentage = TO_FLOAT(I)/TO_FLOAT(MaxNumCheckpoints)
//			BlockPercentage *= 100
//			FLOAT iBlockPercentage = (BlockPercentage)
//			
//			IF SHOULD_DRAW_SQUARE(i, iVisibleBoxes)
//				GET_SPRITE_LEVEL_BAR(Sprites[0], iBlockPercentage,Sprites[1], FALSE, TRUE, MaxNumCheckpoints)
//				SPRITE_PLACEMENT sTempPlacement = SET_SPRITE_COLOUR_ELIMINATION(Sprites[1], IsActive, FirstColour, SecondColour, BoxColour, BoxColour_Inactive)
//				DRAW_2D_SPRITE("TimerBars", "GaugeForIngameHUD_BackgroundAndFill", sTempPlacement, FALSE)
//			ENDIF
//		ENDFOR
//		DRAW_DIVIDING_LINES(Sprites[0],aRect, MaxNumCheckpoints)
//	ELSE	
//		NET_NL()NET_PRINT("waiting on TimerBars to load")NET_NL()
//
//	ENDIF



ENDPROC




//FUNC BOOL IS_CHECKBOX_DISPLAYING()
//	RETURN MPGlobalsScoreHud.bRunningCheckbox 
//ENDFUNC
//
//FUNC BOOL IS_SCREEN_TIMER_DISPLAYING()
//	RETURN MPGlobalsScoreHud.bRunningScreenTimer
//ENDFUNC

//FUNC INT GET_NUM_BAR_DISPLAYS_ON_SCREEN()
//	INT Count
//	IF IS_CHECKBOX_DISPLAYING()
//		Count++
//	ENDIF
//	IF IS_SCREEN_TIMER_DISPLAYING()
//		Count++
//	ENDIF
//	RETURN Count
//ENDFUNC


FUNC FLOAT GET_Y_SHIFT(UIELEMENTS WhichSpace)
	SWITCH WhichSpace
		CASE UIELEMENTS_BOTTOMRIGHT
			RETURN MPGlobalsScoreHud.BottomStartY
		BREAK
		CASE UIELEMENTS_MIDDLERIGHT
			RETURN MPGlobalsScoreHud.HudStartY
		BREAK
	ENDSWITCH 
	RETURN MPGlobalsScoreHud.BottomStartY
ENDFUNC


PROC CHANGE_Y_SHIFT_END(UIELEMENTS WhichSpace, FLOAT Amount)
	SWITCH WhichSpace
		CASE UIELEMENTS_BOTTOMRIGHT
			MPGlobalsScoreHud.BottomStartY  += Amount
		BREAK
		CASE UIELEMENTS_MIDDLERIGHT
			MPGlobalsScoreHud.HudStartY += Amount
		BREAK
	ENDSWITCH
	
ENDPROC




PROC SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(INT iNumRows = 1)
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_TraceTimersShiftUp")
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME - called - iNumRows = ")NET_PRINT_INT(iNumRows)
		ENDIF
	#ENDIF
	MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud = iNumRows
ENDPROC

PROC SET_GOLF_SCORE_UNDER_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bGolfScoreUnderHud = TRUE
ENDPROC

PROC SET_CORONA_UNDER_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bCoronaUnderHud = TRUE
ENDPROC

PROC SET_PHONE_UNDER_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bPhoneUnderHud = TRUE
ENDPROC
PROC SET_PHONE_UNDER_HUD_NO_RISE_THIS_FRAME()
	MPGlobalsScoreHud.bPhoneUnderHudNoRise = TRUE
ENDPROC
PROC SET_TOP_RIGHT_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bTopRightHud = TRUE
ENDPROC

PROC SET_SWITCH_WHEEL_UNDER_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bSwitchWheel = TRUE
ENDPROC

PROC SET_SWITCH_WHEEL_AND_STATS_UNDER_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bSwitchWheelAndStats = TRUE
ENDPROC

PROC SET_SNIPER_SCOPE_UNDER_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bSniperScopeOn = TRUE
ENDPROC

PROC SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE
ENDPROC

PROC SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
	MPGlobalsScoreHud.bTitleMiddleJustified = TRUE
ENDPROC

PROC SET_DECIMAL_POINTS_FOR_BOTTOM_RIGHT_HUD_THIS_FRAME(INT iDecimalPoints = 1)
	g_b_iNumberOfDecimalPlacesForScore = iDecimalPoints
ENDPROC



//BOOL ValueToTitleWidgetStart
//FLOAT ValueToTitle_THREESETS_WIDGET
//FLOAT ValueToTitle_TWOSETS_WIDGET
//FLOAT ValueToTitle_SINGLE_NUMBER_WIDGET
//
//FLOAT ValueToTitle_DASHSINGLE_WIDGET
//FLOAT ValueToTitle_DASHDOUBLE_WIDGET
//FLOAT ValueToTitle_DASHTRIPLE_WIDGET
//FLOAT ValueToTitle_POSITION_SYMBOL_WIDGET
//FLOAT ValueToTitle_SCORE_WIDGET
//FLOAT ValueToTitle_SCORE_SMALL_WIDGET
//FLOAT ValueToTitle_SCORE_SMALL_NON_ROMANIC_WIDGET
//FLOAT ValueToTitle_TIMER_PLYR_NAME
//FLOAT ValueToTitle_TIMER_AMPM

FUNC FLOAT GET_VALUE_TO_TITLE_OFFSET(TEXT_STYLE& BottomTextUsed)
					
//	#IF IS_DEBUG_BUILD					
//		IF ValueToTitleWidgetStart = FALSE
//			START_WIDGET_GROUP("GET_VALUE_TO_TITLE_OFFSET")
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: THREESETS", ValueToTitle_THREESETS_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: TWOSETS", ValueToTitle_TWOSETS_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: SINGLE NUMBER", ValueToTitle_SINGLE_NUMBER_WIDGET, -1, 1, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: DASHSINGLE", ValueToTitle_DASHSINGLE_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: DASHDOUBLE", ValueToTitle_DASHDOUBLE_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: DASHTRIPLE", ValueToTitle_DASHTRIPLE_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: POSITION", ValueToTitle_POSITION_SYMBOL_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: SCORE", ValueToTitle_SCORE_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: SCORE SMALL", ValueToTitle_SCORE_SMALL_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: SCORE SMALL NON ROMANIC", ValueToTitle_SCORE_SMALL_NON_ROMANIC_WIDGET, -1, 1, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: PLAYER NAME", ValueToTitle_TIMER_PLYR_NAME, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_TO_TITLE_OFFSET: AMPM", ValueToTitle_TIMER_AMPM, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			ValueToTitleWidgetStart = TRUE
//		ENDIF
//	#ENDIF						
			
	FLOAT Result					
						
	SWITCH BottomTextUsed.aTextType

		CASE TEXTTYPE_TS_UI_TIMERNUMBER_THREESETS
			Result = (-0.010)-0.005+0.004-0.0005-0.002+0.002+0.0005
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_THREESETS_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_TIMERNUMBER_TWOSETS
			Result =  (-0.025)-0.003+0.002-0.006+0.001
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_TWOSETS_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SINGLE_NUMBER
			Result =  (-0.047)-0.004+0.012+0.001+0.007
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_SINGLE_NUMBER_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_DASHSINGLE
			Result =  (-0.019)+0.011+0.004
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_DASHSINGLE_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_DASHDOUBLE
			Result =  (-0.012)+0.001
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_DASHDOUBLE_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_DASHTRIPLE
			Result =  (-0.012)+0.001+0.001
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_DASHTRIPLE_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_POSITION_SYMBOL
			Result =  (-0.035)-0.008+0.006+0.013+0.0005
			
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_POSITION_SYMBOL_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SCORE
			Result = (-0.010)-0.005+0.004-0.001+0.001
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_SCORE_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SCORE_SMALL
			Result = (-0.010)-0.005+0.004+0.005+0.0007
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_SCORE_SMALL_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SCORE_SMALL_NON_ROMANIC
			Result = (-0.010)-0.005+0.004+0.005+0.003
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_SCORE_SMALL_NON_ROMANIC_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_TIMER_PLYR_NAME
			Result = (-0.010)-0.005+0.004+0.005
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_TIMER_PLYR_NAME
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_AMPM
			Result = (-0.010)-0.005+0.004+0.005
//			#IF IS_DEBUG_BUILD
//				Result += ValueToTitle_TIMER_AMPM
//			#ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_LANGUAGE_NON_ROMANIC()
		Result += 0.003
	ENDIF
	
	
	RETURN Result 
	
ENDFUNC


//BOOL TITLEToTitleWidgetStart
//FLOAT TitleOffset_TIMER_PLYR_NAME
//FLOAT TitleOffset_TS_STANDARDSMALLHUD

FUNC FLOAT GET_TITLE_OFFSET(TEXT_STYLE& BottomTextUsed)
					
//	#IF IS_DEBUG_BUILD					
//		IF TITLEToTitleWidgetStart = FALSE
//			START_WIDGET_GROUP("GET_TITLE_OFFSET")
//				ADD_WIDGET_FLOAT_SLIDER("GET_TITLE_OFFSET: PLAYER NAME", TitleOffset_TIMER_PLYR_NAME, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_TITLE_OFFSET: SMALL HUD", TitleOffset_TS_STANDARDSMALLHUD, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			TITLEToTitleWidgetStart = TRUE
//		ENDIF
//	#ENDIF						
			
	FLOAT Result					
						
	SWITCH BottomTextUsed.aTextType

		
		CASE TEXTTYPE_TS_TIMER_PLYR_NAME
			Result = 0
//			#IF IS_DEBUG_BUILD
//				Result += TitleOffset_TIMER_PLYR_NAME
//			#ENDIF
		BREAK
//		CASE TEXTTYPE_TS_STANDARDSMALLHUD
//			Result = 0.0
//			#IF IS_DEBUG_BUILD
//				Result += TitleOffset_TS_STANDARDSMALLHUD
//			#ENDIF
//		BREAK
	ENDSWITCH
	
	
	
	RETURN Result 
	
ENDFUNC

//BOOL BarToTitleWidgetStart
//FLOAT BAR_TO_TITLE_WIDGET

FUNC FLOAT GET_BAR_TO_TITLE_OFFSET()

//	#IF IS_DEBUG_BUILD					
//		IF BarToTitleWidgetStart = FALSE
//			START_WIDGET_GROUP("GET_BAR_TO_TITLE_OFFSET")
//				ADD_WIDGET_FLOAT_SLIDER("GET_BAR_TO_TITLE_OFFSET: BAR_TO_TITLE", BAR_TO_TITLE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			BarToTitleWidgetStart = TRUE
//		ENDIF
//	#ENDIF

	FLOAT Result = (0.013)-0.002+0.001+0.001-0.001
//	#IF IS_DEBUG_BUILD
//		Result += BAR_TO_TITLE_WIDGET
//	#ENDIF
	RETURN Result
ENDFUNC


//BOOL CHECKPOINTToTitleWidgetStartX
//FLOAT CHECKPOINT_TO_TITLE_WIDGETX

FUNC FLOAT GET_CHECKPOINT_TO_TITLE_OFFSET_X()

//	#IF IS_DEBUG_BUILD					
//		IF CHECKPOINTToTitleWidgetStartX = FALSE
//			START_WIDGET_GROUP("GET_CHECKPOINT_TO_TITLE_OFFSET_X")
//				ADD_WIDGET_FLOAT_SLIDER("GET_CHECKPOINT_TO_TITLE_OFFSET_X: CHECKPOINT_TO_TITLE_WIDGETX", CHECKPOINT_TO_TITLE_WIDGETX, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			CHECKPOINTToTitleWidgetStartX = TRUE
//		ENDIF
//	#ENDIF

	FLOAT Result = 0.919-0.081+0.004-0.006+0.050-0.001-0.005+0.065-0.0005
//	#IF IS_DEBUG_BUILD
//		Result += CHECKPOINT_TO_TITLE_WIDGETX
//	#ENDIF
	RETURN Result
ENDFUNC

//BOOL BarToTitleWidgetStartX
//FLOAT BAR_TO_TITLE_WIDGETX


FUNC FLOAT GET_BAR_TO_TITLE_OFFSET_X()

//	#IF IS_DEBUG_BUILD					
//		IF BarToTitleWidgetStartX = FALSE
//			START_WIDGET_GROUP("GET_BAR_TO_TITLE_OFFSET_X")
//				ADD_WIDGET_FLOAT_SLIDER("GET_BAR_TO_TITLE_OFFSET_X: BAR_TO_TITLE_WIDGETX", BAR_TO_TITLE_WIDGETX, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			BarToTitleWidgetStartX = TRUE
//		ENDIF
//	#ENDIF

	FLOAT Result = 0.919-0.081+0.004-0.006+0.050-0.001
//	#IF IS_DEBUG_BUILD
//		Result += BAR_TO_TITLE_WIDGETX
//	#ENDIF
	RETURN Result
ENDFUNC

//BOOL DamageBarToTitleWidgetStartX
//FLOAT DAMAGE_BAR_TO_TITLE_WIDGETX

FUNC FLOAT GET_DAMAGE_BAR_TO_TITLE_OFFSET_X()

//	#IF IS_DEBUG_BUILD					
//		IF DamageBarToTitleWidgetStartX = FALSE
//			START_WIDGET_GROUP("GET_DAMAGE_BAR_TO_TITLE_OFFSET_X")
//				ADD_WIDGET_FLOAT_SLIDER("GET_DAMAGE_BAR_TO_TITLE_OFFSET_X: BAR_TO_TITLE_WIDGETX", DAMAGE_BAR_TO_TITLE_WIDGETX, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			DamageBarToTitleWidgetStartX = TRUE
//		ENDIF
//	#ENDIF

	FLOAT Result = 0.919-0.081+0.028+0.050-0.001-0.002
//	#IF IS_DEBUG_BUILD
//		Result += DAMAGE_BAR_TO_TITLE_WIDGETX
//	#ENDIF
	RETURN Result
ENDFUNC


//BOOL EXTRATIMEToTitleWidgetStart
//FLOAT EXTRATIME_TO_TITLE_WIDGET

FUNC FLOAT GET_EXTRATIME_TO_TITLE_OFFSET()

//	#IF IS_DEBUG_BUILD					
//		IF EXTRATIMEToTitleWidgetStart = FALSE
//			START_WIDGET_GROUP("GET_EXTRATIME_TO_TITLE_OFFSET")
//				ADD_WIDGET_FLOAT_SLIDER("GET_EXTRATIME_TO_TITLE_OFFSET: EXTRATIME_TO_TITLE", EXTRATIME_TO_TITLE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			EXTRATIMEToTitleWidgetStart = TRUE
//		ENDIF
//	#ENDIF

	FLOAT Result = -0.004-0.007
//	#IF IS_DEBUG_BUILD
//		Result += EXTRATIME_TO_TITLE_WIDGET
//	#ENDIF
	RETURN Result
ENDFUNC

//BOOL GapWordWidgetStart
//FLOAT TIMERNUMBER_THREESETS_WIDGET
//FLOAT TIMERNUMBER_TWOSETS_WIDGET
//FLOAT SINGLE_NUMBER_WIDGET
//FLOAT TRIPLE_NUMBER_WIDGET
//FLOAT DOUBLE_NUMBER_WIDGET
//FLOAT SCORE_NUMBER_WIDGET
//FLOAT SCORE_SMALL_NUMBER_WIDGET
//FLOAT SCORE_SMALL_NUMBER_NON_ROMANIC_WIDGET
//FLOAT AMPM_NUMBER_WIDGET

FUNC FLOAT GET_VALUE_OF_GAP(TEXT_STYLE& BottomTextUsed)
			
//	#IF IS_DEBUG_BUILD					
//		IF GapWordWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("GET_VALUE_OF_GAP")
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: THREESETS", TIMERNUMBER_THREESETS_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: TWOSETS", TIMERNUMBER_TWOSETS_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: SINGLE NUMBER", SINGLE_NUMBER_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: TRIPLE NUMBER", TRIPLE_NUMBER_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: DOUBLE NUMBER", DOUBLE_NUMBER_WIDGET, -1, 1, 0.001)				
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: SCORE NUMBER", SCORE_NUMBER_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: SCORE SMALL NUMBER", SCORE_SMALL_NUMBER_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: AMPM NUMBER", AMPM_NUMBER_WIDGET, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP: SCORE SMALL NON ROMANIC NUMBER", SCORE_SMALL_NUMBER_NON_ROMANIC_WIDGET, -1, 1, 0.001)
//			
//			STOP_WIDGET_GROUP()
//			GapWordWidgetStart = TRUE
//		ENDIF
//	#ENDIF
		
	FLOAT Result					
						
	SWITCH BottomTextUsed.aTextType


		CASE TEXTTYPE_TS_UI_TIMERNUMBER_THREESETS
			Result= (0.035)+0.023-0.003+0.001-0.007-0.012+0.001+0.0018+0.0005-0.0005
//			#IF IS_DEBUG_BUILD
//				Result += TIMERNUMBER_THREESETS_WIDGET
//			#ENDIF
	
		BREAK
		CASE TEXTTYPE_TS_UI_TIMERNUMBER_TWOSETS
			Result= (0.035)+0.023-0.003+0.001-0.007+0.003+0.002-0.004
//			#IF IS_DEBUG_BUILD
//				Result += TIMERNUMBER_TWOSETS_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SINGLE_NUMBER
			Result= (0.065)+0.009-0.006-0.009+0.001-0.009
//			#IF IS_DEBUG_BUILD
//				Result += SINGLE_NUMBER_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_DASHSINGLE
			Result= 0.0 //HudOffsetArray[3]
		BREAK
		CASE TEXTTYPE_TS_UI_DASHDOUBLE
			Result= (0.065)+0.009
//			#IF IS_DEBUG_BUILD
//				Result += DOUBLE_NUMBER_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_DASHTRIPLE
			Result= (0.065)+0.009-0.015-0.022
//			#IF IS_DEBUG_BUILD
//				Result += TRIPLE_NUMBER_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_POSITION_SYMBOL
			Result= 0.0 //HudOffsetArray[5]
		BREAK
		CASE TEXTTYPE_TS_UI_SCORE
			Result= (0.035)+0.023-0.003+0.001-0.007-0.012+0.001+0.002+0.0003
//			#IF IS_DEBUG_BUILD
//				Result += SCORE_NUMBER_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SCORE_SMALL
			Result= (0.035)+0.023-0.003+0.001-0.007-0.012-0.0005
//			#IF IS_DEBUG_BUILD
//				Result += SCORE_SMALL_NUMBER_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_SCORE_SMALL_NON_ROMANIC
			Result= (0.035)+0.023-0.003+0.001-0.007-0.012
//			#IF IS_DEBUG_BUILD
//				Result += SCORE_SMALL_NUMBER_NON_ROMANIC_WIDGET
//			#ENDIF
		BREAK
		CASE TEXTTYPE_TS_UI_AMPM
			Result= (0.035)+0.023-0.003+0.001-0.007-0.012
//			#IF IS_DEBUG_BUILD
//				Result += AMPM_NUMBER_WIDGET
//			#ENDIF
		BREAK

	ENDSWITCH

	RETURN Result
	
ENDFUNC


//BOOL GapBarWidgetStart
//FLOAT GapBarWidgetValue

FUNC FLOAT GET_VALUE_OF_GAP_BAR_SPRITE(BOOL bBigMeter , BOOL SpriteBar)

//	#IF IS_DEBUG_BUILD					
//		IF GapBarWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("GET_VALUE_OF_GAP_BAR_SPRITE")
//				ADD_WIDGET_FLOAT_SLIDER("GET_VALUE_OF_GAP_BAR_SPRITE: GapBarWidgetValue", GapBarWidgetValue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapBarWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	
	FLOAT Result = (0.025)+0.006+0.0009
	IF bBigMeter = TRUE
		 Result +=  0.008
	ENDIF
	IF SpriteBar
		Result +=  0.008
	ENDIF
//	#IF IS_DEBUG_BUILD
//		Result += GapBarWidgetValue
//	#ENDIF
	
	
	RETURN Result
ENDFUNC


//PROC SET_Y_SHIFT_START(UIELEMENTS WhichSpace, TEXT_STYLE& BottomTextUsed)
PROC SET_Y_SHIFT_START(UIELEMENTS WhichSpace)
	SWITCH WhichSpace
		CASE UIELEMENTS_BOTTOMRIGHT
			MPGlobalsScoreHud.BottomStartY  = BASE_LINE_OFFSET()	
		BREAK
		CASE UIELEMENTS_MIDDLERIGHT
			MPGlobalsScoreHud.HudStartY =  BASE_LINE_OFFSET()

		BREAK
	ENDSWITCH 
	
ENDPROC

FUNC BOOL IS_SCREEN_NARROW()
	IF IS_PC_VERSION()
		INT iX, iY
		GET_ACTUAL_SCREEN_RESOLUTION(iX, iY)
		IF iX <= 1024
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//BOOL TitleWrapWidgetStart
//FLOAT TitleWrapWidgetValue

PROC SET_WORD_WRAPPING_TITLE(TEXT_STYLE& aTitle)
	
	FLOAT Result = 0.880-0.062+0.026+0.027+0.030
	
//	#IF IS_DEBUG_BUILD					
//		IF TitleWrapWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_TITLE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_TITLE: TitleWrapWidgetValue", TitleWrapWidgetValue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			TitleWrapWidgetStart = TRUE
//		ENDIF
//		Result += TitleWrapWidgetValue
//	#ENDIF
	

	IF MPGlobalsScoreHud.bTitleFarLeftJustified
//		NET_NL()NET_PRINT("SET_WORD_WRAPPING_TITLE: bTitleFarLeftJustified = TRUE ")
		Result += -0.034
		IF GET_IS_WIDESCREEN() = FALSE
			Result += -0.02
		ENDIF
	ENDIF
	
	
	IF MPGlobalsScoreHud.bTitleMiddleJustified
	AND MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE 
//		NET_NL()NET_PRINT("SET_WORD_WRAPPING_TITLE: bTitleMiddleJustified = TRUE ")
		Result += -0.015-0.003
		IF GET_IS_WIDESCREEN() = FALSE
			Result += -0.017
		ENDIF
	ENDIF 
	
	IF MPGlobalsScoreHud.bTitleExtraLeftJustified 
	AND MPGlobalsScoreHud.bTitleMiddleJustified = FALSE 
	AND MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE 
//		NET_NL()NET_PRINT("SET_WORD_WRAPPING_TITLE: bTitleExtraLeftJustified = TRUE ")
		Result += -0.038
	ENDIF

	IF MPGlobalsScoreHud.bCoronaUnderHud = TRUE
//		NET_NL()NET_PRINT("SET_WORD_WRAPPING_TITLE: bCoronaUnderHud = TRUE ")
		Result += RIGHT_EDGE_CORONA_OFFSET
	ENDIF
	
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
	AND GET_CURRENT_LANGUAGE() = LANGUAGE_FRENCH
	AND NOT GET_IS_WIDESCREEN()
	AND MPGlobalsScoreHud.bTitleExtraLeftJustified
	
		Result += -0.007
	ENDIF
 
 	IF  MPGlobalsScoreHud.bTitleInsaneLeftJustified
		IF GET_CURRENT_LANGUAGE() != LANGUAGE_ENGLISH
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_PORTUGUESE
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_POLISH
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_MEXICAN
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE_SIMPLIFIED 
			Result += -0.009
 		ELSE
			Result += -0.009
		ENDIF
	ENDIF
	
	
	IF MPGlobalsScoreHud.bTitleInsanePlusLeftJustified
		IF IS_SCREEN_NARROW()
			Result += -0.03
		ELSE
			IF GET_CURRENT_LANGUAGE() != LANGUAGE_ENGLISH
			AND GET_CURRENT_LANGUAGE() != LANGUAGE_PORTUGUESE
			AND GET_CURRENT_LANGUAGE() != LANGUAGE_POLISH
			AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE
			AND GET_CURRENT_LANGUAGE() != LANGUAGE_MEXICAN
			AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE_SIMPLIFIED 
				Result += -0.024
	 		ELSE
				Result += -0.019 //-0.014
			ENDIF
		ENDIF
	ENDIF
	
	
	IF (GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_FRENCH)
	AND MPGlobalsScoreHud.bTitleMiddleJustified = FALSE 
	AND MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE 
	AND MPGlobalsScoreHud.bCoronaUnderHud = FALSE
	AND MPGlobalsScoreHud.bTitleExtraLeftJustified = FALSE
	AND MPGlobalsScoreHud.bTitleInsaneLeftJustified = FALSE
	AND MPGlobalsScoreHud.bTitleInsanePlusLeftJustified = FALSE
	AND IS_PC_VERSION() 	
		Result += -0.005
	ENDIF
	
	

	aTitle.WrapEndX = Result
ENDPROC


//BOOL MULTIPLYERWrapWidgetStart
//FLOAT MULTIPLYERWrapWidgetValue

PROC SET_WORD_WRAPPING_MULTIPLYER(TEXT_STYLE& aTitle)
	
	FLOAT Result = 0.880-0.062+0.026+0.027+0.037+0.003
	
//	#IF IS_DEBUG_BUILD					
//		IF MULTIPLYERWrapWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_MULTIPLYER")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_MULTIPLYER: MULTIPLYERWrapWidgetValue", MULTIPLYERWrapWidgetValue, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			MULTIPLYERWrapWidgetStart = TRUE
//		ENDIF
//		Result += MULTIPLYERWrapWidgetValue
//	#ENDIF

	
	aTitle.WrapEndX = Result
ENDPROC


PROC SET_WORD_WRAPPING_EXTRATIME(TEXT_STYLE& aTitle, INT Number)//, STRING Title)
//	FLOAT Width = GET_STRING_WIDTH(Title) 
//	aTitle.WrapEndX = (1/Width*(0.006))+0.790 // (1/Width*(HudOffsetArray[1]))+HudOffsetArray[0]// 
	IF Number < 9999  
		aTitle.WrapEndX = 0.933
	ELSE
		aTitle.WrapEndX = 0.933+0.008
	ENDIF
ENDPROC

//BOOL GapWordDASHDOUBLEWidgetStart
//FLOAT DASH_DOUBLE_WIDGET

PROC SET_WORD_WRAPPING_DASH_DOUBLE(TEXT_STYLE& aTitle)
//	#IF IS_DEBUG_BUILD					
//		IF GapWordDASHDOUBLEWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_DASH_DOUBLE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_DASH_DOUBLE: DASH_DOUBLE_WIDGET", DASH_DOUBLE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapWordDASHDOUBLEWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	aTitle.WrapEndX = (0.928)-0.051+0.050-0.021-0.005+0.011-0.005
	
//	#IF IS_DEBUG_BUILD
//		aTitle.WrapEndX += DASH_DOUBLE_WIDGET
//	#ENDIF
	
ENDPROC

//BOOL GapWordDASHDOUBLEPlaceWidgetStart
//FLOAT DASH_DOUBLE_PLACE_WIDGET

PROC SET_WORD_WRAPPING_DASH_DOUBLE_PLACE(TEXT_STYLE& aTitle)
//	#IF IS_DEBUG_BUILD					
//		IF GapWordDASHDOUBLEPlaceWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_DASH_DOUBLE_PLACE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_DASH_DOUBLE_PLACE: DASH_DOUBLE_PLACE_WIDGET", DASH_DOUBLE_PLACE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapWordDASHDOUBLEPlaceWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	aTitle.WrapEndX = (0.928)-0.051+0.050-0.021+0.023
	
//	#IF IS_DEBUG_BUILD
//		aTitle.WrapEndX += DASH_DOUBLE_PLACE_WIDGET
//	#ENDIF
	
ENDPROC

//BOOL GapWordDASHTRIPLEWidgetStart
//FLOAT DASH_TRIPLE_WIDGET

PROC SET_WORD_WRAPPING_DASH_TRIPLE(TEXT_STYLE& aTitle)
//	#IF IS_DEBUG_BUILD					
//		IF GapWordDASHTRIPLEWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_DASH_TRIPLE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_DASH_TRIPLE: DASH_TRIPLE_WIDGET", DASH_TRIPLE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapWordDASHTRIPLEWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	aTitle.WrapEndX = (0.928)-0.051+0.050-0.010+0.001
	
//	#IF IS_DEBUG_BUILD
//		aTitle.WrapEndX += DASH_TRIPLE_WIDGET
//	#ENDIF
	
ENDPROC

//BOOL GapWordDASHTRIPLEPLACEWidgetStart
//FLOAT DASH_TRIPLE_PLACE_WIDGET

PROC SET_WORD_WRAPPING_DASH_TRIPLE_PLACE(TEXT_STYLE& aTitle)
//	#IF IS_DEBUG_BUILD					
//		IF GapWordDASHTRIPLEPLACEWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_DASH_TRIPLE_PLACE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_DASH_TRIPLE_PLACE: DASH_TRIPLE_PLACE_WIDGET", DASH_TRIPLE_PLACE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapWordDASHTRIPLEPLACEWidgetStart = TRUE
//		ENDIF
//	#ENDIF
	aTitle.WrapEndX = (0.928)-0.051+0.050-0.010+0.001+0.007-0.002
	
//	#IF IS_DEBUG_BUILD
//		aTitle.WrapEndX += DASH_TRIPLE_PLACE_WIDGET
//	#ENDIF
	
ENDPROC

//BOOL GapWordDASHSINGLEWidgetStart
//FLOAT DASH_SINGLE_WIDGET

PROC SET_WORD_WRAPPING_DASH_SINGLE(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF GapWordDASHSINGLEWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_DASH_SINGLE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_DASH_SINGLE: DASH_SINGLE_WIDGET", DASH_SINGLE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapWordDASHSINGLEWidgetStart = TRUE
//		ENDIF
//	#ENDIF

	aTitle.WrapEndX = (0.918)-0.054+0.050+0.010
	
//	#IF IS_DEBUG_BUILD
//		aTitle.WrapEndX += DASH_SINGLE_WIDGET
//	#ENDIF
ENDPROC

//BOOL GapWordDASHSINGLEPLACEWidgetStart
//FLOAT DASH_SINGLEPLACE_WIDGET

PROC SET_WORD_WRAPPING_DASH_SINGLE_PLACE(TEXT_STYLE& aTitle)

//	#IF IS_DEBUG_BUILD					
//		IF GapWordDASHSINGLEPLACEWidgetStart = FALSE
//			
//			START_WIDGET_GROUP("SET_WORD_WRAPPING_DASH_SINGLE_PLACE")
//				ADD_WIDGET_FLOAT_SLIDER("SET_WORD_WRAPPING_DASH_SINGLE_PLACE: DASH_SINGLEPLACE_WIDGET", DASH_SINGLEPLACE_WIDGET, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			GapWordDASHSINGLEPLACEWidgetStart = TRUE
//		ENDIF
//	#ENDIF

	aTitle.WrapEndX = (0.933)-0.050+0.049+0.001-0.001
	
//	#IF IS_DEBUG_BUILD
//		aTitle.WrapEndX += DASH_SINGLEPLACE_WIDGET
//	#ENDIF
	
ENDPROC





PROC SET_WORD_WRAPPING_TO_POSITION(TEXT_STYLE& aTitle, FLOAT WrapTo)
	aTitle.WrapEndX = WrapTo
ENDPROC



/// PURPOSE:
///    Multiplier for spacing between timer elements based on screen aspect ratio
FUNC FLOAT GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT()
	FLOAT fAR = GET_ASPECT_RATIO(FALSE)
	
	
	INT x, y
	GET_ACTUAL_SCREEN_RESOLUTION(x, y)
//	PRINTLN("x = ",x," y = ", y, " " )
	FLOAT vAR = TO_FLOAT(x)/TO_FLOAT(y)

	
	#IF IS_DEBUG_BUILD
	BOOL bPrint = FALSE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ScreenRatioPrints")
		bPrint = TRUE
	ENDIF

	IF bPrint 
		PRINTLN("[TIMERRATIO] fAR = ", fAR)
		PRINTLN("[TIMERRATIO] vAR = ", vAR)
		PRINTLN("[TIMERRATIO] PROFILE_DISPLAY_SAFEZONE_SIZE = ",GET_PROFILE_SETTING(PROFILE_DISPLAY_SAFEZONE_SIZE))
	ENDIF
	#ENDIF
	
	fAR = fMIN(fAR, vAR)
	
	#IF IS_DEBUG_BUILD
	IF bPrint 
		PRINTLN("[TIMERRATIO] after fAR = ", fAR)
	ENDIF
	#ENDIF
	
	IF vAR > 3.5
	AND fAR > 1.7
		#IF IS_DEBUG_BUILD
		IF bPrint 
			PRINTLN("[TIMERRATIO] Big adjustment actually needed vAR is very large, returning 1.4 ")
		ENDIF
		#ENDIF
		RETURN 1.4
		
	ENDIF
	
	if fAR > 1.7		//16:9, default
		#IF IS_DEBUG_BUILD
		IF bPrint 
			PRINTLN("[TIMERRATIO] RETURN 1.0 ")
		ENDIF
		#ENDIF
		RETURN 1.0
	elif fAR > 1.5		//16:10, mild adjustment
		#IF IS_DEBUG_BUILD
		IF bPrint 
			PRINTLN("[TIMERRATIO] RETURN 1.2 ")
		ENDIF
		#ENDIF
		RETURN 1.2
	ELIF fAR > 1.3 		//4:3, big adjustment
		#IF IS_DEBUG_BUILD
		IF bPrint 
			PRINTLN("[TIMERRATIO] RETURN 1.3 ")
		ENDIF
		#ENDIF
		RETURN 1.3
	ENDIF
	#IF IS_DEBUG_BUILD
	IF bPrint 
		PRINTLN("[TIMERRATIO] RETURN 1.4 ")
	ENDIF
	#ENDIF
	
	RETURN 1.4		//Shouldn't get here, almost square AR, even bigger adjustment
ENDFUNC


//BOOL OFleckWidgets
//RECT aFleckDebug

PROC DRAW_ORGANISATION_FLECK(SPRITE_PLACEMENT& Overlay, HUD_COLOURS aColour)

	IF aColour = HUD_COLOUR_PURE_WHITE 
		EXIT
	ENDIF

//	IF OFleckWidgets = FALSE
//		
//		START_WIDGET_GROUP("SCORE PLACE")
//			CREATE_A_RECT_PLACEMENT_WIDGET(aFleckDebug, "Rectangle Fleck")
//		
//		STOP_WIDGET_GROUP()
//		OFleckWidgets = TRUE
//	ENDIF
	
	RECT aFleck
	aFleck.x = 0.951
	aFleck.y = Overlay.y
	aFleck.w = 0.002
	aFleck.h = Overlay.h
	
	INT R, G, B, A
	GET_HUD_COLOUR(aColour, R, G, B, A)
	aFleck.r = R
	aFleck.g = G
	aFleck.b = B
	aFleck.a = A
	
//	#IF IS_DEBUG_BUILD
//	UPDATE_RECT_WIDGET_VALUE(aFleck, aFleckDebug)
//	#ENDIF

	
	DRAW_RECTANGLE(aFleck)
	

ENDPROC







PROC INIT_SCREEN_GENERAL_SINGLE_SCORE(TEXT_STYLE& NumberStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace, UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle )

	FLOAT LargeGap = GET_VALUE_OF_GAP(NumberStyle) //0.115

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
			
	NumberPlace.x = 0.795
	NumberPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberStyle)
		
	CHANGE_Y_SHIFT_END(WhichSpace, -LargeGap)
	

ENDPROC


PROC DISPLAY_TEXT_FROM_SINGLE_SCORE_GUTS(TEXT_STYLE& TitleStyle, TEXT_PLACEMENT& TitlePlace, STRING Title, HUD_COLOURS TitleColour, HUD_COLOURS aColour, INT TitleNumber, BOOL IsPlayer, BOOL UseTitleColourForTitle)

	IF IsPlayer = TRUE
		IF UseTitleColourForTitle
			DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title,"",  TitleColour, FONT_RIGHT)
		ELSE
			DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title,"",  aColour, FONT_RIGHT)
		ENDIF

	ELSE
		IF TitleNumber = -1
			DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
		ELSE
			TitlePlace.y += (TITLE_NON_ROMANIC_OFFSET_LITTLE - 0.001)
			DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
		ENDIF  
	ENDIF
	
ENDPROC

//
//TEXT_STYLE TitleStyleWidgetScore, NumberStyleWidgetScore
//TEXT_PLACEMENT TitlePlacementWidgetScore, NumberPlacmentWidgetScore
//SPRITE_PLACEMENT OverlayWidgetScore, XPWidgetScore
//BOOL ScoreWidgets
//FLOAT TitleWidgetOffset
//
//
//FLOAT WidgetStringLengthConverter_SCORE
//FLOAT WidgetStringLengthConverter_x_SCORE
//FLOAT WidgetStringLengthConverter_b_SCORE
//FLOAT WidgetStringLengthConverter_m_SCORE
//SPRITE_PLACEMENT WarningWidgetSCORE

PROC SET_NUMBER_PULSE_COLOUR(TEXT_STYLE &eNumberStyle, HUD_COLOURS eStartingColour, HUD_COLOURS ePulseColour, INT iPulseTime, SCRIPT_TIMER stPulseTimer)
	INT iR, iG, iB, iA, iPR, iPG, iPB, iPA
	INT iElapsedTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPulseTimer)
	FLOAT fPulseTime
	
	IF (iPulseTime / 2) > iElapsedTime
		fPulseTime = (TO_FLOAT(iElapsedTime / 2) / TO_FLOAT(iPulseTime / 2))
		GET_HUD_COLOUR(eStartingColour, iR, iG, iB, iA)
		GET_HUD_COLOUR(ePulseColour, iPR, iPG, iPB, iPA)
	ELSE
		fPulseTime = (TO_FLOAT(iElapsedTime) / TO_FLOAT(iPulseTime))
		GET_HUD_COLOUR(ePulseColour, iR, iG, iB, iA)
		GET_HUD_COLOUR(eStartingColour, iPR, iPG, iPB, iPA)
	ENDIF
	
	eNumberStyle.r = FLOOR(LERP_FLOAT(TO_FLOAT(iR), TO_FLOAT(iPR), fPulseTime))
	eNumberStyle.g = FLOOR(LERP_FLOAT(TO_FLOAT(iG), TO_FLOAT(iPG), fPulseTime))
	eNumberStyle.b = FLOOR(LERP_FLOAT(TO_FLOAT(iB), TO_FLOAT(iPB), fPulseTime))
	eNumberStyle.a = FLOOR(LERP_FLOAT(TO_FLOAT(iA), TO_FLOAT(iPA), fPulseTime))
ENDPROC

PROC DRAW_GENERAL_SINGLE_SCORE_GUTS(INT index, TEXT_STYLE& TitleStyle, TEXT_STYLE& NumberStyle,TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace, INT Number, UIELEMENTS WhichSpace, STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber, BOOL isPlayer, STRING NumberString, BOOL isFloat, FLOAT FloatValue, HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS TitleColour, BOOL bDisplayWarning, INT MaXNumber, BOOL DrawInfinity, ACTIVITY_POWERUP aPowerup, HUD_COUNTER_STYLE eCounterStyle, STRING sPlacementText, BOOL bUseNonPlayerFont = FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, INT iAlpha = 255, BOOL bDisplayBlankScore = FALSE, PLAYER_INDEX pPlayerID = NULL, BOOL bFlashTitle = FALSE, BOOL bDrawLineUnderName = FALSE, HUD_COLOURS LineUnderNameColour = HUD_COLOUR_WHITE, BOOL bEnablePulsing = FALSE, HUD_COLOURS PulseToColour = HUD_COLOUR_PURE_WHITE, INT iPulseTime = -1, BOOL bUsePlacementText = FALSE, BOOL bLiteralPlacementText = FALSE, BOOL bCustomFont = FALSE, TEXT_FONTS eCustomFont = FONT_STANDARD)

	MPGlobalsScoreHud.iHowManyDisplays++
//	INT I	
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		
			
		SPRITE_PLACEMENT WarningIcon
		SET_SPRITE_PLACEMENT(WarningIcon, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		SPRITE_PLACEMENT UpIcon
		SET_SPRITE_PLACEMENT(UpIcon, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		SPRITE_PLACEMENT DownIcon
		SET_SPRITE_PLACEMENT(DownIcon, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)

		BOOL bDisplayTimerCheckbox
		
		IF IS_LANGUAGE_NON_ROMANIC()
			IF bUseNonPlayerFont
				SET_STANDARD_SMALL_HUD_TEXT_NON_ROMANIC(TitleStyle, DROPSTYLE_NONE)
			ELIF isPlayer
				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
			ELSE
				SET_STANDARD_SMALL_HUD_TEXT_NON_ROMANIC(TitleStyle, DROPSTYLE_NONE)
			ENDIF
		ELSE
			IF bUseNonPlayerFont
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ELIF isPlayer
				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
			ELSE
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ENDIF
		ENDIF
		
		
		#IF USE_TU_CHANGES
			IF g_b_ChangePlayerNameToTeamName
			AND isPlayer
				IF IS_LANGUAGE_NON_ROMANIC()
					SET_STANDARD_SMALL_HUD_TEXT_NON_ROMANIC(TitleStyle, DROPSTYLE_NONE)
				ELSE
					SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
				ENDIF
				
			ENDIF
		#ENDIF
		
		
		
		IF Number < 1000000
		
			IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
			AND (FloatValue >= 100)
			AND ARE_STRINGS_EQUAL("AMCH_KMHN", NumberString) 
				SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(NumberStyle, DROPSTYLE_NONE)
			ELIF (GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE  OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED 	)
			AND (Number > 999 OR MaXNumber > 999 OR FloatValue > 1000)
				SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(NumberStyle, DROPSTYLE_NONE)
			ELSE
				IF MaXNumber > 99
					SET_STANDARD_UI_SCORE_SMALL_NUMBER(NumberStyle, DROPSTYLE_NONE)
				ELSE
					SET_STANDARD_UI_SCORE_NUMBER(NumberStyle, DROPSTYLE_NONE)
				ENDIF
			ENDIF
		ELSE	
			IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
			AND ( ARE_STRINGS_EQUAL("HUD_CASH", NumberString)
			OR ARE_STRINGS_EQUAL("HUD_CASH_NEG", NumberString))
			
				SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(NumberStyle, DROPSTYLE_NONE)
			ELSE
				SET_STANDARD_UI_SCORE_SMALL_NUMBER(NumberStyle, DROPSTYLE_NONE)
			ENDIF
		ENDIF
		
		
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
				
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		

		
//		IF ScoreWidgets = FALSE
//		
//			START_WIDGET_GROUP("SCORE PLACE")
//
//				CREATE_A_TEXT_PLACEMENT_WIDGET(TitlePlacementWidgetScore, "TitlePlacementWidgetScore")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(NumberPlacmentWidgetScore, "NumberPlacmentWidgetScore")
//				
//				CREATE_A_TEXT_STYLE_WIGET(TitleStyleWidgetScore, "TitleStyleWidgetScore")
//				CREATE_A_TEXT_STYLE_WIGET(NumberStyleWidgetScore, "NumberStyleWidgetScore")
//			
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(XPWidgetScore, "XPWidgetScore")
//			
//				ADD_WIDGET_FLOAT_SLIDER("TitleWidgetOffset", TitleWidgetOffset, -5, 5, 0.001)
//				
//			
//			STOP_WIDGET_GROUP()
//		
//			START_WIDGET_GROUP("SINGLE SCORE OVERLAY")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetScore, "OverlayWidgetScore")
//			
//			STOP_WIDGET_GROUP()
//			
//			
//			START_WIDGET_GROUP("SINGLE SCORE WARNING")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(WarningWidgetSCORE, "WarningWidgetSCORE")
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_SCORE", WidgetStringLengthConverter_SCORE, -1000, 1000, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_x_SCORE", WidgetStringLengthConverter_x_SCORE, -1000, 1000, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_m_SCORE", WidgetStringLengthConverter_m_SCORE, -1000, 1000, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_b_SCORE", WidgetStringLengthConverter_b_SCORE, -1000, 1000, 0.001)
//			
//			STOP_WIDGET_GROUP()
//			
//			ScoreWidgets = TRUE
//			
//		ENDIF
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_SCORE, Index) 
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		INIT_SCREEN_GENERAL_SINGLE_SCORE(NumberStyle, TitlePlace, NumberPlace, WhichSpace, TitleStyle)
	
	
		// Invert black/white foreground/background to highlight as input counter
		IF eCounterStyle = HUD_COUNTER_STYLE_INPUT_ARROWS
			TitleColour		= HUD_COLOUR_BLACK
			aColour			= HUD_COLOUR_BLACK
		ENDIF
	
		SET_WORD_HUD_COLOUR(NumberStyle, aColour)
		
		
//		#IF IS_DEBUG_BUILD
//		
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TitlePlace,  TitlePlacementWidgetScore)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberPlace,  NumberPlacmentWidgetScore)
//
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, TitleStyleWidgetScore)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle,NumberStyleWidgetScore)
//		
//		#ENDIF
		
		IF FlashTime = 0
			RESET_GENERIC_SCORE_FLASHING(Index)
		ENDIF
		
		IF ColourFlash = 0 
			RESET_GENERIC_SCORE_FLASHING_COLOUR(Index)
		ENDIF
	
	
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
		
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
		
			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericScore_Hud[index],MPGlobalsScoreHud.iFlashing_GenericScore_MiniHud[index])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF
		
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			
			WarningIcon.x = TitlePlace.x
			WarningIcon.y = TitlePlace.y+0.001
			
			// Display with larger background height to highlight as input counter
			IF eCounterStyle = HUD_COUNTER_STYLE_INPUT_ARROWS
			
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_BIGNUM
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_BIGNUM
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				
			ELIF Number < 1000000
			
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_SCORE
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_SCORE
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				
			ELSE
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_SCORESML
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_SCORESML
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			ENDIF
			
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			
//			#IF IS_DEBUG_BUILD
//			UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetScore)
//			UPDATE_SPRITE_WIDGET_VALUE(WarningIcon, WarningWidgetSCORE)
//			#ENDIF
			
					
			
			IF ColourFlash > 0
				SPRITE_PLACEMENT ColourOverlays = Overlays
				IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericScore[Index], 2000) = FALSE
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericScore[Index], 1250)
						MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[Index]-17
					ENDIF
					ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[Index]
					IF ColourFlashType = HUDFLASHING_FLASHRED
						SET_SPRITE_HUD_COLOUR(ColourOverlays, HUD_COLOUR_RED)
					ELIF ColourFlashType = HUDFLASHING_FLASHGREEN
						SET_SPRITE_HUD_COLOUR(ColourOverlays, HUD_COLOUR_GREEN)
					ELSE
						SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
					ENDIF					
					DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				ENDIF
			ELSE	
				MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[Index] = 255
				REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericScore[Index])
			ENDIF
			
			// Invert black/white foreground/background to highlight as input counter
			IF eCounterStyle = HUD_COUNTER_STYLE_INPUT_ARROWS
				DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			ELSE
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			ENDIF
			

			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)

			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)

			SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, TitleColour) 
			
			BOOL UseTitleColourForTitle = TRUE
			#IF USE_TU_CHANGES
				IF g_b_ChangePlayerNameToTeamName
					UseTitleColourForTitle = FALSE
				ENDIF
				IF g_b_UseBottomRightTitleColour = TRUE
					UseTitleColourForTitle = TRUE
				ENDIF
			#ENDIF

			SET_TEXT_STYLE(TitleStyle)
			
			IF IS_LANGUAGE_NON_ROMANIC()
			
				IF isPlayer
				
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED 
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
						NumberPlace.y += PLAYERNAME_TITLE_OFFSET
					ELSE
						TitlePlace.y += PLAYERNAME_TITLE_OFFSET
					ENDIF
				ELIF IS_LANGUAGE_NON_ROMANIC()
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED 
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
						NumberPlace.y += PLAYERNAME_TITLE_OFFSET
					ELSE
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
					ENDIF
				ENDIF	
			
			ELSE
				IF bUseNonPlayerFont
					TitlePlace.y +=0.0
				ELIF isPlayer
				#IF USE_TU_CHANGES
				AND g_b_ChangePlayerNameToTeamName = FALSE
				#ENDIF
					TitlePlace.y += -0.002-0.004
				ENDIF
			ENDIF

			
			// Display with larger background height to highlight as input counter
			IF eCounterStyle = HUD_COUNTER_STYLE_INPUT_ARROWS
				TitlePlace.y -= 0.007
				NumberPlace.y -= 0.007
			ENDIF			

//			#IF IS_DEBUG_BUILD
//				TitlePlace.y += TitleWidgetOffset
//			#ENDIF
		
			IF aPowerup = ACTIVITY_POWERUP_ROCKETS
			AND IS_STRING_NULL_OR_EMPTY(Title)
				Title = "HUD_ROCKET"
				
			ELIF aPowerup = ACTIVITY_POWERUP_BOOSTS
				Title = "HUD_BOOST"
			
			ELIF aPowerup = ACTIVITY_POWERUP_SPIKES
				Title = "HUD_SPIKES"
			
			ENDIF
			
			// Set title and score alpha B* 3360285
			TitleStyle.a = iAlpha
			NumberStyle.a = iAlpha
			
			// Display blank score B* 3360276
			IF bDisplayBlankScore
				NumberStyle.a = 0
			ENDIF
			
			IF (bCustomFont)
				TitleStyle.aFont = eCustomFont
			ENDIF
			
			IF bFlashTitle
				IF bDisplayTimerCheckbox
					DISPLAY_TEXT_FROM_SINGLE_SCORE_GUTS(TitleStyle, TitlePlace, Title, TitleColour, aColour, TitleNumber, IsPlayer, UseTitleColourForTitle)
				ENDIF			
			ELSE
				DISPLAY_TEXT_FROM_SINGLE_SCORE_GUTS(TitleStyle, TitlePlace, Title, TitleColour, aColour, TitleNumber, IsPlayer, UseTitleColourForTitle)
			ENDIF
			
			// Pulse between aColour & PulseToColour
			IF bEnablePulsing
				IF iPulseTime > 0
					IF NOT HAS_NET_TIMER_STARTED(MPGlobalsScoreHud.iColourPulsing_GenericBigNumber_Hud[index])
						START_NET_TIMER(MPGlobalsScoreHud.iColourPulsing_GenericBigNumber_Hud[index])
					ELIF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iColourPulsing_GenericBigNumber_Hud[index], iPulseTime)
						REINIT_NET_TIMER(MPGlobalsScoreHud.iColourPulsing_GenericBigNumber_Hud[index])
					ENDIF
					
					SET_NUMBER_PULSE_COLOUR(NumberStyle, aColour, PulseToColour, iPulseTime, MPGlobalsScoreHud.iColourPulsing_GenericBigNumber_Hud[index])
				ENDIF
			ENDIF
			
			// Display with up/down arrows to highlight as input counter
			IF eCounterStyle = HUD_COUNTER_STYLE_INPUT_ARROWS

				UpIcon.x = NumberStyle.WrapEndX
				UpIcon.y = NumberPlace.y-0.0175

				DownIcon.x = NumberStyle.WrapEndX
				DownIcon.y = NumberPlace.y+0.0175
				
				REQUEST_STREAMED_TEXTURE_DICT("MPArrow")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPArrow")
					
					UpIcon.x += 0.0095
					UpIcon.x -= 0.015
					UpIcon.y += 0.019//0.008
					UpIcon.w += 0.01//0.0125//0.022
					UpIcon.h += 0.01//0.0125//0.022//0.040
					UpIcon.r += 0//194
					UpIcon.g += 0//80
					UpIcon.b += 0//80
					UpIcon.a -= 50 
					UpIcon.fRotation = -90.0

					DRAW_2D_SPRITE("MPArrow", "MP_ArrowXLarge", UpIcon, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					
					DownIcon.x += 0.0095
					DownIcon.x -= 0.015
					DownIcon.y += 0.019//0.008
					DownIcon.w += 0.01//0.0125//0.022
					DownIcon.h += 0.01//0.0125//0.022//0.040
					DownIcon.r += 0//194
					DownIcon.g += 0//80
					DownIcon.b += 0//80
					DownIcon.a -= 50 
					DownIcon.fRotation = 90.0

					DRAW_2D_SPRITE("MPArrow", "MP_ArrowXLarge", DownIcon, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					
				ENDIF
			ENDIF
			
			IF bDrawLineUnderName
				FLOAT BarYPos
				FLOAT BarXPos
				SPRITE_PLACEMENT LineSprite
				
				BarYPos = GET_BAR_TO_TITLE_OFFSET()+GET_Y_SHIFT(WhichSpace)
				BarXPos = GET_DAMAGE_BAR_TO_TITLE_OFFSET_X()
				
				IF IS_LANGUAGE_NON_ROMANIC()
					LineSprite.x = BarXPos-0.061
				ELSE
					LineSprite.x = BarXPos-0.0365
				ENDIF
				
				LineSprite.y = BarYPos+0.057
				LineSprite.w = Overlays.w-0.004
				LineSprite.h = 0.01
				LineSprite.r = 255
				LineSprite.g = 255
				LineSprite.b = 255
				LineSprite.a = 255
				
				SET_SPRITE_HUD_COLOUR(LineSprite, LineUnderNameColour)
				DRAW_2D_SPRITE("TimerBars", "DamagebarFill_128", LineSprite, FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			ENDIF
			
			IF bDisplayWarning
			
				REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
					
					FLOAT StringIconOffset = 0.0
					FLOAT x
					IF IsPlayer = TRUE
						BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(Title)
						x = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
						
					ELSE
						BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT((Title))
							IF TitleNumber != -1
								// Double spacing so display warning symbol doesn't intersect
								ADD_TEXT_COMPONENT_INTEGER(TitleNumber)
								ADD_TEXT_COMPONENT_INTEGER(TitleNumber)
							ENDIF
						x = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
					ENDIF

					FLOAT b
					FLOAT m
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
					AND IsPlayer = FALSE
						b = 0.153-0.072
						m = -0.457 
					ELIF GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
					AND IsPlayer = FALSE
						b = 0.153-0.010-0.060
						m = -0.457 
					ELIF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					AND IsPlayer = FALSE
						b = 0.153-0.012-0.060
						m = -0.457 
						
					ELIF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED 
					AND IsPlayer = FALSE
						b = 0.153-0.012-0.060
						m = -0.457 
						
					ELSE
					
						b = 0.153-0.037-0.036
						m = -0.457+0.194
					ENDIF

					b += 0.03
					IF MPGlobalsScoreHud.bTitleFarLeftJustified
						b += -0.030
					ENDIF
					
					IF IS_PC_VERSION()
					AND NOT GET_IS_WIDESCREEN()
						b += -0.015
					ENDIF
					
					
					IF MPGlobalsScoreHud.bTitleMiddleJustified
					AND MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE
						b += -0.015-0.003
					ENDIF 


					
//					#IF IS_DEBUG_BUILD
//						StringIconOffset += WidgetStringLengthConverter_SCORE
//						x += WidgetStringLengthConverter_x_SCORE
//						b += WidgetStringLengthConverter_b_SCORE
//						m += WidgetStringLengthConverter_m_SCORE
//						
//						
//						NET_NL()
//						IF IsPlayer
//							NET_PRINT("PLAYER NAME - ")
//						ENDIF
//						NET_PRINT(" x = ")NET_PRINT_FLOAT(x)
//						NET_PRINT(" b = ")NET_PRINT_FLOAT(b)
//						NET_PRINT(" m = ")NET_PRINT_FLOAT(m)
//
//					#ENDIF
					StringIconOffset = (m*x)+B
					
					WarningIcon.x += StringIconOffset
					WarningIcon.x -= 0.015
					WarningIcon.y += 0.008
					WarningIcon.w += 0.022
					WarningIcon.h += 0.040
					WarningIcon.r += 194
					WarningIcon.g += 80
					WarningIcon.b += 80
					WarningIcon.a -= 50 
					
					
					DRAW_2D_SPRITE("CommonMenu", "MP_AlertTriangle",WarningIcon, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				ENDIF
			ENDIF
		
			IF bDisplayTimerCheckbox
			

				IF aPowerup != ACTIVITY_POWERUP_NONE
				
					
					SET_WORD_WRAPPING_RIGHTEDGE_AND_XP(NumberStyle)
					
					
					SPRITE_PLACEMENT XPIconSprite
					SPRITE_PLACEMENT secondaryXPIconSprite
				
					XPIconSprite.x = NumberPlace.x+0.145+0.001
					IF IS_LANGUAGE_NON_ROMANIC()
						XPIconSprite.y = NumberPlace.y+0.016-0.006
					ELSE
						XPIconSprite.y = NumberPlace.y+0.016
					ENDIF
					XPIconSprite.w = 0.016+0.003
					XPIconSprite.h = 0.032+0.004
					XPIconSprite.a = iAlpha // Set powerup alpha B* 3360285
					SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_WHITE)
					
					
					// For Overlaping Icon - Powerup ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
					secondaryXPIconSprite.x = NumberPlace.x+0.145+0.001
					IF IS_LANGUAGE_NON_ROMANIC()
						secondaryXPIconSprite.y = NumberPlace.y+0.016-0.006
					ELSE
						secondaryXPIconSprite.y = NumberPlace.y+0.016
					ENDIF
					secondaryXPIconSprite.w = 0.016+0.003
					secondaryXPIconSprite.h = 0.032+0.004
					secondaryXPIconSprite.a = 255
					SET_SPRITE_HUD_COLOUR(secondaryXPIconSprite, HUD_COLOUR_WHITE)
					
					
					// Display blank icon B* 3360276
					IF bDisplayBlankScore
						XPIconSprite.a = 0
					ENDIF
					
//					#IF IS_DEBUG_BUILD
//						UPDATE_SPRITE_WIDGET_VALUE(XPIconSprite, XPWidgetScore)
//					#ENDIF
					
					PEDHEADSHOT_ID pHeadshotID
					STRING secondaryTextureDictName = ""
					STRING secondaryTextureSpriteName
					HUD_COLOURS secondaryTextureHudColour = HUD_COLOUR_WHITE
					
					STRING textureDictName = ""
					STRING textureSpriteName
					HUD_COLOURS textureHudColour = HUD_COLOUR_WHITE
					SWITCH aPowerup
						CASE ACTIVITY_POWERUP_XP
							XPIconSprite.h += -0.009
							XPIconSprite.w += -0.002
							
							IF IS_LANGUAGE_NON_ROMANIC()
								XPIconSprite.y += 0.0055
							ELSE
								XPIconSprite.y += 0.0025
							ENDIF
							
							textureDictName = "MPRPSymbol"
							textureSpriteName = "RP"
						BREAK
						
						CASE ACTIVITY_POWERUP_ROCKETS
							textureDictName = "TimerBars"
							textureSpriteName = "Rockets"
						BREAK
						
						CASE ACTIVITY_POWERUP_HOMING_ROCKETS
							textureDictName = "MpSpecialRace"
							textureSpriteName = "HOMING_ROCKET"
						BREAK
						
						CASE ACTIVITY_POWERUP_SPIKES
							textureDictName = "TimerBars"
							textureSpriteName = "Spikes"
						BREAK
						
						CASE ACTIVITY_POWERUP_BOOSTS
							textureDictName = "TimerBars"
							textureSpriteName = "Boost"
						BREAK
						
						CASE ACTIVITY_POWERUP_TICK
							textureDictName = "CrossTheLine"
							textureSpriteName = "Timer_LargeTick_32"
							textureHudColour = HUD_COLOUR_GREEN
						BREAK
						
						CASE ACTIVITY_POWERUP_CROSS
							textureDictName = "CrossTheLine"
							textureSpriteName = "Timer_LargeCross_32"
							textureHudColour = HUD_COLOUR_RED
						BREAK
						
						CASE ACTIVITY_POWERUP_BEAST
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Beast"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						
						CASE ACTIVITY_POWERUP_BULLET
							textureDictName = "MPSpecialRace"
							textureSpriteName = "MACHINE_GUN"
						BREAK
						CASE ACTIVITY_POWERUP_RANDOM
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Random"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_SLOW_TIME
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Slow_Time"
							//textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_SWAP
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Swap"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_TESTOSTERONE
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Testosterone"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_THERMAL
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Thermal"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_WEED
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Weed"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_HIDDEN
							textureDictName = "TimerBar_Icons"
							textureSpriteName = "Pickup_Hidden"
							textureHudColour = HUD_COLOUR_FRIENDLY
						BREAK
						CASE ACTIVITY_POWERUP_PED_HEADSHOT
						CASE ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
							IF pPlayerID != INVALID_PLAYER_INDEX()
								pHeadshotID = Get_HeadshotID_For_Player(pPlayerID)
								IF pHeadshotID != NULL
									textureDictName = GET_PEDHEADSHOT_TXD_STRING(pHeadshotID)
									textureSpriteName = GET_PEDHEADSHOT_TXD_STRING(pHeadshotID)
								ENDIF
							ENDIF
							
							// Headshot Positioning
							IF IS_LANGUAGE_NON_ROMANIC()
								XPIconSprite.y = NumberPlace.y+0.016-0.0005
							ELSE
								XPIconSprite.y = NumberPlace.y+0.0185
							ENDIF
							
							XPIconSprite.w = 0.016+0.004
							XPIconSprite.h = 0.032+0.002
							
							// Dead Headshot Cross Overlay
							IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
								
								IF IS_LANGUAGE_NON_ROMANIC()
									secondaryXPIconSprite.y = NumberPlace.y+0.016
								ELSE
									secondaryXPIconSprite.y = NumberPlace.y+0.019
								ENDIF
							
								secondaryXPIconSprite.w = 0.016+0.004
								secondaryXPIconSprite.h = 0.032+0.002
								
								secondaryTextureDictName = "timerbar_sr"
								secondaryTextureSpriteName = "timer_cross"
								secondaryTextureHudColour = TitleColour
								XPIconSprite.a = 127 // Dead headshot half alpha
							ENDIF
						BREAK
						
					ENDSWITCH
					
					IF NOT IS_STRING_NULL_OR_EMPTY(textureDictName)
						REQUEST_STREAMED_TEXTURE_DICT(textureDictName)
						IF HAS_STREAMED_TEXTURE_DICT_LOADED(textureDictName)
							SET_SPRITE_HUD_COLOUR(XPIconSprite, textureHudColour)
							DRAW_2D_SPRITE(textureDictName, textureSpriteName, XPIconSprite)
						ELSE
							PRINTLN("[BCTIMERS] ", aPowerup, " - ", textureDictName, " dict is loading... ")
						ENDIF
					ENDIF
					
					IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
						IF NOT IS_STRING_NULL_OR_EMPTY(secondaryTextureDictName)
							REQUEST_STREAMED_TEXTURE_DICT(secondaryTextureDictName)
							IF HAS_STREAMED_TEXTURE_DICT_LOADED(secondaryTextureDictName)
								SET_SPRITE_HUD_COLOUR(secondaryXPIconSprite, secondaryTextureHudColour)
								DRAW_2D_SPRITE(secondaryTextureDictName, secondaryTextureSpriteName, secondaryXPIconSprite)
							ELSE
								PRINTLN("[BCTIMERS] (SECONDARY) ", aPowerup, " - ", secondaryTextureDictName, " dict is loading... ")
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				
			
				SET_TEXT_STYLE(NumberStyle)
				anOrder = SET_GFX_TIMERS_DRAW_ORDER()
				SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
	
				IF aPowerup = ACTIVITY_POWERUP_NONE
				OR aPowerup = ACTIVITY_POWERUP_XP
				OR aPowerup = ACTIVITY_POWERUP_BULLET
					
					TEXT_STYLE ePlacementTextStyle = TitleStyle
					SET_WORD_WRAPPING_RIGHTEDGE(ePlacementTextStyle)
					
					IF DrawInfinity

						DRAW_TEXT_WITH_PLAYER_NAME(NumberPlace, NumberStyle, "∞", "", HUD_COLOUR_WHITE, FONT_RIGHT)
					ELSE	
						IF aPowerup = ACTIVITY_POWERUP_XP
							
							IF (bUsePlacementText)
								IF NOT IS_STRING_NULL_OR_EMPTY(sPlacementText)
									DISPLAY_TEXT_FROM_SINGLE_SCORE_GUTS(ePlacementTextStyle, TitlePlace, sPlacementText, TitleColour, aColour, -1, bLiteralPlacementText, UseTitleColourForTitle)
								ENDIF
							ELSE
								IF isFloat = FALSE
									DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,"HUD_KSMULTI", Number,FONT_RIGHT)
								ELSE
									DRAW_TEXT_WITH_FLOAT(NumberPlace,NumberStyle,"HUD_KSMULTI", FloatValue,g_b_iNumberOfDecimalPlacesForScore,FONT_RIGHT)
								ENDIF
							ENDIF
							
						ELSE
							IF IS_STRING_EMPTY_HUD(NumberString)
								
								IF (bUsePlacementText)
									IF NOT IS_STRING_NULL_OR_EMPTY(sPlacementText)
										DISPLAY_TEXT_FROM_SINGLE_SCORE_GUTS(ePlacementTextStyle, TitlePlace, sPlacementText, TitleColour, aColour, -1, bLiteralPlacementText, UseTitleColourForTitle)
									ENDIF
								ELSE
									IF MaXNumber = 0
										IF isFloat = FALSE
											DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,"NUMBER", Number,FONT_RIGHT)
										ELSE
											DRAW_TEXT_WITH_FLOAT(NumberPlace, NumberStyle,"NUMBER", FloatValue,g_b_iNumberOfDecimalPlacesForScore,FONT_RIGHT)
										ENDIF
									ELSE
										DRAW_TEXT_WITH_2_NUMBERS(NumberPlace,NumberStyle,"TIMER_DASHES", Number, MaXNumber ,FONT_RIGHT)
									ENDIF
								ENDIF
								
							ELSE
								
								IF (bUsePlacementText)
									IF NOT IS_STRING_NULL_OR_EMPTY(sPlacementText)
										DISPLAY_TEXT_FROM_SINGLE_SCORE_GUTS(ePlacementTextStyle, TitlePlace, sPlacementText, TitleColour, aColour, -1, bLiteralPlacementText, UseTitleColourForTitle)
									ENDIF
								ELSE
								
									IF ARE_STRINGS_EQUAL("HUD_CASH", NumberString)
									OR  ARE_STRINGS_EQUAL("HUD_CASH_S", NumberString)
										NumberString = "HUD_CASH_S"	
										NumberStyle.aFont = FONT_STYLE_FIXED_WIDTH_NUMBERS
										SET_TEXT_STYLE(NumberStyle)
										DRAW_CASH_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number,FONT_RIGHT)

									ELIF ARE_STRINGS_EQUAL("HUD_CASH_NEG", NumberString)
									OR ARE_STRINGS_EQUAL("HUD_CASH_NEG_S", NumberString)								
										NumberStyle.aFont = FONT_STYLE_FIXED_WIDTH_NUMBERS
										SET_TEXT_STYLE(NumberStyle)
										NumberString = "HUD_CASH_NEG_S"
										DRAW_CASH_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number,FONT_RIGHT)
									ELSE
										IF isFloat = FALSE
											DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number,FONT_RIGHT)
										ELSE
											DRAW_TEXT_WITH_FLOAT(NumberPlace,NumberStyle,NumberString, FloatValue,g_b_iNumberOfDecimalPlacesForScore,FONT_RIGHT)
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
	ENDIF
ENDPROC


PROC ACTUALLY_DRAW_GENERAL_SINGLE_SCORE(INT Index, INT Number, STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber,BOOL isPlayer, STRING NumberString, BOOL isFloat, FLOAT FloatValue, HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS TitleColour, BOOL bDisplayWarning, INT MaXNumber, BOOL DrawInfinity, ACTIVITY_POWERUP aPowerup, HUD_COUNTER_STYLE eCounterStyle, BOOL bIsLiteralTitle, HUD_COLOURS FleckColour, INT iAlpha, BOOL bDisplayBlankScore, PLAYER_INDEX pPlayerID, BOOL bFlashTitle, BOOL bDrawLineUnderName, HUD_COLOURS LineUnderNameColour)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SCORE, Index)
		TEXT_STYLE TitleStyle, NumberStyle
		TEXT_PLACEMENT TitlePlace, NumberPlace
				
		DRAW_GENERAL_SINGLE_SCORE_GUTS(index, TitleStyle, NumberStyle, TitlePlace, NumberPlace, Number, UIELEMENTS_BOTTOMRIGHT, Title, aColour, FlashTime, TitleNumber, isPlayer, NumberString, isFloat, FloatValue, ColourFlashType, ColourFlash, TitleColour, bDisplayWarning, MaXNumber, DrawInfinity, aPowerup, eCounterStyle, "", bIsLiteralTitle, FleckColour, iAlpha, bDisplayBlankScore, pPlayerID, bFlashTitle, bDrawLineUnderName, LineUnderNameColour)

		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SINGLE_SCORE index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC






PROC INIT_SCREEN_GENERAL_TIME_TIMER(TEXT_STYLE& NumberStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& TimerPlace, TEXT_PLACEMENT& ExtraTimePlace, UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle, TEXT_STYLE& AmPmStyle, TEXT_PLACEMENT& AMPMPLace )

	FLOAT Gap = GET_VALUE_OF_GAP(NumberStyle) //0.04

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
				
	TimerPlace.x = 0.795
	TimerPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberStyle) //-0.017
	
	AMPMPLace.x = 0.795
	AMPMPLace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(AmPmStyle) //-0.017
	
	ExtraTimePlace.x = TimerPlace.x
	ExtraTimePlace.y = TimerPlace.y //TitlePlace.y+GET_EXTRATIME_TO_TITLE_OFFSET()  //0.038
	
	CHANGE_Y_SHIFT_END(WhichSpace, -Gap)

ENDPROC

PROC RESET_HUD_TIMER_EXTRA_TIME(INT Index)
	RESET_NET_TIMER(MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtraTimeTimer[Index])
ENDPROC



//TEXT_STYLE TitleStyleWidget, TimerTextStyleWidget, ExtraDisplayStyleWidget, AMPMStyleWidget
//TEXT_PLACEMENT TitlePlacementWidget, TimerPlacementWidget, ExtraDisplayPlacementWidget, AMPMPlacementWidget
//SPRITE_PLACEMENT OverlayWidgetTime
//BOOL TimerTimerWidgetBOol
//FLOAT RatioMultiplyerWidget
//// 
//INT ColourFlashTypeWidget
//INT ColourFlashingTime

PROC DRAW_GENERAL_TIME_TIMER_GUTS(INT Index, TEXT_STYLE& TitleStyle, TEXT_STYLE& NumberStyle, TEXT_STYLE& ExtraTimeStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& TimerPlace, TEXT_PLACEMENT& ExtraTimePlace,  INT TimerRunner, UIELEMENTS WhichSpace, INT ExtraTime, PODIUMPOS MedalDisplay, STRING Title, HUD_COLOURS aColour, TIMER_STYLE aTimerStyle, INT FlashTime, INT TitleNumber, BOOL isPlayer, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL bDisplayAllAsDashes, HUD_COLOURS TitleColour, BOOL bIsLiteral, HUD_COLOURS FleckColour, ACTIVITY_POWERUP aPowerup, BOOL bHideUnusedZeros)
	MPGlobalsScoreHud.iHowManyDisplays++
//	INT I
//	IF IS_BOTTOM_RIGHT_AREA_FREE()
			
		IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF	
			
		TEXT_STYLE AMPMStyle
		TEXT_PLACEMENT AMPMPlace
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		
//		#IF IS_DEBUG_BUILD
//		IF TimerTimerWidgetBOol = FALSE
//			
//			WIDGET_GROUP_ID MpToSpWidgetGroup = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
//			SET_CURRENT_WIDGET_GROUP(MpToSpWidgetGroup)
//			START_WIDGET_GROUP("TIMER TIME")	
//				CREATE_A_TEXT_STYLE_WIGET(TitleStyleWidget, "Timer Title")
//				CREATE_A_TEXT_STYLE_WIGET(TimerTextStyleWidget, "Timer Number Style")
//				CREATE_A_TEXT_STYLE_WIGET(ExtraDisplayStyleWidget, "Extra Number Style")
//				CREATE_A_TEXT_STYLE_WIGET(AMPMStyleWidget, "AM PM Style")
//
//				CREATE_A_TEXT_PLACEMENT_WIDGET(TitlePlacementWidget, "Timer Title Placement")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(ExtraDisplayPlacementWidget, "Extra Number Placement")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(TimerPlacementWidget, "Timer Number Placement")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(AMPMPlacementWidget, "AM PM Placement")
//
//				ADD_WIDGET_FLOAT_SLIDER("RatioMultiplyerWidget", RatioMultiplyerWidget, -5, 5, 0.001)
//
//			STOP_WIDGET_GROUP()
//			
//
//			START_WIDGET_GROUP("TIME OVERLAY")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetTime, "OverlayWidgetTime")
//				ADD_WIDGET_INT_SLIDER("FlashTypeWidget", ColourFlashTypeWidget, 0, 5, 1)
//				ADD_WIDGET_INT_SLIDER("ColourFlashingTime", ColourFlashingTime, 0, 50000, 500)
//			STOP_WIDGET_GROUP()
//			CLEAR_CURRENT_WIDGET_GROUP(MpToSpWidgetGroup)
//			
//			TimerTimerWidgetBOol = TRUE
//		ENDIF
//		#ENDIF
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_TIMER, Index)
		
		
		IF bIsLiteral
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		ELIF isPlayer
			SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
		ELIF IS_LANGUAGE_NON_ROMANIC()
			SET_STANDARD_SMALL_HUD_TEXT_NON_ROMANIC(TitleStyle, DROPSTYLE_NONE)
		ELSE
			SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		ENDIF
		SET_STANDARD_UI_AMPM(AMPMStyle, DROPSTYLE_NONE)
		
		
		
		SWITCH aTimerStyle
			CASE TIMER_STYLE_USEMILLISECONDS
			CASE TIMER_STYLE_DONTUSEMILLISECONDS
			CASE TIMER_STYLE_ONLY_SECONDS
			
				IF bDisplayAllAsDashes 
					SET_STANDARD_UI_POSITION_SYMBOL(ExtraTimeStyle, DROPSTYLE_NONE)
					SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE, FONT_STANDARD)
				ELSE
					SET_STANDARD_UI_POSITION_SYMBOL(ExtraTimeStyle, DROPSTYLE_NONE)
					SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE, FONT_STYLE_FIXED_WIDTH_NUMBERS)
				ENDIF
				
				SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
				SET_WORD_WRAPPING_RIGHTEDGE(ExtraTimeStyle)//, Title)
				
			BREAK
//			CASE TIMER_STYLE_DONTUSEMILLISECONDS
//				
//				IF bDisplayAllAsDashes 
//					SET_STANDARD_UI_TIMERNUMBER_TWOSET(ExtraTimeStyle, DROPSTYLE_NONE, FONT_STANDARD)
//					SET_STANDARD_UI_TIMERNUMBER_TWOSET(NumberStyle, DROPSTYLE_NONE, FONT_STANDARD)
//				ELSE
//					SET_STANDARD_UI_TIMERNUMBER_TWOSET(ExtraTimeStyle, DROPSTYLE_NONE, FONT_STANDARD)
//					SET_STANDARD_UI_TIMERNUMBER_TWOSET(NumberStyle, DROPSTYLE_NONE, FONT_STYLE_FIXED_WIDTH_NUMBERS)
//				ENDIF
//				SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
//				SET_WORD_WRAPPING_RIGHTEDGE(ExtraTimeStyle)//, Title)
//			BREAK
			CASE TIMER_STYLE_CLOCKAM
				
				SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE, FONT_STANDARD)
				SET_WORD_WRAPPING_RIGHTEDGE(AMPMStyle )
				SET_WORD_WRAPPING_RIGHTEDGE(ExtraTimeStyle)//, Title)
				SET_WORD_WRAPPING_RIGHTEDGE_AMPM(NumberStyle)
				
			BREAK
			CASE TIMER_STYLE_CLOCKPM
				
				SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE, FONT_STANDARD)
				SET_WORD_WRAPPING_RIGHTEDGE(AMPMStyle )
				SET_WORD_WRAPPING_RIGHTEDGE(ExtraTimeStyle)//, Title)
				SET_WORD_WRAPPING_RIGHTEDGE_AMPM(NumberStyle)
			BREAK
			CASE TIMER_STYLE_STUNTPLANE		
				
				SET_STANDARD_UI_TIMERNUMBER_THREESET(ExtraTimeStyle, DROPSTYLE_NONE, FONT_STANDARD)
				IF bDisplayAllAsDashes 
				
					SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE, FONT_STANDARD)	
				ELSE
					SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE, FONT_STYLE_FIXED_WIDTH_NUMBERS)	
				ENDIF
				SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
				SET_WORD_WRAPPING_RIGHTEDGE(ExtraTimeStyle)//, Title)
			BREAK
//			CASE TIMER_STYLE_ONLY_SECONDS
//				
//				SET_STANDARD_UI_TIMERNUMBER_TWOSET(ExtraTimeStyle, DROPSTYLE_NONE, FONT_STANDARD)
//				SET_STANDARD_UI_TIMERNUMBER_TWOSET(NumberStyle, DROPSTYLE_NONE, FONT_STYLE_FIXED_WIDTH_NUMBERS)
//				SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
//				SET_WORD_WRAPPING_RIGHTEDGE(ExtraTimeStyle)//, Title)
//			BREAK
			
			
		ENDSWITCH
		
		
		SET_WORD_WRAPPING_TITLE(TitleStyle)

		
	
		IF MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE
		AND MPGlobalsScoreHud.bTitleMiddleJustified = FALSE
			IF aTimerStyle = TIMER_STYLE_USEMILLISECONDS
				TitleStyle.wrapEndX += -0.016
				IF IS_LANGUAGE_NON_ROMANIC()
					TitleStyle.wrapEndX += -0.008
				ENDIF
			ENDIF
		ELIF MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE
		AND MPGlobalsScoreHud.bTitleMiddleJustified = TRUE
			IF aTimerStyle = TIMER_STYLE_USEMILLISECONDS
				TitleStyle.wrapEndX += 0.0
				IF IS_LANGUAGE_NON_ROMANIC()
					TitleStyle.wrapEndX += -0.009
				ENDIF
			ENDIF
		ENDIF		
	
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace) 
		ENDIF
		INIT_SCREEN_GENERAL_TIME_TIMER( NumberStyle, TitlePlace, TimerPlace, ExtraTimePlace, WhichSpace, TitleStyle, AMPMStyle,AMPMPlace )



		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )

		

		SET_WORD_HUD_COLOUR(NumberStyle, aColour)
		
//		#IF IS_DEBUG_BUILD
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TitlePlace, TitlePlacementWidget)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TimerPlace, TimerPlacementWidget)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TimerPlace, ExtraDisplayPlacementWidget)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(AMPMPlace, AMPMPlacementWidget)
//
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle,TitleStyleWidget)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle,TimerTextStyleWidget)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(ExtraTimeStyle, ExtraDisplayStyleWidget)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(AMPMStyle, AMPMStyleWidget)
//		#ENDIF
		
		
		BOOL bDisplayTimerCheckbox

		IF FlashTime = 0 
			RESET_GENERIC_TIMER_FLASHING(Index)
		ENDIF
		
		IF ColourFlash = 0 
			RESET_GENERIC_TIMER_FLASHING_COLOUR(Index)
		ENDIF

		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")

			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)

			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericTimer_Hud[Index],MPGlobalsScoreHud.iFlashing_GenericTimer_MiniHud[Index])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF

			
			//Setting up rectangular overlay below the timer text/bar
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
					
			IF MPGlobalsScoreHud.bCoronaUnderHud = TRUE
				Overlays.x += RIGHT_EDGE_CORONA_OFFSET
			ENDIF
			
			SWITCH aTimerStyle
				CASE TIMER_STYLE_USEMILLISECONDS
				CASE TIMER_STYLE_DONTUSEMILLISECONDS
				CASE TIMER_STYLE_ONLY_SECONDS
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_TIME
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_TIME
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				BREAK
//				CASE TIMER_STYLE_DONTUSEMILLISECONDS
//					Overlays.x += TIMER_OVERLAY_X
//					Overlays.y += TIMER_OVERLAY_Y_BIGTIME
//					Overlays.w += TIMER_OVERLAY_W
//					Overlays.h += TIMER_OVERLAY_H_BIGTIME
//					Overlays.r += 255
//					Overlays.g += 255
//					Overlays.b += 255
//					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
//				BREAK
				CASE TIMER_STYLE_CLOCKAM
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_TIME
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_TIME
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				BREAK
				CASE TIMER_STYLE_CLOCKPM
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_TIME
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_TIME
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				BREAK
				CASE TIMER_STYLE_STUNTPLANE
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_TIME
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_TIME
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				BREAK
//				CASE TIMER_STYLE_ONLY_SECONDS
//					Overlays.x += TIMER_OVERLAY_X
//					Overlays.y += TIMER_OVERLAY_Y_BIGTIME
//					Overlays.w += TIMER_OVERLAY_W
//					Overlays.h += TIMER_OVERLAY_H_BIGTIME
//					Overlays.r += 255
//					Overlays.g += 255
//					Overlays.b += 255
//					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
//				BREAK

				
			ENDSWITCH
			
					
			SPRITE_PLACEMENT XPIconSprite
			XPIconSprite.x = TimerPlace.x+0.145+0.001
			IF IS_LANGUAGE_NON_ROMANIC()
				XPIconSprite.y = TimerPlace.y+0.019-0.006
			ELSE
				XPIconSprite.y = TimerPlace.y+0.019
			ENDIF
			XPIconSprite.w = 0.016+0.003
			XPIconSprite.h = 0.032+0.004
			XPIconSprite.a = 255
			SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_WHITE)
			
			// Position icon between title and timer
			XPIconSprite.x = TitleStyle.WrapEndX - ((NumberStyle.WrapEndX - TitleStyle.WrapEndX)/8)
			IF IS_LANGUAGE_NON_ROMANIC()
				XPIconSprite.x -= 0.003
			ENDIF
			
			SWITCH aPowerup
				CASE ACTIVITY_POWERUP_XP
					XPIconSprite.h += -0.009
					XPIconSprite.w += -0.002
					
					IF IS_LANGUAGE_NON_ROMANIC()
						XPIconSprite.y += 0.0055
					ELSE
						XPIconSprite.y += 0.0025
					ENDIF
				
					REQUEST_STREAMED_TEXTURE_DICT("MPRPSymbol")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPRPSymbol")
						DRAW_2D_SPRITE("MPRPSymbol", "RP", XPIconSprite)
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_ROCKETS
					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
						DRAW_2D_SPRITE("TimerBars", "Rockets", XPIconSprite)
					ENDIF
				
				BREAK
				CASE ACTIVITY_POWERUP_SPIKES
					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
						DRAW_2D_SPRITE("TimerBars", "Spikes", XPIconSprite)
					ENDIF
				
				BREAK
				CASE ACTIVITY_POWERUP_BOOSTS
					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars") 
						DRAW_2D_SPRITE("TimerBars", "Boost", XPIconSprite)
					ENDIF
				
				BREAK
				CASE ACTIVITY_POWERUP_TICK
					REQUEST_STREAMED_TEXTURE_DICT("CrossTheLine")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("CrossTheLine")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_GREEN)
						DRAW_2D_SPRITE("CrossTheLine", "Timer_LargeTick_32", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_TICK - CrossTheLine dict is loading... ")
					ENDIF
				
				BREAK
				CASE ACTIVITY_POWERUP_CROSS
					REQUEST_STREAMED_TEXTURE_DICT("CrossTheLine")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("CrossTheLine")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_RED)
						DRAW_2D_SPRITE("CrossTheLine", "Timer_LargeCross_32", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_CROSS - CrossTheLine dict is loading... ")
					ENDIF
				
				BREAK
				CASE ACTIVITY_POWERUP_BEAST
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Beast", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_BEAST - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_BULLET
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_B_Time", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_BULLET - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_RANDOM
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Random", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_RANDOM - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_SLOW_TIME
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Slow_Time", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_SLOW_TIME - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_SWAP
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Swap", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_SWAP - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_TESTOSTERONE
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Testosterone", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_TESTOSTERONE - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_THERMAL
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Thermal", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_THERMAL - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_WEED
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Weed", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_WEED - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
				CASE ACTIVITY_POWERUP_HIDDEN
					REQUEST_STREAMED_TEXTURE_DICT("TimerBar_Icons")
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBar_Icons")
						SET_SPRITE_HUD_COLOUR(XPIconSprite, HUD_COLOUR_FRIENDLY)
						DRAW_2D_SPRITE("TimerBar_Icons", "Pickup_Hidden", XPIconSprite)
					ELSE
						NET_NL()NET_PRINT("[BCTIMERS] ACTIVITY_POWERUP_HIDDEN - TimerBar_Icons dict is loading... ")
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			

			IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE




				IF ColourFlash > 0
					SPRITE_PLACEMENT ColourOverlays = Overlays
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericTimer[Index], 2000) = FALSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericTimer[Index], 1250)
							MPGlobalsScoreHud.iGoalFadeFlashing_GenericTimer[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericTimer[Index]-17
						ENDIF
						ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericTimer[Index]
						IF ColourFlashType = HUDFLASHING_FLASHRED
							SET_SPRITE_HUD_COLOUR(ColourOverlays, HUD_COLOUR_RED)
						ELIF ColourFlashType = HUDFLASHING_FLASHGREEN
							SET_SPRITE_HUD_COLOUR(ColourOverlays, HUD_COLOUR_GREEN)
						ELSE
							SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
						ENDIF
						
						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ELSE	
					MPGlobalsScoreHud.iGoalFadeFlashing_GenericTimer[Index] = 255
					REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericTimer[Index])
				ENDIF
				
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)


			ENDIF
			
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)

			IF isPlayer 
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += PLAYERNAME_TITLE_OFFSET
				ENDIF
			ELIF IS_LANGUAGE_NON_ROMANIC()
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
				ENDIF
			ENDIF
			
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			SET_WORD_HUD_COLOUR(TitleStyle, TitleColour)
			
			IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
				SET_TEXT_STYLE(TitleStyle)
				IF IS_STRING_EMPTY_HUD(Title) = FALSE
				
					IF bIsLiteral = TRUE
						DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TitleColour, FONT_RIGHT)					
					
					ELIF IsPlayer = TRUE
					
						TitleStyle.aFont = FONT_CONDENSED
						DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TitleColour, FONT_RIGHT)
					ELSE
						IF TitleNumber = -1
							DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
						ELSE
							DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)										
						ENDIF  
					ENDIF	
				ENDIF
			ENDIF
		

			IF bDisplayTimerCheckbox
			
				
			
				IF ExtraTime = 0 
					RESET_HUD_TIMER_EXTRA_TIME(Index)
				ENDIF
			
				
			
				IF ExtraTime <> 0
				AND HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtraTimeTimer[Index], 4000) = FALSE
				
				
					IF (aTimerStyle != TIMER_STYLE_STUNTPLANE)					
						IF ExtraTime > 0
							SET_TEXT_GREEN(ExtraTimeStyle)
							DRAW_TEXT_TIMER(ExtraTimePlace, ExtraTimeStyle, ExtraTime, TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS, "TIMER_POS", FALSE, TRUE)
							SET_TEXT_WHITE(ExtraTimeStyle)
						ELSE
							SET_TEXT_RED(ExtraTimeStyle)
							DRAW_TEXT_TIMER(ExtraTimePlace, ExtraTimeStyle, ExtraTime, TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS, "STRING", FALSE, TRUE)
							SET_TEXT_WHITE(ExtraTimeStyle)
						ENDIF
					ELSE 
						
						
						IF ExtraTime > 0					
							SET_TEXT_RED(ExtraTimeStyle)							
							DRAW_TEXT_TIMER(ExtraTimePlace, ExtraTimeStyle, ExtraTime, TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS, "TIMER_POS", FALSE, TRUE) 							
							SET_TEXT_WHITE(ExtraTimeStyle)
						ELSE					
							SET_TEXT_GREEN(ExtraTimeStyle)	
							//Do not add ABSI to this. Pass in the correct number instead. 
							DRAW_TEXT_TIMER(ExtraTimePlace, ExtraTimeStyle, (ExtraTime), TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS, "STRING", FALSE, TRUE) 							
							SET_TEXT_WHITE(ExtraTimeStyle)
						ENDIF					
						
					ENDIF
				ELSE
					
					IF MedalDisplay <> PODIUMPOS_NONE
						IF MedalDisplay = PODIUMPOS_BRONZE
							SET_TEXT_BRONZE(NumberStyle)
						ELIF MedalDisplay = PODIUMPOS_SILVER
							SET_TEXT_SILVER(NumberStyle)
						ELIF MedalDisplay = PODIUMPOS_GOLD
							SET_TEXT_GOLD(NumberStyle)
						ENDIF
					ENDIF
					
					
					IF MPGlobalsScoreHud.bCoronaUnderHud = TRUE
						NumberStyle.drop = DROPSTYLE_NONE
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
					ENDIF
					
					
					
					SET_TEXT_STYLE(NumberStyle)
					

					
					SWITCH aTimerStyle
						CASE TIMER_STYLE_USEMILLISECONDS
							
							IF bDisplayAllAsDashes 
								DRAW_TEXT_WITH_PLAYER_NAME(TimerPlace,NumberStyle, "--:--:--","", HUD_COLOUR_WHITE, FONT_RIGHT )
							ELSE
								
								TimerPlace.x += 0.117
								IF bHideUnusedZeros
									DRAW_TEXT_TIMER(TimerPlace,NumberStyle, TimerRunner, TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER|TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS, "", FALSE, TRUE)
								ELSE
									DRAW_TEXT_TIMER(TimerPlace,NumberStyle, TimerRunner, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER|TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT, "", FALSE, TRUE)
								ENDIF
							ENDIF
						BREAK
						CASE TIMER_STYLE_DONTUSEMILLISECONDS
							
							IF bDisplayAllAsDashes 
								DRAW_TEXT_WITH_PLAYER_NAME(TimerPlace,NumberStyle, "--:--","", HUD_COLOUR_WHITE, FONT_RIGHT )
							ELSE
								TimerPlace.x += 0.117
								DRAW_TEXT_TIMER(TimerPlace,NumberStyle, TimerRunner, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS, "", FALSE, TRUE)
							ENDIF
						BREAK
						CASE TIMER_STYLE_CLOCKAM
							TimerPlace.x += 0.120
							DRAW_TEXT_TIMER(TimerPlace,NumberStyle, TimerRunner, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS, "", FALSE, TRUE)
							DRAW_TEXT_WITH_ALIGNMENT(AMPMPlace, AMPMStyle, "TIMER_AM_O", FALSE, TRUE) 							

						BREAK
						CASE TIMER_STYLE_CLOCKPM
							TimerPlace.x += 0.120
							DRAW_TEXT_TIMER(TimerPlace,NumberStyle, TimerRunner, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS, "", FALSE, TRUE)
							DRAW_TEXT_WITH_ALIGNMENT(AMPMPlace, AMPMStyle, "TIMER_PM_O", FALSE, TRUE) 		
							

						BREAK
						CASE TIMER_STYLE_STUNTPLANE
							TimerPlace.x += 0.120
							IF bDisplayAllAsDashes 
								DRAW_TEXT_WITH_PLAYER_NAME(TimerPlace,NumberStyle, "--:--:--","", HUD_COLOUR_WHITE, FONT_RIGHT )
							ELSE
								DRAW_TEXT_TIMER(TimerPlace, NumberStyle, TimerRunner, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER|TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT, "", FALSE, TRUE)										
							ENDIF
						BREAK
						CASE TIMER_STYLE_ONLY_SECONDS
							TimerPlace.x += 0.117
							DRAW_TEXT_TIMER(TimerPlace,NumberStyle, TimerRunner, TIME_FORMAT_SECONDS, "", FALSE, TRUE)

						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
	
//	ENDIF
ENDPROC


PROC ACTUALLY_DRAW_GENERAL_TIME_TIMER(INT index, INT TimerRunning, STRING Title, INT ExtraTimeGiven, PODIUMPOS MedalDisplay, TIMER_STYLE TimerStyle, HUD_COLOURS aColour, INT iFlashingTimer, INT TitleNumber,BOOL isPlayer, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL bDisplayAsDashes, HUD_COLOURS TitleColour, BOOL bIsLiteral, HUD_COLOURS FleckColour, ACTIVITY_POWERUP aPowerup, BOOL bHideUnusedZeros)
	
	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_TIMER, Index)
		
		TEXT_STYLE TitleStyle, TimerTextStyle, ExtraDisplayStyle
		TEXT_PLACEMENT TitlePlacement, TimerPlacement, ExtraDisplayPlacement
		
		DRAW_GENERAL_TIME_TIMER_GUTS(Index, TitleStyle, TimerTextStyle,ExtraDisplayStyle, TitlePlacement, TimerPlacement, ExtraDisplayPlacement, TimerRunning, UIELEMENTS_BOTTOMRIGHT, ExtraTimeGiven, MedalDisplay, Title, aColour, TimerStyle, iFlashingTimer, TitleNumber, isPlayer, ColourFlashType, ColourFlash, bDisplayAsDashes, TitleColour, bIsLiteral, FleckColour, aPowerup, bHideUnusedZeros)

		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_TIME_TIMER index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF

	ENDIF
	
ENDPROC

PROC INIT_SCREEN_GENERAL_SINGLE_BIG_NUMBER(TEXT_STYLE& NumberStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace, UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle )

	FLOAT LargeGap = GET_VALUE_OF_GAP(NumberStyle) //0.115

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
			
	NumberPlace.x = 0.795
	NumberPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberStyle)
		
	CHANGE_Y_SHIFT_END(WhichSpace, -LargeGap)

ENDPROC

//SPRITE_PLACEMENT OverlayWidgetSingleBig
//TEXT_STYLE styleWidgetSingleBig
//TEXT_STYLE styleWidgetSingleBigTitle
//
//BOOL SingleBigWidget

PROC DRAW_GENERAL_SINGLE_BIG_NUMBER_GUTS(INT index, TEXT_STYLE& TitleStyle, TEXT_STYLE& NumberStyle,TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace, INT Number, UIELEMENTS WhichSpace, STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber,BOOL isPlayer, STRING NumberString, HUD_COLOURS TitleColour, BOOL bDrawInfinity, HUDFLASHING ColourFlashType, INT ColourFlash,HUD_COLOURS FleckColour)

	IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF

	MPGlobalsScoreHud.iHowManyDisplays++
//	INT I	
//	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		BOOL bDisplayTimerCheckbox

//		IF SingleBigWidget = FALSE
//
//			START_WIDGET_GROUP("SINGLE BIG NUMBER STYLE")
//				CREATE_A_TEXT_STYLE_WIGET(styleWidgetSingleBig, "styleWidgetSingleBig")
//				CREATE_A_TEXT_STYLE_WIGET(styleWidgetSingleBigTitle, "styleWidgetSingleBigTitle")
//			STOP_WIDGET_GROUP()
//			
//			START_WIDGET_GROUP("SINGLE BIG NUMBER OVERLAY")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetSingleBig, "OverlayWidgetSingleBig")
//			STOP_WIDGET_GROUP()
//			
//			SingleBigWidget = TRUE
//
//		ENDIF
		
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_SINGLE_NUMBER, Index) 
		
		IF isPlayer
			SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
		ELSE
			SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		ENDIF
		SET_STANDARD_UI_SINGLE_NUMBER(NumberStyle, DROPSTYLE_NONE)
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
		
//		#IF IS_DEBUG_BUILD
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle, styleWidgetSingleBig)
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, styleWidgetSingleBigTitle)
//			
//		#ENDIF
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		INIT_SCREEN_GENERAL_SINGLE_BIG_NUMBER(NumberStyle, TitlePlace, NumberPlace, WhichSpace, TitleStyle)
	
	
		SET_WORD_HUD_COLOUR(NumberStyle, aColour)
		
		IF FlashTime = 0
			RESET_GENERIC_NUMBER_FLASHING(index)
		ENDIF
		IF ColourFlash = 0 
			RESET_GENERIC_NUMBER_FLASHING_COLOUR(Index)
		ENDIF
		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
		
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericNumber_Hud[index],MPGlobalsScoreHud.iFlashing_GenericNumber_MiniHud[index])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF
			
			SET_WORD_HUD_COLOUR(TitleStyle, TitleColour)
		
		
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			
			Overlays.x += TIMER_OVERLAY_X
			Overlays.y += TIMER_OVERLAY_Y_SINGLEBIGNUM
			Overlays.w += TIMER_OVERLAY_W
			Overlays.h += TIMER_OVERLAY_H_SINGLEBIGNUM
			Overlays.r += 255
			Overlays.g += 255
			Overlays.b += 255
			Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			
			
//			#IF IS_DEBUG_BUILD
//			UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetSingleBig)
//			#ENDIF
			
//			IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsScoreHud.iColourFlashing_GenericNumber_Hud[Index], MPGlobalsScoreHud.iColourFlashing_GenericNumber_MiniHud[Index])
//
//				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays,TRUE,HIGHLIGHT_OPTION_NORMAL,  anOrder )
//			ELSE
//				
//				SWITCH ColourFlashType
//					CASE HUDFLASHING_FLASHWHITE
//						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays,TRUE,HIGHLIGHT_OPTION_NORMAL,  anOrder )
//					BREAK
//					CASE HUDFLASHING_FLASHRED
//						DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays,TRUE,HIGHLIGHT_OPTION_NORMAL,  anOrder )
//					BREAK
//					
//					DEFAULT
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays,TRUE,HIGHLIGHT_OPTION_NORMAL,  anOrder )
//					
//					BREAK
//				
//				ENDSWITCH
//			ENDIF

//			IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsHud.iColourFlashing_GenericNumber_Hud[Index], MPGlobalsHud.iColourFlashing_GenericNumber_MiniHud[Index])
				
				IF ColourFlash > 0
					SPRITE_PLACEMENT ColourOverlays = Overlays
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericNumber[Index], 2000) = FALSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericNumber[Index], 1250)
							MPGlobalsScoreHud.iGoalFadeFlashing_GenericNumber[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericNumber[Index]-17
						ENDIF
						ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericNumber[Index]
						SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ELSE	
					MPGlobalsScoreHud.iGoalFadeFlashing_GenericNumber[Index] = 255
					REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericNumber[Index])
				ENDIF
				
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)

//			ELSE
//				
//				SWITCH ColourFlashType
//					CASE HUDFLASHING_FLASHWHITE
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						Overlays.a = 255
//						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						
//					BREAK
//					CASE HUDFLASHING_FLASHRED
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						Overlays.a = 255
//						DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					
//					DEFAULT
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					
//					BREAK
//				
//				ENDSWITCH
//			ENDIF

		
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)
		
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			IF isPlayer
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += PLAYERNAME_TITLE_OFFSET
				ENDIF
			ELIF IS_LANGUAGE_NON_ROMANIC()
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
				ENDIF
			ENDIF
		
			SET_TEXT_STYLE(TitleStyle)
			IF IsPlayer = TRUE
				TitleStyle.aFont = FONT_CONDENSED
				
//				#IF IS_DEBUG_BUILD
//					TitlePlace.y += GET_VALUE_TO_TITLE_OFFSET(TitleStyle)
//				#ENDIF
				
				DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TitleColour, FONT_RIGHT)
			ELSE
				IF TitleNumber = -1
					DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
				ELSE
					DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
				ENDIF  
			ENDIF	
			
			IF bDisplayTimerCheckbox
				
				
				SET_TEXT_STYLE(NumberStyle)
				IF bDrawInfinity = FALSE
					DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number,FONT_RIGHT)
				ELSE
					DRAW_TEXT_WITH_PLAYER_NAME(NumberPlace, NumberStyle, "∞", "", HUD_COLOUR_WHITE, FONT_RIGHT)
				ENDIF
			ENDIF
		RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
//	ENDIF
ENDPROC


PROC ACTUALLY_DRAW_GENERAL_SINGLE_BIG_NUMBER(INT Index, INT Number, STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber,BOOL isPlayer, STRING NumberString, HUD_COLOURS TitleColour, BOOL bDrawInfinity, HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS Fleckcolour, BOOL bEnablePulsing, HUD_COLOURS PulseToColour, INT iPulseTime)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SINGLE_NUMBER, Index)
		TEXT_STYLE TitleStyle, NumberStyle
		TEXT_PLACEMENT TitlePlace, NumberPlace

//		DRAW_GENERAL_SINGLE_BIG_NUMBER_GUTS(index, TitleStyle, NumberStyle, TitlePlace, NumberPlace, Number, UIELEMENTS_BOTTOMRIGHT, Title, aColour, FlashTime, TitleNumber, isPlayer, NumberString, TitleColour, bDrawInfinity, ColourFlashType, ColourFlash)
		
		
		DRAW_GENERAL_SINGLE_SCORE_GUTS(index, TitleStyle, NumberStyle, TitlePlace, NumberPlace, Number,UIELEMENTS_BOTTOMRIGHT,Title, aColour, FlashTime, TitleNumber, isPlayer,NumberString, FALSE, 0.0, ColourFlashType, ColourFlash, TitleColour, FALSE, 0,bDrawInfinity, ACTIVITY_POWERUP_NONE, HUD_COUNTER_STYLE_DEFAULT, "", DEFAULT, Fleckcolour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bEnablePulsing, PulseToColour, iPulseTime)
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SINGLE_BIG_NUMBER index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC









PROC INIT_SCREEN_GENERAL_DOUBLE_BIG_NUMBER(TEXT_STYLE& NumberStyle,TEXT_STYLE& NumberTwoStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace, TEXT_PLACEMENT& NumberTwoPlace, UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle )

	FLOAT LargeGap = GET_VALUE_OF_GAP(NumberTwoStyle) //0.115

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)

	
	NumberPlace.x = 0.795
	NumberPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberStyle)
	
	NumberTwoPlace.x = 0.795
	NumberTwoPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberTwoStyle)

	CHANGE_Y_SHIFT_END(WhichSpace, -LargeGap)

ENDPROC
//
//TEXT_STYLE TitleStyleWidgetDouble, NumberStyleWidgetDouble, SecondNumberStyleWidgetDouble
//TEXT_PLACEMENT TitlePlacementWidgetDouble, NumberPlacmentWidgetDouble, SecondNumberPlacementWidgetDouble
//SPRITE_PLACEMENT OverlayWidgetBig
//SPRITE_PLACEMENT WarningWidgetBig
//FLOAT WidgetStringLengthConverter
//FLOAT WidgetStringLengthConverter_x
//FLOAT WidgetStringLengthConverter_m
//FLOAT WidgetStringLengthConverter_b
//BOOL BIG_Number_Widget

PROC DRAW_GENERAL_DOUBLE_BIG_NUMBER_GUTS(INT INDEX, TEXT_STYLE& TitleStyle, TEXT_STYLE& NumberStyle,TEXT_STYLE& NumberTwoStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace,TEXT_PLACEMENT& NumberTwoPlace, INT Number, INT Number2, UIELEMENTS WhichSpace, 
										STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber,BOOL isPlayer, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL bDisplayWarning, HUD_COLOURS FleckColour)

	IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF

	MPGlobalsScoreHud.iHowManyDisplays++
		
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		BOOL bDisplayTimerCheckbox
		SPRITE_PLACEMENT Overlays
		SPRITE_PLACEMENT WarningIcon
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		SET_SPRITE_PLACEMENT(WarningIcon, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
	
//		IF BIG_Number_Widget = FALSE
//
//			START_WIDGET_GROUP("DOUBLE BIG NUMBER")
//
//				CREATE_A_TEXT_PLACEMENT_WIDGET(TitlePlacementWidgetDouble, "TitlePlacementWidgetDouble")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(NumberPlacmentWidgetDouble, "NumberPlacmentWidgetDouble")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(SecondNumberPlacementWidgetDouble, "SecondNumberPlacementWidgetDouble")
//				
//				CREATE_A_TEXT_STYLE_WIGET(TitleStyleWidgetDouble, "TitleStyleWidgetDouble")
//				CREATE_A_TEXT_STYLE_WIGET(NumberStyleWidgetDouble, "NumberStyleWidgetDouble")
//				CREATE_A_TEXT_STYLE_WIGET(SecondNumberStyleWidgetDouble, "SecondNumberStyleWidgetDouble")
//			
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("BIG NUMBER OVERLAY")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetBig, "OverlayWidgetBig")
//			
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("BIG NUMBER WARNING")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(WarningWidgetBig, "WarningWidgetBig")
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter", WidgetStringLengthConverter, -1000, 1000, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_x", WidgetStringLengthConverter_x, -1000, 1000, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_m", WidgetStringLengthConverter_m, -1000, 1000, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("WidgetStringLengthConverter_b", WidgetStringLengthConverter_b, -1000, 1000, 0.001)
//			
//			STOP_WIDGET_GROUP()
//			
//			BIG_Number_Widget = TRUE
//			
//			
//		ENDIF

		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_DOUBLE_NUMBER, Index)
		
//		IF Number2 > 99
//			SET_STANDARD_UI_DASHTRIPLE(NumberStyle, DROPSTYLE_NONE)
//		ELSE
//			SET_STANDARD_UI_SINGLE_NUMBER(NumberStyle, DROPSTYLE_NONE)
//		ENDIF

//		IF Number2 > 9
//			SET_STANDARD_UI_DASHDOUBLE(NumberTwoStyle, DROPSTYLE_NONE)
//		ELSE

		IF Number2 > 99
			SET_STANDARD_UI_DASHTRIPLE(NumberTwoStyle, DROPSTYLE_NONE)
		ELSE
			SET_STANDARD_UI_SINGLE_NUMBER(NumberTwoStyle, DROPSTYLE_NONE)
		ENDIF
		
		IF IS_LANGUAGE_NON_ROMANIC()
		
			IF isPlayer
				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
			ELSE
				SET_STANDARD_SMALL_HUD_TEXT_NON_ROMANIC(TitleStyle, DROPSTYLE_NONE)
			ENDIF
		ELSE
		
			IF isPlayer
				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
			ELSE
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ENDIF
			
		ENDIF
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		
//		IF Number2 > 99
//			SET_WORD_WRAPPING_DASH_TRIPLE(NumberStyle)
//		ELIF Number2 > 9
//			SET_WORD_WRAPPING_DASH_DOUBLE(NumberStyle)
//		ELSE
//			SET_WORD_WRAPPING_DASH_SINGLE(NumberStyle)
//		ENDIF
		SET_WORD_WRAPPING_RIGHTEDGE(NumberTwoStyle)
	
	
		
	
		
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		INIT_SCREEN_GENERAL_DOUBLE_BIG_NUMBER(NumberStyle, NumberTwoStyle,TitlePlace, NumberPlace, NumberTwoPlace, WhichSpace, TitleStyle)
	
	
		SET_WORD_HUD_COLOUR(NumberStyle, aColour)
		
//		#IF IS_DEBUG_BUILD
//		
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TitlePlace,  TitlePlacementWidgetDouble)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberPlace,  NumberPlacmentWidgetDouble)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberTwoPlace,  SecondNumberPlacementWidgetDouble)
//		
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, TitleStyleWidgetDouble)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle,NumberStyleWidgetDouble )
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberTwoStyle, SecondNumberStyleWidgetDouble)
//	
//		
//		#ENDIF
		
		IF FlashTime = 0
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING(Index)
		ENDIF
		
		IF ColourFlash = 0 
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(Index)
		ENDIF
	
		IF bDisplayWarning
			REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
		ENDIF
		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
		
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericDoubleNumber_Hud[INDEX] ,MPGlobalsScoreHud.iFlashing_GenericDoubleNumber_MiniHud[INDEX])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF
		
		
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			
			WarningIcon.x = TitlePlace.x
			WarningIcon.y = TitlePlace.y+0.001

			
			IF Number2 > 99
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_BIGNUMSML
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_BIGNUMSML
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			
			ELSE
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_BIGNUM
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_BIGNUM
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			ENDIF
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			
//			#IF IS_DEBUG_BUILD
//			UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetBig)
//			UPDATE_SPRITE_WIDGET_VALUE(WarningIcon, WarningWidgetSCORE)
//			#ENDIF
			
			
//			IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumber_Hud[Index], MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumber_MiniHud[Index])
//
//				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder )
//			ELSE
//				
//				SWITCH ColourFlashType
//					CASE HUDFLASHING_FLASHWHITE
//						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					CASE HUDFLASHING_FLASHRED
//						DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					
//					DEFAULT
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					
//					BREAK
//				
//				ENDSWITCH
//			ENDIF



//			IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsHud.iColourFlashing_GenericDoubleNumber_Hud[Index], MPGlobalsHud.iColourFlashing_GenericDoubleNumber_MiniHud[Index])
				
				IF ColourFlash > 0
					SPRITE_PLACEMENT ColourOverlays = Overlays
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumber[Index], 2000) = FALSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumber[Index], 1250)
							MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumber[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumber[Index]-17
						ENDIF
						ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumber[Index]
						SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ELSE	
					MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumber[Index] = 255
					REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumber[Index])
				ENDIF
				
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)

//			ELSE
//				
//				SWITCH ColourFlashType
//					CASE HUDFLASHING_FLASHWHITE
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						Overlays.a = 255
//						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						
//					BREAK
//					CASE HUDFLASHING_FLASHRED
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						Overlays.a = 255
//						DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					
//					DEFAULT
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					
//					BREAK
//				
//				ENDSWITCH
//			ENDIF
		
	
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)
	
			 anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			IF isPlayer
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += PLAYERNAME_TITLE_OFFSET
				ENDIF
			ELIF IS_LANGUAGE_NON_ROMANIC()
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
				ENDIF
			ENDIF
			
			SET_TEXT_STYLE(TitleStyle)
			IF IsPlayer = TRUE
				DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title,"",  HUD_COLOUR_WHITE, FONT_RIGHT)
			ELSE
				IF TitleNumber = -1
					DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
				ELSE
					DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
				ENDIF  
			ENDIF
			
			IF bDisplayWarning
			
				REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
					
					FLOAT StringIconOffset = 0.0
					
		
					
					FLOAT x = TO_FLOAT(GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(Title))
					FLOAT b = 0.081
					FLOAT m = -0.0059
					
					
					b += 0.03
					IF MPGlobalsScoreHud.bTitleFarLeftJustified
						b += -0.030
					ENDIF
					
					
					IF MPGlobalsScoreHud.bTitleMiddleJustified
					AND MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE
						b += -0.015-0.003
					ENDIF  
					
//					#IF IS_DEBUG_BUILD
//						StringIconOffset += WidgetStringLengthConverter
//						x += WidgetStringLengthConverter_x
//						b += WidgetStringLengthConverter_b
//						m += WidgetStringLengthConverter_m
//												
//					#ENDIF
					StringIconOffset = (m*x)+B
					
					WarningIcon.x += StringIconOffset
					WarningIcon.x -= 0.015
					WarningIcon.y += 0.008
					WarningIcon.w += 0.022
					WarningIcon.h += 0.040
					WarningIcon.r += 194
					WarningIcon.g += 80
					WarningIcon.b += 80
					WarningIcon.a -= 50 
					
					
					
					DRAW_2D_SPRITE("CommonMenu", "MP_AlertTriangle",WarningIcon, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				ENDIF
			ENDIF
		
			IF bDisplayTimerCheckbox
			
//				SET_TEXT_STYLE(NumberStyle)
				SET_TEXT_STYLE(NumberTwoStyle)
			
//				DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,"NUMBER", Number,FONT_RIGHT)
//				DRAW_TEXT_WITH_NUMBER(NumberTwoPlace,NumberTwoStyle,"TIMER_DASH", Number2,FONT_RIGHT)
				DRAW_TEXT_WITH_2_NUMBERS(NumberTwoPlace,NumberTwoStyle,  "TIMER_DASHES",Number, Number2, FONT_RIGHT)
			ENDIF
		ENDIF
			
		
		
		RESET_GFX_TIMERS_DRAW_ORDER()

	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER(INT index, INT CurrentNumber, INT MaxNumber, STRING Title, HUD_COLOURS aColour, INT iFlashTimer, INT TitleNumber, BOOL isPlayer, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL bDisplayWarning, BOOL bUseNonPlayerFont, HUD_COLOURS TitleColour, HUD_COLOURS FleckColour, INT iAlpha, BOOL bFlashTitle)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, Index)
		TEXT_STYLE TitleStyle, NumberStyle//, SecondNumberStyle
		TEXT_PLACEMENT TitlePlace, NumberPlace//, SecondNumberPlacement 
		
//		DRAW_GENERAL_DOUBLE_BIG_NUMBER_GUTS(Index,TitleStyle, NumberStyle, SecondNumberStyle, TitlePlace, NumberPlace, SecondNumberPlacement, 
//											CurrentNumber, MaxNumber, UIELEMENTS_BOTTOMRIGHT, Title,  aColour,  iFlashTimer, TitleNumber, isPlayer, 
//											ColourFlashType, ColourFlash, bDisplayWarning)
	
	
	
		DRAW_GENERAL_SINGLE_SCORE_GUTS(index, TitleStyle, NumberStyle, TitlePlace, NumberPlace, CurrentNumber,UIELEMENTS_BOTTOMRIGHT, Title, aColour,iFlashTimer, TitleNumber, isPlayer,
										"", FALSE, 0, ColourFlashType, ColourFlash, TitleColour, bDisplayWarning, MaxNumber, FALSE, ACTIVITY_POWERUP_NONE, HUD_COUNTER_STYLE_DEFAULT, "",
										bUseNonPlayerFont,FleckColour, iAlpha, DEFAULT, DEFAULT, bFlashTitle)

	
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF
		
	
	ENDIF
ENDPROC
//
//BOOL PlacementStandingWidget
//FLOAT PlacementStandingWidget_VAlueSingle
//FLOAT PlacementStandingWidget_VAlueDouble
//FLOAT PlacementStandingWidget_VAlueTriple

FUNC FLOAT GET_POSITION_SYMBOL_XPOS(TEXT_STYLE& NumberTwoStyle, TEXT_STYLE& NumberStyle)

//	IF PlacementStandingWidget = FALSE
//	
//		START_WIDGET_GROUP("GET_POSITION_SYMBOL_XPOS")
//			ADD_WIDGET_FLOAT_SLIDER("PlacementStandingWidget_VAlueSingle", PlacementStandingWidget_VAlueSingle, -2, 2, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("PlacementStandingWidget_VAlueDouble", PlacementStandingWidget_VAlueDouble, -2, 2, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("PlacementStandingWidget_VAlueTriple", PlacementStandingWidget_VAlueTriple, -2, 2, 0.001)
//		STOP_WIDGET_GROUP()
//		PlacementStandingWidget = TRUE
//		
//	ENDIF
	
	FLOAT Result 


	SWITCH NumberTwoStyle.aTextType

		CASE TEXTTYPE_TS_UI_DASHSINGLE
			Result = (0.930)-0.049+0.048+0.001
			
//			#IF IS_DEBUG_BUILD
//			Result += PlacementStandingWidget_VAlueSingle
//			#ENDIF
		
			RETURN Result
		BREAK
		CASE TEXTTYPE_TS_UI_DASHDOUBLE
			IF NumberStyle.aTextType = TEXTTYPE_TS_UI_DASHTRIPLE
			
				Result = (0.925)-0.046+0.048-0.001-0.010+0.002
				
//				#IF IS_DEBUG_BUILD
//				Result += PlacementStandingWidget_VAlueTriple
//				#ENDIF
			
			ELSE
				Result = (0.925)-0.046+0.048-0.001
//				#IF IS_DEBUG_BUILD
//				Result += PlacementStandingWidget_VAlueDouble
//				#ENDIF
			ENDIF
			
			RETURN Result
		BREAK
		
		
	ENDSWITCH
	RETURN 0.0
ENDFUNC

PROC INIT_SCREEN_GENERAL_DOUBLE_BIG_NUMBER_PLACE(TEXT_STYLE& NumberStyle,TEXT_STYLE& NumberTwoStyle,TEXT_STYLE& PlaceStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace, TEXT_PLACEMENT& NumberTwoPlace, TEXT_PLACEMENT& ThePlacePlace, UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle )

	FLOAT LargeGap = GET_VALUE_OF_GAP(NumberStyle) //0.115

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
			
	NumberPlace.x = 0.795
	NumberPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberStyle)
	
	NumberTwoPlace.x = 0.795
	NumberTwoPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberTwoStyle)
	
	ThePlacePlace.x = GET_POSITION_SYMBOL_XPOS(NumberTwoStyle, NumberStyle)
	ThePlacePlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(PlaceStyle)

	
	CHANGE_Y_SHIFT_END(WhichSpace, -LargeGap)

ENDPROC

//TEXT_STYLE TitleStyleWidgetDoublePlace, NumberStyleWidgetDoublePlace, SecondNumberStyleWidgetDoublePlace, PlaceStyleWidgetDoublePlace
//TEXT_PLACEMENT TitlePlacementWidgetDoublePlace, NumberPlacmentWidgetDoublePlace, SecondNumberPlacementWidgetDoublePlace, PlacePlacmentWidgetDoublePlace
//SPRITE_PLACEMENT OverlayWidgetBigPlace
//BOOL DoubleNumberPlaceWidget

PROC DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE_GUTS(INT Index, TEXT_STYLE& TitleStyle, TEXT_STYLE& NumberStyle,TEXT_STYLE& NumberTwoStyle, TEXT_STYLE& PlaceStyle, TEXT_PLACEMENT& TitlePlace, TEXT_PLACEMENT& NumberPlace,TEXT_PLACEMENT& NumberTwoPlace,TEXT_PLACEMENT& PlacePlace,  INT Number, INT Number2, UIELEMENTS WhichSpace, STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber,BOOL isPlayer, HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS FleckColour)

	MPGlobalsScoreHud.iHowManyDisplays++
	
		
	IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF	
		
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
//		SET_SPRITE_PLACEMENT(Overlays[1], 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		BOOL bDisplayTimerCheckbox

//		IF DoubleNumberPlaceWidget = FALSE
//
//			START_WIDGET_GROUP("DOUBLE BIG NUMBER PLACE")
//
//				CREATE_A_TEXT_PLACEMENT_WIDGET(TitlePlacementWidgetDoublePlace, "TitlePlacementWidgetDoublePlace")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(NumberPlacmentWidgetDoublePlace, "NumberPlacmentWidgetDoublePlace")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(SecondNumberPlacementWidgetDoublePlace, "SecondNumberPlacementWidgetDoublePlace")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(PlacePlacmentWidgetDoublePlace, "PlacePlacmentWidgetDoublePlace")
//				
//				CREATE_A_TEXT_STYLE_WIGET(TitleStyleWidgetDoublePlace, "TitleStyleWidgetDoublePlace")
//				CREATE_A_TEXT_STYLE_WIGET(NumberStyleWidgetDoublePlace, "NumberStyleWidgetDoublePlace")
//				CREATE_A_TEXT_STYLE_WIGET(SecondNumberStyleWidgetDoublePlace, "SecondNumberStyleWidgetDoublePlace")
//				CREATE_A_TEXT_STYLE_WIGET(PlaceStyleWidgetDoublePlace, "PlaceStyleWidgetDoublePlace")
//			
//			STOP_WIDGET_GROUP()
//
//			START_WIDGET_GROUP("BIG NUMBER PLACE OVERLAY")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetBigPlace, "OverlayWidgetBigPlace")
//			
//			STOP_WIDGET_GROUP()
//			DoubleNumberPlaceWidget = TRUE
//
//		ENDIF
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_DOUBLE_NUMBER_PLACE, Index)
		
		
		

		SET_STANDARD_UI_SINGLE_NUMBER(NumberStyle, DROPSTYLE_NONE)

		SET_STANDARD_UI_DASHSINGLE(NumberTwoStyle, DROPSTYLE_NONE)
		SET_STANDARD_UI_POSITION_SINGLE_SYMBOL(PlaceStyle, DROPSTYLE_NONE)
			
		IF isPlayer
			SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
		ELSE
			SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		ENDIF
		SET_WORD_WRAPPING_TITLE(TitleStyle) 
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		
		IF Number2 > 99
			SET_WORD_WRAPPING_DASH_TRIPLE_PLACE(NumberStyle)
		ELSE
			SET_WORD_WRAPPING_DASH_SINGLE_PLACE(NumberStyle)
		ENDIF
		SET_WORD_WRAPPING_RIGHTEDGE(NumberTwoStyle)
		SET_WORD_WRAPPING_RIGHTEDGE_AND_PLACE(PlaceStyle)
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		INIT_SCREEN_GENERAL_DOUBLE_BIG_NUMBER_PLACE(NumberStyle, NumberTwoStyle, PlaceStyle, TitlePlace, NumberPlace,NumberTwoPlace,PlacePlace, WhichSpace, TitleStyle)

		
		SET_WORD_HUD_COLOUR(NumberStyle, aColour)
		SET_WORD_HUD_COLOUR(NumberTwoStyle, aColour)
		SET_WORD_HUD_COLOUR(PlaceStyle, aColour)


//		#IF IS_DEBUG_BUILD
//		
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TitlePlace,  TitlePlacementWidgetDoublePlace)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberPlace,  NumberPlacmentWidgetDoublePlace)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberTwoPlace,  SecondNumberPlacementWidgetDoublePlace)
//		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(PlacePlace, PlacePlacmentWidgetDoublePlace)
//		
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, TitleStyleWidgetDoublePlace)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle,NumberStyleWidgetDoublePlace )
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberTwoStyle, SecondNumberStyleWidgetDoublePlace)
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(PlaceStyle,PlaceStyleWidgetDoublePlace)
//		
//		#ENDIF
		
		IF FlashTime = 0
			RESET_GENERIC_DOUBLE_NUMBER_PLACE_FLASHING(Index)
		ENDIF
		
		IF ColourFlash = 0 
			RESET_GENERIC_DOUBLE_NUMBER_PLACE_FLASHING_COLOUR(Index)
		ENDIF
	
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
		
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
		
			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericDoubleNumberPlace_Hud[Index],MPGlobalsScoreHud.iFlashing_GenericDoubleNumberPlace_MiniHud[Index])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF
	
		
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			
			Overlays.x += TIMER_OVERLAY_X
			Overlays.y += TIMER_OVERLAY_Y_BIGNUMPLACE
			Overlays.w += TIMER_OVERLAY_W
			Overlays.h += TIMER_OVERLAY_H_BIGNUMPLACE
			Overlays.r += 255
			Overlays.g += 255
			Overlays.b += 255
			Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			
//			#IF IS_DEBUG_BUILD
//			UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetBigPlace)
//			#ENDIF
			
			
//			IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumberPlace_Hud[Index], MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumberPlace_MiniHud[Index])
//
//				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//			ELSE
//				
//				SWITCH ColourFlashType
//					CASE HUDFLASHING_FLASHWHITE
//						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					CASE HUDFLASHING_FLASHRED
//						DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					
//					DEFAULT
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					
//					BREAK
//				
//				ENDSWITCH
//			ENDIF
//		
//			IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsHud.iColourFlashing_GenericDoubleNumberPlace_Hud[Index], MPGlobalsHud.iColourFlashing_GenericDoubleNumberPlace_MiniHud[Index])
				
				IF ColourFlash > 0
					SPRITE_PLACEMENT ColourOverlays = Overlays
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumberPlace[Index], 2000) = FALSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumberPlace[Index], 1250)
							MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumberPlace[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumberPlace[Index]-17
						ENDIF
						ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumberPlace[Index]
						SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ELSE	
					MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumberPlace[Index] = 255
					REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumberPlace[Index])
				ENDIF
				
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)

//			ELSE
//				
//				SWITCH ColourFlashType
//					CASE HUDFLASHING_FLASHWHITE
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						Overlays.a = 255
//						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						
//					BREAK
//					CASE HUDFLASHING_FLASHRED
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						Overlays.a = 255
//						DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					BREAK
//					
//					DEFAULT
//						DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//					
//					BREAK
//				
//				ENDSWITCH
//			ENDIF

			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)

			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			IF isPlayer
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += PLAYERNAME_TITLE_OFFSET
				ENDIF
			ELIF IS_LANGUAGE_NON_ROMANIC()
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
				ENDIF
			ENDIF
		
			SET_TEXT_STYLE(TitleStyle)
			IF IsPlayer = TRUE
				DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title,"",  HUD_COLOUR_WHITE, FONT_RIGHT)
			ELSE
				IF TitleNumber = -1
					DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
				ELSE
					DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
				ENDIF  
			ENDIF
		
			IF bDisplayTimerCheckbox
			

				SET_TEXT_STYLE(NumberStyle)
				SET_TEXT_STYLE(NumberTwoStyle)
				SET_TEXT_STYLE(PlaceStyle)
				
				TEXT_LABEL_63 Position = GET_POSITION_STRING(Number)
				
				
				DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,"NUMBER", Number,FONT_RIGHT)
				DRAW_TEXT_WITH_NUMBER(NumberTwoPlace,NumberTwoStyle,  "TIMER_DASH", Number2, FONT_RIGHT)
				DRAW_TEXT_WITH_ALIGNMENT(PlacePlace, PlaceStyle, Position, FALSE, TRUE)
			ENDIF
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
	ENDIF
ENDPROC


PROC ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE(INT Index, INT CurrentNumber, INT MaxNumber, STRING Title, HUD_COLOURS aColour, INT FlashTime, INT TitleNumber,BOOL isPlayer, HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS TitleColour, HUD_COLOURS FleckColour, BOOL bCustomFont, TEXT_FONTS eCustomFont)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, Index)
		
		TEXT_STYLE TitleStyle, NumberStyle//, SecondNumberStyle, PlacementStyle
		TEXT_PLACEMENT TitlePlace, NumberPlace//, SecondNumberPlace, PlacePlace
		

//		
//		DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE_GUTS(Index, TitleStyle, NumberStyle,SecondNumberStyle, PlacementStyle, 
//												TitlePlace, NumberPlace,SecondNumberPlace,PlacePlace,  CurrentNumber, MaxNumber,
//												UIELEMENTS_BOTTOMRIGHT, Title, aColour, FlashTime, TitleNumber, isPlayer, 
//												ColourFlashType, ColourFlash)
//	
	
		DRAW_GENERAL_SINGLE_SCORE_GUTS(Index, TitleStyle, NumberStyle, TitlePlace, NumberPlace, CurrentNumber, UIELEMENTS_BOTTOMRIGHT, 
										Title, aColour, FlashTime, TitleNumber, isPlayer, "", FALSE, 0.0, ColourFlashType, ColourFlash, 
										TitleColour, FALSE,MaxNumber,FALSE, ACTIVITY_POWERUP_NONE, HUD_COUNTER_STYLE_DEFAULT, "", DEFAULT, 
										FleckColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
										DEFAULT, DEFAULT, bCustomFont, eCustomFont)
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF
	ENDIF
	

ENDPROC




FUNC BOOL IS_BAR_FREEROAM(FLOAT Xpos, FLOAT Ypos)
	IF Xpos = -1
	AND Ypos = -1
		RETURN FALSE
	ENDIF
	RETURN TRUE

ENDFUNC


PROC SET_BAR_SPRITES(SPRITE_PLACEMENT& Sprite[], UIELEMENTS WhichSpace, FLOAT Xpos, FLOAT Ypos)
	
	FLOAT BarYPos
	FLOAT BarXPos
	
	IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
		BarYPos = GET_BAR_TO_TITLE_OFFSET()+GET_Y_SHIFT(WhichSpace)
		BarXPos = GET_DAMAGE_BAR_TO_TITLE_OFFSET_X()  //0.919
	ELSE
		BarYPos = Ypos
		BarXPos = Xpos
	ENDIF
		
	Sprite[0].x = BarXPos
	Sprite[0].y = BarYPos
	Sprite[0].w = 0.062
	Sprite[0].h = 0.016
	Sprite[0].r = 255
	Sprite[0].g = 255
	Sprite[0].b = 0
	Sprite[0].a = 250
	
	//Masked Sprite
	Sprite[1].x = BarXPos
	Sprite[1].y = BarYPos
	Sprite[1].w = 0.069
	Sprite[1].h = 0.011
	Sprite[1].r = 255
	Sprite[1].g = 255
	Sprite[1].b = 0
	Sprite[1].a = 250
	

	Sprite[2].x = BarXPos
	Sprite[2].y = BarYPos
	Sprite[2].w = 0.069
	Sprite[2].h = 0.009
	Sprite[2].r = 0
	Sprite[2].g = 0
	Sprite[2].b = 0
	Sprite[2].a = 120
	
	//Fill rect
	Sprite[3].x = BarXPos
	Sprite[3].y = BarYPos
	Sprite[3].w = 0.069
	Sprite[3].h = 0.008
	Sprite[3].r = 0
	Sprite[3].g = 0
	Sprite[3].b = 0
	Sprite[3].a = 90
	

ENDPROC


PROC SET_CIRCLE_SPRITES(SPRITE_PLACEMENT& Sprite[], UIELEMENTS WhichSpace, FLOAT Xpos, FLOAT Ypos)
	
	FLOAT BarYPos
	FLOAT BarXPos
	
	IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
		BarYPos = GET_BAR_TO_TITLE_OFFSET()+GET_Y_SHIFT(WhichSpace)
		BarXPos = GET_CHECKPOINT_TO_TITLE_OFFSET_X()  //0.919
	ELSE
		BarYPos = Ypos
		BarXPos = Xpos
	ENDIF
		
	//Circle 1	
	Sprite[0].x = BarXPos
	Sprite[0].y = BarYPos
	Sprite[0].w = CHECKPOINT_DOT_W
	Sprite[0].h = CHECKPOINT_DOT_H
	Sprite[0].r = 0
	Sprite[0].g = 0
	Sprite[0].b = 0
	Sprite[0].a = 250
	
	//Circle 2
	Sprite[1].x = BarXPos
	Sprite[1].y = BarYPos
	Sprite[1].w = CHECKPOINT_DOT_W
	Sprite[1].h = CHECKPOINT_DOT_H
	Sprite[1].r = 0
	Sprite[1].g = 0
	Sprite[1].b = 0
	Sprite[1].a = 250
	
	//Circle 3
	Sprite[2].x = BarXPos
	Sprite[2].y = BarYPos
	Sprite[2].w = CHECKPOINT_DOT_W
	Sprite[2].h = CHECKPOINT_DOT_H
	Sprite[2].r = 0
	Sprite[2].g = 0
	Sprite[2].b = 0
	Sprite[2].a = 250
	
	//Circle 4
	Sprite[3].x = BarXPos
	Sprite[3].y = BarYPos
	Sprite[3].w = CHECKPOINT_DOT_W
	Sprite[3].h = CHECKPOINT_DOT_H
	Sprite[3].r = 0
	Sprite[3].g = 0
	Sprite[3].b = 0
	Sprite[3].a = 250
	
	//Circle 5
	Sprite[4].x = BarXPos
	Sprite[4].y = BarYPos
	Sprite[4].w = CHECKPOINT_DOT_W
	Sprite[4].h = CHECKPOINT_DOT_H
	Sprite[4].r = 0
	Sprite[4].g = 0
	Sprite[4].b = 0
	Sprite[4].a = 250
	
	//Circle 6
	Sprite[5].x = BarXPos
	Sprite[5].y = BarYPos
	Sprite[5].w = CHECKPOINT_DOT_W
	Sprite[5].h = CHECKPOINT_DOT_H
	Sprite[5].r = 0
	Sprite[5].g = 0
	Sprite[5].b = 0
	Sprite[5].a = 250
	
	//Circle 7
	Sprite[6].x = BarXPos
	Sprite[6].y = BarYPos
	Sprite[6].w = CHECKPOINT_DOT_W
	Sprite[6].h = CHECKPOINT_DOT_H
	Sprite[6].r = 0
	Sprite[6].g = 0
	Sprite[6].b = 0
	Sprite[6].a = 250
	
	//Circle 8
	Sprite[7].x = BarXPos
	Sprite[7].y = BarYPos
	Sprite[7].w = CHECKPOINT_DOT_W
	Sprite[7].h = CHECKPOINT_DOT_H
	Sprite[7].r = 0
	Sprite[7].g = 0
	Sprite[7].b = 0
	Sprite[7].a = 250
	


ENDPROC


PROC SET_CIRCLE_CHECKPOINT_SPRITES(SPRITE_PLACEMENT& Sprite[], UIELEMENTS WhichSpace, FLOAT Xpos, FLOAT Ypos)
	
	FLOAT BarYPos
	FLOAT BarXPos
	
	IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
		BarYPos = GET_BAR_TO_TITLE_OFFSET()+GET_Y_SHIFT(WhichSpace)
		BarXPos = GET_CHECKPOINT_TO_TITLE_OFFSET_X()  //0.919
	ELSE
		BarYPos = Ypos
		BarXPos = Xpos
	ENDIF
		
	//Circle 1	
	Sprite[0].x = BarXPos
	Sprite[0].y = BarYPos
	Sprite[0].w = CHECKPOINT_DOT_W
	Sprite[0].h = CHECKPOINT_DOT_H
	Sprite[0].r = 0
	Sprite[0].g = 0
	Sprite[0].b = 0
	Sprite[0].a = 250
	
	//Circle 2
	Sprite[1].x = BarXPos
	Sprite[1].y = BarYPos
	Sprite[1].w = CHECKPOINT_DOT_W
	Sprite[1].h = CHECKPOINT_DOT_H
	Sprite[1].r = 0
	Sprite[1].g = 0
	Sprite[1].b = 0
	Sprite[1].a = 250
	
	//Circle 3
	Sprite[2].x = BarXPos
	Sprite[2].y = BarYPos
	Sprite[2].w = CHECKPOINT_DOT_W
	Sprite[2].h = CHECKPOINT_DOT_H
	Sprite[2].r = 0
	Sprite[2].g = 0
	Sprite[2].b = 0
	Sprite[2].a = 250
	
	//Circle 4
	Sprite[3].x = BarXPos
	Sprite[3].y = BarYPos
	Sprite[3].w = CHECKPOINT_DOT_W
	Sprite[3].h = CHECKPOINT_DOT_H
	Sprite[3].r = 0
	Sprite[3].g = 0
	Sprite[3].b = 0
	Sprite[3].a = 250
	
	//Circle 5
	Sprite[4].x = BarXPos
	Sprite[4].y = BarYPos
	Sprite[4].w = CHECKPOINT_DOT_W
	Sprite[4].h = CHECKPOINT_DOT_H
	Sprite[4].r = 0
	Sprite[4].g = 0
	Sprite[4].b = 0
	Sprite[4].a = 250
	
	//Circle 6
	Sprite[5].x = BarXPos
	Sprite[5].y = BarYPos
	Sprite[5].w = CHECKPOINT_DOT_W
	Sprite[5].h = CHECKPOINT_DOT_H
	Sprite[5].r = 0
	Sprite[5].g = 0
	Sprite[5].b = 0
	Sprite[5].a = 250
	
	//Circle 7
	Sprite[6].x = BarXPos
	Sprite[6].y = BarYPos
	Sprite[6].w = CHECKPOINT_DOT_W
	Sprite[6].h = CHECKPOINT_DOT_H
	Sprite[6].r = 0
	Sprite[6].g = 0
	Sprite[6].b = 0
	Sprite[6].a = 250
	
	//Circle 8
	Sprite[7].x = BarXPos
	Sprite[7].y = BarYPos
	Sprite[7].w = CHECKPOINT_DOT_W
	Sprite[7].h = CHECKPOINT_DOT_H
	Sprite[7].r = 0
	Sprite[7].g = 0
	Sprite[7].b = 0
	Sprite[7].a = 250
	
	
	//Circle 9
	Sprite[8].x = BarXPos
	Sprite[8].y = BarYPos
	Sprite[8].w = CHECKPOINT_DOT_W
	Sprite[8].h = CHECKPOINT_DOT_H
	Sprite[8].r = 0
	Sprite[8].g = 0
	Sprite[8].b = 0
	Sprite[8].a = 250
	
	//Circle 10
	Sprite[9].x = BarXPos
	Sprite[9].y = BarYPos
	Sprite[9].w = CHECKPOINT_DOT_W
	Sprite[9].h = CHECKPOINT_DOT_H
	Sprite[9].r = 0
	Sprite[9].g = 0
	Sprite[9].b = 0
	Sprite[9].a = 250
	

ENDPROC

PROC SET_LINE_DIVIDER(RECT& aRECT)
	//First line divider
	aRECT.x = 0.851
	aRECT.y = 0.0750
	aRECT.w = 0.001
	aRECT.h = 0.009
	aRECT.r = 0
	aRECT.g = 0
	aRECT.b = 0
	aRECT.a = 250

ENDPROC

PROC HIDE_ALL_TOP_RIGHT_HUD(BOOL bHideReticle = TRUE, BOOL bHideWeapon = TRUE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	IF bHideWeapon
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	ENDIF
	IF bHideReticle
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	ENDIF
ENDPROC

PROC HIDE_ALL_BOTTOM_RIGHT_HUD()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME) 
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
ENDPROC

PROC INIT_SCREEN_GENERAL_CHECKPOINT(INT MaxNum, TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[],SPRITE_PLACEMENT& Overlays, TEXT_PLACEMENT& NumberPlace,TEXT_STYLE& NumberStyle, RECT& Rects, UIELEMENTS WhichSpace, FLOAT xPos, FLOAT yPos, TEXT_STYLE& TitleStyle, BOOL bBigCheckpoint )


	FLOAT TheGap = GET_VALUE_OF_GAP(NumberStyle)

	IF MaxNum < CHECKPOINT_TIMER_SWITCH
		 TheGap = GET_VALUE_OF_GAP_BAR_SPRITE(bBigCheckpoint, FALSE)
	ENDIF

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)

	NumberPlace.x = 0.795
	NumberPlace.y = TitlePlace.y+GET_VALUE_TO_TITLE_OFFSET(NumberStyle)

	SET_CIRCLE_CHECKPOINT_SPRITES(Sprites,WhichSpace, XPos, YPos)
	SET_LINE_DIVIDER(Rects)
	
	IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
		CHANGE_Y_SHIFT_END(WhichSpace, -TheGap)
	ENDIF
	
	SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
	
ENDPROC

//SPRITE_PLACEMENT OverlayWidgetsCheckpoint
//BOOL InitCheckpointWidgets
//TEXT_STYLE CheckpointNumberStyle_Widget
//TEXT_STYLE CheckpointTitleStyle_Widget
//TEXT_STYLE CheckpointMultiplyerStyle_Widget
//
//TEXT_PLACEMENT CheckpointNumberPlacement_Widget
//TEXT_PLACEMENT CheckpointMultiplyerPlacement_Widget
//
//FLOAT TitleOffset_Widget


PROC DRAW_GENERAL_CHECKPOINT_GUTS(INT index, TEXT_STYLE& TitleStyle, TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[], RECT& aRect, 
								TEXT_STYLE& NumberStyle, TEXT_PLACEMENT& NumberPlace, INT Number, 
								INT MaxNumber, UIELEMENTS WhichSpace, STRING Title, HUD_COLOURS aColour, INT TitleNumber,BOOL isPlayer, INT FlashTime, 
								FLOAT XPos, FLOAT YPos, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL DisplaySingleDot, INT iInBuiltMultiplyer
								#IF USE_TU_CHANGES , BOOL CrossedDot0,BOOL CrossedDot1,BOOL CrossedDot2,BOOL CrossedDot3,BOOL CrossedDot4,BOOL CrossedDot5,BOOL CrossedDot6,BOOL CrossedDot7 #ENDIF,
								HUD_COLOURS FleckColour)



	MPGlobalsScoreHud.iHowManyDisplays++
		
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF
		
		SPRITE_PLACEMENT Overlays
		TEXT_PLACEMENT InBuiltMultiplyer_Placement
		TEXT_STYLE MultiplyerStyle 
		
		
		
		BOOL bDisplayTimerCheckbox

//		IF InitCheckpointWidgets = FALSE
//			START_WIDGET_GROUP("CHECKPOINT SIZE")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetsCheckpoint, "OverlayWidgetsCheckpoint")
//				CREATE_A_TEXT_STYLE_WIGET(CheckpointNumberStyle_Widget, "CheckpointNumberStyle_Widget")
//				CREATE_A_TEXT_STYLE_WIGET(CheckpointTitleStyle_Widget, "CheckpointTitleStyle_Widget")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(CheckpointNumberPlacement_Widget, "CheckpointNumberPlacement_Widget")
//
//				CREATE_A_TEXT_PLACEMENT_WIDGET(CheckpointMultiplyerPlacement_Widget, "CheckpointMultiplyerPlacement_Widget")
//				CREATE_A_TEXT_STYLE_WIGET(CheckpointMultiplyerStyle_Widget, "CheckpointMultiplyerStyle_Widget")
//				ADD_WIDGET_FLOAT_SLIDER("TitleOffset_Widget", TitleOffset_Widget, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			 
//			InitCheckpointWidgets = TRUE
//			
//			 
//		ENDIF

		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_CHECKPOINT, Index)
		
		IF IS_LANGUAGE_NON_ROMANIC()
		
//			IF isPlayer
//				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
//			ELSE
				SET_STANDARD_UI_METER_BIG_TITLE(TitleStyle, DROPSTYLE_NONE)
//			ENDIF
		
		ELSE
		
			IF isPlayer
				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
			ELSE
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ENDIF
		ENDIF
		
		SET_STANDARD_SMALL_HUD_TEXT(MultiplyerStyle, DROPSTYLE_NONE)
		MultiplyerStyle.YScale += 0.166+0.095
		
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		SET_STANDARD_UI_TIMERNUMBER_THREESET(NumberStyle, DROPSTYLE_NONE)
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
		SET_TEXT_STYLE(NumberStyle)
		
//		#IF IS_DEBUG_BUILD
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle, CheckpointNumberStyle_Widget)
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, CheckpointTitleStyle_Widget)
//			UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(NumberPlace,CheckpointNumberPlacement_Widget)
//			
//			
//		#ENDIF
		
		BOOL bBigCheckpoint = FALSE
		IF iInBuiltMultiplyer > 0
			bBigCheckpoint = TRUE
		ENDIF
		
		INIT_SCREEN_GENERAL_CHECKPOINT(MaxNumber, TitlePlace, Sprites, Overlays,NumberPlace, NumberStyle, aRect, WhichSpace, XPos, YPos, TitleStyle, bBigCheckpoint)

		InBuiltMultiplyer_Placement.x = TitlePlace.x
		InBuiltMultiplyer_Placement.y = TitlePlace.y
		
		InBuiltMultiplyer_Placement.y += -0.006-0.007

//		#IF IS_DEBUG_BUILD
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(MultiplyerStyle, CheckpointMultiplyerStyle_Widget)
//			UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(InBuiltMultiplyer_Placement,CheckpointMultiplyerPlacement_Widget)
//
//		#ENDIF
		
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		
		SET_WORD_WRAPPING_MULTIPLYER(MultiplyerStyle)
		
		IF FlashTime = 0
			RESET_GENERIC_CHECKPOINT_FLASHING(Index)
		ENDIF
		
		IF ColourFlash = 0 
			RESET_GENERIC_CHECKPOINT_FLASHING_COLOUR(Index)
		ENDIF
		
		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
			
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericCheckpoint_Hud[index],MPGlobalsScoreHud.iFlashing_GenericCheckpoint_MiniHud[index])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF
			
			IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
				Overlays.x = TitlePlace.x
				Overlays.y = TitlePlace.y
				
				IF MaxNumber < CHECKPOINT_TIMER_SWITCH 
				AND iInBuiltMultiplyer = 0
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_SPRITES
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_SPRITES
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421 //reduce furhter 1291727
					
					
				ELSE
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_TIME
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_TIME
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				ENDIF
				
				IF IS_LANGUAGE_NON_ROMANIC()
					Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
					Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
				ENDIF

				MPGlobalsScoreHud.TopOfTimers += Overlays.h

//				#IF IS_DEBUG_BUILD
//				UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetsCheckpoint)
//				#ENDIF
				
				
				

//				IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsScoreHud.iColourFlashing_GenericCheckpoint_Hud[Index], MPGlobalsScoreHud.iColourFlashing_GenericCheckpoint_MiniHud[Index])
					
					IF (Number = MaxNumber
					AND Number > 0)
					OR ColourFlash > 0
						SPRITE_PLACEMENT ColourOverlays = Overlays
						IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericCheckpoint[Index], 2000) = FALSE
							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericCheckpoint[Index], 1250)
								MPGlobalsScoreHud.iGoalFadeFlashing_GenericCheckpoint[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericCheckpoint[Index]-17
							ENDIF
							ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericCheckpoint[Index]
							SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
							DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
						ENDIF
					ELSE	
						MPGlobalsScoreHud.iGoalFadeFlashing_GenericCheckpoint[Index] = 255
						REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericCheckpoint[Index])
					ENDIF
				
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)

//				ELSE
//					
//					SWITCH ColourFlashType
//						CASE HUDFLASHING_FLASHWHITE
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//							Overlays.a = 255
//							DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//							
//						BREAK
//						CASE HUDFLASHING_FLASHRED
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//							Overlays.a = 255
//							DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						BREAK
//						
//						DEFAULT
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						
//						BREAK
//					
//					ENDSWITCH
//				ENDIF
			ENDIF
			
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)
			
			IF isPlayer

				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE
					
					IF IS_LANGUAGE_NON_ROMANIC() = FALSE
						TitlePlace.y += -0.002
					ENDIF
					
				ENDIF

			ELIF IS_LANGUAGE_NON_ROMANIC()
		
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE
				ENDIF
				
				
				
			ENDIF
			
//			#IF IS_DEBUG_BUILD
//				TitlePlace.y  += TitleOffset_Widget
//			#ENDIF
			
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
	
			IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
				SET_TEXT_STYLE(TitleStyle)
			
				IF isPlayer = TRUE
					DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", HUD_COLOUR_WHITE, FONT_RIGHT)
				ELSE
					IF TitleNumber = -1
						DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
					ELSE
						DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			
			IF iInBuiltMultiplyer > 0
			
		
			
				DRAW_TEXT_WITH_NUMBER(InBuiltMultiplyer_Placement, MultiplyerStyle, "HUD_MULTSMAL", iInBuiltMultiplyer, FONT_RIGHT)
			ENDIF
			
	
			IF bDisplayTimerCheckbox
				RUN_CHECKPOINT_GUTS(Number, MaxNumber, Sprites, NumberStyle, NumberPlace,  aColour, anOrder, DisplaySingleDot
									#IF USE_TU_CHANGES , CrossedDot0, CrossedDot1, CrossedDot2, CrossedDot3, CrossedDot4, CrossedDot5, CrossedDot6, CrossedDot7 #ENDIF)
			ENDIF
			
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF

	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_CHECKPOINT(INT index, INT CurrentNumber, INT MaxNumber, STRING Title, HUD_COLOURS aColour, INT TitleNumber, BOOL IsPlayer, INT FlashTime, FLOAT XPos, FLOAT YPos, HUDFLASHING ColourFlashType, INT ColourFlash, INT inBuiltMultiplyer 
									#IF USE_TU_CHANGES , BOOL CrossedDot1,BOOL CrossedDot2,BOOL CrossedDot3,BOOL CrossedDot4,BOOL CrossedDot5,BOOL CrossedDot6,BOOL CrossedDot7,BOOL CrossedDot8 #ENDIF, HUD_COLOURS FleckColour)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_CHECKPOINT, Index)
	
		TEXT_STYLE TitleStyle
		TEXT_PLACEMENT TitlePlacement
		SPRITE_PLACEMENT CheckpointSprites[11]
		TEXT_STYLE NumberStyle
		TEXT_PLACEMENT NumberPlacement
		RECT CheckpointRect
		
		
//		IF MaxNumber < 11
			DRAW_GENERAL_CHECKPOINT_GUTS(index, TitleStyle, TitlePlacement, CheckpointSprites, CheckpointRect, NumberStyle, NumberPlacement, CurrentNumber, MaxNumber, UIELEMENTS_BOTTOMRIGHT, Title, aColour, TitleNumber, IsPlayer, FlashTime, XPos, YPos, ColourFlashType, ColourFlash, TRUE, inBuiltMultiplyer 
										#IF USE_TU_CHANGES , CrossedDot1,CrossedDot2,CrossedDot3,CrossedDot4,CrossedDot5,CrossedDot6,CrossedDot7, CrossedDot8 #ENDIF, FleckColour)
			
			#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_CHECKPOINT index = ")NET_PRINT_INT(index)
				ENDIF
			#ENDIF
			
			
//		ELSE
//			NET_NL()NET_PRINT("DRAW_GENERIC_CHECKPOINT MaxNumber needs to be 10 or less. If you need something bigger I'd recommend DRAW_GENERIC_BIG_NUMBER. ")
//		ENDIF
	ENDIF

ENDPROC

PROC INIT_SCREEN_GENERAL_METER(TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[],SPRITE_PLACEMENT& Overlays,UIELEMENTS WhichSpace, FLOAT Xpos, FLOAT YPos, TEXT_STYLE& TitleStyle, BOOL bBigMeter )

	
	FLOAT TheGap = GET_VALUE_OF_GAP_BAR_SPRITE(bBigMeter, FALSE) 

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
	
	SET_BAR_SPRITES(Sprites, WhichSpace, XPos, YPos)
	
	IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
		CHANGE_Y_SHIFT_END(WhichSpace, -TheGap)
	ENDIF
	
	SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
	
ENDPROC

FUNC BOOL IS_VALID_GFX_DRAW_ORDER(GFX_DRAW_ORDER eGFXDrawOrder)
	RETURN (eGFXDrawOrder >= GFX_ORDER_BEFORE_HUD_PRIORITY_LOW AND eGFXDrawOrder <= GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
ENDFUNC

//SPRITE_PLACEMENT OverlayWidgetsMeter
//TEXT_STYLE MeterTitleStyleWidgetsMeter
//TEXT_PLACEMENT MeterTitlePLACEMENTWidgetsMeter
//BOOL MeterGutsWidget 
//FLOAT MeterTitleOffsetWidget

PROC DRAW_GENERAL_METER_GUTS(INT index, TEXT_STYLE& TitleStyle, TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[], INT Number, INT MaxNumber, UIELEMENTS WhichSpace, STRING Title, HUD_COLOURS aColour, INT TitleNumber,BOOL isPlayer, INT FlashTime, FLOAT Xpos, FLOAT YPos, BOOL OnlyZeroIsEmpty, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL bBigMeter, INT iDrawRedDangerZonePercent = 0, BOOL isLiteralString=FALSE, PERCENTAGE_METER_LINE PercentageLine = PERCENTAGE_METER_LINE_NONE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TextColour = HUD_COLOUR_PURE_WHITE, BOOL bDrawLineUnderName = FALSE, HUD_COLOURS LineUnderNameColour = HUD_COLOUR_WHITE, BOOL MakeBarUrgent = FALSE, INT iUrgentPercentage = 0, HUD_COLOURS PulseToColour = HUD_COLOUR_WHITE, INT iPulseTime = -1, BOOL bUseScoreTitle = FALSE, FLOAT fNumber = -1.0, FLOAT fMaxNumber = -1.0, BOOL bUseSecondaryBar = FALSE, HUD_COLOURS eSecondaryBarColour = HUD_COLOUR_WHITE, FLOAT fSecondaryBarPercentage = 0.0, BOOL bTransparentSecBarIntersectingMainBar = FALSE, HUD_COLOURS eSecBarPulseToColour = HUD_COLOUR_WHITE, INT iSecBarPulseTime = -1, FLOAT fSecBarStartPercentage = 0.0, INT iGFXDrawOrder = -1, BOOL bCapMaxPercentage = TRUE)

	MPGlobalsScoreHud.iHowManyDisplays++
		
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF
		
		SPRITE_PLACEMENT Overlays
		
		BOOL bDisplayTimerCheckbox
//
//		IF MeterGutsWidget = FALSE
//
//			START_WIDGET_GROUP("DAMAGE METER")
//			
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetsMeter, "OverlayWidgetsMeter")
//				CREATE_A_TEXT_STYLE_WIGET(MeterTitleStyleWidgetsMeter, "MeterTitleStyleWidgetsMeter")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(MeterTitlePLACEMENTWidgetsMeter, "MeterTitlePLACEMENTWidgetsMeter")
//			
//				ADD_WIDGET_FLOAT_SLIDER("MeterTitleOffsetWidget", MeterTitleOffsetWidget, -5, 5, 0.001)
//			
//			STOP_WIDGET_GROUP()
//			
//			MeterGutsWidget = TRUE
//			
//		ENDIF
		
		
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_METER, Index) 

		IF IS_LANGUAGE_NON_ROMANIC()
//			IF isPlayer
//				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
//			ELSE
				IF bBigMeter
					SET_STANDARD_UI_METER_BIG_TITLE_NON_ROMAN(TitleStyle, DROPSTYLE_NONE)
				ELSE
					SET_STANDARD_UI_METER_BIG_TITLE(TitleStyle, DROPSTYLE_NONE)
				ENDIF
//			ENDIF
		ELSE
			IF isLiteralString
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ELSE
				IF isPlayer
					SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
				ELSE
					IF bBigMeter
						SET_STANDARD_UI_METER_BIG_TITLE_WAVE(TitleStyle, DROPSTYLE_NONE)
					ELSE
						SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		INIT_SCREEN_GENERAL_METER(TitlePlace, Sprites,Overlays, WhichSpace, Xpos, YPos, TitleStyle, bBigMeter)

		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		
//		#IF IS_DEBUG_BUILD
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, MeterTitleStyleWidgetsMeter)
//		
//		#ENDIF
		
		
		
		
		IF FlashTime = 0
			RESET_GENERIC_METER_FLASHING(Index)
		ENDIF 
		
		IF ColourFlash = 0 
			RESET_GENERIC_METER_FLASHING_COLOUR(Index)
		ENDIF

		IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericMeter_Hud[index],MPGlobalsScoreHud.iFlashing_GenericMeter_MiniHud[index])
			bDisplayTimerCheckbox = TRUE
		ELSE
			bDisplayTimerCheckbox = FALSE
		ENDIF

		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
			
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
				Overlays.x = TitlePlace.x
				Overlays.y = TitlePlace.y
				
				IF bBigMeter
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_BIGMETER
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_BIGMETER
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				ELSE
					Overlays.x += TIMER_OVERLAY_X
					Overlays.y += TIMER_OVERLAY_Y_SPRITES
					Overlays.w += TIMER_OVERLAY_W
					Overlays.h += TIMER_OVERLAY_H_SPRITES
					Overlays.r += 255
					Overlays.g += 255
					Overlays.b += 255
					Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				
				
				ENDIF
				
				IF IS_LANGUAGE_NON_ROMANIC()
					Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
					Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
				ENDIF
				
				
				MPGlobalsScoreHud.TopOfTimers += Overlays.h
				
//				#IF IS_DEBUG_BUILD
//				UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetsMeter)
//				#ENDIF
				
//				IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsScoreHud.iColourFlashing_GenericMeter_Hud[Index], MPGlobalsScoreHud.iColourFlashing_GenericMeter_MiniHud[Index])
//
//					DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//				ELSE
//					
//					SWITCH ColourFlashType
//						CASE HUDFLASHING_FLASHWHITE
//							DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						BREAK
//						CASE HUDFLASHING_FLASHRED
//							DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						BREAK
//						
//						DEFAULT
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						
//						BREAK
//					
//					ENDSWITCH
//				ENDIF



//				IF DO_COLOUR_FLASHING(ColourFlash, MPGlobalsmeterHud.iColourFlashing_Genericmeter_Hud[Index], MPGlobalsmeterHud.iColourFlashing_Genericmeter_MiniHud[Index])
				
				IF ColourFlash > 0
					SPRITE_PLACEMENT ColourOverlays = Overlays
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[Index], 2000) = FALSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[Index], 1250)
							MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index]-17
						ENDIF
						ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index]
						SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ELSE	
					MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index] = 255
					REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[Index])
				ENDIF
								
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)

//				ELSE
//					
//					SWITCH ColourFlashType
//						CASE HUDFLASHING_FLASHWHITE
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//							Overlays.a = 255
//							DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//							
//						BREAK
//						CASE HUDFLASHING_FLASHRED
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//							Overlays.a = 255
//							DRAW_2D_SPRITE("TimerBars", "ALL_RED_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						BREAK
//						
//						DEFAULT
//							DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
//						
//						BREAK
//					
//					ENDSWITCH
//				ENDIF

			ENDIF
		
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)
		
//			#IF IS_DEBUG_BUILD
//				UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TitlePlace, MeterTitlePLACEMENTWidgetsMeter)
//			#ENDIF
			
			
			IF IS_LANGUAGE_NON_ROMANIC()
				IF bBigMeter
					TitlePlace.y += -0.010
					

				ELIF isPlayer
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
					ELSE
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE
	
					ENDIF
				ELSE
					IF bBigMeter = FALSE
						IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
						OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
							TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
						ELSE
							TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE
						ENDIF
					ELSE
						IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
						OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
							TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
						ELSE
							TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				
				IF bBigMeter
					TitlePlace.y += -0.010+0.0022+0.001

				ELIF isPlayer
					TitlePlace.y += -0.005+0.001
					TitlePlace.y += -0.002
				
				ENDIF
			
			
			ENDIF 
			
		
//			#IF IS_DEBUG_BUILD
//				TitlePlace.y += MeterTitleOffsetWidget
//			#ENDIF
			
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			IF (iGFXDrawOrder != -1)
				IF IS_VALID_GFX_DRAW_ORDER(INT_TO_ENUM(GFX_DRAW_ORDER, iGFXDrawOrder))
					anOrder = INT_TO_ENUM(GFX_DRAW_ORDER, iGFXDrawOrder)
				ENDIF
			ENDIF
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
				
				IF isLiteralString
					SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, aColour)
					SET_TEXT_STYLE(TitleStyle)
					DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TextColour, FONT_RIGHT)	
				ELSE
					IF isPlayer = TRUE
						SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, aColour)
						SET_TEXT_STYLE(TitleStyle)
						DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TextColour, FONT_RIGHT)
					ELSE
						SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, TextColour)
						SET_TEXT_STYLE(TitleStyle)
						
						IF bUseScoreTitle
							DRAW_TEXT_AND_NUMBERS_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE, Number, MaxNumber)
						ELSE
							IF TitleNumber = -1
								DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
							ELSE
								DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDrawLineUnderName
				FLOAT BarYPos
				FLOAT BarXPos
				SPRITE_PLACEMENT LineSprite
				
				IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
					BarYPos = GET_BAR_TO_TITLE_OFFSET()+GET_Y_SHIFT(WhichSpace)
					BarXPos = GET_DAMAGE_BAR_TO_TITLE_OFFSET_X()
				ELSE
					BarYPos = Ypos
					BarXPos = Xpos
				ENDIF
				
				LineSprite.y = BarYPos+0.0486
				LineSprite.x = BarXPos-0.0505
				LineSprite.w = 0.180
				LineSprite.h = 0.01
				LineSprite.r = 255
				LineSprite.g = 255
				LineSprite.b = 255
				LineSprite.a = 255
				
				SET_SPRITE_HUD_COLOUR(LineSprite, LineUnderNameColour)
				DRAW_2D_SPRITE("TimerBars", "TPBar", LineSprite, FALSE, HIGHLIGHT_OPTION_NORMAL, anOrder)
			ENDIF
			
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			IF (iGFXDrawOrder != -1)
				IF IS_VALID_GFX_DRAW_ORDER(INT_TO_ENUM(GFX_DRAW_ORDER, iGFXDrawOrder))
					anOrder = INT_TO_ENUM(GFX_DRAW_ORDER, iGFXDrawOrder)
				ENDIF
			ENDIF
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			IF bDisplayTimerCheckbox
				IF bBigMeter
					RUN_BIG_METER_GUTS(Number, MaxNumber, Sprites, aColour, OnlyZeroIsEmpty, anOrder, PercentageLine, MPGlobalsScoreHud.iColourPulsing_GenericMeter_Hud[Index], MPGlobalsScoreHud.iColourPulsing_GenericMeter_SecBarHud[Index], fNumber, fMaxNumber, PulseToColour, iPulseTime, bUseSecondaryBar, eSecondaryBarColour, fSecondaryBarPercentage, bTransparentSecBarIntersectingMainBar, eSecBarPulseToColour, iSecBarPulseTime, bCapMaxPercentage)
				ELSE
					RUN_METER_GUTS(Number, MaxNumber, Sprites, aColour, OnlyZeroIsEmpty, anOrder, MPGlobalsScoreHud.iColourPulsing_GenericMeter_Hud[Index], MPGlobalsScoreHud.iColourPulsing_GenericMeter_SecBarHud[Index], iDrawRedDangerZonePercent, PercentageLine, MakeBarUrgent, iUrgentPercentage, fNumber, fMaxNumber, PulseToColour, iPulseTime, bUseSecondaryBar, eSecondaryBarColour, fSecondaryBarPercentage, bTransparentSecBarIntersectingMainBar, eSecBarPulseToColour, iSecBarPulseTime, fSecBarStartPercentage, bCapMaxPercentage)
				ENDIF
			ENDIF
			
			RESET_GFX_TIMERS_DRAW_ORDER()
			
			#IF IS_DEBUG_BUILD
			//PRINTLN("DRAW_GENERAL_METER_GUTS: Timebars dictionary has loaded") BC: spammy and only really need to know when it fails. 
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("DRAW_GENERAL_METER_GUTS: Timebars dictionary has NOT loaded")
			#ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_METER(INT index, INT CurrentNumber, INT MaxNumber, STRING Title, HUD_COLOURS aColour, INT FlashTime, FLOAT Xpos, FLOAT YPos, BOOL isPlayer, INT TitleNumber, BOOL OnlyZeroIsEmpty, HUDFLASHING ColourFlashType, INT ColourFlash, BOOL BigMeter, INT iDrawRedDangerZonePercent , BOOL isLiteralString, PERCENTAGE_METER_LINE PercentageLine , HUD_COLOURS FleckColour, HUD_COLOURS TextColour, BOOL bDrawLineUnderName, HUD_COLOURS LineUnderNameColour, BOOL MakeBarUrgent, INT iUrgentPercentage, HUD_COLOURS PulseToColour, INT iPulseTime, BOOL bUseScoreTitle, FLOAT fCurrentNumber, FLOAT fMaxNumber, BOOL bUseSecondaryBar, HUD_COLOURS eSecondaryBarColour, FLOAT fSecondaryBarPercentage, BOOL bTransparentSecBarIntersectingMainBar, HUD_COLOURS eSecBarPulseToColour, INT iSecBarPulseTime, FLOAT fSecBarStartPercentage = 0.0, INT iGFXDrawOrder = -1, BOOL bCapMaxPercentage = TRUE)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_METER, Index)
	
	
		TEXT_STYLE TitleStyle
		TEXT_PLACEMENT TitlePlacement
		SPRITE_PLACEMENT MeterSprites[4]
		
		

		DRAW_GENERAL_METER_GUTS(index, TitleStyle, TitlePlacement, MeterSprites, CurrentNumber, MaxNumber, UIELEMENTS_BOTTOMRIGHT, Title, aColour,TitleNumber, isPlayer, FlashTime, XPos, YPos, OnlyZeroIsEmpty, ColourFlashType, ColourFlash, BigMeter, iDrawRedDangerZonePercent, isLiteralString, PercentageLine, FleckColour, TextColour, bDrawLineUnderName, LineUnderNameColour, MakeBarUrgent, iUrgentPercentage, PulseToColour, iPulseTime, bUseScoreTitle, fCurrentNumber, fMaxNumber, bUseSecondaryBar, eSecondaryBarColour, fSecondaryBarPercentage, bTransparentSecBarIntersectingMainBar, eSecBarPulseToColour, iSecBarPulseTime, fSecBarStartPercentage, iGFXDrawOrder, bCapMaxPercentage)
	
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_METER index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC









PROC INIT_SCREEN_GENERAL_SPRITE_METER(TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[],SPRITE_PLACEMENT& Overlays,UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle)

	
	FLOAT TheGap = GET_VALUE_OF_GAP_BAR_SPRITE(FALSE, TRUE) 

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
	
	SET_BAR_SPRITES(Sprites, WhichSpace, -1, -1)
	

	CHANGE_Y_SHIFT_END(WhichSpace, -TheGap)

	
	SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
	
ENDPROC

//SPRITE_PLACEMENT OverlayWidgetsMeter
//TEXT_STYLE MeterTitleStyleWidgetsMeter
//TEXT_PLACEMENT MeterTitlePLACEMENTWidgetsMeter
//BOOL MeterGutsWidget 
//FLOAT MeterTitleOffsetWidget

PROC DRAW_GENERAL_SPRITE_METER_GUTS(INT index, TEXT_STYLE& TitleStyle, TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[], INT Number, INT MaxNumber, UIELEMENTS WhichSpace, STRING Title, HUD_COLOURS aColour, INT TitleNumber,BOOL isPlayer, INT FlashTime, BOOL OnlyZeroIsEmpty, HUDFLASHING ColourFlashType, INT ColourFlash,STRING spriteName, STRING dictionaryName, BOOL isLiteralString=FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TextColour = HUD_COLOUR_WHITE)

	MPGlobalsScoreHud.iHowManyDisplays++

	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF
		
		SPRITE_PLACEMENT Overlays
		
		BOOL bDisplayTimerCheckbox

//		IF MeterGutsWidget = FALSE
//
//			START_WIDGET_GROUP("DAMAGE METER")
//			
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetsMeter, "OverlayWidgetsMeter")
//				CREATE_A_TEXT_STYLE_WIGET(MeterTitleStyleWidgetsMeter, "MeterTitleStyleWidgetsMeter")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(MeterTitlePLACEMENTWidgetsMeter, "MeterTitlePLACEMENTWidgetsMeter")
//			
//				ADD_WIDGET_FLOAT_SLIDER("MeterTitleOffsetWidget", MeterTitleOffsetWidget, -5, 5, 0.001)
//			
//			STOP_WIDGET_GROUP()
//			
//			MeterGutsWidget = TRUE
//			
//		ENDIF
		
		
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_SPRITE_METER, Index) 

		IF IS_LANGUAGE_NON_ROMANIC()
			SET_STANDARD_UI_METER_BIG_TITLE(TitleStyle, DROPSTYLE_NONE)
		ELSE
			IF isLiteralString
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ELSE
				IF isPlayer
					SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
				ELSE
					SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
				ENDIF
			ENDIF
		ENDIF
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		INIT_SCREEN_GENERAL_SPRITE_METER(TitlePlace, Sprites,Overlays, WhichSpace, TitleStyle)

		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		
//		#IF IS_DEBUG_BUILD
//			UPDATE_TEXT_STYLE_WIDGET_VALUE(TitleStyle, MeterTitleStyleWidgetsMeter)
//		
//		#ENDIF
		
		
		
		
		IF FlashTime = 0
			RESET_GENERIC_METER_FLASHING(Index)
		ENDIF 
		
		IF ColourFlash = 0 
			RESET_GENERIC_METER_FLASHING_COLOUR(Index)
		ENDIF

		IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericMeter_Hud[index],MPGlobalsScoreHud.iFlashing_GenericMeter_MiniHud[index])
			bDisplayTimerCheckbox = TRUE
		ELSE
			bDisplayTimerCheckbox = FALSE
		ENDIF

		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
			
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			

			Overlays.x += TIMER_OVERLAY_X
			Overlays.y += TIMER_OVERLAY_Y_SPRITEMETER
			Overlays.w += TIMER_OVERLAY_W
			Overlays.h += TIMER_OVERLAY_H_SPRITEMETER
			Overlays.r += 255
			Overlays.g += 255
			Overlays.b += 255
			Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			
			IF ColourFlash > 0
				SPRITE_PLACEMENT ColourOverlays = Overlays
				IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[Index], 2000) = FALSE
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[Index], 1250)
						MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index]-17
					ENDIF
					ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index]
					SET_SPRITE_HUD_COLOUR(ColourOverlays, aColour)
					DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
				ENDIF
			ELSE	
				MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[Index] = 255
				REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[Index])
			ENDIF
			
			DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)


			
		
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)
		
//			#IF IS_DEBUG_BUILD
//				UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TitlePlace, MeterTitlePLACEMENTWidgetsMeter)
//			#ENDIF
			
			
			IF IS_LANGUAGE_NON_ROMANIC()
			
				IF isPlayer
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
					ELSE
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE
	
					ENDIF
				ELSE
					
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_CHINESE
					ELSE
						TitlePlace.y += TITLE_NON_ROMANIC_OFFSET
					ENDIF
					
					
				ENDIF
			ELSE
				
				
				IF isPlayer
					TitlePlace.y += -0.005+0.001
				ENDIF

			ENDIF 
			
		
			
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			
				
			IF isLiteralString
				SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, aColour)
				SET_TEXT_STYLE(TitleStyle)
				DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TextColour, FONT_RIGHT)	
			ELSE
				IF isPlayer = TRUE
					SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, aColour)
					SET_TEXT_STYLE(TitleStyle)
					DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", TextColour, FONT_RIGHT)
				ELSE
					SET_TEXT_STYLE(TitleStyle)
					IF TitleNumber = -1
						DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
					ELSE
						DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			
			
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			IF bDisplayTimerCheckbox
				RUN_SPRITE_METER_GUTS(Number, MaxNumber, Sprites, aColour, OnlyZeroIsEmpty, anOrder)
				RUN_SPRITE_METER_SPRITE(spriteName, Sprites, anOrder, dictionaryName)
			ENDIF
			
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
		
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_SPRITE_METER(INT index, INT CurrentNumber, INT MaxNumber, STRING Title, HUD_COLOURS aColour, INT FlashTime, BOOL isPlayer, INT TitleNumber, BOOL OnlyZeroIsEmpty, HUDFLASHING ColourFlashType, INT ColourFlash,STRING spriteName, STRING dictionaryName, BOOL isLiteralString, HUD_COLOURS FleckColour, HUD_COLOURS TextColour)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SPRITE_METER, Index)
	
	
		TEXT_STYLE TitleStyle
		TEXT_PLACEMENT TitlePlacement
		SPRITE_PLACEMENT MeterSprites[4]
		
		

		DRAW_GENERAL_SPRITE_METER_GUTS(index, TitleStyle, TitlePlacement, MeterSprites, CurrentNumber, MaxNumber, UIELEMENTS_BOTTOMRIGHT, Title, aColour,TitleNumber, isPlayer, FlashTime, OnlyZeroIsEmpty, ColourFlashType, ColourFlash,spriteName, dictionaryName, isLiteralString, FleckColour, TextColour)
	
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SPRITE_METER index = ")NET_PRINT_INT(index)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC









PROC INIT_SCREEN_GENERAL_ELIMINATION(TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[],SPRITE_PLACEMENT& Overlays,RECT& aRect, UIELEMENTS WhichSpace, FLOAT XPOS, FLOAT YPOS, TEXT_STYLE& TitleStyle )

	FLOAT TheGap = GET_VALUE_OF_GAP_BAR_SPRITE(FALSE, FALSE) 

	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
	
	SET_CIRCLE_SPRITES(Sprites, WhichSpace, XPOS, YPOS)
	SET_LINE_DIVIDER(aRect)
	
	IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
		CHANGE_Y_SHIFT_END(WhichSpace, -TheGap)
	ENDIF
	
	SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
	
ENDPROC








//SPRITE_PLACEMENT OverlayWidgetsElim
//BOOL isEliminationWidget
//FLOAT EliminationTitleOffsetWidget

PROC DRAW_GENERAL_ELIMINATION_GUTS(INT index, TEXT_STYLE& TitleStyle, TEXT_PLACEMENT& TitlePlace, SPRITE_PLACEMENT& Sprites[], RECT& aRect, INT MaxNumber,
									UIELEMENTS WhichSpace,  HUD_COLOURS aColourFirst, HUD_COLOURS aColourSecond, STRING Title, INT VisibleBoxes,
									BOOL IsActive1, BOOL IsActive2,BOOL IsActive3,BOOL IsActive4,BOOL IsActive5,
									BOOL IsActive6,BOOL IsActive7,BOOL IsActive8,  INT FlashTime,  INT TitleNumber,BOOL isPlayer, FLOAT XPOS, FLOAT YPOS,
									HUD_COLOURS Box1Colour,HUD_COLOURS Box2Colour, HUD_COLOURS Box3Colour, HUD_COLOURS Box4Colour, HUD_COLOURS Box5Colour, 
									HUD_COLOURS Box6Colour, HUD_COLOURS Box7Colour, HUD_COLOURS Box8Colour, HUD_COLOURS Box1Colour_Inactive,
									HUD_COLOURS Box2Colour_Inactive, HUD_COLOURS Box3Colour_Inactive, HUD_COLOURS Box4Colour_Inactive, HUD_COLOURS Box5Colour_Inactive, 
									HUD_COLOURS Box6Colour_Inactive, HUD_COLOURS Box7Colour_Inactive, HUD_COLOURS Box8Colour_Inactive,
									HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS TitleColour = HUD_COLOUR_PURE_WHITE
									#IF USE_TU_CHANGES , BOOL Crossed0 = FALSE, BOOL Crossed1 = FALSE, BOOL Crossed2 = FALSE, BOOL Crossed3 = FALSE, BOOL Crossed4 = FALSE, BOOL Crossed5 = FALSE, BOOL Crossed6 = FALSE, BOOL Crossed7  = FALSE #ENDIF, BOOL bUseNonPlayerFont = FALSE,
									HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE #IF USE_TU_CHANGES , HUD_COLOURS CrossedDot0Colour = HUD_COLOUR_BLACK, HUD_COLOURS CrossedDot1Colour = HUD_COLOUR_BLACK, HUD_COLOURS CrossedDot2Colour = HUD_COLOUR_BLACK, 
									HUD_COLOURS CrossedDot3Colour = HUD_COLOUR_BLACK, HUD_COLOURS CrossedDot4Colour = HUD_COLOUR_BLACK, HUD_COLOURS CrossedDot5Colour = HUD_COLOUR_BLACK, HUD_COLOURS CrossedDot6Colour = HUD_COLOUR_BLACK, HUD_COLOURS CrossedDot7Colour = HUD_COLOUR_BLACK #ENDIF)

	MPGlobalsScoreHud.iHowManyDisplays++
	
//	TEXT_STYLE ButtonStyle 
//	TEXT_PLACEMENT ButtonPlacement
	
//	STRING PadButtonString
		
		
	IF IS_BOTTOM_RIGHT_AREA_FREE()
	
		IF ColourFlashType = HUDFLASHING_FLASHRED
		ENDIF
		
		

			
	
		SPRITE_PLACEMENT Overlays
		
		BOOL bDisplayTimerCheckbox

//		IF isEliminationWidget = FALSE
//			START_WIDGET_GROUP("ELIMINATION OVERLAY")
//			
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(OverlayWidgetsElim, "OverlayWidgetsElim")
//				ADD_WIDGET_FLOAT_SLIDER("EliminationTitleOffsetWidget", EliminationTitleOffsetWidget, -5, 5, 0.001)
//			STOP_WIDGET_GROUP()
//			isEliminationWidget = TRUE
//			
//		ENDIF

		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_ELIMINATION, Index)
		
	
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		INIT_SCREEN_GENERAL_ELIMINATION(TitlePlace, Sprites, Overlays, aRect, WhichSpace, XPos, YPos, TitleStyle)
	
			
	
	
	
		IF IS_LANGUAGE_NON_ROMANIC()
		
//			IF isPlayer
//				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
//			ELSE
				SET_STANDARD_UI_METER_BIG_TITLE(TitleStyle, DROPSTYLE_NONE)
//			ENDIF
		
		ELSE
			IF bUseNonPlayerFont
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ELIF isPlayer
				SET_STANDARD_TIMER_PLYR_NAME_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
			ELSE
				SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
			ENDIF
		ENDIF
		
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		//B* Adjust the title WrapEndX coordinate on aspect ratios other than 16:9
		TitleStyle.wrapEndX += 0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())
		//CPRINTLN(debug_dan,"moved wrap by ",0.03*(1-GET_TIMER_TEXT_ASPECT_RATIO_OFFSET_MULT())," to ",TitleStyle.wrapEndX )
		

		IF FlashTime = 0
			RESET_GENERIC_ELIMINATION_FLASHING(Index)
		ENDIF
		
		IF ColourFlash = 0 
			RESET_GENERIC_ELIMINATION_FLASHING_COLOUR(Index)
		ENDIF


		GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
		SET_SCRIPT_GFX_DRAW_ORDER(anOrder)

			

		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
		
			
		
			IF DO_FLASHING(FlashTime, MPGlobalsScoreHud.iFlashing_GenericElimination_Hud[index],MPGlobalsScoreHud.iFlashing_GenericElimination_MiniHud[index])
				bDisplayTimerCheckbox = TRUE
			ELSE
				bDisplayTimerCheckbox = FALSE
			ENDIF
			
			IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
				Overlays.x = TitlePlace.x
				Overlays.y = TitlePlace.y
//				Overlays[1].x = TitlePlace.x
//				Overlays[1].y = TitlePlace.y
				
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_SPRITES
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_SPRITES
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
				
				IF IS_LANGUAGE_NON_ROMANIC()
					Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
					Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
				ENDIF
				
				MPGlobalsScoreHud.TopOfTimers += Overlays.h
				
//				
//				#IF IS_DEBUG_BUILD
//				UPDATE_SPRITE_WIDGET_VALUE(Overlays, OverlayWidgetsElim)
//				#ENDIF
			


				IF ColourFlash > 0
					SPRITE_PLACEMENT ColourOverlays = Overlays
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsScoreHud.iGoalMetFlashing_GenericElimination[Index], 2000) = FALSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsScoreHud.iGoalMetFlashing_GenericElimination[Index], 1250)
							MPGlobalsScoreHud.iGoalFadeFlashing_GenericElimination[Index] = MPGlobalsScoreHud.iGoalFadeFlashing_GenericElimination[Index]-17
						ENDIF
						ColourOverlays.a = MPGlobalsScoreHud.iGoalFadeFlashing_GenericElimination[Index]
						SET_SPRITE_HUD_COLOUR(ColourOverlays, aColourFirst)
						DRAW_2D_SPRITE("TimerBars", "ALL_WHITE_bg",ColourOverlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)
					ENDIF
				ELSE	
					MPGlobalsScoreHud.iGoalFadeFlashing_GenericElimination[Index] = 255
					REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericElimination[Index])
				ENDIF
				
				DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg",Overlays, TRUE, HIGHLIGHT_OPTION_NORMAL, anOrder)


			ENDIF
			
			DRAW_ORGANISATION_FLECK(Overlays, FleckColour)
		
			anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			SET_TEXT_COLOUR_WITH_HUD_COLOUR(TitleStyle, TitleColour) 
			
			IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
			OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
				TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
			ELSE
				TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE
			ENDIF
			
			IF IS_LANGUAGE_NON_ROMANIC()
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_CHINESE
				ELSE
					TitlePlace.y += TITLE_NON_ROMANIC_OFFSET_LITTLE_DOTS
				ENDIF
				
			ENDIF
			
			IF IS_LANGUAGE_NON_ROMANIC() = FALSE
				IF bUseNonPlayerFont
					TitlePlace.y += 0.003
				ELIF isPlayer
					TitlePlace.y += -0.002
				ENDIF
			ELSE
				
				IF isPlayer
					TitlePlace.y += 0.007
				ELSE
					TitlePlace.y += 0.003
				ENDIF
			ENDIF
			
//			#IF Is_DEBUG_BUILD
//				TitlePlace.y  += EliminationTitleOffsetWidget
//			
//			#ENDIF
			
			IF NOT IS_BAR_FREEROAM(Xpos, Ypos)
				SET_TEXT_STYLE(TitleStyle)
				
				IF bUseNonPlayerFont
				
					DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title,"",  TitleColour, FONT_RIGHT)
					
				ELIF isPlayer = TRUE

					DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title,"",  TitleColour, FONT_RIGHT)
					
					
					
				ELSE
					IF TitleNumber = -1
						DRAW_TEXT_WITH_ALIGNMENT(TitlePlace, TitleStyle, Title, FALSE, TRUE)
					ELSE
						TitlePlace.y -= TITLE_NON_ROMANIC_OFFSET_LITTLE
						DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, Title, TitleNumber, FONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
		
			IF bDisplayTimerCheckbox
			
				RUN_ELIMINATION_GUTS(MaxNumber, Sprites, aColourFirst, aColourSecond, VisibleBoxes, 
									IsActive1, IsActive2, IsActive3, IsActive4, IsActive5, IsActive6, IsActive7, IsActive8,
									Box1Colour, Box2Colour, Box3Colour, Box4Colour, Box5Colour, Box6Colour, Box7Colour, Box8Colour,
									Box1Colour_Inactive, Box2Colour_Inactive, Box3Colour_Inactive, Box4Colour_Inactive, Box5Colour_Inactive, 
									Box6Colour_Inactive, Box7Colour_Inactive, Box8Colour_Inactive, anOrder
									#IF USE_TU_CHANGES , Crossed0, Crossed1, Crossed2, Crossed3, Crossed4, Crossed5, Crossed6, Crossed7,
									CrossedDot0Colour, CrossedDot1Colour, CrossedDot2Colour, CrossedDot3Colour, CrossedDot4Colour, CrossedDot5Colour, CrossedDot6Colour, CrossedDot7Colour #ENDIF)

			ENDIF
			
			RESET_GFX_TIMERS_DRAW_ORDER()

		ENDIF
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_ELIMINATION(INT Index, INT MaxNumber, STRING Title, HUD_COLOURS FirstColour,HUD_COLOURS SecondColour , INT iVisibleBoxes, BOOL Active1, BOOL Active2, BOOL Active3, BOOL Active4, BOOL Active5, BOOL Active6, BOOL Active7, BOOL Active8,  INT FlashTime, 
										INT TitleNumber, BOOL ISPlayer, FLOAT XPOS, FLOAT YPOS, 
										HUD_COLOURS Box1Colour,HUD_COLOURS Box2Colour, HUD_COLOURS Box3Colour, HUD_COLOURS Box4Colour, HUD_COLOURS Box5Colour, HUD_COLOURS Box6Colour, HUD_COLOURS Box7Colour, HUD_COLOURS Box8Colour,
										HUD_COLOURS Box1Colour_Inactive ,HUD_COLOURS Box2Colour_Inactive, HUD_COLOURS Box3Colour_Inactive, HUD_COLOURS Box4Colour_Inactive, HUD_COLOURS Box5Colour_Inactive, HUD_COLOURS Box6Colour_Inactive, HUD_COLOURS Box7Colour_Inactive, 
										HUD_COLOURS Box8Colour_Inactive, HUDFLASHING ColourFlashType, INT ColourFlash, HUD_COLOURS TitleColour
										#IF USE_TU_CHANGES , BOOL Crossed0, BOOL Crossed1, BOOL Crossed2, BOOL Crossed3, BOOL Crossed4, BOOL Crossed5, BOOL Crossed6, BOOL Crossed7 #ENDIF, BOOL bUseNonPlayerFont, HUD_COLOURS FleckColour
										#IF USE_TU_CHANGES , HUD_COLOURS CrossedDot0Colour, HUD_COLOURS CrossedDot1Colour, HUD_COLOURS CrossedDot2Colour, HUD_COLOURS CrossedDot3Colour, HUD_COLOURS CrossedDot4Colour, HUD_COLOURS CrossedDot5Colour, HUD_COLOURS CrossedDot6Colour, HUD_COLOURS CrossedDot7Colour #ENDIF )
										
	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, index)
		
		TEXT_STYLE TitleStyle
		TEXT_PLACEMENT TitlePlacement
		SPRITE_PLACEMENT MeterSprites[9]
		RECT aRect
		IF MaxNumber < 9
				
			DRAW_GENERAL_ELIMINATION_GUTS(index, TitleStyle, TitlePlacement, MeterSprites,aRect, MaxNumber, UIELEMENTS_BOTTOMRIGHT, FirstColour,SecondColour,
										Title, iVisibleBoxes, Active1,Active2, Active3, Active4, Active5, Active6, Active7, Active8,FlashTime,TitleNumber,ISPlayer, XPos, YPOS,
										Box1Colour, Box2Colour, Box3Colour, Box4Colour, Box5Colour, Box6Colour, Box7Colour, Box8Colour, 
										Box1Colour_Inactive, Box2Colour_Inactive, Box3Colour_Inactive, Box4Colour_Inactive, Box5Colour_Inactive, Box6Colour_Inactive, Box7Colour_Inactive, 
										Box8Colour_Inactive, ColourFlashType, ColourFlash, TitleColour #IF USE_TU_CHANGES , Crossed0, Crossed1, Crossed2, Crossed3, Crossed4, Crossed5, Crossed6, Crossed7 #ENDIF, bUseNonPlayerFont, FleckColour
										#IF USE_TU_CHANGES , CrossedDot0Colour, CrossedDot1Colour, CrossedDot2Colour, CrossedDot3Colour, CrossedDot4Colour, CrossedDot5Colour, CrossedDot6Colour, CrossedDot7Colour #ENDIF)
		
			#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_ELIMINATION index = ")NET_PRINT_INT(index)
				ENDIF
			#ENDIF
		ELSE
			NET_NL()NET_PRINT("DRAW_GENERIC_ELIMINATION MaxNumber needs to be 8 or less. If you need something bigger I'd recommend DRAW_GENERIC_BIG_NUMBER. ")
		
			
		
		ENDIF	
		
		
	ENDIF
ENDPROC

PROC INIT_SCREEN_GENERAL_WINDMETER(TEXT_PLACEMENT& TitlePlace, UIELEMENTS WhichSpace, TEXT_STYLE& TitleStyle, SPRITE_PLACEMENT & arrowSprite)	
	FLOAT TheGap = GET_VALUE_OF_GAP_BAR_SPRITE(FALSE, FALSE)
	
	TitlePlace.x = 0.795 
	TitlePlace.y = GET_Y_SHIFT(WhichSpace)
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
		
	CHANGE_Y_SHIFT_END(WhichSpace, -TheGap)
	
	TitlePlace.y += GET_TITLE_OFFSET(TitleStyle)
	
	arrowSprite.x = 0.9375
	arrowSprite.y = TitlePlace.y + 0.009
	arrowSprite.w = 0.02
	arrowSprite.h = 0.02
ENDPROC

PROC DRAW_GENERAL_WINDMETER_GUTS(INT idx, TEXT_STYLE & TitleStyle,	TEXT_PLACEMENT & TitlePlace, SPRITE_PLACEMENT & MeterSprites[], 
			UIELEMENTS WhichSpace, 
			STRING sTitle, INT iWindSpeed, FLOAT fWindDirection, INT iR, INT iG, INT iB)
	MPGlobalsScoreHud.iHowManyDisplays++

	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
		SPRITE_PLACEMENT arrowIcon
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)

		SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_WINDMETER, idx) 
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		INIT_SCREEN_GENERAL_WINDMETER(TitlePlace, WhichSpace, TitleStyle, arrowIcon)
	
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		REQUEST_STREAMED_TEXTURE_DICT("Hunting")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("Hunting")
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			Overlays.x += TIMER_OVERLAY_X
			Overlays.y += TIMER_OVERLAY_Y_SCORE
			Overlays.w += TIMER_OVERLAY_W
			Overlays.h += TIMER_OVERLAY_H_SCORE
			Overlays.r += 255
			Overlays.g += 255
			Overlays.b += 255
			Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg", Overlays)
			
			arrowIcon.r = iR
			arrowIcon.g = iG
			arrowIcon.b = iB
			arrowIcon.a = 255
			arrowIcon.fRotation = fWindDirection
			//CDEBUG3LN(DEBUG_HUNTING, "Rendering Arrow Color R: ", iR, " :: G: ", iG, " :: B: ", iB, " :: A: ", arrowIcon.a)
			DRAW_2D_SPRITE("Hunting", "HuntingWindArrow_32", arrowIcon)

			TitleStyle.r = iR
			TitleStyle.g = iG
			TitleStyle.b = iB
			TitleStyle.a = 255
			SET_TEXT_STYLE(TitleStyle)
			
			fWindDirection = fWindDirection
			MeterSprites[0].x = MeterSprites[0].x

			DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, sTitle, iWindSpeed, FONT_RIGHT)
			 
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_WINDMETER(INT idx, STRING sTitle, FLOAT fHeading, INT iSpeed, INT iR, INT iG, INT iB)
	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_WINDMETER, idx)
		
		TEXT_STYLE TitleStyle
		TEXT_PLACEMENT TitlePlacement
		SPRITE_PLACEMENT MeterSprites[2]

		DRAW_GENERAL_WINDMETER_GUTS(idx, TitleStyle, TitlePlacement, MeterSprites, UIELEMENTS_BOTTOMRIGHT,
				sTitle, iSpeed, fHeading, iR, iG, iB)
	
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_WINDMETER index = ")NET_PRINT_INT(idx)
			ENDIF
		#ENDIF
	ENDIF
	
	//ACTUALLY_DRAW_GENERAL_METER
ENDPROC

PROC GET_BIG_RACE_POSITION_TEXT_LABEL(TEXT_LABEL_15& tlRacePositionLabel, INT iRacePosition)

	TEXT_LABEL_15 tlRacePosition = "RACE_POS_"
	tlRacePosition += iRacePosition
	tlRacePositionLabel = tlRacePosition
	
ENDPROC

//TEXT_STYLE OverlayWidgetsBigRace
//BOOL isBigRaceWidget

PROC DRAW_GENERAL_BIG_RACE_POSITION_GUTS(INT idx, TEXT_STYLE& NumberStyle, TEXT_PLACEMENT& NumberPlace, UIELEMENTS WhichSpace, INT iRacePosition, HUD_COLOURS eRacePositionHUDColour)
	MPGlobalsScoreHud.iHowManyDisplays++
	
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		
//		IF isBigRaceWidget = FALSE
//			WIDGET_GROUP_ID topLevelWidgetGroup = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
//			
//			SET_CURRENT_WIDGET_GROUP(topLevelWidgetGroup)
//			START_WIDGET_GROUP("BIG RACE OVERLAY")
//				CREATE_A_TEXT_STYLE_WIGET(OverlayWidgetsBigRace, "Text style")
//			STOP_WIDGET_GROUP()
//			CLEAR_CURRENT_WIDGET_GROUP(topLevelWidgetGroup)
//			
//			isBigRaceWidget = TRUE
//		ENDIF
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(NumberStyle, OverlayWidgetsBigRace)
		
		HIDE_ALL_BOTTOM_RIGHT_HUD()
		
		
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(eRacePositionHUDColour, iR, iG, iB, iA)
		SET_STANDARD_LARGE_RACE_HUD_TEXT(NumberStyle, DROPSTYLE_DROPSHADOWONLY, iR, iG, iB)
		
		IF IS_LANGUAGE_NON_ROMANIC()
			NumberStyle.XScale = 2.0
			NumberStyle.YScale = 2.7
		ENDIF
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		FLOAT fBigRaceCustomOffsetY = 0.131
		IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
			fBigRaceCustomOffsetY = 0.0872
		ELIF GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
			fBigRaceCustomOffsetY = 0.095
		ENDIF
		
		NumberPlace.x = 0.795
		NumberPlace.y = (GET_Y_SHIFT(WhichSpace) + GET_VALUE_TO_TITLE_OFFSET(NumberStyle)) - fBigRaceCustomOffsetY
		
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
		SET_TEXT_STYLE(NumberStyle)
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_BIG_RACE_POSITION, idx) 
		
		
		TEXT_LABEL_15 tlRacePositionLabel
		GET_BIG_RACE_POSITION_TEXT_LABEL(tlRacePositionLabel, iRacePosition)
		
		DRAW_TEXT_WITH_ALIGNMENT(NumberPlace, NumberStyle, GB_TO_STRING(tlRacePositionLabel), FALSE, TRUE)
		
		RESET_GFX_TIMERS_DRAW_ORDER()
		
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_BIG_RACE_POSITION(INT idx, INT iRacePosition, HUD_COLOURS eRacePositionHUDColour)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_BIG_RACE_POSITION, idx)
		
		TEXT_STYLE TitleStyle
		TEXT_PLACEMENT TitlePlacement

		DRAW_GENERAL_BIG_RACE_POSITION_GUTS(idx, TitleStyle, TitlePlacement, UIELEMENTS_BOTTOMRIGHT, iRacePosition, eRacePositionHUDColour)
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_BIG_RACE_POSITION index = ")NET_PRINT_INT(idx)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC GET_HUD_HIGHLIGHTED_PLAYER_DATA(SPRITE_PLACEMENT &PlayerXPIconSprite, TEXT_LABEL_23 &sPlayerTextureDictName, TEXT_LABEL_23 &sPlayerTextureSpriteName, HUD_COLOURS &PlayerTextureHudColour, PLAYER_INDEX &pPlayerToCheckAgainst, PLAYER_INDEX &pPlayerToHighlight, HUD_COLOURS TitleColour)
	
	IF pPlayerToHighlight != INVALID_PLAYER_INDEX()
	AND pPlayerToCheckAgainst != INVALID_PLAYER_INDEX()
		IF pPlayerToHighlight = pPlayerToCheckAgainst
			sPlayerTextureDictName = "timerbar_sr"
			sPlayerTextureSpriteName = "timer_box"
			PlayerTextureHudColour = TitleColour
			
			PlayerXPIconSprite.w = 0.016+0.008
			PlayerXPIconSprite.h = 0.038
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(SPRITE_PLACEMENT &PlayerXPIconSprite, STRING sPlayerTextureDictName, STRING sPlayerTextureSpriteName, HUD_COLOURS PlayerTextureHudColour)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerTextureDictName)
		REQUEST_STREAMED_TEXTURE_DICT(sPlayerTextureDictName)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPlayerTextureDictName)
			SET_SPRITE_HUD_COLOUR(PlayerXPIconSprite, PlayerTextureHudColour)
			DRAW_2D_SPRITE(sPlayerTextureDictName, sPlayerTextureSpriteName, PlayerXPIconSprite)
		ELSE
			PRINTLN("[DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA] ", sPlayerTextureDictName, " dict is loading... ")
		ENDIF
	ENDIF
	
ENDPROC

PROC GET_HUD_POWERUP_DATA(ACTIVITY_POWERUP &aPowerup, SPRITE_PLACEMENT &PlayerXPIconSprite, TEXT_LABEL_23 &sPlayerTextureDictName, TEXT_LABEL_23 &sPlayerTextureSpriteName, HUD_COLOURS &PlayerTextureHudColour, PLAYER_INDEX &pPlayer, PEDHEADSHOT_ID &pPlayerHeadshotID, SPRITE_PLACEMENT &PlayerSecXPIconSprite, TEXT_LABEL_23 &sPlayerSecTextureDictName, TEXT_LABEL_23 &sPlayerSecTextureSpriteName, HUD_COLOURS &PlayerSecTextureHudColour, HUD_COLOURS TitleColour, HUD_COLOURS ePowerupColour = HUD_COLOUR_PURE_WHITE)
	
	SWITCH aPowerup
		CASE ACTIVITY_POWERUP_XP
			PlayerXPIconSprite.h += -0.009
			PlayerXPIconSprite.w += -0.002
			
			IF IS_LANGUAGE_NON_ROMANIC()
				PlayerXPIconSprite.y += 0.0055
			ELSE
				PlayerXPIconSprite.y += 0.0025
			ENDIF
			
			sPlayerTextureDictName = "MPRPSymbol"
			sPlayerTextureSpriteName = "RP"
		BREAK
		CASE ACTIVITY_POWERUP_ROCKETS
			sPlayerTextureDictName = "TimerBars"
			sPlayerTextureSpriteName = "Rockets"
		BREAK
		CASE ACTIVITY_POWERUP_HOMING_ROCKETS
			sPlayerTextureDictName = "MpSpecialRace"
			sPlayerTextureSpriteName = "HOMING_ROCKET"
		BREAK
		CASE ACTIVITY_POWERUP_SPIKES
			sPlayerTextureDictName = "TimerBars"
			sPlayerTextureSpriteName = "Spikes"
		BREAK	
		CASE ACTIVITY_POWERUP_BOOSTS
			sPlayerTextureDictName = "TimerBars"
			sPlayerTextureSpriteName = "Boost"	
		BREAK	
		CASE ACTIVITY_POWERUP_TICK
			sPlayerTextureDictName = "CrossTheLine"
			sPlayerTextureSpriteName = "Timer_LargeTick_32"	
		BREAK	
		CASE ACTIVITY_POWERUP_CROSS
			sPlayerTextureDictName = "CrossTheLine"
			sPlayerTextureSpriteName = "Timer_LargeCross_32"	
		BREAK	
		CASE ACTIVITY_POWERUP_BEAST
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Beast"
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY
		BREAK	
		CASE ACTIVITY_POWERUP_BULLET
			sPlayerTextureDictName = "MPSpecialRace"
			sPlayerTextureSpriteName = "MACHINE_GUN"	
		BREAK	
		CASE ACTIVITY_POWERUP_RANDOM
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Random"	
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY
		BREAK	
		CASE ACTIVITY_POWERUP_SLOW_TIME
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Slow_Time"	
		BREAK		
		CASE ACTIVITY_POWERUP_SWAP
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Swap"
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY	
		BREAK	
		CASE ACTIVITY_POWERUP_TESTOSTERONE
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Testosterone"
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY
		BREAK	
		CASE ACTIVITY_POWERUP_THERMAL
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Thermal"
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY
		BREAK	
		CASE ACTIVITY_POWERUP_WEED
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Weed"
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY
		BREAK
		CASE ACTIVITY_POWERUP_HIDDEN
			sPlayerTextureDictName = "TimerBar_Icons"
			sPlayerTextureSpriteName = "Pickup_Hidden"
			PlayerTextureHudColour = HUD_COLOUR_FRIENDLY
		BREAK	
		CASE ACTIVITY_POWERUP_PED_HEADSHOT
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_FADED
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
		CASE ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
		
			IF pPlayer != INVALID_PLAYER_INDEX()
				pPlayerHeadshotID = Get_HeadshotID_For_Player(pPlayer)
				IF pPlayerHeadshotID != NULL
					sPlayerTextureDictName = GET_PEDHEADSHOT_TXD_STRING(pPlayerHeadshotID)
					sPlayerTextureSpriteName = GET_PEDHEADSHOT_TXD_STRING(pPlayerHeadshotID)
				ENDIF
			ENDIF
			
			// Scale Ped Headshot
			PlayerXPIconSprite.w = 0.016+0.004
			PlayerXPIconSprite.h = 0.034
				
			// Dead Headshot Cross Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_cross"
				PlayerSecTextureHudColour = TitleColour
			ENDIF
			
			// Alive Headshot Tick Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_tick"
				PlayerSecTextureHudColour = TitleColour
			ENDIF
			
			// Zero Score Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_0"
				PlayerSecTextureHudColour = ePowerupColour
			ENDIF
			
			// One Score Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_1"
				PlayerSecTextureHudColour = ePowerupColour
			ENDIF
			
			// Two Score Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_2"
				PlayerSecTextureHudColour = ePowerupColour
			ENDIF
			
			// Three Score Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_3"
				PlayerSecTextureHudColour = ePowerupColour
			ENDIF
			
			// Four Score Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_4"
				PlayerSecTextureHudColour = ePowerupColour
			ENDIF
			
			// Five Score Overlay
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
				sPlayerSecTextureDictName = "timerbar_sr"
				sPlayerSecTextureSpriteName = "timer_5"
				PlayerSecTextureHudColour = ePowerupColour
			ENDIF
			
			// Faded Headshot
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_FADED
				PlayerSecTextureHudColour = TitleColour
			ENDIF
			
			// Dead headshot half alpha
			IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE
			OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_FADED
				PlayerXPIconSprite.a = 127
			ENDIF
		BREAK
		CASE ACTIVITY_POWERUP_NONE
			PlayerXPIconSprite.a		= 0
			PlayerSecXPIconSprite.a 	= 0
		BREAK
	ENDSWITCH
	
ENDPROC

PROC DRAW_2D_SPRITE_FROM_POWERUP_DATA(ACTIVITY_POWERUP aPowerup, SPRITE_PLACEMENT &PlayerXPIconSprite, SPRITE_PLACEMENT &PlayerSecXPIconSprite, STRING sPlayerTextureDictName, STRING sPlayerTextureSpriteName, HUD_COLOURS PlayerTextureHudColour, STRING sPlayerSecTextureDictName, STRING sPlayerSecTextureSpriteName, HUD_COLOURS PlayerSecTextureHudColour)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerTextureDictName)
		REQUEST_STREAMED_TEXTURE_DICT(sPlayerTextureDictName)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPlayerTextureDictName)
			SET_SPRITE_HUD_COLOUR(PlayerXPIconSprite, PlayerTextureHudColour)
			DRAW_2D_SPRITE(sPlayerTextureDictName, sPlayerTextureSpriteName, PlayerXPIconSprite)
		ELSE
			PRINTLN("[DRAW_2D_SPRITE_FROM_POWERUP_DATA] Icon - ", aPowerup, " - ", sPlayerTextureDictName, " dict is loading... ")
		ENDIF
	ENDIF
	
	// 60% Alpha black square infront of headshot
	IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_BLACK, iR, iG, iB, iA)
		DRAW_RECT(PlayerXPIconSprite.x, PlayerXPIconSprite.y, PlayerXPIconSprite.w, PlayerXPIconSprite.h, iR, iG, iB, 153)
	ENDIF
	
	IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_FADED
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
	
		IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerSecTextureDictName)
			REQUEST_STREAMED_TEXTURE_DICT(sPlayerSecTextureDictName)
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPlayerSecTextureDictName)
				SET_SPRITE_HUD_COLOUR(PlayerSecXPIconSprite, PlayerSecTextureHudColour)
				DRAW_2D_SPRITE(sPlayerSecTextureDictName, sPlayerSecTextureSpriteName, PlayerSecXPIconSprite)
			ELSE
				PRINTLN("[DRAW_2D_SPRITE_FROM_POWERUP_DATA] Icon Sec - ", aPowerup, " - ", sPlayerSecTextureDictName, " dict is loading... ")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL REQUEST_AND_LOAD_TEXTURE_DICTS(STRING sPlayerTextureDictName, STRING sPlayerSecTextureDictName)
	BOOL bPlayerTextureDictLoaded
	BOOL bSecPlayerTextureDictLoaded
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerTextureDictName)
		REQUEST_STREAMED_TEXTURE_DICT(sPlayerTextureDictName)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPlayerTextureDictName)
			PRINTLN("[REQUEST_AND_LOAD_TEXTURE_DICTS] ", sPlayerTextureDictName, " dict has loaded ")
			bPlayerTextureDictLoaded = TRUE
		ELSE
			PRINTLN("[REQUEST_AND_LOAD_TEXTURE_DICTS] ", sPlayerTextureDictName, " dict is loading... ")
		ENDIF
	ELSE
		bPlayerTextureDictLoaded = TRUE
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerSecTextureDictName)
		REQUEST_STREAMED_TEXTURE_DICT(sPlayerSecTextureDictName)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPlayerSecTextureDictName)
			PRINTLN("[REQUEST_AND_LOAD_TEXTURE_DICTS] ", sPlayerSecTextureDictName, " dict has loaded ")
			bSecPlayerTextureDictLoaded = TRUE
		ELSE
			PRINTLN("[REQUEST_AND_LOAD_TEXTURE_DICTS] (Sec) ", sPlayerSecTextureDictName, " dict is loading... ")
		ENDIF
	ELSE
		bSecPlayerTextureDictLoaded = TRUE
	ENDIF
	
	IF bPlayerTextureDictLoaded 
	AND bSecPlayerTextureDictLoaded
		PRINTLN("[REQUEST_AND_LOAD_TEXTURE_DICTS] ", sPlayerTextureDictName, " dict has loaded & ", sPlayerSecTextureDictName, " dict has loaded. Returning TRUE.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REQUEST_AND_LOAD_ALL_FIVE_TEXTURE_DICTIONARIES(STRING tlPlayerOneTextureDictName, 
														STRING tlPlayerOneSecTextureDictName,	
														STRING tlPlayerTwoTextureDictName, 
														STRING tlPlayerTwoSecTextureDictName,	
														STRING tlPlayerThreeTextureDictName, 
														STRING tlPlayerThreeSecTextureDictName,	
														STRING tlPlayerFourTextureDictName, 
														STRING tlPlayerFourSecTextureDictName,	
														STRING tlPlayerFiveTextureDictName, 
														STRING tlPlayerFiveSecTextureDictName)

	BOOL complete = TRUE
	IF REQUEST_AND_LOAD_TEXTURE_DICTS(tlPlayerOneTextureDictName, 	 tlPlayerOneSecTextureDictName) = FALSE
		complete = FALSE
	ENDIF
	IF REQUEST_AND_LOAD_TEXTURE_DICTS(tlPlayerTwoTextureDictName, 	 tlPlayerTwoSecTextureDictName) = FALSE
		complete = FALSE
	ENDIF
	IF REQUEST_AND_LOAD_TEXTURE_DICTS(tlPlayerThreeTextureDictName, tlPlayerThreeSecTextureDictName) = FALSE
		complete = FALSE
	ENDIF
	IF REQUEST_AND_LOAD_TEXTURE_DICTS(tlPlayerFourTextureDictName,  tlPlayerFourSecTextureDictName) = FALSE
		complete = FALSE
	ENDIF
	IF REQUEST_AND_LOAD_TEXTURE_DICTS(tlPlayerFiveTextureDictName,  tlPlayerFiveSecTextureDictName) = FALSE
		complete = FALSE
	ENDIF
	
	RETURN complete
	

ENDFUNC

PROC DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(ACTIVITY_POWERUP aPowerup, SPRITE_PLACEMENT &PlayerXPIconSprite, SPRITE_PLACEMENT &PlayerSecXPIconSprite, STRING sPlayerTextureDictName, STRING sPlayerTextureSpriteName, HUD_COLOURS PlayerTextureHudColour, STRING sPlayerSecTextureDictName, STRING sPlayerSecTextureSpriteName, HUD_COLOURS PlayerSecTextureHudColour)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerTextureDictName)
		SET_SPRITE_HUD_COLOUR(PlayerXPIconSprite, PlayerTextureHudColour)
		DRAW_2D_SPRITE(sPlayerTextureDictName, sPlayerTextureSpriteName, PlayerXPIconSprite)
	ENDIF
	
	// 60% Alpha black square infront of headshot
	IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_BLACK, iR, iG, iB, iA)
		DRAW_RECT(PlayerXPIconSprite.x, PlayerXPIconSprite.y, PlayerXPIconSprite.w, PlayerXPIconSprite.h, iR, iG, iB, 153)
	ENDIF
	
	IF aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_FADED
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
	OR aPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
		IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerSecTextureDictName)
			SET_SPRITE_HUD_COLOUR(PlayerSecXPIconSprite, PlayerSecTextureHudColour)
			DRAW_2D_SPRITE(sPlayerSecTextureDictName, sPlayerSecTextureSpriteName, PlayerSecXPIconSprite)
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_GENERAL_FOUR_ICON_BAR_GUTS(INT index, TEXT_STYLE &TitleStyle, TEXT_STYLE &NumberStyle, TEXT_PLACEMENT &TitlePlace, TEXT_PLACEMENT &NumberPlace, HUD_COLOURS TitleColour, UIELEMENTS WhichSpace, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, BOOL bFlashIconOne = FALSE, BOOL bFlashIconTwo = FALSE, BOOL bFlashIconThree = FALSE, BOOL bFlashIconFour = FALSE, INT iFlashTime = -1)
	MPGlobalsScoreHud.iHowManyDisplays++
	
	IF IS_BOTTOM_RIGHT_AREA_FREE()
	
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_FOUR_ICON_BAR, index)
		
		SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		SET_STANDARD_UI_SCORE_NUMBER(NumberStyle, DROPSTYLE_NONE)
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
		
		INIT_SCREEN_GENERAL_SINGLE_SCORE(NumberStyle, TitlePlace, NumberPlace, WhichSpace, TitleStyle)
		
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		
		// Flashing Icons
		BOOL bDisplayFlashingIcons
		
		IF iFlashTime = 0
			RESET_GENERIC_FOUR_ICON_BAR_FLASHING(index)
		ENDIF
		
		IF DO_FLASHING(iFlashTime, MPGlobalsScoreHud.iFlashing_GenericFourIconBar_Hud[index],MPGlobalsScoreHud.iFlashing_GenericFourIconBar_MiniHud[index])
			bDisplayFlashingIcons = TRUE
		ELSE
			bDisplayFlashingIcons = FALSE
		ENDIF
		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
			
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			// Background Bar
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			Overlays.x += TIMER_OVERLAY_X
			Overlays.y += TIMER_OVERLAY_Y_SCORE
			Overlays.w += TIMER_OVERLAY_W
			Overlays.h += TIMER_OVERLAY_H_SCORE
			Overlays.r += 255
			Overlays.g += 255
			Overlays.b += 255
			Overlays.a = TIMER_OVERLAY_ALPHA
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg", Overlays)
			SET_TEXT_STYLE(TitleStyle)
			
			//Icon Setup
			SPRITE_PLACEMENT PlayerOneXPIconSprite
			SPRITE_PLACEMENT PlayerOneSecXPIconSprite

			SPRITE_PLACEMENT PlayerTwoXPIconSprite
			SPRITE_PLACEMENT PlayerTwoSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerThreeXPIconSprite
			SPRITE_PLACEMENT PlayerThreeSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerFourXPIconSprite
			SPRITE_PLACEMENT PlayerFourSecXPIconSprite
			
			// Underlay sprite
			PlayerOneXPIconSprite.x 		= NumberPlace.x+0.145+0.001
			PlayerOneSecXPIconSprite.x 		= NumberPlace.x+0.145+0.001
			
			PlayerTwoXPIconSprite.x 		= NumberPlace.x+0.123
			PlayerTwoSecXPIconSprite.x 		= NumberPlace.x+0.123
			
			PlayerThreeXPIconSprite.x 		= NumberPlace.x+0.101
			PlayerThreeSecXPIconSprite.x 	= NumberPlace.x+0.101
			
			PlayerFourXPIconSprite.x 		= NumberPlace.x+0.078
			PlayerFourSecXPIconSprite.x 	= NumberPlace.x+0.078
			
			IF IS_LANGUAGE_NON_ROMANIC()
				PlayerOneXPIconSprite.y 		= NumberPlace.y+0.016-0.000
				PlayerOneSecXPIconSprite.y 		= NumberPlace.y+0.016+0.0005
				
				PlayerTwoXPIconSprite.y 		= NumberPlace.y+0.016-0.000
				PlayerTwoSecXPIconSprite.y 		= NumberPlace.y+0.016+0.0005
				
				PlayerThreeXPIconSprite.y 		= NumberPlace.y+0.016-0.000
				PlayerThreeSecXPIconSprite.y 	= NumberPlace.y+0.016+0.0005
				
				PlayerFourXPIconSprite.y 		= NumberPlace.y+0.016-0.000
				PlayerFourSecXPIconSprite.y 	= NumberPlace.y+0.016+0.0005
			ELSE
				PlayerOneXPIconSprite.y 		= NumberPlace.y+0.0185
				PlayerOneSecXPIconSprite.y 		= NumberPlace.y+0.019
				
				PlayerTwoXPIconSprite.y 		= NumberPlace.y+0.0185
				PlayerTwoSecXPIconSprite.y 		= NumberPlace.y+0.019
				
				PlayerThreeXPIconSprite.y 		= NumberPlace.y+0.0185
				PlayerThreeSecXPIconSprite.y 	= NumberPlace.y+0.019
				
				PlayerFourXPIconSprite.y 		= NumberPlace.y+0.0185
				PlayerFourSecXPIconSprite.y 	= NumberPlace.y+0.019
			ENDIF
			
			// Width
			PlayerOneXPIconSprite.w 			= 0.016+0.003
			PlayerOneSecXPIconSprite.w 			= 0.016+0.003
			
			PlayerTwoXPIconSprite.w 			= 0.016+0.003
			PlayerTwoSecXPIconSprite.w 			= 0.016+0.003
			
			PlayerThreeXPIconSprite.w 			= 0.016+0.003
			PlayerThreeSecXPIconSprite.w		= 0.016+0.003
			
			PlayerFourXPIconSprite.w 			= 0.016+0.003
			PlayerFourSecXPIconSprite.w 		= 0.016+0.003
			
			// Height
			PlayerOneXPIconSprite.h 			= 0.032+0.004
			PlayerOneSecXPIconSprite.h 			= 0.032+0.004
			
			PlayerTwoXPIconSprite.h 			= 0.032+0.004
			PlayerTwoSecXPIconSprite.h			= 0.032+0.004
			
			PlayerThreeXPIconSprite.h 			= 0.032+0.004
			PlayerThreeSecXPIconSprite.h 		= 0.032+0.004
			
			PlayerFourXPIconSprite.h 			= 0.032+0.004
			PlayerFourSecXPIconSprite.h 		= 0.032+0.004
			
			//Alpha
			PlayerOneXPIconSprite.a 			= 255
			PlayerOneSecXPIconSprite.a 			= 255
			
			PlayerTwoXPIconSprite.a				= 255
			PlayerTwoSecXPIconSprite.a 			= 255
			
			PlayerThreeXPIconSprite.a 			= 255
			PlayerThreeSecXPIconSprite.a 		= 255
			
			PlayerFourXPIconSprite.a 			= 255
			PlayerFourSecXPIconSprite.a 		= 255
			
			// HUD Colour
			SET_SPRITE_HUD_COLOUR(PlayerOneXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerOneSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerTwoXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerTwoSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerThreeXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerThreeSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerFourXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerFourSecXPIconSprite, HUD_COLOUR_WHITE)
			
			// Ped Headshot Vars
			PEDHEADSHOT_ID pPlayerOneHeadshotID
			PEDHEADSHOT_ID pPlayerTwoHeadshotID
			PEDHEADSHOT_ID pPlayerThreeHeadshotID
			PEDHEADSHOT_ID pPlayerFourHeadshotID
			
			// TXD Name Vars
			TEXT_LABEL_23 tlPlayerOneTextureDictName = ""
			TEXT_LABEL_23 tlPlayerOneSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerTwoTextureDictName = ""
			TEXT_LABEL_23 tlPlayerTwoSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerThreeTextureDictName = ""
			TEXT_LABEL_23 tlPlayerThreeSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerFourTextureDictName = ""
			TEXT_LABEL_23 tlPlayerFourSecTextureDictName = ""
			
			// Sprite Name Vars
			TEXT_LABEL_23 tlPlayerOneTextureSpriteName
			TEXT_LABEL_23 tlPlayerOneSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerTwoTextureSpriteName
			TEXT_LABEL_23 tlPlayerTwoSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerThreeTextureSpriteName
			TEXT_LABEL_23 tlPlayerThreeSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerFourTextureSpriteName
			TEXT_LABEL_23 tlPlayerFourSecTextureSpriteName
			
			// Texture HUD Colour Vars
			HUD_COLOURS PlayerOneTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerOneSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerTwoTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerTwoSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerThreeTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerThreeSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerFourTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerFourSecTextureHudColour = HUD_COLOUR_WHITE
			
			GET_HUD_POWERUP_DATA(aPowerupOne,   PlayerOneXPIconSprite,   	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 	 PlayerOneTextureHudColour, 	pPlayerOne, 	pPlayerOneHeadshotID, 	PlayerOneSecXPIconSprite, 	tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour, 	TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupTwo,   PlayerTwoXPIconSprite,   	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName,    PlayerTwoTextureHudColour, 	pPlayerTwo, 	pPlayerTwoHeadshotID, 	PlayerTwoSecXPIconSprite, 	tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour, 	TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName,  PlayerThreeTextureHudColour, 	pPlayerThree, 	pPlayerThreeHeadshotID, PlayerThreeSecXPIconSprite, tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour, TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupFour,  PlayerFourXPIconSprite,  	tlPlayerFourTextureDictName, 	tlPlayerFourTextureSpriteName, 	 PlayerFourTextureHudColour, 	pPlayerFour, 	pPlayerFourHeadshotID, 	PlayerFourSecXPIconSprite, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour, 	TitleColour)
			
			IF bFlashIconOne
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
			ENDIF
			
			IF bFlashIconTwo
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
			ENDIF
			
			IF bFlashIconThree
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
			ENDIF
			
			IF bFlashIconFour
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
			ENDIF
			
		ENDIF
		
		RESET_GFX_TIMERS_DRAW_ORDER()
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_FOUR_ICON_BAR(INT idx, HUD_COLOURS TitleColour, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, BOOL bFlashIconOne = FALSE, BOOL bFlashIconTwo = FALSE, BOOL bFlashIconThree = FALSE, BOOL bFlashIconFour = FALSE, INT iFlashTime = -1)

	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FOUR_ICON_BAR, idx)
		
		TEXT_STYLE TitleStyle, NumberStyle
		TEXT_PLACEMENT TitlePlacement, NumberPlace
		
		DRAW_GENERAL_FOUR_ICON_BAR_GUTS(idx, TitleStyle, NumberStyle, TitlePlacement, NumberPlace, TitleColour, UIELEMENTS_BOTTOMRIGHT, pPlayerOne, pPlayerTwo, pPlayerThree, pPlayerFour, aPowerupOne, aPowerupTwo, aPowerupThree, aPowerupFour, bFlashIconOne, bFlashIconTwo, bFlashIconThree, bFlashIconFour, iFlashTime)
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_FOUR_ICON_BAR index = ")NET_PRINT_INT(idx)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC


PROC PRELOAD_FIVE_ICON_SCORE_BAR(PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL,
								ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE)
	
	
	
	
	// Ped Headshot Vars
	PEDHEADSHOT_ID pPlayerOneHeadshotID
	PEDHEADSHOT_ID pPlayerTwoHeadshotID
	PEDHEADSHOT_ID pPlayerThreeHeadshotID
	PEDHEADSHOT_ID pPlayerFourHeadshotID
	PEDHEADSHOT_ID pPlayerFiveHeadshotID
	
	// TXD Name Vars
	TEXT_LABEL_23 tlPlayerOneTextureDictName 			= ""
	TEXT_LABEL_23 tlPlayerOneSecTextureDictName 		= ""
	
	TEXT_LABEL_23 tlPlayerTwoTextureDictName 			= ""
	TEXT_LABEL_23 tlPlayerTwoSecTextureDictName 		= ""
	
	TEXT_LABEL_23 tlPlayerThreeTextureDictName			= ""
	TEXT_LABEL_23 tlPlayerThreeSecTextureDictName		= ""
	
	TEXT_LABEL_23 tlPlayerFourTextureDictName 			= ""
	TEXT_LABEL_23 tlPlayerFourSecTextureDictName 		= ""
	
	TEXT_LABEL_23 tlPlayerFiveTextureDictName 			= ""
	TEXT_LABEL_23 tlPlayerFiveSecTextureDictName 		= ""
	
	// Sprite Name Vars
	TEXT_LABEL_23 tlPlayerOneTextureSpriteName			= ""
	TEXT_LABEL_23 tlPlayerOneSecTextureSpriteName		= ""
	
	TEXT_LABEL_23 tlPlayerTwoTextureSpriteName			= ""
	TEXT_LABEL_23 tlPlayerTwoSecTextureSpriteName		= ""
	
	TEXT_LABEL_23 tlPlayerThreeTextureSpriteName		= ""
	TEXT_LABEL_23 tlPlayerThreeSecTextureSpriteName		= ""
	
	TEXT_LABEL_23 tlPlayerFourTextureSpriteName			= ""
	TEXT_LABEL_23 tlPlayerFourSecTextureSpriteName		= ""
	
	TEXT_LABEL_23 tlPlayerFiveTextureSpriteName			= ""
	TEXT_LABEL_23 tlPlayerFiveSecTextureSpriteName		= ""
	
	// Texture HUD Colour Vars
	HUD_COLOURS PlayerOneTextureHudColour 			= HUD_COLOUR_WHITE
	
	HUD_COLOURS PlayerTwoTextureHudColour 			= HUD_COLOUR_WHITE
	
	HUD_COLOURS PlayerThreeTextureHudColour 		= HUD_COLOUR_WHITE
	
	HUD_COLOURS PlayerFourTextureHudColour 			= HUD_COLOUR_WHITE
	
	HUD_COLOURS PlayerFiveTextureHudColour 			= HUD_COLOUR_WHITE
	
	//Icon Setup
	SPRITE_PLACEMENT PlayerOneXPIconSprite
	SPRITE_PLACEMENT PlayerOneSecXPIconSprite
	
	SPRITE_PLACEMENT PlayerTwoXPIconSprite
	SPRITE_PLACEMENT PlayerTwoSecXPIconSprite
	
	SPRITE_PLACEMENT PlayerThreeXPIconSprite
	SPRITE_PLACEMENT PlayerThreeSecXPIconSprite
	
	SPRITE_PLACEMENT PlayerFourXPIconSprite
	SPRITE_PLACEMENT PlayerFourSecXPIconSprite
	
	SPRITE_PLACEMENT PlayerFiveXPIconSprite
	SPRITE_PLACEMENT PlayerFiveSecXPIconSprite
	
	GET_HUD_POWERUP_DATA(aPowerupOne,   PlayerOneXPIconSprite,   	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 	 PlayerOneTextureHudColour, 	pPlayerOne, 	pPlayerOneHeadshotID, 	PlayerOneSecXPIconSprite, 	tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE)
	GET_HUD_POWERUP_DATA(aPowerupTwo,   PlayerTwoXPIconSprite,   	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName,    PlayerTwoTextureHudColour, 	pPlayerTwo, 	pPlayerTwoHeadshotID, 	PlayerTwoSecXPIconSprite, 	tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE)
	GET_HUD_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName,  PlayerThreeTextureHudColour, 	pPlayerThree, 	pPlayerThreeHeadshotID, PlayerThreeSecXPIconSprite, tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	HUD_COLOUR_WHITE, HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE)
	GET_HUD_POWERUP_DATA(aPowerupFour,  PlayerFourXPIconSprite,  	tlPlayerFourTextureDictName, 	tlPlayerFourTextureSpriteName, 	 PlayerFourTextureHudColour, 	pPlayerFour, 	pPlayerFourHeadshotID, 	PlayerFourSecXPIconSprite, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE)
	GET_HUD_POWERUP_DATA(aPowerupFive,  PlayerFiveXPIconSprite,		tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 	 PlayerFiveTextureHudColour, 	pPlayerFive, 	pPlayerFiveHeadshotID, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE, 	HUD_COLOUR_WHITE)
	

	
	REQUEST_AND_LOAD_ALL_FIVE_TEXTURE_DICTIONARIES(tlPlayerOneTextureDictName, tlPlayerOneSecTextureDictName,
															tlPlayerTwoTextureDictName, tlPlayerTwoSecTextureDictName,
															tlPlayerThreeTextureDictName, tlPlayerThreeSecTextureDictName,
															tlPlayerFourTextureDictName,  tlPlayerFourSecTextureDictName,
															tlPlayerFiveTextureDictName,  tlPlayerFiveSecTextureDictName)
	
	REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
	
ENDPROC


PROC DRAW_GENERAL_FIVE_ICON_SCORE_BAR_GUTS(INT index, TEXT_STYLE &TitleStyle, TEXT_STYLE &NumberStyle, TEXT_PLACEMENT &TitlePlace, TEXT_PLACEMENT &NumberPlace, INT Number, FLOAT FloatValue, STRING NumberString, BOOL isFloat, INT MaXNumber, BOOL DrawInfinity, HUD_COLOURS TitleColour, UIELEMENTS WhichSpace, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE, PLAYER_INDEX pPlayerToHighlight = NULL, BOOL bEnablePlayerHighlight = FALSE, HUD_COLOURS ePowerupOneColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupTwoColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupThreeColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupFourColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupFiveColour = HUD_COLOUR_PURE_WHITE, INT iInstanceToHighlight = 0, BOOL bPulseHighlight = FALSE, INT iPulseTime = 9999999, PLAYER_INDEX pAvatarToFlash = NULL, BOOL bFlashAvatar = FALSE, INT iAvatarFlashTime = 9999999, INT iAvatarSlotToFlash = 0)
	MPGlobalsScoreHud.iHowManyDisplays++
	
	IF IS_BOTTOM_RIGHT_AREA_FREE()
				
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0,0,0,255)
		
		SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_DROPSHADOWONLY)
		
		IF Number < 1000000
		
			IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
			AND (FloatValue >= 100)
			AND ARE_STRINGS_EQUAL("AMCH_KMHN", NumberString) 
				SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(NumberStyle, DROPSTYLE_NONE)
				
			ELIF (GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
			OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
			 ) 
			AND (Number > 999 OR MaXNumber > 999 OR FloatValue > 1000)
				SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(NumberStyle, DROPSTYLE_NONE)
			ELSE
				IF MaXNumber > 99
					SET_STANDARD_UI_SCORE_SMALL_NUMBER(NumberStyle, DROPSTYLE_NONE)
				ELSE
					SET_STANDARD_UI_SCORE_NUMBER(NumberStyle, DROPSTYLE_NONE)
				ENDIF
			ENDIF
		ELSE	
			IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
			AND ( ARE_STRINGS_EQUAL("HUD_CASH", NumberString)
			OR ARE_STRINGS_EQUAL("HUD_CASH_NEG", NumberString))
			
				SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(NumberStyle, DROPSTYLE_NONE)
			ELSE
				SET_STANDARD_UI_SCORE_SMALL_NUMBER(NumberStyle, DROPSTYLE_NONE)
			ENDIF
		ENDIF
		
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_FIVE_ICON_SCORE_BAR, Index) 
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		INIT_SCREEN_GENERAL_SINGLE_SCORE(NumberStyle, TitlePlace, NumberPlace, WhichSpace, TitleStyle)
		SET_WORD_HUD_COLOUR(NumberStyle, TitleColour)
		NumberStyle.a = 255
		
		
		
		
		
		
		//Icon Setup
		SPRITE_PLACEMENT PlayerOneXPIconSprite
		SPRITE_PLACEMENT PlayerOneSecXPIconSprite
		SPRITE_PLACEMENT PlayerOneThirdXPIconSprite
		
		SPRITE_PLACEMENT PlayerTwoXPIconSprite
		SPRITE_PLACEMENT PlayerTwoSecXPIconSprite
		SPRITE_PLACEMENT PlayerTwoThirdXPIconSprite
		
		SPRITE_PLACEMENT PlayerThreeXPIconSprite
		SPRITE_PLACEMENT PlayerThreeSecXPIconSprite
		SPRITE_PLACEMENT PlayerThreeThirdXPIconSprite
		
		SPRITE_PLACEMENT PlayerFourXPIconSprite
		SPRITE_PLACEMENT PlayerFourSecXPIconSprite
		SPRITE_PLACEMENT PlayerFourThirdXPIconSprite
		
		SPRITE_PLACEMENT PlayerFiveXPIconSprite
		SPRITE_PLACEMENT PlayerFiveSecXPIconSprite
		SPRITE_PLACEMENT PlayerFiveThirdXPIconSprite
		
		// Underlay sprite
		PlayerOneXPIconSprite.x 		= NumberPlace.x+0.095
		PlayerOneSecXPIconSprite.x 		= NumberPlace.x+0.095
		PlayerOneThirdXPIconSprite.x	= NumberPlace.x+0.095
		
		PlayerTwoXPIconSprite.x 		= NumberPlace.x+0.072
		PlayerTwoSecXPIconSprite.x 		= NumberPlace.x+0.072
		PlayerTwoThirdXPIconSprite.x	= NumberPlace.x+0.072
		
		PlayerThreeXPIconSprite.x 		= NumberPlace.x+0.049
		PlayerThreeSecXPIconSprite.x 	= NumberPlace.x+0.049
		PlayerThreeThirdXPIconSprite.x	= NumberPlace.x+0.049
		
		PlayerFourXPIconSprite.x 		= NumberPlace.x+0.026
		PlayerFourSecXPIconSprite.x 	= NumberPlace.x+0.026
		PlayerFourThirdXPIconSprite.x	= NumberPlace.x+0.026
		
		PlayerFiveXPIconSprite.x 		= NumberPlace.x+0.003
		PlayerFiveSecXPIconSprite.x 	= NumberPlace.x+0.003
		PlayerFiveThirdXPIconSprite.x	= NumberPlace.x+0.003
		
		IF IS_LANGUAGE_NON_ROMANIC()
			PlayerOneXPIconSprite.y 		= NumberPlace.y+0.016-0.0005
			PlayerOneSecXPIconSprite.y 		= NumberPlace.y+0.016
			PlayerOneThirdXPIconSprite.y	= NumberPlace.y+0.016
			
			PlayerTwoXPIconSprite.y 		= NumberPlace.y+0.016-0.0005
			PlayerTwoSecXPIconSprite.y 		= NumberPlace.y+0.016
			PlayerTwoThirdXPIconSprite.y	= NumberPlace.y+0.016
			
			PlayerThreeXPIconSprite.y 		= NumberPlace.y+0.016-0.0005
			PlayerThreeSecXPIconSprite.y 	= NumberPlace.y+0.016
			PlayerThreeThirdXPIconSprite.y	= NumberPlace.y+0.016
			
			PlayerFourXPIconSprite.y 		= NumberPlace.y+0.016-0.0005
			PlayerFourSecXPIconSprite.y 	= NumberPlace.y+0.016
			PlayerFourThirdXPIconSprite.y	= NumberPlace.y+0.016
			
			PlayerFiveXPIconSprite.y 		= NumberPlace.y+0.016-0.0005
			PlayerFiveSecXPIconSprite.y 	= NumberPlace.y+0.016
			PlayerFiveThirdXPIconSprite.y	= NumberPlace.y+0.016
		ELSE
			PlayerOneXPIconSprite.y 		= NumberPlace.y+0.0185
			PlayerOneSecXPIconSprite.y 		= NumberPlace.y+0.019
			PlayerOneThirdXPIconSprite.y	= NumberPlace.y+0.019
			
			PlayerTwoXPIconSprite.y 		= NumberPlace.y+0.0185
			PlayerTwoSecXPIconSprite.y 		= NumberPlace.y+0.019
			PlayerTwoThirdXPIconSprite.y	= NumberPlace.y+0.019
			
			PlayerThreeXPIconSprite.y 		= NumberPlace.y+0.0185
			PlayerThreeSecXPIconSprite.y 	= NumberPlace.y+0.019
			PlayerThreeThirdXPIconSprite.y	= NumberPlace.y+0.019
			
			PlayerFourXPIconSprite.y 		= NumberPlace.y+0.0185
			PlayerFourSecXPIconSprite.y 	= NumberPlace.y+0.019
			PlayerFourThirdXPIconSprite.y	= NumberPlace.y+0.019
			
			PlayerFiveXPIconSprite.y 		= NumberPlace.y+0.0185
			PlayerFiveSecXPIconSprite.y 	= NumberPlace.y+0.019
			PlayerFiveThirdXPIconSprite.y	= NumberPlace.y+0.019
		ENDIF
		
		// Width
		PlayerOneXPIconSprite.w 			= 0.016+0.003
		PlayerOneSecXPIconSprite.w 			= 0.016+0.003
		PlayerOneThirdXPIconSprite.w 		= 0.016+0.003
		
		PlayerTwoXPIconSprite.w 			= 0.016+0.003
		PlayerTwoSecXPIconSprite.w 			= 0.016+0.003
		PlayerTwoThirdXPIconSprite.w 		= 0.016+0.003
		
		PlayerThreeXPIconSprite.w 			= 0.016+0.003
		PlayerThreeSecXPIconSprite.w		= 0.016+0.003
		PlayerThreeThirdXPIconSprite.w 		= 0.016+0.003
		
		PlayerFourXPIconSprite.w 			= 0.016+0.003
		PlayerFourSecXPIconSprite.w 		= 0.016+0.003
		PlayerFourThirdXPIconSprite.w 		= 0.016+0.003
		
		PlayerFiveXPIconSprite.w 			= 0.016+0.003
		PlayerFiveSecXPIconSprite.w 		= 0.016+0.003
		PlayerFiveThirdXPIconSprite.w 		= 0.016+0.003
		
		// Height
		PlayerOneXPIconSprite.h 			= 0.032+0.004
		PlayerOneSecXPIconSprite.h 			= 0.032+0.004
		PlayerOneThirdXPIconSprite.h		= 0.032+0.004
		
		PlayerTwoXPIconSprite.h 			= 0.032+0.004
		PlayerTwoSecXPIconSprite.h			= 0.032+0.004
		PlayerTwoThirdXPIconSprite.h		= 0.032+0.004
		
		PlayerThreeXPIconSprite.h 			= 0.032+0.004
		PlayerThreeSecXPIconSprite.h 		= 0.032+0.004
		PlayerThreeThirdXPIconSprite.h		= 0.032+0.004
		
		PlayerFourXPIconSprite.h 			= 0.032+0.004
		PlayerFourSecXPIconSprite.h 		= 0.032+0.004
		PlayerFourThirdXPIconSprite.h		= 0.032+0.004
		
		PlayerFiveXPIconSprite.h 			= 0.032+0.004
		PlayerFiveSecXPIconSprite.h 		= 0.032+0.004
		PlayerFiveThirdXPIconSprite.h		= 0.032+0.004
		
		//Alpha
		PlayerOneXPIconSprite.a 			= 255
		PlayerOneSecXPIconSprite.a 			= 255
		PlayerOneThirdXPIconSprite.a		= 255
		
		PlayerTwoXPIconSprite.a				= 255
		PlayerTwoSecXPIconSprite.a 			= 255
		PlayerTwoThirdXPIconSprite.a		= 255
		
		PlayerThreeXPIconSprite.a 			= 255
		PlayerThreeSecXPIconSprite.a 		= 255
		PlayerThreeThirdXPIconSprite.a		= 255
		
		PlayerFourXPIconSprite.a 			= 255
		PlayerFourSecXPIconSprite.a 		= 255
		PlayerFourThirdXPIconSprite.a		= 255
		
		PlayerFiveXPIconSprite.a 			= 255
		PlayerFiveSecXPIconSprite.a 		= 255
		PlayerFiveThirdXPIconSprite.a		= 255
		
		// HUD Colour
		SET_SPRITE_HUD_COLOUR(PlayerOneXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerOneSecXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerOneThirdXPIconSprite, HUD_COLOUR_WHITE)
		
		SET_SPRITE_HUD_COLOUR(PlayerTwoXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerTwoSecXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerTwoThirdXPIconSprite, HUD_COLOUR_WHITE)
		
		SET_SPRITE_HUD_COLOUR(PlayerThreeXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerThreeSecXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerThreeThirdXPIconSprite, HUD_COLOUR_WHITE)
		
		SET_SPRITE_HUD_COLOUR(PlayerFourXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerFourSecXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerFourThirdXPIconSprite, HUD_COLOUR_WHITE)
		
		SET_SPRITE_HUD_COLOUR(PlayerFiveXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerFiveSecXPIconSprite, HUD_COLOUR_WHITE)
		SET_SPRITE_HUD_COLOUR(PlayerFiveThirdXPIconSprite, HUD_COLOUR_WHITE)
		
	
		// Ped Headshot Vars
		PEDHEADSHOT_ID pPlayerOneHeadshotID
		PEDHEADSHOT_ID pPlayerTwoHeadshotID
		PEDHEADSHOT_ID pPlayerThreeHeadshotID
		PEDHEADSHOT_ID pPlayerFourHeadshotID
		PEDHEADSHOT_ID pPlayerFiveHeadshotID
		
		// TXD Name Vars
		TEXT_LABEL_23 tlPlayerOneTextureDictName 			= ""
		TEXT_LABEL_23 tlPlayerOneSecTextureDictName 		= ""
		TEXT_LABEL_23 tlPlayerOneThirdTextureDictName 		= ""
		
		TEXT_LABEL_23 tlPlayerTwoTextureDictName 			= ""
		TEXT_LABEL_23 tlPlayerTwoSecTextureDictName 		= ""
		TEXT_LABEL_23 tlPlayerTwoThirdTextureDictName 		= ""
		
		TEXT_LABEL_23 tlPlayerThreeTextureDictName			= ""
		TEXT_LABEL_23 tlPlayerThreeSecTextureDictName		= ""
		TEXT_LABEL_23 tlPlayerThreeThirdTextureDictName 	= ""
		
		TEXT_LABEL_23 tlPlayerFourTextureDictName 			= ""
		TEXT_LABEL_23 tlPlayerFourSecTextureDictName 		= ""
		TEXT_LABEL_23 tlPlayerFourThirdTextureDictName 		= ""
		
		TEXT_LABEL_23 tlPlayerFiveTextureDictName 			= ""
		TEXT_LABEL_23 tlPlayerFiveSecTextureDictName 		= ""
		TEXT_LABEL_23 tlPlayerFiveThirdTextureDictName 		= ""
		
		// Sprite Name Vars
		TEXT_LABEL_23 tlPlayerOneTextureSpriteName			= ""
		TEXT_LABEL_23 tlPlayerOneSecTextureSpriteName		= ""
		TEXT_LABEL_23 tlPlayerOneThirdTextureSpriteName		= ""
		
		TEXT_LABEL_23 tlPlayerTwoTextureSpriteName			= ""
		TEXT_LABEL_23 tlPlayerTwoSecTextureSpriteName		= ""
		TEXT_LABEL_23 tlPlayerTwoThirdTextureSpriteName		= ""
		
		TEXT_LABEL_23 tlPlayerThreeTextureSpriteName		= ""
		TEXT_LABEL_23 tlPlayerThreeSecTextureSpriteName		= ""
		TEXT_LABEL_23 tlPlayerThreeThirdTextureSpriteName	= ""
		
		TEXT_LABEL_23 tlPlayerFourTextureSpriteName			= ""
		TEXT_LABEL_23 tlPlayerFourSecTextureSpriteName		= ""
		TEXT_LABEL_23 tlPlayerFourThirdTextureSpriteName	= ""
		
		TEXT_LABEL_23 tlPlayerFiveTextureSpriteName			= ""
		TEXT_LABEL_23 tlPlayerFiveSecTextureSpriteName		= ""
		TEXT_LABEL_23 tlPlayerFiveThirdTextureSpriteName	= ""
		
		// Texture HUD Colour Vars
		HUD_COLOURS PlayerOneTextureHudColour 			= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerOneSecTextureHudColour 		= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerOneThirdTextureHudColour 		= HUD_COLOUR_WHITE
		
		HUD_COLOURS PlayerTwoTextureHudColour 			= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerTwoSecTextureHudColour 		= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerTwoThirdTextureHudColour 		= HUD_COLOUR_WHITE
		
		HUD_COLOURS PlayerThreeTextureHudColour 		= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerThreeSecTextureHudColour 		= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerThreeThirdTextureHudColour 	= HUD_COLOUR_WHITE
		
		HUD_COLOURS PlayerFourTextureHudColour 			= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerFourSecTextureHudColour 		= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerFourThirdTextureHudColour 	= HUD_COLOUR_WHITE
		
		HUD_COLOURS PlayerFiveTextureHudColour 			= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerFiveSecTextureHudColour 		= HUD_COLOUR_WHITE
		HUD_COLOURS PlayerFiveThirdTextureHudColour 	= HUD_COLOUR_WHITE
		
		GET_HUD_POWERUP_DATA(aPowerupOne,   PlayerOneXPIconSprite,   	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 	 PlayerOneTextureHudColour, 	pPlayerOne, 	pPlayerOneHeadshotID, 	PlayerOneSecXPIconSprite, 	tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour, 	TitleColour, 	ePowerupOneColour)
		GET_HUD_POWERUP_DATA(aPowerupTwo,   PlayerTwoXPIconSprite,   	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName,    PlayerTwoTextureHudColour, 	pPlayerTwo, 	pPlayerTwoHeadshotID, 	PlayerTwoSecXPIconSprite, 	tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour, 	TitleColour, 	ePowerupTwoColour)
		GET_HUD_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName,  PlayerThreeTextureHudColour, 	pPlayerThree, 	pPlayerThreeHeadshotID, PlayerThreeSecXPIconSprite, tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour, TitleColour, 	ePowerupThreeColour)
		GET_HUD_POWERUP_DATA(aPowerupFour,  PlayerFourXPIconSprite,  	tlPlayerFourTextureDictName, 	tlPlayerFourTextureSpriteName, 	 PlayerFourTextureHudColour, 	pPlayerFour, 	pPlayerFourHeadshotID, 	PlayerFourSecXPIconSprite, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour, 	TitleColour, 	ePowerupFourColour)
		GET_HUD_POWERUP_DATA(aPowerupFive,  PlayerFiveXPIconSprite,		tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 	 PlayerFiveTextureHudColour, 	pPlayerFive, 	pPlayerFiveHeadshotID, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour, 	TitleColour, 	ePowerupFiveColour)
		
	
		
		
		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		
		IF REQUEST_AND_LOAD_ALL_FIVE_TEXTURE_DICTIONARIES(tlPlayerOneTextureDictName, tlPlayerOneSecTextureDictName,
															tlPlayerTwoTextureDictName, tlPlayerTwoSecTextureDictName,
															tlPlayerThreeTextureDictName, tlPlayerThreeSecTextureDictName,
															tlPlayerFourTextureDictName,  tlPlayerFourSecTextureDictName,
															tlPlayerFiveTextureDictName,  tlPlayerFiveSecTextureDictName)
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")				
						
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
		
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			
			IF Number < 1000000
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_SCORE
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_SCORE
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			ELSE
				Overlays.x += TIMER_OVERLAY_X
				Overlays.y += TIMER_OVERLAY_Y_SCORESML
				Overlays.w += TIMER_OVERLAY_W
				Overlays.h += TIMER_OVERLAY_H_SCORESML
				Overlays.r += 255
				Overlays.g += 255
				Overlays.b += 255
				Overlays.a = TIMER_OVERLAY_ALPHA //reduce by 20% 931421
			ENDIF
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			
			DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg", Overlays)
			
			SET_TEXT_STYLE(TitleStyle)
			
			
			IF iAvatarFlashTime = 0
				RESET_GENERIC_FIVE_ICON_SCORE_BAR_AVATAR_FLASHING(Index)
			ENDIF
			
			BOOL bFlashAvatarThisFrame
			IF DO_FLASHING(iAvatarFlashTime, MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_Avatar_Hud[index],MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_Avatar_MiniHud[index])
				bFlashAvatarThisFrame = TRUE
			ELSE
				bFlashAvatarThisFrame = FALSE
			ENDIF
			
			
			
			

			
			
			IF bFlashAvatar
			AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
								
				IF bFlashAvatarThisFrame
					IF pAvatarToFlash = pPlayerOne
					AND iAvatarSlotToFlash = 5
						DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
					ENDIF
					IF pAvatarToFlash = pPlayerTwo
					AND iAvatarSlotToFlash = 4
						DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
					ENDIF
					IF pAvatarToFlash = pPlayerThree
					AND iAvatarSlotToFlash = 3
						DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupThree, 	PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
					ENDIF
					IF pAvatarToFlash = pPlayerFour
					AND iAvatarSlotToFlash = 2
						DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
					ENDIF
					IF pAvatarToFlash = pPlayerFive
					AND iAvatarSlotToFlash = 1
						DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFive, 	PlayerFiveXPIconSprite, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 		PlayerFiveTextureHudColour, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour)
					ENDIF
				ENDIF
				
				// Draw everyone who isn't flashing
				IF pAvatarToFlash = pPlayerOne
				AND iAvatarSlotToFlash != 5
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash = pPlayerTwo
				AND iAvatarSlotToFlash != 4
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash = pPlayerThree
				AND iAvatarSlotToFlash != 3
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupThree, 	PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash = pPlayerFour
				AND iAvatarSlotToFlash != 2
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash = pPlayerFive
				AND iAvatarSlotToFlash != 1
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFive, 	PlayerFiveXPIconSprite, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 		PlayerFiveTextureHudColour, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour)
				ENDIF
				
				// Draw everyone who isn't flashing
				IF pAvatarToFlash != pPlayerOne
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash != pPlayerTwo
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash != pPlayerThree
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupThree, 	PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash != pPlayerFour
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
				ENDIF
				IF pAvatarToFlash != pPlayerFive
					DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFive, 	PlayerFiveXPIconSprite, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 		PlayerFiveTextureHudColour, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour)
				ENDIF
			ELSE
								
				DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
				DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
				DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupThree, 	PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
				DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
				DRAW_2D_SPRITE_FROM_ALREADY_REQUESTED_POWERUP_DATA(aPowerupFive, 	PlayerFiveXPIconSprite, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 		PlayerFiveTextureHudColour, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour)
			ENDIF

		
			
			IF iPulseTime = 0
				RESET_GENERIC_FIVE_ICON_SCORE_BAR_FLASHING(Index)
			ENDIF
			
			BOOL bPulseThisFrame
			IF DO_FLASHING(iPulseTime, MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_Hud[index],MPGlobalsScoreHud.iFlashing_GenericFiveIconScoreBar_MiniHud[index])
				bPulseThisFrame = TRUE
			ELSE
				bPulseThisFrame = FALSE
			ENDIF
			
			IF bEnablePlayerHighlight
				
				SWITCH iInstanceToHighlight
					CASE -1 // Highlight all instances of player
						
						
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerOneThirdXPIconSprite,		tlPlayerOneThirdTextureDictName, 	tlPlayerOneThirdTextureSpriteName,		PlayerOneThirdTextureHudColour,		pPlayerOne, 	pPlayerToHighlight,		TitleColour)
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerTwoThirdXPIconSprite,		tlPlayerTwoThirdTextureDictName, 	tlPlayerTwoThirdTextureSpriteName,		PlayerTwoThirdTextureHudColour,		pPlayerTwo, 	pPlayerToHighlight,		TitleColour)
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerThreeThirdXPIconSprite,	tlPlayerThreeThirdTextureDictName, 	tlPlayerThreeThirdTextureSpriteName,	PlayerThreeThirdTextureHudColour,	pPlayerThree, 	pPlayerToHighlight,		TitleColour)
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerFourThirdXPIconSprite,	tlPlayerFourThirdTextureDictName, 	tlPlayerFourThirdTextureSpriteName,		PlayerFourThirdTextureHudColour,	pPlayerFour, 	pPlayerToHighlight,		TitleColour)
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerFiveThirdXPIconSprite,	tlPlayerFiveThirdTextureDictName, 	tlPlayerFiveThirdTextureSpriteName,		PlayerFiveThirdTextureHudColour,	pPlayerFive, 	pPlayerToHighlight,		TitleColour)
						
						IF bPulseHighlight
							IF bPulseThisFrame
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerOneThirdXPIconSprite, 	tlPlayerOneThirdTextureDictName, 	tlPlayerOneThirdTextureSpriteName, 		PlayerOneThirdTextureHudColour)
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerTwoThirdXPIconSprite,		tlPlayerTwoThirdTextureDictName, 	tlPlayerTwoThirdTextureSpriteName, 		PlayerTwoThirdTextureHudColour)
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerThreeThirdXPIconSprite, 	tlPlayerThreeThirdTextureDictName, 	tlPlayerThreeThirdTextureSpriteName, 	PlayerThreeThirdTextureHudColour)
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFourThirdXPIconSprite, 	tlPlayerFourThirdTextureDictName, 	tlPlayerFourThirdTextureSpriteName, 	PlayerFourThirdTextureHudColour)
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFiveThirdXPIconSprite, 	tlPlayerFiveThirdTextureDictName, 	tlPlayerFiveThirdTextureSpriteName, 	PlayerFiveThirdTextureHudColour)
							ENDIF
						ELSE
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerOneThirdXPIconSprite, 	tlPlayerOneThirdTextureDictName, 	tlPlayerOneThirdTextureSpriteName, 		PlayerOneThirdTextureHudColour)
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerTwoThirdXPIconSprite,		tlPlayerTwoThirdTextureDictName, 	tlPlayerTwoThirdTextureSpriteName, 		PlayerTwoThirdTextureHudColour)
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerThreeThirdXPIconSprite, 	tlPlayerThreeThirdTextureDictName, 	tlPlayerThreeThirdTextureSpriteName, 	PlayerThreeThirdTextureHudColour)
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFourThirdXPIconSprite, 	tlPlayerFourThirdTextureDictName, 	tlPlayerFourThirdTextureSpriteName, 	PlayerFourThirdTextureHudColour)
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFiveThirdXPIconSprite, 	tlPlayerFiveThirdTextureDictName, 	tlPlayerFiveThirdTextureSpriteName, 	PlayerFiveThirdTextureHudColour)
						ENDIF
						
					BREAK
					CASE 0 // Default - Do nothing
					BREAK
					CASE 1 // Highlight player in slot 1
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerOneThirdXPIconSprite,		tlPlayerOneThirdTextureDictName, 	tlPlayerOneThirdTextureSpriteName,		PlayerOneThirdTextureHudColour,		pPlayerOne, 	pPlayerToHighlight,		TitleColour)
						IF bPulseHighlight
							IF bPulseThisFrame
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerOneThirdXPIconSprite, 	tlPlayerOneThirdTextureDictName, 	tlPlayerOneThirdTextureSpriteName, 		PlayerOneThirdTextureHudColour)
							ENDIF
						ELSE
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerOneThirdXPIconSprite, 	tlPlayerOneThirdTextureDictName, 	tlPlayerOneThirdTextureSpriteName, 		PlayerOneThirdTextureHudColour)
						ENDIF
					BREAK
					CASE 2 // Highlight player in slot 2
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerTwoThirdXPIconSprite,		tlPlayerTwoThirdTextureDictName, 	tlPlayerTwoThirdTextureSpriteName,		PlayerTwoThirdTextureHudColour,		pPlayerTwo, 	pPlayerToHighlight,		TitleColour)
						IF bPulseHighlight
							IF bPulseThisFrame
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerTwoThirdXPIconSprite,		tlPlayerTwoThirdTextureDictName, 	tlPlayerTwoThirdTextureSpriteName, 		PlayerTwoThirdTextureHudColour)
							ENDIF
						ELSE
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerTwoThirdXPIconSprite,		tlPlayerTwoThirdTextureDictName, 	tlPlayerTwoThirdTextureSpriteName, 		PlayerTwoThirdTextureHudColour)
						ENDIF
					BREAK
					CASE 3 // Highlight player in slot 3
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerThreeThirdXPIconSprite,	tlPlayerThreeThirdTextureDictName, 	tlPlayerThreeThirdTextureSpriteName,	PlayerThreeThirdTextureHudColour,	pPlayerThree, 	pPlayerToHighlight,		TitleColour)
						IF bPulseHighlight
							IF bPulseThisFrame
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerThreeThirdXPIconSprite, 	tlPlayerThreeThirdTextureDictName, 	tlPlayerThreeThirdTextureSpriteName, 	PlayerThreeThirdTextureHudColour)
							ENDIF
						ELSE
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerThreeThirdXPIconSprite, 	tlPlayerThreeThirdTextureDictName, 	tlPlayerThreeThirdTextureSpriteName, 	PlayerThreeThirdTextureHudColour)
						ENDIF
					BREAK
					CASE 4 // Highlight player in slot 4
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerFourThirdXPIconSprite,	tlPlayerFourThirdTextureDictName, 	tlPlayerFourThirdTextureSpriteName,		PlayerFourThirdTextureHudColour,	pPlayerFour, 	pPlayerToHighlight,		TitleColour)
						IF bPulseHighlight
							IF bPulseThisFrame
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFourThirdXPIconSprite, 	tlPlayerFourThirdTextureDictName, 	tlPlayerFourThirdTextureSpriteName, 	PlayerFourThirdTextureHudColour)
							ENDIF
						ELSE
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFourThirdXPIconSprite, 	tlPlayerFourThirdTextureDictName, 	tlPlayerFourThirdTextureSpriteName, 	PlayerFourThirdTextureHudColour)
						ENDIF
					BREAK
					CASE 5 // Highlight player in slot 5
						GET_HUD_HIGHLIGHTED_PLAYER_DATA(PlayerFiveThirdXPIconSprite,	tlPlayerFiveThirdTextureDictName, 	tlPlayerFiveThirdTextureSpriteName,		PlayerFiveThirdTextureHudColour,	pPlayerFive, 	pPlayerToHighlight,		TitleColour)
						IF bPulseHighlight
							IF bPulseThisFrame
								DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFiveThirdXPIconSprite, 	tlPlayerFiveThirdTextureDictName, 	tlPlayerFiveThirdTextureSpriteName, 	PlayerFiveThirdTextureHudColour)
							ENDIF
						ELSE
							DRAW_2D_SPRITE_FROM_HIGHLIGHTED_PLAYER_DATA(PlayerFiveThirdXPIconSprite, 	tlPlayerFiveThirdTextureDictName, 	tlPlayerFiveThirdTextureSpriteName, 	PlayerFiveThirdTextureHudColour)
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			SET_TEXT_STYLE(NumberStyle)
			
			IF DrawInfinity
				DRAW_TEXT_WITH_PLAYER_NAME(NumberPlace, NumberStyle, "∞", "", HUD_COLOUR_WHITE, FONT_RIGHT)
			ELSE	
				IF IS_STRING_EMPTY_HUD(NumberString)
					IF MaXNumber = 0
						IF isFloat = FALSE
							IF Number != -999
								DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,"NUMBER", Number,FONT_RIGHT)
							ENDIF
						ELSE
							DRAW_TEXT_WITH_FLOAT(NumberPlace, NumberStyle,"NUMBER", FloatValue,g_b_iNumberOfDecimalPlacesForScore,FONT_RIGHT)
						ENDIF
					ELSE
						DRAW_TEXT_WITH_2_NUMBERS(NumberPlace,NumberStyle,"TIMER_DASHES", Number, MaXNumber ,FONT_RIGHT)
					ENDIF
				ELSE
					
					IF ARE_STRINGS_EQUAL("HUD_CASH", NumberString)
					OR  ARE_STRINGS_EQUAL("HUD_CASH_S", NumberString)
						NumberString = "HUD_CASH_S"	
						NumberStyle.aFont = FONT_STYLE_FIXED_WIDTH_NUMBERS
						SET_TEXT_STYLE(NumberStyle)
						DRAW_CASH_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number, FONT_RIGHT)

					ELIF ARE_STRINGS_EQUAL("HUD_CASH_NEG", NumberString)
					OR ARE_STRINGS_EQUAL("HUD_CASH_NEG_S", NumberString)								
						NumberStyle.aFont = FONT_STYLE_FIXED_WIDTH_NUMBERS
						SET_TEXT_STYLE(NumberStyle)
						NumberString = "HUD_CASH_NEG_S"
						DRAW_CASH_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number, FONT_RIGHT)

					ELSE
						IF isFloat = FALSE
							DRAW_TEXT_WITH_NUMBER(NumberPlace,NumberStyle,NumberString, Number, FONT_RIGHT)
						ELSE
							DRAW_TEXT_WITH_FLOAT(NumberPlace,NumberStyle,NumberString, FloatValue, g_b_iNumberOfDecimalPlacesForScore,FONT_RIGHT)
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			RESET_GFX_TIMERS_DRAW_ORDER()
		ENDIF
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_FIVE_ICON_SCORE_BAR(INT idx, INT Number, FLOAT FloatValue, STRING NumberString, BOOL isFloat, INT MaXNumber, BOOL DrawInfinity, HUD_COLOURS TitleColour, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE, PLAYER_INDEX pPlayerToHighlight = NULL, BOOL bEnablePlayerHighlight = FALSE, HUD_COLOURS ePowerupOneColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupTwoColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupThreeColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupFourColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupFiveColour = HUD_COLOUR_PURE_WHITE, INT iInstanceToHighlight = 0, BOOL bPulseHighlight = FALSE, INT iPulseTime = 9999999, PLAYER_INDEX pAvatarToFlash = NULL, BOOL bFlashAvatar = FALSE, INT iAvatarFlashTime = 9999999, INT iAvatarSlotToFlash = 0)
	
	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FIVE_ICON_SCORE_BAR, idx)
		
		TEXT_STYLE TitleStyle, NumberStyle
		TEXT_PLACEMENT TitlePlacement, NumberPlace
		
		DRAW_GENERAL_FIVE_ICON_SCORE_BAR_GUTS(idx, TitleStyle, NumberStyle, TitlePlacement, NumberPlace, Number, FloatValue, NumberString, isFloat, MaXNumber, DrawInfinity, TitleColour, UIELEMENTS_BOTTOMRIGHT, pPlayerOne, pPlayerTwo, pPlayerThree, pPlayerFour, pPlayerFive, aPowerupOne, aPowerupTwo, aPowerupThree, aPowerupFour, aPowerupFive, pPlayerToHighlight, bEnablePlayerHighlight, ePowerupOneColour, ePowerupTwoColour, ePowerupThreeColour, ePowerupFourColour, ePowerupFiveColour, iInstanceToHighlight, bPulseHighlight, iPulseTime, pAvatarToFlash, bFlashAvatar, iAvatarFlashTime, iAvatarSlotToFlash)
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_FIVE_ICON_SCORE_BAR index = ")NET_PRINT_INT(idx)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC DRAW_GENERAL_SIX_ICON_BAR_GUTS(INT index, TEXT_STYLE &TitleStyle, TEXT_STYLE &NumberStyle, TEXT_PLACEMENT &TitlePlace, TEXT_PLACEMENT &NumberPlace, HUD_COLOURS TitleColour, UIELEMENTS WhichSpace, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL, PLAYER_INDEX pPlayerSix = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupSix = ACTIVITY_POWERUP_NONE, BOOL bFlashIconOne = FALSE, BOOL bFlashIconTwo = FALSE, BOOL bFlashIconThree = FALSE, BOOL bFlashIconFour = FALSE, BOOL bFlashIconFive = FALSE, BOOL bFlashIconSix = FALSE, INT iFlashTime = -1)
	MPGlobalsScoreHud.iHowManyDisplays++
	
	IF IS_BOTTOM_RIGHT_AREA_FREE()
		SET_PROGRESSHUD_INIT_DONE(PROGRESSHUD_SIX_ICON_BAR, index)
		
		SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
		SET_WORD_WRAPPING_TITLE(TitleStyle)
		
		IF MPGlobalsScoreHud.iHowManyDisplays = 1
			SET_Y_SHIFT_START(WhichSpace)
		ENDIF
		
		SET_STANDARD_UI_SCORE_NUMBER(NumberStyle, DROPSTYLE_NONE)
		SET_WORD_WRAPPING_RIGHTEDGE(NumberStyle)
		
		INIT_SCREEN_GENERAL_SINGLE_SCORE(NumberStyle, TitlePlace, NumberPlace, WhichSpace, TitleStyle)
		
		SPRITE_PLACEMENT Overlays
		SET_SPRITE_PLACEMENT(Overlays, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 255)
		
		// Flashing Icons
		BOOL bDisplayFlashingIcons
		
		IF iFlashTime = 0
			RESET_GENERIC_SIX_ICON_BAR_FLASHING(index)
		ENDIF
		
		IF DO_FLASHING(iFlashTime, MPGlobalsScoreHud.iFlashing_GenericSixIconBar_Hud[index], MPGlobalsScoreHud.iFlashing_GenericSixIconBar_MiniHud[index])
			bDisplayFlashingIcons = TRUE
		ELSE
			bDisplayFlashingIcons = FALSE
		ENDIF
		
		REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
		
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
			GFX_DRAW_ORDER anOrder = SET_GFX_TIMERS_DRAW_ORDER()
			
			SET_SCRIPT_GFX_DRAW_ORDER(anOrder)
			
			// Background Bar
			Overlays.x = TitlePlace.x
			Overlays.y = TitlePlace.y
			Overlays.x += TIMER_OVERLAY_X
			Overlays.y += TIMER_OVERLAY_Y_SCORE
			Overlays.w += TIMER_OVERLAY_W
			Overlays.h += TIMER_OVERLAY_H_SCORE
			Overlays.r += 255
			Overlays.g += 255
			Overlays.b += 255
			Overlays.a = TIMER_OVERLAY_ALPHA
			
			IF IS_LANGUAGE_NON_ROMANIC()
				Overlays.x += TIMER_OVERLAY_X_NON_ROMANIC_OFFSET
				Overlays.w += TIMER_OVERLAY_W_NON_ROMANIC_OFFSET
			ENDIF
			
			MPGlobalsScoreHud.TopOfTimers += Overlays.h
			DRAW_2D_SPRITE("TimerBars", "ALL_BLACK_bg", Overlays)
			SET_TEXT_STYLE(TitleStyle)
			
			//Icon Setup
			SPRITE_PLACEMENT PlayerOneXPIconSprite
			SPRITE_PLACEMENT PlayerOneSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerTwoXPIconSprite
			SPRITE_PLACEMENT PlayerTwoSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerThreeXPIconSprite
			SPRITE_PLACEMENT PlayerThreeSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerFourXPIconSprite
			SPRITE_PLACEMENT PlayerFourSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerFiveXPIconSprite
			SPRITE_PLACEMENT PlayerFiveSecXPIconSprite
			
			SPRITE_PLACEMENT PlayerSixXPIconSprite
			SPRITE_PLACEMENT PlayerSixSecXPIconSprite
			
			// Underlay sprite
			PlayerOneXPIconSprite.x 		= NumberPlace.x + 0.145 + 0.001
			PlayerOneSecXPIconSprite.x 		= NumberPlace.x + 0.145 + 0.001
			
			PlayerTwoXPIconSprite.x 		= NumberPlace.x + 0.123
			PlayerTwoSecXPIconSprite.x 		= NumberPlace.x + 0.123
			
			PlayerThreeXPIconSprite.x 		= NumberPlace.x + 0.101
			PlayerThreeSecXPIconSprite.x 	= NumberPlace.x + 0.101
			
			PlayerFourXPIconSprite.x 		= NumberPlace.x + 0.078
			PlayerFourSecXPIconSprite.x 	= NumberPlace.x + 0.078
			
			PlayerFiveXPIconSprite.x 		= NumberPlace.x + 0.056
			PlayerFiveSecXPIconSprite.x 	= NumberPlace.x + 0.056
			
			PlayerSixXPIconSprite.x 		= NumberPlace.x + 0.034
			PlayerSixSecXPIconSprite.x 		= NumberPlace.x + 0.034
			
			IF IS_LANGUAGE_NON_ROMANIC()
				PlayerOneXPIconSprite.y 		= NumberPlace.y + 0.016 - 0.000
				PlayerOneSecXPIconSprite.y 		= NumberPlace.y + 0.016 + 0.0005
				
				PlayerTwoXPIconSprite.y 		= NumberPlace.y + 0.016 - 0.000
				PlayerTwoSecXPIconSprite.y 		= NumberPlace.y + 0.016 + 0.0005
				
				PlayerThreeXPIconSprite.y 		= NumberPlace.y + 0.016 - 0.000
				PlayerThreeSecXPIconSprite.y 	= NumberPlace.y + 0.016 + 0.0005
				
				PlayerFourXPIconSprite.y 		= NumberPlace.y + 0.016 - 0.000
				PlayerFourSecXPIconSprite.y 	= NumberPlace.y + 0.016 + 0.0005
				
				PlayerFiveXPIconSprite.y 		= NumberPlace.y + 0.016 - 0.000
				PlayerFiveSecXPIconSprite.y 	= NumberPlace.y + 0.016 + 0.0005
				
				PlayerSixXPIconSprite.y 		= NumberPlace.y + 0.016 - 0.000
				PlayerSixSecXPIconSprite.y 		= NumberPlace.y + 0.016 + 0.0005
			ELSE
				PlayerOneXPIconSprite.y 		= NumberPlace.y + 0.0185
				PlayerOneSecXPIconSprite.y 		= NumberPlace.y + 0.019
				
				PlayerTwoXPIconSprite.y 		= NumberPlace.y + 0.0185
				PlayerTwoSecXPIconSprite.y 		= NumberPlace.y + 0.019
				
				PlayerThreeXPIconSprite.y 		= NumberPlace.y + 0.0185
				PlayerThreeSecXPIconSprite.y 	= NumberPlace.y + 0.019
				
				PlayerFourXPIconSprite.y 		= NumberPlace.y + 0.0185
				PlayerFourSecXPIconSprite.y 	= NumberPlace.y + 0.019
				
				PlayerFiveXPIconSprite.y 		= NumberPlace.y + 0.0185
				PlayerFiveSecXPIconSprite.y 	= NumberPlace.y + 0.019
				
				PlayerSixXPIconSprite.y 		= NumberPlace.y + 0.0185
				PlayerSixSecXPIconSprite.y 		= NumberPlace.y + 0.019
			ENDIF
			
			// Width
			PlayerOneXPIconSprite.w 			= 0.016 + 0.003
			PlayerOneSecXPIconSprite.w 			= 0.016 + 0.003
			
			PlayerTwoXPIconSprite.w 			= 0.016 + 0.003
			PlayerTwoSecXPIconSprite.w 			= 0.016 + 0.003
			
			PlayerThreeXPIconSprite.w 			= 0.016 + 0.003
			PlayerThreeSecXPIconSprite.w		= 0.016 + 0.003
			
			PlayerFourXPIconSprite.w 			= 0.016 + 0.003
			PlayerFourSecXPIconSprite.w 		= 0.016 + 0.003
			
			PlayerFiveXPIconSprite.w 			= 0.016 + 0.003
			PlayerFiveSecXPIconSprite.w 		= 0.016 + 0.003
			
			PlayerSixXPIconSprite.w 			= 0.016 + 0.003
			PlayerSixSecXPIconSprite.w 			= 0.016 + 0.003
			
			// Height
			PlayerOneXPIconSprite.h 			= 0.032 + 0.004
			PlayerOneSecXPIconSprite.h 			= 0.032 + 0.004
			
			PlayerTwoXPIconSprite.h 			= 0.032 + 0.004
			PlayerTwoSecXPIconSprite.h			= 0.032 + 0.004
			
			PlayerThreeXPIconSprite.h 			= 0.032 + 0.004
			PlayerThreeSecXPIconSprite.h 		= 0.032 + 0.004
			
			PlayerFourXPIconSprite.h 			= 0.032 + 0.004
			PlayerFourSecXPIconSprite.h 		= 0.032 + 0.004
			
			PlayerFiveXPIconSprite.h 			= 0.032 + 0.004
			PlayerFiveSecXPIconSprite.h 		= 0.032 + 0.004
			
			PlayerSixXPIconSprite.h 			= 0.032 + 0.004
			PlayerSixSecXPIconSprite.h 			= 0.032 + 0.004
			
			//Alpha
			PlayerOneXPIconSprite.a 			= 255
			PlayerOneSecXPIconSprite.a 			= 255
			
			PlayerTwoXPIconSprite.a				= 255
			PlayerTwoSecXPIconSprite.a 			= 255
			
			PlayerThreeXPIconSprite.a 			= 255
			PlayerThreeSecXPIconSprite.a 		= 255
			
			PlayerFourXPIconSprite.a 			= 255
			PlayerFourSecXPIconSprite.a 		= 255
			
			PlayerFiveXPIconSprite.a 			= 255
			PlayerFiveSecXPIconSprite.a 		= 255
			
			PlayerSixXPIconSprite.a 			= 255
			PlayerSixSecXPIconSprite.a 			= 255
			
			// HUD Colour
			SET_SPRITE_HUD_COLOUR(PlayerOneXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerOneSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerTwoXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerTwoSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerThreeXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerThreeSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerFourXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerFourSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerFiveXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerFiveSecXPIconSprite, HUD_COLOUR_WHITE)
			
			SET_SPRITE_HUD_COLOUR(PlayerSixXPIconSprite, HUD_COLOUR_WHITE)
			SET_SPRITE_HUD_COLOUR(PlayerSixSecXPIconSprite, HUD_COLOUR_WHITE)
			
			// Ped Headshot Vars
			PEDHEADSHOT_ID pPlayerOneHeadshotID
			PEDHEADSHOT_ID pPlayerTwoHeadshotID
			PEDHEADSHOT_ID pPlayerThreeHeadshotID
			PEDHEADSHOT_ID pPlayerFourHeadshotID
			PEDHEADSHOT_ID pPlayerFiveHeadshotID
			PEDHEADSHOT_ID pPlayerSixHeadshotID
			
			// TXD Name Vars
			TEXT_LABEL_23 tlPlayerOneTextureDictName = ""
			TEXT_LABEL_23 tlPlayerOneSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerTwoTextureDictName = ""
			TEXT_LABEL_23 tlPlayerTwoSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerThreeTextureDictName = ""
			TEXT_LABEL_23 tlPlayerThreeSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerFourTextureDictName = ""
			TEXT_LABEL_23 tlPlayerFourSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerFiveTextureDictName = ""
			TEXT_LABEL_23 tlPlayerFiveSecTextureDictName = ""
			
			TEXT_LABEL_23 tlPlayerSixTextureDictName = ""
			TEXT_LABEL_23 tlPlayerSixSecTextureDictName = ""
			
			// Sprite Name Vars
			TEXT_LABEL_23 tlPlayerOneTextureSpriteName
			TEXT_LABEL_23 tlPlayerOneSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerTwoTextureSpriteName
			TEXT_LABEL_23 tlPlayerTwoSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerThreeTextureSpriteName
			TEXT_LABEL_23 tlPlayerThreeSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerFourTextureSpriteName
			TEXT_LABEL_23 tlPlayerFourSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerFiveTextureSpriteName
			TEXT_LABEL_23 tlPlayerFiveSecTextureSpriteName
			
			TEXT_LABEL_23 tlPlayerSixTextureSpriteName
			TEXT_LABEL_23 tlPlayerSixSecTextureSpriteName
			
			// Texture HUD Colour Vars
			HUD_COLOURS PlayerOneTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerOneSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerTwoTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerTwoSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerThreeTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerThreeSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerFourTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerFourSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerFiveTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerFiveSecTextureHudColour = HUD_COLOUR_WHITE
			
			HUD_COLOURS PlayerSixTextureHudColour = HUD_COLOUR_WHITE
			HUD_COLOURS PlayerSixSecTextureHudColour = HUD_COLOUR_WHITE
			
			GET_HUD_POWERUP_DATA(aPowerupOne,   PlayerOneXPIconSprite,   	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 	 PlayerOneTextureHudColour, 	pPlayerOne, 	pPlayerOneHeadshotID, 	PlayerOneSecXPIconSprite, 	tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour, 	TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupTwo,   PlayerTwoXPIconSprite,   	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName,    PlayerTwoTextureHudColour, 	pPlayerTwo, 	pPlayerTwoHeadshotID, 	PlayerTwoSecXPIconSprite, 	tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour, 	TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName,  PlayerThreeTextureHudColour, 	pPlayerThree, 	pPlayerThreeHeadshotID, PlayerThreeSecXPIconSprite, tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour, TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupFour,  PlayerFourXPIconSprite,  	tlPlayerFourTextureDictName, 	tlPlayerFourTextureSpriteName, 	 PlayerFourTextureHudColour, 	pPlayerFour, 	pPlayerFourHeadshotID, 	PlayerFourSecXPIconSprite, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour, 	TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupFive,  PlayerFiveXPIconSprite,  	tlPlayerFiveTextureDictName, 	tlPlayerFiveTextureSpriteName, 	 PlayerFiveTextureHudColour, 	pPlayerFive, 	pPlayerFiveHeadshotID, 	PlayerFiveSecXPIconSprite, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour, 	TitleColour)
			GET_HUD_POWERUP_DATA(aPowerupSix,	PlayerSixXPIconSprite,  	tlPlayerSixTextureDictName, 	tlPlayerSixTextureSpriteName, 	 PlayerSixTextureHudColour, 	pPlayerSix, 	pPlayerSixHeadshotID, 	PlayerSixSecXPIconSprite, 	tlPlayerSixSecTextureDictName,		tlPlayerSixSecTextureSpriteName, 	PlayerSixSecTextureHudColour, 	TitleColour)
			
			IF bFlashIconOne
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupOne, 	PlayerOneXPIconSprite, 		PlayerOneSecXPIconSprite, 	tlPlayerOneTextureDictName, 	tlPlayerOneTextureSpriteName, 		PlayerOneTextureHudColour, 		tlPlayerOneSecTextureDictName, 		tlPlayerOneSecTextureSpriteName, 	PlayerOneSecTextureHudColour)
			ENDIF
			
			IF bFlashIconTwo
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupTwo, 	PlayerTwoXPIconSprite, 		PlayerTwoSecXPIconSprite, 	tlPlayerTwoTextureDictName, 	tlPlayerTwoTextureSpriteName, 		PlayerTwoTextureHudColour, 		tlPlayerTwoSecTextureDictName, 		tlPlayerTwoSecTextureSpriteName, 	PlayerTwoSecTextureHudColour)
			ENDIF
			
			IF bFlashIconThree
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupThree, PlayerThreeXPIconSprite, 	PlayerThreeSecXPIconSprite, tlPlayerThreeTextureDictName, 	tlPlayerThreeTextureSpriteName, 	PlayerThreeTextureHudColour, 	tlPlayerThreeSecTextureDictName, 	tlPlayerThreeSecTextureSpriteName, 	PlayerThreeSecTextureHudColour)
			ENDIF
			
			IF bFlashIconFour
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupFour,	PlayerFourXPIconSprite, 	PlayerFourSecXPIconSprite,	tlPlayerFourTextureDictName,	tlPlayerFourTextureSpriteName, 		PlayerFourTextureHudColour, 	tlPlayerFourSecTextureDictName, 	tlPlayerFourSecTextureSpriteName, 	PlayerFourSecTextureHudColour)
			ENDIF
			
			IF bFlashIconFive
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupFive,	PlayerFiveXPIconSprite, 	PlayerFiveSecXPIconSprite,	tlPlayerFiveTextureDictName,	tlPlayerFiveTextureSpriteName, 		PlayerFiveTextureHudColour, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupFive,	PlayerFiveXPIconSprite, 	PlayerFiveSecXPIconSprite,	tlPlayerFiveTextureDictName,	tlPlayerFiveTextureSpriteName, 		PlayerFiveTextureHudColour, 	tlPlayerFiveSecTextureDictName, 	tlPlayerFiveSecTextureSpriteName, 	PlayerFiveSecTextureHudColour)
			ENDIF
			
			IF bFlashIconSix
				IF bDisplayFlashingIcons
					DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupSix,	PlayerSixXPIconSprite, 	PlayerSixSecXPIconSprite,	tlPlayerSixTextureDictName,	tlPlayerSixTextureSpriteName, 		PlayerSixTextureHudColour, 	tlPlayerSixSecTextureDictName, 	tlPlayerSixSecTextureSpriteName, 	PlayerSixSecTextureHudColour)
				ENDIF
			ELSE
				DRAW_2D_SPRITE_FROM_POWERUP_DATA(aPowerupSix,	PlayerSixXPIconSprite, 	PlayerSixSecXPIconSprite,	tlPlayerSixTextureDictName,	tlPlayerSixTextureSpriteName, 		PlayerSixTextureHudColour, 	tlPlayerSixSecTextureDictName, 	tlPlayerSixSecTextureSpriteName, 	PlayerSixSecTextureHudColour)
			ENDIF
		ENDIF
		
		RESET_GFX_TIMERS_DRAW_ORDER()
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_GENERAL_SIX_ICON_BAR(INT idx, HUD_COLOURS TitleColour, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL, PLAYER_INDEX pPlayerSix = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupSix = ACTIVITY_POWERUP_NONE, BOOL bFlashIconOne = FALSE, BOOL bFlashIconTwo = FALSE, BOOL bFlashIconThree = FALSE, BOOL bFlashIconFour = FALSE, BOOL bFlashIconFive = FALSE, BOOL bFlashIconSix = FALSE, INT iFlashTime = -1)
	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SIX_ICON_BAR, idx)
		TEXT_STYLE TitleStyle, NumberStyle
		TEXT_PLACEMENT TitlePlacement, NumberPlace
		
		DRAW_GENERAL_SIX_ICON_BAR_GUTS(idx, TitleStyle, NumberStyle, TitlePlacement, NumberPlace, TitleColour, UIELEMENTS_BOTTOMRIGHT, pPlayerOne, pPlayerTwo, pPlayerThree, pPlayerFour, pPlayerFive, pPlayerSix, aPowerupOne, aPowerupTwo, aPowerupThree, aPowerupFour, aPowerupFive, aPowerupSix, bFlashIconOne, bFlashIconTwo, bFlashIconThree, bFlashIconFour, bFlashIconFive, bFlashIconSix, iFlashTime)
		
		#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			NET_NL() NET_PRINT("ACTUALLY_DRAW_GENERAL_SIX_ICON_BAR index = ") NET_PRINT_INT(idx)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC ACTUALLY_DRAW_DOUBLE_TEXT_BAR(INT idx, STRING sTitleLeft, STRING sTitleRight, BOOL bTextLeftLiteral, BOOL bTextRightLiteral, HUD_COLOURS eColour, BOOL bCustomFont, TEXT_FONTS eCustomFont)
	
	IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_TEXT, idx)
		
		TEXT_STYLE TitleStyle, NumberStyle
		TEXT_PLACEMENT TitlePlace, NumberPlace
		
		DRAW_GENERAL_SINGLE_SCORE_GUTS(idx, TitleStyle, NumberStyle, TitlePlace, NumberPlace, 0, UIELEMENTS_BOTTOMRIGHT, sTitleLeft, eColour, 0, 0, bTextLeftLiteral, "", FALSE, 0.0, HUDFLASHING_NONE, 
									   0, eColour, FALSE, 0, FALSE, ACTIVITY_POWERUP_NONE, HUD_COUNTER_STYLE_DEFAULT, sTitleRight, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
									   DEFAULT, DEFAULT, DEFAULT, TRUE, bTextRightLiteral, bCustomFont, eCustomFont)
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("ACTUALLY_DRAW_DOUBLE_TEXT_BAR index = ")NET_PRINT_INT(idx)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

//PROC SET_TITLE_JUSTIFICATION_SCORE(INT Number, STRING NumberString)
//
//	IF Number > 999999999
//		MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE
//	ELIF Number > 9999999
//		IF COMPARE_STRINGS(NumberString, "HUD_CASH") = 0
//		OR COMPARE_STRINGS(NumberString, "HUD_CASH_NEG") = 0
//			MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE
//		ENDIF
//	ELIF Number > 99999
//		MPGlobalsScoreHud.bTitleMiddleJustified = TRUE	
//	ELIF Number > 999
//		IF COMPARE_STRINGS(NumberString, "HUD_CASH") = 0
//		OR COMPARE_STRINGS(NumberString, "HUD_CASH_NEG") = 0
//			MPGlobalsScoreHud.bTitleMiddleJustified = TRUE
//		ENDIF
//	ENDIF
//
//ENDPROC


/// PURPOSE:
///    Displays a timer in the lower right hand corner of the screen with the label "TimeTitle"
/// PARAMS:
///    TimeRunner - Multiply your time in seconds by 1000.
///    TimeTitle - The name for your clock, from the string tables
///    ExtraTimeGiven - 
///    TimerStyle - 
///    iFlashingTime - 
///    MedalDisplay - 
///    WhichOrder - 
///    isPlayer - 
///    aColour - 
PROC DRAW_GENERIC_TIMER(INT TimeRunner, STRING TimeTitle, INT ExtraTimeGiven = 0, TIMER_STYLE TimerStyle = TIMER_STYLE_DONTUSEMILLISECONDS, INT iFlashingTime = -1, PODIUMPOS MedalDisplay = PODIUMPOS_NONE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, HUD_COLOURS aColour = HUD_COLOUR_WHITE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, BOOL bDisplayAsDashes = FALSE, HUD_COLOURS TitleColour = HUD_COLOUR_PURE_WHITE, BOOL bIsLiteralTitle = FALSE ,HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, ACTIVITY_POWERUP ePowerup = ACTIVITY_POWERUP_NONE, BOOL bHideUnusedZeros = FALSE, INT iTextLabelSubInt = -1)
	
	INT FreeIndex 	= -1
	INT I 
		
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_TIMER, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_TIMER, FreeIndex)
		MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_Timer[FreeIndex] = TimeRunner
		MPGlobalsScoreHud.ElementHud_TIMER.sGenericTimer_TimerTitle[FreeIndex] = TimeTitle
		MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtraTime[FreeIndex] = ExtraTimeGiven
		MPGlobalsScoreHud.ElementHud_TIMER.bGenericTimer_TimerStyle[FreeIndex] = TimerStyle
		MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_FlashTimer[FreeIndex] = iFlashingTime
		MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_MedalDisplay[FreeIndex] = MedalDisplay
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsPlayer[FreeIndex] = isPlayer
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Colour[FreeIndex] = aColour
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_ColourFlash[FreeIndex] = ColourFlash
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bDisplayAsDashes[FreeIndex] = bDisplayAsDashes
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_TitleColour[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsLiteral[FreeIndex] = bIsLiteralTitle
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_FleckColour[FreeIndex] = FleckColour
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Powerup[FreeIndex] = ePowerup
		MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bHideUnusedZeros[FreeIndex] = bHideUnusedZeros
		MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_TitleNumber[FreeIndex] = iTextLabelSubInt
	ENDIF
ENDPROC

PROC DRAW_GENERIC_BIG_NUMBER(INT Number, STRING NumberTitle, INT iFlashingTime = -1, HUD_COLOURS aColour = HUD_COLOUR_WHITE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, STRING NumberString = NULL, HUD_COLOURS TitleColour = HUD_COLOUR_WHITE, BOOL bDrawInfinity = FALSE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, BOOL bEnablePulsing = FALSE, HUD_COLOURS PulseToColour = HUD_COLOUR_PURE_WHITE, INT iPulseTime = -1)
	
	IF IS_STRING_EMPTY_HUD(NumberString)
		NumberString = "NUMBER"
	ENDIF
	
	INT FreeIndex 	= -1
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SINGLE_NUMBER, FreeIndex)
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_Number[FreeIndex]  = Number
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberTitle[FreeIndex]  = NumberTitle	
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_Colour[FreeIndex] = aColour
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_FlashTimer[FreeIndex]  = iFlashingTime
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_bIsPlayer[FreeIndex] = isPlayer
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberString[FreeIndex] = NumberString
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_TitleColour[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.bGenericNumber_DrawInfinity[FreeIndex] = bDrawInfinity
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_ColourFlash[FreeIndex] = ColourFlash
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_FleckColour[FreeIndex] = FleckColour
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_EnablePulsing[FreeIndex] = bEnablePulsing
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_PulseColour[FreeIndex] = PulseToColour
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_PulseTime[FreeIndex] = iPulseTime
		
		
//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_BIG_NUMBER Index = -1 NOT DRAWING THE BIG NUMBER")
	ENDIF
ENDPROC

PROC DRAW_GENERIC_BIG_RACE_POSITION(INT iRacePosition, HUD_COLOURS eRacePositionHUDColour, HUDORDER WhichOrder = HUDORDER_DONTCARE)

	INT FreeIndex = -1
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_BIG_RACE_POSITION, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	
	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_BIG_RACE_POSITION, FreeIndex)
		MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_iRacePosition[FreeIndex] = iRacePosition
		MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_eRacePositionHUDColour[FreeIndex] = eRacePositionHUDColour
		MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[FreeIndex] = WhichOrder
	ENDIF
ENDPROC

PROC DRAW_GENERIC_FOUR_ICON_BAR(HUD_COLOURS TitleColour, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL bFlashIconOne = FALSE, BOOL bFlashIconTwo = FALSE, BOOL bFlashIconThree = FALSE, BOOL bFlashIconFour = FALSE, INT iFlashTime = -1)

	INT FreeIndex = -1
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FOUR_ICON_BAR, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	
	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FOUR_ICON_BAR, FreeIndex)
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_TitleColour[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerOne[FreeIndex] = pPlayerOne
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerTwo[FreeIndex] = pPlayerTwo
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerThree[FreeIndex] = pPlayerThree
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerFour[FreeIndex] = pPlayerFour
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupOne[FreeIndex] = aPowerupOne
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupTwo[FreeIndex] = aPowerupTwo
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupThree[FreeIndex] = aPowerupThree
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupFour[FreeIndex] = aPowerupFour
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconOne[FreeIndex] = bFlashIconOne
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconTwo[FreeIndex] = bFlashIconTwo
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconThree[FreeIndex] = bFlashIconThree
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconFour[FreeIndex] = bFlashIconFour
		MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_iFlashTime[FreeIndex] = iFlashTime
	ENDIF
ENDPROC

PROC DRAW_GENERIC_FIVE_ICON_SCORE_BAR(INT Number, FLOAT FloatValue, STRING NumberString, BOOL isFloat, INT MaXNumber, BOOL DrawInfinity, HUD_COLOURS TitleColour, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE, HUDORDER WhichOrder = HUDORDER_DONTCARE, PLAYER_INDEX pPlayerToHighlight = NULL, BOOL bEnablePlayerHighlight = FALSE, HUD_COLOURS ePowerupOneColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupTwoColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupThreeColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupFourColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ePowerupFiveColour = HUD_COLOUR_PURE_WHITE, INT iInstanceToHighlight = 0, BOOL bPulseHighlight = FALSE, INT iPulseTime = 9999999, PLAYER_INDEX pAvatarToFlash = NULL, BOOL bFlashAvatar = FALSE, INT iAvatarFlashTime = 9999999, INT iAvatarSlotToFlash = 0)
	
	INT FreeIndex = -1
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	
	IF FreeIndex > -1 
		
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FIVE_ICON_SCORE_BAR, FreeIndex)
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_Number[FreeIndex] = Number
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_MaXNumber[FreeIndex] = MaXNumber
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_FloatValue[FreeIndex] = FloatValue
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_NumberString[FreeIndex] = NumberString
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_isFloat[FreeIndex]	= isFloat
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_DrawInfinity[FreeIndex] = DrawInfinity
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_TitleColour[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerOne[FreeIndex] = pPlayerOne
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerTwo[FreeIndex] = pPlayerTwo
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerThree[FreeIndex] = pPlayerThree
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFour[FreeIndex] = pPlayerFour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFive[FreeIndex] = pPlayerFive
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupOne[FreeIndex] = aPowerupOne
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupTwo[FreeIndex] = aPowerupTwo
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupThree[FreeIndex] = aPowerupThree
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFour[FreeIndex] = aPowerupFour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFive[FreeIndex] = aPowerupFive
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerToHighlight[FreeIndex] = pPlayerToHighlight
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bEnablePlayerHighlight[FreeIndex] = bEnablePlayerHighlight
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupOneColour[FreeIndex] = ePowerupOneColour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupTwoColour[FreeIndex] = ePowerupTwoColour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupThreeColour[FreeIndex] = ePowerupThreeColour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFourColour[FreeIndex] = ePowerupFourColour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFiveColour[FreeIndex] = ePowerupFiveColour
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iInstanceToHighlight[FreeIndex] = iInstanceToHighlight
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bPulseHighlight[FreeIndex] = bPulseHighlight
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iPulseTime[FreeIndex] = iPulseTime
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pAvatarToFlash[FreeIndex] = pAvatarToFlash
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bFlashAvatar[FreeIndex] = bFlashAvatar
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarFlashTime[FreeIndex] = iAvatarFlashTime
		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarSlotToFlash[FreeIndex] = iAvatarSlotToFlash
	ENDIF
ENDPROC

PROC DRAW_GENERIC_SIX_ICON_BAR(HUD_COLOURS TitleColour, PLAYER_INDEX pPlayerOne = NULL, PLAYER_INDEX pPlayerTwo = NULL, PLAYER_INDEX pPlayerThree = NULL, PLAYER_INDEX pPlayerFour = NULL, PLAYER_INDEX pPlayerFive = NULL, PLAYER_INDEX pPlayerSix = NULL, ACTIVITY_POWERUP aPowerupOne = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupTwo = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupThree = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFour = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupFive = ACTIVITY_POWERUP_NONE, ACTIVITY_POWERUP aPowerupSix = ACTIVITY_POWERUP_NONE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL bFlashIconOne = FALSE, BOOL bFlashIconTwo = FALSE, BOOL bFlashIconThree = FALSE, BOOL bFlashIconFour = FALSE, BOOL bFlashIconFive = FALSE, BOOL bFlashIconSix = FALSE, INT iFlashTime = -1)
	INT FreeIndex = -1
	
	INT I
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT - 1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SIX_ICON_BAR, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	
	IF FreeIndex > -1
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SIX_ICON_BAR, FreeIndex)
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_TitleColour[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerOne[FreeIndex] = pPlayerOne
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerTwo[FreeIndex] = pPlayerTwo
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerThree[FreeIndex] = pPlayerThree
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFour[FreeIndex] = pPlayerFour
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFive[FreeIndex] = pPlayerFive
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerSix[FreeIndex] = pPlayerSix
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupOne[FreeIndex] = aPowerupOne
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupTwo[FreeIndex] = aPowerupTwo
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupThree[FreeIndex] = aPowerupThree
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFour[FreeIndex] = aPowerupFour
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFive[FreeIndex] = aPowerupFive
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupSix[FreeIndex] = aPowerupSix
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconOne[FreeIndex] = bFlashIconOne
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconTwo[FreeIndex] = bFlashIconTwo
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconThree[FreeIndex] = bFlashIconThree
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFour[FreeIndex] = bFlashIconFour
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFive[FreeIndex] = bFlashIconFive
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconSix[FreeIndex] = bFlashIconSix
		MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_iFlashTime[FreeIndex] = iFlashTime
	ENDIF
ENDPROC

PROC DRAW_GENERIC_SCORE(INT Number, STRING NumberTitle, INT iFlashingTime = -1, HUD_COLOURS aColour = HUD_COLOUR_WHITE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, STRING NumberString = NULL, BOOL isFloat = FALSE, FLOAT FloatValue = 0.0, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, HUD_COLOURS TitleColour = HUD_COLOUR_PURE_WHITE, BOOL DisplayWarning = FALSE, INT MaxNumber = 0 , BOOL DrawInfinity = FALSE, ACTIVITY_POWERUP aPowerup = ACTIVITY_POWERUP_NONE, HUD_COUNTER_STYLE eCounterStyle = HUD_COUNTER_STYLE_DEFAULT, BOOL bIsLiteralTitle = FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, INT iAlpha = 255, BOOL bDisplayBlankScore = FALSE, PLAYER_INDEX pPlayerID = NULL, BOOL bFlashTitle = FALSE, BOOL bDrawLineUnderName = FALSE, HUD_COLOURS LineUnderNameColour = HUD_COLOUR_WHITE, INT iTextLabelSubInt = -1)
	
	INT FreeIndex 	= -1
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SCORE, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SCORE, FreeIndex)		
		MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_Number[FreeIndex]  = Number
		MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_Title[FreeIndex]  = NumberTitle	
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Colour[FreeIndex] = aColour
		MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_FlashTimer[FreeIndex]  = iFlashingTime
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsPlayer[FreeIndex] = isPlayer
		MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_NumberString[FreeIndex] = NumberString
		MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_isFloat[FreeIndex] = isFloat
		MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_FloatValue[FreeIndex] = FloatValue
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_ColourFlash[FreeIndex] = ColourFlash
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_TitleColour[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_DisplayWarning[FreeIndex] = DisplayWarning
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_MaxNumber[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_DrawInfinity[FreeIndex] = DrawInfinity
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Powerup[FreeIndex] = aPowerup
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Style[FreeIndex] = eCounterStyle
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsLiteralTitle[FreeIndex] = bIsLiteralTitle
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_FleckColour[FreeIndex] = FleckColour
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_iAlpha[FreeIndex] = iAlpha
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bDisplayBlankScore[FreeIndex] = bDisplayBlankScore
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_pPlayerID[FreeIndex] = pPlayerID
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bFlashTitle[FreeIndex] = bFlashTitle
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bDrawLineUnderName[FreeIndex] = bDrawLineUnderName
		MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_LineUnderNameColour[FreeIndex] = LineUnderNameColour
		MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_TitleNumber[FreeIndex] = iTextLabelSubInt
		
//		SET_TITLE_JUSTIFICATION_SCORE(Number)
		
#IF USE_TU_CHANGES
		IF 	aPowerup = 	ACTIVITY_POWERUP_XP
		AND IS_LANGUAGE_NON_ROMANIC()
			MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE		
		ENDIF
		


		IF IS_PC_VERSION() 
			bool bProblematicResolution = false
			int resX, resY			
			GET_ACTUAL_SCREEN_RESOLUTION(resX,resY)
			if resX = 1280
			and resY >= 960
				bProblematicResolution = true
			endif

			
			IF Number > 99999999													
				MPGlobalsScoreHud.bTitleInsanePlusLeftJustified = TRUE		
			ELIF Number > 9999999 or bProblematicResolution						
				MPGlobalsScoreHud.bTitleInsaneLeftJustified = TRUE				
			ELIF Number > 999														//	Changed to move text left after $999, not $99,999 due to over lap in B*2312515						
				MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE					
			ENDIF

			IF IS_SCREEN_NARROW()
				MPGlobalsScoreHud.bTitleInsanePlusLeftJustified = TRUE
			ENDIF
			
		ENDIF
		
		
#ENDIF	//	USE_TU_CHANGES
		
//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_SCORE Index = -1 NOT DRAWING THE SCORE")
	ENDIF
ENDPROC

PROC DRAW_GENERIC_BIG_DOUBLE_NUMBER(INT Number, INT MaxNumber, STRING NumberTitle, INT iFlashingTime = -1, HUD_COLOURS aColour = HUD_COLOUR_WHITE, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, BOOL bDisplayWarning = FALSE, BOOL bUseNonPlayerFont = FALSE, HUD_COLOURS TitleColour = HUD_COLOUR_WHITE, BOOL bAdjustTitleWithNumIncrease = TRUE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, INT iAlpha = 255, BOOL bFlashTitle = FALSE)
	
	INT FreeIndex 	 = -1
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER, FreeIndex)
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_Number[FreeIndex] = Number
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_NumberTwo[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.sGenericDoubleNumber_Title[FreeIndex] = NumberTitle
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_FlashTimer[FreeIndex] = iFlashingTime
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_COLOUR[FreeIndex] = aColour
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bIsPlayer[FreeIndex] = isPlayer
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_ColourFlash[FreeIndex] = ColourFlash
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bDisplayWarning[FreeIndex] = bDisplayWarning
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bUseNonPlayerFont[FreeIndex] = bUseNonPlayerFont
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_TitleCOLOUR[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_FleckColour[FreeIndex] = FleckColour
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_iAlpha[FreeIndex] = iAlpha
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bFlashTitle[FreeIndex] = bFlashTitle
		
		IF Number > 9
		AND MaxNumber > 9
		AND IS_PC_VERSION() 
		AND bAdjustTitleWithNumIncrease
			MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE		
		ENDIF
		
//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_BIG_DOUBLE_NUMBER Index = -1 NOT DRAWING THE NUMBER")
	ENDIF
ENDPROC

PROC DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(INT Number, INT MaxNumber, STRING NumberTitle, HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT iFlashingTime = -1, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, HUD_COLOURS TitleColour = HUD_COLOUR_WHITE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, BOOL bCustomFont = FALSE, TEXT_FONTS eCustomFont = FONT_STANDARD)
	
	INT FreeIndex = -1	
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER_PLACE, FreeIndex)
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_Number[FreeIndex] = Number
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_NumberTwo[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.sGenericDoubleNumberPlace_Title[FreeIndex] = NumberTitle
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_COLOUR[FreeIndex] = PlacementColour
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_FlashTimer[FreeIndex] = iFlashingTime
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[FreeIndex] = WhichOrder
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bIsPlayer[FreeIndex] = isPlayer
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_ColourFlash[FreeIndex] = ColourFlash
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_TitleCOLOUR[FreeIndex] = TitleColour
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_FleckColour[FreeIndex] = FleckColour
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bCustomFont[FreeIndex] = bCustomFont
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_eCustomFont[FreeIndex] = eCustomFont
//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE Index = -1 NOT DRAWING THE NUMBER PLACE")
	ENDIF
ENDPROC

PROC DRAW_GENERIC_DOUBLE_TEXT(STRING sTitleLeft, STRING sTitleRight, BOOL bTitleLeftLiteral, BOOL bTitleRightLiteral, HUDORDER eWhichOrder = HUDORDER_DONTCARE, HUD_COLOURS eTitleColour = HUD_COLOUR_WHITE, BOOL bCustomFont = FALSE, TEXT_FONTS eCustomFont = FONT_STANDARD)
	
	INT FreeIndex = -1	
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_TEXT, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_TEXT, FreeIndex)
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleLeft[FreeIndex] = sTitleLeft
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleRight[FreeIndex] = sTitleRight
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleLeftLiteral[FreeIndex] = bTitleLeftLiteral
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleRightLiteral[FreeIndex] = bTitleRightLiteral
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[FreeIndex] = eWhichOrder
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_TitleCOLOUR[FreeIndex] = eTitleColour
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bCustomFont[FreeIndex] = bCustomFont
		MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_eCustomFont[FreeIndex] = eCustomFont
	ENDIF
ENDPROC

PROC DRAW_GENERIC_CHECKPOINT(INT Number, INT MaxNumber, STRING NumberTitle, HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT TitleNumber = -1, INT iFlashingTime = -1, HUDORDER WhichOrder = HUDORDER_DONTCARE, FLOAT XPos = -1.0, FLOAT YPos = -1.0, BOOL isPlayer = FALSE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, INT InBuiltMultiplyer = 0
							#IF USE_TU_CHANGES , BOOL DrawCross0 = FALSE, BOOL DrawCross1 = FALSE, BOOL DrawCross2 = FALSE, BOOL DrawCross3 = FALSE, BOOL DrawCross4 = FALSE, BOOL DrawCross5 = FALSE, BOOL DrawCross6 = FALSE, BOOL DrawCross7 = FALSE #ENDIF, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE)
	
	INT FreeIndex = -1	
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_CHECKPOINT, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR
	IF FreeIndex > -1 
		
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_CHECKPOINT, FreeIndex)
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_Number[FreeIndex] = Number
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_MaxNumber[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.sGenericCheckpoint_Title[FreeIndex] = NumberTitle
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_Colour[FreeIndex] = PlacementColour
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_iInBuiltMultiplyer[FreeIndex] =	InBuiltMultiplyer

		
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_TitleNumber[FreeIndex] = TitleNumber
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_FlashTimer[FreeIndex] = iFlashingTime
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[FreeIndex] = WhichOrder
		
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[FreeIndex].x = XPos
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[FreeIndex].y = YPos
		
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_bIsPlayer[FreeIndex] = isPlayer
		
		#IF USE_TU_CHANGES
		GenericCheckpoint_Cross0[FreeIndex] = DrawCross0
		GenericCheckpoint_Cross1[FreeIndex] = DrawCross1
		GenericCheckpoint_Cross2[FreeIndex] = DrawCross2
		GenericCheckpoint_Cross3[FreeIndex] = DrawCross3
		GenericCheckpoint_Cross4[FreeIndex] = DrawCross4
		GenericCheckpoint_Cross5[FreeIndex] = DrawCross5
		GenericCheckpoint_Cross6[FreeIndex] = DrawCross6
		GenericCheckpoint_Cross7[FreeIndex] = DrawCross7
		#ENDIF
		
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_ColourFlash[FreeIndex] = ColourFlash
		MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FleckColour[FreeIndex] = FleckColour
		
		MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE
		
		
//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_CHECKPOINT Index = -1 NOT DRAWING THE CHECKPOINT")
	ENDIF
ENDPROC

PROC DRAW_GENERIC_METER(INT Number, INT MaxNumber, STRING NumberTitle, HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT iFlashingTime = -1, HUDORDER WhichOrder = HUDORDER_DONTCARE, FLOAT XPos = -1.0, FLOAT YPos = -1.0, BOOL isPlayer = FALSE, BOOL OnlyZeroIsEmpty = TRUE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, BOOL bUseBigMeter = FALSE, INT iDrawRedDangerZonePercent = 0, BOOL isLiteralString=FALSE, PERCENTAGE_METER_LINE PercentageLine = PERCENTAGE_METER_LINE_NONE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TextColour = HUD_COLOUR_WHITE, BOOL bDrawLineUnderName = FALSE, HUD_COLOURS LineUnderNameColour = HUD_COLOUR_WHITE, BOOL MakeBarUrgent = FALSE, INT iUrgentPercentage = 0, HUD_COLOURS PulseToColour = HUD_COLOUR_WHITE, INT iPulseTime = -1, BOOL bUseScoreTitle = FALSE, INT iTextLabelSubInt = -1, FLOAT fNumber = -1.0, FLOAT fMaxNumber = -1.0, BOOL bUseSecondaryBar = FALSE, HUD_COLOURS eSecondaryBarColour = HUD_COLOUR_WHITE, FLOAT fSecondaryBarPercentage = 0.0, BOOL bTransparentSecBarIntersectingMainBar = FALSE, HUD_COLOURS eSecBarPulseToColour = HUD_COLOUR_WHITE, INT iSecBarPulseTime = -1, FLOAT fSecBarStartPercentage = 0.0, INT iGFXDrawOrder = -1, BOOL bCapMaxPercentage = TRUE)
			
	INT FreeIndex = -1	
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_METER, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR

	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_METER, FreeIndex)
		MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_Number[FreeIndex] = Number
		MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_MaxNumber[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_METER.sGenericMeter_Title[FreeIndex] = NumberTitle
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_Colour[FreeIndex] = PlacementColour
		MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_FlashTimer[FreeIndex] = iFlashingTime
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[FreeIndex] = WhichOrder
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[FreeIndex].x = XPos
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[FreeIndex].y = YPos
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsPlayer[FreeIndex] = isPlayer
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bOnlyZeroIsEmpty[FreeIndex] = OnlyZeroIsEmpty
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_ColourFlash[FreeIndex] = ColourFlash
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bBigMeter[FreeIndex] = bUseBigMeter
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iDrawRedDangerZonePercent[FreeIndex] = iDrawRedDangerZonePercent
		
		MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsLiteralString[FreeIndex] = isLiteralString		
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_PercentageLine[FreeIndex] = PercentageLine
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FleckColour[FreeIndex] = FleckColour
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_TextColour[FreeIndex] = TextColour
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bDrawLineUnderName[FreeIndex] = bDrawLineUnderName
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_LineUnderNameColour[FreeIndex] = LineUnderNameColour
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_MakeBarUrgent[FreeIndex] = MakeBarUrgent
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iUrgentPercentage[FreeIndex] = iUrgentPercentage
		
		MPGlobalsScoreHud.ElementHud_METER.PulseToColour[FreeIndex] = PulseToColour
		MPGlobalsScoreHud.ElementHud_METER.iPulseTime[FreeIndex] = iPulseTime
		
		MPGlobalsScoreHud.ElementHud_METER.bUseScoreTitle[FreeIndex] = bUseScoreTitle
		MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_TitleNumber[FreeIndex] = iTextLabelSubInt
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fNumber[FreeIndex] = fNumber
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fMaxNumber[FreeIndex] = fMaxNumber
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bUseSecondaryBar[FreeIndex] = bUseSecondaryBar
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_eSecondaryBarColour[FreeIndex] = eSecondaryBarColour
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fSecondaryBarPercentage[FreeIndex] = fSecondaryBarPercentage
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bTransparentSecBarIntersectingMainBar[FreeIndex] = bTransparentSecBarIntersectingMainBar
		
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_eSecBarPulseToColour[FreeIndex] = eSecBarPulseToColour
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iSecBarPulseTime[FreeIndex] = iSecBarPulseTime
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fSecBarStartPercentage[FreeIndex] = fSecBarStartPercentage
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iGFXDrawOrder[FreeIndex] = iGFXDrawOrder
		MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bCapMaxPercentage[FreeIndex] = bCapMaxPercentage
	ELSE
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("DRAW_GENERIC_METER Index = -1 NOT DRAWING THE METER")
		#ENDIF
	ENDIF
ENDPROC

PROC DRAW_LOOT_BAG_UI(LOOT_BAG_UI_DATA data)
	
	IF data.iItems <= 0
		SCRIPT_ASSERT("DRAW_LOOT_BAG_UI - Invalid number of items passed.")
		EXIT
	ENDIF
	
	INT i
	FLOAT fPercentageCount = data.fSpaceFilledPercent
		
	FOR i = (data.iItems - 1) TO 0 STEP -1
		fPercentageCount -= data.element[i].fSpaceUsedPerecnt
		
		IF data.element[i].fSpaceUsedPerecnt = 100
			DRAW_GENERIC_METER(0, 100, data.element[i].sLabel, HUD_COLOUR_WHITE, DEFAULT, data.element[i].eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GREY, DEFAULT, DEFAULT,
					  			 DEFAULT, DEFAULT, HUD_COLOUR_REDDARK, DEFAULT, DEFAULT, DEFAULT, data.fSpaceFilledPercent, DEFAULT, TRUE, HUD_COLOUR_WHITE, data.element[i].fSpaceUsedPerecnt, TRUE, HUD_COLOUR_GREYDARK, DEFAULT, fPercentageCount)
		ELSE
			DRAW_GENERIC_METER(0, 100, data.element[i].sLabel, HUD_COLOUR_GREY, DEFAULT, data.element[i].eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GREY, DEFAULT, DEFAULT,
					  			 DEFAULT, DEFAULT, HUD_COLOUR_REDDARK, DEFAULT, DEFAULT, DEFAULT, data.fSpaceFilledPercent, DEFAULT, TRUE, HUD_COLOUR_WHITE, data.element[i].fSpaceUsedPerecnt, TRUE, HUD_COLOUR_GREYDARK, DEFAULT, fPercentageCount)
		ENDIF
	ENDFOR
	
	DRAW_GENERIC_METER(0, 100, data.sBagLabel, HUD_COLOUR_WHITE, DEFAULT, data.eBagHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
					  			 DEFAULT, DEFAULT, HUD_COLOUR_REDDARK, DEFAULT, DEFAULT, DEFAULT, data.fSpaceFilledPercent, DEFAULT, TRUE, HUD_COLOUR_WHITE, 0, TRUE, HUD_COLOUR_GREYDARK, DEFAULT, 0.0)
	
	#IF IS_DEBUG_BUILD
		IF fPercentageCount != 0.0
			SCRIPT_ASSERT("DRAW_LOOT_BAG_UI - Items space used does not match total space used in loot bag")
		ENDIF
	#ENDIF
ENDPROC

PROC DRAW_GENERIC_SPRITE_METER(INT Number, INT MaxNumber, STRING NumberTitle, STRING SpriteName, HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT iFlashingTime = -1, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, BOOL OnlyZeroIsEmpty = TRUE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, BOOL isLiteralString=FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TextColour = HUD_COLOUR_WHITE)
	
		
	INT FreeIndex = -1	
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SPRITE_METER, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR

	IF FreeIndex > -1 

		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SPRITE_METER, FreeIndex)
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_Number[FreeIndex] = Number
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_MaxNumber[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_Title[FreeIndex] = NumberTitle
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_Colour[FreeIndex] = PlacementColour
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_FlashTimer[FreeIndex] = iFlashingTime
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[FreeIndex] = WhichOrder
		

		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsPlayer[FreeIndex] = isPlayer
		
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bOnlyZeroIsEmpty[FreeIndex] = OnlyZeroIsEmpty
		
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_ColourFlash[FreeIndex] = ColourFlash
		
		MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE		
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsLiteralString[FreeIndex] = isLiteralString		
		
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_FleckColour[FreeIndex] = FleckColour
		
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_TextColour[FreeIndex] = TextColour
		
		MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_SpriteName[FreeIndex] = SpriteName

//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_SPRITE_METER Index = -1 NOT DRAWING THE METER")
	ENDIF
ENDPROC

PROC DRAW_GENERIC_WEAPON_SPRITE_METER(INT Number, INT MaxNumber, STRING NumberTitle, WEAPON_TYPE WeaponType, HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT iFlashingTime = -1, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, BOOL OnlyZeroIsEmpty = TRUE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, BOOL isLiteralString=FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TextColour = HUD_COLOUR_WHITE)
	
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_DictionaryName = "MPKillQuota"
	DRAW_GENERIC_SPRITE_METER(Number, MaxNumber, NumberTitle, GET_WEAPONTYPE_TEXTURE(WeaponType),
		PlacementColour, iFlashingTime, WhichOrder, isPlayer, OnlyZeroIsEmpty, ColourFlashType, ColourFlash, isLiteralString,
		FleckColour, TextColour)
ENDPROC

PROC DRAW_GENERIC_MODEL_SPRITE_METER(INT Number, INT MaxNumber, STRING NumberTitle, MODEL_NAMES modelName, HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT iFlashingTime = -1, HUDORDER WhichOrder = HUDORDER_DONTCARE, BOOL isPlayer = FALSE, BOOL OnlyZeroIsEmpty = TRUE, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, BOOL isLiteralString=FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TextColour = HUD_COLOUR_WHITE)
	
	MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_DictionaryName = "MPAirCraft"
	
	PRINTLN("DRAW_GENERIC_MODEL_SPRITE_METER - modelName = ", GET_MODELNAME_TEXTURE(modelName))
	
	DRAW_GENERIC_SPRITE_METER(Number, MaxNumber, NumberTitle, GET_MODELNAME_TEXTURE(modelName),
		PlacementColour, iFlashingTime, WhichOrder, isPlayer, OnlyZeroIsEmpty, ColourFlashType, ColourFlash, isLiteralString,
		FleckColour, TextColour)
ENDPROC

PROC DRAW_GENERIC_ELIMINATION(INT MaxNumber, STRING Title = NULL, INT VisibleBoxes = -1,
						BOOL IsActive1 = FALSE, BOOL IsActive2 = FALSE,BOOL IsActive3 = FALSE,BOOL IsActive4 = FALSE,
						BOOL IsActive5 = FALSE, BOOL IsActive6 = FALSE,BOOL IsActive7 = FALSE,BOOL IsActive8 = FALSE,	//11
						HUD_COLOURS ColourValid = HUD_COLOUR_GREEN, HUD_COLOURS ColourInValid = HUD_COLOUR_RED, 
						INT FlashTime = -1,  HUDORDER WhichOrder = HUDORDER_DONTCARE, FLOAT XPos = -1.0, FLOAT YPos = -1.0, 
						BOOL isPlayer = FALSE, HUD_COLOURS Box1Colour = HUD_COLOUR_PURE_WHITE,  HUD_COLOURS Box2Colour = HUD_COLOUR_PURE_WHITE,
						HUD_COLOURS Box3Colour = HUD_COLOUR_PURE_WHITE,  HUD_COLOURS Box4Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box5Colour = HUD_COLOUR_PURE_WHITE, //23
						HUD_COLOURS Box6Colour = HUD_COLOUR_PURE_WHITE,HUD_COLOURS Box7Colour = HUD_COLOUR_PURE_WHITE,  HUD_COLOURS Box8Colour = HUD_COLOUR_PURE_WHITE, 
						HUD_COLOURS Box1Colour_Inactive = HUD_COLOUR_PURE_WHITE,  HUD_COLOURS Box2Colour_Inactive = HUD_COLOUR_PURE_WHITE,
						HUD_COLOURS Box3Colour_Inactive = HUD_COLOUR_PURE_WHITE,  HUD_COLOURS Box4Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box5Colour_Inactive = HUD_COLOUR_PURE_WHITE, 
						HUD_COLOURS Box6Colour_Inactive = HUD_COLOUR_PURE_WHITE,HUD_COLOURS Box7Colour_Inactive = HUD_COLOUR_PURE_WHITE,  HUD_COLOURS Box8Colour_Inactive = HUD_COLOUR_PURE_WHITE,
						INT ExtendedTimer = -1, HUDFLASHING ColourFlashType = HUDFLASHING_NONE, INT ColourFlash = 0, HUD_COLOURS TitleColour = HUD_COLOUR_PURE_WHITE				//38
						#IF USE_TU_CHANGES  , BOOL DrawCross0 = FALSE,BOOL DrawCross1 = FALSE,BOOL DrawCross2 = FALSE,BOOL DrawCross3 = FALSE,BOOL DrawCross4 = FALSE,BOOL DrawCross5 = FALSE,BOOL DrawCross6 = FALSE,BOOL DrawCross7 = FALSE #ENDIF , 
						BOOL bUseNonPlayerFont = FALSE, HUD_COLOURS FleckColour = HUD_COLOUR_PURE_WHITE #IF USE_TU_CHANGES  , HUD_COLOURS DrawCross0Colour = HUD_COLOUR_BLACK, HUD_COLOURS DrawCross1Colour = HUD_COLOUR_BLACK, HUD_COLOURS DrawCross2Colour = HUD_COLOUR_BLACK,
						HUD_COLOURS DrawCross3Colour = HUD_COLOUR_BLACK, HUD_COLOURS DrawCross4Colour = HUD_COLOUR_BLACK, HUD_COLOURS DrawCross5Colour = HUD_COLOUR_BLACK, HUD_COLOURS DrawCross6Colour = HUD_COLOUR_BLACK, HUD_COLOURS DrawCross7Colour = HUD_COLOUR_BLACK #ENDIF,
						BOOL bOverrideDontUseFarLeftJustified = FALSE)

	INT FreeIndex = -1	
	INT I 
	FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
		IF FreeIndex = -1
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, I) = FALSE
				FreeIndex = I
			ENDIF
		ENDIF
	ENDFOR

	IF FreeIndex > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELIMINATION, FreeIndex)

		MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_MaxNumber[FreeIndex] = MaxNumber
		MPGlobalsScoreHud.ElementHud_ELIMINATION.sGenericElimination_Title[FreeIndex] = Title
		MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_VisibleBoxes[FreeIndex] = VisibleBoxes
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive1[FreeIndex] = IsActive1
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive2[FreeIndex] = IsActive2
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive3[FreeIndex] = IsActive3
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive4[FreeIndex] = IsActive4
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive5[FreeIndex] = IsActive5
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive6[FreeIndex] = IsActive6
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive7[FreeIndex] = IsActive7
		MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive8[FreeIndex] = IsActive8
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFirst[FreeIndex] = ColourValid
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourSecond[FreeIndex] = ColourInValid
		MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_FlashTimer[FreeIndex] = FlashTime
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[FreeIndex] = WhichOrder
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[FreeIndex].x = XPos
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[FreeIndex].y = YPos
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bIsPlayer[FreeIndex] = isPlayer
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour[FreeIndex] = Box1Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour[FreeIndex] = Box2Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour[FreeIndex] = Box3Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour[FreeIndex] = Box4Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour[FreeIndex] = Box5Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour[FreeIndex] = Box6Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour[FreeIndex] = Box7Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour[FreeIndex] = Box8Colour
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour_InActive[FreeIndex] = Box1Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour_InActive[FreeIndex] = Box2Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour_InActive[FreeIndex] = Box3Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour_InActive[FreeIndex] = Box4Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour_InActive[FreeIndex] = Box5Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour_InActive[FreeIndex] = Box6Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour_InActive[FreeIndex] = Box7Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour_InActive[FreeIndex] = Box8Colour_Inactive
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_TitleColour[FreeIndex] = TitleColour	
		
		#IF USE_TU_CHANGES
		GenericElimination_Cross0[FreeIndex] = DrawCross0
		GenericElimination_Cross1[FreeIndex] = DrawCross1
		GenericElimination_Cross2[FreeIndex] = DrawCross2
		GenericElimination_Cross3[FreeIndex] = DrawCross3
		GenericElimination_Cross4[FreeIndex] = DrawCross4
		GenericElimination_Cross5[FreeIndex] = DrawCross5
		GenericElimination_Cross6[FreeIndex] = DrawCross6
		GenericElimination_Cross7[FreeIndex] = DrawCross7
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross0Colour[FreeIndex] = DrawCross0Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross1Colour[FreeIndex] = DrawCross1Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross2Colour[FreeIndex] = DrawCross2Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross3Colour[FreeIndex] = DrawCross3Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross4Colour[FreeIndex] = DrawCross4Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross5Colour[FreeIndex] = DrawCross5Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross6Colour[FreeIndex] = DrawCross6Colour
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross7Colour[FreeIndex] = DrawCross7Colour
		#ENDIF
		
		IF bOverrideDontUseFarLeftJustified		
			MPGlobalsScoreHud.bTitleFarLeftJustified = FALSE								
		ELSE
			MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE	
		ENDIF
						
		MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_ExtendedTimer[FreeIndex] = ExtendedTimer
		IF ExtendedTimer > -1
			SET_PROGRESSHUD_EXTENDDISPLAY(PROGRESSHUD_ELIMINATION, FreeIndex)
		ELSE
			CLEAR_PROGRESSHUD_EXTENDDISPLAY(PROGRESSHUD_ELIMINATION, FreeIndex)
		ENDIF
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFlashType[FreeIndex] = ColourFlashType
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFlash[FreeIndex] = ColourFlash
		
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bUseNonPlayerFont[FreeIndex] = bUseNonPlayerFont
		MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FleckColour[FreeIndex] = FleckColour
		
//	ELSE
//		NET_NL()NET_PRINT("DRAW_GENERIC_ELIMINATION Index = -1 NOT DRAWING THE ELIMINATION")
	ENDIF
ENDPROC


PROC DRAW_GENERIC_WIND_METER(STRING sTitle, INT iWindSpeed, FLOAT fWindDirection, INT iR, INT iG, INT iB, HUDORDER eHUDOrder)
	INT iFree = -1	
	INT idx 
	REPEAT MAX_NUMBER_HUD_ELEMENT idx
		IF (iFree = -1)
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_WINDMETER, idx) = FALSE
				iFree = idx
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	IF iFree > -1 
		MPGlobalsScoreHud.isSomethingDisplaying = TRUE
		SET_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_WINDMETER, iFree)
		
		MPGlobalsScoreHud.ElementHud_WIND.sGenericMeter_Title[iFree] = sTitle
		MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_WindSpeed[iFree] = iWindSpeed
		MPGlobalsScoreHud.ElementHud_WIND.fGenericMeter_Heading[iFree] = fWindDirection
		MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_RedComponent[iFree] = iR
		MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_BlueComponent[iFree] = iG
		MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_GreenComponent[iFree] = iB
		
		MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[iFree] = eHUDOrder
	ENDIF
ENDPROC















/// PURPOSE:
///    Draws the hud for the Rampages
/// PARAMS:
///    Number - The number of kills
///    NumberTitle - The Title of the big number of kills. Pass in "" to display KILLS
///    TimeRunner - The time displayed on the rampage
///    TimeTitle - The title of the timer on the rampage. Pass in "" to display TIME
///    ExtraTimeGiven - If the player is given extra time, display how much in Milliseconds
///    DisplayMilliseconds - Should the main timer have the milliseconds on show
///    iFlashingTime - how long to flash the timer for in milliseconds
///    cBigNumColor - the color of the big number it's default is HUD_COLOUR_RED [Brenda I added this param to fix B*848972 - Aaron G]
PROC DRAW_RAMPAGE_HUD(INT Number, STRING NumberTitle, INT TimeRunner, STRING TimeTitle, INT ExtraTimeGiven = 0, BOOL DisplayMilliseconds = FALSE, INT iFlashingTime = -1, HUD_COLOURS cBigNumColor = HUD_COLOUR_RED)
	
	IF IS_STRING_EMPTY_HUD(TimeTitle)
		TimeTitle = "TIMER_TIME"
	ENDIF
	TIMER_STYLE aTimerStyle
	IF DisplayMilliseconds = TRUE
		aTimerStyle = TIMER_STYLE_USEMILLISECONDS
	ELSE
		aTimerStyle = TIMER_STYLE_DONTUSEMILLISECONDS
	ENDIF
	DRAW_GENERIC_TIMER(TimeRunner, TimeTitle,ExtraTimeGiven, aTimerStyle, iFlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	
	IF IS_STRING_EMPTY_HUD(NumberTitle)
		NumberTitle = "TIMER_KILL"
	ENDIF
	
	DRAW_GENERIC_BIG_NUMBER(Number, NumberTitle, iFlashingTime, cBigNumColor, HUDORDER_SECONDBOTTOM)
	
ENDPROC

FUNC STRING GET_COMPARISON_STRING(INT iComparison)

	SWITCH iComparison
	
		CASE RACE_SPLIT_COMPARISON_WORLD
		
			RETURN "TIMER_WORLDTIME"
		BREAK
		
		CASE RACE_SPLIT_COMPARISON_FRIEND
		
			RETURN "FRIEND_WORLDTIME"
		BREAK
		
		CASE RACE_SPLIT_COMPARISON_CREW
		
			RETURN "CREW_WORLDTIME"
		BREAK
		
		CASE RACE_SPLIT_COMPARISON_PERSONAL
		
			RETURN "PERS_WORLDTIME"
		BREAK
	
	ENDSWITCH

	RETURN ""
ENDFUNC


/// PURPOSE:
///    Draws the Hud used in a race
/// PARAMS:
///    RaceTime - The timer
///    TimerTitle - The title of the timer. Defaults to TIME with "" passed in
///    LapNumber - Number of laps
///    LapMaxNumber - Max number of laps
///    LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
///    PositionNum - The position Number
///    PositionMaxNumber - The position maximum number
///    PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
///    ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
///    PlacementColour - The position numbers can change colour
///    CheckpointNumber - if you have a checkpoint bar the current number
///    CheckpointMaxNum - if you have a checkpoint bar, the maximum number
///    CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
///    CheckpointColour - the colour the bar should be
///    MeterNumber - if you want a meter displayed pass in the current value
///    MeterMaxNum - If you want a meter displayed pass in the max value
///    MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
///    MeterColour - The meter colour
///    BestTime - If you want to show a best time then pass in a millisecond value
///    BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
///    MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
///    DisplayMilliseconds - True if you want the main timer to display milliseconds
///    FlashingTime - How long you want the whole hud to flash for.
///    FloatTitle - The title the float score will have
///    FloatValue = value the float value will have
///    FloatColour - the colour the float value will have
PROC DRAW_RACE_HUD(INT RaceTime, STRING TimerTitle, INT LapNumber = -1, INT LapMaxNumber = -1, STRING LapTitle = NULL, 
					INT PositionNum = -1, INT PositionMaxNumber = -1, STRING PositionTitle = NULL, INT ExtraTimeGiven = 0,
					HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT CheckpointNumber = -1, INT CheckpointMaxNum = -1, 
					STRING CheckpointTitle = NULL, HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW,INT MeterNumber = -1, 
					INT MeterMaxNum = -1, STRING MeterTitle = NULL, HUD_COLOURS MeterColour = HUD_COLOUR_RED, 
					INT BestTime = -1, STRING BestTimeTitle = NULL, PODIUMPOS MedalDisplay = PODIUMPOS_NONE, BOOL DisplayMilliseconds = TRUE, INT FlashingTime = -1, 
					STRING FloatTitle = NULL, FLOAT FloatValue = -1.0, HUD_COLOURS FloatColour = HUD_COLOUR_WHITE, HUD_COLOURS aBestTimeColor = HUD_COLOUR_WHITE)

	
	IF FloatValue > -1
		DRAW_GENERIC_SCORE(0, "",FlashingTime, FloatColour, HUDORDER_SEVENTHBOTTOM, FALSE, FloatTitle, TRUE, FloatValue )
	ENDIF
	
	IF MeterNumber > -1
		STRING NewMeterTitle = MeterTitle
		IF IS_STRING_EMPTY_HUD(NewMeterTitle)
			NewMeterTitle = "TIM_DAMAGE"
		ENDIF
		DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, NewMeterTitle, MeterColour, FlashingTime, HUDORDER_SIXTHBOTTOM, -1, -1, FALSE, TRUE)
	ENDIF
	
	IF CheckpointNumber > -1
		STRING NewCheckpointTitle = CheckpointTitle
		IF IS_STRING_EMPTY_HUD(NewCheckpointTitle)
			NewCheckpointTitle = "TIM_CHECKPOIN"
		ENDIF
		CheckpointColour = CheckpointColour
		CheckpointMaxNum = CheckpointMaxNum
		/*
		DRAW_GENERIC_CHECKPOINT(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, -1, FlashingTime, HUDORDER_FIFTHBOTTOM)
		*/
	ENDIF
	

	IF PositionNum > -1
		STRING NewTitlePos1 = PositionTitle
		IF IS_STRING_EMPTY_HUD(NewTitlePos1)
			NewTitlePos1 = "TIM_POSIT"
		ENDIF
		
		DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(PositionNum, PositionMaxNumber,NewTitlePos1, PlacementColour, FlashingTime, HUDORDER_SEVENTHBOTTOM)
	ENDIF

	
	IF LapNumber > -1
		STRING NewTitle = LapTitle
		IF IS_STRING_EMPTY_HUD(NewTitle)
			NewTitle = "TIM_LAP"
		ENDIF
		LapNumber = LapNumber
		LapMaxNumber = LapMaxNumber
//		IF g_Show_Lap_Dpad 
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(LapNumber, LapMaxNumber,NewTitle, FlashingTime,HUD_COLOUR_WHITE, HUDORDER_SIXTHBOTTOM)
//		ENDIF
	ENDIF
	

	IF BestTime > -1

		STRING NewTimePos1 = BestTimeTitle
		IF IS_STRING_EMPTY_HUD(NewTimePos1)
			NewTimePos1 = "TIMER_BESLAP"
		ENDIF
		aBestTimeColor = aBestTimeColor
		MedalDisplay = MedalDisplay
		/*
		DRAW_GENERIC_TIMER(BestTime, NewTimePos1,0, TIMER_STYLE_USEMILLISECONDS, FlashingTime, MedalDisplay, HUDORDER_SECONDBOTTOM, FALSE, aBestTimeColor)
		*/
	ENDIF
	
	STRING NewTimePos = TimerTitle
	IF IS_STRING_EMPTY_HUD(NewTimePos)
		NewTimePos = "TIMER_TIME_RCE"
	ENDIF
	TIMER_STYLE aTimerStyle
	IF DisplayMilliseconds
		aTimerStyle = TIMER_STYLE_USEMILLISECONDS
	ELSE
		aTimerStyle = TIMER_STYLE_DONTUSEMILLISECONDS
	ENDIF
	DRAW_GENERIC_TIMER(RaceTime, NewTimePos,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	
ENDPROC

PROC DRAW_GTA_RACE_WEAPON_INFO()
	STRING title
	ACTIVITY_POWERUP icon
	IF g_VehicleSpikeInfo.bIsCollected = TRUE
	OR g_VehicleRocketInfo.bIsCollected = TRUE
	OR g_VehicleBoostInfo.bCollected = TRUE
		IF g_VehicleSpikeInfo.bIsCollected = TRUE
			title = "HUD_SPIKES"
			icon = ACTIVITY_POWERUP_SPIKES
		ELIF g_VehicleRocketInfo.bIsCollected = TRUE
			SWITCH g_VehicleRocketInfo.eRocketType
				CASE VEHICLE_ROCKET_NORMAL
					icon = ACTIVITY_POWERUP_ROCKETS
					title = "HUD_ROCKET"
				BREAK
				CASE VEHICLE_ROCKET_HOMING_ONLY
					icon = ACTIVITY_POWERUP_HOMING_ROCKETS
					title = "HUD_ROCKET_H"
				BREAK
				CASE VEHICLE_ROCKET_NON_HOMING_ONLY
					icon = ACTIVITY_POWERUP_ROCKETS
					title = "HUD_ROCKET_NH"
				BREAK
			ENDSWITCH
		ELIF g_VehicleBoostInfo.bCollected = TRUE
			title = "HUD_BOOSTS"
			icon = ACTIVITY_POWERUP_BOOSTS	
		ENDIF
		DRAW_GENERIC_SCORE(1, title,-1, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE,  "" , FALSE, 0, HUDFLASHING_NONE, 0, HUD_COLOUR_PURE_WHITE, FALSE, 0, FALSE, icon)
	ELIF g_VehicleGunInfo.bCollected = TRUE
		title = "HUD_VEH_GUN"
		icon = ACTIVITY_POWERUP_BULLET
		DRAW_GENERIC_SCORE(g_VehicleGunInfo.iBullets, title,-1, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE,  "" , FALSE, 0, HUDFLASHING_NONE, 0, HUD_COLOUR_PURE_WHITE, FALSE, 0, FALSE, icon)
	ENDIF
ENDPROC
//BOOL bUseBestAsWorldBest - Added for CGtoNG Stock Car Races (Country_Race.sc) as they aren't hooked up to leaderboards. url:bugstar:2056620
PROC DRAW_LOOP_RACE_HUD(INT RaceTime, INT iComparison, STRING TimerTitle, INT LapNumber = -1, INT LapMaxNumber = -1, STRING LapTitle = NULL, 
					INT PositionNum = -1, INT PositionMaxNumber = -1, STRING PositionTitle = NULL, INT ExtraTimeGiven = 0,
					HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT CheckpointNumber = -1, INT CheckpointMaxNum = -1, 
					STRING CheckpointTitle = NULL, HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW,INT MeterNumber = -1, 
					INT MeterMaxNum = -1, STRING MeterTitle = NULL, HUD_COLOURS MeterColour = HUD_COLOUR_RED, 
					INT BestTime = -1, STRING BestTimeTitle = NULL, INT CurrentLapTime = -1, STRING CurrentLapTimeTitle = NULL, PODIUMPOS MedalDisplay = PODIUMPOS_NONE, 
					BOOL DisplayMilliseconds = TRUE, INT FlashingTime = -1, STRING FloatTitle = NULL, FLOAT FloatValue = -1.0, HUD_COLOURS FloatColour = HUD_COLOUR_WHITE, 
					HUD_COLOURS aBestTimeColor = HUD_COLOUR_WHITE, FLOAT fDistanceCheck = -1.0,INT iWorldBestTime = -1, BOOL bTimeTrialMode = FALSE, INT iCheckPointTime = -1, INT iChallengeTime = -1,STRING sWorldName = NULL, BOOL bUseBestAsWorldBest = FALSE)
	
	
	TIMER_STYLE aTimerStyle
	IF DisplayMilliseconds
		aTimerStyle = TIMER_STYLE_USEMILLISECONDS
	ELSE
		aTimerStyle = TIMER_STYLE_DONTUSEMILLISECONDS
	ENDIF
	
	IF iChallengeTime <> -1
		DRAW_GENERIC_TIMER(iChallengeTime, "TIMER_CHALLTIME",ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_EIGHTHBOTTOM)
	ENDIF
	
	DRAW_GTA_RACE_WEAPON_INFO()
	
	IF FloatValue > -1
		DRAW_GENERIC_SCORE(0, "",FlashingTime, FloatColour, HUDORDER_EIGHTHBOTTOM, FALSE, FloatTitle, TRUE, FloatValue )
	ENDIF
	
	IF MeterNumber > -1
		STRING NewMeterTitle = MeterTitle
		IF IS_STRING_EMPTY_HUD(NewMeterTitle)
			NewMeterTitle = "TIM_DAMAGE"
		ENDIF
		DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, NewMeterTitle, MeterColour, FlashingTime, HUDORDER_EIGHTHBOTTOM, -1, -1, FALSE, TRUE)
	ENDIF
	
	IF NOT bTimeTrialMode
	OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		IF PositionNum > -1 
		AND PositionMaxNumber > 0
			STRING NewTitlePos1 = PositionTitle
			IF IS_STRING_EMPTY_HUD(NewTitlePos1)
				NewTitlePos1 = "TIM_POSIT"
			ENDIF
//			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
//				PositionMaxNumber = PositionMaxNumber+1 //Add one for tutorial to represent Lamar
//			ENDIF
			DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(PositionNum, PositionMaxNumber,NewTitlePos1, PlacementColour, FlashingTime, HUDORDER_FIFTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, PlacementColour)
		ENDIF
	ENDIF
	
	IF LapNumber > -1
		STRING NewTitle = LapTitle
		IF IS_STRING_EMPTY_HUD(NewTitle)
			NewTitle = "TIM_LAP"
		ENDIF
		LapNumber = LapNumber
		LapMaxNumber = LapMaxNumber
		IF g_Show_Lap_Dpad 
//			PRINTLN("g_Show_Lap_Dpad =", g_Show_Lap_Dpad)
			IF CAN_DISPLAY_DPADDOWN_PARTS()
				DRAW_GENERIC_BIG_DOUBLE_NUMBER(LapNumber, LapMaxNumber,NewTitle, FlashingTime,HUD_COLOUR_WHITE, HUDORDER_SIXTHBOTTOM)
			ENDIF
		ENDIF
	ENDIF
	
	IF CheckpointNumber > -1 AND CheckpointMaxNum > -1

		STRING NewCheckpointTitle = CheckpointTitle
		IF IS_STRING_EMPTY_HUD(NewCheckpointTitle)
			NewCheckpointTitle = "TIM_CHECKPOIN"
		ENDIF
		CheckpointColour = CheckpointColour
		
		IF g_Show_Lap_Dpad 
			DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, FlashingTime, HUDORDER_FIFTHBOTTOM)
		ENDIF
		
		/*
		IF CheckpointMaxNum < 8
			DRAW_GENERIC_CHECKPOINT(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, -1, FlashingTime, HUDORDER_FIFTHBOTTOM)
		ELSE
			
		ENDIF
		*/
	
	ENDIF
	
	IF fDistanceCheck > -1.0
		STRING DistanceTitle = "TIM_DISTANCE"
		STRING NumberString = "FMMC_LENGTHM"
		DRAW_GENERIC_SCORE(0, DistanceTitle, -1, HUD_COLOUR_WHITE, HUDORDER_FIFTHBOTTOM, FALSE, NumberString, TRUE, fDistanceCheck)
	ENDIF
	
	IF BestTime > -1
		STRING NewTimePos1 = BestTimeTitle
		IF IS_STRING_EMPTY_HUD(NewTimePos1)
			NewTimePos1 = "TIMER_BESLAP"
		ENDIF
		/*
		DRAW_GENERIC_TIMER(BestTime, NewTimePos1,0, TIMER_STYLE_USEMILLISECONDS, FlashingTime, MedalDisplay, HUDORDER_THIRDBOTTOM, FALSE, aBestTimeColor)
		*/
	ENDIF
	
	IF g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_P2P
	AND g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_BOAT_P2P
	AND g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_AIR_P2P
	AND g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_STUNT_P2P
	AND g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_TARGET_P2P
		IF CurrentLapTime > -1
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				STRING NewTimePos1 = CurrentLapTimeTitle
				IF IS_STRING_EMPTY_HUD(NewTimePos1)
					NewTimePos1 = "TIMER_CURLAP"
				ENDIF
				DRAW_GENERIC_TIMER(CurrentLapTime, NewTimePos1,0, TIMER_STYLE_USEMILLISECONDS, FlashingTime, MedalDisplay, HUDORDER_FOURTHBOTTOM, FALSE, aBestTimeColor)
			ENDIF
		ENDIF
	ENDIF
	
	STRING WorldBeatTime
	IF IS_STRING_EMPTY_HUD(WorldBeatTime)
		WorldBeatTime = GET_COMPARISON_STRING(iComparison)
	ENDIF
	
	//Displays words best time
	IF NOT bUseBestAsWorldBest
		IF bTimeTrialMode //This is time trial
		OR g_Show_Lap_Dpad 
		OR GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
			IF bTimeTrialMode
				IF g_Show_Lap_Dpad
				AND NOT IS_STRING_NULL_OR_EMPTY(sWorldName)
//					PRINTLN("3663830, A iWorldBestTime = ", iWorldBestTime)
					DRAW_GENERIC_TIMER(iWorldBestTime, sWorldName,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, (iWorldBestTime <= 0))
				ELSE
					IF iWorldBestTime <= 0
//						PRINTLN("3663830, B iWorldBestTime = ", iWorldBestTime)
						DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM,FALSE,HUD_COLOUR_WHITE,HUDFLASHING_NONE,0,TRUE)
					ELSE
//						PRINTLN("3663830, C iWorldBestTime = ", iWorldBestTime)
						DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM)
					ENDIF
				ENDIF
			ELSE
				IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
				AND NOT IS_STRING_NULL_OR_EMPTY(sWorldName)
//					PRINTLN("3663830, D iWorldBestTime = ", iWorldBestTime)
					DRAW_GENERIC_TIMER(iWorldBestTime, sWorldName,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, (iWorldBestTime <= 0))
				ELSE
					IF iWorldBestTime <= 0
//						PRINTLN("3663830, E iWorldBestTime = ", iWorldBestTime)
						DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM,FALSE,HUD_COLOUR_WHITE,HUDFLASHING_NONE,0,TRUE)
					ELSE
//						PRINTLN("3663830, F iWorldBestTime = ", iWorldBestTime)
						DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE //url:bugstar:2056620
		IF g_Show_Lap_Dpad 
		OR GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
			IF BestTime <= 0
				DRAW_GENERIC_TIMER(BestTime, BestTimeTitle,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM,FALSE,HUD_COLOUR_WHITE,HUDFLASHING_NONE,0,TRUE)
			ELSE
				DRAW_GENERIC_TIMER(BestTime, BestTimeTitle,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM)
			ENDIF
		ENDIF
	ENDIF
	
	STRING NewTimePos = TimerTitle
	IF IS_STRING_EMPTY_HUD(NewTimePos)
		NewTimePos = "TIMER_TIME_RCE"
	ENDIF
	
	//Used for main race time
	IF RaceTime > -1
		DRAW_GENERIC_TIMER(RaceTime, NewTimePos,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
	ENDIF
	
	STRING TimeDifference
	IF IS_STRING_EMPTY_HUD(TimeDifference)
		TimeDifference = "TIMER_CHALLTIME"
	ENDIF
	
	iCheckPointTime = iCheckPointTime
	/* Difference now displayed in top right, keep this incase it changes
	//Used for individual checkpoints times
	IF bTimeTrialMode //This is time trial
		//IF iCheckPointTime > -1
		DRAW_GENERIC_TIMER(iCheckPointTime, TimeDifference,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
		//ENDIF
	ENDIF
	*/

	/*
	IF IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_CHALLENGE)
		DRAW_GENERIC_TIMER(g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestTime
		, TimeDifference,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	ENDIF
	*/
	
	
ENDPROC

PROC DRAW_P2P_RACE_HUD(INT RaceTime, INT iComparison, STRING TimerTitle, INT PositionNum = -1, INT PositionMaxNumber = -1, STRING PositionTitle = NULL, INT ExtraTimeGiven = 0,
					HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE,INT CheckpointNumber = -1, INT CheckpointMaxNum = -1, STRING CheckpointTitle = NULL, HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW,
					BOOL DisplayMilliseconds = TRUE, INT FlashingTime = -1, 
					STRING FloatTitle = NULL, FLOAT FloatValue = -1.0, HUD_COLOURS FloatColour = HUD_COLOUR_WHITE,FLOAT fDistanceCheck = -1.0, INT iWorldBestTime = -1, BOOL bTimeTrialMode = FALSE,INT iCheckPointTime = -1, INT iChallengeTime = -1, STRING sWorldName = NULL)

	
	TIMER_STYLE aTimerStyle
	IF DisplayMilliseconds
		aTimerStyle = TIMER_STYLE_USEMILLISECONDS
	ELSE
		aTimerStyle = TIMER_STYLE_DONTUSEMILLISECONDS
	ENDIF
	
	
	IF iChallengeTime <> -1
		DRAW_GENERIC_TIMER(iChallengeTime, "TIMER_CHALLTIME",ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_EIGHTHBOTTOM)
	ENDIF
	
	DRAW_GTA_RACE_WEAPON_INFO()
		
	IF FloatValue > -1
		DRAW_GENERIC_SCORE(0, "",FlashingTime, FloatColour, HUDORDER_SIXTHBOTTOM, FALSE, FloatTitle, TRUE, FloatValue )
	ENDIF
	
	IF NOT bTimeTrialMode
	OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		IF PositionNum > -1
		AND PositionMaxNumber > 0
			STRING NewTitlePos1 = PositionTitle
			IF IS_STRING_EMPTY_HUD(NewTitlePos1)
				NewTitlePos1 = "TIM_POSIT"
			ENDIF
			
			DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(PositionNum, PositionMaxNumber,NewTitlePos1, PlacementColour, FlashingTime, HUDORDER_FIFTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, PlacementColour)
		ENDIF
	ENDIF
	
	
	IF CheckpointNumber > -1 AND CheckpointMaxNum > -1
		STRING NewCheckpointTitle = CheckpointTitle
		IF IS_STRING_EMPTY_HUD(NewCheckpointTitle)
			NewCheckpointTitle = "TIM_CHECKPOIN"
		ENDIF
		CheckpointColour = CheckpointColour
		
		IF g_Show_Lap_Dpad 
			DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, FlashingTime, HUDORDER_FIFTHBOTTOM)
		ENDIF
		
		/*
		IF CheckpointMaxNum < 8

			DRAW_GENERIC_CHECKPOINT(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, -1, FlashingTime, HUDORDER_FOURTHBOTTOM)
		ELSE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(CheckpointNumber, CheckpointMaxNum,NewCheckpointTitle, CheckpointColour, FlashingTime, HUDORDER_FOURTHBOTTOM)
		ENDIF
		*/
	ENDIF
	
	IF fDistanceCheck > -1.0
		STRING DistanceTitle = "TIM_DISTANCE"
		STRING NumberString = "FMMC_LENGTHM"
		DRAW_GENERIC_SCORE(0, DistanceTitle, -1, HUD_COLOUR_WHITE, HUDORDER_FOURTHBOTTOM, FALSE, NumberString, TRUE, fDistanceCheck)
	ENDIF
	
	
	STRING NewTimePos = TimerTitle
	IF IS_STRING_EMPTY_HUD(NewTimePos)
		NewTimePos = "TIMER_TIME_RCE"
	ENDIF
	
	DRAW_GENERIC_TIMER(RaceTime, NewTimePos,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_THIRDBOTTOM)
	
	STRING WorldBeatTime
	IF IS_STRING_EMPTY_HUD(WorldBeatTime)
		WorldBeatTime = GET_COMPARISON_STRING(iComparison)
	ENDIF
	
	
	IF bTimeTrialMode //This is time trial#
	OR g_Show_Lap_Dpad 
	OR GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
		IF bTimeTrialMode
			IF g_Show_Lap_Dpad
			AND NOT IS_STRING_NULL_OR_EMPTY(sWorldName)
				DRAW_GENERIC_TIMER(iWorldBestTime, sWorldName,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, ( iWorldBestTime <= 0 ))
			ELSE
				IF iWorldBestTime <= 0 //Has not been set yet so display dashes
					DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM,FALSE,HUD_COLOUR_WHITE,HUDFLASHING_NONE,0,TRUE)
				ELSE
					DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
				ENDIF
			ENDIF
		ELSE
			IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
			AND NOT IS_STRING_NULL_OR_EMPTY(sWorldName)
				DRAW_GENERIC_TIMER(iWorldBestTime, sWorldName,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, ( iWorldBestTime <= 0 ))
			ELSE
				IF iWorldBestTime <= 0 //Has not been set yet so display dashes
					DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM,FALSE,HUD_COLOUR_WHITE,HUDFLASHING_NONE,0,TRUE)
				ELSE
					DRAW_GENERIC_TIMER(iWorldBestTime, WorldBeatTime,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	STRING TimeDifference
	IF IS_STRING_EMPTY_HUD(TimeDifference)
		TimeDifference = "TIMER_CHALLTIME"
	ENDIF
	
	iCheckPointTime = iCheckPointTime
	/* Difference now displayed in top right, keep this incase it changes
	//Used for individual checkpoints times
	IF bTimeTrialMode //This is time trial
		//IF iCheckPointTime > -1
		DRAW_GENERIC_TIMER(iCheckPointTime, TimeDifference,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
		//ENDIF
	ENDIF
	*/
	
	/*
	IF IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_CHALLENGE)
		DRAW_GENERIC_TIMER(g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestTime
		, TimeDifference,ExtraTimeGiven, TIMER_STYLE_USEMILLISECONDS, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	ENDIF
	*/
	
	
	
ENDPROC

PROC DRAW_P2P_BASEJUMP_RACE_HUD(INT RacePoints, STRING TimerTitle,   STRING PositionTitle = NULL,
					HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE,INT CheckpointNumber = -1, INT CheckpointMaxNum = -1, STRING CheckpointTitle = NULL, HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW,
					INT FlashingTime = -1, 
					STRING FloatTitle = NULL, FLOAT FloatValue = -1.0, HUD_COLOURS FloatColour = HUD_COLOUR_WHITE, INT opponents_score = -1, STRING bj_leaders_name = NULL, INT iChallengeScore = -1)

	
	IF iChallengeScore <> -1
		DRAW_GENERIC_BIG_NUMBER(iChallengeScore, "SCORE_CHALLENGE", FlashingTime, PlacementColour)
	ENDIF
	
	IF FloatValue > -1
		DRAW_GENERIC_SCORE(0, "",FlashingTime, FloatColour, HUDORDER_SEVENTHBOTTOM, FALSE, FloatTitle, TRUE, FloatValue )
	ENDIF
	
	/*
	IF PositionNum > -1
		STRING NewTitlePos1 = PositionTitle
		IF IS_STRING_EMPTY_HUD(NewTitlePos1)
			NewTitlePos1 = "TIM_POSIT"	//POSITION
		ENDIF
		
		DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(PositionNum, PositionMaxNumber,NewTitlePos1, PlacementColour, FlashingTime, HUDORDER_FIFTHBOTTOM)
		
	ENDIF
	*/
	
	// Draw players points BIG
	
	STRING NewTitlePos1 = PositionTitle
	IF IS_STRING_EMPTY_HUD(NewTitlePos1)
		NewTitlePos1 = "BASEPLP"	//Players Points
	ENDIF
	DRAW_GENERIC_BIG_NUMBER(RacePoints, NewTitlePos1, FlashingTime, PlacementColour,  HUDORDER_FOURTHBOTTOM, FALSE )
	//DRAW_GENERIC_BIG_NUMBER (RacePoints, "SCORE", FlashingTime, PlacementColour,  HUDORDER_FOURTHBOTTOM, TRUE )
	IF CheckpointNumber > -1 AND CheckpointMaxNum > -1
		STRING NewCheckpointTitle = CheckpointTitle
		IF IS_STRING_EMPTY_HUD(NewCheckpointTitle)
			NewCheckpointTitle = "TIM_CHECKPOIN"		// checkpoints
		ENDIF
		
		// removed as per Les's request. 977179
		
		//IF CheckpointMaxNum < 8
		//	DRAW_GENERIC_CHECKPOINT(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, -1, FlashingTime, HUDORDER_FOURTHBOTTOM)
	//	ELSE
	//		DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(CheckpointNumber, CheckpointMaxNum,NewCheckpointTitle, CheckpointColour, FlashingTime, HUDORDER_FOURTHBOTTOM)
	//	ENDIF
		IF CheckpointColour = HUD_COLOUR_YELLOW
			CheckpointColour = HUD_COLOUR_YELLOW
		ENDIF
		//DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(CheckpointNumber, CheckpointMaxNum,NewCheckpointTitle, CheckpointColour, FlashingTime, HUDORDER_FIFTHBOTTOM)
	ENDIF

	//opponents
	
	IF  opponents_score != -1
		STRING NewTimePos = bj_leaders_name
		IF IS_STRING_EMPTY_HUD(NewTimePos)
			NewTimePos = timertitle
			NewTimePos = "BASEOPPOINT"// bj_leaders_name		//points
			//NewTimePos += "BASEPLP"		//points
		//	GET_PLAYER_NAME( PLAYER_INDEX PlayerIndex )
		ENDIF
		
		NewTimePos = "BASEOPPOINT"
		
		DRAW_GENERIC_SCORE(opponents_score, NewTimePos, -1,PlacementColour,  HUDORDER_SIXTHBOTTOM)
		//DRAW_TEXT_WITH_PLAYER_NAME(TitlePlace, TitleStyle, Title, "", HUD_COLOUR_WHITE, FONT_RIGHT)
	ENDIF
	//DRAW_GENERIC_TIMER(RacePoints, NewTimePos,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
ENDPROC

/// PURPOSE:
///    Draws the Hud used in a race
/// PARAMS:
///    RaceTime - The timer
///    TimerTitle - The title of the timer. Defaults to TIME with "" passed in
///    LapNumber - Number of laps
///    LapMaxNumber - Max number of laps
///    LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
///    PositionNum - The position Number
///    PositionMaxNumber - The position maximum number
///    PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
///    ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
///    PlacementColour - The position numbers can change colour
///    CheckpointNumber - if you have a checkpoint bar the current number
///    CheckpointMaxNum - if you have a checkpoint bar, the maximum number
///    CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
///    CheckpointColour - the colour the bar should be
///    MeterNumber - if you want a meter displayed pass in the current value
///    MeterMaxNum - If you want a meter displayed pass in the max value
///    MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
///    MeterColour - The meter colour
///    BestTime - If you want to show a best time then pass in a millisecond value
///    BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
///    MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
///    DisplayMilliseconds - True if you want the main timer to display milliseconds
///    FlashingTime - How long you want the whole hud to flash for.
///    FloatTitle - The title the float score will have
///    FloatValue = value the float value will have
///    FloatColour - the colour the float value will have
PROC DRAW_CHECKPOINT_COUNTDOWN_RACE_HUD(INT RaceTime, STRING TimerTitle, INT LapNumber = -1, INT LapMaxNumber = -1, STRING LapTitle = NULL, 
					INT PositionNum = -1, INT PositionMaxNumber = -1, STRING PositionTitle = NULL, INT ExtraTimeGiven = 0,
					HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT CheckpointNumber = -1, INT CheckpointMaxNum = -1, 
					STRING CheckpointTitle = NULL, HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW,INT MeterNumber = -1, 
					INT MeterMaxNum = -1, STRING MeterTitle = NULL, HUD_COLOURS MeterColour = HUD_COLOUR_RED, 
					INT BestTime = -1, STRING BestTimeTitle = NULL, PODIUMPOS MedalDisplay = PODIUMPOS_NONE, BOOL DisplayMilliseconds = TRUE, INT FlashingTime = -1, 
					STRING FloatTitle = NULL, FLOAT FloatValue = -1.0, HUD_COLOURS FloatColour = HUD_COLOUR_WHITE, HUD_COLOURS aBestTimeColor = HUD_COLOUR_WHITE,
					BOOL bDispTimeString = FALSE)

	
	IF FloatValue > -1
		DRAW_GENERIC_SCORE(0, "",FlashingTime, FloatColour, HUDORDER_SEVENTHBOTTOM, FALSE, FloatTitle, TRUE, FloatValue )
	ENDIF
	
	IF MeterNumber > -1
		STRING NewMeterTitle = MeterTitle
		IF IS_STRING_EMPTY_HUD(NewMeterTitle)
			NewMeterTitle = "TIM_DAMAGE"
		ENDIF
		DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, NewMeterTitle, MeterColour, FlashingTime, HUDORDER_SIXTHBOTTOM, -1, -1, FALSE, TRUE)
	ENDIF
	
	IF CheckpointNumber > -1
		STRING NewCheckpointTitle = CheckpointTitle
		IF IS_STRING_EMPTY_HUD(NewCheckpointTitle)
			NewCheckpointTitle = "TIM_CHECKPOIN"
		ENDIF
		//DRAW_GENERIC_CHECKPOINT(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, -1, FlashingTime, HUDORDER_FIFTHBOTTOM)
		//DRAW_GENERIC_BIG_NUMBER(CheckpointMaxNum-CheckpointNumber, NewCheckpointTitle, FlashingTime, CheckpointColour, HUDORDER_FIFTHBOTTOM)
		//DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(CheckpointNumber, CheckpointMaxNum,NewCheckpointTitle, CheckpointColour, FlashingTime, HUDORDER_FIFTHBOTTOM)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(CheckpointNumber, CheckpointMaxNum,NewCheckpointTitle, FlashingTime,CheckpointColour, HUDORDER_FIFTHBOTTOM)
	ENDIF
	
	IF PositionNum > -1
		STRING NewTitlePos1 = PositionTitle
		IF IS_STRING_EMPTY_HUD(NewTitlePos1)
			NewTitlePos1 = "TIM_POSIT"
		ENDIF
		
		DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(PositionNum, PositionMaxNumber,NewTitlePos1, PlacementColour, FlashingTime, HUDORDER_FOURTHBOTTOM)
	ENDIF
	
	IF LapNumber > -1
		STRING NewTitle = LapTitle
		IF IS_STRING_EMPTY_HUD(NewTitle)
			NewTitle = "TIM_LAP"
		ENDIF
		LapNumber = LapNumber
		LapMaxNumber = LapMaxNumber
		/*
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(LapNumber, LapMaxNumber,NewTitle, FlashingTime,HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM)
		*/
	ENDIF

	IF BestTime > -1
		STRING NewTimePos1 = BestTimeTitle
		IF IS_STRING_EMPTY_HUD(NewTimePos1)
			IF NOT bDispTimeString
				NewTimePos1 = "TIMER_BESLAP"
			ELSE
				NewTimePos1 = "TIMER_BESTIME"
			ENDIF
		ENDIF
		DRAW_GENERIC_TIMER(BestTime, NewTimePos1,0, TIMER_STYLE_USEMILLISECONDS, FlashingTime, MedalDisplay, HUDORDER_SECONDBOTTOM, FALSE, aBestTimeColor)
	ENDIF
	
	STRING NewTimePos = TimerTitle
	IF IS_STRING_EMPTY_HUD(NewTimePos)
		NewTimePos = "TIMER_TIME_RCE"
	ENDIF
	TIMER_STYLE aTimerStyle
	IF DisplayMilliseconds
		aTimerStyle = TIMER_STYLE_USEMILLISECONDS
	ELSE
		aTimerStyle = TIMER_STYLE_DONTUSEMILLISECONDS
	ENDIF
	DRAW_GENERIC_TIMER(RaceTime, NewTimePos,ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	
ENDPROC

/// PURPOSE:
///    Draws the Hud used in a race
/// PARAMS:
///    RaceTime - The timer
///    TimerTitle - The title of the timer. Defaults to TIME with "" passed in
///    LapNumber - Number of laps
///    LapMaxNumber - Max number of laps
///    LapTitle - The title of the lap meter. Defaults to LAP with "" Passed in
///    PositionNum - The position Number
///    PositionMaxNumber - The position maximum number
///    PositionTitle - The title of the position number. Defaults to POSITION with "" Passed in 
///    ExtraTimeGiven - If any extra time is given to the timer, pass this in to display +xs -xs
///    PlacementColour - The position numbers can change colour
///    CheckpointNumber - if you have a checkpoint bar the current number
///    CheckpointMaxNum - if you have a checkpoint bar, the maximum number
///    CheckpointTitle - The title of the checkpoint bar, defaults to CHECKPOINT with "" passed in
///    CheckpointColour - the colour the bar should be
///    MeterNumber - if you want a meter displayed pass in the current value
///    MeterMaxNum - If you want a meter displayed pass in the max value
///    MeterTitle - the meters title. Defaults to DAMAGE with "" Passed in
///    MeterColour - The meter colour
///    BestTime - If you want to show a best time then pass in a millisecond value
///    BestTimeTitle - Title of the best time timer. Defaults to BEST TIME with "" passed in
///    MedalDisplay - If you want to show a medal postion next to the Best timer then pass in a position. Used for a target times.
///    DisplayMilliseconds - True if you want the main timer to display milliseconds
///    FlashingTime - How long you want the whole hud to flash for.
///    FloatTitle - The title the float score will have
///    FloatValue = value the float value will have
///    FloatColour - the colour the float value will have
PROC DRAW_STUNT_PLANE_HUD(INT RaceTime, STRING TimerTitle, INT LapNumber = -1, INT LapMaxNumber = -1, STRING LapTitle = NULL, 
					INT PositionNum = -1, INT PositionMaxNumber = -1, STRING PositionTitle = NULL, INT ExtraTimeGiven = 0,
					HUD_COLOURS PlacementColour = HUD_COLOUR_WHITE, INT CheckpointNumber = -1, INT CheckpointMaxNum = -1, 
					STRING CheckpointTitle = NULL, HUD_COLOURS CheckpointColour = HUD_COLOUR_YELLOW,INT MeterNumber = -1, 
					INT MeterMaxNum = -1, STRING MeterTitle = NULL, HUD_COLOURS MeterColour = HUD_COLOUR_RED, 
					INT GoalTime = -1, STRING GoalTimeTitle = NULL, PODIUMPOS GoalMedalDisplay = PODIUMPOS_NONE, HUD_COLOURS aGoalTimeColor = HUD_COLOUR_WHITE,
					INT BestTime = -1, STRING BestTimeTitle = NULL, PODIUMPOS MedalDisplay = PODIUMPOS_NONE, HUD_COLOURS aBestTimeColor = HUD_COLOUR_WHITE, BOOL DisplayMilliseconds = TRUE, INT FlashingTime = -1, 
					STRING FloatTitle = NULL, FLOAT FloatValue = -1.0, HUD_COLOURS FloatColour = HUD_COLOUR_WHITE)

	
	IF FloatValue > -1
		DRAW_GENERIC_SCORE(0, "",FlashingTime, FloatColour, HUDORDER_SEVENTHBOTTOM, FALSE, FloatTitle, TRUE, FloatValue )
	ENDIF
	
	IF MeterNumber > -1
		STRING NewMeterTitle = MeterTitle
		IF IS_STRING_EMPTY_HUD(NewMeterTitle)
			NewMeterTitle = "TIM_DAMAGE"
		ENDIF
		DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, NewMeterTitle, MeterColour, FlashingTime, HUDORDER_SIXTHBOTTOM, -1, -1, FALSE, TRUE)
	ENDIF
	
	IF CheckpointNumber > -1
		STRING NewCheckpointTitle = CheckpointTitle
		IF IS_STRING_EMPTY_HUD(NewCheckpointTitle)
			NewCheckpointTitle = "TIM_CHECKPOIN"
		ENDIF
//		DRAW_GENERIC_CHECKPOINT(CheckpointNumber, CheckpointMaxNum, NewCheckpointTitle, CheckpointColour, -1, FlashingTime, HUDORDER_FIFTHBOTTOM)
		DRAW_GENERIC_BIG_NUMBER(CheckpointMaxNum-CheckpointNumber, NewCheckpointTitle, FlashingTime, CheckpointColour, HUDORDER_FIFTHBOTTOM)
	ENDIF
	
	IF PositionNum > -1
		STRING NewTitlePos1 = PositionTitle
		IF IS_STRING_EMPTY_HUD(NewTitlePos1)
			NewTitlePos1 = "TIM_POSIT"
		ENDIF
		
		DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(PositionNum, PositionMaxNumber,NewTitlePos1, PlacementColour, FlashingTime, HUDORDER_FOURTHBOTTOM)
	ENDIF
	
	IF LapNumber > -1
		STRING NewTitle = LapTitle
		IF IS_STRING_EMPTY_HUD(NewTitle)
			NewTitle = "TIM_LAP"
		ENDIF
		LapNumber = LapNumber
		LapMaxNumber = LapMaxNumber
		/*
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(LapNumber, LapMaxNumber,NewTitle, FlashingTime,HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM)
		*/
	ENDIF
	
	IF GoalTime > -1
		STRING NewTimePos2 = GoalTimeTitle
		IF IS_STRING_EMPTY_HUD(NewTimePos2)
			NewTimePos2 = "TIMER_BESTIME"
		ENDIF
		DRAW_GENERIC_TIMER(GoalTime, NewTimePos2,0, TIMER_STYLE_USEMILLISECONDS, FlashingTime, GoalMedalDisplay, HUDORDER_SECONDBOTTOM, FALSE, aGoalTimeColor)
	ENDIF
	
	IF BestTime > -1
		STRING NewTimePos1 = BestTimeTitle
		IF IS_STRING_EMPTY_HUD(NewTimePos1)
			NewTimePos1 = "TIMER_BESTIME"
		ENDIF
		DRAW_GENERIC_TIMER(BestTime, NewTimePos1,0, TIMER_STYLE_USEMILLISECONDS, FlashingTime, MedalDisplay, HUDORDER_THIRDBOTTOM, FALSE, aBestTimeColor)
	ENDIF
	
	TEXT_LABEL_63 NewTimePos = TimerTitle
	IF (ExtraTimeGiven < 0)
		NewTimePos = "TIMER_REWARD"
	ELIF (ExtraTimeGiven > 0)
		NewTimePos = "TIMER_PENAL"
	ELIF IS_STRING_EMPTY_HUD(NewTimePos)
		NewTimePos = "TIM_TIMER"
	ENDIF
	DisplayMilliseconds = DisplayMilliseconds
	TIMER_STYLE aTimerStyle	= TIMER_STYLE_STUNTPLANE
	DRAW_GENERIC_TIMER(RaceTime, NewTimePos, ExtraTimeGiven, aTimerStyle, FlashingTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)	
ENDPROC




/// PURPOSE:
///    Draws the hud for the shooting range.
PROC DRAW_SHOOTING_RANGE_HUD(Range_HUD_Data & sHUDDrawData, BOOL bColouriseNames = FALSE)
	// Multiplier first. If we have multiplier data, draw it.
	IF (sHUDDrawData.iMultiplier > -1) AND (sHUDDrawData.iCurMultiplierBlocks > -1) AND (sHUDDrawData.iMaxMultiplierBlocks > -1)
		DRAW_GENERIC_CHECKPOINT(sHUDDrawData.iCurMultiplierBlocks, sHUDDrawData.iMaxMultiplierBlocks, "MULTIPLIER", 
								sHUDDrawData.eMultiplierColor, sHUDDrawData.iMultiplier, 
								-1, HUDORDER_SEVENTHBOTTOM, default, default, default, default, default, sHUDDrawData.iMultiplier)
	ENDIF

	// Next, target data. If we have target data, draw it.
	IF (sHUDDrawData.iCurTargets > -1) AND (sHUDDrawData.iMaxTargets > -1)
		IF IS_STRING_NULL_OR_EMPTY(sHUDDrawData.sTargetsLabel)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sHUDDrawData.iCurTargets, sHUDDrawData.iMaxTargets, "TIMER_TAR", -1, 
					HUD_COLOUR_WHITE, HUDORDER_SIXTHBOTTOM)
		ELSE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sHUDDrawData.iCurTargets, sHUDDrawData.iMaxTargets, sHUDDrawData.sTargetsLabel, -1, 
					HUD_COLOUR_WHITE, HUDORDER_SIXTHBOTTOM)
		ENDIF
	ENDIF
	
	// Next draw best score, if we have one.
	IF (sHUDDrawData.iBestScore > 0)
		IF IS_STRING_NULL_OR_EMPTY(sHUDDrawData.sCurScoreLabel)
			DRAW_GENERIC_SCORE(sHUDDrawData.iBestScore, "TIMER_BESTSCORE", -1, sHUDDrawData.eBestScoreColor, 
					HUDORDER_FIFTHBOTTOM, sHUDDrawData.bDrawPlayerNames)
		ELSE
			DRAW_GENERIC_SCORE(sHUDDrawData.iBestScore, sHUDDrawData.sCurScoreLabel, -1, sHUDDrawData.eBestScoreColor, 
					HUDORDER_FIFTHBOTTOM, sHUDDrawData.bDrawPlayerNames)
		ENDIF
	ENDIF
	
	// Goal score.
	IF (sHUDDrawData.iGoalScore > -1)
		IF IS_STRING_NULL_OR_EMPTY(sHUDDrawData.sGoalScoreLabel)
			DRAW_GENERIC_SCORE(sHUDDrawData.iGoalScore, "TIMER_GOAL", -1, sHUDDrawData.eGoalScoreColor, 
					HUDORDER_FOURTHBOTTOM)
		ELSE
			DRAW_GENERIC_SCORE(sHUDDrawData.iGoalScore, sHUDDrawData.sGoalScoreLabel, -1, sHUDDrawData.eGoalScoreColor, 
					HUDORDER_FOURTHBOTTOM)
		ENDIF
	ENDIF
	
	// Next, the current score.
	IF (sHUDDrawData.iCurScore <> -1)		
		IF IS_STRING_NULL_OR_EMPTY(sHUDDrawData.sCurScoreLabel)
			DRAW_GENERIC_SCORE(sHUDDrawData.iCurScore, "TIMER_SCORE", -1, sHUDDrawData.eCurScoreColor, 
					HUDORDER_THIRDBOTTOM, sHUDDrawData.bDrawPlayerNames)
		ELSE
			IF bColouriseNames
				DRAW_GENERIC_SCORE(sHUDDrawData.iCurScore, sHUDDrawData.sCurScoreLabel, -1, sHUDDrawData.eCurScoreColor, 
						HUDORDER_THIRDBOTTOM, sHUDDrawData.bDrawPlayerNames, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sHUDDrawData.eCurScoreColor)
			ELSE
				DRAW_GENERIC_SCORE(sHUDDrawData.iCurScore, sHUDDrawData.sCurScoreLabel, -1, sHUDDrawData.eCurScoreColor, 
						HUDORDER_THIRDBOTTOM, sHUDDrawData.bDrawPlayerNames)
			ENDIF
		ENDIF
	ENDIF
	
	
	
	// Opponent score.
	IF (sHUDDrawData.iOpponentScore <> -1)
		STRING sNumberString = PICK_STRING(sHUDDrawData.eOpponentScoreColor = HUD_COLOUR_GREEN, "SHR_PLUS_BONUS", "")
		IF bColouriseNames
			DRAW_GENERIC_SCORE(sHUDDrawData.iOpponentScore, sHUDDrawData.sOpponentScoreLabel, -1, sHUDDrawData.eOpponentScoreColor, 
					HUDORDER_SECONDBOTTOM, sHUDDrawData.bDrawPlayerNames, sNumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sHUDDrawData.eOpponentScoreColor)
		ELSE
			DRAW_GENERIC_SCORE(sHUDDrawData.iOpponentScore, sHUDDrawData.sOpponentScoreLabel, -1, sHUDDrawData.eOpponentScoreColor, 
					HUDORDER_SECONDBOTTOM, sHUDDrawData.bDrawPlayerNames, sNumberString)
		ENDIF
	ENDIF
	
	// Timer.
	IF (sHUDDrawData.iTimerVal > -1)
		IF IS_STRING_NULL_OR_EMPTY(sHUDDrawData.sTimerLabel)
			DRAW_GENERIC_TIMER(sHUDDrawData.iTimerVal, "TIM_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, 
					PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, sHUDDrawData.eTimerColor, DEFAULT, DEFAULT, DEFAULT, sHUDDrawData.eTimerColor)
		ELSE
			DRAW_GENERIC_TIMER(sHUDDrawData.iTimerVal, sHUDDrawData.sTimerLabel, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, 
					PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, sHUDDrawData.eTimerColor, DEFAULT, DEFAULT, DEFAULT, sHUDDrawData.eTimerColor)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Used for the shooting range, in cases where ONLY the timer needs to be drawn.
PROC DRAW_SHOOTING_RANGE_HUD_TIMERONLY(INT iTimerVal, HUD_COLOURS eColor)
	DRAW_GENERIC_TIMER(iTimerVal, "TIM_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, 
			PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, eColor, DEFAULT, DEFAULT, DEFAULT, eColor)
ENDPROC



/// PURPOSE:
///    Draws 2 checkpoint bars. With the bottom can change colour
/// PARAMS:
///    LocalCollected - The top bars collected value
///    LocalLimit - The Max the top bar can collect
///    LocalTitle - The title of the top bar
///    OverallCollected - The bottom bar current level
///    OverallLimit - The bottom bar max level
///    OverallTitle - The bottom bars title 
///    BottomColour - The colour the bottom bar should take
///    FlashingTime - The flashing timer in milliseconds
PROC DRAW_COLLECTION_HUD(INT LocalCollected, INT LocalLimit, STRING LocalTitle, INT OverallCollected, INT OverallLimit, STRING OverallTitle, HUD_COLOURS BottomColour = HUD_COLOUR_BLUE, INT FlashingTime = -1)

	DRAW_GENERIC_CHECKPOINT(LocalCollected, LocalLimit, LocalTitle, HUD_COLOUR_YELLOW, -1, FlashingTime, HUDORDER_SECONDBOTTOM)
	
	DRAW_GENERIC_CHECKPOINT(OverallCollected, OverallLimit, OverallTitle, BottomColour, -1, FlashingTime, HUDORDER_BOTTOM)
	
ENDPROC



PROC DRAW_TWO_PACKAGES_EIGHT_HUD(INT LocalLimit,STRING LocalTitle, INT OverallLimit, STRING OverallTitle, HUD_COLOURS TopBox1Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox2Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox3Colour = HUD_COLOUR_BLACK,
					HUD_COLOURS TopBox4Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox5Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox6Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox7Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox8Colour = HUD_COLOUR_BLACK,
					HUD_COLOURS BottomBox1Colour = HUD_COLOUR_BLACK, HUD_COLOURS BottomBox2Colour = HUD_COLOUR_BLACK, HUD_COLOURS BottomBox3Colour = HUD_COLOUR_BLACK, HUD_COLOURS BottomBox4Colour = HUD_COLOUR_BLACK, HUD_COLOURS BottomBox5Colour = HUD_COLOUR_BLACK,
					HUD_COLOURS BottomBox6Colour = HUD_COLOUR_BLACK, HUD_COLOURS BottomBox7Colour = HUD_COLOUR_BLACK, HUD_COLOURS BottomBox8Colour = HUD_COLOUR_BLACK, INT FlashingTime = -1, HUD_COLOURS TitleColour1 = HUD_COLOUR_PURE_WHITE, HUD_COLOURS TitleColour2 = HUD_COLOUR_PURE_WHITE
					#IF USE_TU_CHANGES , BOOL DoTopCross0 = FALSE, BOOL DoTopCross1 = FALSE,BOOL DoTopCross2 = FALSE,BOOL DoTopCross3 = FALSE,BOOL DoTopCross4 = FALSE,BOOL DoTopCross5 = FALSE,BOOL DoTopCross6 = FALSE,BOOL DoTopCross7 = FALSE, BOOL DoBottomCross0 = FALSE, BOOL DoBottomCross1 = FALSE,BOOL DoBottomCross2 = FALSE,BOOL DoBottomCross3 = FALSE,BOOL DoBottomCross4 = FALSE,BOOL DoBottomCross5 = FALSE,BOOL DoBottomCross6 = FALSE,BOOL DoBottomCross7 = FALSE #ENDIF)
	
	HUD_COLOURS BoxInactive[16]
	BOOL isActive[16]
	//INT I
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox1Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[0] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox1Colour = HUD_COLOUR_BLACK
			isActive[0] = FALSE
			BoxInactive[0] = HUD_COLOUR_BLACK
		ELSE
			isActive[0] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox2Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[1] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox2Colour = HUD_COLOUR_BLACK
			isActive[1] = FALSE
			BoxInactive[1] = HUD_COLOUR_BLACK
		ELSE
			isActive[1] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox3Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[2] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox3Colour = HUD_COLOUR_BLACK
			isActive[2] = FALSE
			BoxInactive[2] = HUD_COLOUR_BLACK
		ELSE
			isActive[2] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox4Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[3] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox4Colour = HUD_COLOUR_BLACK
			isActive[3] = FALSE
			BoxInactive[3] = HUD_COLOUR_BLACK
		ELSE
			isActive[3] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox5Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[4] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox5Colour = HUD_COLOUR_BLACK
			isActive[4] = FALSE
			BoxInactive[4] = HUD_COLOUR_BLACK
		ELSE
			isActive[4] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox6Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[5] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox6Colour = HUD_COLOUR_BLACK
			isActive[5] = FALSE
			BoxInactive[5] = HUD_COLOUR_BLACK
		ELSE
			isActive[5] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox7Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[6] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox7Colour = HUD_COLOUR_BLACK
			isActive[6] = FALSE
			BoxInactive[6] = HUD_COLOUR_BLACK
		ELSE
			isActive[6] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF TopBox8Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[7] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF TopBox8Colour = HUD_COLOUR_BLACK
			isActive[7] = FALSE
			BoxInactive[7] = HUD_COLOUR_BLACK
		ELSE
			isActive[7] = TRUE
		ENDIF
	//ENDFOR
	
	
	
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox1Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[8] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox1Colour = HUD_COLOUR_BLACK
			isActive[8] = FALSE
			BoxInactive[8] = HUD_COLOUR_BLACK
		ELSE
			isActive[8] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox2Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[9] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox2Colour = HUD_COLOUR_BLACK
			isActive[9] = FALSE
			BoxInactive[9] = HUD_COLOUR_BLACK
		ELSE
			isActive[9] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox3Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[10] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox3Colour = HUD_COLOUR_BLACK
			isActive[10] = FALSE
			BoxInactive[10] = HUD_COLOUR_BLACK
		ELSE
			isActive[10] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox4Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[11] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox4Colour = HUD_COLOUR_BLACK
			isActive[11] = FALSE
			BoxInactive[11] = HUD_COLOUR_BLACK
		ELSE
			isActive[11] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox5Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[12] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox5Colour = HUD_COLOUR_BLACK
			isActive[12] = FALSE
			BoxInactive[12] = HUD_COLOUR_BLACK
		ELSE
			isActive[12] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox6Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[13] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox6Colour = HUD_COLOUR_BLACK
			isActive[13] = FALSE
			BoxInactive[13] = HUD_COLOUR_BLACK
		ELSE
			isActive[13] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox7Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[14] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox7Colour = HUD_COLOUR_BLACK
			isActive[14] = FALSE
			BoxInactive[14] = HUD_COLOUR_BLACK
		ELSE
			isActive[14] = TRUE
		ENDIF
	//ENDFOR
	//FOR I = 0 TO MAX_NUM_TEAMS-1
		IF BottomBox8Colour = GET_TEAM_HUD_COLOUR(0)
			BoxInactive[15] = GET_CONTRAST_TEAM_COLOUR(0)
		ENDIF
		IF BottomBox8Colour = HUD_COLOUR_BLACK
			isActive[15] = FALSE
			BoxInactive[15] = HUD_COLOUR_BLACK
		ELSE
			isActive[15] = TRUE
		ENDIF
	//ENDFOR
	
	#IF USE_TU_CHANGES 
		IF IS_STRING_NULL_OR_EMPTY(LocalTitle)
			REQUEST_STREAMED_TEXTURE_DICT("Timerbars")
			EXIT
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(OverallTitle)
			REQUEST_STREAMED_TEXTURE_DICT("Timerbars")
			EXIT
		ENDIF
	#ENDIF
	
		DRAW_GENERIC_ELIMINATION(LocalLimit, LocalTitle, -1, isActive[0], isActive[1], isActive[2], isActive[3], isActive[4], isActive[5], isActive[6], isActive[7], HUD_COLOUR_GREEN, HUD_COLOUR_RED, 
					FlashingTime, HUDORDER_SECONDBOTTOM, -1, -1, FALSE, TopBox1Colour, TopBox2Colour, TopBox3Colour, TopBox4Colour, TopBox5Colour, TopBox6Colour, 
					TopBox7Colour, TopBox8Colour,  BoxInactive[0], BoxInactive[1], BoxInactive[2],  BoxInactive[3],
					 BoxInactive[4],  BoxInactive[5],  BoxInactive[6],  BoxInactive[7], -1, HUDFLASHING_NONE, 0, TitleColour1
					  #IF USE_TU_CHANGES , DoTopCross0 ,  DoTopCross1 , DoTopCross2 , DoTopCross3 , DoTopCross4 , DoTopCross5 , DoTopCross6 , DoTopCross7  #ENDIF )
	
	
	
	DRAW_GENERIC_ELIMINATION(OverallLimit, OverallTitle, -1, isActive[8], isActive[9], isActive[10], isActive[11], isActive[12], isActive[13], isActive[14], isActive[15], HUD_COLOUR_GREEN, HUD_COLOUR_RED, 
				FlashingTime, HUDORDER_BOTTOM, -1, -1, FALSE, BottomBox1Colour, BottomBox2Colour, BottomBox3Colour, BottomBox4Colour, BottomBox5Colour, BottomBox6Colour, 
				BottomBox7Colour, BottomBox8Colour, BoxInactive[8], BoxInactive[9], BoxInactive[10],  BoxInactive[11],
				 BoxInactive[12],  BoxInactive[13],  BoxInactive[14],  BoxInactive[15], -1, HUDFLASHING_NONE, 0, TitleColour2
				 #IF USE_TU_CHANGES , DoBottomCross0 , DoBottomCross1 ,DoBottomCross2 ,DoBottomCross3 , DoBottomCross4 , DoBottomCross5 , DoBottomCross6 , DoBottomCross7  #ENDIF )



ENDPROC


PROC DRAW_ONE_PACKAGES_EIGHT_HUD(INT LocalLimit,STRING LocalTitle,BOOL bIsPlayer = FALSE, HUD_COLOURS TopBox1Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox2Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox3Colour = HUD_COLOUR_BLACK,
					HUD_COLOURS TopBox4Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox5Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox6Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox7Colour = HUD_COLOUR_BLACK, HUD_COLOURS TopBox8Colour = HUD_COLOUR_BLACK, 
					INT FlashingTime = -1, HUD_COLOURS TitleColour = HUD_COLOUR_PURE_WHITE #IF USE_TU_CHANGES , BOOL DoCross0 = FALSE, BOOL DoCross1 = FALSE,BOOL DoCross2 = FALSE,BOOL DoCross3 = FALSE,BOOL DoCross4 = FALSE,BOOL DoCross5 = FALSE,BOOL DoCross6 = FALSE,BOOL DoCross7 = FALSE #ENDIF,HUDORDER scoreHudOrder = HUDORDER_SECONDBOTTOM, BOOL bUseNonPlayerName = FALSE)

	HUD_COLOURS BoxInactive[16]
	BOOL isActive[16]
	INT I
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox1Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[0] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox1Colour = HUD_COLOUR_BLACK
			isActive[0] = FALSE
			BoxInactive[0] = HUD_COLOUR_BLACK
		ELSE
			isActive[0] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox2Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[1] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox2Colour = HUD_COLOUR_BLACK
			isActive[1] = FALSE
			BoxInactive[1] = HUD_COLOUR_BLACK
		ELSE
			isActive[1] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox3Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[2] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox3Colour = HUD_COLOUR_BLACK
			isActive[2] = FALSE
			BoxInactive[2] = HUD_COLOUR_BLACK
		ELSE
			isActive[2] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox4Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[3] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox4Colour = HUD_COLOUR_BLACK
			isActive[3] = FALSE
			BoxInactive[3] = HUD_COLOUR_BLACK
		ELSE
			isActive[3] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox5Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[4] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox5Colour = HUD_COLOUR_BLACK
			isActive[4] = FALSE
			BoxInactive[4] = HUD_COLOUR_BLACK
		ELSE
			isActive[4] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox6Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[5] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox6Colour = HUD_COLOUR_BLACK
			isActive[5] = FALSE
			BoxInactive[5] = HUD_COLOUR_BLACK
		ELSE
			isActive[5] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox7Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[6] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox7Colour = HUD_COLOUR_BLACK
			isActive[6] = FALSE
			BoxInactive[6] = HUD_COLOUR_BLACK
		ELSE
			isActive[6] = TRUE
		ENDIF
	ENDFOR
	FOR I = 0 TO FMMC_MAX_TEAMS-1
		IF TopBox8Colour = GET_TEAM_HUD_COLOUR(I)
			BoxInactive[7] = GET_CONTRAST_TEAM_COLOUR(I)
		ENDIF
		IF TopBox8Colour = HUD_COLOUR_BLACK
			isActive[7] = FALSE
			BoxInactive[7] = HUD_COLOUR_BLACK
		ELSE
			isActive[7] = TRUE
		ENDIF
	ENDFOR
	
	#IF USE_TU_CHANGES 
		IF IS_STRING_NULL_OR_EMPTY(LocalTitle)
			REQUEST_STREAMED_TEXTURE_DICT("Timerbars")
			EXIT
		ENDIF

	#ENDIF
	
	DRAW_GENERIC_ELIMINATION(LocalLimit, LocalTitle, -1, isActive[0], isActive[1], isActive[2], isActive[3], isActive[4], isActive[5], isActive[6], isActive[7], HUD_COLOUR_GREEN, HUD_COLOUR_RED, 
				FlashingTime, scoreHudOrder, -1, -1, bIsPlayer, TopBox1Colour, TopBox2Colour, TopBox3Colour, TopBox4Colour, TopBox5Colour, TopBox6Colour, 
				TopBox7Colour, TopBox8Colour,  BoxInactive[0], BoxInactive[1], BoxInactive[2],  BoxInactive[3],
				 BoxInactive[4],  BoxInactive[5],  BoxInactive[6],  BoxInactive[7], -1, HUDFLASHING_NONE, 0, TitleColour 
				 #IF USE_TU_CHANGES , DoCross0, DoCross1 ,DoCross2 ,DoCross3 ,DoCross4 ,DoCross5 ,DoCross6 ,DoCross7  #ENDIF , bUseNonPlayerName)
	

ENDPROC




/// PURPOSE:
///    Draws a general meter 
/// PARAMS:
///    TimerRunning - The current timer
///    TimeToDisplayFor - The max timer
///    Title - The title of the meter
///    aColour - The colour the filling should take
///    FlashTime - The flashing timer
PROC DRAW_TIMER_HUD(INT TimerRunning, INT TimeToDisplayFor, STRING Title, HUD_COLOURS aColour = HUD_COLOUR_RED, INT FlashTime = 0, BOOL OnlyZeroIsEmpty = TRUE, HUDORDER thisHudOrder = HUDORDER_DONTCARE)
	DRAW_GENERIC_METER(TimerRunning, TimeToDisplayFor, Title, aColour, FlashTime, thisHudOrder, -1, -1, FALSE, OnlyZeroIsEmpty)
ENDPROC

/// PURPOSE:
///    Draws a general meter at a given screen position
/// PARAMS:
///    TimerRunning - The Timer current value
///    TimeToDisplayFor - The max value the bar should take
///    XPos - The X position from 0.0 to 1.0
///    YPos - The Y Position from 0.0 to 1.0
///    aColour - The colour the bar should take
///    FlashTime - The flashing timer in milliseconds
PROC DRAW_TIMER_HUD_AT_POSITION(INT TimerRunning, INT TimeToDisplayFor, FLOAT XPos, FLOAT YPos, HUD_COLOURS aColour = HUD_COLOUR_YELLOW, INT FlashTime = 0, BOOL OnlyZeroIsEmpty = TRUE)
	DRAW_GENERIC_METER(TimerRunning, TimeToDisplayFor, "", aColour, FlashTime, HUDORDER_FREEROAM, XPos, YPos, FALSE, OnlyZeroIsEmpty)
ENDPROC

/// PURPOSE:
///    The Checkpoint hud, little blocks within a meter bar
/// PARAMS:
///    NumCheckpointsPassed - The current value of the checkpoints
///    MaxNumCheckpoints - The max number of checkpoints
///    Title - The title of the checkpoint bar
///    aColour - The colour of the checkpoint bar fill
///    FlashTime - The flash timer
PROC DRAW_CHECKPOINT_HUD(INT NumCheckpointsPassed, INT MaxNumCheckpoints, STRING Title, HUD_COLOURS aColour = HUD_COLOUR_YELLOW, INT FlashTime = 0)	
	DRAW_GENERIC_CHECKPOINT(NumCheckpointsPassed, MaxNumCheckpoints, Title, aColour, -1, FlashTime, HUDORDER_DONTCARE)
ENDPROC

/// PURPOSE:
///     The Checkpoint hud, little blocks within a meter bar, drawn at a screen position 
/// PARAMS:
///    NumCheckpointsPassed - The current value of the checkpoints
///    MaxNumCheckpoints - The max number of checkpoints
///    XPos - The checkpoint bar at x pos from 0 to 1.0
///    YPos - The checkpoint bar y position from 0 to 1.0
///    aColour - The colour the checkpoint bar fill will be
///    FlashTime - The flashing timer in milliseconds
PROC DRAW_CHECKPOINT_HUD_AT_POSITION(INT NumCheckpointsPassed, INT MaxNumCheckpoints, FLOAT XPos, FLOAT YPos, HUD_COLOURS aColour = HUD_COLOUR_YELLOW, INT FlashTime = 0)
	DRAW_GENERIC_CHECKPOINT(NumCheckpointsPassed, MaxNumCheckpoints, "", aColour, -1, FlashTime, HUDORDER_FREEROAM, XPos, YPos)
ENDPROC


/// PURPOSE:
///    Bar with sections that can either be on or off
/// PARAMS:
///    MaxNumCheckpoints - Number of boxes
///    Title - Title of the elimination box
///    IsElimiated1 - First box true or false
///    IsElimiated2 - Second box true or false
///    IsElimiated3 - Third box true or false
///    IsElimiated4 - Fourth box true or false
///    IsElimiated5 - Fifth box true or false
///    IsElimiated6 - Sixth box true or false
///    IsElimiated7 - Seventh box true or false
///    IsElimiated8 - Eighth box true or false
///    ValidColour - The colour the true boxes should take
///    inValidColour - The colour the false boxes should take
///    iCompletedBoxes - The number of boxes to make visible, -1 for all to be visible.
///    FlashTime - flash timer in milliseconds
PROC DRAW_ELIMINATION_HUD(INT MaxNumCheckpoints, STRING Title, BOOL IsElimiated1 = FALSE, BOOL IsElimiated2 = FALSE, BOOL IsElimiated3 = FALSE,
							BOOL IsElimiated4 = FALSE, BOOL IsElimiated5 = FALSE, BOOL IsElimiated6 = FALSE, BOOL IsElimiated7 = FALSE, BOOL IsElimiated8 = FALSE, HUD_COLOURS ValidColour= HUD_COLOUR_GREEN, HUD_COLOURS inValidColour = HUD_COLOUR_RED,
							INT iCompletedBoxes = -1, INT FlashTime = 0, HUD_COLOURS Box1Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box2Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box3Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box4Colour = HUD_COLOUR_PURE_WHITE,
							HUD_COLOURS Box5Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box6Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box7Colour = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box8Colour = HUD_COLOUR_PURE_WHITE,
							HUD_COLOURS Box1Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box2Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box3Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box4Colour_Inactive = HUD_COLOUR_PURE_WHITE,
							HUD_COLOURS Box5Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box6Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box7Colour_Inactive = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Box8Colour_Inactive = HUD_COLOUR_PURE_WHITE, 
							 INT ExtendedTimer = -1)
						

	DRAW_GENERIC_ELIMINATION(MaxNumCheckpoints, Title, iCompletedBoxes, IsElimiated1, IsElimiated2, IsElimiated3, IsElimiated4, IsElimiated5, IsElimiated6,
							IsElimiated7, IsElimiated8,ValidColour,inValidColour, FlashTime, HUDORDER_DONTCARE, -1, -1, FALSE, Box1Colour, Box2Colour, Box3Colour,
							Box4Colour, Box5Colour, Box6Colour, Box7Colour, Box8Colour, Box1Colour_Inactive, Box2Colour_Inactive, Box3Colour_Inactive,
							Box4Colour_Inactive, Box5Colour_Inactive, Box6Colour_Inactive, Box7Colour_Inactive, Box8Colour_Inactive, ExtendedTimer)
							
ENDPROC

/// PURPOSE:
///    Bar with sections that can either be on or off. With screen positions
/// PARAMS:
///    MaxNumCheckpoints - Number of boxes
///    XPos - screen position on the X Position 0 to 1.0
///    YPos - screen position on the Y Position 0 to 1.0 
///    IsElimiated1 - First box true or false
///    IsElimiated2 - Second box true or false
///    IsElimiated3 - Third box true or false
///    IsElimiated4 - Fourth box true or false
///    IsElimiated5 - Fifth box true or false
///    IsElimiated6 - Sixth box true or false
///    IsElimiated7 - Seventh box true or false
///    IsElimiated8 - Eighth box true or false
///    ValidColour - The colour the true boxes should take
///    inValidColour - The colour the false boxes should take
///    iCompletedBoxes - The number of boxes to make visible, -1 for all to be visible.
///    FlashTime - flash timer in milliseconds
PROC DRAW_ELIMINATION_HUD_AT_POSITION(INT MaxNumCheckpoints, FLOAT XPos, FLOAT YPos, BOOL IsElimiated1 = FALSE, BOOL IsElimiated2 = FALSE, BOOL IsElimiated3 = FALSE,
							BOOL IsElimiated4 = FALSE, BOOL IsElimiated5 = FALSE, BOOL IsElimiated6 = FALSE, BOOL IsElimiated7 = FALSE, BOOL IsElimiated8 = FALSE, HUD_COLOURS ValidColour= HUD_COLOUR_GREEN, HUD_COLOURS inValidColour = HUD_COLOUR_RED,
							INT iCompletedBoxes = -1, INT FlashTime = 0, HUD_COLOURS Box1Colour = HUD_COLOUR_WHITE, HUD_COLOURS Box2Colour = HUD_COLOUR_WHITE, HUD_COLOURS Box3Colour = HUD_COLOUR_WHITE, HUD_COLOURS Box4Colour = HUD_COLOUR_WHITE,
							HUD_COLOURS Box5Colour = HUD_COLOUR_WHITE, HUD_COLOURS Box6Colour = HUD_COLOUR_WHITE, HUD_COLOURS Box7Colour = HUD_COLOUR_WHITE, HUD_COLOURS Box8Colour = HUD_COLOUR_WHITE)
	
	DRAW_GENERIC_ELIMINATION(MaxNumCheckpoints, "", iCompletedBoxes, IsElimiated1, IsElimiated2, IsElimiated3, IsElimiated4, IsElimiated5, IsElimiated6,
							IsElimiated7, IsElimiated8,ValidColour,inValidColour, FlashTime, HUDORDER_FREEROAM, XPos, YPos, FALSE, Box1Colour, Box2Colour,
							Box3Colour, Box4Colour, Box5Colour, Box6Colour, Box7Colour, Box8Colour)
	
ENDPROC


///// PURPOSE:
/////    Draws the 2 bars needed for verses and an optional timer
///// PARAMS:
/////    NumCheckpointsPassedOne - The number of boxes filled for player one
/////    MaxNumCheckpointsOne - The total number of boxes for player one
/////    PlayerOne - The player index of player one
/////    iTeamOne - the team colour for player one
/////    NumCheckpointsPassedTwo - the number of boxes filled for player two
/////    MaxNumCheckpointsTwo - the total number of boxes for player two
/////    PlayerTwo - the player index of player two
/////    iTeamTwo - the team colour of player two
/////    FlashTime - how long the bars should flash for
PROC DRAW_VERSES_HUD(INT NumCheckpointsPassedOne, INT MaxNumCheckpointsOne, STRING sTitleOne, HUD_COLOURS aColourOne, INT NumCheckpointsPassedTwo, INT MaxNumCheckpointsTwo, STRING sTitleTwo, HUD_COLOURS aColourTwo, INT FlashTime = 0, INT Timer = -1, STRING sTimerName = NULL)	

	DRAW_GENERIC_CHECKPOINT(NumCheckpointsPassedOne, MaxNumCheckpointsOne, sTitleOne, aColourOne, -1, FlashTime, HUDORDER_THIRDBOTTOM)
	DRAW_GENERIC_CHECKPOINT(NumCheckpointsPassedTwo, MaxNumCheckpointsTwo, sTitleTwo, aColourTwo, -1, FlashTime, HUDORDER_SECONDBOTTOM)

	IF Timer > -1
		DRAW_GENERIC_TIMER(Timer, sTimerName,0, TIMER_STYLE_DONTUSEMILLISECONDS, FlashTime, PODIUMPOS_NONE, HUDORDER_BOTTOM)
	ENDIF
ENDPROC


/// PURPOSE:
///    Deathmatch hud with 2 scores, a local target and an optional timer
/// PARAMS:
///    LocalScore - The local players score
///    LocalTarget - The local players target
///    LocalPlayerName - The local players name by string
///    OpponentScore - The opponents score
///    OpponentPlayerName - The opponents name by string
///    LocalColour - The local number colour
///    OpponentColour - The opponent number colour
///    LocalPlace - The local hud display position
///    OpponentPlace - The opponents hud display position
///    Timer - The time in milliseconds. -1 to not display.
///    sTimerName - The timers title
///    FlashTime - The flash timer
PROC DRAW_PLAYER_DEATHMATCH_HUD	(INT LocalScore, INT LocalTarget, STRING LocalPlayerName, INT OpponentScore, STRING OpponentPlayerName, HUD_COLOURS LocalColour, 
								HUD_COLOURS OpponentColour, HUDORDER LocalPlace, HUDORDER OpponentPlace, INT Timer = -1, STRING sTimerName = NULL, INT FlashTime = 0, 
								BOOL isPlayer = TRUE, BOOL bIsTargetMatch = TRUE, BOOL bUseDoubleNumbers = FALSE, INT iEnemyTeamTarget = -1)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Scores_For_DM)
		
		
		IF bUseDoubleNumbers
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(OpponentScore, iEnemyTeamTarget, OpponentPlayerName, FlashTime, OpponentColour, OpponentPlace, isPlayer, DEFAULT, DEFAULT, DEFAULT, DEFAULT, OpponentColour)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(LocalScore, LocalTarget, LocalPlayerName, FlashTime, LocalColour, LocalPlace, isPlayer, DEFAULT, DEFAULT, DEFAULT, DEFAULT, LocalColour)
		ELSE
			DRAW_GENERIC_SCORE(OpponentScore, OpponentPlayerName, FlashTime, OpponentColour, OpponentPlace, isPlayer, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, OpponentColour)
			DRAW_GENERIC_SCORE(LocalScore, LocalPlayerName, FlashTime, LocalColour, LocalPlace, isPlayer, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, LocalColour)
		ENDIF

		// Target number
		IF bIsTargetMatch = TRUE
		AND NOT bUseDoubleNumbers
			IF LocalScore > -1
				STRING NewTarget = "HUD_TARG"
				DRAW_GENERIC_SCORE(LocalTarget, NewTarget, FlashTime, HUD_COLOUR_WHITE, HUDORDER_SECONDBOTTOM, FALSE, "")
			ENDIF
		ENDIF
	ENDIF
	
	HUD_COLOURS hudCol = HUD_COLOUR_WHITE		
	IF Timer < 30000
		hudCol = HUD_COLOUR_RED
	ENDIF	
	
	// Timer
	IF Timer > -1
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_RemoveTimeLimitForDM)
		DRAW_GENERIC_TIMER(Timer, sTimerName, 0, TIMER_STYLE_DONTUSEMILLISECONDS, FlashTime, PODIUMPOS_NONE, HUDORDER_BOTTOM, DEFAULT, hudCol)
	ENDIF
ENDPROC

//// PURPOSE:
 ///    Draws a single number, can only handle double digits
 /// PARAMS:
 ///    Score - The number
 ///    ScoreTitle - The title of the number
 ///    aColour - The colour the number should be
 ///    FlashTime - the flash timer
PROC DRAW_BIG_SINGLE_SCORE_HUD(INT Score, STRING ScoreTitle, HUD_COLOURS aColour = HUD_COLOUR_WHITE, INT FlashTime = -1)
	DRAW_GENERIC_BIG_NUMBER(Score, ScoreTitle, FlashTime, aColour)
ENDPROC

/// PURPOSE:
///    Draws a smaller number, can take a larger number
/// PARAMS:
///    Score - The number
///    ScoreTitle - The title next to the number
///    aColour - The colour the number should be
///    FlashTime - The flash timer
PROC DRAW_SMALL_SINGLE_SCORE_HUD(INT Score, STRING ScoreTitle, HUD_COLOURS aColour = HUD_COLOUR_WHITE, INT FlashTime = -1)
	DRAW_GENERIC_SCORE(Score, ScoreTitle, FlashTime, aColour)
ENDPROC

/// PURPOSE:
///    Draws a smaller number, can take a larger number
/// PARAMS:
///    Score - The number
///    ScoreTitle - The title next to the number
///    aColour - The colour the number should be
///    FlashTime - The flash timer
PROC DRAW_SMALL_DISTANCE_HUD(FLOAT Score, STRING ScoreTitle, HUD_COLOURS aColour = HUD_COLOUR_WHITE, INT FlashTime = -1)
	STRING NumberString
	IF Score < 2
		NumberString = "TIMER_MILESIG"
	ELSE
		NumberString = "TIMER_MILEPLU"
	ENDIF
	NumberString = "TIMER_MILE"

	DRAW_GENERIC_SCORE(0, ScoreTitle, FlashTime, aColour, HUDORDER_DONTCARE, FALSE, NumberString, TRUE, Score)
ENDPROC

/// PURPOSE:
///    Draws a Big double number with a dash ie 3/4 can only handle double digits to 99 in both fields. 
/// PARAMS:
///    Score - The current number
///    MaxScore - The denominator
///    ScoreTitle - The title of the score
///    aColour - The colour the first number should be
///    FlashTime - The flash timer
PROC DRAW_BIG_DOUBLE_SCORE_HUD(INT Score, INT MaxScore, STRING ScoreTitle, HUD_COLOURS aColour =  HUD_COLOUR_WHITE, INT FlashTime = -1)
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(Score, MaxScore, ScoreTitle, FlashTime, aColour)
ENDPROC



PROC RUN_SHOOTING_RANGE_MULTIPLYER(Range_HUD_Data & sHUDDrawData)	//, FLOAT fBarXPos = 0.919, FLOAT fBarYPos = 0.757)
//	TEXT_PLACEMENT TitlePlace
//	
//	TEXT_STYLE TitleStyle
//	SET_STANDARD_SMALL_HUD_TEXT(TitleStyle, DROPSTYLE_NONE)
//	SET_WORD_WRAPPING_TITLE(TitleStyle)
	
	DRAW_GENERIC_CHECKPOINT(sHUDDrawData.iCurMultiplierBlocks, sHUDDrawData.iMaxMultiplierBlocks, "MULTIPLIER", 
							sHUDDrawData.eMultiplierColor, sHUDDrawData.iMultiplier, 
							-1, HUDORDER_SIXTHBOTTOM, default, default, default, default, default, sHUDDrawData.iMultiplier)
//	DRAW_CHECKPOINT_HUD_AT_POSITION(sHUDDrawData.iCurMultiplierBlocks, sHUDDrawData.iMaxMultiplierBlocks, fBarXPos, fBarYPos, sHUDDrawData.eMultiplierColor)

//	DRAW_TEXT_WITH_NUMBER(TitlePlace, TitleStyle, "HUD_MULTSMAL", sHUDDrawData.iMultiplier, FONT_RIGHT)
ENDPROC


/// PURPOSE:
///    Gets the distance from the objective using the GPS route, instead of as the crow flies distance check
/// PARAMS:
///    Destination - the destination you want to calculate to
/// RETURNS:
///    The distance in miles to 3 decimel places
FUNC FLOAT GET_GPS_DISTANCE(VECTOR Destination, BOOL UseGPS = TRUE)
	INT Result
	IF UseGPS
		IF GET_GPS_BLIP_ROUTE_FOUND() 
			Result = GET_GPS_BLIP_ROUTE_LENGTH() 
		ELSE
			Result = FLOOR(VDIST(GET_PLAYER_COORDS(PLAYER_ID()), Destination))
		ENDIF
	ELSE
		Result = FLOOR(VDIST(GET_PLAYER_COORDS(PLAYER_ID()), Destination))
	ENDIF
RETURN CAST_METRES_TO_MILES(TO_FLOAT(Result))
ENDFUNC

/// PURPOSE:
///    Turns on the hud display of the distance to the destination
/// PARAMS:
///    Destination - The destination 
///    OverrideDistance - if you use the override, I ignore the vector and go with this directly. 
//PROC ENABLE_OBJECTIVE_DISTANCE_DISPLAY(VECTOR Destination, FLOAT OverrideDistance = -1.0, STRING OverrideText = NULL)
//	MPGlobalsHud.bObjectiveDistanceActive = TRUE 
//	MPGlobalsHud.vObjectiveDistance = Destination
//	MPGlobalsHud.fObjectiveOverrideDistance = OverrideDistance
//	MPGlobalsHud.sObjectiveOverrideText = OverrideText
//ENDPROC


/// PURPOSE:
///    Turns off the hud display of the distance to destination
//PROC DISABLE_OBJECTIVE_DISTANCE_DISPLAY()
//	MPGlobalsHud.bObjectiveDistanceActive = FALSE 
//ENDPROC

/// PURPOSE:
///    Returns if the objective distance is onscreen
/// RETURNS:
///    true if the objective distance is onscreen
//FUNC BOOL IS_OBJECTIVE_DISTANCE_DISPLAY_ACTIVE()
//	RETURN MPGlobalsHud.bObjectiveDistanceActive
//ENDFUNC

//
//PROC MAINTAIN_OBJECTIVE_DISTANCE_DISPLAY(PLACEMENT_TOOLS& Placement, TEXTSTYLES& aTextStyle)
//
//	IF MPGlobalsHud.bObjectiveDistanceActive
//		
//		SET_STANDARD_INGAME_TEXT_DETAILS(aTextStyle)
//		Placement.TextPlacement[0].x = 0.135
//		Placement.TextPlacement[0].y = 0.930
//			
//		FLOAT fDist 
//		
//		IF MPGlobalsHud.fObjectiveOverrideDistance > -1.0
//			fDist = MPGlobalsHud.fObjectiveOverrideDistance
//		ELSE
//			fDist = GET_GPS_DISTANCE(MPGlobalsHud.vObjectiveDistance)
//		ENDIF
//		
//		IF IS_STRING_EMPTY_HUD(MPGlobalsHud.sObjectiveOverrideText)
//			DRAW_TEXT_WITH_FLOAT(Placement.TextPlacement[0], aTextStyle.TS_STANDARDSMALLHUD, "AHD_DIST", fDist, 1)
//		ELSE
//			DRAW_TEXT_WITH_FLOAT(Placement.TextPlacement[0], aTextStyle.TS_STANDARDSMALLHUD, MPGlobalsHud.sObjectiveOverrideText, fDist, 1)
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL IS_MISSION_CREATOR_NEEDING_INGAMEHUD_ON()

	IF g_bMissionCreatorActive
		RETURN TRUE
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_INGAME_HUD_ELEMENTS_DISPLAY()


	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(TimerHudDebugTimer, 1000)
			bPrint = TRUE
		ENDIF
		IF G_bTurnOnAllHUDBlockers
			bPrint = TRUE
		ENDIF

		IF GET_COMMANDLINE_PARAM_EXISTS("sc_displaytimerhuddisable")
			bPrint = TRUE
		ENDIF
		
	#ENDIF
	
	//Added for bug 1641951
	IF g_bBringUpMPHud
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: g_bBringUpMPHud = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bBrowserVisible
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: g_bBrowserVisible = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_MP_TEXT_CHAT_TYPING()
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: IS_MP_TEXT_CHAT_TYPING = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_sSctvTickerQueue.bActive
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: g_sSctvTickerQueue.bActive = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF MPGlobalsScoreHud.bSwitchWheel
	OR MPGlobalsScoreHud.bSwitchWheelAndStats
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = TRUE: MPGlobalsScoreHud.bSwitchWheel = ")NET_PRINT_BOOL(MPGlobalsScoreHud.bSwitchWheel)
				NET_PRINT(" MPGlobalsScoreHud.bSwitchWheelAndStats = ")NET_PRINT_BOOL(MPGlobalsScoreHud.bSwitchWheelAndStats)
			ENDIF
		#ENDIF
		IF MPGlobalsScoreHud.bSniperScopeOn = FALSE
			IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
				SET_SNIPER_SCOPE_UNDER_HUD_THIS_FRAME()
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("[BCTIMERS] IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID()) 3 ")
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF

	IF IS_DM_PLAYER_DEAD_GLOBAL_SET()
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = TRUE: IS_DM_PLAYER_DEAD_GLOBAL_SET = TRUE ")
			ENDIF
		#ENDIF
		IF MPGlobalsScoreHud.bSniperScopeOn = FALSE
			IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
				SET_SNIPER_SCOPE_UNDER_HUD_THIS_FRAME()
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("[BCTIMERS] IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID()) 2 ")
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF

	#IF USE_TU_CHANGES
	IF g_b_IsATMrenderingForTimers
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: g_b_IsATMrenderingForTimers = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF g_IsMainCreatorCameraRunning
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: g_IsMainCreatorCameraRunning = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_CREATOR_NEEDING_INGAMEHUD_ON()
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = TRUE: IS_MISSION_CREATOR_NEEDING_INGAMEHUD_ON = TRUE ")
			ENDIF
		#ENDIF
		
		IF MPGlobalsScoreHud.bSniperScopeOn = FALSE
			IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
				SET_SNIPER_SCOPE_UNDER_HUD_THIS_FRAME()
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("[BCTIMERS] IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID()) 1 ")
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_sSelectorUI.bOnScreen
	AND IS_THIS_PLAYER_ACTIVE_IN_CORONA(PLAYER_ID()) = FALSE
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: g_sSelectorUI.bOnScreen ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: IS_PLAYER_SWITCH_IN_PROGRESS() ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsScoreHud.bSniperScopeOn = FALSE
		IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
			SET_SNIPER_SCOPE_UNDER_HUD_THIS_FRAME()
			#IF IS_DEBUG_BUILD
				IF bPrint
					NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID()) ")
				ENDIF
			#ENDIF
//			RETURN FALSE
		ENDIF
	ENDIF
	

	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_HUD_PREFERENCE_SWITCHED_ON() = FALSE
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE:  IS_HUD_PREFERENCE_SWITCHED_ON() = FALSE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	 
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState <> RESPAWN_STATE_PLAYING
			IF NETWORK_IS_IN_SPECTATOR_MODE() = FALSE
			AND g_b_On_Deathmatch = FALSE
			AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR)
			AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE: GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState <> RESPAWN_STATE_PLAYING ")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_ELEMENTS_DISPLAY = FALSE:  IS_SPECTATOR_HUD_HIDDEN() = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	/*IF IS_CUTSCENE_ACTIVE()
		RETURN FALSE
	ENDIF
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF*/
	
//	IF IS_PHONE_ONSCREEN(TRUE)
//		RETURN FALSE
//	ENDIF
	
RETURN TRUE

ENDFUNC

#IF USE_TU_CHANGES	

PROC SET_HOLD_DRAWING_OF_TIMERS_THIS_FRAME()	

	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(TimerHudPhoneDebugTimer, 1000)
			bPrint = TRUE
		ENDIF
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_displaytimerhuddisable")
			bPrint = TRUE
		ENDIF
		IF bPrint
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("[BCTIMERS] SET_HOLD_DRAWING_OF_TIMERS_THIS_FRAME is being called. ")
		ENDIF
	#ENDIF

	g_b_HoldOffDrawingTimersThisFrame = TRUE
ENDPROC
#ENDIF


FUNC BOOL CAN_INGAME_HUD_DISPLAY_WITH_PHONE(HUDORDER WhichOrder)

	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(TimerHudPhoneDebugTimer, 1000)
			bPrint = TRUE
		ENDIF
		IF G_bTurnOnAllHUDBlockers
			bPrint = TRUE
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_displaytimerhuddisable")
			bPrint = TRUE
		ENDIF
		
	#ENDIF

	#IF USE_TU_CHANGES	

		IF g_b_HoldOffDrawingTimersThisFrame = TRUE
			#IF IS_DEBUG_BUILD
				IF bPrint
					NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_DISPLAY_WITH_PHONE = FALSE: g_b_HoldOffDrawingTimersThisFrame = TRUE ")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			IF IS_PAUSE_MENU_ACTIVE()
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_DISPLAY_WITH_PHONE = FALSE: IS_PAUSE_MENU_ACTIVE = TRUE ")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	#ENDIF

	IF WhichOrder <> HUDORDER_FREEROAM
	AND MPGlobalsScoreHud.bPhoneUnderHud = FALSE
	AND MPGlobalsScoreHud.bPhoneUnderHudNoRise = FALSE
		IF IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS() 
			#IF IS_DEBUG_BUILD
				IF bPrint
					NET_NL()NET_PRINT("[BCTIMERS] CAN_INGAME_HUD_DISPLAY_WITH_PHONE = FALSE: IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS = TRUE ")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
		
	RETURN TRUE
	

ENDFUNC

/// PURPOSE: Draw a Clock as HUD down in the bottom right hand corner.
/// INPUT PARAMS: 	sClockTitle - Label name that will be used as the clocks title.
///    				bSetWarning - when true the clock will turn red once warning time is reached.
///    			  	iWarningHour - Hour you wish to set warning at
///    				iWarningMin  - Miniute you wish to set warning at
///    				timerStyle - clock style
///    				hud_colour	- colour of clock text before warning(default white)
///    
Proc DRAW_CLOCK(String sClockTitle,bool bSetWarning = true,int iWarningHour = 0,int iWarningMin = 0,TIMER_STYLE timerStyle= TIMER_STYLE_DONTUSEMILLISECONDS,HUD_COLOURS hud_colour = HUD_COLOUR_WHITE)
	int timehours = GET_CLOCK_HOURS()
	int timemins  = GET_CLOCK_MINUTES()
	int currentTime  = (timehours * 60 ) + timemins	
	
	if bsetWarning
		if currentTime >= (iWarningHour*60) + iWarningMin
			hud_colour = HUD_COLOUR_RED
		endif
	endif
	
	if not IS_CUTSCENE_PLAYING()
	and not IS_PHONE_ONSCREEN()
		DRAW_GENERIC_TIMER((currentTime * 1000),sClockTitle,0,timerStyle,-1,PODIUMPOS_NONE,HUDORDER_DONTCARE,false,hud_colour)
	endif
Endproc











//BOOL bArrowWidgets 
//
//SPRITE_PLACEMENT RightSprite_widget
//SPRITE_PLACEMENT LeftSprite_widget

FUNC STRING GET_ARROW_NAME()
	RETURN "MP_ArrowXlarge"
ENDFUNC

PROC REQUEST_ARROW_MOVIE(SCALEFORM_INDEX &siMovie)
	siMovie = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
ENDPROC

PROC CLEANUP_ARROW_MOVIE(SCALEFORM_INDEX &siMovie)
	PRINTLN("[CS_ARR] CLEANUP_ARROW_MOVIE")
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siMovie)
ENDPROC

PROC DO_ARROW_SOUNDS(INT iDirection)
	SWITCH iDirection
		CASE ciRALLY_ARROW_UP
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Ahead",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
		
		CASE ciRALLY_ARROW_DOWN 
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Behind",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
		
		CASE ciRALLY_ARROW_LEFT 	
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Left",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
		
		CASE ciRALLY_ARROW_RIGHT 	
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Right",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
		CASE ciTARGET_ARROW_SPEEDUP
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Ahead",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
		CASE ciTARGET_ARROW_SLOWDOWN
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Behind",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
		CASE ciTARGET_ARROW_STOP
			IF NOT IS_REPLAY_RECORDING()
				PLAY_SOUND_FRONTEND(-1,  "Nav_Arrow_Behind",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DRAW_NEW_RALLY_ARROW(SCALEFORM_INDEX &siMovie, INT iDirection)
	IF IS_SAFE_TO_DRAW_ON_SCREEN()
		IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_RADIO_STATIONS)
			IF NOT g_bBrowserVisible
				IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
					BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_DIRECTION")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDirection)
				//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR) // optional colouring
				//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
				//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					END_SCALEFORM_MOVIE_METHOD()
					
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(siMovie, 255, 255, 255, 255)
			//	ELSE
			//		PRINTLN("[CS_ARR] DRAW_NEW_RALLY_ARROW, HAS_SCALEFORM_MOVIE_LOADED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_RALLY_ARROW(INT iDirection, BOOL bEnableUpDown = FALSE)
	SPRITE_PLACEMENT aSprite
	STRING sArrow = GET_ARROW_NAME()
	REQUEST_STREAMED_TEXTURE_DICT("MPArrow")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPArrow")

		SWITCH iDirection
			CASE ciRALLY_ARROW_UP 		
				IF bEnableUpDown
					aSprite.x = 0.250
					aSprite.y = 0.350
					aSprite.w = 0.050
					aSprite.h = 0.150
					aSprite.a = 255
					aSprite.fRotation = 270.000
				
					SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_BLUE)
					DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
				ENDIF
			BREAK
			
			CASE ciRALLY_ARROW_DOWN 
				IF bEnableUpDown
					aSprite.x = 0.250
					aSprite.y = 0.350
					aSprite.w = 0.050
					aSprite.h = 0.150
					aSprite.a = 255
					aSprite.fRotation = 90.000
				
					SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_BLUE)
					DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
				ENDIF
			BREAK
			
			CASE ciRALLY_ARROW_LEFT 	
				aSprite.x = 0.250
				aSprite.y = 0.350
				aSprite.w = 0.050
				aSprite.h = 0.150
				aSprite.a = 255
				aSprite.fRotation = 180.000
			
				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_BLUE)
				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
			BREAK
			
			CASE ciRALLY_ARROW_RIGHT 	
				aSprite.x = 0.750
				aSprite.y = 0.350
				aSprite.w = 0.050
				aSprite.h = 0.150
				aSprite.a = 255
				aSprite.fRotation = 0
				
				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_BLUE)
				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
			BREAK
			
			CASE ciTARGET_ARROW_SPEEDUP 	
				aSprite.x = 0.750
				aSprite.y = 0.350
				aSprite.w = 0.050
				aSprite.h = 0.150
				aSprite.a = 255
				aSprite.fRotation = 0
				
				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_BLUE)
				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
			BREAK
			
			CASE ciTARGET_ARROW_SLOWDOWN 	
				aSprite.x = 0.750
				aSprite.y = 0.350
				aSprite.w = 0.050
				aSprite.h = 0.150
				aSprite.a = 255
				aSprite.fRotation = 0
				
				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_BLUE)
				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
			BREAK
			
			CASE ciTARGET_ARROW_STOP 	
				aSprite.x = 0.750
				aSprite.y = 0.350
				aSprite.w = 0.050
				aSprite.h = 0.150
				aSprite.a = 255
				aSprite.fRotation = 0
				
				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_RED)
				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//PROC DRAW_SINGLE_RIGHT_ARROW(INT RightIntense)
//
////	#IF IS_DEBUG_BUILD
////	IF bArrowRightWidgets = FALSE
////		START_WIDGET_GROUP("DRAW_ARROW")
////			CREATE_A_SPRITE_PLACEMENT_WIDGET(RightSprite_widget, "RightSprite_widget")
////			CREATE_A_SPRITE_PLACEMENT_WIDGET(LeftSprite_widget, "LeftSprite_widget")
////
////		STOP_WIDGET_GROUP()
////		
////		bArrowRightWidgets = TRUE
////	ENDIF
////	#ENDIF
//	
//	SPRITE_PLACEMENT aSprite
//	STRING sArrow = GET_ARROW_NAME()
//	
//	REQUEST_STREAMED_TEXTURE_DICT("MPArrow")
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPArrow")
//	
//		SWITCH RightIntense
//		
//			CASE 1
//				aSprite.x = 0.750
//				aSprite.y = 0.350
//				aSprite.w = 0.050
//				aSprite.h = 0.150
//				aSprite.a = 255
//				aSprite.fRotation = 0
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_YELLOWDARK)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 2
//				aSprite.x = 0.750
//				aSprite.y = 0.350
//				aSprite.w = 0.075
//				aSprite.h = 0.200
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 0
//			
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_YELLOW)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 3
//				aSprite.x = 0.750
//				aSprite.y = 0.350
//				aSprite.w = 0.100
//				aSprite.h = 0.250
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 0
//			
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_ORANGEDARK)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 4
//				aSprite.x = 0.750
//				aSprite.y = 0.350
//				aSprite.w = 0.125
//				aSprite.h = 0.300
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 0
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_ORANGE)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 5
//				aSprite.x = 0.750
//				aSprite.y = 0.350
//				aSprite.w = 0.150
//				aSprite.h = 0.350
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 0
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_REDDARK)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 6
//				aSprite.x = 0.750
//				aSprite.y = 0.350
//				aSprite.w = 0.175
//				aSprite.h = 0.400
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 0
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_RED)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//		ENDSWITCH
//		
//		//				#IF IS_DEBUG_BUILD
////				aSprite.x += RightSprite_widget.x
////				aSprite.y += RightSprite_widget.y
////				aSprite.w += RightSprite_widget.w
////				aSprite.h += RightSprite_widget.h
////				aSprite.r += RightSprite_widget.r
////				aSprite.g += RightSprite_widget.g
////				aSprite.b += RightSprite_widget.b
////				aSprite.a += RightSprite_widget.a
////				aSprite.fRotation += RightSprite_widget.fRotation
////				#ENDIF
//	ENDIF
//ENDPROC
//
//PROC DRAW_SINGLE_LEFT_ARROW(INT LeftIntense)
//
//	SPRITE_PLACEMENT aSprite
//	STRING sArrow = GET_ARROW_NAME()
//	
//	aSprite.x = 0.0
//	aSprite.y = 0.0
//	aSprite.w = 0.0
//	aSprite.h = 0.0
//	aSprite.r = 0
//	aSprite.g = 0
//	aSprite.b = 0
//	aSprite.a = 255
//	
////	#IF IS_DEBUG_BUILD
////	aSprite.x += LeftSprite_widget.x
////	aSprite.y += LeftSprite_widget.y
////	aSprite.w += LeftSprite_widget.w
////	aSprite.h += LeftSprite_widget.h
////	aSprite.r += LeftSprite_widget.r
////	aSprite.g += LeftSprite_widget.g
////	aSprite.b += LeftSprite_widget.b
////	aSprite.a += LeftSprite_widget.a
////	#ENDIF
//	
//	REQUEST_STREAMED_TEXTURE_DICT("MPArrow")
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPArrow")
//		
//		SWITCH LeftIntense
//		
//			CASE 1
//				aSprite.x = 0.250
//				aSprite.y = 0.350
//				aSprite.w = 0.050
//				aSprite.h = 0.150
//				aSprite.a = 255
//				aSprite.fRotation = 180.000
//			
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_YELLOWDARK)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 2
//				aSprite.x = 0.250
//				aSprite.y = 0.350
//				aSprite.w = 0.075
//				aSprite.h = 0.200
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 180.000
//
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_YELLOW)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 3
//				aSprite.x = 0.250
//				aSprite.y = 0.350
//				aSprite.w = 0.100
//				aSprite.h = 0.250
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 180.000
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_ORANGEDARK)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 4
//				aSprite.x = 0.250
//				aSprite.y = 0.350
//				aSprite.w = 0.125
//				aSprite.h = 0.300
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 180.000
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_ORANGE)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 5
//				aSprite.x = 0.250
//				aSprite.y = 0.350
//				aSprite.w = 0.150
//				aSprite.h = 0.350
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 180.000
//				
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_REDDARK)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//			
//			CASE 6
//				aSprite.x = 0.250
//				aSprite.y = 0.350
//				aSprite.w = 0.175
//				aSprite.h = 0.400
//				aSprite.r = 0
//				aSprite.g = 0
//				aSprite.b = 0
//				aSprite.a = 255
//				aSprite.fRotation = 180.000
//			
//				SET_SPRITE_HUD_COLOUR(aSprite, HUD_COLOUR_RED)
//				DRAW_2D_SPRITE("MPArrow", sArrow, aSprite)
//			BREAK
//		
//		ENDSWITCH
//		
////				#IF IS_DEBUG_BUILD
////				aSprite.x += LeftSprite_widget.x
////				aSprite.y += LeftSprite_widget.y
////				aSprite.w += LeftSprite_widget.w
////				aSprite.h += LeftSprite_widget.h
////				aSprite.r += LeftSprite_widget.r
////				aSprite.g += LeftSprite_widget.g
////				aSprite.b += LeftSprite_widget.b
////				aSprite.a += LeftSprite_widget.a
////				aSprite.fRotation += LeftSprite_widget.fRotation
////				#ENDIF
//	ENDIF
//ENDPROC
//
//
///// PURPOSE:
/////    Draws the arrows to show the driver/passenger the intensity of the directions. 
///// PARAMS:
/////    LeftIntense - 0 - 6 (High Intense)
/////    RightIntense - 0 - 6 (High Intense)
//PROC DRAW_RALLY_ARROWS(INT LeftIntense, INT RightIntense)
//	DRAW_SINGLE_LEFT_ARROW(LeftIntense)
//	DRAW_SINGLE_RIGHT_ARROW(RightIntense)
//ENDPROC

FUNC STRING GET_HUD_ELEMENT_STRING(PROGRESSHUD_ELEMENTS eHudElement)
	SWITCH eHudElement
		CASE PROGRESSHUD_INVALID				RETURN "PROGRESSHUD_INVALID"
		CASE PROGRESSHUD_METER					RETURN "PROGRESSHUD_METER"
		CASE PROGRESSHUD_CHECKPOINT				RETURN "PROGRESSHUD_CHECKPOINT"
		CASE PROGRESSHUD_ELIMINATION			RETURN "PROGRESSHUD_ELIMINATION"
		CASE PROGRESSHUD_SINGLE_NUMBER			RETURN "PROGRESSHUD_SINGLE_NUMBER"
		CASE PROGRESSHUD_DOUBLE_NUMBER			RETURN "PROGRESSHUD_DOUBLE_NUMBER"
		CASE PROGRESSHUD_DOUBLE_NUMBER_PLACE	RETURN "PROGRESSHUD_DOUBLE_NUMBER_PLACE"
		CASE PROGRESSHUD_SCORE					RETURN "PROGRESSHUD_SCORE"
		CASE PROGRESSHUD_TIMER					RETURN "PROGRESSHUD_TIMER"
		CASE PROGRESSHUD_WINDMETER				RETURN "PROGRESSHUD_WINDMETER"
		CASE PROGRESSHUD_BIG_RACE_POSITION		RETURN "PROGRESSHUD_BIG_RACE_POSITION"
		CASE PROGRESSHUD_SPRITE_METER			RETURN "PROGRESSHUD_SPRITE_METER"
		CASE PROGRESSHUD_FOUR_ICON_BAR			RETURN "PROGRESSHUD_FOUR_ICON_BAR"
		CASE PROGRESSHUD_FIVE_ICON_SCORE_BAR	RETURN "PROGRESSHUD_FIVE_ICON_SCORE_BAR"
		CASE PROGRESSHUD_SIX_ICON_BAR			RETURN "PROGRESSHUD_SIX_ICON_BAR"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_HUD_ELEMENT_ACTUALLY_DRAW_STRING(PROGRESSHUD_ELEMENTS eHudElement)
	SWITCH eHudElement
		CASE PROGRESSHUD_INVALID				RETURN "INVALID"
		CASE PROGRESSHUD_METER					RETURN "ACTUALLY_DRAW_GENERAL_METER"
		CASE PROGRESSHUD_CHECKPOINT				RETURN "ACTUALLY_DRAW_GENERAL_CHECKPOINT"
		CASE PROGRESSHUD_ELIMINATION			RETURN "ACTUALLY_DRAW_GENERAL_ELIMINATION"
		CASE PROGRESSHUD_SINGLE_NUMBER			RETURN "ACTUALLY_DRAW_GENERAL_SINGLE_BIG_NUMBER"
		CASE PROGRESSHUD_DOUBLE_NUMBER			RETURN "ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER"
		CASE PROGRESSHUD_DOUBLE_NUMBER_PLACE	RETURN "ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE"
		CASE PROGRESSHUD_SCORE					RETURN "ACTUALLY_DRAW_GENERAL_SINGLE_SCORE"
		CASE PROGRESSHUD_TIMER					RETURN "ACTUALLY_DRAW_GENERAL_TIME_TIMER"
		CASE PROGRESSHUD_WINDMETER				RETURN "ACTUALLY_DRAW_GENERAL_WINDMETER"
		CASE PROGRESSHUD_BIG_RACE_POSITION		RETURN "ACTUALLY_DRAW_GENERAL_BIG_RACE_POSITION"
		CASE PROGRESSHUD_SPRITE_METER			RETURN "ACTUALLY_DRAW_GENERAL_SPRITE_METER"
		CASE PROGRESSHUD_FOUR_ICON_BAR			RETURN "ACTUALLY_DRAW_GENERAL_FOUR_ICON_BAR"
		CASE PROGRESSHUD_FIVE_ICON_SCORE_BAR	RETURN "ACTUALLY_DRAW_GENERAL_FIVE_ICON_SCORE_BAR"
		CASE PROGRESSHUD_SIX_ICON_BAR			RETURN "ACTUALLY_DRAW_GENERAL_SIX_ICON_BAR"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_HUD_ELEMENT_ORDER_INDEX(PROGRESSHUD_ELEMENTS eHudElement)
	SWITCH eHudElement
		CASE PROGRESSHUD_INVALID				RETURN -1
		CASE PROGRESSHUD_TIMER					RETURN 0
		CASE PROGRESSHUD_SINGLE_NUMBER			RETURN 1
		CASE PROGRESSHUD_DOUBLE_NUMBER			RETURN 2
		CASE PROGRESSHUD_DOUBLE_NUMBER_PLACE	RETURN 3
		CASE PROGRESSHUD_CHECKPOINT				RETURN 4
		CASE PROGRESSHUD_METER					RETURN 5
		CASE PROGRESSHUD_SCORE					RETURN 6
		CASE PROGRESSHUD_ELIMINATION			RETURN 7
		CASE PROGRESSHUD_WINDMETER				RETURN 8
		CASE PROGRESSHUD_BIG_RACE_POSITION		RETURN 9
		CASE PROGRESSHUD_SPRITE_METER			RETURN 10
		CASE PROGRESSHUD_FOUR_ICON_BAR			RETURN 11
		CASE PROGRESSHUD_FIVE_ICON_SCORE_BAR	RETURN 12
		CASE PROGRESSHUD_SIX_ICON_BAR			RETURN 13
		CASE PROGRESSHUD_DOUBLE_TEXT			RETURN 14
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL IS_HUD_PRIORITY_VALID(HUD_PRIORITY eHudPriority)
	RETURN (eHudPriority >= HUD_PRIORITY_FIRST AND eHudPriority < HUD_PRIORITY_MAX)
ENDFUNC

FUNC BOOL IS_PRIORITY_HUD_ELEMENT_VALID(HUD_PRIORITY eHudPriority)
	IF IS_HUD_PRIORITY_VALID(eHudPriority)
		RETURN (g_ePriorityHudElement[ENUM_TO_INT(eHudPriority)] != PROGRESSHUD_INVALID)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HUD_ELEMENT_ORDER_INDEX_VALID(INT iOrderIndex)
	RETURN (iOrderIndex > -1 AND iOrderIndex < NUMBER_OF_DIFFERENT_HUD_ELEMENTS)
ENDFUNC

/// PURPOSE: Clears the current HUD element in priority slot
PROC CLEAR_PRIORITY_HUD_ELEMENT(HUD_PRIORITY eHudPriority)
	IF IS_HUD_PRIORITY_VALID(eHudPriority)
		IF g_ePriorityHudElement[ENUM_TO_INT(eHudPriority)] != PROGRESSHUD_INVALID
			#IF IS_DEBUG_BUILD
			PRINTLN("[PRIORITY_HUD] CLEAR_PRIORITY_HUD_ELEMENT - Clearing Priority Hud Element ", ENUM_TO_INT(eHudPriority), " - Called From Script: ", GET_THIS_SCRIPT_NAME())
			#ENDIF
			g_ePriorityHudElement[ENUM_TO_INT(eHudPriority)] = PROGRESSHUD_INVALID
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Clears all the current HUD element priorities
PROC CLEAR_ALL_PRIORITY_HUD_ELEMENTS()
	INT iHudPriority
	INT iMaxPriority = ENUM_TO_INT(HUD_PRIORITY_MAX)
	REPEAT iMaxPriority iHudPriority
		IF g_ePriorityHudElement[iHudPriority] != PROGRESSHUD_INVALID
			#IF IS_DEBUG_BUILD
			PRINTLN("[PRIORITY_HUD] CLEAR_ALL_PRIORITY_HUD_ELEMENTS - Clearing Priority Hud Element ", iHudPriority)
			#ENDIF
			g_ePriorityHudElement[iHudPriority] = PROGRESSHUD_INVALID
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	PRINTLN("[PRIORITY_HUD] CLEAR_ALL_PRIORITY_HUD_ELEMENTS - Called From Script: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
ENDPROC

/// PURPOSE: Processes the passed in HUD element first.
///    		 **Must call CLEAR_PRIORITY_HUD_ELEMENT or CLEAR_ALL_PRIORITY_HUD_ELEMENTS after.
PROC SET_PRIORITY_HUD_ELEMENT(PROGRESSHUD_ELEMENTS eHudElement, HUD_PRIORITY eHudPriority)
	IF IS_HUD_PRIORITY_VALID(eHudPriority)
		IF g_ePriorityHudElement[ENUM_TO_INT(eHudPriority)] != eHudElement
			#IF IS_DEBUG_BUILD
			PRINTLN("[PRIORITY_HUD] SET_PRIORITY_HUD_ELEMENT - Setting Priority Hud ", ENUM_TO_INT(eHudPriority), " Element To: ", GET_HUD_ELEMENT_STRING(eHudElement), " - Called From Script: ", GET_THIS_SCRIPT_NAME())
			#ENDIF
			g_ePriorityHudElement[ENUM_TO_INT(eHudPriority)] = eHudElement
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Gets the current HUD element priority to be processed first 
FUNC PROGRESSHUD_ELEMENTS GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY eHudPriority)
	IF IS_HUD_PRIORITY_VALID(eHudPriority)
		RETURN g_ePriorityHudElement[ENUM_TO_INT(eHudPriority)]
	ENDIF
	RETURN PROGRESSHUD_INVALID
ENDFUNC

/// PURPOSE: Checks if a HUD element is a priority
FUNC BOOL IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_ELEMENTS eHudElement)
	IF IS_HUD_ELEMENT_ORDER_INDEX_VALID(ENUM_TO_INT(eHudElement))
		INT iHudPriority
		INT iMaxPriority = ENUM_TO_INT(HUD_PRIORITY_MAX)
		REPEAT iMaxPriority iHudPriority
			IF (g_ePriorityHudElement[iHudPriority] = eHudElement)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ACTUALLY_DRAW_GENERAL_HUD_ELEMENT(PROGRESSHUD_ELEMENTS eHudElement, INT iHudElementIndex)
	SWITCH eHudElement
		CASE PROGRESSHUD_TIMER
			ACTUALLY_DRAW_GENERAL_TIME_TIMER(iHudElementIndex, MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_Timer[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_TIMER.sGenericTimer_TimerTitle[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtraTime[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_MedalDisplay[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.bGenericTimer_TimerStyle[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_FlashTimer[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_TitleNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsPlayer[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_ColourFlashType[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_ColourFlash[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bDisplayAsDashes[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_TitleColour[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsLiteral[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_FleckColour[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Powerup[iHudElementIndex], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bHideUnusedZeros[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_SINGLE_NUMBER
			ACTUALLY_DRAW_GENERAL_SINGLE_BIG_NUMBER(iHudElementIndex, MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_Number[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberTitle[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_FlashTimer[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_TitleNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_bIsPlayer[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberString[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_TitleColour[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.bGenericNumber_DrawInfinity[iHudElementIndex] ,  MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_ColourFlashType[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_ColourFlash[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_FleckColour[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_EnablePulsing[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_PulseColour[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_PulseTime[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_DOUBLE_NUMBER
			ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER(iHudElementIndex, MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_Number[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_NumberTwo[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.sGenericDoubleNumber_Title[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_COLOUR[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_FlashTimer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_TitleNumber[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bIsPlayer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_ColourFlashType[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_ColourFlash[iHudElementIndex],  MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bDisplayWarning[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bUseNonPlayerFont[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_TitleCOLOUR[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_FleckColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_iAlpha[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bFlashTitle[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_DOUBLE_NUMBER_PLACE
			ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE(iHudElementIndex, MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_Number[iHudElementIndex], 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_NumberTwo[iHudElementIndex], 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.sGenericDoubleNumberPlace_Title[iHudElementIndex],
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_COLOUR[iHudElementIndex], 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_FlashTimer[iHudElementIndex], 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_TitleNumber[iHudElementIndex],
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bIsPlayer[iHudElementIndex] , 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_ColourFlashType[iHudElementIndex], 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_ColourFlash[iHudElementIndex], 
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_TitleCOLOUR[iHudElementIndex],
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_FleckColour[iHudElementIndex],
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bCustomFont[iHudElementIndex],
														MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_eCustomFont[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_CHECKPOINT
			ACTUALLY_DRAW_GENERAL_CHECKPOINT(iHudElementIndex, MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_Number[iHudElementIndex], MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_MaxNumber[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_CHECKPOINT.sGenericCheckpoint_Title[iHudElementIndex], MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_Colour[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_TitleNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_bIsPlayer[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_FlashTimer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[iHudElementIndex].x,
											MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[iHudElementIndex].y, MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_ColourFlashType[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_ColourFlash[iHudElementIndex], MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_iInBuiltMultiplyer[iHudElementIndex], 
											#IF USE_TU_CHANGES GenericCheckpoint_Cross0[iHudElementIndex], GenericCheckpoint_Cross1[iHudElementIndex], GenericCheckpoint_Cross2[iHudElementIndex], GenericCheckpoint_Cross3[iHudElementIndex], GenericCheckpoint_Cross4[iHudElementIndex], 
											GenericCheckpoint_Cross5[iHudElementIndex], GenericCheckpoint_Cross6[iHudElementIndex], GenericCheckpoint_Cross7[iHudElementIndex], #ENDIF 
											MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FleckColour[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_METER
			ACTUALLY_DRAW_GENERAL_METER(iHudElementIndex, MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_Number[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_MaxNumber[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.sGenericMeter_Title[iHudElementIndex],  MPGlobalsScoreHud.ElementHud_METER.GenericMeter_Colour[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_FlashTimer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[iHudElementIndex].x,
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[iHudElementIndex].y, MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsPlayer[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_TitleNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bOnlyZeroIsEmpty[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_ColourFlashType[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_ColourFlash[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bBigMeter[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iDrawRedDangerZonePercent[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsLiteralString[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_PercentageLine[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FleckColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_TextColour[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bDrawLineUnderName[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_LineUnderNameColour[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_MakeBarUrgent[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iUrgentPercentage[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.PulseToColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.iPulseTime[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.bUseScoreTitle[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fMaxNumber[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bUseSecondaryBar[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_eSecondaryBarColour[iHudElementIndex], 
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fSecondaryBarPercentage[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bTransparentSecBarIntersectingMainBar[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_eSecBarPulseToColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iSecBarPulseTime[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fSecBarStartPercentage[iHudElementIndex], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iGFXDrawOrder[iHudElementIndex],
										MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bCapMaxPercentage[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_SCORE
			ACTUALLY_DRAW_GENERAL_SINGLE_SCORE(iHudElementIndex, MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_Number[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_Title[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Colour[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_FlashTimer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_TitleNumber[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsPlayer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_NumberString[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_isFloat[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_FloatValue[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_ColourFlashType[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_ColourFlash[iHudElementIndex],MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_TitleColour[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_DisplayWarning[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_MaxNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_DrawInfinity[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Powerup[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Style[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsLiteralTitle[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_FleckColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_iAlpha[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bDisplayBlankScore[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_pPlayerID[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bFlashTitle[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bDrawLineUnderName[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_LineUnderNameColour[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_ELIMINATION
			ACTUALLY_DRAW_GENERAL_ELIMINATION(iHudElementIndex, MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_MaxNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.sGenericElimination_Title[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFirst[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourSecond[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_VisibleBoxes[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive1[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive2[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive3[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive4[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive5[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive6[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive7[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive8[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_FlashTimer[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_TitleNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bIsPlayer[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[iHudElementIndex].x,MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[iHudElementIndex].y,MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour_InActive[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour_InActive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour_InActive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour_InActive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour_InActive[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour_InActive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour_InActive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour_InActive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFlashType[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFlash[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_TitleColour[iHudElementIndex] #IF USE_TU_CHANGES , GenericElimination_Cross0[iHudElementIndex], GenericElimination_Cross1[iHudElementIndex],GenericElimination_Cross2[iHudElementIndex],GenericElimination_Cross3[iHudElementIndex],GenericElimination_Cross4[iHudElementIndex],GenericElimination_Cross5[iHudElementIndex],
												GenericElimination_Cross6[iHudElementIndex],GenericElimination_Cross7[iHudElementIndex] #ENDIF, MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bUseNonPlayerFont[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FleckColour[iHudElementIndex] #IF USE_TU_CHANGES , MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross0Colour[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross1Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross2Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross3Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross4Colour[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross5Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross6Colour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross7Colour[iHudElementIndex] #ENDIF)
		BREAK
		CASE PROGRESSHUD_WINDMETER
			ACTUALLY_DRAW_GENERAL_WINDMETER(iHudElementIndex, MPGlobalsScoreHud.ElementHud_WIND.sGenericMeter_Title[iHudElementIndex], 
											MPGlobalsScoreHud.ElementHud_WIND.fGenericMeter_Heading[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_WindSpeed[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_RedComponent[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_BlueComponent[iHudElementIndex],
											MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_GreenComponent[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_BIG_RACE_POSITION
			ACTUALLY_DRAW_GENERAL_BIG_RACE_POSITION(iHudElementIndex, MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_iRacePosition[iHudElementIndex], MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_eRacePositionHUDColour[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_SPRITE_METER
			ACTUALLY_DRAW_GENERAL_SPRITE_METER(iHudElementIndex, MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_Number[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_MaxNumber[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_Title[iHudElementIndex],  MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_Colour[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_FlashTimer[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsPlayer[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_TitleNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bOnlyZeroIsEmpty[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_ColourFlashType[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_ColourFlash[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_SpriteName[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_DictionaryName, 
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsLiteralString[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_FleckColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_TextColour[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_FOUR_ICON_BAR
			ACTUALLY_DRAW_GENERAL_FOUR_ICON_BAR(iHudElementIndex, MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_TitleColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerOne[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerThree[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupOne[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupThree[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconOne[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconThree[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_iFlashTime[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_FIVE_ICON_SCORE_BAR
			ACTUALLY_DRAW_GENERAL_FIVE_ICON_SCORE_BAR(iHudElementIndex, MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_Number[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_FloatValue[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_NumberString[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_isFloat[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_MaXNumber[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_DrawInfinity[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_TitleColour[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerOne[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerTwo[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerThree[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFour[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFive[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupOne[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupThree[iHudElementIndex], 
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFive[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerToHighlight[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bEnablePlayerHighlight[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupOneColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupTwoColour[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupThreeColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFourColour[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFiveColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iInstanceToHighlight[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bPulseHighlight[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iPulseTime[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pAvatarToFlash[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bFlashAvatar[iHudElementIndex],
													MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarFlashTime[iHudElementIndex], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarSlotToFlash[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_SIX_ICON_BAR
			ACTUALLY_DRAW_GENERAL_SIX_ICON_BAR(iHudElementIndex, MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_TitleColour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerOne[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerThree[iHudElementIndex], 
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFive[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerSix[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupOne[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupThree[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFive[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupSix[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconOne[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconTwo[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconThree[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFour[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFive[iHudElementIndex],
												MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconSix[iHudElementIndex], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_iFlashTime[iHudElementIndex])
		BREAK
		CASE PROGRESSHUD_DOUBLE_TEXT
			ACTUALLY_DRAW_DOUBLE_TEXT_BAR(iHudElementIndex, MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleLeft[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleRight[iHudElementIndex], 
										  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleLeftLiteral[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleRightLiteral[iHudElementIndex],
										  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_TitleCOLOUR[iHudElementIndex], MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bCustomFont[iHudElementIndex],
										  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_eCustomFont[iHudElementIndex])
													
													
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
	RETURN g_bEnableRPBarInArenaBox
ENDFUNC

PROC SET_RP_BAR_ACTIVE_STATE_IN_ARENA_BOX(BOOL bState)
	IF bState
		#IF IS_DEBUG_BUILD
		PRINTLN("[WHEEL_SPIN] - SET_RP_BAR_ACTIVE_STATE_IN_ARENA_BOX - Setting g_bEnableRPBarInArenaBox to: TRUE")
		PRINTLN("[WHEEL_SPIN] - SET_RP_BAR_ACTIVE_STATE_IN_ARENA_BOX - Callstack:")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		g_bEnableRPBarInArenaBox = TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[WHEEL_SPIN] - SET_RP_BAR_ACTIVE_STATE_IN_ARENA_BOX - Setting g_bEnableRPBarInArenaBox to: FALSE")
		PRINTLN("[WHEEL_SPIN] - SET_RP_BAR_ACTIVE_STATE_IN_ARENA_BOX - Callstack:")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		g_bEnableRPBarInArenaBox = FALSE
	ENDIF
ENDPROC







