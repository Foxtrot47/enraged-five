//╒═════════════════════════════════════════════════════════════════════════════╕
//│																				│
//│						  Decorator Initialiser - GTA V							│
//│																				│
//│				Contains declarations for all decorators used in GTA V.			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_decorator.sch"

PROC INITIALISE_SP_DECORATORS()
	DECOR_REGISTER("PHOTO_TAKEN", DECOR_TYPE_BOOL)
	DECOR_REGISTER("doe_elk", DECOR_TYPE_BOOL)
	DECOR_REGISTER("hunt_score", DECOR_TYPE_INT)
	DECOR_REGISTER("hunt_weapon", DECOR_TYPE_INT)
	DECOR_REGISTER("hunt_undetected", DECOR_TYPE_BOOL)
	DECOR_REGISTER("hunt_nocall", DECOR_TYPE_BOOL)
	DECOR_REGISTER("hunt_chal_weapon", DECOR_TYPE_INT)
	DECOR_REGISTER("hunt_kill_time", DECOR_TYPE_INT)
//	DECOR_REGISTER("BlockFriendGrab", DECOR_TYPE_BOOL)
	DECOR_REGISTER("DismissedBy", DECOR_TYPE_INT)
	DECOR_REGISTER("Darts_name", DECOR_TYPE_INT)
	DECOR_REGISTER("Getaway_Winched", DECOR_TYPE_BOOL)
	DECOR_REGISTER("MapGauntlet", DECOR_TYPE_INT)
	DECOR_REGISTER("IgnoredByQuickSave", DECOR_TYPE_BOOL)
	DECOR_REGISTER("GetawayVehicleValid", DECOR_TYPE_BOOL)
	DECOR_REGISTER("RampageCarExploded", DECOR_TYPE_BOOL)
	DECOR_REGISTER("Carwash_Vehicle_Decorator", DECOR_TYPE_BOOL)
	DECOR_REGISTER("Casino_Game_Info_Decorator", DECOR_TYPE_INT)
ENDPROC

PROC INITIALISE_MP_DECORATORS()
	DECOR_REGISTER("Player_Vehicle", DECOR_TYPE_INT)
	DECOR_REGISTER("PV_Slot", DECOR_TYPE_INT)
	DECOR_REGISTER("Previous_Owner", DECOR_TYPE_INT)
	DECOR_REGISTER("Sprayed_Vehicle_Decorator", DECOR_TYPE_BOOL)	
	DECOR_REGISTER("Sprayed_Vehicle_Timer_Dec",DECOR_TYPE_TIME)
	DECOR_REGISTER("Vehicle_Reward", DECOR_TYPE_INT)
	DECOR_REGISTER("Vehicle_Reward_Teams", DECOR_TYPE_INT)
	DECOR_REGISTER("Skill_Blocker", DECOR_TYPE_BOOL)	
	DECOR_REGISTER("TargetPlayerForTeam", DECOR_TYPE_INT)	
	DECOR_REGISTER("XP_Blocker", DECOR_TYPE_BOOL)	
	DECOR_REGISTER("CrowdControlSetUp", DECOR_TYPE_INT)	
	DECOR_REGISTER("Bought_Drugs", DECOR_TYPE_BOOL)
	DECOR_REGISTER("HeroinInPossession", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("CokeInPossession", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("WeedInPossession", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("MethInPossession", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("bombdec", DECOR_TYPE_INT)	
	DECOR_REGISTER("bombdec1", DECOR_TYPE_INT)	
	DECOR_REGISTER("bombowner", DECOR_TYPE_INT)	
	DECOR_REGISTER("noPlateScan", DECOR_TYPE_BOOL)
	DECOR_REGISTER("prisonBreakBoss", DECOR_TYPE_BOOL)
	//DECOR_REGISTER("BountyTarget",DECOR_TYPE_BOOL)
	DECOR_REGISTER("cashondeadbody", DECOR_TYPE_INT)
	DECOR_REGISTER("MissionType", DECOR_TYPE_INT)	
	DECOR_REGISTER("MatchId", DECOR_TYPE_INT)	
	DECOR_REGISTER("TeamId", DECOR_TYPE_INT)	
	DECOR_REGISTER("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
	DECOR_REGISTER("Veh_Modded_By_Player",DECOR_TYPE_INT)
	
	DECOR_REGISTER("MPBitset", DECOR_TYPE_INT)
	
	DECOR_REGISTER("MC_EntityID", DECOR_TYPE_INT) // for the mission controller to get the creator int from each created entity for data array referenceing.
	DECOR_REGISTER("MC_ChasePedID", DECOR_TYPE_INT) // for the mission controller to get the creator int from each created entity for data array referenceing.
	DECOR_REGISTER("MC_Team0_VehDeliveredRules", DECOR_TYPE_INT) // for the mission controller to remember which rules a vehicle has been delivered on for team 0.
	DECOR_REGISTER("MC_Team1_VehDeliveredRules", DECOR_TYPE_INT) // for the mission controller to remember which rules a vehicle has been delivered on for team 1.
	DECOR_REGISTER("MC_Team2_VehDeliveredRules", DECOR_TYPE_INT) // for the mission controller to remember which rules a vehicle has been delivered on for team 2.
	DECOR_REGISTER("MC_Team3_VehDeliveredRules", DECOR_TYPE_INT) // for the mission controller to remember which rules a vehicle has been delivered on for team 3.

	DECOR_REGISTER("AttributeDamage", DECOR_TYPE_INT)
	DECOR_REGISTER("GangBackup", DECOR_TYPE_INT)
	DECOR_REGISTER("CreatedByPegasus", DECOR_TYPE_BOOL)	
		
	DECOR_REGISTER("BeforeCorona", DECOR_TYPE_INT)
	
	DECOR_REGISTER("Heist_Veh_ID", DECOR_TYPE_INT)
	
	DECOR_REGISTER("CC_iState", DECOR_TYPE_INT)
	DECOR_REGISTER("CC_iStatePrev", DECOR_TYPE_INT)
	DECOR_REGISTER("CC_iBitSet", DECOR_TYPE_INT)
	DECOR_REGISTER("CC_fInfluenceDirectThreat", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("CC_fInfluenceShouting", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("CC_iBeatdownHitsRemaining", DECOR_TYPE_INT)
	DECOR_REGISTER("CC_iBeatdownRounds", DECOR_TYPE_INT)
	
	DECOR_REGISTER("LUXE_MINIGAME_ACT_PROPS", DECOR_TYPE_INT)
	DECOR_REGISTER("LUXE_VEH_INSTANCE_ID", DECOR_TYPE_INT)
	
	DECOR_REGISTER("UsingForTimeTrial", DECOR_TYPE_BOOL)	
	
	DECOR_REGISTER("EnableVehLuxeActs", DECOR_TYPE_INT)
	
	DECOR_REGISTER("Player_Goon", DECOR_TYPE_INT)
	DECOR_REGISTER("Player_Boss", DECOR_TYPE_INT)
	DECOR_REGISTER("Previous_Boss", DECOR_TYPE_INT)
	
	DECOR_REGISTER("PYV_Owner", DECOR_TYPE_INT)
	DECOR_REGISTER("PYV_Vehicle", DECOR_TYPE_INT)
	DECOR_REGISTER("PYV_Yacht", DECOR_TYPE_INT)
	DECOR_REGISTER("PYV_WarpFrom", DECOR_TYPE_INT)
	
	DECOR_REGISTER("ContrabandOwner", DECOR_TYPE_INT)
	PRINTLN("[MAGNATE_GANG_BOSS] - Register ContrabandOwner Decorator")
	DECOR_REGISTER("HeliTaxi", DECOR_TYPE_BOOL)
	
	DECOR_REGISTER("ContrabandDeliveryType", DECOR_TYPE_INT)
	DECOR_REGISTER("RandomID", DECOR_TYPE_INT)
	
	DECOR_REGISTER("ExportVehicle", DECOR_TYPE_INT)
	
	DECOR_REGISTER("RespawnVeh", DECOR_TYPE_INT)
	
	DECOR_REGISTER("Player_Truck", DECOR_TYPE_INT)
	DECOR_REGISTER("Creator_Trailer", DECOR_TYPE_INT)
	
	DECOR_REGISTER("FMDeliverableID", DECOR_TYPE_INT)
	
	DECOR_REGISTER("Player_Avenger", DECOR_TYPE_INT)

	#IF FEATURE_DLC_2_2022
	DECOR_REGISTER("Player_Acid_Lab", DECOR_TYPE_INT)
	DECOR_REGISTER("Player_Support_Bike_Vehicle", DECOR_TYPE_INT)
	#ENDIF

	DECOR_REGISTER("Player_Hacker_Truck", DECOR_TYPE_INT)
	DECOR_REGISTER("BBCarrier", DECOR_TYPE_BOOL)
	
	DECOR_REGISTER("GBMissionFire", DECOR_TYPE_INT)
	
	DECOR_REGISTER("GBCVehicle", DECOR_TYPE_BOOL)
	
	DECOR_REGISTER("CSHVehicle", DECOR_TYPE_BOOL)
	
	DECOR_REGISTER("AllowModSprayRepair", DECOR_TYPE_BOOL)
	
	DECOR_REGISTER("FMCVehicle", DECOR_TYPE_BOOL)
	
	#IF FEATURE_HEIST_ISLAND
	DECOR_REGISTER("Player_Submarine", DECOR_TYPE_INT)
	DECOR_REGISTER("Player_Submarine_Dinghy", DECOR_TYPE_INT)
	DECOR_REGISTER("Player_Moon_Pool", DECOR_TYPE_INT)
	#ENDIF
	
	#IF FEATURE_TUNER
	DECOR_REGISTER("VehicleList", DECOR_TYPE_BOOL)
	#ENDIF
	
	#IF FEATURE_FIXER
	DECOR_REGISTER("Company_SUV", DECOR_TYPE_INT)
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	DECOR_REGISTER("TestDrive", DECOR_TYPE_BOOL)
	#ENDIF
ENDPROC

PROC INTIIALISE_GTA5_DECORATORS()
	INITIALISE_SP_DECORATORS()
	INITIALISE_MP_DECORATORS()
	// KGM 16/5/12: Locks decorator registration after all are registered - no more registration allowed after this point
	DECOR_REGISTER_LOCK()
ENDPROC
