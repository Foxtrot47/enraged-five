/*
USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "common/flow/flow_public_core_override.sch"
USING "Commands_streaming.sch"
USING "commands_ped.sch"
USING "Overlay_Effects.sch"
USING "commands_task.sch"
USING "commands_object.sch"
USING "selector_public.sch"
USING "commands_fire.sch"
USING "commands_event.sch"

using "commands_misc.sch"
USING "commands_graphics.sch"
USING "commands_path.sch"
USING "beam_effect.sch"
*/


USING "net_include.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "menu_public.sch"
USINg "net_spawn.sch"

USING "rage_builtins.sch"
USING "globals.sch"
using "cellphone_public.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
using "commands_misc.sch"
USING "script_maths.sch"
USING "Script_player.sch"
using "commands_path.sch"
//using "beam_effect.sch"
using "script_blips.sch"
using "usefulCommands.sch"
USING "commands_shapetest.sch"
USING "commands_path.sch"
USING "selector_public.sch"
USING "rgeneral_include.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
#ENDIF
USING "VehicleTurretHeader.sch"
//vector vStoreHit

//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:		KB							Date:		01/07/2010	│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│																				│
//│							Police Chopper HUD      							│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛



CONST_INT HUD_FRIEND 0
CONST_INT HUD_BYSTANDER 1
CONST_INT HUD_FOE 2
CONST_INT HUD_UNKNOWN 3
//aaa
TWEAK_FLOAT HUD_PAN_FACTOR_DEFAULT  0.0075
//FLOAT fStoredArrowPos
VECTOR vStoredRotation
INT iCamFrameCounter
BOOL bCamMovementStopped

//CONST_INT MAX_SCANNABLE_PEDS 40
//CONST_INT MAX_HELIHUD_COORD_COUNT 10 //how many target icons can be displayed on HUD

CONST_FLOAT MAX_CAMERA_PITCH -75.0
CONST_FLOAT MAX_CAMERA_PITCH_GROUNDED -25.0
CONST_FLOAT MIN_CAMERA_PITCH 6.0

CONST_FLOAT MIN_CAMERA_HEADING -180.0
CONST_FLOAT MAX_CAMERA_HEADING 180.0

CONST_INT MAX_GROUND_CLIP_FRAMES 10

//CONST_INT MAX_MESSAGES 12
TWEAK_FLOAT cfVOLATOL_NOSE_CAM_X 0.0
TWEAK_FLOAT cfVOLATOL_NOSE_CAM_Y 0.180
TWEAK_FLOAT cfVOLATOL_NOSE_CAM_Z 0.0

TWEAK_FLOAT cfVOLATOL_ROOF_CAM_X 0.0
TWEAK_FLOAT cfVOLATOL_ROOF_CAM_Y 0.0
TWEAK_FLOAT cfVOLATOL_ROOF_CAM_Z 0.50

CONST_FLOAT cfVOLATOL_NOSE_BASE_X 0.00
CONST_FLOAT cfVOLATOL_NOSE_BASE_Y 7.10	
CONST_FLOAT cfVOLATOL_NOSE_BASE_Z -1.52

CONST_FLOAT cfVOLATOL_ROOF_BASE_X 0.00
CONST_FLOAT cfVOLATOL_ROOF_BASE_Y 4.70
CONST_FLOAT cfVOLATOL_ROOF_BASE_Z 2.56

TWEAK_FLOAT cfAVENGER_T0_X	0.0	// <<0.0454, 9.8, -2.4>>
TWEAK_FLOAT cfAVENGER_T0_Y	9.8
TWEAK_FLOAT cfAVENGER_T0_Z 	-2.315
TWEAK_FLOAT cfAVENGER_T0_RX	0.0
TWEAK_FLOAT cfAVENGER_T0_RY 0.0	
TWEAK_FLOAT cfAVENGER_T0_RZ 0.0	

TWEAK_FLOAT cfAVENGER_T1_X	0.0 //<<-0.0007, -1.8604, 2.6528>>
TWEAK_FLOAT cfAVENGER_T1_Y	-1.85
TWEAK_FLOAT cfAVENGER_T1_Z	2.650
TWEAK_FLOAT cfAVENGER_T1_RX 0.0
TWEAK_FLOAT cfAVENGER_T1_RY 0.0
TWEAK_FLOAT cfAVENGER_T1_RZ 180.0

TWEAK_FLOAT cfAVENGER_T2_X  0.0 //<<-0.0078, -9.3894, 0.2028>>
TWEAK_FLOAT cfAVENGER_T2_Y	-9.50
TWEAK_FLOAT cfAVENGER_T2_Z	0.11
TWEAK_FLOAT cfAVENGER_T2_RX 0.0
TWEAK_FLOAT cfAVENGER_T2_RY 0.0
TWEAK_FLOAT cfAVENGER_T2_RZ 180.0

TWEAK_FLOAT cfAKULA_T1_X	1.260
TWEAK_FLOAT cfAKULA_T1_Y 	3.278
TWEAK_FLOAT cfAKULA_T1_Z 	0.695
TWEAK_FLOAT cfAKULA_T1_RX 	0.0
TWEAK_FLOAT cfAKULA_T1_RY	0.0
TWEAK_FLOAT cfAKULA_T1_RZ 	-90.0

TWEAK_FLOAT cfMOC2_0_X		0.0
TWEAK_FLOAT cfMOC2_0_Y		0.0
TWEAK_FLOAT cfMOC2_0_Z		0.0
TWEAK_FLOAT cfMOC2_0_RX		0.0
TWEAK_FLOAT cfMOC2_0_RY		0.0
TWEAK_FLOAT cfMOC2_0_RZ		0.0

TWEAK_FLOAT cfMOC2_1_X		0.0
TWEAK_FLOAT cfMOC2_1_Y		0.0
TWEAK_FLOAT cfMOC2_1_Z		0.0
TWEAK_FLOAT cfMOC2_1_RX		0.0
TWEAK_FLOAT cfMOC2_1_RY		0.0
TWEAK_FLOAT cfMOC2_1_RZ		0.0

struct messageStruct //Displays fancy streaming police messages on HUD
	int messageID //unique number for this message
	bool messagePrinting
	//TEXT_LABEL_63 HelicopterHudlabel
	//string HelicopterHudTextLabel
	//int labelLength
	int charsDisplayed
	float fx,fy
	int nextchartime
	float size
	int textSpeed
	int pauseBeforePrinting	
	int pauseOnCompletion
	bool deleteOnCompletion
	int waitForMessage
	int deletePreviousMessages
	bool active, completed
	int r,g,b,a
ENDSTRUCT

ENUM HELIHUD_MARKER_TYPE
	MARKER_CROSS,
	MARKER_BOX
ENDENUM



STRUCT structPedList //each ped that is scannable is stored in this.
	PED_INDEX pedID
	int lastSeenTime
	int BITPedData
	float fScanTime
	int relType
	int BITnamesAndCrimes
	#if ENABLE_ENHANCED_HUD_FEATURES
		int iSpecialNameCase  = -1
	#ENDIF
	vector vLostLocation
	SHAPETEST_INDEX shapeTest
	//TEXT_LABEL_63 name
	//int status
	//TEXT_LABEL_63 crimes
ENDSTRUCT



/*
struct structPedNames
	TEXT_LABEL_63 nameMale
	TEXT_LABEL_63 nameFemale
	text_label_63 surname
ENDSTRUCT
*/
STRUCT HELIHUD_MARKER
	vector coord	
	vehicle_index veh = null
	ped_index ped = null
	int entry = -1
	HELIHUD_MARKER_TYPE markerType
	BLIP_INDEX blip
	int r,g,b
ENDSTRUCT

struct colourStruct
	int r,g,b
endstruct


//view blocking
#if ENABLE_ENHANCED_HUD_FEATURES
CONST_INT MAX_SPHERE_BLOCKERS 7
CONST_INT MAX_QUAD_BLOCKS 6
float zSpeed

struct quadBlockStruct
	vector vCorner[4]
endstruct

quadBlockStruct quadBlocks[MAX_QUAD_BLOCKS]


STRUCT sphereBlockerStruct
	vector vLoc
	float fRad
	bool inUse
ENDSTRUCT

#endif

STRUCT structHelicopterHUDVars
	bool bHUDInitiated
	bool bHUDIsActive
	bool canScan
	
	//Widgets
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID CopChopperWidget
	VECTOR vTurretCamOffset //= <<-1, -1, -1>>
#ENDIF	//	IS_DEBUG_BUILD
	bool bActivateChopper
	bool isMultiplayerMode
	//------------------------------------------Chopper Variables-------------------------------
	bool onlyShowHUDmarkers
	bool bMoveChopper
	bool mpCamFudge
	vehicle_INDEX vehChopper
	ENTITY_INDEX camEntity
	float fChopperSpeed
	float fChopperRotateSpeed
	float nearClip = 0.5
	float maxNearClip = 5.0
	//hud stuff
	int minutes


	float fMaxChopperSpeed=40.0,fminChopperSpeed
	float fHeight, fTargetHeight=150.0, fChopperVerticalSpeed, fChopperVerticlAccel, fMaxVertSpeedFactor, fChopperHeight
	int flyUpdateTime
	
	FLOAT fMinCameraYaw
	FLOAT fMaxCameraYaw = 0.0
	float angUpLow = 1.059
	float angUpHigh = 2.073
	float angDownLow = 2.052
	float angDownHigh = 2.826
	float angLow = -58.95
	float AngHigh = -9.858
	
	//-------------------------------------------chopper cam variables --------------------------
	CAMERA_INDEX camChopper
	bool bActivateChopperCam
	bool bDeactivateChopperCam
	bool bIsCamAttached
	
	float chopperPanFactor
	float ChopperZoomFactor
	
	float maxFOV = 28.0
	float minFOV = 10.0
	float fFOV	
	float chopperHeading
	#if ENABLE_ENHANCED_HUD_FEATURES
	bool bBlockTargetLost
	int minChopperHours=-1
	int maxChopperHours=-1
	bool bShowData
	bool bSphereBlockersActive
	bool bHoverInArea,bGettoPoint,moveByCamera
	float speedMultiplier=1.0
	bool bheadingfixed
	float fFixedHeading
	float visibleHeading
	bool bDisableCamMovement
	vector vGetToPoint,vLastGetToPoint,vHoverPoint
	float rangeToTarget
	int flightTime
	bool slowOnArrival
	float circleRadius
	float actualSpeed
	float hoverHitRange
	vector lastPos
	float speedLastFrame
	
	vector vHoverCenter
	float fHoverRadius 
	
	float lastVx,lastVy
	
	float actualZAcc
	float targetZAcc
	float lastRotVel
	float rotVelChange
	
	bool bSphereBlockActive
	sphereBlockerStruct sphereBlocker[MAX_SPHERE_BLOCKERS]
	
	
	
	#ENDIF
	//temp variables
	VECTOR vCamRot
	vector vTargetedGround
	//-------------------------------------------chopper HUD variables------------------------------
	int iCurrentPedScanCount

	bool scanning
	bool bDisplayHUD
	bool bDisplayMinimap
	bool bShowTargetOffScreen
	bool targetIsLost
	float fLineAInset
	
	//scanning memory
	ped_index lastScannedPed

	float fStickPitch, fStickYaw

	int colR
	int colG
	int colB
	int colA
	
	//colours
	colourStruct boxColourUnscanned
	colourStruct boxColourtarget
	colourStruct boxColourFriendly
	colourStruct scanningColour
	
	float fBoxSize

	float fTextY
	
	int iPedGivenLOScheck
	float fSightRangeMultiplier
	float fSightRangeMultiplier2
	float fRangeMultiplier

	float fLineAWidth

	float hudBarY
	float hudReticuleSize
	float hudHorizY
	float hudHorizWidth
	float hudHorizHeight

	//bool flags
	bool bTexturesLoaded
	bool bDisableScanner

	//vertical element vars

	float elementWidth
	float elementHeight
	float elementArrowX
	float elementArrowSize
	
	//peds to scan
	int nearestPed
	int lastIdentifiedPed
	int scanButtonPressedRemovalTimer 
	TEXT_LABEL pedCrimes[11]
	TEXT_LABEL pedStatus[4]
	float fMinFindPedOnScreenRange
	
	//#if ENABLE_ENHANCED_HUD_FEATURES
		SHAPETEST_INDEX shpTest
		vector vAimAt
	//#ENDIF
	
	//coord markers
	int helihudMarkerCount
	HELIHUD_MARKER helihudMarker[MAX_HELIHUD_COORD_COUNT]
	
	//structs
	structPedList pedScanList[MAX_SCANNABLE_PEDS]
	//structPedNames pedNames[MAX_PED_NAMES]
	
	//building up display messages
	#if ENABLE_ENHANCED_HUD_FEATURES
		messageStruct message[MAX_MESSAGES]
		int messageCounter = 0
		int CharTimeGap = 100 //ms
		int currentMessage
		float fTrackBeztime
		
		TEXT_LABEL specialNames[7]
		
	#ENDIF
	
	VECTOR vAttachedCamRotationOffset
	VECTOR vAttachedEntityRotation
	
	//-------------------------------Ped control vars----------------------------
	bool bAllowPedOrders
	PED_INDEX selectedPed=null
	int selectID
	//--------------------------------TEST VARS-------------------------------
	
	
	
	
	float fTextRangeScale
	
	float textSpace
	//
	float fCheckHUDFocusRange = 20.0
	
	int nextGroundCheckTime
	float GroundHeightAccum
	
	//AUDIO
	bool bAudioLoaded
	bool rangeSoundPlaying
	int soundID_zoom = -1
	int soundID_turn = -1
	int soundID_scan = -1
	int soundID_background = -1
	int soundID_scanloop = -1
	int soundID_scangood = -1
	int soundID_scanbad = -1
	int soundID_scanRange = -1
	
	#IF IS_DEBUG_BUILD
		//debug
		bool bSaveDebug
		BOOL bDebugDrawRelRot = FALSE
	#ENDIF
	
	INT iCollideFrameCount = 0
	INT iSceneSoundId = -1
	BOOL bCamRotSet = FALSE
	BOOL bDelayRenderScriptCams = FALSE
	
	BOOL bLimitCamWhileGrounded
	
ENDSTRUCT

#IF IS_DEBUG_BUILD
	//WIDGET_GROUP_ID parentWidget
#ENDIF

	//float textXPos = 0.35, textYPos = 0.6, textScale = 2.0
	//#if ENABLE_ENHANCED_HUD_FEATURES
	//	bool resetTime
	//#ENDIF
	//int itextSpeed
SCALEFORM_INDEX mov	

//view blocking checks

	FUNC BOOL POINT_IN_TRI(vector vPointOnPlane, vector vCorner1,vector vCorner2,vector vCorner3)					
		vector v0 = vCorner3 - vCorner1
		vector v1 = vCorner2 - vCorner1
		vector v2 = vPointOnPlane - vCorner1
		
		float dot00 = DOT_PRODUCT(v0, v0)
		float dot01 = DOT_PRODUCT(v0, v1)
		float dot02 = DOT_PRODUCT(v0, v2)
		float dot11 = DOT_PRODUCT(v1, v1)
		float dot12 = DOT_PRODUCT(v1, v2) 
		
		float invDenom = 1 / (dot00 * dot11 - dot01 * dot01)
		float u = (dot11 * dot02 - dot01 * dot12) * invDenom
		float v = (dot00 * dot12 - dot01 * dot02) * invDenom
		
		IF (u >= 0) AND (v >= 0) AND (u + v < 1)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDFUNC

	FUNC BOOL DOES_LINE_INTERSECT_QUAD(vector p1, vector p2, vector vCorner1,vector vCorner2,vector vCorner3,vector vCorner4)
		vector vPointOnPlane

		
		vector normal = CROSS_PRODUCT(vCorner2 - vCorner1, vCorner3 - vCorner1)
		IF NOT IS_VECTOR_ZERO(normal)
			normal = NORMALISE_VECTOR(normal)
			IF GET_LINE_PLANE_INTERSECT(vPointOnPlane, p1, p2, normal, vCorner1)
				IF GET_DISTANCE_BETWEEN_COORDS(p1,vPointOnPlane) < GET_DISTANCE_BETWEEN_COORDS(p1,p2)
					IF POINT_IN_TRI(vPointOnPlane,vCorner1,vCorner2,vCorner3)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF

			normal = CROSS_PRODUCT(vCorner3 - vCorner1, vCorner4 - vCorner1)
			normal = NORMALISE_VECTOR(normal)
			IF NOT IS_VECTOR_ZERO(normal)
				IF GET_LINE_PLANE_INTERSECT(vPointOnPlane, p1, p2, normal, vCorner1)			
					IF GET_DISTANCE_BETWEEN_COORDS(p1,vPointOnPlane) < GET_DISTANCE_BETWEEN_COORDS(p1,p2)
						IF POINT_IN_TRI(vPointOnPlane,vCorner1,vCorner4,vCorner3)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIf

		return false
	ENDFUNC
	#if ENABLE_ENHANCED_HUD_FEATURES
	FUNC BOOL IS_POINT_BLOCKED_BY_QUAD(vector vCoord)
		int i
		IF DOES_CAM_EXIST(GET_RENDERING_CAM())
			IF IS_CAM_RENDERING(GET_RENDERING_CAM())
				vector vCam = GET_CAM_COORD(GET_RENDERING_CAM())
				REPEAT COUNT_OF(quadBlocks) i
					IF DOES_LINE_INTERSECT_QUAD(vCam,vCoord,quadBlocks[i].vCorner[0],quadBlocks[i].vCorner[1],quadBlocks[i].vCorner[2],quadBlocks[i].vCorner[3])
						RETURN TRUE
					ENDIF
				ENDREPEAT				
			ENDIF
		ENDIF
		RETURN FALSE
	ENDFUNC
	
	
	PROC ADD_BLOCKING_PLANE(int iEntry,vector v1,vector v2,vector v3,vector v4)
		quadBlocks[iEntry].vCorner[0] = v1
		quadBlocks[iEntry].vCorner[1] = v2
		quadBlocks[iEntry].vCorner[2] = v3
		quadBlocks[iEntry].vCorner[3] = v4
	endproc
#endif


#IF IS_DEBUG_BUILD
PROC DISPLAY_STRING_WITH_NUMBER(float xi, float yi,string aString,float fNum)
	SET_TEXT_SCALE(0.5,0.5)
	TEXT_LABEL_63 aLabel
	aLabel += aString
	aLabel += GET_STRING_FROM_FLOAT(fNum)
	DISPLAY_TEXT_WITH_LITERAL_STRING(xi,yi,"string",aLabel)
ENDPROC

proc DISPLAY_NUMBER(int iNum, float xi, float yi)
	DISPLAY_TEXT_WITH_NUMBER(xi,yi,"NUM_0",iNum)	
ENDPROC
#ENDIF

FUNC BOOL CHOPPER_INIT(structHelicopterHUDVars &HUD, bool onlyShowHUDmarkers = FALSE)

//	#IF IS_DEBUG_BUILD
//	parentWidget = parentW
//	#ENDIF
     hud.onlyShowHUDmarkers = onlyShowHUDmarkers
	  
	hud.boxColourUnscanned.r = 93
	hud.boxColourUnscanned.g = 182
	hud.boxColourUnscanned.b = 229	
	hud.boxColourtarget.r = 255
	hud.boxColourtarget.g = 0
	hud.boxColourtarget.b = 0
	hud.boxColourFriendly.r = 255//137
	hud.boxColourFriendly.g = 255//180
	hud.boxColourFriendly.b = 255//45
	hud.scanningColour.r = 255
	hud.scanningColour.g = 40
	hud.scanningColour.b = 0
	

	HUD.fMaxChopperSpeed = 40.0
	HUD.chopperPanFactor = HUD_PAN_FACTOR_DEFAULT
	HUD.ChopperZoomFactor = 20 //4.35
	HUD.fFov = 28.0
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		HUD.fFov = 40
	ENDIF
	HUD.fLineAInset = 0.234
	HUD.colR = 22
	HUD.colG = 201
	HUD.colB = 39
	HUD.colA = 181
	HUD.fBoxSize = 0.044
	HUD.fTextY = 0.02	
	HUD.fSightRangeMultiplier = 1240.0
	HUD.fSightRangeMultiplier2 = 5000.0
	HUD.fRangeMultiplier = 1000.0
	HUD.hudBarY = 0.829
	HUD.hudReticuleSize = 0.096
	HUD.hudHorizY = 0.865
	HUD.hudHorizWidth = 0.567
	HUD.hudHorizHeight = 0.087	
	HUD.fTargetHeight = 140.0
	HUD.fChopperVerticalSpeed = 1.0
	HUD.fChopperVerticlAccel = 3.0
	HUD.fMaxVertSpeedFactor = 1.0	

	HUD.elementWidth = 0.052
	HUD.elementHeight = 0.75
	HUD.elementArrowX = 0.86
	HUD.elementArrowSize =0.02
	HUD.fTextRangeScale = 0.65
	HUD.textSpace = 0.024
	HUD.fMinFindPedOnScreenRange = 0.12
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REQUEST_ADDITIONAL_TEXT("CHOPPER",OBJECT_TEXT_SLOT)
		IF HAS_THIS_ADDITIONAL_TEXT_LOADED("CHOPPER",OBJECT_TEXT_SLOT)
			RETURN TRUE
		ENDIF
	ELSE
	REQUEST_ADDITIONAL_TEXT("CHOPPER",MINIGAME_TEXT_SLOT)
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("CHOPPER",MINIGAME_TEXT_SLOT)
		RETURN TRUE
	ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC FLOAT GET_HUD_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=FALSE)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC

FUNC BOOL IS_HELIHUD_ACTIVE(structHelicopterHUDVars &HUD)
	IF HUD.bHUDIsActive = TRUE	
		return TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



func bool getHUDBoxColour(structHelicopterHUDVars &HUD,int pedRelType, int &r, int &g, int&b)
	SWITCH pedRelType
		CASE 0 //unscanned
			r = hud.boxColourUnscanned.r
			g = hud.boxColourUnscanned.g
			b = hud.boxColourUnscanned.b
			return TRUE
		BREAK
		
		CASE 1
			r = 255
			g = 255
			b = 0
			return TRUE
		BREAK
		
		CASE 2
			r = hud.boxColourtarget.r
			g = hud.boxColourtarget.g
			b = hud.boxColourtarget.b
			return TRUE
		BREAK
		
		CASE 3
			r =  hud.boxColourFriendly.r //40
			g = hud.boxColourFriendly.g  //253
			b = hud.boxColourFriendly.b //40
			return TRUE
		BREAK
	
	ENDSWITCH
		return FALSE
ENDFUNC

PROC SET_INFRARED(bool bOnOff, bool dontResetChopperCam=FALSE)
	if bOnOff = TRUE
		IF NOT dontResetChopperCam
			CLEAR_TIMECYCLE_MODIFIER()
			cprintln(debug_trevor3,"CLEAR modifier infrared")
		ENDIF
		SET_SEETHROUGH(TRUE)
		
		//SEETHROUGH_SET_MAX_THICKNESS(80)
		//SEETHROUGH_SET_FADE_STARTDISTANCE(st_2)
		//SEETHROUGH_SET_FADE_ENDDISTANCE(st_3)
		//SEETHROUGH_SET_HIGHLIGHT_NOISE(st_4)
		//SEETHROUGH_SET_HILIGHT_INTENSITY(st_5)
	else		
		
		SET_SEETHROUGH(FALSE)
		IF NOT dontResetChopperCam
			SET_TIMECYCLE_MODIFIER("eyeinthesky")
			cprintln(debug_trevor3,"Set modifier infrared")
		ENDIF
	ENDIF	
ENDPROC

proc set_chopper_to_autopilot(structHelicopterHUDVars &HUD,VEHICLE_INDEX vehChopper,bool setAutopilotOn)
	if IS_VEHICLE_DRIVEABLE(vehChopper)
		hud.vehChopper = vehChopper
		if setAutopilotOn
			HUD.bMoveChopper = TRUE		
			hud.chopperHeading =  GET_ENTITY_HEADING(vehChopper)
		ELSE
			HUD.bMoveChopper = FALSE
		ENDIF
	ENDIF
ENDPROC

proc set_chopper_speed_limits(structHelicopterHUDVars &HUD, float minSpeed = 0.0, float maxSpeed=40.0)
	hud.fMaxChopperSpeed = maxSpeed
	hud.fminChopperSpeed = minSpeed
ENDPROC

#if ENABLE_ENHANCED_HUD_FEATURES
PROC ADD_HUD_VIEW_SPHERE_BLOCKING(structHelicopterHUDVars &HUD,int iIndex,vector vLoc,float fRadius)
	hud.bSphereBlockersActive = TRUE
	hud.sphereBlocker[iIndex].inUse = TRUE
	hud.sphereBlocker[iIndex].vLoc = vLoc
	hud.sphereBlocker[iIndex].frad = fRadius
ENDPROC

FUNC BOOL IS_VIEW_BLOCKED_BY_SPHERES(structHelicopterHUDVars &HUD, ped_index pedToView)
	int iR
	IF hud.bSphereBlockersActive = TRUE
		IF DOES_ENTITY_EXIST(HUD.camEntity)
			REPEAT COUNT_OF(hud.sphereBlocker) iR
				IF DOES_LINE_SPHERE_INTERSECT(GET_ENTITY_COORDS(HUD.camEntity),GET_ENTITY_COORDS(pedToView),hud.sphereBlocker[iR].vLoc,hud.sphereBlocker[iR].frad)
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
#endif


PROC SET_TEXT_FOR_HUD(int r,int g,int b, float fScale, bool shadow=FALSE)
//	float fFontScale=0.319
	
	SET_TEXT_SCALE(fScale,fScale)
	SET_TEXT_COLOUR(r,g,b,150)
	IF shadow
		SET_TEXT_DROPSHADOW(5,0,0,0,255)
	ENDIF
	SET_TEXT_CENTRE(TRUE)
	
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC



#if ENABLE_ENHANCED_HUD_FEATURES
func int FIND_HUD_MESSAGE_ENTRY(structHelicopterHUDVars &HUD,int thisEntry)
	int i
	REPEAT COUNT_OF(HUD.message) i
		if HUD.message[i].messageID = thisEntry
			if HUD.message[i].active = true
				return i
			ENDIF
		ENDIF
	ENDREPEAT
	return -1
ENDFUNC


func int ADD_HUD_MESSAGE(structHelicopterHUDVars &HUD,STRING textLabel, float x, float y, float size, int thisSpeed, int pauseBeforePrinting = 0, int pauseOnCompletion=-1, bool deleteOnCompletion=FALSE, int waitUntilThisMessageCompleted=-1,int deletePreviousMessages=-1)
	int i
	bool bEntryFound
	textLabel=textLabel
	while bEntryFound = FALSE	
		if HUD.message[i].active = FALSE
			HUD.message[i].messageID = HUD.messageCounter
			/*
#IF __ALLOW_CALLS_TO_LITERAL_STRING_COMMANDS
			HUD.message[i].HelicopterHudLabel = GET_STRING_FROM_TEXT_FILE(textLabel)
#ENDIF
			HUD.message[i].HelicopterHudTextlabel = textLabel
			*/
			HUD.message[i].charsDisplayed = 0
			HUD.message[i].fx = x
			HUD.message[i].fy = y
			HUD.message[i].size = size
			HUD.message[i].nextCharTime = 0
		//	HUD.message[i].labelLength = GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(textLabel)
			HUD.message[i].textSpeed = thisSpeed
			HUD.message[i].pauseBeforePrinting = pauseBeforePrinting
			HUD.message[i].pauseOnCompletion = pauseOnCompletion
			HUD.message[i].waitForMessage = waitUntilThisMessageCompleted
			HUD.message[i].deleteOnCompletion = deleteOnCompletion	
			HUD.message[i].deletePreviousMessages = deletePreviousMessages
			HUD.message[i].g=255
			HUD.message[i].active = TRUE
			HUD.message[i].messagePrinting = FALSE
			HUD.message[i].completed = FALSE
		
			HUD.messageCounter++
			if HUD.message[i].waitForMessage = -1			
				HUD.message[i].pauseBeforePrinting += TIMERA()
			ENDIF
			
			return HUD.messageCounter-1
		ENDIF
		i++
		if i = MAX_MESSAGES
			SCRIPT_ASSERT("Helihud message array overrun. Increase MAX_MESSAGES const.")
			return -1
		ENDIF
	ENDWHILE
	return -1
ENDFUNC

/*
func int ADD_HUD_MESSAGE_WITH_NUMBER(structHelicopterHUDVars &HUD,STRING textLabel, int number, float x, float y, float size, int thisSpeed, int pauseBeforePrinting = 0, int pauseOnCompletion=-1, bool deleteOnCompletion=FALSE, int waitUntilThisMessageCompleted=-1,int deletePreviousMessages=-1)
	int thisMes = ADD_HUD_MESSAGE(HUD,textLabel,x,y,size,thisSpeed,pauseBeforePrinting,pauseOnCompletion,deleteOnCompletion,waitUntilThisMessageCompleted,deletePreviousMessages)
	
	int iEntry = FIND_HUD_MESSAGE_ENTRY(HUD,thisMes)
	HUD.message[iEntry].HelicopterHudLabel += number
	return thisMes
ENDFUNC
*/

func bool IS_HUD_MESSAGE_DISPLAYING(structHelicopterHUDVars &HUD,int iEntry)

	int iMes = FIND_HUD_MESSAGE_ENTRY(HUD,iEntry)
	if iMes != -1
		if HUD.message[iMes].charsDisplayed > 0
			return TRUE
		ENDIF
	ENDIF
	
	return FALSE	
ENDFUNC

func bool REMOVE_HUD_MESSAGE(structHelicopterHUDVars &HUD,int messageID)	

	int iMes = FIND_HUD_MESSAGE_ENTRY(HUD,messageID)
	if iMes != -1
		HUD.message[iMes].active = FALSE		
	ENDIF
		
	return TRUE
ENDFUNC

PROC RELEASE_ALL_HUD_MESSAGES(structHelicopterHUDVars &HUD)
	int i
	repeat COUNT_OF(HUD.message) i
		 HUD.message[i].active = FALSE
	ENDrepeat	
ENDPROC

PROC SET_HUD_MESSAGE_COLOUR(structHelicopterHUDVars &HUD,int iEntry,int r,int g,int b)
	int iMes = FIND_HUD_MESSAGE_ENTRY(HUD,iEntry)
	HUD.message[iMes].r = r
	HUD.message[iMes].g = g
	HUD.message[iMes].b = b
ENDPROC

/*
PROC DISPLAY_HUD_MESSAGES(structHelicopterHUDVars &HUD)
	
	
	
		
		int i
		
		TEXT_LABEL_63 hudMessage
		
		if resetTime = TRUE
			resetTime = FALSE
			i=0
			HUD.message[i].charsDisplayed = 0
			HUD.message[i+1].charsDisplayed = 0
			HUD.currentMessage = 0
		ENDIF
		bool bTemp
		for i = 0 to MAX_MESSAGES-1
			if HUD.message[i].active = TRUE
				if HUD.message[i].messagePrinting = FALSE
					if HUD.message[i].waitForMessage != -1
						
						int iPrevMes = FIND_HUD_MESSAGE_ENTRY(HUD,HUD.message[i].waitForMessage)
						bTemp = false
						if iPrevMes = -1 
							bTemp = true
						endif
						if iPrevMes  != -1 
							if HUD.message[iPrevMes].completed = true
								bTemp = TRUE
							ENDIF
						endif
						if bTemp = TRUE
							if HUD.message[i].pauseBeforePrinting > 0						
								HUD.message[i].pauseBeforePrinting += TIMERA()
								HUD.message[i].waitForMessage = -1
							ELSE
								HUD.message[i].messagePrinting = TRUE
							ENDIF					
						ENDIF
					ELSE
						if TIMERA() > HUD.message[i].pauseBeforePrinting
							HUD.message[i].messagePrinting = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				if HUD.message[i].messagePrinting = TRUE
					SET_TEXT_FOR_HUD(HUD.message[i].r,HUD.message[i].g,HUD.message[i].b,HUD.message[i].size)
					if HUD.message[i].charsDisplayed = 0
						HUD.message[i].nextCharTime = TIMERA()-1
					ENDIF
					if HUD.message[i].charsDisplayed < HUD.message[i].labelLength
						if TIMERA() > HUD.message[i].nextCharTime
							HUD.message[i].nextCharTime += HUD.message[i].textSpeed
							HUD.message[i].charsDisplayed++
						ENDIF
						hudMessage = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(HUD.message[i].label,HUD.message[i].charsDisplayed)
						#IF IS_DEBUG_BUILD
						DISPLAY_TEXT_WITH_LITERAL_STRING(HUD.message[i].fx,HUD.message[i].fy,"STRING",hudMessage)
						#ENDIF
						if HUD.message[i].charsDisplayed = HUD.message[i].labelLength					
							if HUD.message[i].pauseOnCompletion != -1
								HUD.message[i].pauseOnCompletion += TIMERA()
							ELSE
								HUD.message[i].completed = TRUE
								if HUD.message[i].deletePreviousMessages != -1
									REMOVE_HUD_MESSAGE(HUD,HUD.message[i].deletePreviousMessages)
								ENDIF
								if HUD.message[i].deleteOnCompletion = TRUE							
									REMOVE_HUD_MESSAGE(HUD,HUD.message[i].messageID)
								endif
							ENDIF
						ENDIF
					ELSE
						hudMessage = HUD.message[i].label
						#IF IS_DEBUG_BUILD
							DISPLAY_TEXT_WITH_LITERAL_STRING(HUD.message[i].fx,HUD.message[i].fy,"STRING",hudMessage)
						#ENDIF
						if HUD.message[i].pauseOnCompletion != -1
						
							if TIMERA()>HUD.message[i].pauseOnCompletion
								HUD.message[i].completed = TRUE
								if HUD.message[i].deletePreviousMessages != -1
									REMOVE_HUD_MESSAGE(HUD,HUD.message[i].deletePreviousMessages)
								
								ENDIF
								if HUD.message[i].deleteOnCompletion = TRUE							
								
									REMOVE_HUD_MESSAGE(HUD,HUD.message[i].messageID)
								
								endif
							ENDIF
						ENDIF
												
					ENDIF

				ENDIF		
			ENDIF
		ENDFOR
			
ENDPROC
*/
#ENDIF






#IF IS_DEBUG_BUILD

	int iDebugScanPed
	
	bool bHighlight, bshowlist
	//bool bpedBit[10]
	//bool bForceScan

	
	//bool bWidgetMade

	
	PROC setUpWidgets(structHelicopterHUDVars &HUD, WIDGET_GROUP_ID thisParentWidget)
		if not DOES_WIDGET_GROUP_EXIST(HUD.CopChopperWidget)
		//	bWidgetMade = true
			IF DOES_WIDGET_GROUP_EXIST(thisParentWidget)
				SET_CURRENT_WIDGET_GROUP(thisParentWidget)
				HUD.CopChopperWidget = START_WIDGET_GROUP("Cop Chopper Widget")
					#if ENABLE_ENHANCED_HUD_FEATURES
				
					ADD_WIDGET_BOOL("Show heli data",HUD.bShowData)
					ADD_WIDGET_BOOL("HUD.bHoverInArea",HUD.bHoverInArea)	
					ADD_WIDGET_FLOAT_READ_ONLY("field of view",HUD.ffov)
					ADD_WIDGET_FLOAT_READ_ONLY("max speed",HUD.fMaxChopperSpeed)
					ADD_WIDGET_VECTOR_SLIDER("HUD.vHoverCenter",HUD.vHoverCenter,-8000.0,8000.0,1.0)
					ADD_WIDGET_VECTOR_SLIDER("hud.vHoverPoint",hud.vHoverPoint,-8000.0,8000.0,1.0)
					ADD_WIDGET_INT_SLIDER("Red",hud.boxColourFriendly.r,0,255,1)
					ADD_WIDGET_INT_SLIDER("Green",hud.boxColourFriendly.g,0,255,1)
					ADD_WIDGET_INT_SLIDER("Blue",hud.boxColourFriendly.b,0,255,1)
					ADD_WIDGET_FLOAT_SLIDER("Z speed",zSpeed,-20.0,20.0,0.01)
					ADD_WIDGET_FLOAT_SLIDER("Near Clip",HUD.nearClip,0,20.0,0.01)
					ADD_WIDGET_FLOAT_SLIDER("maxNearClip",HUD.maxNearClip,0,20.0,0.01)
					ADD_WIDGET_FLOAT_SLIDER("up ang LOW",HUD.angUPLow,0,3,0.01)
					ADD_WIDGET_FLOAT_SLIDER("up ang High",HUD.angUpHigh,0,3,0.01)
					ADD_WIDGET_FLOAT_SLIDER("down ang LOW",HUD.angDownLow,0,3,0.01)
					ADD_WIDGET_FLOAT_SLIDER("down ang High",HUD.angDownHigh,0,3,0.01)					
					ADD_WIDGET_FLOAT_SLIDER("ang range up",HUD.AngHigh,-90,0,0.01)
					ADD_WIDGET_FLOAT_SLIDER("ang range down",HUD.angLow,-90,0,0.01)
					

	
					#ENDIF
					
					//ADD_WIDGET_BOOL("show ped scan list",bshowlist)
					//ADD_WIDGET_INT_READ_ONLY("No of remembered peds",HUD.iCurrentPedScanCount)	
					//ADD_WIDGET_INT_SLIDER("Select ped in scan group",iDebugScanPed,0,MAX_SCANNABLE_PEDS-1,1)
					//ADD_WIDGET_BOOL("Highlight selected ped",bHighlight)
					/*
					START_WIDGET_GROUP("Scan ped data")
						wPedTextData = ADD_TEXT_WIDGET("Ped Type")
						//ADD_WIDGET_STRING(sPedType)				
						ADD_WIDGET_BOOL("Ped scanned",bpedBit[0])
						ADD_WIDGET_BOOL("bShowOffScreenArrow",bpedBit[1])
						ADD_WIDGET_BOOL("bAlwaysShow",bpedBit[2])
						ADD_WIDGET_BOOL("bAlwaysShowOnceScanned",bpedBit[3])
						ADD_WIDGET_BOOL("dataHeld",bpedBit[4])
						ADD_WIDGET_BOOL("enhancedData",bpedBit[5])
						ADD_WIDGET_BOOL("is criminal",bpedBit[6])
						ADD_WIDGET_BOOL("bSelected",bpedBit[7])
						ADD_WIDGET_BOOL("scan ped now",bForceScan)
					STOP_WIDGET_GROUP()
*/
					
				STOP_WIDGET_GROUP()
				
				CLEAR_CURRENT_WIDGET_GROUP(thisParentWidget)

			ENDIF
			
			
			
		ELSE
		
		ENDIF
		
		
		
	ENDPROC
	
	

	proc debugChopper(structHelicopterHUDVars &HUD)

		if DOES_WIDGET_GROUP_EXIST(HUD.CopChopperWidget) //or bWidgetMade = true			
			
			/*
			if HUD.pedScanList[iDebugScanPed].relType = HUD_FRIEND
				sPedType = "Ped type: HUD_FRIEND"
			ELSE
				if HUD.pedScanList[iDebugScanPed].relType = HUD_BYSTANDER
					sPedType = "Ped type: HUD_BYSTANDER"
				ELSE
					if HUD.pedScanList[iDebugScanPed].relType = HUD_FOE
						sPedType = "Ped type: HUD_FOE"
					ELSE
						if HUD.pedScanList[iDebugScanPed].relType = HUD_UNKNOWN
							sPedType = "Ped type: HUD_UNKNOWN"
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			SET_CONTENTS_OF_TEXT_WIDGET(wPedTextData,sPedType)
			for i = 0 to 9
				if IS_BIT_SET(HUD.pedScanList[iDebugScanPed].BITPedData,i) 
				//	bpedBit[i] = true
				ELSE
				//	bpedBit[i] = FALSE
				ENDIF
			ENDFOR
			*/
			if bHighlight = TRUE
				if not IS_ENTITY_DEAD(HUD.pedScanList[iDebugScanPed].pedID)
					DRAW_MARKER(MARKER_CYLINDER, GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[iDebugScanPed].pedID,0), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<3, 3, 3>>, 255, 0, 128, 178)
					DRAW_DEBUG_CROSS(GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[iDebugScanPed].pedID,0),0.1)
				ENDIF
			ENDIF							
		ENDIF
	ENDPROC


FUNC bool save_debug(structHelicopterHUDVars &HUD, string toPrint, int iEntry=-1, float fEntry=-1.0, bool bEntry=FALSE)
	if HUD.bSaveDebug = TRUE

		SAVE_STRING_TO_DEBUG_FILE(toPrint)
		SAVE_STRING_TO_DEBUG_FILE(" ")
		if iEntry != -1
			SAVE_INT_TO_DEBUG_FILE(iEntry)
		ELSE
			if fEntry != -1.0
				SAVE_float_TO_DEBUG_FILE(fEntry)
			ELSE				
				if bEntry = FALSE
					SAVE_STRING_TO_DEBUG_FILE("FALSE")
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("TRUE")
				ENDIF
			ENDIF
		ENDIF
		

		SAVE_NEWLINE_TO_DEBUG_FILE()
	
	ENDIF
	
	RETURN TRUE
ENDFUNC
#ENDIF

proc controlAudio(structHelicopterHUDVars &HUD)
	
	IF HUD.bAudioLoaded = FALSE
	
		IF REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\POLICE_CHOPPER_CAM")
			HUD.bAudioLoaded = TRUE
			HUD.soundID_zoom = GET_SOUND_ID()
			HUD.soundID_turn = GET_SOUND_ID()
			HUD.soundID_background = GET_SOUND_ID()		
			HUD.soundID_scan = GET_SOUND_ID()
			HUD.soundID_scanloop = GET_SOUND_ID()
			HUD.soundID_scangood = GET_SOUND_ID()
			HUD.soundID_scanbad = GET_SOUND_ID()
			HUD.soundID_scanRange = GET_SOUND_ID()
		ENDIF
		
	ENDIF

	
ENDPROC


/*
FUNC BOOL ACTIVATE_CHOPPER_HUD(structHelicopterHUDVars &HUD, VEHICLE_INDEX vChopper)
	vector vCoords
	if not IS_ENTITY_DEAD(vChopper)
		if not IS_ENTITY_DEAD(PLAYER_PED_ID())
			HUD.bMoveChopper = TRUE
			HUD.bActivateChopperCam = TRUE
			HUD.bDisplayHUD = TRUE
			HUD.camEntity = vChopper
			vCoords = GET_ENTITY_COORDS(vChopper)
			HUD.fChopperHeight = vCoords.z
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vChopper)
			return TRUE
		ENDIF
	ENDIF
	SCRIPT_ASSERT("ACTIVATE_CHOPPER_HUD: Vehicle doesn't exist.")
	return FALSE
ENDFUNC
*/


proc GET_HUD_COORD(structHelicopterHUDVars &HUD)

	if DOES_CAM_EXIST(hud.camChopper) and HUD.mpCamFudge = FALSE and HUD.bIsCamAttached 
	
	#IF USE_TU_CHANGES
	and not HUD.isMultiplayerMode
	#ENDIF
	
		//if HUD.fStickPitch != 0 or HUD.fStickYaw != 0 or IS_VECTOR_ZERO(hud.vAimAt)
			
			vector vTempCoord,vRot,vTargetCoord
			float fx,fy,fLength
			int iPoint = 2
			iPoint=iPoint
			vTempCoord = GET_CAM_COORD(hud.camChopper)//GET_RENDERING_CAM())
			vRot = GET_CAM_ROT(hud.camChopper)
	
			//if groundHeight = 0
				//GET_GROUND_Z_FOR_3D_COORD(vTempCoord,fz)
			
			 	//GET_CLOSEST_VEHICLE_NODE(vTempCoord,vReturnedCoord)
				
			//first check if player is aiming at a scan ped
			int j
			vector vPedPos 
			float foundHeight=0.0
			bool hitFound=FALSE
			bool foundPedHeight=FALSE
			foundPedHeight=foundPedHeight
			
		
			
			float heightOnLine = vTempCoord.z
			heightOnLine=heightOnLine
			
			IF hitFound = FALSE
				
				float heightFromPoint=0
			
				
				float fSegLength
	//			int FoundLengthSeg
				
				//float fHeightDiff =999999.0
				
				fSegLength = cos(vRot.x) * 50.0
				#if ENABLE_ENHANCED_HUD_FEATURES
				
				SET_CAM_NEAR_CLIP(hud.camChopper,HUD.nearClip)
				
				IF HUD.bShowData
				/*
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					vector vLookAt
					float horizDist = absf(cos(vRot.x)) * 1000.0
					vLookAt.x = (SIN(vRot.z) * horizDist) *-1.0
					vLookAt.y = COS(vRot.z) * horizDist
					vLookAt.z = SIN(vRot.x) * 1000.0
					vLookAt += vTempCoord
					vLookAt=vLookAt
					DRAW_DEBUG_LINE(vTempCoord,vLookAt,255,0,0)
					*/
					//cprintln(debug_Trevor3,HUD.ffov)
				endif
				#endif
				while ipoint<21
					iPoint++
				
					
					fLength = fSegLength * ipoint
					fx = (SIN(vRot.z) * fLength) *-1.0
					fy = COS(vRot.z) * fLength
					
					heightfromPoint = GET_APPROX_HEIGHT_FOR_AREA(vTempCoord.x+fx-3.0,vTempCoord.y+fy-3.0,vTempCoord.x+fx+3.0,vTempCoord.y+fy+3.0)
					heightfromPoint -= 20.0
					heightOnLine = vTempCoord.z + (sin(vRot.x) * (50.0 * ipoint))
					
					#if ENABLE_ENHANCED_HUD_FEATURES
					IF HUD.bShowData
					/*
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						DRAW_DEBUG_BOX(<<vTempCoord.x+fx-3.0,vTempCoord.y+fy-3.0,vTempCoord.z-2.0>>,<<vTempCoord.x+fx+3.0,vTempCoord.y+fy+3.0,vTempCoord.z+2.0>>)
						DRAW_DEBUG_BOX(<<vTempCoord.x+fx-3.0,vTempCoord.y+fy-3.0,heightOnLine-2.0>>,<<vTempCoord.x+fx+3.0,vTempCoord.y+fy+3.0,heightOnLine+2.0>>,255,0,0)
						DRAW_DEBUG_LINE(<<vTempCoord.x+fx,vTempCoord.y+fy,vTempCoord.z>>,<<vTempCoord.x+fx,vTempCoord.y+fy,heightfromPoint>>)
						*/
					ENDIF
					#endif
					
					if heightfromPoint > heightOnLine
						if hitFound = FALSE
							hitFound = TRUE
							foundHeight = heightfromPoint
							ipoint = 21
	//						FoundLengthSeg = ipoint
							//fHeightDiff = heightfromPoint - heightOnLine
						ELSE
						/*
							if heightfromPoint - heightOnLine < fHeightDiff
								foundHeight = heightfromPoint
	//							FoundLengthSeg = ipoint
								fHeightDiff = heightfromPoint - heightOnLine
							ENDIF
							*/
						ENDIF
				//	ELSE
				//		hitFound = FALSE
					ENDIF
					


				ENDWHILE
			endif
			
			
			
		//	hitFound = FALSE
			float fCheckHeight
			if hitFound = FALSE		
				float fGroundHeight
				
				IF GET_GROUND_Z_FOR_3D_COORD(vTempCoord,fGroundHeight)
					fCheckHeight = vTempCoord.z - fGroundHeight
					IF fCheckHeight < 150
						fCheckHeight = 150
					ENDIF
				ELSE
					fCheckHeight = 150					
				ENDIF
				
				
				IF vRot.x < 0
					fLength = ABSF(fCheckHeight / TAN(vRot.X))
				//	fx = (SIN(vRot.z) * fLength) *-1.0
				//	fy = COS(vRot.z) * fLength
						
					foundHeight = vTempCoord.z - fCheckHeight
				ELSE //cam is looking up
					fLength = 1000.0
				//	fx = (SIN(vRot.z) * fLength) *-1.0
				//	fy = COS(vRot.z) * fLength
					foundHeight = vTempCoord.z + (fLength * TAN(vRot.X))
					
				ENDIF
				//extract hit point from cam angle, presuming the chopper to be 150 above ground
				
				
			ELSE	
			/*
				if vTempCoord.z-foundHeight < 60.0
					//prevent found height being too close to chopper camera
					foundHeight=vTempCoord.z-60.0
				ELSE
					IF NOT foundPedHeight
						foundHeight -= 10.0				
					ENDIF
				ENDIF
				if vRot.x = 0.0
					flength = 1000.0
				ELSE
					flength = absf((vTempCoord.z-foundHeight)/tan(vRot.x))
				ENDIF*/
				//CPRINTLN(DEBUG_TREVOR3,"pASS")
			ENDIF
			fx = (SIN(vRot.z) * fLength) *-1.0
			fy = COS(vRot.z) * fLength
			vTargetCoord = <<fx+vTempCoord.x,fy+vTempCoord.y,foundHeight>>
			
			repeat count_of(Hud.pedScanList) j
				If NOT IS_PED_INJURED(Hud.pedScanList[j].pedID)
					IF is_point_in_screeen_area(GET_ENTITY_COORDS(Hud.pedScanList[j].pedID),0.4,0.4,0.6,0.6)
						hitFound = TRUE
						foundPedHeight=TRUE
						vTargetCoord = GET_ENTITY_COORDS(Hud.pedScanList[j].pedID)						
						foundHeight = vPedPos.z
					endif
				endif
			ENDREPEAT
			
			
		//showFloatOnScreen(0.1,0.1,vTargetCoord.x)
		//	showFloatOnScreen(0.17,0.1,vTargetCoord.y)
		//	showFloatOnScreen(0.24,0.1,vTargetCoord.z)
	
			HUD.vAimAt = vTargetCoord
			//RETURN vTargetCoord
	//	ENDIF
	ENDIF
	
	#if ENABLE_ENHANCED_HUD_FEATURES
			IF HUD.bShowData
				/*
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				DRAW_DEBUG_SPHERE(HUD.vAimAt,4.0)
				//PRINTLN(HUD.vAimAt)
				//PRINTLN("Chopper : ",vTempCoord.z," gound : ",vTargetCoord.z," above ground : ",fCheckHeight)
				*/
			ENDIF
	#endif
	
	//RETURN <<0,0,0>>
ENDPROC
/*

//INFO:
//PARAM NOTES:
//PURPOSE: Gets the coords of a point on the ground projected from the rendering camera.
proc GET_HUD_COORD(structHelicopterHUDVars &HUD)
float zOff

	//CAMERA_INDEX cam
	if DOES_CAM_EXIST(HUD.camChopper)  //GET_RENDERING_CAM())
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
		//cam = GET_RENDERING_CAM()		
		int iHit
		bool doShapeState
		
		vector vFound
		
		
		float fHeight, fZ
		vector vRot, vTempCoord, vTargetCoord, vReturnedCoord
		
		
		switch GET_SHAPE_TEST_LOS_PROBE_RESULT(hUD.shptest, iHit, vFound)
			case SHAPETEST_STATUS_RESULTS_READY
				if iHit != 0
					HUD.vAimAt=vFound
					//vStoreHit = vFound
					
			
				ELSE
		
				ENDIF
				
				doShapeState=true
			BREAK
			case SHAPETEST_STATUS_NONEXISTENT
			
				doShapeState=true
			BREAK
			case SHAPETEST_STATUS_RESULTS_NOTREADY
				
				doShapeState=FALSE
			break
		ENDSWITCH
		
		
		if doShapeState = true
			vTempCoord = GET_CAM_COORD(HUD.camChopper) + <<0,0,-0.5>>//GET_RENDERING_CAM())
								
			vRot = GET_CAM_ROT(HUD.camChopper)
			zOff = 400.0 * sin(vRot.x)
				
			float fDist = 400.0 * cos(vRot.x)
			float xOff = SIN(vRot.z) * fDist		
			float yOff = COS(vRot.z) * fDist
			
			//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
			//DRAW_DEBUG_LINE(vTempCoord,vTempCoord+<<-xOff,yOff,zOff>>)
	
	
			hUD.shptest = START_SHAPE_TEST_LOS_PROBE(vTempCoord,vTempCoord+<<-xOff,yOff,zOff>>)	
			
					
		ENDIF

		vTempCoord = GET_CAM_COORD(HUD.camChopper)//GET_RENDERING_CAM())		
		
		fz = HUD.vAimAt.z
		
		fHeight = vTempCoord.z - fZ
		
		vRot = GET_CAM_ROT(HUD.camChopper)//cam)
	
		float fDist = fHeight / ABSF(TAN(vRot.x))
		float xOff = SIN(vRot.z) * fDist		
		float yOff = COS(vRot.z) * fDist
		
		
		vTargetCoord = <<vTempCoord.x-xOff,vTempCoord.y+yOff,fZ>>
		
		
		
		//float fSphereRadius =  GET_DISTANCE_BETWEEN_COORDS(vGroundLoc,GET_CAM_COORD(HUD.camChopper))//150.0 //distance to point on ground
		//					HUD.vTargetedGround.x = (COS(HUD.vCamRot .x) * fSphereRadius) * sin(-HUD.vCamRot .z)
		//					HUD.vTargetedGround.y = (COS(HUD.vCamRot .x) * fSphereRadius) * cos(-HUD.vCamRot .z)
		//					HUD.vTargetedGround.z = (sin(HUD.vCamRot .x) * fSphereRadius)
		//					HUD.vTargetedGround += GET_CAM_COORD(HUD.camChopper)
		HUD.vAimAt = vTargetCoord		
	//	DRAW_DEBUG_BOX(HUD.vAimAt-<<5.0,5.0,5.0>>,HUD.vAimAt+<<5.0,5.0,5.0>>)
	//	DRAW_DEBUG_BOX(vStoreHit-<<5.0,5.0,5.0>>,vStoreHit+<<5.0,5.0,5.0>>,255,0,0,255)
	ENDIF
	
ENDPROC
*/
#IF IS_DEBUG_BUILD
proc debugShowScanList(structHelicopterHUDVars &HUD)
	int i
	repeat COUNT_OF(HUD.pedScanList) i
		showIntOnScreen(0.1,0.1+(i*0.04),NATIVE_TO_INT(HUD.pedScanList[i].pedID))
		
	ENDREPEAT
ENDPROC
#ENDIF
proc REMOVE_PED_FROM_SCAN_LIST(structHelicopterHUDVars &HUD,PED_INDEX pedID)
	//find ped in scan list
	int i
	bool bEntryFound = FALSE
	IF pedID != null
		repeat COUNT_OF(HUD.pedScanList) i
			
			if HUD.pedScanList[i].pedID = pedID
				#if ENABLE_ENHANCED_HUD_FEATURES
				IF HUD.pedScanList[i].iSpecialNameCase != -1
					HUD.specialNames[HUD.pedScanList[i].iSpecialNameCase] = ""
				endif
				
				HUD.pedScanList[i].iSpecialNameCase = -1
				#endif
				bEntryFound = TRUE
			ENDIF
			
			if bEntryFound = TRUE
				if i<COUNT_OF(HUD.pedScanList)-1
					HUD.pedScanList[i] = HUD.pedScanList[i+1]
				ELSE
					HUD.pedScanList[i].lastSeenTime = 0
					HUD.pedScanList[i].pedID = NULL
					HUD.pedScanList[i].lastSeenTime = 0
					HUD.pedScanList[i].fScanTime = 0
					HUD.pedScanList[i].BITnamesAndCrimes = 0
					HUD.pedScanList[i].BITPedData = 0
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,0) //scanned = false
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,1) //bShowOffScreenArrow = FALSE
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,2) //bAlwaysShow = false
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,3) //bAlwaysShowOnceScanned = FALSE
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,4) //dataHeld = false
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,5) //enhancedData = false
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,6) //is criminal = false
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,7) //bSelected = FALSE
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,8) //is multiplayer ped
					CLEAR_BIT(HUD.pedScanList[i].BITPedData,9) //offscreen arrow
					#if ENABLE_ENHANCED_HUD_FEATURES
					HUD.pedScanList[i].iSpecialNameCase = -1
					#endif
					
					
				ENDIF
			ENDIF
		ENDREPEAT
		if bEntryFound = TRUE
			HUD.iCurrentPedScanCount--
		ENDIF
	endif

ENDPROC

proc CLEAR_SCAN_LIST(structHelicopterHUDVars &HUD)
	int i
	for i = 0 to HUD.iCurrentPedScanCount-1
		HUD.pedScanList[i].pedID = NULL
		HUD.pedScanList[i].lastSeenTime = 0
		HUD.pedScanList[i].fScanTime = 0
		HUD.pedScanList[i].BITnamesAndCrimes = 0
		HUD.pedScanList[i].BITPedData = 0
	ENDFOR
	#if ENABLE_ENHANCED_HUD_FEATURES
		repeat COUNT_OF(HUD.specialNames) i
			HUD.specialNames[i] = ""
		ENDREPEAT
	#endif
	HUD.iCurrentPedScanCount=0
ENDPROC

func bool HAS_PED_BEEN_SCANNED(structHelicopterHUDVars &HUD,PED_INDEX pedID)
	int i
	for i = 0 to HUD.iCurrentPedScanCount-1
		if HUD.pedScanList[i].pedID = pedID
			if IS_BIT_SET(HUD.pedScanList[i].BITPedData,0) //scanned bit
				return TRUE
			ELSE
				return FALSE
			ENDIF
		ENDIF
	ENDFOR
	return FALSE
ENDFUNC

func bool IS_PED_IN_SCAN_LIST(structHelicopterHUDVars &HUD,PED_INDEX pedID)
	int i
	REPEAT COUNT_OF(HUD.pedScanList) i
		if HUD.pedScanList[i].pedID = pedID
			return TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC



func int GET_PED_ENTRY_IN_SCAN_LIST(structHelicopterHUDVars &HUD,PED_INDEX pedID)
	int i
	REPEAT COUNT_OF(HUD.pedScanList) i
		if HUD.pedScanList[i].pedID = pedID
			return i
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC EMPTY_SCAN_LIST(structHelicopterHUDVars &HUD)
	HUD.iCurrentPedScanCount = 0
ENDPROC

FUNC BOOL ADD_PED_TO_SCAN_LIST(structHelicopterHUDVars &HUD,PED_INDEX pedID, bool bShowAdvancedData, int HUDtype=HUD_BYSTANDER, bool scanned=FALSE, bool AlwaysShow=FALSE, bool alwaysShowOnceScanned=false, bool ShowOffScreenArrow=FALSE, bool isMultiplayerPed=FALSE)
	
	//int i, crimeCount, crime
	//int rememberCrimesAdded[4]
	//rememberCrimesAdded[0] = -1
	//rememberCrimesAdded[1] = -1
	//rememberCrimesAdded[2] = -1
	//rememberCrimesAdded[3] = -1
	
	//ensure ped not already in scan list
	int i

	REPEAT COUNT_OF(HUD.pedScanList) i
		if i < MAX_SCANNABLE_PEDS
			if HUD.pedScanList[i].pedID = pedID
				return FALSE
			ENDIF
		ENDIF
	ENDREPEAT



	if HUD.iCurrentPedScanCount < MAX_SCANNABLE_PEDS
	AND HUD.iCurrentPedScanCount >= 0
	
		if scanned = TRUE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,0)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,0)
		ENDIF
		
		if ShowOffScreenArrow = TRUE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,1)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,1)
		ENDIF
		
		if AlwaysShow = TRUE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,2)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,2)
		ENDIF
		
		if alwaysShowOnceScanned = TRUE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,3)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,3)
		ENDIF
		
		if bShowAdvancedData = TRUE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,5)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,5)
		ENDIF
		
		if isMultiplayerPed = TRUE						
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,8)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,8)
		ENDIF
		
		if ShowOffScreenArrow = TRUE						
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,9)
		ELSE
			CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITPedData,9)
		ENDIF
		
		HUD.pedScanList[HUD.iCurrentPedScanCount].pedID = pedID		
		
	
		HUD.pedScanList[HUD.iCurrentPedScanCount].relType = HUDtype
		HUD.pedScanList[HUD.iCurrentPedScanCount].lastSeenTime = 0
		
		HUD.pedScanList[HUD.iCurrentPedScanCount].fScanTime = 0
		
		
		
		//insert data about ped
		
		
	//	TEXT_LABEL_15 text_filename = ""
		if not IS_ENTITY_DEAD(pedID)
			if IS_PED_MALE(pedID)	
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITnamesAndCrimes,GET_RANDOM_INT_IN_RANGE(0,7)) //first name
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITnamesAndCrimes,GET_RANDOM_INT_IN_RANGE(0,7)+16) //surname				
			ELSE
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITnamesAndCrimes,GET_RANDOM_INT_IN_RANGE(8,15)) //first name
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITnamesAndCrimes,GET_RANDOM_INT_IN_RANGE(0,7)+16)		
			ENDIF
		ENDIF

		
		
		
		if HUDtype = HUD_BYSTANDER		
			if GET_RANDOM_INT_IN_RANGE(0,3) > 0
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,4) //dataHeld = TRUE
				CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,6) //isCriminal = FALSE								
			ELSE
				CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,4) //dataHeld = FALSE
			ENDIF
		ENDIF
				
		IF HUDtype = HUD_FOE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,4) //dataHeld = TRUE
			SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,6) //isCriminal = TRUE						
		ENDIF
		
		IF HUDtype = HUD_UNKNOWN
			if GET_RANDOM_INT_IN_RANGE(0,5) < 4
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,4) //dataHeld = TRUE
				if GET_RANDOM_INT_IN_RANGE(0,5) < 2
					SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,6) //isCriminal = TRUE	
				ENDIF
			ELSE
				CLEAR_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].bitPedData,4) //dataHeld = FALSE
			ENDIF
		ENDIF
	
		if IS_BIT_SET(HUD.pedScanList[HUD.iCurrentPedScanCount].BITpedData,6) //isCriminal = TRUE	
		
		//	HUD.pedScanList[HUD.iCurrentPedScanCount].status = GET_RANDOM_INT_IN_RANGE(0,3)
			int crimeCount = GET_RANDOM_INT_IN_RANGE(1,4)
			int crime
//			float fTemp
			i = 0
			for i = 0 to crimeCount - 1
				crime = GET_RANDOM_INT_IN_RANGE(1,7) //prevent prostitution coming up randomly
				
				SET_BIT(HUD.pedScanList[HUD.iCurrentPedScanCount].BITnamesAndCrimes,crime+24)
				
			ENDFOR
			
		ENDIF
		
		HUD.iCurrentPedScanCount++
	
		return TRUE
	ELSE
		
		SCRIPT_ASSERT("Maximum allowed scannable peds reached (40). Ask Kev B to increase this if a problem.")
		RETURN FALSE
	ENDIF
		
ENDFUNC

proc set_ped_scan_crime(structHelicopterHUDVars &HUD, ped_index yourPed, int iCrime1, int iCrime2=-1,  int iCrime3=-1,  int iCrime4=-1)
	int i,iThisPed
	REPEAT COUNT_OF(HUD.pedScanList) i
		if i < MAX_SCANNABLE_PEDS
			if HUD.pedScanList[i].pedID = yourPed
				iThisPed = i
				i = COUNT_OF(HUD.pedScanList)
			ENDIF
		ENDIF
	ENDREPEAT
	set_bit(HUD.pedScanList[iThisPed].BITpedData,6)
	SET_BIT(HUD.pedScanList[iThisPed].BITnamesAndCrimes,iCrime1+24)
	IF iCrime2 != -1 SET_BIT(HUD.pedScanList[iThisPed].BITnamesAndCrimes,iCrime2+24) ENDIF
	IF iCrime3 != -1 SET_BIT(HUD.pedScanList[iThisPed].BITnamesAndCrimes,iCrime3+24) ENDIF
	IF iCrime4 != -1 SET_BIT(HUD.pedScanList[iThisPed].BITnamesAndCrimes,iCrime4+24) ENDIF
endproc

func bool SET_PED_IN_SCAN_LIST_AS_SCANNED(structHelicopterHUDVars &HUD, ped_index yourPed, bool hasBeenScanned=true)
	int iPedEntry = GET_PED_ENTRY_IN_SCAN_LIST(HUD,yourPed)
	if iPedEntry != -1
		If hasBeenScanned = TRUE
			SET_BIT(HUD.pedScanList[iPedEntry].BITPedData,0)
		ELSE
			CLEAR_BIT(HUD.pedScanList[iPedEntry].BITPedData,0)
		ENDIF
		return true
	ELSE
		return FALSE
	ENDIF
ENDFUNC



#if ENABLE_ENHANCED_HUD_FEATURES
func bool ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(structHelicopterHUDVars &HUD,PED_INDEX ped,string sName, int iCrime=-1) //,string name, string crime)
	int i,r,v
	repeat COUNT_OF(HUD.pedScanList) i		
		if HUD.pedScanList[i].pedID = ped		
			repeat COUNT_OF(HUD.specialNames) r
				if are_strings_equal(HUD.specialNames[r],"")
					HUD.specialNames[r] = sName
					HUD.pedScanList[i].iSpecialNameCase = r
					//PRINTLN("---Trying to add crime")
					if iCrime !=-1
						for v = 0 to 7
							CLEAR_BIT(HUD.pedScanList[i].BITnamesAndCrimes,24+v)
						ENDFOR
						SET_BIT(HUD.pedScanList[i].BITnamesAndCrimes,23+iCrime)					
					ENDIF
					return true
				ENDIF
			ENDREPEAT						
		ENDIF
	endrepeat
	//PRINTLN("---No crime added")
	return FALSE
ENDFUNC
#ENDIF

func bool IS_HUD_PED_VISIBLE(structHelicopterHUDVars &HUD,PED_INDEX pedID)
	int iEntry = GET_PED_ENTRY_IN_SCAN_LIST(HUD,pedID)
	if TIMERA() - HUD.pedScanList[iEntry].lastSeenTime < 500
		return TRUE
	ENDIF
	return FALSE
ENDFUNC


/*
func bool ADD_HELIHUD_PED_NAME(structHelicopterHUDVars &HUD,int i,string pedName)	
	if i < MAX_SCANNABLE_PEDS
		HUD.pedScanList[i].name = pedName
		return TRUE
	ELSE
		SCRIPT_ASSERT("Helihud max scannable peds array overrun")
		return FALSE
	ENDIF
ENDFUNC

func bool ADD_HELIHUD_PED_CRIME(structHelicopterHUDVars &HUD,int i,string pedCrime)	
	if i < MAX_SCANNABLE_PEDS
		HUD.pedScanList[i].isCriminal = TRUE
		HUD.pedScanList[i].dataHeld = TRUE
		HUD.pedScanList[i].crimes = pedCrime
		return TRUE
	ELSE
		SCRIPT_ASSERT("Helihud max scannable peds array overrun")
		return FALSE
	ENDIF
ENDFUNC
*/
func bool ADD_HELIHUD_MARKER(structHelicopterHUDVars &HUD, HELIHUD_MARKER &hudMarker)
	int i
		
	if HUD.helihudMarkerCount < MAX_HELIHUD_COORD_COUNT
		FOR i = 0 to MAX_HELIHUD_COORD_COUNT-1
			if HUD.helihudMarker[i].entry =-1
				HUD.helihudMarker[i] = hudMarker
				HUD.helihudMarker[i].entry = i								
				HUD.helihudMarkerCount++
				hudMarker.entry = i
				return true
			ENDIF
		ENDFOR						
	ENDIF
	SCRIPT_ASSERT("Max HUD coord markers reached (10). Need to increase MAX_HELIHUD_COORD_COUNT in helicopterhud.sch")
	return FALSE
ENDFUNC

FUNC HELIHUD_MARKER ADD_HELIHUD_MARKER_FOR_COORD(structHelicopterHUDVars &HUD, vector vCoord,int r=255,int g=255, int b=0, helihud_MARKER_TYPE type = MARKER_CROSS)
	HELIHUD_MARKER thisMarker
	thisMarker.coord = vCoord
	thisMarker.markerType = type 
	thisMarker.r = r
	thisMarker.g = g
	thisMarker.b = b	
	//thisMarker.blip = CREATE_BLIP_FOR_COORD(vCoord,FALSE)
	ADD_HELIHUD_MARKER(HUD,thisMarker)	
	return thisMarker
ENDFUNC



FUNC HELIHUD_MARKER ADD_HELIHUD_MARKER_FOR_VEHICLE(structHelicopterHUDVars &HUD, vehicle_index veh,HELIHUD_MARKER_TYPE markerType, int r, int g, int b)
	HELIHUD_MARKER thisMarker
		
	thisMarker.markerType = markerType
	thisMarker.veh = veh
	thisMarker.r = r
	thisMarker.g = g
	thisMarker.b = b
	//thisMarker.blip = CREATE_BLIP_FOR_VEHICLE(veh,FALSE)
	ADD_HELIHUD_MARKER(HUD,thisMarker)
	return thisMarker	
ENDFUNC

FUNC HELIHUD_MARKER ADD_HELIHUD_MARKER_FOR_PED(structHelicopterHUDVars &HUD, PED_INDEX ped,HELIHUD_MARKER_TYPE markerType, int r, int g, int b)
	HELIHUD_MARKER thisMarker
	
	thisMarker.markerType = markerType
	thisMarker.ped = ped
	thisMarker.r = r
	thisMarker.g = g
	thisMarker.b = b
	//thisMarker.blip = CREATE_BLIP_FOR_PED(ped,FALSE)
	ADD_HELIHUD_MARKER(HUD,thisMarker)
	return thisMarker	
ENDFUNC

PROC remove_helihud_marker(structHelicopterHUDVars &HUD,HELIHUD_MARKER &markerID)
	if markerID.entry >= 0 and markerID.entry < MAX_HELIHUD_COORD_COUNT
		HUD.helihudMarker[markerID.entry].entry = -1
		HUD.helihudMarker[markerID.entry].ped = NULL
		HUD.helihudMarker[markerID.entry].veh = NULL
		HUD.helihudMarker[markerID.entry].coord = <<0,0,0>>
	//	IF DOES_BLIP_EXIST(HUD.helihudMarker[markerID.entry].blip)
	//		REMOVE_BLIP(HUD.helihudMarker[markerID.entry].blip)
	//	ENDIF
		markerID.entry =-1
		HUD.helihudMarkerCount-= 1
	ENDIF
ENDPROC

PROC REMOVE_ALL_HELIHUD_MARKERS(structHelicopterHUDVars &HUD)
	int i = 0
	repeat COUNT_OF(HUD.helihudMarker) i
		HUD.helihudMarker[i].entry = -1
		HUD.helihudMarker[i].ped = NULL
		HUD.helihudMarker[i].veh = NULL
		HUD.helihudMarker[i].coord = <<0,0,0>>				
		//IF DOES_BLIP_EXIST(HUD.helihudMarker[i].blip)
		//	REMOVE_BLIP(HUD.helihudMarker[i].blip)
		//ENDIF
	ENDREPEAT
	HUD.helihudMarkerCount= 0
ENDPROC

//INFO:
//PARAM NOTES:
//PURPOSE: Checks if the HUD marker is within the specified range of the active rendering camera.
func bool IS_HELIHUD_MARKER_IN_RANGE(structHelicopterHUDVars &HUD, HELIHUD_MARKER &markerID, float fRangeToCheck, bool removeMarkerIfInRange)
	CAMERA_INDEX thisCam

	
	if markerID.entry != -1
		thisCam = GET_RENDERING_CAM()
		if DOES_CAM_EXIST(thisCam)
			if GET_DISTANCE_BETWEEN_COORDS(GET_CAM_COORD(thisCam),HUD.helihudMarker[markerID.entry].coord,FALSE) < fRangeToCheck
				if removeMarkerIfInRange = TRUE
					HUD.helihudMarker[markerID.entry].entry = -1
					markerID.entry =-1
					HUD.helihudMarkerCount-= 1
				ENDIF
				return TRUE
			ENDIf
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

PROC ReviseHudMarkers(structHelicopterHUDVars &HUD)
	int i
	repeat COUNT_OF(HUD.helihudMarker) i
		if HUD.helihudMarker[i].entry != -1
			if HUD.helihudMarker[i].veh != NULL
				if not IS_ENTITY_DEAD(HUD.helihudMarker[i].veh)
					HUD.helihudMarker[i].coord = GET_ENTITY_COORDS(HUD.helihudMarker[i].veh)
				ENDIF				
			ENDIF			
			if HUD.helihudMarker[i].ped != NULL			
				if not IS_ENTITY_DEAD(HUD.helihudMarker[i].ped)				
					HUD.helihudMarker[i].coord = GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.helihudMarker[i].ped,0)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC



#if ENABLE_ENHANCED_HUD_FEATURES



proc createHoverInArea(structHelicopterHUDVars &HUD, vector hoverCenter, float hoverRadius, float hoverHitRange = 6.0, bool avoidBuildings = true)

	HUD.bHoverInArea = TRUE
	if avoidBuildings = TRUE hoverCenter.z = -1.0 endif
	HUD.vHoverCenter = hoverCenter
	HUD.hoverHitRange = hoverHitRange
	hud.vHoverPoint = <<0,0,0>>
	hud.fHoverRadius = hoverRadius
ENDPROC

proc fixChopperHeading(structHelicopterHUDVars &HUD, float fHeading, BOOL fixHeading=true)
	if fixHeading = true
		hud.bheadingfixed = TRUE
		hud.fFixedHeading = fHeading
		hud.visibleHeading = hud.chopperHeading
	ELSE			 
		hud.bheadingfixed = FALSE
		hud.chopperHeading = hud.visibleHeading
	ENDIF
ENDPROC

func bool is_heading_fixed(structHelicopterHUDVars &HUD)
	if hud.bheadingfixed = TRUE
		return true
	ENDIF
	return FALSE
ENDFUNC

proc clearHoverInArea(structHelicopterHUDVars &HUD)
	HUD.bHoverInArea = FALSE	
	if is_heading_fixed(hud)
		fixChopperHeading(HUD,hud.chopperHeading,FALSE)
	ENDIF
ENDPROC



func BOOL GET_TO_POINT(structHelicopterHUDVars &HUD,vector vCoord, float passWithinRange=5.0, float circleRadius=-1.0, int arriveInMS=-1, bool slowOnArrival=FALSE, bool overrideExisting = true)
	
	if overrideExisting = TRUE or (overrideExisting = FALSE and HUD.bGettoPoint = FALSE)
		HUD.vGetToPoint = vCoord
		HUD.flightTime = arriveInMS
		HUD.slowOnArrival = slowOnArrival
		HUD.bGettoPoint = TRUE
		HUD.circleRadius = circleRadius
		
		if not ARE_VECTORS_EQUAL(HUD.vGetToPoint,HUD.vLastGetToPoint)
			hud.rangeToTarget = 999999.0
			HUD.vLastGetToPoint = HUD.vGetToPoint 
		ENDIF
		
		if IS_VEHICLE_DRIVEABLE(hud.vehChopper)
			float fRange= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(hud.vehChopper),vCoord,FALSE)

			if fRange < passWithinRange
				return true
			ENDIF

			hud.rangeToTarget = fRange
		ENDIF			
		
	//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
	//	DRAW_DEBUG_SPHERE(hud.vGetToPoint,passWithinRange,255,0,0,65)
		
		
	ENDIF
	return FALSE
ENDfunc

proc CONTROL_GET_TO_POINT(structHelicopterHUDVars &HUD)
	//actual chopper speed
	//actual rotate speed
	//set movement acceleration
	//set rotate acceleration

	
	
	vector vVel,vPos
	vector vTempRot
	
	vVel=vVel
	


	
	if HUD.bGettoPoint = TRUE
		HUD.bGettoPoint = FALSE
		
	
		if IS_VEHICLE_DRIVEABLE(HUD.vehChopper)
	
			SET_VEHICLE_GRAVITY(HUD.vehChopper,FALSE)
			SET_ENTITY_HAS_GRAVITY(HUD.vehChopper,FALSE)
		//	vVel =  GET_ENTITY_VELOCITY(HUD.vehChopper)  //GET_ENTITY_VELOCITY(HUD.vehChopper)
		
	
		
			vector vActual
			vPos = GET_ENTITY_COORDS(HUD.vehChopper)
			if IS_VECTOR_ZERO(hud.lastpos)
				vActual = <<0,0,0>>
			ELSE
				vActual = <<(vpos.x - hud.lastpos.x)/TIMESTEP(),(vpos.y - hud.lastpos.y)/TIMESTEP(),0>>
			ENDIF
			
			
			float gameSpeed = SQRT( (vActual.x * vActual.x) + (vActual.y * vActual.y) )
			
			
			float fHeading = hud.chopperHeading * -1.0
			
			
			vector vTarget
			//if a circle radius has been specified for circling around a point rather than flying straight to it
			if HUD.circleRadius != -1.0
				float fHeadingToPoint = GET_HUD_HEADING_FROM_COORDS(vPos,HUD.vGetToPoint)
				float fHeadingLeft = fHeadingToPoint + 160.0
				float fHeadingRight = fHeadingToPoint - 160.0
				vector vLeft = HUD.vGetToPoint + <<HUD.circleRadius*sin(fHeadingLeft),HUD.circleRadius*cos(fHeadingLeft),0>>
				vector vRight = HUD.vGetToPoint + <<HUD.circleRadius*sin(fHeadingRight),HUD.circleRadius*cos(fHeadingRight),0>>				
				
				//find point which closest matches the chopper current heading
				float fRotToLeft = atan((vLeft.x-vPos.x)/(vLeft.y-vPos.y))
				if vLeft.y-vPos.y < 0 fRotToLeft+= 180.0 ENDIF
				float fRotToRight = atan((vRight.x-vPos.x)/(vRight.y-vPos.y))
				if vRight.y-vPos.y < 0 fRotToRight+= 180.0 ENDIF
																																						
				fRotToLeft = fRotToLeft - fHeading
				fRotToRight = fRotToRight - fHeading														
				
				fRotToLeft = WRAP(fRotToLeft,-180.0,180.0)
				fRotToRight = WRAP(fRotToRight,-180.0,180.0)

				
				if ABSF(fRotToLeft) < ABSF(fRotToRight)
					vTarget = vLeft
					//DRAW_DEBUG_SPHERE(vLeft,1.0,0,255,0,255)
					//DRAW_DEBUG_SPHERE(vRight,1.0,255,0,0,255)
					
				ELSE
					vTarget = vRight
					//DRAW_DEBUG_SPHERE(vLeft,1.0,255,0,0,255)
					//DRAW_DEBUG_SPHERE(vRight,1.0,0,255,0,255)
				ENDIF
			ELSE
				vTarget = HUD.vGetToPoint
			endif
			
			
			//set the rotational speed
		
			float fRotToPoint = GET_HEADING_FROM_COORDS(vPos,vTarget,FALSE)

			float fAngleBla = fRotToPoint - fHeading  //GET_ANGLE_BETWEEN_2D_VECTORS(sin(chopperHeading),cos(chopperHeading),vTarget.x-vPos.x,vTarget.y-vPos.y)			
			
			IF fAngleBla < -180.0 fAngleBla += 360.0 endif
			if fAngleBla > 180.0 fAngleBla -= 360.0 endif 

			//find target height
			
			float actualSpeed = hud.actualspeed //SQRT( (vVel.x * vVel.x) + (vVel.y * vVel.y) )
			
			if TIMERA() > HUD.nextGroundCheckTime
				float zHeight
				HUD.nextGroundCheckTime += 500
				HUD.GroundHeightAccum *= 0.9	
				GET_GROUND_Z_FOR_3D_COORD(vpos,zHeight)
				HUD.GroundHeightAccum += zHeight										
			ENDIF
							
			vector vOffset = <<sin(-fHeading)* actualSpeed * -5.0,cos(-fHeading)* actualSpeed * 5.0,0.0>>
			
			if HUD.circleRadius != -1.0	or vtarget.z = -1.0										
											
				float fHeight = GET_APPROX_HEIGHT_FOR_AREA(vpos.x+vOffset.x-(actualSpeed * 5.0),vpos.y+vOffset.y-(actualSpeed * 5.0),vpos.x+vOffset.x+(actualSpeed * 5.0),vpos.y+vOffset.y+(actualSpeed * 5.0))
												
				float FMAXheight
				if fHeight > HUD.GroundHeightAccum/10.0
					FMAXHeight = fHeight
				ELSE
					FMAXHeight = (HUD.GroundHeightAccum/10.0) + 40.0
				ENDIF						
				
				vTarget.z = FMAXHeight + 100.0
			ENDIF			
			
			
			//setting vetical speed		
			/*float fRealSpeed
			if not IS_VECTOR_ZERO(hud.lastpos)
				fRealSpeed = (vpos.z - hud.lastpos.z) / TIMESTEP()
			ELSE
				fRealSpeed = 0.0
			ENDIF
			*/
			float timeToTarget = GET_DISTANCE_BETWEEN_COORDS(vpos,vTarget)
			
			IF timeToTarget > 6
				timeToTarget = 6
			ENDIF
			
			float targetZSpeed = (vTarget.z - vPos.z) / timeToTarget
			float diffToTargetspeed = targetZSpeed - HUD.actualZAcc//vVel.z
			vVel.z += (diffToTargetspeed * TIMESTEP())
			HUD.actualZAcc += (diffToTargetspeed * TIMESTEP())
			vVel.z = HUD.actualZAcc - 0.57
			/*
			float targetZSpeed = (vTarget.z +1.7 - vpos.z) / 5.0

			float hitTargetMultiplier = ((vTarget.z - vPos.z) / targetZSpeed) / (GET_DISTANCE_BETWEEN_COORDS(vpos,vTarget,FALSE) / gameSpeed)
			
			//if chopper is going to miss target for being too high, apply the multiplier.
			
			
			IF hitTargetMultiplier > 1.0
			AND (vTarget.z - vPos.z) / targetZSpeed > 0
				targetZSpeed *= hitTargetMultiplier
			ENDIF
						
			if targetZSpeed > 20.0 targetZSpeed = 20.0 ENDIF
			if targetZSpeed < -20.0 targetZSpeed = -20.0 ENDIF	
			
			HUD.targetZAcc = ((targetZSpeed - fRealSpeed)*3.0)
			
			IF HUD.actualZAcc < HUD.targetZAcc
				HUD.actualZAcc += 20.0 * TIMESTEP()
				IF HUD.actualZAcc > HUD.targetZAcc
					HUD.actualZAcc = HUD.targetZAcc
				ENDIF
			ELSE
				HUD.actualZAcc -= 20.0 * TIMESTEP()
				IF HUD.actualZAcc < HUD.targetZAcc
					HUD.actualZAcc = HUD.targetZAcc
				ENDIF
			ENDIF
						
			if HUD.actualZAcc > 40.0 HUD.actualZAcc = 40.0 ENDIF
			if HUD.actualZAcc < -40.0 HUD.actualZAcc = -40.0 ENDIF
			
			

			vVel.z += (HUD.actualZAcc * TIMESTEP()) //targetZSpeed
			
			*/
			
			//set entity forward velocity
			//forward velocity based on angle to heading and distance to heading
			//further distance to heading means faster movement while turning
			//closer to target means slower movement while turning
			
			float fDistanceToTarget = GET_DISTANCE_BETWEEN_COORDS(vPos,vTarget,FALSE)

				float fTurnMultiplierForRange = 1.0
				fTurnMultiplierForRange=fTurnMultiplierForRange
				
				gamespeed=gamespeed
			
			float targetSpeed = fDistanceToTarget / 5.0
		
			
			
			if hud.slowOnArrival = FALSE
				if targetSpeed < 30.0 targetSpeed = 30.0 ENDIF
			ENDIF

			
			if ABSF((vTarget.z - vPos.z)) > 2.0
				float maxForwardSpeedToHitHeight = fDistanceToTarget * 10.0 /  ABSF(vTarget.z - vPos.z) //dist / time
				
				if targetSpeed > maxForwardSpeedToHitHeight
					targetSpeed = maxForwardSpeedToHitHeight
				ENDIF
			ENDIF
			
			if targetSpeed > hud.fMaxChopperSpeed targetSpeed = hud.fMaxChopperSpeed ENDIF					
			
			if targetSpeed < hud.fminChopperSpeed targetSpeed = hud.fminChopperSpeed ENDIF

		
			if hud.actualspeed < targetSpeed 
				if ABSF(hud.actualspeed - targetSpeed) < 2.0
					hud.actualspeed += 1.0 * TIMESTEP()
				ELSE
					hud.actualspeed += 5.0 * TIMESTEP()
				ENDIF
			ENDIF
			
			if hud.actualspeed > targetSpeed 
				if ABSF(hud.actualspeed - targetSpeed) < 2.0
					hud.actualspeed -= 1.0 * TIMESTEP()
				ELSE
					hud.actualspeed -= 7.0 * TIMESTEP()
				ENDIF
			ENDIF
			
			//hud.actualspeed = clamp(hud.actualspeed,hud.fminChopperSpeed,hud.fmaxChopperSpeed)
			
		
			vVel.x = sin(fHeading) * hud.actualspeed
			vVel.y = cos(fHeading) * hud.actualspeed
			
			
			
			hud.lastVx = vVel.x
			hud.lastVy = vVel.y
			
		

			//set chopper rotational speed

			//if the chopper has been given a fixed heading
			float desiredRotVel
			if hud.bHeadingFixed = TRUE
				float angleToFixedHeading = hud.fFixedHeading - hud.visibleHeading  //GET_ANGLE_BETWEEN_2D_VECTORS(sin(chopperHeading),cos(chopperHeading),vTarget.x-vPos.x,vTarget.y-vPos.y)			
				angleToFixedHeading = WRAP(angleToFixedHeading,-180.0,180.0)
				desiredRotVel = angleToFixedHeading /8.0
				if desiredRotVel < 0
					desiredRotVel = clamp(desiredRotVel,-20.0,-0.01)
				ELSE
					desiredRotVel = clamp(desiredRotVel,20.0,0.01)
				ENDIF
				hud.visibleHeading += desiredRotVel * TIMESTEP()
			ENDIF

			//desiredRotVel = fAngleBla /2.0	//original
			desiredRotVel = fAngleBla
			
			 // originally not commented out
			if desiredRotVel < -60
				desiredRotVel = -60.0
				//clamp(desiredRotVel,-60.0,-0.01)
			endif
			
			if desiredRotVel > 60.0
				desiredRotVel = 60.0
			//ELSE
			//	clamp(desiredRotVel,60.0,0.01)
			ENDIF
			
			float upperAllowedRotVel = hud.rotVelChange + (10 * timestep())
			float lowerAllowedRotVel = hud.rotVelChange - (10 * timestep())
			
			IF upperAllowedRotVel > 1
				upperAllowedRotVel = 1
			endif
			
			If lowerAllowedRotVel < -1
				lowerAllowedRotVel = -1
			endif
				
				
			IF desiredRotVel > hud.lastRotVel + upperAllowedRotVel
				desiredRotVel = hud.lastRotVel + upperAllowedRotVel
				hud.rotVelChange = upperAllowedRotVel
			ELIF desiredRotVel < hud.lastRotVel + lowerAllowedRotVel
				desiredRotVel = hud.lastRotVel + lowerAllowedRotVel
				hud.rotVelChange = lowerAllowedRotVel
			else	
				hud.rotVelChange = desiredRotVel - hud.lastRotVel 
			endif
			
		
								
			hud.lastRotVel = desiredRotVel //clamp(desiredRotVel,hud.lastRotVel-(12.0*TIMESTEP()),hud.lastRotVel+(12.0*TIMESTEP()))
			
			
			hud.chopperHeading += -hud.lastRotVel * TIMESTEP()
			
			vTempRot = GET_ENTITY_ROTATION(HUD.vehChopper)
			vTempRot.y += ((desiredRotVel/2.0) - vTempRot.y) * TIMESTEP()
			
	
			if hud.bHeadingFixed = TRUE
				SET_ENTITY_ROTATION(HUD.vehChopper,<<0,vTempRot.y,hud.visibleHeading >>)
			ELSE
				SET_ENTITY_ROTATION(HUD.vehChopper,<<0,vTempRot.y,hud.chopperHeading>>)
			ENDIF
			
			hud.lastPos = vPos								
			zSpeed=zSpeed
			
			SET_ENTITY_VELOCITY(HUD.vehChopper,vVel+<<0,0,0.1>>) //<<0,0,zSpeed+(zSpeed - fRealSpeed)>>)//vVel)
			
			hud.speedLastFrame = gameSpeed
									
		ENDIF
	ENDIF

ENDPROC

PROC NAVIGATE_TO_POINT(structHelicopterHUDVars &HUD,entity_index veh,VECTOR vCoord)
	veh=veh
	vCoord.z = 150.0
	GET_TO_POINT(HUD,vCoord,5,100.0,-1,true,FALSE)
ENDPROC

#ENDIF


proc moveChopper(structHelicopterHUDVars &HUD)

	if HUD.bMoveChopper = TRUE
		#if ENABLE_ENHANCED_HUD_FEATURES
		
		if hud.bHoverInArea = TRUE
			if IS_VECTOR_ZERO(hud.vHoverPoint)
				hud.vHoverPoint = HUD.vHoverCenter + <<sin(GET_RANDOM_FLOAT_IN_RANGE(0,360)) * hud.fHoverRadius,cos(GET_RANDOM_FLOAT_IN_RANGE(0,360)) * hud.fHoverRadius,0>>
	
			ENDIF
			if GET_TO_POINT(hud,hud.vHoverPoint,hud.hoverHitRange,-1,-1,true)
		
				if not is_heading_fixed(hud)
					fixChopperHeading(HUD,hud.chopperHeading)
				ENDIF
				hud.vHoverPoint = <<0,0,0>>
			ENDIF			
		else
			if not IS_ENTITY_DEAD(HUD.camEntity)
				if HUD.moveByCamera = TRUE
					NAVIGATE_TO_POINT(HUD,HUD.camEntity,hud.vaimAt)							
				ENDIF
			ENDIF
		ENDIF

		CONTROL_GET_TO_POINT(HUD) //get to a specific point

		#ENDIF
	ELSE
		if DOES_ENTITY_EXIST(HUD.camEntity)
			if not IS_ENTITY_DEAD(HUD.camEntity)
				//FREEZE_ENTITY_POSITION(HUD.camEntity,TRUE)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC



proc POINT_CHOPPER_CAM_AT_COORD(structHelicopterHUDVars &HUD, vector vPointAtCoord)
	IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
	AND NOT DOES_INTERIOR_USE_TURRETS()
	OR g_iShouldLaunchTruckTurret != -1
		HUD.vTargetedGround = vPointAtCoord
		POINT_CAM_AT_COORD(HUD.camChopper,HUD.vTargetedGround)
		hud.vaimat = vPointAtCoord
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "POINT_CHOPPER_CAM_AT_COORD")
	ENDIF
ENDPROC
#IF IS_DEBUG_BUILD
PROC DEBUG_TURRET_DISPLAY(structHelicopterHUDVars &HUD)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawDebugBombushka")
		INT iStrOffset = 0, iColumn = 0, iRow = 0
		INT iRed, iGreen, iBlue, iAlpha
		FLOAT fYvar = 0.017
		TEXT_LABEL_63 str
		
				FLOAT fCamRotSpeed
				IF g_BombushkaTurretCamRotSpeedMultiplier = -1
					fCamRotSpeed = (((HUD.fFOV-(HUD.minFOV-1)) / (HUD.maxFOV-(HUD.minFOV-1)))*6)
				ELSE
					fCamRotSpeed = (((HUD.fFOV-(HUD.minFOV-1)) / (HUD.maxFOV-(HUD.minFOV-1)))*g_BombushkaTurretCamRotSpeedMultiplier)
				ENDIF
			
				str += "fCamRotSpeed: "
				
				str += GET_STRING_FROM_FLOAT(fCamRotSpeed)
			
				
				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
				iGreen = 0
				iRed = 0
				str = ""
				iRow++
				
				INT iCamZoomSpeed

				IF g_BombushkaTurretCamZoomSpeedMultiplier = -1
					iCamZoomSpeed = 4
				ELSE
					iCamZoomSpeed = g_BombushkaTurretCamZoomSpeedMultiplier
				ENDIF

			
				str += "iCamZoomSpeed: "
				
				str += GET_STRING_FROM_INT(iCamZoomSpeed)
			
				
				GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)+(<<0.0,fYvar,0>>*TO_FLOAT(iRow))), iRed, iGreen, iBlue, iAlpha)
				iGreen = 0
				iRed = 0
				str = ""
				iRow++

				
				
	ENDIF			
ENDPROC
#ENDIF


FUNC TURRET_FIRE_SOUND_SET GET_TURRET_AUDIO_SET()

	 
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) 
	OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
		RETURN (TFSS_MOC)
	ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
		RETURN (TFSS_AOC)
	ELIF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		RETURN (TFSS_IAA)
	ENDIF
	
	RETURN (TFSS_INVALID)
ENDFUNC




proc ControlChopperCamera(	structHelicopterHUDVars &HUD, 
							FLOAT fMinPitchOverride = MIN_CAMERA_PITCH, FLOAT fMaxPitchOverride = MAX_CAMERA_PITCH,
							FLOAT fMinHeadingOverride = MIN_CAMERA_HEADING, FLOAT fMaxHeadingOverride = MAX_CAMERA_HEADING,
							FLOAT fDeadzoneMagnitude = 0.15 )
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera")						
	int iZoom
	int iStickYaw
	int iStickPitch
	FLOAT fStickPitch
	FLOAT fStickYaw
	FLOAT fMaxCamPitch = fMaxPitchOverride
	IF HUD.bLimitCamWhileGrounded 
		fMaxCamPitch = MAX_CAMERA_PITCH_GROUNDED
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_TURRET_DISPLAY(HUD)
	#ENDIF
	
	VECTOR vCamPos
	FLOAT fCamZ
	
	if HUD.bActivateChopperCam = TRUE		
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera: bActivateChopperCam = TRUE	")	
		
		if DOES_CAM_EXIST(HUD.camChopper)
			if NOT IS_CAM_ACTIVE(HUD.camChopper)
				SET_CAM_ACTIVE(HUD.camChopper,TRUE)
				
				IF g_iShouldLaunchTruckTurret != -1
				OR g_iInteriorTurretSeat != -1 
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera - NOT rendering script cameras, waiting for all elements to load.")				
					HUD.bDelayRenderScriptCams = TRUE
				ELSE
				
				RENDER_SCRIPT_CAMS(true,FALSE)
				
				ENDIF
				
			ENDIF
		ENDIF		
		
		// Blank out cam if cam pos is below ground
		IF HUD.bIsCamAttached
			IF DOES_CAM_EXIST(HUD.camChopper)
				vCamPos = GET_CAM_COORD(HUD.camChopper)
				g_bUsingTurretHeliCam = TRUE
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera vCamPos = ", vCamPos)
				IF GET_GROUND_Z_FOR_3D_COORD(<<vCamPos.x, vCamPos.y, vCamPos.z+1>>, fCamZ)
				OR IS_ENTITY_IN_WATER(HUD.vehChopper)
					IF fCamZ > vCamPos.z
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera Cam is BELOW ground")
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera vCamPos = ", vCamPos, ", fCamZ = ", fCamZ)
						
						IF g_iShouldLaunchTruckTurret != -1
						OR g_iInteriorTurretSeat != -1
							IF HUD.iCollideFrameCount > MAX_GROUND_CLIP_FRAMES
								
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera iCollideFrameCount = ",HUD.iCollideFrameCount,", max allowance (",MAX_GROUND_CLIP_FRAMES,") exceeded, removing player from cam.")
								
								IF GET_ENTITY_SUBMERGED_LEVEL(HUD.vehChopper) >= 0.01
									IF g_iShouldLaunchTruckTurret != -1
									OR g_iInteriorTurretSeat != -1
										PRINT_HELP("TUR_WATER")
										HUD.bDeactivateChopperCam = TRUE
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera HUD.bDeactivateChopperCam = TRUE, IS_ENTITY_IN_WATER(HUD.vehChopper) = TRUE")
									ENDIF
								ENDIF
								
								//-- Check if we can find a ground z below the camera. If so, probably going under a bridge
								IF GET_GROUND_Z_FOR_3D_COORD(<<vCamPos.x, vCamPos.y, vCamPos.z - 1>>, fCamZ)
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera got ground z below vCamPos, won't bail, vCamPos = ", vCamPos, ", fCamZ = ", fCamZ)
								ELSE
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera Failed to get ground z below vCamPos, bailing. vCamPos = ", vCamPos)
									IF g_iInteriorTurretSeat != -1
										PRINT_HELP("TUR_GR")
									ENDIF
									HUD.bDeactivateChopperCam = TRUE
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera iCollideFrameCount = ",HUD.iCollideFrameCount,", incrementing total below ground frame allowance.")
								HUD.iCollideFrameCount = HUD.iCollideFrameCount + 1
							ENDIF
							
						ELSE
						
						HUD.bDeactivateChopperCam = TRUE
						
						ENDIF
					ELSE
						IF HUD.iCollideFrameCount != 0
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera resetting iCollideFrameCount")
							HUD.iCollideFrameCount = 0
						ENDIF
					ENDIF
				ENDIF
				
				
				
			ENDIF
		ENDIF
		
		if HUD.bIsCamAttached = FALSE
			if DOES_CAM_EXIST(HUD.camChopper)
				if DOES_ENTITY_EXIST(HUD.camEntity)
					if not IS_ENTITY_DEAD(HUD.camEntity)
						IF HUD.isMultiplayerMode
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera: HUD.camEntity = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(HUD.camEntity)))
							VECTOR vTempOffset
							SWITCH GET_ENTITY_MODEL(HUD.camEntity)
								CASE VALKYRIE ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.000,5.320,-0.300>>,TRUE) BREAK
								CASE POLMAV ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,2.75,-1.25>>,TRUE) BREAK 
								CASE MAVERICK ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,2.75,-1.25>>,TRUE) BREAK 
								CASE SAVAGE ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,5.75,-0.75>>,TRUE) BREAK 
								CASE HUNTER ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<-0.0066, 8.9038, -0.5>>,TRUE) BREAK 
								CASE TRAILERLARGE 
									
									SWITCH g_iShouldLaunchTruckTurret
										CASE 1
											vTempOffset = <<0, 8.4, 1.3>>
										BREAK 
										CASE 2
											vTempOffset = <<-2.400, -7.6, 0.920>>
										BREAK
										CASE 3
											vTempOffset = <<2.400, -7.6, 0.920>>
										BREAK
									ENDSWITCH
									
									ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,vTempOffset,TRUE) 
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CalculateTurretCamera1: Offset = ", vTempOffset)
								BREAK 
								CASE ADDER
									VECTOR vAdderPos
									vAdderPos = <<2065.8484, 2967.3196, 45.2947>>
									VECTOR vTurretPos1 
									vTurretPos1 = <<2049.6121, 2947.6570, 49.5560>>
									VECTOR vTurretPos2 
									vTurretPos2 = <<2045.0913, 2943.2578, 49.4991>>
									VECTOR vTurretPos3 
									vTurretPos3 = <<2040.3645, 2952.7544, 49.5688>>
									VECTOR vTurretPos4 
									vTurretPos4 = <<2049.3848, 2953.9709, 49.9635>>
									SWITCH g_iInteriorTurretSeat
										CASE 1
											vTempOffset = vTurretPos1 - vAdderPos
										BREAK 
										CASE 2
											vTempOffset = vTurretPos2 - vAdderPos
										BREAK
										CASE 3
											vTempOffset = vTurretPos3 - vAdderPos				
										BREAK
										CASE 4
											vTempOffset = vTurretPos4 - vAdderPos
										BREAK
									ENDSWITCH
									
									ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,vTempOffset,TRUE) 
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CalculateTurretCamera1: for ADDER: Offset = ", vTempOffset)
								BREAK
//								CASE TITAN 
//									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GETTING TITAN cam Coords, g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)
//									SWITCH g_iShouldLaunchTruckTurret
//										CASE 1
//											vTempOffset = <<0.0454, 10.5501, 1.9211>>
//										BREAK 
//										CASE 2
//											vTempOffset = <<0.0454, 10.5501, 1.9211>>
//										BREAK
//										CASE 3
//											vTempOffset = <<0.0454, 10.5501, 1.9211>>
//										BREAK
//									ENDSWITCH
//									
//									ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,vTempOffset,TRUE) 
//									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CalculateTurretCamera: Offset = ", vTempOffset)
//								BREAK
								
								DEFAULT ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,1.958,-0.618>>,TRUE) BREAK
							ENDSWITCH
						ELSE
							SWITCH GET_ENTITY_MODEL(HUD.camEntity)
							
								// Eye in the sky.
								CASE BUZZARD ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,1.958,-0.468>>,TRUE) BREAK
								CASE POLMAV ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,2.75,-1.25>>,TRUE) BREAK 
								
								// Free mode scripted gun.
	//							CASE HUNTER ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,3.25,-3.0>>,TRUE) BREAK - leave commented, do not remove.
	//							CASE ANNIHILATOR ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,2.25,-1.0>>,TRUE) BREAK
								CASE BUZZARD2  ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,1.958,-0.618>>,TRUE) BREAK
								CASE VALKYRIE ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.000,5.320,-0.300>>,TRUE) BREAK
								DEFAULT ATTACH_CAM_TO_ENTITY(HUD.camChopper,HUD.camEntity,<<0.0,1.958,-0.618>>,TRUE) BREAK
								
							ENDSWITCH
						ENDIF
						HUD.bIsCamAttached = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "bIsCamAttached = ", HUD.bIsCamAttached, ", mpCamFudge: ", HUD.mpCamFudge )
		IF HUD.bIsCamAttached = TRUE and HUD.mpCamFudge=FALSE #IF ENABLE_ENHANCED_HUD_FEATURES and HUD.bDisableCamMovement = FALSE #ENDIF
			FLOAT inputScale = 128
			if DOES_CAM_EXIST(HUD.camChopper)
				if IS_CAM_ACTIVE(HUD.camChopper)
					IF NOT IS_SELECTOR_ONSCREEN()
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CHARACTER_WHEEL)
						AND NOT IS_PAUSE_MENU_ACTIVE()
						
							SET_INPUT_EXCLUSIVE( PLAYER_CONTROL, INPUT_SNIPER_ZOOM) // Needed for PC, as sniper zoom is the same as weapon cycle.
							
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								inputScale = 15
								// Mouse uses float for better precision.
								fStickPitch = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD) * -inputScale
								fStickYaw = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR) * inputScale
							ELSE
								iStickPitch = floor(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD) * -inputScale)
								iStickYaw = floor(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR) * inputScale)
							ENDIF
//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "iZoom = ", iZoom)
							iZoom = floor(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SNIPER_ZOOM) * 128)
						ELSE
							CPRINTLN(debug_trevor3,"player selecting switch control")
						ENDIF

						// No need to dead-zone on mouse
						IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)		
							INT iDeadzoneMagnitude = ROUND(fDeadzoneMagnitude * inputScale)
							//Deadzone zoom
							IF ABSI(iZoom) < iDeadzoneMagnitude
								iZoom = 0
							ENDIF
							//Deadzone camera movement
							VECTOR vecInput = <<iStickPitch, iStickYaw, 0>> 
							IF VMAG(vecInput) < iDeadzoneMagnitude
								iStickPitch = 0
								iStickYaw = 0
							ENDIF
						ENDIF
					
						CDEBUG2LN(DEBUG_NET_GUN_TURRET, "HUD.vCamRot = ", HUD.vCamRot)
						
						float fYawMultiplier = ((HUD.vCamRot.x / fMaxPitchOverride) * 0.5)

						IF fYawMultiplier > 0
							fYawMultiplier += 1.0
						ELSE
							fYawMultiplier = 1.0
						ENDIF
						
						// Using TIMESTEP() with mouse breaks mouse movement.
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							// Mouse uses float for better precision.
							HUD.fStickPitch = (fStickPitch * HUD.fFOV) * HUD.chopperPanFactor
							HUD.fStickYaw = ((fStickYaw * HUD.fFOV) * HUD.chopperPanFactor) * fYawMultiplier
						ELSE
//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera, HUD.chopperPanFactor = ", HUD.chopperPanFactor, ", HUD.fStickPitch = ", HUD.fStickPitch)
							HUD.fStickPitch = ((TO_FLOAT(iStickPitch) * HUD.fFOV) * HUD.chopperPanFactor) * TIMESTEP()
//							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera, fYawMultiplier = ", fYawMultiplier, ", HUD.fStickYaw = ", HUD.fStickYaw)
							HUD.fStickYaw = (((TO_FLOAT(iStickYaw) * HUD.fFOV) * HUD.chopperPanFactor) * fYawMultiplier) * TIMESTEP()
						ENDIF
						
						
						HUD.fStickYaw = RESTRICT_HEADING(HUD.fStickYaw, fMinHeadingOverride, fMaxHeadingOverride)
						
						if are_vectors_equal(HUD.vTargetedGround,<<0,0,0>>)
							
							vector vTemp = GET_CAM_COORD(HUD.camChopper)
							HUD.vTargetedGround.x = SIN(HUD.vCamRot.z) * 150.0
							HUD.vTargetedGround.y = COS(HUD.vCamRot.z) * 150.0
							HUD.vTargetedGround.z = vTemp.z
							
						ENDIF
			
						#IF USE_TU_CHANGES
						IF HUD.isMultiplayerMode
							IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
							AND NOT DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
							OR g_iShouldLaunchTruckTurret != -1
								IF iStickPitch != 0
								AND iStickYaw != 0
									HUD.vCamRot = GET_CAM_ROT(HUD.camChopper)
								ENDIF
							ENDIF
						ELSE
							HUD.vCamRot = GET_CAM_ROT(HUD.camChopper)
						ENDIF
						#ENDIF
						
						#IF NOT USE_TU_CHANGES
							HUD.vCamRot = GET_CAM_ROT(HUD.camChopper)
						#ENDIF
						
						If not are_vectors_equal(GET_CAM_COORD(HUD.camChopper),<<0,0,0>>)
							bool bMaxYawReached
							
							IF HUD.fMaxCameraYaw != 0.0
							AND NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
							AND NOT DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
							OR g_iShouldLaunchTruckTurret != -1
								IF IS_VEHICLE_DRIVEABLE(hud.vehchopper)
									float fVehHeading = GET_ENTITY_HEADING(hud.vehchopper)
									
									IF g_iShouldLaunchTruckTurret = 2
									OR g_iShouldLaunchTruckTurret = 3
										fVehHeading = fVehHeading -174.4552
									ENDIF

									float fCamVehDiff = fVehHeading - HUD.vCamRot .z
									
									int safeAbort
									WHILE absf(fCamVehDiff) >= 180
									AND safeAbort < 30
										#IF USE_TU_CHANGES
											IF fCamVehDiff < 0 
												fCamVehDiff += 360
											else
												fCamVehDiff -= 360 
											endif
										#ENDIF
										#IF NOT USE_TU_CHANGES
											IF fCamVehDiff < 0 
												fCamVehDiff += 360
											endif
											if fCamVehDiff > 0
												fCamVehDiff -= 360
											endif
										#ENDIF
										safeAbort++
										IF safeAbort = 30
											SCRIPT_ASSERT("Stuck in while loop in helicopterHud.sch")
										ENDIF
									endwhile
									
									IF g_iShouldLaunchTruckTurret != -1
									
										IF fCamVehDiff >= fMaxHeadingOverride									
											bMaxYawReached = true
											
											IF fCamVehDiff > 0
												HUD.vCamRot.z = fVehHeading - fMaxHeadingOverride
											ELSE
												HUD.vCamRot.z = fVehHeading + fMaxHeadingOverride
											ENDIF
										ENDIF 
										
										IF fCamVehDiff < fMinHeadingOverride									
											bMaxYawReached = true
											
											IF fCamVehDiff > 0
												HUD.vCamRot.z = fVehHeading - ABSF(fMinHeadingOverride)
											ELSE
												HUD.vCamRot.z = fVehHeading + ABSF(fMinHeadingOverride)
											ENDIF
										ENDIF
										
									ELSE
									
									IF absf(fCamVehDiff) > HUD.fMaxCameraYaw									
										bMaxYawReached = true
										If fCamVehDiff > 0
											HUD.vCamRot .z = fVehHeading - HUD.fMaxCameraYaw
										ELSE
											HUD.vCamRot .z = fVehHeading + HUD.fMaxCameraYaw
										ENDIF
									ENDIF
									
									ENDIF
									
								ENDIF
							ENDIF
							
							// When we use the attach method we should track the difference each frame so we can try and correct the heading/pitch of the turret.
							FLOAT fVehicleDiffX = 0.0
							FLOAT fVehicleDiffY = 0.0
							IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
							OR DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
								IF ARE_VECTORS_EQUAL( HUD.vAttachedEntityRotation, <<0,0,0>>)
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ARE_VECTORS_EQUAL( HUD.vAttachedEntityRotation, <<0,0,0>>) = TRUE")
									VECTOR vTempRotYall = GET_ENTITY_ROTATION(HUD.vehChopper)
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ENTITY_ROTATION(HUD.vehChopper) = ", vTempRotYall)
									
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "GET_ENTITY_MODEL(HUD.camEntity) = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(HUD.camEntity)))
									HUD.vAttachedEntityRotation = GET_ENTITY_ROTATION(HUD.vehChopper)
								ELSE
									VECTOR vCurrentRot = GET_ENTITY_ROTATION(HUD.vehChopper)
									// The Bombushka attachment rotations are different for each turret as we are attaching
									//  the camera to the ped who sits at different angles.
									//  Since we decided to use vAttachedCamRotationOffset to determine the heading difference
									//  we will need to offset.
									FLOAT fPitchScale
									
									IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
										SWITCH g_iVehicleTurretSeat
											CASE 0  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK // top
											CASE 1  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK // nose
											CASE 2  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / -90.0) BREAK // rear
											DEFAULT fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK
										ENDSWITCH
									ENDIF
									 
									IF DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
										SWITCH GET_ENTITY_MODEL(HUD.vehChopper)
											CASE AVENGER
												SWITCH g_iInteriorTurretSeat
													CASE 1  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK // nose
													CASE 2  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / -90.0) BREAK // Top
													CASE 3  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / -90.0) BREAK // rear
													DEFAULT fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK
												ENDSWITCH
											BREAK
											DEFAULT
												SWITCH g_iInteriorTurretSeat
													CASE 1  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK // nose
													CASE 2  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK // Top
													CASE 3  fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / -90.0) BREAK // rear
													DEFAULT fPitchScale = ((ABSF(HUD.vAttachedCamRotationOffset.z)-90.0) / 90.0) BREAK
												ENDSWITCH
											BREAK
										ENDSWITCH
									ENDIF
									//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[OCH] fPitchScale: ", fPitchScale, ", g_iInteriorTurretSeat: ", g_iInteriorTurretSeat, ", HUD.vAttachedCamRotationOffset.z = ", HUD.vAttachedCamRotationOffset.z)
									
									fVehicleDiffX -= ((vCurrentRot.x - HUD.vAttachedEntityRotation.x)*fPitchScale)
									fVehicleDiffY += (vCurrentRot.z - HUD.vAttachedEntityRotation.z)
//									CDEBUG2LN(DEBUG_NET_GUN_TURRET, "fVehicleDiffX = ", fVehicleDiffX)
//									CDEBUG2LN(DEBUG_NET_GUN_TURRET, "fVehicleDiffY = ", fVehicleDiffY)
									// Keep the heading within the -180 to 180 range.
									IF fVehicleDiffY > 180
										fVehicleDiffY -= 360
									ENDIF
									IF fVehicleDiffY < -180
										fVehicleDiffY += 360
									ENDIF
									
									// Limit the pitch update to avoid gimbal lock.
									IF vCurrentRot.x < -80
									OR vCurrentRot.x > 80
										fVehicleDiffX = 0.0
										fVehicleDiffY = 0.0
									ENDIF
									IF IS_ENTITY_UPSIDEDOWN(HUD.vehChopper)
										fVehicleDiffX = 0.0
										fVehicleDiffY = 0.0
									ENDIF
									
									HUD.vAttachedEntityRotation = vCurrentRot
								ENDIF
							ENDIF
							
							// We don't want to include the auto adjust heading/pitch audio so need to handle movement audio separately.
							IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
								IF (HUD.fStickYaw != 0 AND NOT ((HUD.vAttachedCamRotationOffset.z = (-HUD.fMaxCameraYaw / 2.0) OR HUD.vAttachedCamRotationOffset.z = (HUD.fMaxCameraYaw / 2.0))))
									bCamMovementStopped = FALSE
									iCamFrameCounter = 0
									
								ELIF (HUD.fStickPitch != 0 AND NOT ((HUD.vAttachedCamRotationOffset.x = fMaxCamPitch OR HUD.vAttachedCamRotationOffset.x = fMinPitchOverride)))
									bCamMovementStopped = FALSE
									iCamFrameCounter = 0
								ELIF iCamFrameCounter > 5
									bCamMovementStopped = TRUE
								ENDIF
								iCamFrameCounter++
							ENDIF
							
							if HUD.fStickPitch != 0 or HUD.fStickYaw != 0
							OR fVehicleDiffX != 0.0 OR fVehicleDiffY != 0.0
//								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera ++++++")
								
								MODEL_NAMES eHeliModel
								IF IS_VEHICLE_DRIVEABLE(HUD.vehChopper)
									eHeliModel = GET_ENTITY_MODEL(HUD.vehChopper)
								ENDIF
								IF g_iVehicleTurretSeat != -1
								OR g_iInteriorTurretSeat != -1
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera HUD.fFOV = ", HUD.fFOV)
									
									FLOAT fCamRotSpeed 
									
									
									IF NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
									
										fCamRotSpeed = (((HUD.fFOV-(HUD.minFOV-1)) / (HUD.maxFOV-(HUD.minFOV-1)))*6)
									ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
									OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
//										g_BombushkaTurretCamRotSpeedMultiplier = 10
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "g_BombushkaTurretCamRotSpeedMultiplier = 15")
										fCamRotSpeed = (((HUD.fFOV-(HUD.minFOV-1)) / (HUD.maxFOV-(HUD.minFOV-1)))*8)
									
//									ELSE
//										fCamRotSpeed = (((HUD.fFOV-(HUD.minFOV-1)) / (HUD.maxFOV-(HUD.minFOV-1)))*g_BombushkaTurretCamRotSpeedMultiplier)
									ENDIF
									
									
									
									// Lower limit
									IF fCamRotSpeed < 3.0
										fCamRotSpeed = 3.0
									ENDIF
									
									
									HUD.fStickPitch = HUD.fStickPitch * fCamRotSpeed
									HUD.fStickYaw = HUD.fStickYaw * fCamRotSpeed
									
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera fCamRotSpeed: ", fCamRotSpeed, " HUD.fStickPitch: ", HUD.fStickPitch, " HUD.fStickYaw: ", HUD.fStickYaw)
									
								ELIF eHeliModel = HUNTER
									HUD.fStickPitch = HUD.fStickPitch * 4
									HUD.fStickYaw = HUD.fStickYaw * 4
								ENDIF
								
								// Camera attaches to Bombuska so track yaw and pitch offsets separately.
								IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
								OR DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
									
									HUD.vAttachedCamRotationOffset.x += HUD.fStickPitch
									HUD.vAttachedCamRotationOffset.z -= HUD.fStickYaw
									
									HUD.vAttachedCamRotationOffset.x -= fVehicleDiffX
									HUD.vAttachedCamRotationOffset.z -= fVehicleDiffY
									
									IF HUD.vAttachedCamRotationOffset.x < fMaxCamPitch
										HUD.vAttachedCamRotationOffset.x = fMaxCamPitch
									ENDIF
									IF HUD.vAttachedCamRotationOffset.x > fMinPitchOverride
										HUD.vAttachedCamRotationOffset.x = fMinPitchOverride
									ENDIF
									
									IF HUD.fMaxCameraYaw >= 360.0
										WHILE HUD.vAttachedCamRotationOffset.z > 180.0
											HUD.vAttachedCamRotationOffset.z -= 360.0
										ENDWHILE
										WHILE HUD.vAttachedCamRotationOffset.z < -180.0
											HUD.vAttachedCamRotationOffset.z += 360.0
										ENDWHILE
									ELSE
										IF HUD.vAttachedCamRotationOffset.z < (-HUD.fMaxCameraYaw / 2.0)
											HUD.vAttachedCamRotationOffset.z = (-HUD.fMaxCameraYaw / 2.0)
											bMaxYawReached = true
										ENDIF
										IF HUD.vAttachedCamRotationOffset.z > (HUD.fMaxCameraYaw / 2.0)
											HUD.vAttachedCamRotationOffset.z = (HUD.fMaxCameraYaw / 2.0)
											bMaxYawReached = true
										ENDIF
									ENDIF
								ELSE
								
									iCamFrameCounter++
									
									HUD.vCamRot += <<HUD.fStickPitch,0,-HUD.fStickYaw>>
									
									if HUD.vCamRot.x < fMaxCamPitch
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera: HUD.vCamRot.x < fMaxCamPitch: HUD.vCamRot.x = ", HUD.vCamRot.x)
										HUD.vCamRot.x = fMaxCamPitch
									ENDIF
									if HUD.vCamRot.x > fMinPitchOverride
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera: HUD.vCamRot.x > fMinPitchOverride: HUD.vCamRot.x = ", HUD.vCamRot.x)
										HUD.vCamRot.x = fMinPitchOverride
									ENDIF 
									
									IF iCamFrameCounter > 5
										IF !ARE_VECTORS_ALMOST_EQUAL(vStoredRotation, HUD.vCamRot, 0.05)
											vStoredRotation = HUD.vCamRot
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera !ARE_VECTORS_ALMOST_EQUAL vStoredRotation = ", vStoredRotation)
											bCamMovementStopped = FALSE
										ELSE
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera bCamMovementStopped")
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera ARE_VECTORS_ALMOST_EQUAL vStoredRotation = ", vStoredRotation)
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera ARE_VECTORS_ALMOST_EQUAL HUD.vCamRot = ", HUD.vCamRot)
											bCamMovementStopped = TRUE
										ENDIF
										iCamFrameCounter = 0
									ENDIF
								ENDIF
								
																
								IF HUD.soundID_turn != -1							
									//IF NOT HAS_SOUND_FINISHED(HUD.soundID_turn) 
										float fRotateSound
										IF bMaxYawReached
											fRotateSound = 0.0
										else
											fRotateSound += ABSF(HUD.fStickYaw)
											IF (ABSF(HUD.fStickYaw) > 0)
												bCamMovementStopped = FALSE
											ENDIF
										endif
										
										IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
										OR DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
											IF HUD.vAttachedCamRotationOffset.x < fMinPitchOverride AND HUD.vAttachedCamRotationOffset.x > fMaxPitchOverride
												fRotateSound += ABSF(HUD.fStickPitch)
												IF (ABSF(HUD.fStickPitch) > 0)
													bCamMovementStopped = FALSE
												ENDIF
											endif
										ELSE
											IF HUD.vCamRot.x < fMinPitchOverride AND HUD.vCamRot.x > fMaxPitchOverride
												fRotateSound += ABSF(HUD.fStickPitch)
												IF (ABSF(HUD.fStickPitch) > 0)
													bCamMovementStopped = FALSE
												ENDIF
											endif
										ENDIF
										
										IF HAS_SOUND_FINISHED(HUD.soundID_turn) 
										AND !bCamMovementStopped
										
											IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
												PLAY_SOUND_FRONTEND(HUD.soundID_turn,"Pan_Loop",  GET_TURRET_FIRE_SOUNDS_NAME(TFSS_IAA))	
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera PLAY_SOUND_FRONTEND Pan_Loop with set ", GET_TURRET_FIRE_SOUNDS_NAME(TFSS_IAA), " HUD.soundID_turn = ", HUD.soundID_turn)

											ELSE
												TURRET_FIRE_SOUND_SET soundset = GET_TURRET_AUDIO_SET()									
												IF soundset != TFSS_INVALID
													PLAY_SOUND_FRONTEND(HUD.soundID_turn,"Pan", GET_TURRET_FIRE_SOUNDS_NAME(soundset))
													CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera PLAY_SOUND_FRONTEND Pan set ", GET_TURRET_FIRE_SOUNDS_NAME(soundset))
												ELSE
													PLAY_SOUND_FRONTEND(HUD.soundID_turn,"COP_HELI_CAM_TURN")
													CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera PLAY_SOUND_FRONTEND COP_HELI_CAM_TURN ")
												ENDIF
											ENDIF
										
//											PLAY_SOUND_FRONTEND(HUD.soundID_turn,"COP_HELI_CAM_TURN")	
										ENDIF
							
										IF NOT HAS_SOUND_FINISHED(HUD.soundID_turn) 
											IF !bCamMovementStopped
											AND fRotateSound > 0
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera SET_VARIABLE_ON_SOUND Ctrl = ", fRotateSound)
												SET_VARIABLE_ON_SOUND(HUD.soundID_turn,"Ctrl",fRotateSound) 
											ELSE
												CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera STOP_SOUND")
												STOP_SOUND(HUD.soundID_turn)
												bCamMovementStopped = TRUE
												iCamFrameCounter = 6
											ENDIF
										ENDIF
								ENDIF
//								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ControlChopperCamera =======")
							ELSE
								IF HUD.soundID_turn != -1
									IF NOT HAS_SOUND_FINISHED(HUD.soundID_turn)							
										STOP_SOUND(HUD.soundID_turn)
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera STOP_SOUND 2")
										bCamMovementStopped = TRUE
										iCamFrameCounter = 6
									ENDIF
								ENDIF
							ENDIF
							
			
							IF NOT IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
							AND NOT DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
							OR g_iShouldLaunchTruckTurret != -1
							
								vector vGroundLoc = hud.vaimat
								
								#IF USE_TU_CHANGES
									float fSphereRadius
									IF HUD.isMultiplayerMode
										fSphereRadius = 150
									ELSE							
										fSphereRadius =  GET_DISTANCE_BETWEEN_COORDS(vGroundLoc,GET_CAM_COORD(HUD.camChopper))//150.0 //distance to point on ground
									ENDIF
								#ENDIF
								
								#IF NOT USE_TU_CHANGES
									float fSphereRadius =  GET_DISTANCE_BETWEEN_COORDS(vGroundLoc,GET_CAM_COORD(HUD.camChopper))//150.0 //distance to point on ground
								#ENDIF
								
								HUD.vTargetedGround.x = (COS(HUD.vCamRot .x) * fSphereRadius) * sin(-HUD.vCamRot .z)
								HUD.vTargetedGround.y = (COS(HUD.vCamRot .x) * fSphereRadius) * cos(-HUD.vCamRot .z)
								HUD.vTargetedGround.z = (sin(HUD.vCamRot .x) * fSphereRadius)
								HUD.vTargetedGround += GET_CAM_COORD(HUD.camChopper)				
//								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "++POINT_CAM_AT_COORD")
								POINT_CAM_AT_COORD(HUD.camChopper,HUD.vTargetedGround)
							ELSE
							
								VECTOR vTempPosOffset, vTempRotOffset
								IF GET_ENTITY_MODEL(HUD.camEntity) = INT_TO_ENUM(MODEL_NAMES, HASH("VOLATOL"))
								VECTOR vTurretOffsetFromVeh
									SWITCH g_iVehicleTurretSeat
										CASE 1 // nose			
											vTurretOffsetFromVeh = <<cfVOLATOL_NOSE_BASE_X, cfVOLATOL_NOSE_BASE_Y, cfVOLATOL_NOSE_BASE_Z>>
											vTempPosOffset = vTurretOffsetFromVeh + ROTATE_VECTOR_ABOUT_Z(<<cfVOLATOL_NOSE_CAM_X, cfVOLATOL_NOSE_CAM_Y, cfVOLATOL_NOSE_CAM_Z>>, HUD.vAttachedCamRotationOffset.z)
											vTempRotOffset = <<-7, 0.0, 0.0>>
										BREAK
										CASE 2 // Top
											vTurretOffsetFromVeh = <<cfVOLATOL_ROOF_BASE_X, cfVOLATOL_ROOF_BASE_Y, cfVOLATOL_ROOF_BASE_Z>>
											vTempPosOffset = vTurretOffsetFromVeh + ROTATE_VECTOR_ABOUT_Z(<<cfVOLATOL_ROOF_CAM_X, cfVOLATOL_ROOF_CAM_Y, cfVOLATOL_ROOF_CAM_Z>>, HUD.vAttachedCamRotationOffset.z)
											vTempRotOffset = <<-5.8, 0.0, 0.0>>
										BREAK
									ENDSWITCH									
								ENDIF
								IF GET_ENTITY_MODEL(HUD.camEntity) = INT_TO_ENUM(MODEL_NAMES, HASH("BOMBUSHKA"))
									SWITCH g_iVehicleTurretSeat
										CASE 0 // top
											vTempPosOffset = <<-3.420, -4.260, 2.430>> +<<2.82,-1.25,0>>	
											vTempRotOffset = <<0.0, 0.0, 0.0>> 
										BREAK 
										CASE 1 // nose
											IF NOT IS_ENTITY_DEAD(HUD.vehChopper)
											AND GET_VEHICLE_MOD(HUD.vehChopper, MOD_ROOF) = 0 //Modded (-1 = default)
												vTempPosOffset = <<-5.20, -0.22, -1.50>> +<<0.2,0.0, -0.4>>		
												vTempRotOffset = <<0.0, 0.0, 90.0 >> 
											ELSE
												vTempPosOffset = <<-5.20, -0.3, -1.50>> 					
												vTempRotOffset = <<0.0, 0.0, 90.0 >>
											ENDIF
											BREAK
										CASE 2 // butt
											vTempPosOffset = <<23.000, -0.180, 1.230>> 				
											vTempRotOffset = <<0.0, 0.0, -90.0 >> 
										BREAK
									ENDSWITCH
								ENDIF
								
								IF GET_ENTITY_MODEL(HUD.camEntity) = INT_TO_ENUM(MODEL_NAMES, HASH("AVENGER"))
									SWITCH g_iInteriorTurretSeat
										CASE 1
											vTempPosOffset = <<cfAVENGER_T0_X, cfAVENGER_T0_Y, cfAVENGER_T0_z>>
											vTempRotOffset = <<cfAVENGER_T0_RX, cfAVENGER_T0_RY, cfAVENGER_T0_RZ>>
										BREAK 
										CASE 2
											vTempPosOffset = <<cfAVENGER_T1_X, cfAVENGER_T1_Y, cfAVENGER_T1_Z>>
											vTempRotOffset = <<cfAVENGER_T1_RX, cfAVENGER_T1_RY, cfAVENGER_T1_RZ>>
										BREAK
										CASE 3 
											vTempPosOffset = <<cfAVENGER_T2_X, cfAVENGER_T2_Y, cfAVENGER_T2_Z>>
											vTempRotOffset = <<cfAVENGER_T2_RX, cfAVENGER_T2_RY, cfAVENGER_T2_RZ>>
										BREAK
									ENDSWITCH
								ENDIF
								
								IF GET_ENTITY_MODEL(HUD.camEntity) = INT_TO_ENUM(MODEL_NAMES, HASH("AKULA"))
//									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CalculateTurretCamera2: GET_ENTITY_MODEL(HUD.camEntity) = AKULA, g_iInteriorTurretSeat = ", g_iInteriorTurretSeat)
									SWITCH g_iVehicleTurretSeat
										CASE 0
											vTempPosOffset = <<-0.00, 9.0, -1.2>>
											vTempRotOffset = <<-10.2537, -0.0000, 0.0>>
										BREAK 
										CASE 1
											vTempPosOffset = <<-cfAKULA_T1_X, cfAKULA_T1_Y, cfAKULA_T1_Z>>
											vTempRotOffset = <<cfAKULA_T1_RX, cfAKULA_T1_RY, -cfAKULA_T1_RZ>>
										BREAK
										CASE 2
											vTempPosOffset = <<cfAKULA_T1_X, cfAKULA_T1_Y, cfAKULA_T1_Z>>
											vTempRotOffset = <<cfAKULA_T1_RX, cfAKULA_T1_RY, cfAKULA_T1_RZ>>
										BREAK
 
									ENDSWITCH
								ELSE
//									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CalculateTurretCamera2: GET_ENTITY_MODEL(HUD.camEntity) != AKULA, GET_ENTITY_MODEL(HUD.camEntity) =  ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(HUD.camEntity)))
								ENDIF
								
								IF GET_ENTITY_MODEL(HUD.camEntity) = PHANTOM3 //@@ MOC2 << -- Change this when MOC2 model is in
									SWITCH g_iVehicleTurretSeat 
										CASE 0 // Front
											vTempPosOffset = <<cfMOC2_0_X, cfMOC2_0_Y, cfMOC2_0_Z>>
											vTempRotOffset = <<cfMOC2_0_RX, cfMOC2_0_RY, cfMOC2_0_RZ>>
										BREAK
										CASE 1 //Back
											vTempPosOffset = <<cfMOC2_1_X, cfMOC2_1_Y, cfMOC2_1_Z>>
											vTempRotOffset = <<cfMOC2_1_RX, cfMOC2_1_RY, cfMOC2_1_RZ>>
										BREAK
									ENDSWITCH
								ENDIF
								
								
								IF GET_ENTITY_MODEL(HUD.vehChopper) = BOMBUSHKA 
									HARD_ATTACH_CAM_TO_PED_BONE(HUD.camChopper, PLAYER_PED_ID(), BONETAG_NULL, vTempRotOffset + << HUD.vAttachedCamRotationOffset.x, 0.0, HUD.vAttachedCamRotationOffset.z>>, vTempPosOffset, TRUE)
								ELIF g_iInteriorTurretSeat != -1
								OR IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
								AND GET_ENTITY_MODEL(HUD.vehChopper) != BOMBUSHKA
									
									#IF IS_DEBUG_BUILD
									IF HUD.bDebugDrawRelRot
										TEXT_LABEL_15 txtDebug = "Yaw(z) : " 
										txtDebug += FLOAT_TO_STRING(HUD.vAttachedCamRotationOffset.z)
										SET_TEXT_SCALE(0.4, 0.4)
										SET_TEXT_COLOUR(0, 0, 255, 255) 
										SET_TEXT_JUSTIFICATION(FONT_LEFT)
										SET_TEXT_FONT(FONT_STANDARD)
										DISPLAY_TEXT_WITH_LITERAL_STRING(0.2, 0.1, "STRING", txtDebug)
										
										txtDebug = "Pitch(x) : "
										txtDebug += FLOAT_TO_STRING(HUD.vAttachedCamRotationOffset.x)
										SET_TEXT_SCALE(0.4, 0.4)
										SET_TEXT_COLOUR(0, 0, 255, 255) 
										SET_TEXT_JUSTIFICATION(FONT_LEFT)
										SET_TEXT_FONT(FONT_STANDARD)
										DISPLAY_TEXT_WITH_LITERAL_STRING(0.2, 0.2, "STRING", txtDebug)
									ENDIF
									#ENDIF
									
									CDEBUG1LN(DEBUG_NET_GUN_TURRET, "camChopper: vTempRotOffset = ", vTempPosOffset, ", HUD.vAttachedCamRotationOffset = ", HUD.vAttachedCamRotationOffset)
									ATTACH_CAM_TO_VEHICLE_BONE(HUD.camChopper, HUD.camEntity, 0, TRUE , vTempRotOffset + << HUD.vAttachedCamRotationOffset.x, 0.0, HUD.vAttachedCamRotationOffset.z>>, vTempPosOffset, TRUE)
								ENDIF
							ENDIF
							
						ENDIF
						
						IF NOT (iZoom = 0 OR HUD.fFov = HUD.maxFOV OR HUD.fFov = HUD.minFOV)
							IF HUD.soundID_zoom != -1
								IF HAS_SOUND_FINISHED(HUD.soundID_zoom)	
									IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
										PLAY_SOUND_FRONTEND(HUD.soundID_zoom,"Zoom_Loop", GET_TURRET_FIRE_SOUNDS_NAME(TFSS_IAA))	
										CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera PLAY_SOUND_FRONTEND Zoom_Loop with set ", GET_TURRET_FIRE_SOUNDS_NAME(TFSS_IAA))
									ELSE
										TURRET_FIRE_SOUND_SET soundset = GET_TURRET_AUDIO_SET()									
										IF soundset != TFSS_INVALID
											PLAY_SOUND_FRONTEND(HUD.soundID_zoom,"Zoom", GET_TURRET_FIRE_SOUNDS_NAME(soundset))
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera PLAY_SOUND_FRONTEND Zoom set ", GET_TURRET_FIRE_SOUNDS_NAME(soundset))
										ELSE
											PLAY_SOUND_FRONTEND(HUD.soundID_zoom,"COP_HELI_CAM_ZOOM")
											CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[SOUNDCAM] ControlChopperCamera PLAY_SOUND_FRONTEND COP_HELI_CAM_ZOOM ")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF HUD.soundID_zoom != -1			
								IF NOT HAS_SOUND_FINISHED(HUD.soundID_zoom) 
									SET_VARIABLE_ON_SOUND(HUD.soundID_zoom,"Ctrl",ABSF(TO_FLOAT(iZoom) / 128))
									IF iZoom < 0 
										SET_VARIABLE_ON_SOUND(HUD.soundID_zoom,"Dir",-1)
									ELSE
									//	SET_VARIABLE_ON_SOUND(HUD.soundID_zoom,"Dir",0)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF HUD.soundID_zoom != -1
								IF NOT HAS_SOUND_FINISHED(HUD.soundID_zoom)	
									STOP_SOUND(HUD.soundID_zoom)
								ENDIF
							ENDIF
						ENDIF
												
					//	cprintln(debug_Trevor3,HUD.vCamRot .x)
					//	IF iZoom < 0
					//		HUD.fFOV += (TO_FLOAT(iZoom) / HUD.ChopperZoomFactor) * TIMESTEP() * sin(absf(HUD.vCamRot .x))
					//	ELSE
						INT iCamZoomSpeed 
						#IF IS_DEBUG_BUILD
						IF g_BombushkaTurretCamZoomSpeedMultiplier = -1
						#ENDIF
							iCamZoomSpeed = 4
						#IF IS_DEBUG_BUILD
						ELSE
							iCamZoomSpeed = g_BombushkaTurretCamZoomSpeedMultiplier
						ENDIF
						#ENDIF
						IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
							HUD.ChopperZoomFactor = 5
						ENDIF
						IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
						OR DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
							HUD.fFOV += (TO_FLOAT(iZoom) / HUD.ChopperZoomFactor) * TIMESTEP() * iCamZoomSpeed
						ELSE
							HUD.fFOV += (TO_FLOAT(iZoom) / HUD.ChopperZoomFactor) * TIMESTEP() 
						ENDIF
					//	ENDIF
						/*
						if HUD.vCamRot .x < -20 and HUD.vCamRot .x > -35
							
						float fovLimit = sin(absf(HUD.vCamRot .x))-25) 
						if fovLimit < 0.5 fovlimit = 0.5 endif
						float minfov = 28 - (fovLimit * 23)
						float fovToSet = HUD.fFOV
						IF fovToSet < minfov HUD.fFOV = minfov endif
						*/
						
						
						
						if HUD.fFov > HUD.maxFOV
							HUD.fFov = HUD.maxFOV
						ENDIF
						if HUD.fFov < HUD.minFOV
							HUD.fFov = HUD.minFOV			
						ENDIF
						
						SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(1.0)
					
					
						//restrict camera pitch
						/*
						if HUD.fStickPitch < MAX_CAMERA_PITCH
							HUD.fStickPitch = MAX_CAMERA_PITCH
						ENDIF
						
						if HUD.fStickPitch > MIN_CAMERA_PITCH
							HUD.fStickPitch = MIN_CAMERA_PITCH
						ENDIF 
					
						HUD.vCamRot = <<HUD.fStickPitch,0,HUD.fStickYaw>>
						*/
					
						//if not IS_ENTITY_DEAD(HUD.camEntity)
						//	HUD.chopperHeading = GET_ENTITY_HEADING(HUD.camEntity)
						//ENDIF
						
						
						
						//SET_CAM_ROT(HUD.camChopper,HUD.vCamRot)//+<<0,0,HUD.chopperHeading>>)
						
						
						SET_CAM_FOV(HUD.camChopper,HUD.fFOV)
						float fBlurOverride = (HUD.fFOV - 5.0) /42.0
						SET_DISTANCE_BLUR_STRENGTH_OVERRIDE(fBlurOverride)
									
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "HUD.bIsCamAttached: ", HUD.bIsCamAttached, ", HUD.mpCamFudge = ", HUD.mpCamFudge)
			if HUD.bIsCamAttached = TRUE and HUD.mpCamFudge=true			
				vector vGroundLoc = hud.vaimat
				float fSphereRadius =  GET_DISTANCE_BETWEEN_COORDS(vGroundLoc,GET_CAM_COORD(HUD.camChopper))//150.0 //distance to point on ground
				HUD.vTargetedGround.x = (COS(HUD.vCamRot .x) * fSphereRadius) * sin(-HUD.vCamRot .z)
				HUD.vTargetedGround.y = (COS(HUD.vCamRot .x) * fSphereRadius) * cos(-HUD.vCamRot .z)
				HUD.vTargetedGround.z = (sin(HUD.vCamRot .x) * fSphereRadius)
				HUD.vTargetedGround += GET_CAM_COORD(HUD.camChopper)
				POINT_CAM_AT_COORD(HUD.camChopper,HUD.vTargetedGround)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "POINT_CAM_AT_COORD")
				SET_CAM_FOV(HUD.camChopper,HUD.fFOV)
			ENDIF
		ENDIF
	ELSE
		if DOES_CAM_EXIST(HUD.camChopper)
			if IS_CAM_ACTIVE(HUD.camChopper)
				SET_CAM_ACTIVE(HUD.camChopper,FALSE)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



FUNC float angleToProportioon(float angle)
	angle = angle%360
	if angle < 0
		angle += 360
	ENDIF
	angle /= 360.0
	return angle
ENDFUNC







FUNC bool DISPLAY_SMALL_PED_DATA(structHelicopterHUDVars &HUD,int pedEntry,float fx, float fy,int r,int g,int b,float fScale,bool isThreat=FALSE)//int i,int r,int g,int b)
	//string aString

	
	SET_TEXT_FOR_HUD(r,g,b,0.43,isThreat)
	if fScale < 0.7
		fScale = 0.7
	ENDIF
	if fScale > 1.5
		fScale = 1.5
	ENDIF
	
	TEXT_LABEL_15 tLabelA

	int i
	//get name from bit variable
	#if ENABLE_ENHANCED_HUD_FEATURES
		if HUD.pedScanList[pedEntry].iSpecialNameCase != -1
			tLabelA = HUD.specialNames[HUD.pedScanList[pedEntry].iSpecialNameCase]
			SET_TEXT_FOR_HUD(r,g,b,0.43,isThreat)
			
			DISPLAY_TEXT(0.5,0.68,tLabelA,1)
		
		ELSE
	#ENDIF
	
	IF IS_BIT_SET(HUD.pedScanList[pedEntry].BITpedData,8) // Is a multiplayer ped
		// Find this player ped's name
							
			/*	//player can no longer be a criminal
			INT iPlayer	
      REPEAT NUM_NETWORK_PLAYERS iPlayer
	  	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer))
      		IF GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)) = HUD.pedScanList[pedEntry].pedID		  	
				   	SET_TEXT_FOR_HUD(r,g,b,fScale,true)
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
					END_TEXT_COMMAND_DISPLAY_TEXT(fx, fy)
            ENDIF
		ENDIF
      ENDREPEAT   
        	*/	
	ELSE
		/*
		for i = 0 to 23
			if IS_BIT_SET(HUD.pedScanList[pedEntry].BITnamesAndCrimes,i)
				if i < 8
					tLabelA = "NAME_MA"
					tLabelA+= i+1										
					i=15
				ELSE
					if i < 16
						tLabelA += "NAME_FA"
						tLabelA+= i-7
						//sName += GET_STRING_FROM_TEXT_FILE(tName)
						i=15
					ELSE					
						tLabelB = "NAME_S"
						tLabelB+= i-15
						//sName += GET_STRING_FROM_TEXT_FILE(tName)
						i=24
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		SET_TEXT_FOR_HUD(r,g,b,0.43,isThreat)
		//float fTWidth = GET_STRING_WIDTH(tLabelA)
		
		DISPLAY_TEXT_WITH_TWO_STRINGS(0.5,0.68,"NAMES",tLabelA,tLabelB)
		*/
	ENDIF
	
	#if ENABLE_ENHANCED_HUD_FEATURES
		ENDIF
	#ENDIF
	
	int iCrimeCount//, iStringLength
//	float ftotalLength
	for i = 24 to 31
		if IS_BIT_SET(HUD.pedScanList[pedEntry].BITnamesAndCrimes,i)			
			tLabelA = "crimes_"
			tLabelA+= i-23
						
			//SET_TEXT_FOR_HUD(r,g,b,fScale,true)
			
			//if iStringLength + GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(tLabelA) + 2 < 24
			/*	if iCrimeCount>0
					iStringLength+= 2
					SET_TEXT_FOR_HUD(r,g,b,fScale,true)
					DISPLAY_TEXT(fx+ftotalLength,fy+(HUD.textSpace*fScale),"comma")
					SET_TEXT_FOR_HUD(r,g,b,fScale,true)
					ftotalLength+= GET_STRING_WIDTH("comma")
					SET_TEXT_FOR_HUD(r,g,b,fScale,true)
					ftotalLength+= (GET_STRING_WIDTH("space") * 0.3)
				ENDIF*/
				
			SET_TEXT_FOR_HUD(r,g,b,0.43,isThreat)
			DISPLAY_TEXT(0.5,0.68+(0.037*(iCrimeCount+1)),tLabelA,1)
				//SET_TEXT_FOR_HUD(r,g,b,fScale,true)
				//ftotalLength+= GET_STRING_WIDTH(tLabelA)
				
			//	tCrimes += GET_STRING_FROM_TEXT_FILE(tName) 
				//SAVE_STRING_TO_DEBUG_FILE(tCrimes)
				//SAVE_NEWLINE_TO_DEBUG_FILE()
				//iStringLength += GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(tLabelA)
				iCrimeCount++
			//sENDIF		
			
			
		ENDIF
	ENDFOR

	if iCrimeCount = 0
		
		SET_TEXT_FOR_HUD(r,g,b,0.43,isThreat)
		DISPLAY_TEXT(fx,fy+(HUD.textSpace*fScale),"unknown",1)
	ENDIF
	
	
	if HUD.pedScanList[pedEntry].relType = HUD_FRIEND
		SET_TEXT_FOR_HUD(r,g,b,0.43,isThreat)
		DISPLAY_TEXT(fx,fy+(HUD.textSpace*fScale),"HUD_ID2",1)
	//ELSE
	//	SET_TEXT_FOR_HUD(r,g,b,fScale,true)
	//	DISPLAY_TEXT_WITH_LITERAL_STRING(fx,fy+(HUD.textSpace*fScale),"string",tCrimes)	
	ENDIF
	
	/*
	if HUD.pedScanList[pedEntry].dataHeld = TRUE		
		
		if DOES_PED_HAVE_NAME(pedEntry) //not ARE_STRINGS_EQUAL(HUD.pedScanList[pedEntry].name,"")
			DISPLAY_TEXT_WITH_LITERAL_STRING(fx,fy,"string",HUD.pedScanList[pedEntry].name)
			SET_TEXT_FOR_HUD(r,g,b,fScale,true)
			if HUD.pedScanList[pedEntry].isCriminal = TRUE
				if not ARE_STRINGS_EQUAL(HUD.pedStatus[HUD.pedScanList[pedEntry].status],"")
					DISPLAY_TEXT(fx,fy+(HUD.textSpace*fScale),HUD.pedStatus[HUD.pedScanList[pedEntry].status])	
				ENDIF
			ENDIF
			if HUD.pedScanList[pedEntry].enhancedData = TRUE
				SET_TEXT_FOR_HUD(r,g,b,fScale,true)
				if not ARE_STRINGS_EQUAL(HUD.pedScanList[pedEntry].crimes,"")
					DISPLAY_TEXT_WITH_LITERAL_STRING(fx,fy+(HUD.textSpace*2*fScale),"string",HUD.pedScanList[pedEntry].crimes)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		if HUD.pedScanList[pedEntry].relType = HUD_FRIEND		
			if not ARE_STRINGS_EQUAL(HUD.pedScanList[pedEntry].name,"")
				DISPLAY_TEXT_WITH_LITERAL_STRING(fx,fy,"string",HUD.pedScanList[pedEntry].name)
				SET_TEXT_FOR_HUD(r,g,b,fScale,true)
				DISPLAY_TEXT(fx,fy+(HUD.textSpace*fScale),"HUD_ID2")
			ENDIF
			
			
		ELSE			
			DISPLAY_TEXT(fx,fy,"unknown")
		ENDIF
	ENDIF
	//DISPLAY_TEXT(0,0,pedStatus[0])
	*/
	return true
ENDFUNC

func float GET_BOX_SIZE(structHelicopterHUDVars &HUD,ped_index aPed, entity_index veh)
	if not IS_ENTITY_DEAD(aPed)
		vector vPedCoord = GET_WORLD_POSITION_OF_ENTITY_BONE(aPed,0)			
		if not IS_ENTITY_DEAD(veh)
			float fPedRange = GET_DISTANCE_BETWEEN_COORDS(vPedCoord,GET_ENTITY_COORDS(veh))
			float fBoxScaler = HUD.fRangeMultiplier / (HUD.fFOV * fPedRange)
			if fBoxScaler < 0.4
				fBoxScaler = 0.4
			ENDIF
			if fBoxScaler > 2.0
				fBoxScaler = 2.0
			ENDIF
			
			RETURN fBoxScaler
		ENDIF
	endif
	RETURN 0.0
ENDFUNC	

PROC DRAW_RANGE_TEXT(float scale,vector v1, vector v2, float screenX, float screenY, int r, int g, int b)							
	
	float fRange = GET_DISTANCE_BETWEEN_COORDS(v1,v2,FALSE)
	int iRange = floor(fRange)


	SET_TEXT_FOR_HUD(r,g,b,scale,true)
	
	if iRange < 10
		DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"NUM_0",iRange)
	else 
		if iRange < 100
			DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"NUM_0",iRange)
		ELSE	
			if iRange < 1000								
				DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"NUM_0",iRange)
			ELSE
				DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"NUM_0",iRange)
			endif
		ENDIF
	ENDIF
ENDPROC


PROC DRAW_OFFSCREEN_ARROW_FOR_COORD(structHelicopterHUDVars &HUD,vector vDestCoord, int r=255, int g=255, int b=0)
	float fpos
	if DOES_CAM_EXIST(GET_RENDERING_CAM())	
		if not GET_SCREEN_COORD_FROM_WORLD_COORD(vDestCoord,fpos,fpos)
			vector camCoord = GET_CAM_COORD(GET_RENDERING_CAM())
			vector camRot = GET_CAM_ROT(GET_RENDERING_CAM())
			float dist = GET_DISTANCE_BETWEEN_COORDS(<<camCoord.x,camCoord.y,0>>,<<vDestCoord.x,vDestCoord.y,0>>)
			float height = vDestCoord.z - camCoord.z
			float fPitchToPed
			if dist > 0
				fPitchToPed = ATAN(height/dist)
			ELSE
				fPitchToPed = 0
			ENDIF
			
			float fHeadingToPed = GET_HUD_HEADING_FROM_COORDS(camCoord,vDestCoord)
			
			
			//float angleToPoint=ATAN2(COS(lat1)*SIN(lat2)-SIN(lat1)*COS(lat2)*COS(lon2-lon1),SIN(lon2-lon1)*COS(lat2)) 
			float angleToPoint=ATAN2(COS(camRot.x)*SIN(fPitchToPed)-SIN(camRot.x)*COS(fPitchToPed)*COS((fHeadingToPed*-1)-camRot.z),SIN((fHeadingToPed*-1)-camRot.z)*COS(fPitchToPed))
		
			
			if HUD.fChopperSpeed > 0
			endif
		
			float sX = 0.5 - (cos(angleToPoint)*0.29)
			float sY = 0.5 - (sin(angleToPoint)*0.29)
			
		//	float stx = 0.5 - (cos(angleToPoint)*0.25)
		//	float sty = 0.5 - (sin(angleToPoint)*0.25) - 0.02
				
			DRAW_SPRITE("helicopterhud", "hudArrow", sX,sY , 0.02, 0.04, angleToPoint-90, r,g,b, 255,TRUE)						
			SET_TEXT_CENTRE(TRUE)
		//	DRAW_RANGE_TEXT(1.0,camCoord,vDestCoord,stx,sty,r,g,b)
		ENDIF
	ENDIF	
ENDPROC

proc drawBoneBoxes(structHelicopterHUDVars &HUD,float fscanTime, float scanFirst, float scanDuration, float fNudgeX, float fNudgeY, float fBoxScaler, float fBoneX2, float fBoneX1, float fBoneY2, float fBoneY1, float scaler=1.0)		
	int iB
	float fAlpha
	float scanGap = scanDuration / 10.0 //0.03
	for iB = 0 to 10
		fAlpha = fscanTime - (scanFirst+(iB * scanGap))	//0.4 - (0.3 + 0.15)
		if fAlpha > 0 and fAlpha < 0.3
			fAlpha = SIN(fAlpha * 600.0)
			DRAW_SPRITE("helicopterhud","hud_line",fNudgeX*iB,fNudgeY*iB,fBoxScaler*scaler*0.01,fBoxScaler*scaler*0.01,GET_ANGLE_BETWEEN_2D_VECTORS(0.0,1.0,fBoneX2-fBoneX1,fBoneY2-fBoneY1),hud.scanningColour.r,hud.scanningColour.g,hud.scanningColour.b,floor(fAlpha*32),TRUE)
		ENDIF
	ENDFOR
ENDPROC

proc draw_scan_ped(structHelicopterHUDVars &HUD,ped_index scanPed,float fscanTime, float fBoxscaler)

	
	vector vBone1,vBone2
	float fNudgeX,fNudgeY
	float fBoneX1,fBoneY1,fBoneX2,fBoneY2
	vector vNull = <<0,0,0>>
	float scanFirst, scanDuration



	
	//****************************** scan left leg **********************************
	
	
	if fscanTime < 0.5
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_FOOT,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_CALF,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		scanFirst = 0.0
		scanDuration = 0.3
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(hud, fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
		/*
		for iB = 0 to 10
			fAlpha = fscanTime - (scanFirst+(iB * scanGap))			
			if fAlpha > 0 and fAlpha < 0.1
				fAlpha = SIN(fAlpha * 1800.0)
				DRAW_NAMED_SPRITE("helicopterhud","hud_line",fNudgeX*iB,fNudgeY*iB,fBoxScaler*0.01,fBoxScaler*0.01,GET_ANGLE_BETWEEN_2D_VECTORS(0.0,1.0,fBoneX2-fBoneX1,fBoneY2-fBoneY1),0,255,0,floor(fAlpha*128))
			ENDIF
		ENDFOR
	
*/	
		//****************************** scan right leg **********************************
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_FOOT,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_CALF,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		
		scanFirst = 0.0
		scanDuration = 0.3
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(HUD,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
		/*
		for iB = 0 to 10
			fAlpha = fscanTime - (scanFirst+(iB * scanGap))	
			if fAlpha > 0 and fAlpha < 0.1
				fAlpha = SIN(fAlpha * 1800.0)
				DRAW_NAMED_SPRITE("helicopterhud","hud_line",fNudgeX*iB,fNudgeY*iB,fBoxScaler*0.01,fBoxScaler*0.01,GET_ANGLE_BETWEEN_2D_VECTORS(0.0,1.0,fBoneX2-fBoneX1,fBoneY2-fBoneY1),0,255,0,floor(fAlpha*128))
			ENDIF
		ENDFOR*/
	ENDIF
	
	if fscanTime > 0.15 and fscanTime < 0.7
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_CALF,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_THIGH,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		scanFirst = 0.15
		scanDuration = 0.3
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
	
	
		//****************************** scan right leg **********************************
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_CALF,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_THIGH,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		
		scanFirst = 0.15
		scanDuration = 0.3
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
	ENDIF
	
	//body
	if fscanTime > 0.3 and fscanTime < 1.0
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_PELVIS,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_NECK,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		scanFirst = 0.3
		scanDuration = 0.5
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1,2.0)
	ENDIF
	
	//head
	if fscanTime > 0.6 and fscanTime < 1.0
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_HEAD,vnull)
			vBone2 = vBone1 + ((vBone1 - GET_PED_BONE_COORDS(scanPed,BONETAG_NECK,vnull)) * 3.0)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		scanFirst = 0.6
		scanDuration = 0.3
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1,1.3)
	ENDIF
	
	//upper arms
	if fscanTime > 0.6 and fscanTime < 1.0
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_UPPERARM,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_FOREARM,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		scanFirst = 0.6
		scanDuration = 0.3
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
	
	
		//****************************** scan left arm **********************************
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_UPPERARM,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_FOREARM,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		
		scanFirst = 0.6
		scanDuration = 0.3
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
	ENDIF
	
	//lower arms
	if fscanTime > 0.75 and fscanTime < 1.0
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_FOREARM,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_R_HAND,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		scanFirst = 0.75
		scanDuration = 0.25
		//scanGap = scanDuration / 10.0
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
	
	
		//****************************** scan left arm **********************************
		if not IS_PED_INJURED(scanPed)
			vBone1 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_FOREARM,vnull)
			vBone2 = GET_PED_BONE_COORDS(scanPed,BONETAG_L_HAND,vnull)
			SET_DRAW_ORIGIN(vBone1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone1,fBoneX1,fBoneY1)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vBone2,fBoneX2,fBoneY2)		
			
			fNudgeX = (fBoneX2-fBoneX1) / 10.0
			fNudgeY = (fBoneY2-fBoneY1) / 10.0
		ENDIF
	
		
		scanFirst = 0.75
		scanDuration = 0.3
	
		drawBoneBoxes(hud,fscanTime,scanFirst,scanDuration,fNudgeX,fNudgeY,fBoxScaler,fBoneX2,fBoneX1,fBoneY2,fBoneY1)
	ENDIF
		/*
		int iB
		for iB = 0 to 9
			DRAW_NAMED_SPRITE("helicopterhud","hud_line",fNudgeX*iB,fNudgeY*iB,fBoxScaler*0.01,fBoxScaler*0.01,GET_ANGLE_BETWEEN_2D_VECTORS(0.0,1.0,fBoneX2-fBoneX1,fBoneY2-fBoneY1),0,255,0,fAlpha)															
		ENDFOR																												

		fXGap = fBoneX2 - fBoneX1
		fYGap = fBoneY2 - fBoneY1
		
		if fYGap >= fXGap
			fMultiplier = fXGap / fYGap
		ELSE
			fMultiplier = fYGap / fXGap
		ENDIF
		
		fDrawOffset = fBoxScaler * fMultiplier
		
		if fYGap >= fXGap
		
			//DRAW_LINE_2D(0,0,fBoneX2-fBoneX1,fBoneY2-fBoney1,0.001,250,0,0,128)
			//DRAW_LINE_2D(fBoneX1-fBoxScaler,fBoneY1-fDrawOffset,fBoneX2+fBoxScaler,fBoneY2+fDrawOffset,0.05,250,0,0,128)
		ELSE
			//DRAW_LINE_2D(0,0,fBoneX2-fBoneX1,fBoneY2-fBoney1,0.001,250,0,0,128)
			//DRAW_LINE_2D(fBoneX1-fBoxScaler,fBoneY1-fDrawOffset,fBoneX2+fBoxScaler,fBoneY2+fDrawOffset,0.05,250,0,0,128)
		ENDIF
		*/
		CLEAR_DRAW_ORIGIN()
ENDPROC

proc draw_ped_outline(structHelicopterHUDVars &HUD, vector vPedCoord,float fScale,int relType, int r=-1,int g=-1,int b=-1)//, bool bIsTargetted=FALSE)	
	int targetR,targetG,targetB
	getHUDBoxColour(hud,relType,targetR,targetG,targetB)	
	SET_DRAW_ORIGIN(vPedCoord)
	if r!=-1
		targetR = r
		targetG = g
		targetB = b
	ENDIF
	
	
	
	fScale *= 0.03
	
	DRAW_SPRITE("helicopterhud", "hud_corner",-fScale*0.5,-fScale,0.013,0.013,0,targetR,targetG,targetB, 200,TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner",fScale*0.5,-fScale,0.013,0.013,90,targetR,targetG,targetB, 200,TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner",-fScale*0.5,fScale,0.013,0.013,270,targetR,targetG,targetB, 200,TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner",fScale*0.5,fScale,0.013,0.013,180,targetR,targetG,targetB, 200,TRUE)
	
	/*IF bIsTargetted
		DRAW_SPRITE("helicopterhud", "hud_corner",(-fScale*0.5)-0.003,(-fScale)-0.003,0.013,0.013,0,targetR,targetG,targetB, 200)
		DRAW_SPRITE("helicopterhud", "hud_corner",(fScale*0.5)+0.003,-fScale-0.003,0.013,0.013,90,targetR,targetG,targetB, 200)
		DRAW_SPRITE("helicopterhud", "hud_corner",(-fScale*0.5)-0.003,fScale+0.003,0.013,0.013,270,targetR,targetG,targetB, 200)
		DRAW_SPRITE("helicopterhud", "hud_corner",(fScale*0.5)+0.003,fScale+0.003,0.013,0.013,180,targetR,targetG,targetB, 200)
	ENDIF*/
	CLEAR_DRAW_ORIGIN()
ENDPROC

proc displayChopperHud(	structHelicopterHUDVars &HUD, bool onlyShowHudMarkers = FALSE,
						FLOAT fMinHeadingOverride = MIN_CAMERA_HEADING, FLOAT fMaxHeadingOverride = MAX_CAMERA_HEADING)
	
	if HUD.bDisplayHUD = TRUE
	
	
		
		float fMulti
		
		//fMulti = HUD.vCamRot.x / -75
		

		fMulti = (HUD.vCamRot.x - HUD.AngHigh) / (HUD.angLow - HUD.AngHigh)
		IF fMulti < 0 fMulti = 0 ENDIF
		IF fMulti > 1 fMulti = 1 ENDIF
		
		float fLowLOD,fHighLOD
		fLowLod = HUD.angUpLow +((HUD.angDownLow-HUD.angUpLow)*fMulti)
		fHighLod = HUD.angUpHigh +((HUD.angDownHigh-HUD.angUpHigh)*fMulti)

	//	cprintln(debug_Trevor3,"Low LOD = ",fLowLod," high lod = ",fHighLod)
	
		REMAP_LODSCALE_RANGE_THIS_FRAME(1.7,4.7,fLowLod,fHighLod)
		
		
		float fCheckSpeed = GET_ENTITY_SPEED(HUD.vehChopper)
		IF fCheckSpeed > 30
			SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()
		ENDIF
		
		cprintln(debug_trevor3,"FORCE_ALLOW_TIME_BASED_FADING_THIS_FRAME()")
		FORCE_ALLOW_TIME_BASED_FADING_THIS_FRAME()
		
		int i
		
		#if ENABLE_ENHANCED_HUD_FEATURES
	
		IF IS_VEHICLE_DRIVEABLE(HUD.vehChopper)
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				SET_HELI_BLADES_FULL_SPEED(HUD.vehChopper)
			ENDIF
		ENDIF
	
		IF HUD.minChopperHours != -1	
			IF HUD.maxChopperHours < HUD.minChopperHours
				IF GET_CLOCK_HOURS() < HUD.minChopperHours	
				AND GET_CLOCK_HOURS()> HUD.maxChopperHours + 1
					SET_CLOCK_TIME(HUD.minChopperHours,0,0)
				ELSE
					IF GET_CLOCK_HOURS() >= HUD.maxChopperHours
					AND GET_CLOCK_HOURS() < HUD.minChopperHours
						SET_CLOCK_TIME(HUD.maxChopperHours,0,0)
					ENDIF
				ENDIF					
			ELSe
				IF GET_CLOCK_HOURS() < HUD.minChopperHours			
					SET_CLOCK_TIME(HUD.minChopperHours,0,0)
				ELSE
					IF GET_CLOCK_HOURS() >= HUD.maxChopperHours
						SET_CLOCK_TIME(HUD.maxChopperHours,0,0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		#endif
		
		int targetR, targetG, targetB
			
				
			if (not HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")) or (onlyshowhudmarkers=FALSE AND not HAS_SCALEFORM_MOVIE_LOADED(mov))
				cprintln(debug_trevor3,"no scaleform time: ",GET_GAME_TIMER())
				HUD.bTexturesLoaded = FALSE			
			ELSE
				HUD.fLineAWidth = (1.0 - (2*HUD.fLineAInset)) 			

				//SET_SPRITES_DRAW_BEFORE_FADE(true)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
				IF onlyshowhudmarkers=FALSE
				
					IF (g_iShouldLaunchTruckTurret != -1) AND HUD.bDelayRenderScriptCams 
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "DISPLAYCHOPPERHUD: Delay rendering scaleform, waiting for NOT bDelayRenderScriptCams.")
					ELSE
					
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0,1)		
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
					IF NOT HUD.bTexturesLoaded
						IF IS_VEHICLE_DRIVEABLE(hud.vehChopper)
							IF GET_ENTITY_MODEL(hud.vehChopper) = POLMAV
							AND GET_VEHICLE_LIVERY(hud.vehChopper) = 0		
								
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_CAM_LOGO")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								END_SCALEFORM_MOVIE_METHOD()
								
							ELSE						
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_CAM_LOGO")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
						ELSE
							BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_CAM_LOGO")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
					ENDIF
					
					ENDIF
		
				ENDIF	
				
				HUD.bTexturesLoaded = TRUE				
			ENDIF
			
			if HUD.bTexturesLoaded = TRUE
				
				IF onlyshowhudmarkers=FALSE
				ENDIF
				
			//	CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(movieID,"METHOD_NAME",...))
				

				//draw horiz marker at bottom of screen
				/*
				float markerPos = HUD.vCamRot.z + HUD.chopperHeading				
				startMarker = markerPos - (HUD.fFOV/2)
				endMarker = markerPos + (HUD.fFov/2)
				
				markerPos = angleToProportioon(markerPos)
				startMarker = angleToProportioon(startMarker)
				endMarker = angleToProportioon(endMarker)
				
				IF onlyshowhudmarkers=FALSE
					if startMarker > endMarker
						drawX0 = 1.0 - HUD.fLineAInset - (HUD.fLineAWidth-(HUD.fLineAWidth * startMarker))
						drawX1 = 1.0 - HUD.fLineAInset
					
						DRAW_SPRITE("helicopterhud", "hud_block", (drawX0+drawX1)/2, HUD.hudBarY, (drawX1-drawX0)+0.02, 0.025,0, 7, 253, 30, 200)
						DRAW_RECT((drawX0+drawX1)/2,HUD.hudBarY,drawX1-drawX0,0.01,HUD.colR,HUD.colG,HUD.colB,HUD.colA)
						drawX0 = HUD.fLineAInset //1.0 - fLineAInset - (fLineAWidth * startMarker)
						drawX1 = HUD.fLineAInset + (HUD.fLineAWidth * endMarker)
					
						DRAW_SPRITE("helicopterhud", "hud_block", (drawX0+drawX1)/2, HUD.hudBarY, (drawX1-drawX0)+0.02, 0.025,0, 7, 253, 30, 200)
						
						DRAW_RECT((drawX0+drawX1)/2,HUD.hudBarY,drawX1-drawX0,0.01,HUD.colR,HUD.colG,HUD.colB,HUD.colA)
						
					ELSE
						drawX0 = HUD.fLineAInset + (HUD.fLineAWidth * startMarker)
						drawX1 = HUD.fLineAInset + (HUD.fLineAWidth * endMarker)
					
						DRAW_SPRITE("helicopterhud", "hud_block", (drawX0+drawX1)/2, HUD.hudBarY, (drawX1-drawX0)+0.02, 0.025,0, 7, 253, 30, 200)
					
						DRAW_RECT((drawX0+drawX1)/2,HUD.hudBarY,drawX1-drawX0,0.01,HUD.colR,HUD.colG,HUD.colB,HUD.colA)
					
					ENDIF
				ENDIF
				*/
				//Draw reticule over peds for face recognition
				float xPos
				float yPos
				float nearX, nearY
				VECTOR vPedCoord
				float screenRange
				float fPedRange
				float fBoxDrawSize
//				float fPitchtoPed
//				float fPitchVariation
//				float fHeadingToPed
//				float fHeadingVariation
//				float fAngletoPed
			
				float closestRange = HUD.fMinFindPedOnScreenRange
				float zHeight
				int newNearestPed = -1
			
				float fBoxScaler
			//	float fFontScale=0.319
				
//				string sRange
				vector vChopper
		
				if not IS_ENTITY_DEAD(HUD.camEntity)				
					vChopper = GET_ENTITY_COORDS(HUD.camEntity)
				ENDIF
				
				if HUD.bDisableScanner = FALSE
					//show off screen arrows for peds
					i = 0
					//reset this each frame
					HUD.targetIsLost = FALSE
					
					REPEAT COUNT_OF(HUD.pedScanList) i
						if IS_BIT_SET(HUD.pedScanList[i].BITPedData,9)
							if DOES_ENTITY_EXIST(HUD.pedScanList[i].pedID)
								
								if not IS_ENTITY_DEAD(HUD.pedScanList[i].pedID)
									
									if IS_BIT_SET(HUD.pedScanList[i].BITPedData,0) //scanned										
										switch HUD.pedScanList[i].relType
											case HUD_FOE												
												DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID,0),hud.boxColourtarget.r,hud.boxColourtarget.g,hud.boxColourtarget.b)												
											BREAK
											case HUD_UNKNOWN												
												DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID,0),hud.boxColourFriendly.r,hud.boxColourFriendly.g,hud.boxColourFriendly.b)
											break
											case HUD_FRIEND
												DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID,0),hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b)
											break
											case HUD_BYSTANDER
												DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID,0),hud.boxColourFriendly.r,hud.boxColourFriendly.g,hud.boxColourFriendly.b)
											break
										ENDSWITCH										
									ELSE			
										if GET_CLOCK_HOURS() < 19 and GET_CLOCK_HOURS() > 7
											//DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,fBoxDrawSize,fBoxDrawSize*2,0, 255,170,170) //targetR,targetG,targetB, 70)
											DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID,0),hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b)
										ELSE
											DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID,0),hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b)
											//DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,fBoxDrawSize,fBoxDrawSize*2,0, 130,64,64)
										ENDIF										
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				
				
				
						
					
					if not IS_ENTITY_DEAD(HUD.camEntity)				
						GET_GROUND_Z_FOR_3D_COORD(vChopper,zHeight)
						
						//scan one ped each frame to avoid to many LOS checks
					
						if HUD.bShowTargetOffScreen = FALSE and HUD.iCurrentPedScanCount>0
							
							for i = 0 to HUD.iCurrentPedScanCount-1
								
							//REPEAT COUNT_OF(pedScanList) i		
								if DOES_ENTITY_EXIST(HUD.pedScanList[i].pedID)
									
									if not IS_ENTITY_DEAD(HUD.pedScanList[i].pedID) and not IS_ENTITY_DEAD(PLAYER_PED_ID())
										
										//vPedCoord = GET_ENTITY_COORDS(HUD.pedScanList[i].pedID)
										vPedCoord = GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[i].pedID, 0)
										if IS_ENTITY_ON_SCREEN(HUD.pedScanList[i].pedID)
											
																			
											xpos=0
											ypos=0
											fPedRange = GET_DISTANCE_BETWEEN_COORDS(vPedCoord,GET_ENTITY_COORDS(HUD.camEntity))
											fBoxScaler = GET_BOX_SIZE(HUD,HUD.pedScanList[i].pedID,HUD.camEntity)
											fBoxDrawSize = HUD.fBoxSize * fBoxScaler
											
											if IS_BIT_SET(HUD.pedScanList[i].BITPedData,2) or i = HUD.nearestPed //BIT = always show ped
												
												if IS_BIT_SET(HUD.pedScanList[i].BITPedData,0) //scanned = true
													if TIMERA() - HUD.pedScanList[i].lastSeenTime < 500
													#if enable_enhanced_hud_features
													and not IS_VIEW_BLOCKED_BY_SPHERES(HUD,HUD.pedScanList[i].pedID)
													and not IS_POINT_BLOCKED_BY_QUAD(GET_ENTITY_COORDS(HUD.pedScanList[i].pedID))
													#endif
														
														draw_ped_outline(hud,vPedCoord,fBoxScaler,HUD.pedScanList[i].relType)															
														HUD.pedScanList[i].vLostLocation = GET_ENTITY_COORDS(HUD.pedScanList[i].pedID)
													ELSE
														getHUDBoxColour(hud,HUD.pedScanList[i].relType,targetR,targetG,targetB)
														SET_DRAW_ORIGIN(HUD.pedScanList[i].vLostLocation)
														DRAW_SPRITE("helicopterhud", "TargetLost",xpos,ypos,fBoxDrawSize,fBoxDrawSize*2,0, targetR,targetG,targetB, 200,TRUE)
														CLEAR_DRAW_ORIGIN()
//														SET_TEXT_FOR_HUD(targetR,targetG,targetB,0.5,true)
														SET_TEXT_FOR_HUD(targetR,targetG,targetB,0.5,true)
														/*
														#if enable_enhanced_hud_features
														IF NOT HUD.bBlockTargetLost
														#endif
															DISPLAY_TEXT(0.5,0.68,"HUD_LST")
														#if enable_enhanced_hud_features
														ENDIF
														#endif */
														HUD.targetIsLost = true
													ENDIF
												ELSE
													getHUDBoxColour(hud,HUD_UNKNOWN,targetR,targetG,targetB)
													IF i = HUD.nearestPed
														draw_ped_outline(hud,vPedCoord,fBoxScaler,HUD.pedScanList[i].relType,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b)//,TRUE)
														
													ELSE
														draw_ped_outline(hud,vPedCoord,fBoxScaler,HUD.pedScanList[i].relType,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b)
													ENDIF
												ENDIF
												
												if IS_BIT_SET(HUD.pedScanList[i].BITPedData,7)
													draw_ped_outline(hud,vPedCoord,fBoxScaler,HUD.pedScanList[i].relType,227,24,234)
													//SET_DRAW_ORIGIN(vPedCoord)
													//DRAW_SPRITE("helicopterhud", "hud_outline",xpos,ypos,fBoxDrawSize*1.1,(fBoxDrawSize*2)*1.1,0, 227,24,234, 200)
													//CLEAR_DRAW_ORIGIN()	
												ENDIF												
											ENDIF
											
											int iHit
											iHit = 0
											vector vTemp1
											vTemp1 = <<0,0,0>>
											ENTITY_INDEX hitEntity
											hitEntity = NULL
											SWITCH GET_SHAPE_TEST_RESULT(HUD.pedScanList[i].shapeTest,iHit,vTemp1,vTemp1,hitEntity)
												CASE SHAPETEST_STATUS_NONEXISTENT //create a new shape test
													vector v1,v2
													v1 = GET_ENTITY_COORDS(hud.vehChopper)
													v2 = GET_ENTITY_COORDS(HUD.pedScanList[i].pedID) + <<0,0,0.5>>
													HUD.pedScanList[i].shapeTest = START_SHAPE_TEST_LOS_PROBE(v1,v2,SCRIPT_INCLUDE_MOVER)
												BREAK
												
												CASE SHAPETEST_STATUS_RESULTS_READY
													IF iHit = 0 //nothing is blocking shapetest.
														HUD.pedScanList[i].lastSeenTime = TIMERA()
														//HUD.pedScanList[i].vLostLocation = GET_ENTITY_COORDS(HUD.pedScanList[i].pedID)
													ENDIF
												BREAK
											ENDSWITCH						
																									
											if TIMERA() - HUD.pedScanList[i].lastSeenTime < 1500 or IS_BIT_SET( HUD.pedScanList[i].BITPedData,0)  //scanned = true																																																						
												if HUD.fSightRangeMultiplier2/(HUD.fFOV*fPedRange) > 1.0																																													
													GET_SCREEN_COORD_FROM_WORLD_COORD(vPedCoord,xpos,ypos)
													screenRange = SQRT(((xPos-0.5)*(xPos-0.5))+((yPos-0.5)*(yPos-0.5)))
													//save_debug("screen range:",-1,screenRange)
													//save_debug("closestRange:",-1,closestRange)
													if screenRange < closestRange
														
														nearX = xPos
														nearY = yPos
														nearX =nearX
														nearY = nearY
														closestRange = screenRange
														newNearestPed = i
														//save_debug("new near ped:",i)
													ENDIF
												ENDIF																	
											ENDIF
											CLEAR_DRAW_ORIGIN()

										ENDIF
									ENDIF							
								ENDIF
							ENDFOR
						ENDIF
							
						HUD.iPedGivenLOScheck++
						if HUD.iPedGivenLOScheck > HUD.iCurrentPedScanCount-1
							HUD.iPedGivenLOScheck = 0
						ENDIF
					ENDIF	

					if HUD.nearestPed != newNearestPed
						IF HUD.soundID_scan != -1	
							IF HAS_SOUND_FINISHED(HUD.soundID_scan)
								PLAY_SOUND_FRONTEND(HUD.soundID_scan,"COP_HELI_CAM_BLEEP")
							ENDIF
						ENDIF
					ENDIF
					HUD.nearestPed = newNearestPed		
					HUD.scanning = FALSE
					
					
					if HUD.nearestPed!= -1 and HUD.canScan = true
						//if ped hasn't already been scanned
						
						if not is_bit_set(HUD.pedScanList[HUD.nearestPed].BITPedData,0) //scanned = FALSE
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RT)
								//is ped within scan range
								if HUD.fSightRangeMultiplier/(HUD.fFOV*fPedRange) > 0.5
								
								#if ENABLE_ENHANCED_HUD_FEATURES
								AND NOT IS_POINT_BLOCKED_BY_QUAD(GET_ENTITY_COORDS(HUD.pedScanList[HUD.nearestPed].pedID))
								AND NOT IS_VIEW_BLOCKED_BY_SPHERES(HUD,HUD.pedScanList[HUD.nearestPed].pedID)
								#endif
									IF HUD.rangeSoundPlaying = TRUE
										IF HUD.soundID_scanRange != -1
											IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanRange)
												STOP_SOUND(HUD.soundID_scanRange)
											ENDIF
										ENDIF
										HUD.rangeSoundPlaying = FALSE
									ENDIF
								
									HUD.scanning = TRUE
									//has scan completed?
									if HUD.pedScanList[HUD.nearestPed].fScanTime < 1.0
										IF HUD.soundID_scanloop != -1
											IF HAS_SOUND_FINISHED(HUD.soundID_scanloop)
												PLAY_SOUND_FRONTEND(HUD.soundID_scanloop,"COP_HELI_CAM_SCAN_PED_LOOP")
											ENDIF
										ENDIF
							
									
										closestRange = ABSF(1.0 - closestRange)
										HUD.pedScanList[HUD.nearestPed].fScanTime += (closestRange * TIMESTEP()) /3.5
										
										fBoxScaler = GET_BOX_SIZE(HUD,HUD.pedScanList[HUD.nearestPed].pedID,HUD.camEntity)
										fBoxDrawSize = HUD.fBoxSize * fBoxScaler									
										SET_TEXT_FOR_HUD(255,0,0,0.5,true)
										
										//-(fBoxDrawSize/2)+(fBoxDrawSize*0.04),ypos+fBoxDrawSize+0.005
										
									
										IF TIMERA()%600<300
											//DISPLAY_TEXT(nearX-(HUD.fBoxSize/2.0),nearY+(HUD.fBoxSize/2.0)+HUD.fTextY,"scan")
											DISPLAY_TEXT(0.5,0.68,"scan",1)
										//	DISPLAY_TEXT_WITH_LITERAL_STRING(nearX-(HUD.fBoxSize/2.0),nearY+(HUD.fBoxSize/2.0)+HUD.fTextY,"STRING","SCANNING...")
										ENDIF
										
										fBoxScaler = GET_BOX_SIZE(HUD,HUD.pedScanList[HUD.nearestPed].pedID,HUD.camEntity)
										fBoxDrawSize = 0.03//(HUD.fBoxSize * fBoxScaler)-0.01
										
											SET_DRAW_ORIGIN(GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[HUD.nearestPed].pedID, 0))
										draw_scan_ped(HUD,HUD.pedScanList[HUD.nearestPed].pedID,HUD.pedScanList[HUD.nearestPed].fScanTime,fBoxScaler)
										
										CLEAR_DRAW_ORIGIN()
									ELSE
										
										if IS_BIT_SET(HUD.pedScanList[HUD.nearestPed].BITPedData,3) //bAlwaysShowOnceScanned = true								
											SET_BIT(HUD.pedScanList[HUD.nearestPed].BITPedData,2) //bAlwaysShow = TRUE								
										ENDIF
										SET_BIT(HUD.pedScanList[HUD.nearestPed].BITPedData,0) //scanned = TRUE
										HUD.lastScannedPed=HUD.pedScanList[HUD.nearestPed].pedID

										//sounds
										IF HUD.soundID_scanloop != -1
											IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanloop)
												STOP_SOUND(HUD.soundID_scanloop)
											ENDIF
										ENDIF
										IF HUD.pedScanList[HUD.nearestPed].relType = HUD_FOE
											IF HUD.soundID_scangood != -1
												IF HAS_SOUND_FINISHED(HUD.soundID_scangood)
													PLAY_SOUND_FRONTEND(HUD.soundID_scangood,"COP_HELI_CAM_SCAN_PED_SUCCESS")
												ENDIF
											ENDIF
										ELSE
											IF HUD.soundID_scanbad != -1
												IF HAS_SOUND_FINISHED(HUD.soundID_scanbad)
													PLAY_SOUND_FRONTEND(HUD.soundID_scanbad,"COP_HELI_CAM_SCAN_PED_FAILURE")
												ENDIF
											ENDIF
										ENDIF
										
										
										
									ENDIF
								ELSE
									fBoxScaler = GET_BOX_SIZE(HUD,HUD.pedScanList[HUD.nearestPed].pedID,HUD.camEntity)
									fBoxDrawSize = HUD.fBoxSize * fBoxScaler									
									SET_TEXT_FOR_HUD(255,0,0,0.5,true)
									DISPLAY_TEXT(0.5,0.68,"HUD_RNG")
									IF NOT HUD.rangeSoundPlaying
										cprintln(debug_trevor3,"TRY BLEEP 1")
										IF HUD.soundID_scanRange != -1
											cprintln(debug_trevor3,"TRY BLEEP 2")
											IF HAS_SOUND_FINISHED(HUD.soundID_scanRange)
												cprintln(debug_trevor3,"BLEEP BLEEP")
												PLAY_SOUND_FRONTEND(HUD.soundID_scanRange,"COP_HELI_CAM_BLEEP_TOO_FAR")
												HUD.rangeSoundPlaying = TRUE
											ENDIF
											
											
										ENDIF

									ENDIF
								ENDIF
							ELSE
								IF HUD.rangeSoundPlaying = TRUE
									IF HUD.soundID_scanRange != -1
										IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanRange)
											STOP_SOUND(HUD.soundID_scanRange)
										ENDIF
									ENDIF
									HUD.rangeSoundPlaying = FALSE
								ENDIF
							ENDIF
						ELSE	
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RT)
							OR (GET_GAME_TIMER() < HUD.scanButtonPressedRemovalTimer AND HUD.lastIdentifiedPed = HUD.nearestPed)
								if not IS_ENTITY_DEAD(HUD.pedScanList[HUD.nearestPed].pedID)
								
									if (TIMERA() - HUD.pedScanList[HUD.nearestPed].lastSeenTime < 500)
										IF NOT HUD.targetIsLost
											HUD.lastIdentifiedPed = HUD.nearestPed
											HUD.scanButtonPressedRemovalTimer = GET_GAME_TIMER() + 3000
																														
											SET_DRAW_ORIGIN(GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[HUD.nearestPed].pedID, 0))
											fBoxScaler = GET_BOX_SIZE(HUD,HUD.pedScanList[HUD.nearestPed].pedID,HUD.camEntity)
											//GET_SCREEN_COORD_FROM_WORLD_COORD(GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[HUD.nearestPed].pedID, 0),xpos,ypos)
											xpos=0
											ypos=0
											//GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(HUD.pedScanList[HUD.nearestPed].pedID),xpos,ypos)
											getHUDBoxColour(HUD,HUD.pedScanList[HUD.nearestPed].relType,targetR,targetG,targetB)
											CLEAR_DRAW_ORIGIN()
											IF HUD.pedScanList[HUD.nearestPed].relType = 2
												DISPLAY_SMALL_PED_DATA(HUD,HUD.nearestPed,xpos-(fBoxDrawSize/2)+(fBoxDrawSize*0.04),ypos+fBoxDrawSize+0.005,targetR,targetG,targetB,fBoxScaler,TRUE)
											ELSE
												DISPLAY_SMALL_PED_DATA(HUD,HUD.nearestPed,xpos-(fBoxDrawSize/2)+(fBoxDrawSize*0.04),ypos+fBoxDrawSize+0.005,targetR,targetG,targetB,fBoxScaler)
											ENDIF
										endif
									endif
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					if HUD.scanning = FALSE		
						IF HUD.soundID_scanloop != -1
							IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanloop)
								STOP_SOUND(HUD.soundID_scanloop)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				if HUD.helihudMarkerCount>0
					for i = 0 to MAX_HELIHUD_COORD_COUNT-1
						if HUD.helihudMarker[i].entry != -1
							
							if GET_SCREEN_COORD_FROM_WORLD_COORD(HUD.helihudMarker[i].coord,xpos,ypos)
							
								vector camCoord
								IF DOES_ENTITY_EXIST(HUD.vehChopper) AND IS_VEHICLE_DRIVEABLE(HUD.vehChopper)
									camCoord = GET_ENTITY_COORDS(hud.vehChopper)
								ELif DOES_ENTITY_EXIST(hud.camEntity) and not IS_ENTITY_DEAD(hud.camEntity)
									camCoord = GET_ENTITY_COORDS(hud.camEntity)									
								ENDIF																									
														
								SET_DRAW_ORIGIN(HUD.helihudMarker[i].coord)
								xpos = 0.0
								ypos = 0.0
								if HUD.helihudMarker[i].markerType = MARKER_BOX //for fake ped box at long range
									fPedRange = GET_DISTANCE_BETWEEN_COORDS(HUD.helihudMarker[i].coord,camCoord)
									
									fBoxScaler = HUD.fRangeMultiplier / (HUD.fFOV * fPedRange) 
									if fBoxScaler < 0.4
										fBoxScaler = 0.4
									ENDIF
									if fBoxScaler > 2.0
										fBoxScaler = 2.0
									ENDIF	
																				
									draw_ped_outline(hud,HUD.helihudMarker[i].coord,fBoxScaler,0)
									SET_DRAW_ORIGIN(HUD.helihudMarker[i].coord)
								//	DRAW_SPRITE("helicopterhud", "hud_outline", xpos, ypos, 0.022,0.042,0,HUD.helihudMarker[i].r,HUD.helihudMarker[i].g,HUD.helihudMarker[i].b,255)
								
								//	DRAW_RANGE_TEXT(HUD.fTextRangeScale,vChopper,HUD.helihudMarker[i].coord,xpos-0.011,ypos+0.005,HUD.helihudMarker[i].r,HUD.helihudMarker[i].g,HUD.helihudMarker[i].b)
								ELSE
									DRAW_SPRITE("helicopterhud", "hud_dest", xpos, ypos, 0.042,0.042,0, HUD.helihudMarker[i].r,HUD.helihudMarker[i].g,HUD.helihudMarker[i].b,255,TRUE)
								//	DRAW_RANGE_TEXT(HUD.fTextRangeScale,vChopper,HUD.helihudMarker[i].coord,xpos,ypos,HUD.helihudMarker[i].r,HUD.helihudMarker[i].g,HUD.helihudMarker[i].b)
								ENDIF
								CLEAR_DRAW_ORIGIN()
								//draw numerical range on screen

							ELSE
								if HUD.helihudMarker[i].markerType = MARKER_BOX
									DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,HUD.helihudMarker[i].coord,HUD.helihudMarker[i].r,HUD.helihudMarker[i].g,HUD.helihudMarker[i].b)								
								ELSE
									DRAW_OFFSCREEN_ARROW_FOR_COORD(HUD,HUD.helihudMarker[i].coord,HUD.helihudMarker[i].r,HUD.helihudMarker[i].g,HUD.helihudMarker[i].b)
								ENDIF
							ENDIF
							
						ENDIF
					ENDFOR
				ENDIF
				
				if onlyshowhudmarkers = FALSE
					float fDrawAng = HUD.vCamRot.z
					
					IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
					OR DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
						fDrawAng = HUD.vAttachedCamRotationOffset.z
					ENDIF
					
					vector vPos = <<0.0, 0.0 ,0.0>>
					FLOAT fArrowPos, fHeliHeading, fBarMinH, fBarMaxH, fBarCamH
					
					if DOES_ENTITY_EXIST(hud.camEntity)
						if not IS_ENTITY_DEAD(hud.camEntity)
							vPos = GET_ENTITY_COORDS(hud.camEntity) 	
							fHeliHeading = GET_ENTITY_HEADING(hud.camEntity) 
						ENDIF
					ENDIF
					
					IF g_iShouldLaunchTruckTurret != -1
						IF g_iShouldLaunchTruckTurret = 2
						OR g_iShouldLaunchTruckTurret = 3
							fHeliHeading = fHeliHeading -174.4552
						ENDIF
					ENDIF

					//Make camera more than 0
					WHILE fDrawAng < 0.0 		fDrawAng += 360.0 		ENDWHILE
					WHILE fDrawAng > 360.0 		fDrawAng -= 360.0 		ENDWHILE
					
					//Make Heli heading more than 0
					WHILE fHeliHeading < 0.0 	fHeliHeading += 360.0	ENDWHILE
					WHILE fHeliHeading > 360.0 	fHeliHeading -= 360.0 	ENDWHILE
					
					//The heading is = to the cam heading
					fBarCamH = fDrawAng
					
					IF g_iShouldLaunchTruckTurret != -1
						//Get the min heading !! THIS IS ACTUALLY MAX, DON'T BLAME ME - AMEC
						fBarMinH = fHeliHeading - ABSF(fMaxHeadingOverride)
						//get the max heading !! THIS IS ACTUALLY MIN, DON'T BLAME ME - AMEC
						fBarMaxH = fHeliHeading + ABSF(fMinHeadingOverride)
					
					ELIF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(HUD.vehChopper)
					OR DOES_INTERIOR_USE_FLYING_CAM_TURRETS()
						
						//Get the min heading 
						fBarMinH = -(HUD.fMaxCameraYaw*0.5)
						//get the max heading
						fBarMaxH = (HUD.fMaxCameraYaw*0.5)
					
					ELSE
						//Get the min heading 
						fBarMinH = fHeliHeading - HUD.fMaxCameraYaw
						//get the max heading
						fBarMaxH = fHeliHeading + HUD.fMaxCameraYaw
						
					ENDIF
					
					//If its a radian out then move it
					IF fBarCamH < fBarMinH
					AND (fBarCamH + 360.0) <= fBarMaxH
						fBarCamH += 360.0
					ENDIF
					IF fBarCamH > fBarMaxH 
					AND (fBarCamH - 360.0) >= fBarMinH
						fBarCamH -= 360.0
					ENDIF
					
					//Get the current heading as a ratio between these and make it a %
					fArrowPos = (fBarCamH - fBarMinH)/(fBarMaxH - fBarMinH)
					
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "### SUMMARY ###")		
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fHeliHeading	   = ", fHeliHeading)		
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fBarMinH          = ", fBarMinH)			
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fBarMaxH          = ", fBarMaxH)			
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fBarCamH          = ", fBarCamH)			
//					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos         = ", fArrowPos)			
					
					//Cap it
					IF fArrowPos = 2.0 
						fArrowPos = 0.0
						//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.0 x")
					ELIF fArrowPos = -1.0 
						fArrowPos = 1.0
						//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 1.0 x")
					ELIF fArrowPos < 0.0 
					AND fArrowPos >= -0.5 
						//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.0")
						fArrowPos = 0.0
					ELIF fArrowPos < 0.0 
					AND fArrowPos > -1.0 
						//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 1.0 y")
						fArrowPos = 1.0
					ELIF fArrowPos > 1.5
					AND fArrowPos < 2.0 
						fArrowPos = 0.0
						//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.0 y")
					ELIF fArrowPos > 2.0
					OR fArrowPos < -1.0
						fArrowPos = 0.5
						//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.5")
					ENDIF
										
					BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_ALT_FOV_HEADING")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vPos.z)
					IF NETWORK_IS_GAME_IN_PROGRESS()
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fArrowPos)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((hud.fFOV - hud.minFOV) / (HUD.maxFOV - hud.minFOV))
					ENDIF
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fDrawAng)
					END_SCALEFORM_MOVIE_METHOD()
					
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
					
					IF NOT HUD.bDisplayMinimap
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
				ENDIF
		
			ENDIF
	
	ENDIF

ENDPROC	

#if ENABLE_ENHANCED_HUD_FEATURES
PROC FIX_CHOPPER_HUD_TIME(structHelicopterHUDVars &HUD,int iHoursMin, int iHoursMax)
	HUD.minChopperHours = iHoursMin
	HUD.maxChopperHours = iHoursMax

ENDPROC
#endif
FUNC STRING GET_SCALEFORM_MOVIE_TURRET_HUD()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
	OR g_iInteriorTurretSeat != -1
		RETURN 	"turret_cam"
	ENDIF
		
	
		
	RETURN "heli_cam"
	
ENDFUNC

FUNC BOOL SET_CHOPPER_HUD_ACTIVE(structHelicopterHUDVars &HUD,entity_INDEX vChopper, bool setActive, VEHICLE_INDEX vHeli, bool rememberData=FALSE)
	vector vCoords, vrot
	#if ENABLE_ENHANCED_HUD_FEATURES
	hud.lastpos = <<0,0,0>>
	#ENDIF
	if setActive = TRUE and HUD.bHUDIsActive = FALSE
		IF HUD.onlyshowhudmarkers=FALSE
		
			IF g_iShouldLaunchTruckTurret != -1
				mov = REQUEST_SCALEFORM_MOVIE(GET_SCALEFORM_MOVIE_TURRET_HUD())
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_CHOPPER_HUD_ACTIVE: Using turret specific UI: turret_cam.")
			ELSE
		
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_CHOPPER_HUD_ACTIVE: Using heli specific UI: heli_cam.")
			mov = REQUEST_SCALEFORM_MOVIE(GET_SCALEFORM_MOVIE_TURRET_HUD())
			
			ENDIF
			
		ENDIF				
		REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")	
		#IF IS_DEBUG_BUILD
			HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(true)
		#ENDIF
		IF NOT DOES_CAM_EXIST(HUD.camChopper)
			HUD.bIsCamAttached = FALSE
			HUD.camChopper = CREATE_CAM("default_scripted_camera",FALSE)
			
			IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(vHeli)
			OR DOES_INTERIOR_USE_TURRETS()
				HUD.nearClip = 0.1
			ENDIF
			SET_CAM_NEAR_CLIP(HUD.camChopper,HUD.nearClip)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_CHOPPER_HUD_ACTIVE: CREATE_CAM: default_scripted_camera")
			//SET_CAM_CUSTOM_MAX_NEAR_CLIP(HUD.camChopper,HUD.maxNearClip)
		ENDIF
		#if ENABLE_ENHANCED_HUD_FEATURES
			HUD.moveByCamera = true
		#ENDIF
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_HELICAM)
		SET_MULTIHEAD_SAFE(TRUE,TRUE)
		
		if not IS_ENTITY_DEAD(vChopper)		
			if not IS_ENTITY_DEAD(PLAYER_PED_ID())				
		//		if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vChopper)
				if IS_ENTITY_A_VEHICLE(vChopper)
					hud.vehchopper = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vChopper)
					#if ENABLE_ENHANCED_HUD_FEATURES
					IF NOT HUD.isMultiplayerMode
						SET_ENTITY_HAS_GRAVITY(hud.vehChopper,FALSE)
					ENDIF
					#endif
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(300)
				ENDIF
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(300)
					HUD.bMoveChopper = TRUE
					HUD.bActivateChopperCam = TRUE
					HUD.bDisplayHUD = TRUE
					HUD.canScan = true
					HUD.camEntity = vChopper
					vCoords = GET_ENTITY_COORDS(vChopper)
					HUD.fChopperHeight = vCoords.z
					vrot = GET_ENTITY_ROTATION(vChopper)

					hud.chopperHeading = vRot.z
					IF HUD.isMultiplayerMode
					//	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE|SPC_PREVENT_EVERYBODY_BACKOFF)
						DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)						
					ELSE
						DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
						//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE) 
						SET_EVERYONE_IGNORE_PLAYER(player_id(),TRUE) 
					ENDIF
					

					
				
					#if ENABLE_ENHANCED_HUD_FEATURES
						vector vVel = GET_ENTITY_VELOCITY(vChopper)
					
						hud.lastVx = vVel.x
						hud.lastVy = vVel.y
					#ENDIF	
					PUSH_TIMECYCLE_MODIFIER()
					
					
					
					#if USE_TU_CHANGES
						MODEL_NAMES eHeliModel
						IF IS_VEHICLE_DRIVEABLE(HUD.vehChopper)
							eHeliModel = GET_ENTITY_MODEL(HUD.vehChopper)
						ENDIF
						IF eHeliModel = BUZZARD
						OR eHeliModel = SAVAGE
							SET_TIMECYCLE_MODIFIER("heliGunCam")
						ELIF eHeliModel = VALKYRIE
						OR eHeliModel = HUNTER
							SET_TIMECYCLE_MODIFIER("heliGunCam")
						ELSE
							IF g_iShouldLaunchTruckTurret != -1
								CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_CHOPPER_HUD_ACTIVE: Delay timecycle modifiers.")
							ELSE
					
							SET_TIMECYCLE_MODIFIER("eyeinthesky")
							
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_CHOPPER_HUD_ACTIVE: Set timecycle modifier: eyeinthesky")
							ENDIF
						ENDIF
					#endif
					
					#IF NOT USE_TU_CHANGES
						IF HUD.isMultiplayerMode
							SET_TIMECYCLE_MODIFIER("MP_heli_cam")
						ELSE
							cprintln(debug_trevor3,"Set modifier 1")
							SET_TIMECYCLE_MODIFIER("eyeinthesky")
						ENDIF
					#endif
					
					PRINTLN("PUSH_TIMECYCLE_MODIFIER()")
					CASCADE_SHADOWS_SET_AIRCRAFT_MODE(TRUE)
					float zHeight					
					GET_GROUND_Z_FOR_3D_COORD(vCoords,zHeight)
					HUD.GroundHeightAccum = zHeight*10					
					
					IF HUD.bAudioLoaded = TRUE		
						IF HUD.soundID_background != -1
						
							IF g_iShouldLaunchTruckTurret = -1
						
							IF HAS_SOUND_FINISHED(HUD.soundID_background)
								PLAY_SOUND_FRONTEND(HUD.soundID_background,"COP_HELI_CAM_BACKGROUND") 
							ENDIF
							
							ENDIF
							
						ENDIF
					ENDIF
					controlAudio(HUD)
				
					
					
					HUD.bHUDIsActive = true

			ENDIF
		ENDIF
		//SCRIPT_ASSERT("SET_CHOPPER_HUD_ACTIVE(): Vehicle doesn't exist.")						
		return FALSE
	ELSE
	
		if HUD.bHUDIsActive = true and setActive = true
			if HUD.bTexturesLoaded = FALSE
				displayChopperHud(HUD)
				DISABLE_PLAYER_PED_STAT_UPDATES_THIS_FRAME()
			Else
				return true
			ENDIF
		ENDIF
			
		
		if HUD.bHUDIsActive = true and setActive = FALSE
			//SAVE_STRING_TO_DEBUG_FILE("turn off")
			
			if rememberData = FALSE
				HUD.iCurrentPedScanCount = 0
			ENDIF
			HUD.bMoveChopper = FALSE
			HUD.bActivateChopperCam = FALSE
			HUD.bDisplayHUD = FALSE
			HUD.camEntity = null
			HUD.canScan = FALSE
			HUD.lastScannedPed = null
			#if ENABLE_ENHANCED_HUD_FEATURES
			clearHoverInArea(hud)
			#ENDIF
			if not IS_ENTITY_DEAD(PLAYER_PED_ID())
				ENABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
				SET_EVERYONE_IGNORE_PLAYER(player_id(),FALSE) 
				//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			
			
			#IF IS_DEBUG_BUILD
				//DELETE_WIDGET_GROUP(HUD.CopChopperWidget)
			#ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(hud.vehChopper)
				IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(hud.vehChopper)
					DETACH_ENTITY(hud.vehChopper)
				ENDIF
				IF NOT HUD.isMultiplayerMode
					FREEZE_ENTITY_POSITION(hud.vehChopper,FALSE)										
					SET_ENTITY_INVINCIBLE(hud.vehChopper,FALSE)		
					SET_ENTITY_HAS_GRAVITY(hud.vehChopper,TRUE)
					SET_VEHICLE_GRAVITY(hud.vehChopper,TRUE)
				ENDIF
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			
			IF DOES_CAM_EXIST(HUD.camChopper)
				if IS_CAM_ACTIVE(HUD.camChopper)
					SET_CAM_ACTIVE(HUD.camChopper,FALSE)					
				ENDIF
				if IS_CAM_RENDERING(HUD.camChopper)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
				ENDIF
				DESTROY_CAM(HUD.camChopper)
			ENDIF
			
			
			IF HUD.soundID_zoom != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_zoom)
					STOP_SOUND(HUD.soundID_zoom)
				ENDIF
			ENDIF
			
			IF HUD.soundID_turn != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_turn)
					STOP_SOUND(HUD.soundID_turn)
				ENDIF
			ENDIF
			
			IF HUD.soundID_scan != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_scan)
					STOP_SOUND(HUD.soundID_scan)
				ENDIF
			ENDIF
			
			IF HUD.soundID_scanloop != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanloop)
					STOP_SOUND(HUD.soundID_scanloop)
				ENDIF
			ENDIF
			
			IF HUD.soundID_scangood != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_scangood)
					STOP_SOUND(HUD.soundID_scangood)
				ENDIF
			ENDIF
			
			IF HUD.soundID_scanbad != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanbad)
					STOP_SOUND(HUD.soundID_scanbad)
				ENDIF
			ENDIF
			
			IF HUD.soundID_background != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_background)
					STOP_SOUND(HUD.soundID_background)
				ENDIF
			ENDIF
			
			IF HUD.soundID_scanRange != -1
				IF NOT HAS_SOUND_FINISHED(HUD.soundID_scanRange)
					STOP_SOUND(HUD.soundID_scanRange)
				ENDIF
			ENDIF
		
			
			IF HUD.bAudioLoaded = TRUE
				RELEASE_AMBIENT_AUDIO_BANK()	
				
				RELEASE_SOUND_ID(HUD.soundID_background)
				RELEASE_SOUND_ID(HUD.soundID_zoom)
				RELEASE_SOUND_ID(HUD.soundID_turn)
				RELEASE_SOUND_ID(HUD.soundID_scan)
				RELEASE_SOUND_ID(HUD.soundID_scanloop)
				RELEASE_SOUND_ID(HUD.soundID_scangood)
				RELEASE_SOUND_ID(HUD.soundID_scanbad)
				RELEASE_SOUND_ID(HUD.soundID_scanRange)
				HUD.soundID_background = -1
				HUD.soundID_zoom = -1
				HUD.soundID_turn = -1
				HUD.soundID_scan = -1
				HUD.soundID_scanloop = -1
				HUD.soundID_scangood = -1
				HUD.soundID_scanbad = -1
				HUD.soundID_scanRange = -1
				HUD.bAudioLoaded = FALSE
			ENDIF
		//	SAVE_STRING_TO_DEBUG_FILE("CLEAR_TIMECYCLE_MODIFIER()")
			CLEAR_TIMECYCLE_MODIFIER()
			cprintln(debug_trevor3,"clear modifier 1")
			CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterHUD")
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
			POP_TIMECYCLE_MODIFIER()
			SET_MULTIHEAD_SAFE(FALSE)
			#IF IS_DEBUG_BUILD
			HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
			#ENDIF
			#if ENABLE_ENHANCED_HUD_FEATURES
			hud.bSphereBlockersActive = FALSE
			int iR
			IF DOES_ENTITY_EXIST(hud.camEntity)
				REPEAT COUNT_OF(hud.sphereBlocker) iR
					hud.sphereBlocker[iR].inUse = FALSE
				ENDREPEAT
			ENDIF
			#endif
			hud.vehChopper = null
			HUD.bHUDIsActive = FALSE
			controlAudio(HUD)
			return TRUE
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

/*
PROC ControlCops(structHelicopterHUDVars &HUD)
	
	if HUD.bAllowPedOrders = TRUE
		
		//deselect ped
		if IS_BUTTON_JUST_PRESSED(PAD1,TRIANGLE)
			if HUD.iSelectedPed != -1
				HUD.iSelectedPed = -1				
			ENDIF
		ENDIF
		
		if IS_BUTTON_JUST_PRESSED(PAD1,CROSS)
			if HUD.iSelectedPed = -1
				if HUD.nearestPed != -1
					if HUD.pedScanList[HUD.nearestPed].relType = HUD_FRIEND
						HUD.iSelectedPed = HUD.nearestPed
					
					ENDIF
				ENDIF
			ELSE
				if HUD.iSelectedPed = HUD.nearestPed
					HUD.iSelectedPed =-1									
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
*/

func bool GET_LAST_SCANNED_PED(structHelicopterHUDVars &HUD, ped_index &returnedPed)
	if does_entity_exist(HUD.lastScannedPed)
		returnedPed = HUD.lastScannedPed
		HUD.lastScannedPed = null
		
		return true
	ELSE
		return FALSE
	ENDIF
ENDFUNC

func bool IS_PED_BEING_SCANNED(structHelicopterHUDVars &HUD, PED_INDEX checkPed)
	if HUD.scanning = true and HUD.nearestPed = GET_PED_ENTRY_IN_SCAN_LIST(HUD,checkPed)
		return true
	ENDIF
	return FALSE
ENDFUNC

func int GET_HELIHUD_SELECTED(structHelicopterHUDVars &HUD, bool isPedScanned, ped_index &selectedPed, int &selectedPedentry, vector &selectedCoord)
	
	if HUD.nearestPed != -1
		if (isPedScanned = true and IS_BIT_SET(HUD.pedScanList[HUD.nearestPed].bitPedData,0)) or (isPedScanned = FALSE)
			if HUD.pedScanList[HUD.nearestPed].relType = HUD_FRIEND
				selectedPed = HUD.pedScanList[HUD.nearestPed].pedID
				selectedPedentry = HUD.nearestPed
				return 1	
			ELSE
				if HUD.pedScanList[HUD.nearestPed].relType = HUD_BYSTANDER
					selectedPed = HUD.pedScanList[HUD.nearestPed].pedID
					return 3
				ELSE
					if HUD.pedScanList[HUD.nearestPed].relType = HUD_FOE
						selectedPed = HUD.pedScanList[HUD.nearestPed].pedID
						return 4
					ELSE
						if HUD.pedScanList[HUD.nearestPed].relType = HUD_UNKNOWN
							selectedPed = HUD.pedScanList[HUD.nearestPed].pedID
							return 5
						ELSE
							if not IS_ENTITY_DEAD(HUD.pedScanList[HUD.nearestPed].pedID)
//								vector vPedCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(HUD.pedScanList[HUD.nearestPed].pedID, 0)
							//	GET_ENTITY_COORDS(HUD.pedScanList[HUD.nearestPed].pedID)
								selectedCoord = hud.vaimat									
								return 2
							ELSE
								return 0
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			return 0
		ENDIF
	ELSE		
		if DOES_ENTITY_EXIST(selectedPed)
			if not IS_ENTITY_DEAD(selectedPed)
//				vector vPedCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(selectedPed,0)
				selectedCoord = hud.vaimat	
				HUD.selectedPed = null
				return 2
			ELSE
				selectedCoord = hud.vaimat
				return 0
			ENDIF
		ELSE
			selectedCoord = hud.vaimat
			return 0
		ENDIF	
	ENDIF
	return 0
ENDFUNC


func bool IS_HELIHUD_CAM_FOCUSING_PED(structHelicopterHUDVars &HUD,ped_index checkPed)
	if not IS_ENTITY_DEAD(checkPed) and not IS_ENTITY_DEAD(HUD.camEntity)
		IF IS_ENTITY_ON_SCREEN(checkPed)
			float fWidth = TAN(HUD.fFov/2.0) * GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HUD.camEntity),GET_WORLD_POSITION_OF_ENTITY_BONE(checkPed,0))
			if fWidth < HUD.fCheckHUDFocusRange
				return TRUE
			ENDIF						
		ENDIF
	ENDIF
	return FALSE
ENDFUNC



func bool GET_HELIHUD_SELECTED_PED(structHelicopterHUDVars &HUD, ped_index &selectedPed)
	if HUD.nearestPed =-1
		return FALSE		
	ELSE
		selectedPed = HUD.pedScanList[HUD.nearestPed].pedID
		RETURN TRUE
	ENDIF
ENDFUNC

func ped_index GET_OBSERVED_HELIHUD_PED(structHelicopterHUDVars &HUD)
	if HUD.nearestPed != -1
		IF HUD.fFov < 15
			RETURN HUD.pedScanList[HUD.nearestPed].pedID
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC


PROC ALLOW_HELIHUD_COP_ORDER(structHelicopterHUDVars &HUD,bool switchOrdersOn)
	if switchOrdersOn = TRUE
		HUD.bAllowPedOrders = true
	ELSE
		HUD.bAllowPedOrders = FALSE
	ENDIF
ENDPROC

PROC SET_HUD_TO_SHOW_FOR_HELI(structHelicopterHUDVars &HUD,bool enableMinimap)
	HUD.bDisplayMinimap = enableMinimap
ENDPROC

PROC RUN_HELICOPTER_HUD(structHelicopterHUDVars &HUD, bool onlyShowPedMarkers = FALSE,
						FLOAT fMinPitchOverride = MIN_CAMERA_PITCH, FLOAT fMaxPitchOverride = MAX_CAMERA_PITCH,
						FLOAT fMinHeadingOverride = MIN_CAMERA_HEADING, FLOAT fMaxHeadingOverride = MAX_CAMERA_HEADING,
						FLOAT fDeadzoneMagnitude = 0.15)
	
	if DOES_CAM_EXIST(HUD.camChopper)			
		//SET_CAM_CUSTOM_MAX_NEAR_CLIP(HUD.camChopper,HUD.maxNearClip)
	endif
	
	if DOES_ENTITY_EXIST(hud.vehchopper)
		hud.camEntity = CONVERT_TO_ENTITY_INDEX(hud.vehchopper)
	ENDIF
	
	if HUD.bHUDInitiated = FALSE				
		IF CHOPPER_INIT(HUD,onlyShowPedMarkers)	
			HUD.bHUDInitiated = TRUE
		ENDIF
	ELSE
		
		
		#IF IS_DEBUG_BUILD
			debugChopper(HUD)
			if bshowlist =  TRUE
				debugShowScanList(HUD)
			ENDIF
		#ENDIF
		
		IF onlyShowPedMarkers=FALSE
			GET_HUD_COORD(HUD) //this runs bevore controlchoppercamera and it finds the coord of the chopper camera before it is attached to the chopper!
			#if ENABLE_ENHANCED_HUD_FEATURES
		
			moveChopper(HUD)
			#ENDIF
			ControlChopperCamera(HUD, fMinPitchOverride, fMaxPitchOverride, fMinHeadingOverride, fMaxHeadingOverride, fDeadzoneMagnitude)
			ReviseHudMarkers(HUD)			
		ENDIF
		controlAudio(HUD)
		displayChopperHud(HUD, onlyShowPedMarkers, fMinHeadingOverride, fMaxHeadingOverride)
		DISABLE_PLAYER_PED_STAT_UPDATES_THIS_FRAME()
	ENDIF

ENDPROC


