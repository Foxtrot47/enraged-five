//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        arena_contestant_turret_public.sch															//
// Description: This header contains wrappers for the turret_manager system & turret_cam_script 			//
//				specifically for the arena contestant turrets.												//
//																											//
//				Sections:																					//
//				* HELPER				- Helper functions for MISSION-CONTROLLER section					//
//				* MISSION-CONTROLLER	- API specfically targeting the mission controller 					//
//				* EVENTS				- Functions for handling events										//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		17/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "turret_manager_public.sch"
USING "turret_manager_sup.sch"
USING "turret_cam_public.sch"
USING "net_include.sch"
USING "script_weapon_common.sch"
USING "turret_cam_sup.sch"
USING "net_drone.sch"
USING "website_public.sch"
USING "net_arenaannouncer_header.sch"
USING "arena_spectator_turret_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HELPER																									//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////    

PROC ARENA_CONTESTANT_TURRET_MAINTAIN_BLIP(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, INT iTurret, OBJECT_INDEX objTower, HUD_COLOURS eColour)
	IF NOT DOES_ENTITY_EXIST(objTower)
	OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		IF DOES_BLIP_EXIST(ref_data.blips[iTurret])
			REMOVE_BLIP(ref_data.blips[iTurret])
		ENDIF
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(ref_data.blips[iTurret])	
		ref_data.blips[iTurret] = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(objTower))
		SET_BLIP_SPRITE(ref_data.blips[iTurret], RADAR_TRACE_ARENA_TURRET)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(ref_data.blips[iTurret], TRUE)
		SET_BLIP_DISPLAY(ref_data.blips[iTurret], DISPLAY_BOTH)
		SET_BLIP_SCALE(ref_data.blips[iTurret], 1.0)	
		SET_BLIP_NAME_FROM_TEXT_FILE(ref_data.blips[iTurret], "ACT_NAME")
		BLIP_PRIORITY ePriority = GET_CORRECT_BLIP_PRIORITY(BP_OTHER_TEAM) - INT_TO_ENUM(BLIP_PRIORITY, 1)
		SET_BLIP_PRIORITY(ref_data.blips[iTurret], ePriority)
	ENDIF
	
	VECTOR v1, v2
	v1 = GET_ENTITY_COORDS(objTower)
	v1.z = 0
	v2 = GET_ENTITY_COORDS(PLAYER_PED_ID())
	v2.z = 0

	IF VDIST2(v1, v2) <= cf_ARENA_CT_SHOW_BLIP_HIGHT_RADIUS2
		SHOW_HEIGHT_ON_BLIP(ref_data.blips[iTurret], TRUE)	
	ELSE
		SHOW_HEIGHT_ON_BLIP(ref_data.blips[iTurret], FALSE)	
	ENDIF
	
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(ref_data.blips[iTurret], eColour)
ENDPROC

PROC ARENA_CONTESTANT_TURRET_REMOVE_BLIP(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, INT iTurret)
	IF DOES_BLIP_EXIST(ref_data.blips[iTurret])
		REMOVE_BLIP(ref_data.blips[iTurret])
	ENDIF
ENDPROC

PROC ARENA_CONTESTANT_TURRET_REMOVE_ALL_BLIPS(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data)
	INT i
	REPEAT ci_ARENA_CONTESTANT_TURRETS_MAX i
		IF DOES_BLIP_EXIST(ref_data.blips[i])
			REMOVE_BLIP(ref_data.blips[i])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Call at the end of a game to clean up, or to kick the player 
///    out of a contestant turret early.
PROC ARENA_CONTESTANT_TURRET_CLEANUP(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, BOOL bFinalCleanup = TRUE)
	DEBUG_PRINTCALLSTACK()
	CDEBUG1LN(DEBUG_NET_TURRET, "ARENA_CONTESTANT_TURRET_CLEANUP")
	
	IF NOT TURRET_MANAGER_IS_UNLOCKED()
		IF IS_PLAYER_USING_DRONE(PLAYER_ID())
			// Force out of drone missile without timecycle modifier
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- SET_DRONE_FORCE_EXTERNAL_CLEANING_UP ")
			SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
		ENDIF
		
		CLEAR_BIT(ref_data.iBsTurretIsFree, ref_data.iLocateStagger)
		CLEAR_BIT(ref_data.iBsTurretInUse, ref_data.iLocateStagger)
		
		TURRET_MANAGER_UNLOCK()	
	ENDIF

	DISABLE_VEHICLE_MINES(FALSE)
	MPGlobalsAmbience.bDisableMissionHintCam = FALSE
	
	ref_data.eRequestId = TLRI_NONE
	RELEASE_CONTEXT_INTENTION(ref_data.intention)
	
	IF IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_PLAYER_TELEPORT_ACTIVE()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
	ENDIF
	
	IF bFinalCleanup
		SET_AUDIO_FLAG("EnableMissileLockWarningForAllVehicles", FALSE)
		
		ARENA_CONTESTANT_TURRET_REMOVE_ALL_BLIPS(ref_data)
	ENDIF
ENDPROC

/// PURPOSE:
///    Wraps a ref_iTurretId + 1 between 0 -> number of contestant turrets.
PROC ARENA_CONTESTANT_TURRET_INCR_WRAP_ID(INT& ref_iTurretId)
	ref_iTurretId = (ref_iTurretId + 1) % g_arenaContestantTurretStack.iCount
ENDPROC

/// PURPOSE:
///    Returns the number of registered arena contestant turrets.
FUNC INT ARENA_CONTESTANT_TURRET_COUNT()
	RETURN g_arenaContestantTurretStack.iCount
ENDFUNC

/// PURPOSE: 
///    Return an INT bs which represents which modes should be available.
///    Use with ci_ARENA_CT_TYPE_
FUNC INT ARENA_CONTESTANT_TURRET_GENERATE_RAND_TYPES()
	// The weightings are relative and don't need to be normalised so
	// you don't need to update all existing weights if a new one is added.
	CONST_INT ci_NUM_RAND_MODES 4
	
	#IF IS_DEBUG_BUILD
	cf_ARENA_CT_WEIGHT_MG =	g_debug_turretMgWeight
	cf_ARENA_CT_WEIGHT_HM =	g_debug_turretHmWeight
	cf_ARENA_CT_WEIGHT_PM = g_debug_turretPmWeight
	cf_ARENA_CT_WEIGHT_MG_HM_PM = g_debug_turretMgHmPmWeight
	#ENDIF //IS_DEBUG_BUILD
	
	INT index
	INT iBsTypes[ci_NUM_RAND_MODES]
	FLOAT fWeight[ci_NUM_RAND_MODES]
	SET_BIT(iBsTypes[0], ci_ARENA_TURRET_TYPE_MG)
	fWeight[0] = cf_ARENA_CT_WEIGHT_MG
	
	SET_BIT(iBsTypes[1], ci_ARENA_TURRET_TYPE_HM)
	fWeight[1] = cf_ARENA_CT_WEIGHT_HM
		
	SET_BIT(iBsTypes[2], ci_ARENA_TURRET_TYPE_PM)
	fWeight[2] = cf_ARENA_CT_WEIGHT_PM
	
	SET_BIT(iBsTypes[3], ci_ARENA_TURRET_TYPE_MG)
	SET_BIT(iBsTypes[3], ci_ARENA_TURRET_TYPE_HM)
	SET_BIT(iBsTypes[3], ci_ARENA_TURRET_TYPE_PM)
	fWeight[3] = cf_ARENA_CT_WEIGHT_MG_HM_PM
	
	FLOAT fWeightSum
	REPEAT ci_NUM_RAND_MODES index
		fWeightSum += fWeight[index]
	ENDREPEAT
	
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(0.0, fWeightSum)
	CDEBUG1LN(DEBUG_NET_TURRET, " ARENA_CONTESTANT_TURRET_GENERATE_RAND_MODES fWeightSum = ", fWeightSum," fRand ", fRand)
	
	fWeightSum = 0
	REPEAT ci_NUM_RAND_MODES index
		fWeightSum += fWeight[index]
		IF fRand <= fWeightSum 
			BREAKLOOP
		ENDIF
	ENDREPEAT
	RETURN iBsTypes[index]
ENDFUNC

PROC ARENA_CONTESTANT_UPDATE_FIRE_POINTS_LOWER(INT iTurretId)
	TURRET_CAM_CONFIG_UPDATE_REMOVE_STATIC_FIRE_POINTS()
	INT iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Lower_01")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
	
	iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Lower_02")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
	
	iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Lower_03")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
	
	iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Lower_04")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
ENDPROC

PROC ARENA_CONTESTANT_UPDATE_FIRE_POINTS_UPPER(INT iTurretId)
	TURRET_CAM_CONFIG_UPDATE_REMOVE_STATIC_FIRE_POINTS()
	INT iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Upper_01")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)

	iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Upper_02")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
	
	iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Upper_03")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
	
	iBoneId = GET_ENTITY_BONE_INDEX_BY_NAME(g_arenaContestantTurretStack.entTurrets[iTurretId], "Launcher_Upper_04")
	TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(iBoneId)
ENDPROC

PROC ARENA_CONTESTANT_TURRET_CONFIG_MG()
	TURRET_CAM_CONFIG_WEAPON_FULL_AUTO(WEAPONTYPE_DLC_ARENA_MACHINE_GUN, 20, 100, DEFAULT, 0.6)
	TURRET_CAM_CONFIG_WEAPON_SOUNDS(TCWS_ARENA_CONTESTANT_MG_SOUNDS)
	TURRET_CAM_CONFIG_UPDATE_REMOVE_STATIC_FIRE_POINTS()
ENDPROC

PROC ARENA_CONTESTANT_TURRET_CONFIG_HOMING_MISSILE(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data)
	MODEL_NAMES eModelName = INT_TO_ENUM(MODEL_NAMES, HASH("w_ex_vehiclemissile_1"))
	TURRET_CAM_CONFIG_WEAPON_HOMING_MISSILE(WEAPONTYPE_DLC_ARENA_HOMING_MISSILE, eModelName) 
	TURRET_CAM_CONFIG_WEAPON_SOUNDS(TCWS_ARENA_CONTESTANT_HM_SOUNDS)
	ARENA_CONTESTANT_UPDATE_FIRE_POINTS_LOWER(ref_data.iLocateStagger)
ENDPROC

PROC ARENA_CONTESTANT_TURRET_CONFIG_PILOTED_MISSILE(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data)
	// Since we can only fire one at a time this is just weapon switch time. 
	TURRET_CAM_CONFIG_WEAPON_PILOTED_MISSILE(1000) 
	TURRET_CAM_CONFIG_WEAPON_SOUNDS(TCWS_ARENA_CONTESTANT_PM_SOUNDS)
	ARENA_CONTESTANT_UPDATE_FIRE_POINTS_UPPER(ref_data.iLocateStagger)
ENDPROC

/// PURPOSE:
///    Set up turret config based on ARENA_CONTESTANT_TURRET_CONTEXT.iCurrentWeaponType value.
PROC ARENA_CONTESTANT_TURRET_CONFIG_CURRENT_WEAPON(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data)
	// Arena Hud : Default icons to hidden
	ARENA_HUD_WEAPON_ICON eIconMg = AHWI_GREY
	ARENA_HUD_WEAPON_ICON eIconHm = AHWI_GREY
	ARENA_HUD_WEAPON_ICON eIconPm = AHWI_GREY
	
	// Arena Hud : Set icons transparent if available
	IF IS_BIT_SET(ref_data.iBsAvailableWeaponTypes, ci_ARENA_TURRET_TYPE_MG)
		eIconMg = AHWI_TRANSPARENT
	ENDIF
	IF IS_BIT_SET(ref_data.iBsAvailableWeaponTypes, ci_ARENA_TURRET_TYPE_HM)
		eIconHm = AHWI_TRANSPARENT
	ENDIF
	IF IS_BIT_SET(ref_data.iBsAvailableWeaponTypes, ci_ARENA_TURRET_TYPE_PM)
		eIconPm = AHWI_TRANSPARENT
	ENDIF	
		
	// Weapon : Enable selected weapon
	// Arena Hud : Set icon opaque if selected
	SWITCH ref_data.iCurrentWeaponType
		CASE ci_ARENA_TURRET_TYPE_MG 
			eIconMg = AHWI_OPAQUE
			ARENA_CONTESTANT_TURRET_CONFIG_MG()						
			BREAK
		CASE ci_ARENA_TURRET_TYPE_HM 
			eIconHm = AHWI_OPAQUE
			ARENA_CONTESTANT_TURRET_CONFIG_HOMING_MISSILE(ref_data)	
			BREAK
		CASE ci_ARENA_TURRET_TYPE_PM 
			eIconPm = AHWI_OPAQUE
			ARENA_CONTESTANT_TURRET_CONFIG_PILOTED_MISSILE(ref_data)	
			BREAK
	ENDSWITCH
	
	// Arena Hud : Update config
	TURRET_CAM_CONFIG_ARENA_HUD(TRUE, eIconMg, eIconHm, eIconPm)
ENDPROC

/// PURPOSE:
///    Called form ARENA_CONTESTANT_TURRET_UPDATE : handles the turret controls
///    while the player is using one. Pulled out into separate function
///    for clarity.
/// RETURNS:
///  	FALSE if the player quits out of the turret.
FUNC BOOL ARENA_CONTESTANT_TURRET_MAINTAIN_CONTROLS(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data)	
	// Disable controls while player is using the drone missile
	IF IS_LOCAL_PLAYER_USING_DRONE()
		ref_data.bFiredPilotedMissile = TRUE
		IF NOT IS_PAUSE_MENU_ACTIVE()	
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)				
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)	
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RS)	
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)	
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)	
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(ref_data.useTimer, ci_ARENA_CT_USE_DURATION_MS)
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Timeup : Kill")
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
			RETURN FALSE
		ENDIF		
	ELSE	
		IF ref_data.bFiredPilotedMissile
		OR HAS_NET_TIMER_EXPIRED(ref_data.useTimer, ci_ARENA_CT_USE_DURATION_MS)
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Timeup (/drone used ",ref_data.bFiredPilotedMissile," ) : Kill")
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PAUSE_MENU_ACTIVE()	
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
				CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT -----Input: Kill")
				ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
				RETURN FALSE
			ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
				// Find next available weapon type
				INT index = (ref_data.iCurrentWeaponType + 1) % ci_ARENA_TURRET_TYPE_COUNT
				WHILE index <> ref_data.iCurrentWeaponType
					IF IS_BIT_SET(ref_data.iBsAvailableWeaponTypes, index)
						CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT -----Input: Weapon cycle type ", ref_data.iCurrentWeaponType, " --> ", index)
						ref_data.iCurrentWeaponType = index
						
						// Update turret config to match new weapon type
						TURRET_CAM_CONFIG_UPDATE_START()
							ARENA_CONTESTANT_TURRET_CONFIG_CURRENT_WEAPON(ref_data)
						TURRET_CAM_CONFIG_UPDATE_STOP()
						PLAY_SOUND_FRONTEND(-1, "DPAD_WEAPON_SCROLL", "HUD_FRONTEND_DEFAULT_SOUNDSET") 
						BREAKLOOP
					ENDIF
					index = (index + 1) % ci_ARENA_TURRET_TYPE_COUNT
				ENDWHILE
			ENDIF
		ENDIF
	ENDIF	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Applies the turret_cam config for the arena contestant turrets.
/// PARAMS:
///    ref_eCamConfigId - Keeping a persistant copy of this will allow us to use a cached turret config if applicable.
///    out_args - Filled out with args for current locked arena contestant turret.
PROC ARENA_CONTETANT_TURRET_SET_CONFIG(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, TURRET_CAM_ARGS& out_args)
	INT iTurretIndex = TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID())
	
	out_args.vPos = <<0, 0, 16.0>>
	out_args.vRot = <<0, 0, g_arenaContestantTurretStack.fHeadings[iTurretIndex]>>
	out_args.entParent = g_arenaContestantTurretStack.entTurrets[iTurretIndex]
	out_args.eAttachType = TCAT_OBJ
		
	ref_data.iBsAvailableWeaponTypes = ARENA_CONTESTANT_TURRET_GENERATE_RAND_TYPES()
	ref_data.iCurrentWeaponType = GET_LOWEST_BIT_SET(ref_data.iBsAvailableWeaponTypes)
	
	TURRET_CAM_CONFIG_START()
		TURRET_CAM_CONFIG_CAM_LIMITS(180.0, -180.0, -70.0, 25.0, 30.0, 60.0)
		TURRET_CAM_CONFIG_SET_JOINT_Z(<<0.0, 8.6, 0.0>>)
		ARENA_CONTESTANT_TURRET_CONFIG_CURRENT_WEAPON(ref_data)
		ARENA_SPECTATOR_TURRET_CONFIG_HUD()
		TURRET_CAM_CONFIG_HUD_SOUNDS(TCHS_ARENA_CONTESTANT_CAM)
		TURRET_CAM_CONFIG_SET_LOAD_ARGS(TRUE, TRUE, FALSE, 500)
		
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RUP, "ATB_EXIT")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SNIPER_ZOOM, "ATB_ZOOM")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_GROUP(INPUTGROUP_LOOK, "ATB_LOOK")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RT, "ATB_FIRE")
		// If we have more than one weapontype available
		IF ref_data.iBsAvailableWeaponTypes > SHIFT_LEFT(1, ci_ARENA_TURRET_TYPE_COUNT - 1)
			TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RLEFT, "ACT_CYCLE")
		ENDIF
		
		TURRET_CAM_CONFIG_SET_INPUT_MAP("Arena_Turrets")
	TURRET_CAM_CONFIG_STOP(ref_data.eCamConfigId)
	
	CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Created config")
	CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Weapon types availble Bs : ", ref_data.iBsAvailableWeaponTypes)
	CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Weapon type :", ref_data.iCurrentWeaponType)
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_TURRET_AREA(INT iTurretId, FLOAT fRadius, FLOAT& ref_dist)
	VECTOR vPlayerCoord = GET_PLAYER_COORDS(PLAYER_ID())
	VECTOR vTurretCoord = GET_ENTITY_COORDS(g_arenaContestantTurretStack.entTurrets[iTurretId], FALSE)

	FLOAT fHeightDiff = (vPlayerCoord.z - vTurretCoord.z) 
	IF fHeightDiff >= cf_ARENA_CT_AREA_HEIGHT_OFFSET
	AND fHeightDiff <= cf_ARENA_CT_AREA_HEIGHT
		ref_dist = VMAG(vPlayerCoord - vTurretCoord)
		RETURN ref_dist <= fRadius
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the Arena Contestant Turret with turretId has completed its cooldown.
///    The turrets go a shared on cooldown after every use (and at the start of the match).
///    iReducedMs - Subtracted from target time (e.g. with iReducedMs = 100, this will return TRUE at cooldown - 100ms)
FUNC BOOL ARENA_CONTESTANT_TURRET_IS_COOLDOWN_COMPLETE(ARENA_CONTESTANT_SERVER_DATA& ref_server, INT iTurretId, INT iReducedMs = 0)
	RETURN ref_server.tCooldownTimes[iTurretId] <> INT_TO_NATIVE(TIME_DATATYPE, 0)
		AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(ref_server.tCooldownTimes[iTurretId], -iReducedMs))
ENDFUNC

PROC ARENA_CONTESTANT_TURRET_PRINT_TUT_HELP(BOOL bPlural)
	IF NOT g_bPrintedArenaTurretTutHelp
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		g_bPrintedArenaTurretTutHelp  = TRUE
		INT iHelpDisplayCount = GET_PACKED_STAT_INT(PACKED_MP_INT_ARENA_CONTESTANT_TURRET_HELP_COUNT)
		IF iHelpDisplayCount < 3
			STRING sHelp
			IF bPlural
				sHelp = "ACT_HELP_P"
			ELSE
				sHelp = "ACT_HELP_S"
			ENDIF
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Printing turret tut help ",sHelp,". iHelpDisplayCount = ", iHelpDisplayCount)
			PRINT_HELP(sHelp, DEFAULT_HELP_TEXT_TIME)
			SET_PACKED_STAT_INT(PACKED_MP_INT_ARENA_CONTESTANT_TURRET_HELP_COUNT, iHelpDisplayCount+1)
		ENDIF
	ENDIF		
ENDPROC

FUNC BOOL IS_CURRENT_MODE_BOMB_BALL()
	RETURN 	NETWORK_IS_ACTIVITY_SESSION()
			AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType) 
ENDFUNC

FUNC BOOL ARENA_CONTESTANT_TURRETS_ARE_BLOCKED_EXTERNALLY(ARENA_CONTESTANT_TURRET_CONTEXT ref_data)
	RETURN IS_BIT_SET(ref_data.iBsControlSettings, ci_ARENA_CT_BS_CONTROL_BLOCK)
		OR (HAS_NET_TIMER_STARTED(ref_data.unblockTimer)
		AND NOT HAS_NET_TIMER_EXPIRED(ref_data.unblockTimer, ci_ARENA_CT_PREVENT_UNBLOCK_DURATION_MS))
ENDFUNC

/// PURPOSE:
//	   This function manages informing the player how to use the turrets & controlling the context intention.
///    Update visuals, help text, context intention etc.
PROC ARENA_CONTESTANT_TURRET_UPDATE_CONTROLLER(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, ARENA_CONTESTANT_SERVER_DATA& ref_server, BOOL bPlayerIsDriver)
	// Update one turret bit per frame that represents whether a turret is "free".
	BOOL bCoronaStaggerTurretIsFree = FALSE
	BOOL bCoronaStaggerTurretIsInUse = TRUE
	
	PLAYER_INDEX playerInTurret = GET_PLAYER_IN_TURRET(TGT_ARENA_CONTESTANT, 0, ref_data.iCoronaStagger)
	
	IF NATIVE_TO_INT(playerInTurret) = -1
	OR playerInTurret = PLAYER_ID()
		bCoronaStaggerTurretIsInUse = FALSE
		IF ARENA_CONTESTANT_TURRET_IS_COOLDOWN_COMPLETE(ref_server, ref_data.iCoronaStagger)	
			bCoronaStaggerTurretIsFree  = TRUE
		ENDIF
	ENDIF
		
	IF bCoronaStaggerTurretIsFree
	AND NOT bCoronaStaggerTurretIsInUse
	AND NOT IS_BIT_SET(ref_data.iBsTurretIsFree, ref_data.iCoronaStagger)
		ref_data.iActivated++
	ENDIF	
	
	ENABLE_BIT(ref_data.iBsTurretIsFree, ref_data.iCoronaStagger, bCoronaStaggerTurretIsFree)
	ENABLE_BIT(ref_data.iBsTurretInUse, ref_data.iCoronaStagger, bCoronaStaggerTurretIsInUse)
	
	// Check to see if turrets have been disabled by a FMMC rule...
	BOOL bDisabledByRule = FALSE
	IF IS_THIS_A_MISSION()
		INT iTeam, iRule
		iRule = GET_CURRENT_MC_TEAM_AND_RULE_FROM_GLOBAL_DATA(iTeam)
		IF iRule < FMMC_MAX_RULES
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_DISABLE_ACCESS_TO_TURRET_TOWERS)
			bDisabledByRule = TRUE
		ENDIF
	ENDIF	
	
	BOOL bDisabledBySetting = ARENA_CONTESTANT_TURRETS_ARE_BLOCKED_EXTERNALLY(ref_data)
		
	INT r, g, b, a
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
	
	// Check various conditions for showing help / allowing player to use turret
	BOOL bPrintHelp = TRUE
	IF (NOT bPlayerIsDriver)
	OR (ref_data.eRequestId <> TLRI_NONE)
	OR (TURRET_MANAGER_GET_GROUP(PLAYER_ID()) <> TGT_NONE)
	OR (IS_PLAYER_USING_DRONE(PLAYER_ID()))
	OR IS_PHONE_ONSCREEN()
	OR IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
		bPrintHelp = FALSE
	ENDIF
	
	// Check various conditions for showing the coronas (copy help rules atm)
	BOOL bDrawCoronas = TRUE
	IF NOT bPrintHelp
	OR bDisabledByRule
	OR bDisabledBySetting
		bDrawCoronas = FALSE
	ENDIF
	
	INT iClosestTurret = 0
	FLOAT fShortestDistance = cf_ARENA_CT_NEAR_HELP_DIST
	BOOL bHelpTextTurretActive = FALSE
	
	INT iCoronaTurretId
	REPEAT g_arenaContestantTurretStack.iCount iCoronaTurretId
		OBJECT_INDEX objTower = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(g_arenaContestantTurretStack.entTurrets[iCoronaTurretId])
		
		IF NOT DOES_ENTITY_EXIST(objTower)
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Turret entity doesn't exist. TurretId = ", iCoronaTurretId, " Object_Index = ", NATIVE_TO_INT(objTower))
			IF TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID()) = iCoronaTurretId
			AND TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
				ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
			ENDIF	
			RELOOP
		ENDIF	
		
		VECTOR vCoronaCoords = GET_ENTITY_COORDS(objTower) 
		
		// Coronas:
		BOOL bTurretActive = FALSE
		IF IS_BIT_SET(ref_data.iBsTurretIsFree, iCoronaTurretId)
			bTurretActive = TRUE
			
			// Tower lights (consumer wasteland towers use tints):
			IF g_FMMC_STRUCT.sArenaInfo.iArena_Theme = ARENA_THEME_CONSUMERWASTELAND
				IF GET_OBJECT_TINT_INDEX(objTower) <> ci_ARENA_CT_TINT_GREEN
					CDEBUG3LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- TINT Turret ",iCoronaTurretId," set to GREEN @ ", NATIVE_TO_INT(GET_NETWORK_TIME()))
					SET_OBJECT_TINT_INDEX(objTower, ci_ARENA_CT_TINT_GREEN)
				ENDIF
			ELSE
				IF NOT IS_PROP_LIGHT_OVERRIDEN(objTower)
					IF SET_PROP_LIGHT_COLOR(objTower, TRUE, 0, 255, 0)
						CDEBUG3LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- LIGHT Turret ",iCoronaTurretId," set to GREEN @ ", NATIVE_TO_INT(GET_NETWORK_TIME()))
					ELSE
						CDEBUG1LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- failed to set prop lights: turretId = ",iCoronaTurretId," DOES_ENTITY_EXIST(", NATIVE_TO_INT(objTower), ")", DOES_ENTITY_EXIST(objTower), " dead? ", IS_ENTITY_DEAD(objTower))
					ENDIF
				ENDIF
			ENDIF
			
			// Tower sounds
			IF NOT IS_BIT_SET(ref_data.iBsPlayedTowerFreeSound, iCoronaTurretId)
				CDEBUG3LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- SOUND Turret ",iCoronaTurretId," available @ ", NATIVE_TO_INT(GET_NETWORK_TIME()))
				SET_BIT(ref_data.iBsPlayedTowerFreeSound, iCoronaTurretId)
				IF IS_CURRENT_MODE_BOMB_BALL()
					PLAY_SOUND_FROM_COORD(-1, "Turret_Available_Alt", vCoronaCoords, "dlc_aw_Arena_Gun_Turret_Sounds")
				ELSE
					PLAY_SOUND_FROM_COORD(-1, "Turret_Available", vCoronaCoords, "dlc_aw_Arena_Gun_Turret_Sounds")
				ENDIF
			ENDIF
			
			IF bDrawCoronas				
				FLOAT fNewZ = 0.0
				FLOAT fHeight = cf_ARENA_CT_CORONA_HEIGHT - cf_ARENA_CT_CORONA_OFFSET
				IF GET_GROUND_Z_FOR_3D_COORD(vCoronaCoords + <<0, 0, 5.0>>, fNewZ)
					vCoronaCoords.z = fNewZ
				ENDIF
				vCoronaCoords.z += cf_ARENA_CT_CORONA_OFFSET			
				DRAW_MARKER(MARKER_CYLINDER, vCoronaCoords, <<0, 0, 0>>, <<0, 0, 0>>, <<cf_ARENA_CT_CORONA_RADIUS, cf_ARENA_CT_CORONA_RADIUS, fHeight>>, r, g, b, a)
			ENDIF
		ELSE
			// Tower lights (consumer wasteland towers use tints):
			IF g_FMMC_STRUCT.sArenaInfo.iArena_Theme = ARENA_THEME_CONSUMERWASTELAND
				IF GET_OBJECT_TINT_INDEX(objTower) <> ci_ARENA_CT_TINT_RED
					CDEBUG3LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- TINT Turret ",iCoronaTurretId," set to RED @ ", NATIVE_TO_INT(GET_NETWORK_TIME()))
					SET_OBJECT_TINT_INDEX(objTower, ci_ARENA_CT_TINT_RED)
				ENDIF
			ELSE
				IF IS_PROP_LIGHT_OVERRIDEN(objTower)
					CDEBUG3LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- LIGHT Turret ",iCoronaTurretId," set to RED @ ", NATIVE_TO_INT(GET_NETWORK_TIME()))
					SET_PROP_LIGHT_COLOR(objTower, FALSE, 0, 0, 0)
				ENDIF
			ENDIF
			
			// Tower sounds
			IF IS_BIT_SET(ref_data.iBsPlayedTowerFreeSound, iCoronaTurretId)
				CDEBUG3LN(DEBUG_NET_TURRET, "-- ARENA CONTESTANT -- SOUND Turret ",iCoronaTurretId," CLEAR iBsPlayedTowerFreeSound @ ", NATIVE_TO_INT(GET_NETWORK_TIME()))
				CLEAR_BIT(ref_data.iBsPlayedTowerFreeSound, iCoronaTurretId)
			ENDIF		
		ENDIF
					
		FLOAT fDistance = 0
		IF bPrintHelp
		AND IS_LOCAL_PLAYER_IN_TURRET_AREA(iCoronaTurretId, cf_ARENA_CT_NEAR_HELP_DIST, fDistance)
			IF fDistance < fShortestDistance
				bHelpTextTurretActive = bTurretActive
				fShortestDistance = fDistance
				iClosestTurret = iCoronaTurretId
			ENDIF
		ENDIF
	ENDREPEAT

	IF NATIVE_TO_INT(playerInTurret) = -1 // No one in turret
	OR NOT NETWORK_IS_PLAYER_ACTIVE(playerInTurret)
	OR (playerInTurret = PLAYER_ID() AND NOT TURRET_CAM_IS_RUNNING()) 
		IF IS_BIT_SET(ref_data.iBsTurretIsFree, ref_data.iCoronaStagger)
		AND NOT bDisabledBySetting
		AND NOT bDisabledByRule
			OBJECT_INDEX objTower = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(g_arenaContestantTurretStack.entTurrets[ref_data.iCoronaStagger])
			ARENA_CONTESTANT_TURRET_MAINTAIN_BLIP(ref_data, ref_data.iCoronaStagger, objTower, HUD_COLOUR_YELLOW)
		ELSE
			ARENA_CONTESTANT_TURRET_REMOVE_BLIP(ref_data, ref_data.iCoronaStagger)
		ENDIF
	ELSE
		OBJECT_INDEX objTower = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(g_arenaContestantTurretStack.entTurrets[ref_data.iCoronaStagger])
		// Update blip for specific turret...
		HUD_COLOURS eBlipColour = GET_PLAYER_HUD_COLOUR(playerInTurret)	
		ARENA_CONTESTANT_TURRET_MAINTAIN_BLIP(ref_data, ref_data.iCoronaStagger, objTower, eBlipColour)		
	ENDIF
	
	// Update help/intention
	IF fShortestDistance < cf_ARENA_CT_NEAR_HELP_DIST	
		FLOAT fThrowAway
		IF IS_LOCAL_PLAYER_IN_TURRET_AREA(iClosestTurret, cf_ARENA_CT_AREA_RADIUS, fThrowAway)
			IF bDisabledByRule
				RELEASE_CONTEXT_INTENTION(ref_data.intention)
				PRINT_HELP("ACT_BLOCK_RULE")
			ELIF bDisabledBySetting
				RELEASE_CONTEXT_INTENTION(ref_data.intention)
				PRINT_HELP("ACT_BLOCK_SET")
			ELSE
				IF bHelpTextTurretActive			
					BOOL bVehWheelsOnGround = TRUE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				 		IF NOT IS_ENTITY_ALIVE(veh)
						OR NOT IS_ENTITY_UPRIGHT(veh)
							CDEBUG3LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Vehicle IS_ENTITY_ALIVE = ", IS_ENTITY_ALIVE(veh))
							bVehWheelsOnGround = FALSE
						ENDIF
					ENDIF	
					
					IF bVehWheelsOnGround
						PRINT_HELP("ACT_USE")
						IF ref_data.intention = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(ref_data.intention, CP_MEDIUM_PRIORITY, "", TRUE)	
							ref_data.iLocateStagger = iClosestTurret
							CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- New context intention (",ref_data.intention,") for turretId ", iClosestTurret)
						ENDIF
					ELSE
						RELEASE_CONTEXT_INTENTION(ref_data.intention)
					ENDIF
				ELSE
					RELEASE_CONTEXT_INTENTION(ref_data.intention)
					IF IS_BIT_SET(ref_data.iBsTurretInUse, iClosestTurret)
						PRINT_HELP("ACT_BUSY")
					ELSE
						PRINT_HELP("ACT_COOLDOWN")
					ENDIF
				ENDIF
			ENDIF	
		ELSE
			RELEASE_CONTEXT_INTENTION(ref_data.intention)
			IF bHelpTextTurretActive
			AND NOT bDisabledByRule
			AND NOT bDisabledBySetting
				DISPLAY_HELP_TEXT_THIS_FRAME("ACT_NEAR", FALSE)
				REINIT_NET_TIMER(ref_data.nearHelpTimer)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(ref_data.nearHelpTimer)
		AND NOT HAS_NET_TIMER_EXPIRED(ref_data.nearHelpTimer, ci_ARENA_CT_HELP_LINGER_DURATION_MS)
			DISPLAY_HELP_TEXT_THIS_FRAME("ACT_NEAR", FALSE)
		ENDIF
		RELEASE_CONTEXT_INTENTION(ref_data.intention)
	ENDIF
	
	// Draw "Time left" meter	
	IF TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
	AND TURRET_CAM_IS_READY()
		INT iTimeLeft = ci_ARENA_CT_USE_DURATION_MS - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_data.useTimer)
		FLOAT fNormalised = iTimeLeft / TO_FLOAT(ci_ARENA_CT_USE_DURATION_MS)
		HUD_COLOURS eColour = HUD_COLOUR_WHITE
		IF fNormalised <= cf_ARENA_CT_GENERIC_BAR_LOW_THRESHOLD
			eColour = HUD_COLOUR_RED
		ENDIF
		DRAW_GENERIC_METER(iTimeLeft, ci_ARENA_CT_USE_DURATION_MS, "ACT_TIME", eColour, DEFAULT, HUDORDER_TOP)
	ENDIF
	
	IF ref_data.iCoronaStagger = (g_arenaContestantTurretStack.iCount-1)
		PRINTLN("-- ARENA CONTESTANT -- Turrets Activated after this staggered loop = ", ref_data.iActivated, " Resetting Count back to 0.")
		IF ref_data.iActivated = 1
			ARENA_CONTESTANT_TURRET_PRINT_TUT_HELP(FALSE)
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RemoteGunTowerAvailable, FALSE, TRUE, FALSE)
		ELIF ref_data.iActivated >= 2
			ARENA_CONTESTANT_TURRET_PRINT_TUT_HELP(TRUE)
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RemoteGunTowerAvailable, FALSE, FALSE, TRUE)
		ENDIF
		ref_data.iActivated = 0
	ENDIF
	
	// Inc the slow stagger used for expensive turret checks
	ARENA_CONTESTANT_TURRET_INCR_WRAP_ID(ref_data.iCoronaStagger)
ENDPROC

/// PURPOSE:
///    Updates the server data for the arena contestant turrets. This is called 
///    from inside ARENA_CONTESTANT_TURRET_UPDATE.
///    
/// PARAMS:
///    ref_server - Server broadcast data required for the Arena Contestant Turrets.
PROC ARENA_CONTESTANT_TURRET_UPDATE_SERVER(ARENA_CONTESTANT_SERVER_DATA& ref_server)
	TIME_DATATYPE tNetTime = GET_NETWORK_TIME()
	INT iTurretId
	REPEAT g_arenaContestantTurretStack.iCount iTurretId
		IF IS_THIS_TURRET_ID_IS_FREE(TGT_ARENA_CONTESTANT, 0, iTurretId)
			
			#IF IS_DEBUG_BUILD
			IF g_debug_turretCooldownReset
				ref_server.tCooldownTimes[iTurretId] = GET_TIME_OFFSET(tNetTime, -ci_ARENA_CT_COOLDOWN_DURATION_MS)
			ENDIF
			#ENDIF //IS_DEBUG_BUILD
			
			IF ref_server.tCooldownTimes[iTurretId] = INT_TO_NATIVE(TIME_DATATYPE, 0)
				ref_server.tCooldownTimes[iTurretId] = GET_TIME_OFFSET(tNetTime, ci_ARENA_CT_COOLDOWN_DURATION_MS)
			ENDIF
		ELSE	
			ref_server.tCooldownTimes[iTurretId] = INT_TO_NATIVE(TIME_DATATYPE, 0)
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	g_debug_turretCooldownReset = FALSE
	#ENDIF //IS_DEBUG_BUILD
ENDPROC

FUNC BOOL ARENA_CONTESTANT_TURRET_IS_PED_DRIVING_VALID_VEHICLE(PED_INDEX ped)
	IF IS_PED_IN_ANY_VEHICLE(ped)
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_USING(ped)
		IF GET_PED_IN_VEHICLE_SEAT(veh) = ped
			MODEL_NAMES eModel = GET_ENTITY_MODEL(veh)
			IF eModel <> RCBANDITO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MISSION-CONTROLLER																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////    

/// PURPOSE:
///    Clears the global container of arena contestant turrets.
///    
///    Call once on init to ensure the stack is empty before 
///    adding new turrets with ARENA_CONTESTANT_TURRET_STACK_PUSH.
///    
PROC ARENA_CONTESTANT_TURRET_STACK_CLEAR()
	CDEBUG1LN(DEBUG_NET_TURRET, "ARENA_CONTESTANT_TURRET_STACK_CLEAR")
	g_arenaContestantTurretStack.iCount = 0
ENDPROC

/// PURPOSE:
///    Adds a new arena contestant turret to the global container. 
/// RETURNS:
///    Index of turret - used for various query methods. Will be -1 if there is no space left (increase ci_ARENA_CONTESTANT_TURRETS_MAX).
FUNC INT ARENA_CONTESTANT_TURRET_STACK_PUSH(ENTITY_INDEX entTurret, FLOAT fCamStartWorldHeading = 0.0)
	INT index = g_arenaContestantTurretStack.iCount
	CDEBUG1LN(DEBUG_NET_TURRET, "ARENA_CONTESTANT_TURRET_STACK_PUSH | entTurret = ",NATIVE_TO_INT(entTurret), " fCamStartWorldHeading = ", fCamStartWorldHeading)
	
	IF index >= ci_ARENA_CONTESTANT_TURRETS_MAX
		CASSERTLN(DEBUG_NET_TURRET, "   Too many arena contestant turrets. Increase ci_ARENA_CONTESTANT_TURRETS_MAX")
		RETURN -1
	ENDIF
	
	FLOAT fOffsetHeading = 0.0
	IF DOES_ENTITY_EXIST(entTurret)
		fOffsetHeading = fCamStartWorldHeading - GET_ENTITY_HEADING(entTurret)
	ENDIF
	CDEBUG1LN(DEBUG_NET_TURRET, "   Cam heading offset = ", fOffsetHeading)
	
	g_arenaContestantTurretStack.fHeadings[index] = fOffsetHeading
	g_arenaContestantTurretStack.entTurrets[index] = entTurret
	g_arenaContestantTurretStack.iCount++
	
	RETURN index
ENDFUNC

/// PURPOSE:
///    Force the cooldown of all free turrets to a specific time
PROC ARENA_CONTESTANT_TURRET_SET_REMAINING_COOLDOWN(ARENA_CONTESTANT_SERVER_DATA& ref_server, INT iMilliseconds)
	TIME_DATATYPE tNetTime = GET_NETWORK_TIME()
	CDEBUG1LN(DEBUG_NET_TURRET, "ARENA_CONTESTANT_TURRET_SET_REMAINING_COOLDOWN ", iMilliseconds, " starting @ ", NATIVE_TO_INT(tNetTime))

	INT iTurretId
	REPEAT g_arenaContestantTurretStack.iCount iTurretId
		IF IS_THIS_TURRET_ID_IS_FREE(TGT_ARENA_CONTESTANT, 0, iTurretId)
			ref_server.tCooldownTimes[iTurretId] = GET_TIME_OFFSET(tNetTime, iMilliseconds)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Call every frame to manage the management, launching and control 
///    of the arena contestant turrets
///    
///    Please call ARENA_CONTESTANT_TURRET_CLEANUP() if you need to force the player
///    out of the turret early.
///    
/// PARAMS:
///    ref_data - This state needs to persist for the duration of the arena event.
/// RETURNS:
///    TRUE if the player is using a turret.
FUNC BOOL ARENA_CONTESTANT_TURRET_UPDATE(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, ARENA_CONTESTANT_SERVER_DATA& ref_server, BOOL bHost)
	IF bHost
		ARENA_CONTESTANT_TURRET_UPDATE_SERVER(ref_server)
	ENDIF
	
	IF ARENA_CONTESTANT_TURRET_COUNT() = 0
		IF TURRET_CAM_IS_RUNNING()
		AND TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
		AND NOT TURRET_MANAGER_IS_UNLOCKED()
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- There are no turrets but I'm in one...? ")
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
		ENDIF
		RETURN FALSE
	ENDIF
	
	//*/ Bail if this is end of a mission B*5460013
	IF NOT g_bMissionClientGameStateRunning
	OR g_bCelebrationScreenIsActive
	OR g_bMissionEnding
	OR IS_MC_PLAYER_MINI_ROUND_RESTARTING()
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- END OF MISSION : BLOCKING FURTHER PROCESSING ")
		IF NOT TURRET_MANAGER_IS_UNLOCKED()
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
			ref_data.eRequestId = TLRI_NONE
		ENDIF
		RETURN FALSE	
	ENDIF	
	//*/
	
	BOOL bPlayerIsDriving = ARENA_CONTESTANT_TURRET_IS_PED_DRIVING_VALID_VEHICLE(PLAYER_PED_ID())	
	ARENA_CONTESTANT_TURRET_UPDATE_CONTROLLER(ref_data, ref_server, bPlayerIsDriving)
	
	// If in spectator box no need to do any further proccessing
	IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
		IF TURRET_CAM_IS_RUNNING()
		AND TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
		AND NOT TURRET_MANAGER_IS_UNLOCKED()
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Moved to spectator box, kicking from turret")
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
		ENDIF
		RETURN FALSE
	ENDIF	
	
	// Allow lock on sounds...
	SET_AUDIO_FLAG("EnableMissileLockWarningForAllVehicles", TRUE)
	
	// Don't want this to interfere with any other kind of turret
	TURRET_GROUP_TYPE eCurrentGroupType = TURRET_MANAGER_GET_GROUP(PLAYER_ID())
	IF eCurrentGroupType <> TGT_ARENA_CONTESTANT
	AND eCurrentGroupType <> TGT_NONE
		RETURN FALSE
	ENDIF
		
	INT iTurretId = ref_data.iLocateStagger
	
	FLOAT fThrowAway
	BOOL bPlayerInTurretArea = IS_LOCAL_PLAYER_IN_TURRET_AREA(iTurretId, cf_ARENA_CT_AREA_RADIUS, fThrowAway)
	
	IF g_bMission321Done
		IF bPlayerInTurretArea
		AND bPlayerIsDriving
			// Don't let players blow themselves up
			DISABLE_VEHICLE_MINES(TRUE)		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_CIN_CAM)
			
			SET_BIT(ref_data.iBsControlSettings, ci_ARENA_CT_BS_CONTROL_MINES_BLOCKED)
			
			MPGlobalsAmbience.bDisableMissionHintCam = TRUE
		ELSE
			IF IS_BIT_SET(ref_data.iBsControlSettings, ci_ARENA_CT_BS_CONTROL_MINES_BLOCKED)
				MPGlobalsAmbience.bDisableMissionHintCam = FALSE
				DISABLE_VEHICLE_MINES(FALSE)
				CLEAR_BIT(ref_data.iBsControlSettings, ci_ARENA_CT_BS_CONTROL_MINES_BLOCKED)
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT bPlayerIsDriving
	OR NOT bPlayerInTurretArea
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT TURRET_MANAGER_IS_UNLOCKED()
		AND TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Unlocking ")
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
		ENDIF
		RETURN FALSE
	ENDIF	
	
	// Not currently in a turret?
	IF TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_NONE
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > cf_ARENA_CT_SPEED_LIMIT
			CDEBUG3LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Going too fast! ")
			RETURN FALSE
		ENDIF
			 
		IF NOT ARENA_CONTESTANT_TURRET_IS_COOLDOWN_COMPLETE(ref_server, iTurretId)
			CDEBUG3LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Turret on cooldown - iTurretId = ", iTurretId, " time left (seconds): ", GET_TIME_DIFFERENCE(ref_server.tCooldownTimes[iTurretId], GET_NETWORK_TIME()) / 1000.0)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ref_data.eRequestId = TLRI_NONE
		// Check server data to see if turret is free... (no point sending off request if we can see it is not free locally)
		IF NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND NOT IS_BROWSER_OPEN()
		AND HAS_CONTEXT_BUTTON_TRIGGERED(ref_data.intention)
		AND IS_THIS_TURRET_ID_IS_FREE(TGT_ARENA_CONTESTANT, 0, iTurretId)
			RELEASE_CONTEXT_INTENTION(ref_data.intention)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Lock request iTurretId = ", iTurretId)
			ref_data.eRequestId = TURRET_MANAGER_CREATE_LOCK_REQUEST(TGT_ARENA_CONTESTANT, 0, iTurretId, TLST_THIS_TURRET_ONLY)
		ENDIF
		RETURN FALSE
	ELSE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)		
		
		TURRET_LOCK_RESPONSE result = TURRET_MANAGER_GET_LOCK_STATUS(ref_data.eRequestId)
		IF result = TLR_SUCCESS
			IF TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY()
				CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Launching cam")
				// Launch turret cam
				TURRET_CAM_ARGS args
				ARENA_CONTETANT_TURRET_SET_CONFIG(ref_data, args)
				TURRET_MANAGER_LAUNCH_CAM_NOW(args)
				
				// Reset some local state
				REINIT_NET_TIMER(ref_data.useTimer)			
				ref_data.bFiredPilotedMissile = FALSE
				
				IF IS_CURRENT_MODE_BOMB_BALL()
					PLAY_SOUND_FROM_COORD(-1, "Turret_Activated_Alt", GET_ENTITY_COORDS(g_arenaContestantTurretStack.entTurrets[iTurretId], FALSE), "dlc_aw_Arena_Gun_Turret_Sounds", TRUE, 650)
				ELSE
					PLAY_SOUND_FROM_COORD(-1, "Turret_Activated", GET_ENTITY_COORDS(g_arenaContestantTurretStack.entTurrets[iTurretId], FALSE), "dlc_aw_Arena_Gun_Turret_Sounds", TRUE, 650)
				ENDIF
				
				IF ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_MG
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_Generic, g_iAA_PlaySound_Generic_RemoteGunTowerUseMG)
				ELIF ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_HM
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_Generic, g_iAA_PlaySound_Generic_RemoteGunTowerUseRocket)
				ELIF ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_PM
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_Generic, g_iAA_PlaySound_Generic_RemoteGunTowerUseRedeemer)
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_FROZEN_CHANGES | NSPC_PREVENT_COLLISION_CHANGES | NSPC_CANCEL_TELEPORT_THIS_FRAME)
			ELIF TURRET_MANAGER_CURRENT_CAM_MATCHES_LOCK()	
				
				// ** IN TURRET CAM **
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_BROWSER_OPEN()
					RETURN ARENA_CONTESTANT_TURRET_MAINTAIN_CONTROLS(ref_data)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ELIF result <> TLR_PENDING
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- Lock failed")
			ARENA_CONTESTANT_TURRET_CLEANUP(ref_data, FALSE)
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Call to disable/enable the Arena Contestant Turrets for the local player.
///    (Default behaviour is to have them enabled, i.e. bBlocked = FALSE)
PROC ARENA_CONTESTANT_TURRET_SET_BLOCKED(ARENA_CONTESTANT_TURRET_CONTEXT& ref_data, BOOL bBlocked)
	CDEBUG3LN(DEBUG_NET_TURRET, "ARENA_CONTESTANT_TURRET_SET_BLOCKED ", bBlocked)
	
	IF bBlocked
		CDEBUG3LN(DEBUG_NET_TURRET, "ARENA_CONTESTANT_TURRET_SET_BLOCKED REINIT_NET_TIMER")
		REINIT_NET_TIMER(ref_data.unblockTimer)
	ENDIF
	
	ENABLE_BIT(ref_data.iBsControlSettings, ci_ARENA_CT_BS_CONTROL_BLOCK, bBlocked)
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EVENTS           																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////    

/// PURPOSE:
///    Handle the damage event : this should be called if a damage event
///    comes in where the local player has killed another player.
PROC ARENA_CONTESTANT_TURRET_HANDLE_PLAYER_KILL(INT iWeaponType)
	BOOL bIncrementKillStat = FALSE
	IF iWeaponType = ENUM_TO_INT(WEAPONTYPE_EXPLOSION)
		IF TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
		AND TURRET_CAM_IS_READY()
			bIncrementKillStat = TRUE
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- : Killed player with piloted missile")
		ENDIF
	ELIF iWeaponType = ENUM_TO_INT(WEAPONTYPE_DLC_ARENA_HOMING_MISSILE)
		bIncrementKillStat = TRUE
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- : Killed player with homing missile")
	ELIF iWeaponType = ENUM_TO_INT(WEAPONTYPE_DLC_ARENA_MACHINE_GUN)	
		// This IF check can be removed when a new weapontype has been added! @@!evaluate
		IF TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT
		AND TURRET_CAM_IS_READY()
			bIncrementKillStat = TRUE
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT ----- : Killed player with the MG")
		ENDIF	
	ENDIF
	
	IF bIncrementKillStat
		INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_TOWER_OFFENSE)
		SET_LOCAL_PLAYER_ARENA_GARAGE_TROPHY_TOWER_DATA()
	ENDIF
ENDPROC
