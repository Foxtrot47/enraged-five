// This functions allow you to define a plane in the world and then perform a LOS check between two coords,
// checking if the line of sight intersects the defined plane. Booyakasha!
// Note: Doesn't check collision with any map objects.

//example of use: Trevor 3
//Help text needs to be shown when you approach certain peds but a simple distance check wasn't sufficient.
//Many trailers could block the player's view, so the player could be close enough to trigger the help but not actually see the ped in question.
//using locates wasn't an option as the areas to be defined would become too complex and fiddly to set up.

USING "rgeneral_include.sch"

FUNC BOOL POINT_IN_TRI(vector vPointOnPlane, vector vCorner1,vector vCorner2,vector vCorner3)					
	vector v0 = vCorner3 - vCorner1
	vector v1 = vCorner2 - vCorner1
	vector v2 = vPointOnPlane - vCorner1
	
	float dot00 = DOT_PRODUCT(v0, v0)
	float dot01 = DOT_PRODUCT(v0, v1)
	float dot02 = DOT_PRODUCT(v0, v2)
	float dot11 = DOT_PRODUCT(v1, v1)
	float dot12 = DOT_PRODUCT(v1, v2) 
	
	float invDenom = 1 / (dot00 * dot11 - dot01 * dot01)
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom
	
	IF (u >= 0) AND (v >= 0) AND (u + v < 1)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LINE_INTERSECT_QUAD(vector p1, vector p2, vector vCorner1,vector vCorner2,vector vCorner3,vector vCorner4)
	vector vPointOnPlane

	
	vector normal = CROSS_PRODUCT(vCorner2 - vCorner1, vCorner3 - vCorner1)
	IF NOT IS_VECTOR_ZERO(normal)
		normal = NORMALISE_VECTOR(normal)
		IF GET_LINE_PLANE_INTERSECT(vPointOnPlane, p1, p2, normal, vCorner1)
			IF GET_DISTANCE_BETWEEN_COORDS(p1,vPointOnPlane) < GET_DISTANCE_BETWEEN_COORDS(p1,p2)
				IF POINT_IN_TRI(vPointOnPlane,vCorner1,vCorner2,vCorner3)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF

		normal = CROSS_PRODUCT(vCorner3 - vCorner1, vCorner4 - vCorner1)
		normal = NORMALISE_VECTOR(normal)
		IF NOT IS_VECTOR_ZERO(normal)
			IF GET_LINE_PLANE_INTERSECT(vPointOnPlane, p1, p2, normal, vCorner1)			
				IF GET_DISTANCE_BETWEEN_COORDS(p1,vPointOnPlane) < GET_DISTANCE_BETWEEN_COORDS(p1,p2)
					IF POINT_IN_TRI(vPointOnPlane,vCorner1,vCorner4,vCorner3)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIf

	return false
ENDFUNC



	
	
