//|=======================================================================================|
//|                 Author:  Lukasz Bogaj	 				Date: 04/08/2011              |
//|=======================================================================================|
//|                                  		YOGA.sch	                              	  |
//|                            		 	YOGA HEADER FILE                          	  	  |
//|                             									                      |
//|=======================================================================================|

//|==================================== INCLUDE FILES ====================================|

USING "rage_builtins.sch"
USING "globals.sch"

//Commands
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_path.sch"
USING "commands_audio.sch"

//Script things
USING "script_player.sch"
USING "script_buttons.sch"
USING "script_maths.sch"
USING "script_blips.sch"

//Public files
USING "locates_private.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "locates_public.sch"
USING "building_control_public.sch"
USING "dialogue_public.sch"
USING "commands_hud.sch"
USING "finance_modifiers_public.sch"
USING "stats_public.sch"

//Debug things
USING "script_debug.sch"

//===================================== ENUMS & STRUCTS ==================================|

/// PURPOSE: Specifies possible yoga moves in yoga minigame.
ENUM YOGAMOVE
	YOGAMOVE_WARRIOR 						= 0,
	YOGAMOVE_TRIANGLE,
	YOGAMOVE_SUNSALUTATION
ENDENUM

/// PURPOSE: Specifies yoga minigame stages.
ENUM YOGAMINIGAMESTAGES 
	YOGAMINIGAMESTAGE_SETUP 				= 0,
	YOGAMINIGAMESTAGE_DOMOVE,
	YOGAMINIGAMESTAGE_DOFAIL,
	YOGAMINIGAMESTAGE_WAITFAIL,
	YOGAMINIGAMESTAGE_FAILRESET,
	YOGAMINIGAMESTAGE_RESTART,
	YOGAMINIGAMESTAGE_PASSED,
	YOGAMINIGAMESTAGE_FINISHED
ENDENUM

/// PURPOSE: Specifies yoga move stages.
ENUM YOGAMOVESTAGES
	YOGAMOVESTAGE_DOINGPOSES 				= 0,
	YOGAMOVESTAGE_PLAY_POSE_ANIMATION,
	YOGAMOVESTAGE_BREATHE,
	YOGAMOVESTAGE_WAIT_BETWEEN_POSES,
	YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION,
	YOGAMOVESTAGE_FINISHED,
	YOGAMOVESTAGE_FAILED
ENDENUM

/// PURPOSE: Specifies yoga breathing stages.
ENUM YOGABREATHESTAGES
	YOGABREATHESTAGES_INHALE				= 0,
	YOGABREATHESTAGES_EXHALE,
	YOGABREATHESTAGES_DISPLAY_HELP,
	YOGABREATHESTAGES_START_DELAY
ENDENUM

/// PURPOSE: Specifies the two analogue sticks.
ENUM STICKS
	STICK_LEFT 								= 0,
	STICK_RIGHT
ENDENUM

/// PURPOSE: specifies the direction of stick graphic icon rotation.
ENUM STICKROTATIONS
	STICKROTATION_CLOCKWISE					= 0,
	STICKROTATION_COUNTERCLOCKWISE
ENDENUM

/// PURPOSE: Specifies analogue stick directions.
ENUM STICKDIRECTIONS
	STICKDIRECTION_UP 						= 0,
	STICKDIRECTION_UPRIGHT 					= 45,
	STICKDIRECTION_RIGHT 					= 90,
	STICKDIRECTION_DOWNRIGHT				= 135,
	STICKDIRECTION_DOWN 					= 180,
	STICKDIRECTION_DOWNLEFT 				= 225,
	STICKDIRECTION_LEFT 					= 270,
	STICKDIRECTION_UPLEFT 					= 315
ENDENUM

/// PURPOSE: Specifies analogue sticks position check.
ENUM STICKPOSITIONSCHECKS
	STICKPOSITIONCHECK_MOVE					= 0,		//Sticks need to be moved into position.
	STICKPOSITIONCHECK_HOLD								//Sticks need to be held in position.
ENDENUM

/// PURPOSE: Specifies a structure storing yoga minigame information.
STRUCT YOGASTRUCT
	PED_INDEX 			PedIndex													//Ped Index
	VECTOR 				vPosition													//Ped position vector
	VECTOR 				vRotation													//Ped rotation vector
	YOGAMINIGAMESTAGES 	eYogaMinigameStage											//Current yoga minigame stage
	YOGAMOVE 			eYogaMove													//Current yoga move
	YOGAMOVESTAGES 		eYogaMoveStage												//Current yoga move stage
	YOGABREATHESTAGES	eYogaBreatheStage
	STRING 				sYogaAnimDict												//Animation dictionary name
	BOOL 				bFailing													//
	INT					iBreathingValue
	INT 				iFailCounter												//Number of fails of current yoga move
	INT					iBreathingCounter											//Number of times inhale/exhale cycle was repeated
	INT 				iPosesCompleted												//Number of comleted poses for current yoga move
	INT					iFailedPose
	INT 				iShakeTimer													//Number of miliseconds to shake the controller
	INT 				iTimer														//timer
	INT					iInhaleTimer
	INT					iExhaleTimer
	BOOL				bInhaleInProgress
	BOOL				bExhaleInProgress
	INT					iInhaleSoundID
	INT					iExhaleSoundID
	INT					iFailSoundID
	INT					iLeftStickPosition
	INT					iRightStickPosition
	INT					iLeftStickCurrentAngle
	INT					iRightStickCurrentAngle
	BOOL				bLeftStickInDeadZone
	BOOL				bRightStickInDeadZone
	FLOAT				fMinRange
	FLOAT				fMaxRange
	BOOL				bInhaleSoundTriggered
	BOOL				bExhaleSoundTriggered
	BOOL				bInhaleCompleted
	BOOL				bExhaleCompleted
	INT					iBreatheDelayTime
	INT					iBreatheDelayTimer
	INT					iBreathsCompleted
	BOOL				bBreathCompleted
	BOOL				bBreathingPassed
	BOOL				bInhaleFailed
	BOOL				bExhaleFailed
	INT					iHelpTextTimer
	BOOL				bHelpTextDelayDone
	FLOAT				fValue
	BOOL				bChangeCamera
	FLOAT				fModifierStrength
	FLOAT				fModifierStrengthMax
	BOOL				bChangePoseFacial
	BOOL				bAmbientSpeechPlayed
	FLOAT				fBlushDecalAlpha
	INT					iHoldBreathTimer
	INT					iFailCameraCutTimer
ENDSTRUCT

//|======================================= CONSTANTS =====================================|

CONST_INT				YCF_CHANGE						0
CONST_INT				YCF_INTERP						1
CONST_INT				YCF_ACTIVE						2

CONST_INT 				MAX_YOGAMOVES					3
CONST_INT 				MAX_ANIMATIONS_IN_YOGAMOVE		15
CONST_INT 				MAX_POSES_IN_YOGAMOVE			7

CONST_INT				MOUSE_SENSITIVITY				4 // Divides mouse values by this, so higher the number = less sensitive.


FLOAT					YOGA_BUTTONS_X					= 0.5
FLOAT					YOGA_BUTTONS_Y					= 0.88
FLOAT					YOGA_BUTTONS_WIDTH				= 0.609375
FLOAT					YOGA_BUTTONS_HEIGHT				= 0.266666					//0.355555 * 0.75
FLOAT					YOGA_BUTTONS_SCALE				= 1.2

//|========================================= ARRAYS ======================================|

INT 					iPoses[MAX_YOGAMOVES]										//number of poses for each yoga move
INT 					iAnimations[MAX_YOGAMOVES]									//number of animations for each yoga move
BOOL					bScaleformYogaButtonsPressed[2]								//yoga button flags used for scaleform
STRING 					sYogaPosesAnims[MAX_YOGAMOVES][MAX_ANIMATIONS_IN_YOGAMOVE]	//animation names for each pose in each yoga move
STICKDIRECTIONS 		eStickDirections[MAX_YOGAMOVES][MAX_POSES_IN_YOGAMOVE][2]	//stick directions for each pose in each yoga move
STICKROTATIONS			eStickRotations[MAX_YOGAMOVES][MAX_POSES_IN_YOGAMOVE][2]	//stick rotations from each direction to the next

VECTOR					vYogaFailCameraPositions[2]									//yoga fail camera position vectors 0 - Amanda 1 - Fabien
VECTOR					vYogaFailCameraRotations[2]									//yoga fail camera rotation vecotrs 0 - Amanda 1 - Fabien
FLOAT					vYogaFailCameraFOVs[2]										//yoga fail camera FOVs				0 - Amanda 1 - Fabien

VECTOR					vYogaCameraPositions[MAX_POSES_IN_YOGAMOVE + 1][2]			//yoga custom camera position vectors
VECTOR					vYogaCameraRotations[MAX_POSES_IN_YOGAMOVE + 1][2]			//yoga custom camera rotation vectors
FLOAT					fYogaCameraFOVs[MAX_POSES_IN_YOGAMOVE + 1][2]				//yoga custom camera FOVs
INT						iYogaCameraInterpTimes[MAX_POSES_IN_YOGAMOVE + 1]			//yoga custom camera interp times
INT						iYogaCameraFailCutTimes[MAX_POSES_IN_YOGAMOVE + 1]			//yoga custom camera fail cut times
BOOL					bYogaCameraFlags[MAX_POSES_IN_YOGAMOVE + 1][3]				//yoga custom camera flags, sets custom cameras active

//======================================== VARIABLES =====================================|

BOOL					HELP_STICKS_TRIGGERED
BOOL					HELP_INHALE_TRIGGERED
BOOL					HELP_EXHALE_TRIGGERED

BOOL 					bShowBreathingButtons										//flags if scaleform buttons should be rendered
BOOL 					bBreathingButtonsAdded										//flags if scaleform buttons have been added
SCALEFORM_INDEX 		siYogaScaleform												//scaleform buttons movie clip
SCALEFORM_INDEX 		siYogaScaleformKBM											//scaleform keyboard/mouse movie clip
CAMERA_INDEX			YogaFailCamera												//camera used for fail cuts
CAMERA_INDEX			YogaCustomCamera											//camera used for custom angles during yoga minigame

FLOAT 					ShakeIntensity

INT 					iLeftRed, iLeftGreen, iLeftBlue								//RGB value for left stick scaleform image 
INT 					iRightRed, iRightGreen, iRightBlue							//RGB value for right stick scaleform image

INT 					iStickXPrev, iStickYPrev									// Previous values for right stick, for PC mouse detection.
BOOL					bSouthpaw													// Southpaw mouse and keyboard mode for PC controls - swaps the two sets of inputs so they correspond to the users mouse and keyboard layout.	Not to be confused with gamepad southpaw.

//================================== FUNCTIONS & PROCEDURES ==============================|

FUNC BOOL IS_ANY_YOGA_HELP_MESSAGE_BEING_DISPLAYED()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STICKS")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("INHALE_NEW")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("EXHALE_NEW")
		RETURN TRUE
	ENDIF
	
	// Check PC text separately as it's not included on console.
	IF IS_PC_VERSION()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STICKS_KM")
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC


FUNC BOOL HAS_YOGA_SCALEFORM_LOADED()

	IF IS_PC_VERSION()
	
		IF HAS_SCALEFORM_MOVIE_LOADED(siYogaScaleform)
		AND HAS_SCALEFORM_MOVIE_LOADED(siYogaScaleformKBM)
			RETURN TRUE
		ENDIF
	
	ELSE
	
		IF HAS_SCALEFORM_MOVIE_LOADED(siYogaScaleform)
			RETURN TRUE
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Sets the ped variations for yoga minigame. Works for player ped Michael only.
/// PARAMS:
///    PedIndex - Ped index to set variations on.
///    iPlayerPed - Number indicating which player ped it is (0 - Michael, 1 - Franklin, 2 - Trevor). Works for Michael only.
PROC SET_PLAYER_PED_VARIATIONS_FOR_YOGA(PED_INDEX PedIndex, INT iPlayerPed = 0)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
			SWITCH iPlayerPed
				CASE 0		//Michael, the only player ped allowed to do yoga
				
					SET_PED_COMP_ITEM_CURRENT_SP(PedIndex, COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA, FALSE)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_PLAYER_PED_VARIATIONS_FOR_YOGA() - Setting player ped variation to yoga outfit.")
					#ENDIF
				
				BREAK
				CASE 1		//Franklin, not used for now
				
				BREAK
				CASE 2		//Trevor, not used for now
				
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
ENDPROC

PROC CONVERGE_VALUE(FLOAT &fCurrentValue, FLOAT fDesiredValue, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF fCurrentValue != fDesiredValue
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF fCurrentValue - fDesiredValue > fConvergeAmountThisFrame
			fCurrentValue -= fConvergeAmountThisFrame
		ELIF fCurrentValue - fDesiredValue < -fConvergeAmountThisFrame
			fCurrentValue += fConvergeAmountThisFrame
		ELSE
			fCurrentValue = fDesiredValue
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the lowest float value from two specified float values.
/// PARAMS:
///    fValueA - First float value.
///    fValueB - Second float value.
/// RETURNS:
///    Lowest float value of the two.
FUNC FLOAT GET_MINIMUM_FLOAT(FLOAT fValueA, FLOAT fValueB)

	IF (fValueA = fValueB)
	
		RETURN fValueA
	
	ELIF (fValueA > fValueB)
	
		RETURN fValueB
		
	ELIF (fValueA < fValueB)
	
		RETURN fValueA
	
	ENDIF
	
	RETURN fValueA

ENDFUNC

/// PURPOSE:
///    Returns a value how much shoulder2 button is pressed in the range of 0 - 100.
/// PARAMS:
///    iTriggerValue - Shoulder2 button value in the range of 0-255.
/// RETURNS:
///    Returns shoulder2 button in the range of 0 - 100.
FUNC INT GET_SCALED_SHOULDER_VALUE(INT iTriggerValue)

	RETURN CEIL((100.0 / 255.0) * TO_FLOAT(iTriggerValue))

ENDFUNC

/// PURPOSE:
///    Returns breathing value calculated from both trigger/shoulder buttons.
/// PARAMS:
///    iLeftTriggerValue - Value of the left trigger (L2).
///    iRightTriggerValue - Value of the right trigger (R2).
/// RETURNS:
///    Exhale value in the range of 0-100.
FUNC INT GET_BREATHING_VALUE(INT iLeftTriggerValue, INT iRightTriggerValue)

	RETURN CEIL( ( ( TO_FLOAT(GET_SCALED_SHOULDER_VALUE(iLeftTriggerValue) + GET_SCALED_SHOULDER_VALUE(iRightTriggerValue)) ) / 200.0) * 100.0)

ENDFUNC

FUNC BOOL IS_BREATHING_VALUE_IN_BUTTON_TARGET(INT iValue, INT iMinTarget, INT iMaxTarget)
	
	IF ( iValue >= iMinTarget AND iValue <= iMaxTarget )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_TIME_PASSED_ON_TIMER(INT iTimeAmount, INT iTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Sets up yoga moves information specific to each yoga move: number of poses, buttons, animations.
PROC SETUP_YOGAMOVE(YOGAMOVE eMove, INT iLocation)

	//Setup number of poses
	iPoses[YOGAMOVE_WARRIOR] 		= 3
	iPoses[YOGAMOVE_TRIANGLE] 		= 4
	iPoses[YOGAMOVE_SUNSALUTATION] 	= 7
	
	//Setup animation names and animation numbers
	iAnimations[YOGAMOVE_WARRIOR] = 7
	sYogaPosesAnims[YOGAMOVE_WARRIOR][0] = "start_to_a1"
	sYogaPosesAnims[YOGAMOVE_WARRIOR][1] = "a1_pose"
	sYogaPosesAnims[YOGAMOVE_WARRIOR][2] = "a1_to_a2"
	sYogaPosesAnims[YOGAMOVE_WARRIOR][3] = "a2_pose"
	sYogaPosesAnims[YOGAMOVE_WARRIOR][4] = "a2_to_a3"
	sYogaPosesAnims[YOGAMOVE_WARRIOR][5] = "a3_pose"
	sYogaPosesAnims[YOGAMOVE_WARRIOR][6] = "a3_to_start"
	
	iAnimations[YOGAMOVE_TRIANGLE] = 9
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][0] = "start_to_a1"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][1] = "a1_pose"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][2] = "a1_to_a2"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][3] = "a2_pose"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][4] = "a2_to_a3"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][5] = "a3_pose"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][6] = "a3_to_b4"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][7] = "b4_pose"
	sYogaPosesAnims[YOGAMOVE_TRIANGLE][8] = "b4_to_start"
	
	iAnimations[YOGAMOVE_SUNSALUTATION] = 15
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][0] = "start_to_c1"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][1] = "c1_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][2] = "c1_to_c2"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][3] = "c2_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][4] = "c2_to_c3"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][5] = "c3_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][6] = "c3_to_c4"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][7] = "c4_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][8] = "c4_to_c5"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][9] = "c5_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][10] = "c5_to_c6"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][11] = "c6_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][12] = "c6_to_c7"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][13] = "c7_pose"
	sYogaPosesAnims[YOGAMOVE_SUNSALUTATION][14] = "c7_to_start"
	
	
	//WARRIOR
	eStickDirections[YOGAMOVE_WARRIOR][0][STICK_LEFT] = STICKDIRECTION_DOWN
	eStickDirections[YOGAMOVE_WARRIOR][0][STICK_RIGHT] = STICKDIRECTION_DOWN
	
	eStickRotations[YOGAMOVE_WARRIOR][0][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_WARRIOR][0][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	eStickDirections[YOGAMOVE_WARRIOR][1][STICK_LEFT] = STICKDIRECTION_LEFT
	eStickDirections[YOGAMOVE_WARRIOR][1][STICK_RIGHT] = STICKDIRECTION_RIGHT
	
	eStickRotations[YOGAMOVE_WARRIOR][1][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_WARRIOR][1][STICK_RIGHT] = STICKROTATION_COUNTERCLOCKWISE
	
	eStickDirections[YOGAMOVE_WARRIOR][2][STICK_LEFT] = STICKDIRECTION_UPLEFT
	eStickDirections[YOGAMOVE_WARRIOR][2][STICK_RIGHT] = STICKDIRECTION_DOWNRIGHT
	
	eStickRotations[YOGAMOVE_WARRIOR][2][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_WARRIOR][2][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	
	//TRIANGLE
	eStickDirections[YOGAMOVE_TRIANGLE][0][STICK_LEFT] = STICKDIRECTION_DOWNLEFT
	eStickDirections[YOGAMOVE_TRIANGLE][0][STICK_RIGHT] = STICKDIRECTION_DOWNRIGHT
	
	eStickRotations[YOGAMOVE_TRIANGLE][0][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_TRIANGLE][0][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	eStickDirections[YOGAMOVE_TRIANGLE][1][STICK_LEFT] = STICKDIRECTION_UPLEFT
	eStickDirections[YOGAMOVE_TRIANGLE][1][STICK_RIGHT] = STICKDIRECTION_UPRIGHT
	
	eStickRotations[YOGAMOVE_TRIANGLE][1][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_TRIANGLE][1][STICK_RIGHT] = STICKROTATION_COUNTERCLOCKWISE
	
	eStickDirections[YOGAMOVE_TRIANGLE][2][STICK_LEFT] = STICKDIRECTION_LEFT
	eStickDirections[YOGAMOVE_TRIANGLE][2][STICK_RIGHT] = STICKDIRECTION_DOWNRIGHT
	
	eStickRotations[YOGAMOVE_TRIANGLE][2][STICK_LEFT] = STICKROTATION_COUNTERCLOCKWISE
	eStickRotations[YOGAMOVE_TRIANGLE][2][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	eStickDirections[YOGAMOVE_TRIANGLE][3][STICK_LEFT] = STICKDIRECTION_DOWNLEFT
	eStickDirections[YOGAMOVE_TRIANGLE][3][STICK_RIGHT] = STICKDIRECTION_UP
	
	eStickRotations[YOGAMOVE_TRIANGLE][3][STICK_LEFT] = STICKROTATION_COUNTERCLOCKWISE
	eStickRotations[YOGAMOVE_TRIANGLE][3][STICK_RIGHT] = STICKROTATION_COUNTERCLOCKWISE
	
	
	//SUNSALUTATION
	eStickDirections[YOGAMOVE_SUNSALUTATION][0][STICK_LEFT] = STICKDIRECTION_LEFT
	eStickDirections[YOGAMOVE_SUNSALUTATION][0][STICK_RIGHT] = STICKDIRECTION_RIGHT
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][0][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][0][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	eStickDirections[YOGAMOVE_SUNSALUTATION][1][STICK_LEFT] = STICKDIRECTION_DOWN
	eStickDirections[YOGAMOVE_SUNSALUTATION][1][STICK_RIGHT] = STICKDIRECTION_DOWN
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][1][STICK_LEFT] = STICKROTATION_COUNTERCLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][1][STICK_RIGHT] = STICKROTATION_CLOCKWISE

	eStickDirections[YOGAMOVE_SUNSALUTATION][2][STICK_LEFT] = STICKDIRECTION_DOWNLEFT
	eStickDirections[YOGAMOVE_SUNSALUTATION][2][STICK_RIGHT] = STICKDIRECTION_DOWNRIGHT
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][2][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][2][STICK_RIGHT] = STICKROTATION_COUNTERCLOCKWISE

	eStickDirections[YOGAMOVE_SUNSALUTATION][3][STICK_LEFT] = STICKDIRECTION_DOWN
	eStickDirections[YOGAMOVE_SUNSALUTATION][3][STICK_RIGHT] = STICKDIRECTION_DOWN
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][3][STICK_LEFT] = STICKROTATION_COUNTERCLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][3][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	eStickDirections[YOGAMOVE_SUNSALUTATION][4][STICK_LEFT] = STICKDIRECTION_UP
	eStickDirections[YOGAMOVE_SUNSALUTATION][4][STICK_RIGHT] = STICKDIRECTION_UP
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][4][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][4][STICK_RIGHT] = STICKROTATION_COUNTERCLOCKWISE
	
	eStickDirections[YOGAMOVE_SUNSALUTATION][5][STICK_LEFT] = STICKDIRECTION_DOWNLEFT
	eStickDirections[YOGAMOVE_SUNSALUTATION][5][STICK_RIGHT] = STICKDIRECTION_DOWNRIGHT
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][5][STICK_LEFT] = STICKROTATION_COUNTERCLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][5][STICK_RIGHT] = STICKROTATION_CLOCKWISE
	
	eStickDirections[YOGAMOVE_SUNSALUTATION][6][STICK_LEFT] = STICKDIRECTION_UPRIGHT
	eStickDirections[YOGAMOVE_SUNSALUTATION][6][STICK_RIGHT] = STICKDIRECTION_UPLEFT
	
	eStickRotations[YOGAMOVE_SUNSALUTATION][6][STICK_LEFT] = STICKROTATION_CLOCKWISE
	eStickRotations[YOGAMOVE_SUNSALUTATION][6][STICK_RIGHT] = STICKROTATION_COUNTERCLOCKWISE
	
	//Custom cameras
	
	SWITCH eMove
	
		CASE YOGAMOVE_WARRIOR
			
			SWITCH iLocation
			
				CASE 0	//Michael's house location
				
					//1st pose
					vYogaCameraPositions[0][0]		= <<-788.328918,187.789871,72.757515>>
					vYogaCameraRotations[0][0]		= <<-2.995436,0.021138,119.407288>>
					fYogaCameraFOVs[0][0]			= 38.843723
					
					vYogaCameraPositions[0][1]		= <<-788.328918,187.789871,72.757515>>
					vYogaCameraRotations[0][1]		= <<-2.995436,0.021138,119.407288>>
					fYogaCameraFOVs[0][1]			= 38.843723
					
					iYogaCameraFailCutTimes[0]		= 0
					
					bYogaCameraFlags[0][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[0][YCF_INTERP]	= FALSE
					bYogaCameraFlags[0][YCF_ACTIVE]	= FALSE
					
					//2nd pose
					vYogaCameraPositions[1][0]		= <<-777.072632,178.258270,73.161331>>
					vYogaCameraRotations[1][0]		= <<0.894393,0.026403,59.980026>>
					fYogaCameraFOVs[1][0]			= 38.472504
					
					vYogaCameraPositions[1][1]		= <<-777.072632,178.258270,73.161331>>
					vYogaCameraRotations[1][1]		= <<0.894393,0.026403,59.980026>>
					fYogaCameraFOVs[1][1]			= 38.472504
					
					iYogaCameraFailCutTimes[1]		= 0
					
					bYogaCameraFlags[1][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[1][YCF_INTERP]	= FALSE
					bYogaCameraFlags[1][YCF_ACTIVE]	= FALSE
					
					//3rd pose
					vYogaCameraPositions[2][0]		= <<-789.175659,189.242294,72.391922>>
					vYogaCameraRotations[2][0]		= <<7.114471,0.027280,162.324341>>
					fYogaCameraFOVs[2][0]			= 39.136875
					
					vYogaCameraPositions[2][1]		= <<-789.175659,189.242294,72.391922>>
					vYogaCameraRotations[2][1]		= <<7.114471,0.027280,162.324341>>
					fYogaCameraFOVs[2][1]			= 39.136875
					
					iYogaCameraFailCutTimes[2]		= 0
					
					bYogaCameraFlags[2][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[2][YCF_INTERP]	= FALSE
					bYogaCameraFlags[2][YCF_ACTIVE]	= TRUE
					
					//end pose animation
					vYogaCameraPositions[3][0]		= <<-789.809937,186.571686,73.313194>>
					vYogaCameraRotations[3][0]		= <<-4.724632,-0.029824,123.259636>>
					fYogaCameraFOVs[3][0]			= 36.660973
					
					vYogaCameraPositions[3][1]		= <<-789.809937,186.571686,73.313194>>
					vYogaCameraRotations[3][1]		= <<2.853551,-0.029824,107.239113>>
					fYogaCameraFOVs[3][1]			= 36.660973
					
					iYogaCameraInterpTimes[3]		= 2500
					iYogaCameraFailCutTimes[3]		= 0
					
					bYogaCameraFlags[3][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[3][YCF_INTERP]	= TRUE
					bYogaCameraFlags[3][YCF_ACTIVE]	= TRUE
					
				BREAK
				
				CASE 1	//North Coast location
				
					//1st pose
					vYogaCameraPositions[0][0]		= <<2863.545410,5942.737305,357.634216>>
					vYogaCameraRotations[0][0]		= <<7.496217,-0.044239,35.309185>>
					fYogaCameraFOVs[0][0]			= 39.355549
					
					vYogaCameraPositions[0][1]		= <<2863.545410,5942.737305,357.634216>>
					vYogaCameraRotations[0][1]		= <<7.496217,-0.044239,35.309185>>
					fYogaCameraFOVs[0][1]			= 39.355549
					
					iYogaCameraFailCutTimes[0]		= 0
					
					bYogaCameraFlags[0][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[0][YCF_INTERP]	= FALSE
					bYogaCameraFlags[0][YCF_ACTIVE]	= FALSE
					
					//2nd pose
					vYogaCameraPositions[1][0]		= <<2863.545410,5942.737305,357.634216>>
					vYogaCameraRotations[1][0]		= <<7.496217,-0.044239,35.309185>>
					fYogaCameraFOVs[1][0]			= 39.355549
					
					vYogaCameraPositions[1][1]		= <<2863.545410,5942.737305,357.634216>>
					vYogaCameraRotations[1][1]		= <<7.496217,-0.044239,35.309185>>
					fYogaCameraFOVs[1][1]			= 39.355549
					
					iYogaCameraFailCutTimes[1]		= 0
					
					bYogaCameraFlags[1][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[1][YCF_INTERP]	= FALSE
					bYogaCameraFlags[1][YCF_ACTIVE]	= FALSE
					
					//3rd pose
					vYogaCameraPositions[2][0]		= <<2863.545410,5942.737305,357.634216>>
					vYogaCameraRotations[2][0]		= <<7.496217,-0.044239,35.309185>>
					fYogaCameraFOVs[2][0]			= 39.355549
					
					vYogaCameraPositions[2][1]		= <<2863.545410,5942.737305,357.634216>>
					vYogaCameraRotations[2][1]		= <<7.496217,-0.044239,35.309185>>
					fYogaCameraFOVs[2][1]			= 39.355549
					
					iYogaCameraFailCutTimes[2]		= 0
					
					bYogaCameraFlags[2][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[2][YCF_INTERP]	= FALSE
					bYogaCameraFlags[2][YCF_ACTIVE]	= TRUE
					
					//end pose animation
					vYogaCameraPositions[3][0]		= <<2863.846680,5945.487793,357.874786>>
					vYogaCameraRotations[3][0]		= <<8.808952,0.000863,79.576752>>
					fYogaCameraFOVs[3][0]			= 39.355549
													
					vYogaCameraPositions[3][1]		= <<2863.368408,5945.402344,357.959564>>
					vYogaCameraRotations[3][1]		= <<14.608078,0.000863,76.607201>>
					fYogaCameraFOVs[3][1]			= 39.355549
					
					iYogaCameraInterpTimes[3]		= 3500
					iYogaCameraFailCutTimes[3]		= 0
					
					bYogaCameraFlags[3][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[3][YCF_INTERP]	= TRUE
					bYogaCameraFlags[3][YCF_ACTIVE]	= TRUE
				
				BREAK
			
			ENDSWITCH
			
		BREAK
		
		CASE YOGAMOVE_TRIANGLE
		
			SWITCH iLocation
				
				CASE 0	//Michael's house location
	
					//1st pose
					vYogaCameraPositions[0][0]		= <<-780.270569,181.499008,72.134193>>
					vYogaCameraRotations[0][0]		= <<5.718471,-0.071512,58.085785>>
					fYogaCameraFOVs[0][0]			= 39.355549
					
					vYogaCameraPositions[0][1]		= <<-780.270569,181.499008,72.134193>>
					vYogaCameraRotations[0][1]		= <<5.718471,-0.071512,58.085785>>
					fYogaCameraFOVs[0][1]			= 39.355549
					
					iYogaCameraFailCutTimes[0]		= 0
					
					bYogaCameraFlags[0][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[0][YCF_INTERP]	= FALSE
					bYogaCameraFlags[0][YCF_ACTIVE]	= FALSE
					
					//2nd pose
					vYogaCameraPositions[1][0]		= <<-780.270569,181.499008,72.134193>>
					vYogaCameraRotations[1][0]		= <<5.718471,-0.071512,58.085785>>
					fYogaCameraFOVs[1][0]			= 39.355549
					
					vYogaCameraPositions[1][1]		= <<-780.270569,181.499008,72.134193>>
					vYogaCameraRotations[1][1]		= <<5.718471,-0.071512,58.085785>>
					fYogaCameraFOVs[1][1]			= 39.355549
					
					iYogaCameraFailCutTimes[1]		= 0
					
					bYogaCameraFlags[1][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[1][YCF_INTERP]	= FALSE
					bYogaCameraFlags[1][YCF_ACTIVE]	= TRUE
					
					//3rd pose
					vYogaCameraPositions[2][0]		= <<-788.851196,184.026611,72.534042>>
					vYogaCameraRotations[2][0]		= <<4.066512,-0.006667,50.292328>>
					fYogaCameraFOVs[2][0]			= 35.458775
					
					vYogaCameraPositions[2][1]		= <<-788.851196,184.026611,72.534042>>
					vYogaCameraRotations[2][1]		= <<4.066512,-0.006667,50.292328>>
					fYogaCameraFOVs[2][1]			= 35.458775
					
					iYogaCameraFailCutTimes[2]		= 1500
					
					bYogaCameraFlags[2][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[2][YCF_INTERP]	= FALSE
					bYogaCameraFlags[2][YCF_ACTIVE]	= TRUE
					
					//4th pose
					vYogaCameraPositions[3][0]		= <<-789.059692,187.839645,72.443619>>
					vYogaCameraRotations[3][0]		= <<5.096624,0.047362,143.410507>>
					fYogaCameraFOVs[3][0]			= 38.767960
					
					vYogaCameraPositions[3][1]		= <<-789.059692,187.839645,72.443619>>
					vYogaCameraRotations[3][1]		= <<5.096624,0.047362,143.410507>>
					fYogaCameraFOVs[3][1]			= 38.767960
					
					iYogaCameraFailCutTimes[3]		= 1500
					
					bYogaCameraFlags[3][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[3][YCF_INTERP]	= FALSE
					bYogaCameraFlags[3][YCF_ACTIVE]	= TRUE
					
					//end pose animation
					vYogaCameraPositions[4][0]		= <<-789.059692,187.839645,72.443619>>
					vYogaCameraRotations[4][0]		= <<5.096624,0.047362,143.410507>>
					fYogaCameraFOVs[4][0]			= 38.767960
					
					vYogaCameraPositions[4][1]		= <<-790.052734,187.877930,73.247765>>
					vYogaCameraRotations[4][1]		= <<-0.732900,-0.070113,153.777069>>
					fYogaCameraFOVs[4][1]			= 39.355549
					
					iYogaCameraInterpTimes[4]		= 5000
					iYogaCameraFailCutTimes[4]		= 0
					
					bYogaCameraFlags[4][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[4][YCF_INTERP]	= TRUE
					bYogaCameraFlags[4][YCF_ACTIVE]	= TRUE
					
				BREAK
				
				CASE 1	//North Coast location
					
					//1st pose
					vYogaCameraPositions[0][0]		= <<2864.709473,5941.522461,357.441345>>
					vYogaCameraRotations[0][0]		= <<12.518688,0.000864,32.242397>>
					fYogaCameraFOVs[0][0]			= 39.355549
					
					vYogaCameraPositions[0][1]		= <<2864.709473,5941.522461,357.441345>>
					vYogaCameraRotations[0][1]		= <<12.518688,0.000864,32.242397>>
					fYogaCameraFOVs[0][1]			= 39.355549
					
					iYogaCameraFailCutTimes[0]		= 0
					
					bYogaCameraFlags[0][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[0][YCF_INTERP]	= FALSE
					bYogaCameraFlags[0][YCF_ACTIVE]	= FALSE
					
					//2nd pose
					vYogaCameraPositions[1][0]		= <<2864.709473,5941.522461,357.441345>>
					vYogaCameraRotations[1][0]		= <<12.518688,0.000864,32.242397>>
					fYogaCameraFOVs[1][0]			= 39.355549
					
					vYogaCameraPositions[1][1]		= <<2864.709473,5941.522461,357.441345>>
					vYogaCameraRotations[1][1]		= <<12.518688,0.000864,32.242397>>
					fYogaCameraFOVs[1][1]			= 39.355549
					
					iYogaCameraFailCutTimes[1]		= 0
					
					bYogaCameraFlags[1][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[1][YCF_INTERP]	= FALSE
					bYogaCameraFlags[1][YCF_ACTIVE]	= TRUE
					
					//3rd pose
					vYogaCameraPositions[2][0]		= <<2867.482422,5947.726074,358.183960>>
					vYogaCameraRotations[2][0]		= <<2.762805,-0.036948,111.423454>>
					fYogaCameraFOVs[2][0]			= 39.355549
					
					vYogaCameraPositions[2][1]		= <<2867.482422,5947.726074,358.183960>>
					vYogaCameraRotations[2][1]		= <<2.762805,-0.036948,111.423454>>
					fYogaCameraFOVs[2][1]			= 39.355549
					
					iYogaCameraFailCutTimes[2]		= 1500
					
					bYogaCameraFlags[2][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[2][YCF_INTERP]	= FALSE
					bYogaCameraFlags[2][YCF_ACTIVE]	= TRUE
					
					//4th pose
					vYogaCameraPositions[3][0]		= <<2863.663330,5943.964844,357.335999>>
					vYogaCameraRotations[3][0]		= <<12.750703,0.011422,62.854511>>
					fYogaCameraFOVs[3][0]			= 39.355549
					
					vYogaCameraPositions[3][1]		= <<2863.663330,5943.964844,357.335999>>
					vYogaCameraRotations[3][1]		= <<12.750703,0.011422,62.854511>>
					fYogaCameraFOVs[3][1]			= 39.355549
					
					iYogaCameraFailCutTimes[3]		= 1500
					
					bYogaCameraFlags[3][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[3][YCF_INTERP]	= FALSE
					bYogaCameraFlags[3][YCF_ACTIVE]	= TRUE
					
					//end pose animation
					vYogaCameraPositions[4][0]		= <<2863.663330,5943.964844,357.335999>>
					vYogaCameraRotations[4][0]		= <<12.750703,0.011422,62.854511>>
					fYogaCameraFOVs[4][0]			= 39.355549
					
					vYogaCameraPositions[4][1]		= <<2863.340820,5944.931152,357.734192>>
					vYogaCameraRotations[4][1]		= <<19.264002,-0.043584,59.654526>>
					fYogaCameraFOVs[4][1]			= 39.355549
					
					iYogaCameraInterpTimes[4]		= 5000
					iYogaCameraFailCutTimes[4]		= 0
					
					bYogaCameraFlags[4][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[4][YCF_INTERP]	= TRUE
					bYogaCameraFlags[4][YCF_ACTIVE]	= TRUE
				
				BREAK
				
			ENDSWITCH
		
		BREAK
		
		CASE YOGAMOVE_SUNSALUTATION
		
			SWITCH iLocation
				
				CASE 0	//Michael's house location
		
					//1st pose
					vYogaCameraPositions[0][0]		= <<-790.052734,187.877930,73.247765>>
					vYogaCameraRotations[0][0]		= <<-0.732900,-0.070113,153.777069>>
					fYogaCameraFOVs[0][0]			= 39.355549
					
					vYogaCameraPositions[0][1]		= <<-790.052734,187.877930,73.247765>>
					vYogaCameraRotations[0][1]		= <<-0.732900,-0.070113,153.777069>>
					fYogaCameraFOVs[0][1]			= 39.355549
					
					iYogaCameraFailCutTimes[0]		= 0
					
					bYogaCameraFlags[0][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[0][YCF_INTERP]	= FALSE
					bYogaCameraFlags[0][YCF_ACTIVE]	= TRUE
					
					//2nd pose
					vYogaCameraPositions[1][0]		= <<-789.207397,185.483109,71.911331>>
					vYogaCameraRotations[1][0]		= <<25.596989,-0.035805,57.893848>>
					fYogaCameraFOVs[1][0]			= 38.164177
					
					vYogaCameraPositions[1][1]		= <<-789.207397,185.483109,71.911331>>
					vYogaCameraRotations[1][1]		= <<25.596989,-0.035805,57.893848>>
					fYogaCameraFOVs[1][1]			= 38.164177
					
					iYogaCameraFailCutTimes[1]		= 0
					
					bYogaCameraFlags[1][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[1][YCF_INTERP]	= FALSE
					bYogaCameraFlags[1][YCF_ACTIVE]	= TRUE
					
					//3rd pose
					vYogaCameraPositions[2][0]		= <<-788.837524,183.624146,71.953735>>
					vYogaCameraRotations[2][0]		= <<10.657258,-0.032140,28.324736>>
					fYogaCameraFOVs[2][0]			= 39.141090
					
					vYogaCameraPositions[2][1]		= <<-788.837524,183.624146,71.953735>>
					vYogaCameraRotations[2][1]		= <<10.657258,-0.032140,28.324736>>
					fYogaCameraFOVs[2][1]			= 39.141090
					
					iYogaCameraFailCutTimes[2]		= 1500
					
					bYogaCameraFlags[2][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[2][YCF_INTERP]	= FALSE
					bYogaCameraFlags[2][YCF_ACTIVE]	= TRUE
					
					//4th pose
					vYogaCameraPositions[3][0]		= <<-788.998291,186.104187,72.044945>>
					vYogaCameraRotations[3][0]		= <<5.022902,-0.035508,79.215454>>
					fYogaCameraFOVs[3][0]			= 29.573942
					
					vYogaCameraPositions[3][1]		= <<-788.998291,186.104187,72.044945>>
					vYogaCameraRotations[3][1]		= <<5.022902,-0.035508,79.215454>>
					fYogaCameraFOVs[3][1]			= 29.573942
					
					iYogaCameraFailCutTimes[3]		= 1500
					
					bYogaCameraFlags[3][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[3][YCF_INTERP]	= FALSE
					bYogaCameraFlags[3][YCF_ACTIVE]	= TRUE
					
					//5th pose
					vYogaCameraPositions[4][0]		= <<-790.513062,188.468430,71.985001>>
					vYogaCameraRotations[4][0]		= <<10.313763,-0.036726,167.249725>>
					fYogaCameraFOVs[4][0]			= 31.385864
					
					vYogaCameraPositions[4][1]		= <<-790.513062,188.468430,71.985001>>
					vYogaCameraRotations[4][1]		= <<10.313763,-0.036726,167.249725>>
					fYogaCameraFOVs[4][1]			= 31.385864
					
					iYogaCameraFailCutTimes[4]		= 1500
					
					bYogaCameraFlags[4][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[4][YCF_INTERP]	= FALSE
					bYogaCameraFlags[4][YCF_ACTIVE]	= TRUE
					
					//6th pose
					vYogaCameraPositions[5][0]		= <<-786.631897,188.014984,72.133530>>
					vYogaCameraRotations[5][0]		= <<4.104248,-0.020120,115.940872>>
					fYogaCameraFOVs[5][0]			= 38.674591
					
					vYogaCameraPositions[5][1]		= <<-787.841553,187.671570,72.221588>>
					vYogaCameraRotations[5][1]		= <<4.104248,-0.020120,118.847649>>
					fYogaCameraFOVs[5][1]			= 38.674591
					
					iYogaCameraInterpTimes[5]		= 25000
					iYogaCameraFailCutTimes[5]		= 1500
					
					bYogaCameraFlags[5][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[5][YCF_INTERP]	= TRUE
					bYogaCameraFlags[5][YCF_ACTIVE]	= TRUE
					
					//7th pose
					vYogaCameraPositions[6][0]		= <<-787.474426,188.107056,72.186310>>
					vYogaCameraRotations[6][0]		= <<4.013949,0.013702,116.109329>>
					fYogaCameraFOVs[6][0]			= 38.674591
					
					vYogaCameraPositions[6][1]		= <<-787.474426,188.107056,72.186310>>
					vYogaCameraRotations[6][1]		= <<4.013949,0.013702,116.109329>>
					fYogaCameraFOVs[6][1]			= 38.674591
					
					iYogaCameraFailCutTimes[6]		= 1500
					
					bYogaCameraFlags[6][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[6][YCF_INTERP]	= FALSE
					bYogaCameraFlags[6][YCF_ACTIVE]	= TRUE
					
					//end pose animation
					vYogaCameraPositions[7][0]		= <<-788.856384,186.746552,72.633980>>
					vYogaCameraRotations[7][0]		= <<2.036967,0.013663,104.246063>>
					fYogaCameraFOVs[7][0]			= 38.674591
					
					vYogaCameraPositions[7][1]		= <<-788.856384,186.746552,72.633980>>
					vYogaCameraRotations[7][1]		= <<13.553912,0.013663,104.246063>>
					fYogaCameraFOVs[7][1]			= 38.674591
					
					iYogaCameraInterpTimes[7]		= 3000
					iYogaCameraFailCutTimes[7]		= 1500
					
					bYogaCameraFlags[7][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[7][YCF_INTERP]	= TRUE
					bYogaCameraFlags[7][YCF_ACTIVE]	= TRUE
					
				BREAK
				
				CASE 1	//North Coast location
					
					//1st pose
					vYogaCameraPositions[0][0]		= <<2863.340820,5944.931152,357.734192>>
					vYogaCameraRotations[0][0]		= <<19.264002,-0.043584,59.654526>>
					fYogaCameraFOVs[0][0]			= 39.355549
					
					vYogaCameraPositions[0][1]		= <<2863.340820,5944.931152,357.734192>>
					vYogaCameraRotations[0][1]		= <<19.264002,-0.043584,59.654526>>
					fYogaCameraFOVs[0][1]			= 39.355549
					
					iYogaCameraFailCutTimes[0]		= 0
					
					bYogaCameraFlags[0][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[0][YCF_INTERP]	= FALSE
					bYogaCameraFlags[0][YCF_ACTIVE]	= TRUE
					
					//2nd pose
					vYogaCameraPositions[1][0]		= <<2864.400635,5936.751953,358.648773>>
					vYogaCameraRotations[1][0]		= <<-0.869005,-0.043912,16.599710>>
					fYogaCameraFOVs[1][0]			= 39.355549
					
					vYogaCameraPositions[1][1]		= <<2864.400635,5936.751953,358.648773>>
					vYogaCameraRotations[1][1]		= <<-0.869005,-0.043912,16.599710>>
					fYogaCameraFOVs[1][1]			= 39.355549
					
					iYogaCameraFailCutTimes[1]		= 0
					
					bYogaCameraFlags[1][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[1][YCF_INTERP]	= FALSE
					bYogaCameraFlags[1][YCF_ACTIVE]	= TRUE
					
					//3rd pose
					vYogaCameraPositions[2][0]		= <<2862.145996,5943.193359,357.363007>>
					vYogaCameraRotations[2][0]		= <<9.662308,-0.009141,12.365748>>
					fYogaCameraFOVs[2][0]			= 39.35554
					
					vYogaCameraPositions[2][1]		= <<2862.145996,5943.193359,357.363007>>
					vYogaCameraRotations[2][1]		= <<9.662308,-0.009141,12.365748>>
					fYogaCameraFOVs[2][1]			= 39.35554
					
					iYogaCameraFailCutTimes[2]		= 1500
					
					bYogaCameraFlags[2][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[2][YCF_INTERP]	= FALSE
					bYogaCameraFlags[2][YCF_ACTIVE]	= TRUE
					
					//4th pose
					vYogaCameraPositions[3][0]		= <<2863.212891,5945.368164,357.280640>>
					vYogaCameraRotations[3][0]		= <<7.742859,-0.009142,73.890060>>
					fYogaCameraFOVs[3][0]			= 39.355549
					
					vYogaCameraPositions[3][1]		= <<2863.212891,5945.368164,357.280640>>
					vYogaCameraRotations[3][1]		= <<7.742859,-0.009142,73.890060>>
					fYogaCameraFOVs[3][1]			= 39.355549
					
					iYogaCameraFailCutTimes[3]		= 1500
					
					bYogaCameraFlags[3][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[3][YCF_INTERP]	= FALSE
					bYogaCameraFlags[3][YCF_ACTIVE]	= TRUE
					
					//5th pose
					vYogaCameraPositions[4][0]		= <<2859.712402,5949.607422,357.362701>>
					vYogaCameraRotations[4][0]		= <<10.394834,-0.009141,-152.232468>>
					fYogaCameraFOVs[4][0]			= 39.355549
					
					vYogaCameraPositions[4][1]		= <<2859.712402,5949.607422,357.362701>>
					vYogaCameraRotations[4][1]		= <<10.394834,-0.009141,-152.232468>>
					fYogaCameraFOVs[4][1]			= 39.355549
					
					iYogaCameraFailCutTimes[4]		= 1500
					
					bYogaCameraFlags[4][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[4][YCF_INTERP]	= FALSE
					bYogaCameraFlags[4][YCF_ACTIVE]	= TRUE
					
					//6th pose
					vYogaCameraPositions[5][0]		= <<2870.431641,5948.134277,357.557861>>
					vYogaCameraRotations[5][0]		= <<8.651419,-0.001470,105.720398>>
					fYogaCameraFOVs[5][0]			= 39.355549
					
					vYogaCameraPositions[5][1]		= <<2868.032227,5947.471680,357.798309>>
					vYogaCameraRotations[5][1]		= <<6.054419,0.025583,108.795082>>
					fYogaCameraFOVs[5][1]			= 39.355549
					
					iYogaCameraInterpTimes[5]		= 25000
					iYogaCameraFailCutTimes[5]		= 1500
					
					bYogaCameraFlags[5][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[5][YCF_INTERP]	= TRUE
					bYogaCameraFlags[5][YCF_ACTIVE]	= TRUE
					
					//7th pose
					vYogaCameraPositions[6][0]		= <<2870.431641,5948.134277,357.557861>>
					vYogaCameraRotations[6][0]		= <<8.651419,-0.001470,105.720398>>
					fYogaCameraFOVs[6][0]			= 39.355549
					
					vYogaCameraPositions[6][1]		= <<2870.431641,5948.134277,357.557861>>
					vYogaCameraRotations[6][1]		= <<8.651419,-0.001470,105.720398>>
					fYogaCameraFOVs[6][1]			= 39.355549
					
					iYogaCameraFailCutTimes[6]		= 1500
					
					bYogaCameraFlags[6][YCF_CHANGE]	= FALSE
					bYogaCameraFlags[6][YCF_INTERP]	= FALSE
					bYogaCameraFlags[6][YCF_ACTIVE]	= TRUE
					
					//end pose animation
					vYogaCameraPositions[7][0]		= <<2863.628662,5945.854004,357.845642>>
					vYogaCameraRotations[7][0]		= <<8.768853,-0.026652,91.796844>>
					fYogaCameraFOVs[7][0]			= 39.355549
					
					vYogaCameraPositions[7][1]		= <<2863.184814,5946.305176,358.177734>>
					vYogaCameraRotations[7][1]		= <<8.008410,-0.026652,105.282730>>
					fYogaCameraFOVs[7][1]			= 39.355549
					
					iYogaCameraInterpTimes[7]		= 3000
					iYogaCameraFailCutTimes[7]		= 0
					
					bYogaCameraFlags[7][YCF_CHANGE]	= TRUE
					bYogaCameraFlags[7][YCF_INTERP]	= TRUE
					bYogaCameraFlags[7][YCF_ACTIVE]	= TRUE
				
				BREAK
				
			ENDSWITCH
		
		BREAK
	
	ENDSWITCH
	
	
	//Fail cameras
	SWITCH eMove
		
		CASE YOGAMOVE_WARRIOR
		
			vYogaFailCameraPositions[0]	= <<-790.588867,186.564514,72.969917>>
			vYogaFailCameraRotations[0]	= <<-1.638374,0.042595,-117.007965>>
			vYogaFailCameraFOVs[0]		= 31.157114
			
			vYogaFailCameraPositions[1]	= <<-791.594360,188.758713,72.865608>>
			vYogaFailCameraRotations[1]	= <<0.094575,0.050610,-126.943497>>
			vYogaFailCameraFOVs[1]		= 28.413298

		BREAK
		
		CASE YOGAMOVE_TRIANGLE
		
			vYogaFailCameraPositions[0]	= <<-789.156738,189.344330,72.052315>>
			vYogaFailCameraRotations[0]	= <<12.437154,-0.037231,-164.074646>>
			vYogaFailCameraFOVs[0]		= 19.247688
			
			vYogaFailCameraPositions[1]	= <<-789.346802,185.261658,72.098404>>
			vYogaFailCameraRotations[1]	= <<11.946778,-0.005908,19.566986>>
			vYogaFailCameraFOVs[1]		= 23.454800
		
		BREAK
		
		CASE YOGAMOVE_SUNSALUTATION
		
			vYogaFailCameraPositions[0] = <<-790.521667,187.287933,73.186523>>
			vYogaFailCameraRotations[0] = <<-0.678966,-0.045711,-139.551682>>
			vYogaFailCameraFOVs[0]		= 28.094011
			
			vYogaFailCameraPositions[1] = <<-790.424866,186.153076,73.241264>>
			vYogaFailCameraRotations[1] = <<-0.614909,0.001498,-30.896523>>
			vYogaFailCameraFOVs[1]		= 32.065929
		
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC ANIMATE_STICK_DIRECTION_CHANGE(STICKDIRECTIONS eStartDirection, STICKDIRECTIONS eEndDirection, STICKROTATIONS eRotation,
									INT &iCurrentDirectionValue, INT iMovementSpeed = 1)

	INT iStart 	= ENUM_TO_INT(eStartDirection)
	INT iEnd	= ENUM_TO_INT(eEndDirection)

	IF ( iStart < iEnd )
	
		IF ( eRotation = STICKROTATION_CLOCKWISE )
	
			iCurrentDirectionValue = CLAMP_INT(iCurrentDirectionValue + iMovementSpeed, iStart, iEnd)
			
		ELIF ( eRotation = STICKROTATION_COUNTERCLOCKWISE )
		
			IF ( iCurrentDirectionValue = iStart )
				iCurrentDirectionValue = 360 + iCurrentDirectionValue
			ENDIF
		
			iCurrentDirectionValue = CLAMP_INT(iCurrentDirectionValue - iMovementSpeed, iEnd, 360 + iStart)
			
		ENDIF
	
	ELIF ( iStart >= iEnd )

		IF ( eRotation = STICKROTATION_CLOCKWISE)
		
			iCurrentDirectionValue = CLAMP_INT(iCurrentDirectionValue + iMovementSpeed, iStart, 360 + iEnd)
		
			IF ( iCurrentDirectionValue = 360 + iEnd )
				iCurrentDirectionValue = iEnd
			ENDIF
		
		ELIF ( eRotation = STICKROTATION_COUNTERCLOCKWISE )
		
			iCurrentDirectionValue = CLAMP_INT(iCurrentDirectionValue - iMovementSpeed, iEnd, iStart)
		
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets all scaleform yoga button flags to FALSE.
PROC CLEAR_SCALEFORM_YOGA_BUTTONS(BOOL &Array[])

	INT i = 0

	FOR i = 0 TO ( COUNT_OF(Array) - 1 )
	
		Array[i] = FALSE
	
	ENDFOR

ENDPROC

PROC CLEANUP_AND_REMOVE_YOGA_SCALEFORM(SCALEFORM_INDEX &ScaleformIndex)

	bShowBreathingButtons 	= FALSE
	bBreathingButtonsAdded 	= FALSE
	
	iLeftRed 	= 255
	iLeftGreen 	= 255
	iLeftBlue 	= 255
	
	iRightRed 	= 255
	iRightGreen = 255
	iRightBlue 	= 255
	
	CLEAR_SCALEFORM_YOGA_BUTTONS(bScaleformYogaButtonsPressed)
	
	CALL_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "REMOVE_BUTTONS")
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformIndex)

ENDPROC

/// PURPOSE:
///    Resets yoga structure variables to default values.
/// PARAMS:
///    Yoga - Yoga struct instance to reset.
PROC RESET_YOGA_STRUCT(YOGASTRUCT& Yoga, BOOL bResetTimecycleModifierValues = FALSE, BOOL bResetBlushDecalAlphaValues = FALSE)

	Yoga.PedIndex 					= NULL
	Yoga.vPosition 					= << 0.0, 0.0, 0.0 >>
	Yoga.vRotation 					= << 0.0, 0.0, 0.0 >>
	Yoga.eYogaMinigameStage 		= YOGAMINIGAMESTAGE_SETUP
	Yoga.eYogaMoveStage 			= YOGAMOVESTAGE_DOINGPOSES
	Yoga.iTimer 					= 0
	Yoga.iShakeTimer 				= 0
	Yoga.iFailCounter 				= 0
	Yoga.iPosesCompleted 			= 0
	Yoga.iBreathsCompleted			= 0
	Yoga.iBreathingCounter 			= 0
	
	Yoga.bHelpTextDelayDone 		= FALSE
	Yoga.bAmbientSpeechPlayed		= FALSE
	
	IF ( bResetTimecycleModifierValues = TRUE )
	
		Yoga.fModifierStrength 		= 0.0
		Yoga.fModifierStrengthMax 	= 0.0
	
	ENDIF
	
	IF ( bResetBlushDecalAlphaValues = TRUE )
	
		Yoga.fBlushDecalAlpha 		= 0.0
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks if a given ped is at the last stage of their task sequence.
///    Checks if ped's task sequence progress equals number of stages passed - 1.
///    Can be used to determine if ped has finished playing one animation and is ready to play the next one
///    without visible animation popping from one anim to another.
/// PARAMS:
///    aPed - Ped to chek for task sequence progress.
///    iNumberOfTasksInSequence - Number of tasks in the sequence
/// RETURNS:
///    TRUE if ped is at their last task in the sequence. FALSE if otherwise.
FUNC BOOL IS_PED_AT_LAST_STAGE_OF_SEQUENCE(PED_INDEX aPed, INT iNumberOfTasksInSequence)
		
	IF NOT IS_PED_INJURED(aPed)
		IF GET_SCRIPT_TASK_STATUS(aPed, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
			IF iNumberOfTasksInSequence < 2
				RETURN TRUE
			ENDIF
			IF GET_SEQUENCE_PROGRESS(aPed) = iNumberOfTasksInSequence - 1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC UPDATE_YOGA_FACIAL_ANIMATIONS(YOGASTRUCT &Yoga)

	IF NOT IS_PED_INJURED(Yoga.PedIndex)

		IF ( Yoga.bChangePoseFacial = TRUE )

			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "A1_POSE")
			OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C1_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "A1ANDC1_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation A1ANDC1_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "A2_POSE")
			OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C2_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "A2ANDC2_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation A2ANDC2_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "A3_POSE")
			OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C3_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "A3ANDC3_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation A3ANDC3_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "B4_POSE")
			OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C4_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "B4ANDC4_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation B4ANDC4_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C5_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "C5_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation C5_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C6_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "C6_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation C6_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C7_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "C7_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation C7_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "C8_POSE")
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex, "C8_FACE", Yoga.sYogaAnimDict)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation C8_FACE.")
				#ENDIF
				Yoga.bChangePoseFacial = FALSE
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Plays a sequence of animations on given yoga struct.
/// PARAMS:
///    Yoga - Given yoga struct to play animation on.
///    sAnim1 - First animation in the sequence.
///    sAnim2 - Second animation in the sequence.
PROC PLAY_YOGAPOSE_ANIMATION(YOGASTRUCT &Yoga, STRING sAnim1, STRING sAnim2)

	IF NOT IS_PED_INJURED(Yoga.PedIndex)
	
		SEQUENCE_INDEX SequenceIndex
		
		CLEAR_SEQUENCE_TASK(SequenceIndex)

		OPEN_SEQUENCE_TASK(SequenceIndex)
		
			TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, sAnim1, Yoga.vPosition, Yoga.vRotation , SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
									AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)

			TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, sAnim2 , Yoga.vPosition, Yoga.vRotation , SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
									AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_LOOPING, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)

		CLOSE_SEQUENCE_TASK(SequenceIndex)
		
		TASK_PERFORM_SEQUENCE(Yoga.PedIndex, SequenceIndex)
		
		CLEAR_SEQUENCE_TASK(SequenceIndex)
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets speed of currently played animation to a specified value.
/// PARAMS:
///    Yoga - YOGASTRUCT to play animation on.
///    eMove - Currently performed yoga move.
///    fSpeed - Desired speed of the animation.
PROC SET_YOGAPOSE_ANIMATION_SPEED(YOGASTRUCT &Yoga, YOGAMOVE eMove, FLOAT fSpeed)

	INT i = 0
	
	FOR i = 0 TO iAnimations[eMove] - 1
	
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, sYogaPosesAnims[eMove][i])
		
			SET_ENTITY_ANIM_SPEED(Yoga.PedIndex, Yoga.sYogaAnimDict, sYogaPosesAnims[eMove][i], fSpeed)
		
		ENDIF
	
	ENDFOR

ENDPROC

FUNC INT GET_STICK_ROTATION_ANGLE(STICKS eStick)
	
	INT iLeftStickAngle, iRightStickAngle
	INT iLeftX, iLeftY, iRightX, iRightY
	INT iTempX, iTempY

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLeftX, iLeftY, iRightX, iRightY)

	// On PC We're using a mouse to replace one of the sticks, and as a mouse doesn't have a 'not pressed' state - it's moving or not moving -
	// we need to make the input values 'sticky' so the arrow stays in position when you stop moving the mouse.
	
	//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.1, "NUMBER", iRightX )
	//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.2, "NUMBER", iRightY )
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
//		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", iLeftX )
//		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.2, "NUMBER", iLeftY )
//			
//		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", iRightX )
//		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.2, "NUMBER", iRightY )
//		

		// Switch sticks if yoga mouse and keyboard southpaw mode active.
		IF bSouthpaw
			
			iTempX = iRightX
			iTempY = iRightY
			iRightX = iLeftX
			iRightY = iLeftY
			iLeftX = iTempX
			iLeftY = iTempY
			
			iLeftX = iLeftX / MOUSE_SENSITIVITY
			iLeftY = iLeftY / MOUSE_SENSITIVITY
			
			// Make input 'sticky' so it doesn't auto-centre.
			IF ( iLeftX = 0 OR iLeftY = 0 )
			
				iLeftX = iStickXPrev
				iLeftY = iStickYPrev
			
			ENDIF
			
			iStickXPrev = iLeftX
			iStickYPrev = iLeftY
		
		ELSE
		
			iRightX = iRightX / MOUSE_SENSITIVITY
			iRightY = iRightY / MOUSE_SENSITIVITY
	
			// Make input 'sticky' so it doesn't auto-centre.
			IF ( iRightX = 0 OR iRightY = 0 )
			
				iRightX = iStickXPrev
				iRightY = iStickYPrev
			
			ENDIF
			
			iStickXPrev = iRightX
			iStickYPrev = iRightY
		ENDIF
	
	ENDIF
	
	iLeftStickAngle		= ROUND(GET_ANGLE_BETWEEN_2D_VECTORS(0, -127, TO_FLOAT(iLeftX), TO_FLOAT(iLeftY)))
	iRightStickAngle 	= ROUND(GET_ANGLE_BETWEEN_2D_VECTORS(0, -127, TO_FLOAT(iRightX), TO_FLOAT(iRightY)))
	
	IF ( iLeftX < 0 )
		iLeftStickAngle = 180 + (180 - iLeftStickAngle)
	ENDIF
	
	IF ( iRightX < 0 )
		iRightStickAngle = 180 + (180 - iRightStickAngle)
	ENDIF
	
	SWITCH eStick
	
		CASE STICK_LEFT
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iLeftStickAngle), << 0.1, 0.1, 0.0 >>)
			#ENDIF
		
			RETURN iLeftStickAngle
			
		BREAK
		
		CASE STICK_RIGHT
		
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iRightStickAngle), << 0.3, 0.1, 0.0 >>)
			#ENDIF
		
			RETURN iRightStickAngle
			
		BREAK
		
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

FUNC BOOL IS_STICK_IN_DEAD_ZONE(STICKS eStick)

	INT 	iLeftX, iLeftY, iRightX, iRightY
	FLOAT	fMagnitude
	INT		iTempX, iTempY


	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
	
	// Switch sticks if yoga southpaw mouse and keyboard mode active.
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF bSouthpaw
			iTempX = iRightX
			iTempY = iRightY
			iRightX = iLeftX
			iRightY = iLeftY
			iLeftX = iTempX
			iLeftY = iTempY
		ENDIF
	ENDIF
	
	SWITCH eStick
		CASE STICK_LEFT
		
			// On PC we can't deadzone the left stick in southpaw stick as this is a mouse, and causes problems.
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			AND iStickXPrev != 0 
			AND iStickYPrev != 0
			AND bSouthpaw = TRUE
				RETURN FALSE
			ENDIF
		
			fMagnitude = VMAG(<< iLeftX, iLeftY, 0 >>)
		
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iLeftX), << 0.1, 0.5, 0.0 >>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iLeftY), << 0.2, 0.5, 0.0 >>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fMagnitude), << 0.4, 0.6, 0.0 >>)
			#ENDIF

			IF fMagnitude < 100.0
			
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("DEAD ZONE", << 0.3, 0.5, 0.0 >>)
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE STICK_RIGHT
			
			// On PC we can't deadzone the right stick as this is a mouse, and causes problems.
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			AND iStickXPrev != 0 
			AND iStickYPrev != 0
			AND bSouthpaw = FALSE
				RETURN FALSE
			ENDIF
			
			fMagnitude = VMAG(<< iRightX, iRightY, 0 >>)
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iRightX), << 0.1, 0.6, 0.0 >>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iRightY), << 0.2, 0.6, 0.0 >>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fMagnitude), << 0.4, 0.7, 0.0 >>)
			#ENDIF

			IF fMagnitude < 100.0
			
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("DEAD ZONE", << 0.3, 0.6, 0.0 >>)
				#ENDIF
			
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_STICK_AT_DIRECTION(INT &iAngle, BOOL &bDeadZone, STICKS eStick, STICKDIRECTIONS eDirection, STICKPOSITIONSCHECKS eStickPositionsCheck)
	
	iAngle 		= GET_STICK_ROTATION_ANGLE(eStick)
	bDeadZone 	= IS_STICK_IN_DEAD_ZONE(eStick)
	
	IF ( bDeadZone = FALSE )
	
		SWITCH eStickPositionsCheck
	
			CASE STICKPOSITIONCHECK_MOVE
			
				SWITCH eDirection
					CASE STICKDIRECTION_UP
						IF iAngle >= 345
						OR iAngle <= 15
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_UPRIGHT
						IF 	iAngle >= 30
						AND iAngle <= 60
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_RIGHT
						IF 	iAngle >= 75
						AND iAngle <= 105
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_DOWNRIGHT
						IF 	iAngle >= 120
						AND iAngle <= 150
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_DOWN
						IF 	iAngle >= 165
						AND iAngle <= 195
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_DOWNLEFT
						IF 	iAngle >= 210
						AND iAngle <= 240
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_LEFT
						IF 	iAngle >= 255
						AND iAngle <= 285
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_UPLEFT
						IF 	iAngle >= 300
						AND iAngle <= 330
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			
			BREAK
	
			CASE STICKPOSITIONCHECK_HOLD
			
				SWITCH eDirection
					CASE STICKDIRECTION_UP
						IF 	iAngle >= 305
						OR 	iAngle <= 55
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_UPRIGHT
						IF 	iAngle >= 350
						OR 	iAngle <= 100
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_RIGHT
						IF 	iAngle >= 35
						AND iAngle <= 145
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_DOWNRIGHT
						IF 	iAngle >= 80
						AND iAngle <= 190
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_DOWN
						IF 	iAngle >= 125
						AND iAngle <= 235
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_DOWNLEFT
						IF 	iAngle >= 170
						AND iAngle <= 280
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_LEFT
						IF 	iAngle >= 215
						AND iAngle <= 325
							RETURN TRUE
						ENDIF
					BREAK
					CASE STICKDIRECTION_UPLEFT
						IF 	iAngle >= 260
						OR 	iAngle <= 10 
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			
			BREAK
			
		ENDSWITCH
			
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL ARE_STICKS_IN_CORRECT_POSITION(YOGASTRUCT &Yoga, YOGAMOVE eMove, INT iPoseNumber, STICKPOSITIONSCHECKS eStickPositionsCheck)

	INT iAlpha
	
	ShakeIntensity = 0.0
	
	iLeftRed = 255
	iLeftGreen = 255
	iLeftBlue = 255

	iRightRed = 255
	iRightGreen = 255
	iRightBlue = 255	
	
	BOOL bLeftStickOK = FALSE
	BOOL bRightStickOK = FALSE

	IF IS_STICK_AT_DIRECTION(Yoga.iLeftStickCurrentAngle, Yoga.bLeftStickInDeadZone, STICK_LEFT, eStickDirections[eMove][iPoseNumber][STICK_LEFT], eStickPositionsCheck)
		ShakeIntensity = ShakeIntensity + 30
		
		GET_HUD_COLOUR(HUD_COLOUR_YOGA, iLeftRed, iLeftGreen, iLeftBlue, iAlpha)

		bLeftStickOK = TRUE
		
	ENDIF
	
	IF IS_STICK_AT_DIRECTION(Yoga.iRightStickCurrentAngle, Yoga.bRightStickInDeadZone, STICK_RIGHT, eStickDirections[eMove][iPoseNumber][STICK_RIGHT], eStickPositionsCheck)
		ShakeIntensity = ShakeIntensity + 30
		
		GET_HUD_COLOUR(HUD_COLOUR_YOGA, iRightRed, iRightGreen, iRightBlue, iAlpha)

		bRightStickOK = TRUE
		
	ENDIF

	ShakeIntensity = GET_MINIMUM_FLOAT(ShakeIntensity, 100.0)
	
	SET_CONTROL_SHAKE(PLAYER_CONTROL, 10, FLOOR(ShakeIntensity))

	IF 	bLeftStickOK
	AND bRightStickOK
	
		RETURN TRUE
	
	ELSE
	
		RETURN FALSE
	
	ENDIF

ENDFUNC

PROC DO_SCALEFORM_ICON_UPDATE(YOGASTRUCT &Yoga, YOGAMOVE eMove, SCALEFORM_INDEX &siLocalYogaScaleform)
		
	// PC scaleform
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		IF bSouthpaw	
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_KEYS_WITH_STICK")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_STICK_WITH_KEYS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			END_SCALEFORM_MOVIE_METHOD()	
		
		ELSE

			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_STICK_WITH_KEYS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()	
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_KEYS_WITH_STICK")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			END_SCALEFORM_MOVIE_METHOD()
		
		ENDIF
		
		// Display keyboard and mouse help
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STICKS")
			PRINT_HELP_FOREVER("STICKS_KM")
		ENDIF

	ELSE
		
		// Display gamepad help
		IF IS_PC_VERSION()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STICKS_KM")
				PRINT_HELP_FOREVER("STICKS")
			ENDIF
		ENDIF

	ENDIF
	
	//left stick rotation
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_ANGLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eStickDirections[eMove][Yoga.iPosesCompleted][STICK_LEFT]))
	END_SCALEFORM_MOVIE_METHOD()
	
	//right stick rotation
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_ANGLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eStickDirections[eMove][Yoga.iPosesCompleted][STICK_RIGHT]))
	END_SCALEFORM_MOVIE_METHOD()

	//left stick colour
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_RGB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftBlue)
	END_SCALEFORM_MOVIE_METHOD()
	
	//right stick colour
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_RGB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightBlue)
	END_SCALEFORM_MOVIE_METHOD()


	IF ( Yoga.bLeftStickInDeadZone  = TRUE )
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "HIDE_STICK_POINTER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_HIGHLIGHT_ANGLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Yoga.iLeftStickCurrentAngle)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF ( Yoga.bRightStickInDeadZone  = TRUE )
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "HIDE_STICK_POINTER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_HIGHLIGHT_ANGLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Yoga.iRightStickCurrentAngle)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

PROC DO_SCALEFORM_HIDDEN_INPUT_ICON_UPDATE(YOGASTRUCT &Yoga, SCALEFORM_INDEX &siLocalYogaScaleform)
	
		
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		IF bSouthpaw
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_KEYS_WITH_STICK")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()	
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_STICK_WITH_KEYS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			END_SCALEFORM_MOVIE_METHOD()	
			
		ELSE
		
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_STICK_WITH_KEYS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()	
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_KEYS_WITH_STICK")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			END_SCALEFORM_MOVIE_METHOD()
			
		ENDIF

	ENDIF
	
	//hide player input on left stick
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "HIDE_STICK_POINTER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	//hide player input on right stick
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "HIDE_STICK_POINTER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	END_SCALEFORM_MOVIE_METHOD()
	
	//left stick rotation
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_ANGLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Yoga.iLeftStickPosition)
	END_SCALEFORM_MOVIE_METHOD()
	
	//right stick rotation
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_ANGLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Yoga.iRightStickPosition)
	END_SCALEFORM_MOVIE_METHOD()
	
	//left stick colour
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_RGB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftBlue)
	END_SCALEFORM_MOVIE_METHOD()
	
	//right stick colour
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_STICK_POINTER_RGB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightBlue)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC DO_SCALEFORM_ADD_BREATHING_BUTTONS(SCALEFORM_INDEX &siLocalYogaScaleform)

	CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siLocalYogaScaleform,"ADD_BUTTON_TO_LIST",TO_FLOAT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFTSHOULDER2)))
	CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siLocalYogaScaleform,"ADD_BUTTON_TO_LIST",TO_FLOAT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHTSHOULDER2)))
	CALL_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform,"DRAW_BUTTONS")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_PLAYER_INPUT_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFTSHOULDER2))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_PLAYER_INPUT_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHTSHOULDER2))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DO_SCALEFORM_BREATHING_UPDATE(YOGASTRUCT &Yoga, SCALEFORM_INDEX &siLocalYogaScaleform, INT iRed, INT iGreen, INT iBlue)

	//left trigger button press
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "BUTTON_PRESSED")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFTSHOULDER2))
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "BUTTON_DEPRESSED")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFTSHOULDER2))
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	//right trigger button press
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "BUTTON_PRESSED")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHTSHOULDER2))
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "BUTTON_DEPRESSED")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHTSHOULDER2))
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	//left trigger button circle
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_BUTTON_TARGET")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFTSHOULDER2))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(Yoga.fMaxRange)			//background circle scale
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)							//background circle
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)						//red		0-255
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)					//green		0-255
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)						//blue		0-255
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(50)						//alpha		0-100
	END_SCALEFORM_MOVIE_METHOD()

	
	//right trigger button circle
	BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "SET_BUTTON_TARGET")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHTSHOULDER2))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(Yoga.fMaxRange)			//background circle scale
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)							//background circle
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)						//red		0-255
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)					//green		0-255
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)						//blue		0-255
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(50)						//alpha		0-100
	END_SCALEFORM_MOVIE_METHOD()
	
//	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
//		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_STICK_WITH_KEYS")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//		END_SCALEFORM_MOVIE_METHOD()
//		
//		BEGIN_SCALEFORM_MOVIE_METHOD(siLocalYogaScaleform, "REPLACE_STICK_WITH_KEYS")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
ENDPROC

PROC DRAW_YOGAMOVE_SCALEFORM(YOGASTRUCT &Yoga, YOGAMOVE eMove)
	
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
	
	IF ( GET_IS_WIDESCREEN() = FALSE )
	OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_KOREAN) 	)
	OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_CHINESE) 	)
	OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_JAPANESE) )
	OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_CHINESE_SIMPLIFIED))
		YOGA_BUTTONS_Y = 0.825
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			DRAW_SCALEFORM_MOVIE(siYogaScaleformKBM, YOGA_BUTTONS_X, YOGA_BUTTONS_Y,
							 YOGA_BUTTONS_WIDTH * YOGA_BUTTONS_SCALE, YOGA_BUTTONS_HEIGHT * YOGA_BUTTONS_SCALE, 100, 100, 100, 255)
		ELSE
			DRAW_SCALEFORM_MOVIE(siYogaScaleform, YOGA_BUTTONS_X, YOGA_BUTTONS_Y,
							 YOGA_BUTTONS_WIDTH * YOGA_BUTTONS_SCALE, YOGA_BUTTONS_HEIGHT * YOGA_BUTTONS_SCALE, 100, 100, 100, 255)
		ENDIF
		
					
	ENDIF
	
	IF ( Yoga.eYogaMoveStage <> YOGAMOVESTAGE_WAIT_BETWEEN_POSES )
	
		IF ( Yoga.iPosesCompleted < iPoses[eMove] )
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				DO_SCALEFORM_ICON_UPDATE(Yoga, eMove, siYogaScaleformKBM)
			ELSE
				DO_SCALEFORM_ICON_UPDATE(Yoga, eMove, siYogaScaleform)
			ENDIF
			

		ENDIF
		
	ELIF ( Yoga.eYogaMoveStage = YOGAMOVESTAGE_WAIT_BETWEEN_POSES )

		iLeftRed 	= 255
		iLeftGreen 	= 255
		iLeftBlue 	= 255
		
		iRightRed 	= 255
		iRightGreen = 255
		iRightBlue 	= 255
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			DO_SCALEFORM_HIDDEN_INPUT_ICON_UPDATE(Yoga, siYogaScaleformKBM)
		ELSE
			DO_SCALEFORM_HIDDEN_INPUT_ICON_UPDATE(Yoga, siYogaScaleform)
		ENDIF

	ENDIF
	
	IF ( bShowBreathingButtons = TRUE )

		IF ( bBreathingButtonsAdded = FALSE )
		
			DO_SCALEFORM_ADD_BREATHING_BUTTONS(siYogaScaleform)
			
			IF IS_PC_VERSION()
				DO_SCALEFORM_ADD_BREATHING_BUTTONS(siYogaScaleformKBM)
			ENDIF			
			
			bBreathingButtonsAdded = TRUE
			
		ENDIF
		
		IF ( Yoga.eYogaMoveStage = YOGAMOVESTAGE_BREATHE )
		
			INT iRed, iGreen, iBlue, iAlpha
			
			GET_HUD_COLOUR(HUD_COLOUR_YOGA, iRed, iGreen, iBlue, iAlpha)
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				DO_SCALEFORM_BREATHING_UPDATE(Yoga, siYogaScaleformKBM, iRed, iGreen, iBlue)
			ELSE
				DO_SCALEFORM_BREATHING_UPDATE(Yoga, siYogaScaleform, iRed, iGreen, iBlue)
			ENDIF
		
		ENDIF
		
	ELSE
	
		CALL_SCALEFORM_MOVIE_METHOD(siYogaScaleform, "REMOVE_BUTTONS")
		
		IF IS_PC_VERSION()
			CALL_SCALEFORM_MOVIE_METHOD(siYogaScaleformKBM, "REMOVE_BUTTONS")
		ENDIF
		
		CLEAR_SCALEFORM_YOGA_BUTTONS(bScaleformYogaButtonsPressed)
	
	ENDIF

ENDPROC

FUNC BOOL IS_INHALING_COMPLETED(YOGASTRUCT &Yoga, INT iMaxValue, INT iBottomValue, INT iTopValue)

	INT iLeftShoulderValue 	= ROUND(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LT) * 255.0)
	INT iRightShoulderValue = ROUND(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT) * 255.0)
	
	IF ( iLeftShoulderValue > 0 AND iRightShoulderValue > 0 )
		Yoga.iBreathingValue 	= CLAMP_INT(GET_BREATHING_VALUE(iLeftShoulderValue, iRightShoulderValue), iBottomValue, iTopValue)
	ELSE
		Yoga.iBreathingValue 	= iBottomValue
	ENDIF

	IF ( Yoga.bInhaleInProgress = TRUE )
	
		IF ( iMaxValue = iTopValue )
		
			RETURN TRUE

		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_EXHALING_COMPLETED(YOGASTRUCT &Yoga, INT iMinValue, INT iMaxValue, INT iBottomValue, INT iTopValue)

	INT iLeftShoulderValue 	= ROUND(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LT) * 255.0)
	INT iRightShoulderValue = ROUND(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT) * 255.0)
	
	IF ( iLeftShoulderValue > 0 AND iRightShoulderValue > 0 )
		Yoga.iBreathingValue = CLAMP_INT(GET_BREATHING_VALUE(iLeftShoulderValue, iRightShoulderValue), iBottomValue, iTopValue)
	ELSE
		Yoga.iBreathingValue = iBottomValue
	ENDIF

	IF ( Yoga.bExhaleInProgress = TRUE )
	
		IF ( iMaxValue = iBottomValue )
			
			IF IS_BREATHING_VALUE_IN_BUTTON_TARGET(Yoga.iBreathingValue, iMinValue, iMaxValue)
		
				RETURN TRUE

			ENDIF
		ENDIF
	
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_YOGAMOVE_COMPLETED(YOGASTRUCT &Yoga, YOGAMOVE eMove, INT iBreathingAttempts = 3, BOOL bPlayEndPoseAnimation = FALSE,
								FLOAT fAnimationPlaybackSpeed = 1.0, BOOL bDisplayHelpText = TRUE, BOOL bDelayBreathing = FALSE)
	
	BOOL bIsPCHelpDisplayed = FALSE
	
	SWITCH Yoga.eYogaMoveStage
	
		CASE YOGAMOVESTAGE_DOINGPOSES
				
			IF ARE_STICKS_IN_CORRECT_POSITION(Yoga, eMove, Yoga.iPosesCompleted, STICKPOSITIONCHECK_MOVE)

				Yoga.iTimer = GET_GAME_TIMER()

				PLAY_YOGAPOSE_ANIMATION(Yoga, sYogaPosesAnims[eMove][Yoga.iPosesCompleted * 2], sYogaPosesAnims[eMove][(Yoga.iPosesCompleted * 2) + 1])
				
				Yoga.bChangeCamera		= TRUE
				Yoga.bChangePoseFacial 	= TRUE
				Yoga.eYogaMoveStage 	= YOGAMOVESTAGE_PLAY_POSE_ANIMATION
				
			ELSE
		
				IF ( bDisplayHelpText = TRUE )
				
					// Wrap this to ensure it doesn't fire on console, as PC text isn't included.
					IF IS_PC_VERSION()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STICKS_KM")
							bIsPCHelpDisplayed = TRUE
						ENDIF
					ENDIF
				
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STICKS")
					AND bIsPCHelpDisplayed = FALSE
						IF 	IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
							IF NOT HELP_STICKS_TRIGGERED
								
								IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
									PRINT_HELP_FOREVER("STICKS_KM")
								ELSE
									PRINT_HELP_FOREVER("STICKS")
								ENDIF
								
								HELP_STICKS_TRIGGERED = TRUE
								HELP_INHALE_TRIGGERED = FALSE
								HELP_EXHALE_TRIGGERED = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				bShowBreathingButtons = FALSE
				bBreathingButtonsAdded = FALSE
				
			ENDIF

		BREAK
		
		CASE YOGAMOVESTAGE_PLAY_POSE_ANIMATION

			IF ARE_STICKS_IN_CORRECT_POSITION(Yoga, eMove, Yoga.iPosesCompleted, STICKPOSITIONCHECK_HOLD)
		
				SET_YOGAPOSE_ANIMATION_SPEED(Yoga, eMove, fAnimationPlaybackSpeed)
			
				IF IS_PED_AT_LAST_STAGE_OF_SEQUENCE(Yoga.PedIndex, 2)
					
					SETTIMERA(0)
					CLEAR_HELP()
					
					bShowBreathingButtons = TRUE
					
					Yoga.eYogaMoveStage 		= YOGAMOVESTAGE_BREATHE
					
					Yoga.eYogaBreatheStage		= YOGABREATHESTAGES_START_DELAY
					
					Yoga.iHelpTextTimer			= GET_GAME_TIMER()
					
					IF 	( bDelayBreathing = TRUE AND Yoga.bHelpTextDelayDone = FALSE )
						Yoga.eYogaBreatheStage	= YOGABREATHESTAGES_DISPLAY_HELP
					ENDIF
					
					Yoga.bInhaleSoundTriggered 	= FALSE
					Yoga.bExhaleSoundTriggered 	= FALSE
					
					Yoga.fMaxRange				= 0.0
					Yoga.fMinRange				= 0.0
					
					Yoga.iInhaleTimer			= 0
					Yoga.bInhaleInProgress		= FALSE
					
					Yoga.iExhaleTimer			= 0
					Yoga.bExhaleInProgress		= FALSE
					
					Yoga.bInhaleCompleted		= FALSE
					Yoga.bExhaleCompleted		= FALSE
					
					Yoga.bBreathCompleted		= FALSE
					Yoga.bBreathingPassed 		= FALSE

					Yoga.iBreathingCounter		= 0
					Yoga.iBreathsCompleted		= 0
					
				ENDIF
				
			ELSE
			
				Yoga.eYogaMoveStage = YOGAMOVESTAGE_FAILED
			
			ENDIF
		
		BREAK
		
		CASE YOGAMOVESTAGE_BREATHE

			IF ARE_STICKS_IN_CORRECT_POSITION(Yoga, eMove, Yoga.iPosesCompleted, STICKPOSITIONCHECK_HOLD)
				
				SWITCH Yoga.eYogaBreatheStage
				
					CASE YOGABREATHESTAGES_DISPLAY_HELP
					
						IF ( TIMERA() > 100 )
							IF ( bDisplayHelpText = TRUE )
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("INHALE_NEW")
									IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
										IF NOT HELP_INHALE_TRIGGERED
											PRINT_HELP_FOREVER("INHALE_NEW")
											HELP_STICKS_TRIGGERED = FALSE
											HELP_INHALE_TRIGGERED = TRUE
											HELP_EXHALE_TRIGGERED = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF HAS_TIME_PASSED_ON_TIMER(2000, Yoga.iHelpTextTimer)
						
							Yoga.bHelpTextDelayDone	= TRUE
						
							Yoga.eYogaBreatheStage 	= YOGABREATHESTAGES_INHALE
							
						ENDIF
					
					BREAK
					
					CASE YOGABREATHESTAGES_START_DELAY
					
						IF ( TIMERA() > 100 )
							IF ( bDisplayHelpText = TRUE )
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("INHALE_NEW")
									IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
										IF NOT HELP_INHALE_TRIGGERED
											PRINT_HELP_FOREVER("INHALE_NEW")
											HELP_STICKS_TRIGGERED = FALSE
											HELP_INHALE_TRIGGERED = TRUE
											HELP_EXHALE_TRIGGERED = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF HAS_TIME_PASSED_ON_TIMER(500, Yoga.iHelpTextTimer)
						
							Yoga.eYogaBreatheStage 	= YOGABREATHESTAGES_INHALE
							
						ENDIF
					
					BREAK
					
					CASE YOGABREATHESTAGES_INHALE
					
						IF ( TIMERA() > 100 )
							IF ( bDisplayHelpText = TRUE )
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("INHALE_NEW")
									IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
										IF NOT HELP_INHALE_TRIGGERED
											PRINT_HELP_FOREVER("INHALE_NEW")
											HELP_STICKS_TRIGGERED = FALSE
											HELP_INHALE_TRIGGERED = TRUE
											HELP_EXHALE_TRIGGERED = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF ( Yoga.bInhaleSoundTriggered = FALSE )
						
							IF 	IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
							AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
							
								IF 	HAS_SOUND_FINISHED(Yoga.iInhaleSoundID)
								AND	HAS_SOUND_FINISHED(Yoga.iExhaleSoundID)
								
									PLAY_FACIAL_ANIM(Yoga.PedIndex, "michael_breathing_face", Yoga.sYogaAnimDict)
									PLAY_SOUND_FROM_ENTITY(Yoga.iInhaleSoundID, "YOGA_INHALE", Yoga.PedIndex, "FAMILY_5_SOUNDS")
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sound YOGA_INHALE.")
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iInhaleSoundID is ", Yoga.iInhaleSoundID, ".")
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation michael_breathing_face.")
									#ENDIF
									
									Yoga.fMaxRange				= 40.0
									Yoga.fMinRange				= 30.0
									Yoga.bInhaleSoundTriggered 	= TRUE
									Yoga.bExhaleSoundTriggered 	= FALSE
									Yoga.iBreatheDelayTimer		= 0
									
									Yoga.bInhaleCompleted 		= FALSE
									Yoga.bExhaleCompleted 		= FALSE
									Yoga.bBreathCompleted		= FALSE
									
									Yoga.bInhaleFailed			= FALSE
									Yoga.bExhaleFailed			= FALSE

									Yoga.bInhaleInProgress 		= TRUE
									Yoga.bExhaleInProgress 		= FALSE
									
								ENDIF
								
							ENDIF
							
						ELSE
							
							Yoga.fMaxRange = CLAMP(Yoga.fMaxRange +@Yoga.fValue, 40.0, 100.0)					//slow down the time it takes to
							Yoga.fMinRange = CLAMP(Yoga.fMinRange +@Yoga.fValue, 30.0, 90.0)					//fill the breathing circle
							
							IF 	HAS_SOUND_FINISHED(Yoga.iInhaleSoundID)
							AND	HAS_SOUND_FINISHED(Yoga.iExhaleSoundID)
							AND ( Yoga.fMinRange = 90.0 AND Yoga.fMaxRange = 100.0 )
								
								IF ( Yoga.iBreatheDelayTimer = 0 )
								
									Yoga.iBreatheDelayTime	= 1200 + GET_RANDOM_INT_IN_RANGE(0, 401)			//more time between inhale & exhale
									Yoga.iBreatheDelayTimer = GET_GAME_TIMER()
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iBreatheDelayTime is ", Yoga.iBreatheDelayTime, ".")
									#ENDIF
									
								ELSE
								
									IF HAS_TIME_PASSED_ON_TIMER(Yoga.iBreatheDelayTime, Yoga.iBreatheDelayTimer)
									
										Yoga.eYogaBreatheStage 	= YOGABREATHESTAGES_EXHALE
										
										Yoga.iExhaleTimer		= 0
										Yoga.bExhaleInProgress	= FALSE
										Yoga.iHoldBreathTimer	= GET_GAME_TIMER()
										
										IF IS_ANY_YOGA_HELP_MESSAGE_BEING_DISPLAYED()
											CLEAR_HELP()
											SETTIMERA(0)
											HELP_STICKS_TRIGGERED = FALSE
											HELP_INHALE_TRIGGERED = FALSE
											HELP_EXHALE_TRIGGERED = FALSE
										ENDIF
										
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Moving to YOGABREATHESTAGES_EXHALE.")
										#ENDIF
									ENDIF
									
								ENDIF
								
							ENDIF
						ENDIF

						IF IS_INHALING_COMPLETED(Yoga, FLOOR(Yoga.fMaxRange), 40, 100)
							Yoga.bInhaleCompleted = TRUE
						ELSE
							Yoga.bInhaleCompleted = FALSE
						ENDIF
					
					BREAK
				
					CASE YOGABREATHESTAGES_EXHALE
					
						IF ( TIMERA() > 100 )
							IF ( bDisplayHelpText = TRUE )
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("EXHALE_NEW")
									IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
										IF NOT HELP_EXHALE_TRIGGERED
											PRINT_HELP_FOREVER("EXHALE_NEW")
											HELP_STICKS_TRIGGERED = FALSE
											HELP_INHALE_TRIGGERED = FALSE
											HELP_EXHALE_TRIGGERED = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF ( Yoga.bExhaleSoundTriggered = FALSE )
						
							IF 	NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
							AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
							
								IF	HAS_SOUND_FINISHED(Yoga.iInhaleSoundID)
								AND	HAS_SOUND_FINISHED(Yoga.iExhaleSoundID)
								
									INT iExhaleSoundVersion
									
									iExhaleSoundVersion = GET_RANDOM_INT_IN_RANGE(1, 6)
								
									SWITCH iExhaleSoundVersion
										CASE 1	//aaaahhh
										CASE 4	//aaaahhh
										CASE 5	//aaaahhh
											PLAY_SOUND_FROM_ENTITY(Yoga.iExhaleSoundID, "YOGA_EXHALE", Yoga.PedIndex, "FAMILY_5_SOUNDS")
											SET_VARIABLE_ON_SOUND(Yoga.iExhaleSoundID, "Version", TO_FLOAT(iExhaleSoundVersion))
											PLAY_FACIAL_ANIM(Yoga.PedIndex, "michael_breathing_face_exhale", Yoga.sYogaAnimDict)
										BREAK
										
										CASE 2	//ooow
										CASE 3	//ooow
											PLAY_SOUND_FROM_ENTITY(Yoga.iExhaleSoundID, "YOGA_EXHALE", Yoga.PedIndex, "FAMILY_5_SOUNDS")
											SET_VARIABLE_ON_SOUND(Yoga.iExhaleSoundID, "Version", TO_FLOAT(iExhaleSoundVersion))
											PLAY_FACIAL_ANIM(Yoga.PedIndex, "michael_breathing_face_exhale_oow", Yoga.sYogaAnimDict)
										BREAK
									ENDSWITCH
									
									
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sound YOGA_EXHALE.")
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iExhaleSoundID is ", Yoga.iExhaleSoundID, ".")
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation michael_breathing_face_exhale.")
									#ENDIF
									
									Yoga.fMaxRange				= 100.0
									Yoga.fMinRange				= 90.0
									Yoga.bExhaleSoundTriggered 	= TRUE
									Yoga.bInhaleSoundTriggered 	= FALSE
									
									Yoga.iBreatheDelayTimer		= 0
									
									Yoga.bExhaleCompleted 		= FALSE
									Yoga.bExhaleInProgress 		= TRUE
									Yoga.bExhaleFailed			= FALSE
									
								ENDIF
							
							ELSE
							
								IF HAS_TIME_PASSED_ON_TIMER(15000, Yoga.iHoldBreathTimer)
									Yoga.eYogaMoveStage = YOGAMOVESTAGE_FAILED
								ENDIF
							
							ENDIF
							
						ELSE

							Yoga.fMaxRange = CLAMP(Yoga.fMaxRange -@Yoga.fValue, 40.0, 100.0)					//slow down the time it takes to
							Yoga.fMinRange = CLAMP(Yoga.fMinRange -@Yoga.fValue, 30.0, 90.0)					//fill the breathing circle
							
							IF 	HAS_SOUND_FINISHED(Yoga.iInhaleSoundID)
							AND	HAS_SOUND_FINISHED(Yoga.iExhaleSoundID)
							AND ( Yoga.fMinRange = 30.0 AND Yoga.fMaxRange = 40.0 )
							
								IF ( Yoga.iBreatheDelayTimer = 0 ) 
									Yoga.iBreatheDelayTime	= 500 + GET_RANDOM_INT_IN_RANGE(0, 251)
									Yoga.iBreatheDelayTimer = GET_GAME_TIMER()
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iBreatheDelayTime is ", Yoga.iBreatheDelayTime, ".")
									#ENDIF
									
								ELSE
									IF HAS_TIME_PASSED_ON_TIMER(Yoga.iBreatheDelayTime, Yoga.iBreatheDelayTimer)
									
										Yoga.iBreathingCounter++
								
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Incrementing iBreathingCounter. Yoga.iBreathingCounter: ", Yoga.iBreathingCounter, ".")
										#ENDIF
										
										Yoga.eYogaBreatheStage 	= YOGABREATHESTAGES_INHALE
										
										Yoga.iInhaleTimer		= 0
										Yoga.bInhaleInProgress	= FALSE
										
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Moving to YOGABREATHESTAGES_INHALE.")
										#ENDIF
										
									ENDIF
								ENDIF
							
							ENDIF
						ENDIF

						IF IS_EXHALING_COMPLETED(Yoga, FLOOR(Yoga.fMinRange), FLOOR(Yoga.fMaxRange), 40, 100)
						
							Yoga.bExhaleCompleted = TRUE
							
							IF (Yoga.bInhaleCompleted = TRUE AND Yoga.bExhaleCompleted = TRUE)

								IF ( Yoga.bBreathCompleted = FALSE )
									Yoga.iBreathsCompleted++
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Incrementing iBreathsCompleted. Yoga.iBreathsCompleted: ", Yoga.iBreathsCompleted, ".")
									#ENDIF
									Yoga.bBreathCompleted = TRUE
								ENDIF

								Yoga.bBreathingPassed = TRUE
								
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Breathing passed for current yoga pose.")
								#ENDIF

							ENDIF

						ENDIF
						
						IF ( Yoga.bBreathingPassed = TRUE )
							
								Yoga.iPosesCompleted++
								
								Yoga.fBlushDecalAlpha		= CLAMP(Yoga.fBlushDecalAlpha + 0.035, Yoga.fBlushDecalAlpha, 0.5)
								Yoga.fModifierStrengthMax 	= CLAMP(Yoga.fModifierStrengthMax + 0.01035, Yoga.fModifierStrengthMax , 0.145)
								
								IF NOT IS_PED_INJURED(Yoga.PedIndex)
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(Yoga.PedIndex, PDZ_HEAD, "blushing")
									APPLY_PED_DAMAGE_DECAL(Yoga.PedIndex, PDZ_HEAD, 0.50, 0.513, 0.0, 1.0, Yoga.fBlushDecalAlpha, 0, FALSE, "blushing")
								ENDIF
								
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Completed pose ", Yoga.iPosesCompleted, " for current yoga move.")
								#ENDIF
								
								Yoga.bInhaleFailed			= FALSE						//rested flags for next pose
								Yoga.bExhaleFailed			= FALSE

								IF (Yoga.iPosesCompleted = iPoses[eMove])				//if all poses for move are completed
								
									IF ( bPlayEndPoseAnimation = FALSE )				//if endpose animations are not needed (mission minigame)
								
										Yoga.eYogaMoveStage = YOGAMOVESTAGE_FINISHED	//go to move finished stage
										
									ELIF ( bPlayEndPoseAnimation = TRUE )				//if endpose animations are needed (off mission minigame)
										
										//play last animation in current move and 'start_pose'
										PLAY_YOGAPOSE_ANIMATION(Yoga, sYogaPosesAnims[eMove][iAnimations[eMove] - 1], "START_POSE")
										
										Yoga.bChangeCamera	= TRUE
										Yoga.eYogaMoveStage = YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION
										
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing end pose animation for current yoga move.")
										#ENDIF
									
									ENDIF
									
								ELSE

									Yoga.iBreathingCounter = 0
									Yoga.iBreathsCompleted = 0
									
									bShowBreathingButtons = FALSE	//reset the scaleform breathing buttons flags
									bBreathingButtonsAdded = FALSE
									
									Yoga.iLeftStickPosition 	= ENUM_TO_INT(eStickDirections[eMove][Yoga.iPosesCompleted - 1][0])
									Yoga.iRightStickPosition 	= ENUM_TO_INT(eStickDirections[eMove][Yoga.iPosesCompleted - 1][1])
									
									Yoga.eYogaMoveStage = YOGAMOVESTAGE_WAIT_BETWEEN_POSES
									
								ENDIF
							
						ELSE
						
							IF ( Yoga.iBreathingCounter = iBreathingAttempts )
								
								Yoga.eYogaMoveStage = YOGAMOVESTAGE_FAILED
								
							ENDIF
							
						ENDIF
						
					BREAK
				
				ENDSWITCH

			ELSE
			
				Yoga.eYogaMoveStage = YOGAMOVESTAGE_FAILED
			
			ENDIF
		
		BREAK
		
		CASE YOGAMOVESTAGE_WAIT_BETWEEN_POSES
		
			IF IS_ANY_YOGA_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
				HELP_STICKS_TRIGGERED = FALSE
				HELP_INHALE_TRIGGERED = FALSE
				HELP_EXHALE_TRIGGERED = FALSE
			ENDIF
			
			Yoga.bLeftStickInDeadZone 	= TRUE
			Yoga.bRightStickInDeadZone 	= TRUE
		
			ANIMATE_STICK_DIRECTION_CHANGE(eStickDirections[eMove][Yoga.iPosesCompleted - 1][STICK_LEFT],
										   eStickDirections[eMove][Yoga.iPosesCompleted][STICK_LEFT],
										   eStickRotations[eMove][Yoga.iPosesCompleted][STICK_LEFT], Yoga.iLeftStickPosition, 4)
			ANIMATE_STICK_DIRECTION_CHANGE(eStickDirections[eMove][Yoga.iPosesCompleted - 1][STICK_RIGHT],
										   eStickDirections[eMove][Yoga.iPosesCompleted][STICK_RIGHT],
										   eStickRotations[eMove][Yoga.iPosesCompleted][STICK_RIGHT], Yoga.iRightStickPosition, 4)
					
			IF  ( Yoga.iLeftStickPosition = ENUM_TO_INT(eStickDirections[eMove][Yoga.iPosesCompleted][STICK_LEFT]) )
			AND ( Yoga.iRightStickPosition = ENUM_TO_INT(eStickDirections[eMove][Yoga.iPosesCompleted][STICK_RIGHT]) )

				Yoga.eYogaMoveStage = YOGAMOVESTAGE_DOINGPOSES
				
			ENDIF
		
		BREAK
		
		CASE YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION

			IF IS_ANY_YOGA_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
				HELP_STICKS_TRIGGERED = FALSE
				HELP_INHALE_TRIGGERED = FALSE
				HELP_EXHALE_TRIGGERED = FALSE
			ENDIF

			SET_YOGAPOSE_ANIMATION_SPEED(Yoga, eMove, fAnimationPlaybackSpeed)
			
			IF IS_PED_AT_LAST_STAGE_OF_SEQUENCE(Yoga.PedIndex, 2)

				CLEAR_HELP()
				
				Yoga.eYogaMoveStage = YOGAMOVESTAGE_FINISHED
			
			ENDIF
			
		BREAK
		
		CASE YOGAMOVESTAGE_FINISHED
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": All poses for yoga move completed. Yoga move is completed.")
			#ENDIF
			
			RETURN TRUE
		
		BREAK
		
		CASE YOGAMOVESTAGE_FAILED
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga move failed.")
			#ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PLAY_YOGAMOVE_FAIL_ANIMATION(YOGASTRUCT& Yoga)

	IF NOT IS_PED_INJURED(Yoga.PedIndex)
	
		STRING sFailAnimationName
	
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "start_to_a1")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a1_pose")
			IF 	IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "start_to_a1")
			AND GET_ENTITY_ANIM_CURRENT_TIME(Yoga.PedIndex, Yoga.sYogaAnimDict, "start_to_a1") <= 0.4
				sFailAnimationName = "midway_fail_from_a1_to_start"	
			ELSE
				sFailAnimationName = "a1_fail_to_start"	
			ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a1_to_a2")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a2_pose")
			sFailAnimationName = "a2_fail_to_start"	
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a2_to_a3")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a3_pose")
			IF 	IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a2_to_a3")
			AND GET_ENTITY_ANIM_CURRENT_TIME(Yoga.PedIndex, Yoga.sYogaAnimDict, "a2_to_a3") <= 0.55
				sFailAnimationName = "a2_fail_to_start"	
			ELSE
				sFailAnimationName = "a3_fail_to_start"	
			ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a3_to_b4")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "b4_pose")
			IF 	IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "a3_to_b4")
			AND GET_ENTITY_ANIM_CURRENT_TIME(Yoga.PedIndex, Yoga.sYogaAnimDict, "a3_to_b4") <= 0.625
				sFailAnimationName = "midway_fail_from_a3_to_start"	
			ELSE
				sFailAnimationName = "b4_fail_to_start"	
			ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "start_to_c1")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c1_pose")
			sFailAnimationName = "c1_fail_to_start"	
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c1_to_c2")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c2_pose")
			IF 	IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c1_to_c2")
			AND GET_ENTITY_ANIM_CURRENT_TIME(Yoga.PedIndex, Yoga.sYogaAnimDict, "c1_to_c2") <= 0.450
				sFailAnimationName = "c1_fail_to_start"	
			ELSE
				sFailAnimationName = "c2_fail_to_start"	
			ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c2_to_c3")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c3_pose")
			sFailAnimationName = "c3_fail_to_start"	
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c3_to_c4")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c4_pose")
			sFailAnimationName = "c4_fail_to_start"	
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c4_to_c5")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c5_pose")
			sFailAnimationName = "c5_fail_to_start"	
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c5_to_c6")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c6_pose")
			sFailAnimationName = "c6_fail_to_start"	
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c6_to_c7")
		OR IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c7_pose")
			IF 	IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "c6_to_c7")
			AND GET_ENTITY_ANIM_CURRENT_TIME(Yoga.PedIndex, Yoga.sYogaAnimDict, "c6_to_c7") <= 0.65
				sFailAnimationName = "c6_fail_to_start"	
			ELSE
				sFailAnimationName = "c2_fail_to_start"	
			ENDIF
		ENDIF
		
		SEQUENCE_INDEX 	SequenceIndex
		CLEAR_SEQUENCE_TASK(SequenceIndex)
		OPEN_SEQUENCE_TASK(SequenceIndex)
			TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, sFailAnimationName, Yoga.vPosition, Yoga.vRotation, SLOW_BLEND_IN, SLOW_BLEND_OUT,
									-1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
			TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, "start_pose", Yoga.vPosition, Yoga.vRotation,	SLOW_BLEND_IN, NORMAL_BLEND_OUT,
									-1, AF_EXTRACT_INITIAL_OFFSET| AF_LOOPING | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		TASK_PERFORM_SEQUENCE(Yoga.PedIndex, SequenceIndex)
		CLEAR_SEQUENCE_TASK(SequenceIndex)
								
		FORCE_PED_AI_AND_ANIMATION_UPDATE(Yoga.PedIndex)
		
	ENDIF

ENDPROC

PROC PLAY_YOGAMOVE_IDLE_ANIMATION(YOGASTRUCT& Yoga, BOOL bPlayRestartAnimations = FALSE)
	
	IF NOT IS_PED_INJURED(Yoga.PedIndex)
		IF NOT IS_ENTITY_PLAYING_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "start_pose")
		
			IF bPlayRestartAnimations = FALSE	//if no restart an animations are needed, go to start_pose
			
				TASK_PLAY_ANIM(Yoga.PedIndex, Yoga.sYogaAnimDict, "start_pose", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0, FALSE, AIK_DISABLE_LEG_IK)
				
			ELSE								//if restart animations are needed, play fail_to_start and start_pose animations in sequence
			
				SEQUENCE_INDEX 	SequenceIndex
				CLEAR_SEQUENCE_TASK(SequenceIndex)
				OPEN_SEQUENCE_TASK(SequenceIndex)
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
						CASE 0
							TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, "fail_to_start_a", Yoga.vPosition, Yoga.vRotation,
													INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						BREAK
						CASE 1
							TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, "fail_to_start_b", Yoga.vPosition, Yoga.vRotation,
													INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						BREAK
						CASE 2
							TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, "fail_to_start_c", Yoga.vPosition, Yoga.vRotation,
													INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						BREAK
					ENDSWITCH
					TASK_PLAY_ANIM_ADVANCED(NULL, Yoga.sYogaAnimDict, "start_pose", Yoga.vPosition, Yoga.vRotation,
											SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1,
											AF_EXTRACT_INITIAL_OFFSET| AF_LOOPING | AF_USE_MOVER_EXTRACTION, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
				CLOSE_SEQUENCE_TASK(SequenceIndex)
				TASK_PERFORM_SEQUENCE(Yoga.PedIndex, SequenceIndex)
				CLEAR_SEQUENCE_TASK(SequenceIndex)
				
				CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex)
				PLAY_FACIAL_ANIM(Yoga.PedIndex, "fail_face", Yoga.sYogaAnimDict)	//also play faical animation during the restart animations
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation michael_breathing_face_exhale.")
				#ENDIF
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(Yoga.PedIndex)
				
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE:
///    Checks if yoga minigame animation dictionary and audio banks have loaded.
/// RETURNS:
///    True if animation dictionary and audio banks have loaded.
FUNC BOOL ARE_YOGA_REQUESTS_LOADED()

	REQUEST_ANIM_DICT("missfam5_yoga")
	
	REQUEST_ADDITIONAL_TEXT("YOGA", MINIGAME_TEXT_SLOT)
	
	IF 	HAS_ANIM_DICT_LOADED("missfam5_yoga")
	AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
	AND REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_01")
	AND REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_02")

		RETURN TRUE

	ENDIF

	RETURN FALSE

ENDFUNC

PROC RUN_YOGA_FAIL_CAMERA(YOGASTRUCT Yoga, INT i = 0)

	SWITCH Yoga.eYogaMinigameStage
	
		CASE YOGAMINIGAMESTAGE_FAILRESET

			IF NOT DOES_CAM_EXIST(YogaFailCamera)
					
				YogaFailCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				SHAKE_CAM(YogaFailCamera, "HAND_SHAKE", 0.1)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating yoga fail camera with number ", i, ".")
				#ENDIF
				
				SET_CAM_PARAMS(YogaFailCamera, vYogaFailCameraPositions[i], vYogaFailCameraRotations[i], vYogaFailCameraFOVs[i])
				
				SET_ENTITY_VISIBLE(Yoga.PedIndex, FALSE)
				
			ENDIF
		
		BREAK
		
		CASE YOGAMINIGAMESTAGE_DOMOVE
		CASE YOGAMINIGAMESTAGE_RESTART
		
			IF DOES_CAM_EXIST(YogaFailCamera)
			
				DESTROY_CAM(YogaFailCamera)
				
				SET_ENTITY_VISIBLE(Yoga.PedIndex, TRUE)
				
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC RUN_YOGA_CUSTOM_CAMERA(YOGASTRUCT &Yoga)

	SWITCH Yoga.eYogaMinigameStage
	
		CASE YOGAMINIGAMESTAGE_DOMOVE
						
			IF ( Yoga.bChangeCamera = TRUE )
			
				IF ( bYogaCameraFlags[Yoga.iPosesCompleted][YCF_CHANGE] = TRUE )
				
					IF NOT DOES_CAM_EXIST(YogaCustomCamera)
						YogaCustomCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
					ENDIF
					
					IF DOES_CAM_EXIST(YogaCustomCamera)
		
							SET_CAM_PARAMS(YogaCustomCamera, vYogaCameraPositions[Yoga.iPosesCompleted][0],
															 vYogaCameraRotations[Yoga.iPosesCompleted][0],
															 fYogaCameraFOVs[Yoga.iPosesCompleted][0])
														 
						IF ( bYogaCameraFlags[Yoga.iPosesCompleted][YCF_INTERP] = TRUE )
						
							SET_CAM_PARAMS(YogaCustomCamera, vYogaCameraPositions[Yoga.iPosesCompleted][1],
															 vYogaCameraRotations[Yoga.iPosesCompleted][1],
															 fYogaCameraFOVs[Yoga.iPosesCompleted][1],
															 iYogaCameraInterpTimes[Yoga.iPosesCompleted],
															 GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
						ENDIF

						SHAKE_CAM(YogaCustomCamera, "HAND_SHAKE", 0.1)
						SET_CAM_ACTIVE(YogaCustomCamera, bYogaCameraFlags[Yoga.iPosesCompleted][YCF_ACTIVE])
						
					ENDIF

				ENDIF
				
				Yoga.bChangeCamera = FALSE
				
			ENDIF
			
		BREAK
				
		//CASE YOGAMINIGAMESTAGE_DOFAIL
		CASE YOGAMINIGAMESTAGE_WAITFAIL
		CASE YOGAMINIGAMESTAGE_FAILRESET
		
			IF DOES_CAM_EXIST(YogaCustomCamera)
				IF IS_CAM_RENDERING(YogaCustomCamera)
				OR IS_CAM_INTERPOLATING(YogaCustomCamera)
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iFailedPose: ", Yoga.iFailedPose, ".")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iFailCameraCutTimer: ", Yoga.iFailCameraCutTimer, ".")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iYogaCameraFailCutTimes[", Yoga.iFailedPose, "]: ",iYogaCameraFailCutTimes[Yoga.iFailedPose], ".")
					#ENDIF
				
					IF ( iYogaCameraFailCutTimes[Yoga.iFailedPose] = 0 )

						DESTROY_CAM(YogaCustomCamera)
						
					ELSE

						IF ( Yoga.iFailCameraCutTimer = 0 )

							Yoga.iFailCameraCutTimer = GET_GAME_TIMER()
							
						ELSE

							IF HAS_TIME_PASSED_ON_TIMER(iYogaCameraFailCutTimes[Yoga.iFailedPose], Yoga.iFailCameraCutTimer)

								DESTROY_CAM(YogaCustomCamera)
						
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF							
			ENDIF
		
		BREAK
		
		CASE YOGAMINIGAMESTAGE_FINISHED
		
			IF DOES_CAM_EXIST(YogaCustomCamera)
				SWITCH Yoga.eYogaMove
					CASE YOGAMOVE_WARRIOR
						SET_CAM_ACTIVE(YogaCustomCamera, FALSE)
					BREAK
					DEFAULT
						//
					BREAK
				ENDSWITCH
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC UPDATE_YOGA_TIMECYCLE_MODIFIER(YOGASTRUCT &Yoga, BOOL bMinigameActive = TRUE, FLOAT fDesiredValue = 0.0, FLOAT fDesiredSpeed = 0.001)

	IF ( bMinigameActive = TRUE )

		SWITCH Yoga.eYogaMinigameStage
		
			CASE YOGAMINIGAMESTAGE_SETUP
				SET_TIMECYCLE_MODIFIER("STONED_CUTSCENE")
				SET_TIMECYCLE_MODIFIER_STRENGTH(Yoga.fModifierStrength)
			BREAK
			
			CASE YOGAMINIGAMESTAGE_DOMOVE
			CASE YOGAMINIGAMESTAGE_DOFAIL
			CASE YOGAMINIGAMESTAGE_WAITFAIL
			CASE YOGAMINIGAMESTAGE_FAILRESET
			CASE YOGAMINIGAMESTAGE_RESTART
			CASE YOGAMINIGAMESTAGE_PASSED
			CASE YOGAMINIGAMESTAGE_FINISHED
				CONVERGE_VALUE(Yoga.fModifierStrength, Yoga.fModifierStrengthMax, 0.0001, TRUE)
				SET_TIMECYCLE_MODIFIER_STRENGTH(Yoga.fModifierStrength)
			BREAK
		
		ENDSWITCH
		
	ELSE
	
		CONVERGE_VALUE(Yoga.fModifierStrength, fDesiredValue, fDesiredSpeed, TRUE)
		SET_TIMECYCLE_MODIFIER_STRENGTH(Yoga.fModifierStrength)
	
	ENDIF
	
ENDPROC

/// PURPOSE:
/// 	Handles switching mouse and keyboard controls for left handed users.
///     Not the same as gamepad southpaw mode!
PROC UPDATE_MK_SOUTHPAW_MODE()

	// Don't do anything if not using keyboard and mouse.
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		EXIT
	ENDIF
	
	// Check for mouse and keyboard southpaw switch
	IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN )
		
		IF bSouthPaw = TRUE
			bSouthPaw = FALSE
		ELSE
			bSouthPaw = TRUE
		ENDIF
		
		iStickXPrev = 0
		iStickYPrev = 0
		
	ENDIF
	
	/* DEBUG
	IF bSouthPaw
		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", 1 )
	ELSE
		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", 0 )
	ENDIF
	*/

ENDPROC


/// PURPOSE:
///    Runs yoga minigame for a specified yoga move.
/// PARAMS:
///    Yoga - Struct that holds yoga data.
///    eMove - Yoga move to perform.
///    bPlayEndPoseAnimation - Indicates if end pose animations should be played. FALSE on mission, TRUE on off mission minigame.
///    bDisplayHelpText - Indicates if help text be displyed.
///    bDelayBreathing - Indicates if first breathing should be delayed (to display help text).
///    iPedStatIncrease - Value to increment stats after a passed yoga move.
///    iLocation - Location 0 - Michael's house, 1 - North Coast.
///    bPlayAmbientSpeech - Should ambient speech be played when failing. Used in off mission minigame
/// RETURNS:
///    TRUE when the minigame has has been completed.
FUNC BOOL HAS_PLAYER_COMPLETED_YOGA_MOVE(YOGASTRUCT& Yoga, YOGAMOVE eMove, BOOL bPlayEndPoseAnimation = FALSE, BOOL bDisplayHelpText = TRUE,
										 BOOL bDelayBreathing = FALSE, INT iPedStatIncrease = 1, INT iLocation = 0, BOOL bPlayAmbientSpeech = FALSE)
	
	Yoga.PedIndex 			= PLAYER_PED_ID()
	Yoga.eYogaMove 			= eMove
	Yoga.sYogaAnimDict 		= "missfam5_yoga"
	
	UPDATE_YOGA_FACIAL_ANIMATIONS(Yoga)
	
	UPDATE_MK_SOUTHPAW_MODE() // Switch mouse and keyboard controls - not the same as gamepad soutpaw!
	
	SWITCH Yoga.eYogaMinigameStage
	
		CASE YOGAMINIGAMESTAGE_SETUP 
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting yoga minigame setup.")
			#ENDIF
			
			IF NOT IS_PED_INJURED(Yoga.PedIndex)
			
				SET_ENTITY_HEALTH(Yoga.PedIndex, GET_PED_MAX_HEALTH(Yoga.PedIndex))		//restore health to max ped health
			
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(Yoga.PedIndex, TRUE)				//hide ped weapon
				
				SET_CURRENT_PED_WEAPON(Yoga.PedIndex, WEAPONTYPE_UNARMED, TRUE)			//make ped weapon unarmed
				
				IF ( Yoga.fBlushDecalAlpha = 0.0)
					CLEAR_PED_DAMAGE_DECAL_BY_ZONE(Yoga.PedIndex, PDZ_HEAD, "blushing")
					APPLY_PED_DAMAGE_DECAL(Yoga.PedIndex, PDZ_HEAD, 0.50, 0.513, 0.0, 1.0, Yoga.fBlushDecalAlpha, 0, TRUE, "blushing")
				ENDIF
				
				Yoga.vPosition = GET_ENTITY_COORDS(Yoga.PedIndex)
				Yoga.vRotation = GET_ENTITY_ROTATION(Yoga.PedIndex)
				
				siYogaScaleform = REQUEST_SCALEFORM_MOVIE("yoga_buttons")
	
				IF IS_PC_VERSION()
					siYogaScaleformKBM = REQUEST_SCALEFORM_MOVIE("yoga_keys")
				ENDIF

				IF 	ARE_YOGA_REQUESTS_LOADED()
				AND HAS_YOGA_SCALEFORM_LOADED()
				
					SETUP_YOGAMOVE(eMove, iLocation)

					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_FORCE_FOOTSTEP_UPDATE(Yoga.PedIndex, TRUE)
				
					PLAY_YOGAMOVE_IDLE_ANIMATION(Yoga)

					Yoga.iBreathingValue  		= 0
					Yoga.iShakeTimer 			= 0
					Yoga.iInhaleSoundID 		= GET_SOUND_ID()					
					Yoga.iExhaleSoundID 		= GET_SOUND_ID()
					Yoga.iFailSoundID 			= GET_SOUND_ID()
					Yoga.iBreathingCounter		= 0
					Yoga.iBreathsCompleted		= 0
					
					Yoga.iInhaleTimer			= 0
					Yoga.iExhaleTimer			= 0
					
					Yoga.bInhaleInProgress		= FALSE
					Yoga.bExhaleInProgress		= FALSE
					
					Yoga.bInhaleCompleted		= FALSE
					Yoga.bExhaleCompleted		= FALSE
					
					Yoga.bBreathCompleted		= FALSE
					Yoga.bBreathingPassed 		= FALSE
					
					Yoga.bInhaleFailed			= FALSE
					Yoga.bExhaleFailed			= FALSE
					
					Yoga.bHelpTextDelayDone 	= FALSE

					Yoga.eYogaMinigameStage 	= YOGAMINIGAMESTAGE_DOMOVE
					
					Yoga.fValue					= 32.0
					
					Yoga.bChangeCamera			= FALSE
					Yoga.bChangePoseFacial 		= FALSE
					
					HELP_STICKS_TRIGGERED 		= FALSE
					HELP_INHALE_TRIGGERED 		= FALSE
					HELP_EXHALE_TRIGGERED 		= FALSE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished yoga minigame setup.")
					#ENDIF
					
				ENDIF
				
			ENDIF
		BREAK
		
		CASE YOGAMINIGAMESTAGE_DOMOVE

			IF IS_YOGAMOVE_COMPLETED(Yoga, eMove, 3, bPlayEndPoseAnimation, 1.15, bDisplayHelpText, bDelayBreathing)
				Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_PASSED
			ELSE
				IF ( Yoga.eYogaMoveStage = YOGAMOVESTAGE_FAILED )
					Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_DOFAIL
				ENDIF
			ENDIF

			IF 	( Yoga.eYogaMoveStage <> YOGAMOVESTAGE_PLAY_ENDPOSE_ANIMATION )	//draw scaleform for all move stages 
			AND	( Yoga.eYogaMoveStage <> YOGAMOVESTAGE_FINISHED )				//apart from playing endpose animation and finished stage
				DRAW_YOGAMOVE_SCALEFORM(Yoga, eMove)
			ENDIF
			
		BREAK
		
		CASE YOGAMINIGAMESTAGE_DOFAIL
		
			STOP_SOUND(Yoga.iInhaleSoundID)
			STOP_SOUND(Yoga.iExhaleSoundID)
		
			PLAY_SOUND_FROM_ENTITY(Yoga.iFailSoundID, "YOGA_FAIL", Yoga.PedIndex, "FAMILY_5_SOUNDS")
		
			Yoga.iFailCounter++
		
			Yoga.bFailing 				= TRUE
			
			Yoga.iFailedPose 			= Yoga.iPosesCompleted
			Yoga.iFailCameraCutTimer	= 0
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Move failed at pose ", Yoga.iFailedPose, ". Number of times failed in this move is ", Yoga.iFailCounter, ".")
			#ENDIF
			
			//reset variables used to do yoga moves
			Yoga.iBreathingValue  		= 0
			Yoga.iPosesCompleted 		= 0
			Yoga.iBreathingCounter 		= 0
			Yoga.iBreathsCompleted		= 0
			Yoga.bInhaleInProgress 		= FALSE
			Yoga.bBreathCompleted		= FALSE
			Yoga.bBreathingPassed		= FALSE
			Yoga.bInhaleSoundTriggered 	= FALSE
			Yoga.bExhaleSoundTriggered 	= FALSE
			Yoga.bInhaleFailed			= FALSE
			Yoga.bExhaleFailed			= FALSE
			
			Yoga.bChangeCamera			= FALSE
			Yoga.bChangePoseFacial 		= FALSE
			
			Yoga.fModifierStrengthMax	= 0.0
			Yoga.fBlushDecalAlpha		= CLAMP(Yoga.fBlushDecalAlpha - 0.08, 0.0, 0.5)
			
			Yoga.bAmbientSpeechPlayed	= FALSE
			
			CLEANUP_AND_REMOVE_YOGA_SCALEFORM(siYogaScaleform)	//remove yoga scaleform movie and reset the scaleform data
			
			IF IS_PC_VERSION()
				CLEANUP_AND_REMOVE_YOGA_SCALEFORM(siYogaScaleformKBM)
			ENDIF
			
			CLEAR_PED_SECONDARY_TASK(Yoga.PedIndex)
			CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex)
			
			IF NOT IS_PED_INJURED(Yoga.PedIndex)
				CLEAR_PED_DAMAGE_DECAL_BY_ZONE(Yoga.PedIndex, PDZ_HEAD, "blushing")
				APPLY_PED_DAMAGE_DECAL(Yoga.PedIndex, PDZ_HEAD, 0.50, 0.513, 0.0, 1.0, Yoga.fBlushDecalAlpha, 0, FALSE, "blushing")
			ENDIF
			
			SWITCH GET_RANDOM_INT_IN_RANGE(1, 7)
				CASE 1
					PLAY_FACIAL_ANIM(Yoga.PedIndex, "pain_1")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation pain_1.")
					#ENDIF
				BREAK
				CASE 2
					PLAY_FACIAL_ANIM(Yoga.PedIndex, "pain_2")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation pain_2.")
					#ENDIF
				BREAK
				CASE 3
					PLAY_FACIAL_ANIM(Yoga.PedIndex, "pain_3")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation pain_3.")
					#ENDIF
				BREAK
				CASE 4
					PLAY_FACIAL_ANIM(Yoga.PedIndex, "pain_4")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation pain_4.")
					#ENDIF
				BREAK
				CASE 5
					PLAY_FACIAL_ANIM(Yoga.PedIndex, "pain_5")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation pain_5.")
					#ENDIF
				BREAK
				CASE 6
					PLAY_FACIAL_ANIM(Yoga.PedIndex, "pain_6")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing facial animation pain_6.")
					#ENDIF
				BREAK
			ENDSWITCH
						
			PLAY_YOGAMOVE_FAIL_ANIMATION(Yoga)
			
			Yoga.iTimer 				= GET_GAME_TIMER()
			Yoga.iShakeTimer 			= 0
			
			CLEAR_HELP()
			HELP_STICKS_TRIGGERED 		= FALSE
			HELP_INHALE_TRIGGERED 		= FALSE
			HELP_EXHALE_TRIGGERED 		= FALSE
			
			Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_WAITFAIL
			
		BREAK
		
		CASE YOGAMINIGAMESTAGE_WAITFAIL
						
			IF HAS_TIME_PASSED_ON_TIMER(1000, Yoga.iTimer)
				Yoga.bFailing = FALSE
			ENDIF
			
			
			IF ( bPlayAmbientSpeech = TRUE )
				IF HAS_TIME_PASSED_ON_TIMER(1000, Yoga.iTimer)
					IF ( Yoga.bAmbientSpeechPlayed = FALSE )
						IF NOT IS_AMBIENT_SPEECH_PLAYING(Yoga.PedIndex)
							
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
								CASE 0
									PLAY_PED_AMBIENT_SPEECH(Yoga.PedIndex, "GENERIC_CURSE_MED", SPEECH_PARAMS_FORCE_NORMAL)
								BREAK
							ENDSWITCH
							
							Yoga.bAmbientSpeechPlayed	= TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_TIME_PASSED_ON_TIMER(2500, Yoga.iTimer)
				Yoga.iTimer 			= GET_GAME_TIMER()
				Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_FAILRESET
			ENDIF

		BREAK
		
		CASE YOGAMINIGAMESTAGE_FAILRESET
			
			siYogaScaleform = REQUEST_SCALEFORM_MOVIE("yoga_buttons")
			
			IF IS_PC_VERSION()
				siYogaScaleformKBM = REQUEST_SCALEFORM_MOVIE("yoga_keys")
			ENDIF
			
			IF HAS_YOGA_SCALEFORM_LOADED()
			
				IF 	NOT DOES_CAM_EXIST(YogaFailCamera)
					IF IS_PED_AT_LAST_STAGE_OF_SEQUENCE(Yoga.PedIndex, 2)
						PLAY_YOGAMOVE_IDLE_ANIMATION(Yoga)
						Yoga.eYogaMoveStage 	= YOGAMOVESTAGE_DOINGPOSES
						Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_DOMOVE
					ENDIF
				ELSE

					IF HAS_TIME_PASSED_ON_TIMER(3000, Yoga.iTimer)
				
						IF NOT IS_PED_INJURED(Yoga.PedIndex)

							CLEAR_PED_TASKS_IMMEDIATELY(Yoga.PedIndex)

							PLAY_YOGAMOVE_IDLE_ANIMATION(Yoga, TRUE)

							Yoga.iTimer 			= GET_GAME_TIMER()
							Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_RESTART
						
						ENDIF
						
					ENDIF

				ENDIF
				
			ENDIF

		BREAK
		
		CASE YOGAMINIGAMESTAGE_RESTART
			
			IF IS_PED_AT_LAST_STAGE_OF_SEQUENCE(Yoga.PedIndex, 2)
				Yoga.eYogaMoveStage 	= YOGAMOVESTAGE_DOINGPOSES
				Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_DOMOVE
			ENDIF
		
		BREAK
		
		CASE YOGAMINIGAMESTAGE_PASSED
			
			IF HAS_SOUND_FINISHED(Yoga.iExhaleSoundID)				//make sure exhale sound is not playing
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga minigame completed successfully.")
				#ENDIF
			
				IF IS_ANY_YOGA_HELP_MESSAGE_BEING_DISPLAYED()	//clear any yoga minigame help that is currently displayed
					CLEAR_HELP()
					HELP_STICKS_TRIGGERED = FALSE
					HELP_INHALE_TRIGGERED = FALSE
					HELP_EXHALE_TRIGGERED = FALSE
				ENDIF
				
				INCREMENT_PLAYER_PED_STAT(GET_PLAYER_PED_ENUM(Yoga.PedIndex), PS_STAMINA, 		iPedStatIncrease)
				INCREMENT_PLAYER_PED_STAT(GET_PLAYER_PED_ENUM(Yoga.PedIndex), PS_STRENGTH, 		iPedStatIncrease)
				INCREMENT_PLAYER_PED_STAT(GET_PLAYER_PED_ENUM(Yoga.PedIndex), PS_LUNG_CAPACITY, iPedStatIncrease)
				
				//BAWSAQ_INCREMENT_MODIFIER(BSMF_YOGATIMES)			//inform the game stats that yoga minigame was completed successfully
				
				STOP_SOUND(Yoga.iFailSoundID)
				STOP_SOUND(Yoga.iInhaleSoundID)
				
				RELEASE_SOUND_ID(Yoga.iFailSoundID)
				RELEASE_SOUND_ID(Yoga.iInhaleSoundID)
				RELEASE_SOUND_ID(Yoga.iExhaleSoundID)
				
				CLEAR_PED_SECONDARY_TASK(Yoga.PedIndex)
				CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(Yoga.PedIndex)
				SET_FORCE_FOOTSTEP_UPDATE(Yoga.PedIndex, FALSE)
				
				CLEANUP_AND_REMOVE_YOGA_SCALEFORM(siYogaScaleform)	//remove yoga scaleform movies
				
				IF IS_PC_VERSION()
					CLEANUP_AND_REMOVE_YOGA_SCALEFORM(siYogaScaleformKBM)
				ENDIF

				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siYogaScaleform)
				
				IF IS_PC_VERSION()
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siYogaScaleformKBM)
				ENDIF
				
				IF DOES_CAM_EXIST(YogaFailCamera)
					SET_CAM_ACTIVE(YogaFailCamera, FALSE)
					DESTROY_CAM(YogaFailCamera)
				ENDIF
		
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga.iInhaleSoundID is ", Yoga.iInhaleSoundID, ".")
				#ENDIF
				
				Yoga.eYogaMinigameStage = YOGAMINIGAMESTAGE_FINISHED
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Yoga minigame finished.")
				#ENDIF
				
			ENDIF

		BREAK
		
		CASE YOGAMINIGAMESTAGE_FINISHED
			BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_YOGA)		//stockmarket #1514495
			
			RETURN TRUE
		
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC
