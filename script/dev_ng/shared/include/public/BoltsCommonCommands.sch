using "dialogue_public.sch"
using "commands_hud.sch"
USING "script_blips.sch"
USING "usefulCommands.sch"
USING "selector_public.sch"
USING "help_at_location.sch"


//NOTES:
//For this file to work, in the mission script the following need to be defined before adding USING "BoltsCommonCommands.sch".
// CONST_INT MAX_ASSETS - max count of how many assets will be used in a mission
// ENUM enumMissionStage - define each stage of the mission

//**************************** Common vars *******************************
vector vNull = <<0,0,0>>

//***************************** PEDS DATA ********************************

#IF IS_DEBUG_BUILD
bool bOutputScriptDebug
#ENDIF



struct pedStruct
	PED_INDEX id
	BLIP_INDEX blip
	//enumAction action
	//enumAction storedAction
	int actionFlag
	float floatA
	int intA
	AI_BLIP_STRUCT aiBlip
	bool registeredKilled
ENDSTRUCT


pedStruct ped[MAX_PED_COUNT]

//******************************* VEH DATA *****************************************

struct vehStruct
	VEHICLE_INDEX id
	BLIP_INDEX blip
ENDSTRUCT

vehStruct vehicle[MAX_VEH_COUNT]

//************************** blips **************************************************

//************************** parent widget ************************************

//************************* Mission stage selector




// **************************** common variables *******************************
int iFlag //= iFlag






//relationship groups
REL_GROUP_HASH HASH_FRIEND_GROUP
REL_GROUP_HASH HASH_FOE_GROUP
REL_GROUP_HASH HASH_IGNORE_GROUP
REL_GROUP_HASH HASH_WARY_GROUP

proc INIT_REL_GROUPS()
	vNull=vNull
	ADD_RELATIONSHIP_GROUP("MYFRIEND",HASH_FRIEND_GROUP)
	ADD_RELATIONSHIP_GROUP("FOE",HASH_FOE_GROUP)
	ADD_RELATIONSHIP_GROUP("WARY",HASH_WARY_GROUP)
	ADD_RELATIONSHIP_GROUP("IGNORE",HASH_IGNORE_GROUP)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,HASH_FRIEND_GROUP,HASH_FOE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,HASH_FOE_GROUP,HASH_FRIEND_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_FRIEND_GROUP,HASH_IGNORE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_FOE_GROUP,HASH_IGNORE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_FOE_GROUP,HASH_WARY_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_IGNORE_GROUP,HASH_FRIEND_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_IGNORE_GROUP,HASH_FOE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE,HASH_WARY_GROUP,HASH_FRIEND_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_WARY_GROUP,HASH_IGNORE_GROUP)
	
	
//	RELGROUPHASH_PLAYER
ENDPROC


//prep peds

enum pedRoleEnum
	pedrole_void,
	pedrole_cop,
	pedrole_civilian,
	ped_friend_gang,
	pedrole_no_ai,
	pedrole_enemygang,
	pedrole_enemyNOAI,
	pedrole_wary,
	pedrole_enemyUnaware
ENDENUM

bool bPlayersUseHeadsets = FALSE

PROC initPed(int pedIndex, MODEL_NAMES model, vector pedSpawnCoords, float pedHeading, VEHICLE_INDEX vehicleToSpawnIn = NULL, VEHICLE_SEAT vehicleSeatToSpawnIn=VS_DRIVER, pedRoleEnum pedrole=pedrole_void, WEAPON_TYPE giveWeapon=WEAPONTYPE_INVALID, bool bUseRandomVariation=FALSE, bool noOffset=FALSE)
	if pedIndex =-1 or pedindex  >= max_ped_count
		SCRIPT_ASSERT("ASSERT: initPed() attempting to create ped with invalid array entry.")
	ENDIF
	
	if pedIndex != 0
		IF IS_PED_INJURED(ped[pedIndex].id)
			SET_PED_AS_NO_LONGER_NEEDED(ped[pedIndex].id)
		ELSE
			if ped[pedIndex].id = PLAYER_PED_ID()
				if GET_ENTITY_MODEL(ped[pedIndex].id) != GET_ENTITY_MODEL(PLAYER_PED_ID())
					ped[pedIndex].id = null
				ENDIF
			ELSE
				if GET_ENTITY_MODEL(ped[pedIndex].id) != model and model != DUMMY_MODEL_FOR_SCRIPT
					//SET_PED_AS_NO_LONGER_NEEDED(ped[pedIndex].id)
					DELETE_PED(ped[pedIndex].id)
					ped[pedIndex].id = null
				ENDIF
			ENDIF
		ENDIF
	ELSE
	//	ped[pedIndex].id = PLAYER_PED_ID() //added this line to ensure ped_index 0 is always the player
	ENDIF
	
	iFlag=iFlag

	if vehicleToSpawnIn = NULL
		//debugMessage(debug_array,"aa10 pedIndex = ","",pedIndex)
		IF NOT DOES_ENTITY_EXIST(ped[pedIndex].id) and pedIndex != 0
			If model = DUMMY_MODEL_FOR_SCRIPT		
				IF CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
					ped[pedIndex].id = CREATE_RANDOM_PED(pedSpawnCoords)
					SET_ENTITY_HEADING(ped[pedIndex].id,pedHeading)
				ENDIF				
			ELSE
			
				ped[pedIndex].id = CREATE_PED(PEDTYPE_MISSION,model,pedSpawnCoords,pedHeading)				
				if noOffset = TRUE
					SET_ENTITY_COORDS_NO_OFFSET(ped[pedIndex].id,pedSpawnCoords)
				ENDIF
			ENDIF
		ELSE
			if not IS_VECTOR_ZERO(pedSpawnCoords )
				if pedIndex = 0
					SET_ENTITY_COORDS(PLAYER_PED_ID(),pedSpawnCoords) //Removed because in Carsteal2 Franklin was getting repositioned in his car SET_PED_COORDS_KEEP_VEHICLE
					SET_ENTITY_HEADING(PLAYER_PED_ID(),pedHeading)				
				ELSE
					IF NOT IS_PED_INJURED(ped[pedIndex].id)			
						if noOffset = TRUE
							SET_ENTITY_COORDS_NO_OFFSET(ped[pedIndex].id,pedSpawnCoords)
						ELSE
							SET_ENTITY_COORDS(ped[pedIndex].id,pedSpawnCoords)
						ENDIF
						SET_ENTITY_HEADING(ped[pedIndex].id,pedHeading)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE		
		PRINTLN("pedIndex: ",pedIndex)
		PRINTLN("Ped CREATE: ",native_to_int(ped[pedIndex].id))
		PRINTLN("PLAYER_PED_ID: ",native_to_int(PLAYER_PED_ID()))
		IF NOT DOES_ENTITY_EXIST(ped[pedIndex].id) and pedIndex != 0
			PRINTLN("Franklin ped id doesn't exist")
			IF IS_VEHICLE_DRIVEABLE(vehicleToSpawnIn)		
				ped[pedIndex].id = CREATE_PED_INSIDE_VEHICLE(vehicleToSpawnIn,PEDTYPE_MISSION,model,vehicleSeatToSpawnIn)
			ENDIF
		ELSE
			PRINTLN("Franklin ped id does exist")
			if not IS_PED_INJURED(ped[pedIndex].id)
				vector va
				va = GET_ENTITY_COORDS(ped[pedIndex].id)
				PRINTLN("Ped coords: ",va.x," ",va.y)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehicleToSpawnIn)
				if pedIndex = 0
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicleToSpawnIn,vehicleSeatToSpawnIn)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(ped[pedIndex].id)
						SET_PED_INTO_VEHICLE(ped[pedIndex].id,vehicleToSpawnIn,vehicleSeatToSpawnIn)
					ENDIF
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
	
	if ped[pedIndex].id != PLAYER_PED_ID()
		if not IS_PED_INJURED(ped[pedIndex].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(ped[pedIndex].id,true)
			if bUseRandomVariation
				SET_PED_RANDOM_COMPONENT_VARIATION(ped[pedIndex].id)
			ELSE
				SET_PED_DEFAULT_COMPONENT_VARIATION(ped[pedIndex].id)
			ENDIF
		ENDIF
	ENDIF

	if pedrole != pedrole_void
		if not IS_PED_INJURED(ped[pedIndex].id)
			switch pedrole
				case pedrole_cop
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_FOE_GROUP)					
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_AGGRESSIVE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_BLIND_FIRE_IN_COVER, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_CAN_INVESTIGATE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_LEAVE_VEHICLES, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_USE_COVER, TRUE)	
					if giveWeapon != WEAPONTYPE_INVALID
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ELSE
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,WEAPONTYPE_PISTOL)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,WEAPONTYPE_PISTOL,500,true)
						ENDIF	
					ENDIF
					if not DOES_BLIP_EXIST(ped[pedIndex].blip)
						ped[pedIndex].blip = CREATE_BLIP_FOR_ENTITY(ped[pedIndex].id,true)				
					ENDIF
				BREAK
				CASE ped_friend_gang
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_FRIEND_GROUP)				
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_AGGRESSIVE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_BLIND_FIRE_IN_COVER, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_CAN_INVESTIGATE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_LEAVE_VEHICLES, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_USE_COVER, TRUE)				
					SET_PED_CAN_BE_TARGETTED(ped[pedIndex].id,FALSE)
					if giveWeapon != WEAPONTYPE_UNARMED
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ELSE
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,WEAPONTYPE_PISTOL)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,WEAPONTYPE_PISTOL,500,true)
						ENDIF
					ENDIF
				BREAK
				CASE pedrole_enemygang
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_FOE_GROUP)
					if giveWeapon != WEAPONTYPE_INVALID
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ELSE
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,WEAPONTYPE_PUMPSHOTGUN) and not HAS_PED_GOT_WEAPON(ped[pedIndex].id,WEAPONTYPE_PISTOL)
							if GET_RANDOM_INT_IN_RANGE(0,3) = 0
								GIVE_WEAPON_TO_PED(ped[pedIndex].id,WEAPONTYPE_PUMPSHOTGUN,500,true)
							ELSE
								GIVE_WEAPON_TO_PED(ped[pedIndex].id,WEAPONTYPE_PISTOL,500,true)
							ENDIF
						ENDIF
					ENDIF
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_AGGRESSIVE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_BLIND_FIRE_IN_COVER, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_CAN_INVESTIGATE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_LEAVE_VEHICLES, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,  CA_USE_COVER, TRUE)	
					
					if not DOES_BLIP_EXIST(ped[pedIndex].blip)
						ped[pedIndex].blip = CREATE_BLIP_FOR_ENTITY(ped[pedIndex].id,true)
					ENDIF
				break
				case pedrole_enemyNOAI
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[pedIndex].id,true)									
					if giveWeapon != WEAPONTYPE_INVALID and giveWeapon != WEAPONTYPE_UNARMED
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ENDIF
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_IGNORE_GROUP)
					if not DOES_BLIP_EXIST(ped[pedIndex].blip)
						ped[pedIndex].blip = CREATE_BLIP_FOR_ENTITY(ped[pedIndex].id,true)
					ENDIF
				break	
				case pedrole_enemyUnaware
					//enemy ped that can be targetted but isn't set to react to the player and buddies
					//make sure targetable
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[pedIndex].id,true)									
					
					if giveWeapon != WEAPONTYPE_INVALID and giveWeapon != WEAPONTYPE_UNARMED
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ENDIF
					SET_PED_CAN_BE_TARGETTED(ped[pedIndex].id,TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_IGNORE_GROUP)	
				break
				case pedrole_no_ai
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[pedIndex].id,true)									
					
					if giveWeapon != WEAPONTYPE_INVALID and giveWeapon != WEAPONTYPE_UNARMED
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ENDIF
					SET_PED_CAN_BE_TARGETTED(ped[pedIndex].id,FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_IGNORE_GROUP)					
				break	
				case pedrole_civilian
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[pedIndex].id,true)									
					
					if giveWeapon != WEAPONTYPE_INVALID and giveWeapon != WEAPONTYPE_UNARMED
						if not HAS_PED_GOT_WEAPON(ped[pedIndex].id,giveWeapon)
							GIVE_WEAPON_TO_PED(ped[pedIndex].id,giveWeapon,500,true)
						ENDIF
					ENDIF
					//SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_IGNORE_GROUP)
					SET_PED_CAN_BE_TARGETTED(ped[pedIndex].id,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(ped[pedIndex].id,CA_ALWAYS_FLEE,TRUE)
				BREAK
				case pedrole_wary
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[pedIndex].id,HASH_WARY_GROUP)	
				break
			ENDSWITCH
		endif
	ENDIF
	
	if bPlayersUseHeadsets = TRUE
		PRINTLN("tRY givING headset: ",pedIndex)
		if not IS_PED_INJURED(ped[pedIndex].id)
			PRINTLN("HEADSET ENTITY ALIVE")
			PRINTLN("HEADSET ENTITY MODEL = ",GET_ENTITY_MODEL(ped[pedIndex].id))
			PRINTLN("GET_PLAYER_PED_MODEL(char_franklin) = ",GET_PLAYER_PED_MODEL(char_franklin))
			PRINTLN("GET_PLAYER_PED_MODEL(CHAR_TREVOR) = ",GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			PRINTLN("GET_PLAYER_PED_MODEL(CHAR_MICHAEL) = ",GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			if GET_ENTITY_MODEL(ped[pedIndex].id) = GET_PLAYER_PED_MODEL(char_franklin)	
				SET_PED_COMP_ITEM_CURRENT_SP(ped[pedIndex].id, COMP_TYPE_PROPS, PROPS_P1_HEADSET)
			ENDIF
			if GET_ENTITY_MODEL(ped[pedIndex].id) = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
				SET_PED_COMP_ITEM_CURRENT_SP(ped[pedIndex].id, COMP_TYPE_PROPS, PROPS_P2_HEADSET)
			ENDIF
			if GET_ENTITY_MODEL(ped[pedIndex].id) = GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
				SET_PED_COMP_ITEM_CURRENT_SP(ped[pedIndex].id, COMP_TYPE_PROPS, PROPS_P0_HEADSET)
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

proc initPedNoOffset(int pedIndex, MODEL_NAMES model, vector pedSpawnCoords, float pedHeading, VEHICLE_INDEX vehicleToSpawnIn = NULL, VEHICLE_SEAT vehicleSeatToSpawnIn=VS_DRIVER, pedRoleEnum pedrole=pedrole_void, WEAPON_TYPE giveWeapon=WEAPONTYPE_INVALID, bool bUseRandomVariation=FALSE)
	initPed(pedIndex,model,pedSpawnCoords,pedHeading,vehicleToSpawnIn,vehicleSeatToSpawnIn,pedrole,giveWeapon,bUseRandomVariation,TRUE)
ENDPROC

PROC initPedRandom(int pedIndex, MODEL_NAMES model, vector pedSpawnCoords, float pedHeading, VEHICLE_INDEX vehicleToSpawnIn = NULL, VEHICLE_SEAT vehicleSeatToSpawnIn=VS_DRIVER, pedRoleEnum pedrole=pedrole_void)
	initPed(pedIndex,model,pedSpawnCoords,pedHeading,vehicleToSpawnIn,vehicleSeatToSpawnIn,pedrole,true)
ENDPROC

proc initPedOnFoot(int pedIndex, MODEL_NAMES model, vector pedSpawnCoords, float pedHeading, pedRoleEnum pedrole=pedrole_void, weapon_type giveWeapon=WEAPONTYPE_INVALID, bool bUseRandomVariation=FALSE)
	initPed(pedIndex,model,pedSpawnCoords,pedHeading,null,VS_DRIVER,pedrole,giveWeapon,bUseRandomVariation)
ENDPROC

proc initPedInVehicle(int pedIndex, MODEL_NAMES model,VEHICLE_INDEX vehicleToSpawnIn = NULL, VEHICLE_SEAT vehicleSeatToSpawnIn=VS_DRIVER, pedRoleEnum pedrole=pedrole_void, weapon_type giveWeapon=WEAPONTYPE_INVALID)
	initPed(pedIndex,model,vNull,0.0,vehicleToSpawnIn,vehicleSeatToSpawnIn,pedrole,giveWeapon)
ENDPROC


proc keepTasks(int pedID)
	if not IS_PED_INJURED(ped[pedID].id)
		SET_PED_KEEP_TASK(ped[pedID].id,TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(ped[pedID].id)
	ENDIF
ENDPROC

//vehicles

PROC CHANGE_PLAYER(SELECTOR_SLOTS_ENUM selPed)
	PRINTLN("Changing ped to",ENUM_TO_INT(selPed))
	int i
	REPEAT COUNT_OF(ped) i
		if DOES_ENTITY_EXIST(ped[i].id)
			if GET_ENTITY_MODEL(ped[i].id) != GET_ENTITY_MODEL(PLAYER_PED_ID())			
				ped[i].id = NULL			
			ENDIF
		ENDIF
	ENDREPEAT
	

	switch selPed
		case SELECTOR_PED_FRANKLIN
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
				wait(0)
			ENDWHILE
		BREAK
		
		case SELECTOR_PED_MICHAEL
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				wait(0)
			ENDWHILE
		BREAK
		
		case SELECTOR_PED_TREVOR
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
				wait(0)
			ENDWHILE
		BREAK
	ENDSWITCH
ENDPROC

proc initVehicle(int vehicleIndex, MODEL_NAMES model, vector pos, float heading, float velocity=-100.0)
	if vehicleIndex =-1 or vehicleIndex  >= max_veh_count
		SCRIPT_ASSERT("ASSERT: initVehicle() attempting to create vehicle with invalid array entry.")
	ENDIF
	
	//delete veh if model name is not same
	if IS_VEHICLE_DRIVEABLE(vehicle[vehicleIndex].id)
		if GET_ENTITY_MODEL(vehicle[vehicleIndex].id) != model and model != DUMMY_MODEL_FOR_SCRIPT
			DELETE_VEHICLE(vehicle[vehicleIndex].id)
		ENDIF
	ENDIF
	
	//debugMessage(debug_array,"aa11 vehicleIndex = ","",vehicleIndex)
	if not DOES_ENTITY_EXIST(vehicle[vehicleIndex].id)
		vehicle[vehicleIndex].id = CREATE_VEHICLE(model,pos,heading)
	ELSE
		IF NOT IS_ENTITY_DEAD(vehicle[vehicleIndex].id)
			SET_ENTITY_COORDS(vehicle[vehicleIndex].id,pos)
			SET_ENTITY_HEADING(vehicle[vehicleIndex].id,heading)
		ELSE //vehicle exists but has been destroyed
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[vehicleIndex].id)
			vehicle[vehicleIndex].id = CREATE_VEHICLE(model,pos,heading)
		ENDIF
	ENDIF
	
	if IS_VEHICLE_DRIVEABLE(vehicle[vehicleIndex].id)
		if velocity != -100.0
			SET_VEHICLE_FORWARD_SPEED(vehicle[vehicleIndex].id,velocity)			
		ENDIF
		if IS_ENTITY_A_MISSION_ENTITY(vehicle[vehicleIndex].id)
			SET_VEHICLE_CAN_LEAK_OIL(vehicle[vehicleIndex].id,FALSE)
			SET_VEHICLE_CAN_LEAK_PETROL(vehicle[vehicleIndex].id,FALSE)
		ENDIF
	ENDIF
ENDPROC

		

//doors

func bool set_door_lock(MODEL_NAMES thisModel,vector vCoord,bool bIsLocked,float fOpenRatio=-2.0)
	bool bempty
	float fEmpty,fGetOpenRatio
	bool bGetLockState
	if DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vCoord,5.0,thisModel)
		If fOpenRatio != -2.0	
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(thisModel,vCoord,bIsLocked,fOpenRatio)
			GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(thisModel,vCoord,bGetLockState,fGetOpenRatio)
			IF bGetLockState = bIsLocked and fGetOpenRatio = fOpenRatio			
				return TRUE
			ENDIF
		ELSE
			GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(thisModel,vCoord,bempty,fEmpty)			
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(thisModel,vCoord,bIsLocked,fEmpty)
			return TRUE
		ENDIF	
	ENDIF
	return FALSE
ENDFUNC

//hiding entities for cutscenes
proc show_entities(bool bShow, ped_index ped1a, ped_index ped1b=null, ped_index ped1c=null)
	if not IS_PED_INJURED(ped1a)
		SET_ENTITY_VISIBLE(ped1a,bShow)
	ENDIF
	if not IS_PED_INJURED(ped1b)
		SET_ENTITY_VISIBLE(ped1b,bShow)
	ENDIF
	if not IS_PED_INJURED(ped1c)
		SET_ENTITY_VISIBLE(ped1c,bShow)
	ENDIF
ENDPROC

//************************** conversation stuff *************************

FUNC BOOL IS_THIS_CONVERSATION_ROOT_PLAYING(STRING RootToCheck)
	TEXT_LABEL_23 blah
	blah  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	IF ARE_STRINGS_EQUAL(blah,RootToCheck)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
structPedsForConversation MyLocalPedStruct
string convBlock

FUNC BOOL IS_STRING_VOID(STRING sString)
	IF IS_STRING_NULL(sString)
		//DEBUG_MESSAGE("IS_STRING_EMPTY: String is null!")
		RETURN TRUE
	ENDIF
	IF (GET_LENGTH_OF_LITERAL_STRING(sString) <= 0)
		//DEBUG_MESSAGE("IS_STRING_EMPTY: String is empty!")
		RETURN TRUE
	ENDIF
	
	if ARE_STRINGS_EQUAL(sString,"NULL")
		return TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/*
CONST_INT MAX_CONVERSATIONS 6

struct structConversations	
	string conv_string
	int conv_entry
	int conv_duration, conv_delay
	bool conv_exists, primed, playing
	PED_INDEX conv_ped, conv_lookAtPed
ENDSTRUCT



structConversations conversations[MAX_CONVERSATIONS]

CONST_INT MAX_DIALOGUE_PEDS 8


struct structPedDialogueBank
	bool added
	bool setToNull
	int pedID
	TEXT_LABEL_15 name
ENDSTRUCT

structPedDialogueBank pedDialogueBank[MAX_DIALOGUE_PEDS]

proc clear_all_ped_dialogues()
	//debugMessage(DEBUG_CONVERSATION,"**********************clear_all_ped_dialogues()*********************")
	int i
	for i = 0 to MAX_DIALOGUE_PEDS-1
		REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,i)
		pedDialogueBank[i].added = FALSE
		pedDialogueBank[i].setToNull = FALSE
	endfor	
ENDPROC


proc ADD_DIALOGUE_PED(structPedsForConversation strPeds, int blockEntry, ped_index pedID, string nameString)
	strPeds=strPeds
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,blockEntry,pedID,nameString)
ENDPROC

proc checkAddDialoguePed()	
	int i
	for i = 0 to MAX_DIALOGUE_PEDS-1
		if pedDialogueBank[i].pedID != -1
			if pedDialogueBank[i].added = FALSE
				if not Is_String_Null_Or_Empty(pedDialogueBank[i].name)
					if not is_ped_injured(ped[pedDialogueBank[i].pedID].id)		
						PRINTLN("+++++++++++++++++++++++Add named ped to dialogue ",i)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,i,ped[pedDialogueBank[i].pedID].id,pedDialogueBank[i].name)
						pedDialogueBank[i].added = TRUE											
					ELSE	
						PRINTLN("+++++++++++++++++++++++Add null ped to dialogue",i)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,i,null,pedDialogueBank[i].name)						
						pedDialogueBank[i].added = TRUE
						pedDialogueBank[i].setToNull = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			if not Is_String_Null_Or_Empty(pedDialogueBank[i].name)
				if pedDialogueBank[i].added = FALSE
					PRINTLN("+++++++++++++++++++++++Add null ped to dialogue",i)
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,i,null,pedDialogueBank[i].name)
					pedDialogueBank[i].added = TRUE
					pedDialogueBank[i].setToNull = TRUE
				ENDIF
			ENDIF
		ENDIF
				
		if pedDialogueBank[i].added = TRUE
			if pedDialogueBank[i].pedID != -1				
				if not is_ped_injured(ped[pedDialogueBank[i].pedID].id) //or not DOES_ENTITY_EXIST(ped[pedDialogueBank[i].pedID].id)
					if pedDialogueBank[i].setToNull = TRUE
						if not Is_String_Null_Or_Empty(pedDialogueBank[i].name)
							PRINTLN("+++++++++++++++++++++++Add named ped to dialogue after being null")
							REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,i)
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,i,null,pedDialogueBank[i].name)	
							pedDialogueBank[i].setToNull = FALSE
						ENDIF
					ENDIF
				ELSE
					if pedDialogueBank[i].setToNull = FALSE
						REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,i)
						PRINTLN("+++++++++++++++++++++++Set named ped to null dialogue",i)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,i,null,pedDialogueBank[i].name)
						pedDialogueBank[i].setToNull = TRUE
					ENDIF
				ENDIF				
			ENDIF
		ENDIF
		
	ENDFOR
ENDPROC


proc add_conversation(string block, int iDuration=-1,int delay=0, int Entry=-1)
	//debugMessage(DEBUG_CONVERSATION,"**********************add_conversation()*********************")
	//debugMessage(DEBUG_CONVERSATION,"add_conversation(): ",block)
	checkAddDialoguePed()
	if entry != -1 //debugMessage(DEBUG_CONVERSATION,"add_conversation() includes Entry: ","",entry) ENDIF
	//debugMessage(DEBUG_CONVERSATION,"add_conversation(): duration = ","",iDuration)
	//CLEAR_PRINTS()
	int i=0
	
	while conversations[i].conv_exists = TRUE
		i++
		//debugMessage(debug_array,"aa12 conversations[i] i= ","",i)
		if i =-1 or i  >= MAX_CONVERSATIONS
			SCRIPT_ASSERT("ASSERT: add_conversation() attempting to create conversation with invalid array entry.")
		ENDIF
	ENDWHILE
	//debugMessage(DEBUG_CONVERSATION,"add_conversation: found empty entry at","",i)
	//if i=0 clear_all_ped_dialogues() ENDIF
	


	if i < COUNT_OF(conversations)
		conversations[i].conv_string = block
		conversations[i].conv_entry = Entry
		conversations[i].conv_duration = iDuration
		conversations[i].conv_exists = true
		conversations[i].conv_delay = delay
		conversations[i].primed = FALSE
		conversations[i].playing = FALSE		
	ENDIF
ENDPROC




func string GET_CURRENT_CONVERSATION_BLOCK()
	if conversations[0].playing = true
		return conversations[0].conv_string
	ENDIF
	return ""
ENDFUNC


func bool IS_CONVERSATION_ENTRY_PLAYING(string block, int line=-1)
	//IF not IS_STRING_NULL(conversations[0].conv_entry) //GET_CURRENT_SCRIPTED_CONVERSATION_LINE()

	if line!=-1
		if GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = line
			if not IS_STRING_NULL(conversations[0].conv_string)
				if ARE_STRINGS_EQUAL(conversations[0].conv_string,block)
					return TRUE
				ENDIF
			ENDIF
		ENDIF
		
		if not IS_STRING_NULL(conversations[0].conv_string)
			If conversations[0].conv_entry = line
				if conversations[0].playing = true
					return TRUE		
				ENDIF
			ENDIF
		ENDIF		
	ENDIF

	

	return FALSE
ENDFUNC

func string GET_RANDOM_CONV_LINE(string lineA, string lineB, string lineC=null, string lineD=null)
	int lineCount=2
	int chosenLine

	if not is_string_null(lineC)
		lineCount++
	ENDIF
	if not is_string_null(lineD)
		lineCount++
	ENDIF
	chosenLine = GET_RANDOM_INT_IN_RANGE(0,lineCount)
	
	if chosenLine = 0 return lineA ENDIF
	if chosenLine = 1 return lineB ENDIF
	if chosenLine = 2 return lineC ENDIF
	if chosenLine = 3 return lineD ENDIF

	return ""
ENDFUNC

proc start_new_conversation(string block, int iDuration=-1,int delay=0, int Entry=-1)
	PRINTLN("KILL CONV A")
	KILL_ANY_CONVERSATION()
	//debugMessage(DEBUG_CONVERSATION,"**********************KILL_ANY_CONVERSATION() called*********************")
	add_conversation(block,iDuration,delay,Entry)
ENDPROC


func bool IS_CONVERSATION_QUEUED()	
	int i
	for i = 0 to MAX_CONVERSATIONS-1
		IF conversations[i].conv_exists = TRUE
			IF conversations[i].playing=FALSE
				return TRUE
			ENDIF
		ENDIF
	ENDFOR
	return FALSE
ENDFUNC

FUNC bool kill_all_conversations()
	//debugMessage(DEBUG_CONVERSATION,"**********************kill_all_conversations() called*********************")
	int i
//	//debugMessage(DEBUG_CONVERSATION,"kill_all_conversations()")
//	IF IS_SCRIPTED_CONVERSATION_ONGOING() OR IS_CONVERSATION_QUEUED() = TRUE
		//debugMessage(DEBUG_CONVERSATION,"kill_all_conversations(): IF IS_SCRIPTED_CONVERSATION_ONGOING() OR conv_queued = TRUE  TRUE")
		PRINTLN("KILL CONV B")
		KILL_ANY_CONVERSATION()
		//CLEAR_PRINTS()
		for i = 0 to MAX_CONVERSATIONS-1
			conversations[i].conv_exists = FALSE				
		ENDFOR
		RETURN TRUE
//	ELSE
//		//debugMessage(DEBUG_CONVERSATION,"kill_all_conversations(): IF IS_SCRIPTED_CONVERSATION_ONGOING() OR conv_queued = TRUE FALSE")
//		RETURN TRUE
//	ENDIF
//	RETURN TRUE
ENDFUNC



proc SET_CONVERSATION(string block, int iDuration=-1,int delay=0, int Entry=-1)
	//PRINTLN("CONV PLAY")
	kill_all_conversations()
	add_conversation(block,iDuration,delay,Entry)
ENDPROC


//enter a line number for which dialogue line to play
proc BUILD_CONVERSATION_LINE(string block, int iEntry, int duration=-1)
	SET_CONVERSATION(block, duration, 0, iEntry)
	//SET_CONVERSATION("cs2_tmp18",3000,0,"cs2_tmp18_1")
ENDPROC

proc SET_RANDOM_CONVERSATION(string block,int no_of_random_lines)
	SET_CONVERSATION(block,-1,0,GET_RANDOM_INT_IN_RANGE(1,no_of_random_lines+1))
ENDPROC


proc control_conversations()	

	int i
	//debugMessage(DEBUG_CONVERSATION,"control_conversations(): START")
	
	if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND conversations[0].playing = true
		//debugMessage(DEBUG_CONVERSATION,"control_conversations(): STOP_ANIM")
	
	
		for i = 1 to COUNT_OF(conversations) -1
			conversations[i-1] = conversations[i]
		ENDFOR
		conversations[COUNT_OF(conversations) -1].conv_exists = FALSE
	ENDIF
	
	if not IS_SCRIPTED_CONVERSATION_ONGOING()
		//debugMessage(DEBUG_CONVERSATION,"control_conversations(): checked conv ongoing. Returned False")
		
		if IS_CONVERSATION_QUEUED() = true	and conversations[0].playing = FALSE
			#IF IS_DEBUG_BUILD
				IF IS_STRING_NULL(convBlock)
					SCRIPT_ASSERT("convBlock is null. Need to init convBlock to dialogue block file name.")
				ENDIF
			#ENDIF
			
			IF conversations[0].primed = FALSE
				conversations[0].primed = TRUE
				conversations[0].conv_delay += GET_GAME_TIMER()
			ENDIF
			
			if GET_GAME_TIMER() > conversations[0].conv_delay 
				if conversations[0].conv_entry = -1

						if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, conversations[0].conv_string, CONV_PRIORITY_MEDIUM)					
							conversations[0].playing = true
							if conversations[0].conv_duration != -1
								conversations[0].conv_duration += GET_GAME_TIMER()
							ENDIF
						ENDIF

				ELSE				
					TEXT_LABEL_15 tPlayThisLine = conversations[0].conv_string
					tPlayThisLine+= "_"
					tPlayThisLine+= (conversations[0].conv_entry+1)
					if PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, convBlock, conversations[0].conv_string,tPlayThisLine, CONV_PRIORITY_MEDIUM)
						conversations[0].playing = true
						if conversations[0].conv_duration != -1
							conversations[0].conv_duration += GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF	
			ENDIF


		ENDIF		
	ELSE
		//debugMessage(DEBUG_CONVERSATION,"control_conversations(): checked conv ongoing. Returned TRUE")
		if conversations[0].conv_duration != -1
			if GET_GAME_TIMER() > conversations[0].conv_duration
				//debugMessage(DEBUG_CONVERSATION,"control_conversations(): KILL_ANY_CONVERSATION()")
				PRINTLN("KILL CONV C")
				KILL_ANY_CONVERSATION()
			ENDIF
		ENDIF
	ENDIF	
	
	if conversations[0].playing = TRUE
		//debugMessage(DEBUG_CONVERSATION,"***************** conv data ********************")
		//debugMessage(DEBUG_CONVERSATION,"conv_string = ",conversations[0].conv_string)
		//debugMessage(DEBUG_CONVERSATION,"conv_entry = ","",conversations[0].conv_entry)
		//debugMessage(DEBUG_CONVERSATION,"conv_duration = ","",conversations[0].conv_duration)
		if conversations[0].conv_exists = TRUE
			//debugMessage(DEBUG_CONVERSATION,"conv_exists = true")
		else
			//debugMessage(DEBUG_CONVERSATION,"conv_exists = false")
		endif
		if conversations[0].primed = TRUE
			//debugMessage(DEBUG_CONVERSATION,"primed = true")
		else
			//debugMessage(DEBUG_CONVERSATION,"primed = false")
		endif
		//debugMessage(DEBUG_CONVERSATION,"***************** end conv data ********************")
	ENDIF
	
	//debugMessage(DEBUG_CONVERSATION,"end of control conversation")
ENDPROC

func bool are_conversations_ongoing()
	if conversations[0].conv_exists = true or IS_SCRIPTED_CONVERSATION_ONGOING()
		return true
	ENDIF
	return FALSE
ENDFUNC





TEXT_LABEL_15 copDialogue, leaveCarDialogue//, lastConvRoot
//TEXT_LABEL_23 pauseConvLabel
vehicle_index leaveCar
bool bCopDialogueInUse, bCopDialogueUsed
bool bLeaveCarDialogueInUse, bLeaveCarDialogueUsed
int interrupt_timer
int conv_delay_time


proc define_cop_dialogue(string Dialogue)
	copDialogue = Dialogue
	bCopDialogueInUse=FALSE
	bCopDialogueUsed=TRUE	
ENDPROC

proc clear_cop_dialogue()
	copDialogue = ""
	bCopDialogueInUse=FALSE
	bCopDialogueUsed=FALSE
ENDPROC

proc define_leave_car_dialogue(string Dialogue, vehicle_index carToLeave)
	leaveCarDialogue = Dialogue
	leaveCar = carToLeave
	bLeaveCarDialogueUsed  = TRUE
	bLeaveCarDialogueInUse=FALSE
ENDPROC

proc clear_leave_car_dialogue()	
	leaveCarDialogue = ""
	leaveCar = null
	bLeaveCarDialogueUsed  = FALSE
	bLeaveCarDialogueInUse=FALSE
	interrupt_timer=0
ENDPROC

bool story_conv_plays_single_line,story_conv_plays_from_line,story_conv_interrupts
TEXT_LABEL_23 story_root_request, story_line_request,story_root_playing,story_line_playing

bool is_interrupt_ongoing, stored_line_is_playing
TEXT_LABEL_23 stored_conv_line,stored_conv_root

proc reviseWhatsPlaying()
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		TEXT_LABEL_23 whatsPlaying = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		TEXT_LABEL_23 whatLinePlaying = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		if not IS_STRING_NULL_OR_EMPTY(whatsPlaying)
			story_root_playing = whatsPlaying	
			story_line_playing = whatLinePlaying
		ELSE
			story_root_playing = ""
			story_line_playing = ""
		ENDIF
	ELSE
		if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			story_root_playing = ""
			story_line_playing = ""
		ENDIF
	ENDIF
ENDPROC

func bool is_requested_line_playing(string checkRoot, string checkLine = null)
	reviseWhatsPlaying()
	if are_strings_equal(story_root_playing,checkRoot)
		if IS_STRING_NULL_OR_EMPTY(checkLine)
			return TRUE
		ELSE
			if are_strings_equal(story_line_playing,checkLine)
				return TRUE
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC



proc clear_request()
	story_root_request = ""
	story_line_request = ""
	story_conv_interrupts = FALSE
	story_conv_plays_single_line = FALSE	
	story_conv_plays_from_line=FALSE
ENDPROC

proc clear_all_dialogue()
	KILL_ANY_CONVERSATION()	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	clear_request()
	clear_cop_dialogue()
	clear_cop_dialogue()
ENDPROC

func bool request_conversation_from_line(string requested_root, string requested_line, bool interrupts = FALSE)
	if is_requested_line_playing(requested_root)
		return TRUE
	ELSE
		story_root_request = requested_root	
		story_line_request = requested_line
		story_conv_interrupts = interrupts
		story_conv_plays_single_line = FALSE
		story_conv_plays_from_line=TRUE
	ENDIF

	return FALSE
ENDFUNC

func bool request_conversation_line(string requested_root, string requested_line, bool interrupts = FALSE)
	if is_requested_line_playing(requested_root,requested_line)
		return TRUE
	ELSE
		story_root_request = requested_root	
		story_line_request = requested_line
		story_conv_interrupts = interrupts		
		story_conv_plays_single_line = TRUE	
		story_conv_plays_from_line=FALSE
	ENDIF


	return FALSE
ENDFUNC

func bool request_conversation(string requested_root, bool interrupts = FALSE, int delayTime=0 )

	if is_requested_line_playing(requested_root)
		return TRUE	
	ELSE
		story_root_request = requested_root	
		story_line_request = NULL_STRING()
		story_conv_interrupts = interrupts
		story_conv_plays_single_line = FALSE	
		story_conv_plays_from_line=FALSE
		if conv_delay_time = 0
			conv_delay_time = GET_GAME_TIMER() + delayTime
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

func bool check_interrupts()
	bLeaveCarDialogueInUse=FALSE
	if bLeaveCarDialogueUsed
		if not is_string_null(leaveCarDialogue)		
			if DOES_ENTITY_EXIST(leaveCar)
				if IS_VEHICLE_DRIVEABLE(leaveCar)						
					if not IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),leaveCar)
						if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())																																												
							bLeaveCarDialogueInUse=TRUE
							return TRUE							
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	if bCopDialogueUsed
		if not is_string_null(copDialogue)
			if GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
				bCopDialogueInUse = TRUE	
				return TRUE
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

func bool check_stored_lines()	
	if not is_string_void(stored_conv_line)
		return TRUE		
	ENDIF	
	return FALSE
ENDFUNC

func bool check_story_lines()

	if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		if not IS_STRING_VOID(story_root_request)
		//	story_root_playing = NULL_STRING()
		//	story_line_playing = NULL_STRING()
		//	story_line_playing=story_line_playing
			return TRUE
		ENDIF
	ELSE
		if not ARE_STRINGS_EQUAL(story_root_request,story_root_playing)
			if story_conv_interrupts = TRUE
		//		PRINTLN("KILL CONV E")
				KILL_ANY_CONVERSATION()
		//		story_root_playing = NULL_STRING()
		//		story_line_playing = NULL_STRING()
				return TRUE
			ENDIF
		ENDIF
	ENDIF
	conv_delay_time = 0
	return FALSE		
ENDFUNC



proc manage_conversations()	

	reviseWhatsPlaying()
	
	
	
	
	if check_interrupts()
		if is_interrupt_ongoing = FALSE
			stored_conv_line = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			if not is_string_void(stored_conv_line)
				stored_conv_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			ENDIF
			KILL_ANY_CONVERSATION()
			//PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
			interrupt_timer = GET_GAME_TIMER() + 1000
			stored_line_is_playing = FALSE
			is_interrupt_ongoing = TRUE
		ELSE
			if not IS_MESSAGE_BEING_DISPLAYED() or (IS_MESSAGE_BEING_DISPLAYED() and IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
				if GET_GAME_TIMER() > interrupt_timer
					if bLeaveCarDialogueInUse
						checkAddDialoguePed()
						if CREATE_CONVERSATION(MyLocalPedStruct,convBlock,leaveCarDialogue,CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							interrupt_timer = GET_GAME_TIMER() + 5000
						ENDIF
					ELIF bCopDialogueInUse = TRUE
						checkAddDialoguePed()
						if not are_strings_equal(story_root_playing,copDialogue)							
							if CREATE_CONVERSATION(MyLocalPedStruct,convBlock,copDialogue,CONV_PRIORITY_MEDIUM)								
								interrupt_timer = GET_GAME_TIMER() + 5000
							ENDIF							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE 
		
	
		if is_interrupt_ongoing = TRUE
			is_interrupt_ongoing = FALSE
		ENDIF
		if not IS_MESSAGE_BEING_DISPLAYED() or (IS_MESSAGE_BEING_DISPLAYED() and IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
			if check_stored_lines()	
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					if stored_line_is_playing = TRUE
						stored_line_is_playing = FALSE
						stored_conv_root = NULL_STRING()
						stored_conv_line = NULL_STRING()
					ELSE
						checkAddDialoguePed()
						//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						if GET_GAME_TIMER() > conv_delay_time
							if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,convBlock,stored_conv_root,stored_conv_line,CONV_PRIORITY_MEDIUM)
							//	PRINTLN("I'm playing interrupt line ",stored_conv_root)
								story_root_playing = stored_conv_root
								story_line_playing = stored_conv_line
								stored_line_is_playing = TRUE
								clear_request()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF check_story_lines()
				checkAddDialoguePed()
				if story_conv_plays_single_line = TRUE
					if GET_GAME_TIMER() >= conv_delay_time
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct,convBlock,story_root_request,story_line_request,CONV_PRIORITY_MEDIUM)
							PRINTLN("Line started: ",story_line_request)
							story_root_playing = story_root_request
							story_line_playing = story_line_request
							clear_request()
						ENDIF
					ENDIF
				ELSE
					IF story_conv_plays_from_line = TRUE
						if GET_GAME_TIMER() >= conv_delay_time
							if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,convBlock,story_root_request,story_line_request,CONV_PRIORITY_MEDIUM)
								story_root_playing = story_root_request
								story_line_playing = story_line_request
								clear_request()
							ENDIF
						ENDIF
					ELSE
						if GET_GAME_TIMER() >= conv_delay_time
							if CREATE_CONVERSATION(MyLocalPedStruct,convBlock,story_root_request,CONV_PRIORITY_MEDIUM)
								PRINTLN("CREATED CONV ROOT: ",story_root_request)
								story_root_playing = story_root_request								
								clear_request()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF

endproc

/*
TEXT_LABEL_23 current_conv_root = NULL_STRING(),current_conv_line = NULL_STRING()
TEXT_LABEL_23 stored_conv_root = NULL_STRING(), stored_conv_line = NULL_STRING()
TEXT_LABEL_23 requested_conv_root = NULL_STRING(), requested_conv_line = NULL_STRING()

bool is_conv_queued=FALSE, next_conv_interrupts, bPlayingLineIsStored
bool next_conv_plays_from_line,next_conv_plays_single_line

proc updateCurrentLines()
	if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		if not IS_CONVERSATION_QUEUED()
			if IS_SCRIPTED_CONVERSATION_ONGOING()
				current_conv_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				current_conv_line = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
			ENDIF
		ENDIF
	ELSE
		current_conv_root = NULL_STRING()
		current_conv_line = NULL_STRING()
	ENDIF
	
	PRINTLN("current_conv_root",current_conv_root)
ENDPROC

func bool request_conversation_from_line(string requested_root, string requested_line, bool interrupts = FALSE)
	if are_strings_equal(current_conv_root,requested_root) and are_strings_equal(current_conv_line,requested_line)
		return TRUE
	ENDIF	
	PRINTLN("request_conversation_from_line() is_conv_queued = true")
	is_conv_queued = TRUE
	next_conv_plays_from_line = TRUE
	requested_conv_root = requested_root
	requested_conv_line = requested_line
	if interrupts = true next_conv_interrupts = TRUE endif	
	return FALSE
ENDFUNC

func bool request_conversation_line(string requested_root, string requested_line, bool interrupts = FALSE)
	
	if are_strings_equal(current_conv_root,requested_root) and are_strings_equal(current_conv_line,requested_line)
		return TRUE
	ENDIF
	PRINTLN("request_conversation_line() is_conv_queued = true")
	is_conv_queued = TRUE
	next_conv_plays_single_line = TRUE
	requested_conv_root = requested_root
	requested_conv_line = requested_line
	if interrupts = true next_conv_interrupts = TRUE endif	
	return FALSE
ENDFUNC

func bool request_conversation(string requested_root, bool interrupts = FALSE )

	if are_strings_equal(current_conv_root,requested_root)
		return TRUE
	ENDIF
	PRINTLN("request_conversation() is_conv_queued = true")
	is_conv_queued = TRUE
	requested_conv_root = requested_root
	if interrupts = true next_conv_interrupts = TRUE endif	
	return FALSE
ENDFUNC



func bool has_conv_been_interrupted()
	bLeaveCarDialogueInUse=FALSE
	if bLeaveCarDialogueUsed
		if not is_string_null(leaveCarDialogue)		
			if DOES_ENTITY_EXIST(leaveCar)
				if IS_VEHICLE_DRIVEABLE(leaveCar)					
					if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),leaveCar)
						if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())																																					
							showIntOnScreen(0.1,0.1,0,"Left car")
							bLeaveCarDialogueInUse=TRUE
							return TRUE							
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	if bCopDialogueUsed
		if not is_string_null(copDialogue)
			if GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0																					
				bCopDialogueInUse = TRUE	
				return TRUE
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

func bool is_conv_stored()
	PRINTLN("is_conv_stored()")
	if bPlayingLineIsStored = TRUE
		if not IS_STRING_VOID(stored_conv_root)
			PRINTLN("	stored_conv_root = ",stored_conv_root)
			return TRUE
		ELSE
			bPlayingLineIsStored = FALSE
			return FALSE
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

func bool is_conv_requested()	
	if is_conv_queued = TRUE
		println("	is_conv_requested()")
		println("	requested_conv_root: ",requested_conv_root)
		println("	requested_conv_line: ",requested_conv_line)
		if not is_string_null(stored_conv_root)
			stored_conv_root=NULL_STRING()
			stored_conv_line=NULL_STRING()
			bPlayingLineIsStored = FALSE
		ENDIF
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

func bool is_conv_ongoing()
	PRINTLN("is_conv_ongoing(): current_conv_root",current_conv_root)
	if IS_STRING_VOID(current_conv_root)
		PRINTLN("is_conv_ongoing(): current_conv_root is NULL returning false")
		return FALSE
	ENDIF
	PRINTLN("is_conv_ongoing(): current_conv_root is not null or empty returning true")
	return TRUE
ENDFUNC

func bool can_requested_conversation_interrupt()
	if next_conv_interrupts = TRUE
		KILL_ANY_CONVERSATION()
		return TRUE
	ENDIF
	return FALSE
endfunc

proc clearConvQueue()
	PRINTLN("clearConvQueue()")
	is_conv_queued=FALSE
	next_conv_plays_from_line = FALSE
	next_conv_plays_single_line = FALSE
	requested_conv_root = NULL_STRING()
	requested_conv_line = NULL_STRING()
ENDPROC

proc request_stored_conversation()
	PRINTLN("request_stored_conversation() using root: ", stored_conv_root," line: ",stored_conv_line)
	request_conversation_from_line(stored_conv_root,stored_conv_line)
ENDPROC

func bool is_stored_conversation_playing()
	
	TEXT_LABEL_23 txtA = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	TEXT_LABEL_23 txtB = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	if are_strings_equal(txtA,stored_conv_root)
	and are_strings_equal(txtB,stored_conv_line)
		stored_conv_root=NULL_STRING()
		stored_conv_line = NULL_STRING()
		PRINTLN("is_stored_conversation_playing() TRUE")
		return TRUE
	ENDIF
	PRINTLN("is_stored_conversation_playing() FALSE")
	return FALSE
ENDFUNC

proc build_requested_conversation()
	PRINTLN("build_requested_conversation()")
	if is_conv_queued = TRUE
		checkAddDialoguePed()
		if next_conv_plays_from_line = TRUE
			if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, convBlock, requested_conv_root,requested_conv_line,CONV_PRIORITY_MEDIUM)	
				current_conv_root = requested_conv_root
				current_conv_line = requested_conv_line
				clearConvQueue()
			ENDIF
		ELIF next_conv_plays_single_line = TRUE
			if PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct,convBlock,requested_conv_root,requested_conv_line,CONV_PRIORITY_MEDIUM)
				current_conv_root = requested_conv_root
				current_conv_line = requested_conv_line
				clearConvQueue()
			ENDIF
		ELSE
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, requested_conv_root,CONV_PRIORITY_MEDIUM)
				current_conv_root = requested_conv_root				
				clearConvQueue()
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

proc control_interrupt_dialogue()
	PRINTLN("control_interrupt_dialogue()")
	if bLeaveCarDialogueInUse=TRUE
		if bPlayingLineIsStored = FALSE			
			stored_conv_line = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()			
			if not IS_STRING_VOID(stored_conv_line)
				stored_conv_root = current_conv_root				
				PRINTLN("	stored_conv_root = ",stored_conv_root)
			ELSE
				stored_conv_root = NULL_STRING()
				PRINTLN("	no future line to store")
			ENDIF
			KILL_ANY_CONVERSATION()
			current_conv_root = NULL_STRING()
			current_conv_line = NULL_STRING()		
			bPlayingLineIsStored = TRUE
		ENDIF
		if GET_GAME_TIMER() > interrupt_timer
			checkAddDialoguePed()
			if CREATE_CONVERSATION(MyLocalPedStruct,convBlock,leaveCarDialogue,CONV_PRIORITY_MEDIUM)
				clearConvQueue()
				interrupt_timer = GET_GAME_TIMER() + 6000
			ENDIF
		ENDIF
	ENDIF
	if bCopDialogueInUse=TRUE
		if bPlayingLineIsStored = FALSE
			
			stored_conv_line = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			if not IS_STRING_VOID(stored_conv_line)
				stored_conv_root = current_conv_root
				PRINTLN("	stored_conv_root = ",stored_conv_root)
			ELSE
				stored_conv_root = NULL_STRING()
			ENDIF
			KILL_ANY_CONVERSATION()
			current_conv_root = NULL_STRING()
			current_conv_line = NULL_STRING()
			bPlayingLineIsStored = TRUE
		ENDIF
		if GET_GAME_TIMER() > interrupt_timer
			checkAddDialoguePed()
			if CREATE_CONVERSATION(MyLocalPedStruct,convBlock,copDialogue,CONV_PRIORITY_MEDIUM)
				clearConvQueue()
				interrupt_timer = GET_GAME_TIMER() + 6000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

proc manage_conversations()
	PRINTLN(" ")
	PRINTLN("---------------------------manage_conversations: start----------------------")
	updateCurrentLines()		
	
		
	
	
	if not has_conv_been_interrupted()
		PRINTLN("manage_conversations: 	has_conv_been_interrupted() FALSE")
		IF is_conv_stored()
			if is_stored_conversation_playing()
			ELSE			
				request_stored_conversation()
				build_requested_conversation()
			ENDIF
		ELSE
			if is_conv_requested()
				PRINTLN("manage_conversations: is_conv_requested() true")
				if is_conv_ongoing()
					PRINTLN("manage_conversations: is_conv_ongoing() true")
					if can_requested_conversation_interrupt()
						PRINTLN("manage_conversations: can_requested_conversation_interrupt()")
						build_requested_conversation()
					ENDIF
				ELSE
					PRINTLN("manage_conversations: is_conv_ongoing() false")
					build_requested_conversation()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("manage_conversations: has_conv_been_interrupted() true")
		control_interrupt_dialogue()
	ENDIF
		
	//current_conv_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	//current_conv_line = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	
	
	PRINTLN("current_conv_root",current_conv_root)
	PRINTLN("stored_conv_root",stored_conv_root)
	PRINTLN("requested_conv_root",requested_conv_root)
	PRINTLN("*********************** manage_conversations: end *****************************")	
ENDPROC

func TEXT_LABEL_15 GET_CURRENT_CONV_ROOT()
	RETURN current_conv_root	
ENDFUNC

func TEXT_LABEL_15 GET_CURRENT_CONV_LINE()
	RETURN current_conv_line
ENDFUNC
/*
func bool make_conversation(string rootLabel, bool interruptCurrentLine=FALSE, string fromLine=null, bool isAConvInterruptLine=FALSE, string singleLineToPlay=NULL)

	if isAConvInterruptLine = FALSE						
		if not is_string_null(copDialogue)
			if GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
				return FALSE
			ENDIF
		ENDIF
		
		if not is_string_null(leaveCarDialogue)
			if DOES_ENTITY_EXIST(leaveCar)
				if IS_VEHICLE_DRIVEABLE(leaveCar)
					if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),leaveCar)
					//	if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
							return FALSE
					//	ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		if interruptCurrentLine = FALSE
			if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				return FALSE
			ENDIF
		ELSE			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			KILL_ANY_CONVERSATION()
		ENDIF
	ELSE	
		if IS_SCRIPTED_CONVERSATION_ONGOING()
			pauseConvLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			lastConvRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			PRINTLN("lastConvRoot:", lastConvRoot," pauseConvLabel: ",pauseConvLabel)
		ENDIF
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		KILL_ANY_CONVERSATION()
	ENDIF
	
	if IS_MESSAGE_BEING_DISPLAYED()
		return FALSE
	ENDIF
	
	
	checkAddDialoguePed()
	
	if not Is_String_Null_Or_Empty(fromLine)
		if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, convBlock, rootLabel,fromLine,CONV_PRIORITY_MEDIUM)						
			PRINTLN("Conv interrupt created at time: ",GET_GAME_TIMER())
			return TRUE	
		ENDIF
	ELSE
		if not Is_String_Null_Or_Empty(singleLineToPlay)
			if PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, convBlock, rootLabel,singleLineToPlay,CONV_PRIORITY_MEDIUM)
				return TRUE
			ENDIF
		ELSE
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, rootLabel, CONV_PRIORITY_MEDIUM)	
				PRINTLN("Conv created at time: ",GET_GAME_TIMER())
				return TRUE	
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

func bool make_conversation_line(string rootLabel, string singleLineToPlay=NULL)
	if make_conversation(rootLabel, FALSE, NULL_STRING(), FALSE, singleLineToPlay)
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

proc CHECK_INTERRUPT_DIALOGUE()	
	if bCopDialogueUsed
		if bCopDialogueInUse = FALSE
			if not is_string_null(copDialogue)
				if GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0																					
					
					
					if make_conversation(copDialogue,TRUE,NULL_STRING(),TRUE)
						PRINTLN(" ------> Play wanted level conversation")
						bCopDialogueInUse = TRUE						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			if GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0				
				if not Is_String_Null_Or_Empty(pauseConvLabel)									
					if make_conversation(lastConvRoot,true,pauseConvLabel,TRUE)
						bCopDialogueInUse = FALSE
						PRINTLN(" ------> Play paused conversation after wanted level reset")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	if bLeaveCarDialogueUsed
		if not is_string_null(leaveCarDialogue)		
			if DOES_ENTITY_EXIST(leaveCar)
				if IS_VEHICLE_DRIVEABLE(leaveCar)
					if bLeaveCarDialogueInUse = FALSE
						if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),leaveCar)
							if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())																														
								if make_conversation(leaveCarDialogue,true,NULL_STRING(),TRUE)
									PRINTLN("pauseConvLabel after: ",pauseConvLabel)
									PRINTLN(" ------> Play leave car dialogue for first time")
									pausebetweenLines = GET_GAME_TIMER() + 5000
									bLeaveCarDialogueInUse = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE						
						if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),leaveCar)													
							if make_conversation(lastConvRoot,true,pauseConvLabel)
								PRINTLN(" ------> Play back in car dialogue paused conversation. Root: ",lastConvRoot," pauseConvLabel: ",pauseConvLabel )
								bLeaveCarDialogueInUse = FALSE	
							ENDIF
						ELSE
							if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),leaveCar)
								if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())	
									if GET_GAME_TIMER() > pausebetweenLines 
										if make_conversation(leaveCarDialogue,FALSE,NULL_STRING(),TRUE)
											PRINTLN(" ------> Play another in car dialogue line")
											pausebetweenLines = GET_GAME_TIMER() + 5000
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC
*/
string sPrintMessage = null
string sConvLabel = null
int iPlayingEntry = -1

enum PRINT_STATUS
	PRN_NEVER_DISPLAYED,
	PRN_QUEUING,
	PRN_DISPLAYING,
	PRN_FINISHED
ENDENUM

func PRINT_STATUS get_conversation_status(string convLabel, int iEntry=-1)

	if iEntry != -1
		if IS_THIS_CONVERSATION_ROOT_PLAYING(convLabel)
			IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = iEntry
				sConvLabel = convLabel
				iPlayingEntry = iEntry
				return PRN_DISPLAYING
			ENDIF
		ELSE
			if not IS_STRING_NULL(sConvLabel)
				if are_strings_equal(sConvLabel,convLabel) and iEntry = iPlayingEntry
					return PRN_FINISHED
				ELSE
					sConvLabel = NULL_STRING()
					return PRN_NEVER_DISPLAYED					
				ENDIF
			ELSE
				return PRN_NEVER_DISPLAYED
			ENDIF
		ENDIF
	ELSE		
		if IS_THIS_CONVERSATION_ROOT_PLAYING(convLabel)
			sConvLabel = convLabel
			return PRN_DISPLAYING
		ELSE
			if not IS_STRING_NULL(sConvLabel)
				if are_strings_equal(sConvLabel,convLabel)
					return PRN_FINISHED
				ELSE
					sConvLabel = NULL_STRING()
					return PRN_NEVER_DISPLAYED
				ENDIF
			ELSE								
				return PRN_NEVER_DISPLAYED				
			ENDIF
		ENDIF
	ENDIF


	return PRN_NEVER_DISPLAYED
ENDFUNC

/*
func PRINT_STATUS TRACK_CONVERSATION_STATUS(string tConvLabel, int &trackerInt, int playingLabel=-1)
	TEXT_LABEL_23 txt,txtReturn, txtLblReturn, txtLbl
	txt += tConvLabel
	txtReturn = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	txtLblReturn = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	txtLbl += txt
	txtLbl += "_"
	txtLbl += (playingLabel+1)
	switch trackerInt
		case 0						
			if are_strings_equal(txt,txtReturn)
				if playingLabel = -1
					trackerInt = 1
					return PRN_DISPLAYING
				ELSE
					if are_strings_equal(txtLbl,txtLblReturn)
						return PRN_DISPLAYING
					ENDIF
				ENDIF
			ENDIF			
		break
		case 1
			if is_string_null(txtReturn)
				trackerInt = 2
				return PRN_FINISHED
			ELSE
				if not are_strings_equal(txt,txtReturn)
					trackerInt = 2
					return PRN_FINISHED
				ELSE
					if playingLabel != -1
						if not are_strings_equal(txtLbl,txtLblReturn) 
							trackerInt = 2
							return PRN_FINISHED
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			return PRN_DISPLAYING
		BREAK
		case 2
			return PRN_FINISHED
		BREAK
	ENDSWITCH
	return PRN_NEVER_DISPLAYED
ENDFUNC

//need to reset sPrintMessage if mission stage skip is used.
func PRINT_STATUS get_conversation_status(string convLabel, int iEntry=-1)

	if iEntry != -1
	
		if IS_CONVERSATION_ENTRY_PLAYING(convLabel,iEntry)
			sConvLabel = convLabel
			iPlayingEntry = iEntry
			return PRN_DISPLAYING
		ELSE
			if iPlayingEntry = iEntry
				return PRN_FINISHED
			ENDIF
		ENDIF
	ELSE		
		if conversations[0].conv_exists = true and ARE_STRINGS_EQUAL(conversations[0].conv_string,convLabel)
			if conversations[0].playing = TRUE
				sConvLabel = convLabel
				iPlayingEntry = -1
				return PRN_DISPLAYING
			ELSE
				//conversation is waiting to play due to a delay given.
				return PRN_QUEUING
			ENDIF
		ELSE
			if not IS_STRING_NULL(sConvLabel)
				if are_strings_equal(sConvLabel,convLabel)
					return PRN_FINISHED
				ENDIF
			ENDIF
		ENDIF
		

	ENDIF


	return PRN_NEVER_DISPLAYED
ENDFUNC

*/
//need to reset sPrintMessage if mission stage skip is used.

func PRINT_STATUS TRACK_PRINT_STATUS(string sPrintLabel, int &trackerInt)
	switch trackerInt
		case 0
			if IS_THIS_PRINT_BEING_DISPLAYED(sPrintLabel) or IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPrintLabel)
				trackerInt = 1
				return PRN_DISPLAYING
			ENDIF
			
		break
		case 1
			if not IS_THIS_PRINT_BEING_DISPLAYED(sPrintLabel) and not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPrintLabel)
				trackerInt = 2
				return PRN_FINISHED
			ENDIF
		BREAK
		case 2
			return PRN_FINISHED
		BREAK
	ENDSWITCH
	return PRN_NEVER_DISPLAYED
ENDFUNC

func PRINT_STATUS get_print_status(string sPrintLabel)
	if IS_THIS_PRINT_BEING_DISPLAYED(sPrintLabel) or IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sPrintLabel)
		sPrintMessage = sPrintLabel
		//outputDebugData("Print: PRN_DISPLAYING")
		return PRN_DISPLAYING
	ELSE
		if not IS_STRING_NULL(sPrintMessage)
			if ARE_STRINGS_EQUAL(sPrintMessage,sPrintLabel)			
				//outputDebugData("Print: PRN_FINISHED")
				return PRN_FINISHED
			ENDIF
		ENDIF
	ENDIF
	//outputDebugData("Print: PRN_NEVER_DISPLAYED")
	return PRN_NEVER_DISPLAYED
ENDFUNC


PROC SET_DIALOGUE_PEDS(int pedA,int dialogueIDA,int pedB=-1,int dialogueIDB=0,int pedC=-1,int dialogueIDC=0,int pedD=-1,int dialogueIDD=0)
	int convPed[4]
	int convID[4]
	int i
	TEXT_LABEL voiceID = ""
	convPed[0] = pedA  
	convPed[1] = pedB  
	convPed[2] = pedC  
	convPed[3] = pedD
	convID[0] = dialogueIDA  
	convID[1] = dialogueIDB  
	convID[2] = dialogueIDC  
	convID[3] = dialogueIDD
	
	MODEL_NAMES model[3]
	model[0] = GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
	model[1] = GET_PLAYER_PED_MODEL(CHAR_FRANKLIN)
	model[2] = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
	
	MODEL_NAMES peDModel
	ped_index entID
	
	for i = 0 to 3
		if convPed[i] != -1
			if convPed[i] = 0
				entID = PLAYER_PED_ID()
			ELSE
				entID = ped[convPed[i]].id
			ENDIF
			
			
					
			if not IS_PED_INJURED(entID)
				pedModel = GET_ENTITY_MODEL(entID)
				if peDModel = model[0] or convID[i]=0
					voiceID = "MICHAEL"
				ELIF peDModel = model[1] or convID[i]=1
					voiceID = "FRANKLIN"
				ELIF peDModel = model[2] or convID[i]=2
					voiceID = "TREVOR"
				ELSE
					voiceID = "PED"
					voiceID += convID[i] - 3
				ENDIF
					
				//debugMessage(DEBUG_CONVERSATION,"convID[i] = ","",convID[i])
				//debugMessage(DEBUG_CONVERSATION,"voiceID = ",voiceID)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,convID[i],entID,voiceID)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC





//*************************************************** Help Text ********************************************************


//INT iHelpDuration
//STRING sHelpText
//VECTOR vHelpText
//ENTITY_INDEX entHelpText

/*
PROC PRINT_HELP_ADV_POSB(STRING sPrint, VECTOR vWorldPos, ENTITY_INDEX entWorldPos = NULL)
      IF entWorldPos = NULL
            vHelpText = vWorldPos
            entHelpText = NULL
      ELSE
            entHelpText = entWorldPos
      ENDIF
      
      PRINT_HELP_FOREVER(sPrint)
      sHelpText = sPrint
      iHelpDuration = GET_GAME_TIMER() + 7500
      HELP_TEXT_UPDATES()
ENDPROC
*/




CONST_INT MAX_HELP_TEXT_ENTRIES 5
struct structHelpText
	string message
	int iTime
	int iDelayTime
	bool bDelayPreped
	bool bForever
	int iSpecialClearTextCase
	VECTOR vHelpLoc
	ENTITY_INDEX vHelpEnt
	bool printing
	eARROW_DIRECTION dir
ENDSTRUCT


structHelpText helpTextQueue[MAX_HELP_TEXT_ENTRIES]
int helpTextPrintTime


proc clear_help_text()
	int i
	//debugMessage(DEBUG_HELP,"clear_help_text : CLEARING")
	for i = 0 to COUNT_OF(helpTextQueue)-1
		helpTextQueue[i].message = null
		helpTextQueue[i].iTime = -1
		helpTextQueue[i].iDelayTime = -1
		helpTextQueue[i].bDelayPreped = FALSE
		helpTextQueue[i].iSpecialClearTextCase = -1		
		helpTextQueue[i].printing = FALSE	
		//helpTextQueue[i].fID = null
	ENDFOR
	CLEAR_HELP()
	CLEAR_ALL_FLOATING_HELP()
	//sHelpText = NULL
ENDPROC

func int add_help_text(string textLabel, int time=DEFAULT_GOD_TEXT_TIME, int delayTime=-1, bool bForever=FALSE, bool clearExistingHelpMessages=FALSE, int specialCase = -1)
	int i
	//debugMessage(DEBUG_HELP,"add_help_text : ",textLabel)
	IF clearExistingHelpMessages = TRUE		
		CLEAR_HELP()
		//sHelpText = NULL
	ENDIF
	
	for i = 0 to COUNT_OF(helpTextQueue)-1
		if IS_STRING_NULL(helpTextQueue[i].message)
			helpTextQueue[i].message = textLabel
			helpTextQueue[i].iTime = time
			helpTextQueue[i].iDelayTime = delayTime
			helpTextQueue[i].bDelayPreped = FALSE
			helpTextQueue[i].bForever = bForever
			helpTextQueue[i].iSpecialClearTextCase = specialCase
			helpTextQueue[i].printing = FALSE
			return i
		ENDIF
	ENDFOR
	return -1
endfunc

proc set_help_text(string textLabel, int time=DEFAULT_GOD_TEXT_TIME, int delayTime=-1, bool bForever=FALSE, bool clearExistingHelpMessages=FALSE, int specialCase = -1)
	clear_help_text()
	CLEAR_HELP()
	add_help_text(textLabel, time, delayTime, bForever, clearExistingHelpMessages, specialCase)
ENDPROC

PROC add_help_text_3D(STRING sPrint, VECTOR vWorldPos, eArrow_Direction dir=0, ENTITY_INDEX entWorldPos = NULL, int iSpecialCase=-1, int duration=DEFAULT_GOD_TEXT_TIME)	
	//debugMessage(DEBUG_HELP,"new 3d help message. Label = ",sPrint)
	int iEntry = add_help_text(sPrint)
	if iEntry =-1 or iEntry  >= MAX_HELP_TEXT_ENTRIES
		TEXT_LABEL_63 txt=""
		txt+= "ASSERT:See Kev"
		txt+= helpTextQueue[0].message 		
		txt+= helpTextQueue[1].message
		txt+= helpTextQueue[2].message
		txt+= helpTextQueue[3].message
		txt+= helpTextQueue[4].message
		 
		SCRIPT_ASSERT(txt)
	ENDIF
	
	//debugMessage(debug_array,"aa15 helpTextQueue[iEntry] iEntry= ","",iEntry)
	helpTextQueue[iEntry].vHelpLoc = vWorldPos
	helpTextQueue[iEntry].vHelpEnt = entWorldPos
	helpTextQueue[iEntry].iSpecialClearTextCase = iSpecialCase
	helpTextQueue[iEntry].iTime = duration
	helpTextQueue[iEntry].dir = dir
	if duration <= 0 or iSpecialCase != -1
		helpTextQueue[iEntry].bForever = true
	ENDIF
	
	if iSpecialCase != -1
		helpTextQueue[iEntry].iSpecialClearTextCase = iSpecialCase
		//debugMessage(DEBUG_HELP,"add_help_text_3D: if iSpecialCase != -1 TRUE")
		//debugMessage(DEBUG_HELP,"add_help_text_3D: special case is ","",iSpecialCase)
	ENDIF
ENDPROC



func bool is_help_queue_empty()
	if IS_STRING_NULL(helpTextQueue[0].message)
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

//int helpcol


proc displayHelpText()
	int i
	
	if not IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF helpTextQueue[0].printing = TRUE
			//debugMessage(DEBUG_HELP,"displayHelpText: cleanup old help")
			for i = 0 to COUNT_OF(helpTextQueue)-2
				helpTextQueue[i] = helpTextQueue[i+1]
			ENDFOR
			helpTextQueue[COUNT_OF(helpTextQueue)-1].message = null
			helpTextQueue[COUNT_OF(helpTextQueue)-1].iTime = -1
			helpTextQueue[COUNT_OF(helpTextQueue)-1].iDelayTime = -1
			helpTextQueue[COUNT_OF(helpTextQueue)-1].bDelayPreped = FALSE
			helpTextQueue[COUNT_OF(helpTextQueue)-1].iSpecialClearTextCase = -1
			helpTextQueue[COUNT_OF(helpTextQueue)-1].printing = FALSE
		ENDIF
		
		if not IS_STRING_NULL(helpTextQueue[0].message)	
			//debugMessage(DEBUG_HELP,"displayHelpText: if not IS_STRING_NULL(helpTextQueue[0].message) TRUE")
			if helpTextQueue[0].iDelayTime != -1
				if helpTextQueue[0].bDelayPreped = FALSE
					helpTextQueue[0].iDelayTime += GET_GAME_TIMER()
					helpTextQueue[0].bDelayPreped = true
				ENDIF
			ENDIF
			
			if GET_GAME_TIMER() > helpTextQueue[0].iDelayTime

				PRINT_HELP_FOREVER(helpTextQueue[0].message)
		
				if helpTextQueue[0].bForever != TRUE				
					if helpTextQueue[0].iTime = 0
						helpTextQueue[0].iTime = DEFAULT_GOD_TEXT_TIME
					ENDIF		
				ENDIF
				helpTextQueue[0].printing = true
				
				if helpTextQueue[0].iTime > 0
					helpTextPrintTime = GET_GAME_TIMER() + helpTextQueue[0].iTime
					//iHelpDuration = GET_GAME_TIMER() + helpTextQueue[0].iTime
					//debugMessage(DEBUG_HELP,"displayHelpText: iHelpDuration = ","",iHelpDuration)
				ELSE
					helpTextPrintTime =-1
					//debugMessage(DEBUG_HELP,"displayHelpText: helpTextPrintTime =-1")
				ENDIF
			
				
			ENDIF
		ELSE
			
			helpTextPrintTime = -1
		ENDIF
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED() 
		//SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_NET_PLAYER15)
		////debugMessage(DEBUG_HELP,"displayHelpText: IF IS_HELP_MESSAGE_BEING_DISPLAYED() TRUE")
		
		if not is_string_null(helpTextQueue[0].message)
			

			if helpTextPrintTime > 0
				//debugMessage(DEBUG_HELP,"displayHelpText(): GET_GAME_TIMER() = ","",GET_GAME_TIMER())
				//debugMessage(DEBUG_HELP,"displayHelpText(): helpTextPrintTime = ","",helpTextPrintTime)
				if GET_GAME_TIMER() > helpTextPrintTime
					//debugMessage(DEBUG_HELP,"displayHelpText: if GET_GAME_TIMER() > helpTextPrintTime")
					PRINTLN("CLEAR b: ",GET_GAME_TIMER()," help = ",helpTextQueue[0].message)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(helpTextQueue[0].message)
						CLEAR_HELP()				
					ENDIF
				ENDIF
			ENDIF	
			
			
		//	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(helpTextQueue[0].message)
		//		PRINTLN("CLEAR a: ",GET_GAME_TIMER()," help = ",helpTextQueue[0].message)
				
			//	CLEAR_HELP()
		//	ENDIF
		ENDIF	
	ENDIF
ENDPROC

//if the script has set up a special case for the current displayed help text
func bool HAS_HELP_TEXT_SPECIAL_CASE(int &specialCase)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		////debugMessage(DEBUG_HELP,"HAS_HELP_TEXT_SPECIAL_CASE: IS_HELP_MESSAGE_BEING_DISPLAYED() TRUE")
		if helpTextQueue[0].iSpecialClearTextCase != -1
			specialCase = helpTextQueue[0].iSpecialClearTextCase
			return true
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

//****************************************************************************************************************************
//							Activities
//****************************************************************************************************************************

//bool bPreventChangeInBlockingstate=FALSE


enum animBlend
	blendSlow = 0,
	blendNormal,
	blendFast,
	blendInstant
ENDENUM
	




/*
#IF IS_DEBUG_BUILD

bool bPrintActivityCompletionReason
#ENDIF
*/


#IF IS_DEBUG_BUILD
bool bShowEvents
#ENDIF

proc REMOVE_PED_BLIPS(int pedA, int pedB=-1, int pedC=-1, int pedD=-1)
	if DOES_BLIP_EXIST(ped[pedA].blip)
		REMOVE_BLIP(ped[pedA].blip)
	ENDIF
	IF pedB != -1
		if DOES_BLIP_EXIST(ped[pedB].blip)
			REMOVE_BLIP(ped[pedB].blip)
		ENDIF
	ENDIF
	IF pedC != -1
		if DOES_BLIP_EXIST(ped[pedC].blip)
			REMOVE_BLIP(ped[pedC].blip)
		ENDIF
	ENDIF
	IF pedD != -1
		if DOES_BLIP_EXIST(ped[pedD].blip)
			REMOVE_BLIP(ped[pedD].blip)
		ENDIF
	ENDIF
ENDPROC

proc REMOVE_VEH_BLIPS(int vehA, int vehB=-1, int vehC=-1, int vehD=-1)
	if DOES_BLIP_EXIST(vehicle[vehA].blip)
		REMOVE_BLIP(vehicle[vehA].blip)
	ENDIF
	IF vehB != -1
		if DOES_BLIP_EXIST(vehicle[vehB].blip)
			REMOVE_BLIP(vehicle[vehB].blip)
		ENDIF
	ENDIF
	IF vehC != -1
		if DOES_BLIP_EXIST(vehicle[vehC].blip)
			REMOVE_BLIP(vehicle[vehC].blip)
		ENDIF
	ENDIF
	IF vehD != -1
		if DOES_BLIP_EXIST(vehicle[vehD].blip)
			REMOVE_BLIP(vehicle[vehD].blip)
		ENDIF
	ENDIF
ENDPROC




/*
proc define_and_play_anim(int i)
	bool looped=FALSE
	bool bUpperBody=FALSE
	bool useSync=FALSE
	float blendIN=16.0
	float blendOUT=16.0
	INT sceneId
	int thisPed = activityList[i].userPed
	
	int AFint=0
	ANIMATION_FLAGS AF
	IF bUpperBody=true AFint += 16 ENDIF
	

	IF NOT IS_PED_INJURED(ped[thisped].id)
		IF IS_BIT_SET(activityList[i].linkedPed,0) blendIN = 0.5 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,1) blendIN = 2.0 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,2) blendIN =16.0 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,3) blendIN = 1000.0 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,4) blendOUT = 0.5 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,5) blendOUT = 2.0 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,6) blendOUT =16.0 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,7) blendOUT = 1000.0 ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,8) looped=true ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,9) bUpperBody=true ENDIF
		IF IS_BIT_SET(activityList[i].linkedPed,10) useSync=true ENDIF
		
		IF looped=true 
			AFint += 1 			
		ENDIF
		AF = INT_TO_ENUM(ANIMATION_FLAGS,AFint)
		
		IF useSync = true
			IF not ARE_VECTORS_EQUAL(activityList[i].vCoord,vNull)
				sceneId = CREATE_SYNCHRONIZED_SCENE(activityList[i].vCoord, << -0.000, 0.000, activityList[i].fHeading >>)
			ELSE
				sceneId = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(ped[thisped].id), << -0.000, 0.000, GET_ENTITY_HEADING(ped[thisped].id) >>)
			ENDIF
			
			if looped = true
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneId,true)
			ELSE
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneId,FALSE)
			ENDIF
					
			TASK_SYNCHRONIZED_SCENE (ped[thisped].id, sceneId, activityList[i].sString, activityList[i].sStringB, blendIN, blendOUT)
			activityList[i].iDataB = sceneId
		ELSE
			if activityList[i].fHeading != 0.0
				SET_ENTITY_HEADING(ped[thisped].id,activityList[i].fHeading)
			ENDIF
			IF looped = true				
				TASK_PLAY_ANIM(ped[thisped].id,activityList[i].sString,activityList[i].sStringB,blendIN,blendOUT,activityList[i].iDataA,AF)
			ELSE
				TASK_PLAY_ANIM(ped[thisped].id,activityList[i].sString,activityList[i].sStringB,blendIN,blendOUT,activityList[i].iDataA,AF)
			ENDIF
			
			
		
		
			//set blend speed
			/*
			if activityList[i].fHeading > 0
				fTemp = activityList[i].fHeading
			ELSE
				fTemp = 0.5
			ENDIF
			
			if activityList[i].iDataA > 0
				TASK_PLAY_ANIM(ped[thisped].id,activityList[i].sString,activityList[i].sStringB,fTemp,fTemp,activityList[i].iDataA,AF_LOOPING | AF_NOT_INTERRUPTABLE)
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("play anim")
				//TASK_PLAY_ANIM(ped[thisped].id,activityList[i].sString,activityList[i].sStringB,fTemp,fTemp,AF_NOT_INTERRUPTABLE) // | AF_OVERRIDE_PHYSICS)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC
*/


//********************************************* EVENTS *************************************************************************

struct structEvents
	enumEvents thisEvent
	bool active
	int flag
	int timer
	bool persists
	enumMissionStage ForceCleanupStage
ENDSTRUCT



structEvents events[MAX_EVENTS]
CONST_INT CLEANUP 99

func bool add_event(enumEvents eventToAdd, enumMissionStage stageToCleanup=STAGE_GAME_OVER, bool bPersists=FALSE)
	int i
	//cprintln(debug_trevor3,"add event ",eventToAdd)
	//check if this event is already in use and if so, don't add it.
	for i = 0 to MAX_EVENTS-1
		if events[i].active = TRUE
			if events[i].thisEvent = eventToAdd			
				//cprintln(debug_trevor3,"event exits and is active. Flag: ",events[i].flag)
				return FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	for i = 0 to MAX_EVENTS-1
		if events[i].active = FALSE
			events[i].active = TRUE
			events[i].thisEvent = eventToAdd
			events[i].flag = 0
			events[i].persists = bPersists
			events[i].ForceCleanupStage = stageToCleanup
			//cprintln(debug_trevor3,"event added: ",eventToAdd," at ",i)
			return true
		ENDIF
	ENDFOR
	SCRIPT_ASSERT("No room in events array to add new event")
	return FALSE
ENDFUNC

FUNC bool set_event_flag(enumEvents event, int iFlagValue)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].thisEvent = event
			if events[i].active = true
				events[i].flag = iFlagValue
				RETURN TRUE
			ENDIF						
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

func int get_event_flag(enumEvents event)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].thisEvent = event
			if events[i].active = true
				RETURN events[i].flag
			ENDIF						
		ENDIF
	ENDFOR
	return -1
ENDFUNC

func bool is_event_complete(enumEvents eventToCheck)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].thisEvent = eventToCheck
			if events[i].active = true
				return FALSE
			ENDIF						
		ENDIF
	ENDFOR
	return true
ENDFUNC

func bool does_event_exist(enumEvents eventToCheck)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].thisEvent = eventToCheck
			if events[i].active = true
				return true
			ELSE
				return FALSE
			ENDIF
		ENDIF
	ENDFOR
	return FALSE
ENDFUNC

proc kill_event(enumEvents thisEvent)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].thisEvent = thisEvent
			events[i].flag = CLEANUP
			//control_events(thisEvent)
		ENDIF
	ENDFOR	
ENDPROC

proc kill_all_events(bool keepEventsThatMustPersist = FALSE)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].active = TRUE
			if events[i].persists = FALSE or (events[i].persists =true and keepEventsThatMustPersist = FALSE)
				events[i].flag = CLEANUP
				events[i].timer = 0
			ENDIF
		ENDIF
	ENDFOR	
ENDPROC

bool bMakeMissionFail
string sFailMessage
proc FailMission(string sFailReason)	
	bMakeMissionFail = TRUE
	bMakeMissionFail = bMakeMissionFail
	sFailMessage = sFailMessage
	sFailMessage = sFailReason	
	
ENDPROC

proc instant_add_or_reset_event(enumEvents thisEvent, enumMissionStage stageToCleanup=STAGE_GAME_OVER, bool bPersists=FALSE)
	int i
	bool bFound
	for i = 0 to MAX_EVENTS-1
		if events[i].thisEvent = thisEvent		
			events[i].active = TRUE
			events[i].flag = 0
			events[i].timer = 0
			events[i].persists = bPersists
			events[i].ForceCleanupStage	= stageToCleanup		
			bFound = TRUE
		ENDIF
	ENDFOR
	IF NOT bFound
		add_Event(thisEvent,stageToCleanup,bPersists)
	ENDIF
endproc

/*
func bool has_this_print_been_displayed(string sPrintLabel)
	if IS_STRING_NULL(sPrintMessage) or not ARE_STRINGS_EQUAL(sPrintMessage,sPrintLabel)
		if IS_THIS_PRINT_BEING_DISPLAYED(sPrintLabel)
			sPrintMessage = sPrintLabel
		ENDIF
	ELSE
		if ARE_STRINGS_EQUAL(sPrintMessage,sPrintLabel)
			if not IS_THIS_PRINT_BEING_DISPLAYED(sPrintLabel)
				sPrintMessage = NULL
				return true
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC
*/
//blips
proc controlBlips()
	int i
	repeat COUNT_OF(ped) i
		if DOES_BLIP_EXIST(ped[i].blip)
			if DOES_ENTITY_EXIST(ped[i].id)
				if IS_ENTITY_DEAD(ped[i].id)
					REMOVE_BLIP(ped[i].blip)
				ENDIF
			ELSE
				REMOVE_BLIP(ped[i].blip)
			ENDIF
		ENDIF		 
	ENDREPEAT
ENDPROC



#IF IS_DEBUG_BUILD
proc reset_all_peds()
	int i
	for i = 1 to MAX_PED_COUNT-1
		if DOES_entity_EXIST(ped[i].id)
			DELETE_PED(ped[i].id)
		ENDIF
	ENDFOR
	
	if not IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),GET_entity_COORDS(PLAYER_PED_ID())+<<0,0,2>>)	
	ENDIF	
ENDPROC
					
proc reset_all_vehicles()
	int i
	for i = 0 to MAX_VEH_COUNT-1
		if DOES_ENTITY_EXIST(vehicle[i].id)
			DELETE_VEHICLE(vehicle[i].id)
		ENDIF
	ENDFOR	
ENDPROC	
#ENDIF





#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID Boltwidget
//bool bVehWidgetActive
bool bShowHelp
bool bshowConversations


//structVehWidget vehicleWidget[MAX_VEH_COUNT]

PROC BOLT_WIDGET_CLEANUP()

	IF DOES_WIDGET_GROUP_EXIST(Boltwidget)
		DELETE_WIDGET_GROUP(Boltwidget)
	ENDIF
	
ENDPROC




PROC boltDebug(WIDGET_GROUP_ID parent=NULL)

	int r,i
	float fYGap = 0.02
	TEXT_LABEL_63 txA
	IF DOES_WIDGET_GROUP_EXIST(parent)
		IF NOT DOES_WIDGET_GROUP_EXIST(Boltwidget)
			SET_CURRENT_WIDGET_GROUP(parent)
				Boltwidget = START_WIDGET_GROUP("BOLT WIDGET")
				ADD_WIDGET_BOOL("Save script debug data to RAG",bOutputScriptDebug)
				ADD_WIDGET_BOOL("Show scipt events",bShowEvents)
				ADD_WIDGET_BOOL("Show help list",bShowHelp)
				ADD_WIDGET_BOOL("Show conversations List",bshowConversations)
				TEXT_LABEL_23 txt
				START_WIDGET_GROUP("edit Events")
					for r = 0 to MAX_EVENTS-1
						txt="Event "
						txt+=r
						txt+=" flag"
						ADD_WIDGET_INT_SLIDER(txt,events[r].flag,0,9999,1)
					ENDFOR
				STOP_WIDGET_GROUP()
		
				STOP_WIDGET_GROUP()												
			CLEAR_CURRENT_WIDGET_GROUP(parent)
			
		
			
		ELSE
			
			
		
			IF bShowEvents = TRUE
				
				SET_TEXT_SCALE(0.35,0.35)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.13,"string","Event    Active     Flag     timer    persists")
				
				for i = 0 to MAX_EVENTS-1										
					IF events[i].active
						txA = ""
						txA += enum_to_int(events[i].thisEvent)
						SET_TEXT_SCALE(0.35,0.35)
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.11,0.2+(i*fYGap),"string",txA)
						
						SET_TEXT_SCALE(0.35,0.35)
						If events[i].active = TRUE						
							DISPLAY_TEXT_WITH_LITERAL_STRING(0.14,0.2+(i*fYGap),"string","TRUE")
						ELSE
							DISPLAY_TEXT_WITH_LITERAL_STRING(0.14,0.2+(i*fYGap),"string","FALSE")
						ENDIF
						
						txA = ""
						txA += events[i].flag
						SET_TEXT_SCALE(0.35,0.35)
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.193,0.2+(i*fYGap),"string",txA)
						
						txA = ""
						txA += events[i].timer
						SET_TEXT_SCALE(0.35,0.35)
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.228,0.2+(i*fYGap),"string",txA)
						
						SET_TEXT_SCALE(0.35,0.35)
						If events[i].persists = TRUE						
							DISPLAY_TEXT_WITH_LITERAL_STRING(0.26,0.2+(i*fYGap),"string","TRUE")
						ELSE
							DISPLAY_TEXT_WITH_LITERAL_STRING(0.26,0.2+(i*fYGap),"string","FALSE")
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			If bShowHelp = TRUE
				for i = 0 to MAX_HELP_TEXT_ENTRIES-1
					SET_TEXT_SCALE(0.35,0.35)
					txA = ""
					txA += i
					txA += " "
					txA += helpTextQueue[i].message
					IF helpTextQueue[i].printing = TRUE
						txA = " Printing"
					ELSE
						txA += " not printing"
					ENDIF
										
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.11,0.2+(i*fYGap),"string",txA)
				ENDFOR
			endif
			
		
		ENDIF
	ENDIF
	//int i
	/*
	IF NOT DOES_WIDGET_GROUP_EXIST(Boltwidget)
		Boltwidget = START_WIDGET_GROUP("BOLT WIDGET")
		ADD_WIDGET_BOOL("Activate",bVehWidgetActive)
		STOP_WIDGET_GROUP()
	ELSE
		IF bVehWidgetActive = true
		/*
			REPEAT COUNT_OF(vehicle) i
				IF DOES_ENTITY_EXIST(vehicle[i].id)
					if not IS_ENTITY_DEAD(vehicle[i].id)
						if not DOES_WIDGET_EXIST(vehicleWidget[i])
							SET_CURRENT_WIDGET_GROUP(Boltwidget)
							vehicleWidget[i].widgetID = START_WIDGET_GROUP("Vehicle")
								ADD_WIDGET_VECTOR_SLIDER("Veh Pos",vehicleWidget[i].vehPos,-10000,10000,0.1)
								ADD_WIDGET_FLOAT_SLIDER("Recording position",vehicleWidget[i].rectime,0,100000,0.1)
								ADD_WIDGET_BOOL("Edit vehicle recording position",vehicleWidget[i].editRecordingPos)
							STOP_WIDGET_GROUP()
						ELSE
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[i].id)
								IF vehicleWidget[i].editRecordingPos = FALSE
									vehicleWidget[i].rectime = GET_TIME_POSITION_IN_RECORDING(vehicle[i].id)
								ELSE
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDIF
	*/
ENDPROC

#ENDIF

FUNC BOOL ARE_TASKS_DONE(int pedID)
	IF NOT IS_PED_INJURED(ped[pedID].id)
		IF GET_SCRIPT_TASK_STATUS(ped[pedID].id,SCRIPT_TASK_ANY) != PERFORMING_TASK
			return TRUE
		ENDIF	
	ENDIF
	return FALSE	
ENDFUNC

//*********************BRAIN TRACKERS*********************
//effectively mini scripts that can run for a specific ped.
/*
enum enumBrainTrackers
	TRACKER_NONE,
	TRACK_WAYPOINT_SPEED
ENDENUM
*/

ENUM defArea
	defAreaNone,
	defAreaNoneAndClear,
	defAreaMedium,
	DefAreaSmall
ENDENUM

ENUM MOVESTYLE
	MV_NAVMESH,
	MV_STRLINE
ENDENUM



//func SEQUENCE_INDEX closeAndStoreBrainSequence()
	
//ENDFUNC

// ========================= Hotwap =================================
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails

enum enumSwitchState
	SWITCH_SETUP,
	SWITCH_SELECTING,
	SWITCH_SWAP_PLAYER_PEDS,
	SWITCH_COMPLETE,
	SWITCH_NULL
endenum

enumSwitchState switchState 

proc prep_selector_ped(selector_slots_enum slot)
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[slot])
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,slot,false)

	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,slot,true)
	
	ENDIF
endproc

FUNC BOOL do_switch(ped_index pedFranklin, ped_index pedTrevor, ped_index pedMichael, SELECTOR_SLOTS_ENUM setHint = SELECTOR_PED_MULTIPLAYER, SWITCH_TYPE forceSwitchType = SWITCH_TYPE_AUTO, bool bCS2ForceSwitch=FALSE)  //SELECTOR_PED_MULTIPLAYER = none
	setHint=setHint
	SWITCH switchState
		CASE SWITCH_SETUP
			sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = pedFranklin
			sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = pedTrevor
			sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = pedMichael
	
			
			prep_selector_ped(SELECTOR_PED_FRANKLIN)
			prep_selector_ped(SELECTOR_PED_TREVOR)
			prep_selector_ped(SELECTOR_PED_MICHAEL)	
			
			IF setHint != SELECTOR_PED_MULTIPLAYER
				SET_SELECTOR_PED_HINT(sSelectorPeds,setHint,TRUE)
			ENDIF
			
			PRINTSTRING("switchState=SWITCH_SELECTING")PRINTNL()
			switchState=SWITCH_SELECTING
		BREAK
		CASE SWITCH_SELECTING
			//cprintln(debug_trevor3,"make switch choice")
			IF IS_PLAYER_PLAYING(player_id())
				IF bCS2ForceSwitch
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds,SELECTOR_PED_TREVOR)
					cprintln(debug_Trevor3,"FORCE TREVOR")
				ELSE 
					UPDATE_SELECTOR_HUD(sSelectorPeds,IS_PLAYER_CONTROL_ON(PLAYER_ID()))     // Returns TRUE when the player has made a selection
				ENDIF
				cprintln(debug_Trevor3,"SWITCH TREVOR")
				IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_FRANKLIN)
				OR HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MICHAEL)
				OR HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_TREVOR)
			
					
					sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
					
					sCamDetails.bRun  = TRUE
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,TRUE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)
					PRINTSTRING("switchState = SWITCH_SWAP_PLAYER_PEDS")PRINTNL()
					switchState = SWITCH_SWAP_PLAYER_PEDS
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SWITCH_SWAP_PLAYER_PEDS
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails,forceSwitchType) 		
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds,true,true)
							 
							switchState = SWITCH_COMPLETE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SWITCH_COMPLETE
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails,forceSwitchType) 		
				//switch still in progress
			ELSE
				pedFranklin = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
				pedTrevor = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
				pedMichael = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
				
				PRINTSTRING("switchState = SWITCH_SETUP")PRINTNL()
				switchState = SWITCH_SETUP
				SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,FALSE)
				SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
				SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,FALSE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC ScriptManagement(#IF IS_DEBUG_BUILD WIDGET_GROUP_ID parent #endif)
	#IF IS_DEBUG_BUILD
		boltDebug(parent)
	#ENDIF
ENDPROC
