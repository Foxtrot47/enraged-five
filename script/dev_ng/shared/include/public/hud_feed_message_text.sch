USING "globals.sch"
USING "rage_builtins.sch"

// Header support for HUD_FEED_MESSAGE_TEXT.gfx

/// PURPOSE: Displays the text message contact image and localised text message string in the top left of the HUD
/// Make sure you have loaded the movie with REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE, 
/// and checked it has loaded with HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED before using this.
/// PARAMS:
///    sHUDFeedTextMessage - Scaleform movie ID for the text message UI
///    sMessageText - body string of the Text Message
///    sTXD - The Texture Dictionary for the contact image used in the Text Message
///    sImageName - The Image name string for the contact image used in the Text Message
PROC Set_Feed_Text_Message(eSCRIPT_HUD_COMPONENT sHUDFeedTextMessage, STRING sMessageText, STRING sTXD, STRING sImageName)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedTextMessage, "SET_FEED_MESSAGE_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMessageText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sImageName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Removes text message component from screen. Be sure to remove the scaleform movie when it is no longer required
///    
/// PARAMS:
///    sHUDFeedTextMessage - Scaleform movie ID
PROC Remove_Feed_Text_Message(eSCRIPT_HUD_COMPONENT sHUDFeedTextMessage)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedTextMessage, "REMOVE_FEED_MESSAGE_TEXT")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC
