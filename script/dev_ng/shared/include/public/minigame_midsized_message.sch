USING "globals.sch"
USING "commands_camera.sch"
USING "minigames_helpers.sch"


STRUCT SCRIPT_SCALEFORM_MIDSIZED_MESSAGE
	SCALEFORM_INDEX siMovie
	INT 			iDuration
	structTimer		movieTimer
ENDSTRUCT

FUNC SCALEFORM_INDEX REQUEST_MG_MIDSIZED_MESSAGE()
	RETURN REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
ENDFUNC

FUNC SCALEFORM_INDEX REQUEST_BIG_MESSAGE()
	RETURN REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
ENDFUNC

PROC CLEANUP_MG_MIDSIZED_MESSAGE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct)
	IF HAS_SCALEFORM_MOVIE_LOADED(scaleformStruct.siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformStruct.siMovie)
	ENDIF
ENDPROC

/// PURPOSE:
///    Set big message for a given duration
/// PARAMS:
///    scaleformStruct - 
///    labeToDisp - big message text label
///    strapLine - byline for the big message
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_MIDSIZED_MESSAGE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_MIDSIZED_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set midsized message with a number token
///    UPDATE: this has been changed to support hud coloring.  details in the params.
/// PARAMS:
///    scaleformStruct - local big message scaleform
///    labeToDisp - main splash text string - this needs to be in this format: ~a~ ~1~
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_NUMBER(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_MIDSIZED_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("GEN_BIGM_NUM")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message for a given duration
/// PARAMS:
///    scaleformIndex - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_STRING_IN_STRAPLINE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine, STRING subString, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_MIDSIZED_MESSAGE")
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(subString)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set big message with a number token
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_NUMBER_IN_STRAPLINE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, STRING strapLine = NULL, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_MIDSIZED_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC


/// PURPOSE:
///    Set big message for a given duration using the player's username/gamertag
///    Used mainly in MP
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine, STRING sPlayerName, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_YELLOW)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHOW_MIDSIZED_MESSAGE")
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Renders a scaleform splash movie.
/// RETURNS:
///    FALSE when the movie is done drawing and you should move on.
FUNC BOOL UPDATE_SCALEFORM_MIDSIZED_MESSAGE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct, BOOL bAllowSkip = FALSE)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	ENDIF
	
	// Hide the reticle.
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	
	// Draw the moobie.
	//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleformStruct.siMovie, 255, 255, 255, 255)
	
	// If we can skip, detect that button press
	IF bAllowSkip
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// When it's no longer visible, return FALSE. -- Changed. Now, when its curation is up, return FALSE
	IF (scaleformStruct.iDuration = -1)
		RETURN TRUE
	ENDIF

	// Is it time to leave?
	IF (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 > TO_FLOAT(scaleformStruct.iDuration))
		CANCEL_TIMER(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if this scaleform splash would be displaying. Mainly for Golf, as golf was using an odd method to check this.
FUNC BOOL WOULD_SCALEFORM_MIDSIZED_MESSAGE_BE_VISIBLE(SCRIPT_SCALEFORM_MIDSIZED_MESSAGE & scaleformStruct)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 <= TO_FLOAT(scaleformStruct.iDuration))
ENDFUNC

STRUCT SCRIPT_SHARD_BIG_MESSAGE
	SCALEFORM_INDEX siMovie
	INT 			iDuration
	structTimer		movieTimer
	structTimer		animOutTimer
	BOOL 			bAnimOutOverride
	HUD_COLOURS		eEndFlash
ENDSTRUCT

ENUM SHARD_BIG_MESSAGE_TYPE
	SHARD_MESSAGE_CENTERED,
	SHARD_MESSAGE_CENTERED_TOP,
	SHARD_MESSAGE_WASTED,
	SHARD_MESSAGE_RANKUP,
	SHARD_MESSAGE_CREW_RANKUP,
	SHARD_MESSAGE_MIDSIZED
ENDENUM

FUNC STRING GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(SHARD_BIG_MESSAGE_TYPE eMessageType)
	SWITCH eMessageType
		CASE SHARD_MESSAGE_CENTERED
			RETURN "SHOW_SHARD_CENTERED_MP_MESSAGE"		
		CASE SHARD_MESSAGE_CENTERED_TOP
			RETURN "SHOW_SHARD_CENTERED_TOP_MP_MESSAGE"
		CASE SHARD_MESSAGE_WASTED
			RETURN "SHOW_SHARD_WASTED_MP_MESSAGE"
		CASE SHARD_MESSAGE_RANKUP
			RETURN "SHOW_SHARD_RANKUP_MP_MESSAGE"
		CASE SHARD_MESSAGE_CREW_RANKUP
			RETURN "SHOW_SHARD_CREW_RANKUP_MP_MESSAGE"
		CASE SHARD_MESSAGE_MIDSIZED
			IF NETWORK_IS_GAME_IN_PROGRESS()
				RETURN "SHOW_COND_SHARD_MESSAGE"
			ENDIF
			RETURN "SHOW_SHARD_MIDSIZED_MESSAGE"
		
		DEFAULT
			PRINTLN("minigame_big_message - GET_STRING_FROM_BIG_MESSAGE_TYPE_ENUM - Could't find a matching string. Returning default")
			RETURN "SHOW_SHARD_CENTERED_MP_MESSAGE"
	ENDSWITCH
ENDFUNC

// new big message shards - will place here for now but may move later
PROC SET_SHARD_BIG_MESSAGE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine = NULL, INT iDuration = 4000, 
							SHARD_BIG_MESSAGE_TYPE eMessageType = SHARD_MESSAGE_CENTERED, HUD_COLOURS eEndFlash = HUD_COLOUR_BLACK)
	
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(eMessageType)
	
	IF eMessageType <> SHARD_MESSAGE_MIDSIZED	// RESET_MOVIE doesn't exist for midsized messages
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//IF NOT IS_STRING_NULL_OR_EMPTY(strapline)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
		//ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	
	scaleformStruct.eEndFlash = eEndFlash
ENDPROC

PROC SET_SHARD_BIG_MESSAGE_WITH_PLAYER_NAME_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine, STRING sPlayerName, INT iDuration = 4000, 
															SHARD_BIG_MESSAGE_TYPE eMessageType = SHARD_MESSAGE_CENTERED, HUD_COLOURS eEndFlash = HUD_COLOUR_BLACK)
	
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(eMessageType)
	
	IF eMessageType <> SHARD_MESSAGE_MIDSIZED	// RESET_MOVIE doesn't exist for midsized messages
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	
	scaleformStruct.eEndFlash = eEndFlash
ENDPROC

/// PURPOSE:
///    Display a shard message with a string in the strapline
/// PARAMS:
///    scaleformStruct - 
///    labeToDisp - bigger header text label
///    strapLine - Substring under the bigger header
///    sString - substring in the strapline
///    iDuration - 
///    eMessageType - 
///    eEndFlash - color of background on animate out
PROC SET_SHARD_BIG_MESSAGE_WITH_STRING_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, STRING strapLine, STRING sString, INT iDuration = 4000, 
															SHARD_BIG_MESSAGE_TYPE eMessageType = SHARD_MESSAGE_CENTERED, HUD_COLOURS eEndFlash = HUD_COLOUR_BLACK)
	
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(eMessageType)
	
	IF eMessageType <> SHARD_MESSAGE_MIDSIZED	// RESET_MOVIE doesn't exist for midsized messages
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapLine)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sString)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	
	scaleformStruct.eEndFlash = eEndFlash
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    scaleformStruct - 
///    labeToDisp - 
///    iNum1 - first number
///    iNum2 - second number
///    strapLine - string with two ~1~ tokens in it
///    iDuration - 
///    eHudColor - 
///    eMessageType - 
PROC SET_SHARD_BIG_MESSAGE_WITH_2_NUMBERS_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum1, INT iNum2, STRING strapLine = NULL, INT iDuration = 4000, SHARD_BIG_MESSAGE_TYPE eMessageType = SHARD_MESSAGE_CENTERED, HUD_COLOURS eEndFlash = HUD_COLOUR_BLACK)
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(eMessageType)
	
	IF eMessageType <> SHARD_MESSAGE_MIDSIZED	// RESET_MOVIE doesn't exist for midsized messages 
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			ADD_TEXT_COMPONENT_INTEGER(iNum1)
			ADD_TEXT_COMPONENT_INTEGER(iNum2)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	
	scaleformStruct.eEndFlash = eEndFlash
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    scaleformStruct - 
///    labeToDisp - 
///    iNum1 - 
///    iNum2 - 
///    strapLine - 
///    iDuration - 
///    eHudColor - 
///    eMessageType - 
PROC SET_SHARD_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iNum, STRING strapLine = NULL, INT iDuration = 4000, SHARD_BIG_MESSAGE_TYPE eMessageType = SHARD_MESSAGE_CENTERED, HUD_COLOURS eEndFlash = HUD_COLOUR_BLACK)
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(eMessageType)
	
	IF eMessageType <> SHARD_MESSAGE_MIDSIZED	// RESET_MOVIE doesn't exist for midsized messages
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "RESET_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	
	scaleformStruct.eEndFlash = eEndFlash
ENDPROC

PROC END_SHARD_BIG_MESSAGE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, FLOAT fAnimTime = 0.33)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SHARD_ANIM_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(scaleformStruct.eEndFlash)) 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimTime)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC STOP_SHARD_BIG_MESSAGE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct)
	scaleformStruct.bAnimOutOverride = TRUE
ENDPROC

PROC RESET_SHARD_BIG_MESSAGE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct)
	scaleformStruct.bAnimOutOverride = FALSE
	CANCEL_TIMER(scaleformStruct.movieTimer)
	CANCEL_TIMER(scaleformStruct.animOutTimer)
ENDPROC

FUNC BOOL UPDATE_SHARD_BIG_MESSAGE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, BOOL bAllowSkip = FALSE)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	ENDIF
	
	// Hide the reticle.
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	
	// Draw the moobie.
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleformStruct.siMovie, 255, 255, 255, 255)
	
	// If we can skip, detect that button press
	IF bAllowSkip
	OR scaleformStruct.bAnimOutOverride
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR scaleformStruct.bAnimOutOverride
			IF NOT IS_TIMER_STARTED(scaleformStruct.animOutTimer)
				RESTART_TIMER_NOW(scaleformStruct.animOutTimer)
				END_SHARD_BIG_MESSAGE(scaleformStruct)
			ENDIF
		ENDIF
		
		IF IS_TIMER_STARTED(scaleformStruct.animOutTimer)
			IF GET_TIMER_IN_SECONDS(scaleformStruct.animOutTimer) > 0.33
				CANCEL_TIMER(scaleformStruct.animOutTimer)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF (scaleformStruct.iDuration = -1)
		RETURN TRUE
	ENDIF

	// Is it time to leave?
	IF (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 > TO_FLOAT(scaleformStruct.iDuration))
		CPRINTLN(DEBUG_MINIGAME, "timer hit end")
		IF NOT IS_TIMER_STARTED(scaleformStruct.animOutTimer)
			CPRINTLN(DEBUG_MINIGAME, "shard started to end")
			RESTART_TIMER_NOW(scaleformStruct.animOutTimer)
			END_SHARD_BIG_MESSAGE(scaleformStruct)
		ELIF GET_TIMER_IN_SECONDS(scaleformStruct.animOutTimer) > 0.33
			CPRINTLN(DEBUG_MINIGAME, "shard animated out")
			CANCEL_TIMER(scaleformStruct.movieTimer)
			//CANCEL_TIMER(scaleformStruct.animOutTimer)
			RETURN FALSE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MINIGAME, "duration: ", TO_FLOAT(scaleformStruct.iDuration))
		CPRINTLN(DEBUG_MINIGAME, "movie timer: ", (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0))
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN ((GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) - 1.0) * 1000.0 <= TO_FLOAT(scaleformStruct.iDuration))
ENDFUNC

PROC SET_SHARD_BIG_MESSAGE_GENERIC(SCALEFORM_INDEX siMovie, STRING labeToDisp, STRING strapLine = NULL, SHARD_BIG_MESSAGE_TYPE eMessageType = SHARD_MESSAGE_CENTERED)
	
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(eMessageType)
	
	IF eMessageType <> SHARD_MESSAGE_MIDSIZED	// RESET_MOVIE doesn't exist for midsized messages
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "RESET_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF NOT IS_STRING_NULL_OR_EMPTY(strapline)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strapline)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC END_SHARD_BIG_MESSAGE_GENERIC(SCALEFORM_INDEX siMovie, HUD_COLOURS eEndFlash = HUD_COLOUR_WHITE, FLOAT fAnimTime = 0.33)
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHARD_ANIM_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eEndFlash)) 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAnimTime)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC UPDATE_SHARD_BIG_MESSAGE_GENERIC(SCALEFORM_INDEX siMovie)
	
	// Hide the reticle.
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	
	// Draw the moobie.
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(siMovie, 255, 255, 255, 255)
	
ENDPROC
