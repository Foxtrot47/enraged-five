//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Adam Westwood & Colin Considine						│
//│				Date: 01/08/10			  										│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Hacking Public								        │
//│																				│
//│			Hacking for both singleplayer and multiplayer            			│
//|			                            										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

/*
One shots:
 HACKING_MOVE_CURSOR   -  Moving cursor around
 HACKING_CLICK  -  Generic clicking on something
 HACKING_CLICK_BAD  -  Mis-click
 HACKING_CLICK_GOOD   -   Find IP / lock password letter
 HACKING_DOWNLOADED  -  Finish downloading the documents
 HACKING_FAILURE  -  Out of time
 HACKING_SUCCESS  -  Succesfully crack password

Loops:
 HACKING_COUNTDOWN_CRACK_PASS   -  Background loop for the password crack section
 HACKING_COUNTDOWN_IP_FIND   -  Background loop for the IP finding section
 HACKING_DOWNLOADING   -   Loop to play while downloading files.
*/

USING "timer_public.sch"

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"
USING "freemode_header.sch"

USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"

USING "LineActivation.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

INT iHackMinutes
INT iHackSeconds
INT iHackMilliseconds
INT i_clock_start

BOOL bStartHackTimer
BOOL bHelpText
BOOL bDoMoreHelpText


FLOAT FPCMouseMoveDebounce = 0.0

CONST_INT MAX_CONNECT_NUMBERS_HC 80
CONST_INT MAX_SCREEN_ELEMENTS 30
CONST_INT MAX_COLUMN_LENGTH_BF 8
CONST_INT MAX_CHARACTERS_FOR_BF 26
CONST_INT MAX_RANDOM_WORDS 15
CONST_INT NUMBER_OF_LINE_ELEMENTS 300
CONST_INT MAX_SOUNDS 5


CONST_INT i_Minute_Val  1 
CONST_INT i_Second_Val  60	
CONST_INT i_MiliSecond_Val  1000
INT sHacking[MAX_SOUNDS]

TWEAK_FLOAT fSensitivity 8.0

FLOAT fPCMouseAverageX = 0.0
FLOAT fPCMouseAverageY = 0.0
FLOAT fPCMouseAverageCount = 0

CAMERA_INDEX cam_temp

SCALEFORM_RETURN_INDEX minigame_return_index

//STRUCT WORLD_HACK_LINE
//	BOOL b_visible
//	BOOL b_diverted
//	INT i_last_node_entered
//	FLOAT x_pos
//	FLOAT y_pos
//ENDSTRUCT


CONST_INT BS_IS_HACKING 0
CONST_INT BS_QUIT_HACKING 1
CONST_INT BS_MY_COMPUTER_OPEN 2
CONST_INT BS_MOBILE_OPEN 3
CONST_INT BS_HACK_CONNECT_OPEN 4
CONST_INT BS_BRUTE_FORCE_OPEN 5
CONST_INT BS_SECURITY_DEF_OPEN 6
CONST_INT BS_GENERATE_IP 7
CONST_INT BS_GENERATE_BRUTE_FORCE_DATA 8
CONST_INT BS_BRUTE_FORCE_GAME_SOLVED 9
CONST_INT BS_DRAW_MOUSE 10
CONST_INT BS_ACCESSED_DENIED_OPEN 11
CONST_INT BS_HACKING_SUCCESSFUL 12
CONST_INT BS_NODE_INCREMENTED_X 13
CONST_INT BS_NODE_DECREMENTED_X 14
CONST_INT BS_NODE_INCREMENTED_Y 15
CONST_INT BS_NODE_DECREMENTED_Y 16
CONST_INT BS_HACK_CONNECT_FAIL_TRIGGERED 17
CONST_INT BS_HACK_CONNECT_GAME_SOLVED 18
CONST_INT BS_HACK_CONNECT_GAME_END 19
CONST_INT BS_FIRST_RUN 20
CONST_INT BS_SHOW_FAIL 21
CONST_INT BS_TXD_HACK_LOADED 22
CONST_INT BS_GOT_COLUMN_WRONG 23
CONST_INT BS_FORCE_START_HACKING 24
CONST_INT BS_MP_HACK_FAIL 25
CONST_INT BS_NEW_HACKING_GAME_SOLVED 26
CONST_INT BS_DOWNLOAD_ICON_DISPLAYED 27
CONST_INT BS_DOWNLOAD_OPEN 28
CONST_INT BS_WALLPAPER_INITIALISED 29
CONST_INT BS_BRUTE_FORCE_FAIL_TRIGGER 30
CONST_INT BS_BRUTE_FORCE_INCREASE_FAIL_TELEMETRY 31


CONST_INT BS2_LAUNCH_MG_IMMEDIATIALY			0
CONST_INT BS2_QUIT_MG_IMMEDIATIALY				1
CONST_INT BS2_MAP_SHOWN							2




// ################################# CIRCUIT HACKING MINIGAME VARIABLES - START ################################# //





CONST_FLOAT DEFAULT_ASPECT_RATIO_MODIFIER		1.778



// CIRCUIT HACKING MINIGAME BIT SET BOOL VALUES
CONST_INT 	BIT_SET_CIRCUIT_MINIGAME_SOLVED				0	// BOOL: Handles completion flag - Initialises as FALSE
CONST_INT 	BIT_SET_CIRCUIT_MOVE_HEAD					1	// BOOL: Starts/stops Head moving - Initialises as FALSE
CONST_INT 	BIT_SET_CIRCUIT_ENTRY_COLLISION				2	// BOOL: Handles collision on start port so player doesn't die when inside rect, activates port collision when the player is outside the rect - Intialises as FALSE
CONST_INT 	BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING	3	// BOOL: Stops player turning up/down when starting level so exits start port properly - Initialises as TRUE (see INIT FUNC)
CONST_INT 	BIT_SET_CIRCUIT_CHANGE_DIRECTION			4	// BOOL: Handles delay on turning so player cant turn into self - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_LEVEL_PASSED				5	// BOOL: Tells us if we have finished the gameplay part of a level - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_LEVEL_FAILED				6	// BOOL: Tells us if we have finished the gameplay part of a level - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_REMOVE_HEAD					7	// BOOL: Tells when to remove the head sprite on level completion - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_KILL_SCREEN					8	// BOOL: Handles if we should do the kill anim on the snake - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED			9	// BOOL: Tells when to draw the system unlocked sprites - Initialses as FALSE
CONST_INT	BIT_SET_CIRCUIT_DRAWN_SYS_UNLOCKED_BKGROUND	10	// BOOL: Tells when the System Unlocked Background has been drawn - Initialses as FALSE
CONST_INT	BIT_SET_CIRCUIT_DRAW_UNLOCKED_LOCK			11	// BOOL: Tells when the closed lock has finished rotating so can draw the open lock - Initialses as FALSE
CONST_INT	BIT_SET_CIRCUIT_START_PHONE_UNLOCK			12	// BOOL: Tells when the player has pressed button to start unlock sequence - Initialses as FALSE
CONST_INT	BIT_SET_CIRCUIT_GAME_RUNNING				13	// BOOL: Tells if game is currently running - Initialses as TRUE (see INIT FUNC)
CONST_INT	BIT_SET_CIRCUIT_PULSE_END_PORT_LIGHTS		14	// BOOL: Tells when to stop flashing start port lights to swap to end port lights - Initialises as FASLE
CONST_INT	BIT_SET_CIRCUIT_PULSE_UP					15	// BOOL: Tells when to swap between the alpha fade in and fade out - Initialises as FASLE
CONST_INT	BIT_SET_CIRCUIT_PLAY_PASS_AUDIO				16	// BOOL: Signifies if the pass audio has been played - Initialises as FASLE
CONST_INT	BIT_SET_CIRCUIT_PLAY_FAIL_AUDIO				17	// BOOL: Signifies if the fail audio has been played - Initialises as FASLE
CONST_INT	BIT_SET_CIRCUIT_WAIT_TIME_PICKED			18	// BOOL: Signifies if we have a wait time selected for the period between debug output spews - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_RECYCLE_DEBUG_OUTPUT		19	// BOOL: Signifies if we have a reached the end of the debug output array, so we can override from the begining - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW		20	// BOOL: Controls if scaleform reset has finished - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_DROP_CORE_LIGHTS			21	// BOOL: Signifies when to drop the core lights - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_AUDIO_ONESHOT				22	// BOOL: Controls if a oneshot audio has been fired - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_SCALEFORM_SET_UP			23	// BOOL: Controls if scaleform setup has finished - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_SCALEFORM_RESET				24	// BOOL: Controls if scaleform reset has finished - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_START_TIMER      			25	// BOOL: Tells if timer has starter for heist telemetry - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_SAFE_TO_USE_PHONE  			26	// BOOL: Tells if launching stage is finished and safe to task ped to use phone - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_ALLOW_DAMAGE	  			27	// BOOL: Allow player damage

#IF IS_DEBUG_BUILD
CONST_INT	BIT_SET_CIRCUIT_DRAW_WALLS					30	// BOOL: DEBUG ONLY - Controls if we should draw the numbered collision rect boxes for the level's walls - Initialises as FALSE
CONST_INT	BIT_SET_CIRCUIT_DRAW_ASPECT_TEST			31	// BOOL: DEBUG ONLY - Controls if we should be drawing with the Widescreen Aspect Ratio test state - Initialises as FALSE
#ENDIF


// Moved data structs to x:\gta5\script\dev_ng\multiplayer\globals\mp_globals_ambience_definitions.sch to prevent stack overflows from size of struct


//For multiplayer displaying help text
CONST_INT bsClickMyComputer		0
CONST_INT bsConnectViaMobile	1
CONST_INT bsStartHackConnect	2
CONST_INT bsUseHackConnect		3
CONST_INT bsCloseHackConnect	4
CONST_INT bsStartBruteForce		5
CONST_INT bsUseBruteForce		6
CONST_INT bsLogOff				7

/*
                           888           .d888                               
                           888          d88P"                                
                           888          888                                  
.d8888b   .d8888b  8888b.  888  .d88b.  888888 .d88b.  888d888 88888b.d88b.  
88K      d88P"        "88b 888 d8P  Y8b 888   d88""88b 888P"   888 "888 "88b 
"Y8888b. 888      .d888888 888 88888888 888   888  888 888     888  888  888 
     X88 Y88b.    888  888 888 Y8b.     888   Y88..88P 888     888  888  888 
 88888P'  "Y8888P "Y888888 888  "Y8888  888    "Y88P"  888     888  888  888
 
 */
 
SCALEFORM_INDEX sfHacking
//anim to be updated with actual hacking anim
//STRING sAnimHacking = "amb@computer@hacking@"

INT iThisHacking

INT iHackTimer
CONST_INT cPopUpTime 2500
CONST_INT cPopUpMinTime 500
INT iErrorPopupTimer = 0
BOOL WarningPopup = false

//BOOL bTestMiniGame = true

// List of minigame feedback flags
//ENUM minigame_scaleform_flags
//	mgsf_nothing = 0,
//	mgsf_pc_window = 40,
//	mgsf_av_open = 2,
//	mgsf_av_start_clean = 50,
//	mgsf_av_exterminate = 51,
//	mgsf_av_popup_close = 60
//ENDENUM

ENUM DESKTOP_WALLPAPER
	WALL_FBI  = 0,
	WALL_BANK,
	WALL_HUMANE,
	WALL_GOVERMENT,
	WALL_GENERIC,
	WALL_MERRYWEATHER,
	WALL_DUGGAN
ENDENUM

ENUM UNLOCK_SOUND
	UNLOCK_SOUND_NONE,
	UNLOCK_SOUND_VAULT,
	UNLOCK_SOUND_STANDARD_DOOR,
	UNLOCK_SOUND_METAL_BAR_DOOR,
	UNLOCK_SOUND_GARAGE,
	UNLOCK_SOUND_METAL_GATE,
	UNLOCK_SOUND_CHAINLINK_FENCE
ENDENUM

/// REQUEST_HACKING_MINI_GAME 
/// NOTES: called in the hacking script but can be precalled and checked by the scripter if necessary
/// PURPOSE:
///   requests assets.
/// PARAMS:
///  
PROC REQUEST_HACKING_MINI_GAME(BOOL bRequestMPAssets = TRUE)
	//REQUEST_ANIM_DICT(sAnimHacking)
	IF bRequestMPAssets
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_0")
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_1")
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_2")
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_3")
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_4")
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_5")
		REQUEST_STREAMED_TEXTURE_DICT("HACKING_PC_desktop_6")
	ENDIF
	// bug 2051842, changed to skip render while paused
	sfHacking = REQUEST_SCALEFORM_MOVIE_SKIP_RENDER_WHILE_PAUSED("Hacking_PC")
	//sfHacking = REQUEST_SCALEFORM_MOVIE("Hacking_PC")
ENDPROC

/// HAVE_HACKING_ASSETS_LOADED()
/// NOTES: called in the hacking script but can be precalled and checked by the scripter if necessary
/// PURPOSE:
/// checking assets are loaded
/// PARAMS:
/// RETURNS: 
  
FUNC BOOL HAVE_HACKING_ASSETS_LOADED(BOOL bCheckMPAssets = TRUE)
	//REQUEST_ANIM_DICT(sAnimHacking)
	//sfHacking = REQUEST_SCALEFORM_MOVIE("Hacking_PC")
	//REQUEST_STREAMED_TEXTURE_DICT
	IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
	AND ( NOT bCheckMPAssets 
	OR ( HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_0")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_1")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_2")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_3")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_4")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_5")
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("HACKING_PC_desktop_6") ) )
	//AND HAS_ANIM_DICT_LOADED(sAnimHacking)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MANAGE_HACKING_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	PRINTLN("[AW_MISSIONDEBUG] Manage Hacking Timer - ", current_time - start_time, "of " , time, " remaining.")
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_TIME_AS_MINUTES_SECONDS_MILLISECONDS(INT raw_time, INT &mins, INT &secs, INT &millisecs)
	mins = (raw_time % (1000*60*60)) / (1000*60)
    secs = ((raw_time % (1000*60*60)) % (1000*60)) / 1000
    millisecs = ((raw_time % (1000*60*60)) % (1000*60) % 1000)
ENDPROC

// POPUP_MSG()
// NOTES: opens a custom error message popup, used thoughout the UI
// PARAMS: errorStr
PROC POPUP_MSG(BOOL show = TRUE, STRING errorStr = NULL)

	PRINTSTRING(errorStr)
	
	IF show 
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"OPEN_ERROR_POPUP", TO_FLOAT(1),INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM, errorStr)
		iErrorPopupTimer =  GET_GAME_TIMER()
		WarningPopup = TRUE
	ELSE
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"OPEN_ERROR_POPUP", TO_FLOAT(0),INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM, errorStr)
		WarningPopup = FALSE
	ENDIF
		
ENDPROC

PROC MANAGE_SOUNDS()

	IF iThisHacking < 5
		IF IS_CONTROL_JUST_PRESSED (FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK")
		ENDIF
	ENDIF
	
ENDPROC

PROC RANDOMISE_PASSWORD(S_HACKING_DATA &hacking_data, BOOL bUseNewPasswords = FALSE, BOOL bDugganPassword = FALSE)
	INT iRand

	IF bUseNewPasswords
		iRand = GET_RANDOM_INT_IN_RANGE(0,15)
	
		IF iRand = 0
			hacking_data.sPassword = "GR_PWD_RA"
		ELIF iRand = 1
			hacking_data.sPassword = "GR_PWD_PA"
		ELIF iRand = 2
			hacking_data.sPassword = "GR_PWD_PE"
		ELIF iRand = 3
			hacking_data.sPassword = "GR_PWD_GE"
		ELIF iRand = 4
			hacking_data.sPassword = "GR_PWD_CH"
		ELIF iRand = 5
			hacking_data.sPassword = "GR_PWD_FI"
		ELIF iRand = 6
			hacking_data.sPassword = "GR_PWD_PR"
		ELIF iRand = 7
			hacking_data.sPassword = "GR_PWD_UN"
		ELIF iRand = 8
			hacking_data.sPassword = "GR_PWD_DE"
		ELIF iRand = 9
			hacking_data.sPassword = "GR_PWD_FO"
		ELIF iRand = 10
			hacking_data.sPassword = "GR_PWD_AR"
		ELIF iRand = 11
			hacking_data.sPassword = "GR_PWD_TO"
		ELIF iRand = 12
			hacking_data.sPassword = "GR_PWD_IN"
		ELIF iRand = 13
			hacking_data.sPassword = "GR_PWD_PRI"
		ELIF iRand = 14
			hacking_data.sPassword = "GR_PWD_LA"
		ELSE
			hacking_data.sPassword = "GR_PWD_PA"
		ENDIF
	ELIF bDugganPassword
		iRand = GET_RANDOM_INT_IN_RANGE(0,4)
		IF iRand = 0
			hacking_data.sPassword = "DG_PWD_0"
		ELIF iRand = 1
			hacking_data.sPassword = "DG_PWD_1"
		ELIF iRand = 2
			hacking_data.sPassword = "DG_PWD_2"
		ELIF iRand = 3
			hacking_data.sPassword = "DG_PWD_3"
		ENDIF
	ELSE
		iRand = GET_RANDOM_INT_IN_RANGE(0,8)
		
		IF iRand = 0
			hacking_data.sPassword = "PASSWORD"
		ELIF iRand = 1
			hacking_data.sPassword = "PETSNAME"
		ELIF iRand = 2
			hacking_data.sPassword = "DYNAMITE"
		ELIF iRand = 3
			hacking_data.sPassword = "BACKDOOR"
		ELIF iRand = 4
			hacking_data.sPassword = "MORPHEUS"
		ELIF iRand = 5
			hacking_data.sPassword = "CREAMPIE"
		ELIF iRand = 6
			hacking_data.sPassword = "WORMSIGN"
		ELIF iRand = 7
			hacking_data.sPassword = "UNLOCKED"
		ELIF iRand = 8
			hacking_data.sPassword = "BLUEBOOK"
		ELIF iRand = 9
			hacking_data.sPassword = "DECIPHER"
		ELIF iRand = 10
			hacking_data.sPassword = "JUNKYARD"
		ELSE
			hacking_data.sPassword = "PASSWORD"
		ENDIF
	ENDIF
	
ENDPROC

//RESET_HACKING_DATA
//run once to reset the data as though the machine had not been hacked

PROC INIT_HACKING_DATA(S_HACKING_DATA &hacking_data)
	INT HackingInt = 0
	FOR HackingInt = (MAX_SOUNDS - 1) to 0
		sHacking[HackingInt] = -1		
	ENDFOR
	hacking_data.i_hacking_sound = -1
ENDPROC

PROC RESET_HACKING_DATA(S_HACKING_DATA &hacking_data,BOOL bRemoveScaleForm = TRUE, BOOL  bAllData = TRUE, BOOL bBlockAudioRelease = FALSE)	
	int HackingInt = 0
	
	IF NOT bBlockAudioRelease
		FOR HackingInt = (MAX_SOUNDS - 1) to 0
			IF sHacking[HackingInt] != -1 
				IF NOT HAS_SOUND_FINISHED(sHacking[HackingInt])
					STOP_SOUND(sHacking[HackingInt])
					PRINTLN("STOPPING SOUND sHacking[HackingInt]")
					
					RELEASE_SOUND_ID(sHacking[HackingInt])
					sHacking[HackingInt] = -1 
				ENDIF
			ENDIF
		ENDFOR
		IF hacking_data.i_hacking_sound > -1
			IF NOT HAS_SOUND_FINISHED(hacking_data.i_hacking_sound)
				PRINTLN("RESET_NEW_HACKING_MINIGAME_DATA - Releasing hacking_data.i_hacking_sound  (", hacking_data.i_hacking_sound, ")")
				STOP_SOUND(hacking_data.i_hacking_sound)
				RELEASE_SOUND_ID(hacking_data.i_hacking_sound)
				hacking_data.i_hacking_sound = -1
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
	CLEAR_BIT(hacking_data.bsHacking,BS_BRUTE_FORCE_OPEN)

	IF bAllData = TRUE
		CLEAR_BIT(hacking_data.bsHacking,BS_HACKING_SUCCESSFUL)
		CLEAR_BIT(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
		CLEAR_BIT(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
		CLEAR_BIT(hacking_data.iBS2Hacking, BS2_MAP_SHOWN)

		//Agency
		CLEAR_BIT(hacking_data.bsHacking,BS_DOWNLOAD_ICON_DISPLAYED)
		CLEAR_BIT(hacking_data.bsHacking,BS_DOWNLOAD_OPEN)
	ENDIF
	
	hacking_data.iBS2Hacking = 0
	hacking_data.i_lives = hacking_data.i_max_lives
	iThisHacking = 0
	//REMOVE_ANIM_DICT(sAnimHacking)
	IF bRemoveScaleForm
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfHacking)
	ENDIF
	
	CLEAR_BIT(hacking_data.bsHacking,BS_WALLPAPER_INITIALISED)
	SET_MULTIHEAD_SAFE(FALSE, TRUE)
	
//	SET_SPRITES_DRAW_BEFORE_FADE(FALSE)
//	SET_SCRIPT_GFX_DRAW_ORDER(FALSE)

ENDPROC

/// FORCE_QUIT_FAIL_HACKING_MINIGAME_KEEP_DATA (pass in your own hacking data struct)
/// NOTES: Force the termination of the hacking minigame and cause the player to fail but preserves variable data.
/// PURPOSE:
///   Force the termination of the hacking minigame and cause the player to fail.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No Returns
///  
PROC FORCE_QUIT_FAIL_HACKING_MINIGAME_KEEP_DATA(S_HACKING_DATA &hacking_data)
//	SET_SPRITES_DRAW_BEFORE_FADE(FALSE)
//	SET_SCRIPT_GFX_DRAW_ORDER(FALSE)
	SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	DESTROY_CAM(cam_temp)
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	SET_BIT(hacking_data.bsHacking,BS_QUIT_HACKING)
	FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(FALSE)
	RESET_HACKING_DATA(hacking_data,TRUE,FALSE)
	#IF IS_NEXTGEN_BUILD
		// DISABLE MULTIPLE DISPLAYS/BLINDERS
		SET_MULTIHEAD_SAFE(FALSE, TRUE)
		PRINTLN( "Hacking mini game RENDER_PC()Disable SET_MULTIHEAD_SAFE")
	#ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_QUIT_THE_HACKING(S_HACKING_DATA &hacking_data)
	IF IS_BIT_SET(hacking_data.bsHacking,BS_QUIT_HACKING)
		CLEAR_BIT(hacking_data.bsHacking,BS_QUIT_HACKING)
		FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(FALSE)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

// FORCE_PASSWORD_TO_BE_STRING()
// NOTES: ensure you call this if you have set the random password flag to false.
// checking assets are loaded
// PARAMS:
// RETURNS:
PROC FORCE_PASSWORD_TO_BE_STRING(S_HACKING_DATA &hacking_data,STRING sYourString)
	hacking_data.sPassword = sYourString
ENDPROC

/// PURPOSE:
///    Initialises the wallpaper of the hacking scaleform and icon labels
/// PARAMS:
///    wall - the wall paper to use
PROC INITIALISE_HACKING_WALLPAPER_AND_ICONS(S_HACKING_DATA &hacking_data, DESKTOP_WALLPAPER wall = WALL_BANK, BOOL bHideMyComputer = FALSE)
	IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_WALLPAPER_INITIALISED) //IF NOT hacking_data.b_wallpaper_initialised 
		// initialise wall paper
		PRINTLN("[AW_HACKING] DESKTOP_WALLPAPER selected as : ", ENUM_TO_INT(wall))
		PRINTLN("[AW_HACKING] bHideMyComputer : ", bHideMyComputer)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_BACKGROUND",TO_FLOAT(ENUM_TO_INT(wall)))
		
		IF (!bHideMyComputer)
			// Intialise icons and labels
			IF GET_IS_WIDESCREEN()
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"ADD_PROGRAM",TO_FLOAT(7),TO_FLOAT(4),INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"MyComputer")
			ELSE
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"ADD_PROGRAM",TO_FLOAT(8),TO_FLOAT(4),INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"MyComputer")
			ENDIF
		ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LABELS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_1")
			IF wall = WALL_FBI
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_2a")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_2")
			ENDIF
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_3")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_4")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_5")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("H_ICON_6")
		END_SCALEFORM_MOVIE_METHOD()
		
		SET_BIT(hacking_data.bsHacking,BS_WALLPAPER_INITIALISED)
		//hacking_data.b_wallpaper_initialised = TRUE
	ENDIF

ENDPROC

	
STRUCT S_SPRITE_DATA
	FLOAT fCentreX
	FLOAT fCentreY
//	FLOAT fWidth
//	FLOAT fHeight
	//FLOAT fRotation
ENDSTRUCT

STRUCT S_DC_MINIGAME_DATA

	INT iCurrentlySelectedColumn
	INT iIncrementFailCount

ENDSTRUCT

STRUCT S_DC_COLUMN_DATA

	BOOL bRowLocked
	FLOAT fSpeed
	FLOAT fPosition
	BOOL bGoUP
	//FLOAT fSoundValue

ENDSTRUCT

S_SPRITE_DATA sMainWindowData
S_SPRITE_DATA sMainComponentData[8]

S_DC_MINIGAME_DATA sThisDCHacking

S_DC_COLUMN_DATA sColumnData[8]

FLOAT fSoundValue

//0.568
//0.574

//next gen only tolerance change
//FLOAT UpperTol = 0.59
//FLOAT LowerTol = 0.54
FLOAT UpperTol = 0.62
FLOAT LowerTol = 0.51

FLOAT fScaleDifficulty = 0.55

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID wgThisMissionsWidget
	
	PROC CREATE_HACK_WIDGETS()
		wgThisMissionsWidget = START_WIDGET_GROUP("Hacking MiniGame")
		SET_CURRENT_WIDGET_GROUP(wgThisMissionsWidget)
		START_WIDGET_GROUP("Hacking Minigame") 
            ADD_WIDGET_FLOAT_SLIDER("sMainWindowData.fCentreX",sMainWindowData.fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainWindowData.fCentreY", sMainWindowData.fCentreY,0.0,1.0,0.001) 
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[0].fCentreX", sMainComponentData[0].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[0].fCentreY", sMainComponentData[0].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[1].fCentreX", sMainComponentData[1].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[1].fCentreY", sMainComponentData[1].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[2].fCentreX", sMainComponentData[2].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[2].fCentreY", sMainComponentData[2].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[3].fCentreX", sMainComponentData[3].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[3].fCentreY", sMainComponentData[3].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[4].fCentreX", sMainComponentData[4].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[4].fCentreY", sMainComponentData[4].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[5].fCentreX", sMainComponentData[5].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[5].fCentreY", sMainComponentData[5].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[6].fCentreX", sMainComponentData[6].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[6].fCentreY", sMainComponentData[6].fCentreY,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[7].fCentreX", sMainComponentData[7].fCentreX,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[7].fCentreY", sMainComponentData[7].fCentreY,0.0,1.0,0.001)
            //ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[7].fWidth", sMainComponentData[7].fWidth,-10.0,10.0,0.001)
			//ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[7].fHeight", sMainComponentData[7].fHeight,-10.0,10.0,0.001)                                   
          	//ADD_WIDGET_FLOAT_SLIDER("sMainComponentData[7].fRotation", sMainComponentData[7].fRotation,-360,360,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[0].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[1].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[2].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[3].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[4].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[5].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[6].fSpeed,-360,360,0.0001)
			ADD_WIDGET_FLOAT_SLIDER("fColumnSpeed", sColumnData[7].fSpeed,-360,360,0.0001)
			
			ADD_WIDGET_FLOAT_SLIDER("UpperTol", UpperTol,0.0,1.0,0.001)
            ADD_WIDGET_FLOAT_SLIDER("LowerTol", LowerTol,0.0,1.0,0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("fScaleDifficulty ", fScaleDifficulty ,0.0,10.0,0.001)

		STOP_WIDGET_GROUP()
	ENDPROC
#ENDIF

PROC INITIALISE_MINI_GAME_VALUES()
	
	UpperTol = 0.6
	LowerTol = 0.53
	
	sColumnData[0].fSpeed = 0.02 * fScaleDifficulty
	sColumnData[1].fSpeed = 0.025 * fScaleDifficulty
	sColumnData[2].fSpeed = 0.03 * fScaleDifficulty
	sColumnData[3].fSpeed = 0.035 * fScaleDifficulty
	sColumnData[4].fSpeed = 0.04 * fScaleDifficulty
	sColumnData[5].fSpeed = 0.045 * fScaleDifficulty 
	sColumnData[6].fSpeed = 0.048 * fScaleDifficulty
	sColumnData[7].fSpeed = 0.05 * fScaleDifficulty
	
	sColumnData[0].bRowLocked = FALSE
	sColumnData[1].bRowLocked = FALSE
	sColumnData[2].bRowLocked = FALSE
	sColumnData[3].bRowLocked = FALSE
	sColumnData[4].bRowLocked = FALSE
	sColumnData[5].bRowLocked = FALSE
	sColumnData[6].bRowLocked = FALSE
	sColumnData[7].bRowLocked = FALSE
	
	sMainWindowData.fCentreX = 0.5
    sMainWindowData.fCentreY = 0.5                               

	
	sMainComponentData[0].fCentreX = 0.350
    sMainComponentData[0].fCentreY = 0.4 //0.572 centre                                  
	
	sMainComponentData[1].fCentreX = 0.390
    sMainComponentData[1].fCentreY = 0.4                                
	
	sMainComponentData[2].fCentreX = 0.430
    sMainComponentData[2].fCentreY = 0.4                                
	
	sMainComponentData[3].fCentreX = 0.470
    sMainComponentData[3].fCentreY = 0.4                                
	
	sMainComponentData[4].fCentreX = 0.510
    sMainComponentData[4].fCentreY = 0.740                                  
	
	sMainComponentData[5].fCentreX = 0.550
    sMainComponentData[5].fCentreY = 0.740                                 
	
	sMainComponentData[6].fCentreX = 0.590
    sMainComponentData[6].fCentreY = 0.740                                 
	
	sMainComponentData[7].fCentreX = 0.630
    sMainComponentData[7].fCentreY = 0.740                                 

ENDPROC

FUNC BOOL IS_COLUMN_IN_POSITION(INT iColumnNum)

	IF sMainComponentData[iColumnNum].fCentreY >= LowerTol
	AND sMainComponentData[iColumnNum].fCentreY <= UpperTol
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_COLUMN_PERFECTLY_IN_POSITION(INT iColumnNum)

	IF sMainComponentData[iColumnNum].fCentreY = 0.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Safe zone
//0.568
//0.574

//Movement
//0.4
//0.744

FLOAT fNormalisingValue = 25
CONST_INT ciNUMBER_OF_NEW_MINI_GAME_SOUND_IDS 8
INT iLockOutTimer
INT iNewMiniGameSoundID[ciNUMBER_OF_NEW_MINI_GAME_SOUND_IDS]
INT iNewMiniGameSoundBitSet

PROC RUN_NEW_MINIGAME(S_HACKING_DATA &hacking_data)
	INT iDCColumn = 0

	//PRINTLN("iThisHackingNG")PRINTLN(iThisHackingNG)PRINTNL()
	
	IF hacking_data.iThisHackingNG > 0
		IF IS_BIT_SET(hacking_data.bsHacking,BS_TXD_HACK_LOADED)	
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("hackingNG")
				IF NOT IS_PAUSE_MENU_ACTIVE()
					DRAW_SPRITE("hackingNG", "DHMain", sMainWindowData.fCentreX, sMainWindowData.fCentreY, 0.731, 1.306 , 0, 255, 255, 255, 255)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF sColumnData[0].bRowLocked = TRUE
	AND sColumnData[1].bRowLocked = TRUE
	AND sColumnData[2].bRowLocked = TRUE
	AND sColumnData[3].bRowLocked = TRUE
	AND sColumnData[4].bRowLocked = TRUE
	AND sColumnData[5].bRowLocked = TRUE
	AND sColumnData[6].bRowLocked = TRUE
	AND sColumnData[7].bRowLocked = TRUE
		PLAY_SOUND_FRONTEND(-1,"Hack_Success" ,"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS")
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HNG")
			CLEAR_HELP()
		ENDIF
		
		IF IS_PC_VERSION()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HNG_KM")
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		SET_BIT(hacking_data.bsHacking,BS_NEW_HACKING_GAME_SOLVED)
		//hacking_data.b_new_hacking_game_solved = TRUE
	ELSE
		
		// Different help on PC keyboard and mouse
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HNG_KM")
				PRINT_HELP_FOREVER("H1_HNG_KM")
			ENDIF
			
		ELSE
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HNG")
				PRINT_HELP_FOREVER("H1_HNG")
			ENDIF
		
		ENDIF
		
	ENDIF
	
	SWITCH hacking_data.iThisHackingNG
	
		CASE 0	
			#IF IS_DEBUG_BUILD
				PRINTLN("RUN_NEW_MINIGAME - SCALING WITH FRAMERATE ")
			#ENDIF
			REQUEST_STREAMED_TEXTURE_DICT("hackingNG")
			CLEAR_BIT(hacking_data.bsHacking,BS_TXD_HACK_LOADED)
			PRINTLN("RUN_NEW_MINIGAME()")PRINTNL()
			INITIALISE_MINI_GAME_VALUES()
			iLockOutTimer = GET_GAME_TIMER()
			sThisDCHacking.iCurrentlySelectedColumn = 0
			CLEAR_BIT(iNewMiniGameSoundBitSet,0)
			CLEAR_BIT(iNewMiniGameSoundBitSet,1)
			CLEAR_BIT(iNewMiniGameSoundBitSet,2)
			CLEAR_BIT(iNewMiniGameSoundBitSet,3)
			CLEAR_BIT(iNewMiniGameSoundBitSet,4)
			CLEAR_BIT(iNewMiniGameSoundBitSet,5)
			CLEAR_BIT(iNewMiniGameSoundBitSet,6)
			CLEAR_BIT(iNewMiniGameSoundBitSet,7)			
			hacking_data.iThisHackingNG ++
		BREAK
		
		CASE 1
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("hackingNG")
				SET_BIT(hacking_data.bsHacking,BS_TXD_HACK_LOADED)
				#IF IS_DEBUG_BUILD
					//CREATE_HACK_WIDGETS()
				#ENDIF
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_CURSOR_VISIBILITY")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
				hacking_data.iThisHackingNG ++
			ENDIF
		BREAK
		
		CASE 2
			
			#IF IS_DEBUG_BUILD // Reset
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
					sThisDCHacking.iCurrentlySelectedColumn = 0
					sColumnData[0].bRowLocked = FALSE
					sColumnData[1].bRowLocked = FALSE
					sColumnData[2].bRowLocked = FALSE
					sColumnData[3].bRowLocked = FALSE
					sColumnData[4].bRowLocked = FALSE
					sColumnData[5].bRowLocked = FALSE
					sColumnData[6].bRowLocked = FALSE
					sColumnData[7].bRowLocked = FALSE
					hacking_data.iThisHackingNG = 0
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
					fNormalisingValue = fNormalisingValue - 1
					PRINTLN("fNormalisingValue:  ",fNormalisingValue)
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
					fNormalisingValue = fNormalisingValue + 1
					PRINTLN("fNormalisingValue:  ",fNormalisingValue)
				ENDIF
			#ENDIF
			
			FOR iDCColumn = 0 TO ( 7 )
				IF sColumnData[iDCColumn].bGoUP = TRUE
					IF sColumnData[iDCColumn].fPosition < 1
					   sColumnData[iDCColumn].fPosition = sColumnData[iDCColumn].fPosition + (sColumnData[iDCColumn].fSpeed *(TIMESTEP()) * fNormalisingValue)
					ELSE
						sColumnData[iDCColumn].bGoUP = FALSE
					ENDIF
				ELSE
					IF sColumnData[iDCColumn].fPosition > 0
					   sColumnData[iDCColumn].fPosition = sColumnData[iDCColumn].fPosition - (sColumnData[iDCColumn].fSpeed *(TIMESTEP()) * fNormalisingValue)
					ELSE
						sColumnData[iDCColumn].bGoUP = TRUE
					ENDIF
				ENDIF
				
				IF sColumnData[iDCColumn].bRowLocked = FALSE
					sMainComponentData[iDCColumn].fCentreY = COSINE_INTERP_FLOAT(0.744,0.4,sColumnData[iDCColumn].fPosition) //*TIMESTEP()
				ENDIF
			ENDFOR
			
			//Position Sound
			FOR iDCColumn = 0 TO ( 7 )
				IF sColumnData[iDCColumn].bRowLocked = FALSE
					IF sThisDCHacking.iCurrentlySelectedColumn = iDCColumn
						IF NOT IS_BIT_SET(iNewMiniGameSoundBitSet,iDCColumn)
							iNewMiniGameSoundID[iDCColumn] = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(iNewMiniGameSoundID[iDCColumn],"Pin_Movement","DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS")
							SET_BIT(iNewMiniGameSoundBitSet,iDCColumn)
							PRINTLN("iNewMiniGameSoundID: for column : ",iDCColumn)
						ELSE
							IF sColumnData[iDCColumn].fPosition >= 0.5 
								fSoundValue = 2 * (1- sColumnData[iDCColumn].fPosition)
							ELSE
								fSoundValue = 2 * sColumnData[iDCColumn].fPosition
							ENDIF
							SET_VARIABLE_ON_SOUND(iNewMiniGameSoundID[iDCColumn],"PinMovementY",fSoundValue)
							PRINTLN("iNewMiniGameSoundID: setting variable sound on column: ",iDCColumn, "with the following value: ", fSoundValue)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iNewMiniGameSoundBitSet,iDCColumn)
						STOP_SOUND(iNewMiniGameSoundID[iDCColumn])
						RELEASE_SOUND_ID(iNewMiniGameSoundID[iDCColumn])
						CLEAR_BIT(iNewMiniGameSoundBitSet,iDCColumn)
						iNewMiniGameSoundID[iDCColumn] = -1
					ENDIF
				ENDIF
			ENDFOR
			
			IF IS_COLUMN_PERFECTLY_IN_POSITION(sThisDCHacking.iCurrentlySelectedColumn)
				PLAY_SOUND_FRONTEND(-1,"Pin_Centred" ,"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS")
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				IF MANAGE_HACKING_TIMER(iLockOutTimer,300)
					PRINTLN("sThisDCHacking.iCurrentlySelectedColumn:")PRINTLN(sThisDCHacking.iCurrentlySelectedColumn)PRINTNL()
					PRINTLN("fCentreY:")PRINTLN(sMainComponentData[sThisDCHacking.iCurrentlySelectedColumn].fCentreY)PRINTNL()
					IF IS_COLUMN_IN_POSITION(sThisDCHacking.iCurrentlySelectedColumn)
						sColumnData[sThisDCHacking.iCurrentlySelectedColumn].bRowLocked = TRUE
						sMainComponentData[sThisDCHacking.iCurrentlySelectedColumn].fCentreY = 0.572
						PLAY_SOUND_FRONTEND(-1,"Pin_Good" ,"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS")
						IF sThisDCHacking.iCurrentlySelectedColumn != 7
							sThisDCHacking.iCurrentlySelectedColumn ++
						ENDIF
					ELSE
						
						IF sThisDCHacking.iCurrentlySelectedColumn != 0
							IF IS_BIT_SET(iNewMiniGameSoundBitSet,sThisDCHacking.iCurrentlySelectedColumn)
								PRINTLN("Column:", sThisDCHacking.iCurrentlySelectedColumn, " is not in position - stopping sound.") 
								STOP_SOUND(iNewMiniGameSoundID[sThisDCHacking.iCurrentlySelectedColumn])
								RELEASE_SOUND_ID(iNewMiniGameSoundID[sThisDCHacking.iCurrentlySelectedColumn])
								CLEAR_BIT(iNewMiniGameSoundBitSet,sThisDCHacking.iCurrentlySelectedColumn)
								iNewMiniGameSoundID[sThisDCHacking.iCurrentlySelectedColumn] = -1
							ENDIF
							PRINTLN("!!!Failed ")PRINTLN()
							PRINTLN("Column number ")PRINTLN(sThisDCHacking.iCurrentlySelectedColumn - 1)PRINTLN(" unlocked.")PRINTNL()
							sColumnData[sThisDCHacking.iCurrentlySelectedColumn - 1].bRowLocked = FALSE
							sThisDCHacking.iCurrentlySelectedColumn = sThisDCHacking.iCurrentlySelectedColumn - 1
						ENDIF
						IF NOT IS_PAUSE_MENU_ACTIVE()
							DRAW_RECT(0.5, 0.5, 4.0, 4.0, 255, 0, 0, 100)
						ENDIF
						iLockOutTimer = GET_GAME_TIMER()
						SET_CONTROL_SHAKE(PLAYER_CONTROL,100,100)
						PLAY_SOUND_FRONTEND(-1,"Pin_Bad" ,"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS")
						sThisDCHacking.iIncrementFailCount = 1
					ENDIF
				ELSE
					IF NOT IS_PAUSE_MENU_ACTIVE()
						//Bug Change: 2226859 
						DRAW_SPRITE("hackingNG", "DHMain", sMainWindowData.fCentreX, sMainWindowData.fCentreY, 0.731, 1.306 , 0, 255, 0, 0, 100)
						//DRAW_RECT(0.5, 0.5, 4.0, 4.0, 255, 0, 0, 200)
					ENDIF
					iLockOutTimer = GET_GAME_TIMER()
					SET_CONTROL_SHAKE(PLAYER_CONTROL,100,100)
				ENDIF
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF sThisDCHacking.iCurrentlySelectedColumn = 0
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[0].fCentreX, sMainComponentData[0].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[0].fCentreX, sMainComponentData[0].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 1
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[1].fCentreX, sMainComponentData[1].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[1].fCentreX, sMainComponentData[1].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 2
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[2].fCentreX, sMainComponentData[2].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[2].fCentreX, sMainComponentData[2].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 3
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[3].fCentreX, sMainComponentData[3].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[3].fCentreX, sMainComponentData[3].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 4
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[4].fCentreX, sMainComponentData[4].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[4].fCentreX, sMainComponentData[4].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 5
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[5].fCentreX, sMainComponentData[5].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[5].fCentreX, sMainComponentData[5].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 6
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[6].fCentreX, sMainComponentData[6].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[6].fCentreX, sMainComponentData[6].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF  sThisDCHacking.iCurrentlySelectedColumn = 7
					DRAW_SPRITE("hackingNG", "DHCompHi", sMainComponentData[7].fCentreX, sMainComponentData[7].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ELSE
					DRAW_SPRITE("hackingNG", "DHComp", sMainComponentData[7].fCentreX, sMainComponentData[7].fCentreY, 0.4, 0.4, 0, 255, 255, 255, 255)
				ENDIF
				
				IF IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY) 
					IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							SET_BIT(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
							CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK

	ENDSWITCH
ENDPROC

PROC RESET_NEW_HACKING_MINIGAME_DATA(S_HACKING_DATA &hacking_data)

	RESET_HACKING_DATA(hacking_data)
	
	hacking_data.bsHacking = 0
	hacking_data.iThisHackingNG = 0
	
	sThisDCHacking.iCurrentlySelectedColumn = 0
	sThisDCHacking.iIncrementFailCount = 0
	
	CLEAR_BIT(hacking_data.bsHacking,BS_NEW_HACKING_GAME_SOLVED)
	
	//Unlock collumns 
	sColumnData[0].bRowLocked = FALSE
	sColumnData[1].bRowLocked = FALSE
	sColumnData[2].bRowLocked = FALSE
	sColumnData[3].bRowLocked = FALSE
	sColumnData[4].bRowLocked = FALSE
	sColumnData[5].bRowLocked = FALSE
	sColumnData[6].bRowLocked = FALSE
	sColumnData[7].bRowLocked = FALSE
ENDPROC
BOOL bDownAndOutHelp
BOOL bPlaySuccessBeep = FALSE

// RENDER_PC()
// NOTES: called in the hacking script but can be precalled and checked by the scripter if necessary
// checking assets are loaded
// PARAMS:
// RETURNS:

PROC RENDER_PC(S_HACKING_DATA &hacking_data, INT iStartLives = 5, INT iMaxLives = 5, INT iStartingSpeed = 100, INT iSpeedDecayRate = 10, INT iBruteForceColumns = 8, INT iChanceOfRedHerrings = 10,BOOL bDoTutorial = FALSE,BOOL bJustHackConnect = FALSE,BOOL bJustBruteForce = FALSE,BOOL bRandomPassword = TRUE, BOOL bHackConnectBeforeBrute = FALSE, BOOL bExitButton = FALSE, BOOL bDoDownloadOpen = FALSE, desktop_wallpaper wall = WALL_FBI,INT iHackingStartTime = 60000, BOOL bMP = TRUE, BOOL bSetInstructionalButtonsUnderHud = TRUE, BOOL bUseNewPasswords = FALSE, BOOL bUseNewSound = FALSE, BOOL bHideMyComputer = FALSE, BOOL bDoMapOpen = FALSE, BOOL bDugganPassword = FALSE)
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	IF bSetInstructionalButtonsUnderHud
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	ENDIF
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	//PRINTLN("iThisHacking")PRINTLN(iThisHacking)PRINTNL()

	INT iClick
	INT rawTime
	INT iPadLeftX, iPadLeftY, iPadRightX, iPadRightY
	//SCALEFORM_RETURN_INDEX iClick
	
	//IF iThisHacking > 1
	IF HAVE_HACKING_ASSETS_LOADED(bMP)
	
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			IF GET_PAUSE_MENU_STATE() = PM_INACTIVE
			AND NOT IS_PAUSE_MENU_ACTIVE()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfHacking,255,255,255,0)
		ENDIF
		ENDIF
		
		IF IS_USING_CURSOR( FRONTEND_CONTROL )
				
			//FLOAT f_PCMousePosX, f_PCMousePosY
			//GET_MOUSE_POSITION( f_PCMousePosX, f_PCMousePosY )
			hacking_data.f_PCMousePosX = GET_CONTROL_UNBOUND_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
			hacking_data.f_PCMousePosY = GET_CONTROL_UNBOUND_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
			
			IF hacking_data.f_PCMousePosPrevX != hacking_data.f_PCMousePosX
			OR hacking_data.f_PCMousePosPrevY != hacking_data.f_PCMousePosY

				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_CURSOR")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( hacking_data.f_PCMousePosX )
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( hacking_data.f_PCMousePosY )
				END_SCALEFORM_MOVIE_METHOD()
							
			ENDIF
			
			hacking_data.f_PCMousePosPrevX = hacking_data.f_PCMousePosX
			hacking_data.f_PCMousePosPrevY = hacking_data.f_PCMousePosY

		ELSE
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iPadLeftX,iPadLeftY,iPadRightX,iPadRightY)
		//Fix for unref
		IF iPadRightX < 0
		OR iPadRightY < 0
		
		ENDIF

		
		FLOAT fXMove
		FLOAT fYMove
		
		fXMove = TO_FLOAT(iPadLeftX)*fSensitivity*TIMESTEP()
		fYMove = TO_FLOAT(iPadLeftY)*fSensitivity*TIMESTEP()
		
	//		PRINTSTRING("fXMove:")PRINTFLOAT(fXMove)PRINTNL()
	//		PRINTSTRING("fYMove:")PRINTFLOAT(fYMove)PRINTNL()
	//		PRINTSTRING("Sensitivity")PRINTFLOAT(fSensitivity)PRINTNL()
		
		IF iThisHacking != 8
			IF fXMove > 1 
			OR fYMove > 1
			OR fXMove < -1 
			OR fYMove < -1 
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "MOVE_CURSOR")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXMove)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fYMove)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDIF
		ENDIF
		
		
		
		IF iThisHacking > 4
			
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
					
				hacking_data.f_PCMousePosX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
				hacking_data.f_PCMousePosY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
				
				PRINTLN("hacking_data.f_PCMousePosX")PRINTLN(hacking_data.f_PCMousePosX)
				PRINTLN("hacking_data.f_PCMousePosY")PRINTLN(hacking_data.f_PCMousePosY)
				
				IF IS_PLAYSTATION_PLATFORM()
				
					IF hacking_data.f_PCMousePosPrevX != hacking_data.f_PCMousePosX
				
						IF hacking_data.f_PCMousePosX < 0.49
							PRINTLN("SCALEFORM_INPUT_EVENT_LEFT")
							PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFT))
							END_SCALEFORM_MOVIE_METHOD()
						ELIF hacking_data.f_PCMousePosX > 0.51
							PRINTLN("SCALEFORM_INPUT_EVENT_RIGHT")
							PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHT))
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
							
					ENDIF
				ENDIF
							
				hacking_data.f_PCMousePosPrevX = hacking_data.f_PCMousePosX
				hacking_data.f_PCMousePosPrevY = hacking_data.f_PCMousePosY
			
			ENDIF
			
			// PC Mouse support handled differently from PS4 trackpad for hacking minigame cursor movement - Steve R. LDS.
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
								
				
				FLOAT fPCMouseMoveDistX = GET_DISABLED_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_LOOK_LR )
				FLOAT fPCMouseMoveDistY = GET_DISABLED_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_LOOK_UD )
				
				BOOL  bMouseMoved = FALSE
									
				// Mouse movement is too fast by default, so we only allow to move when a certain time has passed, based on the game timestep.
				fPCMouseMoveDebounce = fPCMouseMoveDebounce + TIMESTEP()
				
				//PRINTLN("fPCMouseMoveDebounce")PRINTLN(fPCMouseMoveDebounce)
				
				// As we're not checking mouse movement every frame we work out an average value over the frames we don't check.
				fPCMouseAverageX += fPCMouseMoveDistX
				fPCMouseAverageY += fPCMouseMoveDistY
				
				fPCMouseAverageCount ++
				
				IF fPCMouseMoveDebounce > 0.15
				
					fPCMouseMoveDebounce = 0.0
					
					// Work out average of mouse since last debounce
					fPCMouseAverageX = fPCMouseAverageX / fPCMouseAverageCount
					fPCMouseAverageY = fPCMouseAverageY / fPCMouseAverageCount
					
					IF ABSF(fPCMouseAverageY) <0.2
					
						IF fPCMouseAverageX < 0
							PRINTLN("SCALEFORM_INPUT_EVENT_LEFT - MOUSE")
							bMouseMoved = TRUE
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFT))
							END_SCALEFORM_MOVIE_METHOD()
						ELIF fPCMouseAverageX > 0
							PRINTLN("SCALEFORM_INPUT_EVENT_RIGHT - MOUSE")
							bMouseMoved = TRUE
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHT))
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF	
						
					ENDIF
					
					IF ABSF(fPCMouseAverageX) < 0.2
						
						IF fPCMouseAverageY < 0
							PRINTLN("SCALEFORM_INPUT_EVENT_UP - MOUSE")
							bMouseMoved = TRUE
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
							END_SCALEFORM_MOVIE_METHOD()
						ELIF fPCMouseAverageY > 0
							PRINTLN("SCALEFORM_INPUT_EVENT_DOWN - MOUSE")
							bMouseMoved = TRUE
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
					
					ENDIF
											
					IF bMouseMoved
						PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
					ENDIF
					
					fPCMouseAverageX = 0.0
					fPCMouseAverageY = 0.0
					fPCMouseAverageCount = 0.0
					
				ENDIF
							
			ENDIF
			
			INT fAnalogue[4] 
			INT iTemp 
            
            GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(fAnalogue[0], fAnalogue[1], fAnalogue[2], fAnalogue[3]) 
            REPEAT COUNT_OF(fAnalogue) iTemp 
	            IF ABSI(fAnalogue[iTemp]) > 0 
					IF iTemp = 0
					OR iTemp = 1
	                    IF HAS_SOUND_FINISHED(hacking_data.i_hacking_sound)
							IF iThisHacking != 8
								PLAY_SOUND_FRONTEND(hacking_data.i_hacking_sound,"HACKING_MOVE_CURSOR")
							ENDIF
						ENDIF
					ENDIF
	            ENDIF 
            ENDREPEAT

			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_X)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_Y)
			OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_X)
			OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_Y)
				
			ENDIF
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_MOVE_UP_ONLY)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
					PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
					END_SCALEFORM_MOVIE_METHOD()
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_MOVE_DOWN_ONLY)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
					PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
					END_SCALEFORM_MOVIE_METHOD()
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_MOVE_LEFT_ONLY)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
					PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFT))
					END_SCALEFORM_MOVIE_METHOD()
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_MOVE_RIGHT_ONLY)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
					PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHT))
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			
			ELSE
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
				PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP))
				END_SCALEFORM_MOVIE_METHOD()
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
				PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN))
				END_SCALEFORM_MOVIE_METHOD()
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
				PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFT))
				END_SCALEFORM_MOVIE_METHOD()
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
				PLAY_SOUND_FRONTEND(-1,"HACKING_MOVE_CURSOR")
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHT))
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
				
			ENDIF
			
		ENDIF
		
		IF bExitButton
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				//CALL_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
				//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
				minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			ENDIF
			IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
				iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
				IF iClick = 6
					FORCE_QUIT_FAIL_HACKING_MINIGAME_KEEP_DATA(hacking_data)
				ENDIF
			ENDIF
		ENDIF
		
		MANAGE_SOUNDS()
	ENDIF
		//REQUEST_HACKING_MINI_GAME()
	//ENDIF
	
	// fix for negative time - 489549
	IF iThisHacking = 5
	OR iThisHacking = 6
		IF bStartHackTimer
			GET_TIME_AS_MINUTES_SECONDS_MILLISECONDS(iHackingStartTime - (GET_GAME_TIMER() - i_clock_start), iHackMinutes,iHackSeconds,iHackMilliseconds)
			IF iHackMinutes >= 0
			AND iHackSeconds >= 0
			AND iHackMilliseconds >= 0
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COUNTDOWN",to_float(iHackMinutes),to_float(iHackSeconds),to_float(iHackMilliseconds))
			ELSE
				iHackMinutes = 0
				iHackSeconds = 0
				iHackMilliseconds = 0
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COUNTDOWN",to_float(iHackMinutes),to_float(iHackSeconds),to_float(iHackMilliseconds))
			ENDIF
		ENDIF
	ENDIF
	
	//hack connect
	IF iThisHacking = 5
		IF MANAGE_HACKING_TIMER(iHackTimer, 10000 / iSpeedDecayRate)
			IF hacking_data.f_speed > 20
				hacking_data.f_speed = hacking_data.f_speed - 1
				PRINTSTRING("&&&&&&&&&&&&&&&&Speed:")PRINTFLOAT(hacking_data.f_speed)PRINTNL()
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",hacking_data.f_speed)
				iHackTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	//brute force
	IF iThisHacking = 6
		IF MANAGE_HACKING_TIMER(iHackTimer, 10000 / iSpeedDecayRate)
			IF hacking_data.f_speed > 20
				hacking_data.f_speed = hacking_data.f_speed - 1
				PRINTSTRING("&&&&&&&&&&&&&&&&Speed:")PRINTFLOAT(hacking_data.f_speed)PRINTNL()
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",0,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",1,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",2,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",3,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",4,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",5,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",6,hacking_data.f_speed)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",7,hacking_data.f_speed)
				iHackTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTSTRING("iThisHacking:")PRINTINT(iThisHacking)PRINTNL()
	
	IF IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY) // Going to skip the sequence where you have to click 'my computer' / 'external device' etc
		IF iThisHacking = 2
			//iClick = 4
			iThisHacking = 3
		ELIF iThisHacking = 3
			iClick = 81
		ELIF iThisHacking = 4
			IF bJustHackConnect 
				iClick = 82
			ELIF bJustBruteForce 
				iClick = 83
			ENDIF
		ENDIF
	ENDIF
	
	
	SWITCH iThisHacking
		
		//LOAD ASSETS
		CASE 0	
			//IF NOT hacking_data.b_wallpaper_initialised
			IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_WALLPAPER_INITIALISED)	
				INITIALISE_HACKING_WALLPAPER_AND_ICONS(hacking_data, wall, bHideMyComputer)
			ENDIF
			
			REQUEST_HACKING_MINI_GAME()
			FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(TRUE)
			hacking_data.i_hacking_sound = GET_SOUND_ID()
			
			//FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(TRUE)
			
			bDownAndOutHelp = FALSE
			bPlaySuccessBeep = FALSE
			hacking_data.iThisHackingNG = 0
			iThisHacking ++
		BREAK
		
		//INITIALISE DATA
		CASE 1
			INT iInitLoop
			REPEAT MAX_SOUNDS iInitLoop
				sHacking[iInitLoop] = -1 
			ENDREPEAT
			//DISABLE_CELLPHONE(TRUE)

			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives),TO_FLOAT(iMaxLives))
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",TO_FLOAT(iStartingSpeed))

			IF bExitButton = TRUE
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"ADD_PROGRAM",TO_FLOAT(7),TO_FLOAT(6),INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"EXIT")
			ENDIF
			//Sort
			iBruteForceColumns = iBruteForceColumns
			iChanceOfRedHerrings = iChanceOfRedHerrings
			hacking_data.i_lives = iStartLives
			hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
			CLEAR_BIT(hacking_data.bsHacking,BS_QUIT_HACKING)
		
			fPCMouseAverageX = 0.0
			fPCMouseAverageY = 0.0
			fPCMouseAverageCount = 0.0
		
			sHacking[0] = GET_SOUND_ID()
			sHacking[1] = GET_SOUND_ID()
			
			
			#IF IS_NEXTGEN_BUILD
				// ENABLE MULTIPLE DISPLAYS/BLINDERS
				SET_MULTIHEAD_SAFE(TRUE, TRUE)
				PRINTLN( "Hacking mini game RENDER_PC()Enabled SET_MULTIHEAD_SAFE")
			#ENDIF
			
			bHelpText = FALSE
			
			IF hacking_data.bTestHacking
				iThisHacking = 8
			ELSE
				iThisHacking ++
			ENDIF

		BREAK
		
		//CHECK IF PLAYER HAS CLICKED MY COMPUTER
		CASE 2
		
			IF bDoTutorial = TRUE
				IF bHelpText = FALSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_EXIT_PROMPT")
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ1")
							PRINT_HELP_FOREVER("HACKOBJ1")
							bHelpText = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	        
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF IS_USING_CURSOR(FRONTEND_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
						//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
						minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
					ENDIF
				ELSE
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
					//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
					minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
				ENDIF
			ENDIF
			ENDIF
			
			//MY COMPUTER
			IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
				iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
				IF iClick = 4
					IF bDoTutorial = TRUE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_EXIT_PROMPT")
							CLEAR_HELP()
						ENDIF
					ENDIF
					bHelpText = FALSE
					iThisHacking ++
				ENDIF
			ENDIF

			IF WarningPopup = TRUE
				IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime
					IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime 
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						PRINTSTRING("CLOSING")
						POPUP_MSG(FALSE, "")
				    ENDIF
				ENDIF
			ENDIF
			
			IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
				iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
				IF iClick = 80
					IF WarningPopup = FALSE
						PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
						POPUP_MSG(TRUE, "ACCESSD")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//CHECK IF PLAYER HAS CLICKED OPEN THE PHONE
		CASE 3
			
			IF bDoTutorial = TRUE
				IF iThisHacking = 3
					IF bHelpText = FALSE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ2")
							PRINT_HELP_FOREVER("HACKOBJ2")
							bHelpText = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
						//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
						minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
					ENDIF
					ELSE
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
							minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
				ENDIF
			ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF WarningPopup = FALSE
					IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_BACK")
							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CIRCLE))
							minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
							IF bDoTutorial = TRUE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ2")
									CLEAR_HELP()
								ENDIF
								PRINT_HELP_FOREVER("H_EXIT_PROMPT")
								bHelpText = FALSE
								iThisHacking --
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF WarningPopup = TRUE
				IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime
					IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime 
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						PRINTSTRING("CLOSING")
						POPUP_MSG(FALSE, "")
				    ENDIF
				ENDIF
			ENDIF
			
			IF WarningPopup = FALSE
				IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
					PRINTSTRING("*********iClick")PRINTINT(iClick)PRINTNL()
				ENDIF
			ENDIF
			
			//IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
				//iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
				IF iClick = 80
					IF WarningPopup = FALSE
						PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
						POPUP_MSG(TRUE, "ACCESSD")
					ENDIF
				ENDIF
			//ENDIF
			
			//IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
				//iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
				//PRINTSTRING("*********iClick")PRINTINT(iClick)PRINTNL()
				//OPEN THE PHONE
				IF iClick = 81
					bHelpText = FALSE
					iThisHacking ++
				ENDIF
			//ENDIF
		
		BREAK
		
		//CHECK IF AN APP HAS BEEN CLICKED
		CASE 4
			IF sHacking[1] != -1 
				IF NOT HAS_SOUND_FINISHED(sHacking[1])
					STOP_SOUND(sHacking[1])
					sHacking[1] = -1 
				ENDIF
			ENDIF
			
			IF sHacking[0] != -1 
				IF NOT HAS_SOUND_FINISHED(sHacking[0])
					STOP_SOUND(sHacking[0])
					sHacking[0] = -1 
				ENDIF
			ENDIF
			IF sHacking[1] != -1 
				IF NOT HAS_SOUND_FINISHED(sHacking[1])
					STOP_SOUND(sHacking[1])
					sHacking[1] = -1 
				ENDIF
			ENDIF

			IF bDoTutorial = TRUE
				IF iThisHacking = 4
					IF bJustBruteForce = TRUE
						//IF hacking_data.b_brute_force_open = FALSE
						IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_OPEN)
							IF bHelpText = FALSE
								//RUN BRUTEFORCE.EXE
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ5")
									PRINT_HELP_FOREVER("HACKOBJ5")
									bHelpText = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//IF hacking_data.b_hack_connect_game_solved = FALSE
						IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
							IF bHelpText = FALSE
								//RUN HACKCONNECT.EXE
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ3")
									PRINT_HELP_FOREVER("HACKOBJ3")
									bHelpText = TRUE
								ENDIF
							ENDIF
						ELSE
							//IF hacking_data.b_brute_force_open = FALSE
							IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_OPEN)
								IF bHelpText = FALSE
									//RUN BRUTEFORCE.EXE
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ5")
										PRINT_HELP_FOREVER("HACKOBJ5")
										bHelpText = TRUE
									ENDIF
								ENDIF
							ELSE
								IF bHelpText = TRUE
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ5")
										CLEAR_HELP()
										bHelpText = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	        
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF WarningPopup = FALSE
					IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
						IF IS_USING_CURSOR(FRONTEND_CONTROL)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
								//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
								minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
							//PRINTSTRING("*********iClick")PRINTINT(iClick)PRINTNL()
						ENDIF
						ELSE
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
								BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
									//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
									minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
								//PRINTSTRING("*********iClick")PRINTINT(iClick)PRINTNL()
							ENDIF
					ENDIF
				ENDIF
			ENDIF
			ENDIF
			
			IF WarningPopup = FALSE
				IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_BACK")
							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CIRCLE))
							minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
						//PRINTSTRING("*********iClick")PRINTINT(iClick)PRINTNL()
						IF bDoTutorial = TRUE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ5")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ3")
								CLEAR_HELP()
							ENDIF
							bHelpText = FALSE
							iThisHacking --
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF WarningPopup = TRUE
				IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime
					IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime 
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						PRINTSTRING("CLOSING")
						POPUP_MSG(FALSE, "")
				    ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
					PRINTSTRING("@@@@@iClick")PRINTINT(iClick)PRINTNL()
				ENDIF
			ENDIF
			
			//IF minigame_return_index != NULL
			
			IF iClick = 80
				IF WarningPopup = FALSE
					PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
					POPUP_MSG(TRUE, "ACCESSD")
				ENDIF
			ENDIF
			//ENDIF
			
			//APP1 - HACK CONNECT
			//IF minigame_return_index != NULL
				
			IF iClick = 82
				IF bJustBruteForce = FALSE
					IF bJustHackConnect = TRUE
						iHackTimer = GET_GAME_TIMER()
						hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
						IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY)	
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"OPEN_APP",TO_FLOAT(0))
						ELSE
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"RUN_PROGRAM",TO_FLOAT(4))  
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"OPEN_APP",TO_FLOAT(0))
						ENDIF
						i_clock_start = GET_GAME_TIMER()
						bStartHackTimer = TRUE
						bHelpText = FALSE
						
						//Get a sound ID reference
						sHacking[0] = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(sHacking[0],"HACKING_COUNTDOWN_IP_FIND")
						iThisHacking ++
					ELSE
						//IF hacking_data.b_hack_connect_game_solved = FALSE
						IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
							iHackTimer = GET_GAME_TIMER()
							hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"OPEN_APP",TO_FLOAT(0))
							i_clock_start = GET_GAME_TIMER()
							bStartHackTimer = TRUE
							bHelpText = FALSE

							//Get a sound ID reference
							sHacking[0] = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(sHacking[0],"HACKING_COUNTDOWN_IP_FIND")
							iThisHacking ++
						ELSE
							IF WarningPopup = TRUE
								IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime
									IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime 
									OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
										PRINTSTRING("CLOSING")
										POPUP_MSG(FALSE, "")
								    ENDIF
								ENDIF
							ENDIF
							
							IF WarningPopup = FALSE
								PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
								IF bDoDownloadOpen AND IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED) //hacking_data.b_brute_force_game_solved = TRUE
									POPUP_MSG(TRUE, "TRYDL")
								ELSE
									POPUP_MSG(TRUE, "TRYBRUTE")
									CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - TRYBRUTE 0" )
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE

					IF WarningPopup = TRUE
						IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime
							IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
								PRINTSTRING("CLOSING")
								POPUP_MSG(FALSE, "")
						    ENDIF
						ENDIF
					ENDIF
					
					IF WarningPopup = FALSE
						PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
						IF bDoDownloadOpen AND IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED) //hacking_data.b_brute_force_game_solved = TRUE
							POPUP_MSG(TRUE, "TRYDL")
						ELSE
							POPUP_MSG(TRUE, "TRYBRUTE")
							CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - TRYBRUTE 1" )
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//ENDIF
			
			//APP2 - BRUTEFORCE
			//IF minigame_return_index != NULL
				
					//iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
			IF iClick = 83
				IF bJustHackConnect = FALSE //TRUE
					IF bJustBruteForce = TRUE
						iHackTimer = GET_GAME_TIMER()
						hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",TO_FLOAT(iStartingSpeed))
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",0,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",1,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",2,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",3,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",4,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",5,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",6,hacking_data.f_speed)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",7,hacking_data.f_speed)
						IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY)	
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"OPEN_APP",TO_FLOAT(1))
						ELSE
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"RUN_PROGRAM",TO_FLOAT(4))  
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"OPEN_APP",TO_FLOAT(1))
						ENDIF
						i_clock_start = GET_GAME_TIMER()
						bStartHackTimer = TRUE
						bHelpText = FALSE
						
						sHacking[1] = GET_SOUND_ID()
						IF bRandomPassword
							RANDOMISE_PASSWORD(hacking_data, bUseNewPasswords, bDugganPassword)
						ENDIF
						CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING(sfHacking,"SET_ROULETTE_WORD",hacking_data.sPassword)
						PLAY_SOUND_FRONTEND(sHacking[1],"HACKING_COUNTDOWN_CRACK_PASS")
						CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - PLAY_SOUND_FRONTEND - HACKING_COUNTDOWN_CRACK_PASS 1" )
						iThisHacking = 6
					ELSE	
						//IF hacking_data.b_hack_connect_game_solved = TRUE
						IF (NOT bHackConnectBeforeBrute OR IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)) //hacking_data.b_hack_connect_game_solved = TRUE)
						AND NOT IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED) //hacking_data.b_brute_force_game_solved = FALSE
							iHackTimer = GET_GAME_TIMER()
							hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_SPEED",TO_FLOAT(iStartingSpeed))
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",0,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",1,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",2,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",3,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",4,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",5,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",6,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_COLUMN_SPEED",7,hacking_data.f_speed)
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"OPEN_APP",TO_FLOAT(1))
							i_clock_start = GET_GAME_TIMER()
							bStartHackTimer = TRUE
							bHelpText = FALSE
							
							sHacking[1] = GET_SOUND_ID()
							IF bRandomPassword
								RANDOMISE_PASSWORD(hacking_data, bUseNewPasswords, bDugganPassword)
							ENDIF
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ5")
								CLEAR_HELP()
							ENDIF
							
							CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING(sfHacking,"SET_ROULETTE_WORD",hacking_data.sPassword)
							PLAY_SOUND_FRONTEND(sHacking[1],"HACKING_COUNTDOWN_CRACK_PASS")
							CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - PLAY_SOUND_FRONTEND - HACKING_COUNTDOWN_CRACK_PASS 2" )
							iThisHacking = 6
						ELSE
							IF WarningPopup = TRUE
								IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime 	
									IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime
									OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
										PRINTSTRING("CLOSING")
										POPUP_MSG(FALSE, "")
								    ENDIF
								ENDIF
							ENDIF
							
							IF WarningPopup = FALSE
								PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
								IF bDoDownloadOpen AND IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)//hacking_data.b_hack_connect_game_solved = TRUE
									POPUP_MSG(TRUE, "TRYDL")
								ELSE
									POPUP_MSG(TRUE, "TRYHACK")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF WarningPopup = TRUE
						IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime 
							IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
								PRINTSTRING("CLOSING")
								POPUP_MSG(FALSE, "")
						    ENDIF
						ENDIF
					ENDIF
					IF WarningPopup = FALSE
						PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
						IF bDoDownloadOpen AND IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)//AND hacking_data.b_hack_connect_game_solved = TRUE
							POPUP_MSG(TRUE, "TRYDL")
						ELSE
							POPUP_MSG(TRUE, "TRYHACK")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoDownloadOpen
				IF IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)//hacking_data.b_download_icon_displayed
					IF iClick = 93
						IF IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED) //hacking_data.b_hack_connect_game_solved
						AND IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED) //hacking_data.b_brute_force_game_solved
							IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_DOWNLOAD_OPEN) //hacking_data.b_download_open
								// the actual opening of the window will be done by the script
								//hacking_data.b_download_open = TRUE
								SET_BIT(hacking_data.bsHacking,BS_DOWNLOAD_OPEN)
								iThisHacking = 8
							ENDIF
						ELSE
							IF WarningPopup = TRUE
								IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpMinTime 	
									IF GET_GAME_TIMER()-iErrorPopupTimer > cPopUpTime
									OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
										PRINTSTRING("CLOSING")
										POPUP_MSG(FALSE, "")
								    ENDIF
								ENDIF
							ENDIF
							
							IF WarningPopup = FALSE
								PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
								//IF hacking_data.b_hack_connect_game_solved = TRUE
								IF IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
									POPUP_MSG(TRUE, "TRYBRUTE")
									CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - TRYBRUTE 2" )
								ELSE
									POPUP_MSG(TRUE, "TRYHACK")
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//ENDIF
		
		BREAK
		
		//MANAGE IP CONNECT
		CASE 5
			IF bDoTutorial = TRUE
				IF bHelpText = FALSE
					PRINT_HELP("H_USE_PC6")
					bHelpText = TRUE
				ELSE
					IF bDoMoreHelpText = TRUE
						PRINT_HELP("H_USE_PC7")
						bDoMoreHelpText = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
					IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							hacking_data.i_lives = iStartLives
							CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"CLOSE_APP")
							IF sHacking[1] != -1 
								IF NOT HAS_SOUND_FINISHED(sHacking[1])
									STOP_SOUND(sHacking[1])
									sHacking[1] = -1 
								ENDIF
							ENDIF
							
							IF sHacking[0] != -1 
							IF NOT HAS_SOUND_FINISHED(sHacking[0])
								STOP_SOUND(sHacking[0])
									sHacking[0] = -1 
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY) 
								iThisHacking = 4
							ELSE
								SET_BIT(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
							ENDIF
							
		//					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_BACK")
		//					//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CIRCLE))
		//					minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF MANAGE_HACKING_TIMER(iHackTimer, 2000)
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
						IF IS_USING_CURSOR(FRONTEND_CONTROL)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
							minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
						ENDIF
						ELSE
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
								BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
								//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
								minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
							ENDIF
					ENDIF
					ENDIF
					
					IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
						iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
					ENDIF
				ENDIF
				
				//LOSE A LIFE
				IF iClick = 85
					PRINTSTRING("************************************YOU HAVE THIS MANY LIVES:")PRINTINT(hacking_data.i_lives)PRINTNL()
					IF hacking_data.i_lives > 1
						PRINTSTRING("*******************Lost a Life******************YOU HAVE THIS MANY LIVES:")PRINTINT(hacking_data.i_lives)PRINTNL()
						hacking_data.i_lives --
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(hacking_data.i_lives))
						PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
					ELSE
						IF NOT HAS_SOUND_FINISHED(sHacking[0])
							STOP_SOUND(sHacking[0])
						ENDIF
						//hacking_data.b_MP_Hack_fail = TRUE
						SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
						PRINTSTRING("*******************Dead******************")PRINTNL()
						bStartHackTimer = FALSE
						bDoMoreHelpText = TRUE
						PLAY_SOUND_FRONTEND(-1,"HACKING_FAILURE")
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"SET_IP_OUTCOME",0 ,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"LOSTIP")
					ENDIF
				ENDIF
				
				IF iClick = 90
					hacking_data.i_lives = iStartLives
					PRINTSTRING("*******************RETRY****************\n")
					PRINTINT(iStartLives)
					PRINTNL()
					CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
					hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
					i_clock_start = GET_GAME_TIMER()
					sHacking[0] = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(sHacking[0],"HACKING_COUNTDOWN_IP_FIND")
					//hacking_data.b_MP_Hack_fail = TRUE
					SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
					bStartHackTimer = TRUE
				ENDIF
				
				//RETRY - NO
				//IF minigame_return_index != NULL
				IF iClick = 91
					hacking_data.i_lives = iStartLives
					CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"CLOSE_APP")
					
					IF sHacking[0] != -1 
					IF NOT HAS_SOUND_FINISHED(sHacking[0])
						STOP_SOUND(sHacking[0])
							sHacking[0] = -1 
					ENDIF
					ENDIF
					//hacking_data.b_MP_Hack_fail = TRUE
					SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
					iThisHacking = 4
				ENDIF
				//ENDIF
				
				//HAS THE PLAYER RUN OUT OF TIME
				IF bStartHackTimer = TRUE
					rawTime = 60000 - (GET_GAME_TIMER() - i_clock_start)
					IF rawTime < 1
						//hacking_data.b_MP_Hack_fail = TRUE
						SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
						hacking_data.i_lives = iStartLives
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
						hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
						i_clock_start = GET_GAME_TIMER()
						bStartHackTimer = FALSE
						bDoMoreHelpText = TRUE
						PLAY_SOUND_FRONTEND(-1,"HACKING_FAILURE")
						IF sHacking[0] != -1 
						IF NOT HAS_SOUND_FINISHED(sHacking[0])
							STOP_SOUND(sHacking[0])
								sHacking[0] = -1 
						ENDIF
						ENDIF
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"SET_IP_OUTCOME",0 ,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"LOSTIP")
					ENDIF
				ENDIF

				//WIN
				IF iClick = 84
					CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"SET_IP_OUTCOME",1 ,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"WINIP")
					iHackTimer = GET_GAME_TIMER()
					//hacking_data.b_hack_connect_game_solved = TRUE
					SET_BIT(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
					bStartHackTimer = FALSE
					bHelpText = FALSE
					PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_GOOD")
					IF sHacking[0] != -1 
					IF NOT HAS_SOUND_FINISHED(sHacking[0])
						STOP_SOUND(sHacking[0])
							sHacking[0] = -1 
					ENDIF
					ENDIF
					iThisHacking = 7
				ENDIF
		BREAK
		
		//MANAGE BRUTEFORCE
		CASE 6
		
			IF bDoTutorial = TRUE
				IF bHelpText = FALSE
					PRINT_HELP("H_USE_PC8")
					bHelpText = TRUE
				ELSE
					IF bDoMoreHelpText = TRUE
						PRINT_HELP("H_USE_PC9")
						bDoMoreHelpText = FALSE
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
						IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
								hacking_data.i_lives = iStartLives
								CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"CLOSE_APP")
								IF NOT HAS_SOUND_FINISHED(sHacking[0])
									STOP_SOUND(sHacking[0])
								ENDIF
								IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY) 
									iThisHacking = 4
								ELSE
									SET_BIT(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
								ENDIF
							ENDIF
						ELSE
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
							OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) AND bDugganPassword) //Limiting this to the ULP version to be safe
								hacking_data.i_lives = iStartLives
								CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"CLOSE_APP")
								IF NOT HAS_SOUND_FINISHED(sHacking[0])
									STOP_SOUND(sHacking[0])
								ENDIF
								IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY) 
									iThisHacking = 4
								ELSE
									SET_BIT(hacking_data.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF

		   		IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
						//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
						minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
					ENDIF
					ELSE
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_INPUT_EVENT_SELECT")
							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
							minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
						ENDIF
				ENDIF
				ENDIF
				
				IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
					iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index)
					PRINTSTRING("*********************iClick")PRINTINT(iClick)PRINTNL()
				ENDIF
			ENDIF
				
				//ROULETTE SUCCESS
				IF iClick = 86
					PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_GOOD")
				ENDIF
				
				IF iClick = 92
					PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK")
				ENDIF
				
				//IF hacking_data.bGotColumnWrong
				IF IS_BIT_SET(hacking_data.bsHacking,BS_GOT_COLUMN_WRONG)
					IF MANAGE_HACKING_TIMER(hacking_data.i_BF_Timer,500)
						CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"RESET_ROULETTE")
						CLEAR_BIT(hacking_data.bsHacking,BS_GOT_COLUMN_WRONG)
						//hacking_data.bGotColumnWrong = FALSE
					ENDIF
				ENDIF
	
				//LOSE A LIFE
				IF iClick = 87
					IF hacking_data.i_lives > 1
						PLAY_SOUND_FRONTEND(-1,"HACKING_CLICK_BAD")
						CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"STOP_ROULETTE")
						hacking_data.i_BF_Timer = GET_GAME_TIMER()
						//hacking_data.bGotColumnWrong = TRUE
						SET_BIT(hacking_data.bsHacking,BS_GOT_COLUMN_WRONG)
						SET_BIT(hacking_data.bsHacking,BS_BRUTE_FORCE_INCREASE_FAIL_TELEMETRY)
						PRINTSTRING("[HACKING]hacking_data.bsHacking,BS_BRUTE_FORCE_INCREASE_FAIL_TELEMETRY SET")
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							hacking_data.i_lives --
							PRINTSTRING("*********************Lose a life")PRINTINT(iClick)PRINTNL()
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(hacking_data.i_lives))
						ENDIF
					ELSE
						//hacking_data.b_MP_Hack_fail = TRUE
						SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
						SET_BIT(hacking_data.bsHacking,BS_BRUTE_FORCE_INCREASE_FAIL_TELEMETRY)
						PLAY_SOUND_FRONTEND(-1,"HACKING_FAILURE")
						hacking_data.i_lives = iStartLives
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
						hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
						i_clock_start = GET_GAME_TIMER()
						IF sHacking[1] != -1 
						IF NOT HAS_SOUND_FINISHED(sHacking[1])
							STOP_SOUND(sHacking[1])
								sHacking[1] = -1 
						ENDIF
						ENDIF
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							PRINTSTRING("*********************Lost all lives")PRINTINT(iClick)PRINTNL()
							CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"SET_ROULETTE_OUTCOME",0 ,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"LOSEBRUTE")
						ENDIF
					ENDIF
				ENDIF

				//HAS THE PLAYER RUN OUT OF TIME
				IF bStartHackTimer = TRUE
					rawTime = 60000 - (GET_GAME_TIMER() - i_clock_start)
					IF rawTime < 1
						hacking_data.i_lives = iStartLives
						//hacking_data.b_MP_Hack_fail = TRUE
						SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
						hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
						i_clock_start = GET_GAME_TIMER()
						bStartHackTimer = FALSE
						bDoMoreHelpText = TRUE
						PLAY_SOUND_FRONTEND(-1,"HACKING_FAILURE")
						CLEAR_HELP()
						IF sHacking[1] != -1 
						IF NOT HAS_SOUND_FINISHED(sHacking[1])
							STOP_SOUND(sHacking[1])
								sHacking[1] = -1 
						ENDIF
						ENDIF
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"SET_ROULETTE_OUTCOME",0 ,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"LOSEBRUTE")
					ENDIF
				ENDIF
				
				//RETRY - YES
				IF iClick = 88
					hacking_data.i_lives = iStartLives
					//hacking_data.b_MP_Hack_fail = TRUE
					SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
					CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sfHacking,"SET_LIVES",TO_FLOAT(iStartLives))
					hacking_data.f_speed = TO_FLOAT(iStartingSpeed)
					i_clock_start = GET_GAME_TIMER()
					sHacking[1] = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(sHacking[1],"HACKING_COUNTDOWN_CRACK_PASS")
					bStartHackTimer = TRUE
				ENDIF
				//ENDIF
				
				//RETRY - NO
				IF iClick = 89
					hacking_data.i_lives = iStartLives
					//hacking_data.b_MP_Hack_fail = TRUE
					SET_BIT(hacking_data.bsHacking,BS_MP_HACK_FAIL)
					CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"CLOSE_APP")
					IF sHacking[1] != -1 
					IF NOT HAS_SOUND_FINISHED(sHacking[1])
						STOP_SOUND(sHacking[1])
							sHacking[1] = -1 
					ENDIF
					ENDIF
					iThisHacking = 4
				ENDIF
				
				//WIN
				IF iClick = 86
					CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sfHacking,"SET_ROULETTE_OUTCOME",1 ,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,INVALID_SCALEFORM_PARAM,"WINBRUTE")
					iHackTimer = GET_GAME_TIMER()
					//hacking_data.b_brute_force_game_solved = TRUE
					SET_BIT(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
					IF bUseNewSound
						PLAY_SOUND_FRONTEND(-1,"Hack_Success","DLC_GR_Steal_Railguns_Sounds")
						CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - PLAY_SOUND_FRONTEND - Hack_Success" )
					ELSE
						PLAY_SOUND_FRONTEND(-1,"HACKING_SUCCESS")
						CPRINTLN( DEBUG_MIKE, "HACK MINIGAME() - PLAY_SOUND_FRONTEND - HACKING_SUCCESS" )
					ENDIF
					IF sHacking[1] != -1 
					IF NOT HAS_SOUND_FINISHED(sHacking[1])
						STOP_SOUND(sHacking[1])
							sHacking[1] = -1 
					ENDIF
					ENDIF
					iThisHacking = 7
				ENDIF
			//ENDIF
		
		BREAK
		
		//CHECKS IF THE PLAYER IS SUCCESSFUL
		CASE 7
			IF bDoTutorial = TRUE
//				IF hacking_data.b_brute_force_game_solved = TRUE
//				AND hacking_data.b_hack_connect_game_solved = TRUE
				IF IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
				AND IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
					IF bDownAndOutHelp = FALSE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ7")
							PRINT_HELP_FOREVER("HACKOBJ7")
							bDownAndOutHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_USE_PC6")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_USE_PC7")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_USE_PC8")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_USE_PC9")
					CLEAR_HELP(TRUE)
				ENDIF
			ENDIF
//			
//			IF hacking_data.b_brute_force_game_solved = TRUE
//			AND hacking_data.b_hack_connect_game_solved = TRUE
//			AND (NOT bDoDownloadOpen OR (bDoDownloadOpen AND hacking_data.b_download_open))
			IF IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
			AND IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
			AND (NOT bDoDownloadOpen OR (bDoDownloadOpen AND IS_BIT_SET(hacking_data.bsHacking,BS_DOWNLOAD_OPEN) ))
			AND (NOT bDoMapOpen OR (bDoMapOpen AND IS_BIT_SET(hacking_data.iBS2Hacking, BS2_MAP_SHOWN)))
			
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HACKOBJ7")
					CLEAR_HELP(TRUE)
				ENDIF
				IF NOT bPlaySuccessBeep 
					PLAY_SOUND_FROM_ENTITY(-1,"unlocked_bleep",PLAYER_PED_ID(),"HACKING_DOOR_UNLOCK_SOUNDS")
					bPlaySuccessBeep = TRUE
				ENDIF
				IF MANAGE_HACKING_TIMER(iHackTimer, 3000)
					CLEAR_BIT(hacking_data.bsHacking,BS_IS_HACKING)
					SET_BIT(hacking_data.bsHacking,BS_HACKING_SUCCESSFUL)
					//hacking_data.b_is_hacking = FALSE
					//hacking_data.b_hacking_successful = TRUE
					FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(FALSE)
				ENDIF
			ELIF bDoMapOpen
			AND IS_BIT_SET(hacking_data.bsHacking, BS_BRUTE_FORCE_GAME_SOLVED)
				IF NOT IS_BIT_SET(hacking_data.iBS2Hacking, BS2_MAP_SHOWN)
					PRINTLN("RENDER_PC - Setting BS2_MAP_SHOWN")
					SET_BIT(hacking_data.iBS2Hacking, BS2_MAP_SHOWN)
					CALL_SCALEFORM_MOVIE_METHOD(sfHacking, "SHOW_MAP")
				ENDIF
				
				IF MANAGE_HACKING_TIMER(iHackTimer, 7000)
					CLEAR_BIT(hacking_data.bsHacking, BS_IS_HACKING)
					SET_BIT(hacking_data.bsHacking, BS_HACKING_SUCCESSFUL)
					FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(FALSE)
				ENDIF
				
			ELSE
				// Display the download app icon if needed and only after the hack connect has been established
				IF bDoDownloadOpen
				AND NOT IS_BIT_SET(hacking_data.bsHacking,BS_DOWNLOAD_ICON_DISPLAYED)
				AND IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
				AND IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
//				AND NOT hacking_data.b_download_icon_displayed
//				AND hacking_data.b_brute_force_game_solved = TRUE
//				AND hacking_data.b_hack_connect_game_solved = TRUE
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "OPEN_DOWNLOAD")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					END_SCALEFORM_MOVIE_METHOD()
					SET_BIT(hacking_data.bsHacking,BS_DOWNLOAD_ICON_DISPLAYED)
					//hacking_data.b_download_icon_displayed = TRUE
				ENDIF
			
				IF MANAGE_HACKING_TIMER(iHackTimer, 2000)	
					CALL_SCALEFORM_MOVIE_METHOD(sfHacking,"CLOSE_APP")
					iThisHacking = 4
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 8
			
			RUN_NEW_MINIGAME(hacking_data)
		
		BREAK
	
	
	ENDSWITCH

ENDPROC


/// RUN_HACKING_MINIGAME (pass in your own hacking data struct)
/// NOTES: run every frame, initalise local hacking data struct and pass this through. 
/// PURPOSE:
///    Runs necessary hacking scripts, launched through IS_PED_HACKING_AT_TERMINAL or by setting .b_is_hacking to TRUE
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No returns
///  
PROC RUN_HACKING_MINIGAME(S_HACKING_DATA &hacking_data, BOOL bDoTutorial = FALSE, BOOL bJustHackConnect = FALSE, BOOL bJustBruteForce = FALSE, BOOL bDoDownloadOpen = FALSE #IF USE_TU_CHANGES , BOOL bMP = TRUE #ENDIF )
	//IF hacking_data.b_is_hacking = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_IS_HACKING)
		RENDER_PC(hacking_data,5,5,100,10,8,10,bDoTutorial,bJustHackConnect,bJustBruteForce, bDoDownloadOpen, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT #IF USE_TU_CHANGES , bMP #ENDIF )
	ENDIF
ENDPROC



//INT iMaxLives = 5
//INT iStartLives = 5
//INT iStartingSpeed = 10

/// RUN_HACKING_MINIGAME (pass in your own hacking data struct)
/// NOTES: run every frame, initalise local hacking data struct and pass this through. 
/// PURPOSE:
///    Runs necessary hacking scripts, launched through IS_PED_HACKING_AT_TERMINAL or by setting .b_is_hacking to TRUE
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No returns
///  
PROC RUN_HACKING_MINIGAME_WITH_PARAMETERS(S_HACKING_DATA &hacking_data, INT iStartLives = 5, INT iMaxLives = 5, INT iStartingSpeed = 10, INT iSpeedDecayRate = 10, INT iBruteForceColumns = 8, INT iChanceOfRedHerrings = 10,BOOL bDoTutorial = FALSE, BOOL bJustHackConnect = FALSE, BOOL bJustBruteForce = FALSE,  BOOL bRandomPassword = TRUE, BOOL bHackConnectBeforeBrute = TRUE, BOOL bExitButton = FALSE, BOOL bDoDownloadOpen = FALSE, INT iTotalTime = 60000 , BOOL bMP = TRUE , BOOL bSetInstructionalButtonsUnderHud = TRUE, BOOL bUseNewPasswords = FALSE, BOOL bUseNewSound = FALSE, BOOL bHideMyComputerIcon = FALSE, BOOL bShowMap = FALSE, BOOL bDugganPassword = FALSE)
	//IF hacking_data.b_is_hacking = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking, BS_IS_HACKING)
		RENDER_PC(hacking_data,iStartLives,iMaxLives,iStartingSpeed,iSpeedDecayRate,iBruteForceColumns,iChanceOfRedHerrings,bDoTutorial,bJustHackConnect,bJustBruteForce,bRandomPassword, bHackConnectBeforeBrute,bExitButton, bDoDownloadOpen,WALL_FBI,iTotalTime , bMP , bSetInstructionalButtonsUnderHud, bUseNewPasswords, bUseNewSound, bHideMyComputerIcon, bShowMap, bDugganPassword)
	ENDIF
ENDPROC

PROC GENERATE_HACKING_DATA_FROM_PLAYER_LEVEL(S_HACKING_DATA &hacking_data,INT iHackingLevel = 0, INT iTerminalLevel = 0,BOOL bDoTutorial = FALSE,BOOL bJustHackConnect = FALSE,BOOL bHackConnectBeforeBrute = FALSE,BOOL bRandomPassword = TRUE, BOOL bJustBruteForce = FALSE, BOOL bExitButton = FALSE)
	//defaults for now
	INT iStartLives = 5
	INT iMaxLives = 5 
	INT iStartingSpeed = 10
	INT iSpeedDecayRate = 10
	INT iBruteForceColumns = 8
	INT iChanceOfRedHerrings = 10
	
	iStartLives = ROUND((iHackingLevel / 10)-(iTerminalLevel / 10))
	IF iStartLives > 7
		iStartLives = 7
	ELSE
		IF iStartLives < 1
			iStartLives = 1
		ENDIF
	ENDIF
	
	RUN_HACKING_MINIGAME_WITH_PARAMETERS(hacking_data,iStartLives,iMaxLives,iStartingSpeed,iSpeedDecayRate,iBruteForceColumns,iChanceOfRedHerrings,bDoTutorial,bJustHackConnect,bJustBruteForce, bRandomPassword,bHackConnectBeforeBrute,bExitButton)
ENDPROC

/// RUN_HACKING_MINIGAME_BASED_ON_PLAYER_SKILL (pass in your own hacking data struct)
/// NOTES: run every frame, initalise local hacking data struct and pass this through. 
/// PURPOSE:
///    Runs necessary hacking scripts, launched through IS_PED_HACKING_AT_TERMINAL or by setting .b_is_hacking to TRUE
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No returns
///  
PROC RUN_HACKING_MINIGAME_BASED_ON_PLAYER_SKILL(S_HACKING_DATA &hacking_data, INT iHackingLevel, INT iTerminalLevel,BOOL bDoTutorial = FALSE, BOOL bJustHackConnect = FALSE, BOOL bJustBruteForce = FALSE) 
	//IF hacking_data.b_is_hacking = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_IS_HACKING)
		//PC_CHECKS(hacking_data)
		GENERATE_HACKING_DATA_FROM_PLAYER_LEVEL(hacking_data,iHackingLevel, iTerminalLevel,bDoTutorial,bJustHackConnect,bJustBruteForce)
	ENDIF
ENDPROC


/// PURPOSE:
///    Determines whether the player is at a hacking terminal
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
///    VECTOR v_terminal - the vector of the terminal
///    FLOAT f_heading - the heading of the terminal
///    ECOMPASS e_compass - the compass heading of the terminal 
///    FLOAT f_locate_size - locate size of terminal
///    EACTIVATIONEASE - possible points of activation
/// RETURNS:
///    True if the player is activating the terminal
FUNC BOOL IS_PED_AT_HACKING_TERMINAL(S_HACKING_DATA &hacking_data ,VECTOR v_terminal,FLOAT f_terminal_heading,ECOMPASS e_compass,FLOAT f_locate_size = 0.4, BOOL bAllowPlayerDamage = FALSE,EACTIVATIONEASE eEase =EACTIVATIONEASE_MEDIUM)
	
	VECTOR v_temp

	IF IS_PLAYER_ACTIVATING_COORD(v_terminal,f_terminal_heading, e_compass, f_locate_size, EACTIVESIDE_ONEWAY,eEase)
	//OR hacking_data.bForceStartHacking
	OR IS_BIT_SET(hacking_data.bsHacking,BS_FORCE_START_HACKING)
		//IF hacking_data.b_hacking_successful = FALSE
		IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_HACKING_SUCCESSFUL)
			//sfHacking = REQUEST_SCALEFORM_MOVIE("hacking_pc")
			IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
				IF NOT IS_BIT_SET(hacking_data.bsHacking,BS_IS_HACKING)
				//IF hacking_data.b_is_hacking = FALSE
					IF bAllowPlayerDamage = TRUE
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE)
					ELSE
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ENDIF
					
//					IF hacking_data.bForceStartHacking=TRUE
//						hacking_data.bForceStartHacking=FALSE
//					ENDIF
					IF IS_BIT_SET(hacking_data.bsHacking,BS_FORCE_START_HACKING)
						CLEAR_BIT(hacking_data.bsHacking,BS_FORCE_START_HACKING)
					ENDIF
					
					DISABLE_CELLPHONE(TRUE)
					
					REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
					IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("HACK", MINIGAME_TEXT_SLOT)
						WAIT(0)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						v_temp = GET_GAMEPLAY_CAM_COORD()				
						cam_temp = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA",v_temp, GET_GAMEPLAY_CAM_ROT(), 1.0, TRUE )
						SET_CAM_ACTIVE(cam_temp,TRUE)
						SET_CAM_FAR_CLIP(cam_temp,15)
						SET_CAM_NEAR_CLIP(cam_temp,15)
						SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),FALSE)
						//hacking_data.b_is_hacking = TRUE
						SET_BIT(hacking_data.bsHacking,BS_IS_HACKING)
					ENDIF
				ENDIF
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC

//HAS_PLAYER_SUCCESSFULLY_HACKED_TERMINAL
//check to see if player has hacked terminal

FUNC BOOL HAS_PLAYER_SUCCESSFULLY_HACKED_TERMINAL(S_HACKING_DATA &hacking_data)
	
	//IF hacking_data.b_brute_force_game_solved = TRUE AND hacking_data.b_hack_connect_game_solved = TRUE AND hacking_data.b_hacking_successful = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
	AND IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
	AND IS_BIT_SET(hacking_data.bsHacking,BS_HACKING_SUCCESSFUL)
		RETURN TRUE	
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC



///MANAGE LIVES - manages the rendering of lives in the game screen.
///PRIVATE FUNCTION DON'T USE IN SCRIPTS
///     
///
PROC MANAGE_LIVES(S_HACKING_DATA &hacking_data)
	//RENDER LIVES
	IF hacking_data.i_lives = 3
		DRAW_RECT(0.8710, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
	ENDIF
	
	IF hacking_data.i_lives = 2
		DRAW_RECT(0.8710, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
		DRAW_RECT(0.8590, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
	ENDIF
	
	IF hacking_data.i_lives = 1
		DRAW_RECT(0.8710, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
		DRAW_RECT(0.8590, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
		DRAW_RECT(0.8510, 0.1500, 0.0100, 0.0540, 0, 0, 0, 255)
	ENDIF
	
	IF hacking_data.i_lives = 0
		DRAW_RECT(0.8390, 0.1480, 0.0100, 0.0540, 0, 0, 0, 255)
		DRAW_RECT(0.8710, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
		DRAW_RECT(0.8590, 0.1410, 0.0100, 0.0540, 0, 0, 0, 255)
		DRAW_RECT(0.8510, 0.1500, 0.0100, 0.0540, 0, 0, 0, 255)
	ENDIF
ENDPROC

/// DISPLAY_TIMER - displays timer for use in mini games
///PRIVATE FUNCTION DON'T USE IN SCRIPTS
///     
///
PROC DISPLAY_TIMER(FLOAT DisplayAtX, FLOAT DisplayAtY, structTimer tThisTimer)
	INT timer_in_ms		= ROUND(GET_TIMER_IN_SECONDS(tThisTimer)*1000.0)
	
    INT iMinutes        = (timer_in_ms/1000)/60
    INT iSeconds        = (timer_in_ms-(iMinutes*60*1000))/1000
    INT iMiliseconds    =  timer_in_ms-((iSeconds+(iMinutes*60))*1000)

	iMinutes = i_Minute_Val - iMinutes
	iSeconds = i_Second_Val - iSeconds
	iMiliseconds = i_MiliSecond_Val - iMiliseconds
	
	TEXT_LABEL str = "PS_TIMEH_"
	
	IF iMinutes > 0
		str += "M0"
	ENDIF
	
	IF iSeconds > 0
		str += "S0"
	ELSE
		str += "S1"
	ENDIF
	
    IF iMiliseconds < 10
        str += "M2"
    ELIF iMiliseconds < 100
        str += "M1"
	ELSE
        str += "M0"
    ENDIF
	
	//TURN RED AT 10 SECONDS LEFT
	IF iSeconds < 10
		SET_TEXT_SCALE(0.7500, 0.7500)
		SET_TEXT_COLOUR(200, 0, 0, 255)
	ELSE
		SET_TEXT_SCALE(0.7500, 0.7500)
		SET_TEXT_COLOUR(0, 200, 0, 255)
	ENDIF
	
	IF iMinutes > 0
		//DISPLAY_TEXT_WITH_3_NUMBERS(DisplayAtX, DisplayAtY, str, iMinutes, iSeconds, iMiliseconds)
		DISPLAY_TEXT_WITH_2_NUMBERS(DisplayAtX, DisplayAtY, str, iSeconds, iMiliseconds)
	ELSE
		DISPLAY_TEXT_WITH_2_NUMBERS(DisplayAtX, DisplayAtY, str, iSeconds, iMiliseconds)
	ENDIF
ENDPROC

/// IS_MOUSE_IN_SCREEN_AREA - checks if the mouse pointer is in a certain area of the screen defined by the top left point and the bottom right. 
///PRIVATE FUNCTION DON'T USE IN SCRIPTS
///     
///
FUNC BOOL IS_MOUSE_IN_SCREEN_AREA(S_HACKING_DATA &hacking_data,VECTOR vTopLCorner,VECTOR vBottomRCorner)
	IF (hacking_data.f_mouse_x >= vTopLCorner.x AND hacking_data.f_mouse_x <= vBottomRCorner.x) AND (hacking_data.f_mouse_y >= vTopLCorner.y AND hacking_data.f_mouse_y <= vBottomRCorner.y)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// HAS_ANALOGUE_STICK_BEEN_RESET- checks to see if analogue stick has been moved back to centre position
///PRIVATE FUNCTION DON'T USE IN SCRIPTS
///     
///

PROC HAS_ANALOGUE_STICK_BEEN_RESET(S_HACKING_DATA &hacking_data)

	INT i_left_x,i_left_y,i_right_x,i_right_y
	//GET_POSITION_OF_ANALOGUE_STICKS(PLAYER_CONTROL,i_left_x,i_left_y,i_right_x,i_right_y)
	i_left_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
	i_left_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128
	i_right_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
	i_right_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128
	
	
//	IF hacking_data.b_hack_connect_open = TRUE 
//	OR hacking_data.b_brute_force_open = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_OPEN)
	OR IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_OPEN)
		
		IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_INCREMENTED_X)
			IF i_right_x = 0
				CLEAR_BIT(hacking_data.bsHacking,BS_NODE_INCREMENTED_X)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_DECREMENTED_X)
			IF i_right_x = 0
				CLEAR_BIT(hacking_data.bsHacking,BS_NODE_DECREMENTED_X)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_INCREMENTED_Y)
			IF i_right_y = 0
				CLEAR_BIT(hacking_data.bsHacking,BS_NODE_INCREMENTED_Y)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_DECREMENTED_Y)
			IF i_right_y = 0
				CLEAR_BIT(hacking_data.bsHacking,BS_NODE_DECREMENTED_Y)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_INCREMENTED_X)
		IF i_left_x = 0
			CLEAR_BIT(hacking_data.bsHacking,BS_NODE_INCREMENTED_X)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_DECREMENTED_X)
		IF i_left_x = 0
			CLEAR_BIT(hacking_data.bsHacking,BS_NODE_DECREMENTED_X)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_INCREMENTED_Y)
		IF i_left_y = 0
			CLEAR_BIT(hacking_data.bsHacking,BS_NODE_INCREMENTED_Y)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(hacking_data.bsHacking,BS_NODE_DECREMENTED_Y)
		IF i_right_y = 0
			CLEAR_BIT(hacking_data.bsHacking,BS_NODE_DECREMENTED_Y)
		ENDIF
	ENDIF
	

ENDPROC

/// GET_TEXT_FROM_INT - checks if the mouse pointer is in a certain area of the screen defined by the top left point and the bottom right. 
///PRIVATE FUNCTION DON'T USE IN SCRIPTS
///     
///
FUNC STRING GET_TEXT_FROM_INT(INT number)
	STRING text = "0"

	SWITCH number
		CASE 1
			text = "1"
		BREAK
		
		CASE 2
			text = "2"
		BREAK
		
		CASE 3
			text = "3"
		BREAK
		
		CASE 4
			text = "4"
		BREAK
		
		CASE 5
			text = "5"
		BREAK
		
		CASE 6
			text = "6"
		BREAK
		
		CASE 7
			text = "7"
		BREAK
		
		CASE 8
			text = "8"
		BREAK
		
		CASE 9
			text = "9"
		BREAK
	ENDSWITCH
	
	RETURN text
ENDFUNC


/// FORCE_QUIT_FAIL_HACKING_MINIGAME (pass in your own hacking data struct)
/// NOTES: Force the termination of the hacking minigame and cause the player to fail.
/// PURPOSE:
///   Force the termination of the hacking minigame and cause the player to fail.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No Returns
///  
PROC FORCE_QUIT_FAIL_HACKING_MINIGAME(S_HACKING_DATA &hacking_data)
//	SET_SPRITES_DRAW_BEFORE_FADE(FALSE)
//	SET_SCRIPT_GFX_DRAW_ORDER(FALSE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
	ENDIF
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	CLEAR_HELP()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	DESTROY_CAM(cam_temp)
	CLEAR_BIT(hacking_data.bsHacking,BS_IS_HACKING)
	CLEAR_BIT(hacking_data.bsHacking,BS_HACKING_SUCCESSFUL)
	//hacking_data.b_is_hacking = FALSE
	//hacking_data.b_hacking_successful = FALSE
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(FALSE)
	//RESET_HACKING_DATA(hacking_data)
ENDPROC


/// FORCE_QUIT_PASS_HACKING_MINIGAME (pass in your own hacking data struct)
/// NOTES: Force the termination of the hacking minigame and cause the player to pass.
/// PURPOSE:
///   Force the termination of the hacking minigame and cause the player to pass.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No Returns
///  
PROC FORCE_QUIT_PASS_HACKING_MINIGAME(S_HACKING_DATA &hacking_data, BOOL bRestoreControl = TRUE, BOOL bRestoreWeaponVisibility = TRUE)
	//hacking_data.b_is_hacking = FALSE
	//hacking_data.b_hacking_successful = TRUE
	CLEAR_BIT(hacking_data.bsHacking,BS_IS_HACKING)
	SET_BIT(hacking_data.bsHacking,BS_HACKING_SUCCESSFUL)
	//SET_SPRITES_DRAW_BEFORE_FADE(FALSE)
	//SET_SCRIPT_GFX_DRAW_ORDER(FALSE)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	IF bRestoreWeaponVisibility
		IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) // 2601506
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
		ENDIF
	ENDIF
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	IF bRestoreControl
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		CPRINTLN(DEBUG_MISSION, "FORCE_QUIT_PASS_HACKING_MINIGAME - SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)")
	ENDIF
	DESTROY_CAM(cam_temp)
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	FORCE_POSTFX_BULLET_IMPACTS_AFTER_HUD(FALSE)
	RESET_HACKING_DATA(hacking_data)
	
	#IF IS_NEXTGEN_BUILD
		// DISABLE MULTIPLE DISPLAYS/BLINDERS
		SET_MULTIHEAD_SAFE(FALSE, TRUE)
		PRINTLN( "Hacking mini game RENDER_PC()Disable SET_MULTIHEAD_SAFE")
	#ENDIF
	
ENDPROC
/// HAS_PLAYER_FAILED_HACKING (pass in your own hacking data struct)
/// NOTES: checks if the player has failed.
/// PURPOSE:
///   checks if the player has failed.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
///    bAndForceQuit - pass TRUE to terminate hacking on fail.
/// RETURNS:
///  TRUE on the player losing all thier lives
///  
FUNC BOOL HAS_PLAYER_FAILED_HACKING(S_HACKING_DATA &hacking_data,BOOL bAndForceQuit = FALSE)

	IF hacking_data.i_lives < 1
		IF bAndForceQuit 
			FORCE_QUIT_FAIL_HACKING_MINIGAME(hacking_data)
			
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
RETURN FALSE
ENDFUNC
FUNC BOOL HAS_PLAYER_FAILED_HACKING_MP(S_HACKING_DATA &hacking_data)

	//IF hacking_data.b_MP_Hack_fail
	IF IS_BIT_SET(hacking_data.bsHacking,BS_MP_HACK_FAIL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// HAS_PLAYER_BEAT_BRUTEFORCE (pass in your own hacking data struct)
/// NOTES: checks if a player has beaten Bruteforce
/// PURPOSE:
///   checks if a player has beaten Bruteforce
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
///    bAndForceQuit - pass TRUE to terminate hacking on success
/// RETURNS:
///  TRUE on player beating bruceforce
///  
FUNC BOOL HAS_PLAYER_BEAT_BRUTEFORCE(S_HACKING_DATA &hacking_data,BOOL bAndForceQuit = FALSE)

	//IF hacking_data.b_brute_force_game_solved = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_BRUTE_FORCE_GAME_SOLVED)
		IF bAndForceQuit 
			FORCE_QUIT_PASS_HACKING_MINIGAME(hacking_data)
			
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_VIEWED_HACKING_MAP(S_HACKING_DATA &hacking_data, BOOL bAndForceQuit = FALSE)

	IF IS_BIT_SET(hacking_data.iBS2Hacking, BS2_MAP_SHOWN)
		IF bAndForceQuit 
			FORCE_QUIT_PASS_HACKING_MINIGAME(hacking_data)
			
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// HAS_PLAYER_BEAT_BRUTEFORCE (pass in your own hacking data struct)
/// NOTES: checks if a player has beaten Bruteforce
/// PURPOSE:
///   checks if a player has beaten Bruteforce
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
///    bAndForceQuit - pass TRUE to terminate hacking on success
/// RETURNS:
///  TRUE on player beating bruceforce
///  
FUNC BOOL HAS_PLAYER_BEAT_HACK_CONNECT(S_HACKING_DATA &hacking_data,BOOL bAndForceQuit = FALSE)

	//IF hacking_data.b_hack_connect_game_solved = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_HACK_CONNECT_GAME_SOLVED)
		IF bAndForceQuit 
			FORCE_QUIT_PASS_HACKING_MINIGAME(hacking_data)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks to see if the download window has been opened
FUNC BOOL HAS_DOWNLOAD_WINDOW_BEEN_OPENED(S_HACKING_DATA &hacking_data, BOOL bAndForceQuit = FALSE)
	
	//IF hacking_data.b_download_open = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_DOWNLOAD_OPEN)
		IF bAndForceQuit
			FORCE_QUIT_PASS_HACKING_MINIGAME(hacking_data)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// HAS_PLAYER_BEAT_NEW_HACKING (pass in your own hacking data struct)
/// NOTES: checks if a player has beaten the new hacking mini game
/// PURPOSE:
///   checks if a player has beaten the new hacking mini game
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
///    bAndForceQuit - pass TRUE to terminate hacking on success
/// RETURNS:
///  TRUE on player beating new hacking
///  
FUNC BOOL HAS_PLAYER_BEAT_NEW_HACKING(S_HACKING_DATA &hacking_data,BOOL bAndForceQuit = FALSE)

	//IF hacking_data.b_new_hacking_game_solved = TRUE
	IF IS_BIT_SET(hacking_data.bsHacking,BS_NEW_HACKING_GAME_SOLVED)
		IF bAndForceQuit 
			FORCE_QUIT_PASS_HACKING_MINIGAME(hacking_data)
			
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC





















// ################################# CIRCUIT HACKING MINIGAME - START ################################# //



/// PURPOSE:
///    Handles making the debug spew text scroll upwards when all array positions are full and a new value needs to be inserted
///    Shifts all array position values 1 closer to array position 0, deleting the currrent values in position 0
///    [Position 1] -> [Position 0], ..., [Position 14] -> [Position 13]
///    Makes room for a new value to be added to the last position in the array (arrayPos[14])
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
///    i_ValueA		-	INT: New value to add to last position in Bit range A array
///    i_ValueB		-	INT: New value to add to last position in Bit range B array
PROC SHIFT_AND_STORE_DEBUG_OUTPUT_SPEW_BIT_RANGE_VALUES(S_HACKING_DATA &hackingData, INT i_ValueA, INT i_ValueB)
	INT i_DebugSpewArrayPosition
	
	FOR i_DebugSpewArrayPosition = 0 TO 13
		IF i_DebugSpewArrayPosition < 9
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], (i_DebugSpewArrayPosition * 3), ((i_DebugSpewArrayPosition * 3) + 2), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], ((i_DebugSpewArrayPosition + 1) * 3), (((i_DebugSpewArrayPosition + 1) * 3) + 2)))
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], (i_DebugSpewArrayPosition * 3), ((i_DebugSpewArrayPosition * 3) + 2), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], ((i_DebugSpewArrayPosition + 1) * 3), (((i_DebugSpewArrayPosition + 1) * 3) + 2)))
		
		ELIF i_DebugSpewArrayPosition = 9
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], (i_DebugSpewArrayPosition * 3), ((i_DebugSpewArrayPosition * 3) + 2), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 0, 2))
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], (i_DebugSpewArrayPosition * 3), ((i_DebugSpewArrayPosition * 3) + 2), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 0, 2))
		
		ELSE
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], ((i_DebugSpewArrayPosition - 10) * 3), (((i_DebugSpewArrayPosition - 10) * 3) + 2), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], (((i_DebugSpewArrayPosition + 1) - 10) * 3), ((((i_DebugSpewArrayPosition + 1) - 10) * 3) + 2)))
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], ((i_DebugSpewArrayPosition - 10) * 3), (((i_DebugSpewArrayPosition - 10) * 3) + 2), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], (((i_DebugSpewArrayPosition + 1) - 10) * 3), ((((i_DebugSpewArrayPosition + 1) - 10) * 3) + 2)))
		ENDIF
	ENDFOR
	
	
	// NEW VALUES -> 14
	SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 12, 14, i_ValueA)
	SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 12, 14, i_ValueB)
ENDPROC



/// PURPOSE:
///    Stores the randomly generated values passed to the bit ranges
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
///    i_CurrentBit	-	INT: Current string
///    i_BitRangeA	-	INT: Bit range A
///    i_BitRangeB	-	INT: Bit range B
PROC STORE_DEBUG_OUTPUT_SPEW_BIT_RANGE_VALUE(S_HACKING_DATA &hackingData, INT i_CurrentBit, INT i_BitRangeA, INT i_BitRangeB)
	SWITCH i_CurrentBit
		CASE 0
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 0, 2, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 0, 2, i_BitRangeB)
		BREAK
		
		CASE 1
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 3, 5, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 3, 5, i_BitRangeB)
		BREAK
		
		CASE 2
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 6, 8, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 6, 8, i_BitRangeB)
		BREAK
		
		CASE 3
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 9, 11, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 9, 11, i_BitRangeB)
		BREAK
		
		CASE 4
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 12, 14, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 12, 14, i_BitRangeB)
		BREAK
		
		CASE 5
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 15, 17, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 15, 17, i_BitRangeB)
		BREAK
		
		CASE 6
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 18, 20, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 18, 20, i_BitRangeB)
		BREAK
		
		CASE 7
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 21, 23, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 21, 23, i_BitRangeB)
		BREAK
		
		CASE 8
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 24, 26, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 24, 26, i_BitRangeB)
		BREAK
		
		CASE 9
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], 27, 29, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], 27, 29, i_BitRangeB)
		BREAK
		
		CASE 10
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 0, 2, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 0, 2, i_BitRangeB)
		BREAK
		
		CASE 11
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 3, 5, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 3, 5, i_BitRangeB)
		BREAK
		
		CASE 12
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 6, 8, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 6, 8, i_BitRangeB)
		BREAK
		
		CASE 13
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 9, 11, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 9, 11, i_BitRangeB)
		BREAK
		
		CASE 14
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], 12, 14, i_BitRangeA)
			SET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], 12, 14, i_BitRangeB)
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Uses two int values stored in bit ranges to randomly determine the contents of the string to be output
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
///    i_CurrentBit	-	INT: The current possition in the string array (on screen print)
///    i_ContentA	-	INT: Value from a bit range used as first random number when picking the string
///    i_ContentB	-	INT: Value from a bit range used as second random number when picking the string
/// RETURNS:
///    STRING: The value determined by the two content values passed
FUNC STRING GET_DEBUG_OUTPUT_SPEW_STRING_CONTENTS(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels, INT i_CurrentBit, INT i_ContentA, INT i_ContentB, bool bHeist2)
	// OUTPUT SPEW STRING CONTENTS
	TEXT_LABEL_31 str_String
	str_String = ""
	
	IF i_CurrentBit < iHackSeconds
	OR IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_RECYCLE_DEBUG_OUTPUT)
		SWITCH i_ContentA
			// fx0
			CASE 0
				str_String = "fx"
				
				SWITCH i_ContentB
					CASE 0
						str_String += "437265616d706965"
					BREAK
			
					CASE 1
						str_String += "43756e744675636b"
					BREAK
				
					CASE 2
						str_String += "42756c6c73686974"
					BREAK
					
					CASE 3
						str_String += "416e616c536f6170"
					BREAK
					
					CASE 4
						str_String += "436c69746f726973"
					BREAK
					
					CASE 5
						str_String += "0000436865657365"
					BREAK
					
					DEFAULT
						str_String = "MEMORY LEAK: Error 1"
					BREAK
				ENDSWITCH
			BREAK
			
			// ERROR CODE: 0x
			CASE 1
				str_String = "ERROR CODE: 0x"
				
				SWITCH i_ContentB
					CASE 0
						str_String += "4675636b"
					BREAK
			
					CASE 1
						str_String += "436f636b"
					BREAK
					
					CASE 2
						str_String += "53686974"
					BREAK
					
					CASE 3
						str_String += "53544453"
					BREAK
					
					CASE 4
						str_String += "41727365"
					BREAK
					
					CASE 5
						str_String += "54697473"
					BREAK
					
					DEFAULT
						str_String = "MEMORY LEAK: Error 2"
					BREAK
				ENDSWITCH
			BREAK
			
			// BLANK SPACE
			CASE 2
				str_String = ""
			BREAK
			
			// CALL: Override_
			CASE 3
				str_String = "CALL: Override_"
				
				SWITCH i_ContentB
					CASE 0
						str_String += "A"
					BREAK
			
					CASE 1
						str_String += "B"
					BREAK
					
					CASE 2
						str_String += "C"
					BREAK
					
					CASE 3
						str_String += "D"
					BREAK
					
					CASE 4
						str_String += "E"
					BREAK
					
					CASE 5
						str_String += "F"
					BREAK
					
					DEFAULT
						str_String = "MEMORY LEAK: Error 3"
					BREAK
				ENDSWITCH
			BREAK
			
			// DETECTED:
			CASE 4
				str_String = "DETECTED: "
				
				SWITCH i_ContentB
					CASE 0
						str_String += "Hack"
					BREAK
			
					CASE 1
						str_String += "Tunnel"
					BREAK
					
					CASE 2
						str_String += "Breach"
					BREAK
					
					CASE 3
						str_String += "Lag"
					BREAK
					
					CASE 4
						str_String += "Load"
					BREAK
					
					CASE 5
						str_String += "Unauthorized User"
					BREAK
					
					DEFAULT
						str_String = "MEMORY LEAK: Error 4"
					BREAK
				ENDSWITCH
			BREAK
			
			// RELAUNCHING:
			CASE 5
				str_String = "RELAUNCHING: "
				
				SWITCH i_ContentB
					CASE 0
						str_String += "Firewall"
					BREAK
			
					CASE 1
						str_String += "Proxy"
					BREAK
					
					CASE 2
						str_String += "Antivirus"
					BREAK
					
					CASE 3
						str_String += "Security"
					BREAK
					
					CASE 4
						str_String += "Log Capture"
					BREAK
					
					CASE 5
						str_String += "Failed"
					BREAK
					
					DEFAULT
						str_String = "MEMORY LEAK: Error 5"
					BREAK
				ENDSWITCH
			BREAK
			
			// WARNING:
			CASE 6
				str_String = "WARNING: "
				
				SWITCH i_ContentB
					CASE 0
						str_String += "Memory Leak"
					BREAK
			
					CASE 1
						str_String += "Latency Increase"
					BREAK
					
					CASE 2
						IF b_UseDifficultLevels
							IF bHeist2
								str_String += "Accessing server"
							ELSE
								str_String += "Accessing Fleeca Vault"
							ENDIF
						ELSE
							str_String += "Searching DIR for Porn"
						ENDIF
					BREAK
					
					CASE 3
						str_String += "Additional User Found"
					BREAK
					
					CASE 4
						str_String += "Protocol Changed"
					BREAK
					
					CASE 5
						str_String += "Port 69 Unavailable"
					BREAK
					
					DEFAULT
						str_String = "MEMORY LEAK: Error 6"
					BREAK
				ENDSWITCH
			BREAK
			
			DEFAULT
				str_String = "MEMORY LEAK: Fatal Error"
			BREAK
		ENDSWITCH
	ENDIF
	
	
	// RETURN TEXTLABEL CONTENTS AS STRING TO BE DISLPAYED
	RETURN GET_STRING_FROM_TEXTLABEL(str_String)
ENDFUNC



/// PURPOSE:
///    Starts the timer used to determine when to print debug output
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
PROC START_DEBUG_OUTPUT_SPEW_TIMER(S_HACKING_DATA &hackingData)
	RESTART_TIMER_NOW(hackingData.t_PhoneLaunchTimer)
ENDPROC



/// PURPOSE:
///    Stops the timer used to determine when to print debug output
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
///    b_Restart	-	BOOL: Pass TRUE to reset the timer to 0.0
PROC STOP_DEBUG_OUTPUT_SPEW_TIMER(S_HACKING_DATA &hackingData, BOOL b_Restart = TRUE)
	IF IS_TIMER_STARTED(hackingData.t_PhoneLaunchTimer)
		IF b_Restart
			RESTART_TIMER_NOW(hackingData.t_PhoneLaunchTimer)
		ENDIF
		
		PAUSE_TIMER(hackingData.t_PhoneLaunchTimer)
	ENDIF
ENDPROC



/// PURPOSE:
///    Draws the debug output to the left of the screen
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
PROC DRAW_DEBUG_OUTPUT_SPEW(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels, BOOL bHeist2)
	// RECYCLED VALUES FROM ADAM'S MINIGAMES
		// i_DebugOutputSpewIterator	=>	iHackSeconds
		// i_RandomDebugOutputWaitTime	=>	iHackMilliseconds
		// t_DebugOutputSpewTimer		=>	hackingData.t_PhoneLaunchTimer
	
	
	// UPDATE SPEW TIMER
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_WAIT_TIME_PICKED)
		iHackMilliseconds = GET_RANDOM_INT_IN_RANGE(0, 6)
		SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_WAIT_TIME_PICKED)
	ENDIF
	
	
	// UPDATE DEBUG SPEW
	IF IS_TIMER_STARTED(hackingData.t_PhoneLaunchTimer)
	AND NOT IS_TIMER_PAUSED(hackingData.t_PhoneLaunchTimer)
		IF GET_TIMER_IN_SECONDS_SAFE(hackingData.t_PhoneLaunchTimer) >= iHackMilliseconds * 0.7
			INT i_BitSetA = GET_RANDOM_INT_IN_RANGE(0, 7)
			INT i_BitSetB = GET_RANDOM_INT_IN_RANGE(0, 6)
			
			
			// IF FINAL ARRAY POSITION
			IF iHackSeconds > 14
				iHackSeconds = 15
				
				// SHIFT BITS SO CAN ADD A NEW VALUE IN LAST ARRAY POSITION - DELETES FIRST POSITION VALUE
				SHIFT_AND_STORE_DEBUG_OUTPUT_SPEW_BIT_RANGE_VALUES(hackingData, i_BitSetA, i_BitSetB)
				
				IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_RECYCLE_DEBUG_OUTPUT)
					SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_RECYCLE_DEBUG_OUTPUT)
				ENDIF
				
			ELSE
				// ADD NEW BIT VALUES TO BIT RANGES WHILE NOT REACHED FINAL ARRAY POSITION
				STORE_DEBUG_OUTPUT_SPEW_BIT_RANGE_VALUE(hackingData, iHackSeconds, i_BitSetA, i_BitSetB)
			
				iHackSeconds++
			ENDIF
			
			
			// ALLOW NEW WAIT TIME TO BE PICKED
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_WAIT_TIME_PICKED)
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_WAIT_TIME_PICKED)
			ENDIF
			
			RESTART_TIMER_NOW(hackingData.t_PhoneLaunchTimer)
		ENDIF
	ENDIF
	
	
	// DRAW OUTPUT VARIABLES
	TEXT_LABEL_31 str_DebugOutputString
	FLOAT f_DebugOutputSpewX = 0.035
	FLOAT f_DebugOutputSpewY = 0.30
	FLOAT f_DebugOutputSpewSpacing = 0.018
	INT i_DebugOutputSpewAlpha = 70
	
	
	// CHANGE ALPHA FOR WHEN SCALEFROM IS ON SCREEN - TO LOOK LIKE SCALEFORM IS DRAWN OVER THE SPEW
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
		i_DebugOutputSpewAlpha = 20
	ENDIF
	
	
	// DRAW DEBUG SPEW
	INT i_DebugSpewArrayPosition = 0
	
	
	// BIT SET ZERO - ONE
	FOR i_DebugSpewArrayPosition = 0 TO 9
		SET_TEXT_COLOUR(255, 255, 255, i_DebugOutputSpewAlpha)
		SET_TEXT_SCALE(0.200, 0.275)
		SET_TEXT_WRAP(0.000, 1.000)
		
		str_DebugOutputString = GET_DEBUG_OUTPUT_SPEW_STRING_CONTENTS(hackingData, b_UseDifficultLevels, i_DebugSpewArrayPosition, GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[0], (i_DebugSpewArrayPosition * 3), ((i_DebugSpewArrayPosition * 3) + 2)), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[1], (i_DebugSpewArrayPosition * 3), ((i_DebugSpewArrayPosition * 3) + 2)), bHeist2)
		
		DISPLAY_TEXT_WITH_PLAYER_NAME((0.5 - ((0.5 - f_DebugOutputSpewX) * hackingData.f_AspectRatioModifier)), f_DebugOutputSpewY + (f_DebugOutputSpewSpacing * i_DebugSpewArrayPosition), "STRING", str_DebugOutputString, HUD_COLOUR_WHITE)
		
		str_DebugOutputString = ""
	ENDFOR
	
	
	// BIT SET TWO - THREE
	FOR i_DebugSpewArrayPosition = 10 TO 14
		SET_TEXT_COLOUR(255, 255, 255, i_DebugOutputSpewAlpha)
		SET_TEXT_SCALE(0.200, 0.275)
		SET_TEXT_WRAP(0.000, 1.000)
		
		str_DebugOutputString = GET_DEBUG_OUTPUT_SPEW_STRING_CONTENTS(hackingData, b_UseDifficultLevels, i_DebugSpewArrayPosition, GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[2], ((i_DebugSpewArrayPosition - 10) * 3), (((i_DebugSpewArrayPosition - 10) * 3) + 2)), GET_BITS_IN_RANGE(hackingData.i_DebugOutputBitRange[3], ((i_DebugSpewArrayPosition - 10) * 3), (((i_DebugSpewArrayPosition - 10) * 3) + 2)), bHeist2)
		
		DISPLAY_TEXT_WITH_PLAYER_NAME((0.5 - ((0.5 - f_DebugOutputSpewX) * hackingData.f_AspectRatioModifier)), f_DebugOutputSpewY + (f_DebugOutputSpewSpacing * i_DebugSpewArrayPosition), "STRING", str_DebugOutputString, HUD_COLOUR_WHITE)
		
		str_DebugOutputString = ""
	ENDFOR
ENDPROC



/// PURPOSE:
///    Clears the bit range values used to generate the string output
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data
PROC CLEAR_DEBUG_OUTPUT_SPEW(S_HACKING_DATA &hackingData)
	STOP_DEBUG_OUTPUT_SPEW_TIMER(hackingData)
	
	iHackSeconds = 0
	iHackMilliseconds = 0
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_WAIT_TIME_PICKED)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_WAIT_TIME_PICKED)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_RECYCLE_DEBUG_OUTPUT)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_RECYCLE_DEBUG_OUTPUT)
	ENDIF
	
				
	// RESET ALL DEBUG SPEW BIT RANGES
	hackingData.i_DebugOutputBitRange[0] = 0
	hackingData.i_DebugOutputBitRange[1] = 0
	hackingData.i_DebugOutputBitRange[2] = 0
	hackingData.i_DebugOutputBitRange[3] = 0
ENDPROC



/// PURPOSE:
///    Draws the Circuit Hacking Minigame scaleform movie this frame
PROC DRAW_CIRCUIT_HACKING_SCALEFORM()
	IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
		//CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DRAW_CIRCUIT_HACKING_SCALEFORM() - CHECK 1 -  Draw Scaleform")
		
		// url:bugstar:2168936
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfHacking, 100, 100, 100, 255)
	ENDIF
ENDPROC



/// PURPOSE:
///    Unloads the scalefrom movie used by the Circuit Hacking Minigame
PROC CLEANUP_CIRCUIT_HACKING_SCALEFORM()
	IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CLEANUP_CIRCUIT_HACKING_SCALEFORM() - CHECK 1 - Clean Up Scaleform")
		
		// RESET SCALEFORM
		IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
			BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			END_SCALEFORM_MOVIE_METHOD()
			
			PRINTLN("[TMS] CLEANUP_CIRCUIT_HACKING_SCALEFORM - Scaleform should be cleaned up now")
		ENDIF
		
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfHacking)
		sfHacking = NULL
	ELSE
		PRINTLN("[TMS] CLEANUP_CIRCUIT_HACKING_SCALEFORM - inside cleanup function but the scaleform isn't loaded")
	ENDIF
ENDPROC



/// PURPOSE:
///    Compresses two floats into a single float value
/// PARAMS:
///    f_InputX		-	FLOAT: Value 1 to be compressed
///    f_InputY		-	FLOAT: Value 2 to be compressed
///    i_Precision	-	INT: Degree of precision value - hackingData.s_SnakeData.i_CompressPrecisionValue / (4096 LG | 9974 NG) is a good sweet spot
/// RETURNS:
///    FLOAT: Single float value made from compressing the two input float values together
FUNC FLOAT FLOAT_COMPRESSION_PACK(FLOAT f_InputX, FLOAT f_InputY, INT i_Precision)
	FLOAT i_OutputX = TO_FLOAT(FLOOR(f_InputX * (i_Precision - 1)))
	FLOAT i_OutputY = TO_FLOAT(FLOOR(f_InputY * (i_Precision - 1)))
	
	RETURN (i_OutputX * i_Precision) + i_OutputY
ENDFUNC



/// PURPOSE:
///    Decompresses a float variable output by FLOAT_COMPRESSION_PACK
/// PARAMS:
///    f_CompressedInput	-	FLOAT: Value output by FLOAT_COMPRESSION_PACK
///   &f_OutputX			-	FLOAT: A refewrence to a float value - Stores value 1 decompressed from the input value
///   &f_OutputY			-	FLOAT: A refewrence to a float value - Stores value 2 decompressed from the input value
///    i_Precision			-	INT: Degree of precision value - Use the same value used to pack input value - hackingData.s_SnakeData.i_CompressPrecisionValue / (4096 LG | 9974 NG)
PROC FLOAT_COMPRESSION_UNPACK(FLOAT f_CompressedInput, FLOAT &f_OutputX, FLOAT &f_OutputY, INT i_Precision)
	f_OutputX = TO_FLOAT(FLOOR(f_CompressedInput / i_Precision))
	f_OutputY = f_CompressedInput % i_Precision

	f_OutputX = f_OutputX / (i_Precision - 1)
	f_OutputY = f_OutputY / (i_Precision - 1)
ENDPROC



/// PURPOSE:
///    Decompresses and returns the X float variable of the output from FLOAT_COMPRESSION_PACK
/// PARAMS:
///    f_CompressedInput	-	FLOAT: Value output by FLOAT_COMPRESSION_PACK
///    i_Precision			-	INT: Degree of precision value - Use the same value used to pack input value - hackingData.s_SnakeData.i_CompressPrecisionValue / (4096 LG | 9974 NG)
/// RETURNS:
///    FLOAT: X float value used in the original compression
FUNC FLOAT FLOAT_COMPRESSION_UNPACK_RETURN_X(FLOAT f_CompressedInput, INT i_Precision)
	FLOAT f_Output = TO_FLOAT(FLOOR(f_CompressedInput / i_Precision))
	f_Output = f_Output / (i_Precision - 1)
	
	RETURN f_Output
ENDFUNC



/// PURPOSE:
///    Decompresses and returns the Y float variable of the output from FLOAT_COMPRESSION_PACK
/// PARAMS:
///    f_CompressedInput	-	FLOAT: Value output by FLOAT_COMPRESSION_PACK
///    i_Precision			-	INT: Degree of precision value - Use the same value used to pack input value - hackingData.s_SnakeData.i_CompressPrecisionValue / (4096 LG | 9974 NG)
/// RETURNS:
///    FLOAT: Y float value used in the original compression
FUNC FLOAT FLOAT_COMPRESSION_UNPACK_RETURN_Y(FLOAT f_CompressedInput, INT i_Precision)
	FLOAT f_Output = f_CompressedInput % i_Precision
	f_Output = f_Output / (i_Precision - 1)
	
	RETURN f_Output
ENDFUNC



/// PURPOSE:
///    Single command that decides which controller vibration command to use based on whether running LG or NG
/// PARAMS:
///    i_Duration	-	INT: Time to vibrate for in milliseconds
///    i_Frequency	-	INT: Strength of the vibrations
PROC DO_CIRCUIT_HACKING_CONTROLLER_VIBRATIONS(INT i_Duration, INT i_Frequency)
	#IF IS_NEXTGEN_BUILD
		SET_CONTROL_SHAKE(FRONTEND_CONTROL, i_Duration, i_Frequency)
	#ENDIF
	
	#IF NOT IS_NEXTGEN_BUILD
		SET_PAD_SHAKE(PAD1, i_Duration, i_Frequency)
	#ENDIF
ENDPROC



/// PURPOSE:
///    Checks if the hacking player is in an upright vehicle
/// PARAMS:
///   &hackingData	-	STRUCT: Holds all data passed into the header
/// RETURNS:
///    BOOL: TRUE if player is not in a vehicle. TRUE if player is in an upright vehicle. FALSE if player is in upside down vehicle
FUNC BOOL CHECK_IS_CIRCUIT_HACKING_PLAYER_IN_UPRIGHT_VEHICLE(S_HACKING_DATA &hackingData)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX v_TempVehicleIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF GET_ENTITY_UPRIGHT_VALUE(v_TempVehicleIndex) < 0.0
			// RESET PHONE LAUNCH VARIBALES
			RESTART_TIMER_AT(hackingData.t_PhoneLaunchTimer, 0.0)
			PAUSE_TIMER(hackingData.t_PhoneLaunchTimer)
			
			hackingData.e_IntroLaunchState = LIS_INITIALISE
			
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CHECK_IS_CIRCUIT_HACKING_PLAYER_IN_UPRIGHT_VEHICLE() - CHECK 1 - Player not in upright vehicle - Resetting Phone Launch State")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    Handles launching the hacking mini game from the phone
/// PARAMS:
///   &hackingData	-	STRUCT: Holds all data passed into the header
/// RETURNS:
///    ENUM: The cellphone launch state - LS_PASSED, LS_FAILED or LS_WORKING
FUNC LAUNCH_STATE DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO(S_HACKING_DATA &hackingData)
	SWITCH hackingData.e_IntroLaunchState
		// IF WE DON'T WANT A PHONE INTRO OR THE PLAYER IS SPECTATING THEN SKIP IT
		CASE LIS_INITIALISE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
					MP_FORCE_TERMINATE_INTERNET()
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 1a - Initialise - MP_FORCE_TERMINATE_INTERNET_ACTIVE() is FALSE - Calling MP_FORCE_TERMINATE_INTERNET()")
				ENDIF
				
				g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuNextFrame = TRUE
				
			ELSE
				// SP TERMINATE & CLEAR INTERNET APP
				#IF IS_DEBUG_BUILD
					g_bBSWebsiteNoSpaceTrigger = FALSE
				    IF g_sBrowserHelpMessage.bShow = TRUE
				        IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				            IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(g_sBrowserHelpMessage.tl15Tag)
				                CLEAR_HELP(TRUE)
				            ENDIF
				        ENDIF
						
				        g_sBrowserHelpMessage.bShow = FALSE
				    ENDIF
				    
					
				    IF HAS_ADDITIONAL_TEXT_LOADED(SHOP_TEXT_SLOT)
				        CLEAR_ADDITIONAL_TEXT(SHOP_TEXT_SLOT, FALSE)
				    ENDIF
					
					
					SET_BROWSER_OPEN(FALSE)
				    g_BrowserStartState = SBSS_Default
				    g_bBrowserNagScreenState = FALSE
				    g_bBrowserGoToStoreTrigger = FALSE
					
				    MP_FORCE_TERMINATE_INTERNET_CLEAR()
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 1b - Initialise - Calling MP_FORCE_TERMINATE_INTERNET_CLEAR()")
				#ENDIF
			ENDIF
			
			
			#IF IS_NEXTGEN_BUILD
				// ENABLE MULTIPLE DISPLAYS/BLINDERS
				SET_MULTIHEAD_SAFE(TRUE, FALSE)
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 2 - Initialise - Enabled SET_MULTIHEAD_SAFE")
			#ENDIF
			
			
			//IF PHONE IS RUNNING, KILL IT
			IF g_Cellphone.PhoneDS >= PDS_RUNNINGAPP
			OR IS_PHONE_ONSCREEN()
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				
				// EXIT CONTACT SUBMENU
				IF NETWORK_IS_GAME_IN_PROGRESS()
					g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuNextFrame = TRUE
				ENDIF

				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 3 - Initialise - Hang up and put away phone - Moving to Triggered")
				hackingData.e_IntroLaunchState = LIS_TRIGGERED
			
			ELSE
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 4 - Initialise - Moving to Triggered")
				hackingData.e_IntroLaunchState = LIS_TRIGGERED
			ENDIF
		BREAK
		
		
		CASE LIS_TRIGGERED
			// LAUNCH CAMERA APP
			IF NOT IS_PHONE_ONSCREEN()
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 5 - Triggered - Launch App")
				LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
				
				IF IS_TIMER_PAUSED(hackingData.t_PhoneLaunchTimer)
				OR IS_TIMER_STARTED(hackingData.t_PhoneLaunchTimer)
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 6 - Triggered - Reset Timer")
					RESTART_TIMER_AT(hackingData.t_PhoneLaunchTimer, 0.0)
				
				ELSE
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 7 - Triggered - Start Timer")
					START_TIMER_NOW_SAFE(hackingData.t_PhoneLaunchTimer)
				ENDIF
				
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 8 - Triggered - Move to WAIT State")
				hackingData.e_IntroLaunchState = LIS_WAITING_FOR_CELL_FULL_SCREEN
			
			ELSE
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 9 - Triggered - Waiting for phone to be removed from screen")
			ENDIF
		BREAK
		
		
		CASE LIS_WAITING_FOR_CELL_FULL_SCREEN
			// CHECK SCREEN ISN'T FADED OUT
			IF NOT IS_PC_VERSION()
				IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO() - CHECK 10 - Wait - Do Screen Fade in")
					DO_SCREEN_FADE_IN(500)
				ENDIF
			ENDIF
			
			// MAKE SURE SAFE TO USE CELLPHONE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NETWORK_HAS_CONTROL_OF_ENTITY(PLAYER_PED_ID())	
					IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						TASK_USE_MOBILE_PHONE(PLAYER_PED_ID(), TRUE)
						
						// MAY NEED MORE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PC_VERSION()
				IF (GET_TIMER_IN_SECONDS_SAFE(hackingData.t_PhoneLaunchTimer) > 1
				AND (NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()))
					DO_SCREEN_FADE_OUT(400)
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - CHECK 11 - Wait - PC Version Calling Screen Fade Out")
				ENDIF
			ENDIF
			
			
			VECTOR v_Test3dPhonePosVec
			GET_MOBILE_PHONE_POSITION(v_Test3dPhonePosVec)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - CHECK 12 - Wait - phone position: ", v_Test3dPhonePosVec, "	g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden)
			
			
			// IS PHONE FINISHED EXPANDING TO FILL SCREEN
			IF (v_Test3dPhonePosVec.x = 1.5 AND v_Test3dPhonePosVec.z = -17.0)
			OR (IS_PC_VERSION() AND IS_SCREEN_FADED_OUT())
			//OR IS_SCREEN_FADED_OUT()
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				hackingData.e_IntroLaunchState = LIS_INITIALISE
				
				IF IS_PC_VERSION()
					DO_SCREEN_FADE_IN(0)
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - CHECK 13 - Wait - PC Version Calling Screen Fade In")
				ENDIF
				
				RETURN LS_PASSED
				
			ELSE
				// SAFETY BACKUP FOR IF THE PHONE REFUSES TO OPEN UP
				IF NOT IS_PHONE_ONSCREEN()
					IF GET_TIMER_IN_SECONDS_SAFE(hackingData.t_PhoneLaunchTimer) > 2
						// TRY AGAIN
						LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - CHECK 14 - Wait - Phone not appeared on screen, try again")
					ENDIF
					
				ELSE
					IF GET_TIMER_IN_SECONDS_SAFE(hackingData.t_PhoneLaunchTimer) > 6
						// GIVE UP
						HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - CHECK 15 - Wait - Phone not appeared on screen, give up")
						
						hackingData.e_IntroLaunchState = LIS_WAITING_FOR_CELL_FULL_SCREEN
						
						RETURN LS_FAILED
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - CHECK 16 - Returning LS_WORKING")
	RETURN LS_WORKING
ENDFUNC



/// PURPOSE:
///    Cleanup function used by the minigame to cleanup between levels
/// PARAMS:
///   &hackingData				-	STRUCT: Hacking data struct
///    b_ResetRunnnigState		-	BOOL: Pass TRUE to reset the game flow state back to CHMS_RUNNING
///    b_BlockClearDebugSpew	-	BOOL: Pass TRUE to clear all debug output spew
///    b_ResetDeaths			-	BOOL: Pass TRUE to reset the death counter
PROC CIRCUIT_MINIGAME_RESET(S_HACKING_DATA &hackingData, BOOL b_ResetRunnnigState = TRUE, BOOL b_BlockClearDebugSpew = FALSE, BOOL b_ResetDeaths = FALSE, BOOL b_ResetSafeToUsePhone = FALSE)
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CIRCUIT_MINIGAME_RESET() - CHECK 1 - Variable Reset Started")
	
	// RESET ALL NECESSARY BIT FLAGS
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CIRCUIT_MINIGAME_RESET() - CHECK 2 - Clearing MINIGAME_SOLVED Bit")
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ENTRY_COLLISION)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ENTRY_COLLISION)
	ENDIF
	
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
		SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_PASSED)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_PASSED)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_REMOVE_HEAD)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_REMOVE_HEAD)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAWN_SYS_UNLOCKED_BKGROUND)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAWN_SYS_UNLOCKED_BKGROUND)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_UNLOCKED_LOCK)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_UNLOCKED_LOCK)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_START_PHONE_UNLOCK)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_START_PHONE_UNLOCK)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_END_PORT_LIGHTS)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_END_PORT_LIGHTS)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_PASS_AUDIO)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_PASS_AUDIO)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_FAIL_AUDIO)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_FAIL_AUDIO)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DROP_CORE_LIGHTS)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DROP_CORE_LIGHTS)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_Circuitbitset, BIT_SET_CIRCUIT_SCALEFORM_RESET)
		CLEAR_BIT(hackingData.i_Circuitbitset, BIT_SET_CIRCUIT_SCALEFORM_RESET)
	ENDIF
	
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SAFE_TO_USE_PHONE)
	AND b_ResetSafeToUsePhone
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SAFE_TO_USE_PHONE)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CIRCUIT_MINIGAME_RESET() - CHECK 3 - Clearing BIT_SET_CIRCUIT_SAFE_TO_USE_PHONE Bit")
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS)
	ENDIF
#ENDIF
	
	
	// SET PASS TIMER
	hackingData.i_CircuitPassedLevelTimer = GET_GAME_TIMER()
	
	
	// RESET SNAKE VARIABLES
	hackingData.s_SnakeData.i_TailIndex = 0
	hackingData.s_SnakeData.i_Alpha = 255
	
	
	// RESET ALL SNAKE TAIL SEGMENTS
	INT i_Counter
	FOR i_Counter = 0 TO (NUM_MAX_TAIL_RECTS - 1)
		hackingData.s_SnakeData.f_Tail[i_Counter] = 0.0
	ENDFOR
	
	
	// RESET SPARK
	hackingData.s_SnakeData.f_SparkWidth = 0.018
	hackingData.s_SnakeData.i_SparkAlpha = 255
	
	
	// RESET FINAL SCREEN CORE LIGHTS
	hackingData.s_SnakeData.i_CoreLightOneColourA = 255
	hackingData.s_SnakeData.i_CoreLightTwoColourA = 255
	hackingData.s_SnakeData.i_CoreLightThreeColourA = 255
	hackingData.s_SnakeData.i_CoreLightFourColourA = 255
	
	
	// RESET PORT LIGHTS
	hackingData.s_SnakeData.i_StartPortLightColourA = 255
	hackingData.s_SnakeData.i_EndPortLightColourA = 255
	
	
	// NUM TIMES DIED - BORROWED FROM ADAM
	IF hackingData.i_CircuitCurrentLevel >= 3
	AND b_ResetDeaths
		hackingData.i_lives = 0
	ENDIF
	
	
	// CLEAR DEBUG SPEW
	IF NOT b_BlockClearDebugSpew
		CLEAR_DEBUG_OUTPUT_SPEW(hackingData)
	ENDIF
	
	// RESET SCALEFORM
	IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	// RESET GAME BACK TO RUNNING STATE
	IF b_ResetRunnnigState
		hackingData.e_CircuitHackingMinigameState = CHMS_RUNNING
	ENDIF
	
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CIRCUIT_MINIGAME_RESET() - CHECK 4 - Variable Reset Finished")
ENDPROC



/// PURPOSE:
///    Get the screen aspect ratio based on the screen resolution
/// PARAMS:
///   &hackingData				-	STRUCT: Hacking data struct
///    b_PrintResolution		-	BOOL: Pass TRUE to print the current screen resolution to logs
/// RETURNS:
///    FLOAT: the screen width / height
FUNC FLOAT GET_CUSTOM_SCREEN_ASPECT_RATIO(S_HACKING_DATA &hackingData, BOOL b_PrintResolution = FALSE)
	INT i_ScreenX = 0
	INT i_ScreenY = 0
	
	
	// LG - HARD CODED VALUES FROM CODE
	#IF NOT IS_NEXTGEN_BUILD
		IF GET_IS_HIDEF()
			i_ScreenX = 1280
			i_ScreenY = 720
			
		ELSE
			i_ScreenX = 720
			i_ScreenY = 480
		ENDIF
	#ENDIF
	
	
	// NG
	#IF IS_NEXTGEN_BUILD
		GET_ACTUAL_SCREEN_RESOLUTION(i_ScreenX, i_ScreenY)
	#ENDIF
	
	
	IF b_PrintResolution
		// LG
		#IF NOT IS_NEXTGEN_BUILD
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: GET_CUSTOM_SCREEN_ASPECT_RATIO() - CHECK 1a - Last Gen Screen Resolution: ", i_ScreenX, "x", i_ScreenY)
		#ENDIF
		
		// NG
		#IF IS_NEXTGEN_BUILD
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: GET_CUSTOM_SCREEN_ASPECT_RATIO() - CHECK 1b - Next Gen Screen Resolution: ", i_ScreenX, "x", i_ScreenY)
		#ENDIF
	ENDIF
	
	
	// CHECK IF SHOULD DISPLAY DEBUG OUTPUT SPEW - SCREEN SIZE CHECK - STOPS CLIPPING OF DEBUG SPEW
	IF i_ScreenY < 600
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: GET_CUSTOM_SCREEN_ASPECT_RATIO() - CHECK 2 - Screen Resolution: ", i_ScreenX, "x", i_ScreenY, " - Blocking Debug Output Spew")
		ENDIF
		
	ELSE
		IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
			CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: GET_CUSTOM_SCREEN_ASPECT_RATIO() - CHECK 3 - Screen Resolution: ", i_ScreenX, "x", i_ScreenY, " - Unblocking Debug Output Spew")
		ENDIF
	ENDIF
	
	
	FLOAT f_Aspect = TO_FLOAT(i_ScreenX) / TO_FLOAT(i_ScreenY)
	
	
	// FIX FOR EYEFINITY TRIPLE MONITOR - STOPS SPRITES BEING SQUASHED
	IF IS_PC_VERSION()
		IF f_Aspect > 4.0
			f_Aspect = f_Aspect / 3
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: GET_CUSTOM_SCREEN_ASPECT_RATIO() - CHECK 4 - Multiple Screen Resolutions Detected - Setting Aspect Ratio from: ", (TO_FLOAT(i_ScreenX) / TO_FLOAT(i_ScreenY)), "    To:", f_Aspect)
		ENDIF
	ENDIF
	
	
	RETURN f_Aspect
ENDFUNC



/// PURPOSE:
///    Calculates the modifer value used for the width of sprites based on the current aspect ratio and 
///    the default aspect modifier value
/// PARAMS:
///    f_CurrentAspectModifier	-	FLOAT: The current aspect ratio value.
/// RETURNS:
///    FLOAT: The value the sprite needs to be multiplied by to keep the correct aspect ratio. Default / current aspect value
FUNC FLOAT CALCULATE_CURRENT_ASPECT_RATIO_MODIFIER(FLOAT f_CurrentAspectModifier)
	RETURN DEFAULT_ASPECT_RATIO_MODIFIER / f_CurrentAspectModifier
ENDFUNC



/// PURPOSE:
///    Updates the current aspect value and the aspect modifier value
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct holding all variables
PROC SET_CURRENT_ASPECT_RATIO_MODIFIER(S_HACKING_DATA &hackingData)
	hackingData.f_CurrentAspectRatio = GET_CUSTOM_SCREEN_ASPECT_RATIO(hackingData)
	
	hackingData.f_AspectRatioModifier = CALCULATE_CURRENT_ASPECT_RATIO_MODIFIER(hackingData.f_CurrentAspectRatio)
ENDPROC



/// PURPOSE:
///    Initialises all data needed for a level. ie Wall collision, starting point, start/end ports...
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct holding all variables
///    i_Level		-	INT: The level that will have its data loaded
PROC INITIALISE_CIRCUIT_MINIGAME_LEVEL(S_HACKING_DATA &hackingData, INT i_Level)
	// LOAD ALL LEVEL DATA
	SWITCH i_Level
		CASE 0
			// HEADER - Level: 1

			hackingData.s_SnakeData.f_StartX			= 0.1740
			hackingData.s_SnakeData.f_StartY			= 0.7390
			hackingData.s_SnakeData.f_CenterX			= hackingData.s_SnakeData.f_StartX
			hackingData.s_SnakeData.f_CenterY			= hackingData.s_SnakeData.f_StartY

			hackingData.s_SnakeData.i_StartDirection	= 3
			hackingData.s_SnakeData.i_Direction			= hackingData.s_SnakeData.i_StartDirection
			hackingData.s_SnakeData.f_SpeedX			= 0.0880
			hackingData.s_SnakeData.f_SpeedY			= hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio

			hackingData.s_SnakeData.f_EndX				= 0.8300
			hackingData.s_SnakeData.f_EndY				= 0.2320
			hackingData.s_SnakeData.f_EndWidth			= 0.0260
			hackingData.s_SnakeData.f_EndHeight			= 0.0700


			// WALLS
			hackingData.s_WallData[0].f_CenterX			= 0.1573
			hackingData.s_WallData[0].f_CenterY			= 0.5000
			hackingData.s_WallData[0].f_Width			= 0.0070
			hackingData.s_WallData[0].f_Height 			= 0.6960

			hackingData.s_WallData[1].f_CenterX			= 0.8423
			hackingData.s_WallData[1].f_CenterY			= 0.5000
			hackingData.s_WallData[1].f_Width			= 0.0070
			hackingData.s_WallData[1].f_Height 			= 0.6960

			hackingData.s_WallData[2].f_CenterX			= 0.5003
			hackingData.s_WallData[2].f_CenterY			= 0.1480
			hackingData.s_WallData[2].f_Width			= 0.6905
			hackingData.s_WallData[2].f_Height 			= 0.0100

			hackingData.s_WallData[3].f_CenterX			= 0.5005
			hackingData.s_WallData[3].f_CenterY			= 0.8530
			hackingData.s_WallData[3].f_Width			= 0.6910
			hackingData.s_WallData[3].f_Height 			= 0.0100

			hackingData.s_WallData[4].f_CenterX			= 0.1710
			hackingData.s_WallData[4].f_CenterY			= 0.7390
			hackingData.s_WallData[4].f_Width			= 0.0270
			hackingData.s_WallData[4].f_Height 			= 0.0780

			hackingData.s_WallData[5].f_CenterX			= 0.2438
			hackingData.s_WallData[5].f_CenterY			= 0.3675
			hackingData.s_WallData[5].f_Width			= 0.1230
			hackingData.s_WallData[5].f_Height 			= 0.4285

			hackingData.s_WallData[6].f_CenterX			= 0.3511
			hackingData.s_WallData[6].f_CenterY			= 0.3157
			hackingData.s_WallData[6].f_Width			= 0.0590
			hackingData.s_WallData[6].f_Height 			= 0.3200

			hackingData.s_WallData[7].f_CenterX			= 0.4125
			hackingData.s_WallData[7].f_CenterY			= 0.2648
			hackingData.s_WallData[7].f_Width			= 0.0308
			hackingData.s_WallData[7].f_Height 			= 0.2235

			hackingData.s_WallData[8].f_CenterX			= 0.4607
			hackingData.s_WallData[8].f_CenterY			= 0.2645
			hackingData.s_WallData[8].f_Width			= 0.0316
			hackingData.s_WallData[8].f_Height 			= 0.2240

			hackingData.s_WallData[9].f_CenterX			= 0.5079
			hackingData.s_WallData[9].f_CenterY			= 0.2645
			hackingData.s_WallData[9].f_Width			= 0.0300
			hackingData.s_WallData[9].f_Height 			= 0.2240

			hackingData.s_WallData[10].f_CenterX		= 0.5611
			hackingData.s_WallData[10].f_CenterY		= 0.2294
			hackingData.s_WallData[10].f_Width			= 0.0443
			hackingData.s_WallData[10].f_Height 		= 0.1520

			hackingData.s_WallData[11].f_CenterX		= 0.6210
			hackingData.s_WallData[11].f_CenterY		= 0.2294
			hackingData.s_WallData[11].f_Width			= 0.0443
			hackingData.s_WallData[11].f_Height 		= 0.1520

			hackingData.s_WallData[12].f_CenterX		= 0.6947
			hackingData.s_WallData[12].f_CenterY		= 0.2031
			hackingData.s_WallData[12].f_Width			= 0.0670
			hackingData.s_WallData[12].f_Height 		= 0.1000

			hackingData.s_WallData[13].f_CenterX		= 0.3678
			hackingData.s_WallData[13].f_CenterY		= 0.7750
			hackingData.s_WallData[13].f_Width			= 0.1280
			hackingData.s_WallData[13].f_Height 		= 0.1480

			hackingData.s_WallData[14].f_CenterX		= 0.5102
			hackingData.s_WallData[14].f_CenterY		= 0.6458
			hackingData.s_WallData[14].f_Width			= 0.1162
			hackingData.s_WallData[14].f_Height 		= 0.4180

			hackingData.s_WallData[15].f_CenterX		= 0.6453
			hackingData.s_WallData[15].f_CenterY		= 0.6630
			hackingData.s_WallData[15].f_Width			= 0.1151
			hackingData.s_WallData[15].f_Height 		= 0.3700

			hackingData.s_WallData[16].f_CenterX		= 0.7790
			hackingData.s_WallData[16].f_CenterY		= 0.6630
			hackingData.s_WallData[16].f_Width			= 0.1140
			hackingData.s_WallData[16].f_Height 		= 0.3700

			hackingData.s_WallData[17].f_CenterX		= 0.3711
			hackingData.s_WallData[17].f_CenterY		= 0.5917
			hackingData.s_WallData[17].f_Width			= 0.0532
			hackingData.s_WallData[17].f_Height 		= 0.0920

			hackingData.s_WallData[18].f_CenterX		= 0.7130
			hackingData.s_WallData[18].f_CenterY		= 0.4130
			hackingData.s_WallData[18].f_Width			= 0.2550
			hackingData.s_WallData[18].f_Height 		= 0.0700

			hackingData.s_WallData[19].f_CenterX		= 0.7648
			hackingData.s_WallData[19].f_CenterY		= 0.3458
			hackingData.s_WallData[19].f_Width			= 0.1432
			hackingData.s_WallData[19].f_Height 		= 0.0660

			hackingData.s_WallData[20].f_CenterX		= 0.0000
			hackingData.s_WallData[20].f_CenterY		= 0.0000
			hackingData.s_WallData[20].f_Width			= 0.0000
			hackingData.s_WallData[20].f_Height 		= 0.0000

			hackingData.s_WallData[21].f_CenterX		= 0.0000
			hackingData.s_WallData[21].f_CenterY		= 0.0000
			hackingData.s_WallData[21].f_Width			= 0.0000
			hackingData.s_WallData[21].f_Height 		= 0.0000

			hackingData.s_WallData[22].f_CenterX		= 0.0000
			hackingData.s_WallData[22].f_CenterY		= 0.0000
			hackingData.s_WallData[22].f_Width			= 0.0000
			hackingData.s_WallData[22].f_Height 		= 0.0000

			hackingData.s_WallData[23].f_CenterX		= 0.0000
			hackingData.s_WallData[23].f_CenterY		= 0.0000
			hackingData.s_WallData[23].f_Width			= 0.0000
			hackingData.s_WallData[23].f_Height 		= 0.0000

			hackingData.s_WallData[24].f_CenterX		= 0.0000
			hackingData.s_WallData[24].f_CenterY		= 0.0000
			hackingData.s_WallData[24].f_Width			= 0.0000
			hackingData.s_WallData[24].f_Height 		= 0.0000

			hackingData.s_WallData[25].f_CenterX		= 0.0000
			hackingData.s_WallData[25].f_CenterY		= 0.0000
			hackingData.s_WallData[25].f_Width			= 0.0000
			hackingData.s_WallData[25].f_Height 		= 0.0000

			hackingData.s_WallData[26].f_CenterX		= 0.0000
			hackingData.s_WallData[26].f_CenterY		= 0.0000
			hackingData.s_WallData[26].f_Width			= 0.0000
			hackingData.s_WallData[26].f_Height 		= 0.0000

			hackingData.s_WallData[27].f_CenterX		= 0.0000
			hackingData.s_WallData[27].f_CenterY		= 0.0000
			hackingData.s_WallData[27].f_Width			= 0.0000
			hackingData.s_WallData[27].f_Height 		= 0.0000

			hackingData.s_WallData[28].f_CenterX		= 0.0000
			hackingData.s_WallData[28].f_CenterY		= 0.0000
			hackingData.s_WallData[28].f_Width			= 0.0000
			hackingData.s_WallData[28].f_Height 		= 0.0000

			hackingData.s_WallData[29].f_CenterX		= 0.0000
			hackingData.s_WallData[29].f_CenterY		= 0.0000
			hackingData.s_WallData[29].f_Width			= 0.0000
			hackingData.s_WallData[29].f_Height 		= 0.0000

			hackingData.s_WallData[30].f_CenterX		= 0.0000
			hackingData.s_WallData[30].f_CenterY		= 0.0000
			hackingData.s_WallData[30].f_Width			= 0.0000
			hackingData.s_WallData[30].f_Height 		= 0.0000

			hackingData.s_WallData[31].f_CenterX		= 0.0000
			hackingData.s_WallData[31].f_CenterY		= 0.0000
			hackingData.s_WallData[31].f_Width			= 0.0000
			hackingData.s_WallData[31].f_Height 		= 0.0000

			hackingData.s_WallData[32].f_CenterX		= 0.0000
			hackingData.s_WallData[32].f_CenterY		= 0.0000
			hackingData.s_WallData[32].f_Width			= 0.0000
			hackingData.s_WallData[32].f_Height 		= 0.0000

			hackingData.s_WallData[33].f_CenterX		= 0.0000
			hackingData.s_WallData[33].f_CenterY		= 0.0000
			hackingData.s_WallData[33].f_Width			= 0.0000
			hackingData.s_WallData[33].f_Height 		= 0.0000

			hackingData.s_WallData[34].f_CenterX		= 0.0000
			hackingData.s_WallData[34].f_CenterY		= 0.0000
			hackingData.s_WallData[34].f_Width			= 0.0000
			hackingData.s_WallData[34].f_Height 		= 0.0000

			hackingData.s_WallData[35].f_CenterX		= 0.0000
			hackingData.s_WallData[35].f_CenterY		= 0.0000
			hackingData.s_WallData[35].f_Width			= 0.0000
			hackingData.s_WallData[35].f_Height 		= 0.0000

			hackingData.s_WallData[36].f_CenterX		= 0.0000
			hackingData.s_WallData[36].f_CenterY		= 0.0000
			hackingData.s_WallData[36].f_Width			= 0.0000
			hackingData.s_WallData[36].f_Height 		= 0.0000

			hackingData.s_WallData[37].f_CenterX		= 0.0000
			hackingData.s_WallData[37].f_CenterY		= 0.0000
			hackingData.s_WallData[37].f_Width			= 0.0000
			hackingData.s_WallData[37].f_Height 		= 0.0000

			hackingData.s_WallData[38].f_CenterX		= 0.0000
			hackingData.s_WallData[38].f_CenterY		= 0.0000
			hackingData.s_WallData[38].f_Width			= 0.0000
			hackingData.s_WallData[38].f_Height 		= 0.0000

			hackingData.s_WallData[39].f_CenterX		= 0.0000
			hackingData.s_WallData[39].f_CenterY		= 0.0000
			hackingData.s_WallData[39].f_Width			= 0.0000
			hackingData.s_WallData[39].f_Height 		= 0.0000
		BREAK
		
		CASE 1
			// HEADER - Level: 2

			hackingData.s_SnakeData.f_StartX			= 0.1740
			hackingData.s_SnakeData.f_StartY			= 0.7390
			hackingData.s_SnakeData.f_CenterX			= hackingData.s_SnakeData.f_StartX
			hackingData.s_SnakeData.f_CenterY			= hackingData.s_SnakeData.f_StartY

			hackingData.s_SnakeData.i_StartDirection	= 3
			hackingData.s_SnakeData.i_Direction			= hackingData.s_SnakeData.i_StartDirection
			hackingData.s_SnakeData.f_SpeedX			= 0.0880
			hackingData.s_SnakeData.f_SpeedY			= hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio

			hackingData.s_SnakeData.f_EndX				= 0.8300
			hackingData.s_SnakeData.f_EndY				= 0.3010
			hackingData.s_SnakeData.f_EndWidth			= 0.0260
			hackingData.s_SnakeData.f_EndHeight			= 0.0700


			// WALLS
			hackingData.s_WallData[0].f_CenterX			= 0.1573
			hackingData.s_WallData[0].f_CenterY			= 0.5000
			hackingData.s_WallData[0].f_Width			= 0.0070
			hackingData.s_WallData[0].f_Height 			= 0.6960

			hackingData.s_WallData[1].f_CenterX			= 0.8423
			hackingData.s_WallData[1].f_CenterY			= 0.5000
			hackingData.s_WallData[1].f_Width			= 0.0070
			hackingData.s_WallData[1].f_Height 			= 0.6960

			hackingData.s_WallData[2].f_CenterX			= 0.5003
			hackingData.s_WallData[2].f_CenterY			= 0.1480
			hackingData.s_WallData[2].f_Width			= 0.6905
			hackingData.s_WallData[2].f_Height 			= 0.0100

			hackingData.s_WallData[3].f_CenterX			= 0.5005
			hackingData.s_WallData[3].f_CenterY			= 0.8530
			hackingData.s_WallData[3].f_Width			= 0.6910
			hackingData.s_WallData[3].f_Height 			= 0.0100

			hackingData.s_WallData[4].f_CenterX			= 0.1710
			hackingData.s_WallData[4].f_CenterY			= 0.7390
			hackingData.s_WallData[4].f_Width			= 0.0270
			hackingData.s_WallData[4].f_Height 			= 0.0780

			hackingData.s_WallData[5].f_CenterX			= 0.2034
			hackingData.s_WallData[5].f_CenterY			= 0.3984
			hackingData.s_WallData[5].f_Width			= 0.0890
			hackingData.s_WallData[5].f_Height 			= 0.4880

			hackingData.s_WallData[6].f_CenterX			= 0.3278
			hackingData.s_WallData[6].f_CenterY			= 0.2125
			hackingData.s_WallData[6].f_Width			= 0.1720
			hackingData.s_WallData[6].f_Height 			= 0.1130

			hackingData.s_WallData[7].f_CenterX			= 0.3763
			hackingData.s_WallData[7].f_CenterY			= 0.6380
			hackingData.s_WallData[7].f_Width			= 0.1240
			hackingData.s_WallData[7].f_Height 			= 0.4367

			hackingData.s_WallData[8].f_CenterX			= 0.4860
			hackingData.s_WallData[8].f_CenterY			= 0.5433
			hackingData.s_WallData[8].f_Width			= 0.0526
			hackingData.s_WallData[8].f_Height 			= 0.6100

			hackingData.s_WallData[9].f_CenterX			= 0.6212
			hackingData.s_WallData[9].f_CenterY			= 0.3510
			hackingData.s_WallData[9].f_Width			= 0.1257
			hackingData.s_WallData[9].f_Height 			= 0.3996

			hackingData.s_WallData[10].f_CenterX		= 0.7034
			hackingData.s_WallData[10].f_CenterY		= 0.3048
			hackingData.s_WallData[10].f_Width			= 0.0400
			hackingData.s_WallData[10].f_Height 		= 0.3010

			hackingData.s_WallData[11].f_CenterX		= 0.7397
			hackingData.s_WallData[11].f_CenterY		= 0.2289
			hackingData.s_WallData[11].f_Width			= 0.0330
			hackingData.s_WallData[11].f_Height 		= 0.1490

			hackingData.s_WallData[12].f_CenterX		= 0.6249
			hackingData.s_WallData[12].f_CenterY		= 0.7105
			hackingData.s_WallData[12].f_Width			= 0.1412
			hackingData.s_WallData[12].f_Height 		= 0.1499

			hackingData.s_WallData[13].f_CenterX		= 0.7900
			hackingData.s_WallData[13].f_CenterY		= 0.8010
			hackingData.s_WallData[13].f_Width			= 0.0955
			hackingData.s_WallData[13].f_Height 		= 0.1032

			hackingData.s_WallData[14].f_CenterX		= 0.7704
			hackingData.s_WallData[14].f_CenterY		= 0.5977
			hackingData.s_WallData[14].f_Width			= 0.0564
			hackingData.s_WallData[14].f_Height 		= 0.1630

			hackingData.s_WallData[15].f_CenterX		= 0.8086
			hackingData.s_WallData[15].f_CenterY		= 0.4081
			hackingData.s_WallData[15].f_Width			= 0.0593
			hackingData.s_WallData[15].f_Height 		= 0.0931

			hackingData.s_WallData[16].f_CenterX		= 0.3295
			hackingData.s_WallData[16].f_CenterY		= 0.3910
			hackingData.s_WallData[16].f_Width			= 0.0304
			hackingData.s_WallData[16].f_Height 		= 0.0642

			hackingData.s_WallData[17].f_CenterX		= 0.3961
			hackingData.s_WallData[17].f_CenterY		= 0.3097
			hackingData.s_WallData[17].f_Width			= 0.0346
			hackingData.s_WallData[17].f_Height 		= 0.0785

			hackingData.s_WallData[18].f_CenterX		= 0.5495
			hackingData.s_WallData[18].f_CenterY		= 0.5064
			hackingData.s_WallData[18].f_Width			= 0.0150
			hackingData.s_WallData[18].f_Height 		= 0.0900

			hackingData.s_WallData[19].f_CenterX		= 0.0000
			hackingData.s_WallData[19].f_CenterY		= 0.0000
			hackingData.s_WallData[19].f_Width			= 0.0000
			hackingData.s_WallData[19].f_Height 		= 0.0000

			hackingData.s_WallData[20].f_CenterX		= 0.0000
			hackingData.s_WallData[20].f_CenterY		= 0.0000
			hackingData.s_WallData[20].f_Width			= 0.0000
			hackingData.s_WallData[20].f_Height 		= 0.0000

			hackingData.s_WallData[21].f_CenterX		= 0.0000
			hackingData.s_WallData[21].f_CenterY		= 0.0000
			hackingData.s_WallData[21].f_Width			= 0.0000
			hackingData.s_WallData[21].f_Height 		= 0.0000

			hackingData.s_WallData[22].f_CenterX		= 0.0000
			hackingData.s_WallData[22].f_CenterY		= 0.0000
			hackingData.s_WallData[22].f_Width			= 0.0000
			hackingData.s_WallData[22].f_Height 		= 0.0000

			hackingData.s_WallData[23].f_CenterX		= 0.0000
			hackingData.s_WallData[23].f_CenterY		= 0.0000
			hackingData.s_WallData[23].f_Width			= 0.0000
			hackingData.s_WallData[23].f_Height 		= 0.0000

			hackingData.s_WallData[24].f_CenterX		= 0.0000
			hackingData.s_WallData[24].f_CenterY		= 0.0000
			hackingData.s_WallData[24].f_Width			= 0.0000
			hackingData.s_WallData[24].f_Height 		= 0.0000

			hackingData.s_WallData[25].f_CenterX		= 0.0000
			hackingData.s_WallData[25].f_CenterY		= 0.0000
			hackingData.s_WallData[25].f_Width			= 0.0000
			hackingData.s_WallData[25].f_Height 		= 0.0000

			hackingData.s_WallData[26].f_CenterX		= 0.0000
			hackingData.s_WallData[26].f_CenterY		= 0.0000
			hackingData.s_WallData[26].f_Width			= 0.0000
			hackingData.s_WallData[26].f_Height 		= 0.0000

			hackingData.s_WallData[27].f_CenterX		= 0.0000
			hackingData.s_WallData[27].f_CenterY		= 0.0000
			hackingData.s_WallData[27].f_Width			= 0.0000
			hackingData.s_WallData[27].f_Height 		= 0.0000

			hackingData.s_WallData[28].f_CenterX		= 0.0000
			hackingData.s_WallData[28].f_CenterY		= 0.0000
			hackingData.s_WallData[28].f_Width			= 0.0000
			hackingData.s_WallData[28].f_Height 		= 0.0000

			hackingData.s_WallData[29].f_CenterX		= 0.0000
			hackingData.s_WallData[29].f_CenterY		= 0.0000
			hackingData.s_WallData[29].f_Width			= 0.0000
			hackingData.s_WallData[29].f_Height 		= 0.0000

			hackingData.s_WallData[30].f_CenterX		= 0.0000
			hackingData.s_WallData[30].f_CenterY		= 0.0000
			hackingData.s_WallData[30].f_Width			= 0.0000
			hackingData.s_WallData[30].f_Height 		= 0.0000

			hackingData.s_WallData[31].f_CenterX		= 0.0000
			hackingData.s_WallData[31].f_CenterY		= 0.0000
			hackingData.s_WallData[31].f_Width			= 0.0000
			hackingData.s_WallData[31].f_Height 		= 0.0000

			hackingData.s_WallData[32].f_CenterX		= 0.0000
			hackingData.s_WallData[32].f_CenterY		= 0.0000
			hackingData.s_WallData[32].f_Width			= 0.0000
			hackingData.s_WallData[32].f_Height 		= 0.0000

			hackingData.s_WallData[33].f_CenterX		= 0.0000
			hackingData.s_WallData[33].f_CenterY		= 0.0000
			hackingData.s_WallData[33].f_Width			= 0.0000
			hackingData.s_WallData[33].f_Height 		= 0.0000

			hackingData.s_WallData[34].f_CenterX		= 0.0000
			hackingData.s_WallData[34].f_CenterY		= 0.0000
			hackingData.s_WallData[34].f_Width			= 0.0000
			hackingData.s_WallData[34].f_Height 		= 0.0000

			hackingData.s_WallData[35].f_CenterX		= 0.0000
			hackingData.s_WallData[35].f_CenterY		= 0.0000
			hackingData.s_WallData[35].f_Width			= 0.0000
			hackingData.s_WallData[35].f_Height 		= 0.0000

			hackingData.s_WallData[36].f_CenterX		= 0.0000
			hackingData.s_WallData[36].f_CenterY		= 0.0000
			hackingData.s_WallData[36].f_Width			= 0.0000
			hackingData.s_WallData[36].f_Height 		= 0.0000

			hackingData.s_WallData[37].f_CenterX		= 0.0000
			hackingData.s_WallData[37].f_CenterY		= 0.0000
			hackingData.s_WallData[37].f_Width			= 0.0000
			hackingData.s_WallData[37].f_Height 		= 0.0000

			hackingData.s_WallData[38].f_CenterX		= 0.0000
			hackingData.s_WallData[38].f_CenterY		= 0.0000
			hackingData.s_WallData[38].f_Width			= 0.0000
			hackingData.s_WallData[38].f_Height 		= 0.0000

			hackingData.s_WallData[39].f_CenterX		= 0.0000
			hackingData.s_WallData[39].f_CenterY		= 0.0000
			hackingData.s_WallData[39].f_Width			= 0.0000
			hackingData.s_WallData[39].f_Height 		= 0.0000
		BREAK
		
		CASE 2
			// HEADER - Level: 3

			hackingData.s_SnakeData.f_StartX			= 0.1750
			hackingData.s_SnakeData.f_StartY			= 0.7720
			hackingData.s_SnakeData.f_CenterX			= hackingData.s_SnakeData.f_StartX
			hackingData.s_SnakeData.f_CenterY			= hackingData.s_SnakeData.f_StartY

			hackingData.s_SnakeData.i_StartDirection	= 3
			hackingData.s_SnakeData.i_Direction			= hackingData.s_SnakeData.i_StartDirection
			hackingData.s_SnakeData.f_SpeedX			= 0.0880
			hackingData.s_SnakeData.f_SpeedY			= hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio

			hackingData.s_SnakeData.f_EndX				= 0.1710
			hackingData.s_SnakeData.f_EndY				= 0.2300
			hackingData.s_SnakeData.f_EndWidth			= 0.0260
			hackingData.s_SnakeData.f_EndHeight			= 0.0700


			// WALLS
			hackingData.s_WallData[0].f_CenterX			= 0.1573
			hackingData.s_WallData[0].f_CenterY			= 0.5000
			hackingData.s_WallData[0].f_Width			= 0.0070
			hackingData.s_WallData[0].f_Height 			= 0.6960

			hackingData.s_WallData[1].f_CenterX			= 0.8423
			hackingData.s_WallData[1].f_CenterY			= 0.5000
			hackingData.s_WallData[1].f_Width			= 0.0070
			hackingData.s_WallData[1].f_Height 			= 0.6960

			hackingData.s_WallData[2].f_CenterX			= 0.5003
			hackingData.s_WallData[2].f_CenterY			= 0.1480
			hackingData.s_WallData[2].f_Width			= 0.6905
			hackingData.s_WallData[2].f_Height 			= 0.0100

			hackingData.s_WallData[3].f_CenterX			= 0.5005
			hackingData.s_WallData[3].f_CenterY			= 0.8530
			hackingData.s_WallData[3].f_Width			= 0.6910
			hackingData.s_WallData[3].f_Height 			= 0.0100

			hackingData.s_WallData[4].f_CenterX			= 0.1710
			hackingData.s_WallData[4].f_CenterY			= 0.7720
			hackingData.s_WallData[4].f_Width			= 0.0280
			hackingData.s_WallData[4].f_Height 			= 0.0790

			hackingData.s_WallData[5].f_CenterX			= 0.2405
			hackingData.s_WallData[5].f_CenterY			= 0.5073
			hackingData.s_WallData[5].f_Width			= 0.1739
			hackingData.s_WallData[5].f_Height 			= 0.4143

			hackingData.s_WallData[6].f_CenterX			= 0.4010
			hackingData.s_WallData[6].f_CenterY			= 0.7611
			hackingData.s_WallData[6].f_Width			= 0.0698
			hackingData.s_WallData[6].f_Height 			= 0.1734

			hackingData.s_WallData[7].f_CenterX			= 0.4172
			hackingData.s_WallData[7].f_CenterY			= 0.4899
			hackingData.s_WallData[7].f_Width			= 0.2000
			hackingData.s_WallData[7].f_Height 			= 0.1679

			hackingData.s_WallData[8].f_CenterX			= 0.5370
			hackingData.s_WallData[8].f_CenterY			= 0.3792
			hackingData.s_WallData[8].f_Width			= 0.0701
			hackingData.s_WallData[8].f_Height 			= 0.2620

			hackingData.s_WallData[9].f_CenterX			= 0.5992
			hackingData.s_WallData[9].f_CenterY			= 0.5190
			hackingData.s_WallData[9].f_Width			= 0.0994
			hackingData.s_WallData[9].f_Height 			= 0.0891

			hackingData.s_WallData[10].f_CenterX		= 0.7266
			hackingData.s_WallData[10].f_CenterY		= 0.7504
			hackingData.s_WallData[10].f_Width			= 0.2270
			hackingData.s_WallData[10].f_Height 		= 0.2018

			hackingData.s_WallData[11].f_CenterX		= 0.7684
			hackingData.s_WallData[11].f_CenterY		= 0.4950
			hackingData.s_WallData[11].f_Width			= 0.1410
			hackingData.s_WallData[11].f_Height 		= 0.1301

			hackingData.s_WallData[12].f_CenterX		= 0.7276
			hackingData.s_WallData[12].f_CenterY		= 0.2681
			hackingData.s_WallData[12].f_Width			= 0.2250
			hackingData.s_WallData[12].f_Height 		= 0.2300

			hackingData.s_WallData[13].f_CenterX		= 0.4103
			hackingData.s_WallData[13].f_CenterY		= 0.2460
			hackingData.s_WallData[13].f_Width			= 0.0704
			hackingData.s_WallData[13].f_Height 		= 0.1924

			hackingData.s_WallData[14].f_CenterX		= 0.3368
			hackingData.s_WallData[14].f_CenterY		= 0.1981
			hackingData.s_WallData[14].f_Width			= 0.0743
			hackingData.s_WallData[14].f_Height 		= 0.0938

			hackingData.s_WallData[15].f_CenterX		= 0.5345
			hackingData.s_WallData[15].f_CenterY		= 0.6472
			hackingData.s_WallData[15].f_Width			= 0.0653
			hackingData.s_WallData[15].f_Height 		= 0.2456

			hackingData.s_WallData[16].f_CenterX		= 0.4531
			hackingData.s_WallData[16].f_CenterY		= 0.2952
			hackingData.s_WallData[16].f_Width			= 0.0225
			hackingData.s_WallData[16].f_Height 		= 0.0954

			hackingData.s_WallData[17].f_CenterX		= 0.0000
			hackingData.s_WallData[17].f_CenterY		= 0.0000
			hackingData.s_WallData[17].f_Width			= 0.0000
			hackingData.s_WallData[17].f_Height 		= 0.0000

			hackingData.s_WallData[18].f_CenterX		= 0.0000
			hackingData.s_WallData[18].f_CenterY		= 0.0000
			hackingData.s_WallData[18].f_Width			= 0.0000
			hackingData.s_WallData[18].f_Height 		= 0.0000

			hackingData.s_WallData[19].f_CenterX		= 0.0000
			hackingData.s_WallData[19].f_CenterY		= 0.0000
			hackingData.s_WallData[19].f_Width			= 0.0000
			hackingData.s_WallData[19].f_Height 		= 0.0000

			hackingData.s_WallData[20].f_CenterX		= 0.0000
			hackingData.s_WallData[20].f_CenterY		= 0.0000
			hackingData.s_WallData[20].f_Width			= 0.0000
			hackingData.s_WallData[20].f_Height 		= 0.0000

			hackingData.s_WallData[21].f_CenterX		= 0.0000
			hackingData.s_WallData[21].f_CenterY		= 0.0000
			hackingData.s_WallData[21].f_Width			= 0.0000
			hackingData.s_WallData[21].f_Height 		= 0.0000

			hackingData.s_WallData[22].f_CenterX		= 0.0000
			hackingData.s_WallData[22].f_CenterY		= 0.0000
			hackingData.s_WallData[22].f_Width			= 0.0000
			hackingData.s_WallData[22].f_Height 		= 0.0000

			hackingData.s_WallData[23].f_CenterX		= 0.0000
			hackingData.s_WallData[23].f_CenterY		= 0.0000
			hackingData.s_WallData[23].f_Width			= 0.0000
			hackingData.s_WallData[23].f_Height 		= 0.0000

			hackingData.s_WallData[24].f_CenterX		= 0.0000
			hackingData.s_WallData[24].f_CenterY		= 0.0000
			hackingData.s_WallData[24].f_Width			= 0.0000
			hackingData.s_WallData[24].f_Height 		= 0.0000

			hackingData.s_WallData[25].f_CenterX		= 0.0000
			hackingData.s_WallData[25].f_CenterY		= 0.0000
			hackingData.s_WallData[25].f_Width			= 0.0000
			hackingData.s_WallData[25].f_Height 		= 0.0000

			hackingData.s_WallData[26].f_CenterX		= 0.0000
			hackingData.s_WallData[26].f_CenterY		= 0.0000
			hackingData.s_WallData[26].f_Width			= 0.0000
			hackingData.s_WallData[26].f_Height 		= 0.0000

			hackingData.s_WallData[27].f_CenterX		= 0.0000
			hackingData.s_WallData[27].f_CenterY		= 0.0000
			hackingData.s_WallData[27].f_Width			= 0.0000
			hackingData.s_WallData[27].f_Height 		= 0.0000

			hackingData.s_WallData[28].f_CenterX		= 0.0000
			hackingData.s_WallData[28].f_CenterY		= 0.0000
			hackingData.s_WallData[28].f_Width			= 0.0000
			hackingData.s_WallData[28].f_Height 		= 0.0000

			hackingData.s_WallData[29].f_CenterX		= 0.0000
			hackingData.s_WallData[29].f_CenterY		= 0.0000
			hackingData.s_WallData[29].f_Width			= 0.0000
			hackingData.s_WallData[29].f_Height 		= 0.0000

			hackingData.s_WallData[30].f_CenterX		= 0.0000
			hackingData.s_WallData[30].f_CenterY		= 0.0000
			hackingData.s_WallData[30].f_Width			= 0.0000
			hackingData.s_WallData[30].f_Height 		= 0.0000

			hackingData.s_WallData[31].f_CenterX		= 0.0000
			hackingData.s_WallData[31].f_CenterY		= 0.0000
			hackingData.s_WallData[31].f_Width			= 0.0000
			hackingData.s_WallData[31].f_Height 		= 0.0000

			hackingData.s_WallData[32].f_CenterX		= 0.0000
			hackingData.s_WallData[32].f_CenterY		= 0.0000
			hackingData.s_WallData[32].f_Width			= 0.0000
			hackingData.s_WallData[32].f_Height 		= 0.0000

			hackingData.s_WallData[33].f_CenterX		= 0.0000
			hackingData.s_WallData[33].f_CenterY		= 0.0000
			hackingData.s_WallData[33].f_Width			= 0.0000
			hackingData.s_WallData[33].f_Height 		= 0.0000

			hackingData.s_WallData[34].f_CenterX		= 0.0000
			hackingData.s_WallData[34].f_CenterY		= 0.0000
			hackingData.s_WallData[34].f_Width			= 0.0000
			hackingData.s_WallData[34].f_Height 		= 0.0000

			hackingData.s_WallData[35].f_CenterX		= 0.0000
			hackingData.s_WallData[35].f_CenterY		= 0.0000
			hackingData.s_WallData[35].f_Width			= 0.0000
			hackingData.s_WallData[35].f_Height 		= 0.0000

			hackingData.s_WallData[36].f_CenterX		= 0.0000
			hackingData.s_WallData[36].f_CenterY		= 0.0000
			hackingData.s_WallData[36].f_Width			= 0.0000
			hackingData.s_WallData[36].f_Height 		= 0.0000

			hackingData.s_WallData[37].f_CenterX		= 0.0000
			hackingData.s_WallData[37].f_CenterY		= 0.0000
			hackingData.s_WallData[37].f_Width			= 0.0000
			hackingData.s_WallData[37].f_Height 		= 0.0000

			hackingData.s_WallData[38].f_CenterX		= 0.0000
			hackingData.s_WallData[38].f_CenterY		= 0.0000
			hackingData.s_WallData[38].f_Width			= 0.0000
			hackingData.s_WallData[38].f_Height 		= 0.0000

			hackingData.s_WallData[39].f_CenterX		= 0.0000
			hackingData.s_WallData[39].f_CenterY		= 0.0000
			hackingData.s_WallData[39].f_Width			= 0.0000
			hackingData.s_WallData[39].f_Height 		= 0.0000
		BREAK
		
		CASE 3
			// HEADER - Level: 4

			hackingData.s_SnakeData.f_StartX			= 0.1740
			hackingData.s_SnakeData.f_StartY			= 0.7530
			hackingData.s_SnakeData.f_CenterX			= hackingData.s_SnakeData.f_StartX
			hackingData.s_SnakeData.f_CenterY			= hackingData.s_SnakeData.f_StartY

			hackingData.s_SnakeData.i_StartDirection	= 3
			hackingData.s_SnakeData.i_Direction			= hackingData.s_SnakeData.i_StartDirection
			hackingData.s_SnakeData.f_SpeedX			= 0.0880
			hackingData.s_SnakeData.f_SpeedY			= hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio

			hackingData.s_SnakeData.f_EndX				= 0.8300
			hackingData.s_SnakeData.f_EndY				= 0.2900
			hackingData.s_SnakeData.f_EndWidth			= 0.0260
			hackingData.s_SnakeData.f_EndHeight			= 0.0700


			// WALLS
			hackingData.s_WallData[0].f_CenterX			= 0.1573
			hackingData.s_WallData[0].f_CenterY			= 0.5000
			hackingData.s_WallData[0].f_Width			= 0.0070
			hackingData.s_WallData[0].f_Height 			= 0.6960

			hackingData.s_WallData[1].f_CenterX			= 0.8423
			hackingData.s_WallData[1].f_CenterY			= 0.5000
			hackingData.s_WallData[1].f_Width			= 0.0070
			hackingData.s_WallData[1].f_Height 			= 0.6960

			hackingData.s_WallData[2].f_CenterX			= 0.5003
			hackingData.s_WallData[2].f_CenterY			= 0.1480
			hackingData.s_WallData[2].f_Width			= 0.6905
			hackingData.s_WallData[2].f_Height 			= 0.0100

			hackingData.s_WallData[3].f_CenterX			= 0.5005
			hackingData.s_WallData[3].f_CenterY			= 0.8530
			hackingData.s_WallData[3].f_Width			= 0.6910
			hackingData.s_WallData[3].f_Height 			= 0.0100

			hackingData.s_WallData[4].f_CenterX			= 0.1710
			hackingData.s_WallData[4].f_CenterY			= 0.7530
			hackingData.s_WallData[4].f_Width			= 0.0260
			hackingData.s_WallData[4].f_Height 			= 0.0780

			hackingData.s_WallData[5].f_CenterX			= 0.2406
			hackingData.s_WallData[5].f_CenterY			= 0.3722
			hackingData.s_WallData[5].f_Width			= 0.1605
			hackingData.s_WallData[5].f_Height 			= 0.4400

			hackingData.s_WallData[6].f_CenterX			= 0.3290
			hackingData.s_WallData[6].f_CenterY			= 0.7922
			hackingData.s_WallData[6].f_Width			= 0.1040
			hackingData.s_WallData[6].f_Height 			= 0.1245

			hackingData.s_WallData[7].f_CenterX			= 0.4985
			hackingData.s_WallData[7].f_CenterY			= 0.6060
			hackingData.s_WallData[7].f_Width			= 0.3826
			hackingData.s_WallData[7].f_Height 			= 0.0964

			hackingData.s_WallData[8].f_CenterX			= 0.4807
			hackingData.s_WallData[8].f_CenterY			= 0.7167
			hackingData.s_WallData[8].f_Width			= 0.1194
			hackingData.s_WallData[8].f_Height 			= 0.1268

			hackingData.s_WallData[9].f_CenterX			= 0.7102
			hackingData.s_WallData[9].f_CenterY			= 0.7833
			hackingData.s_WallData[9].f_Width			= 0.2629
			hackingData.s_WallData[9].f_Height 			= 0.1297

			hackingData.s_WallData[10].f_CenterX		= 0.8100
			hackingData.s_WallData[10].f_CenterY		= 0.5780
			hackingData.s_WallData[10].f_Width			= 0.0600
			hackingData.s_WallData[10].f_Height 		= 0.3630

			hackingData.s_WallData[11].f_CenterX		= 0.7690
			hackingData.s_WallData[11].f_CenterY		= 0.4530
			hackingData.s_WallData[11].f_Width			= 0.0240
			hackingData.s_WallData[11].f_Height 		= 0.0430

			hackingData.s_WallData[12].f_CenterX		= 0.6691
			hackingData.s_WallData[12].f_CenterY		= 0.4250
			hackingData.s_WallData[12].f_Width			= 0.1854
			hackingData.s_WallData[12].f_Height 		= 0.1048

			hackingData.s_WallData[13].f_CenterX		= 0.6643
			hackingData.s_WallData[13].f_CenterY		= 0.3040
			hackingData.s_WallData[13].f_Width			= 0.0429
			hackingData.s_WallData[13].f_Height 		= 0.1461

			hackingData.s_WallData[14].f_CenterX		= 0.4955
			hackingData.s_WallData[14].f_CenterY		= 0.3611
			hackingData.s_WallData[14].f_Width			= 0.2535
			hackingData.s_WallData[14].f_Height 		= 0.0936

			hackingData.s_WallData[15].f_CenterX		= 0.3947
			hackingData.s_WallData[15].f_CenterY		= 0.3276
			hackingData.s_WallData[15].f_Width			= 0.0519
			hackingData.s_WallData[15].f_Height 		= 0.2157

			hackingData.s_WallData[16].f_CenterX		= 0.5282
			hackingData.s_WallData[16].f_CenterY		= 0.2075
			hackingData.s_WallData[16].f_Width			= 0.1359
			hackingData.s_WallData[16].f_Height 		= 0.1086

			hackingData.s_WallData[17].f_CenterX		= 0.7504
			hackingData.s_WallData[17].f_CenterY		= 0.2068
			hackingData.s_WallData[17].f_Width			= 0.0550
			hackingData.s_WallData[17].f_Height 		= 0.1120

			hackingData.s_WallData[18].f_CenterX		= 0.4298
			hackingData.s_WallData[18].f_CenterY		= 0.5236
			hackingData.s_WallData[18].f_Width			= 0.2205
			hackingData.s_WallData[18].f_Height 		= 0.0686

			hackingData.s_WallData[19].f_CenterX		= 0.7586
			hackingData.s_WallData[19].f_CenterY		= 0.5968
			hackingData.s_WallData[19].f_Width			= 0.0431
			hackingData.s_WallData[19].f_Height 		= 0.2474

			hackingData.s_WallData[20].f_CenterX		= 0.3614
			hackingData.s_WallData[20].f_CenterY		= 0.2587
			hackingData.s_WallData[20].f_Width			= 0.0174
			hackingData.s_WallData[20].f_Height 		= 0.0782

			hackingData.s_WallData[21].f_CenterX		= 0.0000
			hackingData.s_WallData[21].f_CenterY		= 0.0000
			hackingData.s_WallData[21].f_Width			= 0.0000
			hackingData.s_WallData[21].f_Height 		= 0.0000

			hackingData.s_WallData[22].f_CenterX		= 0.0000
			hackingData.s_WallData[22].f_CenterY		= 0.0000
			hackingData.s_WallData[22].f_Width			= 0.0000
			hackingData.s_WallData[22].f_Height 		= 0.0000

			hackingData.s_WallData[23].f_CenterX		= 0.0000
			hackingData.s_WallData[23].f_CenterY		= 0.0000
			hackingData.s_WallData[23].f_Width			= 0.0000
			hackingData.s_WallData[23].f_Height 		= 0.0000

			hackingData.s_WallData[24].f_CenterX		= 0.0000
			hackingData.s_WallData[24].f_CenterY		= 0.0000
			hackingData.s_WallData[24].f_Width			= 0.0000
			hackingData.s_WallData[24].f_Height 		= 0.0000

			hackingData.s_WallData[25].f_CenterX		= 0.0000
			hackingData.s_WallData[25].f_CenterY		= 0.0000
			hackingData.s_WallData[25].f_Width			= 0.0000
			hackingData.s_WallData[25].f_Height 		= 0.0000

			hackingData.s_WallData[26].f_CenterX		= 0.0000
			hackingData.s_WallData[26].f_CenterY		= 0.0000
			hackingData.s_WallData[26].f_Width			= 0.0000
			hackingData.s_WallData[26].f_Height 		= 0.0000

			hackingData.s_WallData[27].f_CenterX		= 0.0000
			hackingData.s_WallData[27].f_CenterY		= 0.0000
			hackingData.s_WallData[27].f_Width			= 0.0000
			hackingData.s_WallData[27].f_Height 		= 0.0000

			hackingData.s_WallData[28].f_CenterX		= 0.0000
			hackingData.s_WallData[28].f_CenterY		= 0.0000
			hackingData.s_WallData[28].f_Width			= 0.0000
			hackingData.s_WallData[28].f_Height 		= 0.0000

			hackingData.s_WallData[29].f_CenterX		= 0.0000
			hackingData.s_WallData[29].f_CenterY		= 0.0000
			hackingData.s_WallData[29].f_Width			= 0.0000
			hackingData.s_WallData[29].f_Height 		= 0.0000

			hackingData.s_WallData[30].f_CenterX		= 0.0000
			hackingData.s_WallData[30].f_CenterY		= 0.0000
			hackingData.s_WallData[30].f_Width			= 0.0000
			hackingData.s_WallData[30].f_Height 		= 0.0000

			hackingData.s_WallData[31].f_CenterX		= 0.0000
			hackingData.s_WallData[31].f_CenterY		= 0.0000
			hackingData.s_WallData[31].f_Width			= 0.0000
			hackingData.s_WallData[31].f_Height 		= 0.0000

			hackingData.s_WallData[32].f_CenterX		= 0.0000
			hackingData.s_WallData[32].f_CenterY		= 0.0000
			hackingData.s_WallData[32].f_Width			= 0.0000
			hackingData.s_WallData[32].f_Height 		= 0.0000

			hackingData.s_WallData[33].f_CenterX		= 0.0000
			hackingData.s_WallData[33].f_CenterY		= 0.0000
			hackingData.s_WallData[33].f_Width			= 0.0000
			hackingData.s_WallData[33].f_Height 		= 0.0000

			hackingData.s_WallData[34].f_CenterX		= 0.0000
			hackingData.s_WallData[34].f_CenterY		= 0.0000
			hackingData.s_WallData[34].f_Width			= 0.0000
			hackingData.s_WallData[34].f_Height 		= 0.0000

			hackingData.s_WallData[35].f_CenterX		= 0.0000
			hackingData.s_WallData[35].f_CenterY		= 0.0000
			hackingData.s_WallData[35].f_Width			= 0.0000
			hackingData.s_WallData[35].f_Height 		= 0.0000

			hackingData.s_WallData[36].f_CenterX		= 0.0000
			hackingData.s_WallData[36].f_CenterY		= 0.0000
			hackingData.s_WallData[36].f_Width			= 0.0000
			hackingData.s_WallData[36].f_Height 		= 0.0000

			hackingData.s_WallData[37].f_CenterX		= 0.0000
			hackingData.s_WallData[37].f_CenterY		= 0.0000
			hackingData.s_WallData[37].f_Width			= 0.0000
			hackingData.s_WallData[37].f_Height 		= 0.0000

			hackingData.s_WallData[38].f_CenterX		= 0.0000
			hackingData.s_WallData[38].f_CenterY		= 0.0000
			hackingData.s_WallData[38].f_Width			= 0.0000
			hackingData.s_WallData[38].f_Height 		= 0.0000

			hackingData.s_WallData[39].f_CenterX		= 0.0000
			hackingData.s_WallData[39].f_CenterY		= 0.0000
			hackingData.s_WallData[39].f_Width			= 0.0000
			hackingData.s_WallData[39].f_Height 		= 0.0000
		BREAK
		
		CASE 4
			// HEADER - Level: 5

			hackingData.s_SnakeData.f_StartX			= 0.1740
			hackingData.s_SnakeData.f_StartY			= 0.2900
			hackingData.s_SnakeData.f_CenterX			= hackingData.s_SnakeData.f_StartX
			hackingData.s_SnakeData.f_CenterY			= hackingData.s_SnakeData.f_StartY

			hackingData.s_SnakeData.i_StartDirection	= 3
			hackingData.s_SnakeData.i_Direction			= hackingData.s_SnakeData.i_StartDirection
			hackingData.s_SnakeData.f_SpeedX			= 0.0880
			hackingData.s_SnakeData.f_SpeedY			= hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio

			hackingData.s_SnakeData.f_EndX				= 0.7910
			hackingData.s_SnakeData.f_EndY				= 0.8310
			hackingData.s_SnakeData.f_EndWidth			= 0.0400
			hackingData.s_SnakeData.f_EndHeight			= 0.0450


			// WALLS
			hackingData.s_WallData[0].f_CenterX			= 0.1573
			hackingData.s_WallData[0].f_CenterY			= 0.5000
			hackingData.s_WallData[0].f_Width			= 0.0070
			hackingData.s_WallData[0].f_Height 			= 0.6960

			hackingData.s_WallData[1].f_CenterX			= 0.8423
			hackingData.s_WallData[1].f_CenterY			= 0.5000
			hackingData.s_WallData[1].f_Width			= 0.0070
			hackingData.s_WallData[1].f_Height 			= 0.6960

			hackingData.s_WallData[2].f_CenterX			= 0.5003
			hackingData.s_WallData[2].f_CenterY			= 0.1480
			hackingData.s_WallData[2].f_Width			= 0.6905
			hackingData.s_WallData[2].f_Height 			= 0.0100

			hackingData.s_WallData[3].f_CenterX			= 0.5005
			hackingData.s_WallData[3].f_CenterY			= 0.8530
			hackingData.s_WallData[3].f_Width			= 0.6910
			hackingData.s_WallData[3].f_Height 			= 0.0100

			hackingData.s_WallData[4].f_CenterX			= 0.1670
			hackingData.s_WallData[4].f_CenterY			= 0.2900
			hackingData.s_WallData[4].f_Width			= 0.0360
			hackingData.s_WallData[4].f_Height 			= 0.0780

			hackingData.s_WallData[5].f_CenterX			= 0.3475
			hackingData.s_WallData[5].f_CenterY			= 0.2064
			hackingData.s_WallData[5].f_Width			= 0.1869
			hackingData.s_WallData[5].f_Height 			= 0.1068

			hackingData.s_WallData[6].f_CenterX			= 0.2903
			hackingData.s_WallData[6].f_CenterY			= 0.3900
			hackingData.s_WallData[6].f_Width			= 0.0721
			hackingData.s_WallData[6].f_Height 			= 0.2870

			hackingData.s_WallData[7].f_CenterX			= 0.2260
			hackingData.s_WallData[7].f_CenterY			= 0.4760
			hackingData.s_WallData[7].f_Width			= 0.0601
			hackingData.s_WallData[7].f_Height 			= 0.0735

			hackingData.s_WallData[8].f_CenterX			= 0.4322
			hackingData.s_WallData[8].f_CenterY			= 0.5830
			hackingData.s_WallData[8].f_Width			= 0.3560
			hackingData.s_WallData[8].f_Height 			= 0.1035

			hackingData.s_WallData[9].f_CenterX			= 0.3125
			hackingData.s_WallData[9].f_CenterY			= 0.6660
			hackingData.s_WallData[9].f_Width			= 0.1171
			hackingData.s_WallData[9].f_Height 			= 0.0692

			hackingData.s_WallData[10].f_CenterX		= 0.1885
			hackingData.s_WallData[10].f_CenterY		= 0.6079
			hackingData.s_WallData[10].f_Width			= 0.0601
			hackingData.s_WallData[10].f_Height 		= 0.0538

			hackingData.s_WallData[11].f_CenterX		= 0.2675
			hackingData.s_WallData[11].f_CenterY		= 0.8070
			hackingData.s_WallData[11].f_Width			= 0.0726
			hackingData.s_WallData[11].f_Height 		= 0.0892

			hackingData.s_WallData[12].f_CenterX		= 0.5352
			hackingData.s_WallData[12].f_CenterY		= 0.7972
			hackingData.s_WallData[12].f_Width			= 0.2359
			hackingData.s_WallData[12].f_Height 		= 0.1064

			hackingData.s_WallData[13].f_CenterX		= 0.5231
			hackingData.s_WallData[13].f_CenterY		= 0.7283
			hackingData.s_WallData[13].f_Width			= 0.0537
			hackingData.s_WallData[13].f_Height 		= 0.0467

			hackingData.s_WallData[14].f_CenterX		= 0.7517
			hackingData.s_WallData[14].f_CenterY		= 0.5701
			hackingData.s_WallData[14].f_Width			= 0.0219
			hackingData.s_WallData[14].f_Height 		= 0.1489

			hackingData.s_WallData[15].f_CenterX		= 0.7182
			hackingData.s_WallData[15].f_CenterY		= 0.6725
			hackingData.s_WallData[15].f_Width			= 0.0486
			hackingData.s_WallData[15].f_Height 		= 0.3545

			hackingData.s_WallData[16].f_CenterX		= 0.5527
			hackingData.s_WallData[16].f_CenterY		= 0.5174
			hackingData.s_WallData[16].f_Width			= 0.1125
			hackingData.s_WallData[16].f_Height 		= 0.0673

			hackingData.s_WallData[17].f_CenterX		= 0.4222
			hackingData.s_WallData[17].f_CenterY		= 0.4538
			hackingData.s_WallData[17].f_Width			= 0.0780
			hackingData.s_WallData[17].f_Height 		= 0.0774

			hackingData.s_WallData[18].f_CenterX		= 0.4251
			hackingData.s_WallData[18].f_CenterY		= 0.3047
			hackingData.s_WallData[18].f_Width			= 0.0319
			hackingData.s_WallData[18].f_Height 		= 0.1075

			hackingData.s_WallData[19].f_CenterX		= 0.4953
			hackingData.s_WallData[19].f_CenterY		= 0.3437
			hackingData.s_WallData[19].f_Width			= 0.0277
			hackingData.s_WallData[19].f_Height 		= 0.0830

			hackingData.s_WallData[20].f_CenterX		= 0.5222
			hackingData.s_WallData[20].f_CenterY		= 0.1977
			hackingData.s_WallData[20].f_Width			= 0.1650
			hackingData.s_WallData[20].f_Height 		= 0.0892

			hackingData.s_WallData[21].f_CenterX		= 0.5540
			hackingData.s_WallData[21].f_CenterY		= 0.2726
			hackingData.s_WallData[21].f_Width			= 0.0439
			hackingData.s_WallData[21].f_Height 		= 0.0620

			hackingData.s_WallData[22].f_CenterX		= 0.5895
			hackingData.s_WallData[22].f_CenterY		= 0.2454
			hackingData.s_WallData[22].f_Width			= 0.0307
			hackingData.s_WallData[22].f_Height 		= 0.0180

			hackingData.s_WallData[23].f_CenterX		= 0.6824
			hackingData.s_WallData[23].f_CenterY		= 0.4000
			hackingData.s_WallData[23].f_Width			= 0.2089
			hackingData.s_WallData[23].f_Height 		= 0.0642

			hackingData.s_WallData[24].f_CenterX		= 0.6821
			hackingData.s_WallData[24].f_CenterY		= 0.2876
			hackingData.s_WallData[24].f_Width			= 0.0478
			hackingData.s_WallData[24].f_Height 		= 0.1780

			hackingData.s_WallData[25].f_CenterX		= 0.7637
			hackingData.s_WallData[25].f_CenterY		= 0.1870
			hackingData.s_WallData[25].f_Width			= 0.0217
			hackingData.s_WallData[25].f_Height 		= 0.0681

			hackingData.s_WallData[26].f_CenterX		= 0.8290
			hackingData.s_WallData[26].f_CenterY		= 0.2930
			hackingData.s_WallData[26].f_Width			= 0.0215
			hackingData.s_WallData[26].f_Height 		= 0.0686

			hackingData.s_WallData[27].f_CenterX		= 0.7780
			hackingData.s_WallData[27].f_CenterY		= 0.4940
			hackingData.s_WallData[27].f_Width			= 0.0542
			hackingData.s_WallData[27].f_Height 		= 0.1207

			hackingData.s_WallData[28].f_CenterX		= 0.8240
			hackingData.s_WallData[28].f_CenterY		= 0.6610
			hackingData.s_WallData[28].f_Width			= 0.0299
			hackingData.s_WallData[28].f_Height 		= 0.0921

			hackingData.s_WallData[29].f_CenterX		= 0.3549
			hackingData.s_WallData[29].f_CenterY		= 0.7440
			hackingData.s_WallData[29].f_Width			= 0.0322
			hackingData.s_WallData[29].f_Height 		= 0.0864

			hackingData.s_WallData[30].f_CenterX		= 0.4358
			hackingData.s_WallData[30].f_CenterY		= 0.6514
			hackingData.s_WallData[30].f_Width			= 0.0691
			hackingData.s_WallData[30].f_Height 		= 0.0428

			hackingData.s_WallData[31].f_CenterX		= 0.6290
			hackingData.s_WallData[31].f_CenterY		= 0.5832
			hackingData.s_WallData[31].f_Width			= 0.0458
			hackingData.s_WallData[31].f_Height 		= 0.1989

			hackingData.s_WallData[32].f_CenterX		= 0.5213
			hackingData.s_WallData[32].f_CenterY		= 0.3994
			hackingData.s_WallData[32].f_Width			= 0.0798
			hackingData.s_WallData[32].f_Height 		= 0.0648

			hackingData.s_WallData[33].f_CenterX		= 0.0000
			hackingData.s_WallData[33].f_CenterY		= 0.0000
			hackingData.s_WallData[33].f_Width			= 0.0000
			hackingData.s_WallData[33].f_Height 		= 0.0000

			hackingData.s_WallData[34].f_CenterX		= 0.0000
			hackingData.s_WallData[34].f_CenterY		= 0.0000
			hackingData.s_WallData[34].f_Width			= 0.0000
			hackingData.s_WallData[34].f_Height 		= 0.0000

			hackingData.s_WallData[35].f_CenterX		= 0.0000
			hackingData.s_WallData[35].f_CenterY		= 0.0000
			hackingData.s_WallData[35].f_Width			= 0.0000
			hackingData.s_WallData[35].f_Height 		= 0.0000

			hackingData.s_WallData[36].f_CenterX		= 0.0000
			hackingData.s_WallData[36].f_CenterY		= 0.0000
			hackingData.s_WallData[36].f_Width			= 0.0000
			hackingData.s_WallData[36].f_Height 		= 0.0000

			hackingData.s_WallData[37].f_CenterX		= 0.0000
			hackingData.s_WallData[37].f_CenterY		= 0.0000
			hackingData.s_WallData[37].f_Width			= 0.0000
			hackingData.s_WallData[37].f_Height 		= 0.0000

			hackingData.s_WallData[38].f_CenterX		= 0.0000
			hackingData.s_WallData[38].f_CenterY		= 0.0000
			hackingData.s_WallData[38].f_Width			= 0.0000
			hackingData.s_WallData[38].f_Height 		= 0.0000

			hackingData.s_WallData[39].f_CenterX		= 0.0000
			hackingData.s_WallData[39].f_CenterY		= 0.0000
			hackingData.s_WallData[39].f_Width			= 0.0000
			hackingData.s_WallData[39].f_Height 		= 0.0000
		BREAK
		
		CASE 5
			// HEADER - Level: 6

			hackingData.s_SnakeData.f_StartX			= 0.7900
			hackingData.s_SnakeData.f_StartY			= 0.1780
			hackingData.s_SnakeData.f_CenterX			= hackingData.s_SnakeData.f_StartX
			hackingData.s_SnakeData.f_CenterY			= hackingData.s_SnakeData.f_StartY

			hackingData.s_SnakeData.i_StartDirection	= 1
			hackingData.s_SnakeData.i_Direction			= hackingData.s_SnakeData.i_StartDirection
			hackingData.s_SnakeData.f_SpeedX			= 0.0880
			hackingData.s_SnakeData.f_SpeedY			= hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio

			hackingData.s_SnakeData.f_EndX				= 0.1710
			hackingData.s_SnakeData.f_EndY				= 0.2440
			hackingData.s_SnakeData.f_EndWidth			= 0.0260
			hackingData.s_SnakeData.f_EndHeight			= 0.0700


			// WALLS
			hackingData.s_WallData[0].f_CenterX			= 0.1573
			hackingData.s_WallData[0].f_CenterY			= 0.5000
			hackingData.s_WallData[0].f_Width			= 0.0070
			hackingData.s_WallData[0].f_Height 			= 0.6960

			hackingData.s_WallData[1].f_CenterX			= 0.8423
			hackingData.s_WallData[1].f_CenterY			= 0.5000
			hackingData.s_WallData[1].f_Width			= 0.0070
			hackingData.s_WallData[1].f_Height 			= 0.6960

			hackingData.s_WallData[2].f_CenterX			= 0.5003
			hackingData.s_WallData[2].f_CenterY			= 0.1480
			hackingData.s_WallData[2].f_Width			= 0.6905
			hackingData.s_WallData[2].f_Height 			= 0.0100

			hackingData.s_WallData[3].f_CenterX			= 0.5005
			hackingData.s_WallData[3].f_CenterY			= 0.8530
			hackingData.s_WallData[3].f_Width			= 0.6910
			hackingData.s_WallData[3].f_Height 			= 0.0100

			hackingData.s_WallData[4].f_CenterX			= 0.7900
			hackingData.s_WallData[4].f_CenterY			= 0.1690
			hackingData.s_WallData[4].f_Width			= 0.0410
			hackingData.s_WallData[4].f_Height 			= 0.0530

			hackingData.s_WallData[5].f_CenterX			= 0.7227
			hackingData.s_WallData[5].f_CenterY			= 0.3900
			hackingData.s_WallData[5].f_Width			= 0.0500
			hackingData.s_WallData[5].f_Height 			= 0.4769

			hackingData.s_WallData[6].f_CenterX			= 0.8089
			hackingData.s_WallData[6].f_CenterY			= 0.3596
			hackingData.s_WallData[6].f_Width			= 0.0587
			hackingData.s_WallData[6].f_Height 			= 0.0819

			hackingData.s_WallData[7].f_CenterX			= 0.7713
			hackingData.s_WallData[7].f_CenterY			= 0.5076
			hackingData.s_WallData[7].f_Width			= 0.0510
			hackingData.s_WallData[7].f_Height 			= 0.1008

			hackingData.s_WallData[8].f_CenterX			= 0.6870
			hackingData.s_WallData[8].f_CenterY			= 0.7990
			hackingData.s_WallData[8].f_Width			= 0.3008
			hackingData.s_WallData[8].f_Height 			= 0.1046

			hackingData.s_WallData[9].f_CenterX			= 0.7007
			hackingData.s_WallData[9].f_CenterY			= 0.7211
			hackingData.s_WallData[9].f_Width			= 0.1060
			hackingData.s_WallData[9].f_Height 			= 0.0540

			hackingData.s_WallData[10].f_CenterX		= 0.5632
			hackingData.s_WallData[10].f_CenterY		= 0.6991
			hackingData.s_WallData[10].f_Width			= 0.0539
			hackingData.s_WallData[10].f_Height 		= 0.0940

			hackingData.s_WallData[11].f_CenterX		= 0.4796
			hackingData.s_WallData[11].f_CenterY		= 0.6816
			hackingData.s_WallData[11].f_Width			= 0.1175
			hackingData.s_WallData[11].f_Height 		= 0.0587

			hackingData.s_WallData[12].f_CenterX		= 0.5470
			hackingData.s_WallData[12].f_CenterY		= 0.6250
			hackingData.s_WallData[12].f_Width			= 0.0442
			hackingData.s_WallData[12].f_Height 		= 0.0600

			hackingData.s_WallData[13].f_CenterX		= 0.5885
			hackingData.s_WallData[13].f_CenterY		= 0.5550
			hackingData.s_WallData[13].f_Width			= 0.1268
			hackingData.s_WallData[13].f_Height 		= 0.0837

			hackingData.s_WallData[14].f_CenterX		= 0.6261
			hackingData.s_WallData[14].f_CenterY		= 0.4294
			hackingData.s_WallData[14].f_Width			= 0.0519
			hackingData.s_WallData[14].f_Height 		= 0.1730

			hackingData.s_WallData[15].f_CenterX		= 0.6472
			hackingData.s_WallData[15].f_CenterY		= 0.2786
			hackingData.s_WallData[15].f_Width			= 0.0370
			hackingData.s_WallData[15].f_Height 		= 0.1251

			hackingData.s_WallData[16].f_CenterX		= 0.5127
			hackingData.s_WallData[16].f_CenterY		= 0.3840
			hackingData.s_WallData[16].f_Width			= 0.1757
			hackingData.s_WallData[16].f_Height 		= 0.0820

			hackingData.s_WallData[17].f_CenterX		= 0.4650
			hackingData.s_WallData[17].f_CenterY		= 0.3118
			hackingData.s_WallData[17].f_Width			= 0.0240
			hackingData.s_WallData[17].f_Height 		= 0.0629

			hackingData.s_WallData[18].f_CenterX		= 0.5500
			hackingData.s_WallData[18].f_CenterY		= 0.2257
			hackingData.s_WallData[18].f_Width			= 0.0791
			hackingData.s_WallData[18].f_Height 		= 0.1228

			hackingData.s_WallData[19].f_CenterX		= 0.4637
			hackingData.s_WallData[19].f_CenterY		= 0.1888
			hackingData.s_WallData[19].f_Width			= 0.0211
			hackingData.s_WallData[19].f_Height 		= 0.0712

			hackingData.s_WallData[20].f_CenterX		= 0.3855
			hackingData.s_WallData[20].f_CenterY		= 0.3728
			hackingData.s_WallData[20].f_Width			= 0.0215
			hackingData.s_WallData[20].f_Height 		= 0.0702

			hackingData.s_WallData[21].f_CenterX		= 0.3719
			hackingData.s_WallData[21].f_CenterY		= 0.2010
			hackingData.s_WallData[21].f_Width			= 0.0425
			hackingData.s_WallData[21].f_Height 		= 0.0995

			hackingData.s_WallData[22].f_CenterX		= 0.3269
			hackingData.s_WallData[22].f_CenterY		= 0.3440
			hackingData.s_WallData[22].f_Width			= 0.0519
			hackingData.s_WallData[22].f_Height 		= 0.3836

			hackingData.s_WallData[23].f_CenterX		= 0.3505
			hackingData.s_WallData[23].f_CenterY		= 0.6085
			hackingData.s_WallData[23].f_Width			= 0.0510
			hackingData.s_WallData[23].f_Height 		= 0.3014

			hackingData.s_WallData[24].f_CenterX		= 0.4005
			hackingData.s_WallData[24].f_CenterY		= 0.5702
			hackingData.s_WallData[24].f_Width			= 0.0510
			hackingData.s_WallData[24].f_Height 		= 0.0546

			hackingData.s_WallData[25].f_CenterX		= 0.4748
			hackingData.s_WallData[25].f_CenterY		= 0.5469
			hackingData.s_WallData[25].f_Width			= 0.0216
			hackingData.s_WallData[25].f_Height 		= 0.1392

			hackingData.s_WallData[26].f_CenterX		= 0.2159
			hackingData.s_WallData[26].f_CenterY		= 0.8031
			hackingData.s_WallData[26].f_Width			= 0.1140
			hackingData.s_WallData[26].f_Height 		= 0.0896

			hackingData.s_WallData[27].f_CenterX		= 0.1826
			hackingData.s_WallData[27].f_CenterY		= 0.6674
			hackingData.s_WallData[27].f_Width			= 0.0430
			hackingData.s_WallData[27].f_Height 		= 0.1915

			hackingData.s_WallData[28].f_CenterX		= 0.2088
			hackingData.s_WallData[28].f_CenterY		= 0.5968
			hackingData.s_WallData[28].f_Width			= 0.0140
			hackingData.s_WallData[28].f_Height 		= 0.0503

			hackingData.s_WallData[29].f_CenterX		= 0.2506
			hackingData.s_WallData[29].f_CenterY		= 0.6819
			hackingData.s_WallData[29].f_Width			= 0.0216
			hackingData.s_WallData[29].f_Height 		= 0.0688

			hackingData.s_WallData[30].f_CenterX		= 0.3119
			hackingData.s_WallData[30].f_CenterY		= 0.6508
			hackingData.s_WallData[30].f_Width			= 0.0286
			hackingData.s_WallData[30].f_Height 		= 0.0921

			hackingData.s_WallData[31].f_CenterX		= 0.2752
			hackingData.s_WallData[31].f_CenterY		= 0.5175
			hackingData.s_WallData[31].f_Width			= 0.0497
			hackingData.s_WallData[31].f_Height 		= 0.0920

			hackingData.s_WallData[32].f_CenterX		= 0.1871
			hackingData.s_WallData[32].f_CenterY		= 0.4220
			hackingData.s_WallData[32].f_Width			= 0.0526
			hackingData.s_WallData[32].f_Height 		= 0.1849

			hackingData.s_WallData[33].f_CenterX		= 0.2287
			hackingData.s_WallData[33].f_CenterY		= 0.3770
			hackingData.s_WallData[33].f_Width			= 0.0368
			hackingData.s_WallData[33].f_Height 		= 0.0940

			hackingData.s_WallData[34].f_CenterX		= 0.2924
			hackingData.s_WallData[34].f_CenterY		= 0.2899
			hackingData.s_WallData[34].f_Width			= 0.0180
			hackingData.s_WallData[34].f_Height 		= 0.0752

			hackingData.s_WallData[35].f_CenterX		= 0.2360
			hackingData.s_WallData[35].f_CenterY		= 0.3090
			hackingData.s_WallData[35].f_Width			= 0.0222
			hackingData.s_WallData[35].f_Height 		= 0.0562

			hackingData.s_WallData[36].f_CenterX		= 0.2432
			hackingData.s_WallData[36].f_CenterY		= 0.1858
			hackingData.s_WallData[36].f_Width			= 0.0221
			hackingData.s_WallData[36].f_Height 		= 0.0667

			hackingData.s_WallData[37].f_CenterX		= 0.8108
			hackingData.s_WallData[37].f_CenterY		= 0.6542
			hackingData.s_WallData[37].f_Width			= 0.0541
			hackingData.s_WallData[37].f_Height 		= 0.0815

			hackingData.s_WallData[38].f_CenterX		= 0.6899
			hackingData.s_WallData[38].f_CenterY		= 0.6045
			hackingData.s_WallData[38].f_Width			= 0.0194
			hackingData.s_WallData[38].f_Height 		= 0.0493

			hackingData.s_WallData[39].f_CenterX		= 0.4123
			hackingData.s_WallData[39].f_CenterY		= 0.7818
			hackingData.s_WallData[39].f_Width			= 0.1749
			hackingData.s_WallData[39].f_Height 		= 0.0504
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_MINIGAME_LEVEL() - CHECK 1 - Loading Level: ", i_Level)
ENDPROC



/// HAS_PLAYER_BEAT_CIRCUIT_HACKING (pass in your own hacking data struct)
/// NOTES: checks if a player has beaten the circuit hacking minigame.
/// PURPOSE:
///   Checks if a player has beaten the new hacking mini game
/// PARAMS:
///   &hackingData		-	STRUCT: hacking data
///    b_AndForceQuit	-	BOOL: Pass TRUE to terminate hacking on success
/// RETURNS:
///    BOOL: TRUE on player beating new hacking
FUNC BOOL HAS_PLAYER_BEAT_CIRCUIT_HACKING(S_HACKING_DATA &hackingData, BOOL b_AndForceQuit = FALSE)
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
		IF b_AndForceQuit 
			FORCE_QUIT_PASS_HACKING_MINIGAME(hackingData)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// HAS_PLAYER_FAILED_CIRCUIT_HACKING (pass in your own hacking data struct)
/// NOTES: checks if a player has failed the circuit hacking minigame
/// PURPOSE:
///   Checks if a player has beaten the new hacking mini game
/// PARAMS:
///   &hackingData		-	STRUCT: Hacking data struct
///    b_AndForceQuit	-	BOOL: pass TRUE to terminate hacking on success
/// RETURNS:
///	   BOOL: TRUE on player beating new hacking
FUNC BOOL HAS_PLAYER_FAILED_CIRCUIT_HACKING(S_HACKING_DATA &hackingData, BOOL b_AndForceQuit = FALSE)
	IF IS_BIT_SET(hackingData.bsHacking, BS_MP_HACK_FAIL)
	//IF hackingData.b_MP_Hack_fail
		IF b_AndForceQuit 
			FORCE_QUIT_FAIL_HACKING_MINIGAME(hackingData)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Checks if the circuit hacking minigame is currently running
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
/// RETURNS:
///    BOOL: TRUE if the minigame is currently running
FUNC BOOL IS_CIRCUIT_HACKING_MINIGAME_RUNNING(S_HACKING_DATA &hackingData)
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Lets the Hacking Ped know when its safe to use TASK_USE_MOBILE_PHONE without the prop phone being cleaned up
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
/// RETURNS:
///    BOOL: TRUE if the minigame has finished the launching state
FUNC BOOL IS_SAFE_TO_RUN_FAKE_PHONE_TASK(S_HACKING_DATA &hackingData)
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SAFE_TO_USE_PHONE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Resets the bit used in the MP Controller to increment a fail stat
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC CHECK_SHOULD_RESET_LEVEL_FAIL_BIT(S_HACKING_DATA &hackingData)
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_FAILED)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CHECK_SHOULD_RESET_LEVEL_FAIL_BIT() - CHECK 1 - Clearing BIT_SET_CIRCUIT_LEVEL_FAILED Bit")
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_FAILED)
	ENDIF
ENDPROC



/// PURPOSE:
///    Quits the game by setting the game to be solved and then cleans up all assets
/// PARAMS:
///   &hackingData				-	STRUCT: Hacking data struct
///    b_UseDifficultLevels		-	BOOL: Determines the use of the final three levels (uses first three levels if FALSE)
///    b_ReturnPlayerControl	-	BOOL: Enable player controls on cleanup
PROC CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels, BOOL b_ReturnPlayerControl = TRUE)
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Circuit Hacking Minigame Cleanup and Quit Called by [", GET_THIS_SCRIPT_NAME(), "]")
	
	// RE-ENABLE PLAYER CONTROL
	IF b_ReturnPlayerControl
	AND IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	
	#IF IS_NEXTGEN_BUILD
		// DISABLE MULTIPLE DISPLAYS/BLINDERS
		SET_MULTIHEAD_SAFE(FALSE, TRUE)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Disabled SET_MULTIHEAD_SAFE")
	#ENDIF
	
	
	// RE-ENABLE THE INTERACTION MENU
	ENABLE_INTERACTION_MENU()
	
	
	// UNLOAD GENERAL MINI GAME SPRITE DICT
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack")
	
	// UNLOAD TUTORIAL OR FINALE LEVEL SPRITES
	IF b_UseDifficultLevels
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack3")
	
	ELSE
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack2")
	ENDIF
	
	
	// RELEASE AUDIO BANK
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")
	
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("HACK", MINIGAME_TEXT_SLOT)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_1")
		#IF IS_NEXTGEN_BUILD
		OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_1_KM") OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_3_KM")))
		#ENDIF
			CLEAR_HELP()
		ENDIF
	ENDIF

	// UNLOAD MINIGAME TEXT
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	
	
	// UNLOAD THE SCALEFORM MOVIE
	CLEANUP_CIRCUIT_HACKING_SCALEFORM()
	
	
	// STOP AUDIO SCENE
	STOP_AUDIO_SCENE("DLC_HEISTS_SNAKE_HACKING_SCENE")
	
	
	// STOP BACKGROUND AUDIO
	STOP_SOUND(hackingData.i_node)
	
	
	// STOP TRAIL LOOP
	STOP_SOUND(hackingData.i_hacking_sound)
	
	
	// RESET PHONE LAUNCH VARIABLES
	RESTART_TIMER_AT(hackingData.t_PhoneLaunchTimer, 0.0)
	PAUSE_TIMER(hackingData.t_PhoneLaunchTimer)
	hackingData.e_IntroLaunchState = LIS_INITIALISE
	
	
	// RESET CURRENT GAME STATE
	hackingData.e_CircuitHackingMinigameState = CHMS_INITIALISE
	
	
	// HANDLES ALL THE GAME CLEANUP AND SETS THE EXIT BIT
	CIRCUIT_MINIGAME_RESET(hackingData, FALSE, FALSE, TRUE, TRUE)
	
	
	// RESET SCALEFORM INDEX - MAKES SURE ALL REFERENCES ARE CLEANED UP
	sfHacking = NULL
	
	
	// ENABLE PHONE INSTRUCTIONS AGAIN
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	
	
	// CLEAR THE INTERNET APP BLOCKING
    MP_FORCE_TERMINATE_INTERNET_CLEAR()
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Calling MP_FORCE_TERMINATE_INTERNET_CLEAR()")
	
	
	// ENABLE THE RANK AND AWARD NOTIFICATIONS
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME() - CHECK 4 - Enable Rank and Award Notifications")
	
	
	// CLEAR THE MINIGAME RUNNING BIT
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
		CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME() - CHECK 5 - Clearing BIT_SET_CIRCUIT_GAME_RUNNING Bit")
	ENDIF
ENDPROC



/// PURPOSE:
///    Check to see if two rectangles overlap with each other
/// PARAMS:
///    f_CentreX1	-	FLOAT: X coord center point of rectangle 1
///    f_CentreY1	-	FLOAT: Y coord center point of rectangle 1
///    f_Width1		-	FLOAT: Width of rectangle 1
///    f_Height1	-	FLOAT: Height of rectangle 1
///    f_CentreX	-	FLOAT: X coord center point of rectangle 2
///    f_CentreY2	-	FLOAT: Y coord center point of rectangle 2
///    f_Width2		-	FLOAT: Width of rectangle 1
///    f_Height2	-	FLOAT: Width of rectangle 1
/// RETURNS:
///    BOOL: TRUE if the two rectangles overlap
FUNC BOOL DO_RECTANGLES_OVERLAP(FLOAT f_CentreX1, FLOAT f_CentreY1, FLOAT f_Width1, FLOAT f_Height1, FLOAT f_CentreX2, FLOAT f_CentreY2, FLOAT f_Width2, FLOAT f_Height2)
	IF (f_CentreX1 + (f_Width1 * 0.5)) < (f_CentreX2 - (f_Width2 * 0.5))
	    RETURN FALSE
	ENDIF

	IF (f_CentreX2 + (f_Width2 * 0.5)) < (f_CentreX1 - (f_Width1 * 0.5))
	    RETURN FALSE
	ENDIF

	IF (f_CentreY1 + (f_Height1 * 0.5)) < (f_CentreY2 - (f_Height2 * 0.5))
	    RETURN FALSE
	ENDIF

	IF (f_CentreY2 + (f_Height2 * 0.5)) < (f_CentreY1 - (f_Height1 * 0.5))
	    RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    Main logic update loop. Handles calculations and collision checks - (excludes tail collision checks)
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC UPDATE_GAME_LOGIC_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL bHeists2 = FALSE)
	INT i_Counter
	
	
	// START SNAKE MOVING
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		#IF IS_NEXTGEN_BUILD OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)) #ENDIF )
	AND NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
		SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
		
		
		// STOP START PORT LIGHTS FLASHING AND START END PORTS
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_END_PORT_LIGHTS)
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_END_PORT_LIGHTS)
		ENDIF
		
		
		// PLAY START AUDIO
		PLAY_SOUND_FRONTEND(-1, "Start", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
		
		
		// START TRAIL LOOP AUDIO
		PLAY_SOUND_FRONTEND(hackingData.i_hacking_sound, "Trail_Custom", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
		
		
		// HANDLE PAD VIBRATIONS
		DO_CIRCUIT_HACKING_CONTROLLER_VIBRATIONS(1, 150)
		
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_LOGIC() - CHECK 1 - Start: Player pressed button to start moving")
	ENDIF
	
	
	// SET MOVING SOUND INTENSITY
	IF NOT HAS_SOUND_FINISHED(hackingData.i_hacking_sound)
		SWITCH hackingData.i_CircuitCurrentLevel
			CASE 0
				SET_VARIABLE_ON_SOUND(hackingData.i_hacking_sound, "SnakeSpeed", 0.16666666666666666666666666666667)
			BREAK
			
			CASE 1
				SET_VARIABLE_ON_SOUND(hackingData.i_hacking_sound, "SnakeSpeed", 0.33333333333333333333333333333333)
			BREAK
			
			CASE 2
				SET_VARIABLE_ON_SOUND(hackingData.i_hacking_sound, "SnakeSpeed", 0.5)
			BREAK
			
			CASE 3
				SET_VARIABLE_ON_SOUND(hackingData.i_hacking_sound, "SnakeSpeed", 0.66666666666666666666666666666667)
			BREAK
			
			CASE 4
				SET_VARIABLE_ON_SOUND(hackingData.i_hacking_sound, "SnakeSpeed", 0.83333333333333333333333333333333)
			BREAK
			
			CASE 5
				SET_VARIABLE_ON_SOUND(hackingData.i_hacking_sound, "SnakeSpeed", 1.0)
			BREAK
		ENDSWITCH
	ENDIF
	
	
	// MOVING LOGIC
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
		INT i_prevSnakeDirection = hackingData.s_SnakeData.i_Direction
		
		
		// ANALOG STICK VALUES
		FLOAT f_LeftStickX, f_LeftStickY
		f_LeftStickX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		f_LeftStickY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		
		
		// CHECK FOR DIRECTION CHANGES COMING FROM THE PLAYER
		// UP
		IF ((f_LeftStickY < 0 AND ABSF(f_LeftStickY) > 0.80)
			#IF IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) #ENDIF
			#IF NOT IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP) #ENDIF )
		AND hackingData.s_SnakeData.i_Direction != 1
		AND NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
			ENDIF
			
	        hackingData.s_SnakeData.i_NewSnakeDirection = 0
		
		// DOWN
	    ELIF ((f_LeftStickY > 0 AND ABSF(f_LeftStickY) > 0.80)
			#IF IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) #ENDIF
			#IF NOT IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN) #ENDIF )
		AND hackingData.s_SnakeData.i_Direction != 0
		AND NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
			ENDIF
			
	        hackingData.s_SnakeData.i_NewSnakeDirection = 1
		
		// LEFT
		ELIF ((f_LeftStickX < 0 AND ABSF(f_LeftStickX) > 0.80)
			#IF IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) #ENDIF
			#IF NOT IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT) #ENDIF )
		AND hackingData.s_SnakeData.i_Direction != 3
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
			ENDIF
			
	        hackingData.s_SnakeData.i_NewSnakeDirection = 2
		
		// RIGHT
		ELIF ((f_LeftStickX > 0 AND ABSF(f_LeftStickX) > 0.80)
			#IF IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) #ENDIF
			#IF NOT IS_NEXTGEN_BUILD OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT) #ENDIF )
		AND hackingData.s_SnakeData.i_Direction != 2
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
			ENDIF
			
	        hackingData.s_SnakeData.i_NewSnakeDirection = 3
	    ENDIF
		
		
		// HANDLE TURN DELAY SO HEAD DOESN'T TURN BACK ON SELF
		IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
		AND hackingData.s_SnakeData.i_TailIndex > 0
			FLOAT f_TailCenterX, f_TailCenterY
			FLOAT_COMPRESSION_UNPACK(hackingData.s_SnakeData.f_Tail[hackingData.s_SnakeData.i_TailIndex - 1], f_TailCenterX, f_TailCenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
			
			SWITCH hackingData.s_SnakeData.i_NewSnakeDirection
				CASE 0
				CASE 1
					// TURNING VERTICAL
					IF ABSF(hackingData.s_SnakeData.f_CenterX - f_TailCenterX) > (hackingData.s_SnakeData.f_Width * hackingData.f_CurrentAspectRatio) * 0.3
						CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
						
						hackingData.s_SnakeData.i_Direction = hackingData.s_SnakeData.i_NewSnakeDirection
						
						// TURN AUDIO
						PLAY_SOUND_FRONTEND(-1, "Turn", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					ENDIF
				BREAK
				
				
				CASE 2
				CASE 3
					// TURNING HORIZONTAL
					IF ABSF(hackingData.s_SnakeData.f_CenterY - f_TailCenterY) > (hackingData.s_SnakeData.f_Width * hackingData.f_CurrentAspectRatio) * (0.3 * hackingData.f_CurrentAspectRatio)
						CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_CHANGE_DIRECTION)
						
						hackingData.s_SnakeData.i_Direction = hackingData.s_SnakeData.i_NewSnakeDirection
						
						// TURN AUDIO
						PLAY_SOUND_FRONTEND(-1, "Turn", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		
		// STORE INITIAL TAIL INDEX
		IF hackingData.s_SnakeData.i_TailIndex = 0
			hackingData.s_SnakeData.f_Tail[hackingData.s_SnakeData.i_TailIndex] = FLOAT_COMPRESSION_PACK(hackingData.s_SnakeData.f_CenterX, hackingData.s_SnakeData.f_CenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
			hackingData.s_SnakeData.i_TailIndex++
		ENDIF
		
		
		// IF WE CHANGED DIRECTION THIS FRAME THEN ADD THE CURRENT HEAD POSITION AS A NODE IN THE SNAKE TAIL ARRAY
		IF i_prevSnakeDirection != hackingData.s_SnakeData.i_Direction
			// THE SNAKE ARRAY IS FULL, WE NEED TO DELETE THE FIRST ELEMENT OF THE ARRAY AND PUSH EVERYTHING BACK
			IF hackingData.s_SnakeData.i_TailIndex >= NUM_MAX_TAIL_RECTS
				FOR i_Counter = 0 TO (NUM_MAX_TAIL_RECTS - 1)
					IF i_Counter = NUM_MAX_TAIL_RECTS - 1
						hackingData.s_SnakeData.f_Tail[i_Counter] = FLOAT_COMPRESSION_PACK(hackingData.s_SnakeData.f_CenterX, hackingData.s_SnakeData.f_CenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
						
					ELSE
						FLOAT f_TailCenterX = 0, f_TailCenterY = 0
						FLOAT_COMPRESSION_UNPACK(hackingData.s_SnakeData.f_Tail[i_Counter + 1], f_TailCenterX, f_TailCenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
						
						hackingData.s_SnakeData.f_Tail[i_Counter] = FLOAT_COMPRESSION_PACK(f_TailCenterX, f_TailCenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
					ENDIF
				ENDFOR
				
			ELSE
				// WE STILL HAVE SPACE IN THE SNAKE ARRAY, SO JUST ASSIGN THE CURRENT HEAD POSITION TO THE NEXT EMPTY ELEMENT
				hackingData.s_SnakeData.f_Tail[hackingData.s_SnakeData.i_TailIndex] = FLOAT_COMPRESSION_PACK(hackingData.s_SnakeData.f_CenterX, hackingData.s_SnakeData.f_CenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
				
				hackingData.s_SnakeData.i_TailIndex++
			ENDIF
		ENDIF
		
		
		// UPDATE THE SNAKE HEAD DIRECTION, SPEED & SPRITE ROATAION
	    SWITCH hackingData.s_SnakeData.i_Direction
	        CASE 0 // UP
	        	hackingData.s_SnakeData.f_CenterY = hackingData.s_SnakeData.f_CenterY -@ hackingData.s_SnakeData.f_SpeedY
	            hackingData.s_SnakeData.f_HeadRotation = 0.0
	        BREAK
	        
			
	        CASE 1 // DOWN
	            hackingData.s_SnakeData.f_CenterY = hackingData.s_SnakeData.f_CenterY +@ hackingData.s_SnakeData.f_SpeedY
	            hackingData.s_SnakeData.f_HeadRotation = 180.0
	        BREAK
	        
			
	        CASE 2 // LEFT
	            hackingData.s_SnakeData.f_CenterX = hackingData.s_SnakeData.f_CenterX -@ hackingData.s_SnakeData.f_SpeedX
	            hackingData.s_SnakeData.f_HeadRotation = 270.0
	        BREAK
	        
			
	        CASE 3 // RIGHT
	            hackingData.s_SnakeData.f_CenterX = hackingData.s_SnakeData.f_CenterX +@ hackingData.s_SnakeData.f_SpeedX
	            hackingData.s_SnakeData.f_HeadRotation = 90.0
	        BREAK
	    ENDSWITCH
	ENDIF
	
	
	// WIN CHECK LOGIC - COLLIDED WITH EXIT PORT
	IF (NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_PASSED)
	AND IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
	AND DO_RECTANGLES_OVERLAP((0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, (hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier) * 0.52, hackingData.s_SnakeData.f_Height * 0.52,
								(0.5 - ((0.5 - hackingData.s_SnakeData.f_EndX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_EndY, hackingData.s_SnakeData.f_EndWidth * hackingData.f_AspectRatioModifier, hackingData.s_SnakeData.f_EndHeight))
		
		// SET LEVEL PASSED
		SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_PASSED)
		
		// STOP SNAKE MOVING
		IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
			CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
		ENDIF
		
		// REMOVE SNAKE HEAD ON WIN
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_REMOVE_HEAD)
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_REMOVE_HEAD)
		ENDIF
		
		// HANDLE PAD VIBRATIONS
		DO_CIRCUIT_HACKING_CONTROLLER_VIBRATIONS(500, 50)
		
		// SET PASS TIMER
		hackingData.i_CircuitPassedLevelTimer = GET_GAME_TIMER()
	
		// PLAY GOAL AUDIO
		PLAY_SOUND_FRONTEND(-1, "Goal", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
		
		// STOP TRAIL LOOP AUDIO
		STOP_SOUND(hackingData.i_hacking_sound)
			
		IF NOT bHeists2
			
			// MOVE TO PASS STATE
			hackingData.e_CircuitHackingMinigameState = CHMS_PASS
			
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_LOGIC() - CHECK 2 - Win: Collided against End Point")
		ELSE
			// JUMP TO FINAL COMPLETION SPRITES
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_LOGIC() - CHECK 3 - Skipping to Final Screen due to Heists 2")
			
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_LOGIC() - CHECK 4 - Clearing MINIGAME_SOLVED Bit")
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
			ENDIF
			
			hackingData.i_WinScreenDelay = GET_GAME_TIMER()
		
			// DRAW FINALE CONNECTED CIRCUIT SPRITE
			hackingData.e_CircuitHackingMinigameState = CHMS_SYSTEM_HACKED
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED)
			
		ENDIF
		
		
			
	ENDIF
	
	
	// STOP WALL COLLISION UNTIL HEAD HAS LEFT ENTRANCE PORT COLLISION BOX
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ENTRY_COLLISION)
		IF NOT DO_RECTANGLES_OVERLAP((0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, (hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier) * 0.52, hackingData.s_SnakeData.f_Height * 0.52,
										(0.5 - ((0.5 - hackingData.s_WallData[4].f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_WallData[4].f_CenterY, hackingData.s_WallData[4].f_Width * hackingData.f_AspectRatioModifier, hackingData.s_WallData[4].f_Height)
			
			// SET COLLISION ACTIVE
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ENTRY_COLLISION)
			
			
			// ALLOW PLAYER TO TURN UP AND DOWN AGAIN
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
			ENDIF
		ENDIF
	ENDIF
	
	
	// WALL COLLISION FAIL CHECKS LOGIC
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ENTRY_COLLISION)
		FOR i_Counter = 0 TO (NUM_MAX_WALLS - 1)
			IF DO_RECTANGLES_OVERLAP((0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, (hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier) * 0.52, hackingData.s_SnakeData.f_Height * 0.52, 
										(0.5 - ((0.5 - hackingData.s_WallData[i_Counter].f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_WallData[i_Counter].f_CenterY, hackingData.s_WallData[i_Counter].f_Width * hackingData.f_AspectRatioModifier, hackingData.s_WallData[i_Counter].f_Height)
				
				// STOP SNAKE MOVING
				IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
					CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
				ENDIF
				
				
				// STOP TRAIL LOOP AUDIO
				STOP_SOUND(hackingData.i_hacking_sound)
				
				
				// PLAY CRASH AUDIO
				PLAY_SOUND_FRONTEND(-1, "Crash", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
				
				
				// SET PLAY SNAKE DEAD ANIM
				IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
					SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
				ENDIF
				
				
				// HANDLE PAD VIBRATIONS
				DO_CIRCUIT_HACKING_CONTROLLER_VIBRATIONS(1000, 100)
				
				
				// SET PASS TIMER
				hackingData.i_CircuitPassedLevelTimer = GET_GAME_TIMER()
				
				
				// MOVE TO FAIL STATE
				hackingData.e_CircuitHackingMinigameState = CHMS_FAIL
				
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_LOGIC() - CHECK 3 - Fail: Collided against wall: ", i_Counter)
			ENDIF
		ENDFOR
	ENDIF
	
	
	// "TOO HARD" CHECK FOR THE THREE FINALE LEVELS
	IF hackingData.i_CircuitCurrentLevel >= 3
		IF hackingData.i_lives >= 5
		AND hackingData.s_SnakeData.f_SpeedX <> (0.082 * hackingData.f_AspectRatioModifier)
			// MAKE SNAKE MOVE SLIGHTLY SLOWER
			hackingData.s_SnakeData.f_SpeedX = (0.082 * hackingData.f_AspectRatioModifier)
			hackingData.s_SnakeData.f_SpeedY = hackingData.s_SnakeData.f_SpeedX * hackingData.f_CurrentAspectRatio
			
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_LOGIC() - CHECK 4 - Player has Died >= 5 Times: Reducing Snake Movement Speed")
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Main draw function loop. HAndles drawing all sprites
/// PARAMS:
///   &hackingData			-	STRUCT: Hacking data struct
///    b_DrawTail			-	BOOL: Pass TRUE to draw the tail
///    b_DrawWallCollisions	-	BOOL: Pass TRUE to draw white boxes where the collision is setup - DEBUG ONLY
PROC UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels = FALSE, BOOL b_DrawTail = TRUE, BOOL bHeist2 = FALSE #IF IS_DEBUG_BUILD, BOOL b_DrawWallCollisions = FALSE #ENDIF )
	INT i_Counter
	
	
	// DRAW BLACK RECT BACKGROUND - COVERS WHERE BACKGROUND SPRITE STOPS
	DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, 255)
	
	
	// DRAW LEVEL BACKGROUND SPRITE
	SWITCH hackingData.i_CircuitCurrentLevel
		CASE 0
			DRAW_SPRITE("MPCircuitHack2", "CBLevel1", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		BREAK
		
		CASE 1
			DRAW_SPRITE("MPCircuitHack2", "CBLevel2", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		BREAK
		
		CASE 2
			DRAW_SPRITE("MPCircuitHack2", "CBLevel3", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		BREAK
		
		CASE 3
			DRAW_SPRITE("MPCircuitHack3", "CBLevel4", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		BREAK
		
		CASE 4
			DRAW_SPRITE("MPCircuitHack3", "CBLevel5", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		BREAK
		
		CASE 5
			DRAW_SPRITE("MPCircuitHack3", "CBLevel6", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		BREAK
	ENDSWITCH
	
	
	// DRAW SNAKE TAIL SEGMENTS
	IF b_DrawTail
		i_Counter = 0
		
		FLOAT f_CentreX, f_CentreY
		FLOAT f_SegmentLength, f_SegmentHeight, f_TailRotation = 0
		FLOAT f_NextNodeX, f_NextNodeY
		
		
		// CYCLE THROUGH SNAKE TAIL SEGMENTS
		WHILE i_Counter < NUM_MAX_TAIL_RECTS
		AND (FLOAT_COMPRESSION_UNPACK_RETURN_X(hackingData.s_SnakeData.f_Tail[i_Counter], hackingData.s_SnakeData.i_CompressPrecisionValue) != 0.0 OR FLOAT_COMPRESSION_UNPACK_RETURN_Y(hackingData.s_SnakeData.f_Tail[i_Counter], hackingData.s_SnakeData.i_CompressPrecisionValue) != 0.0)
			// IF THE NEXT NODE IS INVALID THEN THE CURRENT NODE NEEDS TO BE CONNECTED TO THE SNAKE HEAD
			IF i_Counter + 1 >= NUM_MAX_TAIL_RECTS
			OR (FLOAT_COMPRESSION_UNPACK_RETURN_X(hackingData.s_SnakeData.f_Tail[i_Counter + 1], hackingData.s_SnakeData.i_CompressPrecisionValue) = 0.0 AND FLOAT_COMPRESSION_UNPACK_RETURN_Y(hackingData.s_SnakeData.f_Tail[i_Counter + 1], hackingData.s_SnakeData.i_CompressPrecisionValue) = 0.0)
				f_NextNodeX = hackingData.s_SnakeData.f_CenterX
				f_NextNodeY = hackingData.s_SnakeData.f_CenterY
			
			ELSE
				f_NextNodeX = FLOAT_COMPRESSION_UNPACK_RETURN_X(hackingData.s_SnakeData.f_Tail[i_Counter + 1], hackingData.s_SnakeData.i_CompressPrecisionValue)
				f_NextNodeY = FLOAT_COMPRESSION_UNPACK_RETURN_Y(hackingData.s_SnakeData.f_Tail[i_Counter + 1], hackingData.s_SnakeData.i_CompressPrecisionValue)
			ENDIF
			
			
			FLOAT f_TailCenterX = 0, f_TailCenterY = 0
			FLOAT_COMPRESSION_UNPACK(hackingData.s_SnakeData.f_Tail[i_Counter], f_TailCenterX, f_TailCenterY, hackingData.s_SnakeData.i_CompressPrecisionValue)
			
			
			// CONNECT THIS NODE TO THE NEXT NODE
			f_CentreX = f_TailCenterX + ((f_NextNodeX - f_TailCenterX) * 0.5)
			f_CentreY = f_TailCenterY + ((f_NextNodeY - f_TailCenterY) * 0.5)
			
			
			// SEGMENT IS HORIZONTAL: USE THE DIFFERENCE IN X TO WORK OUT THE LENGTH
			IF (ABSF(f_TailCenterX - f_NextNodeX) > 0.001)
				f_TailRotation = 90.0
				
				f_SegmentLength = ABSF(f_NextNodeX - f_TailCenterX) + (0.003 * 0.5)
				f_SegmentHeight = (0.003 * hackingData.f_CurrentAspectRatio)
			ENDIF
			
			
			// SEGMENT IS VERTICAL: USE THE DIFFERENCE IN Y TO WORK OUT THE HEIGHT
			IF (ABSF(f_TailCenterY - f_NextNodeY) > 0.001)
				f_TailRotation = 0.0
				
				f_SegmentLength = 0.003
				
				f_SegmentHeight = ABSF(f_NextNodeY - f_TailCenterY) + (0.003 * 0.5 * hackingData.f_CurrentAspectRatio)
			ENDIF
			
			
			// DRAW SNAKE TAIL
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
				DRAW_SPRITE("MPCircuitHack", "Tail", (0.5 - ((0.5 - f_CentreX) * hackingData.f_AspectRatioModifier)), f_CentreY, f_SegmentLength * hackingData.f_AspectRatioModifier, f_SegmentHeight, f_TailRotation, 45, 203, 134, hackingData.s_SnakeData.i_Alpha)
			ELSE
				DRAW_SPRITE("MPCircuitHack", "Tail", (0.5 - ((0.5 - f_CentreX) * hackingData.f_AspectRatioModifier)), f_CentreY, f_SegmentLength * hackingData.f_AspectRatioModifier, f_SegmentHeight, f_TailRotation, 188, 49, 43, hackingData.s_SnakeData.i_Alpha)
			ENDIF
			
			
			// DO TAIL COLLISION LOGIC HERE TO SAVE VARIABLES
			// HORIZONTAL
			IF ((hackingData.s_SnakeData.i_Direction = 2 OR hackingData.s_SnakeData.i_Direction = 3)
				AND DO_RECTANGLES_OVERLAP((0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, (hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier) * 0.52, hackingData.s_SnakeData.f_Height * 0.52,
											(0.5 - ((0.5 - f_CentreX) * hackingData.f_AspectRatioModifier)), f_CentreY, (f_SegmentLength * 0.95) * hackingData.f_AspectRatioModifier, f_SegmentHeight * (0.565 * hackingData.f_CurrentAspectRatio)))
			
			// VERTICAL
			OR ((hackingData.s_SnakeData.i_Direction = 0 OR hackingData.s_SnakeData.i_Direction = 1)
				AND DO_RECTANGLES_OVERLAP((0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, (hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier) * 0.52, hackingData.s_SnakeData.f_Height * 0.52,  
											(0.5 - ((0.5 - f_CentreX) * hackingData.f_AspectRatioModifier)), f_CentreY, (f_SegmentLength * (0.565 * hackingData.f_CurrentAspectRatio)) * hackingData.f_AspectRatioModifier, f_SegmentHeight * 0.95))
				
				// IF NOT THE NEWEST TAIL SEGMENT						
				IF  i_Counter < (hackingData.s_SnakeData.i_TailIndex - 3)
				AND IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
					// STOP SNAKE MOVING
					IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
						CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
					ENDIF
					
					
					// STOP TRAIL LOOP AUDIO
					STOP_SOUND(hackingData.i_hacking_sound)
					
					
					// PLAY CRASH AUDIO
					PLAY_SOUND_FRONTEND(-1, "Crash", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					
					
					// SET PLAY SNAKE DEAD ANIM
					IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
						SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
					ENDIF
					
					
					// HANDLE PAD VIBRATIONS
					DO_CIRCUIT_HACKING_CONTROLLER_VIBRATIONS(1000, 100)
					
					
					// SET PASS TIMER
					hackingData.i_CircuitPassedLevelTimer = GET_GAME_TIMER()
					
					
					// MOVE TO FAIL STATE
					hackingData.e_CircuitHackingMinigameState = CHMS_FAIL
					
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_GAME_DISPLAY() - CHECK 1 - Fail: Collided against tail segment: ", i_Counter)
				ENDIF
			ENDIF
			
			i_Counter++
		ENDWHILE
	ENDIF
	
	
	// DRAW SNAKE HEAD
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
		// DRAW GREEN SNAKE HEAD - IF NOT PASSED LEVEL
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_REMOVE_HEAD)
			DRAW_SPRITE("MPCircuitHack", "Head", (0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier, hackingData.s_SnakeData.f_Height, hackingData.s_SnakeData.f_HeadRotation, 45, 203, 134, hackingData.s_SnakeData.i_Alpha)
		ENDIF
	ELSE
		// DRAW RED SNAKE HEAD FOR FAILS
		DRAW_SPRITE("MPCircuitHack", "Head", (0.5 - ((0.5 - hackingData.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_CenterY, hackingData.s_SnakeData.f_Width * hackingData.f_AspectRatioModifier, hackingData.s_SnakeData.f_Height, hackingData.s_SnakeData.f_HeadRotation, 188, 49, 43, hackingData.s_SnakeData.i_Alpha)
	ENDIF
	
	
	// PORT VARIABLES - START & END
	FLOAT f_StartPortX
	FLOAT f_StartPortY
	FLOAT f_StartPortWidth
	FLOAT f_StartPortHeight
	FLOAT f_StartPortRotation
	FLOAT f_EndPortX
	FLOAT f_EndPortY
	FLOAT f_EndPortWidth
	FLOAT f_EndPortHeight
	FLOAT f_EndPortRotation
	
	
	// PORT LIGHT VARIABLES - START & END
	FLOAT f_LightWidth = 0.0080
	FLOAT f_LightHeight = f_LightWidth * hackingData.f_CurrentAspectRatio
	
	FLOAT f_StartPortLightOneX
	FLOAT f_StartPortLightOneY
	FLOAT f_StartPortLightTwoX
	FLOAT f_StartPortLightTwoY
	FLOAT f_EndPortLightOneX
	FLOAT f_EndPortLightOneY
	FLOAT f_EndPortLightTwoX
	FLOAT f_EndPortLightTwoY
	
	
	// ASSIGN PORT DATA PER LEVEL
	SWITCH hackingData.i_CircuitCurrentLevel
		CASE 0	// LEVEL 1
			// PORTS
			f_StartPortX = 0.1710
			f_StartPortY = 0.7380
			f_StartPortWidth = 0.0300
			f_StartPortHeight = 0.0800
			f_StartPortRotation = 0.0000
			f_EndPortX = 0.8290
			f_EndPortY = 0.2320
			f_EndPortWidth = 0.0300
			f_EndPortHeight = 0.0800
			f_EndPortRotation = 180.0000
			
			// PORT LIGHTS
			f_StartPortLightOneX = 0.1811
			f_StartPortLightOneY = 0.7065
			f_StartPortLightTwoX = 0.1810
			f_StartPortLightTwoY = 0.7700
			f_EndPortLightOneX = 0.8195
			f_EndPortLightOneY = 0.2000
			f_EndPortLightTwoX = 0.8195
			f_EndPortLightTwoY = 0.2640
		BREAK
		
		
		CASE 1	// LEVEL 2
			// PORTS
			f_StartPortX = 0.1710
			f_StartPortY = 0.7380
			f_StartPortWidth = 0.0300
			f_StartPortHeight = 0.0800
			f_StartPortRotation = 0.0000
			f_EndPortX = 0.8290
			f_EndPortY = 0.3010
			f_EndPortWidth = 0.0300
			f_EndPortHeight = 0.0800
			f_EndPortRotation = 180.0000
			
			// PORT LIGHTS
			f_StartPortLightOneX = 0.1811
			f_StartPortLightOneY = 0.7065
			f_StartPortLightTwoX = 0.1810
			f_StartPortLightTwoY = 0.7700
			f_EndPortLightOneX = 0.8195
			f_EndPortLightOneY = 0.2695
			f_EndPortLightTwoX = 0.8195
			f_EndPortLightTwoY = 0.3330
		BREAK
		
		
		CASE 2	// LEVEL 3
			// PORTS
			f_StartPortX = 0.1710
			f_StartPortY = 0.7720
			f_StartPortWidth = 0.0300
			f_StartPortHeight = 0.0800
			f_StartPortRotation = 0.0000
			f_EndPortX = 0.1710
			f_EndPortY = 0.2300
			f_EndPortWidth = 0.0300
			f_EndPortHeight = 0.0800
			f_EndPortRotation = 0.0000
			
			// PORT LIGHTS
			f_StartPortLightOneX = 0.1811
			f_StartPortLightOneY = 0.7405
			f_StartPortLightTwoX = 0.1811
			f_StartPortLightTwoY = 0.8040
			f_EndPortLightOneX = 0.1811
			f_EndPortLightOneY = 0.1989
			f_EndPortLightTwoX = 0.1811
			f_EndPortLightTwoY = 0.2618
		BREAK
		
		
		CASE 3	// LEVEL 4
			// PORTS
			f_StartPortX = 0.1710
			f_StartPortY =  0.7530
			f_StartPortWidth = 0.0300
			f_StartPortHeight = 0.0800
			f_StartPortRotation = 0.0000
			f_EndPortX = 0.8290
			f_EndPortY = 0.2900
			f_EndPortWidth = 0.0300
			f_EndPortHeight = 0.0800
			f_EndPortRotation = 180.0000
			
			// PORT LIGHTS
			f_StartPortLightOneX = 0.1811
			f_StartPortLightOneY = 0.7215
			f_StartPortLightTwoX = 0.1811
			f_StartPortLightTwoY = 0.7850
			f_EndPortLightOneX = 0.8195
			f_EndPortLightOneY = 0.2585
			f_EndPortLightTwoX = 0.8195
			f_EndPortLightTwoY = 0.3220
		BREAK
		
		
		CASE 4	// LEVEL 5
			// PORTS
			f_StartPortX = 0.1710
			f_StartPortY = 0.2900
			f_StartPortWidth = 0.0300
			f_StartPortHeight = 0.0800
			f_StartPortRotation = 0.0000
			f_EndPortX = 0.7900
			f_EndPortY = 0.8300
			f_EndPortWidth = 0.0440
			f_EndPortHeight = 0.0530
			f_EndPortRotation = 270.0000
			
			// PORT LIGHTS
			f_StartPortLightOneX = 0.1810
			f_StartPortLightOneY = 0.2585
			f_StartPortLightTwoX = 0.1810
			f_StartPortLightTwoY = 0.3220
			f_EndPortLightOneX = 0.7730
			f_EndPortLightOneY = 0.8128
			f_EndPortLightTwoX = 0.8082
			f_EndPortLightTwoY = 0.8130
		BREAK
		
		
		CASE 5	// LEVEL 6
			// PORTS
			f_StartPortX = 0.7900
			f_StartPortY = 0.1710
			f_StartPortWidth = 0.0440
			f_StartPortHeight = 0.0530
			f_StartPortRotation = 90.0000
			f_EndPortX = 0.1710
			f_EndPortY = 0.2440
			f_EndPortWidth = 0.0300
			f_EndPortHeight = 0.0800
			f_EndPortRotation = 0.0000
			
			// PORT LIGHTS
			f_StartPortLightOneX = 0.7725
			f_StartPortLightOneY = 0.1890
			f_StartPortLightTwoX = 0.8075
			f_StartPortLightTwoY = 0.1890
			f_EndPortLightOneX = 0.1813
			f_EndPortLightOneY = 0.2120
			f_EndPortLightTwoX = 0.1813
			f_EndPortLightTwoY = 0.2755
		BREAK
	ENDSWITCH
	
	
	// DRAW ENTRY & EXIT PORT SPRITES
	//START PORT
	DRAW_SPRITE("MPCircuitHack", "GenericPort", (0.5 - ((0.5 - f_StartPortX) * hackingData.f_AspectRatioModifier)), f_StartPortY, f_StartPortWidth * hackingData.f_AspectRatioModifier, f_StartPortHeight, f_StartPortRotation, 255, 255, 255, 255)
	
	// END PORT
	DRAW_SPRITE("MPCircuitHack", "GenericPort", (0.5 - ((0.5 - f_EndPortX) * hackingData.f_AspectRatioModifier)), f_EndPortY, f_EndPortWidth * hackingData.f_AspectRatioModifier, f_EndPortHeight, f_EndPortRotation, 255, 255, 255, 255)
	
	
	// CONTROLS PULSE SPEED
	INT i_PulseValue = 800
	
	
	// PULSE LIGHTS
	// END PORT LIGHTS
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_END_PORT_LIGHTS)
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
			// CHANGE TO ALPHA LIGHTS UP
			IF hackingData.s_SnakeData.i_StartPortLightColourA <= 0
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
			ENDIF
			
			hackingData.s_SnakeData.i_StartPortLightColourA = ROUND(hackingData.s_SnakeData.i_StartPortLightColourA -@ i_PulseValue)
			
		ELSE
			// CHANGE TO ALPHA LIGHTS DOWN
			IF hackingData.s_SnakeData.i_StartPortLightColourA >= 255
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
			ENDIF
			
			hackingData.s_SnakeData.i_StartPortLightColourA = ROUND(hackingData.s_SnakeData.i_StartPortLightColourA +@ i_PulseValue)
		ENDIF
		
	ELSE
		// START PORT LIGHTS
		hackingData.s_SnakeData.i_StartPortLightColourA = 255
		
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
			// CHANGE TO ALPHA LIGHTS UP
			IF hackingData.s_SnakeData.i_EndPortLightColourA <= 0
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
			ENDIF
			
			hackingData.s_SnakeData.i_EndPortLightColourA = ROUND(hackingData.s_SnakeData.i_EndPortLightColourA -@ i_PulseValue)
			
		ELSE
			// CHANGE TO ALPHA LIGHTS DOWN
			IF hackingData.s_SnakeData.i_EndPortLightColourA >= 255
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PULSE_UP)
			ENDIF
			
			hackingData.s_SnakeData.i_EndPortLightColourA = ROUND(hackingData.s_SnakeData.i_EndPortLightColourA +@ i_PulseValue)
		ENDIF
	ENDIF
	
	
	// DRAW START PORT LIGHTS - ALWAYS GREEN LIGHTS
	DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - f_StartPortLightOneX) * hackingData.f_AspectRatioModifier)), f_StartPortLightOneY, f_LightWidth * hackingData.f_AspectRatioModifier, f_LightHeight, 0.0, 45, 203, 134, hackingData.s_SnakeData.i_StartPortLightColourA)
	DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - f_StartPortLightTwoX) * hackingData.f_AspectRatioModifier)), f_StartPortLightTwoY, f_LightWidth * hackingData.f_AspectRatioModifier, f_LightHeight, 0.0, 45, 203, 134, hackingData.s_SnakeData.i_StartPortLightColourA)
	
	
	// DRAW END PORT LIGHTS - START RED AND CHANGE TO GREEN WHEN LEVEL IS COMPLETED
	IF (IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_PASSED)
	AND (GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 500)
		// DRAW END GREEN LIGHTS
		DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - f_EndPortLightOneX) * hackingData.f_AspectRatioModifier)), f_EndPortLightOneY, f_LightWidth * hackingData.f_AspectRatioModifier, f_LightHeight, 0.0, 45, 203, 134, 255)
		DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - f_EndPortLightTwoX) * hackingData.f_AspectRatioModifier)), f_EndPortLightTwoY, f_LightWidth * hackingData.f_AspectRatioModifier, f_LightHeight, 0.0, 45, 203, 134, 255)
		
	ELSE
		// DRAW END RED LIGHTS
		DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - f_EndPortLightOneX) * hackingData.f_AspectRatioModifier)), f_EndPortLightOneY, f_LightWidth * hackingData.f_AspectRatioModifier, f_LightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_EndPortLightColourA)
		DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - f_EndPortLightTwoX) * hackingData.f_AspectRatioModifier)), f_EndPortLightTwoY, f_LightWidth * hackingData.f_AspectRatioModifier, f_LightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_EndPortLightColourA)
	ENDIF
	
	
	// HANDLES DRAWING THE DEBUG OUTPUT SPEW
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_BLOCK_DEBUG_OUTPUT_SPEW)
		DRAW_DEBUG_OUTPUT_SPEW(hackingData, b_UseDifficultLevels, bHeist2)
	ENDIF
	
	
#IF IS_DEBUG_BUILD
	// DEBUG DRAW WALL COLLISIONS
	IF b_DrawWallCollisions
		FOR i_Counter = 0 TO (NUM_MAX_WALLS - 1)
			IF hackingData.s_WallData[i_Counter].f_Width <> 0.0
			OR hackingData.s_WallData[i_Counter].f_Height <> 0.0
				DRAW_RECT((0.5 - ((0.5 - hackingData.s_WallData[i_Counter].f_CenterX) * hackingData.f_AspectRatioModifier)), hackingData.s_WallData[i_Counter].f_CenterY, 
							hackingData.s_WallData[i_Counter].f_Width * hackingData.f_AspectRatioModifier, hackingData.s_WallData[i_Counter].f_Height, 190, 190, 190, 190)
							
				// PRINT COLLISION WALL NUMBER
				SET_TEXT_COLOUR(0, 0, 0, 255)
				SET_TEXT_SCALE(0.200, 0.275)
				SET_TEXT_WRAP(0.000, 1.000)
				DISPLAY_TEXT_WITH_NUMBER((0.5 - ((0.5 - hackingData.s_WallData[i_Counter].f_CenterX) * hackingData.f_AspectRatioModifier)) - 0.0043, hackingData.s_WallData[i_Counter].f_CenterY - 0.01, "NUMBER", i_Counter)
			ENDIF
		ENDFOR
		
		
		// START POS
		DRAW_RECT((0.5 - ((0.5 - hackingData.s_SnakeData.f_StartX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_StartY, 0.030 * hackingData.f_AspectRatioModifier, (0.030 * hackingData.f_CurrentAspectRatio), 0, 255, 0, 190)
		SET_TEXT_COLOUR(0, 0, 0, 255)
		SET_TEXT_SCALE(0.200, 0.275)
		SET_TEXT_WRAP(0.000, 1.000)
		DISPLAY_TEXT_WITH_PLAYER_NAME((0.5 - ((0.5 - hackingData.s_SnakeData.f_StartX) * hackingData.f_AspectRatioModifier)) - 0.013, hackingData.s_SnakeData.f_StartY - 0.01, "STRING", "START", HUD_COLOUR_BLACK)
		
		
		// END POS
		DRAW_RECT((0.5 - ((0.5 - hackingData.s_SnakeData.f_EndX) * hackingData.f_AspectRatioModifier)), hackingData.s_SnakeData.f_EndY, hackingData.s_SnakeData.f_EndWidth * hackingData.f_AspectRatioModifier, hackingData.s_SnakeData.f_EndHeight, 255, 0, 0, 190)
		SET_TEXT_COLOUR(0, 0, 0, 255)
		SET_TEXT_SCALE(0.200, 0.275)
		SET_TEXT_WRAP(0.000, 1.000)
		DISPLAY_TEXT_WITH_PLAYER_NAME((0.5 - ((0.5 - hackingData.s_SnakeData.f_EndX) * hackingData.f_AspectRatioModifier)) - 0.0083, hackingData.s_SnakeData.f_EndY - 0.01, "STRING", "END", HUD_COLOUR_BLACK)
	ENDIF
	
	
	// DEBUG ASPECT RATIO TEST
	IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_ASPECT_TEST)
		SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
		DRAW_SPRITE("MPCircuitHack", "SafezoneLines", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ENDIF
#ENDIF

ENDPROC



#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Main debug update loop. Handles all debug
	/// PARAMS:
	///   &hackingData			-	STRUCT: Hacking data struct
	///    b_UseDifficultLevels	-	BOOL: Pass TRUE for the final three levels
	PROC UPDATE_DEBUG_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels)
		CPRINTLN(DEBUG_GOLF, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - RUN CHECK 1 - Running Debug")
		
		// RESET
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 1 - Debug Reset")
			
			CIRCUIT_MINIGAME_RESET(hackingData, TRUE, FALSE, TRUE, FALSE)
			
			hackingData.i_CircuitCurrentLevel = 0
			
			INITIALISE_CIRCUIT_MINIGAME_LEVEL(hackingData, hackingData.i_CircuitCurrentLevel)
			
			START_DEBUG_OUTPUT_SPEW_TIMER(hackingData)
		ENDIF
		
		
		// SKIP TO PREVIOUS STAGE
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 2 - Debug Skip to Previous Level")
			
			CIRCUIT_MINIGAME_RESET(hackingData, TRUE, FALSE, TRUE, FALSE)
			
			IF hackingData.i_CircuitCurrentLevel > 0
				hackingData.i_CircuitCurrentLevel--
			ELSE
				hackingData.i_CircuitCurrentLevel = 0
			ENDIF
			
			INITIALISE_CIRCUIT_MINIGAME_LEVEL(hackingData, hackingData.i_CircuitCurrentLevel)
			
			START_DEBUG_OUTPUT_SPEW_TIMER(hackingData)
		ENDIF
		
		
		// SKIP TO NEXT STAGE
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 3 - Debug Skip to Next Level")
			
			CIRCUIT_MINIGAME_RESET(hackingData, TRUE, FALSE, TRUE, FALSE)
			
			IF hackingData.i_CircuitCurrentLevel < 5
				hackingData.i_CircuitCurrentLevel++
			ELSE
				hackingData.i_CircuitCurrentLevel = 5
				hackingData.e_CircuitHackingMinigameState = CHMS_SYSTEM_HACKED
			ENDIF
			
			INITIALISE_CIRCUIT_MINIGAME_LEVEL(hackingData, hackingData.i_CircuitCurrentLevel)
			
			START_DEBUG_OUTPUT_SPEW_TIMER(hackingData)
		ENDIF
		
		
		// DRAW WALL COLLISION
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_ASPECT_TEST)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_ASPECT_TEST)
			ELSE
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_ASPECT_TEST)
			ENDIF
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 4 - Draw 4:3 Aspect Test Sprite: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_ASPECT_TEST)))
		ENDIF
		
		
		// DRAW WALL COLLISION
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS)
			ELSE
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS)
			ENDIF
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 5 - Draw wall collision boxes: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS)))
		ENDIF
		
		
		// STOP MOVING
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
			ELIF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
			ENDIF
			
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 6 - Moving: ", IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD))
		ENDIF
		
		
		// JUMP TO FINAL COMPLETION SPRITES
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 7 - Debug Skip to Final Screen")
			
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 8 - Clearing MINIGAME_SOLVED Bit")
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
			ENDIF
			
			hackingData.i_WinScreenDelay = GET_GAME_TIMER()
		
			// DRAW FINALE CONNECTED CIRCUIT SPRITE
			hackingData.e_CircuitHackingMinigameState = CHMS_SYSTEM_HACKED
		ENDIF
		
		
		// FORCE SOLVE
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "Force Solve Circuit Hacking Mingame")
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 9 - Debug Force Solve")
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 10 - Setting MINIGAME_SOLVED Bit")
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
			ENDIF
		ENDIF
				
		// JUMP TO FINAL COMPLETION SPRITES
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 7 - Debug Skip to Final Screen")
			
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 8 - Clearing MINIGAME_SOLVED Bit")
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
			ENDIF
			
			hackingData.i_WinScreenDelay = GET_GAME_TIMER()
		
			// DRAW FINALE CONNECTED CIRCUIT SPRITE
			hackingData.e_CircuitHackingMinigameState = CHMS_SYSTEM_HACKED
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED)
		ENDIF
		
		// FORCE CLEANUP
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD2, KEYBOARD_MODIFIER_SHIFT, "Force Cleanup Circuit Hacking Mingame")
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 11 - Debug Force Cleanup & Quit")
			CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels)
		ENDIF
		
		
		// INFO DUMP
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD3, KEYBOARD_MODIFIER_SHIFT, "Print SIZE_OF info.")
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: UPDATE_CIRCUIT_MINIGAME_DEBUG() - CHECK 12 - Debug Info")
			
			// DEBUG VALUE CHECK
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: Current Level: ", hackingData.i_CircuitCurrentLevel)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: Current Deaths: ", hackingData.i_lives)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: Move Speed X: ", hackingData.s_SnakeData.f_SpeedX)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: Move Speed Y: ", hackingData.s_SnakeData.f_SpeedY)
			CPRINTLN(DEBUG_COLIN, "")
		ENDIF
	ENDPROC
#ENDIF



/// PURPOSE:
///    Handles all updates needed every frame
/// PARAMS:
///   &hackingData		-	STRUCT: Hacking data struct
///    b_CanExitVehicle	-	BOOL: Pass TRUE to lock Player in a vehicle during minigame
PROC UPDATE_PER_FRAME_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_CanExitVehicle = FALSE)
	// GENERAL SETUP & BLOCKING FUNCTIONS
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	DISABLE_SELECTOR_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	
	
#IF IS_NEXTGEN_BUILD
	// HIDE THE RECORDING HELP TEXT/FEED UPDATES
	#IF USE_REPLAY_RECORDING_TRIGGERS
		// DISABLES FEED AND INPUT HANDLER WHEN RECORDING STOPS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
	
	// PREVENT RECORDING DURING THE MINIGAME
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
#ENDIF
	
	
	// STOP PLAYER EXITING VEHICLE WHILE MINIGAME IS RUNNING
	IF NOT b_CanExitVehicle
		//DISABLE_VEHICLE_EXIT_THIS_FRAME()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	
	
	// DISABLE CELLPHONE
	IF hackingData.e_CircuitHackingMinigameState > CHMS_LAUNCHING
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	
	
	// GET SCREEN RESOLUTION
	SET_CURRENT_ASPECT_RATIO_MODIFIER(hackingData)
	
	
	// USED IN CONTROLLER TO INCREMENT A STAT
	CHECK_SHOULD_RESET_LEVEL_FAIL_BIT(hackingData)
	
	
	// PAUSE MENU CHECK
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND hackingData.e_CircuitHackingMinigameState > CHMS_LAUNCHING
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF
ENDPROC

PROC SELECT_RANDOM_CIRCUIT_HACKING_LEVEL(S_HACKING_DATA &hackingData)
	hackingData.i_CircuitCurrentLevel = -2
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: SELECT_RANDOM_CIRCUIT_HACKING_LEVEL")
ENDPROC


/// PURPOSE:
///    Initialises the Circuit Hacking Minigame
/// PARAMS:
///   &hackingData			-	STRUCT: Hacking data struct
///    b_UseDifficultLevels	-	BOOL: Pass TRUE for the final three levels
PROC INITIALISE_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels = FALSE)
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 1 - Game state = CHMS_INITIALISE")
	
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 0 - Circuit Hacking Minigame Initialised by [", GET_THIS_SCRIPT_NAME(), "]")
	
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Initialise Started")
	
	// Reset the bit
	hackingData.i_CircuitBitSet = 0
	
	// SET GAME RUNNING BIT
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
		SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
	ENDIF
	
	
	// PRINT CURRENT SCREEN RESOLUTION
	GET_CUSTOM_SCREEN_ASPECT_RATIO(hackingData, TRUE)
	
	
	// INITIALISE MINIGAME VARIABLES
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Initialise Variables Started")
	
	
	// DISABLE PHONE INSTRUCTIONS
	g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
	
	
	// GET BACKGROUND AUDIO ID
	hackingData.i_node = GET_SOUND_ID()
	
	
	// GET TRAIL LOOP AUDIO ID
	hackingData.i_hacking_sound = GET_SOUND_ID()


	// SETUP PHONE LAUNCH VARIBALES
	RESTART_TIMER_AT(hackingData.t_PhoneLaunchTimer, 0.0)
	PAUSE_TIMER(hackingData.t_PhoneLaunchTimer)
	
	hackingData.e_IntroLaunchState = LIS_INITIALISE
	
	
#IF IS_NEXTGEN_BUILD
	// NEXT GEN ADVANCED COMPRESSION PRECISION VALUE
	hackingData.s_SnakeData.i_CompressPrecisionValue = 9974
#ENDIF
	
	
	// STOP AIMING GUN WHILE PHONE LAUNCHES GLITCH
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ALLOW_DAMAGE)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON | NSPC_ALLOW_PLAYER_DAMAGE)
		ELSE
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
		ENDIF
		
		DISABLE_INTERACTION_MENU() 

	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
	ENDIF
	
	
	// RESET SCALEFORM INDEX & DEBUG OUTPUT VARIABLES BORROWED FROM ADAM
	sfHacking = NULL
	iHackSeconds = 0
	iHackMilliseconds = 0
	hackingData.i_lives = 0
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Initialise Variables Finished")
	
	
	// BLOCK THE RANK AND AWARD NOTIFICATIONS
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 4 - Block Rank and Award Notifications")

	
	
	// DISABLE ABILITY TO TURN UP/DOWN (FOR START ONLY)
	IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
		SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DISABLE_VERTICAL_TURNING)
		CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 5 - Disable Vertical Turning")
	ENDIF
	
	
	// REQUEST MINIGAME TEXT
	REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 6 - Request Text")
	
	
	// NEW, FOR SELECTING RANDOM LEVELS
	IF hackingData.i_CircuitCurrentLevel = -2
		IF b_UseDifficultLevels
			hackingData.i_CircuitCurrentLevel = GET_RANDOM_INT_IN_RANGE(3, 6)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME - SELECT_RANDOM_CIRCUIT_HACKING_LEVEL - Difficult - Level: ", hackingData.i_CircuitCurrentLevel)
		ELSE
			hackingData.i_CircuitCurrentLevel = GET_RANDOM_INT_IN_RANGE(0, 3)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME - SELECT_RANDOM_CIRCUIT_HACKING_LEVEL - Easy - Level: ", hackingData.i_CircuitCurrentLevel)
		ENDIF
	ELSE
		// SET EITHER TUTORIAL/FINALE LEVEL PACK
		IF b_UseDifficultLevels
			hackingData.i_CircuitCurrentLevel = 3
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 7a - Initialise Finale Levels")

		ELSE
			hackingData.i_CircuitCurrentLevel = 0
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 7b - Initialise Tutorial Levels")
		ENDIF
	ENDIF
	
	// GENERATE LEVEL DATA (DEPENDS ON WHICH LEVEL PACK PASSED)
	INITIALISE_CIRCUIT_MINIGAME_LEVEL(hackingData, hackingData.i_CircuitCurrentLevel)
	
	
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 8 - Initialise Finished")
	
	
	// MOVE TO REQUEST ASSETS STATE
	hackingData.e_CircuitHackingMinigameState = CHMS_REQUEST_ASSETS
	CPRINTLN(DEBUG_COLIN, "Circuit Hacking: INITIALISE_CIRCUIT_HACKING_MINIGAME() - CHECK 9 - Moving to Stage: CHMS_REQUEST_ASSETS")
ENDPROC



/// PURPOSE:
///    Requests and loads all Circuit Hacking minigame assets
/// PARAMS:
///   &hackingData			-	STRUCT: Hacking data struct
///    b_UseDifficultLevels	-	BOOL: Pass TRUE for the final three levels
///    b_TestAllLevels		-	BOOL: Debug only. Allows to play all six levels in a row - DEBUG ONLY
PROC REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels = FALSE #IF IS_DEBUG_BUILD, BOOL b_TestAllLevels = FALSE #ENDIF )
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 2 - Game state = CHMS_REQUEST_ASSETS")

	BOOL b_LoadAllLevelAssets
	#IF IS_DEBUG_BUILD b_LoadAllLevelAssets = b_TestAllLevels #ENDIF
	
	
	// IF NOT DEBUG
	IF NOT b_LoadAllLevelAssets
		// REQUEST THE TEXTURE DICTIONARY
		REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack")
		sfHacking = REQUEST_SCALEFORM_MOVIE("HACKING_MESSAGE")
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")
		
		IF NOT b_UseDifficultLevels
			REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack2")
			
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack")
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack2")
			AND HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")
				// DISABLE PLAYER CONTROL
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ALLOW_DAMAGE)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE)
					ELSE
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ENDIF
					DISABLE_INTERACTION_MENU() 
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				hackingData.i_CircuitPassedLevelTimer = 0
				
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - CHECK 1a - Tutorial Levels Assets Loaded")
				
				hackingData.e_CircuitHackingMinigameState = CHMS_LAUNCHING
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - CHECK 2a - Moving to Stage: CHMS_LAUNCHING")
			ENDIF
		
		ELSE
			REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack3")
			
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack")
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack3")
			AND HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")
				// DISABLE PLAYER CONTROL
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_ALLOW_DAMAGE)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE)
					ELSE
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ENDIF
					DISABLE_INTERACTION_MENU() 
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				hackingData.i_CircuitPassedLevelTimer = 0
				
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - CHECK 1b - Finale Levels Assets Loaded")
				
				hackingData.e_CircuitHackingMinigameState = CHMS_LAUNCHING
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - CHECK 2b - Moving to Stage: CHMS_LAUNCHING")
			ENDIF
		ENDIF
		
	ELSE
		// DEBUG LOAD ALL ASSETS FOR ALL LEVELS
		REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack")
		REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack2")
		REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack3")
		sfHacking = REQUEST_SCALEFORM_MOVIE("HACKING_MESSAGE")
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")
		
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack2")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack3")
		AND HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
		AND REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_HACK_SNAKE")
			// DISABLE PLAYER CONTROL
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				DISABLE_INTERACTION_MENU() 

			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			hackingData.i_CircuitPassedLevelTimer = 0
			
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - CHECK 1c - All Levels Assets Loaded")
			
			hackingData.e_CircuitHackingMinigameState = CHMS_LAUNCHING
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME() - CHECK 2c - Moving to Stage: CHMS_LAUNCHING")
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Launches the phone camera app to fill the screen before starting the Circuit Hacking minigame
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC LAUNCH_PHONE_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels, BOOL bHeist2)
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 3 - Game state = CHMS_LAUNCHING")
	
	
	// DISABLE PlAYER ABILITY TO LAUNCH PHONE MANUALLY
	#IF IS_NEXTGEN_BUILD 
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	#ENDIF
	#IF NOT IS_NEXTGEN_BUILD
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
	#ENDIF
	
	
	// DISABLE ENTER/EXIT VEHICLE UNTIL PHONE HAS LAUNCHED
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	
	
	// CARRY OUT MINIGAME CELLPHONE LAUNCH IF VEHICLE IS UPRIGHT
	IF CHECK_IS_CIRCUIT_HACKING_PLAYER_IN_UPRIGHT_VEHICLE(hackingData)
		IF DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO(hackingData) <> LS_WORKING
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Launch Finished")
			
			
			// UPDATE DISPLAY FOR ONE FRAME DELAY BETWEEN HAND OVER TO THE MAIN RUNNING LOOP
			UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, TRUE, bHeist2 #IF IS_DEBUG_BUILD, IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS) #ENDIF )
			
			
			// INITIALISE VARIABLES
			START_AUDIO_SCENE("DLC_HEISTS_SNAKE_HACKING_SCENE")
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Audio Scene Started")
			
			
			// BACKGROUND AUDIO
			PLAY_SOUND_FRONTEND(hackingData.i_node, "Background", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Background Audio Started")
			
			
			// SET DELAY TIMER
			hackingData.i_CircuitPassedLevelTimer = GET_GAME_TIMER()
			
			
			// HANDLES DEBUG TEXT ON LEFT SIDE OF SCREEN
			START_DEBUG_OUTPUT_SPEW_TIMER(hackingData)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - CHECK 4 - Debug Output Spew Started")
			
			
			// SAFE FOR LOCAL PLAYER PED TO USE PHONE TASK
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SAFE_TO_USE_PHONE)
			
			
			// MOVE TO MAIN GAME LOOP
			hackingData.e_CircuitHackingMinigameState = CHMS_RUNNING
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - CHECK 5 - Moving to Stage: CHMS_RUNNING")
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Runs the display and logic update loop for Circuit Hacking minigame
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC RUN_MAIN_LOOP_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels, BOOL bHeists2)
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: RUN_MAIN_LOOP_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 4 - Game state = CHMS_RUNNING")
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		//UPDATE GAME LOGIC
		UPDATE_GAME_LOGIC_CIRCUIT_HACKING_MINIGAME(hackingData, bHeists2)
		
		
		// UPDATE DISPLAY
		UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, TRUE, bHeists2 #IF IS_DEBUG_BUILD, IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS) #ENDIF )
		
		
		// DRAW SCAN LINES
		DRAW_CIRCUIT_HACKING_SCALEFORM()
		
		
		// DISPLAY INGAME HELP TEXT
		IF (GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 500
			hackingData.i_CircuitPassedLevelTimer = 0
			
			IF FM_EVENT_GET_VARIATION_PLAYER_IS_ON(PLAYER_ID(), FMMC_TYPE_SUPER_BUSINESS_BATTLES) = ENUM_TO_INT(BBCV_FACTORY_RAID)
				#IF IS_NEXTGEN_BUILD
					IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MOVE_HEAD)
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							PRINT_HELP_FOREVER("CIRC_HACK_1_KM")
						ELSE
							PRINT_HELP_FOREVER("CIRC_HACK_1")
						ENDIF
					ELSE
						IF (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_1_KM"))
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_1")
							CLEAR_HELP()
						ENDIF
					ENDIF
				#ENDIF
				
				#IF NOT IS_NEXTGEN_BUILD
					PRINT_HELP_FOREVER("CIRC_HACK_1")
				#ENDIF
			ELSE
				#IF IS_NEXTGEN_BUILD
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_1_KM", FALSE)
					ELSE
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_1", FALSE)
					ENDIF
				#ENDIF
				
				#IF NOT IS_NEXTGEN_BUILD
					DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_1", FALSE)
				#ENDIF
			ENDIF
		ENDIF
		
		// HANDLE PROGRESSION IF PASS/FAIL
		IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
		OR IS_BIT_SET(hackingData.bsHacking, BS_MP_HACK_FAIL)
		//OR hackingData.b_MP_Hack_fail
			hackingData.e_CircuitHackingMinigameState = CHMS_CLEANUP
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_MAIN_LOOP_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Moving to Stage: CHMS_CLEANUP")
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Handles the pass screen for Circuit Hacking minigame
/// PARAMS:
///   &hackingData		-	STRUCT: Hacking data struct
///    b_TestAllLevels	-	BOOL: Debug only. Allows to play all six levels in a row - DEBUG ONLY
PROC PASS_SCREEN_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOl b_UseDifficultLevels #IF IS_DEBUG_BUILD, BOOL b_TestAllLevels = FALSE #ENDIF )
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 5 - Game state = CHMS_PASS")
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		// UPDATE DISPLAY
		UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, TRUE #IF IS_DEBUG_BUILD, IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS) #ENDIF )
		
		
		// DRAW SCAN LINES
		DRAW_CIRCUIT_HACKING_SCALEFORM()
		
		
		// IF LEVEL PASSED AND TIMER FINISHED
		IF (IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_PASSED)
		AND (GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 1000)
			// SCALEFORM SETUP
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
					
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Stage Passed - Begin Scaleform Movie Method")
					
					SWITCH hackingData.i_CircuitCurrentLevel
						CASE 0
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_WIN")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
							//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						BREAK
						
						
						CASE 1
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_WIN")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
							//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						BREAK
						
						
						CASE 2
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_WIN")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
							//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						BREAK
						
						
						CASE 3
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FIN_WIN")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
							//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						BREAK
						
						
						CASE 4
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FIN_WIN")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
							//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						BREAK
						
						
						CASE 5
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FIN_WIN")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
							//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						BREAK
					ENDSWITCH
					
				END_SCALEFORM_MOVIE_METHOD()
			
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
			ENDIF
			
			
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_PASS_AUDIO)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_PASS_AUDIO)
				
				// PLAY CRASH AUDIO
				PLAY_SOUND_FRONTEND(-1, "Success", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
			ENDIF
			
			
			// CHECK IF WE'RE FINISHED TUTORIAL LEVELS
			IF hackingData.i_CircuitCurrentLevel = 2
			#IF IS_DEBUG_BUILD AND NOT b_TestAllLevels #ENDIF
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Finished Tutorial Levels")
				
				
				// SET DELAT TIMER
				hackingData.i_WinScreenDelay = GET_GAME_TIMER()
				
				
				// MOVE TO TUTORIAL FINISHED STATE
				hackingData.e_CircuitHackingMinigameState = CHMS_FINISHED_TUTORIAL
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Moving to Stage: CHMS_FINISHED_TUTORIAL")
				
				
				// EXIT HERE SO INPUT ISN'T REGISTERED BELOW
				EXIT
			
			// CHECK IF WE'RE FINISHED FINALE LEVELS
			ELIF hackingData.i_CircuitCurrentLevel = 5
				// IF WIN DELAY IS OVER
				IF (GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 3000
					IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						#IF IS_NEXTGEN_BUILD OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)) #ENDIF
					OR ((GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 25000))
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 4 - Finished Finale Levels")
						
						
						// SET DELAY TIMER
						hackingData.i_WinScreenDelay = GET_GAME_TIMER()
						
						
						// PLAY FINALE LEVEL COMPLETED INPUT ACCEPTED SOUND
						PLAY_SOUND_FRONTEND(-1, "Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
						
						
						// MOVE TO SYSTEM HACKED STATE
						hackingData.e_CircuitHackingMinigameState = CHMS_SYSTEM_HACKED
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 5 - Moving to Stage: CHMS_SYSTEM_HACKED")
						
						
						// EXIT HERE SO INPUT ISN'T REGISTERED BELOW
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			
			// IF WE HAVE MORE LEVELS TO PLAY
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				IF (GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 3000
					// DISPLAY PASS HELP TEXT
					#IF IS_NEXTGEN_BUILD
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_2_KM", FALSE)
						ELSE
							DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_2", FALSE)
						ENDIF
					#ENDIF
					
					#IF NOT IS_NEXTGEN_BUILD
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_2", FALSE)
					#ENDIF
					
					
					// HAS PLAYER PRESS ACCEPT
					IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						#IF IS_NEXTGEN_BUILD OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)) #ENDIF
					OR ((GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 25000))
						//MOVE ON TO NEXT LEVEL
						hackingData.i_CircuitCurrentLevel++
						
						
						// RESET NUM TIMES DIED - i_lives BORROWED FROM ADAM
						IF hackingData.i_CircuitCurrentLevel >= 3
							hackingData.i_lives = 0
							CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 6 - Reset Deaths to 0")
						ENDIF
						
						
						// PLAY PASS INPUT ACCEPTED SOUND
						PLAY_SOUND_FRONTEND(-1, "Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
						
						
						// INITIALISE NEXT LEVEL
						INITIALISE_CIRCUIT_MINIGAME_LEVEL(hackingData, hackingData.i_CircuitCurrentLevel)
						
						
						// RESET THE MINIGAME
						CIRCUIT_MINIGAME_RESET(hackingData, TRUE, FALSE, TRUE, FALSE)
						
						
						// RESTART DEBUG SPEW
						START_DEBUG_OUTPUT_SPEW_TIMER(hackingData)
						
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: PASS_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 7 - Moving to Stage: CHMS_RUNNING")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Handles the fail screen for Circuit Hacking minigame
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels)
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 6 - Game state = CHMS_FAIL")

	IF NOT IS_PAUSE_MENU_ACTIVE()
		// UPDATE DISPLAY
		UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, TRUE #IF IS_DEBUG_BUILD, IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS) #ENDIF )
		
		
		// DRAW SCAN LINES
		DRAW_CIRCUIT_HACKING_SCALEFORM()
		
		
		// HANDLE SNAKE DEATH ANIM
		IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Kill Screen")
			
			
			// HANDLE SPARK ANIM
			IF hackingData.s_SnakeData.i_SparkAlpha >= 15
				hackingData.s_SnakeData.f_SparkWidth = hackingData.s_SnakeData.f_SparkWidth +@ 0.05
				
				FLOAT f_height
				f_height = hackingData.s_SnakeData.f_SparkWidth * hackingData.f_CurrentAspectRatio
				
				hackingData.s_SnakeData.i_SparkAlpha = ROUND(hackingData.s_SnakeData.i_SparkAlpha -@ 255)
				
				// DRAW SPARK
				DRAW_SPRITE("MPCircuitHack", "Spark", (0.5 - ((0.5 - hackingdata.s_SnakeData.f_CenterX) * hackingData.f_AspectRatioModifier)), hackingdata.s_SnakeData.f_CenterY, hackingData.s_SnakeData.f_SparkWidth * hackingData.f_AspectRatioModifier, f_height, hackingdata.s_SnakeData.f_HeadRotation, 255, 255, 255, hackingData.s_SnakeData.i_SparkAlpha)
			ENDIF
			
			
			// FADE OUT SNAKE
			IF hackingData.s_SnakeData.i_Alpha > 0
				hackingData.s_SnakeData.i_Alpha = ROUND(hackingData.s_SnakeData.i_Alpha -@ 240)
			ENDIF
			
			
			// WHEN SNAKE FADED ENOUGH
			IF hackingData.s_SnakeData.i_Alpha < 5
				hackingData.s_SnakeData.i_Alpha = 0
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_KILL_SCREEN)
			ENDIF
			
		ELSE
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_FAIL_AUDIO)
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_PLAY_FAIL_AUDIO)
				
				// PLAY CRASH AUDIO
				PLAY_SOUND_FRONTEND(-1, "Failure", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
			ENDIF
			
			
			// SCALEFORM SETUP
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
					
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Begin Scaleform Movie Method")
					
					SWITCH hackingData.i_CircuitCurrentLevel
						CASE 0
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_FAIL")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_FAIL")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(188)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(49)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(43)
							
							//PARAM SEVEN - BOOL: FALSE FOR FAIL SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						BREAK
						
						CASE 1
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_FAIL")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_FAIL")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(188)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(49)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(43)
							
							//PARAM SEVEN - BOOL: FALSE FOR FAIL SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						BREAK
						
						CASE 2
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_FAIL")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_FAIL")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(188)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(49)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(43)
							
							//PARAM SEVEN - BOOL: FALSE FOR FAIL SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						BREAK
						
						CASE 3
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_FAIL")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FIN_FAIL")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(188)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(49)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(43)
							
							//PARAM SEVEN - BOOL: FALSE FOR FAIL SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						BREAK
						
						CASE 4
							// PARAM ONE - LOCK IMAGE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_FAIL")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FIN_FAIL")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(188)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(49)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(43)
							
							//PARAM SEVEN - BOOL: FALSE FOR FAIL SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						BREAK
						
						CASE 5
							// PARAM ONE - NUM OPEN LOCKS
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
							
							// PARAM TWO - LARGE TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_FAIL")
							
							// PARAM THREE - SUB TEXT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FIN_FAIL")
							
							//PARAM FOUR - COLOUR R
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(188)
							
							//PARAM FIVE - COLOUR G
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(49)
							
							//PARAM SIX - COLOUR B
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(43)
							
							//PARAM SEVEN - BOOL: FALSE FOR FAIL SCREEN
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						BREAK
					ENDSWITCH
					
				END_SCALEFORM_MOVIE_METHOD()
			
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
			ENDIF
			
			
			// IF FAIL DELAY FINSIHED
			IF (GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 2000
				// DISPLAY FAIL HELP TEXT
				#IF IS_NEXTGEN_BUILD
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_3_KM", FALSE)
					ELSE
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_3", FALSE)
					ENDIF
				#ENDIF
				
				#IF NOT IS_NEXTGEN_BUILD
					DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_3", FALSE)
				#ENDIF
				
				
				// HAS PLAYER PRESSED ACCEPT
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					#IF IS_NEXTGEN_BUILD OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)) #ENDIF
				OR ((GET_GAME_TIMER() - hackingData.i_CircuitPassedLevelTimer) > 25000))
					// RESET SNAKE POSITION AND START DIRECTION
					hackingData.s_SnakeData.i_Direction = hackingData.s_SnakeData.i_StartDirection
					hackingData.s_SnakeData.f_CenterX = hackingData.s_SnakeData.f_StartX
					hackingData.s_SnakeData.f_CenterY = hackingData.s_SnakeData.f_StartY
					
					
					// PLAY FAIL INPUT ACCEPTED SOUND
					PLAY_SOUND_FRONTEND(-1, "Click", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					
					
					// INCREMENT NUM TIMES DIED - BORROWED FROM ADAM
					IF hackingData.i_CircuitCurrentLevel >= 3
						hackingData.i_lives++
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Increasing Lives by 1")
					ENDIF
					
					
					// SETTING LEVEL FAILED STAT BIT
					IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_FAILED)
						SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_FAILED)
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 4 - Setting BIT_SET_CIRCUIT_LEVEL_FAILED Bit")
					ENDIF
					
					
					// RESET MINIGAME
					CIRCUIT_MINIGAME_RESET(hackingData, TRUE, TRUE, FALSE, FALSE)
					
					
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 5 - Replaying Level: ", hackingData.i_CircuitCurrentLevel)
					
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 6 - Moving to Stage: CHMS_RUNNING")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Handles tutorial levels finish screen for Circuit Hacking minigame
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels)
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 7 - Game state = CHMS_FINISHED_TUTORIAL")
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		// UPDATE DISPLAY
		UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, TRUE #IF IS_DEBUG_BUILD, IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS) #ENDIF )
				
		// DRAW SCAN LINES
		DRAW_CIRCUIT_HACKING_SCALEFORM()
		
		// SCALEFORM SETUP
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
			BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
				
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Begin Scaleform Movie Method")
				
				// PARAM ONE - LOCK IMAGE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
				
				// PARAM TWO - LARGE TEXT
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_COMP")
				
				// PARAM THREE - SUB TEXT
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TUT_WIN")
							
				//PARAM FOUR - COLOUR R
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
				
				//PARAM FIVE - COLOUR G
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
				
				//PARAM SIX - COLOUR B
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
							
				//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				
			END_SCALEFORM_MOVIE_METHOD()
		
			SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
		ENDIF
		
		
		// IF WIN DELAY FINISHED
		IF (GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 7000
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Setting Minigame Finished")
				
				
				// CLEAR HELP
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_2")
					CLEAR_HELP(TRUE)
				ENDIF
				
				#IF IS_NEXTGEN_BUILD
					IF IS_PC_VERSION()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_2_KM")
							CLEAR_HELP(TRUE)
						ENDIF
					ENDIF
				#ENDIF
				
				
				// STOP BACKGROUND AUDIO
				STOP_SOUND(hackingData.i_node)
				
				
				// SET MINIGAME SOLVED
				CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Setting MINIGAME_SOLVED Bit")
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
				
				
				// RESET GAME RUNNING BIT
				IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
					CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME() - CHECK 4 - Clearing BIT_SET_CIRCUIT_GAME_RUNNNIG Bit")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Handles Finale levels System Unlocked finish screen for Circuit Hacking minigame
/// PARAMS:
///   &hackingData	-	STRUCT: Hacking data struct
PROC SYSTEM_HACKED_SCREEN_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels, BOOL bHeists2 = FALSE)
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: SYSTEM_HACKED_SCREEN_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 8 - Game state = CHMS_SYSTEM_HACKED")
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		// RESETS THE SCALEFORM FROM THE LAST PASS SCREEN (STOPS FRAME POP)
		IF NOT IS_BIT_SET(hackingData.i_Circuitbitset, BIT_SET_CIRCUIT_SCALEFORM_RESET)
			SET_BIT(hackingData.i_Circuitbitset, BIT_SET_CIRCUIT_SCALEFORM_RESET)
			
			
			// RESET SCALEFORM SETUP BIT
			IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
				CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
			ENDIF
			
			
			// RESET SCALEFORM
			BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	
	
		// UPDATE BACKGROUND SPRITE
		IF !bHeists2
			DRAW_SPRITE("MPCircuitHack", "CBFinalScreen", 0.5, 0.5, 1.0 * hackingData.f_AspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
		ELSE
			// UPDATE DISPLAY
			UPDATE_GAME_DISPLAY_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, TRUE, bHeists2 #IF IS_DEBUG_BUILD, IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_WALLS) #ENDIF )
		ENDIF
		
		// CORE LIGHT VARIABLES
		FLOAT f_CoreLightWidth = 0.01
		FLOAT f_CoreLightHeight = f_CoreLightWidth * hackingData.f_CurrentAspectRatio
		FLOAT f_FadeValue = 700.0
		
		
		// IF SHOULD NOT DRAW SYSTEM UNLOCKED
		IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED)
			// IF NOT DROPPING CORE LIGHTS
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DROP_CORE_LIGHTS)
				// IF DELAY FINAIHED
				IF (GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 1000
					// PLAY POWER DOWN AUDIO
					PLAY_SOUND_FRONTEND(-1, "Power_Down", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					
					
					// SET DROP CORE LIGHTS
					SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DROP_CORE_LIGHTS)
					
					
					// SET DELAY TIMER
					hackingData.i_WinScreenDelay = GET_GAME_TIMER()
				ENDIF
				
				
				// DRAW RED CORE LIGHTS - FULL ALPHA
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3117) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightOneColourA)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3230) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightTwoColourA)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3340) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightThreeColourA)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3450) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightFourColourA)
				
			ELSE
				// FADE CORE LIGHTS OUYT ON BY ONE
				hackingData.s_SnakeData.i_CoreLightOneColourA = ROUND(hackingData.s_SnakeData.i_CoreLightOneColourA -@ f_FadeValue)
			
				IF hackingData.s_SnakeData.i_CoreLightOneColourA <= 0
					IF hackingData.s_SnakeData.i_CoreLightTwoColourA > 250 
						PLAY_SOUND_FRONTEND(-1, "Beep_Red", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					ENDIF
					
					hackingData.s_SnakeData.i_CoreLightTwoColourA = ROUND(hackingData.s_SnakeData.i_CoreLightTwoColourA -@ f_FadeValue)
				ENDIF
				
				IF hackingData.s_SnakeData.i_CoreLightTwoColourA <= 0
					IF hackingData.s_SnakeData.i_CoreLightThreeColourA > 250 
						PLAY_SOUND_FRONTEND(-1, "Beep_Red", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					ENDIF
					
					hackingData.s_SnakeData.i_CoreLightThreeColourA = ROUND(hackingData.s_SnakeData.i_CoreLightThreeColourA -@ f_FadeValue)
				ENDIF
				
				IF hackingData.s_SnakeData.i_CoreLightThreeColourA <= 0
					IF hackingData.s_SnakeData.i_CoreLightFourColourA > 250 
						PLAY_SOUND_FRONTEND(-1, "Beep_Red", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
						hackingData.i_WinScreenDelay = 0
					ENDIF
					
					hackingData.s_SnakeData.i_CoreLightFourColourA = ROUND(hackingData.s_SnakeData.i_CoreLightFourColourA -@ f_FadeValue)
				ENDIF
				
				
				// IF THE FINAL CORE LIGHT HAS FADED OUT
				IF hackingData.s_SnakeData.i_CoreLightFourColourA <= 0
				AND hackingData.i_WinScreenDelay = 0
					PLAY_SOUND_FRONTEND(-1, "Beep_Red", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					
					
					// SET DELAY TIMER
					hackingData.i_WinScreenDelay = GET_GAME_TIMER()
				ENDIF
				
				
				// DRAW LIGHTS - FADING
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3117) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightOneColourA)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3230) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightTwoColourA)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3340) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightThreeColourA)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3450) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 188, 49, 43, hackingData.s_SnakeData.i_CoreLightFourColourA)
			
				
				// IF THE FINAL CORE LIGHT HAS FADED OUT & DELAY TIMER FINISHED
				IF hackingData.s_SnakeData.i_CoreLightFourColourA <= 0
				AND (GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 500
					// PLAY AUDIO
					PLAY_SOUND_FRONTEND(-1, "Beep_Green", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					
					
					// SET DELAY TIMER
					hackingData.i_WinScreenDelay = GET_GAME_TIMER()
					
					
					// SET ALLOW TO DRAW SYSTEM UNLOCKED
					SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_DRAW_SYS_UNLOCKED)
				ENDIF
			ENDIF
		
		ELSE
			IF !bHeists2
				// DRAW GREEN CORE LIGHTS
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3117) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 45, 203, 134, 255)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3230) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 45, 203, 134, 255)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3340) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 45, 203, 134, 255)
				DRAW_SPRITE("MPCircuitHack", "Light", (0.5 - ((0.5 - 0.3450) * hackingData.f_AspectRatioModifier)), 0.6995, f_CoreLightWidth * hackingData.f_AspectRatioModifier, f_CoreLightHeight, 0.0, 45, 203, 134, 255)
			ENDIF
			
			// SCALEFORM SETUP
			IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
			AND (GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 1000
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
					
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: SYSTEM_HACKED_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 1 - Begin Scaleform Movie Method")
					
					// PARAM ONE - LOCK IMAGE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
					
					// PARAM TWO - LARGE TEXT
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CIRC_HACK")
					
					// PARAM THREE - SUB TEXT
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("UNLCK_FIN")
							
					//PARAM FOUR - COLOUR R
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(45)
					
					//PARAM FIVE - COLOUR G
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(203)
					
					//PARAM SIX - COLOUR B
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(134)
					
					//PARAM SEVEN - BOOL: TRUE FOR PASS SCREEN
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					
				END_SCALEFORM_MOVIE_METHOD()
				
				
				// SET FINISHED SCALEFORM SETUP
				SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_SCALEFORM_SET_UP)
				
				
				// RESET ONE SHOT AUDIO BIT
				IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
					CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
				ENDIF
				
				
				// SET DELAY TIMER
				hackingData.i_WinScreenDelay = GET_GAME_TIMER()
			ENDIF
			
			
			// IF DELAY FINISHED
			IF (GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 500
				IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
					// PLAY SUCCESS AUDIO
					PLAY_SOUND_FRONTEND(-1, "Success", "DLC_HEIST_HACKING_SNAKE_SOUNDS")
					SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
				ENDIF
			ENDIF
			
			
			// IF DELAY TIMER FINISHED
			IF (GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 5000
				// DISPLAY PASS HELP TEXT
				#IF IS_NEXTGEN_BUILD
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_2_KM", FALSE)
					ELSE
						DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_2", FALSE)
					ENDIF
				#ENDIF
				
				#IF NOT IS_NEXTGEN_BUILD
					DISPLAY_HELP_TEXT_THIS_FRAME("CIRC_HACK_2", FALSE)
				#ENDIF
			ENDIF
			
			
			// IF WIN DELAY FINISHED
			IF (((GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 12000)
				#IF IS_NEXTGEN_BUILD OR (((GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 3000) AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)) #ENDIF
			OR (((GET_GAME_TIMER() - hackingData.i_WinScreenDelay) > 3000) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)))
				IF NOT IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
					// SET GAME SOLVED
					CPRINTLN(DEBUG_COLIN, "Circuit Hacking: SYSTEM_HACKED_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 2 - Setting MINIGAME_SOLVED Bit")
					SET_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_MINIGAME_SOLVED)
					
					
					// CLEAR HELP
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_2")
						CLEAR_HELP(TRUE)
					ENDIF
					
					#IF IS_NEXTGEN_BUILD
						IF IS_PC_VERSION()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CIRC_HACK_2_KM")
								CLEAR_HELP(TRUE)
							ENDIF
						ENDIF
					#ENDIF
					
					
					// RESET SCALEFORM
					IF  HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
						BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_DISPLAY")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
						END_SCALEFORM_MOVIE_METHOD()
					ENDIF
					
					
					// BACKGROUND AUDIO
					STOP_SOUND(hackingData.i_node)
					
					
					// PLAY HANG UP BEEP
					PLAY_SOUND_FRONTEND(-1, "Put_Away", "Phone_SoundSet_Michael")
					
					
					// CLEAR GAME RUNNING BIT
					IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
						CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
						CPRINTLN(DEBUG_COLIN, "Circuit Hacking: SYSTEM_HACKED_SCREEN_CIRCUIT_HACKING_MINIGAME() - CHECK 3 - Clearing BIT_SET_CIRCUIT_GAME_RUNNING Bit")
					ENDIF
					
					
					// RESET AUDIO ONESHOT
					IF IS_BIT_SET(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
						CLEAR_BIT(hackingData.i_CircuitBitSet, BIT_SET_CIRCUIT_AUDIO_ONESHOT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		// DRAW SCALEFORM SCAN LINES - DRAW OVER THE CORE LIGHTS
		DRAW_CIRCUIT_HACKING_SCALEFORM()
	ENDIF
ENDPROC



/// PURPOSE:
///    Specific update procedure for Circuit Hacking minigame.
/// PARAMS:
///   &hackingData			-	STRUCT: Hacking data struct
///    b_UseDifficultLevels	-	BOOL: Pass TRUE for the final three levels
///    b_CanExitVehicle		-	BOOL: Pass TRUE to lock Player in a vehicle during minigame
///    b_TestAllLevels		-	BOOL: Debug only. Allows to play all six levels in a row - DEBUG ONLY
/// NOTES:
///    Run every frame, initalise local hacking data struct and pass this through.
PROC RUN_CIRCUIT_HACKING_MINIGAME(S_HACKING_DATA &hackingData, BOOL b_UseDifficultLevels = FALSE, BOOL b_CanExitVehicle = FALSE, BOOL bHeists2 = FALSE #IF IS_DEBUG_BUILD, BOOL b_TestAllLevels = FALSE #ENDIF )
	CPRINTLN(DEBUG_GOLF, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - RUN CHECK - Circuit Hacking Minigame Running")
	
	
	// HANDLES ALL UPDATES NEEDED EVERY FRAME
	UPDATE_PER_FRAME_CIRCUIT_HACKING_MINIGAME(hackingData, b_CanExitVehicle)
	
	
	// MINI GAME FLOW
	SWITCH hackingData.e_CircuitHackingMinigameState
		CASE CHMS_INITIALISE
			INITIALISE_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels)
		BREAK
		
		CASE CHMS_REQUEST_ASSETS
			REQUEST_ASSETS_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels #IF IS_DEBUG_BUILD, b_TestAllLevels #ENDIF )
		BREAK
		
		CASE CHMS_LAUNCHING
			IF bHeists2
				hackingData.e_CircuitHackingMinigameState = CHMS_RUNNING //Skip the phone animation if this hack is happening in its Heists 2 context
			ELSE
				LAUNCH_PHONE_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, bHeists2)
			ENDIF
		BREAK
		
		CASE CHMS_RUNNING
			RUN_MAIN_LOOP_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, bHeists2)
		BREAK
		
		CASE CHMS_PASS
			PASS_SCREEN_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels #IF IS_DEBUG_BUILD, b_TestAllLevels #ENDIF )
		BREAK
		
		CASE CHMS_FAIL
			FAIL_SCREEN_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels)
		BREAK
		
		CASE CHMS_FINISHED_TUTORIAL
			FINISHED_TUTORIAL_LEVELS_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels)
		BREAK
		
		CASE CHMS_SYSTEM_HACKED
			SYSTEM_HACKED_SCREEN_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels, bHeists2)
		BREAK
		
		CASE CHMS_CLEANUP
			CPRINTLN(DEBUG_COLIN, "Circuit Hacking: RUN_CIRCUIT_HACKING_MINIGAME() - RUN CHECK 9 - Game state = CHMS_CLEANUP")
			CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels)
		BREAK
	ENDSWITCH
	
	
	// DEBUG UPDATES
#IF IS_DEBUG_BUILD
	UPDATE_DEBUG_CIRCUIT_HACKING_MINIGAME(hackingData, b_UseDifficultLevels)
#ENDIF


ENDPROC



// ################################# CIRCUIT HACKING MINIGAME - END ################################# //





//SCRIPT
//	#IF IS_DEBUG_BUILD
//		CREATE_HACK_WIDGETS()
//	#ENDIF
//ENDSCRIPT


