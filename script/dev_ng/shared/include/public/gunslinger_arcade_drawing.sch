USING "gunslinger_arcade_using.sch"
USING "gunslinger_arcade_levels.sch"
USING "gunslinger_arcade_leaderboard.sch"
USING "arcade_games_postfx.sch"

PROC TWS_DRAW_FRONT_FX()


	ARCADE_GAMES_POSTFX_DRAW()
	
	#IF IS_DEBUG_BUILD
		IF sTLGData.bDisableFacade
			EXIT
		ENDIF
	#ENDIF
	
	

	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31FacadeTextDict, "Badlands_Bezel", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0, sTLGData.rgbaSprite)	
//	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31FacadeTextDict, "BADLANDS_REVENGE2_43_Frame2", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0, sTLGData.rgbaSprite)	
ENDPROC

FUNC BOOL TLG_IS_CLOUD(STRING sCloud)
	RETURN ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_DESERT_CLOUDS_TILE")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_01")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_02")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_03")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_04")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_05")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_06")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_07")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_01")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_02")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_03")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_04")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_05")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_06")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_07")

ENDFUNC

FUNC INT TLG_GET_FIRST_BACKGROUND_ELEMENT_OF_TYPE(STRING sType)
	INT i
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
		IF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, sType)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC


FUNC INT TLG_GET_NUMBER_OF_BACKGROUND_ELEMENTS_OF_TYPE(STRING sType, INT iFirstElement)
	INT iCount
	INT i
	FOR i = iFirstElement TO ciTLG_MAX_BACKGROUND_ELEMS - 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sType)
			IF NOT IS_STRING_NULL_OR_EMPTY(sTLGData.sBackgroundData[i].sSprite)
				IF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, sType)
					iCount++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_GET_NUMBER_OF_BACKGROUND_ELEMENTS_OF_TYPE] Type: ", sType, " Count: ", iCount)
	RETURN iCount
ENDFUNC 

/// PURPOSE:
///    Figure out where to warp the background element specified when it disappears off the screen LHS
/// PARAMS:
///    iElement - 
/// RETURNS:
///    
FUNC FLOAT GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(INT iElement)
	FLOAT fWidth = sTLGData.sBackgroundData[iElement].vSpriteSize.x
	INT iFirst = TLG_GET_FIRST_BACKGROUND_ELEMENT_OF_TYPE(sTLGData.sBackgroundData[iElement].sSprite)
	INT iTotal = TLG_GET_NUMBER_OF_BACKGROUND_ELEMENTS_OF_TYPE(sTLGData.sBackgroundData[iElement].sSprite, iFirst)
	INT iPrevious = iElement -1 
	IF iPrevious < iFirst
		iPrevious = iFirst + (iTotal - 1) //(ciTLG_MAX_BACKGROUND_TILES - 1)
	ENDIF
	
	RETURN sTLGData.sBackgroundData[iPrevious].vSpritePos.X + fWidth
ENDFUNC

PROC TLG_DRAW_BACKGROUND()
	
	TEXT_LABEL_63 tl23BackgroundSprite
	TEXT_LABEL_31 tl31textDict = sTLGData.tl31BaseTextDict
	SWITCH sTLGData.eCurrentLevel
		CASE TLG_LEVEL_DESERT
			tl23BackgroundSprite = "LEVEL_01_DESERT_BACKGROUND_TILE" // "bg-tile1" //
			tl31textDict = sTLGData.tl31Level1TextDict
		BREAK
		
		CASE TLG_LEVEL_TOWN
			tl23BackgroundSprite =  "LEVEL_02_TOWN_BACKGROUND_TILE" //"bg-tile2"
			tl31textDict = sTLGData.tl31Level2TextDict
		BREAK
		
		CASE TLG_LEVEL_FOREST
			tl23BackgroundSprite = "LEVEL_03_FOREST_BACKGROUND_TILE" //"bg-tile3"
			tl31textDict = sTLGData.tl31Level3TextDict
		BREAK
		
		CASE TLG_LEVEL_MINE
			tl23BackgroundSprite = "LEVEL_04_GOLDMINE_BACKGROUND_TILE" //"bg-tile4"
			tl31textDict = sTLGData.tl31Level4TextDict
		BREAK
		
		CASE TLG_LEVEL_GRAVEYARD
			tl23BackgroundSprite = "LEVEL_05_GRAVEYARD_BACKGROUND_TILE" //"bg-tile5"
			tl31textDict = sTLGData.tl31Level5TextDict
		BREAK
	ENDSWITCH
	
	INT i
	INT iPrevious
	//INT iFirst
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_TILES - 1
				
		ARCADE_DRAW_PIXELSPACE_SPRITE(tl31textDict, tl23BackgroundSprite, INIT_VECTOR_2D(sTLGData.sBackgroundTilesData[i].vSpritePos.x, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTLG_BCAKGROUND_TILE_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTLGData.rgbaSprite)

		IF sTLGData.sBackgroundTilesData[i].vSpritePos.x < cfTLG_BCAKGROUND_TILE_WIDTH/2
			iPrevious = i - 1
			IF iPrevious < 0
				iPrevious =  ciTLG_MAX_BACKGROUND_TILES - 1
			ENDIF
			//sTLGData.sBackgroundTilesData[i].vSpritePos.x = (cfTLG_BCAKGROUND_TILE_WIDTH * 7) - cfTLG_BCAKGROUND_TILE_WIDTH/2 - (4.0 * TLG_GET_PREVIOUS_TIMESTEP())
			sTLGData.sBackgroundTilesData[i].vSpritePos.x = sTLGData.sBackgroundTilesData[iPrevious].vSpritePos.x + cfTLG_BCAKGROUND_TILE_WIDTH
		ENDIF
		
		IF IS_MOVING_LEVEL()
			sTLGData.sBackgroundTilesData[i].vSpritePos.x -= TLG_GET_BACKGROUND_TILE_UPDATE()
		ENDIF
		
	ENDFOR
	
	FLOAT fClouds
	FLOAT fCloudSpace
	// Far canyons
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31Level2TextDict, "mountains", GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_MOUNTAIN_1_SPRITE_WIDTH, cfTLG_MOUNTAIN_1_SPRITE_HEIGHT), 0, sTLGData.rgbaSprite)
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, "LEVEL_05_GRAVEYARD_BACKGROUND_TILE_TREES", GET_DEBUG_TEXTURE_POS(1), INIT_VECTOR_2D(cfTLG_LEVEL5_BACKGROUND_TREES_WIDTH, cfTLG_LEVEL5_BACKGROUND_TREES_HEIGHT), 0, sTLGData.rgbaSprite)
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, "LEVEL_05_GRAVEYARD_BACKGROUND_TILE_GATE", GET_DEBUG_TEXTURE_POS(2), INIT_VECTOR_2D(cfTLG_LEVEL5_BACKGROUND_FENCE_WIDTH, cfTLG_LEVEL5_BACKGROUND_FENCE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		IF sTLGData.sBackgroundData[i].bRendering AND sTLGData.sBackgroundData[i].iLane = 0 
			ARCADE_DRAW_PIXELSPACE_SPRITE(tl31textDict, sTLGData.sBackgroundData[i].sSprite, sTLGData.sBackgroundData[i].vSpritePos, sTLGData.sBackgroundData[i].vSpriteSize, 0, sTLGData.rgbaSprite)
			
			IF TLG_IS_CLOUD(sTLGData.sBackgroundData[i].sSprite)
				IF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_DESERT
					IF sTLGData.sBackgroundData[i].vSpritePos.X + (cfTLG_LEVEL1_CLOUD_WIDTH / 2.0 ) <= 0
						sTLGData.sBackgroundData[i].vSpritePos.X = cfTLG_LEVEL1_CLOUD_WIDTH + cfTLG_LEVEL1_CLOUD_WIDTH + (cfTLG_LEVEL1_CLOUD_WIDTH / 2.0 )
						CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Warping cloud ", i, " New Pos x: ", sTLGData.sBackgroundData[i].vSpritePos.X)
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CLOUD_UPDATE()
					ENDIF
				ELIF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_TOWN
					fCloudSpace = cfTLG_LEVEL2_CLOUD_GAP
					fClouds = cfTLG_LEVEL2_CLOUD1_WIDTH + fCloudSpace + cfTLG_LEVEL2_CLOUD2_WIDTH + fCloudSpace + cfTLG_LEVEL2_CLOUD3_WIDTH + fCloudSpace + cfTLG_LEVEL2_CLOUD4_WIDTH + fCloudSpace + cfTLG_LEVEL2_CLOUD5_WIDTH + fCloudSpace + cfTLG_LEVEL2_CLOUD6_WIDTH + fCloudSpace + cfTLG_LEVEL2_CLOUD7_WIDTH
					IF sTLGData.sBackgroundData[i].vSpritePos.X + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 ) <= 300.0
						sTLGData.sBackgroundData[i].vSpritePos.X = 300.0 + fClouds + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 )
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CLOUD_UPDATE()
					ENDIF
				ELIF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_FOREST
					fCloudSpace = cfTLG_LEVEL3_CLOUD_GAP
					fClouds = cfTLG_LEVEL3_CLOUD1_WIDTH + fCloudSpace + cfTLG_LEVEL3_CLOUD2_WIDTH + fCloudSpace + cfTLG_LEVEL3_CLOUD3_WIDTH + fCloudSpace + cfTLG_LEVEL3_CLOUD4_WIDTH + fCloudSpace + cfTLG_LEVEL3_CLOUD5_WIDTH + fCloudSpace + cfTLG_LEVEL3_CLOUD6_WIDTH + fCloudSpace + cfTLG_LEVEL3_CLOUD7_WIDTH
					IF sTLGData.sBackgroundData[i].vSpritePos.X + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 ) <= 300.0
						sTLGData.sBackgroundData[i].vSpritePos.X = 300.0 + fClouds + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 )
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CLOUD_UPDATE()
					ENDIF
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "mountains")
				IF sTLGData.sBackgroundData[i].vSpritePos.X + (cfTLG_MOUNTAIN_1_SPRITE_WIDTH / 2.0 ) <= 0
					sTLGData.sBackgroundData[i].vSpritePos.X = cfTLG_MOUNTAIN_1_SPRITE_WIDTH + cfTLG_MOUNTAIN_1_SPRITE_WIDTH + (cfTLG_MOUNTAIN_1_SPRITE_WIDTH / 2.0 )
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Warping mountains ", i, " New Pos x: ", sTLGData.sBackgroundData[i].vSpritePos.X)
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "LEVEL_03_FOREST_BACKGROUND_TREES")
				fClouds = 7 * cfTLG_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds
				ENDIF
				
			//	CPRINTLN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] Level 3 trees : ", i)
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "LEVEL_04_GOLDMINE_CAVE_BACKGROUND_CAVE")
				fClouds = 7 * cfTLG_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CLOUD_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "LEVEL_04_GOLDMINE_CAVE_TILE_02")
				fClouds = 7 * cfTLG_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "LEVEL_05_GRAVEYARD_BACKGROUND_CLOUDS")
				fClouds = 7 * cfTLG_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds// + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CLOUD_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "LEVEL_05_GRAVEYARD_BACKGROUND_TILE_TREES")
				fClouds = 7 * cfTLG_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds// + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
				
			ELIF ARE_STRINGS_EQUAL(sTLGData.sBackgroundData[i].sSprite, "LEVEL_05_GRAVEYARD_BACKGROUND_TILE_GATE")
				fClouds = 7 * cfTLG_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds// + (sTLGData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_DEFAULT_NEAR_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELSE
				
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X < cfTLG_BCAKGROUND_TILE_WIDTH/2
					sTLGData.sBackgroundData[i].vSpritePos.X = cfBASE_SCREEN_WIDTH
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	// Close canyons
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
		
		IF sTLGData.sBackgroundData[i].bRendering AND sTLGData.sBackgroundData[i].iLane = 1
			//sTLGData.sBackgroundData[i].vSpritePos.y = sTLGData.fTexturePosY[0]

			ARCADE_DRAW_PIXELSPACE_SPRITE(tl31textDict, sTLGData.sBackgroundData[i].sSprite, sTLGData.sBackgroundData[i].vSpritePos, sTLGData.sBackgroundData[i].vSpriteSize, 0, sTLGData.rgbaSprite)
			
			IF ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_CAVE_TILE", sTLGData.sBackgroundData[i].sSprite)
			OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_ASSETS_10", sTLGData.sBackgroundData[i].sSprite) 
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CUSTOM_FAST_NEAR_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL("fence-small", sTLGData.sBackgroundData[i].sSprite)
				IF sTLGData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sTLGData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_DEFAULT_NEAR_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELSE
				
				IF sTLGData.sBackgroundData[i].vSpritePos.X < cfTLG_BCAKGROUND_TILE_WIDTH/2
					sTLGData.sBackgroundData[i].vSpritePos.X = cfBASE_SCREEN_WIDTH
				ENDIF
				
				IF IS_MOVING_LEVEL()
					IF ARE_STRINGS_EQUAL("rocks", sTLGData.sBackgroundData[i].sSprite) 
					OR ARE_STRINGS_EQUAL("FOREST_FLOOR_GRASS", sTLGData.sBackgroundData[i].sSprite)
					OR ARE_STRINGS_EQUAL("LEVEL_03_FOREST_ROCKS", sTLGData.sBackgroundData[i].sSprite)
					OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_ROCK", sTLGData.sBackgroundData[i].sSprite)
					OR ARE_STRINGS_EQUAL("WOODENSTRUCTURE", sTLGData.sBackgroundData[i].sSprite)
			//		OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_ASSETS_10", sTLGData.sBackgroundData[i].sSprite)
			//		OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_CAVE_TILE", sTLGData.sBackgroundData[i].sSprite)

						sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CUSTOM_FAST_NEAR_BACKGROUND_ELEMENT_UPDATE()
					ELIF ARE_STRINGS_EQUAL("mountain-small1", sTLGData.sBackgroundData[i].sSprite) 
					OR ARE_STRINGS_EQUAL("mountain-small2", sTLGData.sBackgroundData[i].sSprite) 
						sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CUSTOM_SLOW_NEAR_BACKGROUND_ELEMENT_UPDATE()
					ELSE
						sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_DEFAULT_NEAR_BACKGROUND_ELEMENT_UPDATE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR

ENDPROC
	
PROC TLG_DRAW_HOSTAGES(INT iLane)

	TEXT_LABEL_23 tl23Sprite
	
	INT i
	
	IF iLane = 1
	//	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HostagesTextDict, "hostage1",GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_HOSTAGE_M_SPRITE_WIDTH, cfTLG_HOSTAGE_M_SPRITE_HEIGHT), 0, sTLGData.rgbaSprite)
	ENDIF
	FOR i = 0 TO ciTLG_MAX_HOSTAGES -1
		
		IF sTLGData.sHostageData[i].iHostageLane = iLane
			IF sTLGData.sHostageData[i].iHealth > 0
				SWITCH sTLGData.sHostageData[i].eHostageType
				CASE TLG_GIRL
					tl23Sprite = "hostage1"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_GUY
					tl23Sprite = "hostage2"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_EAGLE
					tl23Sprite = "eagle"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_VULTURE
					tl23Sprite = "vulture_0"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_BAT
					tl23Sprite = "bat_0"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_SCORPION
					tl23Sprite = "ANIMALBONUS_SCORPION_0"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_MOUSE
					tl23Sprite = "ANIMALBONUS_RAT_0"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				CASE TLG_PRISONER_1
					tl23Sprite = "prisoner"
					tl23Sprite += (sTLGData.sHostageData[i].iSpriteAnimFrame + 1)
				BREAK
				ENDSWITCH
				
//				IF iLane = 2
//					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_HOSTAGES] {DSW}  Drawing Hostage ", i, " Type: ", TLG_GET_HOSTAGE_TYPE_AS_STRING(sTLGData.sHostageData[i].eHostageType), " Lane: ", sTLGData.sHostageData[i].iHostageLane, " Pos: ", sTLGData.sHostageData[i].vSpritePos.x,  ", ", sTLGData.sHostageData[i].vSpritePos.y, " Size: ", sTLGData.sHostageData[i].vSize.x, ", ", sTLGData.sHostageData[i].vSize.y)
//				ENDIF
				IF sTLGData.sHostageData[i].bGoingBack AND (sTLGData.sHostageData[i].eHostageType != TLG_EAGLE AND sTLGData.sHostageData[i].eHostageType != TLG_VULTURE)
					ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HostagesTextDict, tl23Sprite,sTLGData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(-sTLGData.sHostageData[i].vSize.x, sTLGData.sHostageData[i].vSize.y), 0, sTLGData.rgbaSprite)
				ELSE
					ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HostagesTextDict, tl23Sprite,sTLGData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sHostageData[i].vSize.x, sTLGData.sHostageData[i].vSize.y), 0, sTLGData.rgbaSprite)
				ENDIF
				
			ELIF sTLGData.sHostageData[i].bDying
				SWITCH sTLGData.sHostageData[i].eHostageType
				CASE TLG_GIRL
					tl23Sprite = "ouch1"
					ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HostagesTextDict, tl23Sprite,sTLGData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_SPEECH_SPRITE_WIDTH, cfTLG_SPEECH_SPRITE_HEIGHT), 0, sTLGData.rgbaSprite)
				BREAK
				CASE TLG_GUY
					tl23Sprite = "ouch1"
					ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HostagesTextDict, tl23Sprite,sTLGData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_SPEECH_SPRITE_WIDTH, cfTLG_SPEECH_SPRITE_HEIGHT), 0, sTLGData.rgbaSprite)
				BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
	ENDFOR
		
ENDPROC


PROC TLG_DRAW_COVER(INT iDistance, BOOL bForLeaderboard = FALSE)

	TEXT_LABEL_23 tl23Sprite
	TEXT_LABEL_31 tl31Dict
	FLOAT fScale = 1.0
	
	INT iRemoveWhenOffScreen[10]
	INT iNumRemove = 0
	
	BOOL bRepositionWhenOffScreenForMovingLevel
	BOOL bUpdatePosition
	INT i
	
//	IF iDistance =1
//		fScale = TLG_GET_COVER_TYPE_SCALE_FOR_LANE(TLG_ASSET_COFFIN, iDistance)
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31Level5TextDict, "GRAVEYARD_COFFIN_01",  GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_GRAVEYARD_COFFIN_SPRITE_WIDTH*fScale, cfTLG_GRAVEYARD_COFFIN_SPRITE_HEIGHT*fScale), 0, sTLGData.rgbaSprite)
//	ENDIF

	FOR i = 0 TO ciTLG_MAX_COVER -1
		IF sTLGData.sCoverData[i].iCoverLane = iDistance 
		AND (sTLGData.sCoverData[i].bRendering 
		OR sTLGData.sCoverData[i].iRenderAfterWarpCount > 0 
		OR sTLGData.sCoverData[i].bNeverRender)
			IF sTLGData.sCoverData[i].iActiveInStage <= TLG_GET_CURRENT_LEVEL_STAGE()
				fScale = TLG_GET_COVER_TYPE_SCALE_FOR_LANE(sTLGData.sCoverData[i].eType, iDistance)

				SWITCH (sTLGData.sCoverData[i].eType)
					CASE TLG_ASSET_OUTHOUSE
						IF sTLGData.sCoverData[i].iHitCount = 0
							tl23Sprite = "outhouse_01"
							tl31Dict = sTLGData.tl31Level1TextDict
						ELSE
							tl23Sprite = "outhouse_empty"
							tl31Dict = sTLGData.tl31Level1TextDict
						ENDIF
					BREAK
					CASE TLG_ASSET_CACTUS_1
						tl23Sprite = "cactus-1"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_2
						tl23Sprite = "cactus-2"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_3
						tl23Sprite = "cactus-3"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_4
						tl23Sprite = "PROP_CACTUS_02"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CRATE_1
						//-- Loot crate
						IF sTLGData.sCoverData[i].iHitCount = 0
							tl23Sprite = "crate_01"
							tl31Dict = sTLGData.tl31GameplayCommonTextDict
						ELIF sTLGData.sCoverData[i].iHitCount = 1
							tl23Sprite = "crate_02"
							tl31Dict = sTLGData.tl31GameplayCommonTextDict
						ELIF sTLGData.sCoverData[i].iHitCount = 2
							tl23Sprite = "crate_03"
							tl31Dict = sTLGData.tl31GameplayCommonTextDict
							
						ELSE
							
							RELOOP
						ENDIF
					BREAK
					CASE TLG_ASSET_BUILDING_1
						tl23Sprite = "LEVEL_02_TOWN_PROP_03"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_BUILDING_2
						tl23Sprite = "LEVEL_02_TOWN_PROP_04"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_BUILDING_3
						tl23Sprite = "LEVEL_02_TOWN_PROP_05"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_BUILDING_4
						tl23Sprite = "LEVEL_02_TOWN_PROP_06"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_BUILDING_5
						tl23Sprite = "LEVEL_02_TOWN_PROP_07"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_BUILDING_6
						tl23Sprite = "LEVEL_02_TOWN_PROP_08"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_BUILDING_7
						tl23Sprite = "LEVEL_02_TOWN_PROP_09"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK		
					CASE TLG_ASSET_BUILDING_8
						tl23Sprite = "LEVEL_02_TOWN_PROP_10"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_FENCE
						tl23Sprite = "fence"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_FENCE_SMALL
						tl23Sprite = "fence-small"
						tl31Dict = sTLGData.tl31Level2TextDict
					BREAK
					CASE TLG_ASSET_CACTUS_5
						tl23Sprite = "PROP_CACTUS_03"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_6
						tl23Sprite = "PROP_CACTUS_04"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_7
						tl23Sprite = "PROP_CACTUS_05"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_8
						tl23Sprite = "PROP_CACTUS_06"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_9
						tl23Sprite = "PROP_CACTUS_07"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_10
						tl23Sprite = "PROP_CACTUS_08"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_CACTUS_11
						tl23Sprite = "PROP_CACTUS_09"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_FOREST_TREE_1
						tl23Sprite = "TREE_01"
						tl31Dict = sTLGData.tl31Level3TextDict
					BREAK
					CASE TLG_ASSET_FOREST_TREE_2
						tl23Sprite = "TREE_02"
						tl31Dict = sTLGData.tl31Level3TextDict
					BREAK
					CASE TLG_ASSET_GRASS
						tl23Sprite = "FOREST_FLOOR_GRASS"
						tl31Dict = sTLGData.tl31Level3TextDict
					BREAK
					CASE TLG_ASSET_BUSH_1
						tl23Sprite = "PROP_BUSH_SMALL"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_BUSH_2
						tl23Sprite = "PROP_BUSH_LARGE"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_ROCK_SMALL
						tl23Sprite = "PROP_ROCK_SMALL"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_ROCK_LARGE
						tl23Sprite = "PROP_ROCK_LARGE"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_STACKED_CRATES_1
						tl31Dict = sTLGData.tl31Level4TextDict
						tl23Sprite = "PROP_STACKED_CRATES_01"
					BREAK
					CASE TLG_ASSET_STACKED_CRATES_2
						tl31Dict = sTLGData.tl31Level4TextDict
						tl23Sprite = "PROP_STACKED_CRATES_02"
					BREAK
					CASE TLG_ASSET_BARRELS
						tl23Sprite = "PROP_BARRELS"
						tl31Dict = sTLGData.tl31CommonPropsTextDict
					BREAK
					CASE TLG_ASSET_BIG_TOMB
						tl31Dict = sTLGData.tl31Level5TextDict
						tl23Sprite = "GRAVEYARD_PROP_06"
					BREAK
					CASE TLG_ASSET_TOMBSTONE_1
						tl31Dict = sTLGData.tl31Level5TextDict
						tl23Sprite = "GRAVEYARD_PROP_01"
					BREAK
					CASE TLG_ASSET_TOMBSTONE_2
						tl31Dict = sTLGData.tl31Level5TextDict
						tl23Sprite = "GRAVEYARD_PROP_02"
					BREAK
					CASE TLG_ASSET_TOMBSTONE_3
						tl31Dict = sTLGData.tl31Level5TextDict
						tl23Sprite = "GRAVEYARD_PROP_03"
					BREAK
					CASE TLG_ASSET_TOMBSTONE_4
						tl31Dict = sTLGData.tl31Level5TextDict
						tl23Sprite = "GRAVEYARD_PROP_04"
					BREAK
					
					CASE TLG_ASSET_TOMBSTONE_5
						tl31Dict = sTLGData.tl31Level5TextDict
						tl23Sprite = "GRAVEYARD_PROP_05"
					BREAK
					
					CASE TLG_ASSET_COFFIN
						tl23Sprite = "GRAVEYARD_COFFIN_01"
						IF sTLGData.sCoverData[i].iHitCount = 0
							tl23Sprite = "GRAVEYARD_COFFIN_01"
							tl31Dict = sTLGData.tl31Level5TextDict
						ELSE
							tl23Sprite = "GRAVEYARD_COFFIN_EMPTY"
							tl31Dict = sTLGData.tl31Level5TextDict
						ENDIF
					BREAK
					
					CASE TLG_ASSET_WATER_TOWER
						tl31Dict = sTLGData.tl31Level2TextDict
						tl23Sprite = "WATER-TANK"
					BREAK
					CASE TLG_ASSET_SPECIAL_TNT_CRATE
						tl23Sprite = "PROP_TNT_CRATE_STATE_01"
						tl31Dict = sTLGData.tl31GameplayCommonTextDict
					BREAK
					CASE TLG_ASSET_SPECIAL_TNT_BARREL_1
						tl23Sprite = "BARREL_HORIZONTAL"
						tl31Dict = sTLGData.tl31Level4TextDict
					BREAK
					CASE TLG_ASSET_SPECIAL_TNT_BARREL_2
						tl23Sprite = "BARREL_VERTICAL"
					BREAK
					CASE TLG_ASSET_SPECIAL_OUTHOUSE_HOST
						tl31Dict = sTLGData.tl31Level1TextDict
						tl23Sprite = "OUTHOUSE_EMPTY"
					BREAK

				ENDSWITCH
				
				//-- Track the 'right-most' cover / element currently visible
				IF sTLGData.sCoverData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH AND sTLGData.sCoverData[i].vSpritePos.x > 0
					IF sTLGData.sCoverData[i].bRendering 
						ARCADE_DRAW_PIXELSPACE_SPRITE(tl31Dict, tl23Sprite,  sTLGData.sCoverData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sCoverData[i].fCoverWidth*fScale, sTLGData.sCoverData[i].fCoverHeight*fScale), 0, sTLGData.rgbaSprite)
					ENDIF
				//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Drawing cover ", i , " Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType), " At x pos: ",sTLGData.sCoverData[i].vSpritePos.x)
					IF sTLGData.sCoverData[i].vSpritePos.x - (sTLGData.sCoverData[i].fCoverWidth / 2.0) < cfRIGHT_SCREEN_LIMIT
					AND sTLGData.sCoverData[i].vSpritePos.x + (sTLGData.sCoverData[i].fCoverWidth / 2.0) > cfLEFT_SCREEN_LIMIT
					
						IF sTLGData.iRightMostCover = -1
							sTLGData.fRightMostCoverX = sTLGData.sCoverData[i].vSpritePos.x
							sTLGData.iRightMostCover = i
							
							#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Init iRightMostCover: ", i, " Rightmost x: ", sTLGData.fRightMostCoverX, " Prop: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
							#ENDIF
						ELIF i = sTLGData.iRightMostCover
							sTLGData.fRightMostCoverX = sTLGData.sCoverData[i].vSpritePos.x
							
//							#IF IS_DEBUG_BUILD
//								CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Update iRightMostCover: ", i, " Rightmost x: ", sTLGData.fRightMostCoverX, " Prop: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
//							#ENDIF
						ELSE
							IF sTLGData.sCoverData[i].vSpritePos.x > sTLGData.fRightMostCoverX
								IF sTLGData.sCoverData[i].iScreen != sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen
									IF sTLGData.sCoverData[i].iScreen > sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen 
										sTLGData.fRightMostCoverX = sTLGData.sCoverData[i].vSpritePos.x
										sTLGData.iRightMostCover = i
										
										#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} New iRightMostCover: ", i, " Screen:", sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen, " Rightmost x: ", sTLGData.fRightMostCoverX, " Prop: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ELIF sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen = 4 AND  sTLGData.sCoverData[i].iScreen = 1
										sTLGData.fRightMostCoverX = sTLGData.sCoverData[i].vSpritePos.x
										sTLGData.iRightMostCover = i
										
										#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} New iRightMostCover: ", i, " Screen:", sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen, " Rightmost x: ", sTLGData.fRightMostCoverX, " Prop: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF

				bUpdatePosition = TRUE	
				bRepositionWhenOffScreenForMovingLevel = FALSE
				
				IF bForLeaderboard
					
					//-- If it's the LB background, always update
					bRepositionWhenOffScreenForMovingLevel = TRUE
				ELIF IS_MOVING_LEVEL() 
					
					IF TLG_IS_MOVING_LEVEL_ABOUT_TO_STOP()
						IF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_DESERT
						OR sTLGData.sCoverData[i].iCoverLane = ciTLG_LANE_FOREGROUND
							//-- Don't want to reprosition if we're about to stop scrolling as otherwise they'll appear again on
							//-- the static screen
							bRepositionWhenOffScreenForMovingLevel = FALSE
							
							IF sTLGData.sCoverData[i].iActiveInStage < TLG_GET_CURRENT_LEVEL_STAGE()
								FLOAT fLeftEdge
								fLeftEdge = sTLGData.sCoverData[i].vSpritePos.x - (sTLGData.sCoverData[i].fCoverWidth /2.0)
								IF fLeftEdge > cfBASE_SCREEN_WIDTH - 250
									//-- If we're about to stop, don't update anything that's about to appear from the right
									#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Not updating cover from right - about to stop ", i, " Lane: ", sTLGData.sCoverData[i].iCoverLane ," Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType), " Left edge Pos: ", fLeftEdge)
									#ENDIF
									bUpdatePosition = FALSE
								ENDIF
							ENDIF
						ELSE
							//-- Level is moving, fine to update
							bRepositionWhenOffScreenForMovingLevel = TRUE
						ENDIF
					ELSE
						//-- Level is moving, fine to update
						bRepositionWhenOffScreenForMovingLevel = TRUE
					ENDIF
					
				ELSE
					
					//-- Stationary, don't move anything
					bRepositionWhenOffScreenForMovingLevel = FALSE
					bUpdatePosition = FALSE
				ENDIF

//				IF TLG_GET_CURRENT_LEVEL() != TLG_LEVEL_DESERT
//					bRepositionWhenOffScreenForMovingLevel = FALSE
//				ENDIF
				
				IF iDistance = 2
					IF sTLGData.sCoverData[i].vSpritePos.x >= -200
						IF bUpdatePosition
							sTLGData.sCoverData[i].vSpritePos.x -= TLG_GET_COVER_FOREGROUND_UPDATE()
						ENDIF
					ELIF sTLGData.sCoverData[i].vSpritePos.x < -200
						IF bRepositionWhenOffScreenForMovingLevel
							sTLGData.sCoverData[i].vSpritePos.x = cfBASE_SCREEN_WIDTH*2
						ENDIF
					ENDIF
				ELSE
					IF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_DESERT
						IF sTLGData.sCoverData[i].vSpritePos.x >= -200
							IF bUpdatePosition
								sTLGData.sCoverData[i].vSpritePos.x -= TLG_GET_COVER_UPDATE()
							ENDIF
						ELIF sTLGData.sCoverData[i].vSpritePos.x < -200
							
							IF bRepositionWhenOffScreenForMovingLevel
								IF TLG_SHOULD_REMOVE_COVER_WHEN_OFF_SCREEN(i)
									//-- When some cover has finished animating, don't want to reposition
									IF iNumRemove < 10
										iRemoveWhenOffScreen[iNumRemove] = i
										iNumRemove++
										
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Want to remove when offscreen ", i, " Lane: ", sTLGData.sCoverData[i].iCoverLane ," Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Want to remove when offscreen BUT ARRAY FULL! ", i, " Lane: ", sTLGData.sCoverData[i].iCoverLane ," Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ENDIF
								ELSE
									sTLGData.sCoverData[i].vSpritePos.x = cfBASE_SCREEN_WIDTH + sTLGData.sCoverData[i].fCoverWidth/2
								ENDIF
							
							ENDIF

						ENDIF
					ELIF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_TOWN
					OR TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_FOREST
					OR TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_MINE 
					OR TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_GRAVEYARD

						
						IF sTLGData.sCoverData[i].vSpritePos.x > 0
							IF bUpdatePosition
								sTLGData.sCoverData[i].vSpritePos.x -= TLG_GET_COVER_UPDATE()
							ENDIF
							
						ELIF sTLGData.sCoverData[i].vSpritePos.x <= 0
							
							IF bRepositionWhenOffScreenForMovingLevel
								IF TLG_SHOULD_REMOVE_COVER_WHEN_OFF_SCREEN(i)
									//-- When some cover has finished animating, don't want to reposition
									IF iNumRemove < 10
										iRemoveWhenOffScreen[iNumRemove] = i
										iNumRemove++
										
										IF sTLGData.sCoverData[i].eType = TLG_ASSET_COFFIN
											TLG_ADD_COVER_TO_LIST(sTLGData.sCoverData[i].vSpritePos, TLG_ASSET_COFFIN, 	ciTLG_LANE_BACKGROUND, ciTLG_STAGE_0, sTLGData.sCoverData[i].iScreen)
										ENDIF
										
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Want to remove when offscreen ", i, " Lane: ", sTLGData.sCoverData[i].iCoverLane ," Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Want to remove when offscreen BUT ARRAY FULL! ", i, " Lane: ", sTLGData.sCoverData[i].iCoverLane ," Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ENDIF
								ELSE
									sTLGData.sCoverData[i].vSpritePos.x = TLG_GET_OFFSET_FOR_ELEMENT_WARP_FOR_CURRENT_LEVEL()
									
									#IF IS_DEBUG_BUILD
										IF sTLGData.sCoverData[i].eType = TLG_ASSET_SPECIAL_TNT_BARREL_1
											CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Found TNT Barrel Cover: ", i, " Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType), " Render after warp count: ", sTLGData.sCoverData[i].iRenderAfterWarpCount)

										ENDIF
									#ENDIF
									
									sTLGData.sCoverData[i].iWarpCount++
									IF sTLGData.sCoverData[i].iRenderAfterWarpCount > 0
									AND (sTLGData.sCoverData[i].iRenderAfterWarpCount = sTLGData.sCoverData[i].iWarpCount)
										sTLGData.sCoverData[i].bRendering = TRUE
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Setting prop ", i, "to render after warp count", sTLGData.sCoverData[i].iRenderAfterWarpCount, " Lane: ", sTLGData.sCoverData[i].iCoverLane ," Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType))
										#ENDIF
									ENDIF
								ENDIF
							
							ENDIF

						ENDIF
					ENDIF
				ENDIF
					
				IF IS_MOVING_LEVEL() 
					IF TLG_IS_MOVING_LEVEL_ABOUT_TO_STOP()
						IF TLG_SHOULD_MOVING_LEVEL_STOP(i)
							//CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_COVER] {DSW} Stopping because Prop ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType), " For stage ", sTLGData.sCoverData[i].iActiveInStage, " At X pos ", sTLGData.sCoverData[i].vSpritePos.x)
							TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_STOPPED)
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	//-- Delete / reset any cover we don't want to re-appear next time the screen scrolls
	IF iNumRemove > 0
		FOR i = 0 TO iNumRemove - 1
			TLG_RESET_COVER(iRemoveWhenOffScreen[i])
		ENDFOR
	ENDIF
ENDPROC



PROC TLG_DRAW_THROWN_ENEMY_PROJECTILES()
	INT i
	TEXT_LABEL_31 tl31Sprite
	VECTOR_2D vSize
	
//	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, "THROWING_DYNAMITE_SMALL_01" ,GET_DEBUG_TEXTURE_POS(0),  INIT_VECTOR_2D(ciTLG_DYNAMITE_SMALL_WIDTH, ciTLG_DYNAMITE_SMALL_HEIGHT),0, sTLGData.rgbaSprite)
	
	REPEAT ciTLG_MAX_ENEMY_PROJECTILES i
		IF sTLGData.sEnemyProjectiles[i].eState = TLG_ENEMY_PROJECTILE_STATE_INACTIVE
		ELIF sTLGData.sEnemyProjectiles[i].eState = TLG_ENEMY_PROJECTILE_STATE_AIRBORNE
			SWITCH sTLGData.sEnemyProjectiles[i].eType
				CASE TLG_ENEMY_PROJECTILE_DYNAMITE
					SWITCH sTLGData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0		tl31Sprite = "THROWING_DYNAMITE_SMALL_01"	vSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_SMALL_WIDTH, ciTLG_DYNAMITE_SMALL_HEIGHT)		BREAK
						CASE 1		tl31Sprite = "THROWING_DYNAMITE_SMALL_02"	vSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_SMALL_WIDTH, ciTLG_DYNAMITE_SMALL_HEIGHT)		BREAK
						CASE 2		tl31Sprite = "THROWING_DYNAMITE_MEDIUM_01"	vSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_MEDIUM_WIDTH, ciTLG_DYNAMITE_MEDIUM_HEIGHT)	BREAK
						CASE 3		tl31Sprite = "THROWING_DYNAMITE_MEDIUM_02"	vSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_MEDIUM_WIDTH, ciTLG_DYNAMITE_MEDIUM_HEIGHT)	BREAK
						CASE 4		tl31Sprite = "THROWING_DYNAMITE_LARGE_01"	vSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_LARGE_WIDTH, ciTLG_DYNAMITE_LARGE_HEIGHT)		BREAK
						CASE 5		tl31Sprite = "THROWING_DYNAMITE_LARGE_02"	vSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_LARGE_WIDTH, ciTLG_DYNAMITE_LARGE_HEIGHT)		BREAK

						
					ENDSWITCH
					
					
					
					
				BREAK
			ENDSWITCH
			
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_DRAW_THROWN_ENEMY_PROJECTILES] Sprite: ", tl31Sprite)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31CommonPropsTextDict, tl31Sprite,sTLGData.sEnemyProjectiles[i].vSpritePos, vSize, 0, sTLGData.rgbaSprite)
		ELIF sTLGData.sEnemyProjectiles[i].eState = TLG_ENEMY_PROJECTILE_STATE_EXPLODE
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC TLG_DRAW_ITEMS(INT iDistance)

	TEXT_LABEL_23 tl23Sprite
	FLOAT fScale = 1.0

	SWITCH iDistance
	CASE 0
		fScale = 0.75
	BREAK
	CASE 2
		//fScale = 2.5
	BREAK
	ENDSWITCH
	
	IF iDistance = 2
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "ammo1",  GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_PISTOL_AMMO_CLIP_WIDTH*1.0, cfTLG_SPRITE_HUD_PISTOL_AMMO_CLIP_HEIGHT*1.0), 0, sTLGData.rgbaSprite)
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "POWERUP_HEALTH",  GET_DEBUG_TEXTURE_POS(1), INIT_VECTOR_2D(cfTLG_ITEM_HEALTH_WIDTH*1.0, cfTLG_ITEM_HEALTH_HEIGHT*1.0), 0, sTLGData.rgbaSprite)
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "ammo4",  GET_DEBUG_TEXTURE_POS(2), INIT_VECTOR_2D(cfTLG_ITEM_HEALTH_WIDTH*1.0, cfTLG_ITEM_HEALTH_HEIGHT*1.0), 0, sTLGData.rgbaSprite)
	ENDIF
	
	INT i
	FOR i = 0 TO ciTLG_MAX_ITEMS -1
		
		IF sTLGData.sItemData[i].iLane = iDistance 
		AND (sTLGData.sItemData[i].bRendering OR sTLGData.sItemData[i].iRenderAfterWarpCount > 0 OR sTLGData.sItemData[i].iMinActiveStage > 0)
			SWITCH (sTLGData.sItemData[i].eItemType)
				CASE TLG_HEALTH
					tl23Sprite = "POWERUP_HEALTH"
				BREAK
				
				CASE TLG_REVOLVER_AMMO
					tl23Sprite = "ammo1"
				BREAK
				CASE TLG_MACHINE_GUN_AMMO
					tl23Sprite = "ammo4"
				BREAK
				
				CASE TLG_SHOTGUN_AMMO
					tl23Sprite = "ammo2"
				BREAK
				
				CASE TLG_RIFLE_AMMO
					tl23Sprite = "ammo3"
				BREAK
				
				CASE TLG_SNAKE_OIL
					tl23Sprite = "POWERUP_SNAKEOIL"
				BREAK
				
				CASE TLG_SHERIFF
					tl23Sprite = "POWERUP_SHERIFFBADGE"
				BREAK
			ENDSWITCH
			
		//	CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [TLG_DRAW_ITEMS] Drawing item ", i, " Type:", ENUM_TO_INT(sTLGData.sItemData[i].eItemType), " At pos: ", sTLGData.sItemData[i].vSpritePos.x, ", ", sTLGData.sItemData[i].vSpritePos.y)
			IF sTLGData.sItemData[i].bRendering
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite,  sTLGData.sItemData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sItemData[i].vSize.X*fScale, sTLGData.sItemData[i].vSize.Y*fScale), 0, sTLGData.rgbaSprite)
			ENDIF
				
			IF IS_MOVING_LEVEL()
				IF iDistance = 2

					sTLGData.sItemData[i].vSpritePos.x -= TLG_GET_ITEM_UPDATE()
					IF sTLGData.sItemData[i].vSpritePos.x < -200
						sTLGData.sItemData[i].bRendering = FALSE
					ENDIF
				ELSE
					sTLGData.sItemData[i].vSpritePos.x -= TLG_GET_ITEM_UPDATE()
					IF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_DESERT
						IF sTLGData.sItemData[i].vSpritePos.x < -200
							sTLGData.sItemData[i].vSpritePos.x = cfBASE_SCREEN_WIDTH + sTLGData.sItemData[i].vSize.X/2
							sTLGData.sItemData[i].iWarpCount++
						ENDIF
					ELSE
						IF sTLGData.sItemData[i].vSpritePos.x < 0
							sTLGData.sItemData[i].vSpritePos.x = TLG_GET_OFFSET_FOR_ELEMENT_WARP_FOR_CURRENT_LEVEL()
							sTLGData.sItemData[i].iWarpCount++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT sTLGData.sItemData[i].bRendering
				IF sTLGData.sItemData[i].iRenderAfterWarpCount > 0
					IF sTLGData.sItemData[i].iWarpCount = sTLGData.sItemData[i].iRenderAfterWarpCount
						sTLGData.sItemData[i].bRendering = TRUE
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [TLG_DRAW_ITEMS] Setting to render item ", i, " Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[i].eItemType), " As warped this num times: ", sTLGData.sItemData[i].iRenderAfterWarpCount) 
						#ENDIF
					ENDIF
				ENDIF
				
				IF sTLGData.sItemData[i].iMinActiveStage > 0 AND sTLGData.sItemData[i].iMinActiveStage = TLG_GET_CURRENT_LEVEL_STAGE()
					IF NOT TLG_IS_POSITION_ON_SCREEN(sTLGData.sItemData[i].vSpritePos)
						sTLGData.sItemData[i].bRendering = TRUE
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [TLG_DRAW_ITEMS] Setting to render item ", i, " Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[i].eItemType), " iMinActiveStage ", sTLGData.sItemData[i].iMinActiveStage, " = Current stage: ", TLG_GET_CURRENT_LEVEL_STAGE()) 
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC



PROC TLG_DRAW_ENEMIES(INT iLane = 0)

	TEXT_LABEL_63 tl23Sprite
	TEXT_LABEL_31 tl31TextDict = sTLGData.tl31EnemiesTextDict
	INT i
	FLOAT fScale
	VECTOr_2D v2Scale
	RGBA_COLOUR_STRUCT rgbaEnemy = sTLGData.rgbaSprite
	//ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, "cart1", GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_ENEMY_CART_SPRITE_WIDTH*0.9, cfTLG_ENEMY_CART_SPRITE_HEIGHT*0.9), 0, sTLGData.rgbaSprite)/
//	IF iLane = 1
//		fScale = TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(TLG_THROWING_ENEMY_1, 1) 
//		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31EnemiesTextDict, "THROWING_ENEMY_01" ,GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_ENEMY_THROWING_SPRITE_WIDTH*fScale, cfTLG_ENEMY_THROWING_SPRITE_HEIGHT*fScale), 0, sTLGData.rgbaSprite)
//	ENDIF
	FOR i = 0 TO ciTLG_MAX_ENEMIES -1
		
		fScale = TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(sTLGData.sEnemyData[i].iEnemyType, iLane) 
		tl31TextDict = sTLGData.tl31EnemiesTextDict
		IF sTLGData.sEnemyData[i].eState = TLG_DYING 
		AND (sTLGData.sEnemyData[i].iEnemyLane = iLane 
			OR (iLane = ciTLG_LANE_BACKGROUND AND(sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_POPUP_LHS OR sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_POPUP_RHS)))
		
			SWITCH sTLGData.sEnemyData[i].iEnemyType
			CASE TLG_ENEMY_1
				tl23Sprite = "ENEMY_01_DEAD"
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfTLG_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE TLG_ENEMY_2
				tl23Sprite = "ENEMY_02_DEAD"
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfTLG_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE TLG_ENEMY_3
				tl23Sprite = "ENEMY_03_DEAD"
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfTLG_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE TLG_ENEMY_4
				tl23Sprite = "ENEMY_04_DEAD"
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfTLG_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE TLG_ENEMY_5
				tl23Sprite = "ENEMY_05_DEAD"
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfTLG_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			
			CASE TLG_THROWING_ENEMY_1
				tl23Sprite = "THROWING_ENEMY_06"
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfTLG_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			
			CASE TLG_CART_1
				tl23Sprite = "cart"
				tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
				v2Scale = INIT_VECTOR_2D(cfTLG_ENEMY_CART_SPRITE_WIDTH*fScale, cfTLG_ENEMY_CART_SPRITE_HEIGHT*fScale)
			BREAK
			
			CASE TLG_OUTHOUSE_ENEMY
					
				IF sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES -1
					tl23Sprite = "OUTHOUSE_ENEMY_06"
				ELIF sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES
					tl23Sprite = "OUTHOUSE_EMPTY"
					tl31TextDict = sTLGData.tl31Level1TextDict
				ELSE
					tl23Sprite = "OUTHOUSE_ENEMY_05"
				ENDIF
				
			//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_ENEMIES] {DSW} TLG_OUTHOUSE_ENEMY - iSpriteAnimFrame: ", sTLGData.sEnemyData[i].iSpriteAnimFrame, " texture: ", tl23Sprite)
				v2Scale = INIT_VECTOR_2D(cfTLG_OUTHOUSE_SPECIAL_SPRITE_WIDTH*fScale, cfTLG_OUTHOUSE_SPECIAL_SPRITE_HEIGHT*fScale)
			BREAK
			
			CASE TLG_COFFIN_ENEMY
				IF sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES -1
					tl23Sprite = "LEVEL_05_GRAVEYARD_COFFIN_ENEMY_06"
					tl31TextDict = sTLGData.tl31Level5TextDict
				ELIF sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES
					tl23Sprite = "GRAVEYARD_COFFIN_EMPTY"
					tl31TextDict = sTLGData.tl31Level5TextDict
				ELSE
					tl23Sprite = "LEVEL_05_GRAVEYARD_COFFIN_ENEMY_05"
					tl31TextDict = sTLGData.tl31Level5TextDict
				ENDIF
				
			//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_ENEMIES] {DSW} TLG_COFFIN_ENEMY - iSpriteAnimFrame: ", sTLGData.sEnemyData[i].iSpriteAnimFrame, " texture: ", tl23Sprite)
				v2Scale = INIT_VECTOR_2D(cfTLG_COFFIN_ENEMY_SPRITE_WIDTH*fScale, cfTLG_COFFIN_ENEMY_SPRITE_HEIGHT*fScale)
	
			BREAK
			ENDSWITCH
			
			IF sTLGData.sEnemyData[i].vSpritePos.x > 0 AND sTLGData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite,sTLGData.sEnemyData[i].vSpritePos, v2Scale, 0, sTLGData.rgbaSprite)
			ENDIF
			
		ElIF sTLGData.sEnemyData[i].eState != TLG_INACTIVE AND sTLGData.sEnemyData[i].eState != TLG_WAITING 
			IF sTLGData.sEnemyData[i].iEnemyLane = iLane
			OR (iLane = ciTLG_LANE_BACKGROUND  AND (sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_POPUP_LHS OR sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_POPUP_RHS))
				
				//-- Flash injured peds
				IF sTLGData.sEnemyData[i].iHealth < TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[i].iEnemyType)
					IF sTLGData.sEnemyData[i].iEnemyFlashTimer = 0
						sTLGData.sEnemyData[i].iEnemyFlashTimer = GET_GAME_TIMER()
						rgbaEnemy = sTLGData.rgbaSprite
					ELIF GET_GAME_TIMER() - sTLGData.sEnemyData[i].iEnemyFlashTimer >= 1000
						rgbaEnemy = sTLGData.rgbaSprite
					ELSE
						IF (ROUND(TO_FLOAT(GET_GAME_TIMER() - sTLGData.sEnemyData[i].iEnemyFlashTimer)/ 100.0)) % 2 = 0
							rgbaEnemy = sTLGData.rgbaEnemyShot
						ELSE
							rgbaEnemy = sTLGData.rgbaSprite
						ENDIF
					ENDIF
				ENDIF
					
				// Process pop up peds at same time as background peds

				SWITCH sTLGData.sEnemyData[i].iEnemyType
				CASE TLG_ENEMY_1
					IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "ENEMY_01_ATTACKING"
					ELSE
						tl23Sprite = "ENEMY_01_RUNNING_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				BREAK
				CASE TLG_ENEMY_2
					IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "ENEMY_02_ATTACKING"
					ELSE
						tl23Sprite = "ENEMY_02_RUNNING_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				BREAK
				CASE TLG_ENEMY_3
					IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "ENEMY_03_ATTACKING"
					ELSE
						tl23Sprite = "ENEMY_03_RUNNING_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				BREAK
				CASE TLG_ENEMY_4
					IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "ENEMY_04_ATTACKING"
					ELSE
						tl23Sprite = "ENEMY_04_RUNNING_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				BREAK
				CASE TLG_ENEMY_5
					IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "ENEMY_05_ATTACKING"
					ELSE
						tl23Sprite = "ENEMY_05_RUNNING_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				BREAK
				CASE TLG_CART_1
					tl23Sprite = "cart1"
				BREAK
				
				CASE TLG_THROWING_ENEMY_1
					IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "THROWING_ENEMY_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 3)
					ELSE
						tl23Sprite = "THROWING_ENEMY_0"
						tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				BREAK
				CASE TLG_OUTHOUSE_ENEMY
					IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
						
						IF !sTLGData.sEnemyData[i].bShotAtLeastOnce
							tl23Sprite = "OUTHOUSE_01"
							tl31TextDict = sTLGData.tl31Level1TextDict
						ELSE
							tl23Sprite = "OUTHOUSE_ENEMY_0"
							tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
						ENDIF
					ELIF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "OUTHOUSE_ENEMY_05"
					ENDIF
				BREAK
				
				CASE TLG_COFFIN_ENEMY
					IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
						
						IF !sTLGData.sEnemyData[i].bShotAtLeastOnce
							tl23Sprite = "GRAVEYARD_COFFIN_01"
							tl31TextDict = sTLGData.tl31Level5TextDict
						ELSE
							tl23Sprite = "LEVEL_05_GRAVEYARD_COFFIN_ENEMY_0"
							tl23Sprite += (sTLGData.sEnemyData[i].iSpriteAnimFrame + 1)
							tl31TextDict = sTLGData.tl31Level5TextDict
						ENDIF
					ELIF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
						tl23Sprite = "LEVEL_05_GRAVEYARD_COFFIN_ENEMY_05"
						tl31TextDict = sTLGData.tl31Level5TextDict
					ENDIF
				BREAK
				ENDSWITCH
				

				IF sTLGData.sEnemyData[i].vSpritePos.x > 0 AND sTLGData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH
				//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_DRAW_ENEMIES] Drawing enemy ",i, " At position X ", sTLGData.sEnemyData[i].vSpritePos.x, " Size: ", sTLGData.sEnemyData[i].vSize.X, ", ", sTLGData.sEnemyData[i].vSize.y, " Scale: ", fScale, " Lane: ",sTLGData.sEnemyData[i].iEnemyLane) 
					ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite,sTLGData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSize.X*fScale, sTLGData.sEnemyData[i].vSize.y*fScale), 0, rgbaEnemy)
				ELSE
				//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_DRAW_ENEMIES] NOT Drawing enemy ", i, " As X Pos: ", sTLGData.sEnemyData[i].vSpritePos.x, " Lane: ",sTLGData.sEnemyData[i].iEnemyLane) 
				ENDIF
			ENDIF
		ElIF sTLGData.sEnemyData[i].iEnemyLane = iLane AND sTLGData.sEnemyData[i].eState = TLG_WAITING 
			SWITCH sTLGData.sEnemyData[i].iEnemyType
				CASE TLG_OUTHOUSE_ENEMY
					IF sTLGData.sEnemyData[i].vSpritePos.x > 0 AND sTLGData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH
						ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, "OUTHOUSE_01",sTLGData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSize.X*fScale, sTLGData.sEnemyData[i].vSize.y*fScale), 0, sTLGData.rgbaSprite)
					ENDIF
				BREAK
				
				CASE TLG_COFFIN_ENEMY
					IF sTLGData.sEnemyData[i].vSpritePos.x > 0 AND sTLGData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH
						tl31TextDict = sTLGData.tl31Level5TextDict
						ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, "GRAVEYARD_COFFIN_01",sTLGData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSize.X*fScale, sTLGData.sEnemyData[i].vSize.y*fScale), 0, sTLGData.rgbaSprite)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDFOR
		
ENDPROC

PROC TLG_DRAW_FOREGROUND_ENEMIES()
	
	TEXT_LABEL_23 tl23Sprite
	TEXT_LABEL_31 tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
	FLOAT fScale = 1.2
	
	INT i
	FOR i = 0 TO ciTLG_MAX_ENEMIES -1
	
		IF sTLGData.sEnemyData[i].iEnemyLane = 2 AND sTLGData.sEnemyData[i].iHealth > 0
			SWITCH sTLGData.sEnemyData[i].iEnemyType
			CASE TLG_FOREGROUND_ENEMY_T1_1
				tl23Sprite = "FOREGROUND_ENEMY_01_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_2
				tl23Sprite = "FOREGROUND_ENEMY_02_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_3
				tl23Sprite = "FOREGROUND_ENEMY_03_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_4
				tl23Sprite = "FOREGROUND_ENEMY_04_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_5
				tl23Sprite = "FOREGROUND_ENEMY_05_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesBTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_1
				tl23Sprite = "FOREGROUND_ENEMY_06_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesBTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_2
				tl23Sprite = "FOREGROUND_ENEMY_07_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesBTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_3
				tl23Sprite = "FOREGROUND_ENEMY_08_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesCTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_4
				tl23Sprite = "FOREGROUND_ENEMY_09_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesCTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_5
				tl23Sprite = "FOREGROUND_ENEMY_10_01"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesCTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_BEAR
				tl23Sprite = "LEVEL_03_FOREST_BEAR_01"
				tl31TextDict = sTLGData.tl31HostagesTextDict
			BREAK
			ENDSWITCH
			ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite,sTLGData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSize.X*fScale, sTLGData.sEnemyData[i].vSize.Y*fScale), 0, sTLGData.rgbaSprite)
		
		ELIF sTLGData.sEnemyData[i].iEnemyLane = 2 AND sTLGData.sEnemyData[i].eState = TLG_DYING
			SWITCH sTLGData.sEnemyData[i].iEnemyType
			CASE TLG_FOREGROUND_ENEMY_T1_1
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
				tl23Sprite = "FOREGROUND_ENEMY_01_02"
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_2
				tl23Sprite = "FOREGROUND_ENEMY_02_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_3
				tl23Sprite = "FOREGROUND_ENEMY_03_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_4
				tl23Sprite = "FOREGROUND_ENEMY_04_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesATextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T1_5
				tl23Sprite = "FOREGROUND_ENEMY_05_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesBTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_1
				tl23Sprite = "FOREGROUND_ENEMY_06_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesBTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_2
				tl23Sprite = "FOREGROUND_ENEMY_07_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesBTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_3
				tl23Sprite = "FOREGROUND_ENEMY_08_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesCTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_4
				tl23Sprite = "FOREGROUND_ENEMY_09_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesCTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_T2_5
				tl23Sprite = "FOREGROUND_ENEMY_10_02"
				tl31TextDict = sTLGData.tl31ForegroundEnemiesCTextDict
			BREAK
			CASE TLG_FOREGROUND_ENEMY_BEAR
				tl23Sprite = "LEVEL_03_FOREST_BEAR_02"
			BREAK
			ENDSWITCH
			
			IF sTLGData.sEnemyData[i].iEnemyType = TLG_FOREGROUND_ENEMY_BEAR
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HostagesTextDict, tl23Sprite,sTLGData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_ENEMY_BEAR_DEAD_SPRITE_WIDTH*1.2, cfTLG_ENEMY_BEAR_DEAD_SPRITE_HEIGHT*1.2), 0, sTLGData.rgbaSprite)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite,sTLGData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_ENEMY_FOREGROUND_DEAD_SPRITE_WIDTH*1.2, cfTLG_ENEMY_FOREGROUND_DEAD_SPRITE_HEIGHT*1.2), 0, sTLGData.rgbaSprite)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC TLG_FLASH_SCREEN_FOR_ACTIVE_EFFECT(RGBA_COLOUR_STRUCT colour, INT iMaxAlpha = 50)

	
	FLOAT fTimeToMaxAlpha = 5.0
	FLOAT fFrameTime = GET_FRAME_TIME()
	IF NOT sTLGData.bEffectHitmaxAlpha
		sTLGData.fEffectFlashAlpha += (TO_FLOAT(iMaxAlpha) / (fTimeToMaxAlpha/fFrameTime))
	//	stlgdata.fTotalTimeTaken += fFrameTime
		IF sTLGData.fEffectFlashAlpha > TO_FLOAT(iMaxAlpha)
			sTLGData.fEffectFlashAlpha = TO_FLOAT(iMaxAlpha)
			sTLGData.bEffectHitmaxAlpha = TRUE
		ENDIF
	ELSE
//		stlgdata.fTotalTimeTaken = 0
		sTLGData.fEffectFlashAlpha -= (TO_FLOAT(iMaxAlpha) / (fTimeToMaxAlpha/fFrameTime))
		IF sTLGData.fEffectFlashAlpha < 0
			sTLGData.fEffectFlashAlpha = 0
			sTLGData.bEffectHitmaxAlpha = FALSE
		ENDIF
	ENDIF
	
	colour.iA = ROUND(sTLGData.fEffectFlashAlpha)
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), colour)	
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [TLG_FLASH_SCREEN_FOR_ACTIVE_EFFECT] Alpha: ", sTLGData.fEffectFlashAlpha, " Frame time: ", fFrameTime, " Delta: ", ROUND(TO_FLOAT(iMaxAlpha) / (fTimeToMaxAlpha/fFrameTime)))
ENDPROC



PROC TLG_DRAW_BULLETS_AND_EFFECTS(INT iDistance)
	
	TEXT_LABEL_63 tl23Sprite
	INT p,i
	
	// Bullets
	FOR p = 0 TO ciTLG_MAX_PLAYERS - 1
	
		FOR i = 0  TO ciTLG_MAX_PROJECTILES - 1
			IF sTLGData.sPlayerData[p].sBulletData[i].bShot
					
				IF sTLGData.sPlayerData[p].sBulletData[i].bHit
					tl23Sprite = "bullet-hole"
				
				ELIF sTLGData.sPlayerData[p].sBulletData[i].bHitObject OR sTLGData.sPlayerData[p].sBulletData[i].vSpritePos.y < 300
					tl23Sprite = "smoke"
				
				ELSE
					tl23Sprite = "bullet"
					
				ENDIF
				
				
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, sTLGData.sPlayerData[p].sBulletData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_SPRITE_IMPACT_WIDTH, cfTLG_SPRITE_IMPACT_HEIGHT), 0, sTLGData.rgbaSprite)
				
				IF NOT sTLGData.sPlayerData[p].sBulletData[i].bPlayedAudio
				//	TLG_PLAY_SOUND_FRONT_END(TLG_AUDIO_EFFECT_BULLET_HIT_GENERIC)
					
					sTLGData.sPlayerData[p].sBulletData[i].bPlayedAudio = TRUE
				ENDIF
				
//				FOR j = 0 TO ciTLG_MAX_MENU_OPTIONS-1
//					IF sTLGData.sMenuOptionsData[j].eMenuOption = TLG_START
//						CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] Drawing temp sprite") 
//						x1 = sTLGData.sMenuOptionsData[j].vSpritePos.x - (sTLGData.sMenuOptionsData[j].vSize.x/2)
//						y1 = sTLGData.sMenuOptionsData[j].vSpritePos.y - (sTLGData.sMenuOptionsData[j].vSize.y/2)
//				
//						x2 = sTLGData.sMenuOptionsData[j].vSpritePos.x + (sTLGData.sMenuOptionsData[j].vSize.x/2)
//						y2 = sTLGData.sMenuOptionsData[j].vSpritePos.y + (sTLGData.sMenuOptionsData[j].vSize.y/2)
//						
//						ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, tl23Sprite,INIT_VECTOR_2D(x1, y1), INIT_VECTOR_2D(cfTLG_SPRITE_IMPACT_WIDTH, cfTLG_SPRITE_IMPACT_HEIGHT), 0, sTLGData.rgbaSprite)
//						ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, tl23Sprite,INIT_VECTOR_2D(x2, y2), INIT_VECTOR_2D(cfTLG_SPRITE_IMPACT_WIDTH, cfTLG_SPRITE_IMPACT_HEIGHT), 0, sTLGData.rgbaSprite)
//						ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, tl23Sprite,INIT_VECTOR_2D(sTLGData.sMenuOptionsData[j].vSpritePos.x, sTLGData.sMenuOptionsData[j].vSpritePos.Y), INIT_VECTOR_2D(cfTLG_SPRITE_IMPACT_WIDTH, cfTLG_SPRITE_IMPACT_HEIGHT), 0, sTLGData.rgbaSprite)
//					ENDIF
//				ENDFOR
			ENDIF
		ENDFOR
		
	ENDFOR
	
	// FX
	VECTOR_2D vSize
	
	FOR i = 0  TO ciTLG_MAX_FX - 1
		IF sTLGData.sFXData[i].bActive AND sTLGData.sFXData[i].iLane = iDistance
			
			SWITCH sTLGData.sFXData[i].eFXType
			CASE TLG_FX_ENEMY_SHOT
			
				tl23Sprite = "fg-enemy-gunfire"
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31GameplayCommonTextDict, tl23Sprite, sTLGData.sFXData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_SPRITE_ENEMY_SHOT_WIDTH, cfTLG_SPRITE_ENEMY_SHOT_HEIGHT), 0, sTLGData.rgbaSprite)
			
			BREAK
			CASE TLG_FX_EXPLOSION
			
				tl23Sprite = "PROP_TNT_CRATE_STATE_0"
				tl23Sprite += (sTLGData.sFXData[i].iSpriteBlastAnimFrame + 2)
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31GameplayCommonTextDict, tl23Sprite, sTLGData.sFXData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_SPRITE_EXPLOSION_WIDTH, cfTLG_SPRITE_EXPLOSION_HEIGHT), 0, sTLGData.rgbaSprite)
			
			BREAK
			
			CASE TLG_FX_ENEMY_TNT_EXP
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
				OR sTLGData.sFXData[i].iSpriteBlastAnimFrame = 1
					tl23Sprite = "THROWN_WEAPON_EXP_01"
					vSize = INIT_VECTOR_2D(cfTLG_SPRITE_ENEMY_THROWN_EXPLOSION_SMALL_WIDTH, cfTLG_SPRITE_ENEMY_THROWN_EXPLOSION_SMALL_HEIGHT)
				ELSE
					tl23Sprite = "THROWN_WEAPON_EXP_02"
					vSize = INIT_VECTOR_2D(cfTLG_SPRITE_ENEMY_THROWN_EXPLOSION_LARGE_WIDTH, cfTLG_SPRITE_ENEMY_THROWN_EXPLOSION_LARGE_HEIGHT)
				ENDIF
			//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_BULLETS_AND_EFFECTS] {DSW} SPrite: ", tl23Sprite, " Frame: ", sTLGData.sFXData[i].iSpriteBlastAnimFrame)
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31GameplayCommonTextDict, tl23Sprite, sTLGData.sFXData[i].vSpritePos, vSize, 0, sTLGData.rgbaSprite)
			BREAK
			
			CASE TLG_FX_LOOT_CRATE
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
					tl23Sprite = "CRATE_04"
				ELIF sTLGData.sFXData[i].iSpriteBlastAnimFrame = 1
					tl23Sprite = "CRATE_05"
				ELIF sTLGData.sFXData[i].iSpriteBlastAnimFrame = 2
					tl23Sprite = "CRATE_06"
				ENDIF
				vSize = INIT_VECTOR_2D(cfTLG_SPRITE_LOOT_CRATE_EXPLOSION_WIDTH, cfTLG_SPRITE_LOOT_CRATE_EXPLOSION_HEIGHT)
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_BULLETS_AND_EFFECTS] {DSW} SPrite: ", tl23Sprite, " Frame: ", sTLGData.sFXData[i].iSpriteBlastAnimFrame)
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31GameplayCommonTextDict, tl23Sprite, sTLGData.sFXData[i].vSpritePos, vSize, 0, sTLGData.rgbaSprite)
			BREAK
			
			CASE TLG_FX_FEATHERS
				tl23Sprite = "ANIMALBONUS_BIRDS_KILL_FEATHERS_0"
				tl23Sprite += (sTLGData.sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31GameplayCommonTextDict, tl23Sprite, sTLGData.sFXData[i].vSpritePos, INIT_VECTOR_2D(cfTLG_SPRITE_FEATHERS_WIDTH, cfTLG_SPRITE_FEATHERS_HEIGHT), 0, sTLGData.rgbaSprite)
			BREAK
			ENDSWITCH
		ENDIF
	ENDFOR
	
	//TLG_FLASH_SCREEN_FOR_ACTIVE_EFFECT(sTLGData.rgbaWhite)
	
	RGBA_COLOUR_STRUCT rgbaDead = sTLGData.rgbaRed
	rgbaDead.iA = 255
	
	IF TLG_IS_PLAYER_PLAYING()
		IF TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0
			IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_INVULNERABILE)
				TLG_FLASH_SCREEN_FOR_ACTIVE_EFFECT(sTLGData.rgbaYellow)
			ELIF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_HIDE_RETICULE)
				TLG_FLASH_SCREEN_FOR_ACTIVE_EFFECT(sTLGData.rgbaGreen)
			ELSE
				IF sTLGData.fEffectFlashAlpha != 0
					sTLGData.fEffectFlashAlpha = 0
				ENDIF
			ENDIF
		ELSE
			
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaDead)
			
		ENDIF
	ELSE
		IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_INVULNERABILE)
			TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_INVULNERABILE)
			TLG_STOP_SOUND(TLG_AUDIO_EFFECT_POWERUP)
			TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
		ENDIF
		
		IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_HIDE_RETICULE)
			TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_HIDE_RETICULE)
			TLG_STOP_SOUND(TLG_AUDIO_EFFECT_POWERDOWN)
			TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
		ENDIF
	ENDIF
	
ENDPROC

PROC TLG_DRAW_RETICULES()
	IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.0
		EXIT
	ENDIF
	
	TEXT_LABEL_23 tl23Sprite
	
	IF TLG_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
		tl23Sprite = "RETICLE_PLAYER1"
	ELSE
		tl23Sprite = "RETICLE_PLAYER2"
	ENDIF
	
	RGBA_COLOUR_STRUCT rgbaReticule = sTLGData.rgbaSprite
		
	IF TLG_IS_PLAYER_PLAYING()
		IF TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() <= 0
			tl23Sprite = "RETICLE_RELOAD"
		ENDIF
	ENDIF
	
	IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_HIDE_RETICULE)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite,sTLGData.sPlayerData[0].vSpriteMarkerPos, INIT_VECTOR_2D(cfTLG_SPRITE_MARKER_WIDTH, cfTLG_SPRITE_MARKER_HEIGHT), 0, rgbaReticule)
	ENDIF
	
	
//	INT p
//	FOR p = 0 TO ciTLG_MAX_PLAYERS - 1
//		
//		IF p = 0
//			tl23Sprite = "RETICLE_PLAYER1"
//		ELSE
//			tl23Sprite = "RETICLE_PLAYER2"
//		ENDIF
//		
//	ENDFOR
	
ENDPROC

PROC TLG_DRAW_SHOT_FLASH()
	TEXT_LABEL_23 tl23Sprite

	INT p
	FOR p = 0 TO ciTLG_MAX_PLAYERS - 1
		
		IF sTLGData.sPlayerData[p].bHasJustShot
			/*
				ON PC
				Pistol shot diff ~ 450ms
				Gatling gun ~ 120 ms
			*/
			
			IF sTLGData.sPlayerData[p].iLastShotTime = 0
			OR GET_GAME_TIMER() - sTLGData.sPlayerData[p].iLastShotTime > 200
				ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTLGData.rgbaSprite)
				sTLGData.sPlayerData[p].iLastShotTime = GET_GAME_TIMER()
			ENDIF
			
//			IF sTLGData.sPlayerData[p].iLastShotTime = 0
//				sTLGData.sPlayerData[p].iLastShotTime = GET_GAME_TIMER()
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_SHOT_FLASH] {DSW} Grabbed last shot time")
//			ELSE
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_SHOT_FLASH] {DSW} Time since last shot ",  GET_GAME_TIMER() - sTLGData.sPlayerData[p].iLastShotTime)
//				sTLGData.sPlayerData[p].iLastShotTime = GET_GAME_TIMER()
//			ENDIF
		ENDIF
		
	ENDFOR
	
	BOOL bShowRedRect = TRUE
	BOOL bAttackedBybear = TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_DAMAGED_BY_BEAR)
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_BadlandsNoDeath")
			bShowRedRect = FALSE
			bAttackedBybear = FALSE
		ENDIF
	#ENDIF
	
	
	IF bShowRedRect
		IF sTLGData.sPlayerData[0].iHitCountdown > 0
			sTLGData.sPlayerData[0].iHitCountdown --
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTLGData.rgbaRed)
			
		ENDIF
	ENDIF
	

	
	
	IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.2
	OR GET_PLAYER_TOTAL_AMMO_FOR_ALL_WEAPON_TYPES() <= 0
		
		tl23Sprite = "damage1"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_1_X_POS, cfTLG_DAMAGE_1_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
			
		tl23Sprite = "damage2"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_2_X_POS, cfTLG_DAMAGE_2_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "damage3"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_3_X_POS, cfTLG_DAMAGE_3_X_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "damage4"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_4_X_POS, cfTLG_DAMAGE_4_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
	
		tl23Sprite = "damage1"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_5_X_POS, cfTLG_DAMAGE_5_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
	
	ELIF TLG_GET_LOCAL_PLAYER_HEALTH() < 0.35
		
		tl23Sprite = "damage1"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_1_X_POS, cfTLG_DAMAGE_1_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
			
		tl23Sprite = "damage2"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_2_X_POS, cfTLG_DAMAGE_2_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "damage3"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_3_X_POS, cfTLG_DAMAGE_3_X_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "damage4"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_4_X_POS, cfTLG_DAMAGE_4_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
	
	ELIF TLG_GET_LOCAL_PLAYER_HEALTH() < 0.50
		
		tl23Sprite = "damage1"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_1_X_POS, cfTLG_DAMAGE_1_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
			
		tl23Sprite = "damage2"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_2_X_POS, cfTLG_DAMAGE_2_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "damage3"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_3_X_POS, cfTLG_DAMAGE_3_X_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
	ELIF TLG_GET_LOCAL_PLAYER_HEALTH() < 0.75

		tl23Sprite = "damage1"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_1_X_POS, cfTLG_DAMAGE_1_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "damage2"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_2_X_POS, cfTLG_DAMAGE_2_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
		
	ELIF TLG_GET_LOCAL_PLAYER_HEALTH() < 1.0
		
		tl23Sprite = "damage1"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_1_X_POS, cfTLG_DAMAGE_1_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_DAMAGE_WIDTH, cfTLG_SPRITE_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
			
	ENDIF
	
	IF bAttackedBybear
		tl23Sprite = "BEARCLAWS_SCREEN_DAMAGE" 
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_DAMAGE_BEAR_X_POS, cfTLG_DAMAGE_BEAR_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_BEAR_DAMAGE_WIDTH, cfTLG_SPRITE_BEAR_DAMAGE_HEIGHT), 0, sTLGData.rgbaSprite)
	ENDIF
ENDPROC

PROC TLG_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(TLG_PLAYER_WEAPON weapon, INT iClipPos, VECTOR_2D &v2Pos, VECTOR_2D &v2Size)

	SWITCH weapon
		CASE TLG_PLAYER_WEAPON_PISTOL	
			v2Size 	= INIT_VECTOR_2D(cfTLG_SPRITE_HUD_PISTOL_AMMO_SINGLE_WIDTH, cfTLG_SPRITE_HUD_PISTOL_AMMO_SINGLE_HEIGHT)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_0_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_1_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_2_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_3_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_4_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_5_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
		
		CASE TLG_PLAYER_WEAPON_RIFLE	
			v2Size 	= INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RIFLE_AMMO_SINGLE_WIDTH, cfTLG_SPRITE_HUD_RIFLE_AMMO_SINGLE_HEIGHT)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_0_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_1_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_2_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_3_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_4_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_5_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
		
		CASE TLG_PLAYER_WEAPON_SHOTGUN	
			v2Size 	= INIT_VECTOR_2D(cfTLG_SPRITE_HUD_SHOTGUN_AMMO_SINGLE_WIDTH, cfTLG_SPRITE_HUD_SHOTGUN_AMMO_SINGLE_HEIGHT)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_0_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_1_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_2_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_3_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_4_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfTLG_SINGLE_PISTOL_AMMO_5_X, cfTLG_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC TLG_DRAW_SINGLE_AMMO(STRING texture, VECTOR_2D pos, VECTOR_2D size, INT iClipPos, INT iAmmoInClip, INT iClipSize)
	RGBA_COLOUR_STRUCT rgba = sTLGData.rgbaSprite
	INT iBulletsFired = iClipSize - iAmmoInClip

	IF iBulletsFired > iClipPos 

		rgba = sTLGData.rgbaSpriteUsedBullet
	ENDIF
	
//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_DRAW_SINGLE_AMMO] iClipPos: ", iClipPos, " Fired: ", iBulletsFired, " iAmmoInClip: ",iAmmoInClip, " iClipSize: ", iClipSize, " Should fade? ", bShouldFade)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, texture, pos, size, 0, rgba)
ENDPROC

PROC TLG_DRAW_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
	TEXT_LABEL_31 tl23Sprite
	
	INT iAmmoInClip
	INT iClipSize
	INT i
	VECTOR_2D v2AmmoPos
	VECTOR_2D v2AmmoSize
	SWITCH TLG_GET_PLAYER_CURRENT_WEAPON()
		CASE TLG_PLAYER_WEAPON_PISTOL
			tl23Sprite = "ammo_pistol1"
			iAmmoInClip = TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = TLG_GET_PLAYER_CURRENT_WEAPON_CLIP_SIZE()
			
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				TLG_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(TLG_PLAYER_WEAPON_PISTOL, i, v2AmmoPos, v2AmmoSize)
				TLG_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK
		
		CASE TLG_PLAYER_WEAPON_SHOTGUN
			tl23Sprite = "INDIVIDUAL_AMMO_ICON_SHOTGUN"
			iAmmoInClip = TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = TLG_GET_PLAYER_CURRENT_WEAPON_CLIP_SIZE()
			
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				TLG_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(TLG_PLAYER_WEAPON_PISTOL, i, v2AmmoPos, v2AmmoSize)
				TLG_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK
		
		CASE TLG_PLAYER_WEAPON_RIFLE
			tl23Sprite = "INDIVIDUAL_AMMO_ICON_RIFLE"
			iAmmoInClip = TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = TLG_GET_PLAYER_CURRENT_WEAPON_CLIP_SIZE()
			
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				TLG_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(TLG_PLAYER_WEAPON_PISTOL, i, v2AmmoPos, v2AmmoSize)
				TLG_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK
	ENDSWITCH
ENDPROC
PROC TLG_DRAW_HUD()
	
	TEXT_LABEL_23 tl23Sprite
	
	//FLOAT fScale = GET_DEBUG_TEXTURE_SCALE(0)
	
	
	// Fade
	IF NOT sTLGData.bFadingOut AND sTLGData.iFade > 0
		
		RGBA_COLOUR_STRUCT rgbaFade
		rgbaFade.iA = sTLGData.iFade
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaFade)
		sTLGData.iFade -= 2
		
		// Placeholder
		tl23Sprite = "goodluck"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(328, 140), 0, sTLGData.rgbaSprite)
	
	ELIF sTLGData.bFadingOut AND sTLGData.iFade < 255
		
		RGBA_COLOUR_STRUCT rgbaFade
		rgbaFade.iA = sTLGData.iFade
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaFade)
		sTLGData.iFade += 2
		
		IF TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0	
			// Placeholder
			tl23Sprite = "scenecleared"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(500, 152), 0, sTLGData.rgbaSprite)
		ENDIF
	
	ENDIF
	
	// HP Bar
	IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_INVULNERABILE)
		tl23Sprite = "HPbar"
	ELSE
		tl23Sprite = "HPBarYellow"
	ENDIF
	FLOAT fBarSize = cfTLG_SPRITE_HUD_HP_WIDTH * TLG_GET_LOCAL_PLAYER_HEALTH()
	FLOAT fOffset = (cfTLG_SPRITE_HUD_HP_WIDTH - fBarSize)/2
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, ADD_VECTOR_2D(INIT_VECTOR_2D(0,0), INIT_VECTOR_2D(cfTLG_HP_X_POS - fOffset, cfTLG_HP_Y_POS)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(fBarSize, cfTLG_SPRITE_HUD_HP_HEIGHT), 1), 0, sTLGData.rgbaSprite)

	// HP Border
	tl23Sprite = "HPborder"
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_HP_X_POS, cfTLG_HP_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_HP_WIDTH, cfTLG_SPRITE_HUD_HP_HEIGHT), 0, sTLGData.rgbaSprite)
	
	IF TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_GATLING
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "HPborder", INIT_VECTOR_2D(cfTLG_GATLING_TEMP_BORDER_X_POS, cfTLG_GATLING_TEMP_BORDER_Y_POS),  INIT_VECTOR_2D(cfTLG_GATLING_TEMP_BORDER_WIDTH, cfTLG_GATLING_TEMP_BORDER_HEIGHT), 0, sTLGData.rgbaSprite)
	
		IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_GATLING_OVERHEAT)
			tl23Sprite = "HPbar"
		ELSE
			tl23Sprite = "HPBarYellow"
		ENDIF
		fBarSize = (cfTLG_GATLING_TEMP_BORDER_WIDTH ) * sTLGData.sPlayerData[0].fGatlingGunTemp
		fOffset = ((cfTLG_GATLING_TEMP_BORDER_WIDTH ) - fBarSize)/2
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, ADD_VECTOR_2D(INIT_VECTOR_2D(0,0), INIT_VECTOR_2D(cfTLG_GATLING_TEMP_BORDER_X_POS - fOffset, cfTLG_GATLING_TEMP_BORDER_Y_POS)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(fBarSize, cfTLG_GATLING_TEMP_BORDER_HEIGHT), 1), 0, sTLGData.rgbaSprite)
	ENDIF
	
	// Ammo
//	tl23Sprite = "ammo1"
//	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_AMMO_X_POS, cfTLG_AMMO_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_AMMO_WIDTH, cfTLG_SPRITE_HUD_AMMO_HEIGHT), 0, sTLGData.rgbaSpriteUsedBullet)
	
	TLG_DRAW_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
	
	TEXT_LABEL_23 n1, n2
	INT iTotalAmmo = TLG_GET_PLAYER_TOTAL_AMMO_IN_CURRENT_WEAPON(FALSE) 
//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW}[TLG_GET_PLAYER_TOTAL_AMMO_IN_CURRENT_WEAPON] iTotalAmmo: ", iTotalAmmo)
	IF iTotalAmmo > 99
		n1 = "AMMO_NUMERICAL_9"
		n2 = "AMMO_NUMERICAL_9"
	ELSE
		n1 = "AMMO_NUMERICAL_"
		n1 += iTotalAmmo / 10
		n2 = "AMMO_NUMERICAL_"
		n2 += iTotalAmmo % 10
	ENDIF

	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, n1, INIT_VECTOR_2D(cfTLG_AMMO_QTY_1_X_POS, cfTLG_AMMO_QTY_1_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_AMMO_QTY_WIDTH, cfTLG_SPRITE_HUD_AMMO_QTY_HEIGHT), 0, sTLGData.rgbaSprite)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, n2, INIT_VECTOR_2D(cfTLG_AMMO_QTY_2_X_POS, cfTLG_AMMO_QTY_2_Y_POS), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_AMMO_QTY_WIDTH, cfTLG_SPRITE_HUD_AMMO_QTY_HEIGHT), 0, sTLGData.rgbaSprite)
	
	INT iCurrentLevel = TLG_GET_CURRENT_LEVEL_AS_INT()
	
	IF TLG_GET_NUMBER_OF_PLAYERS() <= 1
		// My Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_DOLLAR_Y", INIT_VECTOR_2D(cfTLG_MY_SCORE_DOLLAR_X_POS, cfTLG_MY_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		TLG_DRAW_SCORE_INTEGER(GET_LOCAL_PLAYER_SCORE(), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_MY_SCORE_X_POS, cfTLG_MY_SCORE_Y_POS), sTLGData.rgbaSprite,TLG_SCORE_INTEGER_TYPE_MY_SCORE)
		
		
		// My Hi Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_DOLLAR_Y", INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_POS, cfTLG_MY_HISCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		TLG_DRAW_SCORE_INTEGER(TLG_GET_LOCAL_PLAYER_HI_SCORE(), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_MY_HISCORE_X_POS, cfTLG_MY_HISCORE_Y_POS), sTLGData.rgbaSprite, TLG_SCORE_INTEGER_TYPE_HI_SCORE ) //
	
	ELSE
	 	// 'Red' (1P) Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_DOLLAR_Y", INIT_VECTOR_2D(cfTLG_MY_SCORE_DOLLAR_X_POS, cfTLG_MY_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		TLG_DRAW_SCORE_INTEGER(TLG_GET_RED_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT()), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_MY_SCORE_X_POS, cfTLG_MY_SCORE_Y_POS), sTLGData.rgbaSprite,TLG_SCORE_INTEGER_TYPE_RED_SCORE)
		
		
		// Player with Highest score on this level
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_DOLLAR_Y", INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_POS, cfTLG_MY_HISCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		
		INT iPart = TLG_GET_PARTICIPANT_WITH_HIGHEST_HI_SCORE_FOR_CURRENT_LEVEL()
		
		INT iHighestScore = TLG_GET_PARTICIPANT_HI_SCORE_FOR_LEVEL_FROM_HOST(iPart,iCurrentLevel )
		TLG_SCORE_INTEGER_TYPE eHiScoreType
		
		IF iHighestScore > 0
			IF iPart = PARTICIPANT_ID_TO_INT()
				//-- I've got the highest score
				IF TLG_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
					//-- I'm in the left hand cabinet - Hi score should be red
					eHiScoreType = TLG_SCORE_INTEGER_TYPE_RED_SCORE
				ELSE
					//-- I'm in the right hand cabinbet - Hi score should be blue
					eHiScoreType = TLG_SCORE_INTEGER_TYPE_BLUE_SCORE
				ENDIF
			ELSE
				//--Rival has highest score
				IF TLG_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
					//-- Rival in right hand cabinet - score should be blue
					eHiScoreType = TLG_SCORE_INTEGER_TYPE_BLUE_SCORE
				ELSE
					//-- Rival in left hand cabinet - score should be red
					eHiScoreType = TLG_SCORE_INTEGER_TYPE_RED_SCORE
				ENDIF
			ENDIF
		ELSE
			//-- Just make the hi score white if nobody has a score yet
			eHiScoreType = TLG_SCORE_INTEGER_TYPE_HI_SCORE
		ENDIF
		TLG_DRAW_SCORE_INTEGER(iHighestScore, ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_MY_HISCORE_X_POS, cfTLG_MY_HISCORE_Y_POS), sTLGData.rgbaSprite, eHiScoreType) //
		
		// 'Blue' (2P) Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_DOLLAR_Y", INIT_VECTOR_2D(cfTLG_BLUE_SCORE_DOLLAR_X_POS, cfTLG_BLUE_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		TLG_DRAW_SCORE_INTEGER(TLG_GET_BLUE_PLAYER_SCORE_FOR_LEVEL(iCurrentLevel), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_BLUE_SCORE_X_POS, cfTLG_BLUE_SCORE_Y_POS), sTLGData.rgbaSprite, TLG_SCORE_INTEGER_TYPE_BLUE_SCORE)
		
		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel) > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel)
			//-- Red 'Win'
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_WIN_GRAPHIC_RED", INIT_VECTOR_2D(cfTLG_SCORE_WIN_RED_X_POS, cfTLG_SCORE_WIN_RED_Y_POS), INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE, cfTLG_SCORE_WIN_Y_SIZE), 0, sTLGData.rgbaSprite)
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel) < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel)	
			//-- Blue 'Win'
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_WIN_GRAPHIC_BLUE", INIT_VECTOR_2D(cfTLG_SCORE_WIN_BLUE_X_POS, cfTLG_SCORE_WIN_BLUE_Y_POS), INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE, cfTLG_SCORE_WIN_Y_SIZE), 0, sTLGData.rgbaSprite)
		ENDIF
	
	ENDIF
	
	IF TLG_SHOULD_SHOW_RELOAD_PROMPT()
		IF TLG_GET_PLAYER_TOTAL_AMMO_IN_CURRENT_WEAPON() = 0
		AND sTLGData.sPlayerData[0].weaponType = TLG_PLAYER_WEAPON_TYPE_DEFAULT
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "TYPE_PROMPT_NO_AMMO", INIT_VECTOR_2D(cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_X, cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_Y), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_WIDTH, cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_HEIGHT), 0, sTLGData.rgbaSprite)
		ELSE
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "TYPE_PROMPT_RELOAD", INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RELOAD_PROMPT_X, cfTLG_SPRITE_HUD_RELOAD_PROMPT_Y), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RELOAD_PROMPT_WIDTH, cfTLG_SPRITE_HUD_RELOAD_PROMPT_HEIGHT), 0, sTLGData.rgbaSprite)
		ENDIF		
	ENDIF
	
//	IF TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_GATLING
//		IF sTLGData.sPlayerData[0].fGatlingGunTemp = 1.0
//			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "TYPE_PROMPT_RELOAD", INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RELOAD_PROMPT_X, cfTLG_SPRITE_HUD_RELOAD_PROMPT_Y), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RELOAD_PROMPT_WIDTH, cfTLG_SPRITE_HUD_RELOAD_PROMPT_HEIGHT), 0, sTLGData.rgbaSprite)
//		ELIF sTLGData.sPlayerData[0].fGatlingGunTemp = 0.0
//			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "TYPE_PROMPT_NO_AMMO", INIT_VECTOR_2D(cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_X, cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_Y), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_WIDTH, cfTLG_SPRITE_HUD_NO_AMMO_PROMPT_HEIGHT), 0, sTLGData.rgbaSprite)
//		ENDIF
//	ENDIF
//	DEGENATRON_GAMES_DRAW_INTEGER(INT iValue, INT iDigits, VECTOR_2D vCenter,RGBA_COLOUR_STRUCT rgba)
ENDPROC

#IF IS_DEBUG_BUILD

PROC DEBUG_TEXT_FLOAT_IN_SCREEN(STRING text, FLOAT fValue,FLOAT x, FLOAT y, INT r = 255, INT g = 255, INT b = 255)
	
	TEXT_PLACEMENT textPlacement
	textPlacement.x = x
	textPlacement.y = y
	TEXT_STYLE textStyle
	SET_TITLE_SMALL_TEXT(textStyle)
	textStyle.XScale = 0.2
	textStyle.YScale = 0.2
	
	TEXT_LABEL_63 output
	output = text
	output += " = "
	output += FLOAT_TO_STRING(fValue)
	textStyle.r = r
	textStyle.g = g
	textStyle.b = b

	DRAW_TEXT_WITH_STRING(textPlacement, textStyle, "DBG_STRING ",TEXT_LABEL_TO_STRING(output))
	
ENDPROC 
PROC DEBUG_TEXT_INT_IN_SCREEN(STRING text, INT iValue,FLOAT x, FLOAT y, INT r = 255, INT g = 255, INT b = 255)
	TEXT_PLACEMENT textPlacement
	textPlacement.x = x
	textPlacement.y = y
	TEXT_STYLE textStyle
	SET_TITLE_SMALL_TEXT(textStyle)
	textStyle.XScale = 0.2
	textStyle.YScale = 0.2
	
	TEXT_LABEL_63 output
	output = text
	output += " = "
	output += iValue
	textStyle.r = r
	textStyle.g = g
	textStyle.b = b

	DRAW_TEXT_WITH_STRING(textPlacement, textStyle, "DBG_STRING",TEXT_LABEL_TO_STRING(output)) 
ENDPROC

PROC TLG_DO_ON_SCREEN_DEBUG()
	//	DEBUG_TEXT_FLOAT_IN_SCREEN("fTravelRate ",sDGMonkeyData.sLevelData.fTravelRate,0.16,0.05+0.01*iLine)
	FLOAT fYpos =  0.2
	DEBUG_TEXT_INT_IN_SCREEN("Stage: ", TLG_GET_CURRENT_LEVEL_STAGE(), 0.16, fYpos)
	fYpos+=0.025
	DEBUG_TEXT_INT_IN_SCREEN("Enemies: ", TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE(), 0.16, fYpos)
	fYpos+=0.025
	DEBUG_TEXT_INT_IN_SCREEN("Rightmost: ", sTLGData.iRightMostCover, 0.16, fYpos)
	fYpos+=0.025
	DEBUG_TEXT_INT_IN_SCREEN("Screen: ", sTLGData.iCurrentScreen, 0.16, fYpos)
	fYpos+=0.025
	DEBUG_TEXT_INT_IN_SCREEN("Stop screen: ", sTLGData.iScreenToStopOn, 0.16, fYpos)
//	DEBUG_TEXT_FLOAT_IN_SCREEN("RetX", sTLGData.sPlayerData[0].vSpriteMarkerPos.x, 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_FLOAT_IN_SCREEN("RetY", sTLGData.sPlayerData[0].vSpriteMarkerPos.y, 0.16, fYpos)
//	DEBUG_TEXT_INT_IN_SCREEN("Lane 0: ", sTLGData.iCurrentActiveEnemies[0], 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_INT_IN_SCREEN("Lane 1: ", sTLGData.iCurrentActiveEnemies[1], 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_INT_IN_SCREEN("Lane 2: ", sTLGData.iCurrentActiveEnemies[2], 0.16, fYpos)
ENDPROC
#ENDIF

PROC TLG_DRAW_CLIENT_STATE_PLAYING()
	
	// Draw black and grey background
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTLGData.rgbaBlack)
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), sTLGData.rgbaRed)
		
	TLG_DRAW_BACKGROUND()
	
	TLG_DRAW_HOSTAGES(ciTLG_LANE_BACKGROUND)
	
	TLG_DRAW_ENEMIES(ciTLG_LANE_BACKGROUND)

	TLG_DRAW_ITEMS(ciTLG_LANE_BACKGROUND)
	
	TLG_DRAW_COVER(ciTLG_LANE_BACKGROUND)
	
//	FLOAT fScale = TLG_GET_COVER_TYPE_SCALE_FOR_LANE(TLG_ASSET_BUILDING_6, 0)
//	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, "LEVEL_02_TOWN_PROP_08",  GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfTLG_BUILDING_2_SPRITE_WIDTH*fScale, cfTLG_BUILDING_2_SPRITE_HEIGHT*fScale), 0, sTLGData.rgbaSprite)
//	
	TLG_DRAW_BULLETS_AND_EFFECTS(ciTLG_LANE_BACKGROUND)
	
	TLG_DRAW_HOSTAGES(ciTLG_LANE_MIDDLE)
	
	TLG_DRAW_ENEMIES(ciTLG_LANE_MIDDLE)
	
	TLG_DRAW_ITEMS(ciTLG_LANE_MIDDLE)
	
	TLG_DRAW_COVER(ciTLG_LANE_MIDDLE)
	
	TLG_DRAW_BULLETS_AND_EFFECTS(ciTLG_LANE_MIDDLE)
	
	TLG_DRAW_HOSTAGES(ciTLG_LANE_FOREGROUND)
	
	TLG_DRAW_ITEMS(ciTLG_LANE_FOREGROUND)
	
	TLG_DRAW_FOREGROUND_ENEMIES()
	
	TLG_DRAW_COVER(ciTLG_LANE_FOREGROUND)
	
	TLG_DRAW_THROWN_ENEMY_PROJECTILES()
	
	TLG_DRAW_BULLETS_AND_EFFECTS(ciTLG_LANE_FOREGROUND)
		
	TLG_DRAW_RETICULES()
		
	TLG_DRAW_SHOT_FLASH()
	
	TLG_DRAW_HUD()
	
	TWS_DRAW_FRONT_FX()
		
ENDPROC

PROC TLG_DRAW_MENU_BACKGROUND()

	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTLGData.rgbaBlack)
	
ENDPROC

PROC TLG_DRAW_LEADERBOARD_BACKGROUND()
	INT i
	INT iPrevious
	
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_TILES - 1
				
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31Level1TextDict, "LEVEL_01_DESERT_BACKGROUND_TILE", INIT_VECTOR_2D(sTLGData.sBackgroundTilesData[i].vSpritePos.x, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(276.0, cfGAME_SCREEN_HEIGHT), 0, sTLGData.rgbaSprite)
		
		IF sTLGData.sBackgroundTilesData[i].vSpritePos.x < cfTLG_BCAKGROUND_TILE_WIDTH/2
			iPrevious = i - 1
			IF iPrevious < 0
				iPrevious =  ciTLG_MAX_BACKGROUND_TILES - 1
			ENDIF
			//sTLGData.sBackgroundTilesData[i].vSpritePos.x = (cfTLG_BCAKGROUND_TILE_WIDTH * 7) - cfTLG_BCAKGROUND_TILE_WIDTH/2 - (4.0 * TLG_GET_PREVIOUS_TIMESTEP())
			sTLGData.sBackgroundTilesData[i].vSpritePos.x = sTLGData.sBackgroundTilesData[iPrevious].vSpritePos.x + cfTLG_BCAKGROUND_TILE_WIDTH
		ENDIF
		

		sTLGData.sBackgroundTilesData[i].vSpritePos.x -= TLG_GET_BACKGROUND_TILE_UPDATE()
		
		
	ENDFOR
	
	// Far canyons
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
	
		IF sTLGData.sBackgroundData[i].bRendering AND sTLGData.sBackgroundData[i].iLane = 0 
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31Level1TextDict, sTLGData.sBackgroundData[i].sSprite, sTLGData.sBackgroundData[i].vSpritePos, sTLGData.sBackgroundData[i].vSpriteSize, 0, sTLGData.rgbaSprite)
			
			IF TLG_IS_CLOUD(sTLGData.sBackgroundData[i].sSprite)
				IF sTLGData.sBackgroundData[i].vSpritePos.X + (cfTLG_LEVEL1_CLOUD_WIDTH / 2.0 ) <= 0
					sTLGData.sBackgroundData[i].vSpritePos.X = cfTLG_LEVEL1_CLOUD_WIDTH + cfTLG_LEVEL1_CLOUD_WIDTH + (cfTLG_LEVEL1_CLOUD_WIDTH / 2.0 )
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_LEADERBOARD_BACKGROUND] {DSW} Warping cloud ", i, " New Pos x: ", sTLGData.sBackgroundData[i].vSpritePos.X)
				ENDIF
				
				
				sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_CLOUD_UPDATE()
				
			ELSE
				IF sTLGData.sBackgroundData[i].vSpritePos.X < cfTLG_BCAKGROUND_TILE_WIDTH/2
					sTLGData.sBackgroundData[i].vSpritePos.X = cfBASE_SCREEN_WIDTH
				ENDIF
				sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_BACKGROUND_ELEMENT_UPDATE()
			ENDIF

		ENDIF
		
	ENDFOR
	
	// Close canyons
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
		
		IF sTLGData.sBackgroundData[i].bRendering AND sTLGData.sBackgroundData[i].iLane = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31Level1TextDict, sTLGData.sBackgroundData[i].sSprite, sTLGData.sBackgroundData[i].vSpritePos, sTLGData.sBackgroundData[i].vSpriteSize, 0, sTLGData.rgbaSprite)
			
			IF sTLGData.sBackgroundData[i].vSpritePos.X < cfTLG_BCAKGROUND_TILE_WIDTH/2
				sTLGData.sBackgroundData[i].vSpritePos.X = cfBASE_SCREEN_WIDTH
			ENDIF
			
			sTLGData.sBackgroundData[i].vSpritePos.X -= TLG_GET_DEFAULT_NEAR_BACKGROUND_ELEMENT_UPDATE()
			
			
		ENDIF
		
	ENDFOR
ENDPROC



PROC TLG_DRAW_MENU_LEVELS()

	
	FLOAT fFactor = 0.65
	TEXT_LABEL_23 tl23Sprite
	TEXT_LABEL_31 tl31TextDict
	
	INT iNumPlayers = TLG_GET_NUMBER_OF_PLAYERS()
	INT iLevel
	
	TLG_SCORE_INTEGER_TYPE eLeftHandCabScoreType = TLG_SCORE_INTEGER_TYPE_RED_SCORE
	
	
	//Placeholder
	tl23Sprite = "selectbounty"
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31LevelSelectTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/8), INIT_VECTOR_2D(328, 52), 0, sTLGData.rgbaSprite)
	
		
	//-- Level 1	
	tl31TextDict = sTLGData.tl31LevelSelectTextDict
	tl23Sprite = "LVL01"
	IF sTLGData.bLevelsCompleted[0]
		tl23Sprite += "_COMPLETED"
		tl31TextDict = sTLGData.tl31LevelCompTextDict
	ENDIF

	
	ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*3, cfBASE_SCREEN_HEIGHT/8*3), INIT_VECTOR_2D(cfTLG_SPRITE_POSTER_MENU_WIDTH*fFactor, cfTLG_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sTLGData.rgbaSprite)
							
	//-- Level 2
	tl31TextDict = sTLGData.tl31LevelSelectTextDict
	tl23Sprite = "LVL02"
	IF sTLGData.bLevelsCompleted[1]
		tl23Sprite += "_COMPLETED"
		tl31TextDict = sTLGData.tl31LevelCompTextDict
	ENDIF
	ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*5, cfBASE_SCREEN_HEIGHT/8*3), INIT_VECTOR_2D(cfTLG_SPRITE_POSTER_MENU_WIDTH*fFactor, cfTLG_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sTLGData.rgbaSprite)
	
							
	//-- Level 3
	tl31TextDict = sTLGData.tl31LevelSelectTextDict
	IF sTLGData.bLevelsCompleted[2]
		tl23Sprite = "LVL03_COMPLETED"
		tl31TextDict = sTLGData.tl31LevelCompTextDict
	ELIF IS_LEVEL_UNLOCKED(TLG_LEVEL_FOREST)
		tl23Sprite = "LVL03"
	ELSE
		tl23Sprite = "LVL03_LOCKED"
	ENDIF
	ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*7, cfBASE_SCREEN_HEIGHT/8*3), INIT_VECTOR_2D(cfTLG_SPRITE_POSTER_MENU_WIDTH*fFactor, cfTLG_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sTLGData.rgbaSprite)
	
							
	//-- Level 4
	tl31TextDict = sTLGData.tl31LevelSelectTextDict
	IF sTLGData.bLevelsCompleted[3]
		tl23Sprite = "LVL04_COMPLETED"
		tl31TextDict = sTLGData.tl31LevelCompTextDict
	ELIF IS_LEVEL_UNLOCKED(TLG_LEVEL_MINE)
		tl23Sprite = "LVL04"
	ELSE
		tl23Sprite = "LVL04_LOCKED"
	ENDIF
	ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*4, cfBASE_SCREEN_HEIGHT/8*6), INIT_VECTOR_2D(cfTLG_SPRITE_POSTER_MENU_WIDTH*fFactor, cfTLG_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sTLGData.rgbaSprite)
	
	//-- Level 5
	tl31TextDict = sTLGData.tl31LevelSelectTextDict
	IF sTLGData.bLevelsCompleted[4]
		tl23Sprite = "LVL05_COMPLETED"
		tl31TextDict = sTLGData.tl31LevelCompTextDict
	ELIF IS_LEVEL_UNLOCKED(TLG_LEVEL_GRAVEYARD)
		tl23Sprite = "LVL05"
	ELSE
		tl23Sprite = "LVL05_LOCKED"
	ENDIF
	ARCADE_DRAW_PIXELSPACE_SPRITE(tl31TextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*6, cfBASE_SCREEN_HEIGHT/8*6), INIT_VECTOR_2D(cfTLG_SPRITE_POSTER_MENU_WIDTH*fFactor, cfTLG_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sTLGData.rgbaSprite)
	
	IF iNumPlayers = 1
		//-- 1 PLAYER
		
		
		BOOL bInRedCabinet = TRUE
		TLG_SCORE_INTEGER_TYPE eCabScoreType = TLG_SCORE_INTEGER_TYPE_RED_SCORE
		
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()) = 1
			bInRedCabinet = FALSE
		ENDIF
		
		IF NOT bInRedcabinet
			eCabScoreType = TLG_SCORE_INTEGER_TYPE_BLUE_SCORE
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_DESERT)
		INT iMyScore = TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			TLG_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfTLG_SOLO_LEVEL1_SCORE_X_POS, cfTLG_SOLO_LEVEL1_SCORE_Y_POS), sTLGData.rgbaSprite,
									eCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_TOWN)
		iMyScore = TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			TLG_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfTLG_SOLO_LEVEL2_SCORE_X_POS, cfTLG_SOLO_LEVEL1_SCORE_Y_POS), sTLGData.rgbaSprite,
									eCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_FOREST)
		iMyScore = TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			TLG_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfTLG_SOLO_LEVEL3_SCORE_X_POS, cfTLG_SOLO_LEVEL1_SCORE_Y_POS), sTLGData.rgbaSprite,
									eCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_MINE)
		iMyScore = TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			TLG_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfTLG_SOLO_LEVEL4_SCORE_X_POS, cfTLG_SOLO_LEVEL4_SCORE_Y_POS), sTLGData.rgbaSprite,
									eCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_GRAVEYARD)
		iMyScore = TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			TLG_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfTLG_SOLO_LEVEL5_SCORE_X_POS, cfTLG_SOLO_LEVEL4_SCORE_Y_POS), sTLGData.rgbaSprite,
									eCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
									
		ENDIF
		
		INT iTotalScore = TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_ALL_LEVELS()
		IF iTotalScore > 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "NUMERICAL_DOLLAR", INIT_VECTOR_2D(cfTLG_LEVEL_RED_SCORE_DOLLAR_X_POS, cfTLG_LEVEL_RED_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
			TLG_DRAW_SCORE_INTEGER(TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_ALL_LEVELS(), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_LEVEL_RED_HI_SCORE_X_POS, cfTLG_LEVEL_RED_HI_SCORE_Y_POS), sTLGData.rgbaSprite,eCabScoreType)
		ENDIF
		
	ELIF iNumPlayers > 1
		//-- 2 PLAYERS
		iLevel = ENUM_TO_INT(TLG_LEVEL_DESERT)
		INT iRedScore = TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		INT iBlueScore = TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		eLeftHandCabScoreType = TLG_SCORE_INTEGER_TYPE_RED_SCORE
		
		//Red Hi-Score - Level 1
		IF iRedScore > 0
			TLG_DRAW_DOLLAR_SYMBOL("NUMERICAL_DOLLAR",
								INIT_VECTOR_2D(cfTLG_RED_LEVEL1_HI_DOLLAR_X_POS,cfTLG_RED_LEVEL1_HI_DOLLAR_Y_POS),
								INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* cfTLG_LEVEL_SCORE_SCALE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * cfTLG_LEVEL_SCORE_SCALE))

			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iRedScore,
									INIT_VECTOR_2D(cfTLG_RED_LEVEL1_HI_SCORE_X_POS,cfTLG_RED_LEVEL1_HI_SCORE_Y_POS),
									sTLGData.rgbaSprite,
									eLeftHandCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
		
		//Blue Hi-Score - Level 1
		IF iBlueScore > 0
			
			TLG_DRAW_SCORE_INTEGER_CENTER_RIGHT_MOST_WITH_DOLLAR(iBlueScore,
							INIT_VECTOR_2D(cfTLG_BLUE_LEVEL1_HI_SCORE_X_POS,cfTLG_BLUE_LEVEL1_HI_SCORE_Y_POS),
							sTLGData.rgbaSprite,
							TLG_SCORE_INTEGER_TYPE_BLUE_SCORE, "NUMERICAL_DOLLAR", cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
		
		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			// Red 'Win' - Level 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_RED", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL1_WIN_X_POS, cfTLG_RED_LEVEL1_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			
			// Blue 'Win' - Level 1

			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
										INIT_VECTOR_2D(cfTLG_BLUE_LEVEL1_WIN_X_POS, cfTLG_BLUE_LEVEL1_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ENDIF
		
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_TOWN)
		iRedScore = TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		// Red hi-score - Level 2
		IF iRedScore > 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"NUMERICAL_DOLLAR", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL2_HI_DOLLAR_X_POS,cfTLG_RED_LEVEL2_HI_DOLLAR_Y_POS), 
										INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* cfTLG_LEVEL_SCORE_SCALE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		
		
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iRedScore, 
									INIT_VECTOR_2D(cfTLG_RED_LEVEL2_HI_SCORE_X_POS,cfTLG_RED_LEVEL2_HI_SCORE_Y_POS),
									sTLGData.rgbaSprite,
									eLeftHandCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
			
		// Blue hi-score - Level 2
		IF iBlueScore > 0
			TLG_DRAW_SCORE_INTEGER_CENTER_RIGHT_MOST_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfTLG_BLUE_LEVEL2_HI_SCORE_X_POS,cfTLG_BLUE_LEVEL1_HI_SCORE_Y_POS),
								sTLGData.rgbaSprite,
								TLG_SCORE_INTEGER_TYPE_BLUE_SCORE, "NUMERICAL_DOLLAR", cfTLG_LEVEL_SCORE_SCALE)
		ENDIF	

		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			// Red 'Win' - Level 2
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_RED", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL2_WIN_X_POS, cfTLG_RED_LEVEL1_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			// Blue 'Win' - Level 2
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
										INIT_VECTOR_2D(cfTLG_BLUE_LEVEL2_WIN_X_POS, cfTLG_BLUE_LEVEL1_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_FOREST)
		iRedScore = TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		// Red Hi-score - level 3
		IF iRedScore > 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"NUMERICAL_DOLLAR", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL3_HI_DOLLAR_X_POS,cfTLG_RED_LEVEL3_HI_DOLLAR_Y_POS), 
										INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* cfTLG_LEVEL_SCORE_SCALE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
										
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iRedScore,
									INIT_VECTOR_2D(cfTLG_RED_LEVEL3_HI_SCORE_X_POS,cfTLG_RED_LEVEL3_HI_SCORE_Y_POS),
									sTLGData.rgbaSprite,
									eLeftHandCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
				
		
		
		// Blue Hi-score - level 3
		IF iBlueScore > 0
			TLG_DRAW_SCORE_INTEGER_CENTER_RIGHT_MOST_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfTLG_BLUE_LEVEL3_HI_SCORE_X_POS,cfTLG_BLUE_LEVEL1_HI_SCORE_Y_POS),
								sTLGData.rgbaSprite,
								TLG_SCORE_INTEGER_TYPE_BLUE_SCORE, "NUMERICAL_DOLLAR", cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
		
		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			// Red 'Win' - Level 3
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_RED", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL3_WIN_X_POS, cfTLG_RED_LEVEL1_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
										
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)							
			// Blue 'Win' - Level 3
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
										INIT_VECTOR_2D(cfTLG_BLUE_LEVEL3_WIN_X_POS, cfTLG_BLUE_LEVEL1_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_MINE)
		iRedScore = TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		// Red Hi-score - level 4
		IF iRedScore > 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"NUMERICAL_DOLLAR", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL4_HI_DOLLAR_X_POS,cfTLG_RED_LEVEL4_HI_DOLLAR_Y_POS), 
										INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* cfTLG_LEVEL_SCORE_SCALE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
										
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iRedScore,
									INIT_VECTOR_2D(cfTLG_RED_LEVEL4_HI_SCORE_X_POS,cfTLG_RED_LEVEL4_HI_SCORE_Y_POS),
									sTLGData.rgbaSprite,
									eLeftHandCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
							
		ENDIF
		
		// Blue Hi-score - level 4
		IF iBlueScore > 0
			TLG_DRAW_SCORE_INTEGER_CENTER_RIGHT_MOST_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfTLG_BLUE_LEVEL4_HI_SCORE_X_POS,cfTLG_BLUE_LEVEL4_HI_SCORE_Y_POS),
								sTLGData.rgbaSprite,
								TLG_SCORE_INTEGER_TYPE_BLUE_SCORE, "NUMERICAL_DOLLAR", cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
		
		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			// Red 'Win' - Level 4
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_RED", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL4_WIN_X_POS, cfTLG_RED_LEVEL4_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
			// Blue 'Win' - Level 4
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
										INIT_VECTOR_2D(cfTLG_BLUE_LEVEL4_WIN_X_POS, cfTLG_RED_LEVEL4_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ENDIF
		
		iLevel = ENUM_TO_INT(TLG_LEVEL_GRAVEYARD)
		iRedScore = TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		//-- Red Hi-score - level 5
		IF iRedScore > 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"NUMERICAL_DOLLAR", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL5_HI_DOLLAR_X_POS,cfTLG_RED_LEVEL5_HI_DOLLAR_Y_POS), 
										INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* cfTLG_LEVEL_SCORE_SCALE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
										
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iRedScore,
									INIT_VECTOR_2D(cfTLG_RED_LEVEL5_HI_SCORE_X_POS,cfTLG_RED_LEVEL5_HI_SCORE_Y_POS),
									sTLGData.rgbaSprite,
									eLeftHandCabScoreType, cfTLG_LEVEL_SCORE_SCALE)
		ENDIF	
	
		//-- Blue Hi-score - level 5
		IF iBlueScore > 0
			TLG_DRAW_SCORE_INTEGER_CENTER_RIGHT_MOST_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfTLG_BLUE_LEVEL5_HI_SCORE_X_POS,cfTLG_BLUE_LEVEL4_HI_SCORE_Y_POS),
								sTLGData.rgbaSprite,
								TLG_SCORE_INTEGER_TYPE_BLUE_SCORE, "NUMERICAL_DOLLAR", cfTLG_LEVEL_SCORE_SCALE)
		ENDIF
							
		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
			// Red 'Win' - Level 5
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_RED", 
										INIT_VECTOR_2D(cfTLG_RED_LEVEL5_WIN_X_POS, cfTLG_RED_LEVEL4_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)	
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)	
			// Blue 'Win' - Level 5
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
										INIT_VECTOR_2D(cfTLG_BLUE_LEVEL5_WIN_X_POS, cfTLG_RED_LEVEL4_WIN_Y_POS), 
										INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE * cfTLG_LEVEL_SCORE_SCALE, cfTLG_SCORE_WIN_Y_SIZE* cfTLG_LEVEL_SCORE_SCALE), 
										0, 
										sTLGData.rgbaSprite)
		ENDIF
		
		// 'Red' (1P) Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "NUMERICAL_DOLLAR", INIT_VECTOR_2D(cfTLG_LEVEL_RED_SCORE_DOLLAR_X_POS, cfTLG_LEVEL_RED_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		TLG_DRAW_SCORE_INTEGER(TLG_GET_RED_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST(), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_LEVEL_RED_HI_SCORE_X_POS, cfTLG_LEVEL_RED_HI_SCORE_Y_POS), sTLGData.rgbaSprite,eLeftHandCabScoreType)
	
		// 'Blue' (2P) Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "NUMERICAL_DOLLAR", INIT_VECTOR_2D(cfTLG_LEVEL_BLUE_SCORE_DOLLAR_X_POS, cfTLG_LEVEL_BLUE_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
		TLG_DRAW_SCORE_INTEGER(TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST(), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_LEVEL_BLUE_HI_SCORE_X_POS, cfTLG_LEVEL_BLUE_HI_SCORE_Y_POS), sTLGData.rgbaSprite, TLG_SCORE_INTEGER_TYPE_BLUE_SCORE)
		
		IF TLG_GET_RED_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST() > TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST()
		
			// Red 'Win'
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_WIN_GRAPHIC_RED", INIT_VECTOR_2D(cfTLG_LEVEL_SCORE_WIN_RED_X_POS, cfTLG_LEVEL_SCORE_WIN_RED_Y_POS), INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE, cfTLG_SCORE_WIN_Y_SIZE), 0, sTLGData.rgbaSprite)
		ELIF TLG_GET_RED_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST() < TLG_GET_BLUE_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST()
	
			// Blue 'Win'
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_WIN_GRAPHIC_BLUE", INIT_VECTOR_2D(cfTLG_LEVEL_SCORE_WIN_BLUE_X_POS, cfTLG_LEVEL_SCORE_WIN_BLUE_Y_POS), INIT_VECTOR_2D(cfTLG_SCORE_WIN_X_SIZE, cfTLG_SCORE_WIN_Y_SIZE), 0, sTLGData.rgbaSprite)
		ENDIF
	ENDIF
ENDPROC

PROC TLG_DRAW_CLIENT_STATE_MENU()
	
	TLG_DRAW_MENU_BACKGROUND()
	TLG_DRAW_MENU_LEVELS()
	TLG_DRAW_BULLETS_AND_EFFECTS(1)
	TLG_DRAW_RETICULES()
	TWS_DRAW_FRONT_FX()
	
ENDPROC

PROC TLG_DRAW_TITLE_SCREEN_ELEMENTS()
	BOOL bHighlight
	FLOAT x1, x2, y1, y2
	
	x1 = sTLGData.sMenuOptionsData[0].vSpritePos.x - (sTLGData.sMenuOptionsData[0].vSize.x/2)
	y1 = sTLGData.sMenuOptionsData[0].vSpritePos.y - (sTLGData.sMenuOptionsData[0].vSize.y/2)
	x2 = sTLGData.sMenuOptionsData[0].vSpritePos.x + (sTLGData.sMenuOptionsData[0].vSize.x/2)
	y2 = sTLGData.sMenuOptionsData[0].vSpritePos.y + (sTLGData.sMenuOptionsData[0].vSize.y/2)
	
	TEXT_LABEL_31 tl31Sprite
	tl31Sprite = "COWBOY_CHARACTER"
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/3 + 60), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_COWBOY_TITLE_WIDTH, cfTLG_SPRITE_HUD_COWBOY_TITLE_HEIGHT), 0, sTLGData.rgbaSprite)
	
	tl31Sprite = "TITLE_SCREEN_LOGO"
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2+80), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_LOGO_WIDTH, cfTLG_SPRITE_HUD_LOGO_HEIGHT), 0, sTLGData.rgbaSprite)
	
	
	x1 = sTLGData.sMenuOptionsData[TLG_START].vSpritePos.x - (sTLGData.sMenuOptionsData[TLG_START].vSize.x/2)
	y1 = sTLGData.sMenuOptionsData[TLG_START].vSpritePos.y - (sTLGData.sMenuOptionsData[TLG_START].vSize.y/2)
	x2 = sTLGData.sMenuOptionsData[TLG_START].vSpritePos.x + (sTLGData.sMenuOptionsData[TLG_START].vSize.x/2)
	y2 = sTLGData.sMenuOptionsData[TLG_START].vSpritePos.y + (sTLGData.sMenuOptionsData[TLG_START].vSize.y/2)
	
	IF (sTLGData.sPlayerData[0].vSpriteMarkerPos.X > x1 AND sTLGData.sPlayerData[0].vSpriteMarkerPos.x < x2)
	AND(sTLGData.sPlayerData[0].vSpriteMarkerPos.y > y1 AND sTLGData.sPlayerData[0].vSpriteMarkerPos.Y < y2)
		bHighlight = TRUE
	ENDIF
	
	IF NOT bHighlight
		tl31Sprite = "start"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, tl31Sprite, sTLGData.sMenuOptionsData[TLG_START].vSpritePos, sTLGData.sMenuOptionsData[TLG_START].vSize, 0, sTLGData.rgbaSprite)
	ELSE
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, "TYPE_MAIN_MENU_START_HIGHLIGHT", sTLGData.sMenuOptionsData[TLG_START].vSpritePos, sTLGData.sMenuOptionsData[TLG_START].vSize, 0, sTLGData.rgbaSprite)
	ENDIF
	
	bHighlight = FALSE
	IF NOT TLG_SHOULD_SKIP_LEADERBOARD()
		x1 = sTLGData.sMenuOptionsData[TLG_SCORES].vSpritePos.x - (sTLGData.sMenuOptionsData[TLG_SCORES].vSize.x/2)
		y1 = sTLGData.sMenuOptionsData[TLG_SCORES].vSpritePos.y - (sTLGData.sMenuOptionsData[TLG_SCORES].vSize.y/2)
		x2 = sTLGData.sMenuOptionsData[TLG_SCORES].vSpritePos.x + (sTLGData.sMenuOptionsData[TLG_SCORES].vSize.x/2)
		y2 = sTLGData.sMenuOptionsData[TLG_SCORES].vSpritePos.y + (sTLGData.sMenuOptionsData[TLG_SCORES].vSize.y/2)
		
		IF (sTLGData.sPlayerData[0].vSpriteMarkerPos.X > x1 AND sTLGData.sPlayerData[0].vSpriteMarkerPos.x < x2)
		AND(sTLGData.sPlayerData[0].vSpriteMarkerPos.y > y1 AND sTLGData.sPlayerData[0].vSpriteMarkerPos.Y < y2)
			bHighlight = TRUE
		ENDIF
	
		IF NOT bHighlight
			tl31Sprite = "leaderboards"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, tl31Sprite, sTLGData.sMenuOptionsData[TLG_SCORES].vSpritePos, sTLGData.sMenuOptionsData[TLG_SCORES].vSize, 0, sTLGData.rgbaSprite)
		ELSE
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, "TYPE_MAIN_MENU_LEADERBOARDS_HIGHLIGHT", sTLGData.sMenuOptionsData[TLG_SCORES].vSpritePos, sTLGData.sMenuOptionsData[TLG_SCORES].vSize, 0, sTLGData.rgbaSprite)
		ENDIF
	ENDIF

	tl31Sprite = "PIXTRO_LOGO"
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/6*5 + 60), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_PIXTRO_LOGO_WIDTH, cfTLG_SPRITE_HUD_PIXTRO_LOGO_HEIGHT), 0, sTLGData.rgbaSprite)
		
ENDPROC

PROC TLG_DRAW_CLIENT_STATE_TITLE_SCREEN()
	
	TLG_DRAW_MENU_BACKGROUND()
	TLG_DRAW_TITLE_SCREEN_ELEMENTS()
	TLG_DRAW_BULLETS_AND_EFFECTS(1)
	TLG_DRAW_RETICULES()
	TWS_DRAW_FRONT_FX()
ENDPROC

FUNC TEXT_LABEL_15 TLG_GET_END_SCREEN_SUBTITLE(BOOL bWin)
	TEXT_LABEL_15 tl15Subtitle = ""
	INT iLevel = ENUM_TO_INT(sTLGData.eCurrentLevel)+1
	IF bWin
		tl15Subtitle =  "BADII_LVW" 
	ELSE
		tl15Subtitle =  "BADII_LVD" 
	ENDIF
	
	tl15Subtitle += iLevel
	
	RETURN tl15Subtitle
ENDFUNC

PROC TLG_DRAW_RESULT_SCREEN_ELEMENTS(BOOL bDoOutro)
	
	TEXT_LABEL_23 tl23Sprite
	INT iScore
	INT iAccuracy
	INT iAccuracyBonus
	INT iHostagesSaved
	INT iHostagesSavedBonus
	INT iLevel = TLG_GET_CURRENT_LEVEL_AS_INT()
	BOOL bInRedCabinet = TRUE
	
	BOOL bShouldShowScore = TRUE
	BOOL bShouldShowAccuracy = FALSE
	BOOL bShouldShowHostages = FALSE
	
	BOOL bPlayerDead
	
	TEXT_LABEL_15 tl15Sub = "" 
	
	IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.0
		bPlayerDead = TRUE
	ENDIF
	
	TLG_SCORE_INTEGER_TYPE eCabScoreType = TLG_SCORE_INTEGER_TYPE_RED_SCORE
	
	IF GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()) = 1
		bInRedCabinet = FALSE
	ENDIF
	
	IF NOT bInRedcabinet
		eCabScoreType = TLG_SCORE_INTEGER_TYPE_BLUE_SCORE
	ENDIF
	
	IF sTLGData.iResultsScreenTimer = 0
		sTLGData.iResultsScreenTimer = GET_GAME_TIMER()
	ENDIF
	
	
	IF NOT bPlayerDead
		IF GET_GAME_TIMER() - sTLGData.iResultsScreenTimer > 1500
			bShouldShowAccuracy = TRUE
			
	//     NEEDS TO TAKE INTO ACCOUNT ANIMALS SHOT
			iAccuracy = ROUND(100.0 * (TO_FLOAT(TLG_GET_TOTAL_ENEMY_HITS_FOR_CURRENT_LEVEL()) / TO_FLOAT(TLG_GET_TOTAL_SHOTS_FIRED_IN_CURRENT_LEVEL())))
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_ADDED_ON_ACCURACY_BONUS)
				//-- Accuracy bonus is accuracy * level score
				iAccuracyBonus = ROUND(TO_FLOAT(playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]) * (TO_FLOAT(iAccuracy) / 100.0))
				playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel] += iAccuracyBonus
				
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} Accuracy Bonus: ", iAccuracyBonus)
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel])
				
				
				IF playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] < playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
		 			playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New Hi-score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel])
				ENDIF
				
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_ADDED_ON_ACCURACY_BONUS)
			ENDIF
		ENDIF
		
		IF GET_GAME_TIMER() - sTLGData.iResultsScreenTimer > 3000
			bShouldShowHostages = TRUE
			iHostagesSaved = TLG_GET_TOTAL_NUMBER_OF_HOSTAGES_SAVED_FOR_CURRENT_LEVEL()
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_ADDED_ON_HOSTAGE_BONUS)
				iHostagesSavedBonus = iHostagesSaved * 1000
				playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel] += iHostagesSavedBonus
				
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} Hostage Bonus: ", iHostagesSavedBonus)
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel])
				
				IF playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] < playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
		 			playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New Hi-score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel])
				ENDIF
				
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_ADDED_ON_HOSTAGE_BONUS)
			ENDIF
		ENDIF
	ENDIF
	

	//bDoOutro = TRUE
	IF bDoOutro
		//-- Poster
		tl23Sprite = "FINAL_SCENE"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31ResultsTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_SPRITE_FINAL_SCENE_POSTER_X, cfTLG_SPRITE_FINAL_SCENE_POSTER_Y), INIT_VECTOR_2D(cfTLG_SPRITE_FINAL_SCENE_POSTER_WIDTH, cfTLG_SPRITE_FINAL_SCENE_POSTER_HEIGHT), 0, sTLGData.rgbaSprite)
		
		//--Text
		tl23Sprite = "FINAL_SCENE_TEXT"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31ResultsTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_SPRITE_FINAL_SCENE_TEXT_X, cfTLG_SPRITE_FINAL_SCENE_TEXT_Y), INIT_VECTOR_2D(cfTLG_SPRITE_FINAL_SCENE_TEXT_WIDTH, cfTLG_SPRITE_FINAL_SCENE_TEXT_HEIGHT), 0, sTLGData.rgbaSprite)
		
		//-- Subtitles
		tl15Sub = "BADII_FNL"
		
		IF bShouldShowScore
		
			//-- Score
			tl23Sprite = "SCORE_SCREEN_TEXT_01"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_Y), 0, sTLGData.rgbaSprite)
			
			TLG_DRAW_DOLLAR_SYMBOL("NUMERICAL_DOLLAR", 
									INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_SCORE_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_SCORE_DOLLAR_POS_Y),
									INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* 1.0, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * 1.0))
						
						
			iScore = TLG_GET_LOCAL_PLAYER_TOTAL_SCORE()
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iScore,
													INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_SCORE_VAL_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_SCORE_VAL_POS_Y),
													sTLGData.rgbaSprite,
												eCabScoreType, 1)
												
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
			ENDIF
		ENDIF
		

		
		IF bShouldShowAccuracy
			//-- Accuracy
			tl23Sprite = "SCORE_SCREEN_TEXT_02"
	//		INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_Y)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_GAME_SCREEN_COMPLETE_ACCURACY_POS_X, cfTLG_RESULTS_GAME_SCREEN_COMPLETE_ACCURACY_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_Y), 0, sTLGData.rgbaSprite)
			

			
		//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] iAccuracy: ", iAccuracy)
		// INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_VAL_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_VAL_POS_Y)
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iAccuracy,
													INIT_VECTOR_2D(cfTLG_RESULTS_GAME_SCREEN_COMPLETE_ACCURACY_VAL_POS_X, cfTLG_RESULTS_GAME_SCREEN_COMPLETE_ACCURACY_VAL_POS_Y),
													sTLGData.rgbaSprite,
													eCabScoreType, 1, TRUE)
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
			ENDIF
		ENDIF
//		
//		
		IF bShouldShowHostages
			//-- Hostages
			//INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_POS_X,cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_POS_Y)
			tl23Sprite = "SCORE_SCREEN_TEXT_03"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SIZE_Y), 0, sTLGData.rgbaSprite)
			
			//-- Hostages - number saved
			// INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SAVED_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SAVED_POS_Y)
			TLG_DRAW_SCORE_INTEGER(iHostagesSaved, 0,INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_SAVED_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_SAVED_POS_Y), sTLGData.rgbaSprite, eCabScoreType, 1)
			
			
			tl23Sprite = TLG_GET_CROSS_CHAR_TEXTURE_FOR_SCORE_TYPE(eCabScoreType)
			//-- Hostages - 'x' multiplier symbol
			// INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_MULTI_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_MULTI_POS_Y)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
											tl23Sprite,
											INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_MULTI_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_MULTI_POS_Y),
											INIT_VECTOR_2D(32.0 * 1.0,36.0* 1.0),
											0, 
											sTLGData.rgbaSprite)
			
			//-- Hostages - Dollar symbol
			// INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_DOLLAR_POS_Y)
			TLG_DRAW_DOLLAR_SYMBOL("NUMERICAL_DOLLAR", 
								INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_DOLLAR_POS_Y),
								INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* 1.0, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * 1.0))
			
			//-- Hostages - bonus numerical award
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(1000,
													INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_BONUS_POS_X, cfTLG_RESULTS_SCREEN_GAME_COMPLETE_HOSTAGES_BONUS_POS_Y),
													sTLGData.rgbaSprite,
													eCabScoreType, 1)
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_HOSTAGE_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)										
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_HOSTAGE_SCORE_AUDIO)
			ENDIF
		ENDIF
		
	ELIF NOT bPlayerDead
	
		tl15Sub = TLG_GET_END_SCREEN_SUBTITLE(TRUE)
	
		tl23Sprite = "LVL0"
		tl23Sprite += ENUM_TO_INT(sTLGData.eCurrentLevel)+1
		tl23Sprite += "_COMPLETED"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31LevelCompTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/6*2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTLG_SPRITE_POSTER_MENU_WIDTH, cfTLG_SPRITE_POSTER_MENU_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "TYPE_LEVEL_0"
		tl23Sprite += ENUM_TO_INT(sTLGData.eCurrentLevel)+1
		tl23Sprite += "_OUTRO_WIN"
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31ResultsTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/8*5, cfBASE_SCREEN_HEIGHT/3), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RESULT_TEXT_WIDTH, cfTLG_SPRITE_HUD_RESULT_TEXT_HEIGHT), 0, sTLGData.rgbaSprite)

		
		
		
		
		IF bShouldShowScore
		
			//-- Score
			tl23Sprite = "SCORE_SCREEN_TEXT_01"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_Y), 0, sTLGData.rgbaSprite)
			
			TLG_DRAW_DOLLAR_SYMBOL("NUMERICAL_DOLLAR", 
									INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_Y),
									INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* 1.0, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * 1.0))
						
						
			iScore = TLG_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT())
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iScore,
													INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_Y),
													sTLGData.rgbaSprite,
												eCabScoreType, 1)
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
			ENDIF
		ENDIF
		
		
		IF bShouldShowAccuracy
			//-- Accuracy
			tl23Sprite = "SCORE_SCREEN_TEXT_02"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_Y), 0, sTLGData.rgbaSprite)
			

			
		//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] iAccuracy: ", iAccuracy)
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iAccuracy,
													INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_VAL_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_VAL_POS_Y),
													sTLGData.rgbaSprite,
													eCabScoreType, 1, TRUE)
													
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)	
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
			ENDIF
		ENDIF
		
		
		IF bShouldShowHostages
			//-- Hostages
			tl23Sprite = "SCORE_SCREEN_TEXT_03"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_POS_X,cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SIZE_Y), 0, sTLGData.rgbaSprite)
			
			//-- Hostages - number saved
			
			TLG_DRAW_SCORE_INTEGER(iHostagesSaved, 0, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SAVED_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_SAVED_POS_Y), sTLGData.rgbaSprite, eCabScoreType, 1)
			
			
			tl23Sprite = TLG_GET_CROSS_CHAR_TEXTURE_FOR_SCORE_TYPE(eCabScoreType)
			//-- Hostages - 'x' multiplier symbol
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, 
											tl23Sprite,
											INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_MULTI_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_MULTI_POS_Y),
											INIT_VECTOR_2D(32.0 * 1.0,36.0* 1.0),
											0, 
											sTLGData.rgbaSprite)
			
			//-- Hostages - Dollar symbol
			TLG_DRAW_DOLLAR_SYMBOL("NUMERICAL_DOLLAR", 
								INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_DOLLAR_POS_Y),
								INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* 1.0, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * 1.0))
			
			//-- Hostages - bonus numerical award
			TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(1000,
													INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_BONUS_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_HOSTAGES_BONUS_POS_Y),
													sTLGData.rgbaSprite,
													eCabScoreType, 1)
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_HOSTAGE_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)	
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_HOSTAGE_SCORE_AUDIO)
			ENDIF
		ENDIF
		
	// If player is dead
	ELSE
		tl15Sub = TLG_GET_END_SCREEN_SUBTITLE(FALSE)
		
		tl23Sprite = "LVL0"
		tl23Sprite += ENUM_TO_INT(sTLGData.eCurrentLevel)+1
		tl23Sprite += "_OUTRO_FAIL"
		//
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31ResultsTextDict, tl23Sprite, INIT_VECTOR_2D(684.500, cfBASE_SCREEN_HEIGHT/3), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_RESULT_TEXT_WIDTH, cfTLG_SPRITE_HUD_RESULT_TEXT_HEIGHT), 0, sTLGData.rgbaSprite)
		
		tl23Sprite = "DEAD_SCREEN_DESATURATED"
		ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31DeadTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/6*4, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_COWBOY_DEAD_WIDTH, cfTLG_SPRITE_HUD_COWBOY_DEAD_HEIGHT), 0, sTLGData.rgbaSprite)
		
		IF sTLGData.iFade > 0
			RGBA_COLOUR_STRUCT rgbaDeadCowboy
			INIT_RGBA_STRUCT(rgbaDeadCowboy, 255, 255, 255, sTLGData.iFade)
			tl23Sprite = "DEAD_SCREEN_COLOR"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31DeadTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/6*4, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_COWBOY_DEAD_WIDTH, cfTLG_SPRITE_HUD_COWBOY_DEAD_HEIGHT), 0, rgbaDeadCowboy)
			sTLGData.iFade -= 2
		ELSE
			tl23Sprite = "DEAD_SCREEN_TEXT"
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31DeadTextDict, tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/6*4, cfBASE_SCREEN_HEIGHT/3), INIT_VECTOR_2D(cfTLG_SPRITE_HUD_DEAD_SCREEN_TEXT_WIDTH, cfTLG_SPRITE_HUD_DEAD_SCREEN_TEXT_HEIGHT), 0, sTLGData.rgbaSprite)
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_DEAD_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_RESULTS_DEAD)	
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_DEAD_AUDIO)
			ENDIF
		ENDIF
		

		IF bShouldShowScore
		
			//-- Score
			tl23Sprite = "SCORE_SCREEN_TEXT_01"
			// INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_POS_Y)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, tl23Sprite, INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_DEAD_SCORE_POS_X, cfTLG_RESULTS_SCREEN_DEAD_SCORE_POS_Y), INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_Y), 0, sTLGData.rgbaSprite)
			
			//INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_Y)
			TLG_DRAW_DOLLAR_SYMBOL("NUMERICAL_DOLLAR", 
									INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_DEAD_SCORE_DOLLAR_POS_X, cfTLG_RESULTS_SCREEN_DEAD_SCORE_DOLLAR_POS_Y),
									INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE* 1.0, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE * 1.0))
						
						
			iScore = TLG_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT())
			
			IF iScore > 0
				TLG_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iScore,
														INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_DEAD_SCORE_VAL_POS_X, cfTLG_RESULTS_SCREEN_DEAD_SCORE_VAL_POS_Y),
														sTLGData.rgbaSprite,
														eCabScoreType, 1)
			ELSE
				TLG_DRAW_SCORE_ZERO(INIT_VECTOR_2D(cfTLG_RESULTS_SCREEN_DEAD_SCORE_VAL_POS_X, cfTLG_RESULTS_SCREEN_DEAD_SCORE_VAL_POS_Y), sTLGData.rgbaSprite, eCabScoreType)
			ENDIF
			
			IF playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] < playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
	 			playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New Hi-score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel])
			ENDIF
			
			IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
			ENDIF
		ENDIF
		
		IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_DEATH_SCREEN_AUDIO)
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_DEATH)
			TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_PLAYED_DEATH_SCREEN_AUDIO)
		ENDIF
	ENDIF
	
	
	//-- Hi Score
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "SCORE_TEXT_DOLLAR_Y", INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_POS, cfTLG_MY_HISCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfTLG_MY_HISCORE_DOLLAR_X_SIZE, cfTLG_MY_HISCORE_DOLLAR_Y_SIZE), 0, sTLGData.rgbaSprite)
	TLG_DRAW_SCORE_INTEGER(TLG_GET_LOCAL_PLAYER_HI_SCORE(), ciTLG_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfTLG_MY_HISCORE_X_POS, cfTLG_MY_HISCORE_Y_POS), sTLGData.rgbaSprite, TLG_SCORE_INTEGER_TYPE_HI_SCORE ) //
	
	IF GET_CURRENT_LANGUAGE() != LANGUAGE_ENGLISH
	
		IF NOT IS_STRING_NULL_OR_EMPTY(tl15Sub)
			BEGIN_TEXT_COMMAND_PRINT(tl15Sub)
			END_TEXT_COMMAND_PRINT(1, TRUE)
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_RESULT_SCREEN_ELEMENTS] Subtitle string is empty!")
		ENDIF
	ENDIF
ENDPROC

PROC TLG_DRAW_CLIENT_STATE_RESULT_SCREEN(BOOL bDoOutro)
	
	TLG_DRAW_MENU_BACKGROUND()
	TLG_DRAW_RESULT_SCREEN_ELEMENTS(bDoOutro)
	TWS_DRAW_FRONT_FX()
	
	
ENDPROC

PROC TLG_DRAW_CLIENT_STATE_LEADERBOARD()
//	TLG_DRAW_MENU_BACKGROUND()
	
	TLG_DRAW_LEADERBOARD_BACKGROUND()
	TLG_DRAW_COVER(0, TRUE)
	TLG_DRAW_COVER(1, TRUE)
	TLG_DRAW_COVER(2, TRUE)
	
	BOOL bIsPlayerTwo
	IF NOT TLG_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
		bIsPlayerTwo = TRUE
	ENDIF
	
	ARCADE_GAMES_LEADERBOARD_DRAW(sTLGData.sLbData, playerBD[PARTICIPANT_ID_TO_INT()].iLeaderboardInitials, tlgServerBd.sLeaderboard, bIsPlayerTwo)
	
	TWS_DRAW_FRONT_FX()
ENDPROC
