USING "globals.sch"
USING "rage_builtins.sch"

// Header support for HUD_FEED_SOCIALCLUB.gfx

/// PURPOSE: Displays the social club feed image and localised string in the top left of the HUD
/// Make sure you have loaded the movie with REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE, 
/// and checked it has loaded with HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED before using this.
/// PARAMS:
///    sHUDFeedSocialClub - Scaleform movie ID for the Social Club Feed UI
///    sTitleText - Title string of the Social Club Message
///    sBodyText - Body string of the Social Club Message
///    sTXD - The Texture Dictionary for the image used in the Social Club Message
///    sImageName - The Image name string for the image used in the Social Club Message
PROC Set_Feed_SocialClub(eSCRIPT_HUD_COMPONENT sHUDFeedSocialClub, STRING sTitleText, STRING sBodyText, STRING sTXD, STRING sImageName)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedSocialClub, "SET_FEED_SOCIALCLUB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitleText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBodyText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sImageName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Remove social club component from screen. Be sure to remove the scaleform movie when it is no longer required
///    
/// PARAMS:
///    sHUDFeedSocialClub - Scaleform movie ID for the Social Club Feed UI
PROC Remove_Feed_SocialClub(eSCRIPT_HUD_COMPONENT sHUDFeedSocialClub)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedSocialClub, "REMOVE_FEED_SOCIALCLUB")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC
