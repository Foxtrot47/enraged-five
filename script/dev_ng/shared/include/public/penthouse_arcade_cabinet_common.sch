USING "arcade_cabinet_minigame_common.sch"

STRUCT ARCADE_PLAYSTATS_STRUCT
	TEXT_LABEL_63 gameType		// - gameType (hash - invade and persuade 2, street crimes - gang wars edition)
	INT numPlayers = 0			// - numPlayers (int - number of players)
	INT powerUps = 0			// - powerUps (int - number of power ups collected)
	INT kills = 0				// - kills (int - number of entities destroyed/killed)
	INT timePlayed = 0			// - timePlayed (int - time played)
	INT score = 0				// - score (int - score at the end of the game)
	int location = 0			// - location (int - property the game is placed in)
ENDSTRUCT

ARCADE_PLAYSTATS_STRUCT sArcadePlayStats

PROC ARCADE_INITIALISE_PLAY_STATS(BOOL bStreetCrimes)

	IF bStreetCrimes
		sArcadePlayStats.gameType = "Street Crime: Gang Wars Edition."
	ELSE
		sArcadePlayStats.gameType = "Invade and Persuade II."
	ENDIF
	sArcadePlayStats.numPlayers = 0
	sArcadePlayStats.powerUps = 0
	sArcadePlayStats.kills = 0
	sArcadePlayStats.timePlayed = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
	sArcadePlayStats.score = 0
	sArcadePlayStats.location = 0
ENDPROC

PROC ARCADE_SEND_PLAY_STATS()
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sArcadePlayStats.gameType)
		PRINTLN("[ARCADE] [JS] - ARCADE_SEND_PLAY_STATS - Not sending, no gametype set")
		EXIT
	ENDIF
	
	IF sArcadePlayStats.timePlayed = 0
		PRINTLN("[ARCADE] [JS] - ARCADE_SEND_PLAY_STATS - Not sending, no time played")
		EXIT
	ENDIF
	
	sArcadePlayStats.timePlayed = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) - sArcadePlayStats.timePlayed
	
	PRINTLN("[ARCADE] [JS] - ARCADE_SEND_PLAY_STATS - PLAYSTATS_ARCADE_GAME gameType:\"", sArcadePlayStats.gameType,
						"\", numPlayers:", sArcadePlayStats.numPlayers,
						", powerUps:", sArcadePlayStats.powerUps,
						", kills:", sArcadePlayStats.kills,
						", timePlayed:", sArcadePlayStats.timePlayed,
						", score:", sArcadePlayStats.score)
	PLAYSTATS_ARCADE_GAME(GET_HASH_KEY(sArcadePlayStats.gameType), sArcadePlayStats.numPlayers, sArcadePlayStats.powerUps, sArcadePlayStats.kills, sArcadePlayStats.timePlayed, sArcadePlayStats.score, sArcadePlayStats.location)
						
ENDPROC			
