//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_cam_public.sch																		//
// Description: Publiic/global enums, structs, consts, and look-up tables for the turret_cam system.		//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		12/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "types.sch"
USING "weapon_enums.sch"
USING "MP_globals_script_timers.sch"
USING "commands_pad.sch"
USING "commands_shapetest.sch"

/// PURPOSE: Bits used in g_iBsTurretCamConfigSettings
CONST_INT ci_TURRET_CAM_CONFIG_BS_LIMITS 		0
CONST_INT ci_TURRET_CAM_CONFIG_BS_SPEEDS 		1
CONST_INT ci_TURRET_CAM_CONFIG_BS_WMODE	 		2
CONST_INT ci_TURRET_CAM_CONFIG_BS_LOAD			3
CONST_INT ci_TURRET_CAM_CONFIG_BS_HUD			4
CONST_INT ci_TURRET_CAM_CONFIG_BS_HUD_SOUNDS 	5
CONST_INT ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS	6
CONST_INT ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL	7
CONST_INT ci_TURRET_CAM_CONFIG_BS_HELP			8	
CONST_INT ci_TURRET_CAM_CONFIG_BS_JOINT_Z		9
CONST_INT ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS	10
CONST_INT ci_TURRET_CAM_CONFIG_BS_PC_INPUT_MAP  11
CONST_INT ci_TURRET_CAM_CONFIG_BS_ARENA_HUD 	12
// NOTE: When adding new config params please udpate TURRET_CAM_PRINT_CONFIG in turret_cam_sup.sch
//		 and ci_TURRET_CAM_CONFIG_BS_LAST_VALUE (below).
CONST_INT ci_TURRET_CAM_CONFIG_BS_LAST_VALUE	12

/// PURPOSE: Bits used in g_iBsTurretCam
CONST_INT ci_TURRET_CAM_BS_LAUNCH_DRONE_MISSILE	0 // Tell the drone script to fire a missile.
CONST_INT ci_TURRET_CAM_BS_CONFIG_UPDATE		1 // The config has been force updated.
CONST_INT ci_TURRET_CAM_BS_STARTED_SCRIPT		2 // The turret_cam_script has been started and is running.
CONST_INT ci_TURRET_CAM_BS_LOADED_INTO_CAM		3 // The turret_cam_script has completed inital loading stage.
CONST_INT ci_TURRET_CAM_BS_SIM_INPUT_FIRE		4 // Simulate "Fire" input (needs to be set every frame)
CONST_INT ci_TURRET_CAM_BS_FORCE_INSTR_BTN_RELOAD	5 // Reloads the instructional buttons menu

ENUM TURRET_CAM_ATTACH_TYPE
	TCAT_NONE,
	TCAT_OBJ
ENDENUM

STRUCT TURRET_CAM_ARGS
	VECTOR vPos
	VECTOR vRot
	TURRET_CAM_ATTACH_TYPE eAttachType
	ENTITY_INDEX entParent
ENDSTRUCT

/// PURPOSE: Special purpose enum to represent a turret config
ENUM TURRET_CAM_CONFIG_ID
	TCCI_NONE = 0
ENDENUM

ENUM WEAPON_MODE
	WM_NO_WEAPON,
	WM_FULL_AUTO, 		// Fully automatic weapon (like a machine gun).
	WM_HOMING_MISSILE,  // Applies targetting and homing logic (PLAYERS ONLY for now - could add config?@@).
	WM_PILOTED_MISSILE	// Player-piloted drone missile.
ENDENUM	

STRUCT CAM_CONTROL_LIMITS
	FLOAT fMinHeading
	FLOAT fMaxHeading
	
	FLOAT fMinPitch
	FLOAT fMaxPitch
	
	FLOAT fMinFov
	FLOAT fMaxFov
ENDSTRUCT

STRUCT CAM_CONTROL_SPEEDS
	FLOAT fBaseZoomSpeed
	FLOAT fBaseRotSpeed
ENDSTRUCT

STRUCT CAM_WEAPON_BASE_DATA
	WEAPON_TYPE eShotType
	WEAPON_MODE eFiringMode
	INT iTimeBetweenShotsMs
	INT iWeaponDamage
	FLOAT fRange
ENDSTRUCT

STRUCT CAM_WEAPON_FULL_AUTO
	FLOAT fSpreadAngleMax
	INT	iAmmoDurationMs
	INT iReloadDurationMs
ENDSTRUCT

STRUCT CAM_WEAPON_HOMING_MISSILE
	MODEL_NAMES eModel
ENDSTRUCT

/// PURPOSE: Used for CAM_LOAD_SETTINGS.iBs
CONST_INT ci_TURRET_CAM_LOAD_BS_LOADSCENE	0
CONST_INT ci_TURRET_CAM_LOAD_BS_FADEOUT		1
CONST_INT ci_TURRET_CAM_LOAD_BS_FADEIN		2

STRUCT CAM_LOAD_SETTINGS
	INT iLoadSceneWaitTimeMs
	// Uses ci_TURRET_CAM_LOAD_BS_ 
	INT iBs
ENDSTRUCT

ENUM TURRET_CAM_HUD_TYPE
	TCHT_TURRET_CAM,
	TCHT_ARENA_APOC,
	TCHT_ARENA_SCIFI,
	TCHT_ARENA_CONS,

	// Don't want default to be "none" as it is a special case
	TCHT_NONE
ENDENUM

ENUM TURRET_CAM_HUD_SOUNDS
	TCHS_ARENA_CAM,
	TCHS_ARENA_CONTESTANT_CAM,
	// Don't want default to be "none" as it is a special case
	TCHS_NONE
ENDENUM

ENUM ARENA_HUD_WEAPON_ICON
	AHWI_TRANSPARENT 	= 1,
	AHWI_OPAQUE 		= 2,
	AHWI_HIDDEN 		= 3,
	AHWI_GREY			= 4
ENDENUM

STRUCT TURRET_CAM_ARENA_HUD
	BOOL bZoomVisible = TRUE
	ARENA_HUD_WEAPON_ICON eIconMg = AHWI_HIDDEN // Machine gun icon
	ARENA_HUD_WEAPON_ICON eIconHm = AHWI_HIDDEN // Homing missile icon
	ARENA_HUD_WEAPON_ICON eIconPm = AHWI_HIDDEN // Piloted missile icon
ENDSTRUCT

ENUM TURRET_CAM_WEAPON_SOUNDS
	TCWS_ARENA_TURRET_SOUNDS,
	TCWS_ARENA_CONTESTANT_MG_SOUNDS,
	TCWS_ARENA_CONTESTANT_HM_SOUNDS,
	TCWS_ARENA_CONTESTANT_PM_SOUNDS,
	
	TCWS_NONE
ENDENUM

CONST_INT ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS 8
STRUCT TURRET_CAM_INSTRUCTIONAL_BUTTONS
	INT iCount
	TEXT_LABEL_15 txt15Labels[ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS]
	CONTROL_ACTION eActions[ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS]
ENDSTRUCT

CONST_INT ci_TURRET_CAM_MAX_STATIC_FIRE_POINTS 4
STRUCT TURRET_CAM_STATIC_FIRE_POINTS
	INT iCount
	INT iBoneIds[ci_TURRET_CAM_MAX_STATIC_FIRE_POINTS]
ENDSTRUCT

STRUCT TURRET_CAM_CONFIG
	// DEBUGONLY variables to be used while testing out new config features
	// without having to relaunch game (config stored globally).
	#IF IS_DEBUG_BUILD
	INT iInts[5]
	#ENDIF
	
	CAM_CONTROL_LIMITS camLimits
	CAM_CONTROL_SPEEDS camSpeeds
	
	// Base data shared between /most/ firing modes.
	CAM_WEAPON_BASE_DATA weapon
	// Add data required specifically for new firing modes here...
	CAM_WEAPON_FULL_AUTO fullAuto
	CAM_WEAPON_HOMING_MISSILE homingMissile
	
	// Huds and sounds are very specific so they have to be set up inernally. A new hud needs a new implementation.
	TURRET_CAM_HUD_TYPE eHudType
	TURRET_CAM_HUD_SOUNDS eHudSounds
	TURRET_CAM_WEAPON_SOUNDS eWeaponSounds

	// Data for specific huds
	TURRET_CAM_ARENA_HUD arenaHud
	
	TURRET_CAM_INSTRUCTIONAL_BUTTONS instButtons
	TEXT_LABEL_15 txt15HelpLabel
	
	// An offset vector the cam is attached to from the inital cam vCoords.
	// Rotations around z will move this offset vector instead of rotate the cam.
	VECTOR vJointZ 
	
	TURRET_CAM_STATIC_FIRE_POINTS staticFirePoints
	
	TEXT_LABEL_15 txt15InputMap
	
	CAM_LOAD_SETTINGS load
ENDSTRUCT

STRUCT TURRET_CAM_DAMAGE_ACCUMULATOR
	INT iFrame		// Frame this struct was last udpated
	INT iDamage		// Damage taken on iFrame
	INT iHeadshots	// Headshots suffered on iFrame
ENDSTRUCT
