USING "globals.sch"
USING "rage_builtins.sch"

// Header support for HUD_FEED_FRIENDS.gfx

/// PURPOSE: Displays the friends feed image and localised title and body strings in the top left of the HUD
/// Make sure you have loaded the movie with REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE, 
/// and checked it has loaded with HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED before using this.
/// PARAMS:
///    sHUDFeedFriends - Scaleform movie ID for the Friends Feed UI
///    sUserText - Friends Username string for the Friends Feed Message
///    sBodyText - Body string for the Friends Feed Message
///    sTXD - The Texture Dictionary for the image used in the Friends Feed Message
///    sImageName - The Image name string for the image used in the Friends Feed Message
PROC Set_Feed_Friends(eSCRIPT_HUD_COMPONENT sHUDFeedFriends, STRING sUserText, STRING sBodyText, STRING sTXD, STRING sImageName)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedFriends, "SET_FEED_FRIENDS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sUserText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBodyText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sImageName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Removes friends component from screen. Be sure to remove the scaleform movie when it is no longer required
///    
/// PARAMS:
///    sHUDFeedFriends - Scaleform movie ID for the Friends Feed UI
PROC Remove_Feed_Friends(eSCRIPT_HUD_COMPONENT sHUDFeedFriends)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedFriends, "REMOVE_FEED_FRIENDS")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC
