// script_usecontext.sch
USING "commands_graphics.sch"
USING "commands_pad.sch"
USING "script_maths.sch"
USING "minigames_helpers.sch"

/**********************************************************************************
// NOTES:
As of 5/30/2012 & #523806:
- No one was using SIMPLE_USE_CONTEXTs... so I took them out. Can always go back 
	in the depot and rescue them. They were last present in rev. 
***********************************************************************************/

CONST_INT		MAX_USE_CONTEXT_LABELS		8 //4

/// PURPOSE: Flags that are common to simple and complex use contexts.
ENUM USE_CONTEXT_FLAGS
	UCF_NONE 				= 0,
	
	UCF_LOADED 				= BIT0,
	UCF_CREATED				= BIT1,
	UCF_IS_FOR_MENU			= BIT4,
	
	UCF_VERTICAL			= BIT5,		// If not set, defaults to Horizontal.
	UCF_HAS_TRIGGER			= BIT6,
	
	UCF_TRIGGER_OFF_LOADED	= BIT7,
	UCF_TRIGGER_ON_LOADED	= BIT8,
	
	UCF_ABOVE_TIMER			= BIT9,
	UCF_DRAW_FULLSCREEN		= BIT10,
	UCF_BELOW_SCORE			= BIT11,
	
	UCF_CLICKABLE			= BIT12,
	UCF_CHECK_CONTROL_CHANGE = BIT13
ENDENUM

/// PURPOSE: Complex use contexts can have 4 buttons per label.
///    This enum is used to index them.
ENUM COMPLEX_USE_CONTEXT_BUTTONS
	CUCB_BUTTON0_1 = 0,
	CUCB_BUTTON0_2,
	CUCB_BUTTON0_3,
	CUCB_BUTTON0_4,
	
	CUCB_BUTTON1_1,
	CUCB_BUTTON1_2,
	CUCB_BUTTON1_3,
	CUCB_BUTTON1_4,
	
	CUCB_BUTTON2_1,
	CUCB_BUTTON2_2,
	CUCB_BUTTON2_3,
	CUCB_BUTTON2_4,
	
	CUCB_BUTTON3_1,
	CUCB_BUTTON3_2,
	CUCB_BUTTON3_3,
	CUCB_BUTTON3_4,
	
	CUCB_BUTTON4_1,
	CUCB_BUTTON4_2,
	CUCB_BUTTON4_3,
	CUCB_BUTTON4_4,
	
	CUCB_BUTTON5_1,
	CUCB_BUTTON5_2,
	CUCB_BUTTON5_3,
	CUCB_BUTTON5_4,
	
	CUCB_BUTTON6_1,
	CUCB_BUTTON6_2,
	CUCB_BUTTON6_3,
	CUCB_BUTTON6_4,
	
	CUCB_BUTTON7_1,
	CUCB_BUTTON7_2,
	CUCB_BUTTON7_3,
	CUCB_BUTTON7_4,
	
	CUCB_NUM_BUTTONS
ENDENUM

/// PURPOSE: When you have multiple buttons, and multiple labels that each may have multiple buttons, use this.
STRUCT COMPLEX_USE_CONTEXT
	SCALEFORM_INDEX 		sfMov 	= NULL
	USE_CONTEXT_FLAGS		eFlags	= UCF_NONE
	
	STRING 					sLabel[MAX_USE_CONTEXT_LABELS]
	STRING 				    eButton[CUCB_NUM_BUTTONS]
	CONTROL_ACTION			eButtonInput[CUCB_NUM_BUTTONS]		// Input for mouse clickable instructional buttons
	STRING	                sPreButtonLabel[CUCB_NUM_BUTTONS]
	
	STRING 					sTriggerLabel
	
	BOOL					bMouseClickable
	
ENDSTRUCT

/// PURPOSE:
///    Checks to see if the script scaleform UI passed in is valid or not, also checks to see if it's loaded..
/// RETURNS:
///    TRUE if valid and loaded. Also sets an internal flag that says the mov is loaded.
FUNC BOOL IS_COMPLEX_USE_CONTEXT_VALID(COMPLEX_USE_CONTEXT& useContext)
	IF (useContext.sfMov <> NULL)
		IF HAS_SCALEFORM_MOVIE_LOADED(useContext.sfMov)
			SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_LOADED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Setup a complex use context (4 labels, up to 4 buttons per label).
///    This is done in multiple steps. The first being that a single label and single context are setup. The rest are added to the 
///    context by the scripter using ADD_BUTTON_TO_COMPLEX_USE_CONTEXT and 
PROC INIT_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext, STRING sLabel0, STRING eLabel0_Button1, BOOL bVertical = TRUE, BOOL bUseSystemTime = FALSE, BOOL bMouseClickable = FALSE, CONTROL_ACTION caInput = MAX_INPUTS)
	// Request the movie, if we haven't already.
	IF (useContext.sfMov = NULL)
		useContext.sfMov = REQUEST_SCALEFORM_MOVIE_INSTANCE("instructional_buttons")
	ENDIF
	
	// Wipe all existing data.
	INT index
	REPEAT CUCB_NUM_BUTTONS index
		useContext.eButton[index] = NULL
		useContext.sPreButtonLabel[index] = ""
		useContext.eButtonInput[index] = MAX_INPUTS
	ENDREPEAT
	REPEAT MAX_USE_CONTEXT_LABELS index
		useContext.sLabel[index] = ""
	ENDREPEAT
	
	
	useContext.sLabel[0] 				= sLabel0
	useContext.eButton[CUCB_BUTTON0_1] 	= eLabel0_Button1
	useContext.eButtonInput[CUCB_BUTTON0_1] = caInput
	useContext.eFlags 					= UCF_NONE
	IF bVertical
		SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_VERTICAL)
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(useContext.sfMov)
		SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_LOADED)
		IF bUseSystemTime
			SET_SCALEFORM_MOVIE_TO_USE_SYSTEM_TIME(useContext.sfMov, TRUE)
		ENDIF
	ENDIF
	
	IF IS_PC_VERSION()
		useContext.bMouseClickable = bMouseClickable
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets a complex use context as either attached, or detached from a minigame
PROC SET_COMPLEX_USE_CONTEXT_MINIGAME_ATTACHED(COMPLEX_USE_CONTEXT& useContext, BOOL bAttachToMinigame = TRUE)
	IF (bAttachToMinigame)
		SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_IS_FOR_MENU)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_IS_FOR_MENU)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a complex use context above a timer, like in darts
PROC SET_COMPLEX_USE_CONTEXT_ABOVE_TIMER(COMPLEX_USE_CONTEXT& useContext, BOOL bAboveTimer = TRUE)
	IF (bAboveTimer)
		SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_ABOVE_TIMER)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_ABOVE_TIMER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a complex use context to draw fullscreen
PROC SET_COMPLEX_USE_CONTEXT_FULLSCREEN(COMPLEX_USE_CONTEXT& useContext, BOOL bFullScreen = TRUE)
	IF (bFullScreen)
		SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_DRAW_FULLSCREEN)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_DRAW_FULLSCREEN)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a complex use context above a timer, like in darts
PROC SET_COMPLEX_USE_CONTEXT_BELOW_SCORE(COMPLEX_USE_CONTEXT& useContext, BOOL bBelowScore = TRUE)
	IF (bBelowScore)
		SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_BELOW_SCORE)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_BELOW_SCORE)
	ENDIF
ENDPROC

/// PURPOSE:
///	   Adds a button icon to a complex use context. This does not add a label to the use context, but allows overwriting an input there, or adding one.
///    For example, I set up my complex context as ["Jump" A]. This function would allow mt to display ["Jump" A X Up B], if I added all of those buttons 
///    in the slots.
///    This does not check to see if there's a label corresponding to where you added the button. If there's no label, this button will not draw.
PROC ADD_BUTTON_TO_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext, COMPLEX_USE_CONTEXT_BUTTONS eSlotToAddIn, STRING eButtonToAdd, STRING sPreButtonText = NULL)
	useContext.sPreButtonLabel[eSlotToAddIn] = sPreButtonText
	useContext.eButton[eSlotToAddIn] = eButtonToAdd
ENDPROC

/// PURPOSE:
///    Adds a new label and input to a complex use context. This allows new inputs to be added.
/// PARAMS:
///    All fairly self explanatory except...
///    iSlot - The slot where the label will go. Is 0 based. So a 0 passed in means that instruction 0 and it's buttons are set.
PROC ADD_LABEL_TO_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext, STRING sLabelToAdd, STRING eButtontoAdd, INT iSlot, CONTROL_ACTION caInputToAdd = MAX_INPUTS)
	IF (iSlot < 0)
		SCRIPT_ASSERT("ADD_LABEL_TO_COMPLEX_USE_CONTEXT: Trying to add a new label in an invalid slot (< 0)!")
		EXIT
	ENDIF
	
	IF (GET_LENGTH_OF_LITERAL_STRING(sLabelToAdd) = 0)
		SCRIPT_ASSERT("ADD_LABEL_TO_COMPLEX_USE_CONTEXT: Blank label passed in!")
		EXIT
	ENDIF
	
	// Set the label, then set the slot 1 button, and wipe the others.
	useContext.sLabel[iSlot] = sLabelToAdd
	SWITCH (iSlot)
		CASE 0
			useContext.eButton[CUCB_BUTTON0_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON0_2] = NULL
			useContext.eButton[CUCB_BUTTON0_3] = NULL
			useContext.eButton[CUCB_BUTTON0_4] = NULL
			
			useContext.eButtonInput[CUCB_BUTTON0_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON0_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON0_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON0_4] = MAX_INPUTS
			
		BREAK
		
		CASE 1
			useContext.eButton[CUCB_BUTTON1_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON1_2] = NULL
			useContext.eButton[CUCB_BUTTON1_3] = NULL
			useContext.eButton[CUCB_BUTTON1_4] = NULL
			
			useContext.eButtonInput[CUCB_BUTTON1_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON1_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON1_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON1_4] = MAX_INPUTS
			
		BREAK
		
		CASE 2
			useContext.eButton[CUCB_BUTTON2_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON2_2] = NULL
			useContext.eButton[CUCB_BUTTON2_3] = NULL
			useContext.eButton[CUCB_BUTTON2_4] = NULL
			
				
			useContext.eButtonInput[CUCB_BUTTON2_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON2_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON2_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON2_4] = MAX_INPUTS
			
		BREAK
		
		CASE 3
			useContext.eButton[CUCB_BUTTON3_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON3_2] = NULL
			useContext.eButton[CUCB_BUTTON3_3] = NULL
			useContext.eButton[CUCB_BUTTON3_4] = NULL
			
				
			useContext.eButtonInput[CUCB_BUTTON3_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON3_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON3_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON3_4] = MAX_INPUTS
			
		BREAK
		
		CASE 4
			useContext.eButton[CUCB_BUTTON4_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON4_2] = NULL
			useContext.eButton[CUCB_BUTTON4_3] = NULL
			useContext.eButton[CUCB_BUTTON4_4] = NULL
			
				
			useContext.eButtonInput[CUCB_BUTTON4_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON4_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON4_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON4_4] = MAX_INPUTS
			
		BREAK
		
		CASE 5
			useContext.eButton[CUCB_BUTTON5_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON5_2] = NULL
			useContext.eButton[CUCB_BUTTON5_3] = NULL
			useContext.eButton[CUCB_BUTTON5_4] = NULL
			
				
			useContext.eButtonInput[CUCB_BUTTON5_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON5_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON5_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON5_4] = MAX_INPUTS
			
		BREAK
		
		CASE 6
			useContext.eButton[CUCB_BUTTON6_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON6_2] = NULL
			useContext.eButton[CUCB_BUTTON6_3] = NULL
			useContext.eButton[CUCB_BUTTON6_4] = NULL
			
				
			useContext.eButtonInput[CUCB_BUTTON6_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON6_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON6_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON6_4] = MAX_INPUTS
			
		BREAK
		
		CASE 7
			useContext.eButton[CUCB_BUTTON7_1] = eButtontoAdd
			useContext.eButton[CUCB_BUTTON7_2] = NULL
			useContext.eButton[CUCB_BUTTON7_3] = NULL
			useContext.eButton[CUCB_BUTTON7_4] = NULL
			
			useContext.eButtonInput[CUCB_BUTTON7_1] = caInputToAdd
			useContext.eButtonInput[CUCB_BUTTON7_2] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON7_3] = MAX_INPUTS
			useContext.eButtonInput[CUCB_BUTTON7_4] = MAX_INPUTS
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Sets up an instructional button to be clickable, and trigger the associated input when clicked.
/// PARAMS:
///    bMouseClickable - True if clickable
///    caInput - The input that gets triggered by the click
PROC ADD_USE_CONTEXT_MOUSE_CLICKABLE_PARAMS( BOOL bMouseClickable, CONTROL_ACTION caInput  )

	IF IS_PC_VERSION()
	
		IF bMouseClickable
	
			IF caInput != MAX_INPUTS
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // CLICKABLE
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE) // NOT CLICKABLE	
			ENDIF
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(caInput))
			
		ENDIF
	
	ENDIF

ENDPROC

/// PURPOSE:
///    If you want a trigger on your use context, use this. 
///    A full menu will come up if the Trigger button is held down.
//PROC ADD_TRIGGER_TO_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext, STRING sTriggerLabel, TEXT_LABEL_63 eTriggerButton)
//	IF (GET_LENGTH_OF_LITERAL_STRING(sTriggerLabel) = 0)
//		SCRIPT_ASSERT("ADD_TRIGGER_TO_COMPLEX_USE_CONTEXT: Blank trigger label passed in!")
//		EXIT
//	ENDIF
//	
//	useContext.sTriggerLabel = sTriggerLabel
//	useContext.eTriggerButton = eTriggerButton
//	SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_HAS_TRIGGER)
//ENDPROC

/// PURPOSE:
///    Updates a complex use context.
///    Can update use contexts with or without trigger.
/// PARAMS:
///    useContext - 
///    fClearSpace - 
///    eDrawOrder - 
///    bIgnoreFade - 
///    bJPButtonSwitch - Determines whether the buttons change for Japanese controls or not, TRUE is used for frontend menus, FALSE for in-game actions
/// RETURNS:
///    The button in the complex use context that was pressed (does not return the trigger button.)
PROC UPDATE_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext, FLOAT fClearSpace = 200.0, GFX_DRAW_ORDER eDrawOrder = GFX_ORDER_BEFORE_HUD, BOOL bIgnoreFade = FALSE, BOOL bJPButtonSwitch = TRUE, FLOAT fWidth = 1.0)
	IF IS_SCREEN_FADING_OUT() 
	OR IS_SCREEN_FADING_IN()
	OR IS_SCREEN_FADED_OUT() 
	OR IS_FRONTEND_FADING()
		IF NOT bIgnoreFade
			PRINTLN("Some kind of fade happening...")
			EXIT
		ENDIF
	ENDIF
	
	// Make sure we're valid.
	IF NOT IS_COMPLEX_USE_CONTEXT_VALID(useContext)
		PRINTLN("Movie still not valid...")
		EXIT
	ENDIF
		
	//hide the loading icon if we're drawing
	HIDE_LOADING_ON_FADE_THIS_FRAME()
	
	// Align it to a menu, if need be. Draw it.
	SET_SCRIPT_GFX_DRAW_ORDER(eDrawOrder)
//	IF IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_IS_FOR_MENU)
//		DRAW_SCALEFORM_MOVIE(useContext.sfMov, 0.34921875,0.467,1.0,1.0,255,255,255,0)
//	ELIF IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_ABOVE_TIMER)
//		DRAW_SCALEFORM_MOVIE(useContext.sfMov, 0.5,0.425,1.0,1.0,255,255,255,0)
//	ELIF IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_DRAW_FULLSCREEN)
//		DRAW_SCALEFORM_MOVIE_FULLSCREEN(useContext.sfMov, 255,255,255,0)
//	ELIF IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_BELOW_SCORE)
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
//		DRAW_SCALEFORM_MOVIE(useContext.sfMov, 0.5,0.563,1.0,1.0,255,255,255,0)
//		RESET_SCRIPT_GFX_ALIGN()
//	ELSE
//	ENDIF
	
//	// If this complex use context has a trigger, look for that.
//	IF IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_HAS_TRIGGER)
//		// Get our pad button from the trigger button.
//		CONTROL_ACTION eTrigger = TRANSLATE_INSTR_BUTTON_TO_CONTROL_ACTION(useContext.eTriggerButton)	
//		
//		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, eTrigger)
//			// We're not pressing the trigger button.
//			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_TRIGGER_OFF_LOADED)
//				// Have not loaded the trigger up data. Load it.
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_CLEAR_SPACE")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
//				END_SCALEFORM_MOVIE_METHOD()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_DATA_SLOT_EMPTY")
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				// Load the trigger button to draw.
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_DATA_SLOT")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(useContext.eTriggerButton)))
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sTriggerLabel)
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				FLOAT fResult = PICK_FLOAT(bJPButtonSwitch, PICK_FLOAT(IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_VERTICAL), 1.0, 0.0), -1.0)
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fResult)
//				END_SCALEFORM_MOVIE_METHOD()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_BACKGROUND_COLOUR")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
//				END_SCALEFORM_MOVIE_METHOD()
//			
//				// Flag us as loaded.
//				SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_TRIGGER_OFF_LOADED)
//				CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_TRIGGER_ON_LOADED)
//			ENDIF
//		ELSE
//			// We're pressing the trigger button.
//			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_TRIGGER_ON_LOADED)
//				PRINTLN("SETUP!!!!!!!")
//				
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_CLEAR_SPACE")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
//				END_SCALEFORM_MOVIE_METHOD()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_DATA_SLOT_EMPTY")
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				// Load all button presses to draw.
//				FLOAT fSlot = 0.0
//				INT index
//				COMPLEX_USE_CONTEXT_BUTTONS eButtonSlotBase
//				REPEAT MAX_USE_CONTEXT_LABELS index
//					IF NOT IS_STRING_EMPTY(useContext.sLabel[index])
//						eButtonSlotBase = INT_TO_ENUM(COMPLEX_USE_CONTEXT_BUTTONS, index * 4)
//						
//						// Have a button with a label, we need to add it.
//						BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_DATA_SLOT")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(useContext.eButton[eButtonSlotBase])))
//							IF (ENUM_TO_INT(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+1]) != -1)
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+1])))
//							ENDIF
//							IF (ENUM_TO_INT(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+2]) != -1)
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+2])))
//							ENDIF
//							IF (ENUM_TO_INT(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+3]) != -1)
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+3])))
//							ENDIF
//
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sLabel[index])
//						END_SCALEFORM_MOVIE_METHOD()
//							
//						fSlot += 1
//					ENDIF
//				ENDREPEAT
//				
//				FLOAT fResult = PICK_FLOAT(bJPButtonSwitch, PICK_FLOAT(IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_VERTICAL), 1.0, 0.0), -1.0)
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fResult)
//				END_SCALEFORM_MOVIE_METHOD()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_BACKGROUND_COLOUR")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
//				END_SCALEFORM_MOVIE_METHOD()
//			
//				// Flag us as loaded.
//				SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_TRIGGER_ON_LOADED)
//				CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_TRIGGER_OFF_LOADED)
//			ENDIF
//		ENDIF
//		
//	ELSE
			
		
		// This is a use context without a trigger. Load it once, and move on.
		IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_TRIGGER_ON_LOADED)
			PRINTLN("SETUP 2!!!!")
					
			BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_CLEAR_SPACE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fClearSpace)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_MAX_WIDTH")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fWidth)
			END_SCALEFORM_MOVIE_METHOD()

			BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_DATA_SLOT_EMPTY")
			END_SCALEFORM_MOVIE_METHOD()
			
			IF IS_PC_VERSION()
				BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "TOGGLE_MOUSE_BUTTONS")
		    		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(useContext.bMouseClickable)
				END_SCALEFORM_MOVIE_METHOD()

			ENDIF
			
			FLOAT fSlot = 0.0
			INT index
			COMPLEX_USE_CONTEXT_BUTTONS eButtonSlotBase
			REPEAT MAX_USE_CONTEXT_LABELS index
				eButtonSlotBase = INT_TO_ENUM(COMPLEX_USE_CONTEXT_BUTTONS, index * 4)
			
				IF NOT IS_STRING_NULL_OR_EMPTY(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)])  //button was set to somthing

					// Have a button with a label, we need to add it.					
					BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
							
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.sPreButtonLabel[eButtonSlotBase])
								PRINTLN("Adding pre button text to slot ", eButtonSlotBase, " text ", useContext.sPreButtonLabel[eButtonSlotBase])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sPreButtonLabel[eButtonSlotBase])
							ENDIF
							
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.eButton[eButtonSlotBase])
								PRINTLN("Adding instruct button text to slot ", eButtonSlotBase, " number ", useContext.eButton[eButtonSlotBase])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(useContext.eButton[eButtonSlotBase])
							ENDIF
													
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+1])
								PRINTLN("Adding pre button text to slot ", eButtonSlotBase, " text ", useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+1])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+1])
							ENDIF
													
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+1])
								PRINTLN("Adding instruct button text to slot ", eButtonSlotBase, " number ", useContext.eButton[eButtonSlotBase])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+1])
							ENDIF
							
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+2])
								PRINTLN("Adding pre button text to slot ", eButtonSlotBase, " text ", useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+2])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+2])
							ENDIF
							
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+2])
								PRINTLN("Adding instruct button text to slot ", eButtonSlotBase, " number ", useContext.eButton[eButtonSlotBase])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+2])
							ENDIF			
							
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+3])
								PRINTLN("Adding pre button text to slot ", eButtonSlotBase, " text ", useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+3])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sPreButtonLabel[ENUM_TO_INT(eButtonSlotBase)+3])
							ENDIF
							IF NOT IS_STRING_NULL_OR_EMPTY(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+3])
								PRINTLN("Adding instruct button text to slot ", eButtonSlotBase, " number ", useContext.eButton[eButtonSlotBase])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(useContext.eButton[ENUM_TO_INT(eButtonSlotBase)+3])
							ENDIF
							
							IF NOT IS_STRING_EMPTY(useContext.sLabel[index])
								PRINTLN("Adding label text to slot ", eButtonSlotBase, ": ", useContext.sLabel[index])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(useContext.sLabel[index])
							ENDIF
							
						ADD_USE_CONTEXT_MOUSE_CLICKABLE_PARAMS( useContext.bMouseClickable, useContext.eButtonInput[eButtonSlotBase])
						
					END_SCALEFORM_MOVIE_METHOD()
						
					fSlot += 1
				ENDIF
			ENDREPEAT
			
			FLOAT fResult = PICK_FLOAT(bJPButtonSwitch, PICK_FLOAT(IS_BITMASK_ENUM_AS_ENUM_SET(useContext.eFlags, UCF_VERTICAL), 1.0, 0.0), -1.0)
			BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fResult)
			END_SCALEFORM_MOVIE_METHOD()

			BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "SET_BACKGROUND_COLOUR")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
			END_SCALEFORM_MOVIE_METHOD()
		
			// Flag us as loaded.
			SET_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_TRIGGER_ON_LOADED)
			CLEAR_BITMASK_ENUM_AS_ENUM(useContext.eFlags, UCF_TRIGGER_OFF_LOADED)
		ENDIF
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(useContext.sfMov, 255,255,255,0)
	//ENDIF
	
//	// Check to see if any of our buttons were pressed.
//	INT index
//	REPEAT CUCB_NUM_BUTTONS index
//		//IF NOT IS_STRING_EMPTY(useContext.sLabel[index])
//			CONTROL_ACTION eButton = TRANSLATE_INSTR_BUTTON_TO_CONTROL_ACTION(useContext.eButton[index])
//			IF (ENUM_TO_INT(eButton) != -1)
//				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, eButton)
//					RETURN useContext.eButton[index]
//				ENDIF
//			ENDIF
//		//ENDIF
//	ENDREPEAT
	
//	RETURN NULL
ENDPROC

/// PURPOSE:
///    Cleans the simple use context.
PROC CLEANUP_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext)
	

	IF (useContext.sfMov <> NULL)
		
		IF IS_PC_VERSION()
		
			BEGIN_SCALEFORM_MOVIE_METHOD(useContext.sfMov, "TOGGLE_MOUSE_BUTTONS")
	    		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
			
			useContext.bMouseClickable = FALSE
			
		ENDIF
	
	
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(useContext.sfMov)
		useContext.sfMov = NULL
	ENDIF
	
	useContext.sTriggerLabel 	= ""
	//useContext.eTriggerButton 	= ICON_UP
	useContext.eFlags 	= UCF_NONE
	
	INT index
	REPEAT CUCB_NUM_BUTTONS index
		useContext.eButton[index] = NULL
		useContext.eButtonInput[index] = MAX_INPUTS
	ENDREPEAT
	
	REPEAT MAX_USE_CONTEXT_LABELS index
		useContext.sLabel[index] = ""
	ENDREPEAT
ENDPROC

FUNC BOOL IS_BUTTON_IN_COMPLEX_USE_CONTEXT(COMPLEX_USE_CONTEXT& useContext, STRING eButton)
	INT idx
	REPEAT CUCB_NUM_BUTTONS idx
		IF NOT IS_STRING_NULL(useContext.eButton[idx])
			IF ARE_STRINGS_EQUAL(useContext.eButton[idx], eButton)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC








/// PURPOSE: These do most of what COMPLEX_USE_CONTEXTs do (The only omission is that you can't put a pre-label on the inputs)
///    It can do things that COMPLEX_USE_CONTEXTs can't, such as automatically refresh the decals when the control method changes.
///    The way the movie loads, way the flags work, etc is the same as COMPLEX_USE_CONTEXTS, so you should be able to swap them out fairly easily.
///    The main difference in the interface, is that the init function doesn't also setup the first button, it just inits the system.
///    Use the ADD INPUT (or ADD GROUP) functions to add buttons, then call ADD INPUT CONCATENATED (or ADD GROUP CONCATENATED) functions immediately afterwards if you want multiple inputs per button.
///    
///    New feature: You can now make a button pad-only or keyboard-only.
///    So if your script checks for different inputs depending on which control method is used - Add buttons for both versions, using the relevant display condition for each one.
///    Note - this affects the entire button, so any concatenated inputs will have the condition applied to them too.
///    Also note - this reduces the max amount of buttons, as you're having to double up on those ones.

ENUM UC_XO_SWAP_PARAM
	UC_DONT_ALLOW_XO_SWAP = 0,
	UC_ALLOW_XO_SWAP
ENDENUM

ENUM UC_MOUSE_CLICK_PARAM
	UC_DONT_ALLOW_MOUSE_CLICK = 0,
	UC_ALLOW_MOUSE_CLICK
ENDENUM

ENUM UC_CONDITION_PARAM
	UC_CONDITION_ALWAYS_DISPLAY = 0,
	UC_CONDITION_DISPLAY_FOR_KB_ONLY,
	UC_CONDITION_DISPLAY_FOR_PAD_ONLY
ENDENUM

CONST_INT MAX_SIMPLE_USE_CONTEXT_BUTTONS	8
CONST_INT MAX_SIMPLE_USE_CONTEXT_CONTROLS	4

STRUCT SIMPLE_USE_CONTEXT_CONTROL
	CONTROL_TYPE				eType
	INT							iAction
ENDSTRUCT

STRUCT SIMPLE_USE_CONTEXT_BUTTON
	STRING						sLabel
	BOOL						bIsClickable
	UC_CONDITION_PARAM			eCondition
	
	SIMPLE_USE_CONTEXT_CONTROL	mControls[MAX_SIMPLE_USE_CONTEXT_CONTROLS]
	INT							iControlGroupBits
	INT							iControlSwapBits
	INT							iControlCount
ENDSTRUCT

STRUCT SIMPLE_USE_CONTEXT
	SCALEFORM_INDEX 			sfMov 				= NULL
	USE_CONTEXT_FLAGS			eFlags				= UCF_NONE
	
	SIMPLE_USE_CONTEXT_BUTTON	mButtons[MAX_SIMPLE_USE_CONTEXT_BUTTONS]
	INT							iButtonCount
ENDSTRUCT



/// INIT_SIMPLE_USE_CONTEXT()
///    
///    Clears any previously added buttons, and prepares use context for buttons to be added.
///    
///    bIsVertical			- Display the input vertically or horizontally in a bar.
///    bUseSystemTime		- When the game is paused, system time continues to run.
///    bAllowMouseClick		- Overall enable/disable for mouse clicking on instructional buttons [note, you must also enable for each added input too].
///    bAllowAutoRefresh	- Allows the system to recalculate and redraw the sprites, if the control scheme changes between pad / keyboard.
PROC INIT_SIMPLE_USE_CONTEXT(SIMPLE_USE_CONTEXT& system, BOOL bIsVertical = TRUE, BOOL bUseSystemTime = FALSE, BOOL bAllowMouseClick = FALSE, BOOL bAllowAutoRefresh = FALSE)

	// Request the movie, if we haven't already
	IF (system.sfMov = NULL)
		system.sfMov = REQUEST_SCALEFORM_MOVIE_INSTANCE("instructional_buttons")
	ENDIF
	
	// Clear data
	system.eFlags			= UCF_NONE
	system.iButtonCount		= 0

	// Set flags
	IF bIsVertical
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_VERTICAL)
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(system.sfMov)
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_LOADED)
		IF bUseSystemTime
			SET_SCALEFORM_MOVIE_TO_USE_SYSTEM_TIME(system.sfMov, TRUE)
		ENDIF
	ENDIF
	
	IF IS_PC_VERSION()
		IF bAllowMouseClick
			SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_CLICKABLE)
		ENDIF
	ENDIF
	
	IF bAllowAutoRefresh
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_CHECK_CONTROL_CHANGE)
	ENDIF
	
ENDPROC

/// ADD_SIMPLE_USE_CONTEXT_INPUT()
///    
///    Adds a label and an input sprite to the use context (instructional button) bar.
///    Returns true if it manages to add it.
///    
///    You can add a maximum of 8 labels.
///    
///    system			- The use context struct
///    sLabel			- The text string for the prompt
///    eControlType		- Frontend or player control
///    eControlAction	- The input to display
///    eAllowXOSwap		- Allow XO swapping when choosing the sprite [japanese control schemes will often swap these]
///    eAllowMouseClick	- Allow the player to click on the icon in the instructional button bar [note you must also enable mouse clicking when calling INIT_SIMPLE_USE_CONTEXT()]
FUNC BOOL ADD_SIMPLE_USE_CONTEXT_INPUT(SIMPLE_USE_CONTEXT& system, STRING sLabel, CONTROL_TYPE eControlType, CONTROL_ACTION eControlAction, UC_XO_SWAP_PARAM eAllowXOSwap = UC_ALLOW_XO_SWAP, UC_MOUSE_CLICK_PARAM eAllowMouseClick = UC_ALLOW_MOUSE_CLICK, UC_CONDITION_PARAM eCondition = UC_CONDITION_ALWAYS_DISPLAY)

	IF system.sfMov = NULL
		SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_INPUT() - System not init")
		RETURN FALSE
	ENDIF

	BOOL bIsClickable = FALSE
	IF eAllowMouseClick = UC_ALLOW_MOUSE_CLICK
		bIsClickable = TRUE
	ENDIF
	
	INT iButtonID = system.iButtonCount
	IF iButtonID < MAX_SIMPLE_USE_CONTEXT_BUTTONS
	
		// Init button data
		system.mButtons[iButtonID].sLabel				= sLabel
		system.mButtons[iButtonID].bIsClickable			= bIsClickable
		system.mButtons[iButtonID].eCondition			= eCondition
		
		system.mButtons[iButtonID].iControlGroupBits	= 0
		system.mButtons[iButtonID].iControlSwapBits		= 0
		system.mButtons[iButtonID].iControlCount		= 0
		
		// Add button's control[0] data
		system.mButtons[iButtonID].mControls[0].eType	= eControlType
		system.mButtons[iButtonID].mControls[0].iAction	= ENUM_TO_INT(eControlAction)
		
		IF eAllowXOSwap = UC_ALLOW_XO_SWAP
			SET_BIT(system.mButtons[iButtonID].iControlSwapBits, 0)
		ENDIF
		
		// Update counts
		system.mButtons[iButtonID].iControlCount++
		system.iButtonCount++
		RETURN TRUE
	
	ENDIF
	
	SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_INPUT() - Out of button slots")
	RETURN FALSE

ENDFUNC
	
/// ADD_SIMPLE_USE_CONTEXT_GROUP()
///    
///    Adds a label and a group input sprite to the use context (instructional button) bar.
///    Returns true if it manages to add it.
///    
///    You can add a maximum of 8 labels.
///    
///    system				- The use context struct
///    sLabel				- The text string for the prompt
///    eControlType			- Frontend or player control
///    eControlActionGroup	- The input to display
///    eAllowXOSwap			- Allow XO swapping when choosing the sprite [japanese control schemes will often swap these]
FUNC BOOL ADD_SIMPLE_USE_CONTEXT_GROUP(SIMPLE_USE_CONTEXT& system, STRING sLabel, CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eControlActionGroup, UC_XO_SWAP_PARAM eAllowXOSwap = UC_ALLOW_XO_SWAP, UC_CONDITION_PARAM eCondition = UC_CONDITION_ALWAYS_DISPLAY)

	IF system.sfMov = NULL
		SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_GROUP() - System not init")
		RETURN FALSE
	ENDIF

	INT iButtonID = system.iButtonCount
	IF iButtonID < MAX_SIMPLE_USE_CONTEXT_BUTTONS
	
		// Init button data
		system.mButtons[iButtonID].sLabel				= sLabel
		system.mButtons[iButtonID].bIsClickable			= FALSE		// (Groups cannot be clickable)
		system.mButtons[iButtonID].eCondition			= eCondition
		
		system.mButtons[iButtonID].iControlGroupBits	= 0
		system.mButtons[iButtonID].iControlSwapBits		= 0
		system.mButtons[iButtonID].iControlCount		= 0
		
		// Add button's control[0] data
		system.mButtons[iButtonID].mControls[0].eType	= eControlType
		system.mButtons[iButtonID].mControls[0].iAction	= ENUM_TO_INT(eControlActionGroup)
		
		SET_BIT(system.mButtons[iButtonID].iControlGroupBits, 0)

		IF eAllowXOSwap = UC_ALLOW_XO_SWAP
			SET_BIT(system.mButtons[iButtonID].iControlSwapBits, 0)
		ENDIF
		
		// Update counts
		system.mButtons[iButtonID].iControlCount++
		system.iButtonCount++
		RETURN TRUE
	
	ENDIF
	
	SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_GROUP() - Out of button slots")
	RETURN FALSE

ENDFUNC
	
/// ADD_SIMPLE_USE_CONTEXT_INPUT_CONCATENATED()
///    
///    Adds an extra input sprite to the most recently added label.
///    Returns true if it manages to add it.
///    
///    Each label can have a maximum of 4 inputs (including the original one that wasn't added with concatenate).
///    
///    system			- The use context struct
///    eControlType		- Frontend or player control
///    eControlAction	- The input to concatenate
///    eAllowXOSwap		- Allow XO swapping when choosing the sprite [japanese control schemes will often swap these]
FUNC BOOL ADD_SIMPLE_USE_CONTEXT_INPUT_CONCATENATED(SIMPLE_USE_CONTEXT& system, CONTROL_TYPE eControlType, CONTROL_ACTION eControlAction, UC_XO_SWAP_PARAM eAllowXOSwap = UC_ALLOW_XO_SWAP)

	IF system.sfMov = NULL
		SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_INPUT_CONCATENATED() - System not init")
		RETURN FALSE
	ENDIF

	INT iButtonID = system.iButtonCount - 1
	
	IF  iButtonID >= 0
	AND iButtonID < MAX_SIMPLE_USE_CONTEXT_BUTTONS
	
		INT iControlID = system.mButtons[iButtonID].iControlCount
		IF iControlID < MAX_SIMPLE_USE_CONTEXT_CONTROLS
		
			// Add button's control[n] data
			system.mButtons[iButtonID].mControls[iControlID].eType		= eControlType
			system.mButtons[iButtonID].mControls[iControlID].iAction	= ENUM_TO_INT(eControlAction)
		
			IF eAllowXOSwap = UC_ALLOW_XO_SWAP
				SET_BIT(system.mButtons[iButtonID].iControlSwapBits, iControlID)
			ENDIF
		
			// Update count
			system.mButtons[iButtonID].iControlCount++
			RETURN TRUE
		
		ENDIF
	
		SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_INPUT_CONCATENATED() - Out of control slots")
	
	ENDIF
	
	SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_INPUT_CONCATENATED() - No valid button to add to")
	RETURN FALSE

ENDFUNC
	
/// ADD_SIMPLE_USE_CONTEXT_GROUP_CONCATENATED()
///    
///    Adds an extra input group sprite to the most recently added label.
///    Returns true if it manages to add it.
///    
///    Each label can have a maximum of 4 inputs (including the original one that wasn't added with concatenate).
///    
///    system				- The use context struct
///    eControlType			- Frontend or player control
///    eControlActionGroup	- The input group to concatenate
///    eAllowXOSwap			- Allow XO swapping when choosing the sprite [japanese control schemes will often swap these]
FUNC BOOL ADD_SIMPLE_USE_CONTEXT_GROUP_CONCATENATED(SIMPLE_USE_CONTEXT& system, CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eControlActionGroup, UC_XO_SWAP_PARAM eAllowXOSwap = UC_ALLOW_XO_SWAP)

	IF system.sfMov = NULL
		SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_GROUP_CONCATENATED() - System not init")
		RETURN FALSE
	ENDIF

	INT iButtonID = system.iButtonCount - 1
	
	IF 	iButtonID >= 0
	AND iButtonID < MAX_SIMPLE_USE_CONTEXT_BUTTONS
	
		INT iControlID = system.mButtons[iButtonID].iControlCount
		IF iControlID < MAX_SIMPLE_USE_CONTEXT_CONTROLS
		
			// Add button's control[n] data
			system.mButtons[iButtonID].mControls[iControlID].eType		= eControlType
			system.mButtons[iButtonID].mControls[iControlID].iAction	= ENUM_TO_INT(eControlActionGroup)
		
			SET_BIT(system.mButtons[iButtonID].iControlGroupBits, iControlID)
			
			IF eAllowXOSwap = UC_ALLOW_XO_SWAP
				SET_BIT(system.mButtons[iButtonID].iControlSwapBits, iControlID)
			ENDIF
		
			// Update count
			system.mButtons[iButtonID].iControlCount++
			RETURN TRUE
		
		ENDIF
		
		SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_GROUP_CONCATENATED() - Out of control slots")
	
	ENDIF
	
	SCRIPT_ASSERT("ADD_SIMPLE_USE_CONTEXT_GROUP_CONCATENATED() - No valid button to add to")
	RETURN FALSE

ENDFUNC
	
FUNC BOOL IS_SIMPLE_USE_CONTEXT_INPUT_ADDED(SIMPLE_USE_CONTEXT& system, CONTROL_TYPE eControlType, CONTROL_ACTION eControlAction)
	
	IF system.sfMov = NULL
		SCRIPT_ASSERT("IS_SIMPLE_USE_CONTEXT_INPUT_ADDED() - System not init")
		RETURN FALSE
	ENDIF

	INT iControlAction = ENUM_TO_INT(eControlAction)
	
	INT iButtonID
	REPEAT system.iButtonCount iButtonID
	
		INT iControlID
		REPEAT system.mButtons[iButtonID].iControlCount iControlID
		
			// Check group flag is false
			IF NOT IS_BIT_SET(system.mButtons[iButtonID].iControlGroupBits, iControlID)
			
				// Check control data
				IF  system.mButtons[iButtonID].mControls[iControlID].eType		= eControlType
				AND system.mButtons[iButtonID].mControls[iControlID].iAction	= iControlAction
					RETURN TRUE
				ENDIF
			
			ENDIF
		
		ENDREPEAT
	
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_SIMPLE_USE_CONTEXT_GROUP_ADDED(SIMPLE_USE_CONTEXT& system, CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eControlActionGroup)
	
	IF system.sfMov = NULL
		SCRIPT_ASSERT("IS_SIMPLE_USE_CONTEXT_GROUP_ADDED() - System not init")
		RETURN FALSE
	ENDIF

	INT iControlAction = ENUM_TO_INT(eControlActionGroup)
	
	INT iButtonID
	REPEAT system.iButtonCount iButtonID
	
		INT iControlID
		REPEAT system.mButtons[iButtonID].iControlCount iControlID
		
			// Check group flag is true
			IF IS_BIT_SET(system.mButtons[iButtonID].iControlGroupBits, iControlID)
			
				// Check control data
				IF  system.mButtons[iButtonID].mControls[iControlID].eType		= eControlType
				AND system.mButtons[iButtonID].mControls[iControlID].iAction	= iControlAction
					RETURN TRUE
				ENDIF
			
			ENDIF
		
		ENDREPEAT
	
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_SIMPLE_USE_CONTEXT_VALID(SIMPLE_USE_CONTEXT& system)
	IF (system.sfMov != NULL)
		IF HAS_SCALEFORM_MOVIE_LOADED(system.sfMov)
			SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_LOADED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets a complex use context as either attached, or detached from a minigame
PROC SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(SIMPLE_USE_CONTEXT& system, BOOL bAttachToMinigame = TRUE)
	IF (bAttachToMinigame)
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_IS_FOR_MENU)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_IS_FOR_MENU)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a complex use context above a timer, like in darts
PROC SET_SIMPLE_USE_CONTEXT_ABOVE_TIMER(SIMPLE_USE_CONTEXT& system, BOOL bAboveTimer = TRUE)
	IF (bAboveTimer)
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_ABOVE_TIMER)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_ABOVE_TIMER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a complex use context to draw fullscreen
PROC SET_SIMPLE_USE_CONTEXT_FULLSCREEN(SIMPLE_USE_CONTEXT& system, BOOL bFullScreen = TRUE)
	IF (bFullScreen)
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_DRAW_FULLSCREEN)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_DRAW_FULLSCREEN)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a complex use context above a timer, like in darts
PROC SET_SIMPLE_USE_CONTEXT_BELOW_SCORE(SIMPLE_USE_CONTEXT& system, BOOL bBelowScore = TRUE)
	IF (bBelowScore)
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_BELOW_SCORE)
	ELSE
		CLEAR_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_BELOW_SCORE)
	ENDIF
ENDPROC

PROC UPDATE_SIMPLE_USE_CONTEXT(SIMPLE_USE_CONTEXT& system, FLOAT fClearSpace = 200.0, GFX_DRAW_ORDER eDrawOrder = GFX_ORDER_BEFORE_HUD, BOOL bIgnoreFade = FALSE, BOOL bJPButtonSwitch = TRUE, FLOAT fWidth = 1.0)

	//-----------------------------------------
	// Check conditions and set script settings
	//-----------------------------------------
		
	IF IS_SCREEN_FADING_OUT() 
	OR IS_SCREEN_FADING_IN()
	OR IS_SCREEN_FADED_OUT() 
	OR IS_FRONTEND_FADING()
		IF NOT bIgnoreFade
			PRINTLN("  Some kind of fade happening...")
			EXIT
		ENDIF
	ENDIF
	
	// Make sure we're valid.
	IF NOT IS_SIMPLE_USE_CONTEXT_VALID(system)
		PRINTLN("  Movie still not valid...")
		EXIT
	ENDIF
		
	//hide the loading icon if we're drawing
	HIDE_LOADING_ON_FADE_THIS_FRAME()
	
	// Align it to a menu, if need be. Draw it.
	SET_SCRIPT_GFX_DRAW_ORDER(eDrawOrder)
//	IF IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_IS_FOR_MENU)
//		DRAW_SCALEFORM_MOVIE(system.sfMov, 0.34921875,0.467,1.0,1.0,255,255,255,0)
//	ELIF IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_ABOVE_TIMER)
//		DRAW_SCALEFORM_MOVIE(system.sfMov, 0.5,0.425,1.0,1.0,255,255,255,0)
//	ELIF IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_DRAW_FULLSCREEN)
//		DRAW_SCALEFORM_MOVIE_FULLSCREEN(system.sfMov, 255,255,255,0)
//	ELIF IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_BELOW_SCORE)
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
//		DRAW_SCALEFORM_MOVIE(system.sfMov, 0.5,0.563,1.0,1.0,255,255,255,0)
//		RESET_SCRIPT_GFX_ALIGN()
//	ELSE
//	ENDIF
	
	// This is a use context without a trigger. Load it once, and move on.
	IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_TRIGGER_ON_LOADED)
	OR (IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_CHECK_CONTROL_CHANGE) AND HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL))
				
		PRINTLN("UPDATE_SIMPLE_USE_CONTEXT() rebuild...")

		//-----------------------------------
		// Clear data and set scaleform flags
		//-----------------------------------
		
		BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "SET_CLEAR_SPACE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fClearSpace)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "SET_MAX_WIDTH")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fWidth)
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		IF IS_PC_VERSION()
			BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "TOGGLE_MOUSE_BUTTONS")
	    		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_CLICKABLE))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		//------------------------
		// Add buttons and symbols
		//------------------------
		
		CONTROL_TYPE			eControlType
		INT						iControlAction
		BOOL					bControlSwap
		STRING					sControlString
		
		BOOL					bCanDisplay
		INT						iDisplaySlotID = 0
		
		// For each button...
		INT iButtonID
		REPEAT system.iButtonCount iButtonID
		
			// Check button can be displayed
			SWITCH system.mButtons[iButtonID].eCondition
				CASE UC_CONDITION_ALWAYS_DISPLAY			bCanDisplay = TRUE													BREAK
				CASE UC_CONDITION_DISPLAY_FOR_KB_ONLY		bCanDisplay = IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)			BREAK
				CASE UC_CONDITION_DISPLAY_FOR_PAD_ONLY		bCanDisplay = NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)		BREAK
				DEFAULT										bCanDisplay = FALSE													BREAK
			ENDSWITCH
			
			IF bCanDisplay
			
				IF BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "SET_DATA_SLOT")
				
					// Start adding button
					PRINTLN("  Adding button ", iButtonID)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDisplaySlotID)
					iDisplaySlotID++
					
					// For each control...
					INT iControlID
					REPEAT system.mButtons[iButtonID].iControlCount iControlID
						
						// Get control data
						eControlType	= system.mButtons[iButtonID].mControls[iControlID].eType
						iControlAction	= system.mButtons[iButtonID].mControls[iControlID].iAction
						bControlSwap	= IS_BIT_SET(system.mButtons[iButtonID].iControlSwapBits, iControlID)

						IF NOT IS_BIT_SET(system.mButtons[iButtonID].iControlGroupBits, iControlID)
							sControlString = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(eControlType, INT_TO_ENUM(CONTROL_ACTION, iControlAction), bControlSwap)
						ELSE
							sControlString = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(eControlType, INT_TO_ENUM(CONTROL_ACTION_GROUP, iControlAction), bControlSwap)
						ENDIF

						// Add control
						IF NOT IS_STRING_NULL_OR_EMPTY(sControlString)
							PRINTLN("    - control: \"", sControlString, "\"")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sControlString)
						ENDIF
					
					ENDREPEAT
					
					// Add label
					IF NOT IS_STRING_NULL_OR_EMPTY(system.mButtons[iButtonID].sLabel)
						PRINTLN("    - label: \"", system.mButtons[iButtonID].sLabel, "\"")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(system.mButtons[iButtonID].sLabel)
					ENDIF
						
					// Add clickable data (if whole system is enabled for clicks)
					IF IS_PC_VERSION()
						IF IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_CLICKABLE)
							
							IF system.mButtons[iButtonID].bIsClickable
								PRINTLN("    - Clickable")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)		// This button is clickable
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(system.mButtons[iButtonID].mControls[0].iAction)
							ELSE
								PRINTLN("    - NotClickable")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)	// This button is non-clickable
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
							ENDIF
							
						ENDIF
					ENDIF
								
					END_SCALEFORM_MOVIE_METHOD()
				
				ENDIF
			
			ENDIF
			
		ENDREPEAT
				
		FLOAT fResult = PICK_FLOAT(bJPButtonSwitch, PICK_FLOAT(IS_BITMASK_ENUM_AS_ENUM_SET(system.eFlags, UCF_VERTICAL), 1.0, 0.0), -1.0)
		BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fResult)
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "SET_BACKGROUND_COLOUR")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
		END_SCALEFORM_MOVIE_METHOD()
	
		// Flag us as loaded.
		SET_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_TRIGGER_ON_LOADED)
		CLEAR_BITMASK_ENUM_AS_ENUM(system.eFlags, UCF_TRIGGER_OFF_LOADED)

	ENDIF
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(system.sfMov, 255,255,255,0)

ENDPROC

PROC CLEANUP_SIMPLE_USE_CONTEXT(SIMPLE_USE_CONTEXT& system)

	// Unload movie
	IF (system.sfMov != NULL)
		
// fix for B* 2333651 - Don't think this is necessary anyway as the movie gets unloaded immediately after. - Steve R. LDS 12/May/15
//		IF IS_PC_VERSION()
//			BEGIN_SCALEFORM_MOVIE_METHOD(system.sfMov, "TOGGLE_MOUSE_BUTTONS")
//	    		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//			END_SCALEFORM_MOVIE_METHOD()
//		ENDIF
		
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(system.sfMov)
		system.sfMov = NULL

	ENDIF
	
	// Clear data
	system.eFlags			= UCF_NONE
	system.iButtonCount		= 0

ENDPROC

