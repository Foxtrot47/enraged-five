//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	menu_public.sch												//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Public functions to set up a menu and display it.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "net_hud_activating.sch"
USING "cellphone_public.sch"
USING "scaleform_helper.sch"
USING "vehicle_int_to_rgb.sch"
USING "shared_hud_displays.sch"
USING "menu_cursor_public.sch"


FUNC STRING GET_MENU_STRING_FROM_TEXT_LABEL(STRING paramStr)
	RETURN paramStr
ENDFUNC


/// PURPOSE: Returns the texture name for the specified icon
FUNC STRING GET_MENU_ICON_TEXTURE(MENU_ICON_TYPE paramIcon, BOOL paramSelected)

	STRING sTexture[2]
	
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuData.sIconTextureOverride[paramIcon])
		IF GET_HASH_KEY(g_sMenuData.sIconTextureOverride[paramIcon]) = HASH("CREW_LOGO")
			TEXT_LABEL_63 outTXDName
			GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			
			IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(gamerHandle, outTXDName)
				RETURN GET_MENU_STRING_FROM_TEXT_LABEL(outTXDName)
			ENDIF
		ELSE
			RETURN GET_MENU_STRING_FROM_TEXT_LABEL(g_sMenuData.sIconTextureOverride[paramIcon])
		ENDIF
	ENDIF
	
	SWITCH paramIcon 
		CASE MENU_ICON_CROWN		sTexture[0] = "MP_hostCrown"			sTexture[1] = "MP_hostCrown"			BREAK
		CASE MENU_ICON_BLADE		sTexture[0] = "MP_SpecItem_Coke"		sTexture[1] = "MP_SpecItem_Coke"		BREAK
		CASE MENU_ICON_SYRINGE		sTexture[0] = "MP_SpecItem_Heroin"		sTexture[1] = "MP_SpecItem_Heroin"		BREAK
		CASE MENU_ICON_WEED			sTexture[0] = "MP_SpecItem_Weed"		sTexture[1] = "MP_SpecItem_Weed"		BREAK
		CASE MENU_ICON_METH			sTexture[0] = "MP_SpecItem_Meth"		sTexture[1] = "MP_SpecItem_Meth"		BREAK
		CASE MENU_ICON_CASH			sTexture[0] = "MP_SpecItem_Cash"		sTexture[1] = "MP_SpecItem_Cash"		BREAK
		CASE MENU_ICON_STAR			sTexture[0] = "shop_NEW_Star"			sTexture[1] = "shop_NEW_Star"			BREAK
		CASE MENU_ICON_LEFT_STAR	sTexture[0] = "shop_NEW_Star"			sTexture[1] = "shop_NEW_Star"			BREAK
		CASE MENU_ICON_TICK			sTexture[0] = "Shop_Tick_Icon"			sTexture[1] = "Shop_Tick_Icon"			BREAK
		CASE MENU_ICON_BOX_CROSS	sTexture[0] = "Shop_Box_CrossB"			sTexture[1] = "Shop_Box_Cross"			BREAK
		CASE MENU_ICON_BOX_EMPTY	sTexture[0] = "Shop_Box_BlankB"			sTexture[1] = "Shop_Box_Blank"			BREAK
		CASE MENU_ICON_BOX_TICK		sTexture[0] = "Shop_Box_TickB"			sTexture[1] = "Shop_Box_Tick"			BREAK
		CASE MENU_ICON_COLOUR		sTexture[0] = "shop_NEW_Star"			sTexture[1] = "shop_NEW_Star"			BREAK
		CASE MENU_ICON_TSHIRT		sTexture[0] = "Shop_Clothing_Icon_B"	sTexture[1] = "Shop_Clothing_Icon_A"	BREAK
		CASE MENU_ICON_GUN			sTexture[0] = "Shop_GunClub_Icon_B"		sTexture[1] = "Shop_GunClub_Icon_A"		BREAK
		CASE MENU_ICON_AMMO			sTexture[0] = "Shop_Ammo_Icon_B"		sTexture[1] = "Shop_Ammo_Icon_A"		BREAK
		CASE MENU_ICON_ARMOUR		sTexture[0] = "Shop_Armour_Icon_B"		sTexture[1] = "Shop_Armour_Icon_A"		BREAK
		CASE MENU_ICON_HEALTH		sTexture[0] = "Shop_Health_Icon_B"		sTexture[1] = "Shop_Health_Icon_A"		BREAK
		CASE MENU_ICON_MAKEUP		sTexture[0] = "Shop_MakeUp_Icon_B"		sTexture[1] = "Shop_MakeUp_Icon_A"		BREAK
		CASE MENU_ICON_INK			sTexture[0] = "Shop_Tattoos_Icon_B"		sTexture[1] = "Shop_Tattoos_Icon_A"		BREAK
		CASE MENU_ICON_CAR			sTexture[0] = "Shop_Garage_Icon_B"		sTexture[1] = "Shop_Garage_Icon_A"		BREAK
		CASE MENU_ICON_BIKE			sTexture[0] = "Shop_Garage_Bike_Icon_B"	sTexture[1] = "Shop_Garage_Bike_Icon_A"	BREAK
		CASE MENU_ICON_SCISSORS		sTexture[0] = "Shop_Barber_Icon_B"		sTexture[1] = "Shop_Barber_Icon_A"		BREAK
		CASE MENU_ICON_LOCK			sTexture[0] = "shop_Lock"				sTexture[1] = "shop_Lock"				BREAK
		CASE MENU_ICON_LSC			sTexture[0] = "Shop_Tick_Icon"			sTexture[1] = "Shop_Tick_Icon"			BREAK // The LSC logo is big so we load separately and override.
		CASE MENU_ICON_ARROW_L		sTexture[0] = "arrowleft"				sTexture[1] = "arrowleft"				BREAK
		CASE MENU_ICON_ARROW_R		sTexture[0] = "arrowright"				sTexture[1] = "arrowright"				BREAK
		CASE MENU_ICON_ALERT		sTexture[0] = "MP_AlertTriangle"		sTexture[1] = "MP_AlertTriangle"		BREAK
		CASE MENU_ICON_HEADER		sTexture[0] = "shop_NEW_Star"			sTexture[1] = "shop_NEW_Star"			BREAK // Script should override this
		
		CASE MENU_ICON_M			sTexture[0] = "Shop_Michael_Icon_B"		sTexture[1] = "Shop_Michael_Icon_A"		BREAK
		CASE MENU_ICON_F			sTexture[0] = "Shop_Franklin_Icon_B"	sTexture[1] = "Shop_Franklin_Icon_A"	BREAK
		CASE MENU_ICON_T			sTexture[0] = "Shop_Trevor_Icon_B"		sTexture[1] = "Shop_Trevor_Icon_A"		BREAK
		
		CASE MENU_ICON_DISCOUNT		sTexture[0] = "SaleIcon"		sTexture[1] = "SaleIcon"		BREAK
		
		CASE MENU_ICON_YACHT		sTexture[0] = "Shop_Tick_Icon"			sTexture[1] = "Shop_Tick_Icon"		BREAK
		
		CASE MENU_ICON_STAR_FADED	sTexture[0] = "shop_NEW_Star"			sTexture[1] = "shop_NEW_Star"			BREAK
		
		CASE MENU_ICON_BIN_LOCK			sTexture[0] = "Shop_Lock_Arena"				sTexture[1] = "Shop_Lock_Arena"				BREAK
		
		CASE MENU_ICON_CLUBS		sTexture[0] = "Card_Suit_Clubs"			sTexture[1] = "Card_Suit_Clubs"			BREAK
		CASE MENU_ICON_HEARTS		sTexture[0] = "Card_Suit_Hearts"		sTexture[1] = "Card_Suit_Hearts"		BREAK
		CASE MENU_ICON_SPADES		sTexture[0] = "Card_Suit_Spades"		sTexture[1] = "Card_Suit_Spades"		BREAK
		CASE MENU_ICON_DIAMONDS		sTexture[0] = "Card_Suit_Diamonds"		sTexture[1] = "Card_Suit_Diamonds"		BREAK
		CASE MENU_ICON_DECORATION	sTexture[0] = "Shop_Art_Icon_B"			sTexture[1] = "Shop_Art_Icon_A"		BREAK
		CASE MENU_ICON_DECORATION_FADED	sTexture[0] = "Shop_Art_Icon_B"			sTexture[1] = "Shop_Art_Icon_A"		BREAK
		CASE MENU_ICON_CHIPS		sTexture[0] = "Shop_Chips_A"			sTexture[1] = "Shop_Chips_B"		BREAK
		
		CASE MENU_ICON_DUMMY		sTexture[0] = ""						sTexture[1] = ""						BREAK
	ENDSWITCH
	
	IF paramSelected
		RETURN sTexture[0]
	ELSE
		RETURN sTexture[1]
	ENDIF
ENDFUNC

/// PURPOSE: Returns the texture name for the specified icon
FUNC STRING GET_MENU_ICON_TXD(MENU_ICON_TYPE paramIcon)

	IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuData.sIconTXDOverride[paramIcon])
		IF GET_HASH_KEY(g_sMenuData.sIconTXDOverride[paramIcon]) = HASH("CREW_LOGO")
			TEXT_LABEL_63 outTXDName
			GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			NETWORK_CLAN_GET_EMBLEM_TXD_NAME(gamerHandle, outTXDName)
			RETURN GET_MENU_STRING_FROM_TEXT_LABEL(outTXDName)
		ELSE
			RETURN GET_MENU_STRING_FROM_TEXT_LABEL(g_sMenuData.sIconTXDOverride[paramIcon])
		ENDIF
	ENDIF
	
	IF paramIcon = MENU_ICON_DISCOUNT
		RETURN "MPShopSale"
	ENDIF
	
	RETURN "CommonMenu"
ENDFUNC

/// PURPOSE: Returns the rgb for menu icon
PROC GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_TYPE paramIcon, BOOL paramSelected, INT &iR, INT &iG, INT &iB, INT &iA)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	
	iA = 255
	
	SWITCH paramIcon 
		CASE MENU_ICON_ALERT
			iR = 194
			iG = 80
			iB = 80
		BREAK
		CASE MENU_ICON_LOCK
		CASE MENU_ICON_TICK
		CASE MENU_ICON_LSC
		CASE MENU_ICON_ARROW_L
		CASE MENU_ICON_ARROW_R		
		CASE MENU_ICON_SURVIVAL
		CASE MENU_ICON_MISSION
		CASE MENU_ICON_LTS
		CASE MENU_ICON_CAPTURE
		CASE MENU_ICON_BASE_JUMPING
		CASE MENU_ICON_GANG_ATTACK
		CASE MENU_ICON_RACE_LAND
		CASE MENU_ICON_RACE_STUNT
		CASE MENU_ICON_RACE_WATER
		CASE MENU_ICON_RACE_AIR
		CASE MENU_ICON_RACE_BIKE
		CASE MENU_ICON_DEATHMATCH
		CASE MENU_ICON_TEAM_DM
		CASE MENU_ICON_VEH_DM
		CASE MENU_ICON_YACHT
		CASE MENU_ICON_CLUBS
		CASE MENU_ICON_HEARTS
		CASE MENU_ICON_SPADES
		CASE MENU_ICON_DIAMONDS
			IF paramSelected
				iR = 0
				iG = 0
				iB = 0
			ENDIF
		BREAK
		
		CASE MENU_ICON_STAR_FADED
			iA = 100
		BREAK
		
		CASE MENU_ICON_DECORATION_FADED
			iA = 100
		BREAK
		
		CASE MENU_ICON_BIN_LOCK
			GET_HUD_COLOUR(HUD_COLOUR_PINK, iR, iG, iB, iA)
			iA = 255
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Returns the scale multiplier for menu icon. Defaults to 1.0
FUNC FLOAT GET_MENU_ICON_SCALE_MULTIPLIER(MENU_ICON_TYPE paramIcon)
		
	SWITCH paramIcon 	
		CASE MENU_ICON_SURVIVAL
		CASE MENU_ICON_MISSION
		CASE MENU_ICON_LTS
		CASE MENU_ICON_CAPTURE
		CASE MENU_ICON_BASE_JUMPING
		CASE MENU_ICON_GANG_ATTACK
		CASE MENU_ICON_RACE_LAND
		CASE MENU_ICON_RACE_WATER
		CASE MENU_ICON_RACE_AIR
		CASE MENU_ICON_RACE_BIKE
		CASE MENU_ICON_RACE_STUNT
		CASE MENU_ICON_DEATHMATCH
		CASE MENU_ICON_TEAM_DM
		CASE MENU_ICON_VEH_DM
			RETURN 0.85
		BREAK
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

/// PURPOSE: Returns the scale multiplier for menu icon. Defaults to 1.0
FUNC FLOAT GET_MENU_ICON_TEXTURE_SCALE(MENU_ICON_TYPE paramIcon)
		
	SWITCH paramIcon 	
		// Fix for bug # 1859693 - Reducing size as textures were doubled in size
		CASE MENU_ICON_T
		CASE MENU_ICON_TICK
		CASE MENU_ICON_INK
		CASE MENU_ICON_M
		CASE MENU_ICON_MAKEUP
		CASE MENU_ICON_LOCK
//		CASE MENU_ICON_BIN_LOCK
		CASE MENU_ICON_GUN
		CASE MENU_ICON_CAR
		CASE MENU_ICON_BIKE
		CASE MENU_ICON_F
		CASE MENU_ICON_TSHIRT
		CASE MENU_ICON_BOX_TICK
		CASE MENU_ICON_BOX_CROSS
		CASE MENU_ICON_BOX_EMPTY
		CASE MENU_ICON_SCISSORS
		CASE MENU_ICON_ARMOUR
		CASE MENU_ICON_HEALTH
		CASE MENU_ICON_AMMO
		CASE MENU_ICON_ALERT
		CASE MENU_ICON_ARROW_L
		CASE MENU_ICON_ARROW_R
//		CASE MENU_ICON_STAR
//		CASE MENU_ICON_LEFT_STAR
//		CASE MENU_ICON_DISCOUNT
		CASE MENU_ICON_YACHT
		CASE MENU_ICON_CLUBS
		CASE MENU_ICON_HEARTS
		CASE MENU_ICON_SPADES
		CASE MENU_ICON_DIAMONDS
		CASE MENU_ICON_DECORATION
		CASE MENU_ICON_DECORATION_FADED
			RETURN 0.5
		BREAK
		CASE MENU_ICON_CHIPS
			RETURN 0.8
		BREAK
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

/// PURPOSE: Is the horizontal width so much larger than the vertical that we should assume the game
///    is running a triple monitor setup?   
FUNC BOOL IS_PHYSICAL_RES_CONSIDERED_TRIPLE_HEAD(INT iScreenXPixels, INT iScreenYPixels)
	RETURN TO_FLOAT(iScreenXPixels)/TO_FLOAT(iScreenYPixels) > CUSTOM_MENU_TRIPLE_HEAD_THRESHOLD_RATIO
ENDFUNC

/// PURPOSE: Calculate the resolution that the menu should consider as the full screen. NB. This may not be the
///    actual resolution if the game is running a triple head setup where the menu should only use the center screen.
PROC GET_SCREEN_RESOLUTION_FOR_MENU(BOOL bUseActualRes, INT &iScreenXPixels, INT &iScreenYPixels, FLOAT &fAspectMultiplier)
	IF NOT bUseActualRes
		//At least in the past, this native returned a hardcoded 16:9 resolution.
		GET_SCREEN_RESOLUTION(iScreenXPixels, iScreenYPixels)
		EXIT
	ENDIF
	
	GET_ACTUAL_SCREEN_RESOLUTION(iScreenXPixels, iScreenYPixels)
	FLOAT fScreenXPixels = TO_FLOAT(iScreenXPixels)
	FLOAT fScreenYPixels = TO_FLOAT(iScreenYPixels)
	FLOAT fFakeAspect = GET_ASPECT_RATIO(FALSE)
	
	//Is the physical res considered large enough for the game to be running triple head?
	IF IS_PHYSICAL_RES_CONSIDERED_TRIPLE_HEAD(iScreenXPixels, iScreenYPixels)
		//Yes, pretend the screen width is actually the size of a standard 16:9 monitor.
		fAspectMultiplier = 1.0
		iScreenXPixels  = ROUND(fScreenYPixels * fFakeAspect)
		iScreenYPixels = ROUND(fScreenYPixels)
		EXIT
	ENDIF
	
	//No, calculate the real aspect ratio.
	fAspectMultiplier = (fScreenXPixels / fScreenYPixels) / fFakeAspect
	iScreenXPixels = ROUND(fScreenXPixels/fAspectMultiplier)
	iScreenYPixels = ROUND(fScreenYPixels/fAspectMultiplier)
ENDPROC

/// PURPOSE: Gets the width and height of a specific icon in screen space.
FUNC BOOL GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_TYPE paramIcon, BOOL paramSelected, BOOL paramForRender, FLOAT &fWidth, FLOAT &fHeight, BOOL bUseActualRes = FALSE)
	
	TEXT_LABEL_63 sTXD = GET_MENU_ICON_TXD(paramIcon)
	TEXT_LABEL_63 sTexture = GET_MENU_ICON_TEXTURE(paramIcon, paramSelected)
	
	IF GET_HASH_KEY(sTexture) != 0
		
		//	B* 2147964 - Gets the physical screen aspect and compares to fake aspect ratio to get a multiplier
		INT xScreen, yScreen
		FLOAT fAspectMulti = 1.0
		GET_SCREEN_RESOLUTION_FOR_MENU(bUseActualRes, xScreen, yScreen, fAspectMulti)
		
		VECTOR vTexture = GET_TEXTURE_RESOLUTION(sTXD, sTexture)
		FLOAT fTextureResScale = GET_MENU_ICON_TEXTURE_SCALE(paramIcon)/fAspectMulti
		vTexture *= fTextureResScale

		// All dds files have a border around them so subtract
		IF NOT paramForRender
			vTexture.X -= 2.0
			vTexture.Y -= 2.0
		ENDIF
		
		IF paramIcon = MENU_ICON_DLC_IMAGE
			vTexture.X = 288 
			vTexture.Y = 106
		ENDIF
		
		IF paramIcon = MENU_ICON_HEADER
		AND GET_HASH_KEY(g_sMenuData.sIconTextureOverride[MENU_ICON_HEADER]) = HASH("CREW_LOGO")
			vTexture.X = 106
			vTexture.Y = 106
		ENDIF
		
		fWidth = (vTexture.X/xScreen) * (xScreen/yScreen)
		fHeight = (vTexture.Y/yScreen) / (vTexture.X/xScreen) * fWidth
		
		IF NOT bUseActualRes
			IF NOT GET_IS_WIDESCREEN()
			AND paramIcon != MENU_ICON_DLC_IMAGE
				fWidth *= 1.33
			ENDIF
		ENDIF
		
		// Some textures have been upscaled so we need to ensure they do not exceed the menu width.
		IF paramIcon = MENU_ICON_HEADER
			IF fWidth > CUSTOM_MENU_W	
				// Make sure we scale the height to match this..
				fHeight *= (CUSTOM_MENU_W / fWidth)
				fWidth = CUSTOM_MENU_W
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the appropriate text states for heading text
PROC SETUP_MENU_HEADING_TEXT()
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	
	IF g_sMenuData.bUseCustomHeaderTextColour
		iR = g_sMenuData.iHeaderTextR
		iG = g_sMenuData.iHeaderTextG
		iB = g_sMenuData.iHeaderTextB
		iA = g_sMenuData.iHeaderTextA
	ENDIF
	
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
    SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_WRAP(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, CUSTOM_MENU_X+CUSTOM_MENU_W-CUSTOM_MENU_TEXT_INDENT_X)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC

PROC SETUP_MENU_CUSTOM_ITEM_TEXT_COLOUR(BOOL bSelected)
	INT iR, iG, iB, iA
	IF bSelected
		GET_HUD_COLOUR(g_sMenuData.eRowColour[0], iR, iG, iB, iA)
	ELSE
		GET_HUD_COLOUR(g_sMenuData.eRowColour[1], iR, iG, iB, iA)
	ENDIF
	SET_TEXT_COLOUR(iR, iG, iB, 255)
ENDPROC

/// PURPOSE: Sets the appropriate text states for item text
PROC SETUP_MENU_ITEM_TEXT(BOOL bSelected, BOOL bIsSelectable = TRUE, BOOL bIsDefault = FALSE, BOOL bSetCarColour = FALSE, INT iArray = 0, BOOL bPlayerNameFont = FALSE, BOOL bCondensedFont = FALSE)
	INT iR, iG, iB, iA
	IF bIsDefault
		IF bSetCarColour
			GET_VEHICLE_RGB_FROM_INT(g_sMenuData.iCarColour[iArray], iR, iG, iB)
			IF iR   < 20
			AND iG  < 20
			AND iB  < 20
				IF bSelected = FALSE
					GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
				ENDIF
			ELIF iR > 230
			AND iG > 230
			AND iB > 230
				IF bSelected
					iR = 0
					iG = 0
					iB = 0
				ENDIF
			ENDIF
			SET_TEXT_COLOUR(iR, iG, iB, 255)
		ELIF bIsSelectable
			IF bSelected
				GET_HUD_COLOUR(HUD_COLOUR_YELLOWDARK, iR, iG, iB, iA)
				SET_TEXT_COLOUR(iR, iG, iB, 255)
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
				SET_TEXT_COLOUR(iR, iG, iB, 255)
			ENDIF
		ELSE	
			IF bSelected
				SET_TEXT_COLOUR(155, 155, 155, 255)
			ELSE
				SET_TEXT_COLOUR(155, 155, 155, 255)
			ENDIF
		ENDIF	
	ELSE
		IF bIsSelectable
			IF bSelected
				SET_TEXT_COLOUR(0, 0 ,0 , FLOOR(255*0.8)) // 
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
				SET_TEXT_COLOUR(iR, iG, iB, iA)
			ENDIF
		ELSE	
			IF bSelected
				SET_TEXT_COLOUR(155, 155, 155, 255)
			ELSE
				SET_TEXT_COLOUR(155, 155, 155, 255)
			ENDIF
		ENDIF		
	ENDIF		
	SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	
	IF bPlayerNameFont
		SET_TEXT_SCALE(0.0000, CUSTOM_MENU_CONDENSED_TEXT_SCALE_Y)
		SET_TEXT_FONT(FONT_CONDENSED)
	ELIF bCondensedFont
		SET_TEXT_SCALE(0.0000, CUSTOM_MENU_CONDENSED_TEXT_SCALE_Y)
		SET_TEXT_FONT(FONT_CONDENSED_NOT_GAMERNAME)
	ELSE
		SET_TEXT_FONT(FONT_STANDARD)
	ENDIF
	
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC

/// PURPOSE: Sets the appropriate text states for item text
PROC SETUP_MENU_ITEM_MESSAGE_TEXT(FLOAT fXWrapStart)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
	SET_TEXT_LEADING(2)
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_WRAP(fXWrapStart, CUSTOM_MENU_X+CUSTOM_MENU_W-CUSTOM_MENU_TEXT_INDENT_X)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC

/// PURPOSE: Sets the appropriate text states for item text
PROC SETUP_MENU_HELP_KEY_TEXT(FLOAT fWrapStartX = -1.0, FLOAT fWrapEndX = -1.0)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_SCALE(0.0000, 0.315)
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	IF fWrapStartX != -1.0
	OR fWrapEndX != -1.0
		SET_TEXT_WRAP(fWrapStartX, fWrapEndX)
	ELSE
		SET_TEXT_WRAP(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, CUSTOM_MENU_X+CUSTOM_MENU_W-CUSTOM_MENU_TEXT_INDENT_X)
	ENDIF
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC

FUNC FLOAT GET_MENU_HEADING_TEXT_HEIGHT()
	IF NOT DOES_TEXT_LABEL_EXIST(g_sMenuData.tl15Title)
		RETURN GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)
	ENDIF
	
	FLOAT fHeaderGraphicHeight = 0.0
	FLOAT fTempWidth, fTempHeight
	IF g_sMenuData.bUseHeaderGraphic
	AND GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_HEADER, TRUE, TRUE, fTempWidth, fTempHeight)
		fHeaderGraphicHeight = fTempHeight
	ENDIF
	
	INT iIntParam, iFloatParam, iTextParam, iPlayerNameParam, i
	SETUP_MENU_HEADING_TEXT()
	BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(g_sMenuData.tl15Title)
		REPEAT MAX_STORED_TEXT_COMPS i
			IF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_INT
				ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iTitleInt[iIntParam])
				iIntParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_FLOAT
				ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fTitleFloat[iFloatParam], g_sMenuData.iTitleFloatDP[iFloatParam])
				iFloatParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_TEXT
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15TitleText[iTextParam])
				iTextParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_VEHICLE_NAME
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15TitleText[iTextParam])
				iTextParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_PLAYER_NAME
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_RADIO_STATION
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_LITERAL
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ENDIF
		ENDREPEAT
	RETURN (GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, CUSTOM_MENU_Y+fHeaderGraphicHeight+0.008))
ENDFUNC

FUNC FLOAT GET_MENU_HEADING_TEXT_WIDTH()
	IF NOT DOES_TEXT_LABEL_EXIST(g_sMenuData.tl15Title)
		RETURN 0.0
	ENDIF
	FLOAT fWidth
	INT iIntParam, iFloatParam, iTextParam, iPlayerNameParam, i
	SETUP_MENU_HEADING_TEXT()
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(g_sMenuData.tl15Title)
		REPEAT MAX_STORED_TEXT_COMPS i
			IF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_INT
				ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iTitleInt[iIntParam])
				iIntParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_FLOAT
				ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fTitleFloat[iFloatParam], g_sMenuData.iTitleFloatDP[iFloatParam])
				iFloatParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_TEXT
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15TitleText[iTextParam])
				iTextParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_VEHICLE_NAME
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15TitleText[iTextParam])
				iTextParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_PLAYER_NAME
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_RADIO_STATION
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_LITERAL
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
				iPlayerNameParam++
			ENDIF
		ENDREPEAT
	fWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
	
	// Return the largest value
	IF g_sMenuData.fTitleWidth > fWidth
		RETURN g_sMenuData.fTitleWidth
	ENDIF
	
	RETURN fWidth
ENDFUNC
FUNC FLOAT GET_MENU_ITEM_COUNT_WIDTH(STRING sLabel, INT iCurrentItem, INT iTotalItems)
	IF NOT IS_STRING_NULL(sLabel)
		IF GET_HASH_KEY(sLabel) = 0
			RETURN 0.0
		ENDIF
	ELSE
		RETURN 0.0
	ENDIF
	SETUP_MENU_HEADING_TEXT()
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(sLabel)
		ADD_TEXT_COMPONENT_INTEGER(iCurrentItem)
		ADD_TEXT_COMPONENT_INTEGER(iTotalItems)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
ENDFUNC
FUNC FLOAT GET_MENU_ITEM_TEXT_WIDTH(STRING sLabel)
	IF NOT IS_STRING_NULL(sLabel)
		IF GET_HASH_KEY(sLabel) = 0
			RETURN 0.0
		ENDIF
	ELSE
		RETURN 0.0
	ENDIF
	SETUP_MENU_ITEM_TEXT(FALSE)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(sLabel)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
ENDFUNC
FUNC FLOAT GET_LAST_MENU_ITEM_TEXT_WIDTH()
	FLOAT fTotalWidth, fWidth, fHeight
	INT iIntCount, iFloatCount, iIconCount, iPlayerNameCount, i
	
	REPEAT g_sMenuData.iComponentCount i
		IF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_TEXT
			// Use the g_sMenuData.iLastTextItem int for array offset
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME
			// Use the g_sMenuData.iLastTextItem int for array offset
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_INT
			iIntCount++
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_FLOAT
			iFloatCount++
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_ICON
			iIconCount++
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
			iPlayerNameCount++
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_RADIO_STATION
			iPlayerNameCount++
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_LITERAL
			iPlayerNameCount++
		ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
			iPlayerNameCount++
		ENDIF
	ENDREPEAT
	
	SETUP_MENU_ITEM_TEXT(FALSE, TRUE, FALSE, FALSE, 0, (iPlayerNameCount > 0))
	IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
		BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem])
	ENDIF
		INT iCurrentText, iCurrentInt, iCurrentFloat, iCurrentPlayerName
		REPEAT g_sMenuData.iComponentCount i
			IF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_TEXT
				iCurrentText++
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem+iCurrentText])
				ENDIF
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME
				iCurrentText++
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem+iCurrentText])
				ENDIF
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_INT
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iItem[g_sMenuData.iCurrentIntItem-iIntCount+iCurrentInt])
				ENDIF
				iCurrentInt++
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_FLOAT
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fItem[g_sMenuData.iCurrentFloatItem-iFloatCount+iCurrentFloat], g_sMenuData.iFloatDP[g_sMenuData.iCurrentFloatItem-iFloatCount+iCurrentFloat])
				ENDIF
				iCurrentFloat++
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-iPlayerNameCount+iCurrentPlayerName])
				ENDIF
				iCurrentPlayerName++
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_RADIO_STATION
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-iPlayerNameCount+iCurrentPlayerName])
				ENDIF
				iCurrentPlayerName++
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_LITERAL
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-iPlayerNameCount+iCurrentPlayerName])
				ENDIF
				iCurrentPlayerName++
			ELIF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
				IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-iPlayerNameCount+iCurrentPlayerName])
				ENDIF
				iCurrentPlayerName++
			ENDIF
		ENDREPEAT
	IF GET_HASH_KEY(g_sMenuData.tl15Item[g_sMenuData.iLastTextItem]) != 0
		fTotalWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
	ENDIF
	
	REPEAT iIconCount i
		IF g_sMenuData.eIconItem[g_sMenuData.iCurrentIconItem-iIconCount+i] <> MENU_ICON_DUMMY
			GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[g_sMenuData.iCurrentIconItem-iIconCount+i], TRUE, FALSE, fWidth, fHeight)
			fTotalWidth += fWidth
		ENDIF
	ENDREPEAT
	
	RETURN fTotalWidth
ENDFUNC

FUNC FLOAT GET_MENU_ITEM_TEXT_WIDTH_WITH_INT(STRING sLabel, INT iNumber)
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
		RETURN 0.0
	ENDIF
	SETUP_MENU_ITEM_TEXT(TRUE)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(sLabel)
		ADD_TEXT_COMPONENT_INTEGER(iNumber)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
ENDFUNC
FUNC FLOAT GET_MENU_ITEM_TEXT_WIDTH_WITH_FLOAT(STRING sLabel, FLOAT fNumber, INT iDecimalPlaces)
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
		RETURN 0.0
	ENDIF
	SETUP_MENU_ITEM_TEXT(TRUE)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(sLabel)
		ADD_TEXT_COMPONENT_FLOAT(fNumber, iDecimalPlaces)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
ENDFUNC
FUNC FLOAT GET_MENU_ITEM_TEXT_WIDTH_WITH_LITERAL(STRING sLabel, STRING sLiteral, BOOL bCondensedFont = FALSE)
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
		RETURN 0.0
	ENDIF
	SETUP_MENU_ITEM_TEXT(TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bCondensedFont)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(sLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sLiteral)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
ENDFUNC

FUNC FLOAT GET_MENU_ITEM_TEXT_HEIGHT(STRING sLabel)
	// At some point we may add multiply this value by the number of lines that the string occupies
	// We would need to add x/y screen and wrap coords.

	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
	ENDIF
	RETURN GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)
ENDFUNC
FUNC FLOAT GET_LAST_MENU_ITEM_TEXT_HEIGHT()
	// At some point we may add multiply this value by the number of lines that the string occupies
	// We would need to add x/y screen and wrap coords.
	
	INT i, iIconCount
	FLOAT fFinalHeight, fWidth, fHeight
	
	REPEAT g_sMenuData.iComponentCount i
		IF g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][i] = MENU_TEXT_COMP_ICON
			iIconCount++
		ENDIF
	ENDREPEAT
	REPEAT iIconCount i
		IF g_sMenuData.eIconItem[g_sMenuData.iCurrentIconItem-iIconCount+i] <> MENU_ICON_DUMMY
			IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[g_sMenuData.iCurrentIconItem-iIconCount+i], TRUE, FALSE, fWidth, fHeight)
				IF fHeight > fFinalHeight
					fFinalHeight = fHeight
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF fFinalHeight > GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)
		RETURN fFinalHeight
	ENDIF
	
	RETURN GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)
ENDFUNC
FUNC FLOAT GET_MENU_ITEM_TEXT_HEIGHT_WITH_INT(STRING sLabel, INT iNumber)
	// At some point we may add multiply this value by the number of lines that the string occupies
	// We would need to add x/y screen and wrap coords.
	
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
	ENDIF
	IF iNumber = 0
	ENDIF
	RETURN GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)
ENDFUNC
FUNC FLOAT GET_MENU_ITEM_TEXT_HEIGHT_WITH_FLOAT(STRING sLabel, FLOAT fNumber, INT iDecimalPlaces)
	// At some point we may add multiply this value by the number of lines that the string occupies
	// We would need to add x/y screen and wrap coords.
	
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
	ENDIF
	IF fNumber = 0
	ENDIF
	IF iDecimalPlaces = 0
	ENDIF
	RETURN GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)
ENDFUNC

/// PURPOSE: Resets all the stored menu data back to the default state
PROC CLEAR_MENU_DATA(BOOL bUseActualRes = FALSE, BOOL bClearSelectable = FALSE)
	DEBUG_PRINTCALLSTACK()
	INT i, j
	REPEAT MAX_STORED_MENU_TEXT_LABELS i
		g_sMenuData.tl15Item[i] = ""
		REPEAT MAX_STORED_TEXT_COMPS j
			g_sMenuData.eTextItemComps[i][j] = MENU_TEXT_COMP_DUMMY
		ENDREPEAT		
	ENDREPEAT
	REPEAT MAX_STORED_MENU_PLAYER_NAMES i
		g_TUMenuGlobals_tlPlayerNameItem[i] = ""
	ENDREPEAT
	REPEAT MAX_STORED_MENU_INTS i
		g_sMenuData.iItem[i] = 0
	ENDREPEAT
	REPEAT MAX_STORED_MENU_FLOATS i
		g_sMenuData.fItem[i] = 0.0
	ENDREPEAT
	REPEAT MAX_STORED_MENU_ICONS i
		g_sMenuData.eIconItem[i] = MENU_ICON_DUMMY
	ENDREPEAT
	REPEAT MAX_MENU_ROWS i
		g_sMenuData.iItemBitset[i] = 0
		g_sMenuData.bMenuRowDoesntAddToCount[i] = FALSE
		g_sMenuData.bMenuRowHasSpacer[i] = FALSE
		g_sMenuData.fMenuItemScreenY[i] = 0.0
		g_sMenuData.bMenuRowHasDisplayItems[i] = FALSE
		g_sMenuData.fRowHeight[i] = 0.0
	ENDREPEAT
	REPEAT MAX_MENU_COLUMNS i
		g_sMenuData.eItemLayout[i] = MENU_ITEM_DUMMY
		g_sMenuData.fColumnWidth[i] = 0.0
		g_sMenuData.fColumnXOffset[i] = -1.0
		g_sMenuData.bItemToggleable[i] = FALSE
		g_sMenuData.eJustification[i] = FONT_LEFT
	ENDREPEAT
	REPEAT MAX_STORED_HELP_KEYS i
		g_sMenuData.tl15HelpText[i] = ""
		g_sMenuData.iHelpTextINT[i] = -1
		g_sMenuData.caHelpTextInput[i] = MAX_INPUTS
		g_sMenuData.caHelpTextInputGroup[i] = MAX_INPUTGROUPS
	ENDREPEAT
	REPEAT CUSTOM_MENU_ICON_COUNT i
		g_sMenuData.sIconTXDOverride[i] = ""
		g_sMenuData.sIconTextureOverride[i] = ""
	ENDREPEAT
	IF bClearSelectable
		REPEAT MAX_STORED_MENU_TEXT_LABELS i
			g_sMenuData.bIsSelectable[i] = FALSE
		ENDREPEAT
	ENDIF
	#IF USE_TU_CHANGES
		// HELP KEY OVERFLOW
		g_sMenuData_TU.tl15HelpText = ""
		g_sMenuData_TU.iHelpTextINT = -1
	#ENDIF
	
	g_sMenuData.bUseHeaderGraphic 	= 	FALSE
//	g_sMenuData.bForceFooter		=	FALSE
	
	g_sMenuData.iCurrentRow = 0
	g_sMenuData.iCurrentColumn = 0
	g_sMenuData.iCurrentTextItem = 0
	g_sMenuData.iCurrentIntItem = 0
	g_sMenuData.iCurrentFloatItem = 0
	g_sMenuData.iCurrentIconItem = 0
	g_sMenuData.iCurrentPlayerNameItem = 0
	g_sMenuData.bDisplayCurrentItemToggles = FALSE
	g_sMenuData.bDefaultOptionAdded = FALSE
	
	g_sMenuData.iCurrentItem = 0
	g_sMenuData.iTopItem = 0
	g_sMenuData.iLastDisplayItem = 0
	
	g_sMenuData.tl23Desc = ""
	g_sMenuData.iDescTotalParams = 0
	g_sMenuData.iDescIntParams = 0
	g_sMenuData.iDescFloatParams = 0
	g_sMenuData.iDescTextParams = 0
	g_sMenuData.iDescClearTimer = 0
	g_sMenuData.iDescStartTimer = 0
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
	g_sMenuData.eDescIcon = MENU_ICON_DUMMY
	
	g_sMenuData_TU.tl15Discount = ""
	g_sMenuData_TU.iDiscountTotalParams = 0
	g_sMenuData_TU.iDiscountIntParams = 0
	g_sMenuData_TU.iDiscountFloatParams = 0
	g_sMenuData_TU.iDiscountTextParams = 0
	g_sMenuData_TU.iDiscountClearTimer = 0
	g_sMenuData_TU.iDiscountStartTimer = 0
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
	g_sMenuData_TU.eDiscountIcon = MENU_ICON_DUMMY
	
	
	g_sMenuData.tl15Title = ""
	g_sMenuData.fTitleWidth = 0.0
	g_sMenuData.iTitleTotalParams = 0
	g_sMenuData.iTitleIntParams = 0
	g_sMenuData.iTitleFloatParams = 0
	g_sMenuData.iTitleTextParams = 0
	g_sMenuData.iTitlePlayerNameParams = 0
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
	
	g_sMenuData.eLastAddedItemType = MENU_ITEM_DUMMY
	g_sMenuData.iComponentCount = 0
	g_sMenuData.iComponentItems = 0
	g_sMenuData.iLastTextItem = 0
	g_sMenuData.iHelpCount = 0
	g_sMenuData.bHelpCreated = FALSE
	
	g_sMenuData.iMenuRows = 10
	g_sMenuData.iMenuColumns = 0
	
	g_sMenuData.fMenuFinalScreenY = 0.0
	g_sMenuData.fCurrentItemScreenY = 0.0
	
	g_sMenuData.bSetupComplete = FALSE
	g_sMenuData.bDisplayRowsDefined = FALSE
	g_sMenuData.fSetupFinalBodyY = 0.0
	g_sMenuData.iSetupTotalRows = 0
	g_sMenuData.iSetupTotalSelectableRows = 0
	g_sMenuData.iSetupTotalDisplayRows = 0
	g_sMenuData.iSetupCurrentSelectableItem = 0
	
	g_sMenuData.bOverrideTitleRowCounts = FALSE
	g_sMenuData.iTitleRowOverride1 = 0
	g_sMenuData.iTitleRowOverride2 = 0
	
	g_sMenuData.bUseCustomRowColour = FALSE
	
	REPEAT MAX_COLOURED_ITEMS i	
		g_sMenuData.iCarColourItem[i] = -1
		g_sMenuData.iCarColour[i] = -1
	ENDREPEAT
	g_sMenuData.fHelpKeyClearSpace = 0.0
	g_sMenuData.iHelpKeyIsClickableBits = 0
	g_sMenuData.bStackedKeys = FALSE
	
	REPEAT COUNT_OF(g_sMenuData.iRowIsGrayedOutBS) i
		g_sMenuData.iRowIsGrayedOutBS[i] = 0
	ENDREPEAT
	
	g_sMenuData.bUseCustomHeaderTextColour = FALSE
	g_sMenuData.bUseCustomHeaderColour = FALSE
	g_sMenuData.bUseCustomBodyColour = FALSE
	g_sMenuData.bUseCustomFooterColour = FALSE
	g_sMenuData.bUseCustomHelpColour = FALSE
	g_sMenuData.bUseCustomSelectionBarColour = FALSE
	g_sMenuData.bUseInvertedScrollColour = FALSE
	
	CUSTOM_MENU_X = 0.05
	CUSTOM_MENU_Y = 0.05
	CUSTOM_MENU_W = 0.225
	
	// Force the menu width to match the current screen size.
	FLOAT fScreenAspectRatio = GET_ASPECT_RATIO(FALSE)
	IF bUseActualRes
		//	Scales the menu width depending on the aspect ratio (fake) and normalises against a 16:9 ratio
		CUSTOM_MENU_W = ( 0.225 * ( CUSTOM_MENU_SIXTEEN_BY_NINE_ASPECT / fScreenAspectRatio ) )
	ELSE
		IF fScreenAspectRatio < 1.77777
			CUSTOM_MENU_W = ( 0.225 * ( CUSTOM_MENU_SIXTEEN_BY_NINE_ASPECT / fScreenAspectRatio ) )
		ELSE
			CUSTOM_MENU_W = 0.225
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Define the item layout for a menu row
PROC SET_MENU_ITEM_LAYOUT(MENU_ITEM_TYPE param1 = MENU_ITEM_DUMMY, MENU_ITEM_TYPE param2 = MENU_ITEM_DUMMY, MENU_ITEM_TYPE param3 = MENU_ITEM_DUMMY, MENU_ITEM_TYPE param4 = MENU_ITEM_DUMMY, MENU_ITEM_TYPE param5 = MENU_ITEM_DUMMY)
	g_sMenuData.eItemLayout[0] = param1
	g_sMenuData.eItemLayout[1] = param2
	g_sMenuData.eItemLayout[2] = param3
	g_sMenuData.eItemLayout[3] = param4
	g_sMenuData.eItemLayout[4] = param5
	
	g_sMenuData.iMenuColumns = 0
	IF param1 != MENU_ITEM_DUMMY	g_sMenuData.iMenuColumns++	ENDIF
	IF param2 != MENU_ITEM_DUMMY	g_sMenuData.iMenuColumns++	ENDIF
	IF param3 != MENU_ITEM_DUMMY	g_sMenuData.iMenuColumns++	ENDIF
	IF param4 != MENU_ITEM_DUMMY	g_sMenuData.iMenuColumns++	ENDIF
	IF param5 != MENU_ITEM_DUMMY	g_sMenuData.iMenuColumns++	ENDIF
ENDPROC

/// PURPOSE: Sets the x offset for the given menu item
///    NOTE: This offset is from the x pos of the menu, not the previous column
PROC SET_MENU_ITEM_X_OFFSET(FLOAT param1 = -1.0, FLOAT param2 = -1.0, FLOAT param3 = -1.0, FLOAT param4 = -1.0, FLOAT param5 = -1.0)
	g_sMenuData.fColumnXOffset[0] = param1
	g_sMenuData.fColumnXOffset[1] = param2
	g_sMenuData.fColumnXOffset[2] = param3
	g_sMenuData.fColumnXOffset[3] = param4
	g_sMenuData.fColumnXOffset[4] = param5
ENDPROC

/// PURPOSE: Sets the cell alignment of a given menu item
PROC SET_MENU_ITEM_JUSTIFICATION(eTextJustification param1 = FONT_LEFT, eTextJustification param2 = FONT_LEFT, eTextJustification param3 = FONT_LEFT, eTextJustification param4 = FONT_LEFT, eTextJustification param5 = FONT_LEFT)
	g_sMenuData.eJustification[0] = param1
	g_sMenuData.eJustification[1] = param2
	g_sMenuData.eJustification[2] = param3
	g_sMenuData.eJustification[3] = param4
	g_sMenuData.eJustification[4] = param5
ENDPROC

/// PURPOSE: Sets the width of a given menu item
PROC SET_MENU_ITEM_TOGGLEABLE(BOOL param1 = FALSE, BOOL param2 = FALSE, BOOL param3 = FALSE, BOOL param4 = FALSE, BOOL param5 = FALSE)
	g_sMenuData.bItemToggleable[0] = param1
	g_sMenuData.bItemToggleable[1] = param2
	g_sMenuData.bItemToggleable[2] = param3
	g_sMenuData.bItemToggleable[3] = param4
	g_sMenuData.bItemToggleable[4] = param5
ENDPROC

/// PURPOSE: Sets the number of rows in the menu
///    NOTE: This will get set to 10 when CLEAR_MENU_DATA() is called
PROC SET_MENU_ROW_ADDS_TO_COUNT(INT paramRow, BOOL paramAddToCount)
	g_sMenuData.bMenuRowDoesntAddToCount[paramRow] = !paramAddToCount
ENDPROC

/// PURPOSE: Sets the number of rows in the menu
///    NOTE: This will get set to 10 when CLEAR_MENU_DATA() is called
PROC SET_MAX_MENU_ROWS_TO_DISPLAY(INT paramMaxRowsToDisplay)
	g_sMenuData.iMenuRows = paramMaxRowsToDisplay
ENDPROC

/// PURPOSE: Gets the number of rows in the menu
FUNC INT GET_MAX_MENU_ROWS_TO_DISPLAY()
	RETURN g_sMenuData.iMenuRows
ENDFUNC

/// PURPOSE: Sets the x pos of the menu, default is 0.05
PROC SET_MENU_X_POS(FLOAT paramMenuXPos)
	CUSTOM_MENU_X = paramMenuXPos
ENDPROC

/// PURPOSE: Sets the clear space for menu help keys
PROC SET_MENU_HELP_CLEAR_SPACE(FLOAT fScreenPercent)
	g_sMenuData.fHelpKeyClearSpace = fScreenPercent
ENDPROC

/// PURPOSE: Sets if the help keys should be stacked
PROC SET_MENU_HELP_KEYS_STACKED(BOOL bStacked)
	g_sMenuData.bStackedKeys = bStacked
ENDPROC

PROC SET_MENU_ITEM_ICON_TEXTURE_OVERRIDE(MENU_ICON_TYPE paramIcon, STRING paramTXDOverride, STRING paramTextureOverride)
	g_sMenuData.sIconTXDOverride[paramIcon] = paramTXDOverride
	g_sMenuData.sIconTextureOverride[paramIcon] = paramTextureOverride
ENDPROC

PROC SET_MENU_HEADER_COLOUR(INT iRed, INT iGreen, INT iBlue, INT iAlpha, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomHeaderColour = bUseCustomColour
	g_sMenuData.iHeaderR = iRed
	g_sMenuData.iHeaderG = iGreen
	g_sMenuData.iHeaderB = iBlue
	g_sMenuData.iHeaderA = iAlpha
ENDPROC

PROC SET_MENU_HEADER_TEXT_COLOUR(INT iRed, INT iGreen, INT iBlue, INT iAlpha, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomHeaderTextColour = bUseCustomColour
	g_sMenuData.iHeaderTextR = iRed
	g_sMenuData.iHeaderTextG = iGreen
	g_sMenuData.iHeaderTextB = iBlue
	g_sMenuData.iHeaderTextA = iAlpha
ENDPROC

PROC SET_MENU_BODY_COLOUR(INT iRed, INT iGreen, INT iBlue, INT iAlpha, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomBodyColour = bUseCustomColour
	g_sMenuData.iBodyR = iRed
	g_sMenuData.iBodyG = iGreen
	g_sMenuData.iBodyB = iBlue
	g_sMenuData.iBodyA = iAlpha
ENDPROC

PROC SET_MENU_FOOTER_COLOUR(INT iRed, INT iGreen, INT iBlue, INT iAlpha, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomFooterColour = bUseCustomColour
	g_sMenuData.iFooterR = iRed
	g_sMenuData.iFooterG = iGreen
	g_sMenuData.iFooterB = iBlue
	g_sMenuData.iFooterA = iAlpha
ENDPROC

PROC SET_MENU_HELP_COLOUR(INT iRed, INT iGreen, INT iBlue, INT iAlpha, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomHelpColour = bUseCustomColour
	g_sMenuData.iHelpR = iRed
	g_sMenuData.iHelpG = iGreen
	g_sMenuData.iHelpB = iBlue
	g_sMenuData.iHelpA = iAlpha
ENDPROC

PROC SET_MENU_SELECTION_BAR_COLOUR(HUD_COLOURS paramColour, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomSelectionBarColour = bUseCustomColour
	g_sMenuData.eSelectionBarColour = paramColour
ENDPROC

PROC SET_MENU_ROW_TEXT_COLOUR(INT paramRow, HUD_COLOURS paramColourDefault, HUD_COLOURS paramColourSelected, BOOL bUseCustomColour)
	g_sMenuData.bUseCustomRowColour = bUseCustomColour
	g_sMenuData.eRowColour[0] = paramColourDefault
	g_sMenuData.eRowColour[1] = paramColourSelected
	g_sMenuData.iCustomRowColour = paramRow
ENDPROC

PROC SET_MENU_USES_INVERTED_SCROLL_ICON(BOOL bUseInvertedScrollColour)
	g_sMenuData.bUseInvertedScrollColour = bUseInvertedScrollColour
ENDPROC

PROC SET_CURSOR_POSITION_FOR_MENU()

	IF IS_PC_VERSION()
		SET_CURSOR_POSITION(0.325, 0.3)//0.2)
//		SET_CURSOR_POSITION(CUSTOM_MENU_X+(CUSTOM_MENU_W/2.0), CUSTOM_MENU_Y+(CUSTOM_MENU_ITEM_BAR_H*TO_FLOAT(iStartItem)))
	ENDIF

ENDPROC


//////////////////////////////////////////////////////////////////////////////
///    
///    HEADER
///    

/// PURPOSE: Set the header graphic
PROC SET_MENU_USES_HEADER_GRAPHIC(BOOL paramUseGraphic, STRING paramGraphicTXD, STRING paramGraphicTexture)
	g_sMenuData.bUseHeaderGraphic = paramUseGraphic
	SET_MENU_ITEM_ICON_TEXTURE_OVERRIDE(MENU_ICON_HEADER, paramGraphicTXD, paramGraphicTexture)
ENDPROC


//////////////////////////////////////////////////////////////////////////////
///    
///    TITLE
///    

/// PURPOSE: Set the menu title
PROC SET_MENU_TITLE(STRING paramLabel)
	g_sMenuData.tl15Title = paramLabel
	g_sMenuData.iTitleTotalParams = 0
	g_sMenuData.iTitleIntParams = 0
	g_sMenuData.iTitleFloatParams = 0
	g_sMenuData.iTitleTextParams = 0
	g_sMenuData.iTitlePlayerNameParams = 0
	
	INT i
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
ENDPROC
/// PURPOSE: Adds an int to the item description
PROC ADD_MENU_TITLE_INT(INT paramInt)
	IF g_sMenuData.iTitleIntParams >= MAX_STORED_DESC_INTS
	OR g_sMenuData.iTitleTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_TITLE_INT: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eTitleComps[g_sMenuData.iTitleTotalParams] = MENU_TEXT_COMP_INT
	g_sMenuData.iTitleTotalParams++
	
	g_sMenuData.iTitleInt[g_sMenuData.iTitleIntParams] = paramInt
	g_sMenuData.iTitleIntParams++
ENDPROC
/// PURPOSE: Adds a float to the item description
PROC ADD_MENU_TITLE_FLOAT(FLOAT paramFloat, INT paramDecimalPlaces = 1)
	IF g_sMenuData.iTitleFloatParams >= MAX_STORED_DESC_FLOATS
	OR g_sMenuData.iTitleTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_TITLE_FLOAT: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eTitleComps[g_sMenuData.iTitleTotalParams] = MENU_TEXT_COMP_FLOAT
	g_sMenuData.iTitleTotalParams++
	
	g_sMenuData.fTitleFloat[g_sMenuData.iTitleFloatParams] = paramFloat
	g_sMenuData.iTitleFloatDP[g_sMenuData.iTitleFloatParams] = paramDecimalPlaces
	g_sMenuData.iTitleFloatParams++
ENDPROC
/// PURPOSE: Adds a float to the item description
PROC ADD_MENU_TITLE_STRING(STRING paramSubString)
	IF g_sMenuData.iTitleTextParams >= MAX_STORED_DESC_STRINGS
	OR g_sMenuData.iTitleTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_TITLE_STRING: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eTitleComps[g_sMenuData.iTitleTotalParams] = MENU_TEXT_COMP_TEXT
	g_sMenuData.iTitleTotalParams++
	
	g_sMenuData.tl15TitleText[g_sMenuData.iTitleTextParams] = paramSubString
	g_sMenuData.iTitleTextParams++
ENDPROC
/// PURPOSE: Adds a player name to the title
PROC ADD_PLAYER_NAME_TO_MENU_TITLE(TEXT_LABEL_63 tlPlayerNameItem)

	IF g_sMenuData.iTitlePlayerNameParams >= MAX_STORED_DESC_PLAYER_NAMES
	OR g_sMenuData.iTitleTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_PLAYER_NAME_TO_MENU_TITLE: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF

	g_sMenuData.eTitleComps[g_sMenuData.iTitleTotalParams] = MENU_TEXT_COMP_PLAYER_NAME
	g_sMenuData.iTitleTotalParams++
	
	g_sMenuData.tlTitlePlayerName[g_sMenuData.iTitlePlayerNameParams] = tlPlayerNameItem
	g_sMenuData.iTitlePlayerNameParams++
ENDPROC
/// PURPOSE: Adds a player name to the title
PROC ADD_LITERAL_TO_MENU_TITLE(STRING paramLiteral)

	IF g_sMenuData.iTitlePlayerNameParams >= MAX_STORED_DESC_PLAYER_NAMES
	OR g_sMenuData.iTitleTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_LITERAL_TO_MENU_TITLE: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF

	g_sMenuData.eTitleComps[g_sMenuData.iTitleTotalParams] = MENU_TEXT_COMP_LITERAL
	g_sMenuData.iTitleTotalParams++
	
	g_sMenuData.tlTitlePlayerName[g_sMenuData.iTitlePlayerNameParams] = paramLiteral
	g_sMenuData.iTitlePlayerNameParams++
ENDPROC
/// PURPOSE: Adds a radio station name to the title
PROC ADD_RADIO_STATION_NAME_TO_MENU_TITLE(TEXT_LABEL_31 tlRadioStationItem)

	IF g_sMenuData.iTitlePlayerNameParams >= MAX_STORED_DESC_PLAYER_NAMES
	OR g_sMenuData.iTitleTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_RADIO_STATION_NAME_TO_MENU_TITLE: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF

	g_sMenuData.eTitleComps[g_sMenuData.iTitleTotalParams] = MENU_TEXT_COMP_RADIO_STATION
	g_sMenuData.iTitleTotalParams++
	
	g_sMenuData.tlTitlePlayerName[g_sMenuData.iTitlePlayerNameParams] = tlRadioStationItem
	g_sMenuData.iTitlePlayerNameParams++
ENDPROC
/// PURPOSE: Sets the minimum width of the title box
PROC SET_MENU_TITLE_WIDTH(FLOAT paramWidth)
	g_sMenuData.fTitleWidth = paramWidth
ENDPROC
/// PURPOSE: Override the current row selection and total row count that appears on the title bar
PROC SET_MENU_TITLE_ROW_COUNT_OVERRIDE(BOOL bOverride, INT iCurrent, INT iTotal)
	g_sMenuData.bOverrideTitleRowCounts = bOverride
	g_sMenuData.iTitleRowOverride1 = iCurrent
	g_sMenuData.iTitleRowOverride2 = iTotal
ENDPROC

//////////////////////////////////////////////////////////////////////////////
///    
///    DESCRIPTION
///    
/// PURPOSE: Returns the description label
PROC GET_CURRENT_MENU_ITEM_DESCRIPTION(TEXT_LABEL_15 &tlDesc)
	tlDesc = g_sMenuData.tl23Desc
ENDPROC 

/// PURPOSE: Set the current item description
PROC SET_CURRENT_MENU_ITEM_DESCRIPTION_AS_TEXT_LABEL(TEXT_LABEL_23 paramLabel, INT paramClearTimer = 0, MENU_ICON_TYPE eIcon = MENU_ICON_DUMMY)
	g_sMenuData.tl23Desc = paramLabel
	g_sMenuData.iDescTotalParams = 0
	g_sMenuData.iDescIntParams = 0
	g_sMenuData.iDescFloatParams = 0
	g_sMenuData.iDescTextParams = 0
	g_sMenuData.iDescClearTimer = paramClearTimer
	g_sMenuData.iDescStartTimer = GET_GAME_TIMER()
	g_sMenuData.eDescIcon = eIcon
	
	INT i
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
ENDPROC

PROC SET_CURRENT_MENU_ITEM_DESCRIPTION(STRING paramLabel, INT paramClearTimer = 0, MENU_ICON_TYPE eIcon = MENU_ICON_DUMMY)
	g_sMenuData.tl23Desc = paramLabel
	g_sMenuData.iDescTotalParams = 0
	g_sMenuData.iDescIntParams = 0
	g_sMenuData.iDescFloatParams = 0
	g_sMenuData.iDescTextParams = 0
	g_sMenuData.iDescClearTimer = paramClearTimer
	g_sMenuData.iDescStartTimer = GET_GAME_TIMER()
	g_sMenuData.eDescIcon = eIcon
	
	INT i
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
ENDPROC
PROC SET_CURRENT_MENU_ITEM_DESCRIPTION_EXTRA(STRING paramLabel)
	g_sMenuData.tl23DescExtra = paramLabel
ENDPROC

/// PURPOSE: Adds an int to the item description
PROC ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(INT paramInt)
	IF g_sMenuData.iDescIntParams >= MAX_STORED_DESC_INTS
	OR g_sMenuData.iDescTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eDescComps[g_sMenuData.iDescTotalParams] = MENU_TEXT_COMP_INT
	g_sMenuData.iDescTotalParams++
	
	g_sMenuData.iDescInt[g_sMenuData.iDescIntParams] = paramInt
	g_sMenuData.iDescIntParams++
ENDPROC
/// PURPOSE: Adds a float to the item description
PROC ADD_CURRENT_MENU_ITEM_DESCRIPTION_FLOAT(FLOAT paramFloat, INT paramDecimalPlaces = 1)
	IF g_sMenuData.iDescFloatParams >= MAX_STORED_DESC_FLOATS
	OR g_sMenuData.iDescTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DESCRIPTION_FLOAT: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eDescComps[g_sMenuData.iDescTotalParams] = MENU_TEXT_COMP_FLOAT
	g_sMenuData.iDescTotalParams++
	
	g_sMenuData.fDescFloat[g_sMenuData.iDescFloatParams] = paramFloat
	g_sMenuData.iDescFloatDP[g_sMenuData.iDescFloatParams] = paramDecimalPlaces
	g_sMenuData.iDescFloatParams++
ENDPROC
/// PURPOSE: Adds a string to the item description
PROC ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(STRING paramSubString)
	IF g_sMenuData.iDescTextParams >= MAX_STORED_DESC_STRINGS
	OR g_sMenuData.iDescTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eDescComps[g_sMenuData.iDescTotalParams] = MENU_TEXT_COMP_TEXT
	g_sMenuData.iDescTotalParams++
	
	g_sMenuData.tl15DescText[g_sMenuData.iDescTextParams] = paramSubString
	g_sMenuData.iDescTextParams++
ENDPROC
/// PURPOSE: Adds a player name to the item description
PROC ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(STRING paramPlayerName)
	IF g_sMenuData.iDescTextParams >= MAX_STORED_DESC_STRINGS
	OR g_sMenuData.iDescTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eDescComps[g_sMenuData.iDescTotalParams] = MENU_TEXT_COMP_PLAYER_NAME
	g_sMenuData.iDescTotalParams++
	
	g_sMenuData.tl15DescText[g_sMenuData.iDescTextParams] = paramPlayerName
	g_sMenuData.iDescTextParams++
ENDPROC
/// PURPOSE: Adds a player name to the item description
PROC ADD_CURRENT_MENU_ITEM_DESCRIPTION_RADIO_STATION(STRING paramPlayerName)
	IF g_sMenuData.iDescTextParams >= MAX_STORED_DESC_STRINGS
	OR g_sMenuData.iDescTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DESCRIPTION_RADIO_STATION: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData.eDescComps[g_sMenuData.iDescTotalParams] = MENU_TEXT_COMP_RADIO_STATION
	g_sMenuData.iDescTotalParams++
	
	g_sMenuData.tl15DescText[g_sMenuData.iDescTextParams] = paramPlayerName
	g_sMenuData.iDescTextParams++
ENDPROC


//////////////////////////////////////////////////////////////////////////////
///    
///    DISCOUNT
///    
/// PURPOSE: Returns the Discount label
PROC GET_CURRENT_MENU_ITEM_DISCOUNT(TEXT_LABEL_15 &tlDiscount)
	tlDiscount = g_sMenuData_TU.tl15Discount
ENDPROC 

/// PURPOSE: Set the current item Discount
PROC SET_CURRENT_MENU_ITEM_DISCOUNT(STRING paramLabel, INT paramClearTimer = 0, MENU_ICON_TYPE eIcon = MENU_ICON_DUMMY)
	g_sMenuData_TU.tl15Discount = paramLabel
	g_sMenuData_TU.iDiscountTotalParams = 0
	g_sMenuData_TU.iDiscountIntParams = 0
	g_sMenuData_TU.iDiscountFloatParams = 0
	g_sMenuData_TU.iDiscountTextParams = 0
	g_sMenuData_TU.iDiscountClearTimer = paramClearTimer
	g_sMenuData_TU.iDiscountStartTimer = GET_GAME_TIMER()
	g_sMenuData_TU.eDiscountIcon = eIcon
	
	INT i
	REPEAT MAX_STORED_TEXT_COMPS i
		g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_DUMMY
	ENDREPEAT
ENDPROC
/// PURPOSE: Adds an int to the item Discount
PROC ADD_CURRENT_MENU_ITEM_DISCOUNT_INT(INT paramInt)
	IF g_sMenuData_TU.iDiscountIntParams >= MAX_STORED_DISCOUNT_INTS
	OR g_sMenuData_TU.iDiscountTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DISCOUNT_INT: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData_TU.eDiscountComps[g_sMenuData_TU.iDiscountTotalParams] = MENU_TEXT_COMP_INT
	g_sMenuData_TU.iDiscountTotalParams++
	
	g_sMenuData_TU.iDiscountInt[g_sMenuData_TU.iDiscountIntParams] = paramInt
	g_sMenuData_TU.iDiscountIntParams++
ENDPROC
/// PURPOSE: Adds a float to the item Discount
PROC ADD_CURRENT_MENU_ITEM_DISCOUNT_FLOAT(FLOAT paramFloat, INT paramDecimalPlaces = 1)
	IF g_sMenuData_TU.iDiscountFloatParams >= MAX_STORED_DISCOUNT_FLOATS
	OR g_sMenuData_TU.iDiscountTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DISCOUNT_FLOAT: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData_TU.eDiscountComps[g_sMenuData_TU.iDiscountTotalParams] = MENU_TEXT_COMP_FLOAT
	g_sMenuData_TU.iDiscountTotalParams++
	
	g_sMenuData_TU.fDiscountFloat[g_sMenuData_TU.iDiscountFloatParams] = paramFloat
	g_sMenuData_TU.iDiscountFloatDP[g_sMenuData_TU.iDiscountFloatParams] = paramDecimalPlaces
	g_sMenuData_TU.iDiscountFloatParams++
ENDPROC
/// PURPOSE: Adds a string to the item Discount
PROC ADD_CURRENT_MENU_ITEM_DISCOUNT_STRING(STRING paramSubString)
	IF g_sMenuData_TU.iDiscountTextParams >= MAX_STORED_DISCOUNT_STRINGS
	OR g_sMenuData_TU.iDiscountTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DISCOUNT_STRING: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData_TU.eDiscountComps[g_sMenuData_TU.iDiscountTotalParams] = MENU_TEXT_COMP_TEXT
	g_sMenuData_TU.iDiscountTotalParams++
	
	g_sMenuData_TU.tl15DiscountText[g_sMenuData_TU.iDiscountTextParams] = paramSubString
	g_sMenuData_TU.iDiscountTextParams++
ENDPROC
/// PURPOSE: Adds a player name to the item Discount
PROC ADD_CURRENT_MENU_ITEM_DISCOUNT_PLAYER_NAME(STRING paramPlayerName)
	IF g_sMenuData_TU.iDiscountTextParams >= MAX_STORED_DISCOUNT_STRINGS
	OR g_sMenuData_TU.iDiscountTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DISCOUNT_PLAYER_NAME: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData_TU.eDiscountComps[g_sMenuData_TU.iDiscountTotalParams] = MENU_TEXT_COMP_PLAYER_NAME
	g_sMenuData_TU.iDiscountTotalParams++
	
	g_sMenuData_TU.tl15DiscountText[g_sMenuData_TU.iDiscountTextParams] = paramPlayerName
	g_sMenuData_TU.iDiscountTextParams++
ENDPROC
/// PURPOSE: Adds a player name to the item Discount
PROC ADD_CURRENT_MENU_ITEM_DISCOUNT_RADIO_STATION(STRING paramPlayerName)
	IF g_sMenuData_TU.iDiscountTextParams >= MAX_STORED_DISCOUNT_STRINGS
	OR g_sMenuData_TU.iDiscountTotalParams >= MAX_STORED_TEXT_COMPS
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_CURRENT_MENU_ITEM_DISCOUNT_RADIO_STATION: Unable to add new param. If you need more params tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	g_sMenuData_TU.eDiscountComps[g_sMenuData_TU.iDiscountTotalParams] = MENU_TEXT_COMP_RADIO_STATION
	g_sMenuData_TU.iDiscountTotalParams++
	
	g_sMenuData_TU.tl15DiscountText[g_sMenuData_TU.iDiscountTextParams] = paramPlayerName
	g_sMenuData_TU.iDiscountTextParams++
ENDPROC


//////////////////////////////////////////////////////////////////////////////
///    
///    MENU ITEMS
///    

/// PURPOSE: Specifies if the specific menu item should use car colours when drawing to screen
PROC SET_MENU_ITEM_USES_CAR_COLOUR(INT iMenuRow, INT iMenuColumn, INT iCarColour, INT iArray = 0)
	IF iArray < MAX_COLOURED_ITEMS
		g_sMenuData.iCarColourItem[iArray] = (iMenuRow*MAX_MENU_COLUMNS)+iMenuColumn
		g_sMenuData.iCarColour[iArray] = iCarColour
	ENDIF
ENDPROC

/// PURPOSE: Specifies which menu item we should be displaying at the top of the list
PROC SET_TOP_MENU_ITEM(INT iTopItem)
	g_sMenuData.iTopItem = iTopItem
ENDPROC

/// PURPOSE: Specifies which menu item we should be displaying at the top of the list
FUNC INT GET_TOP_MENU_ITEM()
	RETURN g_sMenuData.iTopItem
ENDFUNC


/// PURPOSE: Specifies which menu item we should be highlighting
PROC SET_CURRENT_MENU_ITEM(INT iCurrentItem, BOOL bClearCurrentItemDescription = TRUE, BOOL bDisplayToggles = TRUE)
	g_sMenuData.iCurrentItem = iCurrentItem
	g_sMenuData.bDisplayCurrentItemToggles = bDisplayToggles
	
	// Update top item
	IF g_sMenuData.iCurrentItem < g_sMenuData.iTopItem
		g_sMenuData.iTopItem = g_sMenuData.iCurrentItem
	ELIF (g_sMenuData.bDisplayRowsDefined AND g_sMenuData.iCurrentItem > g_sMenuData.iLastDisplayItem)
	OR (NOT g_sMenuData.bDisplayRowsDefined AND g_sMenuData.iCurrentItem >= (g_sMenuData.iTopItem+g_sMenuData.iMenuRows))
		// Update top item until we have 10 items on display
		INT i, iItemCount
		FOR i = g_sMenuData.iTopItem TO g_sMenuData.iCurrentItem
			IF i >= 0 AND i < MAX_MENU_ROWS-1
				IF g_sMenuData.iItemBitset[i] != 0
					iItemCount++
				ENDIF
			ENDIF
		ENDFOR
		
		WHILE iItemCount > g_sMenuData.iMenuRows
		AND g_sMenuData.iTopItem < MAX_MENU_ROWS
			g_sMenuData.iTopItem++
			iItemCount = 0
			FOR i = g_sMenuData.iTopItem TO g_sMenuData.iCurrentItem
				IF i >= 0 AND i < MAX_MENU_ROWS-1
					IF g_sMenuData.iItemBitset[i] != 0
						iItemCount++
					ENDIF
				ENDIF
			ENDFOR
		ENDWHILE
	ENDIF
	
	// Force update so we can get new item count
	g_sMenuData.bSetupComplete = FALSE
	g_sMenuData.bDisplayRowsDefined = FALSE
	
	// Clear the item description
	IF bClearCurrentItemDescription
		g_sMenuData.tl23Desc = ""
		g_sMenuData_TU.tl15Discount = ""
	ENDIF
ENDPROC

PROC SET_CURRENT_MENU_ITEM_WITH_WRAP(INT iCurrentItem, BOOL bClearCurrentItemDescription = TRUE, BOOL bDisplayToggles = TRUE)
	IF iCurrentItem < 0
		CDEBUG1LN(DEBUG_EXT_MENU, "(lower) Wrapping menu selection from ", iCurrentItem, " to ", g_sMenuData.iCurrentRow)
		iCurrentItem = g_sMenuData.iCurrentRow
	ELIF iCurrentItem > g_sMenuData.iCurrentRow
		CDEBUG1LN(DEBUG_EXT_MENU, "(upper) Wrapping menu selection from ", iCurrentItem, " to ", 0)
		iCurrentItem = 0
	ENDIF
	SET_CURRENT_MENU_ITEM(iCurrentItem, bClearCurrentItemDescription, bDisplayToggles)
ENDPROC

PROC SET_CURRENT_MENU_ITEM_WITH_CLAMP(INT iCurrentItem, BOOL bClearCurrentItemDescription = TRUE, BOOL bDisplayToggles = TRUE)
	IF iCurrentItem < 0
		CDEBUG1LN(DEBUG_EXT_MENU, "(lower) Clamping menu selection from ", iCurrentItem, " to ", 0)
		iCurrentItem = 0
	ELIF iCurrentItem > g_sMenuData.iCurrentRow
		CDEBUG1LN(DEBUG_EXT_MENU, "(upper) Clamping menu selection from ", iCurrentItem, " to ", g_sMenuData.iCurrentRow)
		iCurrentItem = g_sMenuData.iCurrentRow
	ENDIF
	SET_CURRENT_MENU_ITEM(iCurrentItem, bClearCurrentItemDescription, bDisplayToggles)
ENDPROC

PROC SetMenuRowAsGrayedOut(INT iRow, BOOL bSet)
	INT iArray = FLOOR( TO_FLOAT(iRow) / 32.0)
	#IF IS_DEBUG_BUILD
		IF NOT (iArray < COUNT_OF(g_sMenuData.iRowIsGrayedOutBS))
			SCRIPT_ASSERT("SetMenuRowAsGrayedOut - need to increase array size! see g_sMenuData.iRowIsGrayedOutBS[]") 
		ENDIF
	#ENDIF
	IF (bSet)	    
        SET_BIT(g_sMenuData.iRowIsGrayedOutBS[iArray], (iRow - (iArray*32)))	
    ELSE
		CLEAR_BIT(g_sMenuData.iRowIsGrayedOutBS[iArray], (iRow - (iArray*32))) 
    ENDIF
ENDPROC

FUNC BOOL IS_RT_AND_LT_BEING_HELD()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CREATOR_RT) AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CREATOR_LT)
	ENDIF
	
	RETURN IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT) AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
ENDFUNC


FUNC BOOL IS_MENU_ROW_SELECTABLE(INT iRow)
	INT iArray = FLOOR( TO_FLOAT(iRow) / 32.0)
	#IF IS_DEBUG_BUILD
		IF NOT (iArray < COUNT_OF(g_sMenuData.iRowIsGrayedOutBS))
			SCRIPT_ASSERT("IS_MENU_ROW_SELECTABLE - need to increase array size! see MenuData.iRowIsGrayedOutBS[]") 
		ENDIF
	#ENDIF	
	
	IF IS_ROCKSTAR_DEV() AND IS_RT_AND_LT_BEING_HELD() AND GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR 
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_sMenuData.iRowIsGrayedOutBS[iArray], (iRow - (iArray*32)))
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC FLOAT RESIZE_MENU_BASED_ON_STRING_LENGTH(STRING paramLabel, INT iMaxLength)
	// Returns a small increment to be added to whatever the default size of the menu is
	INT iStringOverflow = 0
	FLOAT fMenuExtension = 0.0
	
	g_sMenuData.tl15Item[g_sMenuData.iCurrentTextItem] = paramLabel
	INT iMenuOptionStringLength = GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(paramLabel)
	
	IF iMenuOptionStringLength > iMaxLength
		iStringOverflow = iMenuOptionStringLength - iMaxLength
		fMenuExtension = iStringOverflow / 100.0
	ENDIF
	PRINTLN("[MJL][RESIZE_MENU_BASED_ON_STRING_LENGTH] iMenuOptionStringLength : ", iMenuOptionStringLength, "    iStringOverflow : ", iStringOverflow, "    fMenuExtension : ", fMenuExtension)
	
	RETURN fMenuExtension
ENDFUNC

/// PURPOSE: Set the value of the next text item
PROC ADD_MENU_ITEM_TEXT(INT paramRow, STRING paramLabel, INT paramNumberOfSubComponents = 0, BOOL bIsSelectable = TRUE, BOOL bIsDefault = FALSE, BOOL bAdjustRowHeight = FALSE, BOOL bForceCondensedFont = FALSE)

	// Already defined menu row
	IF (g_sMenuData.iCurrentRow > paramRow)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT: Row ", paramRow, " already defined. Make sure you have called CLEAR_MENU() before building new menu \"", paramLabel, "\".")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of menu row
	IF (g_sMenuData.iCurrentRow >= MAX_MENU_ROWS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT: Menu row is out of range. If you need more rows tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored variables
	IF (g_sMenuData.iCurrentTextItem >= MAX_STORED_MENU_TEXT_LABELS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT: Stored text item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Expecting component item
	IF (g_sMenuData.iComponentCount < g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT: Expecting component item for text label.")
		#ENDIF
		EXIT
	ENDIF
	
	// Update expected item
	IF (g_sMenuData.iCurrentRow != paramRow)
		g_sMenuData.iCurrentRow = paramRow
		g_sMenuData.iCurrentColumn = 0
	ENDIF
	MENU_ITEM_TYPE eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
	
	// Item type not expected (optional item must have been skipped)
	IF (eNextItem != MENU_ITEM_TEXT)
		// Find next item that matches type
		WHILE (g_sMenuData.iCurrentColumn < MAX_MENU_COLUMNS-1)
		AND (eNextItem != MENU_ITEM_TEXT)
			g_sMenuData.iCurrentColumn++
			eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
		ENDWHILE
		// Unable to find matching type
		IF (eNextItem != MENU_ITEM_TEXT)
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT: Unable to add item ", paramRow, " \"", paramLabel, "\". Check that this item has been specified in SET_MENU_LAYOUT().")
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	// Store the value
	g_sMenuData.tl15Item[g_sMenuData.iCurrentTextItem] = paramLabel
#IF IS_DEBUG_BUILD
	IF g_sMenuData.iCurrentColumn = 0
		INT realNameLength = GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(paramLabel)
		IF realNameLength > 23
			realNameLength = 23
		ENDIF
		g_sMenuData.realNames[paramRow] = GET_FIRST_N_CHARACTERS_OF_STRING(paramLabel, realNameLength)
	ENDIF
#ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(paramLabel)
	AND NOT DOES_TEXT_LABEL_EXIST(paramLabel)
		#IF IS_DEBUG_BUILD
		CASSERTLN(DEBUG_SYSTEM, "Text label \"", paramLabel, "\"is not in the GXT, this will not work.")
		#ENDIF
	ENDIF
	g_sMenuData.bIsSelectable[g_sMenuData.iCurrentTextItem] = bIsSelectable
	g_sMenuData.bIsDefault[g_sMenuData.iCurrentTextItem] = bIsDefault
	g_sMenuData.bForceCondensedFont[g_sMenuData.iCurrentTextItem] = bForceCondensedFont
	g_sMenuData.iCurrentTextItem++
	
	IF NOT (bIsSelectable)
		SetMenuRowAsGrayedOut(g_sMenuData.iCurrentRow, TRUE)
	ELSE
		SetMenuRowAsGrayedOut(g_sMenuData.iCurrentRow, FALSE)	
	ENDIF
	
	// Update column offset
	IF paramNumberOfSubComponents = 0
		FLOAT fWidth = GET_MENU_ITEM_TEXT_WIDTH(g_sMenuData.tl15Item[g_sMenuData.iCurrentTextItem])
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF paramNumberOfSubComponents = 0
			FLOAT fHeight = GET_MENU_ITEM_TEXT_HEIGHT(g_sMenuData.tl15Item[g_sMenuData.iCurrentTextItem])
			IF fHeight > g_sMenuData.fRowHeight[paramRow]
				g_sMenuData.fRowHeight[paramRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
	
	// Mark the item as set
	SET_BIT(g_sMenuData.iItemBitset[paramRow], g_sMenuData.iCurrentColumn)
	g_sMenuData.iCurrentColumn++
	
	// Set the last item type
	g_sMenuData.eLastAddedItemType = MENU_ITEM_TEXT
	
	// Set up last text item incase we add sub components
	g_sMenuData.iLastTextItem = g_sMenuData.iCurrentTextItem-1
	g_sMenuData.iComponentCount = 0
	g_sMenuData.iComponentItems = paramNumberOfSubComponents
ENDPROC

/// PURPOSE: Set the value of the next text item using a text label
PROC ADD_MENU_ITEM_TEXT_TL(INT paramRow, TEXT_LABEL_63 paramLabel, INT paramNumberOfSubComponents = 0, BOOL bIsSelectable = TRUE, BOOL bIsDefault = FALSE, BOOL bAdjustRowHeight = FALSE)
	STRING strTextLabel = Get_String_From_TextLabel(paramLabel)
	ADD_MENU_ITEM_TEXT(paramRow, strTextLabel, paramNumberOfSubComponents, bIsSelectable, bIsDefault, bAdjustRowHeight)
ENDPROC

/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_INT(INT paramInt, BOOL bAdjustRowHeight = FALSE)

	// Ran out of stored variables
	IF (g_sMenuData.iCurrentIntItem >= MAX_STORED_MENU_INTS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_INT: Stored int item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_INT: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_INT: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_INT: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_sMenuData.iItem[g_sMenuData.iCurrentIntItem] = paramInt
	g_sMenuData.iCurrentIntItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_INT
	g_sMenuData.iComponentCount++
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(FLOAT paramFloat, INT paramDecimalPlaces = 1, BOOL bAdjustRowHeight = FALSE)
	
	// Ran out of stored variables
	IF (g_sMenuData.iCurrentFloatItem >= MAX_STORED_MENU_FLOATS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT: Stored float item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_sMenuData.fItem[g_sMenuData.iCurrentFloatItem] = paramFloat
	g_sMenuData.iFloatDP[g_sMenuData.iCurrentFloatItem] = paramDecimalPlaces
	g_sMenuData.iCurrentFloatItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_FLOAT
	g_sMenuData.iComponentCount++
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_STRING(STRING paramSubString, BOOL bAdjustRowHeight = FALSE)

	// Ran out of stored variables
	IF (g_sMenuData.iCurrentTextItem >= MAX_STORED_MENU_TEXT_LABELS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_STRING: Stored text item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_STRING: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_STRING: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_STRING: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_sMenuData.tl15Item[g_sMenuData.iCurrentTextItem] = paramSubString
	g_sMenuData.iCurrentTextItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_TEXT
	g_sMenuData.iComponentCount++
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(STRING paramPlayerName, BOOL bAdjustRowHeight = FALSE, BOOL bReduceTextToFitColumn = FALSE, BOOL bSetWidth = TRUE)

	// Ran out of stored variables
	IF (g_sMenuData.iCurrentPlayerNameItem >= MAX_STORED_MENU_PLAYER_NAMES)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME: Stored player name item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem] = paramPlayerName
	g_sMenuData.iCurrentPlayerNameItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_PLAYER_NAME
	g_sMenuData.iComponentCount++
	
	FLOAT fWidth = 0.0
	
	IF (bSetWidth)
		fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
	ENDIF
	
	// Reduce the string if required...
	IF bReduceTextToFitColumn
	
		// Removing for now as it dosent work for us.
		// We will need to scale the text...
	
		/*INT iLen = GET_LENGTH_OF_LITERAL_STRING(paramPlayerName)
		// If bigger than width...
		WHILE fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			IF iLen > 1
				// chop a character off using audio command...
				paramPlayerName = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(paramPlayerName, 0, iLen-1)
				g_sMenuData.tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-1] = paramPlayerName
				fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
				iLen--
			ELSE
				fWidth = -1.0 // Bail
			ENDIF
		ENDWHILE*/
	ENDIF
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_VEHICLE_NAME(STRING paramVehicleName, BOOL bAdjustRowHeight = FALSE)

	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_VEHICLE_NAME: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_VEHICLE_NAME: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_sMenuData.tl15Item[g_sMenuData.iCurrentTextItem] = paramVehicleName
	g_sMenuData.iCurrentTextItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_VEHICLE_NAME
	g_sMenuData.iComponentCount++
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_RADIO_STATION(STRING paramRadioStation, BOOL bAdjustRowHeight = FALSE, BOOL bReduceTextToFitColumn = FALSE)

	// Ran out of stored variables
	IF (g_sMenuData.iCurrentPlayerNameItem >= MAX_STORED_MENU_PLAYER_NAMES)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_RADIO_STATION: Stored player name item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_RADIO_STATION: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_RADIO_STATION: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_RADIO_STATION: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem] = paramRadioStation
	g_sMenuData.iCurrentPlayerNameItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_RADIO_STATION
	g_sMenuData.iComponentCount++
	
	FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
	
	// Reduce the string if required...
	IF bReduceTextToFitColumn
	
		// Removing for now as it dosent work for us.
		// We will need to scale the text...
	
		/*INT iLen = GET_LENGTH_OF_LITERAL_STRING(paramPlayerName)
		// If bigger than width...
		WHILE fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			IF iLen > 1
				// chop a character off using audio command...
				paramPlayerName = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(paramPlayerName, 0, iLen-1)
				g_sMenuData.tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-1] = paramPlayerName
				fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
				iLen--
			ELSE
				fWidth = -1.0 // Bail
			ENDIF
		ENDWHILE*/
	ENDIF
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(STRING paramLiteral, BOOL bAdjustRowHeight = FALSE, BOOL bReduceTextToFitColumn = FALSE, BOOL bCondensedFont = FALSE)

	// Ran out of stored variables
	IF (g_sMenuData.iCurrentPlayerNameItem >= MAX_STORED_MENU_PLAYER_NAMES)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL: Stored literal item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_TUMenuGlobals_tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem] = paramLiteral
	g_sMenuData.iCurrentPlayerNameItem++
	
	// Set the component type for the last text item
	IF bCondensedFont
		g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_CONDENSED_LITERAL
	ELSE
		g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_LITERAL
	ENDIF
	g_sMenuData.iComponentCount++
	
	FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
	
	// Reduce the string if required...
	IF bReduceTextToFitColumn
	
		// Removing for now as it dosent work for us.
		// We will need to scale the text...
	
		/*INT iLen = GET_LENGTH_OF_LITERAL_STRING(paramLiteral)
		// If bigger than width...
		WHILE fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			IF iLen > 1
				// chop a character off using audio command...
				paramLiteral = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(paramLiteral, 0, iLen-1)
				g_sMenuData.tlPlayerNameItem[g_sMenuData.iCurrentPlayerNameItem-1] = paramLiteral
				fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
				iLen--
			ELSE
				fWidth = -1.0 // Bail
			ENDIF
		ENDWHILE*/
	ENDIF
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Set the sub component value of the current text item
PROC ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TYPE paramIcon, BOOL bAdjustRowHeight = FALSE)

	// Ran out of stored variables
	IF (g_sMenuData.iCurrentIconItem >= MAX_STORED_MENU_ICONS)
		#IF IS_DEBUG_BUILD
			PRINTLN("ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Stored icon item limit reached. iCurrentIconItem:", g_sMenuData.iCurrentIconItem, " >= MAX_STORED_MENU_ICONS:", MAX_STORED_MENU_ICONS)
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Stored icon item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored components
	IF (g_sMenuData.iComponentCount >= MAX_STORED_TEXT_COMPS)
		#IF IS_DEBUG_BUILD
			PRINTLN("ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Stored text component limit reached. iComponentCount:", g_sMenuData.iComponentCount, " >= MAX_STORED_TEXT_COMPS:", MAX_STORED_TEXT_COMPS)
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Stored text component limit reached.")
		#ENDIF
		EXIT
	ENDIF
	
	// Last item was not a text label
	IF (g_sMenuData.eLastAddedItemType != MENU_ITEM_TEXT)
		#IF IS_DEBUG_BUILD
			PRINTLN("ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Last item was not a text item. eLastAddedItemType:", g_sMenuData.eLastAddedItemType, " != MENU_ITEM_TEXT:", MENU_ITEM_TEXT)
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Last item was not a text item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Not expecting a component item
	IF (g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			PRINTLN("ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Not expecting a component item. iComponentCount:", g_sMenuData.iComponentCount, " >= iComponentItems:", g_sMenuData.iComponentItems)
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_TEXT_COMPONENT_ICON: Not expecting a component item.")
		#ENDIF
		EXIT
	ENDIF
	
	// Store the value
	g_sMenuData.eIconItem[g_sMenuData.iCurrentIconItem] = paramIcon
	g_sMenuData.iCurrentIconItem++
	
	// Set the component type for the last text item
	g_sMenuData.eTextItemComps[g_sMenuData.iLastTextItem][g_sMenuData.iComponentCount] = MENU_TEXT_COMP_ICON
	g_sMenuData.iComponentCount++
	
	// Update column offset (use -1 as we would have incremented this when adding the main text label)
	IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
		FLOAT fWidth = GET_LAST_MENU_ITEM_TEXT_WIDTH()
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		AND g_sMenuData.iComponentCount = g_sMenuData.iComponentItems //Must be last item
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn-1] = fWidth
		ENDIF
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		IF g_sMenuData.iComponentCount >= g_sMenuData.iComponentItems
			FLOAT fHeight = GET_LAST_MENU_ITEM_TEXT_HEIGHT()
			IF fHeight > g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow]
				g_sMenuData.fRowHeight[g_sMenuData.iCurrentRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Set the value of the next int item
PROC ADD_MENU_ITEM_INT(INT paramRow, INT paramInt, BOOL bAdjustRowHeight = FALSE)

	// Already defined menu row
	IF (g_sMenuData.iCurrentRow > paramRow)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_INT: Row ", paramRow, " already defined. Make sure you have called CLEAR_MENU() before building new menu \"", paramInt, "\".")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of menu row
	IF (g_sMenuData.iCurrentRow >= MAX_MENU_ROWS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_INT: Menu row is out of range. If you need more rows tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored variables
	IF (g_sMenuData.iCurrentIntItem >= MAX_STORED_MENU_INTS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_INT: Stored int item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Expecting component item
	IF (g_sMenuData.iComponentCount < g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_INT: Expecting component item for text label.")
		#ENDIF
		EXIT
	ENDIF
	
	// Update expected item
	IF (g_sMenuData.iCurrentRow != paramRow)
		g_sMenuData.iCurrentRow = paramRow
		g_sMenuData.iCurrentColumn = 0
	ENDIF
	MENU_ITEM_TYPE eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
	
	// Item type not expected (optional item must have been skipped)
	IF (eNextItem != MENU_ITEM_INT)
		// Find next item that matches type
		WHILE (g_sMenuData.iCurrentColumn < MAX_MENU_COLUMNS-1)
		AND (eNextItem != MENU_ITEM_INT)
			g_sMenuData.iCurrentColumn++
			eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
		ENDWHILE
		// Unable to find matching type
		IF (eNextItem != MENU_ITEM_INT)
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_INT: Unable to add item. Check that this item has been specified in SET_MENU_LAYOUT().")
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	// Store the value
	g_sMenuData.iItem[g_sMenuData.iCurrentIntItem] = paramInt
	g_sMenuData.iCurrentIntItem++
	
	// Update column offset
	FLOAT fWidth = GET_MENU_ITEM_TEXT_WIDTH_WITH_INT("NUMBER", paramInt)
	
	// Add toggle icons
	IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		FLOAT fIconWidth
		FLOAT fIconHeight
		GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
		fWidth += (fIconWidth*2)
	ENDIF
	
	IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn]
		g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn] = fWidth
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		FLOAT fHeight = GET_MENU_ITEM_TEXT_HEIGHT_WITH_INT("NUMBER", paramInt)
		IF fHeight > g_sMenuData.fRowHeight[paramRow]
			g_sMenuData.fRowHeight[paramRow] = fHeight
		ENDIF
	ENDIF
	
	// Mark the item as set
	SET_BIT(g_sMenuData.iItemBitset[paramRow], g_sMenuData.iCurrentColumn)
	g_sMenuData.iCurrentColumn++
	
	// Set the last item type
	g_sMenuData.eLastAddedItemType = MENU_ITEM_INT
ENDPROC

/// PURPOSE: Set the value of the next float item
PROC ADD_MENU_ITEM_FLOAT(INT paramRow, FLOAT paramFloat, INT paramDecimalPlaces = 1, BOOL bAdjustRowHeight = FALSE)

	// Already defined menu row
	IF (g_sMenuData.iCurrentRow > paramRow)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_FLOAT: Row ", paramRow, " already defined. Make sure you have called CLEAR_MENU() before building new menu \"", GET_STRING_FROM_FLOAT(paramFloat), "\".")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of menu rows
	IF (g_sMenuData.iCurrentRow >= MAX_MENU_ROWS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_FLOAT: Menu row is out of range. If you need more rows tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored variables
	IF (g_sMenuData.iCurrentFloatItem >= MAX_STORED_MENU_FLOATS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_FLOAT: Stored float item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Expecting component item
	IF (g_sMenuData.iComponentCount < g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_FLOAT: Expecting component item for text label.")
		#ENDIF
		EXIT
	ENDIF
	
	// Update expected item
	IF (g_sMenuData.iCurrentRow != paramRow)
		g_sMenuData.iCurrentRow = paramRow
		g_sMenuData.iCurrentColumn = 0
	ENDIF
	MENU_ITEM_TYPE eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
	
	// Item type not expected (optional item must have been skipped)
	IF (eNextItem != MENU_ITEM_FLOAT)
		// Find next item that matches type
		WHILE (g_sMenuData.iCurrentColumn < MAX_MENU_COLUMNS-1)
		AND (eNextItem != MENU_ITEM_FLOAT)
			g_sMenuData.iCurrentColumn++
			eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
		ENDWHILE
		// Unable to find matching type
		IF (eNextItem != MENU_ITEM_FLOAT)
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_FLOAT: Unable to add item. Check that this item has been specified in SET_MENU_LAYOUT().")
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	// Store the value
	g_sMenuData.fItem[g_sMenuData.iCurrentFloatItem] = paramFloat
	g_sMenuData.iFloatDP[g_sMenuData.iCurrentFloatItem] = paramDecimalPlaces
	g_sMenuData.iCurrentFloatItem++
	
	// Update column offset
	FLOAT fWidth = GET_MENU_ITEM_TEXT_WIDTH_WITH_FLOAT("NUMBER", paramFloat, paramDecimalPlaces)
	
	// Add toggle icons
	IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
		FLOAT fIconWidth
		FLOAT fIconHeight
		GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
		fWidth += (fIconWidth*2)
	ENDIF
	
	IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn]
		g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn] = fWidth
	ENDIF
	
	// Update row offset
	IF bAdjustRowHeight
		FLOAT fHeight = GET_MENU_ITEM_TEXT_HEIGHT_WITH_FLOAT("NUMBER", paramFloat, paramDecimalPlaces)
		IF fHeight > g_sMenuData.fRowHeight[paramRow]
			g_sMenuData.fRowHeight[paramRow] = fHeight
		ENDIF
	ENDIF
	
	// Mark the item as set
	SET_BIT(g_sMenuData.iItemBitset[paramRow], g_sMenuData.iCurrentColumn)
	g_sMenuData.iCurrentColumn++
	
	// Set the last item type
	g_sMenuData.eLastAddedItemType = MENU_ITEM_FLOAT
ENDPROC

/// PURPOSE: Add a space between the current and next menu items
PROC ADD_MENU_SPACER()
	// Ran out of menu rows
	IF (g_sMenuData.iCurrentRow >= MAX_MENU_ROWS-1)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_SPACER: Menu row is out of range. If you need more rows tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	g_sMenuData.bMenuRowHasSpacer[g_sMenuData.iCurrentRow+1] = TRUE
ENDPROC

/// PURPOSE: Set the value of the next icon item
PROC ADD_MENU_ITEM_ICON(INT paramRow, MENU_ICON_TYPE paramIcon, BOOL bAdjustRowHeight = FALSE)

	// Already defined menu row
	IF (g_sMenuData.iCurrentRow > paramRow)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_ICON: Row ", paramRow, " already defined. Make sure you have called CLEAR_MENU() before building new menu \"", ENUM_TO_INT(paramIcon), "\".")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of menu rows
	IF (g_sMenuData.iCurrentRow >= MAX_MENU_ROWS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_ICON: Menu row is out of range. If you need more rows tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Ran out of stored variables
	IF (g_sMenuData.iCurrentIconItem >= MAX_STORED_MENU_ICONS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_ICON: Stored icon item limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Expecting component item
	IF (g_sMenuData.iComponentCount < g_sMenuData.iComponentItems)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_ICON: Expecting component item for text label.")
		#ENDIF
		EXIT
	ENDIF
	
	// Update expected item
	IF (g_sMenuData.iCurrentRow != paramRow)
		g_sMenuData.iCurrentRow = paramRow
		g_sMenuData.iCurrentColumn = 0
	ENDIF
	MENU_ITEM_TYPE eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
	
	// Item type not expected (optional item must have been skipped)
	IF (eNextItem != MENU_ITEM_ICON)
		// Find next item that matches type
		WHILE (g_sMenuData.iCurrentColumn < MAX_MENU_COLUMNS-1)
		AND (eNextItem != MENU_ITEM_ICON)
			g_sMenuData.iCurrentColumn++
			eNextItem = g_sMenuData.eItemLayout[g_sMenuData.iCurrentColumn]
		ENDWHILE
		// Unable to find matching type
		IF (eNextItem != MENU_ITEM_ICON)
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_ITEM_ICON: Unable to add item. Check that this item has been specified in SET_MENU_LAYOUT().")
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	// Store the value
	g_sMenuData.eIconItem[g_sMenuData.iCurrentIconItem] = paramIcon
	g_sMenuData.iCurrentIconItem++
	
	// Update column offset
	IF paramIcon <> MENU_ICON_DUMMY
		FLOAT fWidth, fHeight
		GET_MENU_ICON_SCREEN_RESOLUTION(paramIcon, TRUE, FALSE, fWidth, fHeight)
		
		// Add toggle icons
		IF g_sMenuData.bItemToggleable[g_sMenuData.iCurrentColumn]
			FLOAT fIconWidth
			FLOAT fIconHeight
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fIconWidth, fIconHeight)
			fWidth += (fIconWidth*2)
		ENDIF
		
		IF fWidth > g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn]
			g_sMenuData.fColumnWidth[g_sMenuData.iCurrentColumn] = fWidth
		ENDIF
		
		// If the icon is a shop logo, adjust the row height
		IF bAdjustRowHeight
			IF fHeight > g_sMenuData.fRowHeight[paramRow]
				g_sMenuData.fRowHeight[paramRow] = fHeight
			ENDIF
		ENDIF
	ENDIF
	
	// Mark the item as set
	SET_BIT(g_sMenuData.iItemBitset[paramRow], g_sMenuData.iCurrentColumn)
	g_sMenuData.iCurrentColumn++
	
	// Set the last item type
	g_sMenuData.eLastAddedItemType = MENU_ITEM_ICON
ENDPROC

PROC ADD_MENU_ITEM_ICON_TICK_BOX(INT iParamRow, BOOL bIconState)
	MENU_ICON_TYPE menuIcon
	IF NOT bIconState
		menuIcon = MENU_ICON_BOX_EMPTY
	ELSE
		menuIcon = MENU_ICON_BOX_TICK
	ENDIF
	ADD_MENU_ITEM_ICON(iParamRow, menuIcon)
ENDPROC


/// PURPOSE: Adds a help key to the menu
PROC ADD_MENU_HELP_KEY(STRING paramKey, STRING paramLabel, INT paramINT = -1)
	// Ran out of stored variables
	IF (g_sMenuData.iHelpCount >= MAX_STORED_HELP_KEYS)
		
		#IF USE_TU_CHANGES
			// HELP KEY OVERFLOW
			g_sMenuData_TU.eHelpKey = paramKey
			g_sMenuData_TU.tl15HelpText = paramLabel
			g_sMenuData_TU.iHelpTextINT = paramINT
			PRINTLN("ADD_MENU_HELP_KEY: Stored help limit reached. using overflow.")
			EXIT
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_HELP_KEY: Stored help limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Set non-clickable
	CLEAR_BIT(g_sMenuData.iHelpKeyIsClickableBits, g_sMenuData.iHelpCount)

	// Store the values
	g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey
	g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = paramLabel
	g_sMenuData.iHelpTextINT[g_sMenuData.iHelpCount] = paramINT
	g_sMenuData.caHelpTextInput[g_sMenuData.iHelpCount] = MAX_INPUTS				// Does not store either the input or inputgroup that the control is representing
	g_sMenuData.caHelpTextInputGroup[g_sMenuData.iHelpCount] = MAX_INPUTGROUPS		// This means it will not update the string if the control method changes
	g_sMenuData.iHelpCount++

ENDPROC

/// PURPOSE: Adds a help key to the menu
PROC ADD_MENU_HELP_KEY_INPUT(CONTROL_ACTION paramInput, STRING paramLabel, INT paramINT = -1)
	
	STRING paramKey = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, paramInput)
	
	// Ran out of stored variables
	IF (g_sMenuData.iHelpCount >= MAX_STORED_HELP_KEYS)
		
		#IF USE_TU_CHANGES
			// HELP KEY OVERFLOW
			g_sMenuData_TU.eHelpKey = paramKey
			g_sMenuData_TU.tl15HelpText = paramLabel
			g_sMenuData_TU.iHelpTextINT = paramINT
			PRINTLN("ADD_MENU_HELP_KEY_INPUT: Stored help limit reached. using overflow.")
			EXIT
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_HELP_KEY_INPUT: Stored help limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Set non-clickable
	CLEAR_BIT(g_sMenuData.iHelpKeyIsClickableBits, g_sMenuData.iHelpCount)

	// Store the values
	g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey
	g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = paramLabel
	g_sMenuData.iHelpTextINT[g_sMenuData.iHelpCount] = paramINT
	g_sMenuData.caHelpTextInput[g_sMenuData.iHelpCount] = paramInput				// Stores the input that the control represents
	g_sMenuData.caHelpTextInputGroup[g_sMenuData.iHelpCount] = MAX_INPUTGROUPS		// If the control method changes, the string will be recalculated from this
	g_sMenuData.iHelpCount++

ENDPROC

/// PURPOSE: Checks if the chosen help key exists
FUNC BOOL DOES_HELP_KEY_INPUT_EXIST(CONTROL_ACTION paramInput, STRING paramLabel)
	INT i
	BOOL bFound 
	FOR i=0 TO g_sMenuData.iHelpCount
		IF g_sMenuData.caHelpTextInput[i] = paramInput
			IF ARE_STRINGS_EQUAL(g_sMenuData.tl15HelpText[i], paramLabel)	
				bFound = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bFound
ENDFUNC


/// PURPOSE: Adds a help key to the menu
PROC ADD_MENU_HELP_KEY_GROUP(CONTROL_ACTION_GROUP paramGroupInput, STRING paramLabel, INT paramINT = -1)
	
	STRING paramKey = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, paramGroupInput)
	
	// Ran out of stored variables
	IF (g_sMenuData.iHelpCount >= MAX_STORED_HELP_KEYS)
		
		#IF USE_TU_CHANGES
			// HELP KEY OVERFLOW
			g_sMenuData_TU.eHelpKey = paramKey
			g_sMenuData_TU.tl15HelpText = paramLabel
			g_sMenuData_TU.iHelpTextINT = paramINT
			PRINTLN("ADD_MENU_HELP_KEY_INPUT: Stored help limit reached. using overflow.")
			EXIT
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_HELP_KEY_INPUT: Stored help limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Set non-clickable
	CLEAR_BIT(g_sMenuData.iHelpKeyIsClickableBits, g_sMenuData.iHelpCount)

	// Store the values
	g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey
	g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = paramLabel
	g_sMenuData.iHelpTextINT[g_sMenuData.iHelpCount] = paramINT
	g_sMenuData.caHelpTextInput[g_sMenuData.iHelpCount] = MAX_INPUTS				// Stores the inputgroup that the control represents
	g_sMenuData.caHelpTextInputGroup[g_sMenuData.iHelpCount] = paramGroupInput		// If the control method changes, the string will be recalculated from this
	g_sMenuData.iHelpCount++

ENDPROC

/// PURPOSE: Adds a help key to the menu, clickable with mouse on PC.
PROC ADD_MENU_HELP_KEY_CLICKABLE(CONTROL_ACTION paramInput, STRING paramLabel, INT paramINT = -1, BOOL bIgnoreCursor = FALSE)
	
	STRING paramKey = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, paramInput)
	
	IF (g_sMenuData.iHelpCount >= MAX_STORED_HELP_KEYS)
		
		#IF USE_TU_CHANGES
			// HELP KEY OVERFLOW
			g_sMenuData_TU.eHelpKey = paramKey
			g_sMenuData_TU.tl15HelpText = paramLabel
			g_sMenuData_TU.iHelpTextINT = paramINT
			PRINTLN("ADD_MENU_HELP_KEY_CLICKABLE: Stored help limit reached. using overflow.")
			EXIT
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_HELP_KEY_CLICKABLE: Stored help limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	// Set clickable
	IF NOT bIgnoreCursor
		SET_BIT(g_sMenuData.iHelpKeyIsClickableBits, g_sMenuData.iHelpCount)
	ENDIF

	// Store the values
	g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey
	g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = paramLabel
	g_sMenuData.iHelpTextINT[g_sMenuData.iHelpCount] = paramINT
	g_sMenuData.caHelpTextInput[g_sMenuData.iHelpCount] = paramInput				// Stores the input that the control represents
	g_sMenuData.caHelpTextInputGroup[g_sMenuData.iHelpCount] = MAX_INPUTGROUPS		// If the control method changes, the string will be recalculated from this
	g_sMenuData.iHelpCount++
	
ENDPROC




/// PURPOSE: Adds a help key to the menu
PROC ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS(STRING paramLabel, STRING paramKey1, STRING paramKey2, STRING paramKey3 = NULL, STRING paramKey4 = NULL)
	// Ran out of stored variables
	IF (g_sMenuData.iHelpCount >= MAX_STORED_HELP_KEYS)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS: Stored help limit reached. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	
	PRINTLN( "ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS(", paramLabel,
	", ",paramKey1,
	", ",paramKey2,
	", ",paramKey3,
	", ", paramKey4,")")
	
	// Sore the values
	g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey1
	g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = paramLabel
	g_sMenuData.iHelpCount++
	
	g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey2
	g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = "PREV"
	g_sMenuData.iHelpCount++
	
	IF NOT IS_STRING_NULL_OR_EMPTY(paramKey3)
		g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey3
		g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = "PREV"
		g_sMenuData.iHelpCount++
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(paramKey4)
		g_sMenuData.eHelpKey[g_sMenuData.iHelpCount] = paramKey4
		g_sMenuData.tl15HelpText[g_sMenuData.iHelpCount] = "PREV"
		g_sMenuData.iHelpCount++
	ENDIF
ENDPROC


 
 /// PURPOSE:
 ///    Draws the scroll box highlight when you mouse-over the scroll icons at the bottom of the menu.
 /// PARAMS:
 ///    bIsWardrobe - TRUE if the wardrobe, as this needs to be rendered differently due to the difference in the scrollbox colour.
PROC DRAW_CURSOR_SCROLL_HIGHLIGHT(BOOL bIsWardrobe = FALSE)

	FLOAT fMenuXMin
	FLOAT fMenuYMax
		
	FLOAT fScrollBoxTriggerAreaHeight = CUSTOM_MENU_ITEM_BAR_H / 2
	
	INT iAlpha
	
	IF bIsWardrobe
		iAlpha = 48
	ELSE
		iAlpha = 210
	ENDIF
	
	fMenuXMin = CUSTOM_MENU_X // Default for shops - may have to be changed for other menus.
	fMenuYMax = g_sMenuData.fSetupFinalBodyY
	
	// Set up safe-zone adjust
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
	
	// Highlight scroll up
	IF	g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP
		DRAW_RECT_FROM_CORNER(fMenuXMin, fMenuYMax,CUSTOM_MENU_W, fScrollBoxTriggerAreaHeight, 255, 255, 255, iAlpha )
	// Highlight scroll down
	ELIF g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN
		DRAW_RECT_FROM_CORNER(fMenuXMin, fMenuYMax + fScrollBoxTriggerAreaHeight, CUSTOM_MENU_W, fScrollBoxTriggerAreaHeight, 255, 255, 255, iAlpha )
	ENDIF

	RESET_SCRIPT_GFX_ALIGN()
		
ENDPROC

PROC DISPLAY_DEBUG_FOR_HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
	FLOAT fMenuXMin
	FLOAT fMenuYMin
	FLOAT fMenuXMax
	FLOAT fMenuYMax
	
	fMenuXMin = CUSTOM_MENU_X // Default for shops - may have to be changed for other menus.
	fMenuXMax = fMenuXMin + CUSTOM_MENU_W
	fMenuYMax = g_sMenuData.fSetupFinalBodyY
	fMenuYMin = g_sMenuData.fSetupFinalBodyY - (g_sMenuData.iSetupTotalDisplayRows * CUSTOM_MENU_ITEM_BAR_H )

	IF g_sMenuData.iSetupTotalDisplayRows < 1
		fMenuYMin = g_sMenuData.fSetupFinalBodyY - CUSTOM_MENU_ITEM_BAR_H
	ENDIF
	
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.7, 0.40, "NUMBER", fMenuXMin, 4)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.8, 0.40, "NUMBER", fMenuYMin, 4)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.7, 0.45, "NUMBER", fMenuXMax, 4)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.8, 0.45, "NUMBER", fMenuYMax, 4)
	
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.7, 0.50, "NUMBER", g_sMenuData.fSetupFinalBodyY, 4)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_NUMBER(0.7, 0.55, "NUMBER", g_sMenuData.iSetupTotalDisplayRows)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_NUMBER(0.7, 0.60, "NUMBER", g_sMenuData.iSetupTotalSelectableRows)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_NUMBER(0.7, 0.65, "NUMBER",  g_iMenuCursorItem)
	
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.7, 0.70, "NUMBER", GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X), 4)
	SET_TEXT_SCALE(0.4, 0.4) DISPLAY_TEXT_WITH_FLOAT(0.7, 0.75, "NUMBER", GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y), 4)
	
	DRAW_DEBUG_LINE(<<fMenuXMin, fMenuYMin, 0>>, <<fMenuXMax, fMenuYMin, 0>>)
	DRAW_DEBUG_LINE(<<fMenuXMax, fMenuYMin, 0>>, <<fMenuXMax, fMenuYMax, 0>>)
	DRAW_DEBUG_LINE(<<fMenuXMax, fMenuYMax, 0>>, <<fMenuXMin, fMenuYMax, 0>>)
	DRAW_DEBUG_LINE(<<fMenuXMin, fMenuYMax, 0>>, <<fMenuXMax, fMenuYMin, 0>>)
	
ENDPROC

/// PURPOSE: Adds mouse detection to the shop menu system.
///    Used in combination with functions in menu_cursor.sch
///    This fuinct
///  
///	There are three ways to override default behaviour:
///    bIsGunShopWeaponSelection
///    bDontHighlightFirstItem		- This is used for one of the mp contact menus, that uses the first item as a descriptor, that should not be highlighted when hovered over.
///    bShiftMenuPosDownByOneBar	- When the user of a menu requests the menu system to automatically add an exit option, the system increments the item count correctly, but does not update the Y pos of the bottom of the menu body by one item. This means the highlights are drawn/detected with an offset of one item. Using this overrides shifts them down by one item to compensate.
PROC HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS(BOOL bIsGunShopWeaponSelection = FALSE, BOOL bDontHighlightFirstItem = FALSE, BOOL bShiftMenuPosDownByOneBar = FALSE, BOOL bDrawHighlight = TRUE)

	CONST_INT HIGHLIGHT_ALPHA_MIN	32
	CONST_INT HIGHLIGHT_ALPHA_MAX	180
	
	FLOAT fMenuXMin
	FLOAT fMenuYMin
	FLOAT fMenuXMax
	FLOAT fMenuYMax
	FLOAT fMenuXMinUnAdjusted
	FLOAT fMenuYMinUnadjusted
	
	FLOAT fScrollBoxTriggerAreaHeight = CUSTOM_MENU_ITEM_BAR_H / 2
	
	FLOAT fMouseX
	FLOAT fMouseY
	
	FLOAT fCursorYMenuCoord
	
	INT	iAlpha
	INT	iAlphaRange
	INT iAlphaStep 
	
	INT iHighlightedItem = MENU_CURSOR_NO_ITEM
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
		EXIT
	ENDIF
	
	SET_USE_ADJUSTED_MOUSE_COORDS(TRUE)
	
	fMenuXMin = CUSTOM_MENU_X // Default for shops - may have to be changed for other menus.
	fMenuXMax = fMenuXMin + CUSTOM_MENU_W
	fMenuYMax = g_sMenuData.fSetupFinalBodyY
	fMenuYMin = g_sMenuData.fSetupFinalBodyY - (g_sMenuData.iSetupTotalDisplayRows * CUSTOM_MENU_ITEM_BAR_H )
	
	IF bShiftMenuPosDownByOneBar
		fMenuYMax += CUSTOM_MENU_ITEM_BAR_H
		fMenuYMin += CUSTOM_MENU_ITEM_BAR_H
	ENDIF
	
	// Work-around for the golf menu which sets g_sMenuData.iSetupTotalDisplayRows to -1 when there's only one item to display! 
	IF g_sMenuData.iSetupTotalDisplayRows < 1
		fMenuYMin = g_sMenuData.fSetupFinalBodyY - CUSTOM_MENU_ITEM_BAR_H
	ENDIF
	
	// Set up safe-zone adjust
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
	
	// Store highlight coords un-adjusted, as the safe-zone will automatically move the highlight to the correct coords.
	fMenuXMinUnadjusted = fMenuXMin
	fMenuYMinUnadjusted = fMenuYMin

	// Adjust menu coords for safe-zone, so the mouse detection works.
	GET_SCRIPT_GFX_ALIGN_POSITION( fMenuXMin, fMenuYMin, fMenuXMin, fMenuYMin )
	GET_SCRIPT_GFX_ALIGN_POSITION( fMenuXMax, fMenuYMax, fMenuXMax, fMenuYMax )
	
	// Reset the safe-zone for safety as we have multiple exit points later.
	RESET_SCRIPT_GFX_ALIGN()
	
	//////////////////////////////////////////////
	//     DEBUG - DRAW DEBUG LINES OVER MENU
	//     
//	INT	i
//	FLOAT fMenuItemY
//	
//	SET_TEXT_SCALE(0.2, 0.2)
//	
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	
//	DRAW_DEBUG_LINE_2D(<<fMenuXMin, fMenuYMin,0.0>>,<<fMenuXMax, fMenuYMin,0.0>>)
//	DRAW_DEBUG_LINE_2D(<<fMenuXMax, fMenuYMin,0.0>>,<<fMenuXMax, fMenuYMax,0.0>>)
//	DRAW_DEBUG_LINE_2D(<<fMenuXMax, fMenuYMax,0.0>>,<<fMenuXMin, fMenuYMax,0.0>>)
//	DRAW_DEBUG_LINE_2D(<<fMenuXMin, fMenuYMax,0.0>>,<<fMenuXMin, fMenuYMin,0.0>>)
//	
//	i = 0 
//	WHILE i <  g_sMenuData.iSetupTotalDisplayRows
//	
//		fMenuItemY = fMenuYMin + (i * CUSTOM_MENU_ITEM_BAR_H)
//		DRAW_DEBUG_LINE_2D(<<fMenuXMin, fMenuItemY,0.0>>,<<fMenuXMax, fMenuItemY,0.0>>)
//		
//		++ i
//
//	ENDWHILE
	
	UPDATE_MENU_CURSOR_GLOBALS()
	
	IF g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM
		EXIT
	ENDIF
					
	g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
	
	///////////////////////////////////////////
	
	//GET_MOUSE_POSITION(fMouseX, fMouseY)
	fMouseX = g_fMenuCursorX
	fMouseY = g_fMenuCursorY
	
	//PRINTLN("Mouse: ", fMouseX, " ", fMouseY)
	
	// Check scrolling - if we've got more items in the menu than on-screen then display the scroll icons, and return a value
	// to let the script know the scrolling icon has been highlighted.
	IF g_sMenuData.iSetupTotalSelectableRows > g_sMenuData.iSetupTotalDisplayRows
	
		// Scroll up
		IF 	g_fMenuCursorX >= fMenuXMin
		AND g_fMenuCursorX <= fMenuXMax 
		AND g_fMenuCursorY >= fMenuYMax
		AND g_fMenuCursorY < fMenuYMax + fScrollBoxTriggerAreaHeight
			g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP
			IF bDrawHighlight
				DRAW_CURSOR_SCROLL_HIGHLIGHT()
			ENDIF
			EXIT
		ENDIF
		
		// Scroll down
		IF 	g_fMenuCursorX >= fMenuXMin
		AND g_fMenuCursorX <= fMenuXMax 
		AND g_fMenuCursorY >= fMenuYMax + fScrollBoxTriggerAreaHeight
		AND g_fMenuCursorY < fMenuYMax + CUSTOM_MENU_ITEM_BAR_H
			g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN
			IF bDrawHighlight
				DRAW_CURSOR_SCROLL_HIGHLIGHT()
			ENDIF
			EXIT	
		ENDIF
	
	ENDIF

	// Check if mouse is in the bounding box for the menu
	IF 	fMouseX >= fMenuXMin		
	AND fMouseX <= fMenuXMax 
	AND fMouseY >= fMenuYMin
	AND fMouseY <= fMenuYMax
					
		// Adjust relative Y coord so it's inside the menu detection box.
		fCursorYMenuCoord = fMouseY - fMenuYMin
		iHighlightedItem = FLOOR(fCursorYMenuCoord / CUSTOM_MENU_ITEM_BAR_H )
		
		//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.2, "NUMBER", iHighlightedItem )
		//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.3, "NUMBER", fMouseYMenuCoord / CUSTOM_MENU_ITEM_BAR_H, 2)
		
		// Work-around for Golf which sets the number of menu items to -1 when only one option is available.
		IF  g_sMenuData.iSetupTotalSelectableRows = -1
			g_iMenuCursorItem = 0
			iHighlightedItem = 0
			EXIT
		ENDIF
		
		// Adjust alpha value of menu highlight to take account of the new background gradient.
		iAlphaRange = HIGHLIGHT_ALPHA_MAX - HIGHLIGHT_ALPHA_MIN
		iAlphaStep = iAlphaRange / g_sMenuData.iSetupTotalDisplayRows
		
		iAlpha = HIGHLIGHT_ALPHA_MIN + (iAlphaRange - (iAlphaStep * iHighlightedItem))
		
		// Draw highlight with safe-zone adjust
		IF bDrawHighlight
			IF NOT bDontHighlightFirstItem
			OR iHighlightedItem != 0
			
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
				SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
				DRAW_RECT_FROM_CORNER(fMenuXMinUnadjusted, fMenuyMinUnadjusted + (iHighlightedItem * CUSTOM_MENU_ITEM_BAR_H ),CUSTOM_MENU_W, CUSTOM_MENU_ITEM_BAR_H - 0.0015, 255, 255, 255, iAlpha )
				RESET_SCRIPT_GFX_ALIGN()
			
			ENDIF
		ENDIF
		
//		SET_TEXT_SCALE(0.8, 0.8)
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.1, "NUMBER", g_sMenuData.iTopItem )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.2, "NUMBER", g_sMenuData.iLastDisplayItem )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.3, "NUMBER", g_sMenuData.iSetupTotalDisplayRows )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.4, "NUMBER", g_sMenuData.iSetupTotalSelectableRows )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.5, "NUMBER", iHighlightedItem )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.6, "NUMBER", g_sMenuData.iSetupTotalRows )
//		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", g_sMenuData.iDisplayRow[iHighlightedItem])
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.1, "NUMBER", iAlphaRange )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.2, "NUMBER", iAlphaStep )
//		DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.3, "NUMBER", iAlpha )
		
		
		g_iMenuCursorItem = g_sMenuData.iDisplayRow[iHighlightedItem]
		
		EXIT
		
	ENDIF
	
	// All shops except the gun shop prevent selection to the left of the menu.
	IF NOT bIsGunShopWeaponSelection
		// Cursor is on the menu side of the screen - needed for 
		IF fMouseX < fMenuXMax
			g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE
			EXIT
		ENDIF
		
		// Disable mouse camera move over instructional buttons.
		// This is a temp fix.
		IF fMouseY > 0.9
			g_iMenuCursorItem = MENU_CURSOR_INSTRUCTIONAL_BUTTONS
			EXIT
		ENDIF
		
				
	ELSE
		
		// Gun shop should allow selection below the menu, but not above.
		IF (fMouseX < fMenuXMax AND fMouseY < fMenuYMax + 0.25)
			g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE
			EXIT
		ENDIF
	
	ENDIF
		
	g_iMenuCursorItem = MENU_CURSOR_NO_ITEM

ENDPROC

/// PURPOSE:
///    Handles shop style menu items with increment and decrement icons, such as the Darts and Tennis menus.
/// PARAMS:
///    fDecStart - 		Decrement arrow hit-box X start position
///    fDecSize - 		Decrement arrow hit-box X width
///    iMenuSelection - The menu item whose value is being changed
/// RETURNS:
///    0 if no change, -1 for decrement, or +1 for increment.
FUNC INT GET_CURSOR_MENU_ITEM_VALUE_CHANGE( FLOAT fLeftSideClickConfirmValue = 0.0 )

	FLOAT fMenuXMin
	FLOAT fMenuYMin
	FLOAT fMenuXMax
	FLOAT fTemp

	FLOAT fIncStart
	FLOAT fAcceptEnd 
	
	FLOAT fIncSize = 0.020
	
	fMenuXMin = 0.05 // Default for shops - may have to be changed for other menus.
	fMenuXMax = fMenuXMin + CUSTOM_MENU_W
	//fMenuYMax = g_sMenuData.fSetupFinalBodyY
	fMenuYMin = g_sMenuData.fSetupFinalBodyY - (g_sMenuData.iSetupTotalDisplayRows * CUSTOM_MENU_ITEM_BAR_H )
	
	fAcceptEnd = fMenuXMin + fLeftSideClickConfirmValue
	
	// Setup safe-zone
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
	
	// Adjust menu coords for safe-zone, so the mouse detection works.
	GET_SCRIPT_GFX_ALIGN_POSITION( fMenuXMin, fMenuYMin, fMenuXMin, fMenuYMin )
	GET_SCRIPT_GFX_ALIGN_POSITION( fMenuXMax, fTemp, fMenuXMax, fTemp )
	
	// Reset the safe-zone for safety as we have multiple exit points later.
	RESET_SCRIPT_GFX_ALIGN()
			
	//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.2, "NUMBER", iHighlightedItem )
	//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.3, "NUMBER", fMouseYMenuCoord / CUSTOM_MENU_ITEM_BAR_H, 2)
	
	fIncStart = fMenuXMax - fIncSize
	
	// Debug rect to see if we've got the right click area.
	//DRAW_RECT_FROM_CORNER(fMenuXMin, fMenuYMin, fAcceptEnd, g_sMenuData.iSetupTotalDisplayRows * CUSTOM_MENU_ITEM_BAR_H, 0, 255,0,255)
	//DRAW_RECT_FROM_CORNER(fIncStart, fMenuYMin, fIncSize, g_sMenuData.iSetupTotalDisplayRows * CUSTOM_MENU_ITEM_BAR_H, 0, 255,0,255)


	// On Some menus clicking the left side accepts.
	IF	fLeftSideClickConfirmValue > 0.0
		IF 	g_fMenuCursorX >= fMenuXMin AND g_fMenuCursorX < fAcceptEnd
			RETURN -999
		ENDIF
	ENDIF

	// Decrement
	IF 	g_fMenuCursorX >= fMenuXMin AND g_fMenuCursorX < fIncStart
		RETURN -1 // -1
	ENDIF
	
	// Increment
	IF 	g_fMenuCursorX >= fIncStart AND g_fMenuCursorX <= fMenuXMax
		RETURN 1 // +1
	ENDIF
		
	RETURN 0 // No change

ENDFUNC



FUNC BOOL GET_MENU_ID_FOR_THIS_SCRIPT(INT &iMenuID, BOOl bAssignNewID, INT iMenuType)

	IF iMenuType = -1
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
			iMenuType = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
		ENDIF
	ENDIF
	
	TEXT_LABEL_63 tlScriptName = GET_THIS_SCRIPT_NAME()
	tlScriptName += iMenuType
	
	INT iUniqueHash = GET_HASH_KEY(tlScriptName)
	
	INT iID
	INT iFreeSlot = -1
	REPEAT MAX_MENU_IDS iID
		IF g_sMenuData.iScriptHash[iID] = iUniqueHash
			// Already assigned
			iMenuID = iID
			RETURN TRUE
		ELIF g_sMenuData.iScriptHash[iID] = 0
			iFreeSlot = iID
		ENDIF
	ENDREPEAT
	
	IF bAssignNewID
		IF iFreeSlot != -1
			PRINTLN("GET_MENU_ID_FOR_THIS_SCRIPT - Assigning slot ", iFreeSlot, " to ", GET_THIS_SCRIPT_NAME())
			
			// Not assigned so use free slot
			g_sMenuData.iScriptHash[iFreeSlot] = iUniqueHash
			
			iMenuID = iFreeSlot
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Loads the texture dictionary required for the menu
FUNC BOOL LOAD_MENU_ASSETS(STRING sTextBlockName = NULL, INT iMenuType = -1, BOOL bStreamDiscountTextures = FALSE)
	
	INT iMenuID
	IF NOT GET_MENU_ID_FOR_THIS_SCRIPT(iMenuID, TRUE, iMenuType)
		#IF IS_DEBUG_BUILD
			PRINTLN("LOAD_MENU_ASSETS - Unable to assign id for this menu. Tell Kenneth R.")
			CASSERTLN(DEBUG_SYSTEM, "LOAD_MENU_ASSETS - Unable to assign id for this menu. Tell Kenneth R.")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	BOOL bMenuLoaded = TRUE
	
	g_sMenuData.tlTextBlockName[iMenuID] = sTextBlockName
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuData.tlTextBlockName[iMenuID])
		REQUEST_ADDITIONAL_TEXT(g_sMenuData.tlTextBlockName[iMenuID], MENU_TEXT_SLOT)
		g_sMenuData.bMenuTextRequested[iMenuID] = TRUE
		IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(g_sMenuData.tlTextBlockName[iMenuID], MENU_TEXT_SLOT)
			bMenuLoaded = FALSE
		ENDIF
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
	g_sMenuData.bMenuAssetsRequested[iMenuID] = TRUE
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
		bMenuLoaded = FALSE
	ENDIF
	IF bStreamDiscountTextures
		REQUEST_STREAMED_TEXTURE_DICT("MPShopSale")
		g_sMenuData.bShopDiscountAssetsRequested[iMenuID] = TRUE
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPShopSale")
			bMenuLoaded = FALSE
		ENDIF
	ENDIF
	
	BOOL bHelpLoaded = FALSE
	g_sMenuData.sMenuHelp[iMenuID].filename = "instructional_buttons"
	bHelpLoaded = SETUP_SCALEFORM_MOVIE(g_sMenuData.sMenuHelp[iMenuID])
	
	IF NOT bMenuLoaded 
	OR NOT bHelpLoaded
		PRINTLN("LOAD_MENU_ASSETS - LOADING FOR - ", GET_THIS_SCRIPT_NAME(), ", menu_type=", iMenuType)
	ENDIF
	
	RETURN (bMenuLoaded AND bHelpLoaded)
ENDFUNC

/// PURPOSE: Cleans up the texture dictionary required for the menu
PROC CLEANUP_MENU_ASSETS(BOOL bCleanupScaleformMovie = TRUE, INT iMenuType = -1)

	INT iMenuID
	IF NOT GET_MENU_ID_FOR_THIS_SCRIPT(iMenuID, FALSE, iMenuType)
		EXIT
	ENDIF
	
	IF g_sMenuData.bSubtitlesMoved
		RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
		g_sMenuData.bSubtitlesMoved = FALSE
	ENDIF
	
	THEFEED_SET_SCRIPTED_MENU_HEIGHT(0.0)
	
	DEBUG_PRINTCALLSTACK()
	IF g_sMenuData.bMenuTextRequested[iMenuID]
		PRINTLN("CLEANUP_MENU_ASSETS - CLEANING UP TEXT FOR - ", GET_THIS_SCRIPT_NAME(), ", menu_type=", iMenuType)
		CLEAR_ADDITIONAL_TEXT(MENU_TEXT_SLOT, FALSE)
		g_sMenuData.bMenuTextRequested[iMenuID] = FALSE
	ENDIF
	
	IF g_sMenuData.bMenuAssetsRequested[iMenuID]
		PRINTLN("CLEANUP_MENU_ASSETS - CLEANING UP TXD CommonMenu FOR - ", GET_THIS_SCRIPT_NAME(), ", menu_type=", iMenuType)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("CommonMenu")
		g_sMenuData.bMenuAssetsRequested[iMenuID] = FALSE
	ENDIF
	IF g_sMenuData.bShopDiscountAssetsRequested[iMenuID]
		PRINTLN("CLEANUP_MENU_ASSETS - CLEANING UP TXD MPShopSale FOR - ", GET_THIS_SCRIPT_NAME(), ", menu_type=", iMenuType)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPShopSale")
		g_sMenuData.bShopDiscountAssetsRequested[iMenuID] = FALSE
	ENDIF
	
	IF bCleanupScaleformMovie
		PRINTLN("CLEANUP_MENU_ASSETS - CLEANING UP SCALEFORM FOR - ", GET_THIS_SCRIPT_NAME(), ", menu_type=", iMenuType)
		CLEANUP_SCALEFORM_MOVIE(g_sMenuData.sMenuHelp[iMenuID])
		g_sMenuData.iScriptHash[iMenuID] = 0
	ELSE
		PRINTLN("CLEANUP_MENU_ASSETS - NOT CLEARING SCALEFORM FOR - ", GET_THIS_SCRIPT_NAME(), ", menu_type=", iMenuType)
		g_sMenuData.iScriptHash[iMenuID] = 0
	ENDIF
ENDPROC

/// PURPOSE: Removes all help keys
PROC REMOVE_MENU_HELP_KEYS( INT iMenuType = -1 )
	g_sMenuData.iHelpCount = 0
	g_sMenuData.bHelpCreated = FALSE
	
	INT i
	REPEAT MAX_STORED_HELP_KEYS i
		g_sMenuData.tl15HelpText[i] = ""
		g_sMenuData.iHelpTextINT[i] = -1
		g_sMenuData.caHelpTextInput[i] = MAX_INPUTS
		g_sMenuData.caHelpTextInputGroup[i] = MAX_INPUTGROUPS
	ENDREPEAT
	
	g_sMenuData.iHelpKeyIsClickableBits = 0
	
	#IF USE_TU_CHANGES
		// HELP KEY OVERFLOW
		g_sMenuData_TU.tl15HelpText = ""
		g_sMenuData_TU.iHelpTextINT = -1
	#ENDIF
	
	INT iMenuID
		
	IF IS_PC_VERSION()
		
		IF NOT GET_MENU_ID_FOR_THIS_SCRIPT(iMenuID, FALSE, iMenuType)
			EXIT
		ENDIF
		
//		PRINTLN("REMOVE_MENU_HELP_KEYS - Turning OFF mouse clickable buttons")

		IF HAS_SCALEFORM_MOVIE_LOADED(g_sMenuData.sMenuHelp[iMenuID].movieID)
			BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "TOGGLE_MOUSE_BUTTONS")
	    			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE: Returns TRUE if the menu had been set to draw this frame.
FUNC BOOL IS_CUSTOM_MENU_ON_SCREEN()
	RETURN (GET_GAME_TIMER() <= (g_sMenuData.iLastDrawTimer+100))
ENDFUNC

/// PURPOSE: Returns the final Y coord that the menu was last drawn to
FUNC FLOAT GET_CUSTOM_MENU_FINAL_Y_COORD()
	RETURN (g_sMenuData.fMenuFinalScreenY)
ENDFUNC

/// PURPOSE: Returns the Y coord that the current selected item is positioned at
FUNC FLOAT GET_CUSTOM_MENU_CURRENT_ITEM_Y_COORD()
	RETURN (g_sMenuData.fCurrentItemScreenY)
ENDFUNC

/// PURPOSE: Returns the Y coord that the specified item is positioned at
FUNC FLOAT GET_CUSTOM_MENU_ITEM_Y_COORD(INT paramMenuItem)
	RETURN (g_sMenuData.fMenuItemScreenY[paramMenuItem])
ENDFUNC

/// PURPOSE: Blocks the menu from drawing
PROC DISABLE_CUSTOM_MENU()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("\n DISABLE_CUSTOM_MENU() called by '")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("'")PRINTNL()
	#ENDIF
	g_sMenuData.bDisableMenu = TRUE
ENDPROC
PROC ENABLE_CUSTOM_MENU()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("\n ENABLE_CUSTOM_MENU() called by '")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("'")PRINTNL()
	#ENDIF
	g_sMenuData.bDisableMenu = FALSE
ENDPROC


FUNC BOOL IS_CUSTOM_MENU_SAFE_TO_DRAW(BOOL bAllowDuringPlayerSwitch = FALSE, BOOL bAllowDuringPause = FALSE)

	#IF IS_DEBUG_BUILD
		BOOL bOutputDebug
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bOutputDebug = TRUE
		ENDIF
	#ENDIF

	// Exception for multiplayer if player is on killstrip
	IF MPGlobals.g_KillStrip.iState != STATE_KILL_STRIP_NULL
	
		#IF IS_DEBUG_BUILD
			IF bOutputDebug
				PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : kill strip running")
			ENDIF
		#ENDIF
	
		RETURN TRUE
	ENDIF

	IF NOT IS_SCREEN_FADED_IN()
	OR (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL)
	OR (GET_PAUSE_MENU_STATE() != PM_INACTIVE AND NOT bAllowDuringPause) 
	OR (IS_PLAYER_SWITCH_IN_PROGRESS() AND NOT bAllowDuringPlayerSwitch)  //MP Player Switch is happening.
	OR (IS_COMMERCE_STORE_OPEN())
	OR (g_bResultScreenDisplaying)
	OR (g_sMenuData.bDisableMenu)
	OR (IS_WARNING_MESSAGE_ACTIVE())
	OR (g_sShopSettings.bProcessStoreAlert)
		
		#IF IS_DEBUG_BUILD
			IF bOutputDebug
				IF NOT IS_SCREEN_FADED_IN()
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : screen not faded in")
				ELIF (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL)
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : transition")
				ELIF (GET_PAUSE_MENU_STATE() != PM_INACTIVE AND NOT bAllowDuringPause) 
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : pause menu active")
				ELIF (IS_PLAYER_SWITCH_IN_PROGRESS() AND NOT bAllowDuringPlayerSwitch)  //Player Switch is happening.
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : Player Switch is active")
				ELIF (IS_COMMERCE_STORE_OPEN())
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : commerce store open")
				ELIF (g_bResultScreenDisplaying)
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : result screen displaying")
				ELIF (g_sMenuData.bDisableMenu)
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : disabled")
				ELIF (IS_WARNING_MESSAGE_ACTIVE())
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : warning message active")
				ELSE
					PRINTLN("IS_CUSTOM_MENU_SAFE_TO_DRAW - FALSE : some other reason")
				ENDIF
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC DRAW_MENU_HELP_SCALEFORM(INT iScreenX, INT iMenuType = -1, INT iTimer = 0, BOOL bCheckIfSafeToDraw = TRUE, BOOL bAllowDuringPlayerSwitch = FALSE, BOOL bUseSubStringTime = FALSE, BOOL bPushSubtitlesUp = TRUE, BOOL bDrawForSCTV = TRUE, BOOL bAllowDuringPause = FALSE)//, BOOL bSetWidth = FALSE)

	INT iMenuID
	IF NOT GET_MENU_ID_FOR_THIS_SCRIPT(iMenuID, FALSE, iMenuType)
		#IF IS_DEBUG_BUILD
			PRINTLN("DRAW_MENU_HELP_SCALEFORM - LOAD_MENU_ASSETS not returned TRUE. Tell Kenneth R.")
			CASSERTLN(DEBUG_SYSTEM, "DRAW_MENU_HELP_SCALEFORM - LOAD_MENU_ASSETS not returned TRUE. Tell Kenneth R.")
		#ENDIF
		EXIT
	ENDIF
	iScreenX = iScreenX
	IF bCheckIfSafeToDraw
	AND NOT IS_CUSTOM_MENU_SAFE_TO_DRAW(bAllowDuringPlayerSwitch, bAllowDuringPause)
		EXIT
	ENDIF
	
	// Fix for bug 1592833 - Hide buttons when cellphone is rendering
	IF IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS()
		EXIT
	ENDIF
	
	// Disable instructional buttons if text chat is active. B* 2302619
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF

	
	IF bDrawForSCTV = FALSE
		IF IS_PLAYER_SCTV(PLAYER_ID())
			EXIT
		ENDIF
	ENDIF
	
	// Don't render instructional buttons if keyboard is on-screen. B* 2212715
	IF IS_PC_VERSION()
		IF UPDATE_ONSCREEN_KEYBOARD() = OSK_PENDING
		OR NETWORK_TEXT_CHAT_IS_TYPING() 
			EXIT
		ENDIF
	ENDIF
//	
//	// 2079715
//	IF bSetWidth
//		IF NOT GET_IS_WIDESCREEN()		
//			BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "SET_MAX_WIDTH")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.6)
//				PRINTLN("[2079715] SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.6) ")
//			END_SCALEFORM_MOVIE_METHOD()
//		ENDIF	
//	ENDIF
	
	
	INT i, j
	IF g_sMenuData.iHelpCount != 0
		
		// If control method has changed, refresh the button sprites
		IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			REPEAT g_sMenuData.iHelpCount i
				IF g_sMenuData.caHelpTextInput[i] != MAX_INPUTS
					g_sMenuData.eHelpKey[i] = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, g_sMenuData.caHelpTextInput[i])
					
				ELIF g_sMenuData.caHelpTextInputGroup[i] != MAX_INPUTGROUPS
					g_sMenuData.eHelpKey[i] = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, g_sMenuData.caHelpTextInputGroup[i])	
				ENDIF
			ENDREPEAT
			g_sMenuData.bHelpCreated = FALSE
		ENDIF
		
		IF NOT g_sMenuData.bHelpCreated
			//PRINTLN("setting up menu help")
			BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "CLEAR_ALL")
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "SET_MAX_WIDTH")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.0 - (g_sMenuData.fHelpKeyClearSpace / 100))
			END_SCALEFORM_MOVIE_METHOD()
			
			// Clickable buttons for PC keyboard and mouse
			IF IS_PC_VERSION()
//				PRINTLN("DRAW_MENU_HELP_SCALEFORM - Turning ON mouse clickable buttons")
				BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "TOGGLE_MOUSE_BUTTONS")
    				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
			
			/*BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "SET_CLEAR_SPACE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((iScreenX/100) * g_sMenuData.fHelpKeyClearSpace)
			END_SCALEFORM_MOVIE_METHOD()*/
			
			REPEAT g_sMenuData.iHelpCount i
			
				IF GET_HASH_KEY(g_sMenuData.tl15HelpText[i]) != GET_HASH_KEY("PREV")
				
//					PRINTLN("SHOP MENU: PROCESSING INSTRUCTIONAL BUTTON ", i)
				
					BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(g_sMenuData.eHelpKey[i])
						
						j = i+1
						WHILE j < MAX_STORED_HELP_KEYS AND GET_HASH_KEY(g_sMenuData.tl15HelpText[j]) = GET_HASH_KEY("PREV")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(g_sMenuData.eHelpKey[j])
							j++
						ENDWHILE
						
						//If there is no sub string to add
						IF g_sMenuData.iHelpTextINT[i] = -1
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sMenuData.tl15HelpText[i])							
						ELSE
						
							INT iParam = g_sMenuData.iHelpTextINT[i]
							IF iTimer >= 0
								iParam = iTimer
							ENDIF
						
						//There is a sub string to add!
							PRINTLN("IF g_sMenuData.iHelpTextINT[", i, "] = ", g_sMenuData.iHelpTextINT[i])
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_sMenuData.tl15HelpText[i])
								IF bUseSubStringTime
									ADD_TEXT_COMPONENT_SUBSTRING_TIME(iParam, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
								ELSE
									ADD_TEXT_COMPONENT_INTEGER(iParam)								
								ENDIF
							END_TEXT_COMMAND_SCALEFORM_STRING()
						ENDIF
						
						// Mouse Clickable buttons
						IF IS_PC_VERSION()
							// Store valid input
							IF g_sMenuData.caHelpTextInput[i] != MAX_INPUTS
							AND IS_BIT_SET(g_sMenuData.iHelpKeyIsClickableBits, i)
								//PRINTLN("SHOP MENU: ADDING MOUSE CLICK TO BUTTON ", i)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // CLICKABLE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(g_sMenuData.caHelpTextInput[i]))
							ELSE
							// Not clickable, default value
								//PRINTLN("SHOP MENU: NOT CLICKABLE BUTTON ", i)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE) // NOT CLICKABLE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(MAX_INPUTS))
							ENDIF
									
						ENDIF
					
					END_SCALEFORM_MOVIE_METHOD()
					//PRINTLN("... adding label ", g_sMenuData.tl15HelpText[i])
				ENDIF
			ENDREPEAT
			
			#IF USE_TU_CHANGES
				// HELP KEY OVERFLOW
				IF GET_HASH_KEY(g_sMenuData_TU.tl15HelpText) != GET_HASH_KEY("")
					BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sMenuData.iHelpCount)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(g_sMenuData_TU.eHelpKey)
						
						//If there is no sub string to add
						IF g_sMenuData_TU.iHelpTextINT = -1
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sMenuData_TU.tl15HelpText)							
						ELSE
						
							INT iParam = g_sMenuData.iHelpTextINT[i]
							IF iTimer >= 0
								iParam = iTimer
							ENDIF
						
						//There is a sub string to add!
							PRINTLN("IF g_sMenuData_TU.iHelpTextINT = ", g_sMenuData_TU.iHelpTextINT)
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_sMenuData_TU.tl15HelpText)
								IF bUseSubStringTime
									ADD_TEXT_COMPONENT_SUBSTRING_TIME(iParam, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
								ELSE
									ADD_TEXT_COMPONENT_INTEGER(iParam)								
								ENDIF
							END_TEXT_COMMAND_SCALEFORM_STRING()
						ENDIF
					END_SCALEFORM_MOVIE_METHOD()
					//PRINTLN("... adding label ", g_sMenuData_TU.tl15HelpText)
				ENDIF
			#ENDIF
			
			BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "SET_BACKGROUND_COLOUR")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(80)
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "DRAW_INSTRUCTIONAL_BUTTONS")
				IF g_sMenuData.bStackedKeys
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				ENDIF
			END_SCALEFORM_MOVIE_METHOD()
			
			g_sMenuData.bHelpCreated = TRUE
		ENDIF
		
		
		//PRINTLN("IF g_sMenuData.iHelpTextINT[", 0, "] = ", g_sMenuData.iHelpTextINT[0])
		
		REPEAT g_sMenuData.iHelpCount i
			IF g_sMenuData.iHelpTextINT[i] != -1
			//	PRINTLN("IF g_sMenuData.iHelpTextINT[", i, "] = ", g_sMenuData.iHelpTextINT[i])
				IF iTimer > 0
					//PRINTLN("iTimer = ", iTimer)
					BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "OVERRIDE_RESPAWN_TEXT")
				        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
				        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_sMenuData.tl15HelpText[i])
							IF bUseSubStringTime
								ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimer, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
							ELSE
								ADD_TEXT_COMPONENT_INTEGER(iTimer)
							ENDIF
						END_TEXT_COMMAND_SCALEFORM_STRING()
			   		END_SCALEFORM_MOVIE_METHOD()
					//PRINTLN("END_SCALEFORM_MOVIE_METHOD")
				ENDIF
			ENDIF
		ENDREPEAT
		
		#IF USE_TU_CHANGES
			// HELP KEY OVERFLOW
			IF g_sMenuData_TU.iHelpTextINT != -1
				IF iTimer > 0
					BEGIN_SCALEFORM_MOVIE_METHOD(g_sMenuData.sMenuHelp[iMenuID].movieID, "OVERRIDE_RESPAWN_TEXT")
				        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
				        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_sMenuData_TU.tl15HelpText)
							IF bUseSubStringTime
								ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimer, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
							ELSE
								ADD_TEXT_COMPONENT_INTEGER(iTimer)
							ENDIF
						END_TEXT_COMMAND_SCALEFORM_STRING()
			   		END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDIF
		#ENDIF
		
		
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_BOTTOM)
		SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
		
		IF bPushSubtitlesUp
			IF NOT g_sMenuData.bSubtitlesMoved
				SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0375)
				g_sMenuData.bSubtitlesMoved = TRUE
			ENDIF
		ELSE
			IF g_sMenuData.bSubtitlesMoved
				RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
				g_sMenuData.bSubtitlesMoved = FALSE
			ENDIF
		ENDIF
		
		RESET_SCRIPT_GFX_ALIGN()
		
		IF g_sMenuData.bUseTempKeyCoords
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
			SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
				DRAW_SCALEFORM_MOVIE(g_sMenuData.sMenuHelp[iMenuID].movieID, g_sMenuData.fHelpKeysX, g_sMenuData.fHelpKeysY, 1.0, 1.0, 255, 255, 255, 255)
			RESET_SCRIPT_GFX_ALIGN()
		ELSE
			//DRAW_SCALEFORM_MOVIE(g_sMenuData.sMenuHelp[iMenuID].movieID, 0.550, 0.569, 1.0, 1.0, 255, 255, 255, 255) // old x/y = 0.550, 0.546
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_sMenuData.sMenuHelp[iMenuID].movieID, 255, 255, 255, 255)
		ENDIF
	ENDIF
	
	
ENDPROC

/// PURPOSE: Render the menu to the screen
///    NOTE: Use the iMenuType param to make sure the scaleform movie is loaded for the correct menu.
PROC DRAW_MENU(BOOL bSetButtonsUnderHud = TRUE, INT iMenuType = -1, BOOL bHideHelpText = TRUE, BOOL bAddDefaultExitOption = FALSE, BOOL bPushSubtitlesUp = TRUE, FLOAT fCustomWidth = -1.0, BOOL bAllowDuringPause = FALSE, BOOL bUseActualRes = FALSE, INT iTimer = -1)

	INT iMenuID
	IF NOT GET_MENU_ID_FOR_THIS_SCRIPT(iMenuID, FALSE, iMenuType)	
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "DRAW_MENU - LOAD_MENU_ASSETS not returned TRUE. Tell Kenneth R.")
		#ENDIF
		PRINTLN("DRAW_MENU - LOAD_MENU_ASSETS not returned TRUE. Tell Kenneth R.")
		EXIT
	ENDIF

	IF iMenuID = -1
		#IF IS_DEBUG_BUILD
			PRINTLN("DRAW_MENU() - Script '", GET_THIS_SCRIPT_NAME(), "' with menu_type=", iMenuType, " has not called LOAD_MENU_ASSETS()")
			CASSERTLN(DEBUG_SYSTEM, "LOAD_MENU_ASSETS() not called. Pass logs to Kenneth R.")
			EXIT
		#ENDIF
	ENDIF
	
	IF NOT IS_CUSTOM_MENU_SAFE_TO_DRAW(DEFAULT, bAllowDuringPause)
		PRINTLN("DRAW_MENU - Custom menu not safe to draw - EXIT")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Do not display when we have debug options on screen
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON))
		OR (g_debugMenuControl.bDebugMenuOnScreen)		
			IF IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
				PRINTLN("DRAW_MENU - IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON) - is TRUE")
			ENDIF
			IF g_debugMenuControl.bDebugMenuOnScreen
				PRINTLN("DRAW_MENU - g_debugMenuControl.bDebugMenuOnScreen - is TRUE")
			ENDIF
			PRINTLN("DRAW_MENU - Debug menu on screen - EXIT")
			EXIT
		ENDIF
	#ENDIF
	
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
	
	INT iHudR, iHudG, iHudB, iHudA
	INT iPass, iRow, iRowLoop, iItem, iDisplayRow, iTotalRows, iTotalSelectableRows, iCurrentDisplaySpacer, iSpacerCount
	INT i, iIntParam, iFloatParam, iTextParam, iPlayerNameParam
	INT iTextCount, iIntCount, iFloatCount, iIconCount, iPlayerNameCount
	INT iThisTextItem, iTempTextCount, iTempIntCount, iTempFloatCount, iTempIconCount, iTempPlayerNameCount, iThisTextItemActual
	MENU_TEXT_COMP_TYPE eLastCompType
	BOOL bSelected, bDisplay
	FLOAT fTextX, fTextY
	FLOAT fWidth, fHeight
	VECTOR vTexture
	BOOL bItemAdded
	FLOAT fTempWidth, fTempTextWidth, fTempIconWidth, fTempToggleWidth, fTempColumnWidth
	INT iIconR, iIconG, iIconB, iIconA
	FLOAT fFinalPanelY
	BOOL bCondensed, bPlayerName
	BOOL bAddToggleItems
	BOOL bSetCarColour
	FLOAT fMenuItemBarHeight
	
	FLOAT fHeaderGraphicHeight = 0.0
	FLOAT fHeaderGraphicAspectRatio
	
	IF g_sMenuData.bUseHeaderGraphic
		IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_HEADER, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
			fHeaderGraphicAspectRatio = fHeight/fWidth
		ELSE
			g_sMenuData.bUseHeaderGraphic = FALSE
		ENDIF
	ENDIF
	
	IF fCustomWidth = -1.0
		fCustomWidth = CUSTOM_MENU_W
	ENDIF
	
	//If we're overriding the header graphic width with fCutsomWidth we also need to make sure it's height is altered 
	//to match and preserve the correct aspect ratio.
	fHeaderGraphicHeight = fCustomWidth*fHeaderGraphicAspectRatio
	
	FLOAT fBodyStartY
	IF GET_HASH_KEY(g_sMenuData.tl15Title) = GET_HASH_KEY("HIDE")
		fBodyStartY = (CUSTOM_MENU_Y)
	ELSE
		fBodyStartY = (CUSTOM_MENU_Y + fHeaderGraphicHeight + CUSTOM_MENU_HEADER_H + CUSTOM_MENU_WHITE_LINE_H)
	ENDIF
	
	//	B* 2147964 - Gets the physical screen aspect and compares to fake aspect ratio to get a multiplier
	INT iScreenX, iScreenY
	FLOAT fAspectMulti = 1.0
	GET_SCREEN_RESOLUTION_FOR_MENU(bUseActualRes, iScreenX, iScreenY, fAspectMulti)
	
	// Add a default row with Exit text whenever we have 1 item or less.
	IF bAddDefaultExitOption
		IF g_sMenuData.iCurrentRow <= 1
			ADD_MENU_ITEM_TEXT(g_sMenuData.iCurrentRow+1, "DFLT_MNU_OPT")
			g_sMenuData.bDefaultOptionAdded = TRUE
		ENDIF
	ENDIF
		
	// We currently do 2 passes so we can draw the background before we draw text/icons.
	// To cut down processing we should do a one off setup and then just the 1 pass.
	
	REPEAT 2 iPass
	
		// Only draw the backgrounds when the initial set up has been done as 
		// we need to work out the background size/footer offset.
		IF iPass = 1
		AND g_sMenuData.bSetupComplete
		
			//////////////////////////////////////////////////////////////////////////////////////////
			///       HEADER
			///       
			IF GET_HASH_KEY(g_sMenuData.tl15Title) = GET_HASH_KEY("HIDE")
				fFinalPanelY = (CUSTOM_MENU_Y)
			ELSE
				IF g_sMenuData.bUseHeaderGraphic
				
					TEXT_LABEL_63 tlTXD = GET_MENU_ICON_TXD(MENU_ICON_HEADER)
					TEXT_LABEL_63 tlName = GET_MENU_ICON_TEXTURE(MENU_ICON_HEADER, TRUE)
					
					// Crew logo?
					IF GET_HASH_KEY(g_sMenuData.sIconTextureOverride[MENU_ICON_HEADER]) = HASH("CREW_LOGO")
						DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, CUSTOM_MENU_Y, fCustomWidth, fHeaderGraphicHeight, 0, 0, 0, 255) // Black background
						DRAW_SPRITE(tlTXD, tlName, CUSTOM_MENU_X+(fCustomWidth*0.5), CUSTOM_MENU_Y+(fHeaderGraphicHeight*0.5), fCustomWidth, fHeaderGraphicHeight, 0.0, 255, 255, 255, 255)
					ELSE
						DRAW_SPRITE(tlTXD, tlName, CUSTOM_MENU_X+(fCustomWidth*0.5), CUSTOM_MENU_Y+(fHeaderGraphicHeight*0.5), fCustomWidth, fHeaderGraphicHeight, 0.0, 255, 255, 255, 255)
					ENDIF
				ENDIF
				
				IF g_sMenuData.bUseCustomHeaderColour
					iHudR = g_sMenuData.iHeaderR
					iHudG = g_sMenuData.iHeaderG
					iHudB = g_sMenuData.iHeaderB
					iHudA = g_sMenuData.iHeaderA
				ELSE
					iHudR = 0
					iHudG = 0
					iHudB = 0
					iHudA = tiCOMMON_MENU_H_ALPHA
				ENDIF
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, CUSTOM_MENU_Y+fHeaderGraphicHeight, fCustomWidth, CUSTOM_MENU_HEADER_H, iHudR, iHudG, iHudB, iHudA) // Black panel
				//DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, CUSTOM_MENU_Y+fHeaderGraphicHeight+CUSTOM_MENU_HEADER_H, CUSTOM_MENU_W, CUSTOM_MENU_WHITE_LINE_H, 255, 255, 255, 255) // White line
				
				fFinalPanelY = (CUSTOM_MENU_Y) + (fHeaderGraphicHeight) + (CUSTOM_MENU_HEADER_H) + (CUSTOM_MENU_WHITE_LINE_H)
				
				IF GET_HASH_KEY(g_sMenuData.tl15Title) != 0
					// Add text
					SETUP_MENU_HEADING_TEXT()
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMenuData.tl15Title)
						iIntParam = 0
						iFloatParam = 0
						iTextParam = 0
						iPlayerNameParam = 0
						REPEAT g_sMenuData.iTitleTotalParams i
							IF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_INT
								ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iTitleInt[iIntParam])
								iIntParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_FLOAT
								ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fTitleFloat[iFloatParam], g_sMenuData.iTitleFloatDP[iFloatParam])
								iFloatParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_TEXT
								ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15TitleText[iTextParam])
								iTextParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_VEHICLE_NAME
								ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15TitleText[iTextParam])
								iTextParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_PLAYER_NAME
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
								iPlayerNameParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_RADIO_STATION
								ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
								iPlayerNameParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_LITERAL
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
								iPlayerNameParam++
							ELIF g_sMenuData.eTitleComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tlTitlePlayerName[iPlayerNameParam])
								iPlayerNameParam++
							ENDIF
						ENDREPEAT
					END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TITLE_TEXT_INDENT_X, CUSTOM_MENU_Y+fHeaderGraphicHeight+CUSTOM_MENU_TITLE_TEXT_INDENT_Y)
				ENDIF
				
				IF g_sMenuData.bOverrideTitleRowCounts
					SETUP_MENU_HEADING_TEXT()
					DISPLAY_TEXT_WITH_2_NUMBERS(CUSTOM_MENU_X+fCustomWidth-CUSTOM_MENU_TITLE_TEXT_INDENT_X-GET_MENU_ITEM_COUNT_WIDTH("CM_ITEM_COUNT", g_sMenuData.iTitleRowOverride1, g_sMenuData.iTitleRowOverride2), CUSTOM_MENU_Y+fHeaderGraphicHeight+CUSTOM_MENU_TITLE_TEXT_INDENT_Y, "CM_ITEM_COUNT", g_sMenuData.iTitleRowOverride1, g_sMenuData.iTitleRowOverride2)
				ELIF g_sMenuData.iSetupTotalRows > g_sMenuData.iMenuRows
					IF g_sMenuData.iSetupCurrentSelectableItem != 0
						SETUP_MENU_HEADING_TEXT()
						DISPLAY_TEXT_WITH_2_NUMBERS(CUSTOM_MENU_X+fCustomWidth-CUSTOM_MENU_TITLE_TEXT_INDENT_X-GET_MENU_ITEM_COUNT_WIDTH("CM_ITEM_COUNT", g_sMenuData.iSetupCurrentSelectableItem, g_sMenuData.iSetupTotalSelectableRows), CUSTOM_MENU_Y+fHeaderGraphicHeight+CUSTOM_MENU_TITLE_TEXT_INDENT_Y, "CM_ITEM_COUNT", g_sMenuData.iSetupCurrentSelectableItem, g_sMenuData.iSetupTotalSelectableRows)
					ENDIF
				ENDIF
			ENDIF
			
			
			//////////////////////////////////////////////////////////////////////////////////////////
			///       BODY
			///       
			iRow = g_sMenuData.iTopItem
			iDisplayRow = 0
			
			// [TART UP]
			FLOAT fGradientYPos = fFinalPanelY
			
			IF g_sMenuData.bUseCustomBodyColour
				iHudR = g_sMenuData.iBodyR
				iHudG = g_sMenuData.iBodyG
				iHudB = g_sMenuData.iBodyB
				iHudA = g_sMenuData.iBodyA
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_INGAME_BG, iHudR, iHudG, iHudB, iHudA)
			ENDIF
			
			WHILE iDisplayRow < g_sMenuData.iMenuRows
			AND iRow <= g_sMenuData.iCurrentRow
				IF iRow >= 0
					IF g_sMenuData.bMenuRowHasDisplayItems[iRow]
						IF g_sMenuData.bMenuRowHasSpacer[iRow]
						AND iRow != g_sMenuData.iTopItem
							fFinalPanelY += CUSTOM_MENU_SPACER_H
						ENDIF
						
						fMenuItemBarHeight = CUSTOM_MENU_ITEM_BAR_H
						IF g_sMenuData.fRowHeight[iRow] != 0.0
							fMenuItemBarHeight = g_sMenuData.fRowHeight[iRow]
						ENDIF
						
						// [TART UP]
						//DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY, fCustomWidth, fMenuItemBarHeight, iHudR, iHudG, iHudB, iHudA) // Black panel
						fFinalPanelY += fMenuItemBarHeight
						iDisplayRow++
					ENDIF
				ENDIF
				iRow++
			ENDWHILE
			
			// [TART UP]
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+(fCustomWidth*0.5), fGradientYPos+((fFinalPanelY-fGradientYPos)*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT), fCustomWidth, (fFinalPanelY-fGradientYPos), 0.0, 255, 255, 255, 255)
			/*fWidth = (fCustomWidth / 6)
			fHeight = (fFinalPanelY-fGradientYPos)
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*0)), fGradientYPos+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*1)), fGradientYPos+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*2)), fGradientYPos+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*3)), fGradientYPos+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*4)), fGradientYPos+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
			DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*5)), fGradientYPos+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)*/
			
			
			//////////////////////////////////////////////////////////////////////////////////////////
			///       FOOTER
			///       
			IF g_sMenuData.iSetupTotalRows > g_sMenuData.iMenuRows
//			or g_sMenuData.bForceFooter
				IF g_sMenuData.bUseCustomFooterColour
					iHudR = g_sMenuData.iFooterR
					iHudG = g_sMenuData.iFooterG
					iHudB = g_sMenuData.iFooterB
					iHudA = g_sMenuData.iFooterA
				ELSE
					iHudR = 0
					iHudG = 0
					iHudB = 0
					iHudA = tiCOMMON_MENU_F_ALPHA
				ENDIF
			
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY+CUSTOM_MENU_WHITE_LINE_H, fCustomWidth, CUSTOM_MENU_FOOTER_H, iHudR, iHudG, iHudB, iHudA) // Black panel
				vTexture = GET_TEXTURE_RESOLUTION("CommonMenu", "shop_arrows_upANDdown")
				
				// Fix for bug # 1859693 - Reducing size as textures were doubled in size
				vTexture.x *= ( 0.5 / fAspectMulti )
				vTexture.Y *= ( 0.5 / fAspectMulti )
				
				IF g_sMenuData.bUseInvertedScrollColour
					iHudR = 0
					iHudG = 0
					iHudB = 0
					iHudA = 255
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_WHITE, iHudR, iHudG, iHudB, iHudA)
				ENDIF
				
				DRAW_SPRITE("CommonMenu", "shop_arrows_upANDdown", CUSTOM_MENU_X+(fCustomWidth*0.5), fFinalPanelY+CUSTOM_MENU_WHITE_LINE_H+(CUSTOM_MENU_FOOTER_H*0.5), (vTexture.X / 1280 * fAspectMulti), (vTexture.Y / 720 * fAspectMulti), 0.0, iHudR, iHudG, iHudB, iHudA)
				
				
//				DRAW_SPRITE("CommonMenu", "shop_arrows_upANDdown", CUSTOM_MENU_X+CUSTOM_MENU_W-CUSTOM_MENU_TITLE_TEXT_INDENT_X-((vTexture.X / iScreenX)*0.5), fFinalPanelY+CUSTOM_MENU_WHITE_LINE_H+(CUSTOM_MENU_FOOTER_H*0.5), (vTexture.X / iScreenX), (vTexture.Y / iScreenY), 0.0, 255, 255, 255, 255)
//				
//				IF g_sMenuData.iSetupCurrentSelectableItem != 0
//					SETUP_MENU_ITEM_TEXT(FALSE)
//					DISPLAY_TEXT_WITH_2_NUMBERS(CUSTOM_MENU_X+CUSTOM_MENU_TITLE_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_WHITE_LINE_H+CUSTOM_MENU_TITLE_TEXT_INDENT_Y, "CM_ITEM_COUNT", g_sMenuData.iSetupCurrentSelectableItem, g_sMenuData.iSetupTotalSelectableRows)
//				ENDIF
				fFinalPanelY += (CUSTOM_MENU_WHITE_LINE_H) + (CUSTOM_MENU_FOOTER_H)
			ENDIF
			
			
			//////////////////////////////////////////////////////////////////////////////////////////
			///       DESCRIPTION MESSAGE BOX
			///       
			IF GET_HASH_KEY(g_sMenuData.tl23Desc) != 0
			AND g_sMenuData.iDescClearTimer != -1
				
				// Add box offset
				fFinalPanelY += (CUSTOM_MENU_MESSAGE_OFFSET_Y*2)
				
				fTempWidth = CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X
				IF g_sMenuData.eDescIcon != MENU_ICON_DUMMY
					GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eDescIcon, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
					fTempWidth = CUSTOM_MENU_X+(fWidth)+(CUSTOM_MENU_PIXEL_WIDTH*4)-(CUSTOM_MENU_PIXEL_WIDTH*1)
				ENDIF
				
				// Draw description box
				SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
				BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(g_sMenuData.tl23Desc)
					iIntParam = 0
					iFloatParam = 0
					iTextParam = 0
					REPEAT g_sMenuData.iDescTotalParams i
						IF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_INT
							ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iDescInt[iIntParam])
							iIntParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_FLOAT
							ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fDescFloat[iFloatParam], g_sMenuData.iDescFloatDP[iFloatParam])
							iFloatParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_TEXT
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_PLAYER_NAME
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_RADIO_STATION
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ENDIF
					ENDREPEAT
				iRow = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
				
				// [TART UP]
				//GET_HUD_COLOUR(HUD_COLOUR_WHITE, iHudR, iHudG, iHudB, iHudA)	
				GET_HUD_COLOUR(HUD_COLOUR_BLACK, iHudR, iHudG, iHudB, iHudA)	
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY-CUSTOM_MENU_MESSAGE_OFFSET_Y, fCustomWidth, CUSTOM_MENU_MESSAGE_OFFSET_Y, iHudR, iHudG, iHudB, iHudA)
								
				IF g_sMenuData.bUseCustomHelpColour
					iHudR = g_sMenuData.iHelpR
					iHudG = g_sMenuData.iHelpG
					iHudB = g_sMenuData.iHelpB
					iHudA = g_sMenuData.iHelpA
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_INGAME_BG, iHudR, iHudG, iHudB, iHudA)	
				ENDIF
				// [TART UP]
				//DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY, fCustomWidth, ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)), iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+(fCustomWidth*0.5), fFinalPanelY+(((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1))*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT), fCustomWidth, ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)), 0.0, iHudR, iHudG, iHudB, iHudA)
				/*fWidth = (fCustomWidth / 6)
				fHeight = ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1))
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*0)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*1)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*2)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*3)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*4)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*5)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)*/
				
				// Add text
				SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMenuData.tl23Desc)
					iIntParam = 0
					iFloatParam = 0
					iTextParam = 0
					REPEAT g_sMenuData.iDescTotalParams i
						IF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_INT
							ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iDescInt[iIntParam])
							iIntParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_FLOAT
							ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fDescFloat[iFloatParam], g_sMenuData.iDescFloatDP[iFloatParam])
							iFloatParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_TEXT
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_PLAYER_NAME	
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_RADIO_STATION
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_VEHICLE_NAME
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ENDIF
					ENDREPEAT
				END_TEXT_COMMAND_DISPLAY_TEXT(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
				
				IF g_sMenuData.eDescIcon != MENU_ICON_DUMMY
					GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eDescIcon, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
					GET_MENU_ICON_TEXTURE_RGB(g_sMenuData.eDescIcon, TRUE, iIconR, iIconG, iIconB, iIconA)
					DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eDescIcon), GET_MENU_ICON_TEXTURE(g_sMenuData.eDescIcon, TRUE), CUSTOM_MENU_X+(fWidth*0.5)+(CUSTOM_MENU_PIXEL_WIDTH*2), fFinalPanelY+(fHeight*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT*4), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
				ENDIF
				
				
				fFinalPanelY += ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+(CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)))
				
				// Draw description box (EXTRA)
				IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuData.tl23DescExtra)
					
					fFinalPanelY += (CUSTOM_MENU_PIXEL_HEIGHT*6)
					
					SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
					BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(g_sMenuData.tl23DescExtra)				
					iRow = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
					
					GET_HUD_COLOUR(HUD_COLOUR_BLACK, iHudR, iHudG, iHudB, iHudA)	
					DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY-CUSTOM_MENU_MESSAGE_OFFSET_Y, fCustomWidth, CUSTOM_MENU_MESSAGE_OFFSET_Y, iHudR, iHudG, iHudB, iHudA)
					
					IF g_sMenuData.bUseCustomHelpColour
						iHudR = g_sMenuData.iHelpR
						iHudG = g_sMenuData.iHelpG
						iHudB = g_sMenuData.iHelpB
						iHudA = g_sMenuData.iHelpA
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_INGAME_BG, iHudR, iHudG, iHudB, iHudA)	
					ENDIF
					
					DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+(fCustomWidth*0.5), fFinalPanelY+(((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1))*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT), fCustomWidth, ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)), 0.0, iHudR, iHudG, iHudB, iHudA)
					
					// Add text
					SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMenuData.tl23DescExtra)				
					END_TEXT_COMMAND_DISPLAY_TEXT(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
					
					/*IF g_sMenuData.eDescIcon != MENU_ICON_DUMMY
						GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eDescIcon, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
						GET_MENU_ICON_TEXTURE_RGB(g_sMenuData.eDescIcon, TRUE, iIconR, iIconG, iIconB, iIconA)
						DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eDescIcon), GET_MENU_ICON_TEXTURE(g_sMenuData.eDescIcon, TRUE), CUSTOM_MENU_X+(fWidth*0.5)+(CUSTOM_MENU_PIXEL_WIDTH*2), fFinalPanelY+(fHeight*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT*4), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
					ENDIF*/
									
					fFinalPanelY += ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+(CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)))				
				ENDIF
				
				// Clear description when time is up
				IF g_sMenuData.iDescClearTimer > 0
					IF (GET_GAME_TIMER()-g_sMenuData.iDescStartTimer) > g_sMenuData.iDescClearTimer
						g_sMenuData.tl23Desc = ""
						g_sMenuData.iDescClearTimer = -1
					ENDIF
				ENDIF
			ELIF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuData.tl23DescExtra)
				fFinalPanelY += (CUSTOM_MENU_MESSAGE_OFFSET_Y*2)
				
				fTempWidth = CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X
								
				// Draw description box
				SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
				BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(g_sMenuData.tl23DescExtra)
					iIntParam = 0
					iFloatParam = 0
					iTextParam = 0
					REPEAT g_sMenuData.iDescTotalParams i
						IF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_INT
							ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iDescInt[iIntParam])
							iIntParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_FLOAT
							ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fDescFloat[iFloatParam], g_sMenuData.iDescFloatDP[iFloatParam])
							iFloatParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_TEXT
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_PLAYER_NAME
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_RADIO_STATION
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ELIF g_sMenuData.eDescComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData.tl15DescText[iTextParam])
							iTextParam++
						ENDIF
					ENDREPEAT
				iRow = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
								
				GET_HUD_COLOUR(HUD_COLOUR_BLACK, iHudR, iHudG, iHudB, iHudA)	
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY-CUSTOM_MENU_MESSAGE_OFFSET_Y, fCustomWidth, CUSTOM_MENU_MESSAGE_OFFSET_Y, iHudR, iHudG, iHudB, iHudA)
								
				IF g_sMenuData.bUseCustomHelpColour
					iHudR = g_sMenuData.iHelpR
					iHudG = g_sMenuData.iHelpG
					iHudB = g_sMenuData.iHelpB
					iHudA = g_sMenuData.iHelpA
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_INGAME_BG, iHudR, iHudG, iHudB, iHudA)	
				ENDIF				
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+(fCustomWidth*0.5), fFinalPanelY+(((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1))*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT), fCustomWidth, ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)), 0.0, iHudR, iHudG, iHudB, iHudA)
								
				// Add text
				SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMenuData.tl23DescExtra)					
				END_TEXT_COMMAND_DISPLAY_TEXT(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			ENDIF
			
			#IF USE_TU_CHANGES
						
			//////////////////////////////////////////////////////////////////////////////////////////
			///       DISCOUNT MESSAGE BOX
			///       
			IF GET_HASH_KEY(g_sMenuData_TU.tl15Discount) != 0
			AND g_sMenuData_TU.iDiscountClearTimer != -1
				
				// Add box offset
				fFinalPanelY += (CUSTOM_MENU_MESSAGE_OFFSET_Y*2)
				
				fTempWidth = CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X
				IF g_sMenuData_TU.eDiscountIcon != MENU_ICON_DUMMY
					GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData_TU.eDiscountIcon, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
					fTempWidth = CUSTOM_MENU_X+(fWidth)+(CUSTOM_MENU_PIXEL_WIDTH*4)-(CUSTOM_MENU_PIXEL_WIDTH*1)
				ENDIF
				
				// Draw Discount box
				SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
				BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(g_sMenuData_TU.tl15Discount)
					iIntParam = 0
					iFloatParam = 0
					iTextParam = 0
					REPEAT g_sMenuData_TU.iDiscountTotalParams i
						IF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_INT
							ADD_TEXT_COMPONENT_INTEGER(g_sMenuData_TU.iDiscountInt[iIntParam])
							iIntParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_FLOAT
							ADD_TEXT_COMPONENT_FLOAT(g_sMenuData_TU.fDiscountFloat[iFloatParam], g_sMenuData_TU.iDiscountFloatDP[iFloatParam])
							iFloatParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_TEXT
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_PLAYER_NAME
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_RADIO_STATION
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_VEHICLE_NAME
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ENDIF
					ENDREPEAT
				iRow = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
				
				// [TART UP]
				//GET_HUD_COLOUR(HUD_COLOUR_WHITE, iHudR, iHudG, iHudB, iHudA)	
				GET_HUD_COLOUR(HUD_COLOUR_BLACK, iHudR, iHudG, iHudB, iHudA)	
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY-CUSTOM_MENU_MESSAGE_OFFSET_Y, fCustomWidth, CUSTOM_MENU_MESSAGE_OFFSET_Y, iHudR, iHudG, iHudB, iHudA)
				
				IF g_sMenuData.bUseCustomHelpColour
					iHudR = g_sMenuData.iHelpR
					iHudG = g_sMenuData.iHelpG
					iHudB = g_sMenuData.iHelpB
					iHudA = g_sMenuData.iHelpA
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_INGAME_BG, iHudR, iHudG, iHudB, iHudA)	
				ENDIF
				// [TART UP]
				//DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fFinalPanelY, fCustomWidth, ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)), iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+(fCustomWidth*0.5), fFinalPanelY+(((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1))*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT), fCustomWidth, ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)), 0.0, iHudR, iHudG, iHudB, iHudA)
				/*fWidth = (fCustomWidth / 6)
				fHeight = ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1))
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*0)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*1)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*2)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*3)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*4)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)
				DRAW_SPRITE("CommonMenu", "Gradient_Bgd", CUSTOM_MENU_X+((fWidth*0.5)+(fWidth*5)), fFinalPanelY+(fHeight*0.5), fWidth, fHeight, 0.0, iHudR, iHudG, iHudB, iHudA)*/
				
				// Add text
				SETUP_MENU_ITEM_MESSAGE_TEXT(fTempWidth)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMenuData_TU.tl15Discount)
					iIntParam = 0
					iFloatParam = 0
					iTextParam = 0
					REPEAT g_sMenuData_TU.iDiscountTotalParams i
						IF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_INT
							ADD_TEXT_COMPONENT_INTEGER(g_sMenuData_TU.iDiscountInt[iIntParam])
							iIntParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_FLOAT
							ADD_TEXT_COMPONENT_FLOAT(g_sMenuData_TU.fDiscountFloat[iFloatParam], g_sMenuData_TU.iDiscountFloatDP[iFloatParam])
							iFloatParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_TEXT
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_VEHICLE_NAME
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_PLAYER_NAME	
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_RADIO_STATION
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ELIF g_sMenuData_TU.eDiscountComps[i] = MENU_TEXT_COMP_CONDENSED_LITERAL
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sMenuData_TU.tl15DiscountText[iTextParam])
							iTextParam++
						ENDIF
					ENDREPEAT
				END_TEXT_COMMAND_DISPLAY_TEXT(fTempWidth, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
				
				IF g_sMenuData_TU.eDiscountIcon != MENU_ICON_DUMMY
					GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData_TU.eDiscountIcon, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
					GET_MENU_ICON_TEXTURE_RGB(g_sMenuData_TU.eDiscountIcon, TRUE, iIconR, iIconG, iIconB, iIconA)
					DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData_TU.eDiscountIcon), GET_MENU_ICON_TEXTURE(g_sMenuData_TU.eDiscountIcon, TRUE), CUSTOM_MENU_X+(fWidth*0.5)+(CUSTOM_MENU_PIXEL_WIDTH*2), fFinalPanelY+(fHeight*0.5)-(CUSTOM_MENU_PIXEL_HEIGHT*4), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
				ENDIF
				
				
				fFinalPanelY += ((GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y)*iRow)+(CUSTOM_MENU_PIXEL_HEIGHT*13)+(CUSTOM_MENU_PIXEL_HEIGHT*5*(iRow-1)))
				
				// Clear Discount when time is up
				IF g_sMenuData_TU.iDiscountClearTimer > 0
					IF (GET_GAME_TIMER()-g_sMenuData_TU.iDiscountStartTimer) > g_sMenuData_TU.iDiscountClearTimer
						g_sMenuData_TU.tl15Discount = ""
						g_sMenuData_TU.iDiscountClearTimer = -1
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
						
			//////////////////////////////////////////////////////////////////////////////////////////
			///       HELP KEYS
			///  
			DRAW_MENU_HELP_SCALEFORM(iScreenX, iMenuType, iTimer, FALSE, FALSE, FALSE, bPushSubtitlesUp)    
			
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
		
		ENDIF
		
		
		//////////////////////////////////////////////////////////////////////////////////////////
		///       MENU ITEMS
		///       
		IF iPass = 1
		OR NOT g_sMenuData.bSetupComplete
		
			iTextCount = 0
			iPlayerNameCount = 0
			iIntCount = 0
			iFloatCount = 0
			iIconCount = 0
			iDisplayRow = 0
			iTotalRows = 0
			iTotalSelectableRows = 0
			iCurrentDisplaySpacer = 0
			iSpacerCount = 0
			
			INT iRowTo = g_sMenuData.iCurrentRow
			IF g_sMenuData.bDisplayRowsDefined
				iRowTo = g_sMenuData.iSetupTotalDisplayRows-1
			ENDIF
			
			FLOAT fHeightTrack = 0.0
			FLOAT fSelectectedHeightTrack = 0.0
			
			INT iColumnShift
			
			FOR iRowLoop = 0 TO iRowTo
			
				fMenuItemBarHeight = CUSTOM_MENU_ITEM_BAR_H
				IF g_sMenuData.fRowHeight[iRow] != 0.0
					fMenuItemBarHeight = g_sMenuData.fRowHeight[iRow]
				ENDIF
			
				// Using display row lookup and item offsets?
				IF g_sMenuData.bDisplayRowsDefined
					iRow = g_sMenuData.iDisplayRow[iRowLoop]
				ELSE
					iRow = iRowLoop
				ENDIF
				
				iCurrentDisplaySpacer = iSpacerCount
				
				bDisplay = FALSE
				IF iRow >= g_sMenuData.iTopItem
				AND iDisplayRow < g_sMenuData.iMenuRows
					bDisplay = TRUE
					
					IF (g_sMenuData.iCurrentItem = iRow)
						fSelectectedHeightTrack = fHeightTrack
					ENDIF
					
					IF g_sMenuData.bMenuRowHasSpacer[iRow]
						iCurrentDisplaySpacer++
					ENDIF
					
					// Only update the Y if we are displaying this row
					fTextY = fBodyStartY+(fHeightTrack)+(CUSTOM_MENU_SPACER_H*iCurrentDisplaySpacer)+CUSTOM_MENU_TEXT_INDENT_Y
				ENDIF
				
				// Track the Y pos
				g_sMenuData.fMenuItemScreenY[iRow] = fTextY
				
				fTextX = CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X
				bItemAdded = FALSE
				bSelected = (g_sMenuData.iCurrentItem = iRow)
				
				// Add the highlight bar?
				IF bSelected AND iPass = 1 AND bDisplay
				
					INT iR = 255
					INT iG = 255
					INT iB = 255
					INT iA = 255
					IF g_sMenuData.bUseCustomSelectionBarColour
						GET_HUD_COLOUR(g_sMenuData.eSelectionBarColour, iR, iG, iB, iA)	
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)	
					ENDIF
					// [TART UP]
					//DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fBodyStartY+(fSelectectedHeightTrack)+(CUSTOM_MENU_SPACER_H*iCurrentDisplaySpacer), fCustomWidth, fMenuItemBarHeight, iR, iG, iB, iA) // White highlight
					DRAW_SPRITE("CommonMenu", "Gradient_Nav", CUSTOM_MENU_X+(fCustomWidth*0.5), fBodyStartY+(fSelectectedHeightTrack)+(CUSTOM_MENU_SPACER_H*iCurrentDisplaySpacer)+(fMenuItemBarHeight*0.5), fCustomWidth, fMenuItemBarHeight, 0.0, iR, iG, iB, iA)
					g_sMenuData.fCurrentItemScreenY = fTextY
				ENDIF
				
				REPEAT g_sMenuData.iMenuColumns iItem
					
					IF IS_BIT_SET(g_sMenuData.iItemBitset[iRow], iItem)
					OR g_sMenuData.eItemLayout[iItem] = MENU_ITEM_TAB
					
						IF g_sMenuData.bDisplayRowsDefined
							iTextCount = g_sMenuData.iStoredTextCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
							iIntCount = g_sMenuData.iStoredIntCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
							iFloatCount = g_sMenuData.iStoredFloatCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
							iIconCount = g_sMenuData.iStoredIconCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
							iPlayerNameCount = g_sMenuData.iStoredPlayerNameCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
						ELSE
							g_sMenuData.iStoredTextCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = iTextCount
							g_sMenuData.iStoredIntCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = iIntCount
							g_sMenuData.iStoredFloatCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = iFloatCount
							g_sMenuData.iStoredIconCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = iIconCount
							g_sMenuData.iStoredPlayerNameCount[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = iPlayerNameCount
						ENDIF
					
						INT iArrayPos = 0
						bSetCarColour = FALSE
						IF g_sMenuData.iCarColour[0] != -1
							IF ((iRow*MAX_MENU_COLUMNS)+iItem) = g_sMenuData.iCarColourItem[0]
								bSetCarColour = TRUE
								iArrayPos = 0
							ENDIF
						ENDIF
						IF g_sMenuData.iCarColour[1] != -1
							IF ((iRow*MAX_MENU_COLUMNS)+iItem) = g_sMenuData.iCarColourItem[1]
								bSetCarColour = TRUE
								iArrayPos = 1
							ENDIF
						ENDIF
						
						// Use offset explicitly set by the calling script
						IF g_sMenuData.fColumnXOffset[iItem] != -1
							fTextX = CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X+g_sMenuData.fColumnXOffset[iItem]
						ENDIF
						
						// Work out the width of this column for text justification calculations
						IF iItem < MAX_MENU_COLUMNS-1
						AND g_sMenuData.fColumnXOffset[iItem+1] != -1.0
						AND fTextX < g_sMenuData.fColumnXOffset[iItem+1]
							fTempColumnWidth = (g_sMenuData.fColumnXOffset[iItem+1]-fTextX)
						ELSE
							fTempColumnWidth = (CUSTOM_MENU_X+CUSTOM_MENU_W-CUSTOM_MENU_TEXT_INDENT_X-fTextX)
						ENDIF
						
						IF g_sMenuData.bItemToggleable[iItem]
						AND g_sMenuData.bDisplayCurrentItemToggles
						AND bSelected
							bAddToggleItems = TRUE
						ELSE
							bAddToggleItems = FALSE
						ENDIF
						
						// Add the items
						SWITCH g_sMenuData.eItemLayout[iItem]
							CASE MENU_ITEM_DUMMY
								// Item not required
							BREAK
							CASE MENU_ITEM_TEXT
								iThisTextItem = iTextCount
								
								IF bDisplay
								
									// Work out width of this text item
									IF NOT g_sMenuData.bDisplayRowsDefined
										fTempTextWidth = 0
										fTempIconWidth = 0
										iTempTextCount = 0
										iTempIntCount = 0
										iTempFloatCount = 0
										iTempIconCount = 0
										iTempPlayerNameCount = 0
										IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
											bCondensed = FALSE
											bPlayerName = FALSE
											REPEAT MAX_STORED_TEXT_COMPS i
												IF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
												OR g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME // we may want to change this to condensed, will leave until it gets bugged.
													bPlayerName = TRUE
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
													bCondensed = TRUE
												ENDIF
											ENDREPEAT
											
											IF g_sMenuData.bForceCondensedFont[iThisTextItem]
												bPlayerName = TRUE
											ENDIF
											
											SETUP_MENU_ITEM_TEXT(bSelected, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour, iArrayPos, bPlayerName, bCondensed)
											BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(g_sMenuData.tl15Item[iThisTextItem])
										ENDIF
											REPEAT MAX_STORED_TEXT_COMPS i
												IF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_TEXT
													iTempTextCount++
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15Item[iThisTextItem+iTempTextCount])
													ENDIF
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME
													iTempTextCount++
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15Item[iThisTextItem+iTempTextCount])
													ENDIF
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
													ENDIF
													iTempPlayerNameCount++
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_RADIO_STATION
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
													ENDIF
													iTempPlayerNameCount++
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_LITERAL
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
													ENDIF
													iTempPlayerNameCount++
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
													ENDIF
													iTempPlayerNameCount++
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_INT
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iItem[iIntCount+iTempIntCount])
													ENDIF
													iTempIntCount++
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_FLOAT
													IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
														ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fItem[iFloatCount+iTempFloatCount], g_sMenuData.iFloatDP[iFloatCount+iTempFloatCount])
													ENDIF
													iTempFloatCount++
												ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_ICON
													iTempIconCount++
												ENDIF
											ENDREPEAT
										IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
											fTempTextWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
										ENDIF
										
										IF iTempIconCount > 0
											REPEAT iTempIconCount i
												IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount+i], bSelected, FALSE, fWidth, fHeight, bUseActualRes)
													fTempIconWidth += (fWidth)
													IF i > 0
														fTempIconWidth -= (CUSTOM_MENU_PIXEL_WIDTH*4)
													ENDIF
													IF (g_sMenuData.eIconItem[iIconCount+i] = MENU_ICON_LEFT_STAR)
													OR (g_sMenuData.eIconItem[iIconCount+i] = MENU_ICON_DISCOUNT)
													OR (g_sMenuData.eIconItem[iIconCount+i] = MENU_ICON_CHIPS)
														fTempIconWidth -= (CUSTOM_MENU_PIXEL_WIDTH*5)
													ENDIF
												ENDIF
											ENDREPEAT
										ENDIF
										
										// Use this to track x offset
										fTempWidth = 0
										
										// Offset for text justification
										IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
											fTempWidth += (fTempColumnWidth-(fTempTextWidth+fTempIconWidth))+(CUSTOM_MENU_PIXEL_WIDTH*1)
										ELIF g_sMenuData.eJustification[iItem] = FONT_CENTRE
											fTempWidth += (((fTempColumnWidth-fTextX)*0.5) - ((fTempTextWidth+fTempIconWidth)*0.5))
										ENDIF
										
										g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempWidth
										g_sMenuData.fStoredTempTextWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempTextWidth
										g_sMenuData.fStoredTempIconWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempIconWidth
										
										// Fix up locations for previous column
										IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
											FOR iColumnShift = iItem-1 TO 0 STEP -1
												IF g_sMenuData.eJustification[iColumnShift] = FONT_RIGHT
													g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iColumnShift] -= g_sMenuData.fColumnWidth[iItem]// g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
												ENDIF
											ENDFOR
										ENDIF
									ELSE
										fTempWidth = g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
										fTempTextWidth = g_sMenuData.fStoredTempTextWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
										fTempIconWidth = g_sMenuData.fStoredTempIconWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
									ENDIF
									
									// Offset for toggle item
									IF bAddToggleItems
										IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
										
											IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
												fTempWidth -= (fWidth*2)
											ENDIF
										
											fTempToggleWidth = (fWidth*0.5)
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_L, TRUE, iIconR, iIconG, iIconB, iIconA)
												IF iPass = 1
													DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_L), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_L, TRUE), fTextX+fTempWidth+fTempToggleWidth, fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
												ENDIF
											ENDIF
										ENDIF
										
										IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
										
											fTempWidth += (fWidth)
										
											fTempToggleWidth = (fWidth*0.5)
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_R, TRUE, iIconR, iIconG, iIconB, iIconA)
												IF iPass = 1
													DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_R), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_R, TRUE), fTextX+fTempWidth+fTempToggleWidth+(fTempTextWidth+fTempIconWidth), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									// Display the menu item
									iTempTextCount = 0
									iTempIntCount = 0
									iTempFloatCount = 0
									iTempIconCount = 0
									iTempPlayerNameCount = 0
									
									eLastCompType = MENU_TEXT_COMP_DUMMY
									IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
									AND iPass = 1
									
										bCondensed = FALSE
										bPlayerName = FALSE
										REPEAT MAX_STORED_TEXT_COMPS i
											IF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
											OR g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME
												bPlayerName = TRUE
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
												bCondensed = TRUE
											ENDIF
										ENDREPEAT
										
										IF g_sMenuData.bForceCondensedFont[iThisTextItem]
											bPlayerName = TRUE
										ENDIF
										
										SETUP_MENU_ITEM_TEXT(bSelected, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour, 0, bPlayerName, bCondensed)
										
										IF g_sMenuData.bUseCustomRowColour
										AND g_sMenuData.iCustomRowColour = iRow
											SETUP_MENU_CUSTOM_ITEM_TEXT_COLOUR(bSelected)
										ENDIF																						
										
										BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMenuData.tl15Item[iThisTextItem])											
									ENDIF
										REPEAT MAX_STORED_TEXT_COMPS i
											IF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_TEXT
												iTempTextCount++
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15Item[iThisTextItem+iTempTextCount])
												ENDIF
												eLastCompType = MENU_TEXT_COMP_TEXT
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME
												iTempTextCount++
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sMenuData.tl15Item[iThisTextItem+iTempTextCount])
												ENDIF
												eLastCompType = MENU_TEXT_COMP_VEHICLE_NAME
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
												ENDIF
												iTempPlayerNameCount++
												eLastCompType = MENU_TEXT_COMP_PLAYER_NAME
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_RADIO_STATION
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
												ENDIF
												iTempPlayerNameCount++
												eLastCompType = MENU_TEXT_COMP_RADIO_STATION
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_LITERAL
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
												ENDIF
												iTempPlayerNameCount++
												eLastCompType = MENU_TEXT_COMP_LITERAL
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TUMenuGlobals_tlPlayerNameItem[iPlayerNameCount+iTempPlayerNameCount])
												ENDIF
												iTempPlayerNameCount++
												eLastCompType = MENU_TEXT_COMP_CONDENSED_LITERAL
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_INT
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iItem[iIntCount+iTempIntCount])
												ENDIF
												iTempIntCount++
												eLastCompType = MENU_TEXT_COMP_INT
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_FLOAT
												IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
												AND iPass = 1
													ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fItem[iFloatCount+iTempFloatCount], g_sMenuData.iFloatDP[iFloatCount+iTempFloatCount])
												ENDIF
												iTempFloatCount++
												eLastCompType = MENU_TEXT_COMP_FLOAT
											ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_ICON
											
												IF (g_sMenuData.eIconItem[iIconCount+iTempIconCount] = MENU_ICON_LEFT_STAR)
												OR (g_sMenuData.eIconItem[iIconCount+iTempIconCount] = MENU_ICON_DISCOUNT)
												OR (g_sMenuData.eIconItem[iIconCount+iTempIconCount] = MENU_ICON_CHIPS)
													IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount+iTempIconCount], bSelected, FALSE, fWidth, fHeight, bUseActualRes)
														fTempWidth += (fWidth*0.5)
														IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount+iTempIconCount], bSelected, TRUE, fWidth, fHeight, bUseActualRes)
															GET_MENU_ICON_TEXTURE_RGB(g_sMenuData.eIconItem[iIconCount+iTempIconCount], bSelected, iIconR, iIconG, iIconB, iIconA)
															
															IF iPass = 1
																IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
																	DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eIconItem[iIconCount+iTempIconCount]), GET_MENU_ICON_TEXTURE(g_sMenuData.eIconItem[iIconCount+iTempIconCount], bSelected), fTextX+fTempWidth-(CUSTOM_MENU_PIXEL_WIDTH*8)+(CUSTOM_MENU_PIXEL_WIDTH*4), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
																ELSE
																	DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eIconItem[iIconCount+iTempIconCount]), GET_MENU_ICON_TEXTURE(g_sMenuData.eIconItem[iIconCount+iTempIconCount], bSelected), fTextX+fTempWidth-(CUSTOM_MENU_PIXEL_WIDTH*8), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
																ENDIF
															ENDIF
															
															fTempWidth += (CUSTOM_MENU_PIXEL_WIDTH*3)
														ENDIF
													ENDIF
												ENDIF
												iTempIconCount++
												eLastCompType = MENU_TEXT_COMP_ICON
											ENDIF
										ENDREPEAT
									IF GET_HASH_KEY(g_sMenuData.tl15Item[iThisTextItem]) != 0
									AND iPass = 1
										IF eLastCompType = MENU_TEXT_COMP_ICON
										AND g_sMenuData.eJustification[iItem] = FONT_RIGHT
											END_TEXT_COMMAND_DISPLAY_TEXT(fTextX+fTempWidth+(CUSTOM_MENU_PIXEL_WIDTH*7), fTextY)
										ELSE
											END_TEXT_COMMAND_DISPLAY_TEXT(fTextX+fTempWidth, fTextY)
											
											IF IS_ROCKSTAR_DEV()
											AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("FM_MISSION_CREATOR")) > 0
												IF iItem = 0
													TEXT_LABEL_15 tl15b 
													FLOAT fColour 
													FLOAT fAlpha
													FLOAT fOffsetForNum 
													FLOAT fOffsetY 
													FLOAT fOffsetX
													
													IF g_sMenuData.bForceCondensedFont[iThisTextItem]
														bPlayerName = TRUE
													ENDIF
													
													SETUP_MENU_ITEM_TEXT(FALSE, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour, 0, bPlayerName, bCondensed)
													tl15b = "TEST_LABEL"
													fColour = 0														 
													fAlpha = 55
													fOffsetForNum = 0.0185
													fOffsetY = 0.004													
													fOffsetX = 0.02
													
													SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y*0.7)
													
													SET_TEXT_COLOUR(255, 255, 255, 150)
													
													DRAW_RECT(fTextX-(fOffsetX*0.6), fTextY+(fOffsetForNum*0.75), 0.0175, 0.035, FLOOR(fColour), FLOOR(fColour), FLOOR(fColour), FLOOR(fAlpha))
													BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl15b)
														// Items hiddens aren't incrementing the count. Fix it: url:bugstar:7930348
														ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iTopItem+iThisTextItemActual)
													END_TEXT_COMMAND_DISPLAY_TEXT(fTextX-fOffsetX, fTextY+fOffsetY)
													
													iThisTextItemActual++
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF iTempIconCount > 0
										fTempWidth += (6 * CUSTOM_MENU_PIXEL_WIDTH)
										REPEAT iTempIconCount i
											IF (g_sMenuData.eIconItem[iIconCount+i] != MENU_ICON_LEFT_STAR)
											AND (g_sMenuData.eIconItem[iIconCount+i] != MENU_ICON_DISCOUNT)
											AND (g_sMenuData.eIconItem[iIconCount+i] != MENU_ICON_CHIPS)
												IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount+i], bSelected, FALSE, fWidth, fHeight, bUseActualRes)
													fTempWidth += (fWidth*0.5)
													IF iPass = 1
														IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount+i], bSelected, TRUE, fWidth, fHeight, bUseActualRes)
															GET_MENU_ICON_TEXTURE_RGB(g_sMenuData.eIconItem[iIconCount+i], bSelected, iIconR, iIconG, iIconB, iIconA)
															IF g_sMenuData.eIconItem[iIconCount+i] = MENU_ICON_DLC_IMAGE
																DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eIconItem[iIconCount+i]), GET_MENU_ICON_TEXTURE(g_sMenuData.eIconItem[iIconCount+i], bSelected), CUSTOM_MENU_X+(fWidth*0.5), fTextY+CUSTOM_MENU_TEXT_INDENT_Y+(fHeight*0.5)-(CUSTOM_MENU_PIXEL_WIDTH*11), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
															ELIF g_sMenuData.eJustification[iItem] = FONT_RIGHT
																DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eIconItem[iIconCount+i]), GET_MENU_ICON_TEXTURE(g_sMenuData.eIconItem[iIconCount+i], bSelected), fTextX+fTempWidth+fTempTextWidth-(CUSTOM_MENU_PIXEL_WIDTH*8)+(CUSTOM_MENU_PIXEL_WIDTH*4), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
															ELSE
																DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eIconItem[iIconCount+i]), GET_MENU_ICON_TEXTURE(g_sMenuData.eIconItem[iIconCount+i], bSelected), fTextX+fTempWidth+fTempTextWidth-(CUSTOM_MENU_PIXEL_WIDTH*12), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
															ENDIF
														ENDIF
													ENDIF
													fTempWidth += (12 * CUSTOM_MENU_PIXEL_WIDTH)
												ENDIF
											ENDIF
										ENDREPEAT
									ENDIF
								ENDIF
								bItemAdded = TRUE
								iTextCount++
								
								// Add the sub component items we just processed.
								REPEAT MAX_STORED_TEXT_COMPS i
									IF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_TEXT
										iTextCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_VEHICLE_NAME
										iTextCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_INT
										iIntCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_FLOAT
										iFloatCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_ICON
										iIconCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_PLAYER_NAME
										iPlayerNameCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_RADIO_STATION
										iPlayerNameCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_LITERAL
										iPlayerNameCount++
									ELIF g_sMenuData.eTextItemComps[iThisTextItem][i] = MENU_TEXT_COMP_CONDENSED_LITERAL
										iPlayerNameCount++
									ENDIF
								ENDREPEAT
							BREAK
							CASE MENU_ITEM_INT
								IF bDisplay
								
									// Work out width of this text item
									IF NOT g_sMenuData.bDisplayRowsDefined
										SETUP_MENU_ITEM_TEXT(bSelected, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour)
										IF g_sMenuData.bUseCustomRowColour
										AND g_sMenuData.iCustomRowColour = iRow
											SETUP_MENU_CUSTOM_ITEM_TEXT_COLOUR(bSelected)
										ENDIF
										BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("NUMBER")
											ADD_TEXT_COMPONENT_INTEGER(g_sMenuData.iItem[iIntCount])
										fTempTextWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
										
										// Use this to track x offsets
										fTempWidth = 0
										
										// Offset for text justification
										IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
											fTempWidth += (fTempColumnWidth-(fTempTextWidth))+(CUSTOM_MENU_PIXEL_WIDTH*1)
										ELIF g_sMenuData.eJustification[iItem] = FONT_CENTRE
											fTempWidth += (((fTempColumnWidth-fTextX)*0.5) - ((fTempTextWidth)*0.5))
										ENDIF
										
										g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempWidth
										g_sMenuData.fStoredTempTextWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempTextWidth
									ELSE
										fTempWidth = g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
										fTempTextWidth = g_sMenuData.fStoredTempTextWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
									ENDIF
									
									// Offset for toggle item
									IF bAddToggleItems
										IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
										
											IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
												fTempWidth -= (fWidth*2)
											ENDIF
										
											fTempToggleWidth = (fWidth*0.5)
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_L, TRUE, iIconR, iIconG, iIconB, iIconA)
												IF iPass = 1
													DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_L), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_L, TRUE), fTextX+fTempWidth+fTempToggleWidth, fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
												ENDIF
											ENDIF
										ENDIF
										
										IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
										
											fTempWidth += (fWidth)
										
											fTempToggleWidth = (fWidth*0.5)
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_R, TRUE, iIconR, iIconG, iIconB, iIconA)
												IF iPass = 1
													DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_R), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_R, TRUE), fTextX+fTempWidth+fTempToggleWidth+(fTempTextWidth+fTempIconWidth), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									// Display the menu item
									IF iPass = 1
										SETUP_MENU_ITEM_TEXT(bSelected, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour)
										DISPLAY_TEXT_WITH_NUMBER(fTextX+fTempWidth, fTextY, "NUMBER", g_sMenuData.iItem[iIntCount])
									ENDIF
								ENDIF
								bItemAdded = TRUE
								iIntCount++
							BREAK
							CASE MENU_ITEM_FLOAT
								IF bDisplay
								
									// Work out width of this text item
									IF NOT g_sMenuData.bDisplayRowsDefined
										SETUP_MENU_ITEM_TEXT(bSelected, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour)
										IF g_sMenuData.bUseCustomRowColour
										AND g_sMenuData.iCustomRowColour = iRow
											SETUP_MENU_CUSTOM_ITEM_TEXT_COLOUR(bSelected)
										ENDIF
										BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("NUMBER")
											ADD_TEXT_COMPONENT_FLOAT(g_sMenuData.fItem[iFloatCount], g_sMenuData.iFloatDP[iFloatCount])
										fTempTextWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
										
										// Use this to track x offsets
										fTempWidth = 0
										
										// Offset for text justification
										IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
											fTempWidth += (fTempColumnWidth-(fTempTextWidth))+(CUSTOM_MENU_PIXEL_WIDTH*1)
										ELIF g_sMenuData.eJustification[iItem] = FONT_CENTRE
											fTempWidth += (((fTempColumnWidth-fTextX)*0.5) - ((fTempTextWidth)*0.5))
										ENDIF
										
										g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempWidth
										g_sMenuData.fStoredTempTextWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempTextWidth
									ELSE
										fTempWidth = g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
										fTempTextWidth = g_sMenuData.fStoredTempTextWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
									ENDIF
									
									// Offset for toggle item
									IF bAddToggleItems
										IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fWidth, fHeight)
										
											IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
												fTempWidth -= (fWidth*2)
											ENDIF
										
											fTempToggleWidth = (fWidth*0.5)
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_L, TRUE, iIconR, iIconG, iIconB, iIconA)
												IF iPass = 1
													DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_L), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_L, TRUE), fTextX+fTempWidth+fTempToggleWidth, fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
												ENDIF
											ENDIF
										ENDIF
										
										IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
										
											fTempWidth += (fWidth)
										
											fTempToggleWidth = (fWidth*0.5)
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_R, TRUE, iIconR, iIconG, iIconB, iIconA)
												IF iPass = 1
													DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_R), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_R, TRUE), fTextX+fTempWidth+fTempToggleWidth+(fTempTextWidth+fTempIconWidth), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									// Display the menu item
									SETUP_MENU_ITEM_TEXT(bSelected, g_sMenuData.bIsSelectable[iThisTextItem], g_sMenuData.bIsDefault[iThisTextItem], bSetCarColour)
									DISPLAY_TEXT_WITH_FLOAT(fTextX+fTempWidth, fTextY, "NUMBER", g_sMenuData.fItem[iFloatCount], g_sMenuData.iFloatDP[iFloatCount])
								ENDIF
								bItemAdded = TRUE
								iFloatCount++
							BREAK
							CASE MENU_ITEM_ICON
								IF bDisplay
									IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount], bSelected, FALSE, fWidth, fHeight, bUseActualRes)
									
										// Work out width of this item
										IF NOT g_sMenuData.bDisplayRowsDefined
											fTempIconWidth = (fWidth)
											
											// Use this to track x offsets
											fTempWidth = 0
											
											// Offset for text justification
											IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
												fTempWidth += (fTempColumnWidth-(fTempIconWidth))+(CUSTOM_MENU_PIXEL_WIDTH*1)
											ELIF g_sMenuData.eJustification[iItem] = FONT_CENTRE
												fTempWidth += (((fTempColumnWidth-fTextX)*0.5) - ((fTempIconWidth)*0.5))
											ENDIF
											
											g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempWidth
											g_sMenuData.fStoredTempIconWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem] = fTempIconWidth
										ELSE
											fTempWidth = g_sMenuData.fStoredTempWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
											fTempIconWidth = g_sMenuData.fStoredTempIconWidth[(iDisplayRow*g_sMenuData.iMenuColumns)+iItem]
										ENDIF
										
										// Offset for toggle item
										IF bAddToggleItems
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
											
												IF g_sMenuData.eJustification[iItem] = FONT_RIGHT
													fTempWidth -= (fWidth*2)
												ENDIF
											
												fTempToggleWidth = (fWidth*0.5)
												IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
													GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_L, TRUE, iIconR, iIconG, iIconB, iIconA)
													IF iPass = 1
														DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_L), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_L, TRUE), fTextX+fTempWidth+fTempToggleWidth, fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
													ENDIF
												ENDIF
											ENDIF
											
											IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, FALSE, fWidth, fHeight, bUseActualRes)
											
												fTempWidth += (fWidth)
											
												fTempToggleWidth = (fWidth*0.5)
												IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_R, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
													GET_MENU_ICON_TEXTURE_RGB(MENU_ICON_ARROW_R, TRUE, iIconR, iIconG, iIconB, iIconA)
													IF iPass = 1
														DRAW_SPRITE(GET_MENU_ICON_TXD(MENU_ICON_ARROW_R), GET_MENU_ICON_TEXTURE(MENU_ICON_ARROW_R, TRUE), fTextX+fTempWidth+fTempToggleWidth+(fTempTextWidth+fTempIconWidth), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth, fHeight, 0.0, iIconR, iIconG, iIconB, iIconA)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										// Display the menu item
										IF iPass = 1
											IF GET_MENU_ICON_SCREEN_RESOLUTION(g_sMenuData.eIconItem[iIconCount], bSelected, TRUE, fWidth, fHeight, bUseActualRes)
												GET_MENU_ICON_TEXTURE_RGB(g_sMenuData.eIconItem[iIconCount], bSelected, iIconR, iIconG, iIconB, iIconA)
												DRAW_SPRITE(GET_MENU_ICON_TXD(g_sMenuData.eIconItem[iIconCount]), GET_MENU_ICON_TEXTURE(g_sMenuData.eIconItem[iIconCount], bSelected), fTextX+fTempWidth+(fWidth*0.5), fTextY-CUSTOM_MENU_TEXT_INDENT_Y+(fMenuItemBarHeight*0.5), fWidth*GET_MENU_ICON_SCALE_MULTIPLIER(g_sMenuData.eIconItem[iIconCount]), fHeight*GET_MENU_ICON_SCALE_MULTIPLIER(g_sMenuData.eIconItem[iIconCount]), 0.0, iIconR, iIconG, iIconB, iIconA)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								bItemAdded = TRUE
								iIconCount++
							BREAK
							CASE MENU_ITEM_TAB
								bItemAdded = TRUE
							BREAK
						ENDSWITCH
																		
						// Add column width for the next item
						IF g_sMenuData.eItemLayout[iItem] = MENU_ITEM_TAB
							IF g_sMenuData.fColumnWidth[iItem] > 0.05
								fTextX += g_sMenuData.fColumnWidth[iItem]
							ELSE
								fTextX += 0.05
							ENDIF
						ELSE
							fTextX += g_sMenuData.fColumnWidth[iItem]
							
							// Remove the toggle item width we added at the start as this would have already been added to fColumnWidth
							IF g_sMenuData.bItemToggleable[iItem]
								IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_ARROW_L, TRUE, TRUE, fWidth, fHeight, bUseActualRes)
									fTextX -= fWidth
								ENDIF
							ENDIF
						ENDIF
					ELSE
						// No item added so add on default width
						fTextX += g_sMenuData.fColumnWidth[iItem]
					ENDIF
				ENDREPEAT
								
				// No more items to add in this row so update the final height
				IF bItemAdded
					IF bDisplay
						g_sMenuData.iDisplayRow[iDisplayRow] = iRow
						g_sMenuData.iLastDisplayItem = iRow
						iDisplayRow++
						IF g_sMenuData.bMenuRowHasSpacer[iRow]
							iSpacerCount++
						ENDIF
						
						IF g_sMenuData.fRowHeight[iRow] != 0.0
							fHeightTrack += g_sMenuData.fRowHeight[iRow]
						ELSE
							fHeightTrack += CUSTOM_MENU_ITEM_BAR_H
						ENDIF
						
					ENDIF
					
					IF NOT g_sMenuData.bSetupComplete
						g_sMenuData.bMenuRowHasDisplayItems[iRow] = TRUE
					
						IF g_sMenuData.bMenuRowDoesntAddToCount[iRow]
							IF bSelected
								g_sMenuData.iSetupCurrentSelectableItem = 0
							ENDIF
						ELSE
							iTotalSelectableRows++
							IF bSelected
								g_sMenuData.iSetupCurrentSelectableItem = iTotalSelectableRows
							ENDIF
						ENDIF
						
						iTotalRows++
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT g_sMenuData.bSetupComplete
				g_sMenuData.fSetupFinalBodyY = fBodyStartY+(fHeightTrack)+(CUSTOM_MENU_SPACER_H*iCurrentDisplaySpacer)
				g_sMenuData.iSetupTotalSelectableRows = iTotalSelectableRows
				g_sMenuData.iSetupTotalRows = iTotalRows
				g_sMenuData.bSetupComplete = TRUE
			ENDIF
		ENDIF
		
		IF NOT g_sMenuData.bDisplayRowsDefined
			g_sMenuData.iSetupTotalDisplayRows = iDisplayRow
			g_sMenuData.bDisplayRowsDefined = TRUE
		ENDIF
	ENDREPEAT
	
	g_sMenuData.fMenuFinalScreenY = fFinalPanelY
	
	g_sMenuData.iLastDrawTimer = GET_GAME_TIMER()
	
	THEFEED_SET_SCRIPTED_MENU_HEIGHT(g_sMenuData.fMenuFinalScreenY)
	
	// Block various game states
	IF NOT g_sMenuData.bKeepPhoneForNextDrawMenuCall
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	g_sMenuData.bKeepPhoneForNextDrawMenuCall = FALSE
	
	IF bHideHelpText
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	ENDIF
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	
	// Fix for bug #953251 - Spray booth menu is overlapped by the distance marker for Race to Point
	IF bSetButtonsUnderHud 
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

// This is used to delay control movement up/down
FUNC BOOL ALLOW_ANALOGUE_MOVEMENT(TIME_DATATYPE &selectionIncrementTimer, INT &iIncrement,BOOL bVertical = TRUE)
	// up button 
	BOOL bIncrement
	INT iTimerDelay = 150
	
	IF bVertical
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < -0.3)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		
			//faster response
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				iTimerDelay = 100
			ENDIF

			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay		
				selectionIncrementTimer = GET_NETWORK_TIME()
				bIncrement = TRUE
				iIncrement = 1
			ENDIF
		ENDIF
		
		// down button 
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.3)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			//faster response
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				iTimerDelay = 100
			ENDIF
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
				selectionIncrementTimer = GET_NETWORK_TIME()
				bIncrement = TRUE
				iIncrement = -1
			ENDIF
		ENDIF
	ELSE
		// right button press
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.3)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay		
				selectionIncrementTimer = GET_NETWORK_TIME()
				bIncrement = TRUE
				iIncrement = 1
			ENDIF
		ENDIF
		
		// left button press
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.3)
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
				selectionIncrementTimer = GET_NETWORK_TIME()
				bIncrement = TRUE
				iIncrement = -1
			ENDIF
		ENDIF
	ENDIF
	RETURN bIncrement
ENDFUNC

/// PURPOSE:
///    Returns true if a menu option was selected OR the cancel button was pressed.   
FUNC BOOL HANDLE_CUSTOM_MENU_INPUT( TIME_DATATYPE& tLastScrollTime, BOOL& ref_bCancelled, BOOL& ref_bAccepted )
	ref_bCancelled = FALSE
	ref_bAccepted = FALSE
	
	INT iSelectionChange = 0
	
	IF IS_PC_VERSION()
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF GET_PAUSE_MENU_STATE() = PM_INACTIVE
		AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
		AND NOT IS_WARNING_MESSAGE_ACTIVE()
		AND NOT g_sShopSettings.bProcessStoreAlert
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()	
			// Mouse control support  
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				
				SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - Mouse accept just pressed.")
				IF g_sMenuData.iCurrentItem = g_iMenuCursorItem
					CDEBUG1LN(DEBUG_EXT_MENU, "HANDLE_CUSTOM_MENU_INPUT - Clicked selected option - accepting this menu option. (ACCEPT)")
					ref_bAccepted = TRUE
				ELSE
					iSelectionChange =  g_sMenuData.iCurrentItem-g_iMenuCursorItem   //(ALLOW_ANALOGUE_MOVEMENT returns the negative so we invert this too)
					CDEBUG1LN(DEBUG_EXT_MENU, "HANDLE_CUSTOM_MENU_INPUT - Clicked non-selected option - moving forward selection by ",iSelectionChange,".")
				ENDIF
			ELIF IS_MENU_CURSOR_CANCEL_PRESSED()
			OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - INPUT_FRONTEND_PAUSE_ALTERNATE (CANCEL)")
				ref_bCancelled = TRUE
			ELIF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
				iSelectionChange = 1
			ELIF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
				iSelectionChange = -1
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT ref_bAccepted
	AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - INPUT_FRONTEND_ACCEPT (ACCEPT)")
		ref_bAccepted = TRUE
	ENDIF
	
	IF NOT ref_bCancelled
	AND IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		//Checking for released instead of pressed to prevent conflict with the pause menu
		CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - INPUT_FRONTEND_CANCEL (CANCEL)")
		ref_bCancelled = TRUE
	ENDIF
	
	IF iSelectionChange = 0
	AND (NOT ALLOW_ANALOGUE_MOVEMENT( tLastScrollTime, iSelectionChange, TRUE)
	OR NOT (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) // Normal interior menus only accept dpad (no LStick)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)))
		iSelectionChange = 0 //ALLOW_ANALOGUE_MOVEMENT does processing. Set back to 0 to be sure.
	ENDIF
		
	IF ref_bAccepted
		CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - ACCEPT just pressed. Option ", g_sMenuData.iCurrentItem)
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
		RETURN TRUE
	ELIF ref_bCancelled
		CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - CANCEL just pressed.")
		PLAY_SOUND_FRONTEND(-1, "BACK","HUD_FREEMODE_SOUNDSET")
		RETURN TRUE
	ENDIF
	
	IF iSelectionChange <> 0
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FREEMODE_SOUNDSET")
		SET_CURRENT_MENU_ITEM_WITH_WRAP( g_sMenuData.iCurrentItem - iSelectionChange )
		CDEBUG1LN(DEBUG_EXT_MENU, " HANDLE_CUSTOM_MENU_INPUT - Change selection just pressed. Option ", g_sMenuData.iCurrentItem)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC APPLY_BASIC_CUSTOM_MENU_SETTINGS(BOOL bIncludeBackButton = TRUE, BOOL bIncludeSelectButton = TRUE)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	IF bIncludeSelectButton
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ENDIF
	IF bIncludeBackButton
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	ENDIF
ENDPROC

PROC SETUP_MENU_WITH_TITLE(STRING sTitle)
	CLEAR_MENU_DATA()
	
	SET_MENU_TITLE(sTitle)
	
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
ENDPROC
