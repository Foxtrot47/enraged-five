USING "globals.sch"
USING "rage_builtins.sch"

// Header support for HUD_FEED_MESSAGE_PICTURE.gfx

/// PURPOSE: Displays the picture message contact image, picture message image and localised text message string in the top left of the HUD
/// Make sure you have loaded the movie with REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE, 
/// and checked it has loaded with HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED before using this.
/// PARAMS:
///    sHUDFeedPictureMessage - Scaleform movie ID for the picture message UI
///    sMessageText - body string of the Picture Message
///    sMessageTXD - The Texture Dictionary for the message image used in the Picture Message
///    sMessageImageName - The Image name string for the message image used in the Picture Message
///    sContactTXD - The Texture Dictionary for the contact image used in the Picture Message
///    sContactImageName - The Image name string for the contact image used in the Picture Message
PROC Set_Feed_Picture_Message(eSCRIPT_HUD_COMPONENT sHUDFeedPictureMessage, STRING sMessageText, STRING sMessageTXD, STRING sMessageImageName, STRING sContactTXD, STRING sContactImageName)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedPictureMessage, "SET_FEED_MESSAGE_PICTURE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMessageText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMessageTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMessageImageName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sContactTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sContactImageName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:remove picture message component from screen. Be sure to remove the scaleform movie when it is no longer required
///    
/// PARAMS:
///    sHUDFeedPictureMessage - Scaleform movie ID for the picture message UI
PROC Remove_Feed_Picture_Message(eSCRIPT_HUD_COMPONENT sHUDFeedPictureMessage)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedPictureMessage, "REMOVE_FEED_MESSAGE_PICTURE")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC
