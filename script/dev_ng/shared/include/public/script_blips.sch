
USING "script_blip.sch" // This is the new CORE file, which only includes a subset of this previous file
// The functions in this file are either GTA5 specific, or are deprecated, and will be removed after GTA5 completes
USING "script_MISC.sch"
USING "script_maths.sch"
USING "commands_audio.sch"
USING "charsheet_public.sch"
USING "globals.sch"

CONST_INT BLIP_GANG_PROFESSIONAL 	1
CONST_INT BLIP_GANG_FRIENDLY	 	2
CONST_INT BLIP_GANG_ENEMY			3

// Bit flags for AI_BLIP_STRUCT.iStateBitset
CONST_INT	BIT_AI_BLIP_STATE_FADING_OUT		0
CONST_INT	BIT_AI_BLIP_STATE_CHANGED_COLOR		1
CONST_INT	BIT_AI_BLIP_STATE_CONFIGURED		2	// Has initial config been run on the blip if/when it exists?
CONST_INT	BIT_AI_BLIP_STATE_VEH_CONFIGURED	3	// Has initial config been run on an associaed veh blip if/when it exists?
CONST_INT	BIT_AI_BLIP_STATE_HIDDEN			4	// The blip has been hidden due to another blip conflicting with it.
CONST_INT	BIT_AI_BLIP_STATE_IN_USE			5	// Has this struct been used at all?


STRUCT AI_BLIP_STRUCT
	BLIP_INDEX BlipID
	BLIP_INDEX VehicleBlipID
	TIME_DATATYPE iNoticableTimer
	TIME_DATATYPE iFadeOutTimer
	INT iNoticableTimer_SP
	INT iFadeOutTimer_SP
	
	INT iStateBitset
	
	PED_INDEX PedID
ENDSTRUCT


// Blip Priorities
ENUM BLIP_PRIORITY_TYPE
	BP_GANG_HOUSE,
	BP_WEAPON,
	BP_SHOP,
	BP_DYING,
	BP_DEFAULT,
	BP_OTHER_TEAM,
	BP_SAME_TEAM,
	BP_CUFFED_OR_KEYS,
	BP_ENEMY_AI,
	BP_PLAYER_NOISE,
	BP_HIGHLIGHTED, 
	BP_WANTED
ENDENUM


PROC COPY_AI_BLIP_STRUCT_TO_STRUCT(AI_BLIP_STRUCT structFrom, AI_BLIP_STRUCT &structTo)
	structTo.BlipID = structFrom.BlipID
	structTo.VehicleBlipID = structFrom.VehicleBlipID
	structTo.iNoticableTimer = structFrom.iNoticableTimer
	structTo.iFadeOutTimer = structFrom.iFadeOutTimer
	structTo.iNoticableTimer_SP = structFrom.iNoticableTimer_SP
	structTo.iFadeOutTimer_SP = structFrom.iFadeOutTimer_SP
	structTo.iStateBitset = structFrom.iStateBitset
	structTo.PedID = structFrom.PedID
ENDPROC


FUNC BLIP_INDEX CREATE_BLIP_FOR_ENTITY(ENTITY_INDEX entity, bool isEnemy = FALSE, bool useGreenVehicleBlip = FALSE)
	RETURN CREATE_BLIP_ON_ENTITY(entity, NOT isEnemy, useGreenVehicleBlip)
ENDFUNC

FUNC blip_index CREATE_BLIP_FOR_VEHICLE(VEHICLE_INDEX vehicle, bool isEnemyVehicle=FALSE, bool useGreenVehicleBlip = FALSE)
	RETURN CREATE_BLIP_ON_ENTITY(vehicle, NOT isEnemyVehicle, useGreenVehicleBlip)
ENDFUNC


/// PURPOSE:
///    Creates and sets the scale and colour of a ped blip.///    
/// PARAMS:
///    ped - Ped requiring blip.
///    isEnemyPed - Sets blip red if enemy ped
/// RETURNS:
///    Blip Index. 
FUNC BLIP_INDEX CREATE_BLIP_FOR_PED(PED_INDEX ped, BOOL isEnemyPed=FALSE, enumCharacterList useCharsheetName=NO_CHARACTER)
	BLIP_INDEX pedBlip = CREATE_BLIP_ON_ENTITY(ped, NOT isEnemyPed)
	IF useCharsheetName != NO_CHARACTER
	AND DOES_BLIP_EXIST(pedBlip)
	AND DOES_TEXT_LABEL_EXIST(g_sCharacterSheetAll[useCharsheetName].label)
		SET_BLIP_NAME_FROM_TEXT_FILE(pedBlip, g_sCharacterSheetAll[useCharsheetName].label)
	ENDIF
	RETURN pedBlip
ENDFUNC



/// PURPOSE:
///    Creates and sets the scale and colour of an object blip.///    
/// PARAMS:
///    object - Object requiring blip.
/// RETURNS:
///    Blip Index. 
FUNC BLIP_INDEX CREATE_BLIP_FOR_OBJECT(OBJECT_INDEX object)
	RETURN CREATE_BLIP_ON_ENTITY(object)
ENDFUNC


/// PURPOSE:
///    Creates and sets the scale and color of a radius blip.///    
/// PARAMS:
///    paramCenter - Coords requiring blip.
///    paramRadius - Radius size.
/// RETURNS:
///    Blip Index. 
FUNC BLIP_INDEX CREATE_BLIP_FOR_RADIUS(VECTOR paramCenter, FLOAT paramRadius, BOOL bShortRange = TRUE)
	
	BLIP_INDEX blip = ADD_BLIP_FOR_RADIUS(paramCenter, paramRadius)
	SET_BLIP_COLOUR(blip, BLIP_COLOUR_YELLOW)
	SET_BLIP_ALPHA(blip, 64)
	SET_BLIP_AS_SHORT_RANGE(blip, bShortRange)
	IF NOT bShortRange
		SET_BLIP_DISPLAY(blip, DISPLAY_BOTH)
	ENDIF

	RETURN blip
	
ENDFUNC



////////////////////RANDOM EVENT BLIPS/////////////////////////
///  


/// PURPOSE:
///    Creates the initial sprite blip when approaching the event
/// PARAMS:
///    anEntity - The entity the blip is attached to
/// RETURNS:
///    
FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_INITIAL_ENTITY(ENTITY_INDEX anEntity)

	BLIP_INDEX blip
	IF NOT IS_ENTITY_DEAD(anEntity)
		blip = ADD_BLIP_FOR_ENTITY(anEntity)
		//SET_BLIP_SPRITE(blip, RADAR_TRACE_RANDOM_CHARACTER)
		SET_BLIP_ALPHA(blip, 0) // Bug 799002 Remove question mark blips for REs
		SET_BLIP_DISPLAY(blip, DISPLAY_NOTHING)
	ENDIF
	RETURN blip

ENDFUNC

/// PURPOSE:
///    Creates the initial sprite blip when approaching the event
/// PARAMS:
///    vecCoord - The coordinate the blip should be at 
/// RETURNS:
///    
FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_INITIAL_COORD(VECTOR vecCoord)

	BLIP_INDEX blip
	
	blip = ADD_BLIP_FOR_COORD(vecCoord)
	//SET_BLIP_SPRITE(blip, RADAR_TRACE_RANDOM_CHARACTER)
	SET_BLIP_ALPHA(blip, 0) // Bug 799002 Remove question mark blips for REs
	SET_BLIP_DISPLAY(blip, DISPLAY_NOTHING) 
	
	RETURN blip
	
ENDFUNC

FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_VEHICLE(VEHICLE_INDEX vehicle, BOOL isEnemy = FALSE, BOOL isSprite = FALSE)

	isSprite = isSprite

	RETURN (CREATE_BLIP_FOR_VEHICLE(vehicle, isEnemy))

ENDFUNC

FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_PED(PED_INDEX ped, BOOL isEnemy = FALSE, BOOL isSprite = FALSE)

	isSprite = isSprite

	RETURN (CREATE_BLIP_FOR_PED(ped, isEnemy))

ENDFUNC

FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_COORD(VECTOR vecCoord, BOOL setBlipRoute = FALSE)

//	BLIP_INDEX blip
//
//	blip = ADD_BLIP_FOR_COORD(vecCoord)
//	IF NETWORK_IS_GAME_IN_PROGRESS()
//		SET_BLIP_SCALE(blip,BLIP_SIZE_NETWORK_COORD)
//	ELSE
//		SET_BLIP_SCALE(blip, BLIP_SIZE_COORD)
//	ENDIF
//	
//	SET_BLIP_ROUTE(blip, setBlipRoute)

//	RETURN blip
//	
	RETURN (CREATE_BLIP_FOR_COORD(vecCoord, setBlipRoute))

ENDFUNC

FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_OBJECT(OBJECT_INDEX object)

	RETURN (CREATE_BLIP_FOR_OBJECT(object))

ENDFUNC

FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_PICKUP(PICKUP_INDEX pickup)
//	BLIP_INDEX blip
//
//	blip = ADD_BLIP_FOR_PICKUP(pickup)
//	
//	IF NETWORK_IS_GAME_IN_PROGRESS()
//		SET_BLIP_SCALE(blip,BLIP_SIZE_NETWORK_OBJECT)
//	ELSE
//		SET_BLIP_SCALE(blip,BLIP_SIZE_OBJECT)
//	ENDIF

//	RETURN blip

	RETURN (CREATE_BLIP_FOR_PICKUP(pickup))

ENDFUNC

FUNC BLIP_INDEX CREATE_AMBIENT_BLIP_FOR_ENTITY(ENTITY_INDEX entity, bool isEnemy = FALSE, BOOL isSprite = FALSE)
	//blip index to return
	BLIP_INDEX blip
	
	isSprite = isSprite
	
	IF DOES_ENTITY_EXIST(entity)
	
		IF IS_ENTITY_A_PED(entity)
			blip = CREATE_AMBIENT_BLIP_FOR_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(entity), isEnemy)
		ELIF IS_ENTITY_A_VEHICLE(entity)
			blip = CREATE_AMBIENT_BLIP_FOR_VEHICLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity), isEnemy)
		ELIF IS_ENTITY_AN_OBJECT(entity)
			blip = CREATE_AMBIENT_BLIP_FOR_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(entity))
		ELSE
			SCRIPT_ASSERT("Unnown entity type passed to CREATE_AMBIENT_BLIP_FOR_ENTITY")
		ENDIF
		
	ENDIF
	
	RETURN blip
	
	//old stuff for creating blips of the RE size when passed an entity
	
	/*
	
	IF DOES_ENTITY_EXIST(entity)
					
		//add blip for vehicle
		blip = ADD_BLIP_FOR_ENTITY(entity)
		
		IF isSprite
			SET_BLIP_SPRITE(blip, RADAR_TRACE_RANDOM_CHARACTER)
		ELSE
			IF IS_ENTITY_A_VEHICLE(entity)				
				//set scale for blip
				IF NETWORK_IS_GAME_IN_PROGRESS()
					SET_BLIP_SCALE(blip,BLIP_SIZE_NETWORK_VEHICLE)
				ELSE
					SET_BLIP_SCALE(blip,BLIP_SIZE_PED)//BLIP_SIZE_VEHICLE) As per Imran, changing ambient vehicle blips to same size as peds. 
				ENDIF
				
				//set colour for blip if enemy
				IF isEnemy = FALSE
				
					SET_BLIP_AS_FRIENDLY(blip,TRUE)
				ELSE
					SET_BLIP_AS_FRIENDLY(blip,FALSE)
					SET_BLIP_COLOUR(blip, BLIP_COLOUR_RED)
				ENDIF		
			ELIF IS_ENTITY_A_PED(entity)
				//set scale for blip
				IF NETWORK_IS_GAME_IN_PROGRESS()
					SET_BLIP_SCALE(blip,BLIP_SIZE_NETWORK_PED)
				ELSE
					SET_BLIP_SCALE(blip,BLIP_SIZE_PED)
				ENDIF
				
				//set colour for blip if enemy
				IF isEnemy = FALSE
					SET_BLIP_AS_FRIENDLY(blip,TRUE)
				ELSE
					SET_BLIP_AS_FRIENDLY(blip,FALSE)
					SET_BLIP_COLOUR(blip, BLIP_COLOUR_RED)
				ENDIF
			ELIF IS_ENTITY_AN_OBJECT(entity)				
				//set scale and colour for blip
				IF NETWORK_IS_GAME_IN_PROGRESS()
					SET_BLIP_SCALE(blip,BLIP_SIZE_NETWORK_OBJECT)
				ELSE
					SET_BLIP_SCALE(blip,BLIP_SIZE_OBJECT)
				ENDIF
			ELSE
				SCRIPT_ASSERT("Unknown Entity Type. See Kev B.")
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("Cannot create blip. Entity does not exist")
	ENDIF
	
	*/
	
ENDFUNC


FUNC BLIP_PRIORITY GET_CORRECT_BLIP_PRIORITY(BLIP_PRIORITY_TYPE TypeOfBlip)
	SWITCH 	TypeOfBlip
	
		// highest
		CASE BP_HIGHLIGHTED
			RETURN(BLIPPRIORITY_HIGHEST)
		BREAK	
		
		// high - highest
		CASE BP_OTHER_TEAM
			RETURN(BLIPPRIORITY_HIGH_HIGHEST)
		BREAK
		
		// high
		CASE BP_SAME_TEAM
			RETURN(BLIPPRIORITY_HIGH)
		BREAK
		
		// med - high
		CASE BP_ENEMY_AI
			RETURN(BLIPPRIORITY_MED_HIGH)
		BREAK
		 
		// med - default - reserved for mission blips	
		
		// low - med
		CASE BP_CUFFED_OR_KEYS
			RETURN(BLIPPRIORITY_LOW_MED)
		BREAK		
		
		// low
		
		// lowest-low
		CASE BP_DEFAULT
		CASE BP_WANTED		
			RETURN(BLIPPRIORITY_LOW_LOWEST)
		BREAK
		
		// lowest	
		CASE BP_GANG_HOUSE
		CASE BP_SHOP
		CASE BP_DYING
		CASE BP_PLAYER_NOISE
		CASE BP_WEAPON
			RETURN(BLIPPRIORITY_LOWEST)
		BREAK
		
	ENDSWITCH
	RETURN(BLIPPRIORITY_LOWEST)
ENDFUNC




FUNC FLOAT WEAPON_RANGE_OF_PED(PED_INDEX pedID)
	FLOAT fReturn
	fReturn = GET_MAX_RANGE_OF_CURRENT_PED_WEAPON(PedID) * WEAPON_RANGE_MULTIPLIER_FOR_BLIPS
	IF (fReturn > 400.0)
		fReturn = 400.0
	ENDIF
	RETURN (fReturn)	
ENDFUNC


/// PURPOSE: Combines seperate RGBA components into a single int.
FUNC INT GET_INT_FROM_RGB(INT iRed, INT iGreen, INT iBlue, INT iAlpha = 255)

	// (R * 16777216) + (G * 65536) + (B * 256) + (A)
	
	INT iCol
	
	SET_BITS_IN_RANGE(iCol, 24, 31, iRed)
	SET_BITS_IN_RANGE(iCol, 16, 23, iGreen)
 	SET_BITS_IN_RANGE(iCol, 8,  15, iBlue)
	SET_BITS_IN_RANGE(iCol, 0,  7,  iAlpha) 

	RETURN iCol

ENDFUNC

FUNC INT GET_INT_FROM_HUD_COLOUR(HUD_COLOURS HudColour)
	INT r,g,b,a
	GET_HUD_COLOUR(HudColour, r,g,b,a)
	RETURN(GET_INT_FROM_RGB(r,g,b,a))
ENDFUNC

FUNC BOOL IS_PED_NOTICABLE_TO_PLAYER(PED_INDEX PedID, PLAYER_INDEX PlayerID, FLOAT fNoticableRadius)//, BOOL &bRespondingToSpecialAbility, INT &iResponseTimer, INT &iResponseTime)
	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PlayerID))
	AND DOES_ENTITY_EXIST(PedID)
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(PlayerID))
		AND NOT IS_PED_INJURED(PedID)
			
			// if ped is shooting and within range of the player then blip them
			IF IS_PED_SHOOTING(PedID) 
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(PedID)) < WEAPON_RANGE_OF_PED(pedID))
					RETURN(TRUE)
				ENDIF
			ENDIF	
			
			// if ped is running or shoot and within range of the player them blip them
			IF IS_PED_SPRINTING(PedID)
			OR IS_PED_RUNNING(PedID)
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(PedID)) < fNoticableRadius)			
					RETURN(TRUE)
				ENDIF
			ENDIF
			
			// if ped is involved in currently ongoing scripted conversation (in shootouts it might mean one line random dialogue)
			IF ( IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_PED_IN_CURRENT_CONVERSATION(PedID))
			OR IS_AMBIENT_SPEECH_PLAYING(PedID)
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(PedID)) < HEARD_RADIUS)			
					RETURN(TRUE)
				ENDIF
			ENDIF

			// if the player is pointing their weapon at the ped within range
			IF (PlayerID = GET_PLAYER_INDEX()) // targetting commands can only be called on local peds
				IF 	(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(PedID)) < fNoticableRadius)
				AND	(IS_PLAYER_FREE_AIMING_AT_ENTITY(PlayerID, pedID) OR IS_PLAYER_TARGETTING_ENTITY(PlayerID, pedID))
					RETURN(TRUE)
				ENDIF
			ENDIF
			
			// if ped is any vehicle
			IF IS_PED_IN_ANY_VEHICLE(PedID)
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(PedID)) < fNoticableRadius)
					RETURN TRUE
				ENDIF
			ENDIF
			
			/*
			//comment this out for now, see TODO B*1240376
			// if is ped responding to Trevor's special ability, singleplayer only
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF ( bRespondingToSpecialAbility = FALSE )
					IF IS_PLAYER_PLAYING(PlayerID)
						IF NOT IS_PED_INJURED(GET_PLAYER_PED(PlayerID))
							IF GET_ENTITY_MODEL(GET_PLAYER_PED(PlayerID)) = PLAYER_TWO
								SET_PED_RESET_FLAG(GET_PLAYER_PED(PlayerID), PRF_ForceCombatTaunt, TRUE)
								IF IS_SPECIAL_ABILITY_ACTIVE(PlayerID)
									IF NOT IS_AMBIENT_SPEECH_PLAYING(GET_PLAYER_PED(PlayerID))
										iResponseTime				= 1000 + GET_RANDOM_INT_IN_RANGE(0, 501)
										iResponseTimer 				= GET_GAME_TIMER()
										bRespondingToSpecialAbility = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF ( GET_GAME_TIMER() - iResponseTimer > iResponseTime )
						bRespondingToSpecialAbility = FALSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			*/
			
		ENDIF
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC


PROC CLEANUP_AI_PED_BLIP(AI_BLIP_STRUCT &EnemyBlipData)
	
	BOOL bEmptyStruct
	IF DOES_BLIP_EXIST(EnemyBlipData.BlipID)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Clean up AI blip. Removing blip for ped with ID ", NATIVE_TO_INT(EnemyBlipData.PedID), ".")
		
		REMOVE_BLIP(EnemyBlipData.BlipID)
		bEmptyStruct = TRUE
	ENDIF
	IF DOES_BLIP_EXIST(EnemyBlipData.VehicleBlipID)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Clean up AI blip. Removing associated vehicle blip.")
		REMOVE_BLIP(EnemyBlipData.VehicleBlipID)
		bEmptyStruct = TRUE
	ENDIF

	IF DOES_ENTITY_EXIST(EnemyBlipData.PedID)		
		IF NOT IS_ENTITY_DEAD(EnemyBlipData.PedID)
			IF DOES_PED_HAVE_AI_BLIP(EnemyBlipData.PedID)
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Clean up AI blip. Toggling off code request for AI blip.")
				SET_PED_HAS_AI_BLIP(EnemyBlipData.PedID, FALSE)	
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Clean up AI blip. ped is dead.")	
		ENDIF
		bEmptyStruct = TRUE		
	ENDIF

	IF (bEmptyStruct)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Clean up AI blip. Cleaning out AI blip struct.")
		AI_BLIP_STRUCT emptyAIBlipStruct
		EnemyBlipData = emptyAIBlipStruct
	ENDIF
ENDPROC


PROC CLEANUP_ALL_AI_PED_BLIPS(AI_BLIP_STRUCT &EnemyBlipData[])
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Cleaning up all AI ped blips...")

	INT i
	REPEAT COUNT_OF(EnemyBlipData) i
		CLEANUP_AI_PED_BLIP(EnemyBlipData[i])
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Will make the blips appear for enemy peds when noticed. They will fade away when out of range. 
/// PARAMS:
///    PedID - AI ped to display blip for.
///    EnemyBlipData - AI ped's blip data struct.
///    iBlipGang - 
///    PlayerID - 
///    bForceBlipOn - Set to true to force blip rendering even if the ped is not noticable.
///    fNoticableRadius - Distance from the player within which enemies are noticable when running, sprinting, being aimed at by the player or in vehicle. Passing -1.0 will use default value of NOTICABLE_RADIUS. Override by proving a positive value that fits your situation. 
///    NOTE: Now returns TRUE if the ped is injured/doesn't exist, FALSE if the ped is still uninjured.
FUNC BOOL UPDATE_AI_PED_BLIP(PED_INDEX PedID, AI_BLIP_STRUCT &EnemyBlipData, INT iBlipGang = -1, PLAYER_INDEX PlayerID = NULL, BOOL bForceBlipOn = FALSE, BOOL bShowCone = FALSE, FLOAT fNoticableRadius = -1.0, STRING strBlipName = NULL, INT iColour = -1, BLIP_SPRITE BlipSprite=RADAR_TRACE_INVALID, BOOL bBlipNameIsLiteralString=TRUE, BOOL bDisableEnemyAircraftBlips = FALSE) 
		
//	VEHICLE_INDEX VehicleID	
	
	// safety check
	IF (PlayerID = NULL)
		PlayerID = GET_PLAYER_INDEX()
	ENDIF
	
	//noticable radius check
	IF (fNoticableRadius < 0.0)				//if passed value was lower than 0.0
		fNoticableRadius = NOTICABLE_RADIUS	//use default value
	ENDIF	
	
	
	IF NOT IS_PED_INJURED(PedID)
	
		// Give ped ai blip if they don't already have one.
		IF NOT DOES_PED_HAVE_AI_BLIP(PedID)
//			#IF IS_DEBUG_BUILD
//				IF IS_STRING_NULL_OR_EMPTY(strDebugName)
//					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Adding AI blip for ped with ID ", NATIVE_TO_INT(PedID), ".")
//				ELSE
//					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Adding AI blip for ped ", strDebugName, " with ID ", NATIVE_TO_INT(PedID), ".")
//				ENDIF
//			#ENDIF
			
			BOOL bSetPedHasAIBlip = TRUE
			
			IF (IS_PED_IN_FLYING_VEHICLE(PedID) AND bDisableEnemyAircraftBlips)
				bSetPedHasAIBlip = FALSE
			ENDIF
			
			IF (bSetPedHasAIBlip)
				IF iColour = -1
					SET_PED_HAS_AI_BLIP(PedID, TRUE)		
				ELSE
					SET_PED_HAS_AI_BLIP_WITH_COLOUR(pedID, TRUE, iColour)
				ENDIF
				
				EnemyBlipData.PedID = PedID
				
				SET_PED_AI_BLIP_GANG_ID(PedID, iBlipGang)
				SET_PED_AI_BLIP_NOTICE_RANGE(PedID, fNoticableRadius)
							
				IF DOES_BLIP_EXIST(EnemyBlipData.BlipID)
					SET_BLIP_PRIORITY(EnemyBlipData.BlipID, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT (BlipSprite=RADAR_TRACE_INVALID)
			SET_PED_AI_BLIP_SPRITE(PedID, ENUM_TO_INT(BlipSprite))
		ENDIF			
		
		//Per frame state setting.
		SET_PED_AI_BLIP_FORCED_ON(PedID, bForceBlipOn)
		SET_PED_AI_BLIP_HAS_CONE(PedID, bShowCone)
	
			
		//Get a handle to the blip index as soon as we can.
		EnemyBlipData.BlipID = GET_AI_PED_PED_BLIP_INDEX( PedID )
		
		// if we have changed the sprite we need to re-apply some stuff each frame
		IF NOT (BlipSprite=RADAR_TRACE_INVALID)
			
			IF DOES_BLIP_EXIST(EnemyBlipData.BlipID)
			
				// colour
				IF NOT (iColour = -1)
					SET_BLIP_COLOUR(EnemyBlipData.BlipID, iColour)
				ENDIF
				
				// name
				IF NOT IS_STRING_NULL_OR_EMPTY(strBlipName)
					BEGIN_TEXT_COMMAND_SET_BLIP_NAME("STRING")
						IF (bBlipNameIsLiteralString)
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBlipName)
						ELSE
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strBlipName)
						ENDIF
					END_TEXT_COMMAND_SET_BLIP_NAME(EnemyBlipData.BlipID)
				ENDIF
				
				// priority
				SET_BLIP_PRIORITY(EnemyBlipData.BlipID, BLIPPRIORITY_HIGH)
				
			ENDIF
		ENDIF
		
		// The first time we get a handle to the main blip configure it.
		IF NOT IS_BIT_SET(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_CONFIGURED)
			IF DOES_BLIP_EXIST(EnemyBlipData.BlipID)

				
				#IF IS_DEBUG_BUILD
					IF bForceBlipOn 	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Blip is being forced on.")		ENDIF
					IF bShowCone 		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Blip has cone showing.")		ENDIF
				#ENDIF
			

				
				SET_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_CONFIGURED)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PedID)
			//Get a handle to any associated vehicle blip index as soon as we can.
			EnemyBlipData.VehicleBlipID = GET_AI_PED_VEHICLE_BLIP_INDEX(PedID)
			
			// The first time we get a handle to the main blip configure it.
			IF NOT IS_BIT_SET(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_VEH_CONFIGURED)

				IF DOES_BLIP_EXIST(EnemyBlipData.VehicleBlipID)
//					#IF IS_DEBUG_BUILD
//						IF IS_STRING_NULL_OR_EMPTY(strDebugName)
//							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Got a handle to associated vehicle blip.  Configuring blip.")
//						ELSE
//							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Got a handle to associated vehicle blip for ped ", strDebugName, ".  Configuring blip.")
//						ENDIF
//					#ENDIF

					// colour
					IF NOT (iColour = -1)
						SET_BLIP_COLOUR(EnemyBlipData.VehicleBlipID, iColour)
					ENDIF

					// name
					IF NOT IS_STRING_NULL_OR_EMPTY(strBlipName)
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("STRING")
							IF (bBlipNameIsLiteralString)
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBlipName)
							ELSE
								ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strBlipName)
							ENDIF
						END_TEXT_COMMAND_SET_BLIP_NAME(EnemyBlipData.VehicleBlipID)
					ENDIF
					SET_BLIP_PRIORITY(EnemyBlipData.VehicleBlipID, BLIPPRIORITY_HIGH)
					SET_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_VEH_CONFIGURED)
					
					
				ENDIF
				
			ELSE
			
				IF NOT DOES_BLIP_EXIST(EnemyBlipData.VehicleBlipID)
					EnemyBlipData.VehicleBlipID = NULL
					CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_VEH_CONFIGURED)				
				ENDIF
				
			ENDIF

		ELIF DOES_BLIP_EXIST(EnemyBlipData.VehicleBlipID)
//			#IF IS_DEBUG_BUILD
//				IF IS_STRING_NULL_OR_EMPTY(strDebugName)
//					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped is no longer in a vehicle with associated blip. Cleaning up reference.")
//				ELSE
//					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", strDebugName, " is no longer in a vehicle with associated blip. Cleaning up reference.")
//				ENDIF
//			#ENDIF
			EnemyBlipData.VehicleBlipID = NULL
			CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_VEH_CONFIGURED)
		ENDIF
	ELSE							
		RETURN TRUE		
	ENDIF	
	
	RETURN FALSE

ENDFUNC


PROC UPDATE_AI_PED_BLIP_FLASHING(PED_INDEX PedID, AI_BLIP_STRUCT &EnemyBlipData)
	
	IF NOT IS_PED_INJURED(PedID)
		
		IF NOT DOES_BLIP_EXIST(EnemyBlipData.BlipID)	//blip does not exist
		
			//add blip
			EnemyBlipData.BlipID = ADD_BLIP_FOR_ENTITY(PedID)
			SET_BLIP_SCALE(EnemyBlipData.BlipID, PICK_FLOAT( NETWORK_IS_GAME_IN_PROGRESS(), BLIP_SIZE_NETWORK_PED, BLIP_SIZE_PED))				
			SET_BLIP_PRIORITY(EnemyBlipData.BlipID, GET_CORRECT_BLIP_PRIORITY(BP_ENEMY_AI))
			SHOW_HEIGHT_ON_BLIP(EnemyBlipData.BlipID, TRUE)

			//make sure blip is fully faded in
			SET_BLIP_ALPHA(EnemyBlipData.BlipID, 255)
			
			SET_BLIP_NAME_FROM_TEXT_FILE(EnemyBlipData.BlipID, "BLIP_270")
			
			//reset state
			CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_FADING_OUT)
			CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_CHANGED_COLOR)
			
			// reset timers
			IF NETWORK_IS_GAME_IN_PROGRESS()
				EnemyBlipData.iFadeOutTimer  = GET_NETWORK_TIME()
			ELSE
				EnemyBlipData.iFadeOutTimer_SP = GET_GAME_TIMER()
			ENDIF
			
		ELSE	//blip does exist
		
			IF IS_BIT_SET(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_FADING_OUT)	//if it was previoulsy fading out
			
				//make blip red
				SET_BLIP_COLOUR(EnemyBlipData.BlipID, BLIP_COLOUR_RED)
			
				//make sure blip is fully faded in
				SET_BLIP_ALPHA(EnemyBlipData.BlipID, 255)
				
				SET_BLIP_NAME_FROM_TEXT_FILE(EnemyBlipData.BlipID, "BLIP_270")
			
				//reset state
				CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_FADING_OUT)
				CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_CHANGED_COLOR)
			
				// reset timers
				IF NETWORK_IS_GAME_IN_PROGRESS()
					EnemyBlipData.iFadeOutTimer  = GET_NETWORK_TIME()
				ELSE
					EnemyBlipData.iFadeOutTimer_SP = GET_GAME_TIMER()
				ENDIF
				
			ELSE
			
				IF NOT IS_BIT_SET(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_CHANGED_COLOR)
				
					//make blip red
					SET_BLIP_COLOUR(EnemyBlipData.BlipID, BLIP_COLOUR_RED)
					
					// make sure blip is fully faded in
					SET_BLIP_ALPHA(EnemyBlipData.BlipID, 255)
					
					SET_BLIP_NAME_FROM_TEXT_FILE(EnemyBlipData.BlipID, "BLIP_270")
					
					//reset timers
					IF NETWORK_IS_GAME_IN_PROGRESS()
						EnemyBlipData.iFadeOutTimer = GET_NETWORK_TIME()
					ELSE
						EnemyBlipData.iFadeOutTimer_SP = GET_GAME_TIMER()
					ENDIF
					
					//reset state
					CLEAR_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_FADING_OUT)
					SET_BIT(EnemyBlipData.iStateBitset, BIT_AI_BLIP_STATE_CHANGED_COLOR)
				
				ELSE
				
					BOOL bShouldFlash

					//check flashing timers
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , EnemyBlipData.iFadeOutTimer) > BLIP_FLASH_TIME)
							bShouldFlash = TRUE
						ENDIF
					ELSE
						IF ((GET_GAME_TIMER() - EnemyBlipData.iFadeOutTimer_SP) > BLIP_FLASH_TIME)
							bShouldFlash = TRUE
						ENDIF
					ENDIF
					
					//if it is time to flash/change colour
					IF (bShouldFlash)
				
						//change blip colour based on current blip colour
						SWITCH GET_BLIP_COLOUR(EnemyBlipData.BlipID)
							CASE BLIP_COLOUR_RED
								SET_BLIP_COLOUR(EnemyBlipData.BlipID, BLIP_COLOUR_BLUE)
								SET_BLIP_NAME_FROM_TEXT_FILE(EnemyBlipData.BlipID, "BLIP_270")
							BREAK
							CASE BLIP_COLOUR_BLUE
								SET_BLIP_COLOUR(EnemyBlipData.BlipID, BLIP_COLOUR_RED)
								SET_BLIP_NAME_FROM_TEXT_FILE(EnemyBlipData.BlipID, "BLIP_270")
							BREAK
						ENDSWITCH

						//reset timers
						IF NETWORK_IS_GAME_IN_PROGRESS()
							EnemyBlipData.iFadeOutTimer = GET_NETWORK_TIME()
						ELSE
							EnemyBlipData.iFadeOutTimer_SP = GET_GAME_TIMER()
						ENDIF
						
					ENDIF
				
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		// delete blip when dead
		IF DOES_BLIP_EXIST(EnemyBlipData.BlipID)
			CLEANUP_AI_PED_BLIP(EnemyBlipData)
		ENDIF
	ENDIF

ENDPROC

FUNC INT GET_ANGLE_OF_NORTH_BLIP_FROM_COORDS( VECTOR vCoord )
	BLIP_INDEX 	bNorthBlip 		= GET_NORTH_BLID_INDEX()
	VECTOR		vNorthCoords	= GET_BLIP_COORDS( bNorthBlip )
	
	FLOAT 		fOpposite 		= vNorthCoords.x - vCoord.x
	FLOAT 		fAdjacent  		= vNorthCoords.y - vCoord.y
	
	FLOAT 		fAngle 			= ATAN( fOpposite / fAdjacent )
	INT 		iAngle			= ROUND( fAngle )
	
	RETURN 		iAngle
ENDFUNC



