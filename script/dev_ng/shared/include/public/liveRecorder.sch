USING "rage_builtins.sch"
USING "globals.sch"
using "script_player.sch"
USING "commands_camera.sch"
USING "commands_script.sch"
USING "net_include.sch"

using "Usefulcommands.sch"

//==============================   WIDGETS   ================================
WIDGET_GROUP_ID thisWidget
WIDGET_GROUP_ID widCurrentVeh
WIDGET_GROUP_ID widSetup
TEXT_WIDGET_ID tVehModel,tVehicleRecordingName

//================================ BOOLS ====================================
bool bWarpToVehicle
bool bStart
bool bEXIT //exits this tool script
bool bShowPath, bShowGhost
bool bCatchup
bool bDisablePedsAndCars,bDisableDone

//================================ STRINGS ===================================

//string sVehicleRecordingName = "N/A"
string svehModel, sVehRecName
string sTemp

//================================ INTS ========================================
int iVehicleRecordingNumber = -1
//int iSetVehRecNo = -1
int iSavetoRecording = 999
int iPlaybackTime, iLastPlaybackTime
int IMaxPlaybackTime = 1
int iRecTypeCombo
int iStartTime //for catching up with posiotion in original recording
int iTimeGap
int iTemp

// ================================== FLOAT ===================================
float fCatchupMultiplier,fmultiplier

// ================================ VECTORS ======================================
vector vel

//================================ VEHICLES ======================================
vehicle_index LastVeh
vehicle_index ghostVeh

//================================= model names =================================
MODEL_NAMES vehModel

	

//================================= ENUMS =======================================

enum enumWidget
	WID_SETUP,
	WID_CURRENT_VEHICLE
ENDENUM

enum enumVehRecState
	REC_STATE_NO_VEH,
	REC_STATE_SELECT_VEH,
	REC_STATE_IN_VEH_NO_PLAYBACK,
	REC_STATE_PLAYING_BACK,
	REC_STATE_EDITING,
	REC_STATE_REJOINED_PLAYBACK
ENDENUM

//=========================== from enums ====================================
enumVehRecState vehRecState=REC_STATE_NO_VEH

DEBUGONLY proc initThisWidget(enumWidget WidgetToInit)
	TEXT_LABEL_31 txtLbl
	SWITCH WidgetToInit
		CASE WID_SETUP
			if DOES_WIDGET_GROUP_EXIST(widSetup)
				DELETE_WIDGET_GROUP(widSetup)
			ENDIF
			SET_CURRENT_WIDGET_GROUP(thisWidget)
				widSetup = START_WIDGET_GROUP("Set up recording")
					tVehModel = ADD_TEXT_WIDGET("Vehicle Model")
					tVehicleRecordingName = ADD_TEXT_WIDGET("Recording Name")
					//ADD_WIDGET_INT_SLIDER("Set Rec to playback",iSetVehRecNo,-1,999,-1)
					ADD_WIDGET_INT_SLIDER("PLayback rec no",iVehicleRecordingNumber,-1,999,1)
					ADD_WIDGET_INT_SLIDER("New Recording no",iSavetoRecording,1,999,1)
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("Reset and start Playback")						
						ADD_TO_WIDGET_COMBO("Reset vehicle to start pos")
						ADD_TO_WIDGET_COMBO("Record, starting from here")
					STOP_WIDGET_COMBO("Choose Edit Option",iRecTypeCombo)
					ADD_WIDGET_BOOL("ENGAGE!",bStart)
					ADD_WIDGET_BOOL("catch up with original",bCatchup)					
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(thisWidget)
		BREAK
		CASE WID_CURRENT_VEHICLE
			if DOES_WIDGET_GROUP_EXIST(widCurrentVeh)
				DELETE_WIDGET_GROUP(widCurrentVeh)
			ENDIF			
			SET_CURRENT_WIDGET_GROUP(thisWidget)
				widCurrentVeh = START_WIDGET_GROUP("Current Vehicle")
					if iVehicleRecordingNumber != -1
						txtLbl = "Active Recording Name: "
						txtLbl += sVehRecName
					ELSE
						txtLbl = "No recording active"
					ENDIF
					ADD_WIDGET_STRING(txtLbl)
					ADD_WIDGET_INT_READ_ONLY("Recording no",iVehicleRecordingNumber)
					if iVehicleRecordingNumber > 0
						if HAS_VEHICLE_RECORDING_BEEN_LOADED(iVehicleRecordingNumber,sVehRecName) ///xyz
							ADD_WIDGET_INT_SLIDER("Playback time",iPlaybackTime,0,floor(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iVehicleRecordingNumber,sVehRecName)),1) //xyz
						ELSE
							ADD_WIDGET_INT_SLIDER("Playback time",iPlaybackTime,0,1,1)
						ENDIF
					ELSE
						ADD_WIDGET_INT_SLIDER("Playback time",iPlaybackTime,0,1,1)
					ENDIF
					
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(thisWidget)
		BREAK
	ENDSWITCH
ENDPROC


DEBUGONLY proc INITWidgets()
	if not DOES_WIDGET_GROUP_EXIST(thisWidget)
	
		thisWidget = START_WIDGET_GROUP("Live Recorder")
			ADD_WIDGET_BOOL("EXIT",bEXIT)
			ADD_WIDGET_BOOL("Select vehicle to edit",bWarpToVehicle)	
			ADD_WIDGET_BOOL("Show path of recording",bShowPath)
			ADD_WIDGET_BOOL("Show ghost for original",bShowGhost)
			ADD_WIDGET_BOOL("Turn off peds/veh",bDisablePedsAndCars)
		STOP_WIDGET_GROUP()
		initThisWidget(WID_SETUP)
		initThisWidget(WID_CURRENT_VEHICLE)	
	ENDIF
ENDPROC

proc updateRecName(vehicle_index iveh)
	if IS_VEHICLE_DRIVEABLE(iveh)
		RECORDING_ID rID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(iveh)
		sVehRecName = GET_VEHICLE_RECORDING_NAME(rID)
		
		//sVehRecName = GET_CURRENT_PLAYBACK_NAME_FOR_VEHICLE(iveh)
		
		STRING_TO_INT(GET_STRING_FROM_STRING(sVehRecName,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)-3,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)),iVehicleRecordingNumber)
		sVehRecName=GET_STRING_FROM_STRING(sVehRecName,0,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)-3)
		
		//iVehicleRecordingNumber = string_to_int(GET_STRING_FROM_STRING(sVehRecName,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)-2,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)))
	//	sVehRecName = GET_STRING_FROM_STRING(sVehRecName,0,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)-2)
		initThisWidget(WID_CURRENT_VEHICLE)	
		SET_CONTENTS_OF_TEXT_WIDGET(tVehModel,GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(iveh))
		SET_CONTENTS_OF_TEXT_WIDGET(tVehicleRecordingName,sVehRecName)
	ENDIF
ENDPROC

PROC GET_CURRENT_VEHICLE_STATE()
	VEHICLE_INDEX player_vehicle

	if not IS_PED_INJURED(PLAYER_PED_ID())
		if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			player_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			if IS_VEHICLE_DRIVEABLE(player_vehicle)
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(player_vehicle)
					if IS_STRING_NULL(sVehRecName)
						updateRecName(player_vehicle)
					ELSE
						RECORDING_ID rID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(player_vehicle)
						sTemp = GET_VEHICLE_RECORDING_NAME(rID)
						STRING_TO_INT(GET_STRING_FROM_STRING(sTemp,GET_LENGTH_OF_LITERAL_STRING(sTemp)-3,GET_LENGTH_OF_LITERAL_STRING(sTemp)),iTemp)
						sTemp=GET_STRING_FROM_STRING(sTemp,0,GET_LENGTH_OF_LITERAL_STRING(sTemp)-3)
						if not ARE_STRINGS_EQUAL(sTemp,sVehRecName)
						or iVehicleRecordingNumber != itemp
							updateRecName(player_vehicle)
						ENDIF
					ENDIF
				
					if iLastPlaybackTime = 0
						iPlaybackTime = floor(GET_TIME_POSITION_IN_RECORDING(player_vehicle))
						iLastPlaybackTime = iPlaybackTime
					ENDIF										
					
					if vehRecState = REC_STATE_NO_VEH or vehRecState = REC_STATE_IN_VEH_NO_PLAYBACK
						vehRecState = REC_STATE_PLAYING_BACK
					ENDIF
					
					
					if iLastPlaybackTime != iPlaybackTime
						STOP_PLAYBACK_RECORDED_VEHICLE(player_vehicle)
						START_PLAYBACK_RECORDED_VEHICLE(player_vehicle,iVehicleRecordingNumber,sVehRecName) //xyz
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(player_vehicle,to_float(iPlaybackTime))
						iLastPlaybackTime = iPlaybackTime
					ENDIF
					
					IF floor(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iVehicleRecordingNumber,sVehRecName)) != IMaxPlaybackTime //xyz
						IMaxPlaybackTime = floor(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iVehicleRecordingNumber,sVehRecName)) //xyz
						initThisWidget(WID_CURRENT_VEHICLE)	
					ENDIF
					
					iPlaybackTime = floor(GET_TIME_POSITION_IN_RECORDING(player_vehicle))
					iLastPlaybackTime = iPlaybackTime
					
						
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


DEBUGONLY PROC ON_SCREEN_PRINTOUT()
	TEXT_LABEL_63 txtLbl
	switch vehRecState
		case REC_STATE_PLAYING_BACK
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			txtLbl = "Playing back rec no "
			txtLbl += iVehicleRecordingNumber
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.83,"STRING",txtLbl)
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			IF IS_PS3_VERSION()
				txtLbl = "Press SQUARE to begin editting to rec no "
			ELSE
				txtLbl = "Press X to begin editting to rec no "
			ENDIF
			txtLbl += iSavetoRecording
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.86,"STRING",txtLbl)		
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			txtLbl = "Press A to stop playback."			
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.89,"STRING",txtLbl)	
		BREAK
		
		CASE REC_STATE_SELECT_VEH
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.86,"STRING","Game Paused.")
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.89,"STRING","Use mouse with Fuzzy to select vehicle to edit.")
		BREAK
		
		CASE REC_STATE_EDITING
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.83,"STRING","Editing in progress.")
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			IF IS_PS3_VERSION()
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.86,"STRING","Press SQUARE to return to original route.")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.86,"STRING","Press X to return to original route.")
			ENDIF
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			IF IS_PS3_VERSION()
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.89,"STRING","Press CROSS to end recording.")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.89,"STRING","Press A to end recording.")
			ENDIF
			
		BREAK
		
		case REC_STATE_REJOINED_PLAYBACK
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.83,"STRING","Returning to original route.")
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			IF IS_PS3_VERSION()
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.86,"STRING","Let recording finish or press SQUARE to edit more.")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.86,"STRING","Let recording finish or press X to edit more.")
			ENDIF
			SET_TEXT_SCALE(0.4,0.4)
			SET_TEXT_CENTRE(true)
			IF IS_PS3_VERSION()
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.89,"STRING","Press CROSS to stop recording.")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.89,"STRING","Press A to stop recording.")
			ENDIF
		BREAK
	ENDSWITCH
	
	if IS_VEHICLE_DRIVEABLE(LastVeh)
		if IS_RECORDING_GOING_ON_FOR_VEHICLE(LastVeh)
			if GET_GAME_TIMER() - ((GET_GAME_TIMER() / 1000)*1000) > 300
			
			
				SET_TEXT_SCALE(0.6,0.6)
				SET_TEXT_CENTRE(true)
				SET_TEXT_COLOUR(255,0,0,255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.87,0.1,"STRING","RECORDING...")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

proc SHOW_GHOST()
	if bshowghost = TRUE
		IF vehRecState != REC_STATE_IN_VEH_NO_PLAYBACK
			if iVehicleRecordingNumber != 0 and not IS_STRING_NULL(sVehRecName)			
				if HAS_VEHICLE_RECORDING_BEEN_LOADED(iVehicleRecordingNumber,sVehRecName)
					int iTimer = (GET_GAME_TIMER() - iStartTime)	
					if iTimer >= 0 and iTimer <= GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iVehicleRecordingNumber,sVehRecName)
						vector vPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iVehicleRecordingNumber,to_float(iTimer),sVehRecName)
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
						DRAW_DEBUG_BOX(vPos-<<0.5,0.5,0.5>>,vPos+<<0.5,0.5,0.5>>)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

proc SHOW_RECORDING_PATH()
	VEHICLE_INDEX veh
	if bShowPath = TRUE
		if vehRecState != REC_STATE_IN_VEH_NO_PLAYBACK
			if not IS_PED_INJURED(PLAYER_PED_ID())
				if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
						if not DOES_ENTITY_EXIST(ghostVeh)
							if iVehicleRecordingNumber >= 1 and not IS_STRING_NULL(sVehRecName)							
								ghostVeh = CREATE_VEHICLE(GET_ENTITY_MODEL(veh),GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iVehicleRecordingNumber,0,sVehRecName))
								SET_ENTITY_COLLISION(ghostVeh,FALSE)
								SET_ENTITY_VISIBLE(ghostVeh,FALSE)
								START_PLAYBACK_RECORDED_VEHICLE(ghostVeh,iVehicleRecordingNumber,sVehRecName)
								PAUSE_PLAYBACK_RECORDED_VEHICLE(ghostVeh)
								DISPLAY_PLAYBACK_RECORDED_VEHICLE(ghostVeh,RDM_WHOLELINE)
							ENDIF
						ENDIF				
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	endif
	
	if DOES_ENTITY_EXIST(ghostVeh)
		if not IS_PED_INJURED(PLAYER_PED_ID())
			if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				if IS_VEHICLE_DRIVEABLE(veh)
					if not IS_RECORDING_GOING_ON_FOR_VEHICLE(veh) and not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
						if DOES_ENTITY_EXIST(ghostVeh)
							DELETE_VEHICLE(ghostVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

proc DISABLE_PEDS_CARS()
	if bDisablePedsAndCars = TRUE
		if bDisableDone = FALSE
			//SET_PED_DENSITY_MULTIPLIER(0.0)
			//SET_VEHICLE_DENSITY_MULTIPLIER(0.0)
			if not IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),250.0)
				CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()),250.0)
			ENDIF
			bDisableDone=true
		ENDIF
	ELSE
		if bDisableDone=true
			//SET_PED_DENSITY_MULTIPLIER(1.0)
			//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
			bDisableDone=FALSE
		ENDIF		
	ENDIF
ENDPROC

proc CHANGE_RECORDING_STATE()
	VEHICLE_INDEX veh
	if bWarpToVehicle=TRUE
		vehRecState = REC_STATE_SELECT_VEH
	ELSE
		if vehRecState = REC_STATE_SELECT_VEH
			vehRecState = REC_STATE_NO_VEH
		ENDIF
	ENDIF

	switch vehRecState
		case REC_STATE_PLAYING_BACK
			if IS_BUTTON_JUST_PRESSED(PAD1,SQUARE)				
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
							
							iStartTime = get_game_timer() - FLOOR(GET_TIME_POSITION_IN_RECORDING(veh))
							START_RECORDING_VEHICLE_TRANSITION_FROM_PLAYBACK(veh,iSavetoRecording,sVehRecName,true) //xyz
							vehRecState = REC_STATE_EDITING						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			if not IS_PED_INJURED(PLAYER_PED_ID())
				veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				if IS_VEHICLE_DRIVEABLE(veh)
					if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh) and not IS_RECORDING_GOING_ON_FOR_VEHICLE(veh)
						vehRecState = REC_STATE_IN_VEH_NO_PLAYBACK
						iLastPlaybackTime = 0
					ENDIF
				ENDIF
			ENDIF
			if IS_BUTTON_JUST_PRESSED(PAD1,CROSS)	
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(veh)
							vehRecState = REC_STATE_IN_VEH_NO_PLAYBACK
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		case REC_STATE_EDITING
			if IS_BUTTON_JUST_PRESSED(PAD1,SQUARE)				
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						START_PLAYBACK_RECORDED_VEHICLE(veh,iVehicleRecordingNumber,sVehRecName) //xyz
						SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(veh,4000)
						vehRecState = REC_STATE_REJOINED_PLAYBACK						
					ENDIF
				ENDIF
			ENDIF	
			if IS_BUTTON_JUST_PRESSED(PAD1,cross)	
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						STOP_RECORDING_VEHICLE(veh)
						vehRecState = REC_STATE_IN_VEH_NO_PLAYBACK
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		case REC_STATE_REJOINED_PLAYBACK
			if bCatchup = TRUE
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						if iPlaybackTime != 0
							if fmultiplier = 0.0
								fmultiplier = 1.0
							ENDIF
							
							println("rec time: ",(GET_GAME_TIMER() - iStartTime)," iPlaybackTime: ",iPlaybackTime)
							PRINTLN("fCatchupMultiplier:", fCatchupMultiplier)
							
								
							iTimeGap = (GET_GAME_TIMER() - iStartTime) - iPlaybackTime
							if iTimeGap < -25 or iTimeGap > 25
								fCatchupMultiplier = (TO_FLOAT(iTimeGap) / 2500.0) + 1.0
								if fCatchupMultiplier > 1.45 fCatchupMultiplier = 1.45 ENDIF
								if fCatchupMultiplier < 0.7 fCatchupMultiplier = 0.7 ENDIF
								if fCatchupMultiplier > 0.98 and fCatchupMultiplier < 1.0 fCatchupMultiplier = 0.98 ENDIF
								if fCatchupMultiplier < 1.02 and fCatchupMultiplier > 1.0 fCatchupMultiplier = 1.02 ENDIF
								fmultiplier += (fCatchupMultiplier - fmultiplier) * TIMESTEP() * 0.5
								SET_PLAYBACK_SPEED(veh,fmultiplier)
								
							ELSE
								SET_PLAYBACK_SPEED(veh,1.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF					
			ENDIF
			
			veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			if IS_VEHICLE_DRIVEABLE(veh)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(veh)
					STOP_RECORDING_VEHICLE(veh)
					vehRecState = REC_STATE_IN_VEH_NO_PLAYBACK
				ENDIF
			ENDIF
			
			if IS_BUTTON_JUST_PRESSED(PAD1,SQUARE)				
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						vel = GET_ENTITY_VELOCITY(veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(veh)
						SET_ENTITY_VELOCITY(veh,vel)
						vehRecState = REC_STATE_EDITING	
					ENDIF
				ENDIF
			ENDIF
			if IS_BUTTON_JUST_PRESSED(PAD1,cross)	
				if not IS_PED_INJURED(PLAYER_PED_ID())
					veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					if IS_VEHICLE_DRIVEABLE(veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(veh)
						STOP_RECORDING_VEHICLE(veh)
						vehRecState = REC_STATE_IN_VEH_NO_PLAYBACK
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC MODEL_NAMES GET_VEHICLE_MODEL_FROM_STRING(STRING sModelName)
	int i
	MODEL_NAMES modelName
	
	IF NOT IS_STRING_NULL(sModelName)
		while GET_VEHICLE_MODEL_FROM_INDEX(i,modelName)	
			IF NOT IS_STRING_NULL(GET_MODEL_NAME_FOR_DEBUG(modelName))
				if ARE_STRINGS_EQUAL(GET_MODEL_NAME_FOR_DEBUG(modelName),sModelName)
					RETURN modelName
				ENDIF
			ENDIF
			i++
		ENDWHILE
	ENDIF
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

proc CHECK_RECORDING_INITIALISED()
	
	if bStart = TRUE
		fmultiplier=0.0
		svehModel = GET_CONTENTS_OF_TEXT_WIDGET(tVehModel)
		sVehRecName = GET_CONTENTS_OF_TEXT_WIDGET(tVehicleRecordingName)
		vehModel = DUMMY_MODEL_FOR_SCRIPT
		if GET_LENGTH_OF_LITERAL_STRING(svehModel) > 0 and not IS_STRING_NULL(svehModel)
			IF IS_STRING_A_VEHICLE_MODEL(svehModel)
				vehModel = GET_VEHICLE_MODEL_FROM_STRING(svehModel)	
			ELSE
				vehModel = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
		ELSE
			vehModel = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		if vehModel != DUMMY_MODEL_FOR_SCRIPT
			if iVehicleRecordingNumber > 0		
				if GET_LENGTH_OF_LITERAL_STRING(sVehRecName) > 0
					if DOES_VEHICLE_RECORDING_FILE_EXIST_IN_CDIMAGE(iVehicleRecordingNumber,sVehRecName)
						REQUEST_VEHICLE_RECORDING(iVehicleRecordingNumber,sVehRecName)
						REQUEST_MODEL(vehModel)
						while not HAS_VEHICLE_RECORDING_BEEN_LOADED(iVehicleRecordingNumber,sVehRecName)
						or not HAS_MODEL_LOADED(vehModel)
							WAIT(0)
						ENDWHILE
						
						if iRecTypeCombo != 2
							if IS_VEHICLE_DRIVEABLE(LastVeh)
								if not IS_PED_INJURED(PLAYER_PED_ID())
									if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),LastVeh)
										SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(),GET_ENTITY_COORDS(PLAYER_PED_ID())+<<0,0,10>>)
										DELETE_VEHICLE(LastVeh)
									ELSE
										DELETE_VEHICLE(LastVeh)
									ENDIF
								ENDIF
							ENDIF
							vector vRot 
							vROT = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iVehicleRecordingNumber,0,sVehRecName)
														
							LastVeh = CREATE_VEHICLE(vehModel,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iVehicleRecordingNumber,0,sVehRecName))							
							SET_ENTITY_ROTATION(LastVeh,vRot)
						ELSE
							if not IS_PED_INJURED(PLAYER_PED_ID())
								LastVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
						ENDIF
						if not IS_PED_INJURED(PLAYER_PED_ID()) and IS_VEHICLE_DRIVEABLE(LastVeh)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),LastVeh)
							iStartTime = GET_GAME_TIMER()
							if iRecTypeCombo <= 1
								START_PLAYBACK_RECORDED_VEHICLE(LastVeh,iVehicleRecordingNumber,sVehRecName)								
							ENDIF
							if iRecTypeCombo = 1
								WAIT(0)
								if IS_VEHICLE_DRIVEABLE(LastVeh)
									STOP_PLAYBACK_RECORDED_VEHICLE(LastVeh)
								ENDIF
							ENDIF
							if iRecTypeCombo = 2								
								if IS_VEHICLE_DRIVEABLE(LastVeh)									
									START_RECORDING_VEHICLE(LastVeh,iSavetoRecording,sVehRecName,true)								
									vehRecState = REC_STATE_EDITING						
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SCRIPT_ASSERT("Vehicle recording not found for playback.")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SCRIPT_ASSERT("Can not find vehicle model for playback.")
		ENDIF
		bStart = FALSE
	ENDIF
ENDPROC

proc CHECK_PUT_PLAYER_IN_VEHICLE()
	entity_index focusEnt
	
	VEHICLE_INDEX focusVeh
	ped_index existingDriver
	
	if bWarpToVehicle=TRUE	
		focusEnt = GET_FOCUS_ENTITY_INDEX()
		SET_GAME_PAUSED(true)
		if not IS_ENTITY_DEAD(focusEnt)					
			if IS_ENTITY_A_VEHICLE(focusEnt)
				if not IS_PED_INJURED(PLAYER_PED_ID())
					if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						LastVeh = GET_VEHICLE_PED_IS_in(PLAYER_PED_ID())
					ENDIF
				ENDIF
			
				focusVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(focusEnt)
				if focusVeh != LastVeh
					if IS_VEHICLE_DRIVEABLE(focusVeh)
						if not IS_PED_INJURED(player_ped_id())
							existingDriver = GET_PED_IN_VEHICLE_SEAT(focusVeh,VS_DRIVER)
							
							if existingDriver != PLAYER_PED_ID()
								if DOES_ENTITY_EXIST(existingDriver)
									SET_ENTITY_AS_MISSION_ENTITY(existingDriver,true,true)
									SET_ENTITY_AS_MISSION_ENTITY(focusVeh,true,true)		
									IF DOES_VEHICLE_HAVE_FREE_SEAT(focusVeh)
										SET_PED_INTO_VEHICLE(existingDriver,focusVeh,VS_ANY_PASSENGER) 
									ELSE
										DELETE_PED(existingDriver)
									ENDIF
								ENDIF
							ENDIF
							
							SET_PED_INTO_VEHICLE(player_ped_id(),focusVeh)
							SET_DEBUG_CAM_ACTIVE(FALSE)
							SET_DEBUG_ACTIVE(FALSE)
							SET_GAME_PAUSED(FALSE)
							LastVeh=focusVeh
							bWarpToVehicle=FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF							
	ELSE
		SET_GAME_PAUSED(FALSE)
	ENDIF
ENDPROC

proc CHECK_EXIT_SCRIPT()
	
	if bEXIT = TRUE
		STOP_RECORDING_ALL_VEHICLES()
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

PROC liveRecorder()

	
	INITWidgets()
	
	CHECK_RECORDING_INITIALISED()
	CHECK_PUT_PLAYER_IN_VEHICLE()
	GET_CURRENT_VEHICLE_STATE()
	CHANGE_RECORDING_STATE()
	ON_SCREEN_PRINTOUT()
	SHOW_RECORDING_PATH()
	SHOW_GHOST()
	DISABLE_PEDS_CARS()
	CHECK_EXIT_SCRIPT()	
ENDPROC
