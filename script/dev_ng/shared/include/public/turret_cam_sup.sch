//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_cam_public.sch																		//
// Description: "Private" methods for turret_cam_script 													//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		12/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "turret_cam_def.sch"
USING "commands_graphics.sch"
USING "script_weapon_common.sch"

// @@ make sure this is up to date!
DEBUGONLY PROC TURRET_CAM_PRINT_CONFIG(TURRET_CAM_CONFIG& ref_cfg)
	INT i
	CDEBUG2LN(DEBUG_NET_TURRET, "[TURRET_CAM] TURRET_CAM_PRINT_CONFIG")
	CDEBUG2LN(DEBUG_NET_TURRET, "   LOADING")
	CDEBUG2LN(DEBUG_NET_TURRET, "      iLoadSceneWaitTimeMs ", ref_cfg.load.iLoadSceneWaitTimeMs)
	CDEBUG2LN(DEBUG_NET_TURRET, "      bDoLoadScene         ", IS_BIT_SET(ref_cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_LOADSCENE))
	CDEBUG2LN(DEBUG_NET_TURRET, "      bDoFadeOut           ", IS_BIT_SET(ref_cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEOUT))
	CDEBUG2LN(DEBUG_NET_TURRET, "      bDoFadeIn            ", IS_BIT_SET(ref_cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEIN))
	CDEBUG2LN(DEBUG_NET_TURRET, "   CAM LIMITS")
	CDEBUG2LN(DEBUG_NET_TURRET, "      fMinHeading          ", ref_cfg.camLimits.fMinHeading)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fMaxHeading          ", ref_cfg.camLimits.fMaxHeading)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fMinPitch            ", ref_cfg.camLimits.fMinPitch)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fMaxPitch            ", ref_cfg.camLimits.fMaxPitch)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fMinFov              ", ref_cfg.camLimits.fMinFov)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fMaxFov              ", ref_cfg.camLimits.fMaxFov)
	CDEBUG2LN(DEBUG_NET_TURRET, "   CAM SPEEDS")
	CDEBUG2LN(DEBUG_NET_TURRET, "      fBaseRotSpeed        ", ref_cfg.camSpeeds.fBaseRotSpeed)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fBaseZoomSpeed       ", ref_cfg.camSpeeds.fBaseZoomSpeed)
	CDEBUG2LN(DEBUG_NET_TURRET, "   HUD")
	CDEBUG2LN(DEBUG_NET_TURRET, "      eHudType             ", ref_cfg.eHudType)
	IF ref_cfg.eHudType = TCHT_ARENA_APOC
	OR ref_cfg.eHudType = TCHT_ARENA_CONS
	OR ref_cfg.eHudType = TCHT_ARENA_SCIFI
		CDEBUG2LN(DEBUG_NET_TURRET, "   ARENA HUD")
		CDEBUG2LN(DEBUG_NET_TURRET, "      bZoomVisible         ", ref_cfg.arenaHud.bZoomVisible)
		CDEBUG2LN(DEBUG_NET_TURRET, "      eIconMg              ", ref_cfg.arenaHud.eIconMg)
		CDEBUG2LN(DEBUG_NET_TURRET, "      eIconHm              ", ref_cfg.arenaHud.eIconHm)
		CDEBUG2LN(DEBUG_NET_TURRET, "      eIconPm              ", ref_cfg.arenaHud.eIconPm)
	ENDIF
	CDEBUG2LN(DEBUG_NET_TURRET, "   HUD SOUNDS")
	CDEBUG2LN(DEBUG_NET_TURRET, "      eHudSounds           ", ref_cfg.eHudSounds)
	CDEBUG2LN(DEBUG_NET_TURRET, "   WEAPON SOUNDS")
	CDEBUG2LN(DEBUG_NET_TURRET, "      eWeaponSounds        ", ref_cfg.eWeaponSounds)
	CDEBUG2LN(DEBUG_NET_TURRET, "   WEAPON")
	CDEBUG2LN(DEBUG_NET_TURRET, "      eFiringMode          ", ref_cfg.weapon.eFiringMode) 
	CDEBUG2LN(DEBUG_NET_TURRET, "      eShotType            ", ref_cfg.weapon.eShotType)
	CDEBUG2LN(DEBUG_NET_TURRET, "      iTimeBetweenShotsMs  ", ref_cfg.weapon.iTimeBetweenShotsMs)
	CDEBUG2LN(DEBUG_NET_TURRET, "      iWeaponDamage        ", ref_cfg.weapon.iWeaponDamage)
	CDEBUG2LN(DEBUG_NET_TURRET, "      fRange               ", ref_cfg.weapon.fRange)
	SWITCH ref_cfg.weapon.eFiringMode
		CASE WM_FULL_AUTO
			CDEBUG2LN(DEBUG_NET_TURRET, "   FULL AUTO")
			CDEBUG2LN(DEBUG_NET_TURRET, "      fSpreadAngleMax      ", ref_cfg.fullAuto.fSpreadAngleMax)
			CDEBUG2LN(DEBUG_NET_TURRET, "      iAmmoDurationMs      ", ref_cfg.fullAuto.iAmmoDurationMs)
			CDEBUG2LN(DEBUG_NET_TURRET, "      iReloadDurationMs    ", ref_cfg.fullAuto.iReloadDurationMs)
			BREAK
		CASE WM_HOMING_MISSILE
			CDEBUG2LN(DEBUG_NET_TURRET, "   HOMING MISSILE")
			CDEBUG2LN(DEBUG_NET_TURRET, "      model                ", GET_MODEL_NAME_FOR_DEBUG(ref_cfg.homingMissile.eModel), "(",ref_cfg.homingMissile.eModel,")")
			BREAK
	ENDSWITCH
	CDEBUG2LN(DEBUG_NET_TURRET, "   INSTRUCTIONAL BUTTONS (",ref_cfg.instButtons.iCount,")")
	REPEAT ref_cfg.instButtons.iCount i
		CDEBUG2LN(DEBUG_NET_TURRET, "      btn idx              ", i)
		CDEBUG2LN(DEBUG_NET_TURRET, "         label             ", ref_cfg.instButtons.txt15Labels[i])
		CDEBUG2LN(DEBUG_NET_TURRET, "         action            ", ref_cfg.instButtons.eActions[i])
	ENDREPEAT
	CDEBUG2LN(DEBUG_NET_TURRET, "   HELP TEXT")
	CDEBUG2LN(DEBUG_NET_TURRET, "      label                ", ref_cfg.txt15HelpLabel)
	CDEBUG2LN(DEBUG_NET_TURRET, "   JOINT Z")
	CDEBUG2LN(DEBUG_NET_TURRET, "      offset               ", ref_cfg.vJointZ)
	CDEBUG2LN(DEBUG_NET_TURRET, "   STATIC FIRE POINTS (",ref_cfg.staticFirePoints.iCount,")")
	REPEAT ref_cfg.staticFirePoints.iCount i
		CDEBUG2LN(DEBUG_NET_TURRET, "      boneId[",i,"]             ", ref_cfg.staticFirePoints.iBoneIds[i])
	ENDREPEAT
	CDEBUG2LN(DEBUG_NET_TURRET, "   PC INPUT MAP")
	CDEBUG2LN(DEBUG_NET_TURRET, "      label                ", ref_cfg.txt15InputMap)
ENDPROC

/// PURPOSE:
///    Will load assets and draw menu if buttons have been added.
PROC TURRET_CAM_MAINTAIN_INSTRUCTIONAL_BUTTONS(TURRET_CAM_INSTRUCTIONAL_BUTTONS& ref_btns, BOOL& ref_bHaveAddedBtns)
	IF ref_btns.iCount > 0
		BOOL bLoadedMenuAssets = LOAD_MENU_ASSETS()
		
		IF bLoadedMenuAssets
			IF NOT ref_bHaveAddedBtns
			OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
				REMOVE_MENU_HELP_KEYS()
				
				INT i
				REPEAT ref_btns.iCount i
					// NOTE: ref_btns.eActions[i] Cast control_action_group to a CONTROL_ACTION. The CONTROL_ACTION values are shifted up by .
					// where i < MAX_INPUTGROUPS : cast to CONTROL_ACTION_GROUP
					// where i >= MAX_INPUTGROUPS : Subtract MAX_INPUTGROUPS and use as nromal CONTROL_ACTION.
					//
					// This allows us to store control_action and control_action_group values in the same variable and
					// easily preserve the order that the buttons were added.
					CONTROL_ACTION_GROUP eActionGroup = INT_TO_ENUM(CONTROL_ACTION_GROUP, ENUM_TO_INT(ref_btns.eActions[i]))
					IF eActionGroup < MAX_INPUTGROUPS
						ADD_MENU_HELP_KEY_GROUP(eActionGroup, ref_btns.txt15Labels[i])
					ELSE
						CONTROL_ACTION eAction = INT_TO_ENUM(CONTROL_ACTION, ENUM_TO_INT(eActionGroup - MAX_INPUTGROUPS))
						ADD_MENU_HELP_KEY_CLICKABLE(eAction, ref_btns.txt15Labels[i])
					ENDIF
				ENDREPEAT
				ref_bHaveAddedBtns = TRUE
			ENDIF
			
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			DRAW_MENU_HELP_SCALEFORM(0)
		ENDIF
	ENDIF
ENDPROC

PROC TURRET_CAM_MAINTAIN_HELP_TEXT(STRING sHelp, BOOL bSuppress)
	IF IS_STRING_NULL_OR_EMPTY(sHelp)
		EXIT
	ENDIF
	
	IF bSuppress 
	 	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelp)
			CLEAR_HELP()
		ENDIF		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelp)
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] TURRET_CAM_MAINTAIN_HELP_TEXT - Showing help ", sHelp)
		CLEAR_HELP()
		PRINT_HELP_FOREVER(sHelp)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the bit if it is currently set.
/// RETURNS:
/// 	Returns TRUE if the bit was SET.   
FUNC BOOL CLEAR_BIT_IF_SET(INT& iBitset, INT iBit)
	IF IS_BIT_SET(iBitset, iBit)
		CLEAR_BIT(iBitset, iBit)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the bit if it is currently clear.
/// RETURNS:
///    Returns TRUE if the bit was CLEAR.   
FUNC BOOL SET_BIT_IF_CLEAR(INT& iBitset, INT iBit)
	IF NOT IS_BIT_SET(iBitset, iBit)
		SET_BIT(iBitset, iBit)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Does nothing if ref_iSoundId = -1.
/// PARAMS:
///    ref_iSoundId - Will be to -1 after releasing soundId.
PROC STOP_AND_RELEASE_SOUND_ID(INT& ref_iSoundId)
	IF ref_iSoundId <> -1
		STOP_SOUND(ref_iSoundId)
		RELEASE_SOUND_ID(ref_iSoundId)
		ref_iSoundId = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Does nothing if ref_iSoundId <> -1.
/// PARAMS:
///    ref_iSoundId - Will be set to a valid soundId.
PROC PLAY_SOUND_FRONTEND_WITH_NEW_ID(INT& ref_iSoundId, STRING sSoundName, STRING sSetName = NULL, BOOL bEnableOnReply = TRUE)
	IF ref_iSoundId = -1
		ref_iSoundId = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(ref_iSoundId, sSoundName, sSetName, bEnableOnReply)
	ENDIF
ENDPROC

/// PURPOSE:
///    Add to the record of damage taken this frame for the turret_cam_script.
PROC TURRET_CAM_HANDLE_INCOMING_DAMAGE(STRUCT_ENTITY_DAMAGE_EVENT& ref_eventData)
	INT iFrameCount = GET_FRAME_COUNT()
	IF g_turretDamageTaken.iFrame <> iFrameCount 
		g_turretDamageTaken.iFrame = iFrameCount 
		g_turretDamageTaken.iDamage = 0
		g_turretDamageTaken.iHeadshots = 0
	ENDIF
	
	IF ref_eventData.IsHeadShot
		g_turretDamageTaken.iHeadshots++
	ENDIF
	
	g_turretDamageTaken.iDamage += ROUND(ref_eventData.Damage)

	CDEBUG1LN(DEBUG_NET_TURRET, "  Updating g_turretDamageTaken frame = ", iFrameCount, " damage = ", g_turretDamageTaken.iDamage, " headshots = ", g_turretDamageTaken.iHeadshots)
ENDPROC
