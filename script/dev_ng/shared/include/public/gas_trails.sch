//******************************************************************************************
//************************************ Gas Trails ******************************************
//******************************************************************************************

// What it does...
// This header file will allow you to create and test a petrol trail which you can then add
// to your mission, with a few useful functions to boot.

// *********************************** Implementation *************************************
/* Add these two lines to the top of your script:
CONST_INT MAX_TRAIL_POINTS 80
USING "gas_trails.sch"

To your main loop add:
CONTROL_GAS_TRAILS(#IF IS_DEBUG_BUILD aWidget #ENDIF) //where aWidget is the parent widget that you use in your script. 
							  This will allow the header file to create a child widget that you can use to edit and create fire trails

To create and test a new fire trail:
1) open up the "gas trails" widget within your mission script's main widget.
2) There are a number of options here:
	a) Build -> record route : Pour petrol along the route, as you pour, the route is recorded and can later be exported for script.
	b) Build -> view debug lines : Draws the route with debug lines.
	c) Test -> reset for pouring : Re-equip your fuel can and then test pouring over the newly created trail.
	d) Test -> ignite trail : Demo flames burning along trail
	e) Save/Load -> output to script : Exports the recorded trail to script which you'd then copy and paste in to your mission script.
3) Once you've added these lines to your mission, you can add the following commands where appropriate:

RESET_GAS_TRAIL(bool showTrail) : Reset the gas trail for z-skips
FUNC BOOL IS_GAS_TRAIL_COMPLETE() : REturns true if all the points on the trail are removed.
HIDE_GAS_TRAIL() : Make the trail visible/invisible on the HUD.
FUNC BOOL IS_GAS_TRAIL_LINK_MISSING() : Returns true if the player misses a link in the gas trail.
IGNITE_GAS_TRAIL() : Set the trail to burn. It starts burning from the first point that you recorded on the trail.
CLEANUP_GAS_TRAIL() : Tidies it up on mission completion.

Notes: There will still potentially be missing links along the trail if, for example, you pour from opposite directions.
*/

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_clock.sch"
USING "commands_misc.sch"
USING "script_maths.sch"
USING "Script_player.sch"
USING "commands_path.sch"
USING "script_blips.sch"
USING "commands_weapon.sch"
USING "commands_fire.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	
	WIDGET_GROUP_ID gasTrailWidget

	
#ENDIF

//CONSTANTS
//CONST_INT MAX_TRAIL_POINTS 140   SET THIS IN YOUR SCRIPT

//Bool Variables
//bool bInit
bool bGasNearPoint
bool bHidden
bool bIgniteTrailFromStart
bool bInitGas


//Debug bools
#if IS_DEBUG_BUILD
bool bViewPouring
bool bRecordRoute
bool bWasPouring
bool bResetForPouring
bool bOutput
bool bDebugIgniteTrail
bool bShowGasTrail
bool bShowBurnRoute
bool bClearAllFires
bool bShowBurnProgress
#ENDIF

// bool Variables
bool allowTrailRemoval

//Debug Int Variables
#if IS_DEBUG_BUILD
int iGas
int iRecPoint
bool bDebugOff
#endif
int iIgnitePoint = -1
//int variables
int lastBlipPouredOn
int iLastBlipRemoved
int gasNodesRemaining = -1
int iGenerations = 0
int iCheckPouringTime
float trailIgniteRange = 0.5
float fVertOffset = 0.0
//int iPouringOnPoint

//Debug float Variables
#if IS_DEBUG_BUILD
FLOAT fDistanceTravelled
#endif

//float variables


//vector variables

vector vTrail
vector v_igniteAt
int igniteNode
// gas trail struct
struct gasTrailStruct
	vector coord
	//vector midCoord //coord between this and next point
	blip_index blip
	bool bPouredOn
ENDSTRUCT

struct fireStruct
	FIRE_INDEX id
	vector firePos
	int birthtime
endstruct

CONST_FLOAT CONST_F_BLIP_GAP 1.0

//FIRES
CONST_INT MAX_FIRES 31
fireStruct fire[MAX_FIRES]
bool bFireBurning
bool bUsePetrolFlameFX = TRUE
int	iFireStartTime,iStartFireNode
int iNextFireID
int iLastForwardNodeOnFire, iLastBackwardNodeOnFire

gasTrailStruct gasTrail[MAX_TRAIL_POINTS]

float fireNodeJumpTime = 160.0

FUNC VECTOR GET_GAS_TRAIL_IGNITE_POS()
	RETURN v_igniteAt
ENDFUNC

FUNC INT GET_GAS_TRAIL_IGNITE_NODE()
	RETURN igniteNode
ENDFUNC

PROC RESET_GAS_TRAIL(bool bShowTrail=TRUE)
	int i
	allowTrailRemoval = TRUE
	gasNodesRemaining = 0
	lastBlipPouredOn = -1
	iLastBackwardNodeOnFire=0
	iLastBackwardNodeOnFire=0
	bFireBurning=false
	IF bShowTrail = false
		bHidden = true
	endif
	REPEAT count_of(gasTrail) i
		REMOVE_DECALS_IN_RANGE(gasTrail[i].coord,1.0)
		
		IF NOT DOES_BLIP_EXIST(gasTrail[i].blip)
			IF NOT ARE_VECTORS_EQUAL(gasTrail[i].coord,<<0,0,0>>)
				gasNodesRemaining++
				gasTrail[i].blip = CREATE_BLIP_FOR_COORD(gasTrail[i].coord)
				SET_BLIP_HIDDEN_ON_LEGEND(gasTrail[i].blip,TRUE)
				SET_BLIP_SCALE(gasTrail[i].blip,0.4)
				SHOW_HEIGHT_ON_BLIP(gasTrail[i].blip,FALSE)
				IF NOT bShowTrail
					SET_BLIP_ALPHA(gasTrail[i].blip,0)
				ENDIF
				SET_BLIP_COLOUR(gasTrail[i].blip,BLIP_COLOUR_YELLOW)
			ENDIF
		ELSE
			gasTrail[i].coord = GET_BLIP_COORDS(gasTrail[i].blip) //why wa this commented out?
			
			IF NOT bShowTrail
				SET_BLIP_ALPHA(gasTrail[i].blip,0)
			ELSE
				SET_BLIP_ALPHA(gasTrail[i].blip,255)
			ENDIF
			gasNodesRemaining++
		ENDIF
		gasTrail[i].bPouredOn = FALSE
	ENDREPEAT
	
	REPEAT COUNT_OF(fire) i
		fire[i].firePos = <<0,0,0>>
		fire[i].birthtime = 0
	ENDREPEAT
ENDPROC

FUNC VECTOR GET_TRAIL_POINT()
	
	entity_index wepEnt
	int iNuzzleBone
	vector vNuzzlePour
	vector vGroundLand
	wepEnt = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(player_ped_id())
	IF DOES_ENTITY_EXIST(wepEnt)
		iNuzzleBone = GET_ENTITY_BONE_INDEX_BY_NAME(wepEnt,"Gun_Nuzzle")
		iNuzzleBone = 1 //above line is returning an error saying this is not returning valid bone index
		vNuzzlePour = GET_WORLD_POSITION_OF_ENTITY_BONE(wepEnt,iNuzzleBone)
		vNuzzlePour = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vNuzzlePour,GET_ENTITY_HEADING(wepEnt),<<0.35,0.0,-0.15>>) //these coords are backwards - because jerry can is returning strange heading.
		
		float fGroundHeight
		GET_GROUND_Z_FOR_3D_COORD(vNuzzlePour,fGroundHeight)
		vGroundLand = <<vNuzzlePour.x,vNuzzlePour.y,fGroundHeight>>
		
		#if IS_DEBUG_BUILD
			int i
			IF bViewPouring
				IF bDebugOff
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					bDebugOff=FALSE
				ENDIF
				DRAW_DEBUG_LINE(vNuzzlePour,vGroundLand,255,0,0)
				
				repeat count_of(gasTrail) i
					if not IS_VECTOR_ZERO(gasTrail[i].coord)
						IF DOES_BLIP_EXIST(gasTrail[i].blip)
							DRAW_DEBUG_SPHERE(gasTrail[i].coord,0.1,255,0,0)
						ELSE
							DRAW_DEBUG_SPHERE(gasTrail[i].coord,0.1,0,0,0)
						ENDIF
						IF i > 0
							DRAW_DEBUG_LINE(gasTrail[i-1].coord,gasTrail[i].coord,255,0,0)
						ENDIF
					endif
			ENDREPEAT
				
			ENDIF
		#endif
	endif	
	RETURN vGroundLand
ENDFUNC


//Initialise all the gas trail guff.
PROC INIT_GAS_TRAILS(#if is_debug_build widget_group_id &wWidget #endif)
	IF NOT bInitGas
		#if IS_DEBUG_BUILD		
		
			IF NOT DOES_WIDGET_GROUP_EXIST(gasTrailWidget)
				IF DOES_WIDGET_GROUP_EXIST(wWidget)
					SET_CURRENT_WIDGET_GROUP(wWidget)
					gasTrailWidget = START_WIDGET_GROUP("Gas Trails")
						START_WIDGET_GROUP("Setup")
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("Build")
							ADD_WIDGET_BOOL("Record Route (Pour gas)",bRecordRoute)
							ADD_WIDGET_INT_READ_ONLY("Points Recorded (Max 140)",iRecPoint)
							ADD_WIDGET_BOOL("View debug lines",bViewPouring)
						STOP_WIDGET_GROUP()
					
						START_WIDGET_GROUP("Test")
							ADD_WIDGET_BOOL("Reset for pouring",bResetForPouring)
							ADD_WIDGET_BOOL("Reset fires",bClearAllFires)
							ADD_WIDGET_BOOL("Ignite Trail",bDebugIgniteTrail)
							ADD_WIDGET_INT_SLIDER("Ignite at node",iIgnitePoint,-1,MAX_TRAIL_POINTS,1)
							ADD_WIDGET_INT_SLIDER("Fire generations",iGenerations,0,3,1)							
							ADD_WIDGET_INT_READ_ONLY("Gas in Can",iGas)
							ADD_WIDGET_INT_READ_ONLY("Gas nodes remaining",gasNodesRemaining)
							ADD_WIDGET_BOOL("Show route",bShowGasTrail)
							ADD_WIDGET_BOOL("Show burn route",bShowBurnRoute)
							ADD_WIDGET_BOOL("Show burn progress",bShowBurnProgress)
							ADD_WIDGET_FLOAT_SLIDER("vert offset",fVertOffset,-1.0,1.0,0.01)
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("Save/Load")
							ADD_WIDGET_BOOL("Output to script",bOutput)
						STOP_WIDGET_GROUP()
					
					STOP_WIDGET_GROUP()
					CLEAR_CURRENT_WIDGET_GROUP(wWidget)
				ENDIF
			ENDIF		
			
		//	GIVE_WEAPON_TO_PED(player_ped_id(),WEAPONTYPE_PETROLCAN,INFINITE_AMMO,TRUE)
			
		#endif
		
		bInitGas = TRUE
	ENDIF

ENDPROC

PROC MANAGE_DATA_AND_DEBUG()
	int i
	#if IS_DEBUG_BUILD
	
	IF HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
		iGas = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
	ENDIF		
	
	IF bClearAllFires
		bClearAllFires = FALSE
		STOP_FIRE_IN_RANGE(<<2454.0376, 4950.6436, 44.1316>>,50.0)
		IF bFireBurning
			REPEAT count_of(fire) i
				REMOVE_SCRIPT_FIRE(fire[i].id)
			ENDREPEAT
		ENDIF
		bDebugIgniteTrail = FALSE
		bIgniteTrailFromStart = FALSE
		bFireBurning = FALSE
		RESET_GAS_TRAIL(FALSE)
	ENDIF
	
	IF bResetForPouring
		bResetForPouring=FALSE
		RESET_GAS_TRAIL()
		
		IF HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
			ADD_AMMO_TO_PED(player_ped_id(),WEAPONTYPE_PETROLCAN,INFINITE_AMMO)
		ELSE
			GIVE_WEAPON_TO_PED(player_ped_id(),WEAPONTYPE_PETROLCAN,INFINITE_AMMO)
		ENDIF
	ENDIF
	
	//output data for script
	IF bOutput
		REPEAT count_of(gasTrail) i
			IF NOT ARE_VECTORS_EQUAL(gasTrail[i].coord,<<0,0,0>>)
				PRINTLN("gasTrail[",i,"].coord = ",gasTrail[i].coord)
				SAVE_STRING_TO_DEBUG_FILE("gasTrail[")
				SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("].coord = ")
				SAVE_VECTOR_TO_DEBUG_FILE(gasTrail[i].coord)				
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF			
		ENDREPEAT
		
		vector vTemp = GET_ENTITY_COORDS(player_ped_id())
		PRINTLN("SET_ENTITY_COORDS(player_ped_id(),",vTemp)
		SAVE_STRING_TO_DEBUG_FILE("IF NOT IS_PED_INJURED(player_ped_id())")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	SET_ENTITY_COORDS(player_ped_id(),")
		SAVE_VECTOR_TO_DEBUG_FILE(vTemp)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("ENDIF")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		bOutput=FALSE
	ENDIF
	
	
	#endif
	
	#if IS_DEBUG_BUILD
		IF bDebugIgniteTrail
			STOP_FIRE_IN_RANGE(<<2454.0376, 4950.6436, 44.1316>>,50.0)
			bDebugIgniteTrail = FALSE
			bIgniteTrailFromStart = TRUE
		endif
	#endif
	
	IF bIgniteTrailFromStart
		IF bFireBurning
			REPEAT count_of(fire) i
				REMOVE_SCRIPT_FIRE(fire[i].id)
			ENDREPEAT
		ENDIF
		
		bIgniteTrailFromStart=FALSE
		bFireBurning = TRUE
		iFireStartTime = get_game_timer()

		REPEAT COUNT_OF(gasTrail) i
			gasTrail[i].bPouredOn = TRUE
		ENDREPEAT
		
		IF iIgnitePoint != -1
			iLastBackwardNodeOnFire = iIgnitePoint				
			iLastForwardNodeOnFire = iIgnitePoint
			iStartFireNode = iIgnitePoint
		ELSE
			iLastBackwardNodeOnFire = -1				
			iLastForwardNodeOnFire = -1
			iStartFireNode = iIgnitePoint
		ENDIF
		iNextFireID = 1

	ENDIF
	
	#if IS_DEBUG_BUILD
	IF bShowGasTrail
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			
		TEXT_LABEL_3 txtlbl
		REPEAT count_of(gasTrail) i
			DRAW_DEBUG_LINE(gasTrail[i].coord,gasTrail[i].coord+<<0,0,1.0>>,255,0,0)
			txtlbl = ""
			txtlbl = i
			DRAW_DEBUG_TEXT(txtlbl,gasTrail[i].coord+<<0,0,1>>)
		ENDREPEAT
	endif
	
	if bShowBurnRoute
		REPEAT count_of(gasTrail) i
			IF DOES_BLIP_EXIST(gasTrail[i].blip)
				IF GET_BLIP_ALPHA(gasTrail[i].blip) != 0
					DRAW_DEBUG_LINE(GET_BLIP_COORDS(gasTrail[i].blip),GET_BLIP_COORDS(gasTrail[i].blip)+<<0,0,1.0>>,0,255,0)				
				ELSe
					DRAW_DEBUG_LINE(GET_BLIP_COORDS(gasTrail[i].blip),GET_BLIP_COORDS(gasTrail[i].blip)+<<0,0,1.0>>,0,0,255)									
				ENDIF
			ENDIF
		ENDREPEAT
	endif
	#endif
ENDPROC

#if IS_DEBUG_BUILD
PROC RECORD_GAS_TRAIL
	IF bRecordRoute
		WEAPON_TYPE wep
		GET_CURRENT_PED_WEAPON(player_ped_id(),wep)
		IF wep = WEAPONTYPE_PETROLCAN
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)	
				IF bWasPouring = FALSE
					allowTrailRemoval = FALSE
					bWasPouring = TRUE
					iRecPoint = 1
					fDistanceTravelled = 0.0
					
					//remove all previous blips and reset coords
					int i
					repeat count_of(gasTrail) i
						IF DOES_BLIP_EXIST(gasTrail[i].blip)
							REMOVE_BLIP(gasTrail[i].blip)
						ENDIF
						gasTrail[i].coord = <<0,0,0>>
					ENDREPEAT
					
					gasTrail[0].coord = GET_TRAIL_POINT()
					IF NOT DOES_BLIP_EXIST(gasTrail[0].blip)
						gasTrail[0].blip = CREATE_BLIP_FOR_COORD(gasTrail[0].coord)
						SET_BLIP_HIDDEN_ON_LEGEND(gasTrail[0].blip,TRUE)
						SET_BLIP_SCALE(gasTrail[0].blip,0.4)
						SET_BLIP_COLOUR(gasTrail[0].blip,BLIP_COLOUR_YELLOW)
						SHOW_HEIGHT_ON_BLIP(gasTrail[0].blip,FALSE)
					ELSE
						SET_BLIP_COORDS(gasTrail[0].blip,gasTrail[0].coord)
					ENDIF
				ELSE
					IF iRecPoint < MAX_TRAIL_POINTS
						gasTrail[iRecPoint].coord = GET_TRAIL_POINT()
						fDistanceTravelled = GET_DISTANCE_BETWEEN_COORDS(gasTrail[iRecPoint].coord,gasTrail[iRecPoint-1].coord)
						
						//update gas trail blip
						IF NOT DOES_BLIP_EXIST(gasTrail[iRecPoint].blip)
							gasTrail[iRecPoint].blip = CREATE_BLIP_FOR_COORD(gasTrail[iRecPoint].coord)
							SET_BLIP_HIDDEN_ON_LEGEND(gasTrail[iRecPoint].blip,TRUE)
							SET_BLIP_SCALE(gasTrail[iRecPoint].blip,0.4)
							SHOW_HEIGHT_ON_BLIP(gasTrail[iRecPoint].blip,FALSE)
						ELSE
							SET_BLIP_COORDS(gasTrail[iRecPoint].blip,gasTrail[iRecPoint].coord)
						ENDIF
						
						IF fDistanceTravelled > CONST_F_BLIP_GAP
							iRecPoint++
							fDistanceTravelled = 0
						ENDIF	
						
						
					ELSE
						SCRIPT_ASSERT("Array overran for storing trail points. Check with Kev Bolt.")
					ENDIF
				ENDIF					
			ELSE
				IF bWasPouring = TRUE
					bRecordRoute = FALSE
					allowTrailRemoval = TRUE
					bWasPouring = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		bWasPouring = FALSE
	ENDIF
ENDPROC
#endif

FUNC BOOL HAVE_TRAIL_BLIPS_BEEN_MISSED()
	int iC
	int segmentsOfPouring
	bool pouredOn
	REPEAT COUNT_OF(gasTrail) iC
		IF gasTrail[iC].bPouredOn = TRUE
			IF pouredOn = FALSE
				pouredOn = TRUE
				segmentsOfPouring++
				IF segmentsOfPouring > 1
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF pouredOn = TRUE
				pouredOn = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	RETURN FALSE
ENDFUNC

PROC CHECK_GAS_TRAIL_POURING()
/*
	IF NOT IS_PED_INJURED(player_ped_id())
		WEAPON_TYPE Mywep
		Mywep = WEAPONTYPE_UNARMED
		IF GET_CURRENT_PED_WEAPON(player_ped_id(),Mywep)
			IF Mywep = WEAPONTYPE_PETROLCAN
				IF IS_PED_SHOOTING(player_ped_id())
					SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
*/
	int i
	IF allowTrailRemoval
		WEAPON_TYPE wep
		GET_CURRENT_PED_WEAPON(player_ped_id(),wep)
		IF wep = WEAPONTYPE_PETROLCAN
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
			and IS_PED_SHOOTING(player_ped_id())
				IF iCheckPouringTime = 0
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					iCheckPouringTime = GET_GAME_TIMER() + 250
				ENDIF
				IF GET_GAME_TIMER() > iCheckPouringTime
					IF NOT bGasNearPoint
						//player hasn't yet hit a point on the trail
						float fRange
						float fRangeToLastStoredPetrolPoint
						float fClosest = 9999.9
						int iClosest
						
									
						vTrail = GET_TRAIL_POINT()
						
						
						
						//PRINTLN("vTrail:",vTrail)
						
						repeat count_of(gasTrail) i
							IF DOES_BLIP_EXIST(gasTrail[i].blip)
								fRange = GET_DISTANCE_BETWEEN_COORDS(vTrail,GET_BLIP_COORDS(gasTrail[i].blip))
								//PRINTLN(i,":",fRange)
								fRangeToLastStoredPetrolPoint = GET_DISTANCE_BETWEEN_COORDS(gasTrail[i].coord,GET_BLIP_COORDS(gasTrail[i].blip))
								
								IF fRangeToLastStoredPetrolPoint = 0.0
								or fRange < fRangeToLastStoredPetrolPoint
									IF fRange < 2.0
										gasTrail[i].coord = vTrail
									endif
								endif
				
								if fRange < fClosest
									fClosest = fRange
									iClosest = i								
								endif
							ENDIF
						ENDREPEAT
					
						
						if fClosest < 2.0
							IF lastBlipPouredOn != -1	
								/*
								IF lastBlipPouredOn = 1
								AND GET_BLIP_ALPHA(gasTrail[0].blip) != 0
									lastBlipPouredOn = 0
								ENDIF
								*/
								//to ensure blips continue to get removed if a bunch of them have been missed out
								IF ABSI(iLastBlipRemoved-iClosest) > 1
									iLastBlipRemoved = -1
								ENDIF
								
								If iLastBlipRemoved = -1 iLastBlipRemoved = lastBlipPouredOn endif
									
								IF ABSI(iLastBlipRemoved-iClosest) < 3
								AND ABSI(iLastBlipRemoved-iClosest) > 0
									int iPour
									IF iLastBlipRemoved < iClosest
										for iPour = iLastBlipRemoved to iClosest-1
											IF DOES_BLIP_EXIST(gasTrail[iPour].blip)
												IF GET_BLIP_ALPHA(gasTrail[iPour].blip) > 0						
													SET_BLIP_ALPHA(gasTrail[iPour].blip,0)
													gasTrail[iPour].bPouredOn = TRUE
													gasNodesRemaining--
												ENDIF
											ENDIF
										endfor
									ELSE
										for iPour = iClosest+1 to iLastBlipRemoved
											IF DOES_BLIP_EXIST(gasTrail[iPour].blip)
												IF GET_BLIP_ALPHA(gasTrail[iPour].blip) > 0						
													SET_BLIP_ALPHA(gasTrail[iPour].blip,0)
													gasTrail[iPour].bPouredOn = TRUE
													gasNodesRemaining--
												ENDIF
											ENDIF
										endfor
									ENDIF
									iLastBlipRemoved = lastBlipPouredOn
								ENDIF	
								
								IF iClosest = 0
									IF DOES_BLIP_EXIST(gasTrail[iClosest].blip)
										IF GET_BLIP_ALPHA(gasTrail[iClosest].blip) > 0						
											SET_BLIP_ALPHA(gasTrail[iClosest].blip,0)
											gasTrail[iClosest].bPouredOn = TRUE
											gasNodesRemaining--
										ENDIF
									ENDIF
								ENDIF
								
								IF iClosest = max_trail_points-1 //if this is the last point in the array											
									IF DOES_BLIP_EXIST(gasTrail[iClosest].blip)
										IF GET_BLIP_ALPHA(gasTrail[iClosest].blip) > 0						
											SET_BLIP_ALPHA(gasTrail[iClosest].blip,0)
											gasTrail[iClosest].bPouredOn = TRUE
											gasNodesRemaining--
										ENDIF
									ENDIF
								endif
										
								IF iClosest < max_trail_points-1 //if this isn't last in the array but the next one has not coord set
									IF IS_VECTOR_ZERO(gasTrail[iClosest+1].coord)
										IF DOES_BLIP_EXIST(gasTrail[iClosest].blip)
											IF GET_BLIP_ALPHA(gasTrail[iClosest].blip) > 0						
												SET_BLIP_ALPHA(gasTrail[iClosest].blip,0)
												gasTrail[iClosest].bPouredOn = TRUE
												gasNodesRemaining--
											ENDIF
										ENDIF
									ENDIF
								ENDIF														
							ENDIF
							
							//update coord for closest poured on point
							
					
							lastBlipPouredOn = iClosest
						ENDIF
						/*
						IF gasNodesRemaining = 1
							IF fclosest < 1.0
								IF GET_BLIP_ALPHA(gasTrail[iClosest].blip) > 0
									SET_BLIP_ALPHA(gasTrail[iClosest].blip,0)
									gasNodesRemaining--
								ENDIF
							ENDIF
						ENDIF
						*/
						/*
						IF fClosest > 1.2
							lastBlipPouredOn = -1
							fDistancePouredRecently = 0.0
						ENDIF*/
					ENDIF
				ENDIF
			ELSE	
				lastBlipPouredOn = -1
				iLastBlipRemoved = -1
				iCheckPouringTime = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_GAS_TRAIL_COMPLETE()
	IF gasNodesRemaining = 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ADD_FLAME_AT_COORD(vector flameCoord,bool isPetrolFlame = true)
	IF bUsePetrolFlameFX = FALSE isPetrolFlame = false endif
	REMOVE_SCRIPT_FIRE( fire[iNextFireID].id)
	IF fire[iNextFireID].birthtime != 0
	//	REMOVE_DECALS_IN_RANGE(fire[iNextFireID].firePos,0.4)
		FADE_DECALS_IN_RANGE(fire[iNextFireID].firePos,0.4,1.0)
	ENDIF
	fire[iNextFireID].birthtime = GET_GAME_TIMER()
	fire[iNextFireID].firePos = flameCoord
	fire[iNextFireID].id = START_SCRIPT_FIRE(flameCoord,iGenerations,isPetrolFlame)
	iNextFireID++
	IF iNextFireID >= MAX_FIRES
		iNextFireID = 0
	ENDIF
ENDPROC

PROC MAKE_FIRE_BURN_AT_NODE(int iNode,bool isPetrolFlame = true)
	IF iNode >= 0 //AND iNOde < MAX_TRAIL_POINTS
		
			
		
		
		//IF sin(
		
		IF iNode % 2 = 1 //create fire at mid point between two nodes
			int nodeOnTrail
			nodeOnTrail = iNode/2
			IF nodeOnTrail+1 < MAX_TRAIL_POINTS
				//CPRINTLN(DEBUG_TREVOR3,"Start fire at node : ",iNode," fire ID: ",iNextFireID)
				
				
				
				
				ADD_FLAME_AT_COORD(((gasTrail[iNode/2].coord + gasTrail[(iNode/2)+1].coord) / 2.0)+fVertOffset,isPetrolFlame)
				
				
				#if IS_DEBUG_BUILD
					IF bShowBurnProgress
						DRAW_DEBUG_SPHERE(gasTrail[iNode/2].coord+fVertOffset,0.3,255,255,0)
					ENDIF
				#endif
			//	REMOVE_DECALS_IN_RANGE(gasTrail[iNode/2].coord,0.5)
			endif
		ELSE
			IF iNode/2 < MAX_TRAIL_POINTS
//			CPRINTLN(DEBUG_TREVOR3,"Start fire at node : ",iNode," fire ID: ",iNextFireID)
			
				
				ADD_FLAME_AT_COORD(gasTrail[iNode/2].coord+fVertOffset,isPetrolFlame)
				
				#if IS_DEBUG_BUILD
					IF bShowBurnProgress
						DRAW_DEBUG_SPHERE(gasTrail[iNode/2].coord+fVertOffset,0.3,255,255,0)
					ENDIF
				#endif
			//	REMOVE_DECALS_IN_RANGE(gasTrail[iNode/2].coord,0.5)
			ENDIF
			//CPRINTLN(DEBUG_TREVOR3,"Start fire at node : ",iNode," fire ID: ",iNextFireID)			
		ENDIF
		
		
		
		
	ENDIF
ENDPROC

PROC SET_PETROL_FLAME_TO_NODE(int iNode, bool bOnlyGoForward, bool usePEtrolFlameFX = TRUE)
	bUsePetrolFlameFX = usePEtrolFlameFX
	IF iNode > -1
		bFireBurning = TRUE						
		iFireStartTime = get_game_timer() //floor(to_float(get_game_timer()) - (fDistanceAlongNode * 150.0))
							//fClosestDistAlongNode=fClosestDistAlongNode

		iStartFireNode = iNode * 2 //(floor(fClosestDistAlongNode / (CONST_F_BLIP_GAP / 2.0))) - 1
		iLastForwardNodeOnFire = iStartFireNode
		
		IF bOnlyGoForward
			iLastBackwardNodeOnFire = 0
		ELSE
			iLastBackwardNodeOnFire = iStartFireNode
		ENDIF
		
		int iFire
		REPEAT COUNT_OF(gasTrail) iFire
			gasTrail[iFire].bPouredOn = TRUE
		ENDREPEAT
		
		
		REPEAT COUNT_OF(fire) iFire
			REMOVE_SCRIPT_FIRE( fire[iFire].id)
			IF fire[iFire].birthtime != 0
				//REMOVE_DECALS_IN_RANGE(fire[iFire].firePos,0.4)
				FADE_DECALS_IN_RANGE(fire[iFire].firePos,0.4,1.0)
			ENDIF
			fire[iFire].birthtime = 0
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAKE_GAS_TRAIL_BURN()
	int i
	
	//check if the fire has been ignited by the player
	
	vector vFireLoc
	float fDistanceAlongNode
	int iFireNode
	vFireLoc = <<0,0,0>>
	
	//check if there are any fires in the game world
	//IF IS_GAS_TRAIL_COMPLETE()
		IF bFireBurning = FALSE
			IF GET_CLOSEST_FIRE_POS(vFireLoc,gasTrail[0].coord) //is there a fire anywhere
				//find if any of the petrol nodes have a fire nearby.
				fDistanceAlongNode = 0
				float fThisRange
				float fClosestRange = 999999.0
				float fClosestDistAlongNode
				//int iFireNode
				
				
			
					REPEAT COUNT_OF(gasTrail) i	
						IF i > 0
							fDistanceAlongNode += GET_DISTANCE_BETWEEN_COORDS(gasTrail[i-1].coord,gasTrail[i].coord)
						ENDIF
						
						IF gasTrail[i].bPouredOn
							IF GET_CLOSEST_FIRE_POS(vFireLoc,gasTrail[i].coord)
								
								fThisRange = GET_DISTANCE_BETWEEN_COORDS(gasTrail[i].coord,vFireLoc) //< trailIgniteRange
								if fThisRange < fClosestRange
									fClosestRange = fThisRange						
									fClosestDistAlongNode = fDistanceAlongNode
									iFireNode = i
								endif
							endif
						endif
					
					ENDREPEAT
					
				
					if fClosestRange < trailIgniteRange		
						CDEBUG1LN(DEBUG_MISSION, "[gas_trails.sch] Igniting trail: closest node = ", iFireNode, " node pos = ", gasTrail[iFireNode].coord, " fire pos = ", vFireLoc)
					
					//	STOP_FIRE_IN_RANGE(gasTrail[iFireNode].coord,2.0)
						iFireNode=iFireNode
						bFireBurning = TRUE						
						iFireStartTime = get_game_timer() //floor(to_float(get_game_timer()) - (fDistanceAlongNode * 150.0))
						fClosestDistAlongNode=fClosestDistAlongNode
						
						//store where trail ignited
						v_igniteAt = gasTrail[iFireNode].coord
						igniteNode = iFireNode
						
						iStartFireNode = iFireNode * 2 //(floor(fClosestDistAlongNode / (CONST_F_BLIP_GAP / 2.0))) - 1
						iLastForwardNodeOnFire = iStartFireNode
						iLastBackwardNodeOnFire = iStartFireNode
						IF iLastBackwardNodeOnFire < 0 iLastBackwardNodeOnFire=0 endif
						IF iLastForwardNodeOnFire >= (COUNT_OF(gasTrail)*2) iLastForwardNodeOnFire = (COUNT_OF(gasTrail)*2)-1 ENDIF
					endif
				
			ENDIF
		endif
	//ENDIF

	

	IF bFireBurning = TRUE				
		SET_DISABLE_PETROL_DECALS_IGNITING_THIS_FRAME()
		float fDistanceBurned
	
		fDistanceBurned = to_float(get_game_timer() - iFireStartTime) / fireNodeJumpTime
		int iNodesBurned = (floor(fDistanceBurned / (CONST_F_BLIP_GAP / 2.0))) - 1
		
		int iFurthestNodeFwdOnFire = iStartFireNode + iNodesBurned
		int iFurthestNodeBwdOnFire = iStartFireNode - iNodesBurned
		
		IF iFurthestNodeFwdOnFire >= MAX_TRAIL_POINTS*2
			iFurthestNodeFwdOnFire = MAX_TRAIL_POINTS*2 -1
		ENDIF
		
		IF iFurthestNodeBwdOnFire < 0
			iFurthestNodeBwdOnFire = 0
		ENDIF

		int blipFromFire									
		IF iFurthestNodeFwdOnFire > iLastForwardNodeOnFire
			FOR i = iLastForwardNodeOnFire+1 to iFurthestNodeFwdOnFire
				//if player puts down jerry can, blip status is changes, so need to cater for this
				
				blipFromFire = i / 2
				IF blipFromFire < COUNT_OF(gasTrail)
				
					IF gasTrail[blipFromFire].bPouredOn						
						MAKE_FIRE_BURN_AT_NODE(i)	
					ELSE //stop the fire burning any further
						iFurthestNodeFwdOnFire = i-1
						i = iFurthestNodeFwdOnFire+1 //exit the for loop											
					ENDIF								
				ENDIF
			ENDFOR
			iLastForwardNodeOnFire = iFurthestNodeFwdOnFire
		ENDIF
			
		IF iFurthestNodeBwdOnFire < iLastBackwardNodeOnFire
			FOR i = iLastBackwardNodeOnFire-1  to iFurthestNodeBwdOnFire step -1
				blipFromFire = i / 2
				IF blipFromFire < COUNT_OF(gasTrail)					
					IF gasTrail[blipFromFire].bPouredOn
						MAKE_FIRE_BURN_AT_NODE(i)	
					ELSE //stop the fire burning any further
						iFurthestNodeBwdOnFire = i+1
						i = iFurthestNodeBwdOnFire-1 //exit the for loop					
					ENDIF					
				ENDIF
			ENDFOR
			iLastBackwardNodeOnFire = iFurthestNodeBwdOnFire
		ENDIF
		
		
		repeat count_of(fire) i
			if get_game_timer() - fire[i].birthtime > 1000
				//REMOVE_DECALS_IN_RANGE(fire[i].firePos,0.4)	
				FADE_DECALS_IN_RANGE(fire[i].firePos,0.4,1.0)
			endif
			if get_game_timer() - fire[i].birthtime > 6000
				REMOVE_SCRIPT_FIRE( fire[i].id)							
			endif
		ENDREPEAT
		
		//CPRINTLN(DEBUG_TREVOR3,"fDistanceBurned: ",fDistanceBurned," iNodesBurned: ",iNodesBurned,"iFurthestNodeFwdOnFire : ",iFurthestNodeFwdOnFire," iStartFireNode: ",iStartFireNode)
		//CPRINTLN(DEBUG_TREVOR3,"iFurthestNodeBwdOnFire: ",iFurthestNodeBwdOnFire," iLastForwardNodeOnFire: ",iLastForwardNodeOnFire," iLastBackwardNodeOnFire: ",iLastBackwardNodeOnFire)
	
	
	
	
	
	
	
	ENDIF
ENDPROC



PROC HIDE_GAS_TRAIL(bool hide = TRUE)
	int i
	
	IF hide= TRUE
		IF bHidden = FALSE
			REPEAT COUNT_OF(gasTrail) i
				IF DOES_BLIP_EXIST(gasTrail[i].blip)
					IF GET_BLIP_ALPHA(gasTrail[i].blip) < 255
						REMOVE_BLIP(gasTrail[i].blip)
					ELSE
						SET_BLIP_ALPHA(gasTrail[i].blip,0)
					ENDIF
				ENDIF
			ENDREPEAT
			bHidden = TRUE
		ENDIF
	else
		IF bHidden
			REPEAT COUNT_OF(gasTrail) i
				IF DOES_BLIP_EXIST(gasTrail[i].blip)					
					SET_BLIP_ALPHA(gasTrail[i].blip,255)
				ELSE
					gasTrail[i].blip = CREATE_BLIP_FOR_COORD(gasTrail[i].coord)
					SET_BLIP_HIDDEN_ON_LEGEND(gasTrail[i].blip,TRUE)
					SET_BLIP_SCALE(gasTrail[i].blip,0.4)
					SET_BLIP_ALPHA(gasTrail[i].blip,0)
					SET_BLIP_COLOUR(gasTrail[i].blip,BLIP_COLOUR_YELLOW)
					SHOW_HEIGHT_ON_BLIP(gasTrail[i].blip,FALSE)
				ENDIF
			ENDREPEAT
			bHidden=FALSE
		ENDIF
		
	endif
ENDPROC

FUNC BOOL IS_GAS_TRAIL_LINK_MISSING(int iStartCheck = 0, int iEndCheck=-1)
	int i
	//take in to account player dropping weapon
	IF iEndCheck = -1
		iendCheck = COUNT_OF(gasTrail)-1
	ENDIF
	
	For i = iStartCheck+1 to iendCheck	
		IF NOT gasTrail[i].bPouredOn
			RETURN TRUE			
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GAS_TRAIL_BURNING()
	//int i
	IF bFireBurning = TRUE
		RETURN TRUE
	ENDIF
	/*
	REPEAT COUNT_OF(gasTrail) i	
		IF gasTrail[i].bPouredOn		
			IF GET_NUMBER_OF_FIRES_IN_RANGE(gasTrail[i].coord,0.6) > 0	
				v_igniteAt = gasTrail[i].coord
				igniteNode = i
				RETURN TRUE
			ENDIF			
		ENDIF
	ENDREPEAT*/
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GAS_NODE_BURNING(int iNodeToCheck)
	IF GET_NUMBER_OF_FIRES_IN_RANGE(gasTrail[iNodeToCheck].coord,0.6) > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
//-1 = to the end
	
FUNC BOOL IS_GAS_TRAIL_BURNING_AND_CAN_THE_FIRE_BURN_OUT()
	int i
	
	FOR i = COUNT_OF(gasTrail)-1 to 0 step -1
		IF gasTrail[i].bPouredOn		
			IF GET_NUMBER_OF_FIRES_IN_RANGE(gasTrail[i].coord,0.6) > 0						
				v_igniteAt = gasTrail[i].coord
				igniteNode = i
				RETURN TRUE
			ENDIF	
		ELSE
			RETURN FALSE
		ENDIF	
	endfor
	RETURN FALSE
ENDFUNC

PROC IGNITE_GAS_TRAIL()
	bIgniteTrailFromStart = TRUE
ENDPROC

PROC CLEANUP_GAS_TRAILS()

	int i
	REPEAT count_of(fire) i
		REMOVE_SCRIPT_FIRE(fire[i].id)
		fire[i].birthtime = 0
	ENDREPEAT
	RESET_GAS_TRAIL(FALSE)
	REPEAT COUNT_OF(gasTrail) i
		IF DOES_BLIP_EXIST(gasTrail[i].blip)
			REMOVE_BLIP(gasTrail[i].blip)
		ENDIF
		gasTrail[i].bPouredOn = FALSE
	ENDREPEAT
	gasNodesRemaining = -1 //when = 0, trail is considered complete - default to a high value

ENDPROC

PROC CONTROL_GAS_TRAILS(#if is_debug_build WIDGET_GROUP_ID widget = null #endif)
	IF NOT IS_PED_INJURED(player_ped_id())
		INIT_GAS_TRAILS(#if is_debug_build widget #endif)
		MANAGE_DATA_AND_DEBUG()
		#if IS_DEBUG_BUILD
		RECORD_GAS_TRAIL()
		#endif
		CHECK_GAS_TRAIL_POURING()
		
		MAKE_GAS_TRAIL_BURN()		
	ENDIF
ENDPROC
