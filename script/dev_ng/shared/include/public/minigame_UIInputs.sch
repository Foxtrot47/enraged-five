// minigame_UIInputs.sch
USING "minigames_helpers.sch"

CONST_INT MAX_INST_BUTTONS_PER_LABEL	2
CONST_INT MAX_INST_BUTTONS				4
CONST_INT ANALOG_OFFSET					128

ENUM MINIGAME_INST_ORIENTATION
	SF_UI_Horizontal 	= 0,
	SF_UI_Vertical 		= 1
ENDENUM

STRUCT SCRIPT_SCALEFORM_UI
	SCALEFORM_INDEX 				sfMov 	= NULL
	INT 							iBools	= 0
	
	MINIGAME_INST_ORIENTATION		eOrient = SF_UI_Horizontal
	
	// Can have 2 buttons per label.
	CONTROL_ACTION					eInputs[MAX_INST_BUTTONS*MAX_INST_BUTTONS_PER_LABEL]
	STRING							eButtons[MAX_INST_BUTTONS*MAX_INST_BUTTONS_PER_LABEL]
	STRING							sLabels[MAX_INST_BUTTONS]
ENDSTRUCT

ENUM SCRIPT_SCALEFORM_BOOLS
	SF_UI_LOADED 				= BIT0,
	SF_UI_CREATED_ButtonDown	= BIT1,
	SF_UI_CREATED_ButtonUp		= BIT2,
	SF_UI_IsForMenu				= BIT3
ENDENUM

/// PURPOSE:
///    Checks to see if the script scaleform UI passed in is valid or not, also checks to see if it's loaded..
/// RETURNS:
///    TRUE if valid and loaded. Also sets an internal flag that says the mov is loaded.
FUNC BOOL IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(SCRIPT_SCALEFORM_UI & sfMovieStruct)
	IF (sfMovieStruct.sfMov <> NULL)
		IF HAS_SCALEFORM_MOVIE_LOADED(sfMovieStruct.sfMov)
			SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_LOADED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up the instructions for use
///    NOTE, iNumCols is a funny param. If it's set to 0, it makes one horizontal bar of commands. If it's set to 1, it
///    draws 1 vertical bar.
/// RETURNS:
///    The movie that has been requested
PROC SETUP_MINIGAME_INSTRUCTIONS(SCRIPT_SCALEFORM_UI & sfMovieStruct, BOOL bVertical,
		STRING eButton1 = NULL, STRING sLabel1 = NULL,	
		STRING eButton2 = NULL, STRING sLabel2 = NULL, 
		STRING eButton3 = NULL, STRING sLabel3 = NULL,
		STRING eButton4 = NULL, STRING sLabel4 = NULL)
	
	IF (sfMovieStruct.sfMov = NULL)
		sfMovieStruct.sfMov = REQUEST_SCALEFORM_MOVIE_INSTANCE("instructional_buttons")
	ENDIF	
	sfMovieStruct.iBools = 0
	
	sfMovieStruct.eInputs[0] = MAX_INPUTS
	sfMovieStruct.eInputs[1] = MAX_INPUTS
	sfMovieStruct.eInputs[2] = MAX_INPUTS
	sfMovieStruct.eInputs[3] = MAX_INPUTS
	sfMovieStruct.eInputs[4] = MAX_INPUTS
	sfMovieStruct.eInputs[5] = MAX_INPUTS
	sfMovieStruct.eInputs[6] = MAX_INPUTS
	sfMovieStruct.eInputs[7] = MAX_INPUTS

	sfMovieStruct.eButtons[0] = eButton1
	sfMovieStruct.eButtons[1] = eButton2
	sfMovieStruct.eButtons[2] = eButton3
	sfMovieStruct.eButtons[3] = eButton4
	sfMovieStruct.eButtons[4] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	sfMovieStruct.eButtons[5] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	sfMovieStruct.eButtons[6] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	sfMovieStruct.eButtons[7] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	
	sfMovieStruct.sLabels[0] = sLabel1
	sfMovieStruct.sLabels[1] = sLabel2
	sfMovieStruct.sLabels[2] = sLabel3
	sfMovieStruct.sLabels[3] = sLabel4
	
	IF bVertical
		sfMovieStruct.eOrient = SF_UI_Vertical
	ELSE
		sfMovieStruct.eOrient = SF_UI_Horizontal
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sfMovieStruct.sfMov)
		SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_LOADED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up the instructions for use
///    NOTE, iNumCols is a funny param. If it's set to 0, it makes one horizontal bar of commands. If it's set to 1, it
///    draws 1 vertical bar.
/// RETURNS:
///    The movie that has been requested
PROC SETUP_MINIGAME_INSTRUCTION_INPUTS(SCRIPT_SCALEFORM_UI & sfMovieStruct, BOOL bVertical,
		CONTROL_ACTION eInput1 = MAX_INPUTS, STRING sLabel1 = NULL,	
		CONTROL_ACTION eInput2 = MAX_INPUTS, STRING sLabel2 = NULL, 
		CONTROL_ACTION eInput3 = MAX_INPUTS, STRING sLabel3 = NULL,
		CONTROL_ACTION eInput4 = MAX_INPUTS, STRING sLabel4 = NULL)
	
	IF (sfMovieStruct.sfMov = NULL)
		sfMovieStruct.sfMov = REQUEST_SCALEFORM_MOVIE_INSTANCE("instructional_buttons")
	ENDIF	
	sfMovieStruct.iBools = 0
	
	sfMovieStruct.eInputs[0] = eInput1
	sfMovieStruct.eInputs[1] = eInput2
	sfMovieStruct.eInputs[2] = eInput3
	sfMovieStruct.eInputs[3] = eInput4
	sfMovieStruct.eInputs[4] = MAX_INPUTS
	sfMovieStruct.eInputs[5] = MAX_INPUTS
	sfMovieStruct.eInputs[6] = MAX_INPUTS
	sfMovieStruct.eInputs[7] = MAX_INPUTS

	sfMovieStruct.eButtons[0] = PICK_STRING(eInput1 != MAX_INPUTS, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eInput1), "")
	sfMovieStruct.eButtons[1] = PICK_STRING(eInput2 != MAX_INPUTS, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eInput2), "")
	sfMovieStruct.eButtons[2] = PICK_STRING(eInput3 != MAX_INPUTS, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eInput3), "")
	sfMovieStruct.eButtons[3] = PICK_STRING(eInput4 != MAX_INPUTS, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eInput4), "")
	sfMovieStruct.eButtons[4] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	sfMovieStruct.eButtons[5] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	sfMovieStruct.eButtons[6] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	sfMovieStruct.eButtons[7] = NULL//INT_TO_ENUM(TEXT_LABEL_63, -1)
	
	sfMovieStruct.sLabels[0] = sLabel1
	sfMovieStruct.sLabels[1] = sLabel2
	sfMovieStruct.sLabels[2] = sLabel3
	sfMovieStruct.sLabels[3] = sLabel4
	
	IF bVertical
		sfMovieStruct.eOrient = SF_UI_Vertical
	ELSE
		sfMovieStruct.eOrient = SF_UI_Horizontal
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sfMovieStruct.sfMov)
		SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_LOADED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a set of MINIGAME_INSTRUCTIONS as part of a UI, so that they're shifted and attached to that UI.
PROC SET_INSTRUCTIONS_ATTACHED_TO_MINIGAME_UI(SCRIPT_SCALEFORM_UI & sfMovieStruct, BOOL bIsAttached = TRUE)
	IF bIsAttached
		SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_IsForMenu)
		sfMovieStruct.eOrient = SF_UI_Horizontal
	ELSE
		CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_IsForMenu)
	ENDIF
ENDPROC

/// PURPOSE:
///    Pushes a minigame instruction into a movie.
/// PARAMS:
///    instrToAdd - the butotn press to give the instruction
///    labelToAdd - the label to give the instruction
///    bInFirstEmpty - If this is TRUE, it ignores iAtIndex, and inserts it into the first empty slot (where the instruction is still null)
///    iAtSlot - If this is > 0, and bInFirstEmpty = FALSE, it overwrites an instruction at the given slot. If that slot is empty, it will fill it in.
PROC INSERT_MINIGAME_INSTRUCTION(SCRIPT_SCALEFORM_UI & sfMovieStruct, STRING instrToAdd, STRING labelToAdd, BOOL bInFirstEmpty = TRUE, INT iAtSlot = -1)
	IF NOT IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(sfMovieStruct)
		DEBUG_MESSAGE("INSERT_MINIGAME_INSTRUCTION failing.")
		EXIT
	ENDIF
	
	CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	
	INT index
	IF bInFirstEmpty
		REPEAT MAX_INST_BUTTONS index
			IF IS_STRING_NULL_OR_EMPTY(sfMovieStruct.sLabels[index])
				sfMovieStruct.eInputs[index] = MAX_INPUTS
				sfMovieStruct.eButtons[index] = instrToAdd
				sfMovieStruct.sLabels[index] = labelToAdd
			ENDIF
		ENDREPEAT
	ELSE
		// Check to see what iAtIndex is, that's where the instruction is going.
		IF (iAtSlot > 0)
			sfMovieStruct.eInputs[iAtSlot] = MAX_INPUTS
			sfMovieStruct.eButtons[iAtSlot] = instrToAdd
			sfMovieStruct.sLabels[iAtSlot] = labelToAdd
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Pushes a minigame instruction into a movie.
/// PARAMS:
///    instrToAdd - the butotn press to give the instruction
///    labelToAdd - the label to give the instruction
///    bInFirstEmpty - If this is TRUE, it ignores iAtIndex, and inserts it into the first empty slot (where the instruction is still null)
///    iAtSlot - If this is > 0, and bInFirstEmpty = FALSE, it overwrites an instruction at the given slot. If that slot is empty, it will fill it in.
PROC INSERT_MINIGAME_INSTRUCTION_INPUT(SCRIPT_SCALEFORM_UI & sfMovieStruct, CONTROL_ACTION inputToAdd, STRING labelToAdd, BOOL bInFirstEmpty = TRUE, INT iAtSlot = -1)
	IF NOT IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(sfMovieStruct)
		DEBUG_MESSAGE("INSERT_MINIGAME_INSTRUCTION failing.")
		EXIT
	ENDIF
	
	CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	
	INT index
	IF bInFirstEmpty
		REPEAT MAX_INST_BUTTONS index
			IF IS_STRING_NULL_OR_EMPTY(sfMovieStruct.sLabels[index])
				sfMovieStruct.eInputs[index] = inputToAdd
				sfMovieStruct.eButtons[index] = PICK_STRING(inputToAdd != MAX_INPUTS, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, inputToAdd), "")
				sfMovieStruct.sLabels[index] = labelToAdd
			ENDIF
		ENDREPEAT
	ELSE
		// Check to see what iAtIndex is, that's where the instruction is going.
		IF (iAtSlot > 0)
			sfMovieStruct.eInputs[iAtSlot] = inputToAdd
			sfMovieStruct.eButtons[iAtSlot] = PICK_STRING(inputToAdd != MAX_INPUTS, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, inputToAdd), "")
			sfMovieStruct.sLabels[iAtSlot] = labelToAdd
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles a single use context using the scaleform UI
///    USES ONLY TRIGGERBUTTON and TRIGGERLABEL
/// PARAMS:
///    sfMovieStruct - The VALID movie.
/// RETURNS:
///    TRUE if it was pressed. FALSE if not.
PROC UPDATE_SIMPLE_INSTRUCTION(SCRIPT_SCALEFORM_UI & sfMovieStruct)
	IF NOT IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(sfMovieStruct)
		DEBUG_MESSAGE("UPDATE_SIMPLE_INSTRUCTION failing.")
		EXIT
	ENDIF
	// Always draw the prompt and monitor input.
	IF IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_IsForMenu)
		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.39,0.467,1.0,1.0,255,255,255,0)
	ELSE
		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.5,0.5,1.0,1.0,255,255,255,0)
	ENDIF
	
	// If controls have changed, re-calculate button strings
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		INT index
		REPEAT COUNT_OF(sfMovieStruct.eInputs) index
			IF sfMovieStruct.eInputs[index] != MAX_INPUTS
				sfMovieStruct.eButtons[index] = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, sfMovieStruct.eInputs[index])
			ENDIF
		ENDREPEAT
		
		CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	ENDIF

	// If we're loaded, this is a simple instruction. OOnly has a single button down. Track that.
	IF NOT IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_CLEAR_SPACE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[0])
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[0])
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eOrient)))
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_BACKGROUND_COLOUR")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
		END_SCALEFORM_MOVIE_METHOD()
		
		SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles a single use context using the scaleform UI
///    USES ONLY TRIGGERBUTTON and TRIGGERLABEL
/// PARAMS:
///    sfMovieStruct - The VALID movie.
/// RETURNS:
///    TRUE if it was pressed. FALSE if not.
PROC UPDATE_MINIGAME_INSTRUCTIONS_NOW(SCRIPT_SCALEFORM_UI & sfMovieStruct)
	IF NOT IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(sfMovieStruct)
		DEBUG_MESSAGE("UPDATE_SIMPLE_INSTRUCTION failing.")
		EXIT
	ENDIF
	
	INT index = 0
	
	// Always draw the prompt and monitor input.
	IF IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_IsForMenu)
		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.39,0.467,1.0,1.0,255,255,255,0)
	ELSE
		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.5,0.5,1.0,1.0,255,255,255,0)
	ENDIF
	
	// If controls have changed, re-calculate button strings
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		REPEAT COUNT_OF(sfMovieStruct.eInputs) index
			IF sfMovieStruct.eInputs[index] != MAX_INPUTS
				sfMovieStruct.eButtons[index] = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, sfMovieStruct.eInputs[index])
			ENDIF
		ENDREPEAT
		
		CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	ENDIF

	// If we're loaded, this is a simple instruction. OOnly has a single button down. Track that.
	IF NOT IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_CLEAR_SPACE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		FLOAT fSlot = 0
		REPEAT MAX_INST_BUTTONS index
			IF NOT IS_STRING_NULL_OR_EMPTY(sfMovieStruct.sLabels[index])
				// May need to draw multi labels if we've got a multi button in this slot.
				IF (NOT IS_STRING_NULL_OR_EMPTY(sfMovieStruct.eButtons[index + MAX_INST_BUTTONS]))
					BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[index + MAX_INST_BUTTONS])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[index])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[index])
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[index])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[index])
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				// Advance a slot.
				fSlot += 1
			ENDIF
		ENDREPEAT

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eOrient)))
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_BACKGROUND_COLOUR")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
		END_SCALEFORM_MOVIE_METHOD()
		
		SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Always draws the subset of up to 8 instructions, doesn't require a button to be held down to display the menu.
///    Basically the same as DRAW_MINIGAME_INSTRUCTIONS, but always creates the menu instead of waiting for a button.
PROC DRAW_MINIGAME_INSTRUCTIONS_NOW(SCRIPT_SCALEFORM_UI & sfMovieStruct)
	IF NOT IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(sfMovieStruct)
		DEBUG_MESSAGE("DRAW_MINIGAME_INSTRUCTIONS_NOW failing.")
		EXIT
	ENDIF
		
	// Always draw.
//	IF IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_IsForMenu)
//		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.39,0.467,1.0,1.0,255,255,255,0)
//	ELSE
//		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.5,0.5,1.0,1.0,255,255,255,0)
//	ENDIF
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfMovieStruct.sfMov,255,255,255,0)
	
	// If controls have changed, re-calculate button strings
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		INT index
		REPEAT COUNT_OF(sfMovieStruct.eInputs) index
			IF sfMovieStruct.eInputs[index] != MAX_INPUTS
				sfMovieStruct.eButtons[index] = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, sfMovieStruct.eInputs[index])
			ENDIF
		ENDREPEAT
		
		CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	ENDIF
	
	// Only need to call this block once.
	IF NOT IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_CLEAR_SPACE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		
		FLOAT fSlot = 0
		INT index
		REPEAT MAX_INST_BUTTONS index
			// If there's a 2nd instruction in that slot, print that. Otherwise, print nothing.
			IF NOT IS_STRING_NULL_OR_EMPTY(sfMovieStruct.sLabels[index])
				IF NOT IS_STRING_NULL_OR_EMPTY(sfMovieStruct.eButtons[index + MAX_INST_BUTTONS])
					BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[index + MAX_INST_BUTTONS])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[index])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[index])
					END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(sfMovieStruct.eButtons[index])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[index])
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				fSlot += 1
			ENDIF
		ENDREPEAT

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eOrient)))
		END_SCALEFORM_MOVIE_METHOD()

		BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_BACKGROUND_COLOUR")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
		END_SCALEFORM_MOVIE_METHOD()
		
		SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws up to 8 minigame instructions on screen. Hides until a button is pressed, then pops up all the instructions while it's pressed.
/// PARAMS:
///    sfMovieStruct - the movie struct
//PROC DRAW_MINIGAME_INSTRUCTIONS(SCRIPT_SCALEFORM_UI & sfMovieStruct)
//	IF NOT IS_SCRIPT_SCALEFORM_UI_VALID_AND_LOADED(sfMovieStruct)
//		DEBUG_MESSAGE("DRAW_MINIGAME_INSTRUCTIONS failing.")
//		EXIT
//	ENDIF
//		
//	// Always draw.
//	IF IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_IsForMenu)
//		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.39,0.467,1.0,1.0,255,255,255,0)
//	ELSE
//		DRAW_SCALEFORM_MOVIE(sfMovieStruct.sfMov, 0.5,0.5,1.0,1.0,255,255,255,0)
//	ENDIF
//	
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, padButton)
//		// If the button is pressed, we need to call the new setup, but only once!
//		IF NOT IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_CREATED_ButtonDown)
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_CLEAR_SPACE")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
//			END_SCALEFORM_MOVIE_METHOD()
//
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT_EMPTY")
//			END_SCALEFORM_MOVIE_METHOD()
//		
//			FLOAT fSlot = 0
//			INT index
//			FOR index = 1 TO MAX_INST_BUTTONS - 1
//				IF NOT IS_STRING_NULL_OR_EMPTY(sfMovieStruct.sLabels[index])
//					IF (ENUM_TO_INT(sfMovieStruct.eButtons[index + MAX_INST_BUTTONS]) > -1)						
//						BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eButtons[index + MAX_INST_BUTTONS])))
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eButtons[index])))
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[index])
//						END_SCALEFORM_MOVIE_METHOD()
//					
//					ELSE
//						BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eButtons[index])))
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[index])
//						END_SCALEFORM_MOVIE_METHOD()
//					ENDIF
//					
//					fSlot += 1
//				ENDIF
//			ENDFOR
//
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eOrient)))
//			END_SCALEFORM_MOVIE_METHOD()
//
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_BACKGROUND_COLOUR")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
//			END_SCALEFORM_MOVIE_METHOD()
//		
//			SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonDown)
//			CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
//		ENDIF
//	ELSE
//		// If the button is not set, then we need to call the enw setup, but only once!
//		IF NOT IS_BITMASK_AS_ENUM_SET(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_CLEAR_SPACE")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(200.0)
//			END_SCALEFORM_MOVIE_METHOD()
//
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT_EMPTY")
//			END_SCALEFORM_MOVIE_METHOD()
//			
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_DATA_SLOT")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eButtons[0])))
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sfMovieStruct.sLabels[0])
//			END_SCALEFORM_MOVIE_METHOD()
//						
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "DRAW_INSTRUCTIONAL_BUTTONS")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(ENUM_TO_INT(sfMovieStruct.eOrient)))
//			END_SCALEFORM_MOVIE_METHOD()
//
//			BEGIN_SCALEFORM_MOVIE_METHOD(sfMovieStruct.sfMov, "SET_BACKGROUND_COLOUR")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(80)
//			END_SCALEFORM_MOVIE_METHOD()
//		
//			SET_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonUp)
//			CLEAR_BITMASK_AS_ENUM(sfMovieStruct.iBools, SF_UI_CREATED_ButtonDown)
//		ENDIF
//	ENDIF
//	
//ENDPROC

/// PURPOSE:
///    Cleans the script scaleform UI struct. Should be called before re-use.
PROC CLEANUP_MINIGAME_INSTRUCTIONS(SCRIPT_SCALEFORM_UI & sfMovieStruct)
	IF (sfMovieStruct.sfMov <> NULL)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMovieStruct.sfMov)
		sfMovieStruct.sfMov = NULL
	ENDIF
			
	sfMovieStruct.iBools = 0
	sfMovieStruct.eOrient = SF_UI_Horizontal
	sfMovieStruct.eButtons[0] = NULL
	sfMovieStruct.eButtons[1] = NULL
	sfMovieStruct.eButtons[2] = NULL
	sfMovieStruct.eButtons[3] = NULL
	sfMovieStruct.eButtons[4] = NULL
	sfMovieStruct.eButtons[5] = NULL
	sfMovieStruct.eButtons[6] = NULL
	sfMovieStruct.eButtons[7] = NULL

	sfMovieStruct.eInputs[0] = MAX_INPUTS
	sfMovieStruct.eInputs[1] = MAX_INPUTS
	sfMovieStruct.eInputs[2] = MAX_INPUTS
	sfMovieStruct.eInputs[3] = MAX_INPUTS
	sfMovieStruct.eInputs[4] = MAX_INPUTS
	sfMovieStruct.eInputs[5] = MAX_INPUTS
	sfMovieStruct.eInputs[6] = MAX_INPUTS
	sfMovieStruct.eInputs[7] = MAX_INPUTS

	sfMovieStruct.sLabels[0] = NULL
	sfMovieStruct.sLabels[1] = NULL
	sfMovieStruct.sLabels[2] = NULL
	sfMovieStruct.sLabels[3] = NULL
ENDPROC






///////////////////////
///     INTRO UI	///
///////////////////////
PROC INTROUI_SET_DATA_SLOT_2STRINGS(SCALEFORM_INDEX introUI, INT iSlot1, INT iSlot2, STRING Label, STRING Value)
	IF IS_STRING_NULL(Label)
		Label = ""
	ENDIF
	IF IS_STRING_NULL(Value)
		Value = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(Value)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(Label)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_DATA_SLOT(SCALEFORM_INDEX introUI, INT iSlot1, INT iSlot2, INT iSlot3, FLOAT fSlot4, STRING sText)
	IF IS_STRING_NULL(sText)
		sText = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sText)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    This is for UI slots that need an extra data slot (currently, TIME.)
PROC INTROUI_SET_DATA_SLOT_5(SCALEFORM_INDEX introUI, INT iSlot1, INT iSlot2, INT iSlot3, FLOAT fSlot4, FLOAT fSlot5, STRING sText)
	IF IS_STRING_NULL(sText)
		sText = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSlot5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sText)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_MISSION_TITLE(SCALEFORM_INDEX introUI, STRING sTitle = NULL, STRING sText = NULL)
	IF IS_STRING_NULL(sTitle)
		sTitle = ""
	ENDIF
	IF IS_STRING_NULL(sText)
		sText = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_MISSION_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sText)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_MISSION_TITLE_WITH_SUBSTRING(SCALEFORM_INDEX introUI, STRING sTitle = NULL, STRING sText = NULL, STRING sTextInsert = NULL)
	IF IS_STRING_NULL(sTitle)
		sTitle = ""
	ENDIF
	IF IS_STRING_NULL(sText)
		sText = ""
	ENDIF
	IF IS_STRING_NULL(sTextInsert)
		sTextInsert = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_MISSION_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sText)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sTextInsert)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_MISSION_BG_COLOUR(SCALEFORM_INDEX introUI, INT iRed = 214, INT iGreen = 189, INT iBlue = 97)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_MISSION_BG_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_MISSION_SUBTITLE_COLOUR(SCALEFORM_INDEX introUI, INT iRed = 0, INT iGreen = 0, INT iBlue = 0)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_MISSION_SUBTITLE_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_MEDAL(SCALEFORM_INDEX introUI, INT iMedal = 0)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_MEDAL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMedal)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_SET_TOTAL_STRING(SCALEFORM_INDEX introUI, STRING sTotalString)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_TOTAL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-99)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-99)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTotalString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_DRAW_MENU_LIST(SCALEFORM_INDEX introUI)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "DRAW_MENU_LIST")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC INTROUI_HIGHLIGHT_ITEM(SCALEFORM_INDEX introUI, INT iHighlight = 0, BOOL bHighlight = FALSE)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "HIGHLIGHT_ITEM")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlight)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bHighlight)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC SCALEFORM_INDEX REQUEST_INTRO_UI()
	RETURN REQUEST_SCALEFORM_MOVIE("mission_complete")
ENDFUNC

PROC SETUP_INTRO_UI_BASIC(SCALEFORM_INDEX introUI, STRING sTitle, STRING sDetails)
	INTROUI_SET_MISSION_TITLE(introUI, sTitle, sDetails)
	                                                
	INTROUI_SET_MISSION_BG_COLOUR(introUI, 214, 189, 97)
	INTROUI_SET_MISSION_SUBTITLE_COLOUR(introUI, 0, 0, 0)
	INTROUI_SET_MEDAL(introUI, 0)
	INTROUI_DRAW_MENU_LIST(introUI)
	INTROUI_HIGHLIGHT_ITEM(introUI, 0)		                                               
ENDPROC

PROC SETUP_INTRO_UI_WITH_SUBSTRING(SCALEFORM_INDEX introUI, STRING sTitle, STRING sDetails, STRING sTextInsert)
	INTROUI_SET_MISSION_TITLE_WITH_SUBSTRING(introUI, sTitle, sDetails, sTextInsert)
	                                                
	INTROUI_SET_MISSION_BG_COLOUR(introUI, 214, 189, 97)
	INTROUI_SET_MISSION_SUBTITLE_COLOUR(introUI, 0, 0, 0)
	INTROUI_SET_MEDAL(introUI, 0)
	INTROUI_DRAW_MENU_LIST(introUI)
	INTROUI_HIGHLIGHT_ITEM(introUI, 0)		                                               
ENDPROC

PROC ADD_INTRO_UI_DATA_ITEM(SCALEFORM_INDEX introUI, STRING sItem, INT iSlot, INT iType, INT iCheckboxStatus, INT iData)
	INTROUI_SET_DATA_SLOT(introUI, iSlot, iCheckboxStatus, iType, TO_FLOAT(iData), sItem)
ENDPROC

PROC ADD_INTRO_UI_LIST_ITEM(SCALEFORM_INDEX introUI, STRING sItem, INT iSlot)
	INTROUI_SET_DATA_SLOT(introUI, iSlot, 0, 7, 0.0, sItem)
ENDPROC

PROC CLEAR_INTRO_UI_SLOTS(SCALEFORM_INDEX introUI)
	BEGIN_SCALEFORM_MOVIE_METHOD(introUI, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SELECT_INTRO_UI_LIST_ITEM(SCALEFORM_INDEX introUI, INT iSlot, BOOL bHighlight)
	INTROUI_HIGHLIGHT_ITEM(introUI, iSlot, bHighlight)		                                               
ENDPROC

PROC DISPLAY_INTRO_UI(SCALEFORM_INDEX introUI)
	DRAW_SCALEFORM_MOVIE(introUI, 0.1632, 0.307, 0.225, 0.5111, 255, 255, 255, 0)
ENDPROC

PROC CLEANUP_INTRO_UI(SCALEFORM_INDEX introUI)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(introUI)
ENDPROC

///////////////////////
///     QUIT UI		///
///////////////////////
PROC QUITUI_SET_TEXT(SCALEFORM_INDEX quitUI, STRING sTitle = NULL, STRING sText = NULL)
	IF IS_STRING_NULL(sTitle)
		sTitle = ""
	ENDIF
	IF IS_STRING_NULL(sText)
		sText = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(quitUI, "SET_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sText)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC
/// PURPOSE:
///    Set the quit text with a title, all text, and substring with a number.
/// PARAMS:
///    quitUI - 
///    sTitle - 
///    sText - 
///    iNum - 
PROC QUITUI_SET_TEXT_WITH_NUMBER_SUBSTRING(SCALEFORM_INDEX quitUI, STRING sTitle = NULL, STRING sText = NULL, INT iNum = -1)
	IF IS_STRING_NULL(sTitle)
		sTitle = ""
	ENDIF
	IF IS_STRING_NULL(sText)
		sText = ""
	ENDIF
	BEGIN_SCALEFORM_MOVIE_METHOD(quitUI, "SET_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sText)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC QUITUI_TRANSITION_IN(SCALEFORM_INDEX quitUI, INT iMSIn = 0)
	BEGIN_SCALEFORM_MOVIE_METHOD(quitUI, "TRANSITION_IN")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMSIn)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC QUITUI_TRANSITION_OUT(SCALEFORM_INDEX quitUI, INT iMSOut = 0)
	BEGIN_SCALEFORM_MOVIE_METHOD(quitUI, "TRANSITION_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMSOut)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


FUNC SCALEFORM_INDEX REQUEST_QUIT_UI()
	RETURN REQUEST_SCALEFORM_MOVIE("mission_quit")
ENDFUNC

PROC SETUP_QUIT_UI(SCALEFORM_INDEX quitUI, STRING sTitle, STRING sDetails)
	                                                
	QUITUI_SET_TEXT(quitUI, sTitle, sDetails)
	QUITUI_TRANSITION_IN(quitUI, 0)                                                
ENDPROC

/// PURPOSE:
///    Sets text with a number and transitions in
/// PARAMS:
///    quitUI - 
///    sTitle - 
///    sDetails - 
///    iNum - 
PROC SETUP_QUIT_UI_WITH_NUMBER_SUBSTRING(SCALEFORM_INDEX quitUI, STRING sTitle, STRING sDetails, INT iNum)
	QUITUI_SET_TEXT_WITH_NUMBER_SUBSTRING(quitUI, sTitle, sDetails, iNum)
	QUITUI_TRANSITION_IN(quitUI, 0)                                                
ENDPROC

/// PURPOSE:
///    Nearly the same as SETUP function but doens't call QUITUI_TRANSITION_IN
/// PARAMS:
///    quitUI - 
///    sTitle - 
///    sDetails - 
///    iNum - 
PROC UPDATE_QUIT_UI_WITH_NUMBER_SUBSTRING(SCALEFORM_INDEX quitUI, STRING sTitle, STRING sDetails, INT iNum)
	QUITUI_SET_TEXT_WITH_NUMBER_SUBSTRING(quitUI, sTitle, sDetails, iNum)
ENDPROC

PROC DISPLAY_QUIT_UI(SCALEFORM_INDEX quitUI)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE(quitUI, 0.5,0.5,1.0 ,1.0 ,255,255,255,0)
ENDPROC

PROC CLEANUP_QUIT_UI(SCALEFORM_INDEX quitUI)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(quitUI)
ENDPROC


CONST_INT DEFAULT_SPLASH_DURATION	4000
CONST_INT DEFAULT_SPLASH_R			253
CONST_INT DEFAULT_SPLASH_G			214
CONST_INT DEFAULT_SPLASH_B			83
CONST_INT DEFAULT_SPLASH_A			255
CONST_INT SPLASH_RED_R				235
CONST_INT SPLASH_RED_G				36
CONST_INT SPLASH_RED_B				39

STRUCT SCRIPT_SCALEFORM_SPLASH
	SCALEFORM_INDEX siMovie
	INT 			iDuration
	structTimer		movieTimer
ENDSTRUCT

/// PURPOSE:
///    Asks for the scaleform splash text ui.
FUNC SCALEFORM_INDEX REQUEST_SCALEFORM_SPLASH_UI()
	RETURN REQUEST_SCALEFORM_MOVIE("SPLASH_TEXT")
ENDFUNC

/// PURPOSE:
///    Set splash text for a given duration
/// PARAMS:
///    scaleformIndex - 
///    label - splash text string
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_SPLASH_TEXT(SCRIPT_SCALEFORM_SPLASH & scaleformStruct, STRING labeToDisp, INT iDuration = DEFAULT_SPLASH_DURATION,
		INT iColor_R = DEFAULT_SPLASH_R, INT iColor_G = DEFAULT_SPLASH_G, INT iColor_B = DEFAULT_SPLASH_B, INT iColor_A = DEFAULT_SPLASH_A)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SET_SPLASH_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_R)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_G)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_B)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_A)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set splash text for a given duration
/// PARAMS:
///    scaleformIndex - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_SPLASH_TEXT_WITH_NUMBER(SCRIPT_SCALEFORM_SPLASH & scaleformStruct, STRING labeToDisp, INT iNum, INT iDuration = DEFAULT_SPLASH_DURATION,
		INT iColor_R = DEFAULT_SPLASH_R, INT iColor_G = DEFAULT_SPLASH_G, INT iColor_B = DEFAULT_SPLASH_B, INT iColor_A = DEFAULT_SPLASH_A)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SET_SPLASH_TEXT")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(labeToDisp)
			ADD_TEXT_COMPONENT_INTEGER(iNum)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_R)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_G)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_B)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_A)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set splash text for a given duration
/// PARAMS:
///    scaleformIndex - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_SPLASH_TEXT_WITH_STRING(SCRIPT_SCALEFORM_SPLASH & scaleformStruct, STRING labeToDisp, STRING subString, INT iDuration = DEFAULT_SPLASH_DURATION,
		INT iColor_R = DEFAULT_SPLASH_R, INT iColor_G = DEFAULT_SPLASH_G, INT iColor_B = DEFAULT_SPLASH_B, INT iColor_A = DEFAULT_SPLASH_A)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SET_SPLASH_TEXT")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(labeToDisp)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(subString)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_R)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_G)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_B)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_A)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Set splash text for a given duration using the player's username/gamertag
///    Used mainly in MP
/// PARAMS:
///    scaleformIndex - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
PROC SET_SCALEFORM_SPLASH_TEXT_WITH_PLAYER_NAME(SCRIPT_SCALEFORM_SPLASH & scaleformStruct, STRING labeToDisp, STRING sPlayerName, INT iDuration = DEFAULT_SPLASH_DURATION,
		INT iColor_R = DEFAULT_SPLASH_R, INT iColor_G = DEFAULT_SPLASH_G, INT iColor_B = DEFAULT_SPLASH_B, INT iColor_A = DEFAULT_SPLASH_A)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SET_SPLASH_TEXT")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(labeToDisp)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_R)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_G)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_B)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_A)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
ENDPROC

/// PURPOSE:
///    Renders a scaleform splash movie.
/// RETURNS:
///    FALSE when the movie is done drawing and you should move on.
FUNC BOOL UPDATE_SCALEFORM_SPLASH(SCRIPT_SCALEFORM_SPLASH & scaleformStruct, BOOL bAllowSkip = FALSE)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	ENDIF
	
	// Hide the reticle.
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	
	// Draw the moobie.
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE(scaleformStruct.siMovie, 0.5, 0.5, 1.0 ,1.0, 255,255,255,0)
	
	// If we can skip, detect that button press
	IF bAllowSkip
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// When it's no longer visible, return FALSE. -- Changed. Now, when its curation is up, return FALSE
	IF (scaleformStruct.iDuration = -1)
		RETURN TRUE
	ENDIF

	// Is it time to leave?
	IF (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 > TO_FLOAT(scaleformStruct.iDuration))
		CANCEL_TIMER(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if this scaleform splash would be displaying. Mainly for Golf, as golf was using an odd method to check this.
FUNC BOOL WOULD_SCALEFORM_SPLASH_BE_VISIBLE(SCRIPT_SCALEFORM_SPLASH & scaleformStruct)
	IF NOT IS_TIMER_STARTED(scaleformStruct.movieTimer)
		RETURN FALSE
	ENDIF
	
	RETURN (GET_TIMER_IN_SECONDS(scaleformStruct.movieTimer) * 1000.0 <= TO_FLOAT(scaleformStruct.iDuration))
ENDFUNC

/// PURPOSE:
///    When attempting to manually control the entering and exiting of the scaleform splash text, use this function to set the text
//	   to display. DOES NOT TAKE A TIME AS YOU MUST USE SPLASH_TEXT_TRANSITION_IN & SPLASH_TEXT_TRANSITION_OUT.
PROC SET_SCALEFORM_SPLASH_TEXT_LABEL(SCRIPT_SCALEFORM_SPLASH & scaleformStruct, STRING labeToDisp, 
		INT iColor_R = DEFAULT_SPLASH_R, INT iColor_G = DEFAULT_SPLASH_G, INT iColor_B = DEFAULT_SPLASH_B, INT iColor_A = DEFAULT_SPLASH_A,
		BOOL bStartNow = FALSE)
	scaleformStruct.iDuration = -1
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SPLASH_TEXT_LABEL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_R)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_G)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_B)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColor_A)
	END_SCALEFORM_MOVIE_METHOD()
	
	IF bStartNow
		BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SPLASH_TEXT_TRANSITION_IN")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    When you want to manually control when your splash text comes in, use this.
///    YOU MUST CALL UPDATE_SCALEFORM_SPLASH & SPLASH_TEXT_TRANSITION_OUT, manually.
PROC SPLASH_TEXT_TRANSITION_IN(SCRIPT_SCALEFORM_SPLASH & scaleformStruct)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SPLASH_TEXT_TRANSITION_IN")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    When you want to manually control when your splash text goes out, use this.
///    YOU MUST CALL UPDATE_SCALEFORM_SPLASH & SPLASH_TEXT_TRANSITION_IN, manually.
PROC SPLASH_TEXT_TRANSITION_OUT(SCRIPT_SCALEFORM_SPLASH & scaleformStruct)
	scaleformStruct.iDuration = 300
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, "SPLASH_TEXT_TRANSITION_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(300)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Functional replacement of GET_POSITION_OF_ANALOGUE_STICKS usable for release as per Imran's email
/// PARAMS:
///    returnLeftX - 
///    returnLeftY - 
///    returnRightX - 
///    returnRightY - 
///    bIsControlEnabled - GET_CONTROL_NORMAL won't return values if player control has been disabled, so we'll need to know if player's control has been taken away.
///    You can use IS_PLAYER_CONTROL_ON(PLAYER_ID()) as the last parameter to perform a check every frame.
PROC GET_ANALOG_STICK_VALUES(INT &returnLeftX, INT &returnLeftY, INT &returnRightX, INT &returnRightY, BOOL bIsControlEnabled = TRUE, BOOL bUnboundValues = TRUE)
	FLOAT LeftX, LeftY, RightX, RightY
	IF bUnboundValues = FALSE
		IF bIsControlEnabled
			LeftX = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			LeftY = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			RightX = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			RightY = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		ELSE
			LeftX = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			LeftY = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			RightX = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			RightY = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		ENDIF
	ELSE
		IF bIsControlEnabled
			LeftX = GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			LeftY = GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			RightX = GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			RightY = GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		ELSE
			LeftX = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			LeftY = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			RightX = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			RightY = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		ENDIF
	ENDIF
	
	returnLeftX = ROUND(ANALOG_OFFSET * LeftX)
	returnLeftY = ROUND(ANALOG_OFFSET * LeftY)
	returnRightX = ROUND(ANALOG_OFFSET * RightX)
	returnRightY = ROUND(ANALOG_OFFSET * RightY)
ENDPROC 

PROC GET_ANALOG_STICK_VALUES_MOVE_INPUT(FLOAT &returnLeftX, FLOAT &returnLeftY, FLOAT &returnRightX, FLOAT &returnRightY, BOOL bIsControlEnabled = TRUE)
	FLOAT LeftX, LeftY, RightX, RightY
	IF bIsControlEnabled
		LeftX = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR)
		LeftY = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD)
		RightX = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR)
		RightY = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD)
	ELSE
		LeftX = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR)
		LeftY = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD)
		RightX = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR)
		RightY = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD)
	ENDIF
	
	returnLeftX = 	(LeftX + 1) / 2		//ROUND(ANALOG_OFFSET * LeftX)
	returnLeftY = 	(LeftY + 1) / 2		//ROUND(ANALOG_OFFSET * LeftY)
	returnRightX = 	(RightX + 1) / 2	//ROUND(ANALOG_OFFSET * RightX)
	returnRightY = 	(RightY + 1) / 2	//ROUND(ANALOG_OFFSET * RightY)
ENDPROC

