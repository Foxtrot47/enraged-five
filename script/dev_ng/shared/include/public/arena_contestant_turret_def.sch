//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        arena_contestant_turret_def.sch																//
// Description: Enums, structs, consts, and look-up tables for the arena contestant turrets.				//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		05/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "turret_manager_def.sch"
USING "turret_cam_public_def.sch"

/// PURPOSE: Min duration for blocking the tower after a call to ARENA_CONTESTANT_TURRET_SET_BLOCKED.
CONST_INT ci_ARENA_CT_PREVENT_UNBLOCK_DURATION_MS 2000

/// PURPOSE: How long to linger the "ACT_NEAR" help text after leaving the help radius.
CONST_INT ci_ARENA_CT_HELP_LINGER_DURATION_MS 2000

/// PURPOSE: [0,1] Generic bar "low" threshold (draw as red)
CONST_FLOAT cf_ARENA_CT_GENERIC_BAR_LOW_THRESHOLD 0.25

/// PURPOSE: Tint indices for consumer wasteland towers
CONST_INT ci_ARENA_CT_TINT_RED 		0
CONST_INT ci_ARENA_CT_TINT_GREEN 	1

/// PURPOSE: Radius^2 around tower where height indicator will be used on the blip.
CONST_FLOAT cf_ARENA_CT_SHOW_BLIP_HIGHT_RADIUS2 400.0

/// PURPOSE: How long after each use must the turret be blocked (ms)?
CONST_INT ci_ARENA_CT_COOLDOWN_DURATION_MS 60000

/// PURPOSE: Length of time player can use the turret
CONST_INT ci_ARENA_CT_USE_DURATION_MS 30000

/// PURPOSE: Subtracted from cooldown to show turret as free slightly before it is (so help/lights/corona don't flash).
CONST_INT ci_ARENA_CT_SHOW_TURRET_FREE_EARLY_MS	1500

/// PURPOSE: How close the player has to be to a tower before triggering "ACT_NEAR" help text.
CONST_FLOAT cf_ARENA_CT_NEAR_HELP_DIST 35.0

/// PURPOSE: Speed limit checked before triggering the turret.
CONST_FLOAT cf_ARENA_CT_SPEED_LIMIT 5.0

/// PURPOSE: Arena contestant turret active area dimentions.
CONST_FLOAT cf_ARENA_CT_AREA_RADIUS 6.25
CONST_FLOAT cf_ARENA_CT_AREA_HEIGHT 5.5
CONST_FLOAT cf_ARENA_CT_AREA_HEIGHT_OFFSET -1.5

/// PURPOSE: Arena contestant turret corona dimentions (visual).
TWEAK_FLOAT cf_ARENA_CT_CORONA_RADIUS 10.0
TWEAK_FLOAT cf_ARENA_CT_CORONA_HEIGHT 1.65
TWEAK_FLOAT cf_ARENA_CT_CORONA_OFFSET -1.0

/// PURPOSE: Weapon type combo spawn chance (relative to eachother).
TWEAK_FLOAT cf_ARENA_CT_WEIGHT_MG		1.5
TWEAK_FLOAT cf_ARENA_CT_WEIGHT_HM		1.3
TWEAK_FLOAT cf_ARENA_CT_WEIGHT_PM		1.2
TWEAK_FLOAT cf_ARENA_CT_WEIGHT_MG_HM_PM	1.0

/// PURPOSE: Per frame settings ARENA_CONTESTANT_TURRET_CONTEXT.iBsControlSettings
CONST_INT ci_ARENA_CT_BS_CONTROL_BLOCK			0
CONST_INT ci_ARENA_CT_BS_CONTROL_MINES_BLOCKED	1

STRUCT ARENA_CONTESTANT_TURRET
	VECTOR vCoords	
	FLOAT fHeading
ENDSTRUCT

// Max number of arena drive-in contestant turrets.
CONST_INT ci_ARENA_CONTESTANT_TURRETS_MAX 8

STRUCT ARENA_CONTESTANT_TURRET_STACK
	INT iCount
	ENTITY_INDEX entTurrets[ci_ARENA_CONTESTANT_TURRETS_MAX]
	FLOAT fHeadings[ci_ARENA_CONTESTANT_TURRETS_MAX]
ENDSTRUCT

STRUCT ARENA_CONTESTANT_TURRET_CONTEXT
	TURRET_LOCK_REQUEST_ID eRequestId 	// Required to check on the status of the turret lock request.
	TURRET_CAM_CONFIG_ID eCamConfigId 	// Required to verify the config we last created is the one we want to use for these turrets.
	INT iLocateStagger 					// Current turret index we're checking to see if player is using.
	INT iBsTurretIsFree					// Used for drawing coronas under free turrets. Index this bs with turret index.
	INT iBsTurretInUse					// Used for drawing coronas under free turrets. Index this bs with turret index.
	INT iCoronaStagger					// Curret turret index we're checking to update iBsTurretIsFree.
	INT iBsAvailableWeaponTypes			// Available weapon modes randomly assigned on entry. Bitset indexed with ci_ARENA_CT_TYPE_.
	INT iCurrentWeaponType				// Value directly set to one of the types ci_ARENA_CT_TYPE_.
	SCRIPT_TIMER useTimer				// Record length of use of turret
	BOOL bFiredPilotedMissile			// Has player fired drone missile?
	INT intention
	INT iBsPlayedTowerFreeSound			// Have played the "free" horn? Index with turretId.
	INT iBsControlSettings				// Use ci_ARENA_CT_BS_CONTROL_BLOCK...
	INT iActivated						// Used for the Arena Annoucer to know how many turrets became active during a staggerd loop.
	BLIP_INDEX blips[ci_ARENA_CONTESTANT_TURRETS_MAX] // Turret blips.
	SCRIPT_TIMER nearHelpTimer			// Displaying "acw_near" text after leaving the radius
	SCRIPT_TIMER unblockTimer			// B* url:bugstar:5514867
ENDSTRUCT	

STRUCT ARENA_CONTESTANT_SERVER_DATA
	TIME_DATATYPE tCooldownTimes[ci_ARENA_CONTESTANT_TURRETS_MAX] // Time at which the turret will be considered unblocked : free for use. Note: 0 = blocked.
ENDSTRUCT
