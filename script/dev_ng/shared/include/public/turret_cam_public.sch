//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_cam_public.sch																		//
// Description: Public methods for manipulating a turret_cam_script instance.								//
//				See TURRET_CAM_CONFIG_START and TURRET_CAM_CONFIG_UPDATE_START								//
//				below for info on how to configure a turret_cam.											//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		12/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "turret_cam_public_def.sch"
USING "script_maths.sch"

/// PURPOSE:
///    Returns TRUE if eConfigId matches the currently stored config.
///    NOTE: 	This does not guarentee that the config settings are identical because
///    			the settings could have been updated via TURRET_CAM_CONFIG_UPDATE_.
FUNC BOOL TURRET_CAM_IS_CONFIG_SAVED(TURRET_CAM_CONFIG_ID eConfigId)
	RETURN (eConfigId <> TCCI_NONE) AND (eConfigId = g_eTurretCamConfigId)
ENDFUNC

/// PURPOSE:
///    Kills any running turret_cam_script.
///    This does NOT unlock the turret in the turret management system (i.e. the player still "owns" the index).
PROC KILL_TURRET_CAM_SCRIPT()
	CDEBUG1LN(DEBUG_NET_TURRET, "KILL_TURRET_CAM_SCRIPT")
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(ci_TURRET_SCRIPT_HASH) > 0
		CDEBUG1LN(DEBUG_NET_TURRET, "   Killing current turret script. Uid = ", g_iTurretScriptLaunchUid)
		g_bKillTurretScript = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if a turret_cam_script has been started.
FUNC BOOL TURRET_CAM_IS_RUNNING()
	RETURN IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_STARTED_SCRIPT)		
ENDFUNC

/// PURPOSE:
///    Returns TRUE if a turret_cam_script has been started and
///    the cam has fully loaded (it is now displaying).
FUNC BOOL TURRET_CAM_IS_READY()
	RETURN IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_LOADED_INTO_CAM)		
ENDFUNC

/// PURPOSE:
///    A turret cam config must start with this proc
///    and end with TURRET_CAM_CONFIG_STOP().
///    
///    All config options start with prefix 
///    TURRET_CAM_CONFIG_
///    
///    You do not need to call every config proc - 
///    defaults will be assigned to anything you do not 
///    configure.
///    
///    e.g.
///    TURRET_CAM_CONFIG_START()
///    		TURRET_CAM_CONFIG_CAM_LIMITS(-70, 70, -30, 30, 40, 65)
///    		TURRET_CAM_CONFIG_WEAPON_FULL_AUTO()
///    TURRET_CAM_CONFIG_STOP(eMyConfigId)
PROC TURRET_CAM_CONFIG_START()
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_START")
	g_iBsTurretCamConfigSettings = 0
	g_turretCamConfig.instButtons.iCount = 0
	g_turretCamConfig.staticFirePoints.iCount = 0
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    fMinHeading - Relative min heading (z axis | yaw | heading). If > fMaxHeading the cam heading will loop instead of clamp.
///    fMaxHeading - Relative max heading (z axis | yaw | heading). If < fMaxHeading the cam heading will loop instead of clamp.
///    fMinPitch - Relative min pitch (x axis | pitch | tilt).
///    fMaxPitch - Relative max pitch (x axis | pitch | tilt).
///    fMinFov - Fov is used for zoom. Smaller Fov = more "zoomed in".
///    fMaxFov - Fov is used for zoom. Larger Fov = less "zoomed in".
PROC TURRET_CAM_CONFIG_CAM_LIMITS(FLOAT fMinHeading = -90.0, FLOAT fMaxHeading = 90.0, FLOAT fMinPitch = -45.0, FLOAT fMaxPitch = 45.0, FLOAT fMinFov = 30.0, FLOAT fMaxFov = 60.0)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_CAM_LIMITS")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_LIMITS)
	g_turretCamConfig.camLimits.fMinHeading = fMinHeading
	g_turretCamConfig.camLimits.fMaxHeading = fMaxHeading
	g_turretCamConfig.camLimits.fMinPitch = fMinPitch
	g_turretCamConfig.camLimits.fMaxPitch = fMaxPitch
	g_turretCamConfig.camLimits.fMinFov = fMinFov
	g_turretCamConfig.camLimits.fMaxFov = fMaxFov
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    fBaseRotSpeed - degrees_per_sec = fBaseRotSpeed * fov * zoom_multiplier | where zoom_multiplier is 1->2 derived from current zoom.
///    fBaseZoomSpeed - fov angle change / second.
PROC TURRET_CAM_CONFIG_CAM_SPEEDS(FLOAT fBaseRotSpeed = 0.6, FLOAT fBaseZoomSpeed = 20.0)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_CAM_SPEEDS")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_SPEEDS)
	g_turretCamConfig.camSpeeds.fBaseRotSpeed = fBaseRotSpeed
	g_turretCamConfig.camSpeeds.fBaseZoomSpeed = fBaseZoomSpeed
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eHudType - Hud implementations are very specific so you must choose from these presets.
///    			  New Hud types can be added in turret_cam_script.
PROC TURRET_CAM_CONFIG_HUD(TURRET_CAM_HUD_TYPE eHudType = TCHT_TURRET_CAM)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_HUD") 
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_HUD)
	g_turretCamConfig.eHudType = eHudType
ENDPROC

PROC TURRET_CAM_CONFIG_ARENA_HUD(BOOL bZoomVisible = TRUE, ARENA_HUD_WEAPON_ICON eIconMg = AHWI_HIDDEN,ARENA_HUD_WEAPON_ICON eIconHm = AHWI_HIDDEN, ARENA_HUD_WEAPON_ICON eIconPm = AHWI_HIDDEN)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_ARENA_HUD") 
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_ARENA_HUD)
	g_turretCamConfig.arenaHud.bZoomVisible = bZoomVisible
	g_turretCamConfig.arenaHud.eIconMg = eIconMg
	g_turretCamConfig.arenaHud.eIconHm = eIconHm
	g_turretCamConfig.arenaHud.eIconPm = eIconpm
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    bDoFadeOut - Always fade out while loading.
///    bDoFadeIn - Always fade in when load complete.
///    bDoLoadScene - Do a load scene at the turret cam position.
///    iLoadSceneWaitTimeMs - Give the load scene extra time to complete.
PROC TURRET_CAM_CONFIG_SET_LOAD_ARGS(BOOL bDoFadeOut = TRUE, BOOL bDoFadeIn = TRUE, BOOL bDoLoadScene = FALSE, INT iLoadSceneWaitTimeMs = 500)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_LOAD")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_LOAD)
	g_turretCamConfig.load.iLoadSceneWaitTimeMs = iLoadSceneWaitTimeMs
	ENABLE_BIT(g_turretCamConfig.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEOUT, bDoFadeOut)
	ENABLE_BIT(g_turretCamConfig.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEIN, bDoFadeIn)
	ENABLE_BIT(g_turretCamConfig.load.iBs, ci_TURRET_CAM_LOAD_BS_LOADSCENE, bDoLoadScene)
ENDPROC

/// PURPOSE:
///    Adds an offset to the inital vCoords of the cam.
///    Moving LS horizontally will rotate this offset vector.
///    
///    Vertical movement is unaffected.
PROC TURRET_CAM_CONFIG_SET_JOINT_Z(VECTOR vOffset)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_SET_JOINT_Z | vOffset = ", vOffset)
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_JOINT_Z)
	g_turretCamConfig.vJointZ = vOffset
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
PROC TURRET_CAM_CONFIG_WEAPON_NONE()
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_WEAPON_NONE")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WMODE)
	g_turretCamConfig.weapon.eFiringMode = WM_NO_WEAPON
ENDPROC

/// PURPOSE:
///    Set weapon firing mode to FULL AUTO.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eShotType - Projectile to fire.
///    iWeaponDamage - Projectile damage.
///    iTimeBetweenShots - Wait time between shots (ms) while trigger is held.
///    fRange - Instant hit WEAPON_TYPE varients will scan this far.
///    fSpreadAngleMax - Max bullet spread (degrees)
///    iAmmoDurationMs - Duration of continuous shooting before forcing reload (ms). Set to 0 to have no limit.
///    iReloadDurationMs - Duration of a reload (set iAmmoDurationMs to 0 to ignore reloading). 
PROC TURRET_CAM_CONFIG_WEAPON_FULL_AUTO(WEAPON_TYPE eShotType = WEAPONTYPE_DLC_ARENA_MACHINE_GUN, INT iWeaponDamage = 30, INT iTimeBetweenShots = 200, FLOAT fRange = 200.0, FLOAT fSpreadAngleMax = 0.4, INT iAmmoDurationMs = 0, INT iReloadDurationMs = 3000) 
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_FULL_AUTO")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WMODE)
	g_turretCamConfig.weapon.eFiringMode = WM_FULL_AUTO
	g_turretCamConfig.weapon.eShotType = eShotType
	g_turretCamConfig.weapon.iTimeBetweenShotsMs = iTimeBetweenShots
	g_turretCamConfig.weapon.iWeaponDamage = iWeaponDamage 
	g_turretCamConfig.weapon.fRange = fRange
	g_turretCamConfig.fullAuto.fSpreadAngleMax = fSpreadAngleMax
	g_turretCamConfig.fullAuto.iAmmoDurationMs = iAmmoDurationMs
	g_turretCamConfig.fullAuto.iReloadDurationMs = iReloadDurationMs
ENDPROC

/// PURPOSE:
///    Set weapon firing mode to PILOTED MISSILE.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    iTimeBetweenShots - Min time after firing a missile before another can be fired (ms).
PROC TURRET_CAM_CONFIG_WEAPON_PILOTED_MISSILE(INT iTimeBetweenShots)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_WEAPON_PILOTED_MISSILE")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WMODE)
	g_turretCamConfig.weapon.eFiringMode = WM_PILOTED_MISSILE
	// eShotType is ununsed 	
	g_turretCamConfig.weapon.eShotType = WEAPONTYPE_VEHICLE_PLAYER_BULLET 
	g_turretCamConfig.weapon.iTimeBetweenShotsMs = iTimeBetweenShots
	// iWeaponDamage is ununsed 	
	g_turretCamConfig.weapon.iWeaponDamage = 0
	// Range isn't used because we just fire in some direction
	g_turretCamConfig.weapon.fRange = 0.0
ENDPROC

/// PURPOSE:
///    Set weapon firing mode to HOMING MISSILE.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eShotType - Projectile to fire.
///    iWeaponDamage - Projectile damage.
///    iTimeBetweenShots - Min time after firing a missile before another can be fired (ms).
///    fRange - Targeting range
PROC TURRET_CAM_CONFIG_WEAPON_HOMING_MISSILE(WEAPON_TYPE eShotType = WEAPONTYPE_VEHICLE_ROCKET, MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT,INT iWeaponDamage = 50, INT iTimeBetweenShots = 3000, FLOAT fRange = 300.0)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_WEAPON_HOMING_MISSILE")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WMODE)
	g_turretCamConfig.weapon.eFiringMode = WM_HOMING_MISSILE
	g_turretCamConfig.weapon.eShotType = eShotType 
	g_turretCamConfig.weapon.iTimeBetweenShotsMs = iTimeBetweenShots
	g_turretCamConfig.weapon.iWeaponDamage = iWeaponDamage
	g_turretCamConfig.weapon.fRange = fRange
	g_turretCamConfig.homingMissile.eModel = eModel 
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eHudSounds - Sound implementations are very specific so you must choose from these presets.
///    			    New sound types can be added in turret_cam_script.
PROC TURRET_CAM_CONFIG_HUD_SOUNDS(TURRET_CAM_HUD_SOUNDS eHudSounds = TCHS_ARENA_CAM)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_HUD_SOUNDS") 
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_HUD_SOUNDS)
	g_turretCamConfig.eHudSounds = eHudSounds
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eWeaponSounds - Sound implementations are very specific so you must choose from these presets.
///    			       New sound types can be added in turret_cam_script.
PROC TURRET_CAM_CONFIG_WEAPON_SOUNDS(TURRET_CAM_WEAPON_SOUNDS eWeaponSounds = TCWS_ARENA_TURRET_SOUNDS)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_WEAPON_SOUNDS")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS)
	g_turretCamConfig.eWeaponSounds = eWeaponSounds
ENDPROC

/// PURPOSE:
///    Adds help text that displays while the turret is running.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    sLabel - Help text label.
PROC TURRET_CAM_CONFIG_ADD_HELP_TEXT(STRING sLabel = NULL)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_ADD_HELP_TEXT | label = ", sLabel)
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_HELP)
	g_turretCamConfig.txt15HelpLabel = sLabel
ENDPROC

/// PURPOSE:
///    Add an instructional button.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eActionGroup - Control_action to display.
///    sLabel - Help text label to use.
PROC TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(CONTROL_ACTION eAction, STRING sLabel)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON | idx = ", g_turretCamConfig.instButtons.iCount, " | action = ", eAction, " | label = ", sLabel)
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
	INT iBtnCount = g_turretCamConfig.instButtons.iCount
	
	IF iBtnCount >= ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS
		CASSERTLN(DEBUG_NET_TURRET, "   Increase ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS")
		EXIT
	ENDIF

	g_turretCamConfig.instButtons.txt15Labels[iBtnCount] = sLabel
	// NOTE: Shift up by MAX_INPUTGROUPS so we can store CONTROL_ACTION_GROUP values in same variable.
	g_turretCamConfig.instButtons.eActions[iBtnCount] = INT_TO_ENUM(CONTROL_ACTION, ENUM_TO_INT(MAX_INPUTGROUPS)) + eAction
	g_turretCamConfig.instButtons.iCount++
ENDPROC

/// PURPOSE:
///    Add an instructional button group.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    eActionGroup - Control_action_group to display.
///    sLabel - Help text label to use.
PROC TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_GROUP(CONTROL_ACTION_GROUP eActionGroup, STRING sLabel)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_GROUP | idx = ", g_turretCamConfig.instButtons.iCount, " | action = ", eActionGroup, " | label = ", sLabel)
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
	INT iBtnCount = g_turretCamConfig.instButtons.iCount
	
	IF iBtnCount > ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS
		CASSERTLN(DEBUG_NET_TURRET, "   Increase ci_TURRET_CAM_MAX_INSTRUCTIONAL_BUTTONS")
		EXIT
	ENDIF

	g_turretCamConfig.instButtons.txt15Labels[iBtnCount] = sLabel
	// NOTE: Cast control_action_group to a CONTROL_ACTION. The CONTROL_ACTION values are shifted up by MAX_INPUTGROUPS.
	g_turretCamConfig.instButtons.eActions[iBtnCount] = INT_TO_ENUM(CONTROL_ACTION, ENUM_TO_INT(eActionGroup))
	g_turretCamConfig.instButtons.iCount++
ENDPROC

/// PURPOSE:
///    Add a static fire point: The projectile will start from the specified bone index
///    and fire in the direction of the EULER_TO_DIRECTION(boneRot). The bone which is most
///    closely aligned with the camera direction will be used if multiple points are registered.
///    
///    If this is being used for a config 
///    update please see  TURRET_CAM_CONFIG_UPDATE_REMOVE_STATIC_FIRE_POINTS.
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    iBoneId - The bone which will be used to get pos and rot for projectiles fired from this point.
PROC TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT(INT iBoneId)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_ADD_STATIC_FIRE_POINT | idx = ", g_turretCamConfig.staticFirePoints.iCount, " | iBoneId = ", iBoneId)
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
	
	INT iPointCount = g_turretCamConfig.staticFirePoints.iCount
	IF iPointCount >= ci_TURRET_CAM_MAX_STATIC_FIRE_POINTS
		CASSERTLN(DEBUG_NET_TURRET, "   Increase ci_TURRET_CAM_MAX_STATIC_FIRE_POINTS")
		EXIT
	ENDIF

	g_turretCamConfig.staticFirePoints.iBoneIds[iPointCount] = iBoneId
	g_turretCamConfig.staticFirePoints.iCount++
ENDPROC

/// PURPOSE:
///    Set an input map for pc inputs (see dynamic.meta).
///    Loaded on turret script init, shutdown on script shutdown
///    
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    sInputMap - dynamic.meta input map name - 15 char max.
PROC TURRET_CAM_CONFIG_SET_INPUT_MAP(STRING sInputMap = NULL)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_SET_INPUT_MAP | ", sInputMap)
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_PC_INPUT_MAP)
	g_turretCamConfig.txt15InputMap = sInputMap
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_START for more info.
/// PARAMS:
///    ref_eConfigId - Store a configId to validate that the cached config
///    				   matches this one with TURRET_CAM_IS_CONFIG_SAVED 
///    				   (if not, recreate this one).
PROC TURRET_CAM_CONFIG_STOP(TURRET_CAM_CONFIG_ID& ref_eConfigId)
	ref_eConfigId = INT_TO_ENUM(TURRET_CAM_CONFIG_ID, NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	g_eTurretCamConfigId = ref_eConfigId
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_STOP configId = ", ENUM_TO_INT(ref_eConfigId))
ENDPROC

/// PURPOSE:
///    A turret cam config update must start with this proc
///    and end with TURRET_CAM_CONFIG_UPDATE_STOP().
///    
///    You can call any config function here as if you were
///    setting up the inital config*.
///
///    * You'll hit an assert if the config hasn't been set up
///    for updates yet - Contact Orlando C.-H. or see CONFIG_UPDATE
///    in turret_cam_script.sch
///    
///    All config options start with prefix 
///    TURRET_CAM_CONFIG_
///    
///    e.g. Update the cam limits and the weapon type.
///    TURRET_CAM_CONFIG_UPDATE_START()
///    		TURRET_CAM_CONFIG_CAM_LIMITS(-70, 70, -30, 30, 40, 65)
///    		TURRET_CAM_CONFIG_WEAPON_FULL_AUTO()
///    TURRET_CAM_CONFIG_UPDATE_STOP()
PROC TURRET_CAM_CONFIG_UPDATE_START()
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_UPDATE_START")
	
	// Save current settings bs into update bs so that we can 
	// determine which config settings have changed 
	// (see TURRET_CAM_CONFIG_UPDATE_STOP).
	g_iBsTurretCamConfigUpdate = g_iBsTurretCamConfigSettings
	g_iBsTurretCamConfigSettings = 0
ENDPROC

/// PURPOSE:
///    Thid call is required if you want to change the
///    static fire points during a config update.
PROC TURRET_CAM_CONFIG_UPDATE_REMOVE_STATIC_FIRE_POINTS()
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_UPDATE_REMOVE_STATIC_FIRE_POINTS")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
	g_turretCamConfig.staticFirePoints.iCount = 0
ENDPROC

/// PURPOSE:
///    Thid call is required if you want to change the
///    static fire points during a config update.
PROC TURRET_CAM_CONFIG_UPDATE_REMOVE_INSTRUCTIONAL_BUTTONS()
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_UPDATE_REMOVE_INSTRUCTIONAL_BUTTONS")
	SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
	 g_turretCamConfig.instButtons.iCount = 0
ENDPROC

/// PURPOSE:
///    See TURRET_CAM_CONFIG_UPDATE_START for more info.
///    
///    This will generate a new ConfigId. If you choose to overrwrite your
///    saved configId with this return value then this update will apply to
///    future turrets created using that configId.
///    
FUNC TURRET_CAM_CONFIG_ID TURRET_CAM_CONFIG_UPDATE_STOP()
	SET_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_CONFIG_UPDATE)

	TURRET_CAM_CONFIG_ID eNewConfigId = INT_TO_ENUM(TURRET_CAM_CONFIG_ID, NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	g_eTurretCamConfigId = eNewConfigId
	
	// Reload the stored settings bs and assign the update bs
	// to now show which values have been updated.
	INT iPreviousSettings = g_iBsTurretCamConfigUpdate
	g_iBsTurretCamConfigUpdate = g_iBsTurretCamConfigSettings
	g_iBsTurretCamConfigSettings = iPreviousSettings
	
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_CAM_CONFIG_UPDATE_STOP configId = ", eNewConfigId)
	RETURN eNewConfigId
ENDFUNC

/// PURPOSE:
///    Call to let the turret_cam script a drone missile has been fired.
PROC TURRET_CAM_SET_HAVE_FIRED_DRONE_MISSILE()
	CDEBUG1LN(DEBUG_NET_TURRET,"TURRET_CAM_SET_HAVE_FIRED_DRONE_MISSILE : Firing drone missile")
	CLEAR_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_LAUNCH_DRONE_MISSILE)
ENDPROC

/// PURPOSE:
///    Enable all the controls that are used by turret_cam_script.
PROC TURRET_CAM_ENABLE_CONTROLS()
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_RT)
ENDPROC
