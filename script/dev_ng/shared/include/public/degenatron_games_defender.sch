USING "degenatron_games_using.sch"
USING "degenatron_games_common.sch"
USING "degenatron_games_animation.sch"

CONST_FLOAT cfDG_DEFENDER_GROUND_BASE_HEIGHT 			150.0
CONST_FLOAT cfDG_DEFENDER_GROUND_ROCK_WIDTH 			932.0
CONST_FLOAT cfDG_DEFENDER_GROUND_ROCK_HEIGHT 			262.0
CONST_INT 	ciDG_DEFENDER_GROUND_ROCK_AMOUNT 			3

CONST_FLOAT cfDG_DEFENDER_PLAYER_HEIGHT					110.4175532
CONST_FLOAT cfDG_DEFENDER_PLAYER_WIDTH 					140.0
CONST_INT	ciDG_DEFENDER_PLAYER_SPEED					1500
CONST_FLOAT cfDG_DEFENDER_PLAYER_SCREEN_RATE_SPEED		1.0

CONST_FLOAT cfDG_DEFENDER_ENEMY_HEIGHT					86.75664894
CONST_FLOAT cfDG_DEFENDER_ENEMY_WIDTH 					110.0
CONST_INT 	ciDG_DEFENDER_ENEMY_SPEED					110

CONST_FLOAT cfDG_DEFENDER_OBJECTIVE_HEIGHT				70.98271277
CONST_FLOAT cfDG_DEFENDER_OBJECTIVE_WIDTH 				90.0
CONST_INT	cfDG_DEFENDER_OBJECTIVE_SPEED				100

CONST_INT 	ciDG_DEFENDER_OBJECTIVE_SCORE 				50

CONST_INT 	ciDG_DEFENDER_MAX_LEVEL 					24
CONST_INT 	ciDG_DEFENDER_MAX_ENEMY_AMOUNT 				160
CONST_INT 	ciDG_DEFENDER_MAX_OBJECTIVE_AMOUNT 			150

CONST_FLOAT cfDG_DEFENDER_PLAYER_MIN_SCREEN_POS  		0.45
CONST_FLOAT cfDG_DEFENDER_PLAYER_MAX_SCREEN_POS  		0.55

CONST_INT 	ciDG_DEFENDER_SCORE_DECREMENT_TIME_MAX  	2500
CONST_INT 	ciDG_DEFENDER_SCORE_DECREMENT_TIME_MIN		500
CONST_INT 	ciDG_DEFENDER_SCORE_DECREMENT_TIME_STEP		100

CONST_FLOAT	cfDG_DEFENDER_PLAYER_SPEED_MULTIPLIER_MAX  	3.0
CONST_FLOAT	cfDG_DEFENDER_PLAYER_SPEED_MULTIPLIER_MIN	1.0
CONST_FLOAT	cfDG_DEFENDER_PLAYER_SPEED_MULTIPLIER_STEP	0.05

CONST_INT 	ciDG_DEFENDER_PLAYER_DEAD_FLASHES			2


ENUM DG_DEFENDER_STATE_LEVEL
	DG_DEFENDER_STATE_LEVEL_INIT,
	DG_DEFENDER_STATE_LEVEL_UPDATE,
	DG_DEFENDER_STATE_LEVEL_COMPLETED,
	DG_DEFENDER_STATE_LEVEL_DEAD,
	DG_DEFENDER_STATE_LEVEL_WAIT_TIME,
	DG_DEFENDER_STATE_LEVEL_END
ENDENUM

FUNC STRING DG_DEFENDER_STATE_LEVEL_TO_STRING(DG_DEFENDER_STATE_LEVEL eEnum)
	SWITCH eEnum
		CASE DG_DEFENDER_STATE_LEVEL_INIT 			RETURN "LEVEL_INIT"
		CASE DG_DEFENDER_STATE_LEVEL_UPDATE 		RETURN "LEVEL_UPDATE"
		CASE DG_DEFENDER_STATE_LEVEL_COMPLETED 		RETURN "LEVEL_COMPLETED"
		CASE DG_DEFENDER_STATE_LEVEL_DEAD 			RETURN "LEVEL_DEAD"
		CASE DG_DEFENDER_STATE_LEVEL_WAIT_TIME		RETURN "LEVEL_WAIT_TIME"
		CASE DG_DEFENDER_STATE_LEVEL_END			RETURN "LEVEL_END"
	ENDSWITCH
	RETURN "***INVALID***"
ENDFUNC

STRUCT DG_DEFENDER_PLAYER_DATA
	VECTOR_2D 	vPlayerGamePos
	VECTOR_2D 	vPlayerDirection
	VECTOR_2D 	vPlayerScreenPos
	VECTOR_2D	vPlayerScreenPosRate
	VECTOR_2D	vPlayerGameAcceleration
ENDSTRUCT
STRUCT DG_DEFENDER_ENEMY_DATA
	VECTOR_2D 	vEnemyGamePos
	VECTOR_2D 	vEnemyDirection
	VECTOR_2D 	vEnemyScreenPos
	BOOL		bCollision
ENDSTRUCT
STRUCT DG_DEFENDER_OBJECTIVE_DATA
	VECTOR_2D 	vObjectiveGamePos
	VECTOR_2D 	vObjectiveDirection
	VECTOR_2D 	vObjectiveScreenPos
	BOOL		bCollision
	BOOL		bActive = TRUE
ENDSTRUCT
STRUCT DG_DEFENDER_LEVEL_DATA
	INT 					iMapTiles
	INT 					iEnemiesAmount
	INT 					iObjectivesAmount
	INT						iScoreTimer
	INT						iScoreTimerMax
	FLOAT					fPlayerSpeedMultiplier
	BOOL					bObjectiveJustCaptured
	DG_DEFENDER_STATE_LEVEL eLevelState
ENDSTRUCT

STRUCT DG_DEFENDER_DATA
	INT 							iRocksMap[ciDG_DEFENDER_GROUND_ROCK_AMOUNT]
	DG_DEFENDER_PLAYER_DATA 		sPlayerData
	DG_DEFENDER_ENEMY_DATA 			sEnemyData[ciDG_DEFENDER_MAX_ENEMY_AMOUNT]
	DG_DEFENDER_OBJECTIVE_DATA 		sObjectiveData[2*ciDG_DEFENDER_MAX_LEVEL]
	DG_DEFENDER_LEVEL_DATA			sLevelData
	
	BOOL							bUpdateFirstFrameDone = FALSE
	BOOL							bKeepFaithAward = TRUE
	
	INT								iScoreTimerDecrementMax = ciDG_DEFENDER_SCORE_DECREMENT_TIME_MAX
	INT								iScoreTimerDecrementMin = ciDG_DEFENDER_SCORE_DECREMENT_TIME_MIN
	INT								iScoreTimerDecrementStep = ciDG_DEFENDER_SCORE_DECREMENT_TIME_STEP

	FLOAT							fPlayerSpeedMultiplayerMax = cfDG_DEFENDER_PLAYER_SPEED_MULTIPLIER_MAX
	FLOAT							fPlayerSpeedMultiplayerMin = cfDG_DEFENDER_PLAYER_SPEED_MULTIPLIER_MIN
	FLOAT							fPlayerSpeedMultiplayerStep = cfDG_DEFENDER_PLAYER_SPEED_MULTIPLIER_STEP

	#IF IS_DEBUG_BUILD
	BOOL		bWidgetSetLevel
	INT			iWidgetEnemySelected
	INT			iWidgetObjectiveSelected
	#ENDIF
ENDSTRUCT

DG_DEFENDER_DATA sDGDefenderData

//**************************************************//
//*     DATA ACCESSORS - DEFENDER OF THE FAITH     *//
//**************************************************//

// MAP //
FUNC FLOAT DG_DEFENDER_GET_MAP_TILE_SIZE
	RETURN cfDG_DEFENDER_GROUND_ROCK_WIDTH*3
ENDFUNC
FUNC FLOAT DG_DEFENDER_GET_MAP_SIZE
	RETURN DG_DEFENDER_GET_MAP_TILE_SIZE()*sDGDefenderData.sLevelData.iMapTiles
ENDFUNC

// GROUND //
FUNC FLOAT DG_DEFENDER_GET_GROUND_HEIGHT
	RETURN cfDG_DEFENDER_GROUND_BASE_HEIGHT
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GROUND_POSITION
	VECTOR_2D groundPosition = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	groundPosition.y += DEGENATRON_GAMES_GET_SCREEN_HEIGHT()/2 //- DG_DEFENDER_GET_GROUND_HEIGHT()*0.4
	RETURN groundPosition
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GROUND_SIZE
	RETURN INIT_VECTOR_2D(DEGENATRON_GAMES_GET_SCREEN_WIDTH(),DG_DEFENDER_GET_GROUND_HEIGHT())
ENDFUNC

// ROCKS //
FUNC FLOAT DG_DEFENDER_GET_GROUND_ROCK_WIDTH
	RETURN cfDG_DEFENDER_GROUND_ROCK_WIDTH //DEGENATRON_GAMES_GET_SCREEN_WIDTH()/(ciDG_DEFENDER_GROUND_ROCK_AMOUNT-2)
ENDFUNC
FUNC FLOAT DG_DEFENDER_GET_GROUND_ROCK_HEIGHT
	RETURN cfDG_DEFENDER_GROUND_ROCK_HEIGHT //DEGENATRON_GAMES_GET_SCREEN_WIDTH()/(ciDG_DEFENDER_GROUND_ROCK_AMOUNT-2)
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_GROUND_ROCK_SIZE
	RETURN INIT_VECTOR_2D(DG_DEFENDER_GET_GROUND_ROCK_WIDTH(),DG_DEFENDER_GET_GROUND_ROCK_HEIGHT())
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_ROCK_POSITION(INT iRock)
	VECTOR_2D screenCenter = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	VECTOR_2D screenSize = DEGENATRON_GAMES_GET_SCREEN_SIZE()
//	VECTOR_2D groundSize = DG_DEFENDER_GROUND_SIZE()
	VECTOR_2D rockSize = DG_DEFENDER_GET_GROUND_ROCK_SIZE()

	VECTOR_2D rockPosition
	rockPosition.x = screenCenter.x - screenSize.x/2 + (rockSize.x*iRock + sDGDefenderData.sPlayerData.vPlayerGamePos.x)%DG_DEFENDER_GET_MAP_TILE_SIZE()
	rockPosition.y = screenCenter.y + screenSize.y/2 - rockSize.y/2
	
	RETURN rockPosition
ENDFUNC

// PLAYER //
FUNC FLOAT DG_DEFENDER_GET_PLAYER_HEIGHT
	RETURN cfDG_DEFENDER_PLAYER_HEIGHT
ENDFUNC
FUNC FLOAT DG_DEFENDER_GET_PLAYER_WIDTH
	RETURN cfDG_DEFENDER_PLAYER_WIDTH
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_PLAYER_SIZE
	RETURN INIT_VECTOR_2D(DG_DEFENDER_GET_PLAYER_WIDTH(),DG_DEFENDER_GET_PLAYER_HEIGHT())
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_PLAYER_POSITION()
	RETURN sDGDefenderData.sPlayerData.vPlayerGamePos
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_PLAYER_SCREEN_POSITION()
	RETURN sDGDefenderData.sPlayerData.vPlayerScreenPos
ENDFUNC

// ENEMY //
FUNC FLOAT DG_DEFENDER_GET_ENEMY_HEIGHT
	RETURN cfDG_DEFENDER_ENEMY_HEIGHT
ENDFUNC
FUNC FLOAT DG_DEFENDER_GET_ENEMY_WIDTH
	RETURN cfDG_DEFENDER_ENEMY_WIDTH
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_ENEMY_SIZE
	RETURN INIT_VECTOR_2D(DG_DEFENDER_GET_ENEMY_WIDTH(),DG_DEFENDER_GET_ENEMY_HEIGHT())
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_ENEMY_POSITION(INT iEnemy)
	RETURN sDGDefenderData.sEnemyData[iEnemy].vEnemyGamePos
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_ENEMY_SCREEN_POSITION(INT iEnemy)
	RETURN sDGDefenderData.sEnemyData[iEnemy].vEnemyScreenPos
ENDFUNC

// OBJECTIVE //
FUNC FLOAT DG_DEFENDER_GET_OBJECTIVE_HEIGHT
	RETURN cfDG_DEFENDER_OBJECTIVE_HEIGHT
ENDFUNC
FUNC FLOAT DG_DEFENDER_GET_OBJECTIVE_WIDTH
	RETURN cfDG_DEFENDER_OBJECTIVE_WIDTH
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_OBJECTIVE_SIZE
	RETURN INIT_VECTOR_2D(DG_DEFENDER_GET_OBJECTIVE_WIDTH(),DG_DEFENDER_GET_OBJECTIVE_HEIGHT())
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_OBJECTIVE_POSITION(INT iObjective)
	RETURN sDGDefenderData.sObjectiveData[iObjective].vObjectiveGamePos
ENDFUNC
FUNC VECTOR_2D DG_DEFENDER_GET_OBJECTIVE_SCREEN_POSITION(INT iObjective)
	RETURN sDGDefenderData.sObjectiveData[iObjective].vObjectiveScreenPos
ENDFUNC
FUNC BOOL DG_DEFENDER_GET_OBJECTIVE_IS_ACTIVE(INT iObjective)
	RETURN sDGDefenderData.sObjectiveData[iObjective].bActive
ENDFUNC


//**************************************************//
//*          DRAW - DEFENDER OF THE FAITH          *//
//**************************************************//
PROC DG_DEFENDER_DRAW_PIXELSPACE_RECT(VECTOR_2D vCenter, VECTOR_2D vScale, RGBA_COLOUR_STRUCT rgba, BOOL bLoopTerrain = FALSE, BOOL bAvoidAberrationFX = FALSE)
	unused_parameter(bLoopTerrain)
	DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(vCenter, vScale, rgba,bAvoidAberrationFX)
		IF vCenter.x > DG_DEFENDER_GET_MAP_TILE_SIZE() - vScale.x/2
			DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(ADD_VECTOR_2D(vCenter,INIT_VECTOR_2D(-DG_DEFENDER_GET_MAP_TILE_SIZE(),0)), vScale, rgba,bAvoidAberrationFX)
		ENDIF
//	ENDIF
ENDPROC

PROC DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM eTexture, VECTOR_2D vCenter, VECTOR_2D vScale, RGBA_COLOUR_STRUCT rgba, BOOL bLoopTerrain = FALSE, BOOL bAvoidAberrationFX = FALSE)
	unused_parameter(bLoopTerrain)
	DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(eTexture, vCenter, vScale,0.0, rgba,bAvoidAberrationFX)
	IF bLoopTerrain
		IF vCenter.x > DG_DEFENDER_GET_MAP_TILE_SIZE() - vScale.x/2
			DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(eTexture, ADD_VECTOR_2D(vCenter,INIT_VECTOR_2D(-DG_DEFENDER_GET_MAP_TILE_SIZE(),0)), vScale,0.0, rgba,bAvoidAberrationFX)
		ENDIF
	ENDIF
ENDPROC


PROC DG_DEFENDER_DRAW_PLAYER()
	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		VECTOR_2D vPlayerSize = DG_DEFENDER_GET_PLAYER_SIZE()
		VECTOR_2D vPlayerPos = DG_DEFENDER_GET_PLAYER_SCREEN_POSITION()
		VECTOR_2D vSquareSize
		VECTOR_2D vSquarePos
		RGBA_COLOUR_STRUCT color
		IF sDegenatronGamesData.sGameData.iFlashTime/(ciDEGENATRON_GAMES_PLAYER_FLASH_TIME/4)%2 = 0
			color = sDegenatronGamesData.rgbaWhite
		ELSE
			color = sDegenatronGamesData.rgbaRed
		ENDIF
		
		IF sDegenatronGamesData.sGameData.iFlashTime < ciDEGENATRON_GAMES_PLAYER_FLASH_TIME/2
			vSquareSize.x = vPlayerSize.x*0.35
			vSquareSize.y = vPlayerSize.y*0.35
			vSquarePos.x = vPlayerPos.x - vPlayerSize.x*0.25
			vSquarePos.y = vPlayerPos.y + vPlayerSize.y*0.25
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,vSquarePos, vSquareSize, color)
			vSquareSize.x = vPlayerSize.x*0.15
			vSquareSize.y = vPlayerSize.y*0.15
			vSquarePos.x = vPlayerPos.x - vPlayerSize.x*0.3
			vSquarePos.y = vPlayerPos.y - vPlayerSize.y*0.2
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x + vPlayerSize.x*0.3
			vSquarePos.y = vPlayerPos.y - vPlayerSize.y*0.3
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x + vPlayerSize.x*0.2
			vSquarePos.y = vPlayerPos.y + vPlayerSize.y*0.3
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
		ELIF sDegenatronGamesData.sGameData.iFlashTime < ciDEGENATRON_GAMES_PLAYER_FLASH_TIME
			vSquareSize.x = vPlayerSize.x*0.25
			vSquareSize.y = vPlayerSize.y*0.25
			vSquarePos.x = vPlayerPos.x
			vSquarePos.y = vPlayerPos.y
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_03_SOLID,vSquarePos, vSquareSize, color)
			vSquareSize.x = vPlayerSize.x*0.15
			vSquareSize.y = vPlayerSize.y*0.15
			vSquarePos.x = vPlayerPos.x - vPlayerSize.x*0.3
			vSquarePos.y = vPlayerPos.y
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x + vPlayerSize.x*0.3
			vSquarePos.y = vPlayerPos.y
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x
			vSquarePos.y = vPlayerPos.y - vPlayerSize.y*0.3
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x
			vSquarePos.y = vPlayerPos.y + vPlayerSize.y*0.3
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquareSize.x = vPlayerSize.x*0.1
			vSquareSize.y = vPlayerSize.y*0.1
			vSquarePos.x = vPlayerPos.x - vPlayerSize.x*0.1
			vSquarePos.y = vPlayerPos.y - vPlayerSize.y*0.4
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x + vPlayerSize.x*0.25
			vSquarePos.y = vPlayerPos.y - vPlayerSize.y*0.45
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x + vPlayerSize.x*0.35
			vSquarePos.y = vPlayerPos.y + vPlayerSize.y*0.4
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
			vSquarePos.x = vPlayerPos.x - vPlayerSize.x*0.4
			vSquarePos.y = vPlayerPos.y + vPlayerSize.y*0.34
			DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_04_SOLID,vSquarePos, vSquareSize, color)
		ENDIF
	ELSE
		DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_DEFENDER_GET_PLAYER_SCREEN_POSITION(), DG_DEFENDER_GET_PLAYER_SIZE(), sDegenatronGamesData.rgbaRed)
	ENDIF	
ENDPROC

PROC DG_DEFENDER_DRAW_ENEMY(INT iEnemy)
	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		EXIT
	ENDIF
	VECTOR_2D vPosition = DG_DEFENDER_GET_ENEMY_SCREEN_POSITION(iEnemy)
	VECTOR_2D vSize = DG_DEFENDER_GET_ENEMY_SIZE()
	DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,vPosition, DG_DEFENDER_GET_ENEMY_SIZE(), sDegenatronGamesData.rgbaCyan,DEFAULT)
	IF vPosition.x < vSize.x/2
	OR vPosition.x > DEGENATRON_GAMES_GET_SCREEN_WIDTH() - vSize.x/2
		DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,vPosition, DG_DEFENDER_GET_ENEMY_SIZE(), sDegenatronGamesData.rgbaCyan)
	ENDIF
ENDPROC

PROC DG_DEFENDER_DRAW_ENEMIES
	INT iEnemy
	REPEAT sDGDefenderData.sLevelData.iEnemiesAmount iEnemy
		DG_DEFENDER_DRAW_ENEMY(iEnemy)
	ENDREPEAT
ENDPROC

PROC DG_DEFENDER_DRAW_OBJECTIVE(INT iObjective)
	IF DG_DEFENDER_GET_OBJECTIVE_IS_ACTIVE(iObjective)
		DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_03_SOLID,DG_DEFENDER_GET_OBJECTIVE_SCREEN_POSITION(iObjective), DG_DEFENDER_GET_OBJECTIVE_SIZE(), sDegenatronGamesData.rgbaGreen)
	ENDIF
ENDPROC

PROC DG_DEFENDER_DRAW_OBJECTIVES
	INT iObjective
	REPEAT sDGDefenderData.sLevelData.iObjectivesAmount iObjective
		DG_DEFENDER_DRAW_OBJECTIVE(iObjective)
	ENDREPEAT
ENDPROC

PROC DG_DEFENDER_DRAW_BACKGROUND()
	DG_DEFENDER_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaBlack,DEFAULT,TRUE)
	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		sDegenatronGamesData.sGameData.iFlashTime += DEGENATRON_GAMES_FRAME_TIME()
		IF NOT sDegenatronGamesData.sGameData.bFlashedOnce
			DG_DEFENDER_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaWhite)
			sDegenatronGamesData.sGameData.bFlashedOnce = TRUE
		ELIF NOT sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME/2
			DG_DEFENDER_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaWhite)
			sDegenatronGamesData.sGameData.bFlashedTwice = TRUE
		ENDIF
	ENDIF
	IF sDGDefenderData.sLevelData.bObjectiveJustCaptured
		sDegenatronGamesData.sGameData.iFlashTime += DEGENATRON_GAMES_FRAME_TIME()
		IF NOT sDegenatronGamesData.sGameData.bFlashedOnce
			DG_DEFENDER_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaGreen)
			sDegenatronGamesData.sGameData.bFlashedOnce = TRUE
		ELIF NOT sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME/2
			DG_DEFENDER_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaGreen)
			sDegenatronGamesData.sGameData.bFlashedTwice = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC DG_DEFENDER_DRAW_ROCKS()
	//DRAW ROCKS
	INT iRock
	REPEAT ciDG_DEFENDER_GROUND_ROCK_AMOUNT iRock
		DEGENATRON_GAMES_TEXT_ITEM eRock
		eRock = INT_TO_ENUM(DEGENATRON_GAMES_TEXT_ITEM, ENUM_TO_INT(DEGENATRON_GAMES_TEXT_ITEM_DEFENDER_SCENE_01)+iRock)
		DG_DEFENDER_DRAW_PIXELSPACE_SPRITE(eRock,DG_DEFENDER_GET_ROCK_POSITION(iRock), DG_DEFENDER_GET_GROUND_ROCK_SIZE(), sDegenatronGamesData.rgbaBrown, TRUE)
	ENDREPEAT
	//DRAW GROUND
	DG_DEFENDER_DRAW_PIXELSPACE_RECT(DG_DEFENDER_GROUND_POSITION(), DG_DEFENDER_GROUND_SIZE(), sDegenatronGamesData.rgbaBrown,DEFAULT)
ENDPROC

PROC DG_DEFENDER_CLIENT_STATE_DRAW()
	DG_DEFENDER_DRAW_BACKGROUND()
	DEGENATRON_GAMES_DRAW_SCORE_BAR(TRUE,TRUE)
	SWITCH sDGDefenderData.sLevelData.eLevelState
		CASE DG_DEFENDER_STATE_LEVEL_INIT
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_UPDATE
		CASE DG_DEFENDER_STATE_LEVEL_COMPLETED
		CASE DG_DEFENDER_STATE_LEVEL_DEAD
		CASE DG_DEFENDER_STATE_LEVEL_WAIT_TIME
			DG_DEFENDER_DRAW_ROCKS()
			IF sDGDefenderData.bUpdateFirstFrameDone
				DG_DEFENDER_DRAW_PLAYER()
				DG_DEFENDER_DRAW_ENEMIES()
				DG_DEFENDER_DRAW_OBJECTIVES()
			ELSE
				sDGDefenderData.bUpdateFirstFrameDone = TRUE
			ENDIF
			DEGENATRON_GAMES_DRAW_FAIL_SUCCESS_TEXT()
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_END
		BREAK
	ENDSWITCH
	
ENDPROC

//**************************************************//
//*         UPDATE - DEFENDER OF THE FAITH         *//
//**************************************************//
FUNC FLOAT EASE_IN_OUT_EXPO(FLOAT t)
	IF T < 0.5 
        RETURN (POW( 2, 16 * t ) - 1) / 510
    ELSE
        RETURN 1 - 0.5 * POW( 2, -16 * (t - 0.5) )
    ENDIF
  ENDFUNC
  
FUNC FLOAT EASE_IN_OUT_SINE(FLOAT t)
    IF t < 0.5 
		RETURN 2 * t * t 
	ELSE
		RETURN t * (4 - 2 * t) - 1
	ENDIF
ENDFUNC

FUNC BOOL DG_DEFENDER_CHECK_ENEMY_COLLISION(INT iEnemy, BOOL bForSpawn = FALSE)
	VECTOR_2D playerScreenPosition = DG_DEFENDER_GET_PLAYER_SCREEN_POSITION()
	VECTOR_2D playerSize = MULTIPLY_VECTOR_2D(DG_DEFENDER_GET_PLAYER_SIZE(), 1.0+2.0*BOOL_TO_INT(bForSpawn))
	VECTOR_2D enemyScreenPosition = DG_DEFENDER_GET_ENEMY_SCREEN_POSITION(iEnemy)
	VECTOR_2D enemySize = DG_DEFENDER_GET_ENEMY_SIZE()
	IF playerScreenPosition.x-playerSize.x/2 > enemyScreenPosition.x+enemySize.x/2
	OR playerScreenPosition.x+playerSize.x/2 < enemyScreenPosition.x-enemySize.x/2
	OR playerScreenPosition.y-playerSize.y/2 > enemyScreenPosition.y+enemySize.y/2
	OR playerScreenPosition.y+playerSize.y/2 < enemyScreenPosition.y-enemySize.y/2
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DG_DEFENDER_CHECK_OBJECTIVE_COLLISION(INT iObjective, BOOL bForSpawn = FALSE)
	VECTOR_2D playerScreenPosition = DG_DEFENDER_GET_PLAYER_SCREEN_POSITION()
	VECTOR_2D playerSize = MULTIPLY_VECTOR_2D(DG_DEFENDER_GET_PLAYER_SIZE(), 1.0+2.0*BOOL_TO_INT(bForSpawn))
	VECTOR_2D objectiveScreenPosition = DG_DEFENDER_GET_OBJECTIVE_SCREEN_POSITION(iObjective)
	VECTOR_2D objectiveSize = DG_DEFENDER_GET_OBJECTIVE_SIZE()
	IF playerScreenPosition.x-playerSize.x/2 > objectiveScreenPosition.x+objectiveSize.x/2
	OR playerScreenPosition.x+playerSize.x/2 < objectiveScreenPosition.x-objectiveSize.x/2
	OR playerScreenPosition.y-playerSize.y/2 > objectiveScreenPosition.y+objectiveSize.y/2
	OR playerScreenPosition.y+playerSize.y/2 < objectiveScreenPosition.y-objectiveSize.y/2
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DG_DEFENDER_CHECK_OBJECTIVE_ENEMY_COLLISION(INT iObjective, INT iEnemy)
	VECTOR_2D enemyScreenPosition = DG_DEFENDER_GET_ENEMY_SCREEN_POSITION(iEnemy)
	VECTOR_2D enemySize = DG_DEFENDER_GET_ENEMY_SIZE()
	VECTOR_2D objectiveScreenPosition = DG_DEFENDER_GET_OBJECTIVE_SCREEN_POSITION(iObjective)
	VECTOR_2D objectiveSize = DG_DEFENDER_GET_OBJECTIVE_SIZE()
	IF enemyScreenPosition.x-enemySize.x/2 > objectiveScreenPosition.x+objectiveSize.x/2
	OR enemyScreenPosition.x+enemySize.x/2 < objectiveScreenPosition.x-objectiveSize.x/2
	OR enemyScreenPosition.y-enemySize.y/2 > objectiveScreenPosition.y+objectiveSize.y/2
	OR enemyScreenPosition.y+enemySize.y/2 < objectiveScreenPosition.y-objectiveSize.y/2
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC DG_DEFENDER_UPDATE_COLLISIONS()
	INT iEnemy
	REPEAT sDGDefenderData.sLevelData.iEnemiesAmount iEnemy
		sDGDefenderData.sEnemyData[iEnemy].bCollision = DG_DEFENDER_CHECK_ENEMY_COLLISION(iEnemy)
	ENDREPEAT	
	INT iObjective
	REPEAT sDGDefenderData.sLevelData.iObjectivesAmount iObjective
		IF DG_DEFENDER_GET_OBJECTIVE_IS_ACTIVE(iObjective)
			sDGDefenderData.sObjectiveData[iObjective].bCollision = DG_DEFENDER_CHECK_OBJECTIVE_COLLISION(iObjective)
		ENDIF
	ENDREPEAT	
ENDPROC

PROC DG_DEFENDER_UPDATE_PLAYER()
	VECTOR_2D padInput
	padInput.x = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT) - GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT) - GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
	padInput.y = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)  - GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN) - GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	padInput.x = CLAMP(padInput.x,-1.0,1.0)
	padInput.y = CLAMP(padInput.y,-1.0,1.0)
	
	IF sDegenatronGamesData.sGameData.iLevel = 1
		IF padInput.x > 0.0 OR padInput.y < 0.0 OR padInput.y > 0.0
			CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DG_DEFENDER_UPDATE_PLAYER - Keep The Faith Challenge Failed - padInput.x ",padInput.x," padInput.y ",padInput.y)
			sDGDefenderData.bKeepFaithAward = FALSE
		ENDIF
	ENDIF
	
	IF padInput.x != 0.0
		ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_TICKY_FLY_LOOP)
	ELSE
		ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_TICKY_FLY_LOOP)
	ENDIF
	
	sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x + padInput.x*DEGENATRON_GAMES_FRAME_TIME()/500,-1.0,1.0) // 2 seconds to go full accel
	sDGDefenderData.sPlayerData.vPlayerGameAcceleration.y = padInput.y
	

	IF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x <= 0.3 AND sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x > 0.0
		IF sDGDefenderData.sPlayerData.vPlayerDirection.x > 0.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x -= DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,0.0,1.0)
		ELSE
			sDGDefenderData.sPlayerData.vPlayerDirection.x = 0.0
		ENDIF
	ELIF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x >= -0.3 AND sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x < 0.0
		IF sDGDefenderData.sPlayerData.vPlayerDirection.x < 0.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x += DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,-1.0,0.0)
		ELSE
			sDGDefenderData.sPlayerData.vPlayerDirection.x = 0.0
		ENDIF
	ELIF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x <= 0.5 AND sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x > 0.3
		IF sDGDefenderData.sPlayerData.vPlayerDirection.x < 0.2
			sDGDefenderData.sPlayerData.vPlayerDirection.x += DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,-1.0,0.2)
		ELIF sDGDefenderData.sPlayerData.vPlayerDirection.x > 0.2
			sDGDefenderData.sPlayerData.vPlayerDirection.x -= DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,0.2,1.0)
		ELSE
			sDGDefenderData.sPlayerData.vPlayerDirection.x = 0.2
		ENDIF
	ELIF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x >= -0.5 AND sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x < -0.3
		IF sDGDefenderData.sPlayerData.vPlayerDirection.x > -0.2
			sDGDefenderData.sPlayerData.vPlayerDirection.x -= DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,-0.2,1.0)
		ELIF sDGDefenderData.sPlayerData.vPlayerDirection.x < -0.2
			sDGDefenderData.sPlayerData.vPlayerDirection.x += DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,-1.0,-0.2)
		ELSE
			sDGDefenderData.sPlayerData.vPlayerDirection.x = -0.2
		ENDIF
	ELIF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x > 0.5
		IF sDGDefenderData.sPlayerData.vPlayerDirection.x < 1.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x += DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,0.2,1.0)
		ELSE
			sDGDefenderData.sPlayerData.vPlayerDirection.x = 1.0
		ENDIF
	ELIF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x < -0.5
		IF sDGDefenderData.sPlayerData.vPlayerDirection.x > -1.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x -= DEGENATRON_GAMES_FRAME_TIME()/500.0
			sDGDefenderData.sPlayerData.vPlayerDirection.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerDirection.x,-1.0,-0.2)
		ELSE
			sDGDefenderData.sPlayerData.vPlayerDirection.x = -1.0
		ENDIF
	ENDIF
	sDGDefenderData.sPlayerData.vPlayerDirection.y = sDGDefenderData.sPlayerData.vPlayerGameAcceleration.y
	
	
	sDGDefenderData.sPlayerData.vPlayerGamePos.x += sDGDefenderData.sPlayerData.vPlayerDirection.x * ciDG_DEFENDER_PLAYER_SPEED*sDGDefenderData.sLevelData.fPlayerSpeedMultiplier * DEGENATRON_GAMES_FRAME_TIME()/1000.0
		sDGDefenderData.sPlayerData.vPlayerGamePos.x = sDGDefenderData.sPlayerData.vPlayerGamePos.x%DG_DEFENDER_GET_MAP_SIZE()
	IF sDGDefenderData.sPlayerData.vPlayerGamePos.x < 0
		sDGDefenderData.sPlayerData.vPlayerGamePos.x += DG_DEFENDER_GET_MAP_SIZE()
	ENDIF
	sDGDefenderData.sPlayerData.vPlayerGamePos.y += sDGDefenderData.sPlayerData.vPlayerDirection.y * ciDG_DEFENDER_PLAYER_SPEED * DEGENATRON_GAMES_FRAME_TIME()/1000.0
	sDGDefenderData.sPlayerData.vPlayerGamePos.y = CLAMP(sDGDefenderData.sPlayerData.vPlayerGamePos.y,0,DEGENATRON_GAMES_GET_SCREEN_HEIGHT())
	
	// UPDATE SCREEN POSITION //
	VECTOR_2D screenCenter = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	VECTOR_2D screenSize = DEGENATRON_GAMES_GET_SCREEN_SIZE()
	
	IF sDGDefenderData.sPlayerData.vPlayerGameAcceleration.x >= 0
		sDGDefenderData.sPlayerData.vPlayerScreenPosRate.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerScreenPosRate.x + cfDG_DEFENDER_PLAYER_SCREEN_RATE_SPEED*DEGENATRON_GAMES_FRAME_TIME()/500.0,-1.0,1.0)
	ELSE
		sDGDefenderData.sPlayerData.vPlayerScreenPosRate.x = CLAMP(sDGDefenderData.sPlayerData.vPlayerScreenPosRate.x - cfDG_DEFENDER_PLAYER_SCREEN_RATE_SPEED*DEGENATRON_GAMES_FRAME_TIME()/500.0,-1.0,1.0)
	ENDIF
	sDGDefenderData.sPlayerData.vPlayerScreenPosRate.y = CLAMP(sDGDefenderData.sPlayerData.vPlayerGamePos.y/DEGENATRON_GAMES_GET_SCREEN_HEIGHT(),0.0,1.0)
	
	sDGDefenderData.sPlayerData.vPlayerScreenPos.x = screenCenter.x - screenSize.x/2 + DEGENATRON_GAMES_GET_SCREEN_WIDTH()*(cfDG_DEFENDER_PLAYER_MIN_SCREEN_POS + EASE_IN_OUT_EXPO((sDGDefenderData.sPlayerData.vPlayerScreenPosRate.x+1)/2.0)*(CFDG_DEFENDER_PLAYER_MAX_SCREEN_POS-CFDG_DEFENDER_PLAYER_MIN_SCREEN_POS))
	sDGDefenderData.sPlayerData.vPlayerScreenPos.y = screenCenter.y + screenSize.y/2 - cfDG_DEFENDER_PLAYER_HEIGHT/2.0 - (DEGENATRON_GAMES_GET_SCREEN_HEIGHT()-cfDG_DEFENDER_PLAYER_HEIGHT-ciDEGENATRON_GAMES_SCORE_HEIGHT)*EASE_IN_OUT_SINE(sDGDefenderData.sPlayerData.vPlayerScreenPosRate.y)
	
ENDPROC

PROC DG_DEFENDER_UPDATE_ENEMY(INT iEnemy)
	VECTOR_2D enemySize = DG_DEFENDER_GET_ENEMY_SIZE()
	VECTOR_2D enemyPosition = DG_DEFENDER_GET_ENEMY_POSITION(iEnemy)
	VECTOR_2D newPosition
	newPosition = ADD_VECTOR_2D(enemyPosition, MULTIPLY_VECTOR_2D(NORMALISE_VECTOR_2D(sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection),(ciDG_DEFENDER_ENEMY_SPEED*DEGENATRON_GAMES_FRAME_TIME()/1000.0)))
	IF newPosition.y > DEGENATRON_GAMES_GET_SCREEN_HEIGHT() - enemySize.y/2 - ciDEGENATRON_GAMES_SCORE_HEIGHT
		sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection.y = -ABSF(sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection.y)
		newPosition = ADD_VECTOR_2D(enemyPosition, NORMALISE_VECTOR_2D(sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection))
	ELIF newPosition.y < 0.0 + enemySize.y/2
		sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection.y = ABSF(sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection.y)
		newPosition = ADD_VECTOR_2D(enemyPosition, NORMALISE_VECTOR_2D(sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection))
	ENDIF
	newPosition.x = newPosition.x%DG_DEFENDER_GET_MAP_SIZE()
	IF newPosition.x < 0
		newPosition.x += DG_DEFENDER_GET_MAP_SIZE()
	ENDIF
	sDGDefenderData.sEnemyData[iEnemy].vEnemyGamePos = newPosition

	// UPDATE SCREEN POSITION //
	VECTOR_2D screenCenter = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	VECTOR_2D screenSize = DEGENATRON_GAMES_GET_SCREEN_SIZE()

	sDGDefenderData.sEnemyData[iEnemy].vEnemyScreenPos.x = screenCenter.x - screenSize.x/2 + (enemyPosition.x + sDGDefenderData.sPlayerData.vPlayerGamePos.x)%DG_DEFENDER_GET_MAP_SIZE()
	sDGDefenderData.sEnemyData[iEnemy].vEnemyScreenPos.y = screenCenter.y + screenSize.y/2 - enemyPosition.y

ENDPROC

PROC DG_DEFENDER_UPDATE_OBJECTIVE(INT iObjective)
	VECTOR_2D objectiveSize = DG_DEFENDER_GET_OBJECTIVE_SIZE()
	VECTOR_2D objectivePosition = DG_DEFENDER_GET_OBJECTIVE_POSITION(iObjective)
	VECTOR_2D newPosition
	newPosition = ADD_VECTOR_2D(objectivePosition, MULTIPLY_VECTOR_2D(NORMALISE_VECTOR_2D(sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection),(cfDG_DEFENDER_OBJECTIVE_SPEED*DEGENATRON_GAMES_FRAME_TIME()/1000.0)))
	IF newPosition.y > DEGENATRON_GAMES_GET_SCREEN_HEIGHT() - objectiveSize.y/2 - ciDEGENATRON_GAMES_SCORE_HEIGHT
		sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection.y = -ABSF(sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection.y)
		newPosition = ADD_VECTOR_2D(objectivePosition, NORMALISE_VECTOR_2D(sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection))
	ELIF newPosition.y < 0.0 + objectiveSize.y/2
		sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection.y = ABSF(sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection.y)
		newPosition = ADD_VECTOR_2D(objectivePosition, NORMALISE_VECTOR_2D(sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection))
	ENDIF
	newPosition.x = newPosition.x%DG_DEFENDER_GET_MAP_SIZE()
	IF newPosition.x < 0
		newPosition.x += DG_DEFENDER_GET_MAP_SIZE()
	ENDIF
	sDGDefenderData.sObjectiveData[iObjective].vObjectiveGamePos = newPosition
	
	// UPDATE SCREEN POSITION //
	VECTOR_2D screenCenter = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	VECTOR_2D screenSize = DEGENATRON_GAMES_GET_SCREEN_SIZE()

	sDGDefenderData.sObjectiveData[iObjective].vObjectiveScreenPos.x = screenCenter.x - screenSize.x/2 + (objectivePosition.x + sDGDefenderData.sPlayerData.vPlayerGamePos.x)%DG_DEFENDER_GET_MAP_SIZE()
	sDGDefenderData.sObjectiveData[iObjective].vObjectiveScreenPos.y = screenCenter.y + screenSize.y/2 - objectivePosition.y
ENDPROC

PROC DG_DEFENDER_UPDATE_ENEMIES()
	INT iEnemy
	REPEAT sDGDefenderData.sLevelData.iEnemiesAmount iEnemy
		DG_DEFENDER_UPDATE_ENEMY(iEnemy)
	ENDREPEAT
ENDPROC

PROC DG_DEFENDER_UPDATE_OBJECTIVES()
	INT iObjective
	REPEAT sDGDefenderData.sLevelData.iObjectivesAmount iObjective
		IF DG_DEFENDER_GET_OBJECTIVE_IS_ACTIVE(iObjective)
			DG_DEFENDER_UPDATE_OBJECTIVE(iObjective)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DG_DEFENDER_UPDATE_LEVEL()
	INT iEnemy
	REPEAT sDGDefenderData.sLevelData.iEnemiesAmount iEnemy
		IF sDGDefenderData.sEnemyData[iEnemy].bCollision
			ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_EXPLODE)
			sDegenatronGamesData.sGameData.bPlayerIsDead = TRUE
			sDegenatronGamesData.bShouldDoHeavyShake = TRUE AND sDegenatronGamesData.iMachineBashed = 0
			sDegenatronGamesData.sGameData.iFlashTime = 0
			sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
			sDegenatronGamesData.sGameData.bFlashedTwice = FALSE
			EXIT
		ENDIF
	ENDREPEAT	
	INT iObjective
	REPEAT sDGDefenderData.sLevelData.iObjectivesAmount iObjective
		IF DG_DEFENDER_GET_OBJECTIVE_IS_ACTIVE(iObjective)
			IF sDGDefenderData.sObjectiveData[iObjective].bCollision
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_1POINT)
				sDGDefenderData.sObjectiveData[iObjective].bActive = FALSE
				sDegenatronGamesData.sGameData.iObjectives--
				sDegenatronGamesData.bShouldDoMidShake = TRUE AND sDegenatronGamesData.iMachineBashed = 0
				IF sDegenatronGamesData.sGameData.iObjectives = 0
					ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_GENERATE)
					sDGDefenderData.sLevelData.bObjectiveJustCaptured = TRUE
					sDegenatronGamesData.sGameData.iFlashTime = 0
					sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
					sDegenatronGamesData.sGameData.bFlashedTwice = FALSE
				ENDIF
				sDegenatronGamesData.sGameData.iScore += ciDG_DEFENDER_OBJECTIVE_SCORE
				sDegenatronGamesData.sTelemetry.kills++
			ENDIF
		ENDIF
	ENDREPEAT	
	
	IF sDGDefenderData.sLevelData.bObjectiveJustCaptured
		IF sDegenatronGamesData.sGameData.bFlashedOnce AND sDegenatronGamesData.sGameData.bFlashedTwice 
			sDegenatronGamesData.sGameData.iFlashTime = 0
			sDGDefenderData.sLevelData.bObjectiveJustCaptured = FALSE
			sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
			sDegenatronGamesData.sGameData.bFlashedTwice = FALSE
		ENDIF
	ENDIF
	
	sDGDefenderData.sLevelData.iScoreTimer -= DEGENATRON_GAMES_FRAME_TIME()
	IF sDGDefenderData.sLevelData.iScoreTimer < 0
		IF sDegenatronGamesData.sGameData.iScore > 0
			sDegenatronGamesData.sGameData.iScore--
		ENDIF
		sDGDefenderData.sLevelData.iScoreTimer += sDGDefenderData.sLevelData.iScoreTimerMax
	ENDIF
ENDPROC
PROC DG_DEFENDER_UPDATE_LEVEL_STATE()
	SWITCH sDGDefenderData.sLevelData.eLevelState
		CASE DG_DEFENDER_STATE_LEVEL_INIT
			sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_UPDATE
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_UPDATE
			IF sDegenatronGamesData.sGameData.bPlayerIsDead
				sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_DEAD
			ENDIF
			IF sDegenatronGamesData.sGameData.iObjectives = 0
				sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_COMPLETED
			ENDIF
#IF IS_DEBUG_BUILD
			IF sDGDefenderData.bWidgetSetLevel
				sDGDefenderData.bWidgetSetLevel = FALSE
				sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_INIT
			ENDIF
#ENDIF
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_COMPLETED
			sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_WAIT_TIME
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_DEAD
			sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_WAIT_TIME
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_WAIT_TIME
			IF sDegenatronGamesData.sGameData.iWaitTime > ciDEGENATRON_GAMES_WAIT_TIME_DEAD
				IF sDegenatronGamesData.sGameData.bPlayerIsDead
					IF sDegenatronGamesData.sGameData.bFlashedOnce AND sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME
						IF sDegenatronGamesData.sGameData.iLifes = 0
							sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_END
						ELSE
							sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_INIT
						ENDIF
					ENDIF
				ELSE
					sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_INIT
				ENDIF
			ENDIF
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_END
			sDGDefenderData.sLevelData.eLevelState = DG_DEFENDER_STATE_LEVEL_INIT
		BREAK
	ENDSWITCH

ENDPROC

//**************************************************//
//*       DEBUG INFO - DEFENDER OF THE FAITH       *//
//**************************************************//
#IF IS_DEBUG_BUILD
PROC __DG_DEFENDER_PRINT_DEBUG_LEVEL_INFO
	INT iLine = 0
	DEBUG_TEXT_INT_IN_SCREEN("iMapTiles ",sDGDefenderData.sLevelData.iMapTiles,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_INT_IN_SCREEN("iEnemiesAmount ",sDGDefenderData.sLevelData.iEnemiesAmount,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_INT_IN_SCREEN("iObjectivesAmount ",sDGDefenderData.sLevelData.iObjectivesAmount,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_STRING_IN_SCREEN("eLevelState ",DG_DEFENDER_STATE_LEVEL_TO_STRING(sDGDefenderData.sLevelData.eLevelState),0.16,0.05+0.01*iLine)
	iLine++
ENDPROC
PROC __DG_DEFENDER_PRINT_DEBUG_PLAYER_INFO
	INT iLine = 0
	DEBUG_TEXT_VECTOR_2D_IN_SCREEN("vPlayerGamePos ",sDGDefenderData.sPlayerData.vPlayerGamePos,0.31,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_VECTOR_2D_IN_SCREEN("vPlayerGameAcceleration ",sDGDefenderData.sPlayerData.vPlayerGameAcceleration,0.31,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_VECTOR_2D_IN_SCREEN("vPlayerDirection ",sDGDefenderData.sPlayerData.vPlayerDirection,0.31,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_VECTOR_2D_IN_SCREEN("vPlayerScreenPosRate ",sDGDefenderData.sPlayerData.vPlayerScreenPosRate,0.31,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_VECTOR_2D_IN_SCREEN("vPlayerScreenPos ",sDGDefenderData.sPlayerData.vPlayerScreenPos,0.31,0.05+0.01*iLine)
	iLine++
ENDPROC

PROC __DG_DEFENDER_PRINT_DEBUG_ENEMIES_INFO
	INT iEnemy
	INT iLine = 0
	REPEAT sDGDefenderData.sLevelData.iEnemiesAmount iEnemy
		TEXT_LABEL_63 output
		output = "enemy pos "
		output += iEnemy
		INT r = 255
		INT g = 255*BOOL_TO_INT(NOT sDGDefenderData.sEnemyData[iEnemy].bCollision)
		INT b = 255*BOOL_TO_INT(NOT sDGDefenderData.sEnemyData[iEnemy].bCollision)
		DEBUG_TEXT_VECTOR_2D_IN_SCREEN(output,sDGDefenderData.sEnemyData[iEnemy].vEnemyGamePos,0.46,0.05+0.01*iLine,r,g,b)
		iLine++
	ENDREPEAT
ENDPROC

PROC __DG_DEFENDER_PRINT_DEBUG_OBJECTIVES_INFO
	INT iObjective
	INT iLine = 0
	REPEAT sDGDefenderData.sLevelData.iObjectivesAmount iObjective
		TEXT_LABEL_63 output
		output = "objective "
		output += iObjective
		INT r = 255
		INT g = 255*BOOL_TO_INT(NOT sDGDefenderData.sObjectiveData[iObjective].bCollision)
		INT b = 255*BOOL_TO_INT(NOT sDGDefenderData.sObjectiveData[iObjective].bCollision)
		DEBUG_TEXT_VECTOR_2D_IN_SCREEN(output,sDGDefenderData.sObjectiveData[iObjective].vObjectiveGamePos,0.61,0.05+0.01*iLine,r,g,b)
		iLine++
	ENDREPEAT	
ENDPROC
#ENDIF

//**************************************************//
//*          INIT - DEFENDER OF THE FAITH          *//
//**************************************************//
PROC DG_DEFENDER_INIT_LEVEL(INT iLevel)
	IF iLevel > ciDG_DEFENDER_MAX_LEVEL
		iLevel = ciDG_DEFENDER_MAX_LEVEL
	ENDIF
	sDGDefenderData.sLevelData.iMapTiles = FLOOR(2+TO_FLOAT(iLevel)*0.5)
	sDGDefenderData.sLevelData.iEnemiesAmount = FLOOR(3+2*TO_FLOAT(iLevel)*1.5)
	sDGDefenderData.sLevelData.iObjectivesAmount = 2*iLevel
	sDGDefenderData.sLevelData.iScoreTimerMax = CLAMP_INT(sDGDefenderData.iScoreTimerDecrementMax-sDGDefenderData.iScoreTimerDecrementStep*iLevel,sDGDefenderData.iScoreTimerDecrementMin,sDGDefenderData.iScoreTimerDecrementMax)
	sDGDefenderData.sLevelData.fPlayerSpeedMultiplier = CLAMP(sDGDefenderData.fPlayerSpeedMultiplayerMin+sDGDefenderData.fPlayerSpeedMultiplayerStep*iLevel,sDGDefenderData.fPlayerSpeedMultiplayerMin,sDGDefenderData.fPlayerSpeedMultiplayerMax)
	sDegenatronGamesData.sGameData.iObjectives = sDGDefenderData.sLevelData.iObjectivesAmount
	sDegenatronGamesData.sGameData.iFlashTime = 0
	sDegenatronGamesData.sGameData.iWaitTime = 0
	sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
	sDegenatronGamesData.sGameData.bFlashedTwice = FALSE
	sDegenatronGamesData.sGameData.bPlayerIsDead = FALSE
	sDGDefenderData.sLevelData.bObjectiveJustCaptured = FALSE
	sDGDefenderData.bUpdateFirstFrameDone = FALSE
	sDGDefenderData.sLevelData.iScoreTimer = sDGDefenderData.sLevelData.iScoreTimerMax
	
	sDegenatronGamesData.sTelemetry.level = iLevel
ENDPROC
PROC DG_DEFENDER_INIT_PLAYER()
	sDGDefenderData.sPlayerData.vPlayerGamePos = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	sDGDefenderData.sPlayerData.vPlayerScreenPos = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	sDGDefenderData.sPlayerData.vPlayerScreenPosRate = INIT_VECTOR_2D(0.55,0.5)
	sDGDefenderData.sPlayerData.vPlayerGameAcceleration = INIT_VECTOR_2D(0.0,0.0)
	sDGDefenderData.sPlayerData.vPlayerDirection = INIT_VECTOR_2D(0.0,0.0)
	ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_ARP1)
ENDPROC
PROC DG_DEFENDER_INIT_ENEMIES()
	VECTOR_2D enemySize = DG_DEFENDER_GET_ENEMY_SIZE()
	INT iEnemy
	REPEAT sDGDefenderData.sLevelData.iEnemiesAmount iEnemy
		sDGDefenderData.sEnemyData[iEnemy].vEnemyGamePos = INIT_VECTOR_2D(GET_RANDOM_FLOAT_IN_RANGE(400,DG_DEFENDER_GET_MAP_SIZE()-400),GET_RANDOM_FLOAT_IN_RANGE(0,DEGENATRON_GAMES_GET_SCREEN_HEIGHT() - enemySize.y/2 - ciDEGENATRON_GAMES_SCORE_HEIGHT))
		sDGDefenderData.sEnemyData[iEnemy].vEnemyDirection = INIT_VECTOR_2D(GET_RANDOM_FLOAT_IN_RANGE(-1,1),GET_RANDOM_FLOAT_IN_RANGE(-1,1))
		sDGDefenderData.sEnemyData[iEnemy].bCollision = FALSE
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DG_DEFENDER_INIT_ENEMIES - iEnemy ",iEnemy," vEnemyGamePos ",sDGDefenderData.sEnemyData[iEnemy].vEnemyGamePos.x,",",sDGDefenderData.sEnemyData[iEnemy].vEnemyGamePos.y)
	ENDREPEAT
ENDPROC
PROC DG_DEFENDER_INIT_OBJECTIVES()
	VECTOR_2D objectiveSize = DG_DEFENDER_GET_OBJECTIVE_SIZE()
	INT iObjective
	REPEAT sDGDefenderData.sLevelData.iObjectivesAmount iObjective
		sDGDefenderData.sObjectiveData[iObjective].vObjectiveGamePos = INIT_VECTOR_2D(GET_RANDOM_FLOAT_IN_RANGE(400,DG_DEFENDER_GET_MAP_SIZE()-400),GET_RANDOM_FLOAT_IN_RANGE(0,DEGENATRON_GAMES_GET_SCREEN_HEIGHT() - objectiveSize.y/2 - ciDEGENATRON_GAMES_SCORE_HEIGHT))
		sDGDefenderData.sObjectiveData[iObjective].vObjectiveDirection = INIT_VECTOR_2D(GET_RANDOM_FLOAT_IN_RANGE(-1,1),GET_RANDOM_FLOAT_IN_RANGE(-1,1))
		sDGDefenderData.sObjectiveData[iObjective].bActive = TRUE
		sDGDefenderData.sObjectiveData[iObjective].bCollision = FALSE
		CDEBUG1LN(DEBUG_MINIGAME, "[DEGENATRON_GAMES] DG_DEFENDER_INIT_ENEMIES - iObjective ",iObjective," vObjectiveGamePos ",sDGDefenderData.sObjectiveData[iObjective].vObjectiveGamePos.x,",",sDGDefenderData.sObjectiveData[iObjective].vObjectiveGamePos.y)
	ENDREPEAT
ENDPROC
PROC DG_DEFENDER_INIT_BACKGROUND()
	sDGDefenderData.iRocksMap[0] = 0
	sDGDefenderData.iRocksMap[1] = 0
	sDGDefenderData.iRocksMap[2] = 0
ENDPROC

PROC DG_DEFENDER_LEVEL_INIT()
	DG_DEFENDER_INIT_LEVEL(sDegenatronGamesData.sGameData.iLevel)
	DG_DEFENDER_INIT_PLAYER()
	DG_DEFENDER_INIT_ENEMIES()
	DG_DEFENDER_INIT_OBJECTIVES()
	DG_DEFENDER_INIT_BACKGROUND()
ENDPROC

PROC DG_DEFENDER_LEVEL_UPDATE()
	DG_DEFENDER_UPDATE_PLAYER()
	DG_DEFENDER_UPDATE_ENEMIES()
	DG_DEFENDER_UPDATE_OBJECTIVES()
	DG_DEFENDER_UPDATE_COLLISIONS()
	DG_DEFENDER_UPDATE_LEVEL()
ENDPROC

PROC DG_DEFENDER_CLIENT_STATE_UPDATE()
	
	SWITCH sDGDefenderData.sLevelData.eLevelState
		CASE DG_DEFENDER_STATE_LEVEL_INIT
			DG_DEFENDER_LEVEL_INIT()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_UPDATE
			DG_DEFENDER_LEVEL_UPDATE()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_COMPLETED
			ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_TICKY_FLY_LOOP)
			sDegenatronGamesData.sGameData.iLevel++
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_LEVEL_COMPLETE)
			
			IF sDGDefenderData.bKeepFaithAward
				DEGENATRON_GAMES_SET_CHALLENGE_COMPLETED(DEGENATRON_GAMES_CHALLENGE_BIT_KEEP_THE_FAITH)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_DEGENATRON_RIGHT_KIND_OF_FAITH)
				sDGDefenderData.bKeepFaithAward = FALSE
			ENDIF
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_DEAD
			ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_TICKY_FLY_LOOP)
			sDegenatronGamesData.sGameData.iLifes--
			IF sDegenatronGamesData.sGameData.iLifes > 0
				DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_DEAD)
			ELSE
				ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_FAIL)
				DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_GAMEOVER)
			ENDIF
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_WAIT_TIME
			sDegenatronGamesData.sGameData.iWaitTime += DEGENATRON_GAMES_FRAME_TIME()
		BREAK
		CASE DG_DEFENDER_STATE_LEVEL_END
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
	ENDSWITCH
	
	sDegenatronGamesData.sTelemetry.score = sDegenatronGamesData.sGameData.iScore
	IF sDegenatronGamesData.sGameData.iScore >= g_sMPTunables.iCH_ARCADE_GAMES_DG_DEFENDER_GLITCH_SCORE AND sDegenatronGamesData.iMachineBashed != 0
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_DEGENATRON_PSYCHONAUT)
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_GLITCHED_DEGENATRON_TSHIRT, TRUE)
	ENDIF

	DG_DEFENDER_UPDATE_LEVEL_STATE()

	#IF IS_DEBUG_BUILD
	IF sDegenatronGamesData.bShowHelp
		__DG_DEFENDER_PRINT_DEBUG_LEVEL_INFO()
		__DG_DEFENDER_PRINT_DEBUG_PLAYER_INFO()
		__DG_DEFENDER_PRINT_DEBUG_ENEMIES_INFO()
		__DG_DEFENDER_PRINT_DEBUG_OBJECTIVES_INFO()
	ENDIF
	#ENDIF

ENDPROC

#IF IS_DEBUG_BUILD

PROC DG_DEFENDER_DEBUG_CREATE_WIDGETS
	START_WIDGET_GROUP("Defender of the Faith")
		ADD_WIDGET_INT_SLIDER("LEVEL",sDegenatronGamesData.sGameData.iLevel,0,ciDG_DEFENDER_MAX_LEVEL,1)
		ADD_WIDGET_BOOL("Set Level",sDGDefenderData.bWidgetSetLevel)
		ADD_WIDGET_INT_SLIDER("Max Time Decrement",sDGDefenderData.iScoreTimerDecrementMax,0,5000,1)
		ADD_WIDGET_INT_SLIDER("Min Time Decrement",sDGDefenderData.iScoreTimerDecrementMin,0,5000,1)
		ADD_WIDGET_INT_SLIDER("Step Time Decrement",sDGDefenderData.iScoreTimerDecrementStep,0,5000,1)
		ADD_WIDGET_FLOAT_SLIDER("Max Player Speed Multiplier",sDGDefenderData.fPlayerSpeedMultiplayerMax,0,5.0,0.1)
		ADD_WIDGET_FLOAT_SLIDER("Min Player Speed Multiplier",sDGDefenderData.fPlayerSpeedMultiplayerMin,0,5.0,0.1)
		ADD_WIDGET_FLOAT_SLIDER("Step Player Speed Multiplier",sDGDefenderData.fPlayerSpeedMultiplayerStep,0,5.0,0.01)
	STOP_WIDGET_GROUP()
ENDPROC

PROC DG_DEFENDER_DEBUG_UPDATE_WIDGETS
ENDPROC

#ENDIF
