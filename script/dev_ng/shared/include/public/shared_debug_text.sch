USING "commands_misc.sch"
USING "globals.sch"
USING "script_debug.sch"

/// PURPOSE:
///    Allows for debug printing to the command window
PROC displayDebugAssert(STRING text)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		SCRIPT_ASSERT(text)
	ENDIF
#endif
	DUMMY_REFERENCE_STRING(text)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
FUNC BOOL isDebugPrintingTurnedOn()
#IF IS_DEBUG_BUILD
	RETURN displayDebugDataInCommandWindow
#endif	
RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC switchDebugPrinting(BOOL turnOn)
#IF IS_DEBUG_BUILD
	displayDebugDataInCommandWindow = turnOn
#endif
	DUMMY_REFERENCE_BOOL(turnOn)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugString(STRING text)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTSTRING(text)
		PRINTLN()
	ENDIF
#endif
	DUMMY_REFERENCE_STRING(text)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugStrings(STRING text, STRING text2)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTSTRING(text)
		PRINTSTRING(text2)
	ENDIF
#endif
	DUMMY_REFERENCE_STRING(text)
	DUMMY_REFERENCE_STRING(text2)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugInt(int Number)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTINT(number)
	ENDIF
#endif	
	DUMMY_REFERENCE_INT(number)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugFloat(FLOAT Number)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTFLOAT(number)
	ENDIF
#endif	
	DUMMY_REFERENCE_FLOAT(number)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugStringAndInt(STRING text, INT number)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTSTRING(text)
		PRINTINT(number)
	ENDIF
#endif	
	DUMMY_REFERENCE_STRING(text)
	DUMMY_REFERENCE_INT(number)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugStringAndInts(STRING text, INT number, INT number2)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTSTRING(text)
		PRINTINT(number)
		PRINTSTRING(", ")
		PRINTINT(number2)
	ENDIF
#endif	
	DUMMY_REFERENCE_STRING(text)
	DUMMY_REFERENCE_INT(number)
	DUMMY_REFERENCE_INT(number2)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugStringAndFloat(STRING text, FLOAT number)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTSTRING(text)
		PRINTFLOAT(number)
	ENDIF
#endif	
	DUMMY_REFERENCE_STRING(text)
	DUMMY_REFERENCE_FLOAT(number)
ENDPROC

/// PURPOSE:
///    Allows for debug printing to the command window
PROC printDebugStringAndVector(STRING text, VECTOR number)
#IF IS_DEBUG_BUILD
	IF displayDebugDataInCommandWindow
		PRINTSTRING(text)
		PRINTFLOAT(number.x)
		PRINTSTRING(", ")
		PRINTFLOAT(number.y)
		PRINTSTRING(", ")
		PRINTFLOAT(number.z)
		PRINTSTRING(".")
	ENDIF
#endif	
	DUMMY_REFERENCE_STRING(text)
	DUMMY_REFERENCE_VECTOR(number)
ENDPROC
