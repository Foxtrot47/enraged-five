
// +-------------------------------------------------------------+
// +--------¦	   										¦--------+    
// +--------¦	   			Help At Location     		¦--------+    
// +--------¦	   	Header containing commands for 		¦--------+  
// +--------¦		displaying floating help text.		¦--------+    
// +--------¦	   										¦--------+    
// +-------------------------------------------------------------+
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_script.sch"
USING "script_maths.sch"

ENUM FLOATING_HELP_SLOT_TYPE
	FLOATING_HELP_MISSION_SLOT = 0,
	FLOATING_HELP_AMBIENT_SLOT_1,
	FLOATING_HELP_FIND_NEXT_FREE_SLOT
ENDENUM

CONST_INT FLOATING_HELP_PRINT_FOREVER					-1
CONST_INT FLOATING_HELP_FLAGS_HAS_BEEN_LOOKED_AT		0
CONST_INT FLOATING_HELP_FLAGS_PRINT_WITH_STRING			1
CONST_INT FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER			2
CONST_INT FLOATING_HELP_FLAGS_USE_2D_OFFSET_FROM_ENTITY	3

CONST_FLOAT FLOATING_HELP_2D_Z_VALUE				9999.0


/// PURPOSE:
///    Resets an element of the floating help text global array so that it's no longer processed.
/// PARAMS:
///    iFloatingHelpIndex - The array index of the help element.
PROC private_RESET_FLOATING_HELP_ELEMENT(INT iFloatingHelpIndex)
	g_sFloatingHelpData[iFloatingHelpIndex].i_timer = 0
	g_sFloatingHelpData[iFloatingHelpIndex].s_current_label = ""
	g_sFloatingHelpData[iFloatingHelpIndex].s_literal_label = ""
	g_sFloatingHelpData[iFloatingHelpIndex].entity = NULL
	g_sFloatingHelpData[iFloatingHelpIndex].v_pos = <<0.0, 0.0, 0.0>>
	g_sFloatingHelpData[iFloatingHelpIndex].i_flags = 0
	g_sFloatingHelpData[iFloatingHelpIndex].i_help_number = 0
	g_sFloatingHelpData[iFloatingHelpIndex].i_start_time = 0
ENDPROC

/// PURPOSE:
///    Checks if an element in the floating help array is being displayed.
/// PARAMS:
///    iFloatingHelpIndex - The array index of the help element.
/// RETURNS:
///    TRUE if the help is currently being displayed.
FUNC BOOL private_IS_THIS_FLOATING_HELP_BEING_DISPLAYED(INT iFloatingHelpIndex)
	eHELP_TEXT_FLOATING_ID eSlotID = INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, iFloatingHelpIndex)

	IF NOT ARE_STRINGS_EQUAL(g_sFloatingHelpData[iFloatingHelpIndex].s_current_label, "")
	AND NOT IS_STRING_NULL(g_sFloatingHelpData[iFloatingHelpIndex].s_current_label)
		IF IS_BIT_SET(g_sFloatingHelpData[iFloatingHelpIndex].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_STRING)
			IF IS_BIT_SET(g_sFloatingHelpData[iFloatingHelpIndex].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER)
				RETURN IS_THIS_FLOATING_HELP_TEXT_WITH_STRING_AND_NUM_BEING_DISPLAYED(eSlotID, g_sFloatingHelpData[iFloatingHelpIndex].s_current_label, 
																				   		 g_sFloatingHelpData[iFloatingHelpIndex].s_literal_label, 
																				   		 g_sFloatingHelpData[iFloatingHelpIndex].i_help_number)
			ELSE
				RETURN IS_THIS_FLOATING_HELP_TEXT_WITH_STRING_BEING_DISPLAYED(eSlotID, g_sFloatingHelpData[iFloatingHelpIndex].s_current_label, 
																			  g_sFloatingHelpData[iFloatingHelpIndex].s_literal_label)
			ENDIF
		ELIF IS_BIT_SET(g_sFloatingHelpData[iFloatingHelpIndex].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER)
			RETURN IS_THIS_FLOATING_HELP_TEXT_WITH_NUMBER_BEING_DISPLAYED(eSlotID, g_sFloatingHelpData[iFloatingHelpIndex].s_current_label,
																		  g_sFloatingHelpData[iFloatingHelpIndex].i_help_number)
		ELSE
			RETURN IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(eSlotID, g_sFloatingHelpData[iFloatingHelpIndex].s_current_label)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a given label is being displayed as floating help in any slot.
/// PARAMS:
///    strLabel - The label
/// RETURNS:
///    TRUE if the label is currently bein
FUNC BOOL IS_THIS_FLOATING_HELP_BEING_DISPLAYED(STRING strLabel)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		//Check each array element, also just check the label directly in case there's some inconsistent stored data.
		IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel)
		OR (ARE_STRINGS_EQUAL(strLabel, g_sFloatingHelpData[i].s_current_label) AND private_IS_THIS_FLOATING_HELP_BEING_DISPLAYED(i))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_FLOATING_HELP_WITH_NUMBER_BEING_DISPLAYED_IN_ANY_SLOT(STRING strLabel, INT iNumber)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF IS_THIS_FLOATING_HELP_TEXT_WITH_NUMBER_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel, iNumber)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_FLOATING_HELP_WITH_STRING_BEING_DISPLAYED_IN_ANY_SLOT(STRING strLabel, STRING strLiteralString)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF IS_THIS_FLOATING_HELP_TEXT_WITH_STRING_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel, strLiteralString)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_FLOATING_HELP_WITH_NUMBER_AND_STRING_BEING_DISPLAYED_IN_ANY_SLOT(STRING strLabel, INT iNumber, STRING strLiteralString)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF IS_THIS_FLOATING_HELP_TEXT_WITH_STRING_AND_NUM_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel, strLiteralString, iNumber)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if any floating help is being displayed via the floating help script.
///    NOTE: this only works for help text printed using the help_at_location header, it cannot detect help text displayed by other means.
/// RETURNS:
///    TRUE if any of the slots are currently being displayed.
FUNC BOOL IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF private_IS_THIS_FLOATING_HELP_BEING_DISPLAYED(i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears all floating help slots, and flushes all data stored in the floating help system.
///    bClearNow - If TRUE the text is cleared instantly without a fade.
PROC CLEAR_ALL_FLOATING_HELP(BOOL bClearNow = TRUE)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		CLEAR_FLOATING_HELP(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), bClearNow)
		private_RESET_FLOATING_HELP_ELEMENT(i)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		PRINTLN("help_at_location.sch: CLEAR_ALL_FLOATING_HELP called this frame by ", GET_THIS_SCRIPT_NAME())
	#ENDIF
ENDPROC

/// PURPOSE:
///    Clears a specific floating help print, will also clear any associated data stored in the floating help system.
/// PARAMS:
///    strLabel - The label to clear.
///    bClearNow - If TRUE the text is cleared instantly without a fade.
PROC CLEAR_THIS_FLOATING_HELP(STRING strLabel, BOOL bClearNow = TRUE)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(g_sFloatingHelpData[i].s_current_label, strLabel)
		OR IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel)
			CLEAR_FLOATING_HELP(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), bClearNow)
			private_RESET_FLOATING_HELP_ELEMENT(i)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("help_at_location.sch: CLEAR_THIS_FLOATING_HELP called by ", GET_THIS_SCRIPT_NAME(), ", successfully cleared help label ", strLabel, " found in slot ", i)
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAR_THIS_FLOATING_HELP_WITH_NUMBER(STRING strLabel, INT iNumber, BOOL bClearNow = TRUE)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(g_sFloatingHelpData[i].s_current_label, strLabel)
		OR IS_THIS_FLOATING_HELP_TEXT_WITH_NUMBER_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel, iNumber)
			CLEAR_FLOATING_HELP(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), bClearNow)
			private_RESET_FLOATING_HELP_ELEMENT(i)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("help_at_location.sch: CLEAR_THIS_FLOATING_HELP_WITH_NUMBER called by ", GET_THIS_SCRIPT_NAME(), ", successfully cleared help label ", strLabel, " found in slot ", i)
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAR_THIS_FLOATING_HELP_WITH_STRING(STRING strLabel, STRING strLiteralString, BOOL bClearNow = TRUE)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(g_sFloatingHelpData[i].s_current_label, strLabel)
		OR IS_THIS_FLOATING_HELP_TEXT_WITH_STRING_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel, strLiteralString)
			CLEAR_FLOATING_HELP(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), bClearNow)
			private_RESET_FLOATING_HELP_ELEMENT(i)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("help_at_location.sch: CLEAR_THIS_FLOATING_HELP_WITH_STRING called by ", GET_THIS_SCRIPT_NAME(), ", successfully cleared help label ", strLabel, " found in slot ", i)
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAR_THIS_FLOATING_HELP_WITH_NUMBER_AND_STRING(STRING strLabel, INT iNumber, STRING strLiteralString, BOOL bClearNow = TRUE)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(g_sFloatingHelpData[i].s_current_label, strLabel)
		OR IS_THIS_FLOATING_HELP_TEXT_WITH_STRING_AND_NUM_BEING_DISPLAYED(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), strLabel, strLiteralString, iNumber)
			CLEAR_FLOATING_HELP(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i), bClearNow)
			private_RESET_FLOATING_HELP_ELEMENT(i)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("help_at_location.sch: CLEAR_THIS_FLOATING_HELP_WITH_NUMBER_AND_STRING called by ", GET_THIS_SCRIPT_NAME(), 
						", successfully cleared help label ", strLabel, " found in slot ", i)
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Private function: finds a floating help slot that isn't currently being used. 
FUNC INT private_GET_NEXT_FREE_FLOATING_HELP_SLOT()
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF g_sFloatingHelpData[i].i_timer = 0
			RETURN i
		ENDIF
	ENDREPEAT
	
	//#IF IS_DEBUG_BUILD
	//	SCRIPT_ASSERT("help_at_location.sch - There are no free slots left for floating help")
	//#ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Private function: prints floating help of different forms depending on which public HELP_AT_LOCATION function was used.   
FUNC INT private_PRINT_FLOATING_HELP(STRING sLabel, STRING sLiteral, INT iNumber, INT iTime, HELP_MESSAGE_STYLE eStyle, VECTOR vPos, ENTITY_INDEX entity = NULL, 
								 	 eARROW_DIRECTION eDirection = 0, BOOL bPrintWithLiteral = FALSE, BOOL bPrintWithNumber = FALSE, 
									 FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK, 
									 BOOL bEntityOffsetIs2D = FALSE, INT iArrowOffset = 0 #IF IS_DEBUG_BUILD, BOOL bAddPlayerName = FALSE #ENDIF)
	INT iSlot 
	IF eSlot = FLOATING_HELP_FIND_NEXT_FREE_SLOT
		iSlot = private_GET_NEXT_FREE_FLOATING_HELP_SLOT()
	ELSE
		iSlot = ENUM_TO_INT(eSlot)
	ENDIF
	
	IF iSlot != -1
		eHELP_TEXT_FLOATING_ID eSlotID = INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, iSlot)
		
		//Initialise the data
		IF iTime = -1
			g_sFloatingHelpData[iSlot].i_timer = -1
		ELSE
			g_sFloatingHelpData[iSlot].i_timer = GET_GAME_TIMER() + iTime
		ENDIF
		
		g_sFloatingHelpData[iSlot].s_current_label = sLabel
		g_sFloatingHelpData[iSlot].s_literal_label = sLiteral
		g_sFloatingHelpData[iSlot].i_help_number = iNumber
		g_sFloatingHelpData[iSlot].v_pos = vPos
		g_sFloatingHelpData[iSlot].entity = entity
		g_sFloatingHelpData[iSlot].i_flags = 0
		g_sFloatingHelpData[iSlot].i_start_time = GET_GAME_TIMER()
		
		//Print the message: Add different text components depending on params.
		BEGIN_TEXT_COMMAND_DISPLAY_HELP(sLabel)
			IF bPrintWithLiteral
				#IF IS_DEBUG_BUILD
				IF bAddPlayerName = FALSE
				#ENDIF
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sLiteral)
				#IF IS_DEBUG_BUILD
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sLiteral)
				ENDIF
				#ENDIF
			ENDIF
		
			IF bPrintWithNumber
				ADD_TEXT_COMPONENT_INTEGER(iNumber)
			ENDIF
		END_TEXT_COMMAND_DISPLAY_HELP(INT_TO_ENUM(eHelpMessageId, 1 + ENUM_TO_INT(eSlotID)), TRUE, FALSE)
		
		//Setup any required flags
		IF bPrintWithLiteral
			SET_BIT(g_sFloatingHelpData[iSlot].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_STRING)
		
			IF bPrintWithNumber
				SET_BIT(g_sFloatingHelpData[iSlot].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER)
			ENDIF
		ELIF bPrintWithNumber
			SET_BIT(g_sFloatingHelpData[iSlot].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER)
		ENDIF
		
		IF bEntityOffsetIs2D
			SET_BIT(g_sFloatingHelpData[iSlot].i_flags, FLOATING_HELP_FLAGS_USE_2D_OFFSET_FROM_ENTITY)
		ENDIF
		
		//Set the message style
		IF eStyle = HELP_MESSAGE_STYLE_NORMAL
			SET_FLOATING_HELP_TEXT_STYLE(eSlotID, HELP_MESSAGE_STYLE_NORMAL, eBackgroundColour, 191)
		ELSE
			SET_FLOATING_HELP_TEXT_STYLE(eSlotID, eStyle, eBackgroundColour, 191, eDirection, iArrowOffset)
		ENDIF

		//Set the position depending on options chosen.
		IF g_sFloatingHelpData[iSlot].v_pos.z != FLOATING_HELP_2D_Z_VALUE
			IF g_sFloatingHelpData[iSlot].entity != NULL
				IF NOT IS_ENTITY_DEAD(g_sFloatingHelpData[iSlot].entity)
					IF NOT IS_BIT_SET(g_sFloatingHelpData[iSlot].i_flags, FLOATING_HELP_FLAGS_USE_2D_OFFSET_FROM_ENTITY)
						SET_FLOATING_HELP_TEXT_WORLD_POSITION(eSlotID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sFloatingHelpData[iSlot].entity, g_sFloatingHelpData[iSlot].v_pos))
					ELSE
						SET_FLOATING_HELP_TEXT_TO_ENTITY(eSlotID, g_sFloatingHelpData[iSlot].entity, g_sFloatingHelpData[iSlot].v_pos.x, g_sFloatingHelpData[iSlot].v_pos.y)
					ENDIF
				ENDIF
			ELIF (g_sFloatingHelpData[iSlot].v_pos.x != 0.0 OR g_sFloatingHelpData[iSlot].v_pos.y != 0.0 OR g_sFloatingHelpData[iSlot].v_pos.z != 0.0)
				SET_FLOATING_HELP_TEXT_WORLD_POSITION(eSlotID, g_sFloatingHelpData[iSlot].v_pos)
			ENDIF
		ELSE
			SET_FLOATING_HELP_TEXT_SCREEN_POSITION(eSlotID, g_sFloatingHelpData[iSlot].v_pos.x, g_sFloatingHelpData[iSlot].v_pos.y)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("help_at_location.sch: Help label ", sLabel, " printed this frame in slot ", iSlot, ", called by: ", GET_THIS_SCRIPT_NAME())
		#ENDIF
	ENDIF
	
	RETURN iSlot
ENDFUNC

// +--------------------------¦	 Commands for displaying the help text.   ¦----------------------------+ 

/// PURPOSE:
///    Prints help text at the given world coords.
/// PARAMS:
///    sPrint - The text to print.
///    vLocation - The world coords to print the text at.
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
PROC HELP_AT_LOCATION(STRING sPrint, VECTOR vLocation, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
					  FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK, 
					  HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_TAGGABLE, INT iArrowOffset = 0)
	private_PRINT_FLOATING_HELP(sPrint, "", 0, iDuration, eStyle, vLocation, NULL, eArrow, FALSE, FALSE, eSlot, eBackgroundColour, FALSE, iArrowOffset)
ENDPROC

/// PURPOSE:
///    Prints help text at the given screen location.
/// PARAMS:
///    sPrint - The text to print.
///    fX - The horizontal screen position (between 0 and 1).
///    fY - The vertical screen position (between 0 and 1).
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
PROC HELP_AT_SCREEN_LOCATION(STRING sPrint, FLOAT fX, FLOAT fY, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
							 FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK,
							 HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_TAGGABLE, INT iArrowOffset = 0)
	private_PRINT_FLOATING_HELP(sPrint, "", 0, iDuration, eStyle, <<fX, fY, FLOATING_HELP_2D_Z_VALUE>>, 
								NULL, eArrow, FALSE, FALSE, eSlot, eBackgroundColour, FALSE, iArrowOffset)
ENDPROC


/// PURPOSE:
///    Prints help relative to the given entity, the offset is calculated based on the entity model dimensions.
/// PARAMS:
///    sPrint - The text to print.
///    entity - The entity to attach the print to.
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
///    fZOffset - adds a z-offset from the entity in addition to the calculated offset.
PROC HELP_AT_ENTITY(STRING sPrint, ENTITY_INDEX entity, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
					FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK,
					HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_TAGGABLE, FLOAT fZOffset = 0.0, INT iArrowOffset = 0)
	VECTOR vMin, vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(entity), vMin, vMax)
	
	private_PRINT_FLOATING_HELP(sPrint, "", 0, iDuration, eStyle, <<0.0, 0.0, (((vMax.Z - vMin.Z) / 2) - 0.1) + fZOffset>>, 
								entity, eArrow, FALSE, FALSE, eSlot, eBackgroundColour, FALSE, iArrowOffset)
ENDPROC

 
/// PURPOSE:
///    Prints help relative to the given entity, at a given vector offset.
/// PARAMS:
///    sPrint - The text to print.
///    entity - The entity to attach the print to.
///    vOffset - The offset from the entity
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
PROC HELP_AT_ENTITY_OFFSET(STRING sPrint, ENTITY_INDEX entity, VECTOR vOffset, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
						   FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK,
						   HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_TAGGABLE, INT iArrowOffset = 0)

	private_PRINT_FLOATING_HELP(sPrint, "", 0, iDuration, eStyle, vOffset, entity, eArrow, FALSE, FALSE, eSlot, eBackgroundColour, FALSE, iArrowOffset)
ENDPROC


/// PURPOSE:
///    Prints help relative to the given entity with a given 2D screen offset applied. This can be useful if you need the text to remain at a constant screen
///    offset from the entity regardless of how far away the entity is.
/// PARAMS:
///    sPrint - The text to print.
///    entity - The entity to attach the print to.
///    fX - The horizontal screen offset (between 0 and 1).
///    fY - The vertical screen offset (between 0 and 1).
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
PROC HELP_AT_ENTITY_2D_OFFSET(STRING sPrint, ENTITY_INDEX entity, FLOAT fX, FLOAT fY, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
						   FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK,
						   HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_TAGGABLE, INT iArrowOffset = 0)

	private_PRINT_FLOATING_HELP(sPrint, "", 0, iDuration, eStyle, <<fX, fY, 0.0>>, entity, eArrow, FALSE, FALSE, eSlot, eBackgroundColour, TRUE, iArrowOffset)
ENDPROC


/// PURPOSE:
///    Prints help with a literal string at a given location/entity offset.
/// PARAMS:
///    sPrint - The text to print.
///    sString - The literal string to append.
///    vLocation - If bIs3D is TRUE and no entity is specified this gives the world coords for the text.
///    			 - If bIs3D is TRUE and an entity is specified this gives the offset from the entity.
///    			 - If bIs3D is FALSE this gives a 2D screen position (x and y values must be between 0 and 1).
///    entity - If not NULL specifies an entity to attach the help to.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    bIs3D - Indicates if this help will be printed using world coords or 2D screen coords.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    bUse2DOffsetFromEntity - If TRUE and an entity is specified, vLocation is converted into a 2D screen offset from the entity (similar to HELP_AT_ENTITY_2D_OFFSET).
PROC HELP_AT_LOCATION_WITH_STRING(STRING sPrint,  STRING sString, VECTOR vLocation, ENTITY_INDEX entity = NULL, HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_NORMAL, 
								  eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, BOOL bIs3D = TRUE, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
								  FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK, BOOL bUse2DOffsetFromEntity = FALSE,
								  INT iArrowOffset = 0)
	IF bIs3D
		private_PRINT_FLOATING_HELP(sPrint, sString, 0, iDuration, eStyle, vLocation, entity, eArrow, TRUE, FALSE, eSlot, eBackgroundColour, bUse2DOffsetFromEntity, iArrowOffset)
	ELSE
		private_PRINT_FLOATING_HELP(sPrint, sString, 0, iDuration, eStyle, <<vLocation.x, vLocation.y, FLOATING_HELP_2D_Z_VALUE>>, 
									entity, eArrow, TRUE, FALSE, eSlot, eBackgroundColour, bUse2DOffsetFromEntity, iArrowOffset)
	ENDIF
ENDPROC


/// PURPOSE:
///    Prints help with a number at a given location/entity offset.
/// PARAMS:
///    sPrint - The text to print.
///    iNumber - The number to append.
///    vLocation - If bIs3D is TRUE and no entity is specified this gives the world coords for the text.
///    			 - If bIs3D is TRUE and an entity is specified this gives the offset from the entity.
///    			 - If bIs3D is FALSE this gives a 2D screen position (x and y values must be between 0 and 1).
///    entity - If not NULL specifies an entity to attach the help to.
///    eStyle - Defines whether the help text has an arrow or not: HELP_MESSAGE_STYLE_TAGGABLE will have an arrow, HELP_MESSAGE_STYLE_NORMAL will be just a box.
///    eArrow - If using the HELP_MESSAGE_STYLE_TAGGABLE style this indicates where the arrow will point.
///    bIs3D - Indicates if this help will be printed using world coords or 2D screen coords.
///    iDuration - The length of time (in milliseconds) to display the text. If set to -1 the text is printed forever.
///    eSlot - There are 3 slots available for floating help: If eSlot = FLOATING_HELP_FIND_FREE_SLOT, the command will attempt to find a help slot with no text displaying, 
///    		   otherwise the help will overwrite any currently displaying help in the given slot.
///    eBackgroundColour - The background colour, this should be kept as black except in special cases.
///    bUse2DOffsetFromEntity - If TRUE and an entity is specified, vLocation is converted into a 2D screen offset from the entity (similar to HELP_AT_ENTITY_2D_OFFSET).
PROC HELP_AT_LOCATION_WITH_NUMBER(STRING sPrint,  INT iNumber, VECTOR vLocation, ENTITY_INDEX entity = NULL, HELP_MESSAGE_STYLE eStyle = HELP_MESSAGE_STYLE_NORMAL, 
								  eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, BOOL bIs3D = TRUE, INT iDuration = DEFAULT_HELP_TEXT_TIME, 
								  FLOATING_HELP_SLOT_TYPE eSlot = FLOATING_HELP_MISSION_SLOT, HUD_COLOURS eBackgroundColour = HUD_COLOUR_BLACK, BOOL bUse2DOffsetFromEntity = FALSE,
								  INT iArrowOffset = 0)
	IF bIs3D
		private_PRINT_FLOATING_HELP(sPrint, "", iNumber, iDuration, eStyle, vLocation, entity, eArrow, FALSE, TRUE, eSlot, eBackgroundColour, bUse2DOffsetFromEntity, iArrowOffset)
	ELSE
		private_PRINT_FLOATING_HELP(sPrint, "", iNumber, iDuration, eStyle, <<vLocation.x, vLocation.y, FLOATING_HELP_2D_Z_VALUE>>, 
									entity, eArrow, FALSE, TRUE, eSlot, eBackgroundColour, bUse2DOffsetFromEntity, iArrowOffset)
	ENDIF
ENDPROC


/// PURPOSE:
///    Changes the world position of the given help text label (if it's already being displayed)
/// PARAMS:
///    strLabel - The help text label.
///    vLocation - The new world position.
PROC UPDATE_FLOATING_HELP_WORLD_POSITION(STRING strLabel, VECTOR vLocation)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(strLabel, g_sFloatingHelpData[i].s_current_label)
			g_sFloatingHelpData[i].v_pos = vLocation
			g_sFloatingHelpData[i].entity = NULL
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Changes the screen position of the given help text label (if it's already being displayed)
/// PARAMS:
///    strLabel - The help text label.
///    fX - The new horizontal screen position.
///    fY - The new vertical screen position.
PROC UPDATE_FLOATING_HELP_SCREEN_POSITION(STRING strLabel, FLOAT fX, FLOAT fY)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(strLabel, g_sFloatingHelpData[i].s_current_label)
			g_sFloatingHelpData[i].v_pos = <<fX, fY, FLOATING_HELP_2D_Z_VALUE>>
			g_sFloatingHelpData[i].entity = NULL
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Changes the offset of the given help text label relative to the given entity (if it's already being displayed)
/// PARAMS:
///    strLabel - The help text label.
///    entity - The entity to display relative to.
///    vOffset - The new offset from the entity.
PROC UPDATE_FLOATING_HELP_ENTITY_OFFSET(STRING strLabel, ENTITY_INDEX entity, VECTOR vOffset)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(strLabel, g_sFloatingHelpData[i].s_current_label)
			g_sFloatingHelpData[i].v_pos = vOffset
			g_sFloatingHelpData[i].entity = entity
			CLEAR_BIT(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_USE_2D_OFFSET_FROM_ENTITY)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Changes the screen offset of the given help text label relative to the given entity (if it's already being displayed)
/// PARAMS:
///    strLabel - The help text label.
///    entity - The entity to display relative to.
///    fX - The new horizontal screen offset (between 0 and 1).
///    fY - The new vertical screen offset (between 0 and 1).
PROC UPDATE_FLOATING_HELP_2D_ENTITY_OFFSET(STRING strLabel, ENTITY_INDEX entity, FLOAT fX, FLOAT fY)
	INT i = 0
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF ARE_STRINGS_EQUAL(strLabel, g_sFloatingHelpData[i].s_current_label)
			g_sFloatingHelpData[i].v_pos = <<fX, fY, 0.0>>
			g_sFloatingHelpData[i].entity = entity
			SET_BIT(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_USE_2D_OFFSET_FROM_ENTITY)
		ENDIF
	ENDREPEAT
ENDPROC


