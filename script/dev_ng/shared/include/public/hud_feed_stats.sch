USING "globals.sch"
USING "rage_builtins.sch"

// Header support for HUD_FEED_STATS.gfx

/// PURPOSE: Displays the stats feed image and localised string in the top left of the HUD
/// Make sure you have loaded the movie with REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE, 
/// and checked it has loaded with HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED before using this.
/// PARAMS:
///    sHUDFeedStats - Scaleform movie ID for the stats UI
///    sStatsText - Stats Body string of the Stats Message
///    sTXD - The Texture Dictionary for the image used in the Stats Message
///    sImageName - The Image name string for the image used in the Stats Message
PROC Set_Feed_Stats(eSCRIPT_HUD_COMPONENT sHUDFeedStats, STRING sStatsText, STRING sTXD, STRING sImageName)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedStats, "SET_FEED_STATS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sStatsText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sImageName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Removes stats component from screen. Be sure to remove the scaleform movie when it is no longer required
///    
/// PARAMS:
///    sHUDFeedStats - Scaleform movie ID for the stats UI
PROC Remove_Feed_Stats(eSCRIPT_HUD_COMPONENT sHUDFeedStats)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedStats, "REMOVE_FEED_STATS")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC
