USING "wizard_arcade_using.sch"

FUNC BOOL GET_UV_FOR_BACKGROUND_TILE(TWS_BACKGROUND_TILE_TYPE eType, VECTOR_2D &vU,VECTOR_2D &vV)
	
	SWITCH eType
		CASE TWS_BG_TL_1_INTRO
		CASE TWS_BG_TL_1_BASE
		CASE TWS_BG_TL_1_CAVE
		CASE TWS_BG_TL_1_CLIFF
		CASE TWS_BG_TL_1_SPIDER
		CASE TWS_BG_TL_2_INTRO
		CASE TWS_BG_TL_2_BIG_TREE
		CASE TWS_BG_TL_2_TREES
		CASE TWS_BG_TL_2_DEAD_BODIES
		CASE TWS_BG_TL_3_INTRO
		CASE TWS_BG_TL_3_BIG_WINDOW
		CASE TWS_BG_TL_3_WINDOWS_OPEN
		CASE TWS_BG_TL_3_STATUES
		CASE TWS_BG_TL_3_DOOR_STAIRS
		CASE TWS_BG_TL_3_OUTRO
		CASE TWS_BG_TL_4_INTRO
		CASE TWS_BG_TL_4_INTRO_NPC
		CASE TWS_BG_TL_4_BIG_TREE
		CASE TWS_BG_TL_4_BIG_MUSHROOM
		CASE TWS_BG_TL_4_OPEN_BACK
		CASE TWS_BG_TL_4_TREES

			vU.x = 0.0015625
			vU.y = 0.0
			vV.x = 1.0-vU.x 
			vV.y = 1.0
			RETURN TRUE

		CASE TWS_BG_TL_1_GEN_1
		CASE TWS_BG_TL_1_GEN_2
		CASE TWS_BG_TL_2_GEN_1
		CASE TWS_BG_TL_2_GEN_2
		CASE TWS_BG_TL_2_GEN_3
		CASE TWS_BG_TL_3_GEN_1
		CASE TWS_BG_TL_3_GEN_2
		CASE TWS_BG_TL_4_GEN_1
		CASE TWS_BG_TL_4_GEN_2
		CASE TWS_BG_TL_4_GEN_3
		
			vU.x = 0.003125
			vU.y = 0.0
			vV.x = 1.0-vU.x 
			vV.y = 1.0
			RETURN TRUE

		CASE TWS_BG_TL_1_BOSS
		CASE TWS_BG_TL_2_BOSS
		CASE TWS_BG_TL_4_BOSS
		CASE TWS_BG_TL_3_FINAL
		
			RETURN FALSE

	ENDSWITCH	
	RETURN FALSE
ENDFUNC


PROC TWS_DRAW_BACKGROUND()
	
	INT i
	FLOAT fTilePosX, fShake = 0.0
	TEXT_LABEL_31 tl23BackgroundSprite
	TEXT_LABEL_31 tl23BackgroundDictionary
	
	SWITCH sTWSData.eCurrentLevel
		CASE 	TWS_FOREST_INTRO 
		CASE 	TWS_FOREST 
		CASE 	TWS_SWAMP 
			tl23BackgroundDictionary = "MPWizardsSleeveBackgrounds12"
		BREAK
		CASE 	TWS_CAVES
		CASE 	TWS_CASTLE
		CASE 	TWS_CASTLE_OUTRO 
			tl23BackgroundDictionary = "MPWizardsSleeveBackgrounds34"
		BREAK
	ENDSWITCH 
	VECTOR_2D vU
	VECTOR_2D vV
	
	// Background
	SWITCH sTWSData.eCurrentLevel
		
		CASE TWS_FOREST_INTRO
		
			tl23BackgroundSprite = "forest_start_npc"
			ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)

		BREAK
		CASE TWS_FOREST
		CASE TWS_SWAMP
		CASE TWS_CAVES
		
			// Midground
			FOR i = 0 TO iCurrentFarBackgroundTileToAdd - 1
				
				SWITCH sFarBackgroundTilesData[i].eType
				CASE TWS_FBG_TL_FOREST_1
					tl23BackgroundSprite = "forest_bg_tile1"
				BREAK
				CASE TWS_FBG_TL_FOREST_2
					tl23BackgroundSprite = "forest_bg_tile2 - castle"
				BREAK
				CASE TWS_FBG_TL_SWAMP
					tl23BackgroundSprite = "swamp_bg_tile"
				BREAK
				CASE TWS_FBG_TL_CAVES
					tl23BackgroundSprite = "cave_bg_tile"
				BREAK
				ENDSWITCH
				
				FLOAT fPlayerX
				IF (sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR (sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0)) AND sTWSData.eCurrentLevel != TWS_FOREST_INTRO
				
					fPlayerX = sFarBackgroundTilesData[i].fSpritePos + TWS_GET_OFFSET_TO_PLAYER(0.5, 0.0, cfBASE_SCREEN_WIDTH/4)

				ELIF sPlayerData.ePlayerState = TWS_INTRO OR sPlayerData.vSpritePos.x <= cfBASE_SCREEN_WIDTH/2 OR sTWSData.eCurrentLevel = TWS_FOREST_INTRO
					fPlayerX = sFarBackgroundTilesData[i].fSpritePos
				
				ELIF sTWSData.bLockedScreen
				
					fPlayerX = sFarBackgroundTilesData[i].fSpritePos + TWS_GET_OFFSET_TO_PLAYER(0.5, sTWSData.fLockedScreenPos, DEFAULT, DEFAULT,- cfBASE_SCREEN_WIDTH/4)
					
				ELSE
					fPlayerX = sFarBackgroundTilesData[i].fSpritePos + TWS_GET_OFFSET_TO_PLAYER(0.5, DEFAULT, DEFAULT, DEFAULT, - cfBASE_SCREEN_WIDTH/4)
				ENDIF
				
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(fPlayerX, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)

			ENDFOR
			
		BREAK
		CASE TWS_CASTLE
		CASE TWS_CASTLE_OUTRO
		
			IF sEnemyData[sTWSData.iBossEnemyIndex].eEnemyState = TWS_ENEMY_EXPLODING
			OR sEnemyData[sTWSData.iBossEnemyIndex].eEnemyState = TWS_ENEMY_DYING
			
				sTWSData.iAnimatedBackgroundSpriteAnim = (sTWSData.iAnimatedBackgroundSpriteAnim + sTWSData.iGlobalDefaultUpdateFrames) % 2
				
				tl23BackgroundSprite = "wizard_dying_bg_0000"
				tl23BackgroundSprite += sTWSData.iAnimatedBackgroundSpriteAnim
				
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
			
			ELIF sEnemyData[sTWSData.iBossEnemyIndex].eEnemyState = TWS_ENEMY_DYING_2
							
				tl23BackgroundSprite = "wizard_bg_flash"
				
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
			
			ELSE
			
				IF sTWSData.iAnimatedBackgroundSpriteAnim < 10
					tl23BackgroundSprite = "castle_sky_glow_0000"
				ELSE
					tl23BackgroundSprite = "castle_sky_glow_000"
				ENDIF
				tl23BackgroundSprite += sTWSData.iAnimatedBackgroundSpriteAnim
				
				fTilePosX = cfBASE_SCREEN_WIDTH/2 - cfGAME_SCREEN_WIDTH/2

				FOR i = 0 TO ciTWS_ANIMATED_BACKGROUND_TILES-1
					ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(fTilePosX, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTWS_ANIMATED_BACKGROUND_TILE_WIDTH, cfTWS_BACKGROUND_TILE_HEIGHT), 0, sTWSData.rgbaSprite)
					fTilePosX += cfTWS_ANIMATED_BACKGROUND_TILE_WIDTH
					
				ENDFOR
				
				sTWSData.iAnimatedBackgroundSpriteAnim = (sTWSData.iAnimatedBackgroundSpriteAnim + sTWSData.iGlobalDefaultUpdateFrames) % ciTWS_ANIMATED_BACKGROUND_FRAMES
			ENDIF
			
		BREAK
	ENDSWITCH
	
	
	IF TWS_IS_CAMERA_SHAKE_ACTIVE()
		fShake = SIN(TO_FLOAT(GET_GAME_TIMER()*10))*20
	ENDIF
		
	// Midground
	FOR i = 0 TO iCurrentBackgroundTileToAdd - 1
		
		SWITCH sBackgroundTilesData[i].eType
		CASE TWS_BG_TL_1_INTRO
			tl23BackgroundSprite = "cave_rooms1"
		BREAK
		CASE TWS_BG_TL_1_BASE
			tl23BackgroundSprite = "cave_rooms2"
		BREAK
		CASE TWS_BG_TL_1_CAVE
			tl23BackgroundSprite = "cave_rooms3"
		BREAK
		CASE TWS_BG_TL_1_CLIFF
			tl23BackgroundSprite = "cave_rooms4"
		BREAK
		CASE TWS_BG_TL_1_SPIDER
			tl23BackgroundSprite = "cave_rooms5"
		BREAK
		CASE TWS_BG_TL_1_BOSS
			tl23BackgroundSprite = "cave_rooms6 - boss"
		BREAK
		CASE TWS_BG_TL_1_GEN_1
			tl23BackgroundSprite = "cave_tile_1"
		BREAK
		CASE TWS_BG_TL_1_GEN_2
			tl23BackgroundSprite = "cave_tile_2"
		BREAK
		
		CASE TWS_BG_TL_2_INTRO
			tl23BackgroundSprite = "swamp_room1"
		BREAK
		CASE TWS_BG_TL_2_BIG_TREE
			tl23BackgroundSprite = "swamp_room2"
		BREAK
		CASE TWS_BG_TL_2_TREES
			tl23BackgroundSprite = "swamp_room3"
		BREAK
		CASE TWS_BG_TL_2_DEAD_BODIES
			tl23BackgroundSprite = "swamp_room4"
		BREAK
		CASE TWS_BG_TL_2_BOSS
			tl23BackgroundSprite = "swamp_room5-boss"
		BREAK
		CASE TWS_BG_TL_2_GEN_1
			tl23BackgroundSprite = "swamp_generic1"
		BREAK
		CASE TWS_BG_TL_2_GEN_2
			tl23BackgroundSprite = "swamp_generic2"
		BREAK
		CASE TWS_BG_TL_2_GEN_3
			tl23BackgroundSprite = "swamp_generic3"
		BREAK
		
		CASE TWS_BG_TL_3_INTRO
			tl23BackgroundSprite = "castle_rooms1"
		BREAK
		CASE TWS_BG_TL_3_WINDOWS_OPEN
			tl23BackgroundSprite = "castle_rooms2"
		BREAK
		CASE TWS_BG_TL_3_STATUES
			tl23BackgroundSprite = "castle_rooms3"
		BREAK
		CASE TWS_BG_TL_3_BIG_WINDOW
			tl23BackgroundSprite = "castle_rooms4"
		BREAK
		CASE TWS_BG_TL_3_DOOR_STAIRS
			tl23BackgroundSprite = "castle_rooms5"
		BREAK
		CASE TWS_BG_TL_3_OUTRO
			tl23BackgroundSprite = "castle_rooms6"
		BREAK
		CASE TWS_BG_TL_3_GEN_1
			tl23BackgroundSprite = "castle_generic_parts1"
		BREAK
		CASE TWS_BG_TL_3_GEN_2
			tl23BackgroundSprite = "castle_generic_parts2"
		BREAK
		CASE TWS_BG_TL_3_FINAL
			EXIT
		
		CASE TWS_BG_TL_4_INTRO
			tl23BackgroundSprite = "forest_rooms1"
		BREAK
		CASE TWS_BG_TL_4_INTRO_NPC
			tl23BackgroundSprite = "forest_start_npc"
		BREAK
		CASE TWS_BG_TL_4_BIG_TREE
			tl23BackgroundSprite = "forest_rooms2"
		BREAK
		CASE TWS_BG_TL_4_BIG_MUSHROOM
			tl23BackgroundSprite = "forest_rooms3"
		BREAK
		CASE TWS_BG_TL_4_OPEN_BACK
			tl23BackgroundSprite = "forest_rooms4"
		BREAK
		CASE TWS_BG_TL_4_TREES
			tl23BackgroundSprite = "forest_rooms5"
		BREAK
		CASE TWS_BG_TL_4_GEN_1
			tl23BackgroundSprite = "forest_generic1"
		BREAK
		CASE TWS_BG_TL_4_GEN_2
			tl23BackgroundSprite = "forest_generic2"
		BREAK
		CASE TWS_BG_TL_4_GEN_3
			tl23BackgroundSprite = "forest_generic3"
		BREAK
		CASE TWS_BG_TL_4_BOSS
			tl23BackgroundSprite = "forest_rooms6"
		BREAK
		
		ENDSWITCH
		
		FLOAT fPlayerX
		IF (sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR (sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0)) AND sTWSData.eCurrentLevel != TWS_FOREST_INTRO
		
			fPlayerX = sBackgroundTilesData[i].fSpritePos + TWS_GET_OFFSET_TO_PLAYER(1.0, sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos)

		ELIF sPlayerData.ePlayerState = TWS_INTRO OR sPlayerData.vSpritePos.x <= cfBASE_SCREEN_WIDTH/2 OR sTWSData.eCurrentLevel = TWS_FOREST_INTRO
		
			fPlayerX = sBackgroundTilesData[i].fSpritePos
			
		ELIF sTWSData.bLockedScreen
		
			fPlayerX = sBackgroundTilesData[i].fSpritePos + TWS_GET_OFFSET_TO_PLAYER(1.0, sTWSData.fLockedScreenPos)
			
		ELSE
			fPlayerX = sBackgroundTilesData[i].fSpritePos + TWS_GET_OFFSET_TO_PLAYER()
		ENDIF
		
		IF ABSF(sBackgroundTilesData[i].fSpritePos - sPlayerData.vSpritePos.x) < cfGAME_SCREEN_WIDTH*2
			IF GET_UV_FOR_BACKGROUND_TILE(sBackgroundTilesData[i].eType, vU,vV)
				ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(fPlayerX, cfBASE_SCREEN_HEIGHT/2+fShake), INIT_VECTOR_2D(sBackgroundTilesData[i].fSpriteWidth, cfTWS_BACKGROUND_TILE_HEIGHT),vU,vV, 0, sTWSData.rgbaSprite)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(fPlayerX, cfBASE_SCREEN_HEIGHT/2+fShake), INIT_VECTOR_2D(sBackgroundTilesData[i].fSpriteWidth, cfTWS_BACKGROUND_TILE_HEIGHT), 0, sTWSData.rgbaSprite)
			ENDIF
		ENDIF
		
		IF sBackgroundTilesData[i].eType = TWS_BG_TL_1_CLIFF
			tl23BackgroundSprite = "cave_rooms4_bridge"
			ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(fPlayerX, cfTWS_SC_BRIDGE_OFFSET+fShake), INIT_VECTOR_2D(cfTWS_SC_BRIDGE_WIDTH, cfTWS_SC_BRIDGE_HEIGHT), 0, sTWSData.rgbaSprite)
				
		ENDIF
		
	ENDFOR
		
ENDPROC

PROC TWS_DRAW_FOREGROUND()

	TEXT_LABEL_31 tl23BackgroundSprite
	TEXT_LABEL_31 tl23BackgroundDictionary
	
	SWITCH sTWSData.eCurrentLevel
		CASE 	TWS_FOREST_INTRO 
		CASE 	TWS_FOREST 
		CASE 	TWS_SWAMP 
			tl23BackgroundDictionary = "MPWizardsSleeveBackgrounds12"
		BREAK
		CASE 	TWS_CAVES
		CASE 	TWS_CASTLE
		CASE 	TWS_CASTLE_OUTRO 
			tl23BackgroundDictionary = "MPWizardsSleeveBackgrounds34"
		BREAK
	ENDSWITCH 
	
	FLOAT fShake = 0.0
	IF TWS_IS_CAMERA_SHAKE_ACTIVE()
		fShake = SIN(TO_FLOAT(GET_GAME_TIMER()*15))
	ENDIF
	
	// Foreground
	INT i
	FOR i = 0 TO iCurrentForegroundTileToAdd - 1
		
		SWITCH sForegroundTilesData[i].eType
		CASE TWS_FG_TL_1_INTRO
			tl23BackgroundSprite = "cave_foreground1 - start"
		BREAK
		CASE TWS_FG_TL_1_COL1
			tl23BackgroundSprite = "cave_foreground2"
		BREAK
		CASE TWS_FG_TL_1_COL2
			tl23BackgroundSprite = "cave_foreground3"
		BREAK
		CASE TWS_FG_TL_1_STALAGMITE1
			tl23BackgroundSprite = "cave_foreground4"
		BREAK
		CASE TWS_FG_TL_1_STALAGMITE2
			tl23BackgroundSprite = "cave_foreground5"
		BREAK
		CASE TWS_FG_TL_1_STALAGMITE3
			tl23BackgroundSprite = "cave_foreground6"
		BREAK
		CASE TWS_FG_TL_1_BOSS
			tl23BackgroundSprite = "cave_foreground7 - boss"
		BREAK
		
		CASE TWS_FG_TL_2_GRASS
			tl23BackgroundSprite = "swamp_fore_grass"
		BREAK
		CASE TWS_FG_TL_2_PIKE
			tl23BackgroundSprite = "swamp_fore_pike"
		BREAK
		CASE TWS_FG_TL_2_ROOT
			tl23BackgroundSprite = "swamp_fore_rootskulls"
		BREAK
		CASE TWS_FG_TL_2_VINES_1
			tl23BackgroundSprite = "swamp_fore_topvines_1"
		BREAK
		CASE TWS_FG_TL_2_VINES_2
			tl23BackgroundSprite = "swamp_fore_topvines_2"
		BREAK
		CASE TWS_FG_TL_2_VINES_3
			tl23BackgroundSprite = "swamp_fore_topvines_3"
		BREAK
		
		CASE TWS_FG_TL_3_INTRO
			tl23BackgroundSprite = "castle_foreground_enterance"
		BREAK
		CASE TWS_FG_TL_3_GARGOYLE_1
			tl23BackgroundSprite = "castle_foregrounds1"
		BREAK
		CASE TWS_FG_TL_3_GARGOYLE_2
			tl23BackgroundSprite = "castle_foregrounds2"
		BREAK
		CASE TWS_FG_TL_3_COLUMN
			tl23BackgroundSprite = "castle_foregrounds3"
		BREAK
		CASE TWS_FG_TL_3_CANDLES_1
			tl23BackgroundSprite = "castle_foregrounds4"
		BREAK
		CASE TWS_FG_TL_3_CANDLES_2
			tl23BackgroundSprite = "castle_foregrounds5"
		BREAK
		
		CASE TWS_FG_TL_4_BLUE_FLOWERS
			tl23BackgroundSprite = "forest_foreground1"
		BREAK
		CASE TWS_FG_TL_4_YELLOW_FLOWERS
			tl23BackgroundSprite = "forest_foreground2"
		BREAK
		CASE TWS_FG_TL_4_MUSHROOMS
			tl23BackgroundSprite = "forest_foreground3"
		BREAK
		CASE TWS_FG_TL_4_SHORT_GRASS
			tl23BackgroundSprite = "forest_foreground4"
		BREAK
		CASE TWS_FG_TL_4_TALL_GRASS
			tl23BackgroundSprite = "forest_foreground5"
		BREAK
		ENDSWITCH
		
		FLOAT fPlayerX
		IF sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0
		
			fPlayerX = sForegroundTilesData[i].vSpritePos.X + TWS_GET_OFFSET_TO_PLAYER(2.0, sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos)
			
		ELIF sPlayerData.ePlayerState = TWS_INTRO
		
			fPlayerX = sForegroundTilesData[i].vSpritePos.X*2.0
			
		ELIF sTWSData.bLockedScreen
			fPlayerX = sForegroundTilesData[i].vSpritePos.X + TWS_GET_OFFSET_TO_PLAYER(2.0, sTWSData.fLockedScreenPos)
			
		ELSE
			fPlayerX = sForegroundTilesData[i].vSpritePos.X + TWS_GET_OFFSET_TO_PLAYER(2.0)
		ENDIF
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(tl23BackgroundDictionary, tl23BackgroundSprite, INIT_VECTOR_2D(fPlayerX, sForegroundTilesData[i].vSpritePos.Y+fShake), MULTIPLY_VECTOR_2D(sForegroundTilesData[i].vSpriteSize, cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
	ENDFOR
	
ENDPROC

PROC TWS_DRAW_FADE()
	
	// Fade
	IF sTWSData.eLevelStage = TWS_LEVEL_LEADERBOARD OR sTWSData.eLevelStage = TWS_LEVEL_DEAD
		IF sTWSData.rgbaFade.iA < 100
			sTWSData.rgbaFade.iA += 4
		ENDIF
	ELSE
		IF TWS_IS_SCREEN_FADING_IN()
			sTWSData.rgbaFade.iA -= 4
			CDEBUG3LN(DEBUG_MINIGAME, "[TWS_ARCADE] [GD] : FADE: ",sTWSData.rgbaFade.iA)
		ELIF TWS_IS_SCREEN_FADING_OUT()
			sTWSData.rgbaFade.iA += 4
			CDEBUG3LN(DEBUG_MINIGAME, "[TWS_ARCADE] [GD] : FADE: ",sTWSData.rgbaFade.iA)
		ENDIF
	ENDIF
	
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaFade)
	
ENDPROC

PROC TWS_DRAW_INTEGER(INT iValue, INT iDigits, VECTOR_2D vCenter, RGBA_COLOUR_STRUCT rgba, VECTOR_2D vSize, VECTOR_2D vSeparation, STRING sTextureName, STRING sDictionary)

	VECTOR_2D vCharCenter = vCenter
	BOOL bZero = (iValue = 0)
	INT iChar
	
	IF iDigits = 0
		INT currentValue
		currentValue = iValue
		WHILE currentValue > 0 OR bZero
			bZero = FALSE
			vCharCenter.x = vCenter.x + vSeparation.x * (iDigits-1)/2.0 - vSeparation.x * iChar
			TEXT_LABEL_63 tlTextureName = sTextureName
			tlTextureName += currentValue % 10
			currentValue /= 10
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDictionary, tlTextureName, vCharCenter, vSize, 0.0, rgba)
			iChar++
	  ENDWHILE
	ELSE
		REPEAT iDigits iChar
			vCharCenter.x = vCenter.x + vSeparation.x * (iDigits-1)/2.0 - vSeparation.x * iChar
			TEXT_LABEL_63 tlTextureName = sTextureName
			tlTextureName += (iValue / ROUND(POW(10.0,to_float(iChar)))) % 10
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDictionary, tlTextureName,vCharCenter,vSize,0.0, rgba)
		ENDREPEAT
	ENDIF
ENDPROC

PROC TWS_DRAW_HUD()

	TEXT_LABEL_23 tl23Sprite
	VECTOR_2D vScreenCenter =  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2)
	
	VECTOR_2D vLeftCorner =  SUBTRACT_VECTOR_2D(vScreenCenter,  INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH/2, cfGAME_SCREEN_HEIGHT/2))
	VECTOR_2D vMidTop =  SUBTRACT_VECTOR_2D(vScreenCenter,  INIT_VECTOR_2D(0, cfGAME_SCREEN_HEIGHT/2))
	VECTOR_2D vRightCorner =  SUBTRACT_VECTOR_2D(vScreenCenter,  INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH/2, cfGAME_SCREEN_HEIGHT/2))
	
	TWS_DRAW_FADE()
	
	IF sTWSData.eLevelStage = TWS_LEVEL_DEAD
		
		// Game over
		IF sHUDData[TWS_HUD_GAMEOVER].bActive
			tl23Sprite = "gameth_over"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, sHUDData[TWS_HUD_GAMEOVER].vSpritePos, MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_START_WIDTH, cfTWS_HUD_START_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		ENDIF
		
	ELIF NOT sTWSData.bHUDIsHidden
		// Portrait
		vScreenCenter =  SUBTRACT_VECTOR_2D(vScreenCenter,  INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH/2, cfGAME_SCREEN_HEIGHT/2))
		
		IF sTWSData.bIsHardCoreMode
			tl23Sprite = "grog_"
		ELSE
			tl23Sprite = ""
		ENDIF
		
		tl23Sprite += "portrait_state"
		tl23Sprite += sPlayerData.iLevel
		tl23Sprite += "_"
		
		IF sPlayerData.fHealth < 0.35
			tl23Sprite += "3"
		ELIF sPlayerData.fHealth < 0.65
			tl23Sprite += "2"
		ELSE
			tl23Sprite += "1"
		ENDIF
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vLeftCorner,INIT_VECTOR_2D(cfTWS_HUD_PORTRAIT_X, cfTWS_HUD_PORTRAIT_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_PORTRAIT_WIDTH, cfTWS_HUD_PORTRAIT_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
		// Meter Frames
		tl23Sprite = "meter_fill_hp"
		FLOAT fBarSize = cfTWS_HUD_BAR_WIDTH * sPlayerData.fHealth
		FLOAT fOffset = (cfTWS_HUD_BAR_WIDTH - fBarSize)/2
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vLeftCorner, INIT_VECTOR_2D(cfTWS_HUD_HP_X - fOffset*cfTWS_RESCALE_FACTOR, cfTWS_HUD_HP_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(fBarSize, cfTWS_HUD_BAR_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
		tl23Sprite = "meter_fill_mp"
		fBarSize = cfTWS_HUD_BAR_WIDTH * sPlayerData.fMagic
		fOffset = (cfTWS_HUD_BAR_WIDTH - fBarSize)/2
			
		IF sTWSData.bIsHardCoreMode OR (sPlayerData.fMagic < 1.0 OR sPlayerData.iLevel >= 3 OR (sPlayerData.iLevel < 3 AND sPlayerData.fMagic >= 1.0 AND SIN(GET_GAME_TIMER()/0.8) > 0))
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vLeftCorner, INIT_VECTOR_2D(cfTWS_HUD_MP_X - fOffset*cfTWS_RESCALE_FACTOR, cfTWS_HUD_MP_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(fBarSize, cfTWS_HUD_BAR_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		ENDIF
		
		// Hi (letters)
		tl23Sprite = "hi_score"
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vRightCorner, INIT_VECTOR_2D(cfTWS_HUD_HI_X, cfTWS_HUD_HI_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_HI_WIDTH, cfTWS_HUD_HI_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
		// Score
		TWS_DRAW_INTEGER(sPlayerData.iScore, 9, INIT_VECTOR_2D(cfTWS_HUD_SCORE_X, cfTWS_HUD_SCORE_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")
		
		// Hi Score
		TWS_DRAW_INTEGER(sPlayerData.iHighestScore, 9, INIT_VECTOR_2D(cfTWS_HUD_HI_SCORE_X, cfTWS_HUD_HI_SCORE_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")
		
		IF sTWSData.bIsHardCoreMode
			tl23Sprite = "grog_meters_frames"
		ELSE
			tl23Sprite = "meter_frames"
		ENDIF
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vLeftCorner,INIT_VECTOR_2D(cfTWS_HUD_METER_X, cfTWS_HUD_METER_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_METER_WIDTH, cfTWS_HUD_METER_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
		tl23Sprite = "hud_numbers_"
		tl23Sprite += sPlayerData.iLifes
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vLeftCorner,INIT_VECTOR_2D(cfTWS_HUD_LIFES_NUMBER_1_X, cfTWS_HUD_LIFES_NUMBER_1_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_SMALL_NUMBER_WIDTH, cfTWS_HUD_SMALL_NUMBER_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		tl23Sprite = "hud_numbers_"
		tl23Sprite += sPlayerData.iLevel
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vLeftCorner,INIT_VECTOR_2D(cfTWS_HUD_LEVEL_NUMBER_1_X, cfTWS_HUD_LEVEL_NUMBER_1_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_SMALL_NUMBER_WIDTH, cfTWS_HUD_SMALL_NUMBER_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
		
		// Boss Health
		IF sTWSData.eLevelStage = TWS_LEVEL_BOSS_FIGHT
		
			tl23Sprite = "boss_health_frame"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vMidTop, INIT_VECTOR_2D(cfTWS_HUD_BOSS_METER_X*cfTWS_RESCALE_FACTOR, cfTWS_HUD_BOSS_METER_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_BOSS_METER_WIDTH, cfTWS_HUD_BOSS_METER_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
			
			tl23Sprite = "meter_fill_hp"
			fBarSize = cfTWS_HUD_BOSS_BAR_WIDTH * sEnemyData[sTWSData.iBossEnemyIndex].fHealth
			fOffset = (cfTWS_HUD_BOSS_BAR_WIDTH - fBarSize)/2
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, ADD_VECTOR_2D(vMidTop, INIT_VECTOR_2D(cfTWS_HUD_BOSS_HP_X - fOffset*cfTWS_RESCALE_FACTOR, cfTWS_HUD_BOSS_HP_Y)), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(fBarSize, cfTWS_HUD_BOSS_BAR_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		
		ENDIF
		
		// GO Arrow
		IF sHUDData[TWS_HUD_GO_ARROW].bActive AND sHUDData[TWS_HUD_GO_ARROW].bRendering
			tl23Sprite = "GO_arrow"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/12*9, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTWS_HUD_GO_ARROW_WIDTH, cfTWS_HUD_GO_ARROW_HEIGHT), 0, sTWSData.rgbaSprite)
		ENDIF
			
		// Level start
		IF sHUDData[TWS_HUD_START].bActive
			tl23Sprite = "text_stage_start"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, sHUDData[TWS_HUD_START].vSpritePos, MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_START_WIDTH, cfTWS_HUD_START_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		ENDIF
		
		// Level end
		IF sHUDData[TWS_HUD_END].bActive
			tl23Sprite = "text_stage_complete"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, sHUDData[TWS_HUD_END].vSpritePos, MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_END_WIDTH, cfTWS_HUD_END_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
			
			tl23Sprite = "score_totals1"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, INIT_VECTOR_2D(cfTWS_HUD_HEALTH_SCORE_X, cfTWS_HUD_HEALTH_SCORE_Y), INIT_VECTOR_2D(cfTWS_HUD_SCORE_FINAL_WIDTH, cfTWS_HUD_SCORE_FINAL_HEIGHT), 0, sTWSData.rgbaSprite)
			IF sTWSData.iHealthPoints > 0
				TWS_DRAW_INTEGER(sTWSData.iHealthPoints, 9, INIT_VECTOR_2D(cfTWS_HUD_HEALTH_SCORE_NUMBER_X, cfTWS_HUD_HEALTH_SCORE_NUMBER_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")
			ELSE
				TWS_DRAW_INTEGER(0, 9, INIT_VECTOR_2D(cfTWS_HUD_HEALTH_SCORE_NUMBER_X, cfTWS_HUD_HEALTH_SCORE_NUMBER_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")
			ENDIF
			
			tl23Sprite = "score_totals2"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, INIT_VECTOR_2D(cfTWS_HUD_LIVES_SCORE_X, cfTWS_HUD_LIVES_SCORE_Y), INIT_VECTOR_2D(cfTWS_HUD_SCORE_FINAL_WIDTH, cfTWS_HUD_SCORE_FINAL_HEIGHT), 0, sTWSData.rgbaSprite)
			IF sTWSData.iLivesPoints > 0
				TWS_DRAW_INTEGER(sTWSData.iLivesPoints, 9, INIT_VECTOR_2D(cfTWS_HUD_LIVES_SCORE_NUMBER_X, cfTWS_HUD_LIVES_SCORE_NUMBER_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")
			ELSE
				TWS_DRAW_INTEGER(0, 9, INIT_VECTOR_2D(cfTWS_HUD_LIVES_SCORE_NUMBER_X, cfTWS_HUD_LIVES_SCORE_NUMBER_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")
			ENDIF
			
			tl23Sprite = "score_totals3"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, INIT_VECTOR_2D(cfTWS_HUD_TOTAL_SCORE_X, cfTWS_HUD_TOTAL_SCORE_Y), INIT_VECTOR_2D(cfTWS_HUD_SCORE_FINAL_WIDTH, cfTWS_HUD_SCORE_FINAL_HEIGHT), 0, sTWSData.rgbaSprite)
			TWS_DRAW_INTEGER(sPlayerData.iScore, 9, INIT_VECTOR_2D(cfTWS_HUD_TOTAL_SCORE_NUMBER_X, cfTWS_HUD_TOTAL_SCORE_NUMBER_Y), sTWSData.rgbaSprite, INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH, cfTWS_HUD_SCORE_HEIGHT), INIT_VECTOR_2D(cfTWS_HUD_SCORE_WIDTH*0.8, cfTWS_HUD_SCORE_HEIGHT*1.1), "hud_numbers_", "MPWizardsSleeveHUDAndScreen")

		ENDIF
		
	ENDIF
	
	// Dialogue Frame
	IF sHUDData[TWS_HUD_DIALOGUE_FRAME].bRendering
		
		tl23Sprite = "dialog_frame"
		tl23Sprite += (sHUDData[TWS_HUD_DIALOGUE_FRAME].iSpriteBlastAnimFrame + 1)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, sHUDData[TWS_HUD_DIALOGUE_FRAME].vSpritePos, MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_DIALOGUE_FRAME_WIDTH, cfTWS_HUD_DIALOGUE_FRAME_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
	
		IF sHUDData[TWS_HUD_DIALOGUE_FRAME].iSpriteBlastAnimFrame >= ciTWS_DIALOGUE_FRAME_ANIM_FRAMES -1 AND sHUDData[TWS_HUD_DIALOGUE_FRAME].bActive
			
			IF sTWSData.iCurrentDialogue > 9
				tl23Sprite = "DIALOG_TEXT_000"
				tl23Sprite += sTWSData.iCurrentDialogue
			ELSE
				tl23Sprite = "DIALOG_TEXT_0000"
				tl23Sprite += sTWSData.iCurrentDialogue
			ENDIF
			
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, sHUDData[TWS_HUD_DIALOGUE_FRAME].vSpritePos, MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_DIALOGUE_FRAME_WIDTH, cfTWS_HUD_DIALOGUE_FRAME_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
		ENDIF
	ENDIF
	
	// CHEST
	IF sPlayerData.ePlayerState = TWS_EVOLVING_3
		tl23Sprite = "chest_rip_lvl"
		tl23Sprite += sPlayerData.iLevel
		tl23Sprite += "-"
		tl23Sprite += (sPlayerData.iLevel+1)
		tl23Sprite += "_"
		tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen2", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	ENDIF
	
	
ENDPROC

PROC TWS_DRAW_TRAP(INT i)

	TEXT_LABEL_23 tl23Sprite
	FLOAT fFactor = cfTWS_RESCALE_FACTOR
	FLOAT fShake = 0.0
	
	IF TWS_IS_CAMERA_SHAKE_ACTIVE()
		fShake = SIN(TO_FLOAT(GET_GAME_TIMER()*10))*20
	ENDIF
	
	IF sTrapsData[i].bIsActive
		
		SWITCH sTrapsData[i].eType
			CASE TWS_TRAP_STALAGTITE
			
				IF sTrapsData[i].vSpritePos2.y < sTrapsData[i].vSpritePos.y - sTrapsData[i].vSpriteSize.y
					tl23Sprite = "fx_item_small_shadow"
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl23Sprite, INIT_VECTOR_2D(sTrapsData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sTrapsData[i].vSpritePos.y + fShake), INIT_VECTOR_2D(cfTWS_FX_SHADOW_SMALL_WIDTH*fFactor, cfTWS_FX_SHADOW_SMALL_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
					
					tl23Sprite = "cave_stalactite1"
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveBackgrounds34", tl23Sprite, INIT_VECTOR_2D(sTrapsData[i].vSpritePos2.x + TWS_GET_OFFSET_TO_PLAYER() ,sTrapsData[i].vSpritePos2.y + fShake), MULTIPLY_VECTOR_2D(sTrapsData[i].vSpriteSize, fFactor), 0, sTWSData.rgbaSprite)
				ELSE
					tl23Sprite = "cave_stalactite2"
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveBackgrounds34", tl23Sprite, INIT_VECTOR_2D(sTrapsData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sTrapsData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(sTrapsData[i].vSpriteSize, fFactor), 0, sTWSData.rgbaSprite)
				ENDIF
			BREAK
			CASE TWS_TRAP_HOLE
				tl23Sprite = "cave_hole"
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveBackgrounds34", tl23Sprite, INIT_VECTOR_2D(sTrapsData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sTrapsData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(sTrapsData[i].vSpriteSize, fFactor), 0, sTWSData.rgbaSprite)
			BREAK
		ENDSWITCH
	ENDIF

	
ENDPROC

PROC TWS_DRAW_ITEM(INT i)

	TEXT_LABEL_23 tl23Sprite
	FLOAT fFactor = cfTWS_RESCALE_FACTOR
	FLOAT fShake = 0.0
	
	IF TWS_IS_CAMERA_SHAKE_ACTIVE()
		fShake = SIN(TO_FLOAT(GET_GAME_TIMER()*10))*20
	ENDIF
	
	IF sItemData[i].bIsActive
			
		IF NOT sItemData[i].bIsHit
		
			IF sItemData[i].eItemType != TWS_ITEM_GOLD_1
			AND sItemData[i].eItemType != TWS_ITEM_GOLD_2
			AND sItemData[i].eItemType != TWS_ITEM_GOLD_3
			AND sItemData[i].eItemType != TWS_ITEM_GOLD_4
				tl23Sprite = "fx_item_small_shadow"
				
				IF sItemData[i].bFalling
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl23Sprite, INIT_VECTOR_2D(sItemData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sItemData[i].vSpriteFallPos.y + (sItemData[i].vSpriteSize.y)) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_SMALL_WIDTH*fFactor, cfTWS_FX_SHADOW_SMALL_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
				ELSE
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl23Sprite, INIT_VECTOR_2D(sItemData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sItemData[i].vSpritePos.y + (sItemData[i].vSpriteSize.y)) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_SMALL_WIDTH*fFactor, cfTWS_FX_SHADOW_SMALL_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
				ENDIF
			ENDIF
			
			SWITCH sItemData[i].eItemType
				CASE TWS_ITEM_HP_SMA
					tl23Sprite = "item_hp_small"
				BREAK
				CASE TWS_ITEM_HP_MED
					tl23Sprite = "item_hp_medium"
				BREAK
				CASE TWS_ITEM_HP_LAR
					tl23Sprite = "item_hp_large"
				BREAK
				CASE TWS_ITEM_MP_SMA
					tl23Sprite = "item_mp_small"
				BREAK
				CASE TWS_ITEM_MP_MED
					tl23Sprite = "item_mp_medium"
				BREAK
				CASE TWS_ITEM_MP_LAR
					tl23Sprite = "item_mp_large"
				BREAK
				CASE TWS_ITEM_GOLD_1
					tl23Sprite = "item_gold_1"
				BREAK
				CASE TWS_ITEM_GOLD_2
					tl23Sprite = "item_gold_2"
				BREAK
				CASE TWS_ITEM_GOLD_3
					tl23Sprite = "item_gold_3"
				BREAK
				CASE TWS_ITEM_GOLD_4
					tl23Sprite = "item_gold_4"
				BREAK
				CASE TWS_ITEM_1UP
					tl23Sprite = "item_1up"
				BREAK
			ENDSWITCH
			
			IF ABSF(sItemData[i].vSpritePos.X - sPlayerData.vSpritePos.x) < cfBASE_SCREEN_WIDTH
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl23Sprite, INIT_VECTOR_2D(sItemData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sItemData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(sItemData[i].vSpriteSize, fFactor), 0, sTWSData.rgbaSprite)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC TWS_DRAW_PLAYER()

	TEXT_LABEL_23 tl23Sprite
	TEXT_LABEL_23 tl23Dictionary
	FLOAT fFactor = cfTWS_RESCALE_FACTOR
	VECTOR_2D fOffset
	
	IF sPlayerData.bIsInvincible AND SIN(GET_GAME_TIMER()/0.8) > 0.0
		EXIT
	ENDIF
	
	fOffset = sPlayerData.vSpriteOffset
	
	IF sTWSData.bIsHardCoreMode
		tl23Sprite = "grog_s"
		tl23Dictionary = "MPWizardsSleeveGrog"
	ELSE
		tl23Sprite = "thog_s"
		IF sPlayerData.iLevel = 1
		OR sPlayerData.iLevel = 2
			tl23Dictionary = "MPWizardsSleeveThog12"
		ELIF sPlayerData.iLevel = 3
			tl23Dictionary = "MPWizardsSleeveThog3NPC"
		ENDIF
	ENDIF
	
	// Player
	SWITCH sPlayerData.ePlayerState
		CASE TWS_IDLE
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_idle"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_INTRO
		CASE TWS_WALK
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_walk_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_EVOLVING
		CASE TWS_EVOLVING_6
					
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_levelup_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_EVOLVING_2
		CASE TWS_EVOLVING_5
			
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_levelup_4"
			
		BREAK
		CASE TWS_EVOLVING_3
		CASE TWS_EVOLVING_4
			
			
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_levelup_4"
			
		BREAK
		CASE TWS_SUPERATTACK
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_levelup_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_ATTACK
		CASE TWS_ATTACK_DOUBLE
			
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_attack_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_ATTACK_LOW
						
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_kick"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_CHARGE
			
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_dash"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_JUMP_ATTACK
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_attack_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
			
		BREAK
		CASE TWS_JUMP_ATTACK_DESCEND
			
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_attack_3"
			
		BREAK
		CASE TWS_JUMP_START
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_1"
			
		BREAK
		CASE TWS_JUMP_ASCEND
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_2"
			
		BREAK
		CASE TWS_JUMP_CREST
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_3"
			
		BREAK
		CASE TWS_JUMP_DESCEND
		CASE TWS_FALL_INFINITE
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_4"
			
		BREAK
		CASE TWS_JUMP_END
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_jump_5"
			
		BREAK
		
		CASE TWS_FALL_HOLE_1
			IF sTWSData.bIsHardCoreMode
				tl23Sprite = "grog_no_hole_state"
			ELSE
				tl23Sprite = "no_hole_state"
			ENDIF
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
		BREAK
		
		CASE TWS_FALL_HOLE_2
			EXIT
			
		CASE TWS_STUN
		
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_hurt_stun"
			
			IF SIN(GET_GAME_TIMER()/0.3) > 0.0
				fOffset.x -= cfTWS_ENEMY_HIT_OFFSET
			ELSE
				fOffset.x += cfTWS_ENEMY_HIT_OFFSET
			ENDIF
			
		BREAK
		CASE TWS_FALL
		CASE TWS_FALL_HIGH
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_hurt_fall_1"
		BREAK
		CASE TWS_FALL_2
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_hurt_fall_2"
		BREAK
		CASE TWS_FLAT
		CASE TWS_DEAD
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_hurt_flat_1"
		BREAK
		CASE TWS_GETTING_UP
			tl23Sprite += sPlayerData.iLevel
			tl23Sprite += "_hurt_getup_"
			tl23Sprite += (sPlayerData.iSpriteAnimFrame + 1)
		BREAK
	ENDSWITCH
		
	// Shadow
	//
	
	IF sPlayerData.ePlayerState != TWS_FALL_INFINITE AND sPlayerData.ePlayerState != TWS_FALL_HOLE_1 AND sPlayerData.ePlayerState != TWS_FALL_HOLE_2
		IF sPlayerData.ePlayerState = TWS_INTRO OR sPlayerData.vSpritePos.x < sBackgroundTilesData[0].fSpritePos
			
			IF sPlayerData.bIsJumping
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(sPlayerData.vSpritePos.x + TWS_GET_PLAYER_SHADOW_X_OFFSET(), sPlayerData.vInitialJumpPos.y + (sPlayerData.vSpriteSize.y) + fOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(sPlayerData.vSpritePos.x + TWS_GET_PLAYER_SHADOW_X_OFFSET(), sPlayerData.vSpritePos.y + (sPlayerData.vSpriteSize.y) + fOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ENDIF
			
		ELIF sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0
			
			FLOAT fShadowPosX = cfBASE_SCREEN_WIDTH/2 + (sPlayerData.vSpritePos.x - sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos)

			IF sPlayerData.bIsJumping
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fShadowPosX + TWS_GET_PLAYER_SHADOW_X_OFFSET() , sPlayerData.vInitialJumpPos.y + (sPlayerData.vSpriteSize.y) + fOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fShadowPosX + TWS_GET_PLAYER_SHADOW_X_OFFSET() , sPlayerData.vSpritePos.y + (sPlayerData.vSpriteSize.y) + fOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ENDIF
		
		ELIF sTWSData.bLockedScreen
			
			FLOAT fShadowPosX = cfBASE_SCREEN_WIDTH/2 + (sPlayerData.vSpritePos.x - sTWSData.fLockedScreenPos)

			IF sPlayerData.bIsJumping
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fShadowPosX + TWS_GET_PLAYER_SHADOW_X_OFFSET(), sPlayerData.vInitialJumpPos.y + (sPlayerData.vSpriteSize.y) + fOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fShadowPosX + TWS_GET_PLAYER_SHADOW_X_OFFSET() , sPlayerData.vSpritePos.y + (sPlayerData.vSpriteSize.y) + fOffset.Y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ENDIF
			
		ELSE
			IF sPlayerData.bIsJumping
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2 + TWS_GET_PLAYER_SHADOW_X_OFFSET(), sPlayerData.vInitialJumpPos.y + (sPlayerData.vSpriteSize.y) + fOffset.Y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2 + TWS_GET_PLAYER_SHADOW_X_OFFSET(), sPlayerData.vSpritePos.y + (sPlayerData.vSpriteSize.y) + fOffset.Y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ENDIF
		ENDIF
	ENDIF
	
	// Character
	// 
	FLOAT fPlayerX
	IF sPlayerData.ePlayerState != TWS_INTRO AND sPlayerData.vSpritePos.x > cfBASE_SCREEN_WIDTH/2
	AND sPlayerData.vSpritePos.x < sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos
	AND sTWSData.eLevelStage < TWS_LEVEL_BOSS_FIGHT
		
		IF sTWSData.bLockedScreen
			fPlayerX = cfBASE_SCREEN_WIDTH/2 + (sPlayerData.vSpritePos.x - sTWSData.fLockedScreenPos)
		ELSE
			fPlayerX = cfBASE_SCREEN_WIDTH/2
		ENDIF
		
	ELIF sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0
		fPlayerX = cfBASE_SCREEN_WIDTH/2 + (sPlayerData.vSpritePos.x - sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos)

	ELSE	
		fPlayerX = sPlayerData.vSpritePos.x
	ENDIF
	
//	CDEBUG3LN(DEBUG_MINIGAME, "[TWS_ARCADE] [GD] : Rendering Player: ",fPlayerX)
			
	IF sPlayerData.bFacingLeft
		ARCADE_DRAW_PIXELSPACE_SPRITE(tl23Dictionary, tl23Sprite, INIT_VECTOR_2D(fPlayerX - fOffset.X, sPlayerData.vSpritePos.y + fOffset.Y), INIT_VECTOR_2D(-sPlayerData.vSpriteSize.X*fFactor, sPlayerData.vSpriteSize.Y*fFactor), 0, sTWSData.rgbaSprite)
	ELSE
		ARCADE_DRAW_PIXELSPACE_SPRITE(tl23Dictionary, tl23Sprite, INIT_VECTOR_2D(fPlayerX + fOffset.X, sPlayerData.vSpritePos.y + fOffset.Y), INIT_VECTOR_2D(sPlayerData.vSpriteSize.X*fFactor, sPlayerData.vSpriteSize.Y*fFactor), 0, sTWSData.rgbaSprite)
	ENDIF
ENDPROC

PROC TWS_DRAW_FX(INT i)

	TEXT_LABEL_63 tl31Sprite
	FLOAT fShake, fFactor = cfTWS_RESCALE_FACTOR
	
	IF TWS_IS_CAMERA_SHAKE_ACTIVE()
		fShake = SIN(TO_FLOAT(GET_GAME_TIMER()*10))*20
	ENDIF

	IF sFXData[i].bActive
		SWITCH sFXData[i].eType
			CASE TWS_FX_SWORD_LIGHTNING
				tl31Sprite = "fx_sword_lightning"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y - 20.0 + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_SWORD_LIGHTNING_WIDTH, cfTWS_FX_SWORD_LIGHTNING_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_SUPERATTACK_EXPLOSION
				tl31Sprite = "fx_magic_explosion"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_SUPERATTACK_WIDTH, cfTWS_FX_SUPERATTACK_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_PICKUP
				tl31Sprite = "fx_pickup"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER() ,sFXData[i].vSpritePos.y), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_ITEM_WIDTH, cfTWS_FX_ITEM_HEIGHT), fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_MAGIC_COLUMN
				tl31Sprite = "fx_pillar"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_WIZARD_PILLAR_WIDTH, cfTWS_FX_WIZARD_PILLAR_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_MAGIC_FIRE
				tl31Sprite = "fx_flames"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_WIZARD_FIRE_WIDTH, cfTWS_FX_WIZARD_FIRE_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_SWORD
				tl31Sprite = "forest_boss_slamblade"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_KNIGHT_SWORD_WIDTH, cfTWS_FX_KNIGHT_SWORD_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_SWORD_DROP
				tl31Sprite = "forest_boss_sword_drop"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveEnemies1a", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_BOSS_KNIGHT_SWORD_WIDTH, cfTWS_BOSS_KNIGHT_SWORD_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_TRAP_SPEAR
				tl31Sprite = "castle_trap_spear"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveBackgrounds34", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_TRAP_SPEAR_WIDTH, cfTWS_TRAP_SPEAR_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_GROG_BUBBLE
				tl31Sprite = "grog_bubble"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveGrog", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_GROG_WIDTH, cfTWS_FX_GROG_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_HELP
				IF SIN(GET_GAME_TIMER()/0.9) > 0.0
					tl31Sprite = "help"
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_HELP_WIDTH, cfTWS_FX_HELP_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
				ENDIF
			BREAK
			CASE TWS_FX_COD_DROP
				tl31Sprite = "forest_boss_cod_peice"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveEnemies1a", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_BOSS_KNIGHT_COD_WIDTH, cfTWS_BOSS_KNIGHT_COD_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_SMALL_HIT
				tl31Sprite = "fx_hit_small_"
				IF sPlayerData.iLevel = 2
					tl31Sprite += "blue"
				ENDIF
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_SMALL_HIT_WIDTH, cfTWS_FX_SMALL_HIT_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_LARGE_HIT
				tl31Sprite = "fx_hit_large"
				IF sPlayerData.iLevel = 2
					tl31Sprite += "_blue"
				ELIF sPlayerData.iLevel = 3
					tl31Sprite += "_green"
				ENDIF
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_LARGE_HIT_WIDTH, cfTWS_FX_LARGE_HIT_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_VENOM_HIT
				tl31Sprite = "fx_blood_1_"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_VENOM_HIT_WIDTH, cfTWS_FX_VENOM_HIT_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_BLOOD_HIT
				tl31Sprite = "fx_blood_red_"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_VENOM_HIT_WIDTH, cfTWS_FX_VENOM_HIT_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			BREAK
			CASE TWS_FX_CLOTHES
				IF SIN(GET_GAME_TIMER()/0.4) > 0.0
					tl31Sprite = "ripped_clothes"
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_RIPPED_CLOTHES_WIDTH, cfTWS_FX_RIPPED_CLOTHES_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
				ENDIF
			BREAK
			CASE TWS_FX_BOSS_SPIDER_SPIT
				tl31Sprite = "fx_boss_spray_green"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_BOSS_SPIDER_SPIT_WIDTH, cfTWS_FX_BOSS_SPIDER_SPIT_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_ARROW_LEFT
				tl31Sprite = "crossbow_arrow_1"
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveEnemies1b", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_GRUNT_CROSSBOW_ARROW_WIDTH, cfTWS_GRUNT_CROSSBOW_ARROW_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_ARROW_RIGHT
				tl31Sprite = "crossbow_arrow_1"
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveEnemies1b", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(-cfTWS_GRUNT_CROSSBOW_ARROW_WIDTH, cfTWS_GRUNT_CROSSBOW_ARROW_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_FIREBALL_LEFT
				tl31Sprite = "castle_castergrunt_fx_bolt"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_GRUNT_CASTER_FIREBALL_WIDTH, cfTWS_GRUNT_CASTER_FIREBALL_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_FIREBALL_RIGHT
				tl31Sprite = "castle_castergrunt_fx_bolt"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(-cfTWS_GRUNT_CASTER_FIREBALL_WIDTH, cfTWS_GRUNT_CASTER_FIREBALL_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_COINS_LEFT
				tl31Sprite = "forest_leprechaun_fx_coin_spread"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_LEPRECHAUN_COINS_WIDTH, cfTWS_LEPRECHAUN_COINS_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_COINS_RIGHT
				tl31Sprite = "forest_leprechaun_fx_coin_spread"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(-cfTWS_LEPRECHAUN_COINS_WIDTH, cfTWS_LEPRECHAUN_COINS_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
			CASE TWS_FX_VENOM_PUDDLE
				tl31Sprite = "cave_boss_poison_puddle_"
				tl31Sprite += (sFXData[i].iSpriteBlastAnimFrame + 1)
				CDEBUG3LN(DEBUG_MINIGAME, "[TWS_ARCADE] [GD] : Rendering FX: ",tl31Sprite)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", tl31Sprite, INIT_VECTOR_2D(sFXData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(), sFXData[i].vSpritePos.y + fShake), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_FX_BOSS_SPIDER_VENOM_WIDTH, cfTWS_FX_BOSS_SPIDER_VENOM_HEIGHT),fFactor), 0, sTWSData.rgbaSprite)
			
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC TWS_DRAW_NPC(INT i)
	
	FLOAT fFactor = cfTWS_RESCALE_FACTOR
	FLOAT fEnemyPosX
	TEXT_LABEL_31 tl23Sprite
	
	IF sNPCData[i].bIsActive

		SWITCH sNPCData[i].eNPCType
			CASE TWS_NPC_TWINS
				tl23Sprite = "npc_swamp_2girls"
			BREAK
			CASE TWS_NPC_WOMAN
				tl23Sprite = "npc_forest_girl"
			BREAK
			CASE TWS_NPC_VILLAGER
				tl23Sprite = "npc_forest_villager"
			BREAK
		ENDSWITCH
				
		IF sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0
			
			IF sNPCData[i].eNPCType = TWS_NPC_VILLAGER
				fEnemyPosX = sNPCData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(1.0, sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos)
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fEnemyPosX, sNPCData[i].vSpritePos.y + sNPCData[i].vSpriteSize.Y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ELSE
				fEnemyPosX = sNPCData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(1.0, sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos + 50)
			ENDIF
		ELSE
			fEnemyPosX = sNPCData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER()
			IF sNPCData[i].eNPCType = TWS_NPC_VILLAGER
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fEnemyPosX, sNPCData[i].vSpritePos.y + sNPCData[i].vSpriteSize.Y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
			ENDIF
		ENDIF
		
		IF sNPCData[i].fHealth > 0.0
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveThog3NPC", tl23Sprite, INIT_VECTOR_2D(fEnemyPosX, sNPCData[i].vSpritePos.y), INIT_VECTOR_2D(sNPCData[i].vSpriteSize.X*fFactor, sNPCData[i].vSpriteSize.Y*fFactor), 0, sTWSData.rgbaSprite)
		
		ENDIF
	ENDIF

ENDPROC

PROC TWS_DRAW_ENEMY(INT i)
	
	BOOL bFacingRight
	FLOAT fFactor = cfTWS_RESCALE_FACTOR
	FLOAT fEnemyPosX
	VECTOR_2D vOffset
	TEXT_LABEL_63 tl23Sprite, t123enemyType
	TEXT_LABEL_63 tl23Dictionary
	
	IF sEnemyData[i].bIsActive
		
		vOffset = sEnemyData[i].vSpriteOffset
		bFacingRight = FALSE
		
		IF sEnemyData[i].vSpritePos.x < sPlayerData.vSpritePos.x AND sEnemyData[i].bIsActive
		AND NOT (sEnemyData[i].eEnemyState = TWS_ENEMY_DEAD
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_FLAT
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_FALL
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_1
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_2
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_3
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_4
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_5
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_6
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_7
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_DYING_8
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_EXPLODING
			OR sEnemyData[i].eEnemyType = TWS_ENEMY_SNAKE)
			bFacingRight = TRUE
		ENDIF
		
		SWITCH sEnemyData[i].eEnemyType
			CASE TWS_ENEMY_GRUNT
				t123enemyType = "cave_grunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies3b"
			BREAK
			CASE TWS_ENEMY_GRUNT_SWORD
				t123enemyType = "forest_swordgrunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies1b"
			BREAK
			CASE TWS_ENEMY_GRUNT_FIRESWORD
				t123enemyType = "castle_swordgrunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies4b"
			BREAK
			CASE TWS_ENEMY_GRUNT_FIRESPEAR
				t123enemyType = "castle_speargrunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies4b"
			BREAK
			CASE TWS_ENEMY_GRUNT_SPEAR
				t123enemyType = "forest_speargrunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies1b"
			BREAK
			CASE TWS_ENEMY_GRUNT_CROSSBOW
				t123enemyType = "forest_crossbowgrunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies1b"
			BREAK
			CASE TWS_ENEMY_GRUNT_CASTER
				t123enemyType = "castle_castergrunt_"
				tl23Dictionary = "MPWizardsSleeveEnemies4b"
			BREAK
			CASE TWS_ENEMY_BRUTE
				t123enemyType = "cave_brute_"
				tl23Dictionary = "MPWizardsSleeveEnemies3b"
			BREAK
			CASE TWS_ENEMY_BOULDER
				t123enemyType = "castle_boulder"
				tl23Dictionary = "MPWizardsSleeveEnemies4b"
			BREAK
			CASE TWS_ENEMY_BRUTE_AXE
				t123enemyType = "swamp_brute_"
				tl23Dictionary = "MPWizardsSleeveEnemies2"
			BREAK
			CASE TWS_ENEMY_BOSS_SPIDER
				t123enemyType = "cave_boss_"
				tl23Dictionary = "MPWizardsSleeveEnemies3a"
			BREAK
			CASE TWS_ENEMY_BOSS_AMAZON
				t123enemyType = "swamp_boss_"
				tl23Dictionary = "MPWizardsSleeveEnemies2"
			BREAK
			CASE TWS_ENEMY_BOSS_KNIGHT
			CASE TWS_ENEMY_KNIGHT
				t123enemyType = "forest_boss_"
				tl23Dictionary = "MPWizardsSleeveEnemies1a"
			BREAK
			CASE TWS_ENEMY_BOSS_WIZARD
			CASE TWS_ENEMY_FAKE_WIZARD
			CASE TWS_ENEMY_DIALOGUE_WIZARD
				t123enemyType = "boss_wizard_"
				tl23Dictionary = "MPWizardsSleeveEnemies4a"
			BREAK
			CASE TWS_ENEMY_BAT
				t123enemyType = "cave_bat_"
				tl23Dictionary = "MPWizardsSleeveEnemies3a"
			BREAK
			CASE TWS_ENEMY_FAIRY
				t123enemyType = "forest_fairy_"
				tl23Dictionary = "MPWizardsSleeveEnemies1b"
			BREAK
			CASE TWS_ENEMY_LEPRECHAUN
				t123enemyType = "forest_leprechaun_"
				tl23Dictionary = "MPWizardsSleeveEnemies1b"
			BREAK
			CASE TWS_ENEMY_SPIDER
				t123enemyType = "cave_spider_"
				tl23Dictionary = "MPWizardsSleeveEnemies3a"
			BREAK
			CASE TWS_ENEMY_SLIME
				t123enemyType = "swamp_slime_"
				tl23Dictionary = "MPWizardsSleeveEnemies2"
			BREAK
			CASE TWS_ENEMY_SNAKE
				t123enemyType = "swamp_snake_"
				tl23Dictionary = "MPWizardsSleeveEnemies2"
			BREAK
		ENDSWITCH
				
		SWITCH sEnemyData[i].eEnemyState
			CASE TWS_ENEMY_INACTIVE
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = t123enemyType
					tl23Sprite += "knee1"
					
				ELSE
					IF sEnemyData[i].eEnemyType = TWS_ENEMY_FAKE_WIZARD
						tl23Sprite = "boss_wizard_doppelganger_idle"
					ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_GRUNT_CASTER
						tl23Sprite = "castle_castergrunt_idle-walk"
					ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SNAKE
						tl23Sprite = "swamp_snake_idle-walk"
					ELSE
						tl23Sprite = t123enemyType
						tl23Sprite += "idle"
					ENDIF
					
					IF sEnemyData[i].eEnemyType != TWS_ENEMY_BAT
						tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					ENDIF
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_IDLE
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_FAKE_WIZARD
					tl23Sprite = "boss_wizard_doppelganger_idle"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_GRUNT_CASTER
					tl23Sprite = "castle_castergrunt_idle-walk"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SNAKE
					tl23Sprite = "swamp_snake_idle-walk"
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "idle"
				ENDIF
				
				IF sEnemyData[i].eEnemyType != TWS_ENEMY_BAT
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_ATTACK
			CASE TWS_ENEMY_ATTACK_LOAD
			CASE TWS_ENEMY_ATTACK_HIT
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = t123enemyType
					tl23Sprite += "slash"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "attack_thrust"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
					IF sEnemyData[i].eEnemyState = TWS_ENEMY_ATTACK_HIT
						bFacingRight = !sEnemyData[i].bIsMoving
					ENDIF
					
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "attack"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
			
			CASE TWS_ENEMY_SPECIAL_ATTACK
			CASE TWS_ENEMY_SPECIAL_ATTACK_3
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "jumpattack-divekick"
					bFacingRight = !sEnemyData[i].bIsMoving
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
					tl23Sprite = t123enemyType
					tl23Sprite += "attack"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "slam"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
			BREAK
			
			CASE TWS_ENEMY_SPECIAL_ATTACK_2
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "attack_stab"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
					tl23Sprite = t123enemyType
					tl23Sprite += "attack"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
				ELSE
					tl23Sprite = "forest_boss_slam11"
				ENDIF
				
			BREAK
			
			CASE TWS_ENEMY_ATTACK_WAIT
				tl23Sprite = t123enemyType
				tl23Sprite += "attack5"
			BREAK
			
			CASE TWS_ENEMY_TELEPORT_IN
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "jumpattack-jump_upward"
					
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "teleport_in"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_TELEPORT_OUT
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "jumpattack-jump_downward"
					
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "teleport_out"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_SPIT_1
			CASE TWS_ENEMY_SPIT_2
			CASE TWS_ENEMY_SPIT_3
			
				tl23Sprite = t123enemyType
				tl23Sprite += "spit"
				tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				
			BREAK
			CASE TWS_ENEMY_GO_UP
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER
					tl23Sprite = t123enemyType
					tl23Sprite += "climb1-in air"
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = t123enemyType
					tl23Sprite += "knee"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "jumpattack-jump_upward"
				
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "spawn"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF

			BREAK
			CASE TWS_ENEMY_FALL_OVER_1
			CASE TWS_ENEMY_FALL_OVER_2
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "climb1-in air"
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_LANDING
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = t123enemyType
					tl23Sprite += "jumpattack-land"
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "climb2-land"
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_FALL
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_LEPRECHAUN
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SNAKE
					tl23Sprite = t123enemyType
					tl23Sprite += "kill"
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt_fall"
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_FLAT
				
				tl23Sprite = t123enemyType
				tl23Sprite += "hurt_flat"

			BREAK
			CASE TWS_ENEMY_GETTING_UP
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt_getup_"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_STUN
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_GRUNT_CASTER
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_SLIME
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BRUTE_AXE
					tl23Sprite = t123enemyType
					tl23Sprite += "stun"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt1"
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt_stun"
				ENDIF
				
				IF SIN(GET_GAME_TIMER()/0.3) > 0.0
					vOffset.x -= cfTWS_ENEMY_HIT_OFFSET
				ELSE
					vOffset.x += cfTWS_ENEMY_HIT_OFFSET
				ENDIF
			BREAK
			CASE TWS_ENEMY_EXPLODING
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
					tl23Sprite = t123enemyType
					tl23Sprite += "dying"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt"
				ENDIF
			BREAK
			CASE TWS_ENEMY_DYING
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
					tl23Sprite = t123enemyType
					tl23Sprite += "death"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_LEPRECHAUN
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_GRUNT_CASTER
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_SLIME
					tl23Sprite = t123enemyType
					tl23Sprite += "kill"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
				ElIF sEnemyData[i].eEnemyType = TWS_ENEMY_SNAKE
					tl23Sprite = t123enemyType
					tl23Sprite += "kill"
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "dead"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_DYING_1
			CASE TWS_ENEMY_DYING_2
			CASE TWS_ENEMY_DYING_3
			CASE TWS_ENEMY_DYING_4
			CASE TWS_ENEMY_DYING_5
			CASE TWS_ENEMY_DYING_6
			CASE TWS_ENEMY_DYING_7
			CASE TWS_ENEMY_DYING_8
				
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
					tl23Sprite = "boss_wizard_death15"
				ELSE
					tl23Sprite = t123enemyType
					tl23Sprite += "kill"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
					
					vOffset = TWS_GET_KNIGHT_DEATH_OFFSET(sEnemyData[i].eEnemyState)
				ENDIF
			BREAK
			CASE TWS_ENEMY_DEAD
			
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BAT
					tl23Sprite = "cave_bat_die"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_FAIRY
					tl23Sprite = "forest_fairy_kill"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SPIDER
					tl23Sprite = t123enemyType
					tl23Sprite += "hurt_dead"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
				OR sEnemyData[i].eEnemyType = TWS_ENEMY_KNIGHT
					tl23Sprite = "forest_boss_kill12"
					vOffset = TWS_GET_KNIGHT_DEATH_OFFSET(sEnemyData[i].eEnemyState)
					
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
					tl23Sprite = "swamp_boss_hurt_flat"
				
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER
					tl23Sprite = "cave_boss_dead"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
					tl23Sprite = "boss_wizard_death17"
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_GLIDE_DOWN
				
				tl23Sprite = t123enemyType
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BAT
					tl23Sprite += "glide_down"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SLIME
					tl23Sprite += "jumpattack6"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_FAIRY
					tl23Sprite += "idle"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER
					tl23Sprite += "climb1-in air"
				ELSE
					tl23Sprite += "web"
				ENDIF
				
			BREAK
			CASE TWS_ENEMY_WALK
			
				tl23Sprite = t123enemyType
				IF sEnemyData[i].eEnemyType = TWS_ENEMY_BAT
					tl23Sprite += "fly"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_FAIRY
					tl23Sprite += "attack"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER
					tl23Sprite += "climb1-in air"
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_GRUNT_CASTER
					tl23Sprite += "idle-walk"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SNAKE
					tl23Sprite += "idle-walk"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_SLIME
					tl23Sprite += "jumpattack"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELIF sEnemyData[i].eEnemyType = TWS_ENEMY_BOULDER
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ELSE
					tl23Sprite += "walk"
					tl23Sprite += (sEnemyData[i].iSpriteAnimFrame + 1)
				ENDIF
				
			BREAK
		ENDSWITCH
		
		IF TWS_SHOULD_ENEMY_BE_DRAWN(i)
		AND (sEnemyData[i].fHealth > 0.0
		OR (sEnemyData[i].fHealth <= 0.0 AND SIN(GET_GAME_TIMER()/0.5) > 0)
		OR (sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER
		OR sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_AMAZON
		OR sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_KNIGHT
		OR sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_WIZARD
		OR sEnemyData[i].eEnemyType = TWS_ENEMY_SPIDER))
		
			// Calculate X
			IF sPlayerData.vSpritePos.x >= sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos OR sTWSData.eLevelStage >= TWS_LEVEL_BOSS_FIGHT AND sPlayerData.iLifes > 0
		
				fEnemyPosX = sEnemyData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER(1.0, sBackgroundTilesData[iCurrentBackgroundTileToAdd-1].fSpritePos + 50)

			ELSE
				fEnemyPosX = sEnemyData[i].vSpritePos.x + TWS_GET_OFFSET_TO_PLAYER()
			ENDIF
			
			// Spider web
			IF (sEnemyData[i].eEnemyType = TWS_ENEMY_BOSS_SPIDER OR sEnemyData[i].eEnemyType = TWS_ENEMY_SPIDER)
			AND (sEnemyData[i].eEnemyState = TWS_ENEMY_GLIDE_DOWN
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_GO_UP
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_FALL_OVER_1
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_FALL_OVER_2
			OR sEnemyData[i].eEnemyState = TWS_ENEMY_LANDING)
			
				INT w,  iNumWebTiles = ROUND(sEnemyData[i].vSpritePos.y / cfTWS_WEB_HEIGHT)
				FOR w = 0 TO iNumWebTiles
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "web_line", INIT_VECTOR_2D(fEnemyPosX, w * cfTWS_WEB_HEIGHT), INIT_VECTOR_2D(cfTWS_WEB_WIDTH*fFactor, cfTWS_WEB_HEIGHT*fFactor), 0, sTWSData.rgbaSprite)
				ENDFOR
			ENDIF
			
			//Shadow
			IF sEnemyData[i].fHealth > 0.0 AND sEnemyData[i].vShadowOffset.y != -1.0
				IF sEnemyData[i].vShadowOffset.y != 0.0
					IF sEnemyData[i].iEnemySize >= 2
						FLOAT fDistance = sEnemyData[i].vShadowOffset.y - sEnemyData[i].vSpritePos.y
						fDistance = 1/(CLAMP(fDistance, TWS_GET_ENEMY_SHADOW_Y_POSITION(sEnemyData[i].eEnemyType), 600)/600)
						
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fEnemyPosX + TWS_GET_ENEMY_SHADOW_X_OFFSET(i,bFacingRight), sEnemyData[i].vShadowOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor*fDistance, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor*fDistance), 0, sTWSData.rgbaBlackTransparent)
					ELSE
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fEnemyPosX + TWS_GET_ENEMY_SHADOW_X_OFFSET(i,bFacingRight), sEnemyData[i].vShadowOffset.y) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_MED_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
					ENDIF
				ELSE
					IF sEnemyData[i].iEnemySize > 2
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow",  INIT_VECTOR_2D(fEnemyPosX + TWS_GET_ENEMY_SHADOW_X_OFFSET(i,bFacingRight), sEnemyData[i].vSpritePos.y + TWS_GET_ENEMY_SHADOW_Y_POSITION(sEnemyData[i].eEnemyType)) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_EXTRA_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_EXTRA_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
					
					ELIF sEnemyData[i].iEnemySize = 2
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_amazon_shadow",  INIT_VECTOR_2D(fEnemyPosX + TWS_GET_ENEMY_SHADOW_X_OFFSET(i,bFacingRight), sEnemyData[i].vSpritePos.y + TWS_GET_ENEMY_SHADOW_Y_POSITION(sEnemyData[i].eEnemyType)) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_AMAZON_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
					
					ELIF sEnemyData[i].iEnemySize = 1
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_player-enemy_shadow", INIT_VECTOR_2D(fEnemyPosX + TWS_GET_ENEMY_SHADOW_X_OFFSET(i,bFacingRight), sEnemyData[i].vSpritePos.y + TWS_GET_ENEMY_SHADOW_Y_POSITION(sEnemyData[i].eEnemyType)) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_LARGE_WIDTH*fFactor, cfTWS_FX_SHADOW_LARGE_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
					
					ELSE
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveItemsAndFX", "fx_item_medium_shadow", INIT_VECTOR_2D(fEnemyPosX + TWS_GET_ENEMY_SHADOW_X_OFFSET(i,bFacingRight), sEnemyData[i].vSpritePos.y + TWS_GET_ENEMY_SHADOW_Y_POSITION(sEnemyData[i].eEnemyType)) , INIT_VECTOR_2D(cfTWS_FX_SHADOW_MED_WIDTH*fFactor, cfTWS_FX_SHADOW_MED_HEIGHT*fFactor), 0, sTWSData.rgbaBlackTransparent)
					ENDIF
				ENDIF
			ENDIF
			
			IF bFacingRight
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl23Dictionary, tl23Sprite, ADD_VECTOR_2D(INIT_VECTOR_2D(-vOffset.x, vOffset.y), INIT_VECTOR_2D(fEnemyPosX, sEnemyData[i].vSpritePos.y)), INIT_VECTOR_2D(-sEnemyData[i].vSpriteSize.X*fFactor, sEnemyData[i].vSpriteSize.Y*fFactor), 0, sTWSData.rgbaSprite)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE(tl23Dictionary, tl23Sprite, ADD_VECTOR_2D(vOffset, INIT_VECTOR_2D(fEnemyPosX, sEnemyData[i].vSpritePos.y)), INIT_VECTOR_2D(sEnemyData[i].vSpriteSize.X*fFactor, sEnemyData[i].vSpriteSize.Y*fFactor), 0, sTWSData.rgbaSprite)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC TWS_DRAW_TITLE_ANIM()
	
	TEXT_LABEL_23 tl23Sprite
	
	SWITCH sTWSData.sCutsceneData.eTitleCutsceneStage
	
	CASE TWS_TITLE_ANIM_STRIKE_1
		
		tl23Sprite = "lightning_strike1_"
		tl23Sprite += (sTWSData.sCutsceneData.iSpriteCutsceneAnimFrame + 1)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntro", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	
	BREAK
	CASE TWS_TITLE_ANIM_STRIKE_1_PAUSE
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
	BREAK
	CASE TWS_TITLE_ANIM_STRIKE_2
		
		tl23Sprite = "lightning_strike2_"
		tl23Sprite += (sTWSData.sCutsceneData.iSpriteCutsceneAnimFrame + 1)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntro", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	
	BREAK
	CASE TWS_TITLE_ANIM_STRIKE_2_PAUSE
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
	BREAK
	CASE TWS_TITLE_ANIM_FINAL_STRIKE
		
		tl23Sprite = "lightning_strike3_"
		tl23Sprite += (sTWSData.sCutsceneData.iSpriteCutsceneAnimFrame + 1)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntro", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	
	BREAK
	
	ENDSWITCH
ENDPROC

PROC TWS_DRAW_TITLE_BACKGROUND()

	INT i
	TEXT_LABEL_31 tl23Sprite
	tl23Sprite = "title_bg_scroll"
	
	FOR i = 0 TO 1
		
		IF sTWSData.fMenuBackgroundPosX[i] < -cfTWS_MENU_BACKGROUND_TILE_WIDTH/2
			sTWSData.fMenuBackgroundPosX[i] = cfTWS_MENU_BACKGROUND_TILE_WIDTH + cfTWS_MENU_BACKGROUND_TILE_WIDTH/2 - 4
		ENDIF
		
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntroMenu", tl23Sprite, INIT_VECTOR_2D(sTWSData.fMenuBackgroundPosX[i], cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTWS_MENU_BACKGROUND_TILE_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
		sTWSData.fMenuBackgroundPosX[i] -= 4.0
		
	ENDFOR
	
ENDPROC

PROC TWS_DRAW_TITLE_SCREEN()
	
	TEXT_LABEL_31 tl23Sprite
	
	TWS_DRAW_TITLE_BACKGROUND()
	
	IF sTWSData.eTitleStage = TWS_TITLE_PRESS_START
		
		TWS_DRAW_FADE()
		
		IF SIN(GET_GAME_TIMER()/2.0) > 0.0
			tl23Sprite = "Wizards_ruin_Title_Screen1"
		ELSE
			tl23Sprite = "Wizards_ruin_Title_Screen2"
		ENDIF
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntroMenu", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	
	ELSE
	
		tl23Sprite = "Wizards_ruin_Title_Screen1"
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntroMenu", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
		
		
		IF sTWSData.eTitleStage != TWS_TITLE_OPTION_BLINKING OR SIN(GET_GAME_TIMER()/0.4) > 0.0

			tl23Sprite = "option_scroll_"
			IF sTWSData.iMenuOptionSpriteAnim < 9
				tl23Sprite += "0"
			ENDIF
			tl23Sprite += (sTWSData.iMenuOptionSpriteAnim + 1)
			
			IF NOT sTWSData.bNormalModeIsCompleted AND sTWSData.iMenuOptionSpriteAnim > 8 AND sTWSData.iMenuOptionSpriteAnim < 14
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntroMenu", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/5*4), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_TITLE_OPTION_WIDTH, cfTWS_HUD_TITLE_OPTION_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaBlackTransparent)
			ELSE
				ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntroMenu", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/5*4), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_TITLE_OPTION_WIDTH, cfTWS_HUD_TITLE_OPTION_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
			ENDIF
			
		ELSE
		
			tl23Sprite = "select_arrows"
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntroMenu", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/5*4), MULTIPLY_VECTOR_2D(INIT_VECTOR_2D(cfTWS_HUD_TITLE_ARROWS_WIDTH, cfTWS_HUD_TITLE_ARROWS_HEIGHT), cfTWS_RESCALE_FACTOR), 0, sTWSData.rgbaSprite)
	
		ENDIF
		
		TWS_DRAW_FADE()
		
	ENDIF
ENDPROC

PROC TWS_DRAW_CUTSCENE()

	IF NOT IS_STRING_EMPTY(sTWSData.sCutsceneData.sCutsceneName)
		TEXT_LABEL_31 tl23Sprite = sTWSData.sCutsceneData.sCutsceneName
		tl23Sprite += (sTWSData.sCutsceneData.iSpriteCutsceneAnimFrame + 1)
		
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	ENDIF
	
ENDPROC

PROC TWS_DRAW_MAP_SCREEN()

	TEXT_LABEL_23 tl23Sprite = TWS_GET_MAP_SCREEN_TEXTURE()
	
	IF NOT IS_STRING_EMPTY(tl23Sprite)
		ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveHUDAndScreen", tl23Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	ENDIF
	
ENDPROC

PROC TWS_DRAW_FRONT_FX()

	ARCADE_GAMES_POSTFX_DRAW()
	
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2 - cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2 + cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
	ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveFacade", "facade", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	
	
ENDPROC

PROC TWS_UPDATE_DRAW_CALLS_INDEXES()
	
	INT i
	
	// For each element check if the contiguous are lower and higher
	FOR i = 0 TO ciTWS_MAX_DRAW_CALLS-1
		
		IF i = 0
		
//			WHILE sTWSData.sDrawCalls[i].fZIndex > sTWSData.sDrawCalls[i+1].fZIndex
			IF sTWSData.sDrawCalls[i].fZIndex > sTWSData.sDrawCalls[i+1].fZIndex
				TWS_SWAP_DRAW_CALLS(i, i+1)
			ENDIF
			
		ELIF i = ciTWS_MAX_DRAW_CALLS-1
		
//			WHILE sTWSData.sDrawCalls[i].fZIndex < sTWSData.sDrawCalls[i-1].fZIndex
			IF sTWSData.sDrawCalls[i].fZIndex < sTWSData.sDrawCalls[i-1].fZIndex
				TWS_SWAP_DRAW_CALLS(i, i-1)
			ENDIF
		ELSE
			
			IF sTWSData.sDrawCalls[i].fZIndex < sTWSData.sDrawCalls[i-1].fZIndex
				TWS_SWAP_DRAW_CALLS(i, i-1)
			ELIF sTWSData.sDrawCalls[i].fZIndex > sTWSData.sDrawCalls[i+1].fZIndex
				TWS_SWAP_DRAW_CALLS(i, i+1)
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC TWS_DRAW_ENTITIES()

	INT i
	
	// For each element check if the contiguous are lower and higher
	FOR i = 0 TO ciTWS_MAX_DRAW_CALLS-1

		SWITCH sTWSData.sDrawCalls[i].eType
		CASE TWS_ENTITY_ENEMY
			TWS_DRAW_ENEMY(sTWSData.sDrawCalls[i].iIndex)
		BREAK
		CASE TWS_ENTITY_PLAYER
			TWS_DRAW_PLAYER()
		BREAK
		CASE TWS_ENTITY_FX
			TWS_DRAW_FX(sTWSData.sDrawCalls[i].iIndex)
		BREAK
		CASE TWS_ENTITY_TRAP
			TWS_DRAW_TRAP(sTWSData.sDrawCalls[i].iIndex)
		BREAK
		CASE TWS_ENTITY_ITEM
			TWS_DRAW_ITEM(sTWSData.sDrawCalls[i].iIndex)
		BREAK
		CASE TWS_ENTITY_NPC
			TWS_DRAW_NPC(sTWSData.sDrawCalls[i].iIndex)
		BREAK
		ENDSWITCH
	ENDFOR
ENDPROC

PROC TWS_DRAW_FIB_WARNING()
	
	ARCADE_DRAW_PIXELSPACE_SPRITE("MPWizardsSleeveIntro", "ARCADE_FIB_SCREEN", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sTWSData.rgbaSprite)
	
ENDPROC

PROC TWS_DRAW_CLIENT_STATE(TWS_ARCADE_CLIENT_STATE eStateToDraw = TWS_ARCADE_CLIENT_STATE_PLAYING)
	
	
	SWITCH eStateToDraw
		
		CASE TWS_ARCADE_CLIENT_STATE_FIB_WARNING
			
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
			TWS_DRAW_FIB_WARNING()
			
			TWS_DRAW_FADE()
			
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_PIXTRO_LOGO

			
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_TITLE_ANIM
			
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
			TWS_DRAW_TITLE_ANIM()
			
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_TITLE_SCREEN
			
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
		
			TWS_DRAW_TITLE_SCREEN()
			
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_MAP_SCREEN
			
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
			TWS_DRAW_MAP_SCREEN()
			
			TWS_DRAW_FADE()
			
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_INTRO_CUTSCENE
		CASE TWS_ARCADE_CLIENT_STATE_OUTRO_CUTSCENE
			
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
			TWS_DRAW_CUTSCENE()
			
			TWS_DRAW_FADE()
			
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_PLAYING
			
			TWS_UPDATE_DRAW_CALLS_INDEXES()
			
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
			TWS_DRAW_BACKGROUND()
			
			TWS_DRAW_ENTITIES()
			
			TWS_DRAW_FOREGROUND()
			
			TWS_DRAW_HUD()
						
		BREAK
		
		CASE TWS_ARCADE_CLIENT_STATE_LEADERBOARD
						
			// Draw black and grey background
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
			TWS_DRAW_TITLE_BACKGROUND()
			
			ARCADE_GAMES_LEADERBOARD_DRAW()
			
			TWS_DRAW_FADE()	
		BREAK
		
		DEFAULT
			
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sTWSData.rgbaBlack)
			
		BREAK
	ENDSWITCH
	
	TWS_DRAW_FRONT_FX()
	
ENDPROC
