// *****************************************************************************************
// *****************************************************************************************
//
//		FILE NAME		:	rampageV4_include.sch
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Shared Structs and Functions For Rampages
//		NOTES			:   Starting to split rampage stuff up - but keeping old stuff
//
// *****************************************************************************************
// *****************************************************************************************

//CONST_INT IS_JAPANESE_BUILD 1		/// NEVER COMMENT THIS IN - THIS IS SO I CAN TEST FIX FOR B*2163318
   								 
//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Leadin_public.sch"
USING "completionpercentage_public.sch"
USING "randomChar_private.sch"
USING "shop_public.sch"

USING "mission_control_public.sch"
USING "flow_public_core_override.sch"
USING "stats_public.sch"
USING "event_public.sch"
USING "cutscene_public.sch"
USING "player_ped_public.sch"
USING "mission_stat_public.sch"
USING "shared_hud_displays.sch"
USING "finance_control_public.sch"
USING "initial_scenes_rampage.sch"
USING "achievement_public.sch"
USING "area_checks.sch"
USING "fake_cellphone_public.sch"
USING "cheat_controller_public.sch"

#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
#ENDIF

//----------------------
//	ENUMS
//----------------------
ENUM eRampageSpawnState
	ERS_SPAWNING_ON,			// spawning is allowed until maxActivePeds = MAX_ACTIVE_PEDS
	ERS_SPAWNING_LIMIT_BREAK,	// maxActivePeds = MAX_ACTIVE_PEDS. Set Timer and got into ERS_SPAWNING_WAIT
	ERS_SPAWNING_WAIT			// spawning is paused while maxActivePeds < spawnlimit
ENDENUM

ENUM RAMPAGECAR_MODE 
	RCM_RAM,
	RCM_DRIVEBY,
	RCM_DEPLOY,
	RCM_DEPLOY_ALL,
	RCM_DEPLOY_COORDS,
	RCM_TANK
ENDENUM

ENUM RAMPAGECAR_MODESTATE
	RCMS_STARTDRIVE,
	RCMS_DRIVING,
	RCMS_DEPLOY,
	RCMS_DRIVEOFF
ENDENUM

ENUM eRAMPAGE_CHALLENGE
	CHALLENGE_NONE,
	CHALLENGE_OVERKILL,
	CHALLENGE_HEADSHOT,
	CHALLENGE_KILLSTREAK,
	CHALLENGE_VEHICLES,
	CHALLENGE_OCCUPANT,
	CHALLENGE_CHOPPER,
	CHALLENGE_EXECUTION,
	CHALLENGE_LONGSHOT,
	CHALLENGE_TANK
ENDENUM

ENUM SFORMRAMPAGE_STATE
	SFR_LEADIN,
	SFR_MOCAP,
	SFR_INTROSTART,
	SFR_INTRODONE,
	SFR_RAMPAGEON,
	SFR_OUTROSTART,
	SFR_OUTROWAIT,
	SFR_OUTRODONE_FAIL,
	SFR_OUTRO_ANIM_OUT,
	SFR_OUTRODONE
ENDENUM

ENUM TANK_STATE
	TANK_INIT,
	TANK_DRIVE_TO_PLAYER,
	TANK_FIRE_MISS_SHOT,
	TANK_CHECK_FOR_SHOT,
	TANK_WAIT_BETWEEN_SHOTS,
	TANK_PREP_FOR_NEXT_SHOT,
	TANK_FIRE_HIT_SHOT,
	TANK_IDLE
ENDENUM

//----------------------
//	STRUCTS
//----------------------
STRUCT RAMPAGE_AREA
	BOOL bIsActive = FALSE
	VECTOR vPosition[2]
	FLOAT fWidth
	
	// these are calculated in setup and are used for point generation
	VECTOR vForward
	VECTOR vRight
	FLOAT fLength
	FLOAT fHeight
	
	// debug stuff so we can alter these in RAG in real time
	#IF IS_DEBUG_BUILD
		BOOL bRecalculate
		BOOL bShowDebug
	#ENDIF
ENDSTRUCT

STRUCT RAMPAGE_FORTRESS
	BOOL bIsActive = FALSE
	RAMPAGE_AREA spawnArea			// this is the area where we pick are spawn points
	RAMPAGE_AREA defenseArea		// this is the defensive area - generally bigger than the spawn area
	FLOAT fRoofHeight = 0.0			// any positions above this height are taken as roofs

	INT iPedCount = 0				// the number of peds who are tied to this fortress
	INT iSpawnedCount = 0			// the number of peds spawned from this fortress
	INT iMaxSpawnCount = -1			// the maximum number of peds to spawn until we shut down fortress [-1 for infinite spawning]
	
	FLOAT fRoofFactor = 0.0			// chance of allowing roof peds - [0 - none, 1 - all]
	FLOAT fRushFactor = 0.0			// chance of peds abandoning defensive area and using player defensive area - [0 - none, 1 - all]
	
	INT iMaxActivePeds = -1			// this override the maximum amount of peds while this fortress is active [-1 - don't override]
	INT iMaxActiveCars = -1 		// this override the maximum amount of cars allowed while fortress is open
	
	INT iNextFortress = -1			// when this fortress closes open the next one [-1 = don't open another fortress]
	
	// debug stuff so we can alter these in RAG in real time
	#IF IS_DEBUG_BUILD
		BOOL bRecalculate
		VECTOR vLastSpawnPoint
		VECTOR vTestSpawnPoint
		BOOL bGenerateTestSpawnPoint
		BOOL bShowDebug
		BOOL bShowDebugTestSpawnPoint
	#ENDIF
ENDSTRUCT

STRUCT RAMPAGE_INFO
	STATIC_BLIP_NAME_ENUM blipNameEnum
	INT iRampageIndex 
	
	STRING sTargetName
	STRING sDialogBlockName 
	STRING sDialogRootLabel 
	STRING sUIName
	
	STRING sScenarioGroupName
	VECTOR vScenarioBlockCenter
	FLOAT fScenarioBlockRadius
	
	STRING sIntroAnim
	STRING sOutroAnim
	STRING sOutroRunAnim
	FLOAT fOutroPhase
	FLOAT fOutroRunPhase
	
	INT iTimeLimit = 180000

	WEAPON_TYPE plyrWeapon[3]
	WEAPON_TYPE enemyWeapon[2]
	WEAPONCOMPONENT_TYPE plyrWeaponComp[2]
	
	MODEL_NAMES pedModels[4]
	MODEL_NAMES carModels[3]
	MODEL_NAMES	explosiveModel
	
	INT iMaxPedModels = 0
	INT iMaxCarModels = 0
	
	VECTOR vPlyrCarHidePos
	FLOAT fPlyrCarHideHeading = 0.0
	
	VECTOR vPlyrStartPos
	FLOAT fPlyrStartHeading = 0.0
	
	FLOAT fAmbientPedDensity = 0.0
	FLOAT fAmbientCarDensity = 0.0
	
	//Chance that a car will be tasked to ram the player before deploying its passengers
	FLOAT fCarRamChance = 0.0
	
	BOOL bPassed = FALSE
	BOOL bAllowInnocents = FALSE
	BOOL bExplosiveProps = FALSE
	
	INT iTargetAmount
	
	INT iRandomSeed = 0			// so i can repeat the randomness of the rampage if necessary
ENDSTRUCT

STRUCT RAMPAGE_CAR
	VEHICLE_INDEX carID

	BOOL bKeepAsCover = FALSE	// if this is set on cleanup if the car is okay it will add it to the list of cover cars

	INT iOccupantIndex[6]
	FLOAT fDistToPlayer
	FLOAT fOriginalDist
	
	RAMPAGECAR_MODE rcMode
	INT iTaskState = 0
	INT iIndex = 0
	
	VECTOR vDestination		// if this is zero we assume we're going to player
	FLOAT fCruiseSpeed
	FLOAT fTargetReachDist	
	INT iTimer 
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugAliveCheck = FALSE 
	#ENDIF
ENDSTRUCT

STRUCT RAMPAGE_TANK
	PED_INDEX pedID
	BLIP_INDEX blipID
	VEHICLE_INDEX vehicleID

	FLOAT fPlayerDist = 0.0
	FLOAT fOriginalDist = 0.0
	
	INT iShotTimer = 0
	
	TANK_STATE eTankState
	
	VECTOR vTargetPos
ENDSTRUCT

STRUCT RAMPAGE_PED
	PED_INDEX pedID
	BLIP_INDEX blipID
	BOOL bWasInVehicle = FALSE
	BOOL bIsFleePed = FALSE
	BOOL bMolotovThrower = FALSE
	FLOAT fPlayerDist = 0.0
	FLOAT fOriginalDist = 0.0
	INT iFortIndex = -1
	INT iDeleteTime
	INT iIndex = 0
	
	STRING sVoice = NULL
	STRING sContext = NULL
	INT iInsultTimer = 0
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugAliveCheck = FALSE 
	#ENDIF
	
ENDSTRUCT

STRUCT RAMPAGE_EXPLOSIVE_PROP
	OBJECT_INDEX objID
	VECTOR pos
	VECTOR rot
	BOOL bCreated
ENDSTRUCT

STRUCT RAMPAGE_STATS
	INT iPedDeadCount = 0
	INT iHeadShots = 0
	INT iInnocentsDead = 0
	INT iCrippleCount = 0
	INT iCloseRange = 0
	INT iLongRange = 0
	
	INT iCarDeadCount = 0
	INT iCarAbandonCount = 0
	INT iCarOccupantKills = 0
	INT iChopperDeadCount = 0
	INT iTanksDestroyed = 0
	
	INT iLastKillTime = 0
	INT iPedsSpawned = 0
	
	INT iStartTime 
	INT iEndTime
	INT iPredictedEndTime
	
	INT iChain
	INT iMaxChain
	
	INT iInitialAccuracyHits
	INT iInitialAccuracyShots
ENDSTRUCT

STRUCT RAMPAGE_SPAWNPOINT
	BOOL bIsActive = FALSE
	BOOL bIsAerial = FALSE
	VECTOR vPosition
	FLOAT fHeading = 0.0
ENDSTRUCT

STRUCT RAMPAGE_ALLEYSPAWNER
	BOOL bIsActive = FALSE
	INT iFrameCounter = 0
	
	INT iMinSpawnTime = 1000
	INT iMaxSpawnTime = 3000
	INT iNextSpawnTime 
	
	VECTOR vAlleyPoints[2]
	FLOAT fAlleyRadius = 0.0
	FLOAT fAlleyDiffAdjust = -0.1
	FLOAT fPlayerAlleyUValue = 0.0
	
	RAMPAGE_SPAWNPOINT spawnPoint[33]
	INT iMaxActiveSpawnPoints = 0
	
	#IF IS_DEBUG_BUILD	
		BOOL bShowDebug
	#ENDIF
ENDSTRUCT

STRUCT RAMPAGE_CARSPAWNER
	BOOL bIsActive = FALSE
	
	INT iMinSpawnTime = 10000
	INT iMaxSpawnTime = 20000
	INT iNextSpawnTime
	INT iMinActiveCars = 0
	INT iMaxActiveCars = 1
	
	BOOL bUseSpawnPoints = FALSE
	RAMPAGE_SPAWNPOINT vSpawnPoints[4]
	INT iSpawnPointsUsed = 0
	
	FLOAT fMinSpawnRadius = 60.0
	FLOAT fMaxSpawnRadius = 120.0
	
	INT iFrameCounter = 1
	
	#IF IS_DEBUG_BUILD	
		BOOL bShowSpawnPoints
	#ENDIF
ENDSTRUCT

STRUCT RAMPAGE_CLOUDSPAWNER
	BOOL bIsActive = FALSE
	INT iFrameCounter = 2
	
	BOOL bPavementOnly = TRUE
	INT iMinSpawnTime = 250
	INT iMaxSpawnTime = 1000
	INT iNextSpawnTime 
	
	FLOAT fMinSpawnRadius = 20.0
	FLOAT fMaxSpawnRadius = 60.0
	FLOAT fFleeChance = 0.9
	
	#IF IS_DEBUG_BUILD	
		BOOL bShowSpawnRadius
	#ENDIF
ENDSTRUCT

STRUCT RAMPAGE_FORTSPAWNER
	BOOL bIsActive = FALSE
	FLOAT fMinSpawnRadius = 12.5
	
	INT iMinSpawnTime = 1000
	INT iMaxSpawnTime = 3000
	INT iNextSpawnTime 	
	
	INT iMaxActiveFortress = 0
	INT iFrameCounter = 3
	INT iFortressFrameCounter = 0
	FLOAT fFortressAllowRange = 100.0 // if peds are more than this meters away from a fortress they won't spawn
ENDSTRUCT

STRUCT RAMPAGE_CHALLENGE
	eRAMPAGE_CHALLENGE cType 
	INT iRequiredAmount
	STRING sTxtKey
ENDSTRUCT

//----------------------
//	CONSTANTS
//----------------------
CONST_INT RAMPAGE_VERSION 			032
CONST_INT RAMPAGE_PED_MAXHP 		120
CONST_INT RAMPAGE_PED_MONEY			0

CONST_INT CHAIN_TIMER				3500

CONST_FLOAT CLOSE_KILL_RANGE		2.0		
CONST_FLOAT LONG_KILL_RANGE			50.0		

CONST_FLOAT PLAYER_IN_RAGDOLL_AIM	0.01		//Accuracy of peds while player in ragdoll
CONST_FLOAT TANK_ALIVE_AIM			0.05		//Accuracy of peds while tank is alive

CONST_FLOAT RAMPAGE_SF_HANGTIME		1.25		// How long the rampage sf sticks about after it has started
CONST_INT MAX_SKIP_MENU_LENGTH		3

CONST_INT DELAY_INITIAL_PEDS_TIME	3000
CONST_INT MAX_ACTIVE_PEDS			10			//Reducing max active peds for 1320970

CONST_INT MAX_EXPLOSIVE_PROPS		10

CONST_INT FINAL_TIME_BONUS			100 		// 100 pts per tenth of a second
CONST_INT FINAL_CAR_KILL_BONUS		500
CONST_INT FINAL_CHAIN_BONUS			10000
CONST_INT FINAL_HEADSHOT_BONUS		5000		// 10000 per headshot
CONST_INT FINAL_KILLS_BONUS			1000		// 1000 pts per kill
CONST_INT FINAL_CLOSE_RANGE_BONUS	1250		// 
CONST_INT FINAL_LONG_RANGE_BONUS	2500		// 
CONST_INT FINAL_INNOCENT_BONUS		750			// lose 500 pts for killing innocents

CONST_FLOAT PED_CLEANUP_DISTANCE	100.0
CONST_FLOAT CAR_CLEANUP_DISTANCE	140.0
CONST_FLOAT NO_PED_CHECK_RADIUS		1.5			// if there is a ped within this distance the spawner will fail

CONST_INT SEAT_ADD_PLUS				0

CONST_INT PEDS_TO_SCAN_PER_FRAME	2

//----------------------
//	VARIABLES
//----------------------
g_eRC_MissionIDs rcRampageMissionID
g_structRCScriptArgs rampageScriptArgs
RAMPAGE_STATS rampageStats
RAMPAGE_INFO rampageInfo
RAMPAGE_CHALLENGE rampageChallenge[3]
INT iCompletedChallenges = 0
INT iRampagePedScannerIndex = 0

#IF USE_TU_CHANGES
	INT iOldCheatStateVariable = 0
#ENDIF

COMBAT_PARAMETERS rampageCombatParams

END_SCREEN_DATASET esd

OBJECT_INDEX plyrGunObject
INT iFrameCounter
RAMPAGE_MEDAL iCurrentMedalIndex 

REL_GROUP_HASH activeGroup
REL_GROUP_HASH activePassiveGroup
SCENARIO_BLOCKING_INDEX scenarioBlockID
SCENARIO_BLOCKING_INDEX worldScenarioBlockID

RAMPAGE_EXPLOSIVE_PROP explosiveObject[MAX_EXPLOSIVE_PROPS]

STAGE_PROGRESS stageProgress

INT	i_sound_countdown = GET_SOUND_ID()

BOOL bTimerSoundStarted = FALSE

BOOL bRunOutro = FALSE

BOOL bBloodDamage = FALSE
BOOL bDoPedSpawnBehindCheck = FALSE
BOOL bUseIsAreaOccupied = FALSE

BOOL bInfiniteTime = FALSE
FLOAT fHPRegenMult = 1.5
FLOAT fCarDefenseAreaChance = 0.5		// chance of rampaging peds using small defense area around vehicle
FLOAT fEnemySecondWeaponChance = 0.5	// chance of enemies being given second weapon
FLOAT fPissTakeRocketChance = 0.1		// chance that enemies will use rockets if the player is a dick and decides to piss about in a air vehicle

RAMPAGE_AREA noSpawnArea[6]
INT iMaxActiveNoSpawnAreas = 0

RAMPAGE_PED rampagePed[MAX_ACTIVE_PEDS]
RAMPAGE_CAR rampageCar[5]
RAMPAGE_TANK rampageTank

VEHICLE_INDEX coverCar[7]
INT iAliveCoverCarCounter = 0

INT iCurrentRampagePedIndex = 0
INT iAlivePedCounter = 0
INT iAliveCarCounter = 0
INT iMaxActivePeds = COUNT_OF(rampagePed)

INT iMolotovRespawnTimer = 0

INT iInitialSpawnDelay = 0
INT iInitialCarSpawnDelay = 0

//Tank variables
FLOAT	flWidgetArcOfSafety		 = 30.0
FLOAT	flMinCannonShotDistance	 = 8.0
FLOAT	flMaxCannonShotDistance	 = 10.0
FLOAT flMinimumSafeAngle
FLOAT flMaximumSafeAngle
FLOAT flTankShotAngle

RAMPAGE_FORTRESS rampageFort[8]

RAMPAGE_ALLEYSPAWNER rampageAlleySpawner
RAMPAGE_CARSPAWNER rampageCarSpawner
RAMPAGE_CLOUDSPAWNER rampageCloudSpawner
RAMPAGE_CLOUDSPAWNER rampageBackupCloudSpawner
RAMPAGE_FORTSPAWNER rampageFortressSpawner

BOOL bShowNotifications = FALSE
INT iTimeLineEventIndex = 0
BOOL bDisplayedSpecialHelp = FALSE

VECTOR vBattleCenter
FLOAT fBattleRadius = 200.0

BLIP_INDEX battleBlip
BOOL bInBattleArea = TRUE
INT iGameTimer = 0

INT iWaitTime = 0

STRING sAudioScene = NULL
ANGLED_AREA respotVehicleArea

BOOL bTankDiag = FALSE

INT iNextBeepTime = 10000		// 10 seconds

BOOL bRampage3WeaponBodge = FALSE

//----------------------
//	SCALEFORM VARIABLES
//----------------------
SCALEFORM_INDEX sFormRampage = NULL
SCALEFORM_INDEX sFormRampageStats = NULL
SCALEFORM_INDEX sFormRampagePassMsg = NULL
SFORMRAMPAGE_STATE sFormState = SFR_MOCAP
STAGE_PROGRESS sFailState
HUD_COLOURS medalHudColor[4]		// 0 = no medal, 1 - bronze, 2 - silver, 3 - gold.
INT sFormAlpha = 255		// alpha color for the scaleform video
INT isFormTimer
BOOL bShowZeroScores = FALSE
LEADIN_HANDLER rampageLeadIn

//----------------------
//	MISC VARIABLES
//----------------------
STRING sVoiceContext
STRING sPlayerSceneHandle	// for cutscene screen handles
STRING sWeaponSceneHandle	// for cutscene screen handles
STRING sSceneHandles[8]		// for cutscene screen handles
STRING sMaleVoiceNames[5]
STRING sFemaleVoiceNames[5]
BOOL bPlayerExitStateSet = FALSE

BOOL bCutsceneCanBeSkipped = FALSE

INT iNextBattleRantTime
INT iLastUsedMaleVoiceNameIndex = 0
INT iLastUsedFemaleVoiceNameIndex = 0

INT	iLastUsedRampage2VoiceIndex = 0

BOOL bBlockRampageTempEvents = TRUE
structPedsForConversation convoBattleRant

eRampageSpawnState rampageSpawnState = ERS_SPAWNING_ON
INT iRampageSpawnBlockLimit = MAX_ACTIVE_PEDS / 2
INT iMinRampageSpawnBlockTime = 4000
INT iMaxRampageSpawnBlockTime = 8000
INT iRampageSpawnStateTimer = 0
BOOL bBlockRampageSpawning = TRUE

INT iNextRampageSpawnTime = 0
INT iMinRampagePedSpawnTime = 750
INT iMaxRampagePedSpawnTime = 1500
BOOL bUseGlobalPedSpawnTimer = TRUE
//INT iNextClockTickSoundTime

INT iRampageMusicStartTime		// this is relative to the cutscene time
BOOL bRampageMusicStarted = FALSE

VECTOR vFinalCamCoord
VECTOR vFinalCamRot
BOOL bHackInterpolate = FALSE
BOOL bCutsceneIsPlaying = FALSE

BOOL bRampageShardAnimated

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct sSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID m_RampWidgetGroup
	
	BOOL bDebugShowMissionVersion = FALSE
	BOOL bDebugShowTTY = FALSE
	BOOL bCreateTestVehicle = FALSE
	BOOL bCreateTestTank = FALSE
	INT iTestVehicleSpawnPointIndex = 0
	
	BOOL bPauseRampageSpawning = FALSE
	BOOL bDebugDeletePedsForJapan = FALSE
	
	BOOL bDeletePedsAndCars = FALSE
	BOOL bDeletePeds = FALSE
	BOOL bDeleteCars = FALSE
	
	BOOL bCleanupPedsAndCars = FALSE
	BOOL bCleanupPeds = FALSE
	BOOL bCleanupCars = FALSE
	
	BOOL bInstantPass = FALSE
	BOOL bInstantFail = FALSE
	INT iInstaPassMedal = 0
	BOOL bInstaPassChallenges = FALSE
	
	BOOL bSetPlayerPosAsCheckPoint
	BOOL bCheckSpawnPosition
	VECTOR vCheckSpawnPoint
	BOOL bIsPointOkay 

	BOOL bShowScenarioBlockArea
	INT iRampageSpawnStateDebug
	
	VECTOR vDebugDrawSpot
	BOOL bDebugDrawSpot
#ENDIF

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Setups a rampage area widget
/// PARAMS:
///    area - area reference
///    nme - widget display name
///    idx - number to append to display name
PROC SETUP_RAMPAGE_AREA_WIDGETS(RAMPAGE_AREA &area, STRING nme, INT idx = -1)
	TEXT_LABEL sLabel = nme
	
	IF (idx >= 0)
		sLabel += idx
	ENDIF
	
	START_WIDGET_GROUP(sLabel)
		ADD_WIDGET_BOOL("Is Active", area.bIsActive)
		ADD_WIDGET_BOOL("Show Debug", area.bShowDebug)
		ADD_WIDGET_VECTOR_SLIDER("Point 1", area.vPosition[0], -5000.0, 5000.0, 10.0)
		ADD_WIDGET_VECTOR_SLIDER("Point 2", area.vPosition[1], -5000.0, 5000.0, 10.0)
		ADD_WIDGET_FLOAT_SLIDER("Width", area.fWidth, 0.0, 200.0, 0.1)
		ADD_WIDGET_BOOL("Recalculate", area.bRecalculate)
	STOP_WIDGET_GROUP()
ENDPROC  

/// PURPOSE:
///    Setups a rampage fortress widget
/// PARAMS:
///    fort - fort reference
///    nme - widget display name
///    idx - number to append to display name
PROC SETUP_RAMPAGE_FORTRESS_WIDGETS(RAMPAGE_FORTRESS &fort, STRING nme, INT idx)
	TEXT_LABEL sLabel = nme
	sLabel += idx
	
	START_WIDGET_GROUP(sLabel)
		ADD_WIDGET_BOOL("Is Active", fort.bIsActive)
		ADD_WIDGET_BOOL("Show Debug", fort.bShowDebug)
		ADD_WIDGET_BOOL("Calc Debug Spawn Point", fort.bGenerateTestSpawnPoint)
		ADD_WIDGET_BOOL("Show Debug Spawn Point", fort.bShowDebugTestSpawnPoint)
				
		SETUP_RAMPAGE_AREA_WIDGETS(fort.spawnArea, "Spawn Area")
		SETUP_RAMPAGE_AREA_WIDGETS(fort.spawnArea, "Defense Area")

		START_WIDGET_GROUP("Ped Spawning")
			ADD_WIDGET_FLOAT_SLIDER("Rush Factor", fort.fRushFactor, 0.0, 100.0, 0.1)
			ADD_WIDGET_INT_SLIDER("Max Active Peds", fort.iMaxActivePeds, -1, COUNT_OF(rampagePed), 1)
			ADD_WIDGET_INT_SLIDER("Max Active Cars", fort.iMaxActiveCars, -1, COUNT_OF(rampageCar), 1)
			ADD_WIDGET_INT_SLIDER("Max Spawn Count", fort.iMaxSpawnCount, -1, 99, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Roof Values")
			ADD_WIDGET_FLOAT_SLIDER("Roof Height", fort.fRoofHeight, 0.0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Roof Factor", fort.fRoofFactor, 0.0, 100.0, 0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Read Only")
			ADD_WIDGET_INT_READ_ONLY("Active Peds", fort.iPedCount)
			ADD_WIDGET_INT_READ_ONLY("Peds Spawned", fort.iSpawnedCount)
			ADD_WIDGET_INT_READ_ONLY("Max Spawn Count", fort.iMaxSpawnCount)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Recalculate", fort.bRecalculate)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Creates a rampage spawnpoint widget
/// PARAMS:
///    sp - spawn point reference
///    nme - widget display name
///    idx - number to append to display name
PROC SETUP_SPAWNPOINT_WIDGETS(RAMPAGE_SPAWNPOINT &sp, STRING nme, INT idx)
	TEXT_LABEL sLabel = nme
	sLabel += idx
	
	START_WIDGET_GROUP(sLabel)
		ADD_WIDGET_BOOL("Is Active", sp.bIsActive)
		ADD_WIDGET_BOOL("Is Aerial", sp.bIsAerial)
		ADD_WIDGET_VECTOR_SLIDER("Position", sp.vPosition, -5000.0, 5000.0, 10.0)
		ADD_WIDGET_FLOAT_SLIDER("Heading", sp.fHeading, 0.0, 360.0, 0.1)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Creates a rampage spawnpoint widget
/// PARAMS:
///    sp - spawner point 
PROC SETUP_RAMPAGE_ALLEY_SPAWNER_DEBUG_WIDGET(RAMPAGE_ALLEYSPAWNER &csp)
	START_WIDGET_GROUP("Rampage Alley Spawner")
		ADD_WIDGET_BOOL("Is Active", csp.bIsActive)
		ADD_WIDGET_BOOL("Show Debug", csp.bShowDebug)
		ADD_WIDGET_VECTOR_SLIDER("Position 1", csp.vAlleyPoints[0], -5000.0, 5000.0, 10.0)
		ADD_WIDGET_VECTOR_SLIDER("Position 2", csp.vAlleyPoints[1], -5000.0, 5000.0, 10.0)
		ADD_WIDGET_FLOAT_SLIDER("Radius", csp.fAlleyRadius, 0.0, 150.0, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("U Difference", csp.fAlleyDiffAdjust, -1.0, 1.0, 0.0125)
		ADD_WIDGET_FLOAT_READ_ONLY("Player UValue", csp.fPlayerAlleyUValue)
	STOP_WIDGET_GROUP()
ENDPROC			
			
/// PURPOSE:
///    Creates a rampage spawnpoint widget
/// PARAMS:
///    sp - spawn point reference
///    nme - widget display name
///    idx - number to append to display name
PROC SETUP_RAMPAGE_CAR_SPAWNER_DEBUG_WIDGET(RAMPAGE_CARSPAWNER &csp)
	INT i 
	
	START_WIDGET_GROUP("Rampage Car Spawner")
		ADD_WIDGET_BOOL("Is Active", csp.bIsActive)
		ADD_WIDGET_BOOL("Show Spawn Points", csp.bShowSpawnPoints)
		ADD_WIDGET_INT_SLIDER("Min Active Cars", csp.iMinActiveCars, 0, COUNT_OF(rampageCar), 1)
		ADD_WIDGET_INT_SLIDER("Max Active Cars", csp.iMaxActiveCars, 0, COUNT_OF(rampageCar), 1)
		
		START_WIDGET_GROUP("Spawning")
			ADD_WIDGET_BOOL("Use Spawn Points", csp.bUseSpawnPoints)
			ADD_WIDGET_INT_SLIDER("Spawn Points Used", csp.iSpawnPointsUsed, 0, COUNT_OF(csp.vSpawnPoints), 1)
			ADD_WIDGET_FLOAT_SLIDER("Min Spawn Radius", csp.fMinSpawnRadius, 0.0, 800.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Max Spawn Radius", csp.fMaxSpawnRadius, 1.0, 1000.0, 0.5)
			REPEAT COUNT_OF (csp.vSpawnPoints) i 
				SETUP_SPAWNPOINT_WIDGETS(csp.vSpawnPoints[i], "Spawn Point", i)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Time")
			ADD_WIDGET_INT_SLIDER("Min Spawn Time", csp.iMinSpawnTime, 0, 120000, 125)
			ADD_WIDGET_INT_SLIDER("Max Spawn Time", csp.iMaxSpawnTime, 0, 120000, 125)
			ADD_WIDGET_INT_READ_ONLY("Next Spawn Time", csp.iNextSpawnTime)
		STOP_WIDGET_GROUP()
		

	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Creates a rampage spawnpoint widget
/// PARAMS:
///    sp - spawn point reference
PROC SETUP_RAMPAGE_CLOUD_SPAWNER_DEBUG_WIDGET(RAMPAGE_CLOUDSPAWNER &csp)
	START_WIDGET_GROUP("Rampage Cloud Spawner")
		ADD_WIDGET_BOOL("Is Active", csp.bIsActive)
		ADD_WIDGET_BOOL("Show Spawn Radius", csp.bShowSpawnRadius)
		
		START_WIDGET_GROUP("Spawning")
			ADD_WIDGET_BOOL("Pavement Only", csp.bPavementOnly)
			ADD_WIDGET_FLOAT_SLIDER("Min Spawn Radius", csp.fMinSpawnRadius, 0.0, 800.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Max Spawn Radius", csp.fMaxSpawnRadius, 1.0, 800.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Flee Chance", csp.fFleeChance, 0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Time")
			ADD_WIDGET_INT_SLIDER("Min Spawn Time", csp.iMinSpawnTime, 0, 120000, 125)
			ADD_WIDGET_INT_SLIDER("Max Spawn Time", csp.iMaxSpawnTime, 10, 120000, 125)
			ADD_WIDGET_INT_READ_ONLY("Next Spawn Time", csp.iNextSpawnTime)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Creates a rampage spawnpoint widget
/// PARAMS:
///    sp - spawn point reference
PROC SETUP_RAMPAGE_FORTRESS_SPAWNER_DEBUG_WIDGET(RAMPAGE_FORTSPAWNER &csp)
	INT i 
	
	START_WIDGET_GROUP("Rampage Fortress Spawner")
		ADD_WIDGET_BOOL("Is Active", csp.bIsActive)
		ADD_WIDGET_FLOAT_SLIDER("Min Spawn Radius", csp.fMinSpawnRadius, 0.0, 800.0, 0.5)
		ADD_WIDGET_INT_SLIDER("Max Fortress", csp.iMaxActiveFortress, 0, COUNT_OF(rampageFort), 1)
		ADD_WIDGET_FLOAT_SLIDER("Player Limit", csp.fFortressAllowRange, 0.0, 800.0, 0.5)
		REPEAT COUNT_OF(rampageFort) i
			SETUP_RAMPAGE_FORTRESS_WIDGETS(rampageFort[i], "Fortress", i)
		ENDREPEAT
	
		START_WIDGET_GROUP("Time")
			ADD_WIDGET_INT_SLIDER("Min Spawn Time", csp.iMinSpawnTime, 0, 120000, 125)
			ADD_WIDGET_INT_SLIDER("Max Spawn Time", csp.iMaxSpawnTime, 10, 120000, 125)
			ADD_WIDGET_INT_READ_ONLY("Next Spawn Time", csp.iNextSpawnTime)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Creates a rampage ped widget
/// PARAMS:
///    sp - ped reference
PROC SETUP_RAMPAGE_PED_DEBUG_WIDGET(RAMPAGE_PED &csp)
	TEXT_LABEL lbl = "Rampage Ped" 
	lbl += csp.iIndex
	
	START_WIDGET_GROUP(lbl)
		ADD_WIDGET_BOOL("Is Alive", csp.bDebugAliveCheck)
		ADD_WIDGET_BOOL("In Vehicle", csp.bWasInVehicle)
		ADD_WIDGET_BOOL("Is Flee Ped", csp.bIsFleePed)
		
		ADD_WIDGET_FLOAT_READ_ONLY("Player Dist", csp.fPlayerDist)
		ADD_WIDGET_INT_READ_ONLY("Fort Index", csp.iFortIndex)		
		ADD_WIDGET_INT_READ_ONLY("Index", csp.iIndex)	
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Creates a rampage ped widget
/// PARAMS:
///    sp - ped reference
PROC SETUP_RAMPAGE_CAR_DEBUG_WIDGET(RAMPAGE_CAR &csp)
	INT i
	TEXT_LABEL lbl = "Rampage Car" 
	lbl += csp.iIndex
	
	START_WIDGET_GROUP(lbl)
		ADD_WIDGET_BOOL("Is Alive", csp.bDebugAliveCheck)
		ADD_WIDGET_BOOL("Keep As Cover", csp.bKeepAsCover)
		
		START_WIDGET_GROUP("Occupant Index")
			REPEAT COUNT_OF(csp.iOccupantIndex) i
				lbl = "Occupant" 
				lbl += i
				ADD_WIDGET_INT_READ_ONLY(lbl, csp.iOccupantIndex[i])	
			ENDREPEAT
		STOP_WIDGET_GROUP()
			
		ADD_WIDGET_FLOAT_READ_ONLY("Player Dist", csp. fDistToPlayer)
		ADD_WIDGET_FLOAT_READ_ONLY("Original Dist", csp.fOriginalDist)
		ADD_WIDGET_INT_READ_ONLY("Task State", csp.iTaskState)		
		ADD_WIDGET_INT_READ_ONLY("Index", csp.iIndex)	
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
/// 	Setups All The Widgets  
PROC START_RAMPAGE_DEBUG_WIDGET_GROUP()
	INT i
	m_RampWidgetGroup = START_WIDGET_GROUP("Rampage Configuration")	

	START_WIDGET_GROUP("Debug Options")		
		ADD_WIDGET_BOOL("Show Version Number", bDebugShowMissionVersion)
		ADD_WIDGET_BOOL("Show Debug TTY", bDebugShowTTY)
		ADD_WIDGET_BOOL("Infinite Time", bInfiniteTime)
		ADD_WIDGET_BOOL("Block Temp Events", bBlockRampageTempEvents)
		ADD_WIDGET_BOOL("Camera Interphack", bHackInterpolate)
		ADD_WIDGET_BOOL("Pause Rampage", bPauseRampageSpawning)
		ADD_WIDGET_BOOL("Use Behind Check", bDoPedSpawnBehindCheck)
		ADD_WIDGET_BOOL("Use Is Area Occupied", bUseIsAreaOccupied)
		ADD_WIDGET_BOOL("Japan Delete Test", bDebugDeletePedsForJapan)
		
		START_WIDGET_GROUP("Player Options")		
			ADD_WIDGET_FLOAT_SLIDER("Player Regen Rate", fHPRegenMult, 1.0, 10.0, 0.5)
			ADD_WIDGET_BOOL("Show Notifications", bShowNotifications)
			ADD_WIDGET_INT_SLIDER("Seed", rampageInfo.iRandomSeed, 0, 100000000, 10)
			ADD_WIDGET_VECTOR_SLIDER("BattleCenter", vBattleCenter, -7000.0, 7000.0, 10.0)
			ADD_WIDGET_FLOAT_SLIDER("Battle Radius", fBattleRadius, 1.0, 1000.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("PissTake Rocket Chance", fPissTakeRocketChance, 0.0, 1.0, 0.005)
			ADD_WIDGET_BOOL("Show Debug Draw Spot", bDebugDrawSpot)
			ADD_WIDGET_VECTOR_SLIDER("Debug Draw Spot Pos", vDebugDrawSpot, -7000.0, 7000.0, 10.0)
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("Pass Fail Options")
			ADD_WIDGET_BOOL("Pass Rampage", bInstantPass)
			ADD_WIDGET_BOOL("Fail Rampage", bInstantFail)
			ADD_WIDGET_BOOL("Pass Challenges", bInstaPassChallenges)
			ADD_WIDGET_INT_SLIDER("Set Medal As", iInstaPassMedal, ENUM_TO_INT(RAMPAGE_NOMEDAL), ENUM_TO_INT(RAMPAGE_GOLD), 1)
		STOP_WIDGET_GROUP()	
	STOP_WIDGET_GROUP()	
	
	SETUP_COMBAT_PARAMETERS_WIDGET(rampageCombatParams, "Ped Combat Parameters")
	
	START_WIDGET_GROUP("No Spawn Areas")
		ADD_WIDGET_INT_READ_ONLY("Max No Spawn Areas", iMaxActiveNoSpawnAreas)
		
		i = 0
		REPEAT COUNT_OF(noSpawnArea) i 
			SETUP_RAMPAGE_AREA_WIDGETS(noSpawnArea[i], "Area", i)
		ENDREPEAT	
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Spawning")		
		ADD_WIDGET_INT_SLIDER("Active Peds", iMaxActivePeds, 0, COUNT_OF(rampagePed), 1)
		ADD_WIDGET_BOOL("Use Global Ped Spawn Timer", bUseGlobalPedSpawnTimer)
		
		ADD_WIDGET_INT_SLIDER("Min Ped Spawn Time", iMinRampagePedSpawnTime, 0, 30000, 1000)
		ADD_WIDGET_INT_SLIDER("Max Ped Spawn Time", iMaxRampagePedSpawnTime, 0, 30000, 1000)
		ADD_WIDGET_FLOAT_SLIDER("Second Weapon Chance", fEnemySecondWeaponChance, 0.0, 1.0, 0.05)
		ADD_WIDGET_INT_READ_ONLY("Game Timer", iGameTimer)
		
		START_WIDGET_GROUP("Test Vehicle")	
			ADD_WIDGET_BOOL("Create Test Car", bCreateTestVehicle)
			ADD_WIDGET_BOOL("Create Test Tank", bCreateTestTank)
			ADD_WIDGET_INT_SLIDER("Spawn Point Index", iTestVehicleSpawnPointIndex, 0, COUNT_OF(rampageCarSpawner.vSpawnPoints) - 1, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Cleanup Test")	
			ADD_WIDGET_BOOL("bDeletePedsAndCars", bDeletePedsAndCars)
			ADD_WIDGET_BOOL("bDeletePeds", bDeletePeds)
			ADD_WIDGET_BOOL("bDeleteCars", bDeleteCars)
	
			ADD_WIDGET_BOOL("bCleanupPedsAndCars", bCleanupPedsAndCars)
			ADD_WIDGET_BOOL("bCleanupPeds", bCleanupPeds)
			ADD_WIDGET_BOOL("bCleanupCars", bCleanupCars)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn Blocker")	
			ADD_WIDGET_BOOL("Block Spawning", bBlockRampageSpawning)
			ADD_WIDGET_INT_SLIDER("Spawn Limit Block", iRampageSpawnBlockLimit, 1, COUNT_OF(rampagePed), 1)
			ADD_WIDGET_INT_SLIDER("Min Spawn Block Time", iMinRampageSpawnBlockTime, 0, 30000, 1000)
			ADD_WIDGET_INT_SLIDER("Max Spawn Block Time", iMaxRampageSpawnBlockTime, 0, 30000, 1000)
			ADD_WIDGET_INT_READ_ONLY("Block State", iRampageSpawnStateDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn Pos Checker")		
			ADD_WIDGET_BOOL("Check Position", bCheckSpawnPosition)
			ADD_WIDGET_BOOL("Use Player Pos", bSetPlayerPosAsCheckPoint)
			ADD_WIDGET_VECTOR_SLIDER("Position", vCheckSpawnPoint, -5000.0, 5000.0, 10.0)
			ADD_WIDGET_BOOL("Is Point Okay", bIsPointOkay )
		STOP_WIDGET_GROUP()
		
		SETUP_RAMPAGE_ALLEY_SPAWNER_DEBUG_WIDGET(rampageAlleySpawner)
		SETUP_RAMPAGE_CAR_SPAWNER_DEBUG_WIDGET(rampageCarSpawner)
		SETUP_RAMPAGE_CLOUD_SPAWNER_DEBUG_WIDGET(rampageCloudSpawner)
		SETUP_RAMPAGE_FORTRESS_SPAWNER_DEBUG_WIDGET(rampageFortressSpawner)
		
		START_WIDGET_GROUP("Backup Spawner")		
			SETUP_RAMPAGE_CLOUD_SPAWNER_DEBUG_WIDGET(rampageBackupCloudSpawner)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Scenario Area")		
		ADD_WIDGET_BOOL("Show Area", bShowScenarioBlockArea)
		ADD_WIDGET_VECTOR_SLIDER("Position", rampageInfo.vScenarioBlockCenter, -8000.0, 8000.0, 10.0)
		ADD_WIDGET_FLOAT_SLIDER("Radius", rampageInfo.fScenarioBlockRadius, 0.0, 800.0, 10.0)
	STOP_WIDGET_GROUP()
		
	START_WIDGET_GROUP("Ped Debug")	
		i = 0
		REPEAT COUNT_OF(rampagePed) i 
			SETUP_RAMPAGE_PED_DEBUG_WIDGET(rampagePed[i])
		ENDREPEAT	
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Car Debug")	
		i = 0
		REPEAT COUNT_OF(rampageCar) i 
			SETUP_RAMPAGE_CAR_DEBUG_WIDGET(rampageCar[i])
		ENDREPEAT	
	STOP_WIDGET_GROUP()

	START_WIDGET_GROUP("Stats")
		START_WIDGET_GROUP("Ped Stats")	
			ADD_WIDGET_INT_READ_ONLY("Max Active Peds", iMaxActivePeds)
			ADD_WIDGET_INT_READ_ONLY("Alive Peds", iAlivePedCounter)
			ADD_WIDGET_INT_READ_ONLY("Peds Dead", rampageStats.iPedDeadCount)
			ADD_WIDGET_INT_READ_ONLY("Head Shots", rampageStats.iHeadShots)
			ADD_WIDGET_INT_READ_ONLY("Cripple Shots", rampageStats.iCrippleCount)
			ADD_WIDGET_INT_READ_ONLY("Close Ranges", rampageStats.iCloseRange)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Car Stats")
			ADD_WIDGET_INT_READ_ONLY("Alive Cars", iAliveCarCounter)
			ADD_WIDGET_INT_READ_ONLY("Alive Cover Cars", iAliveCoverCarCounter)
			ADD_WIDGET_INT_READ_ONLY("Cars Destroyed", rampageStats.iCarDeadCount)
			ADD_WIDGET_INT_READ_ONLY("Cars Abandoned", rampageStats.iCarAbandonCount)
			ADD_WIDGET_INT_READ_ONLY("Car Occupants Kills", rampageStats.iCarOccupantKills)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE:
///    Cleans up All The Widgets
PROC CLEANUP_RAMPAGE_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_RampWidgetGroup)
		DELETE_WIDGET_GROUP(m_RampWidgetGroup)
	ENDIF
ENDPROC 

/// PURPOSE:
///    Draw a Ramage Fortress Area in Debug
/// PARAMS:
///    fort - fortress reference
PROC DRAW_DEBUG_RAMPAGE_FORTRESS(RAMPAGE_FORTRESS &fort)
	INT iAlpha = 128
	
	VECTOR v1, v2 

	IF (fort.bIsActive)
		iAlpha = 255
	ENDIF

	DRAW_DEBUG_SPHERE(fort.vLastSpawnPoint, 0.5)
	DRAW_DEBUG_AREA(fort.defenseArea.vPosition[0], fort.defenseArea.vPosition[1], fort.defenseArea.fWidth, 0, 255, 0, iAlpha)
	DRAW_DEBUG_AREA(fort.spawnArea.vPosition[0], fort.spawnArea.vPosition[1], fort.spawnArea.fWidth, 0, 255, 0, 255)
	
	// draw roof
	IF (fort.fRoofHeight > 0.0)
		v1 = fort.spawnArea.vPosition[0]
		v1.z = fort.fRoofHeight
		
		v2 = fort.spawnArea.vPosition[1] 
		v2.z = fort.fRoofHeight
		
		DRAW_DEBUG_AREA(v1, v2, fort.defenseArea.fWidth, 0, 255, 255, 255)
	ENDIF
ENDPROC

#ENDIF

//----------------------
//  AREA FUNCTIONS
//----------------------

/// PURPOSE: 
///  	Resolve cars for mission
PROC RESOLVE_MISSION_VEHICLES_FOR_CUTSCENE()				
	CPRINTLN(DEBUG_RAMPAGE, "Resolve vehicles Resolved For Intro")
	
	VEHICLE_INDEX playerVeh
	VECTOR vRespotPos
	FLOAT fRespotHeading
	playerVeh = GET_MISSION_START_VEHICLE_INDEX()
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			SET_ANGLED_AREA(respotVehicleArea, <<896.120422,3640.058105,36.088860>>, <<922.222107,3639.568359,31.159431>>, 13.500000)												
			IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
				vRespotPos = <<899.9673, 3653.0491, 31.7576>>
				fRespotHeading = 268.2063
			ELSE
				vRespotPos = <<891.0971, 3653.3279, 31.8184>>
				fRespotHeading = 268.1944
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			SET_ANGLED_AREA(respotVehicleArea, <<1179.185303,-421.631012,74.338844>>, <<1185.704590,-388.575287,66.900261>>, 21.500000)
			IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
				vRespotPos = <<1231.5063, -442.6294, 66.5811>>
				fRespotHeading = 191.4606
			ELSE
				vRespotPos = <<1226.6814, -436.0827, 66.5804>>
				fRespotHeading = 76.5787
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			SET_ANGLED_AREA(respotVehicleArea, <<449.730560,-1858.224121,28.845421>>, <<472.455170,-1843.895630,25.801113>>, 37.250000)
			IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
				vRespotPos = <<506.3633, -1842.3701, 26.6440>>
				fRespotHeading = 308.8210
			ELSE
				vRespotPos = <<505.1393, -1845.2905, 26.3862>>
				fRespotHeading = 308.6425
			ENDIF
		BREAK		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			SET_ANGLED_AREA(respotVehicleArea, <<-143.971466,-1646.401001,39.634609>>, <<-172.522293,-1680.794800,31.561562>>, 18.750000)
			IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
				vRespotPos = <<-163.3928, -1698.3533, 30.6384>>
				fRespotHeading = 63.0698
			ELSE
				vRespotPos = <<-225.3203, -1699.5354, 33.0015>>
				fRespotHeading = 107.6143
			ENDIF
		BREAK	
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			SET_ANGLED_AREA(respotVehicleArea, <<-1303.807495,2499.506104,29.048244>>, <<-1228.827271,2569.846191,14.731240>>, 29.500000)
			vRespotPos = <<-1323.6338, 2474.4177, 23.6185>>
			fRespotHeading = 318.5112
		BREAK
	ENDSWITCH
	
	IF IS_REPLAY_IN_PROGRESS()
		CREATE_VEHICLE_FOR_REPLAY( playerVeh, vRespotPos, fRespotHeading, FALSE, FALSE, TRUE, TRUE, FALSE )
	ENDIF
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(respotVehicleArea.vPosition[0], respotVehicleArea.vPosition[1], respotVehicleArea.fWidth, vRespotPos, fRespotHeading, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
	
	IF DOES_ENTITY_EXIST( playerVeh )
		SET_ENTITY_AS_MISSION_ENTITY( playerVeh, TRUE, TRUE )
	ENDIF
	SET_MISSION_VEHICLE_GEN_VEHICLE( playerVeh, <<0,0,0>>, 0 )
ENDPROC	

FUNC BOOL IS_PLAYER_IN_UNDERGROUND_TUNNEL() 
	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -2080.115, 3292.193, -11.667 >>, << -2112.049, 3224.331, 11.667 >>, 115.000, FALSE, FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_JUMPING_GATES()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1599.593, 2818.150, -17.645 >>, << -1612.423, 2806.997, 17.645 >>, 51.000, TRUE, FALSE)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -2301.089, 3385.031, -31.086 >>, << -2305.302, 3379.441, 31.086 >>, 16.000, TRUE, FALSE)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -2287.138, 3385.616, 31.124 >>, << -2292.554, 3378.428, 31.124 >>, 19.8, TRUE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Setups a rampage area
/// PARAMS:
///    area - area reference
///    p1 - area point 1
///    p2 - area point 2
///    width - area width
PROC SETUP_RAMPAGE_AREA(RAMPAGE_AREA &area, VECTOR p1, VECTOR p2, FLOAT width)
	area.bIsActive = TRUE
	
	area.vPosition[0] = p1
	area.vPosition[1] = p2
	area.fWidth = width
	area.fHeight = p2.z - p1.z
	
	// calculate flat forward
	p2.z = p1.z
	area.vForward = NORMALISE_VECTOR(p2 - p1)
	area.fLength = GET_LINE_MAGNITUDE(p1, p2)
	area.vRight = <<area.vForward.y, area.vForward.x, area.vForward.z>>
ENDPROC

/// PURPOSE:
///    Expands an angled area by an amount and returns the expanded version
/// PARAMS:
///    a - area to expand
///    exp - expand amount
FUNC RAMPAGE_AREA EXPAND_RAMPAGE_AREA(RAMPAGE_AREA a, FLOAT exp = 1.0)
	RAMPAGE_AREA aOut
	
	VECTOR vNormal = NORMALISE_VECTOR(a.vPosition[1] - a.vPosition[0]) * exp
	VECTOR v0 = a.vPosition[0] - vNormal
	VECTOR v1 = a.vPosition[1] + vNormal
	FLOAT fWidth = a.fWidth + (exp * 2.0)
	
	SETUP_RAMPAGE_AREA(aOut, v0, v1, fWidth)
	RETURN aOut			
ENDFUNC

/// PURPOSE:
///    Generates a random point within an area
/// PARAMS:
///    area - area reference
/// RETURNS:
///    a generated position
FUNC VECTOR GET_RANDOM_POINT_IN_RAMPAGE_AREA(RAMPAGE_AREA area)
	VECTOR v 

	v = area.vPosition[0] + (area.vForward * GET_RANDOM_FLOAT_IN_RANGE(0, area.fLength))
	v += (area.vRight * GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5) * area.fWidth)
	
	v.z = area.vPosition[0].z + ((area.vPosition[1].z - area.vPosition[0].z) * GET_RANDOM_FLOAT_IN_RANGE(0, 1.5))
	IF (v.z > area.vPosition[1].z)
		v.z = area.vPosition[1].z
	ENDIF
	
	RETURN v
ENDFUNC

/// PURPOSE:
///    Setups a rampage spawn point
/// PARAMS:
///    sp - spawn point reference
///    pos - spawn point location
///    head - spawn point heading
///    aerial - set this to be true if they are on a roof (so people don't fall off)
PROC SET_RAMPAGE_SPAWN_POINT(RAMPAGE_SPAWNPOINT &sp, VECTOR pos, FLOAT head, BOOL aerial = FALSE)
	sp.vPosition = pos
	sp.fHeading = head
	sp.bIsAerial = aerial
ENDPROC

/// PURPOSE:
///    Adds a no spawn area to our current lise
/// PARAMS:
///    p1 - area point 1 
///    p2 - area point 2
///    width - area width
/// RETURNS:
///    True if we can - or this will assert if we've overstepped the area
FUNC BOOL ADD_NO_SPAWN_AREA(VECTOR p1, VECTOR p2, FLOAT width)
	// if we out of allocated no spawn areas - bail
	IF (iMaxActiveNoSpawnAreas >= COUNT_OF(noSpawnArea))
		SCRIPT_ASSERT("INCREASE THE ARRAY SIZE OF NO SPAWN AREA")
		RETURN FALSE
	ENDIF
	
	SETUP_RAMPAGE_AREA(noSpawnArea[iMaxActiveNoSpawnAreas], p1, p2, width)
	iMaxActiveNoSpawnAreas ++
	RETURN TRUE
ENDFUNC

//----------------------
//  AUDIO FUNCTIONS
//----------------------

/// PURPOSE:
///    SETS UP TIMERS AND CONVERSATIONS FOR RAMPAGE RANTING
PROC PREPARE_RAMPAGE_ONE_SHOT()
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			PREPARE_MUSIC_EVENT("RAMPAGE_1_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			PREPARE_MUSIC_EVENT("RAMPAGE_2_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			PREPARE_MUSIC_EVENT("RAMPAGE_3_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			PREPARE_MUSIC_EVENT("RAMPAGE_4_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			PREPARE_MUSIC_EVENT("RAMPAGE_5_OS")
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_RAMPAGE, "PREPARING RAMPAGE ONE SHOT")
ENDPROC

/// PURPOSE:
///    SETS UP TIMERS AND CONVERSATIONS FOR RAMPAGE RANTING
PROC TRIGGER_RAMPAGE_ONE_SHOT()
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			TRIGGER_MUSIC_EVENT("RAMPAGE_1_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			TRIGGER_MUSIC_EVENT("RAMPAGE_2_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			TRIGGER_MUSIC_EVENT("RAMPAGE_3_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			TRIGGER_MUSIC_EVENT("RAMPAGE_4_OS")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			TRIGGER_MUSIC_EVENT("RAMPAGE_5_OS")
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_RAMPAGE, "TRIGGERING RAMPAGE ONE SHOT")
ENDPROC

PROC TRIGGER_RAMPAGE_FX()
	IF NOT ANIMPOSTFX_IS_RUNNING("Rampage")
		ANIMPOSTFX_PLAY("Rampage", 0, TRUE)
	ENDIF
ENDPROC

PROC STOP_RAMPAGE_FX()
	IF ANIMPOSTFX_IS_RUNNING("Rampage")
		ANIMPOSTFX_STOP("Rampage")
		ANIMPOSTFX_PLAY("RampageOut", 0, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    SETS MUSIC FOR RAMPAGE
PROC TRIGGER_RAMPAGE_MUSIC()
	IF (bRampageMusicStarted = TRUE)
		EXIT 
	ENDIF
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			TRIGGER_MUSIC_EVENT("RAMPAGE_1_START")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			TRIGGER_MUSIC_EVENT("RAMPAGE_2_START")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			TRIGGER_MUSIC_EVENT("RAMPAGE_3_START")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			TRIGGER_MUSIC_EVENT("RAMPAGE_4_START")
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			TRIGGER_MUSIC_EVENT("RAMPAGE_5_START")
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_RAMPAGE, "TRIGGERING RAMPAGE MUSIC")
	bRampageMusicStarted = TRUE
ENDPROC

/// PURPOSE:
///    SETS UP TIMERS AND CONVERSATIONS FOR RAMPAGE RANTING
PROC SETUP_RAMPAGE_BATTLE_RANT()
	ADD_PED_FOR_DIALOGUE(convoBattleRant, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
	iNextBattleRantTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(20000, 40000)
ENDPROC

	
/// PURPOSE:
///    Selects lines for Trevor to shout out during battle
PROC UPDATE_RAMPAGE_BATTLE_RANT()
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() < iNextBattleRantTime)
		EXIT
	ENDIF
	
	// don't shout out words until rampage is actually active
	IF (sFormState <> SFR_RAMPAGEON) 
		EXIT 
	ENDIF
	
	iNextBattleRantTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 20000)
	
	#IF IS_DEBUG_BUILD
		IF bDebugShowTTY
			CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE RANT - PED DEAD - BATTLE TIME:", GET_GAME_TIMER() - rampageStats.iStartTime)
		ENDIF
	#ENDIF
	
	CREATE_CONVERSATION(convoBattleRant, rampageInfo.sDialogBlockName, rampageInfo.sDialogRootLabel, CONV_PRIORITY_MEDIUM)
ENDPROC

//----------------------
//  EVENT FUNCTIONS
//----------------------

FUNC INT GET_RAMPAGE_REMAINING_TIME_IN_MILLISECONDS()
	INT time = rampageInfo.iTimeLimit - (GET_GAME_TIMER() - rampageStats.iStartTime )
	IF (time <= 0) 
		time = 0
	ENDIF
	
	RETURN time
ENDFUNC

/// PURPOSE:
///    Disables attack, weapon select and certain movement controls when about to trigger a cutscene
PROC RAMPAGE_DISABLE_CONTROL_ACTIONS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
ENDPROC

/// PURPOSE:
///    If we are repeating a rampage down to repeat play or replay or rampages all clear
/// RETURNS:
///    
FUNC BOOL IS_REPEAT_RAMPAGE_IN_PROGRESS()
	RETURN IS_REPLAY_IN_PROGRESS() OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)
ENDFUNC

/// PURPOSE:
///    Checks to if a ped model is one of the rampage ped models
/// PARAMS:
///    pedID - ped index
/// RETURNS:
///    True if it is 
FUNC BOOL IS_PED_ENEMY_PED_MODEL(PED_INDEX pedID)
	INT i 
	MODEL_NAMES mdl = GET_ENTITY_MODEL(pedID)

	REPEAT COUNT_OF(rampageInfo.pedModels) i
		IF (rampageInfo.pedModels[i] = mdl)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    Checks to if a ped model is one of the rampage ped models
/// PARAMS:
///    pedID - ped index
/// RETURNS:
///    True if it is 
FUNC BOOL IS_VEHICLE_ENEMY_VEHICLE_MODEL(VEHICLE_INDEX pedID)
	INT i 
	MODEL_NAMES mdl = GET_ENTITY_MODEL(pedID)
	
	IF (rampageInfo.iMaxCarModels <= 0)
		RETURN FALSE
	ENDIF
	
	REPEAT COUNT_OF(rampageInfo.carModels) i
		IF (rampageInfo.carModels[i] = mdl)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    Handles Scoring and Stats when a ped dies
/// PARAMS:
///    pedID - pedIndex
PROC RESET_RAMPAGE_STATS(RAMPAGE_STATS &st) 
	st.iPedDeadCount = 0
	st.iHeadShots = 0
	st.iInnocentsDead = 0
	st.iCrippleCount = 0
	st.iCloseRange = 0
	st.iLongRange = 0
	
	st.iCarDeadCount = 0
	st.iCarAbandonCount = 0
	st.iCarOccupantKills = 0
	
	st.iLastKillTime = 0
	st.iPedsSpawned = 0
	
	st.iChain = 0
	st.iMaxChain = 0
	
	STAT_GET_INT(SP2_SHOTS, st.iInitialAccuracyShots)
	STAT_GET_INT(SP2_HITS, st.iInitialAccuracyHits)
ENDPROC

/// PURPOSE:
///    Handles Scoring and Stats when a ped dies
/// PARAMS:
///    pedID - pedIndex
PROC REGISTER_PED_DEATH(PED_INDEX pedID)
	FLOAT dist
 	IS_ENTITY_ALIVE(pedID) // this is done to stop the check entity is alive assert

	//CPRINTLN(DEBUG_RAMPAGE, "Ped Killed Report ")
	
	IF NOT IS_PED_ENEMY_PED_MODEL(pedID)
		rampageStats.iInnocentsDead ++ 
		
		//CPRINTLN(DEBUG_RAMPAGE, " Innocent Killed")
		//CPRINTLN(DEBUG_RAMPAGE, "")
		EXIT
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(pedID, FALSE)
	SET_PED_CAN_BE_TARGETTED(pedID, FALSE)
	UPDATE_RAMPAGE_BATTLE_RANT()
	
	rampageStats.iPedDeadCount ++ 

	
	CPRINTLN(DEBUG_RAMPAGE, "Peds Dead:", rampageStats.iPedDeadCount)
	
	IF (CHECK_HEADSHOT(pedID))
		rampageStats.iHeadShots ++ 
		CPRINTLN(DEBUG_RAMPAGE, "Headshots:", rampageStats.iHeadShots)
		IF (bShowNotifications)
			BEGIN_TEXT_COMMAND_THEFEED_POST("NOT_HEAD") // Headshot
			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
		ENDIF
		
		IF (rampageStats.iPedDeadCount % 5 = 0)
			PLAY_SOUND_FRONTEND(-1, "RAMPAGE_ROAR_MASTER") 
			CPRINTLN(DEBUG_RAMPAGE, " SOUND PLAYED - RAMPAGE_ROAR_MASTER")
		ELSE
			PLAY_SOUND_FRONTEND(-1, "RAMPAGE_KILLED_HEAD_SHOT_MASTER")
			CPRINTLN(DEBUG_RAMPAGE, " SOUND PLAYED - RAMPAGE_KILLED_HEAD_SHOT_MASTER")
		ENDIF		
	ELSE
		IF (rampageStats.iPedDeadCount % 5 = 0)
			PLAY_SOUND_FRONTEND(-1, "RAMPAGE_ROAR_MASTER") 
			CPRINTLN(DEBUG_RAMPAGE, " SOUND PLAYED - RAMPAGE_ROAR_MASTER")
		ELSE
			IF ((GET_GAME_TIMER() - rampageStats.iLastKillTime) > 64)
				PLAY_SOUND_FRONTEND(-1, "RAMPAGE_KILLED_COUNTER_MASTER")
				CPRINTLN(DEBUG_RAMPAGE, " SOUND PLAYED - RAMPAGE_KILLED_COUNTER_MASTER")
			ENDIF
		ENDIF
	ENDIF
	
	rampageStats.iLastKillTime = GET_GAME_TIMER()
	rampageStats.iChain ++
	
	dist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedID)
	//CPRINTLN(DEBUG_RAMPAGE, "  Kill Distance:", dist)
	
	// get square magnitude of distance - point blank range is 2 meters (2*2 = 4)
	IF (dist <= CLOSE_KILL_RANGE)
		rampageStats.iCloseRange ++
		//CPRINTLN(DEBUG_RAMPAGE, "  Close Range Kill")
		
		/*
		IF (bShowNotifications)
			BEGIN_TEXT_COMMAND_THEFEED_POST("NOT_PBLK") // Point Blank
			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
		ENDIF
		*/
	ENDIF
	
	// get square magnitude of distance - point blank range is 2 meters (2*2 = 4)
	IF (dist >= LONG_KILL_RANGE)
		rampageStats.iLongRange ++
		//CPRINTLN(DEBUG_RAMPAGE, "  Long Range Kill")
		
		/*
		IF (bShowNotifications)
			BEGIN_TEXT_COMMAND_THEFEED_POST("NOT_LSHT") // Long shot
			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
		ENDIF
		*/
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(pedID)
		rampageStats.iCarOccupantKills ++
		CPRINTLN(DEBUG_RAMPAGE, "Peds In Cars Killed:", rampageStats.iCarOccupantKills)
		//CPRINTLN(DEBUG_RAMPAGE, "  Vehicle Occupant Kill")
	ENDIF
	
	IF (CHECK_CRIPPLED(pedID))
		rampageStats.iCrippleCount ++ 
		CPRINTLN(DEBUG_RAMPAGE, "Peds Crippled:", rampageStats.iCrippleCount)
		//CPRINTLN(DEBUG_RAMPAGE, "  Cripple Kill")
	ENDIF
	
	//CPRINTLN(DEBUG_RAMPAGE, "")
ENDPROC

/// PURPOSE:
///    Handles scoring and stats when a car is abandoned
/// PARAMS:
///    carID - vehicle index
PROC REGISTER_CAR_ABANDON(VEHICLE_INDEX carID)
	IS_ENTITY_ALIVE(carID) // this is done to stop the check entity is alive assert
	
	rampageStats.iCarAbandonCount ++ 
	CPRINTLN(DEBUG_RAMPAGE, "CARS ABANDONED:", rampageStats.iCarAbandonCount)
ENDPROC

/// PURPOSE:
///    Handles scoring and stats when a car is destroyed
/// PARAMS:
///    carID - vehicle index
PROC REGISTER_CAR_DEATH(VEHICLE_INDEX car, BOOL bFromEvent = TRUE)
	IS_ENTITY_ALIVE(car) // this is done to stop the check entity is alive assert
	
	IF IS_VEHICLE_ENEMY_VEHICLE_MODEL(car) 
		IF NOT DECOR_EXIST_ON(car, "RampageCarExploded")
			IF DECOR_SET_BOOL(car, "RampageCarExploded", TRUE)
				IF (bShowNotifications)
					BEGIN_TEXT_COMMAND_THEFEED_POST("NOT_VDED") // Vehicle Destroyed
					END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
				ENDIF
			
				rampageStats.iCarDeadCount ++
				IF (bFromEvent = TRUE)
				ENDIF
			
				CPRINTLN(DEBUG_RAMPAGE, "VEHICLES DESTROYED:", rampageStats.iCarDeadCount, " REGISTERED BY EVENT:", bFromEvent)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This checks the event queue so we can tell if people or cars have died
PROC PROCESS_RAMPAGE_DAMAGE_EVENT_QUEUE()

    EVENT_NAMES eEventType
	STRUCT_ENTITY_ID sEntityID
	INT iCount = 0
	PED_INDEX ped 
	VEHICLE_INDEX car 
	
	// to stop things crying about unreference variable
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iCount
		eEventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iCount)
		
        SWITCH (eEventType)
            CASE EVENT_ENTITY_DAMAGED
				// Grab the event data and process also kill the ped on injury
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iCount, sEntityID, SIZE_OF(STRUCT_ENTITY_ID))
				
				IF DOES_ENTITY_EXIST(sEntityID.EntityId)
				
					IF IS_ENTITY_A_PED(sEntityID.EntityId)
						ped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.EntityId)
						IF IS_ENTITY_DEAD(ped)
							REGISTER_PED_DEATH(ped)
						ELIF IS_PED_INJURED(ped) 
							SET_ENTITY_HEALTH(ped, 0) // insta kill the ped
							REGISTER_PED_DEATH(ped)
						ENDIF
					ENDIF
					
					IF IS_ENTITY_A_VEHICLE(sEntityID.EntityId)
						car = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.EntityId)
						IF IS_ENTITY_DEAD(car) 
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( car, PLAYER_PED_ID() )
								REGISTER_CAR_DEATH(car)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC PROCESS_PED_SCANNER_VEHICLE_EXPLOSION_CHECKS(PED_INDEX ped)
	INT i, iCnt
	VEHICLE_INDEX array[20]
	
	IF IS_ENTITY_ALIVE(ped)
		iCnt = GET_PED_NEARBY_VEHICLES(ped, array)
		
		REPEAT iCnt i
			IF IS_ENTITY_DEAD(array[i])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(array[i], PLAYER_PED_ID() )
					REGISTER_CAR_DEATH(array[i], FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC PROCESS_ALL_PED_SCANNER_VEHICLE_EXPLOSION_CHECKS()
	INT i
	INT iScanCheck = 0
	
	PROCESS_PED_SCANNER_VEHICLE_EXPLOSION_CHECKS(PLAYER_PED_ID())
	
	REPEAT MAX_ACTIVE_PEDS i
		IF IS_ENTITY_ALIVE(rampagePed[iRampagePedScannerIndex].pedID)
			PROCESS_PED_SCANNER_VEHICLE_EXPLOSION_CHECKS(rampagePed[iRampagePedScannerIndex].pedID)
			iScanCheck ++
		ENDIF
		
		iRampagePedScannerIndex ++
		IF (iRampagePedScannerIndex >= MAX_ACTIVE_PEDS)
			iRampagePedScannerIndex = 0
		ENDIF
		
		IF (iScanCheck >= PEDS_TO_SCAN_PER_FRAME)
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

//----------------------
//	ASSET FUNCTIONS
//----------------------

/// PURPOSE:
///    Loads Relevant Text Assets and Dialog
PROC LOAD_TEXT_ASSETS()
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			rampageInfo.sDialogBlockName = "RAMP1AU"
			rampageInfo.sDialogRootLabel = "RAMP1_FIGHT"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			rampageInfo.sDialogBlockName = "RAMP2AU"
			rampageInfo.sDialogRootLabel = "RAMP2_FIGHT"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			rampageInfo.sDialogBlockName = "RAMP3AU"	
			rampageInfo.sDialogRootLabel = "RAMP3_FIGHT"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			rampageInfo.sDialogBlockName = "RAMP4AU"	
			rampageInfo.sDialogRootLabel = "RAMP4_FIGHT"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			rampageInfo.sDialogBlockName = "RAMP5AU"
			rampageInfo.sDialogRootLabel = "RAMP5_FIGHT"
		BREAK
		DEFAULT	
			SCRIPT_ASSERT("rampageInfo.sDialogRootLabel not set")
			EXIT
	ENDSWITCH
	
	SECURE_REQUEST_AND_LOAD_ADDITIONAL_TEXT(rampageInfo.sDialogBlockName, MISSION_DIALOGUE_TEXT_SLOT, FALSE)	
ENDPROC

/// PURPOSE:
///    Loads Relevant Mission Assets and Dialog	
/// PARAMS:
///    forceWait - if set we pause until this is completed
PROC LOAD_RAMPAGE_ASSETS(BOOL forceWait = TRUE)
	INT i
	
	#IF IS_DEBUG_BUILD
		IF (bDebugShowTTY)
			CPRINTLN(DEBUG_RAMPAGE, "LOADING RAMPAGE ASSETS")
		ENDIF
	#ENDIF
	
	// load text and scaleform
	LOAD_TEXT_ASSETS()
	
	SECURE_REQUEST_AND_LOAD_ADDITIONAL_TEXT("TRVRAMP", MISSION_TEXT_SLOT, forceWait)
	SECURE_REQUEST_AND_LOAD_SCALEFORM("MP_BIG_MESSAGE_FREEMODE", sFormRampage, forceWait)
	SECURE_REQUEST_AND_LOAD_AUDIO_BANK("SCRIPT\\RAMPAGE_01", forceWait)
	SECURE_REQUEST_AND_LOAD_AUDIO_BANK("SCRIPT\\RAMPAGE_02", forceWait)

	BOOL bFinishedLoad = FALSE
	
	if forceWait
		WHILE NOT bFinishedLoad
			IF LOAD_STREAM( "RAMPAGE_STREAMING_BED_MASTER" )
		 		CPRINTLN(DEBUG_RAMPAGE, "FINISHED LOADING RAMPAGE_STREAMING_BED_MASTER")
				bFinishedLoad = TRUE
			ELIF iWaitTime > 50
				CPRINTLN(DEBUG_RAMPAGE, "GIVING UP LOADING RAMPAGE_STREAMING_BED_MASTER")
				bFinishedLoad = TRUE
			ELSE
				iWaitTime++
				CPRINTLN(DEBUG_RAMPAGE, "WAITING FOR STREAM: RAMPAGE_STREAMING_BED_MASTER TO LOAD, ", iWaitTime)
			ENDIF
			WAIT(0)
		ENDWHILE
	else
		LOAD_STREAM( "RAMPAGE_STREAMING_BED_MASTER" )
	endif
	
	SECURE_REQUEST_AND_LOAD_ANIM_DICT( "missrampageintrooutro", forceWait)
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.plyrWeapon) i 
		IF (rampageInfo.plyrWeapon[i] <> WEAPONTYPE_INVALID) AND (rampageInfo.plyrWeapon[i] <> WEAPONTYPE_UNARMED)		
			SECURE_REQUEST_AND_LOAD_WEAPON_ASSET(rampageInfo.plyrWeapon[i], FALSE)
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.pedModels) i 
		IF (rampageInfo.pedModels[i] <> DUMMY_MODEL_FOR_SCRIPT)
			SECURE_REQUEST_AND_LOAD_MODEL(rampageInfo.pedModels[i], forceWait)
			SET_PED_MODEL_IS_SUPPRESSED(rampageInfo.pedModels[i], TRUE)
		ENDIF
	ENDREPEAT
	
	IF (rampageInfo.explosiveModel <> DUMMY_MODEL_FOR_SCRIPT)
		SECURE_REQUEST_AND_LOAD_MODEL(rampageInfo.explosiveModel, forceWait)
	ENDIF
	
	// load cars
	i = 0
	REPEAT COUNT_OF(rampageInfo.carModels) i 
		IF (rampageInfo.carModels[i] <> DUMMY_MODEL_FOR_SCRIPT)
			SECURE_REQUEST_AND_LOAD_MODEL(rampageInfo.carModels[i], forceWait)
		ENDIF
	ENDREPEAT	

	
	#IF IS_DEBUG_BUILD
		IF (bDebugShowTTY)
			CPRINTLN(DEBUG_RAMPAGE, "LOADING RAMPAGE ASSETS FINISHED")
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Checks if all assets are loaded
/// RETURNS:
///    True if they all are
FUNC BOOL ARE_RAMPAGE_ASSETS_LOADED()
	INT i
	INT cnt
	
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		RETURN FALSE
	ENDIF
	
	// check ped models loaded
	cnt = COUNT_OF(rampageInfo.pedModels)
	IF (rampageInfo.iMaxPedModels < cnt)
		cnt = rampageInfo.iMaxPedModels
	ENDIF
			
	IF (cnt > 0)
		i = 0
		REPEAT cnt i 
			IF NOT HAS_MODEL_LOADED(rampageInfo.pedModels[i])
				RETURN FALSE 
		 	ENDIF
		ENDREPEAT
	ENDIF
	
	// check car models loaded
	cnt = COUNT_OF(rampageInfo.carModels)
	IF (rampageInfo.iMaxCarModels < cnt)
		cnt = rampageInfo.iMaxCarModels
	ENDIF
	
	IF (cnt > 0)
		i = 0
		REPEAT cnt i 
			IF NOT HAS_MODEL_LOADED(rampageInfo.carModels[i])
				RETURN FALSE 
		 	ENDIF
		ENDREPEAT
	ENDIF
	
	//@RJP Check explosive model loaded
	IF rampageInfo.bExplosiveProps
		IF (rampageInfo.explosiveModel <> DUMMY_MODEL_FOR_SCRIPT)
			IF NOT HAS_MODEL_LOADED( rampageInfo.explosiveModel )
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\RAMPAGE_01")
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\RAMPAGE_02")
		RETURN FALSE
	ENDIF
	
	IF NOT LOAD_STREAM("RAMPAGE_STREAMING_BED_MASTER")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//----------------------
//	VOICE FUNCTIONS
//----------------------

/// PURPOSE:
///    Check if a index into the rampage voice list is okay
/// PARAMS:
///    ind - index #
FUNC BOOL IS_RAMPAGE_MALE_VOICE_OK(INT &ind)
	IF (ind >= COUNT_OF(sMaleVoiceNames))
		ind = 0
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sMaleVoiceNames[ind])
		ind = 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check if a index into the rampage voice list is okay
/// PARAMS:
///    ind - index #
FUNC BOOL IS_RAMPAGE_FEMALE_VOICE_OK(INT &ind)
	IF (ind >= COUNT_OF(sFemaleVoiceNames))
		ind = 0
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sFemaleVoiceNames[ind])
		ind = 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Sets up ped voices for a ped
/// PARAMS:
///    pedID - ped reference
PROC SETUP_SPECIFIC_PED_VOICE_CONTEXT(PED_INDEX &pedID)

	#IF IS_DEBUG_BUILD
		MODEL_NAMES pedModel = GET_ENTITY_MODEL(pedID)
	#ENDIF
	
	IF IS_PED_MALE(pedID)
		IF IS_RAMPAGE_MALE_VOICE_OK(iLastUsedMaleVoiceNameIndex)
			SET_AMBIENT_VOICE_NAME(pedID, sMaleVoiceNames[iLastUsedMaleVoiceNameIndex])
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_RAMPAGE, "Ambient Voice Applied:", GET_MODEL_NAME_FOR_DEBUG(pedModel), " ", sMaleVoiceNames[iLastUsedMaleVoiceNameIndex], " ", sVoiceContext)
			#ENDIF
			iLastUsedMaleVoiceNameIndex ++
		ELSE
		 	iLastUsedMaleVoiceNameIndex = 0
		ENDIF
	ELIF IS_RAMPAGE_FEMALE_VOICE_OK(iLastUsedFemaleVoiceNameIndex)
		SET_AMBIENT_VOICE_NAME(pedID, sFemaleVoiceNames[iLastUsedFemaleVoiceNameIndex])
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_RAMPAGE, "Ambient Voice Applied:", GET_MODEL_NAME_FOR_DEBUG(pedModel), " ", sFemaleVoiceNames[iLastUsedFemaleVoiceNameIndex], " ", sVoiceContext)
		#ENDIF
		
		iLastUsedFemaleVoiceNameIndex ++
	ELSE
	 	iLastUsedFemaleVoiceNameIndex = 0
	ENDIF
	
	IF NOT (rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2)
		IF NOT IS_STRING_NULL_OR_EMPTY(sVoiceContext)
			PLAY_PED_AMBIENT_SPEECH_NATIVE(pedID, sVoiceContext, "SPEECH_PARAMS_FORCE")
		ENDIF
	ENDIF
	
ENDPROC

PROC SETUP_SPECIFIC_HIPSTER_VOICE( RAMPAGE_PED &ped )
	
	IF iLastUsedRampage2VoiceIndex > 8
		iLastUsedRampage2VoiceIndex = 0
	ENDIF
	
	SWITCH iLastUsedRampage2VoiceIndex
		CASE 0	ped.sContext = "RAMP2_HIP9"	ped.sVoice = "RAMPAGEHIPSTER9"	BREAK
		CASE 1	ped.sContext = "RAMP2_HIP1"	ped.sVoice = "RAMPAGEHIPSTER"	BREAK
		CASE 2	ped.sContext = "RAMP2_HIP2"	ped.sVoice = "RAMPAGEHIPSTER2"	BREAK
		CASE 3	ped.sContext = "RAMP2_HIP3"	ped.sVoice = "RAMPAGEHIPSTER3"	BREAK
		CASE 4	ped.sContext = "RAMP2_HIP4"	ped.sVoice = "RAMPAGEHIPSTER4"	BREAK
		CASE 5	ped.sContext = "RAMP2_HIP5"	ped.sVoice = "RAMPAGEHIPSTER5"	BREAK
		CASE 6	ped.sContext = "RAMP2_HIP6"	ped.sVoice = "RAMPAGEHIPSTER6"	BREAK
		CASE 7	ped.sContext = "RAMP2_HIP7"	ped.sVoice = "RAMPAGEHIPSTER7"	BREAK
		CASE 8	ped.sContext = "RAMP2_HIP8"	ped.sVoice = "RAMPAGEHIPSTER8"	BREAK
	ENDSWITCH
	
	ADD_PED_FOR_DIALOGUE( convoBattleRant, (iLastUsedRampage2VoiceIndex), ped.pedID, ped.sVoice )
	iLastUsedRampage2VoiceIndex++
	IF iLastUsedRampage2VoiceIndex = 2
		iLastUsedRampage2VoiceIndex++
	ENDIF
ENDPROC

PROC RAMPAGE2_HIPSTER_INSULTS( RAMPAGE_PED &ped )
	IF NOT (rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2)
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY( ped.sContext )
	OR IS_STRING_NULL_OR_EMPTY( ped.sVoice )
		SETUP_SPECIFIC_HIPSTER_VOICE( ped )
	ENDIF
	
	IF ped.iInsultTimer = 0
		ped.iInsultTimer = GET_GAME_TIMER()+GET_RANDOM_INT_IN_RANGE(5000,15000)
	ENDIF
	
	IF GET_GAME_TIMER() > ped.iInsultTimer
		CREATE_CONVERSATION( convoBattleRant, rampageInfo.sDialogBlockName, ped.sContext, CONV_PRIORITY_LOW, DO_NOT_DISPLAY_SUBTITLES )
		ped.iInsultTimer = GET_GAME_TIMER()+GET_RANDOM_INT_IN_RANGE(10000,20000)
	ENDIF
ENDPROC

//----------------------
//	ENTITY FUNCTIONS
//----------------------

/// PURPOSE:
///    Returns a random ped model from the current rampage info struct
/// RETURNS:
///    Model Name or null it can't pick one
FUNC MODEL_NAMES GET_RANDOM_RAMPAGE_PED_MODEL()
	IF (rampageInfo.iMaxPedModels = 0)
		SCRIPT_ASSERT("NO RAMPAGE PED MODELS DEFINED!!")
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	RETURN rampageInfo.pedModels[GET_RANDOM_INT_IN_RANGE(0, rampageInfo.iMaxPedModels)]
ENDFUNC

/// PURPOSE:
///    Assigns the Players Rampage Weapons and Weapon Components
PROC GIVE_PLAYER_RAMPAGE_WEAPONS()
	INT i 
	
	// give weapons
	i = 0
	SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 15, FALSE, FALSE)
	
	REPEAT COUNT_OF(rampageInfo.plyrWeapon) i 
		IF (rampageInfo.plyrWeapon[i] <> WEAPONTYPE_INVALID)
			CPRINTLN(DEBUG_RAMPAGE, "GIVING PLAYER WEAPON")
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), rampageInfo.plyrWeapon[i], INFINITE_AMMO , TRUE, TRUE) // INFINITE_AMMO 
			ADD_AMMO_TO_PED(PLAYER_PED_ID(), rampageInfo.plyrWeapon[i], INFINITE_AMMO) // just in case someone turns up to the rampage with weapons with no ammo
			SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), TRUE, rampageInfo.plyrWeapon[i])
		ENDIF
	ENDREPEAT
	
	// NOTE: The weapon components are only for plyrWeapon[0] as this is the primary weapon
	IF (rampageInfo.plyrWeapon[0] <> WEAPONTYPE_INVALID) AND (rampageInfo.plyrWeapon[0] <> WEAPONTYPE_UNARMED)
		i = 0
		REPEAT COUNT_OF(rampageInfo.plyrWeaponComp) i 
			IF (rampageInfo.plyrWeaponComp[i] <> WEAPONCOMPONENT_INVALID)
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), rampageInfo.plyrWeapon[0], rampageInfo.plyrWeaponComp[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
	// force weapon in player hand
	CPRINTLN(DEBUG_RAMPAGE, "ARMS INSTALLATION IS COMPLETE - GOOD LUCK!")
	IF (rampageInfo.plyrWeapon[0] <> WEAPONTYPE_INVALID)
		CPRINTLN(DEBUG_RAMPAGE, "EQUIPING PLAYER FIRST WEAPON")
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), rampageInfo.plyrWeapon[0], TRUE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
	ENDIF
	
	CPRINTLN(DEBUG_RAMPAGE, "ACTION MODE INITIALIZED")
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1,"TREVOR_ACTION")
	//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ACTIONMODE_IDLE, TRUE)
ENDPROC

/// PURPOSE:
///    Assigns the Players Rampage Weapons and Weapon Components
PROC RESET_PLAYER_RAMPAGE_INFINITE_AMMO()
	INT i = 0
	REPEAT COUNT_OF(rampageInfo.plyrWeapon) i 
		IF (rampageInfo.plyrWeapon[i] <> WEAPONTYPE_INVALID)
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), rampageInfo.plyrWeapon[i])
				SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), FALSE, rampageInfo.plyrWeapon[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//----------------------
//	PED FUNCTIONS
//----------------------

/// PURPOSE:
///    Hacky function to fix B*1035547
/// PARAMS:
///    pedID - ped to do the running
///    pos - position to get to
///    bForceNavMesh - force nav mesh use - (don't use this by the military base)
PROC RAMPAGE_SMART_FLEE_HACKERY(PED_INDEX pedID, VECTOR pos, BOOL bForceNavMesh = TRUE)
	SEQUENCE_INDEX seqTask
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, bBlockRampageTempEvents)
	SET_PED_COMBAT_ATTRIBUTES(pedID, CA_ALWAYS_FLEE, TRUE)
	
	OPEN_SEQUENCE_TASK(seqTask)
		IF (bForceNavMesh)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, pos, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING )
		ELSE
			TASK_GO_TO_COORD_ANY_MEANS(NULL, pos, PEDMOVE_RUN, NULL)
		ENDIF
		TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 600.0, -1)
	CLOSE_SEQUENCE_TASK(seqTask)
	TASK_PERFORM_SEQUENCE(pedID, seqTask)
	CLEAR_SEQUENCE_TASK(seqTask)	
	
	FORCE_PED_MOTION_STATE(pedID, MS_ON_FOOT_RUN)
ENDPROC

/// PURPOSE:
///    Hacky function to fix B*1035547
/// PARAMS:
///    pedID - ped to do the running
///    pos - position to get to
PROC RAMPAGE_SMART_FLEE_ATTACK_HACKERY(PED_INDEX pedID, VECTOR pos, FLOAT spd = PEDMOVE_RUN)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, bBlockRampageTempEvents)
	FORCE_PED_MOTION_STATE(pedID, MS_ON_FOOT_RUN)
	TASK_GO_STRAIGHT_TO_COORD(pedID, pos, spd)
ENDPROC

/// PURPOSE:
///    Assigns a rampage ped his weapons and components
/// PARAMS:
///    ped - rampage ped reference
PROC ASSIGN_RAMPAGE_PED_WEAPON(RAMPAGE_PED &ped)

	IF IS_PED_IN_ANY_VEHICLE(ped.pedID)
		GIVE_WEAPON_TO_PED(ped.pedID, WEAPONTYPE_PISTOL, INFINITE_AMMO)
		SET_PED_INFINITE_AMMO(ped.pedID, TRUE, WEAPONTYPE_PISTOL)	
	ENDIF
	
	IF (GET_RANDOM_FLOAT_IN_RANGE() < fEnemySecondWeaponChance) AND (rampageInfo.enemyWeapon[1] <> WEAPONTYPE_INVALID)
		GIVE_WEAPON_TO_PED(ped.pedID, rampageInfo.enemyWeapon[1], INFINITE_AMMO ) // INFINITE_AMMO 
		SET_PED_INFINITE_AMMO(ped.pedID, TRUE, rampageInfo.enemyWeapon[1])
		APPLY_COMBAT_PARAMETERS_TO_PED(ped.pedID, rampageCombatParams, TRUE)
	ELIF (rampageInfo.enemyWeapon[0] <> WEAPONTYPE_INVALID)
		GIVE_WEAPON_TO_PED(ped.pedID, rampageInfo.enemyWeapon[0], INFINITE_AMMO ) // INFINITE_AMMO 
		SET_PED_INFINITE_AMMO(ped.pedID, TRUE, rampageInfo.enemyWeapon[0])
		APPLY_COMBAT_PARAMETERS_TO_PED(ped.pedID, rampageCombatParams)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up hp, relationship groups and other things for ped
/// PARAMS:
///    ped - reference to rampage ped 
PROC SET_RAMAGER_PED_BATTLE_ATTRIBUTES(RAMPAGE_PED &ped, BOOL bAssignWeapon = TRUE)
//	INT rnd 
	IF NOT IS_PED_UNINJURED(ped.pedID)
		EXIT
	ENDIF
	
	IF (bAssignWeapon)
		ASSIGN_RAMPAGE_PED_WEAPON(ped)
	ENDIF
	
	// health and damage
	ped.fOriginalDist = GET_DISTANCE_BETWEEN_ENTITIES(ped.pedID, PLAYER_PED_ID(), FALSE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped.pedID, activeGroup)
	SET_PED_CAN_BE_TARGETTED(ped.pedID, TRUE)
	
	SET_ENTITY_HEALTH(ped.pedID, RAMPAGE_PED_MAXHP)
	SET_PED_MAX_HEALTH(ped.pedID, RAMPAGE_PED_MAXHP)
	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(ped.pedID, TRUE, RELGROUPHASH_PLAYER)

	SET_PED_PATH_AVOID_FIRE(ped.pedID, TRUE)
	SET_PED_SUFFERS_CRITICAL_HITS(ped.pedID, TRUE)
	SET_PED_CAN_BE_TARGETED_WHEN_INJURED(ped.pedID, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(ped.pedID, TRUE)
	SET_PED_DIES_WHEN_INJURED(ped.pedID, TRUE)
	SET_PED_PATH_MAY_ENTER_WATER(ped.pedID, FALSE)
	SET_PED_PATH_PREFER_TO_AVOID_WATER(ped.pedID, TRUE)
			
//	SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped.pedID, FALSE) 
	SET_PED_MONEY(ped.pedID, RAMPAGE_PED_MONEY)
	
	// flee and combat attributes
	SET_PED_FLEE_ATTRIBUTES(ped.pedID, FA_USE_COVER, TRUE)	
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_USE_COVER, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_REQUIRES_LOS_TO_SHOOT, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(ped.pedID, TLR_NEVER_LOSE_TARGET) 
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_ENABLE_TACTICAL_POINTS_WHEN_DEFENSIVE, TRUE)
	
	//@RJP all rampages should have aggressive enemies
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_MOVEMENT(ped.pedID, CM_WILLADVANCE)
	
	//@RJP old version, random chance of different combat movements
	// setup combat movement
//	rnd = GET_RANDOM_INT_IN_RANGE(0, 4)
//	SWITCH (rnd)
//		CASE 0
//	    	SET_PED_COMBAT_MOVEMENT(ped.pedID, CM_DEFENSIVE)             // Seeks a defensive position.
//		BREAK
//		CASE 1
//	    	SET_PED_COMBAT_MOVEMENT(ped.pedID, CM_WILLADVANCE)         // Will advance forward in combat
//			SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)	
//		BREAK
//		CASE 2
//	    	SET_PED_COMBAT_MOVEMENT(ped.pedID, CM_WILLRETREAT)          // Will retreat if the enemy gets too close
//			SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)	
//		BREAK
//		DEFAULT
//			 SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)	
//		BREAK
//	ENDSWITCH
	
	// misc stuff
	SETUP_SPECIFIC_PED_VOICE_CONTEXT(ped.pedID)
	REGISTER_HATED_TARGETS_AROUND_PED(ped.pedID, 200.0)
	SET_PED_MONEY(ped.pedID, RAMPAGE_PED_MONEY)
ENDPROC

/// PURPOSE:
///    Deletes ampage ped - deletes reference and removes blip
/// PARAMS:
///    ped - reference to rampage ped 
PROC SHARED_CLEAN_AND_DELETE_RAMPAGE_PED(RAMPAGE_PED &ped)
	SAFE_REMOVE_BLIP(ped.blipID)
	ped.blipID = NULL
	
	ped.iDeleteTime = 0
	ped.fPlayerDist = 0.0
	ped.bWasInVehicle = FALSE
	ped.bIsFleePed = FALSE
	ped.bMolotovThrower = FALSE
	
	ped.sVoice = NULL
	ped.sContext = NULL
	ped.iInsultTimer = 0
	
	#IF IS_DEBUG_BUILD
		ped.bDebugAliveCheck = FALSE 
	#ENDIF
	
	// drop the counter that tells us how many people are active
	IF (ped.iFortIndex <> -1)
		rampageFort[ped.iFortIndex].iPedCount --
		IF (rampageFort[ped.iFortIndex].iPedCount < 0)
			rampageFort[ped.iFortIndex].iPedCount = 0
		ENDIF
		ped.iFortIndex = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes ampage ped - deletes reference and removes blip
/// PARAMS:
///    ped - reference to rampage ped 
PROC DELETE_RAMPAGE_PED(RAMPAGE_PED &ped)
	IF NOT DOES_ENTITY_EXIST(ped.pedID)
		EXIT
	ENDIF
	
	SHARED_CLEAN_AND_DELETE_RAMPAGE_PED(ped)
	SAFE_DELETE_PED(ped.pedID)
	ped.pedID = NULL
ENDPROC

/// PURPOSE:
///    Cleans up rampage ped - release reference and removes blip
/// PARAMS:
///    ped - reference to rampage ped 
///    keepTask - if this is true ped will retain his task after cleanup
PROC CLEANUP_RAMPAGE_PED(RAMPAGE_PED &ped, BOOL keepTask = FALSE)
	IF NOT DOES_ENTITY_EXIST(ped.pedID)
		EXIT
	ENDIF
	
	// B*650069 - Allows peds who are on roofs to leave once rampage is finished
	IF IS_ENTITY_ALIVE(ped.pedID)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.pedID, TRUE)
		SET_PED_PATH_CAN_USE_LADDERS(ped.pedID, TRUE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.pedID, TRUE)
	ENDIF
	
	// stop targeting as soon as people are cleaned up
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.pedID, keepTask) // so that they follow orders
	SET_ENTITY_IS_TARGET_PRIORITY(ped.pedID, FALSE)
	SET_PED_CAN_BE_TARGETTED(ped.pedID, FALSE)
	SET_PED_CAN_BE_TARGETED_WITHOUT_LOS(ped.pedID, FALSE)
	
	SHARED_CLEAN_AND_DELETE_RAMPAGE_PED(ped)
	SAFE_RELEASE_PED(ped.pedID, keepTask)
	ped.pedID = NULL
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage peds
/// PARAMS:
///    array - reference to ped array
///    keepTask - if this is true peds will retain tasks after cleanup 
PROC CLEANUP_RAMPAGE_PED_ARRAY(RAMPAGE_PED& array[], BOOL keepTask = FALSE)
	INT i
	
	REPEAT COUNT_OF(array) i
		CLEANUP_RAMPAGE_PED(array[i], keepTask)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Deletes an entire array of rampage peds 
/// PARAMS:
///    array - reference to ped array
PROC DELETE_RAMPAGE_PED_ARRAY(RAMPAGE_PED& array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		DELETE_RAMPAGE_PED(array[i])
	ENDREPEAT
ENDPROC

PROC FORCE_DELETE_RAMPAGE_PED_ARRAY(RAMPAGE_PED &array[])

	// loop through all peds and delete them
	INT i
	DELETE_RAMPAGE_PED_ARRAY(array)

	// then run through the ped scanner and wipe everything within x meters of player
	INT cnt
	PED_INDEX pedIndex[32]

	cnt = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedIndex)
	IF (cnt = 0) 
		EXIT
	ENDIF

	CPRINTLN(DEBUG_MINIGAME, "PEDS SCANNED:", cnt)
	
	REPEAT cnt i
		IF DOES_ENTITY_EXIST(pedIndex[i])
			IS_ENTITY_ALIVE(pedIndex[i]) // we need to call this so we don't get an assert we still want to delete dead peds
			IF IS_PED_ENEMY_PED_MODEL(pedIndex[i])
				SAFE_DELETE_PED(pedIndex[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage peds while making them flee
/// PARAMS:
///    array - reference to ped array
///    keepTask - if this is true peds will retain tasks after cleanup 
PROC FLEE_RAMPAGE_PED(RAMPAGE_PED &ped)
	IF NOT DOES_ENTITY_EXIST(ped.pedID)
		EXIT
	ENDIF
	
	IF IS_PED_UNINJURED(ped.pedID)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.pedID, TRUE)
		SET_PED_PATH_CAN_USE_LADDERS(ped.pedID, TRUE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.pedID, TRUE)
		CLEAR_PED_TASKS(ped.pedID)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.pedID, bBlockRampageTempEvents)
		TASK_SMART_FLEE_PED(ped.pedID, PLAYER_PED_ID(), 400.0, -1)
//		SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped.pedID, FALSE) 
		SET_PED_MONEY(ped.pedID, RAMPAGE_PED_MONEY)
	ELSE
		#IF IS_JAPANESE_BUILD
			DELETE_RAMPAGE_PED(ped)
		#ENDIF
	ENDIF
	
	CLEANUP_RAMPAGE_PED(ped, TRUE)
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage peds while making them flee
/// PARAMS:
///    array - reference to ped array
///    keepTask - if this is true peds will retain tasks after cleanup 
PROC FLEE_RAMPAGE_PED_ARRAY(RAMPAGE_PED& array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		FLEE_RAMPAGE_PED(array[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets an entire array of rampage peds to go into combat mode
/// PARAMS:
///    array - reference to ped array
PROC FORCE_RAMPAGE_PED_ARRAY_COMBAT(RAMPAGE_PED& array[])
	INT i
	
	CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE ARRAY IS FORCED INTO COMBAT")
	REPEAT COUNT_OF(array) i
		IF IS_PED_UNINJURED(array[i].pedID) 
			IF NOT array[i].bIsFleePed
				IF NOT IS_PED_IN_ANY_VEHICLE(array[i].pedID) AND (array[i].bIsFleePed = FALSE)
					TASK_COMBAT_PED(array[i].pedID, PLAYER_PED_ID())
//					SET_PED_DROPS_WEAPONS_WHEN_DEAD(array[i].pedID, FALSE) 
					SET_PED_MONEY(array[i].pedID, RAMPAGE_PED_MONEY)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Searches Rampage Ped Array for a free index
/// PARAMS:
///    ind - this index is written to
/// RETURNS:
///    true if there is an index free
FUNC BOOL GET_FREE_RAMPAGE_PED_INDEX(RAMPAGE_PED& array[], INT &ind)
	INT i
	
	REPEAT COUNT_OF(array) i
		IF NOT DOES_ENTITY_EXIST(array[i].pedID)
			ind = i 
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns number of rampage peds who are alive
FUNC INT GET_ALIVE_RAMPAGE_PED_COUNT(RAMPAGE_PED& array[])
	INT i
	INT cnt = 0
	
	REPEAT COUNT_OF(array) i
		IF IS_PED_UNINJURED(array[i].pedID)
			#IF IS_DEBUG_BUILD
				array[i].bDebugAliveCheck = TRUE
			#ENDIF
			cnt ++
		ELSE
			#IF IS_DEBUG_BUILD
				array[i].bDebugAliveCheck = FALSE
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN cnt
ENDFUNC

PROC SET_PED_DEBUG_NAME( PED_INDEX &ped, STRING sName, INT pedIndex )
	text_label tdebugname = sName 
	tdebugname += pedIndex
	SET_PED_NAME_DEBUG(ped, tdebugname)
ENDPROC

/// PURPOSE:
///    Creates a rampage ped at a given position
/// PARAMS:
///    ped - rampage ped reference
///    pos - spawn position
///    head - spawn heading
///    newvariation - set this to try to force random ped components
/// RETURNS:
///    true - if successful
FUNC BOOL CREATE_RAMPAGER_PED_AT_POSITION(RAMPAGE_PED &ped, VECTOR pos, FLOAT head = 0.0, BOOL newvariation = TRUE)
	MODEL_NAMES pedModel = GET_RANDOM_RAMPAGE_PED_MODEL()
	
	#IF IS_JAPANESE_BUILD
		IF (GET_RAMPAGE_REMAINING_TIME_IN_MILLISECONDS() < 2000)
			RETURN FALSE
		ENDIF
	#ENDIF
	
	ped.pedID = CREATE_PED(PEDTYPE_MISSION, pedModel, pos, head)
	IF IS_ENTITY_IN_WATER(ped.pedID)
		CPRINTLN(DEBUG_RAMPAGE, "TRYING TO CREATE PED WHO IS IN WATER")
		DELETE_RAMPAGE_PED(ped)
		RETURN FALSE 
	ENDIF
	
	SAFE_REMOVE_BLIP(ped.blipID)
	ped.blipID = CREATE_PED_BLIP(ped.pedID)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(ped.pedID, activeGroup)
	IF (newvariation = TRUE)
		SET_PED_RANDOM_COMPONENT_VARIATION(ped.pedID)
	ENDIF
	
	SET_ENTITY_COORDS_GROUNDED(ped.pedID, pos)
	SET_RAMAGER_PED_BATTLE_ATTRIBUTES(ped)
	
	IF (head = 0.0)
		SET_ENTITY_HEADING_FACE_PLAYER(ped.pedID)
	ENDIF
	
	CPRINTLN(DEBUG_RAMPAGE, "Ped ", ped.iIndex ," was created")
	TASK_COMBAT_PED(ped.pedID, PLAYER_PED_ID())
	#IF IS_DEBUG_BUILD
		SET_PED_DEBUG_NAME( ped.pedID, "RampPed", ped.iIndex )
	#ENDIF
//	SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped.pedID, FALSE) 
	
	rampageStats.iPedsSpawned ++
	iAlivePedCounter ++
	ped.bIsFleePed = FALSE
	ped.bMolotovThrower = FALSE
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates a rampage ped from a supplied index (useful for turning ambient peds into rampage peds)
/// PARAMS:
///    ped - rampage ped reference
///    nped - ped index
///    flee - should we make this a flee type ped
///    newvariation - set this to try to force random ped components
/// RETURNS:
///    true - if successful
FUNC BOOL CREATE_RAMPAGER_PED_FROM_PED_INDEX(RAMPAGE_PED &ped, PED_INDEX &nped, BOOL flee = FALSE, BOOL newvariation = TRUE, BOOL newTask = TRUE)
	IF IS_ENTITY_IN_WATER(nped)
		CPRINTLN(DEBUG_RAMPAGE, "TRYING TO GRAB PED WHO IS IN WATER")
		RETURN FALSE 
	ENDIF
	
	ped.pedID = nped

	SAFE_REMOVE_BLIP(ped.blipID)
	ped.blipID = CREATE_PED_BLIP(ped.pedID)
	
	FREEZE_ENTITY_POSITION(ped.pedID, FALSE)
	SET_ENTITY_COLLISION(ped.pedID, TRUE)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(ped.pedID, activeGroup)
	IF (newvariation = TRUE)
		SET_PED_RANDOM_COMPONENT_VARIATION(ped.pedID)
	ENDIF
	SET_PED_CAN_BE_TARGETTED(ped.pedID, TRUE)
	SET_RAMAGER_PED_BATTLE_ATTRIBUTES(ped)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.pedID, TRUE)
	
	IF (newTask = TRUE)
		IF (flee = FALSE)
			#IF IS_DEBUG_BUILD
				SET_PED_DEBUG_NAME( ped.pedID, "RampPed", ped.iIndex )
			#ENDIF
			TASK_COMBAT_PED(ped.pedID, PLAYER_PED_ID())
		ELSE
			#IF IS_DEBUG_BUILD
				SET_PED_DEBUG_NAME( ped.pedID, "RampFlee", ped.iIndex )
			#ENDIF
			ped.bIsFleePed = TRUE
			SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_ALWAYS_FLEE, TRUE)
			TASK_SMART_FLEE_PED(ped.pedID, PLAYER_PED_ID(), 600.0, -1)
		ENDIF
	ELSE
		IF (flee = TRUE)
			#IF IS_DEBUG_BUILD
				SET_PED_DEBUG_NAME( ped.pedID, "RampFlee", ped.iIndex )
			#ENDIF
			ped.bIsFleePed = TRUE
		ENDIF
	ENDIF
	
	rampageStats.iPedsSpawned ++
	iAlivePedCounter ++
	ped.bIsFleePed = flee
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates a rampage flee ped at a given position
/// PARAMS:
///    ped - rampage ped reference
///    pos - spawn position
///    head - spawn heading
///    newvariation - set this to try to force random ped components
/// RETURNS:
///    true - if successful
FUNC BOOL CREATE_RAMPAGER_FLEE_PED_AT_POSITION(RAMPAGE_PED &ped, VECTOR pos, FLOAT head = 0.0, BOOL newvariation = TRUE)
	BOOL boolok
	MODEL_NAMES pedModel = GET_RANDOM_RAMPAGE_PED_MODEL()
	
	#IF IS_JAPANESE_BUILD
		IF (GET_RAMPAGE_REMAINING_TIME_IN_MILLISECONDS() < 2000)
			RETURN FALSE
		ENDIF
	#ENDIF
	
	ped.pedID = CREATE_PED(PEDTYPE_MISSION, pedModel, pos, head)
	IF IS_ENTITY_IN_WATER(ped.pedID)
		CPRINTLN(DEBUG_RAMPAGE, "TRYING TO CREATED PED IN WATER - NAH MATE")
		DELETE_RAMPAGE_PED(ped)
		RETURN FALSE 
	ENDIF
	
	SAFE_REMOVE_BLIP(ped.blipID)
	ped.blipID = CREATE_PED_BLIP(ped.pedID)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(ped.pedID, activeGroup)
	IF (newvariation = TRUE)
		SET_PED_RANDOM_COMPONENT_VARIATION(ped.pedID)
	ENDIF
	
	REMOVE_ALL_PED_WEAPONS(ped.pedID)
	SET_ENTITY_COORDS_GROUNDED(ped.pedID, pos)
	SET_PED_MONEY(ped.pedID, RAMPAGE_PED_MONEY)
//	SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped.pedID, FALSE) 
				
	SET_ENTITY_HEALTH(ped.pedID, RAMPAGE_PED_MAXHP)
	SET_PED_MAX_HEALTH(ped.pedID, RAMPAGE_PED_MAXHP)
	
	SET_PED_CAN_BE_TARGETED_WHEN_INJURED(ped.pedID, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(ped.pedID, TRUE)
	
	SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(ped.pedID, TRUE, RELGROUPHASH_PLAYER)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.pedID, FALSE)
	SET_PED_FLEE_ATTRIBUTES(ped.pedID, FA_USE_COVER, TRUE)
	SET_PED_FLEE_ATTRIBUTES(ped.pedID, FA_WANDER_AT_END, TRUE)
	SET_PED_FLEE_ATTRIBUTES(ped.pedID, FA_CAN_SCREAM, TRUE)
	
	
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_USE_COVER, TRUE)

	boolok = TRUE
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_JUST_SEEK_COVER, boolok)
	SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_ALWAYS_FLEE, boolok)
	//TASK_WANDER_STANDARD(ped.pedID)
	TASK_SMART_FLEE_PED(ped.pedID, PLAYER_PED_ID(), 600.0, -1)
	
	rampageStats.iPedsSpawned ++
	iAlivePedCounter ++
	
	#IF IS_DEBUG_BUILD
		SET_PED_DEBUG_NAME( ped.pedID, "RampFlee", ped.iIndex )
	#ENDIF
	
	SETUP_SPECIFIC_PED_VOICE_CONTEXT(ped.pedID)
	SET_PED_ACCURACY(ped.pedID, 2)
	SET_PED_COMBAT_ABILITY(ped.pedID, CAL_POOR)
	
	ped.bIsFleePed = TRUE
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates a rampage ped at a rampage spawn point
/// PARAMS:
///    ped - rampage ped reference
///    sp - spawn point variable
///    newvariation - set this to try to force random ped components
/// RETURNS:
///    true - if successful
FUNC BOOL CREATE_RAMPAGER_PED_AT_SPAWN_POINT(RAMPAGE_PED &ped, RAMPAGE_SPAWNPOINT sp, BOOL newvariation = TRUE, BOOL occuldeCheck = FALSE)

	// check if entity would be visible - just check against pedmodel 0 as most peds are about the same size
	IF (occuldeCheck = TRUE)
		IF NOT WOULD_ENTITY_BE_OCCLUDED(rampageInfo.pedModels[0], sp.vPosition)	
			RETURN FALSE
		ENDIF	
	ENDIF
	
	// create the ped
	IF NOT CREATE_RAMPAGER_PED_AT_POSITION(ped, sp.vPosition, sp.fHeading, newvariation)
		RETURN FALSE
	ENDIF

	// stop people from using climbovers and ladders if spawn point is aerial
	IF (sp.bIsAerial = TRUE)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.pedID, FALSE)
		SET_PED_PATH_CAN_USE_LADDERS(ped.pedID, FALSE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.pedID, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped.pedID, CA_CAN_CHARGE, FALSE)
		
		SET_PED_SPHERE_DEFENSIVE_AREA(ped.pedID, sp.vPosition, 2.0)
		SET_PED_COMBAT_MOVEMENT(ped.pedID, CM_STATIONARY)
	ELSE
		SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.pedID, TRUE)
		SET_PED_PATH_CAN_USE_LADDERS(ped.pedID, TRUE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.pedID, TRUE)
		
		SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.pedID, PLAYER_PED_ID(), <<0, 0, 0>>, GET_RANDOM_FLOAT_IN_RANGE(10.0, 25.0), FALSE)	
	ENDIF
	
	TASK_COMBAT_PED(ped.pedID, PLAYER_PED_ID())
	SET_PED_MONEY(ped.pedID, RAMPAGE_PED_MONEY)
//	SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped.pedID, FALSE) 
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates a rampage ped in a vehicle and assigns him a seat
/// PARAMS:
///    ped - rampage ped reference
///    veh - vehicle index
/// RETURNS:
///    true on success
FUNC BOOL CREATE_RAMPAGE_PED_IN_VEHICLE(RAMPAGE_PED &ped, VEHICLE_INDEX veh)
	
	MODEL_NAMES pedModel = GET_RANDOM_RAMPAGE_PED_MODEL()
	
	ped.pedID = CREATE_PED_INSIDE_VEHICLE_AUTO_SEAT(pedModel, veh, WEAPONTYPE_PISTOL, TRUE, bBlockRampageTempEvents)
	IF NOT IS_ENTITY_ALIVE(ped.pedID)
		RETURN FALSE
	ENDIF
	
	ped.bIsFleePed = FALSE
	ped.bMolotovThrower = FALSE
	
	SET_RAMAGER_PED_BATTLE_ATTRIBUTES(ped, NOT IS_PED_IN_ANY_HELI(ped.pedID))
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.pedID, bBlockRampageTempEvents)
	REGISTER_HATED_TARGETS_AROUND_PED(ped.pedID, 200.0)
	SET_PED_MONEY(ped.pedID, RAMPAGE_PED_MONEY)
//	SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped.pedID, FALSE) 
	
	rampageStats.iPedsSpawned ++
	iAlivePedCounter ++
	
	ped.bWasInVehicle = FALSE // we set this to false initially as the update handles blipping now
	CPRINTLN(DEBUG_RAMPAGE, "Ped ", ped.iIndex ," is created in vehicle")
	#IF IS_DEBUG_BUILD
		SET_PED_DEBUG_NAME( ped.pedID, "RampCar", ped.iIndex )
	#ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates a rampage ped and ties him to rampage fortress
/// PARAMS:
///    fort - rampage fortress reference
///    pos - spawn position
///    iFortIndex - index of the fortress
///    newvariation - set this to try to force random ped components
/// RETURNS:
///    true - if successful  
FUNC BOOL CREATE_RAMPAGER_PED_IN_FORTRESS_AT_POSITION(RAMPAGE_FORTRESS &fort, VECTOR pos, INT iFortIndex = -1, BOOL newvariation = TRUE)
	INT ind 
	
	IF NOT GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
		RETURN FALSE
	ENDIF
	
	IF NOT CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], pos, GET_RANDOM_FLOAT_IN_RANGE() * 360.0, newvariation)
		RETURN FALSE
	ENDIF 
		
	//@RJP all peds should advance on player from fortresses, NO HANGING BACK!  Commenting out old system
	SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), <<GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), GET_RANDOM_FLOAT_IN_RANGE(-10.0, 10.0), 0>>, GET_RANDOM_FLOAT_IN_RANGE(10.0, 25.0))
//	IF (GET_RANDOM_FLOAT_IN_RANGE() < fort.fRushFactor)
//		SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), <<GET_RANDOM_FLOAT_IN_RANGE(-15.0, 15.0), GET_RANDOM_FLOAT_IN_RANGE(-15.0, 15.0), 0>>, GET_RANDOM_FLOAT_IN_RANGE(15.0, 40.0))
//	ELSE
//		SET_PED_ANGLED_DEFENSIVE_AREA(rampagePed[ind].pedID, fort.defenseArea.vPosition[0], fort.defenseArea.vPosition[1], fort.defenseArea.fWidth * 1.5)
//	ENDIF
	
	IF (fort.fRoofHeight > 0.0)
		IF (pos.z >= fort.fRoofHeight)
			SET_PED_PATH_CAN_USE_CLIMBOVERS(rampagePed[ind].pedID, FALSE)
			SET_PED_PATH_CAN_USE_LADDERS(rampagePed[ind].pedID, FALSE)
			SET_PED_PATH_CAN_DROP_FROM_HEIGHT(rampagePed[ind].pedID, FALSE)
		ENDIF
	ELSE
		// let some people have a secondary defense area
		IF (GET_RANDOM_FLOAT_IN_RANGE() < 0.5)
			SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), <<0, 0, 0>>, GET_RANDOM_FLOAT_IN_RANGE(5.0, 20.0), TRUE)
		ENDIF 
	ENDIF
	
	// mark the rampage ped as belonging to a fortress
	rampagePed[ind].iFortIndex = iFortIndex
	fort.iPedCount ++
	fort.iSpawnedCount ++	
		
	// handle increased spawn count and auto shutdown of fortress when enough peds have been spawned
	IF (fort.iSpawnedCount >= fort.iMaxSpawnCount)
		fort.iSpawnedCount = 0
		fort.bIsActive = FALSE
		
		IF (fort.iNextFortress <> -1)
			rampageFort[fort.iNextFortress].bIsActive = TRUE
			rampageFort[fort.iNextFortress].iSpawnedCount = 0

			IF (rampageFort[fort.iNextFortress].iMaxActivePeds <> -1)
				iMaxActivePeds = rampageFort[fort.iNextFortress].iMaxActivePeds
			ENDIF
		ENDIF
	ENDIF
	
	SET_PED_MONEY(rampagePed[ind].pedID, RAMPAGE_PED_MONEY)
//	SET_PED_DROPS_WEAPONS_WHEN_DEAD(rampagePed[ind].pedID, FALSE) 
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Updates rampage ped - handles auto cleanup and blip switching
/// PARAMS:
///    ped - reference to rampage ped 
PROC UPDATE_RAMPAGE_PED(RAMPAGE_PED &ped)
	VECTOR pos 
	BOOL inCar
	IF NOT DOES_ENTITY_EXIST(ped.pedID)
		EXIT 
	ENDIF
	
	IF IS_PED_INJURED(ped.pedID) OR IS_ENTITY_DEAD(ped.pedID)
		CPRINTLN(DEBUG_RAMPAGE, "Ped ", ped.iIndex ," is dead or injured")
		#IF IS_JAPANESE_BUILD
			IF GET_GAME_TIMER() > (GET_PED_TIME_OF_DEATH(ped.pedID)+2000)
				DELETE_RAMPAGE_PED( ped )
			ENDIF
		#ENDIF
		#IF NOT IS_JAPANESE_BUILD
			CLEANUP_RAMPAGE_PED(ped)
		#ENDIF
		EXIT
	ENDIF
	
	// just incase we fall through the world
	pos = GET_ENTITY_COORDS(ped.pedID)
	IF (pos.z < -10.0)
		CPRINTLN(DEBUG_RAMPAGE, "Ped ", ped.iIndex ," feel through the world")
		CLEANUP_RAMPAGE_PED(ped)
		EXIT
	ENDIF
	
	//Run hipster insults
	RAMPAGE2_HIPSTER_INSULTS( ped )
	
	//@RJP check if player is in ragdoll (most likely from being hit by car) and set accuracy low when player in ragdoll
	//also lower accuracy if the tank is alive
	IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE5
	AND IS_ENTITY_ALIVE(rampageTank.vehicleID)
		IF GET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY ) != TANK_ALIVE_AIM
			SET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY, TANK_ALIVE_AIM )
		ENDIF
	ELIF IS_PED_RAGDOLL( PLAYER_PED_ID() )
		IF GET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY ) != PLAYER_IN_RAGDOLL_AIM
			SET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY, PLAYER_IN_RAGDOLL_AIM )
		ENDIF
	ELSE
		IF GET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY ) = PLAYER_IN_RAGDOLL_AIM
			WEAPON_TYPE wtCurrentWeapon
			IF GET_CURRENT_PED_WEAPON( ped.pedID, wtCurrentWeapon )
				IF wtCurrentWeapon = rampageInfo.enemyWeapon[1]
					SET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY, rampageCombatParams.fWeaponAccuracy2 )
				ELSE
					SET_COMBAT_FLOAT( ped.pedID, CCF_WEAPON_ACCURACY, rampageCombatParams.fWeaponAccuracy1 )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(ped.pedID)
		ped.fPlayerDist = GET_DISTANCE_BETWEEN_ENTITIES(GET_VEHICLE_PED_IS_IN(ped.pedID), PLAYER_PED_ID(), FALSE)
	ELSE
		ped.fPlayerDist = GET_DISTANCE_BETWEEN_ENTITIES(ped.pedID, PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF ped.bMolotovThrower
		IF ped.fPlayerDist < 25
		AND IS_ENTITY_ON_SCREEN( ped.pedID )
			SEQUENCE_INDEX seq
			GIVE_WEAPON_TO_PED( ped.pedID, WEAPONTYPE_MOLOTOV, 4, TRUE, TRUE )
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( ped.pedID, TRUE )
			PLAY_PED_AMBIENT_SPEECH_NATIVE(ped.pedID, sVoiceContext, "SPEECH_PARAMS_FORCE")
			CLEAR_PED_TASKS( ped.pedID )
			OPEN_SEQUENCE_TASK( seq )
				TASK_THROW_PROJECTILE( NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				TASK_SWAP_WEAPON( NULL, TRUE )
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
				TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
			CLOSE_SEQUENCE_TASK( seq )
			TASK_PERFORM_SEQUENCE( ped.pedID, seq )
			CLEAR_SEQUENCE_TASK( seq )
			ped.bMolotovThrower = FALSE
		ENDIF
	ENDIF
	
	IF (ped.bWasInVehicle = FALSE)
		IF IS_PED_IN_ANY_VEHICLE(ped.pedID)
			SAFE_REMOVE_BLIP(ped.blipID)
			IF IS_PED_SITTING_IN_VEHICLE_SEAT( ped.pedID, GET_VEHICLE_PED_IS_IN(ped.pedID), VS_DRIVER )
				ped.blipID = CREATE_VEHICLE_BLIP(GET_VEHICLE_PED_IS_IN(ped.pedID), FALSE)
			ENDIF
			ped.bWasInVehicle = TRUE
			EXIT
		ENDIF
	ENDIF
	
	IF (ped.bWasInVehicle = TRUE) 
		IF NOT IS_PED_IN_ANY_VEHICLE(ped.pedID)
			SAFE_REMOVE_BLIP(ped.blipID)
			ped.blipID = CREATE_PED_BLIP(ped.pedID)
			ped.bWasInVehicle = FALSE
			ped.fOriginalDist = ped.fPlayerDist
			CPRINTLN(DEBUG_RAMPAGE, "Ped ", ped.iIndex ," left is vehicle - Resetting Original Distance:", ped.fOriginalDist)
			TASK_COMBAT_PED(ped.pedID, PLAYER_PED_ID())
			EXIT
		ENDIF
	ENDIF
	
	IF (ped.bIsFleePed)
	
		inCar = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF (NOT inCar AND (ped.fPlayerDist > ped.fOriginalDist + 40.0)) OR (inCar AND (ped.fPlayerDist > (PED_CLEANUP_DISTANCE * 2.0)))
			IF NOT IS_ENTITY_ON_SCREEN(ped.pedID)
				CPRINTLN(DEBUG_RAMPAGE, "Flee Ped ", ped.iIndex ," is being deleted for being too far away")
				DELETE_RAMPAGE_PED(ped)
			ELSE
				#IF IS_JAPANESE_BUILD
					DELETE_RAMPAGE_PED(ped)
				#ENDIF
				#IF NOT IS_JAPANESE_BUILD
					CLEANUP_RAMPAGE_PED(ped,TRUE)
				#ENDIF
			ENDIF
		ENDIF
		
	ELSE
		// handle deleting fleeing peds
		IF (ped.bWasInVehicle = FALSE) AND (ped.fPlayerDist > ped.fOriginalDist + PED_CLEANUP_DISTANCE)
			IF NOT IS_ENTITY_ON_SCREEN(ped.pedID) 
				CPRINTLN(DEBUG_RAMPAGE, "Ped ", ped.iIndex ," is being deleted for being too far away")
				DELETE_RAMPAGE_PED(ped)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates rampage ped array
PROC UPDATE_RAMPAGE_PEDS(RAMPAGE_PED& array[])
	INT i

	REPEAT 5 i
		UPDATE_RAMPAGE_PED(array[iCurrentRampagePedIndex])
		
		iCurrentRampagePedIndex ++
		IF (iCurrentRampagePedIndex >= COUNT_OF(array))
			iCurrentRampagePedIndex = 0
		ENDIF
	ENDREPEAT
ENDPROC

//----------------------
//	CAR FUNCTIONS
//----------------------

/// PURPOSE:
///    Sets a ped to optionally hold a defense perimeter around a vehicle
/// PARAMS:
///    pass - ped index
///    veh - car to defend
PROC SET_RAMPAGE_PED_CAR_DEFENDER(PED_INDEX pass, VEHICLE_INDEX veh=NULL)
	FLOAT x, y 
	
	SET_PED_COMBAT_ATTRIBUTES(pass, CA_LEAVE_VEHICLES, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pass, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pass, CA_USE_COVER, TRUE)
	
	x = GET_RANDOM_FLOAT_IN_RANGE(-30.0, 30.0)
	y = GET_RANDOM_FLOAT_IN_RANGE(-30.0, 30.0)
	
	SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(pass, PLAYER_PED_ID(), <<0, 0, 0>>, GET_RANDOM_FLOAT_IN_RANGE(20.0, 30.0), TRUE)

	IF (GET_RANDOM_FLOAT_IN_RANGE() < fCarDefenseAreaChance) AND IS_ENTITY_ALIVE(veh)
		SET_PED_SPHERE_DEFENSIVE_AREA(pass, GET_ENTITY_COORDS(veh), GET_RANDOM_FLOAT_IN_RANGE(10.0, 25.0))		
		SET_PED_COMBAT_ATTRIBUTES(pass, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
	ELSE
		SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(pass, PLAYER_PED_ID(), <<x, y, 0>>, GET_RANDOM_FLOAT_IN_RANGE(30.0, 80.0))
	ENDIF
	
	IF (rampageInfo.blipNameEnum <> STATIC_BLIP_MINIGAME_RAMPAGE2) AND (IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()))
		IF (GET_RANDOM_FLOAT_IN_RANGE() <= fPissTakeRocketChance)
			GIVE_WEAPON_TO_PED(pass, WEAPONTYPE_RPG, 60)
		ENDIF
	ENDIF
	
	SEQUENCE_INDEX seq
	OPEN_SEQUENCE_TASK(seq)
		TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR)
		TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
	CLOSE_SEQUENCE_TASK(seq)
	TASK_PERFORM_SEQUENCE(pass,seq)
	CLEAR_SEQUENCE_TASK(seq)
	
ENDPROC

/// PURPOSE:
///    Gets a random car model from the ones we are allowed to use
/// RETURNS:
///    Car Model
FUNC MODEL_NAMES GET_RANDOM_RAMPAGE_CAR_MODEL()
	IF (rampageInfo.iMaxCarModels = 0)
		SCRIPT_ASSERT("NO RAMPAGE CAR MODELS DEFINED!!")
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	RETURN rampageInfo.carModels[GET_RANDOM_INT_IN_RANGE(0, rampageInfo.iMaxCarModels)]
ENDFUNC

/// PURPOSE:
///    Gets free cover car index
/// PARAMS:
///    ind - new index reference (this is overwritten)
/// RETURNS:
///    true on success
FUNC BOOL GET_FREE_COVER_CAR_INDEX(INT &ind)
	INT i
	
	REPEAT COUNT_OF(coverCar) i
		IF NOT DOES_ENTITY_EXIST(coverCar[i])
			ind = i 
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates a cover vehicle
/// PARAMS:
///    mdl - model name
///    pos - position
///    head - heading
PROC CREATE_COVER_CAR(MODEL_NAMES mdl, VECTOR pos, FLOAT head)
	INT ind 
	
	IF NOT GET_FREE_COVER_CAR_INDEX(ind)
		EXIT 
	ENDIF
	
	coverCar[ind] = CREATE_VEHICLE(mdl, pos, head)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(coverCar[ind], TRUE)
	//SET_VEHICLE_DOORS_LOCKED(coverCar[ind], VEHICLELOCK_LOCKED)
ENDPROC

/// PURPOSE:
///    Sets a supplied vehicle index as a cover vehicle
/// PARAMS:
///    veh - vehicle index
/// RETURNS:
///    true on success
FUNC BOOL SET_AS_COVER_CAR(VEHICLE_INDEX veh)
	INT ind 
	INT i 
	
	IF NOT IS_ENTITY_ALIVE(veh)
		RETURN FALSE
	ENDIF
	
	IF NOT GET_FREE_COVER_CAR_INDEX(ind)
		RETURN FALSE
	ENDIF
	
	i = 0
	REPEAT COUNT_OF(coverCar) i
		IF IS_ENTITY_ALIVE(coverCar[i]) AND (coverCar[i] = veh)
			RETURN TRUE
		ENDIF			
	ENDREPEAT
	
	coverCar[ind] = veh
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(coverCar[ind], TRUE)
	CPRINTLN(DEBUG_RAMPAGE, "VEHICLE SET AS COVER VEHICLE")
	SET_VEHICLE_DOORS_LOCKED(coverCar[ind], VEHICLELOCK_UNLOCKED)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Releases all cover cars
PROC CLEANUP_COVER_CARS()
	INT i
	
	REPEAT COUNT_OF(coverCar) i
		IF DOES_ENTITY_EXIST(coverCar[i])
			IF IS_ENTITY_ALIVE(coverCar[i])
				SET_VEHICLE_DOORS_LOCKED(coverCar[i], VEHICLELOCK_UNLOCKED)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(coverCar[i], FALSE)
			ENDIF
			
			SAFE_RELEASE_VEHICLE(coverCar[i])
			coverCar[i] = NULL
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Updates all cover cars
PROC UPDATE_COVER_CARS()
	INT i = 0
	iAliveCoverCarCounter = 0
	DUMMY_REFERENCE_INT(iAliveCoverCarCounter)
	
	REPEAT COUNT_OF(coverCar) i
		IF DOES_ENTITY_EXIST(coverCar[i])
			IF IS_ENTITY_DEAD(coverCar[i])
				REGISTER_CAR_DEATH(coverCar[i], FALSE)
				SAFE_RELEASE_VEHICLE(coverCar[i])
				CPRINTLN(DEBUG_RAMPAGE, "COVER VEHICLE ", i, " RELEASED")
				coverCar[i] = NULL
			ELSE
				iAliveCoverCarCounter ++
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Returns number of rampage cars who are alive
FUNC INT GET_ALIVE_RAMPAGE_CAR_COUNT()
	INT i = 0
	INT cnt = 0
	
	REPEAT COUNT_OF(rampageCar) i
		IF IS_ENTITY_ALIVE(rampageCar[i].carID)
			#IF IS_DEBUG_BUILD
				rampageCar[i].bDebugAliveCheck = TRUE
			#ENDIF
			cnt ++
		ELSE
			#IF IS_DEBUG_BUILD
				rampageCar[i].bDebugAliveCheck = FALSE
			#ENDIF		
		ENDIF
	ENDREPEAT
	
	RETURN cnt
ENDFUNC

/// PURPOSE:
///    Searches Rampage Car Array for a free index
/// PARAMS:
///    ind - this index is written to
/// RETURNS:
///    true if there is an index free
FUNC BOOL GET_FREE_RAMPAGE_CAR_INDEX(INT &out)
	INT i = 0

	REPEAT COUNT_OF(rampageCar) i
		IF NOT DOES_ENTITY_EXIST(rampageCar[i].carID)
			out = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up rampage car - release reference and kick everyone out vehicle
/// PARAMS:
///    car - reference to rampage car 
PROC CLEANUP_RAMPAGE_CAR(RAMPAGE_CAR &car)
	INT i
	
	IF NOT DOES_ENTITY_EXIST(car.carID)
		EXIT
	ENDIF
	
	// at this point we release any people that happen to be in the car and set them into combat state
	IF (sFormState = SFR_RAMPAGEON)
		
		REPEAT COUNT_OF (car.iOccupantIndex) i 
			IF (car.iOccupantIndex[i] <> -1)
				IF IS_ENTITY_ALIVE(rampagePed[car.iOccupantIndex[i]].pedID)	
					SET_PED_COMBAT_ATTRIBUTES(rampagePed[car.iOccupantIndex[i]].pedID, CA_LEAVE_VEHICLES, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(rampagePed[car.iOccupantIndex[i]].pedID, CA_USE_VEHICLE, FALSE)
					
					IF (rampagePed[car.iOccupantIndex[i]].fPlayerDist > 120.0)
						CLEANUP_RAMPAGE_PED(rampagePed[car.iOccupantIndex[i]])
						car.iOccupantIndex[i] = -1
					ELSE
//						TASK_COMBAT_PED(rampagePed[car.iOccupantIndex[i]].pedID, PLAYER_PED_ID())
						SET_RAMPAGE_PED_CAR_DEFENDER(rampagePed[car.iOccupantIndex[i]].pedID, car.carID)
						CPRINTLN(DEBUG_RAMPAGE, "Ped ", rampagePed[car.iOccupantIndex[i]].iIndex ," was in a car that has been cleanup - enter combat")
						car.iOccupantIndex[i] = -1
					ENDIF	
				ELSE
					IF (i = 0)
						CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," appears to have a dead driver")
						car.iOccupantIndex[i] = -1
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		i = 0
		REPEAT COUNT_OF (car.iOccupantIndex) i 
			car.iOccupantIndex[i] = -1
		ENDREPEAT
		
		// try and keep car as cover or release it
		IF IS_ENTITY_ALIVE(car.carID)
			//Turn off leaking petrol
			SET_VEHICLE_CAN_LEAK_PETROL( car.carID, FALSE )
			IF (car.bKeepAsCover = TRUE) 
				IF SET_AS_COVER_CAR(car.carID)
					CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," has been set as cover car")
					car.carID = NULL
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_RAMPAGE, "Post Rampage Car ", car.iIndex ," has been cleaned up")
		REPEAT COUNT_OF (car.iOccupantIndex) i 
			IF (car.iOccupantIndex[i] <> -1)
				IF IS_ENTITY_ALIVE(rampagePed[car.iOccupantIndex[i]].pedID)	
					TASK_SMART_FLEE_COORD(rampagePed[car.iOccupantIndex[i]].pedID, GET_ENTITY_COORDS(car.carID), 200.0, -1)
					CPRINTLN(DEBUG_RAMPAGE, "Rampage Ped ", car.iOccupantIndex[i], "is Fleeing Car ", car.iIndex)
					CLEANUP_RAMPAGE_PED(rampagePed[car.iOccupantIndex[i]], TRUE)
				ENDIF
			
				car.iOccupantIndex[i] = -1
			ENDIF
		ENDREPEAT
	ENDIF
		
	CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," has been cleaned up")
	SAFE_RELEASE_VEHICLE(car.carID)
	car.carID = NULL
ENDPROC

/// PURPOSE:
///    Deleter up rampage car - release reference and kick everyone out vehicle
/// PARAMS:
///    car - reference to rampage car 
PROC DELETE_RAMPAGE_CAR(RAMPAGE_CAR &car)
	IF NOT DOES_ENTITY_EXIST(car.carID)
		EXIT
	ENDIF
	
	INT i
	REPEAT COUNT_OF (car.iOccupantIndex) i 
		car.iOccupantIndex[i] = -1
	ENDREPEAT
		
	CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," has been cleaned up")
	SAFE_DELETE_VEHICLE(car.carID)
	car.carID = NULL
ENDPROC

/// PURPOSE:
///    Creates a rampage car
/// PARAMS:
///    car - Rampage Car Reference
///    mdl - Model Name
///    pos - Position
///    head - Heading
///    keepcov - Set this as cover car on cleanup
///    nPass - force the # of passengers set this to -1 if you don't care
/// RETURNS:
///    true on success
FUNC BOOL CREATE_RAMPAGE_CAR(RAMPAGE_CAR &car, MODEL_NAMES mdl, VECTOR pos, FLOAT head = 0.0, BOOL keepcov = TRUE, INT nPass = -1)
	INT i
	INT cnt
	INT dridx
	
	IF (mdl = DUMMY_MODEL_FOR_SCRIPT)
		RETURN FALSE
	ENDIF
	
	IF (iAlivePedCounter >= iMaxActivePeds)
		#IF IS_DEBUG_BUILD
			IF (bDebugShowTTY)
				CPRINTLN(DEBUG_RAMPAGE, "CREATE RAMPAGE CAR - NO DRIVERS FREE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_JAPANESE_BUILD
		IF (GET_RAMPAGE_REMAINING_TIME_IN_MILLISECONDS() < 2000)
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// check we at least have a driver available
	IF NOT GET_FREE_RAMPAGE_PED_INDEX(rampagePed, dridx)
		#IF IS_DEBUG_BUILD
			IF (bDebugShowTTY)
				CPRINTLN(DEBUG_RAMPAGE, "CREATE RAMPAGE CAR - NO DRIVERS FREE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// B*1032585 - Don't create vehicle if there is one within 10m of it.
	IF IS_SPHERE_OCCUPIED(pos, 8.0, FALSE, TRUE, FALSE, FALSE, FALSE)
		#IF IS_DEBUG_BUILD
			IF (bDebugShowTTY)
				CPRINTLN(DEBUG_RAMPAGE, "CREATE RAMPAGE CAR - FAILED THERE IS A VEHICLE HERE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// create car
	car.carID = CREATE_VEHICLE(mdl, pos, head)
	IF NOT DOES_ENTITY_EXIST(car.carID)
		#IF IS_DEBUG_BUILD
			IF (bDebugShowTTY)
				CPRINTLN(DEBUG_RAMPAGE, "CREATE RAMPAGE CAR - VEHICLE NOT CREATED")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	SET_ENTITY_HEALTH( car.carID, 600 )
	SET_VEHICLE_PETROL_TANK_HEALTH( car.carID, 600 )
	SET_VEHICLE_ENGINE_HEALTH( car.carID, 600 )
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(car.carID, TRUE)
	
	// B*2012758 - Init occupant indicies
	i = 0
	REPEAT COUNT_OF (car.iOccupantIndex) i 
		car.iOccupantIndex[i] = -1
	ENDREPEAT

	// rampage specific vehicle creation
	SWITCH rampageInfo.blipNameEnum
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			nPass = GET_RANDOM_INT_IN_RANGE(1, 2)
			SET_VEHICLE_DIRT_LEVEL(car.carID, GET_RANDOM_FLOAT_IN_RANGE(10.0, 14.9))
			IF mdl = rampageInfo.carModels[0]
				SET_ENTITY_HEALTH( car.carID, 200 )
				SET_VEHICLE_PETROL_TANK_HEALTH( car.carID, 300 )
				SET_VEHICLE_ENGINE_HEALTH( car.carID, 300 )
			ENDIF
			IF GET_RANDOM_FLOAT_IN_RANGE() < 0.3
				SET_VEHICLE_CAN_LEAK_PETROL( car.carID, TRUE )
				SET_VEHICLE_PETROL_TANK_HEALTH( car.carID, 100 )
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			IF mdl = rampageInfo.carModels[2]
				nPass = 1
				LOWER_CONVERTIBLE_ROOF( car.carID, TRUE )
			ELSE
				nPass = 0
				SET_ENTITY_HEALTH( car.carID, 150 )
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			nPass = GET_RANDOM_INT_IN_RANGE(1, 2)
			IF mdl = rampageInfo.carModels[0]
				SET_ENTITY_HEALTH( car.carID, 300 )
				SET_VEHICLE_PETROL_TANK_HEALTH( car.carID, 300 )
				SET_VEHICLE_ENGINE_HEALTH( car.carID, 300 )
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			nPass = GET_RANDOM_INT_IN_RANGE(1, 2)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			nPass = GET_RANDOM_INT_IN_RANGE(3, 6)
			SET_ENTITY_PROOFS(car.carID, FALSE, TRUE, FALSE, FALSE, TRUE)
		BREAK
	ENDSWITCH
	
	// create driver
	IF NOT CREATE_RAMPAGE_PED_IN_VEHICLE(rampagePed[dridx], car.carID)
		CLEANUP_RAMPAGE_CAR(car)
		RETURN FALSE
	ENDIF
	
	car.bKeepAsCover = keepcov
	car.iOccupantIndex[0] = dridx
	
	car.fDistToPlayer = GET_DISTANCE_BETWEEN_ENTITIES(car.carID, PLAYER_PED_ID(), FALSE)
	car.fOriginalDist = car.fDistToPlayer
	
	CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," has been created")
	
	IF (nPass <= 0)
		RETURN TRUE
	ENDIF
	
	i = 0
	REPEAT nPass i
		cnt = i + 1
		IF (cnt >= COUNT_OF(car.iOccupantIndex))
			RETURN TRUE 
		ENDIF
		
		IF (iAlivePedCounter >= iMaxActivePeds)
			RETURN TRUE 
		ENDIF
		
		IF NOT GET_FREE_RAMPAGE_PED_INDEX(rampagePed, dridx)
			RETURN TRUE 
		ENDIF 
		
		IF NOT CREATE_RAMPAGE_PED_IN_VEHICLE(rampagePed[dridx], car.carID)
		 	RETURN TRUE 
		ENDIF
		
		car.iOccupantIndex[cnt] = dridx
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Sets a rampage car to drive to a set of coords and then kick everyone out
/// PARAMS:
///    rc - rampage car reference
///    pos - desired position
///    bDeployAll - if this is set all occupants will get out, else only passengers will and driver will drive off
PROC SET_RAMPAGE_CAR_DEPLOY_TO_COORDINATES(RAMPAGE_CAR &rc, VECTOR pos, FLOAT spd = 35.0, FLOAT targetDistance = 5.0 )
	INT i
	PED_INDEX pass
	PED_INDEX driver
	INT iTotalSeats 
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF IS_PED_INJURED(driver) OR NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
		
	rc.vDestination	= pos	// if this is zero we assume we're going to player
	rc.fCruiseSpeed = spd
	rc.fTargetReachDist = targetDistance
	
	// give a task to the driver
	IF IS_PED_UNINJURED(driver)
		SET_PED_COMBAT_ATTRIBUTES(driver, CA_DO_DRIVEBYS, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver, bBlockRampageTempEvents)
		TASK_VEHICLE_MISSION_COORS_TARGET(driver, rc.carID, rc.vDestination, MISSION_GOTO, rc.fCruiseSpeed , DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
	ENDIF
	
	// set everyone in the car to be drive-by capable
	iTotalSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(rc.carID) + SEAT_ADD_PLUS
	REPEAT iTotalSeats i
		pass = GET_PED_IN_VEHICLE_SEAT(rc.carID, INT_TO_ENUM(VEHICLE_SEAT, i))
		IF IS_PED_UNINJURED(pass) and (pass <> driver)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_LEAVE_VEHICLES, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_DO_DRIVEBYS, TRUE)
			TASK_COMBAT_PED(pass, PLAYER_PED_ID())
		ENDIF
	ENDREPEAT	
	
	// reset the task enum (andy i'm using numbers for the time being once i lock this down i'll enum it all)
	rc.iTaskState = 0
	
	rc.rcMode = RCM_DEPLOY_ALL
	
ENDPROC

/// PURPOSE:
///    Sets a rampage car to driveby the player
/// PARAMS:
///    rc - 
///    spd - 
PROC SET_RAMPAGE_CAR_DRIVEBY( RAMPAGE_CAR &rc, FLOAT spd = 35.0 )
	INT i
	PED_INDEX pass
	PED_INDEX driver
	INT iTotalSeats 
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF IS_PED_INJURED(driver) OR NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	rc.vDestination	= <<0, 0, 0>>	// if this is zero we assume we're going to player
	rc.fCruiseSpeed = spd
	
	TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_CRUISE, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
	
	// set everyone in the car to be drive-by capable
	iTotalSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(rc.carID) + SEAT_ADD_PLUS
	REPEAT iTotalSeats i
		pass = GET_PED_IN_VEHICLE_SEAT(rc.carID, INT_TO_ENUM(VEHICLE_SEAT, i))
		IF IS_PED_UNINJURED(pass) and (pass <> driver)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_LEAVE_VEHICLES, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_DO_DRIVEBYS, TRUE)
			TASK_COMBAT_PED(pass, PLAYER_PED_ID())
		ELIF IS_PED_UNINJURED(pass)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_DO_DRIVEBYS, TRUE)
		ENDIF
	ENDREPEAT	
	
	// reset the task enum (andy i'm using numbers for the time being once i lock this down i'll enum it all)
	rc.iTaskState = 0
	
	rc.rcMode = RCM_DRIVEBY
	
ENDPROC

/// PURPOSE:
///    @RJP sets a rampage car to try to ram the player
/// PARAMS:
///    rc - 
///    bDeployAll - 
///    spd - 
PROC SET_RAMPAGE_CAR_RAM( RAMPAGE_CAR &rc, FLOAT spd = 35.0 )
	INT i
	PED_INDEX pass
	PED_INDEX driver
	INT iTotalSeats 
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF IS_PED_INJURED(driver) OR NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	rc.vDestination	= <<0, 0, 0>>	// if this is zero we assume we're going to player
	rc.fCruiseSpeed = spd
	
	TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_ATTACK, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
	
	// set everyone in the car to be drive-by capable
	iTotalSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(rc.carID) + SEAT_ADD_PLUS
	REPEAT iTotalSeats i
		pass = GET_PED_IN_VEHICLE_SEAT(rc.carID, INT_TO_ENUM(VEHICLE_SEAT, i))
		IF IS_PED_UNINJURED(pass) and (pass <> driver)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_LEAVE_VEHICLES, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_DO_DRIVEBYS, TRUE)
			TASK_COMBAT_PED(pass, PLAYER_PED_ID())
		ENDIF
	ENDREPEAT	
	
	// reset the task enum (andy i'm using numbers for the time being once i lock this down i'll enum it all)
	rc.iTaskState = 0
	
	rc.rcMode = RCM_RAM
	
	IF (GET_ENTITY_MODEL(rc.carID) = RHINO)
		rc.rcMode = RCM_TANK
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a rampage car to drive towards the player and then kick everyone out
/// PARAMS:
///    rc - rampage car reference
///    bDeployAll - if this is set all occupants will get out, else only passengers will and driver will drive off
PROC SET_RAMPAGE_CAR_DEPLOY(RAMPAGE_CAR &rc, BOOL bDeployAll = TRUE, FLOAT spd = 35.0)
	INT i
	PED_INDEX pass
	PED_INDEX driver
	INT iTotalSeats 
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF IS_PED_INJURED(driver) OR NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
		
	rc.vDestination	= <<0, 0, 0>>	// if this is zero we assume we're going to player
	rc.fCruiseSpeed = spd
	
	//@RJP cars should come much closer to player, everything more aggressive
	rc.fTargetReachDist = GET_RANDOM_FLOAT_IN_RANGE(15.0, 25.0)
	
	//@RJP old values
//	rc.fTargetReachDist = GET_RANDOM_FLOAT_IN_RANGE(30.0, 50.0)
	
	// give a task to the driver
	IF IS_PED_UNINJURED(driver)
		SET_PED_COMBAT_ATTRIBUTES(driver, CA_DO_DRIVEBYS, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(driver, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver, bBlockRampageTempEvents)
		IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE5
			TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS, rc.fTargetReachDist, 15.0, TRUE)
		ELSE
			TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
		ENDIF
	ENDIF
	
	// set everyone in the car to be drive-by capable
	iTotalSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(rc.carID) + SEAT_ADD_PLUS
	REPEAT iTotalSeats i
		pass = GET_PED_IN_VEHICLE_SEAT(rc.carID, INT_TO_ENUM(VEHICLE_SEAT, i))
		IF IS_PED_UNINJURED(pass) and (pass <> driver)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_LEAVE_VEHICLES, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pass, CA_DO_DRIVEBYS, TRUE)
			TASK_COMBAT_PED(pass, PLAYER_PED_ID())
		ENDIF
	ENDREPEAT	
	
	// reset the task enum (andy i'm using numbers for the time being once i lock this down i'll enum it all)
	rc.iTaskState = 0
	
	IF (bDeployAll = TRUE)
		rc.rcMode = RCM_DEPLOY_ALL
	ELSE
		rc.rcMode = RCM_DEPLOY
	ENDIF
	
	IF (GET_ENTITY_MODEL(rc.carID) = RHINO)
		rc.rcMode = RCM_TANK
	ENDIF
ENDPROC

PROC SET_RAMPAGE_CAR_RAISED_COLLISION(RAMPAGE_CAR &rc)
	PED_INDEX driver
	
	IF NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT
	ENDIF
	
	driver = GET_PED_IN_VEHICLE_SEAT(rc.carID)
	IF NOT IS_ENTITY_ALIVE(driver)
		EXIT
	ENDIF
	
	SET_CAN_ADJUST_GROUND_CLEARANCE(rc.carID, TRUE)
ENDPROC

PROC UPDATE_RAMPAGE_CAR_DRIVEBY(RAMPAGE_CAR &rc)
	PED_INDEX driver
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(driver)
		CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has driver is dead - Emergency cleanup - Dist to Player:", rc.fDistToPlayer)
		CLEANUP_RAMPAGE_CAR(rc)
		
	ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_MISSION) > PERFORMING_TASK)
	
		CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has abandoned ram task - redeploy - Dist to Player:", rc.fDistToPlayer)
		IF IS_VECTOR_ZERO(rc.vDestination)
			TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_CRUISE, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
		ELSE
			TASK_VEHICLE_MISSION_COORS_TARGET(driver, rc.carID, rc.vDestination, MISSION_CRUISE, rc.fCruiseSpeed , DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
		ENDIF	
		
	ENDIF
ENDPROC

/// PURPOSE:
///    @RJP makes sure a car set to ram keeps trying to ram until the driver is dead
/// PARAMS:
///    rc - rampage car reference
PROC UPDATE_RAMPAGE_CAR_RAM(RAMPAGE_CAR &rc)
	PED_INDEX driver
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	//If the player has been damaged by the car, change the car to deploy instead of attack so the car doesn't keep running the player over
//	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( PLAYER_PED_ID(), rc.carID, TRUE )
	IF IS_ENTITY_TOUCHING_ENTITY( PLAYER_PED_ID(), rc.carID )
	OR IS_PED_RAGDOLL( PLAYER_PED_ID() )
		IF IS_ENTITY_ALIVE(driver)
			CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has rammed the player, setting car to deploy.")
			CLEAR_PED_TASKS( driver )
			TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)	
			rc.rcMode = RCM_DEPLOY_ALL
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(driver)
		CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has driver is dead - Emergency cleanup - Dist to Player:", rc.fDistToPlayer)
		CLEANUP_RAMPAGE_CAR(rc)
		
	ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_MISSION) > PERFORMING_TASK)
	
		CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has abandoned ram task - redeploy - Dist to Player:", rc.fDistToPlayer)
		IF IS_VECTOR_ZERO(rc.vDestination)
			TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_ATTACK, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
		ELSE
			TASK_VEHICLE_MISSION_COORS_TARGET(driver, rc.carID, rc.vDestination, MISSION_ATTACK, rc.fCruiseSpeed , DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
		ENDIF	
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the state machine for vehicle deployment
/// PARAMS:
///    rc - rampage car reference
///    bGetawayDriver - if this is set the driver doesn't get out
PROC UPDATE_RAMPAGE_CAR_DEPLOY(RAMPAGE_CAR &rc, BOOL bGetawayDriver = TRUE)
	INT i
	PED_INDEX driver
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	
	SWITCH (rc.iTaskState)
		CASE 0		// driving to a destination
			//CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," is driving")
			IF NOT IS_ENTITY_ALIVE(driver)
				CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has driver is dead - Emergency cleanup - Dist to Player:", rc.fDistToPlayer)
				CLEANUP_RAMPAGE_CAR(rc)
				rc.iTaskState = 1
			ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_MISSION) = FINISHED_TASK)
			
				IF IS_VECTOR_ZERO(rc.vDestination) 
					IF (rc.fDistToPlayer > (rc.fTargetReachDist + 20.0))
						CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," claims it has finished deploy to ped task - lies - Dist to Player:", rc.fDistToPlayer)
						TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)	
						EXIT
					ENDIF
				ELSE 
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(rc.carID, rc.vDestination) > (rc.fTargetReachDist + 20.0)
						CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," claims it has finished deploy to point task - lies - Dist to Player:", rc.fDistToPlayer)
						TASK_VEHICLE_MISSION_COORS_TARGET(driver, rc.carID, rc.vDestination, MISSION_GOTO, rc.fCruiseSpeed , DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
						EXIT
					ENDIF	
				ENDIF
				
				rc.iTaskState = 1
				
			ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK)
			
				CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has abandoned task - redeploy - Dist to Player:", rc.fDistToPlayer)
				IF IS_VECTOR_ZERO(rc.vDestination)
					IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE5
						TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS, rc.fTargetReachDist, 15.0, TRUE)
					ELSE
						TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
					ENDIF
				ELSE
					TASK_VEHICLE_MISSION_COORS_TARGET(driver, rc.carID, rc.vDestination, MISSION_GOTO, rc.fCruiseSpeed , DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
				ENDIF	
				
			ENDIF
		BREAK
		CASE 1	// tell everyone to leave and attack
			
			IF (bGetawayDriver = FALSE)
			
				i = 0
				REPEAT COUNT_OF(rc.iOccupantIndex) i 
					IF (rc.iOccupantIndex[i] <> -1)
						CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," Ped ", rc.iOccupantIndex[i], " is getting out")
						IF IS_PED_UNINJURED(rampagePed[rc.iOccupantIndex[i]].pedID)
							SET_RAMPAGE_PED_CAR_DEFENDER(rampagePed[rc.iOccupantIndex[i]].pedID, rc.carID)
						ENDIF
						
					ENDIF
					rc.iOccupantIndex[i] = -1
				ENDREPEAT
			
				rc.iTaskState = 3
				CLEANUP_RAMPAGE_CAR(rc)
			ELSE
			
				i = 0
				REPEAT COUNT_OF(rc.iOccupantIndex) i 
					IF (rc.iOccupantIndex[i] <> -1)
					
						IF (rampagePed[rc.iOccupantIndex[i]].pedID <> driver)
							IF IS_PED_UNINJURED(rampagePed[rc.iOccupantIndex[i]].pedID)
								SET_RAMPAGE_PED_CAR_DEFENDER(rampagePed[rc.iOccupantIndex[i]].pedID, rc.carID)
							ENDIF
							
							rc.iOccupantIndex[i] = -1
						ENDIF
						
					ENDIF
				ENDREPEAT
				
				rc.iTaskState = 2 // this is a null state - we don't care what happens after this
			ENDIF
		BREAK
		CASE 2 // wait for everyone to get out
			IF ((GET_VEHICLE_NUMBER_OF_PASSENGERS(rc.carID) = 0) AND (bGetawayDriver))
				IF IS_PED_UNINJURED(driver)
					TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_FLEE, 80.0, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, 500.0, -1.0)
					rc.iTaskState = 3	// this is a null state - we don't care what happens after this
				ELSE
					EXIT
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handles the state machine for the tank attacking the player
/// PARAMS:
///    rc - rampage car reference
///    bGetawayDriver - if this is set the driver doesn't get out
PROC UPDATE_RAMPAGE_CAR_DEPLOY_TANK(RAMPAGE_CAR &rc)
	PED_INDEX driver
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	SWITCH (rc.iTaskState)
		CASE 0		// driving to a destination
			//CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," is driving")
			IF NOT IS_ENTITY_ALIVE(driver)
				CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has driver is dead - Emergency cleanup - Dist to Player:", rc.fDistToPlayer)
				CLEANUP_RAMPAGE_CAR(rc)
				rc.iTaskState = 1
			ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_MISSION) = FINISHED_TASK)
				CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has finished task - deploy - Dist to Player:", rc.fDistToPlayer)
				
				GIVE_WEAPON_TO_PED(driver, WEAPONTYPE_VEHICLE_WEAPON_TANK, INFINITE_AMMO, TRUE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(driver, CA_USE_VEHICLE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(driver, CA_LEAVE_VEHICLES, FALSE)
				
				rc.iTaskState = 1
			ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK)
				CPRINTLN(DEBUG_RAMPAGE, "Car ", rc.iIndex ," has abandoned task - redeploy - Dist to Player:", rc.fDistToPlayer)
				IF IS_VECTOR_ZERO(rc.vDestination)
					TASK_VEHICLE_MISSION_PED_TARGET(driver, rc.carID, PLAYER_PED_ID(), MISSION_GOTO, rc.fCruiseSpeed, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
				ELSE
					TASK_VEHICLE_MISSION_COORS_TARGET(driver, rc.carID, rc.vDestination, MISSION_GOTO, rc.fCruiseSpeed , DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
				ENDIF	
				
				SCRIPT_ASSERT("DRIVER'S TASK HAS BEEN ABANDONED!!!")
			ENDIF
		BREAK
		CASE 1	// start tank firing
			IF GET_DISTANCE_BETWEEN_ENTITIES(rc.carID, PLAYER_PED_ID()) > 5.0
				TASK_VEHICLE_SHOOT_AT_PED(driver, PLAYER_PED_ID())
				CPRINTLN(DEBUG_RAMPAGE, "TANK IS FIRING")
				rc.iTaskState = 2
			ENDIF
		BREAK
		CASE 2 // wait for everyone to get out
			IF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_SHOOT_AT_ENTITY) = FINISHED_TASK)
				rc.iTimer = GET_GAME_TIMER() + 9000
				CPRINTLN(DEBUG_RAMPAGE, "TANK IS FIRING")
				rc.iTaskState = 3	// this is a null state - we don't care what happens after this
			ELIF (GET_SCRIPT_TASK_STATUS(driver, SCRIPT_TASK_VEHICLE_SHOOT_AT_ENTITY) <> PERFORMING_TASK)
				CPRINTLN(DEBUG_RAMPAGE, "TANK FIRE FAILED")
				rc.iTaskState = 1
			ENDIF			
		BREAK
		CASE 3 // wait for everyone to get out
			IF GET_GAME_TIMER() > rc.iTimer
				rc.iTaskState = 1
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage cars
/// PARAMS:
///    array - reference to car array
PROC FLEE_RAMPAGE_CAR(RAMPAGE_CAR &rc, BOOL faroff = TRUE)
	SEQUENCE_INDEX seqTask
	PED_INDEX driver
	VECTOR v, dest
	
	// don't do this if the car is dead or driver is dead
	IF (rc.iOccupantIndex[0] = -1)
		EXIT 
	ENDIF
	
	driver = rampagePed[rc.iOccupantIndex[0]].pedID
	IF NOT IS_ENTITY_ALIVE(rc.carID)
		EXIT 
	ENDIF
	
	IF IS_ENTITY_ALIVE(driver) AND NOT IS_PED_INJURED(driver)
		IF (faroff)
			v = GET_ENTITY_COORDS(rc.carID) + (NORMALISE_VECTOR(GET_ENTITY_COORDS(rc.carID) - GET_ENTITY_COORDS(PLAYER_PED_ID())) * 500.0)
			IF NOT GET_NTH_CLOSEST_VEHICLE_NODE(v, 0, dest)
				dest = v 
			ENDIF
			
			IF IS_PED_IN_VEHICLE(driver, rc.carID)
				IF GET_SEAT_PED_IS_IN(driver) = VS_DRIVER
					OPEN_SEQUENCE_TASK(seqTask)
						TASK_VEHICLE_MISSION_COORS_TARGET(NULL, rc.carID, dest, MISSION_GOTO, 60.0, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, rc.fTargetReachDist, 15.0, TRUE)
						TASK_VEHICLE_DRIVE_WANDER(NULL, rc.carID, 60.0, DRIVINGMODE_AVOIDCARS)
					CLOSE_SEQUENCE_TASK(seqTask)
					TASK_PERFORM_SEQUENCE(driver, seqTask)
					CLEAR_SEQUENCE_TASK(seqTask)
				ENDIF
			ENDIF
		ELSE
			TASK_SMART_FLEE_PED( driver, PLAYER_PED_ID(), 1000, -1 )
		ENDIF
	ENDIF
	
	CLEANUP_RAMPAGE_PED(rampagePed[rc.iOccupantIndex[0]], TRUE)
	CLEANUP_RAMPAGE_CAR(rc)
ENDPROC
	
/// PURPOSE:
///    Cleans up an entire array of rampage cars
/// PARAMS:
///    array - reference to car array
PROC FLEE_RAMPAGE_CAR_ARRAY(RAMPAGE_CAR& array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		FLEE_RAMPAGE_CAR(array[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage cars
/// PARAMS:
///    array - reference to car array
PROC CLEANUP_RAMPAGE_CAR_ARRAY(RAMPAGE_CAR& array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		CLEANUP_RAMPAGE_CAR(array[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage cars
/// PARAMS:
///    array - reference to car array
PROC DELETE_RAMPAGE_CAR_ARRAY(RAMPAGE_CAR& array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		DELETE_RAMPAGE_CAR(array[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Updates a rampage car and deploy state machine
/// PARAMS:
///    car - rampage car reference
PROC UPDATE_RAMPAGE_CAR(RAMPAGE_CAR &car)
	IF NOT DOES_ENTITY_EXIST(car.carID)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(car.carID)
		REGISTER_CAR_DEATH(car.carID, FALSE)
		CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," is dead - clean it up")
		CLEANUP_RAMPAGE_CAR(car)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_EMPTY(car.carID, TRUE)
		REGISTER_CAR_ABANDON(car.carID)
		CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," has been abandonded - clean it up")
		CLEANUP_RAMPAGE_CAR(car)
		EXIT 
	ENDIF
	
	// if the driver is dead abandon the car
	IF (car.iOccupantIndex[0] <> -1)
		IF NOT IS_ENTITY_ALIVE(rampagePed[car.iOccupantIndex[0]].pedID)
			CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," has no driver - clean it up")
			CLEANUP_RAMPAGE_CAR(car)
			EXIT 
		ENDIF
	ENDIF
	
	// clean up the car if gets too far away from the player
	car.fDistToPlayer = GET_DISTANCE_BETWEEN_ENTITIES(car.carID, PLAYER_PED_ID(), FALSE)
	IF (car.fDistToPlayer >= (car.fOriginalDist + 120.0))
		CPRINTLN(DEBUG_RAMPAGE, "Car ", car.iIndex ," is more than ", car.fOriginalDist + 120.0, " away clean it up")
		CLEANUP_RAMPAGE_CAR(car)
		EXIT
	ENDIF
	
	SWITCH (car.rcMode)
		CASE RCM_RAM
			UPDATE_RAMPAGE_CAR_RAM(car)
		BREAK
		CASE RCM_DRIVEBY
			UPDATE_RAMPAGE_CAR_DRIVEBY(car)
		BREAK
		CASE RCM_DEPLOY
			UPDATE_RAMPAGE_CAR_DEPLOY(car, TRUE)
		BREAK
		CASE RCM_DEPLOY_ALL 
			UPDATE_RAMPAGE_CAR_DEPLOY(car, FALSE)
		BREAK 
		CASE RCM_TANK
			UPDATE_RAMPAGE_CAR_DEPLOY_TANK(car)
		BREAK 			
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Updates rampage car array
PROC UPDATE_RAMPAGE_CARS()
	INT i

	REPEAT COUNT_OF(rampageCar) i
		UPDATE_RAMPAGE_CAR(rampageCar[i])
	ENDREPEAT
	
ENDPROC	

//----------------------
//	TANK FUNCTIONS
//----------------------

FUNC BOOL CREATE_RAMPAGE_TANK(RAMPAGE_TANK &rt, VECTOR pos, FLOAT head = 0.0)
	INT hp 
	rt.vehicleID = CREATE_VEHICLE(RHINO, pos, head)
	IF NOT IS_ENTITY_ALIVE(rt.vehicleID)
		RETURN FALSE
	ENDIF
	
	rt.pedID = CREATE_PED_INSIDE_VEHICLE_AUTO_SEAT(rampageInfo.pedModels[0], rt.vehicleID, WEAPONTYPE_VEHICLE_WEAPON_TANK, TRUE, TRUE)
	rt.blipID = CREATE_VEHICLE_BLIP(rt.vehicleID, FALSE)
	
	SET_PED_CAN_BE_DRAGGED_OUT(rt.pedID, FALSE)
	SET_ENTITY_PROOFS(rt.vehicleID, TRUE, TRUE, FALSE, FALSE, TRUE)
	SET_VEHICLE_DOORS_LOCKED(rt.vehicleID, VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(rt.vehicleID, FALSE)

	hp = GET_ENTITY_MAX_HEALTH(rt.vehicleID) * 2
	SET_ENTITY_MAX_HEALTH(rt.vehicleID, hp)
	SET_ENTITY_HEALTH(rt.vehicleID, hp)
	
	rt.fPlayerDist = GET_DISTANCE_BETWEEN_ENTITIES(rt.vehicleID, PLAYER_PED_ID())
	rt.fOriginalDist = rt.fPlayerDist
	
	SET_PED_SEEING_RANGE(rt.pedID, 100.0)
	SET_PED_COMBAT_ATTRIBUTES(rt.pedID, CA_LEAVE_VEHICLES, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(rt.pedID, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
	//SET_PED_FIRING_PATTERN(rt.pedID, FIRING_PATTERN_DELAY_FIRE_BY_ONE_SEC)
	
//	SET_PED_SHOOT_RATE(rt.pedID, iTankShootRate)
	
	SET_PED_COMBAT_ATTRIBUTES(rt.pedID, CA_PERFECT_ACCURACY, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(rt.pedID, CA_USE_VEHICLE, TRUE)
	
//	TASK_COMBAT_PED(rt.pedID, PLAYER_PED_ID())
	
	rt.iShotTimer = 0
	rt.eTankState = TANK_INIT
	rt.vTargetPos = <<0.0,0.0,0.0>>
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Cleans up an entire array of rampage cars
/// PARAMS:
///    array - reference to car array
PROC DELETE_RAMPAGE_TANK(RAMPAGE_TANK &rt)

	IF NOT DOES_ENTITY_EXIST(rt.vehicleID)
		EXIT
	ENDIF
	
	SAFE_REMOVE_BLIP(rt.blipID)
	SAFE_DELETE_PED(rt.pedID)
	SAFE_DELETE_VEHICLE(rt.vehicleID)
	
	rt.blipID = NULL
	rt.pedID = NULL
	rt.vehicleID = NULL
	
	rt.iShotTimer = 0
	rt.eTankState = TANK_INIT
	rt.vTargetPos = <<0.0,0.0,0.0>>
ENDPROC

/// PURPOSE:
///    Cleans up an entire array of rampage cars
/// PARAMS:
///    array - reference to car array
PROC CLEANUP_RAMPAGE_TANK(RAMPAGE_TANK &rt, BOOL faroff = TRUE)
	SEQUENCE_INDEX seqTask
	VECTOR v, dest
	
	IF NOT DOES_ENTITY_EXIST(rt.vehicleID)
		EXIT
	ENDIF
	
	SAFE_REMOVE_BLIP(rt.blipID)
	
	IF NOT IS_ENTITY_ALIVE(rt.vehicleID)
		SAFE_RELEASE_PED(rt.pedID)
	ELSE
		IF IS_ENTITY_ALIVE(rt.pedID)
			IF (faroff)
				v = GET_ENTITY_COORDS(rt.vehicleID) + (NORMALISE_VECTOR(GET_ENTITY_COORDS(rt.vehicleID) - GET_ENTITY_COORDS(PLAYER_PED_ID())) * 500.0)
				IF NOT GET_NTH_CLOSEST_VEHICLE_NODE(v, 0, dest)
					dest = v 
				ENDIF
				
				OPEN_SEQUENCE_TASK(seqTask)
					TASK_VEHICLE_MISSION_COORS_TARGET(NULL, rt.vehicleID, dest, MISSION_GOTO, 60.0, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1, TRUE)
					TASK_VEHICLE_DRIVE_WANDER(NULL, rt.vehicleID, 60.0, DRIVINGMODE_AVOIDCARS)
				CLOSE_SEQUENCE_TASK(seqTask)
				TASK_PERFORM_SEQUENCE(rt.pedID, seqTask)
				CLEAR_SEQUENCE_TASK(seqTask)			
			ELSE
				TASK_VEHICLE_MISSION_PED_TARGET(rt.pedID, rt.vehicleID, PLAYER_PED_ID(), MISSION_FLEE, 60.0, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1)
			ENDIF
		ENDIF
		
		SAFE_RELEASE_PED(rt.pedID, TRUE) 
	ENDIF
	
	SAFE_RELEASE_VEHICLE(rt.vehicleID)
	rt.blipID = NULL
	rt.pedID = NULL
	rt.vehicleID = NULL
	
	rt.iShotTimer = 0
	rt.eTankState = TANK_INIT
	rt.vTargetPos = <<0.0,0.0,0.0>>
ENDPROC

/// PURPOSE:
///    Generates a vector for the tank to fire that will miss the player
/// PARAMS:
///    rt - 
/// RETURNS:
///    a VECTOR in a safe area for a tank shot close to and around the player
FUNC VECTOR GENERATE_SHOT(RAMPAGE_TANK &rt)
	VECTOR vRetVal, vPlayerPos, vEnemyPos, vDirectionalVector, vShotVectorFromPlayer, vShotOffset
	FLOAT flSafetyAngle, flScalarOfShotFromPlayer, flZCoord
	
	// Get Bulldozer's position
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	// Get Tank's position
	IF IS_ENTITY_OK(rt.vehicleID)
		vEnemyPos = GET_ENTITY_COORDS(rt.vehicleID)
	ENDIF
	
	// Get the direction vector between them
	vDirectionalVector = vPlayerPos - vEnemyPos
	
	// Get the angle between tank and direction vector
	flTankShotAngle = ATAN2(vDirectionalVector.y, vDirectionalVector.x)
	
	// Determine the safety arc that won't shoot through the bulldozer
//	flTankShotAngle = RAD_TO_DEG(flTankShotAngle)

	// The minimum "safe" angle is the angle from tank to bulldozer plus our ArcOfSafety
	flMinimumSafeAngle = flTankShotAngle + flWidgetArcOfSafety
	
	// The maximum "safe" angle is the angle from tank to bulldozer minus our ArcOfSafety PLUS 2 PI
	flMaximumSafeAngle = flTankShotAngle - flWidgetArcOfSafety + 360

	flSafetyAngle = GET_RANDOM_FLOAT_IN_RANGE(flMinimumSafeAngle, flMaximumSafeAngle)
	
	// Determine the distance from the bulldozer to the shot
	flScalarOfShotFromPlayer = GET_RANDOM_FLOAT_IN_RANGE(flMinCannonShotDistance, flMaxCannonShotDistance)
	
	vShotVectorFromPlayer.x = COS(flSafetyAngle)
	vShotVectorFromPlayer.y = SIN(flSafetyAngle)
	vShotVectorFromPlayer.z = 0
	
	vShotOffset = flScalarOfShotFromPlayer * vShotVectorFromPlayer
	vRetVal = vPlayerPos + vShotOffset
	
	IF GET_GROUND_Z_FOR_3D_COORD(vRetVal, flZCoord)
		CPRINTLN(DEBUG_MISSION, "GET_GROUND_Z_FOR_3D_COORD was able to find a valid ground intersection")
		vRetVal.z = flZCoord
	ELSE
		CPRINTLN(DEBUG_MISSION, "GET_GROUND_Z_FOR_3D_COORD was unable to find a valid ground intersection")
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "vRetVal = ", vRetVal)
	
	RETURN vRetVal
	
ENDFUNC

/// PURPOSE:
///    Updates the rampage tank telling it when to fire
/// PARAMS:
///    rt - 
PROC UPDATE_RAMPAGE_TANK(RAMPAGE_TANK &rt)
	IF NOT DOES_ENTITY_EXIST(rt.vehicleID)
		EXIT 
	ENDIF
	
	IF IS_ENTITY_DEAD(rt.vehicleID)
		rampageStats.iTanksDestroyed ++
		CPRINTLN(DEBUG_RAMPAGE, "TANKS DESTROYED:", rampageStats.iTanksDestroyed)
		EXPLODE_VEHICLE(rt.vehicleID)
		CLEANUP_RAMPAGE_TANK(rt)
		EXIT
	ENDIF
	
	rt.fPlayerDist = GET_DISTANCE_BETWEEN_ENTITIES(rt.vehicleID, PLAYER_PED_ID())
	
	IF (rt.fPlayerDist > (rt.fOriginalDist + 100.0))
		CPRINTLN(DEBUG_RAMPAGE, "TANK TOO FAR AWAY CLEAN IT UP")
		CLEANUP_RAMPAGE_TANK(rt)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(rt.pedID)
		CPRINTLN(DEBUG_RAMPAGE, "TANK PILOT SOMEHOW DIED, CLEAN UP TANK")
		CLEANUP_RAMPAGE_TANK(rt)
		EXIT
	ENDIF
	
	VECTOR vMissShot
	SET_VEHICLE_TURRET_SPEED_THIS_FRAME( rt.vehicleID, 0.7 )
	
	SWITCH rt.eTankState
		CASE TANK_INIT
			SET_PED_ACCURACY( rt.pedID, 100 )
			TASK_VEHICLE_MISSION_PED_TARGET( rt.pedId, rt.vehicleID, PLAYER_PED_ID(), MISSION_GOTO, 40, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1 )
			rt.eTankState = TANK_DRIVE_TO_PLAYER
		BREAK
		CASE TANK_DRIVE_TO_PLAYER
			IF rt.fPlayerDist < 35.0
				CLEAR_PED_TASKS( rt.pedID )
				TASK_VEHICLE_TEMP_ACTION( rt.pedId, rt.vehicleID, TEMPACT_BRAKE, 100 )
				CPRINTLN(DEBUG_RAMPAGE, "TANK_DRIVE_TO_PLAYER: SETTING eTankState TO TANK_FIRE_MISS_SHOT")
				rt.eTankState = TANK_FIRE_MISS_SHOT
			ELIF (GET_SCRIPT_TASK_STATUS(rt.pedID, SCRIPT_TASK_VEHICLE_MISSION) > PERFORMING_TASK)
				TASK_VEHICLE_MISSION_PED_TARGET( rt.pedId, rt.vehicleID, PLAYER_PED_ID(), MISSION_GOTO, 50, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, 30, -1 )
			ENDIF
		BREAK
		CASE TANK_FIRE_MISS_SHOT
			IF NOT bTankDiag
				IF CREATE_CONVERSATION( convoBattleRant, rampageInfo.sDialogBlockName, "RAMP5_TANK", CONV_PRIORITY_MEDIUM )
					bTankDiag = TRUE
				ENDIF
			ENDIF
			CLEAR_PED_TASKS( rt.pedID )
			rt.vTargetPos = GET_ENTITY_COORDS( PLAYER_PED_ID() )
			vMissShot = GENERATE_SHOT(rt)
			CPRINTLN(DEBUG_RAMPAGE, "UPDATE_RAMPAGE_TANK: TANK_FIRE_MISS_SHOT: TANK FIRING, SETTING eTankState TO TANK_CHECK_FOR_SHOT")
			TASK_VEHICLE_SHOOT_AT_COORD( rt.pedID, vMissShot )
			rt.iShotTimer = GET_GAME_TIMER() + 6000
			rt.eTankState = TANK_CHECK_FOR_SHOT
		BREAK
		CASE TANK_CHECK_FOR_SHOT
			IF IS_EXPLOSION_IN_SPHERE( EXP_TAG_TANKSHELL, GET_ENTITY_COORDS( PLAYER_PED_ID() ), 60 )
			OR ( GET_GAME_TIMER() >= rt.iShotTimer )
				CPRINTLN(DEBUG_RAMPAGE, "TANK_CHECK_FOR_SHOT: TANK FIRED, SETTING eTankState TO TANK_WAIT_BETWEEN_SHOTS")
				rt.iShotTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE( 3000, 4000 )
				rt.eTankState = TANK_WAIT_BETWEEN_SHOTS
			ENDIF
		BREAK
		CASE TANK_WAIT_BETWEEN_SHOTS
			IF ( GET_GAME_TIMER() >= rt.iShotTimer )
				CLEAR_PED_TASKS( rt.pedID )
				CPRINTLN(DEBUG_RAMPAGE, "TANK_WAIT_BETWEEN_SHOTS: TIMER COMPLETE, SETTING eTankState TO TANK_FIRE_HIT_SHOT")
				rt.eTankState = TANK_FIRE_HIT_SHOT
			ELIF rt.fPlayerDist < 10.0
				IF (GET_SCRIPT_TASK_STATUS(rt.pedID, SCRIPT_TASK_VEHICLE_MISSION) > PERFORMING_TASK)
					TASK_VEHICLE_MISSION_PED_TARGET( rt.pedId, rt.vehicleID, PLAYER_PED_ID(), MISSION_FLEE, 30, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, -1, -1 )
				ENDIF
			ELIF rt.fPlayerDist < 35.0
				IF (GET_SCRIPT_TASK_STATUS(rt.pedID, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK)
					TASK_VEHICLE_TEMP_ACTION( rt.pedId, rt.vehicleID, TEMPACT_BRAKE, 100 )
					CLEAR_PED_TASKS( rt.pedID )
				ELIF (GET_SCRIPT_TASK_STATUS(rt.pedID, SCRIPT_TASK_VEHICLE_AIM_AT_COORD) > PERFORMING_TASK)
					CLEAR_PED_TASKS( rt.pedID )
					TASK_VEHICLE_AIM_AT_COORD( rt.pedID, rt.vTargetPos )
				ENDIF
			ELIF (GET_SCRIPT_TASK_STATUS(rt.pedID, SCRIPT_TASK_VEHICLE_MISSION) > PERFORMING_TASK)
				TASK_VEHICLE_MISSION_PED_TARGET( rt.pedId, rt.vehicleID, PLAYER_PED_ID(), MISSION_GOTO, 50, DF_SwerveAroundAllCars|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions, 30, -1 )
			ENDIF
		BREAK
//		CASE TANK_PREP_FOR_NEXT_SHOT
//			CLEAR_PED_TASKS( rt.pedId )
//			IF IS_ENTITY_AT_COORD( PLAYER_PED_ID(), rt.vTargetPos, <<6,6,6>>, FALSE, FALSE )
//				CPRINTLN(DEBUG_RAMPAGE, "TANK_PREP_FOR_NEXT_SHOT: PLAYER HAS NOT MOVED, eTankState TO TANK_FIRE_HIT_SHOT  HIT! HIT! HIT!")
//				rt.eTankState = TANK_FIRE_HIT_SHOT
//			ELSE
//				CPRINTLN(DEBUG_RAMPAGE, "TANK_PREP_FOR_NEXT_SHOT: PLAYER HAS MOVED, eTankState TO TANK_FIRE_MISS_SHOT")
//				rt.eTankState = TANK_FIRE_MISS_SHOT
//			ENDIF
//		BREAK
		CASE TANK_FIRE_HIT_SHOT
			CLEAR_PED_TASKS( rt.pedID )
			CPRINTLN(DEBUG_RAMPAGE, "TANK_FIRE_HIT_SHOT: TANK FIRING, SETTING eTankState TO TANK_CHECK_FOR_SHOT")
			TASK_VEHICLE_SHOOT_AT_COORD( rt.pedID, rt.vTargetPos )
			rt.vTargetPos = GET_ENTITY_COORDS( PLAYER_PED_ID() )
//			CPRINTLN(DEBUG_RAMPAGE, "TANK_FIRE_HIT_SHOT: TANK FIRING, SETTING eTankState TO TANK_CHECK_FOR_SHOT")
//			TASK_VEHICLE_SHOOT_AT_PED( rt.pedID, PLAYER_PED_ID() )
			rt.iShotTimer = GET_GAME_TIMER() + 6000
			rt.eTankState = TANK_CHECK_FOR_SHOT
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Removes All Rampage Blips
PROC FORCE_REMOVE_ALL_BLIPS()
	INT i 
	
	SAFE_REMOVE_BLIP(battleBlip)
	battleBlip = NULL 

	i = 0
	REPEAT COUNT_OF(rampagePed) i 
		SAFE_REMOVE_BLIP(rampagePed[i].blipID)
		rampagePed[i].blipID = NULL
	ENDREPEAT
ENDPROC

//----------------------
//	SPAWNER FUNCTIONS
//----------------------

/// PURPOSE:
///    Resets a rampage fortress struct (sets everything to zero)
/// PARAMS:
///    fort - rampage fortress reference
PROC ZERO_RAMPAGE_FORTRESS(RAMPAGE_FORTRESS &fort)
	fort.bIsActive = FALSE
	fort.spawnArea.fWidth = 0
	fort.defenseArea.fWidth = 0
	fort.fRoofHeight = 0.0			

	fort.iPedCount = 0			
	fort.iSpawnedCount = 0			
	fort.iMaxSpawnCount = 0		
	
	fort.fRoofFactor = 0.0			
	fort.fRushFactor = 0.0			
	
	fort.iMaxActivePeds = -1			
	fort.iNextFortress = -1
ENDPROC

///------------------------------------------------------------------------------
///	 FUNCTION: 
///    		SETUP_RAMPAGE_FORTRESS()
///  PURPOSE: 
///    		SETS UP AREA FOR SPAWN AREA OF RAMPAGE FORTRESS
///------------------------------------------------------------------------------

/// PURPOSE:
///    Setup an area to use as a rampage fortress
/// PARAMS:
///    fort - area reference
///    p1 - area point 1
///    p2 - area point 2
///    width - area width 
///    active - set this to force the area active on start
///    nextfort - this is the index for the next fortress to turn on once this fortress has stopped spawning
PROC SETUP_RAMPAGE_FORTRESS(RAMPAGE_FORTRESS &fort, VECTOR p1, VECTOR p2, FLOAT width, BOOL active = FALSE, INT nextfort = -1)

	fort.bIsActive = active
	SETUP_RAMPAGE_AREA(fort.spawnArea, p1, p2, width)
	//SETUP_RAMPAGE_AREA(fort.defenseArea, p1, p2, width)
	fort.defenseArea = EXPAND_RAMPAGE_AREA(fort.spawnArea, 2.5)
	fort.iNextFortress = nextfort
ENDPROC

/// PURPOSE:
///    Setup rampage fortress roof parameters
/// PARAMS:
///    fort - fortress reference
///    roofz - any z coord higher than this is counted as roof by the fortress
///    roofactor - chance of peds spawning on roof (0 - none, 1 - all)
PROC SETUP_RAMPAGE_FORTRESS_ROOF(RAMPAGE_FORTRESS &fort, FLOAT roofz, FLOAT roofactor = 0.25)
	fort.fRoofFactor = roofactor
	fort.fRoofHeight = roofz
ENDPROC

/// PURPOSE:
///    Setup rampage fortress spawning overrides
/// PARAMS:
///    fort - fortress reference
///    ratio - how many of the total # of peds to come from the fortress (0.0 for infinite)
///    rushFactor - chance of a spawned ped rushing you or staying in the fortress
///    maxAPeds - maximum of active peds while fortress is operational (set to -1 to not override)
///    maxACars - maximum of active cars while fortress is operational (set to -1 to not override)
PROC SETUP_RAMPAGE_FORTRESS_SPAWNING(RAMPAGE_FORTRESS &fort, FLOAT ratio, FLOAT rushFactor = 0.0, INT maxAPeds = -1)
	IF (ratio > 0.0)
		fort.iMaxSpawnCount = CEIL(TO_FLOAT(rampageInfo.iTargetAmount * 2) * ratio)
	ELSE
		fort.iMaxSpawnCount = 99	// this will ensure the fortress never shuts down
	ENDIF
	
	IF (maxAPeds > iMaxActivePeds)
		maxAPeds = iMaxActivePeds
	ENDIF
	
	IF (maxAPeds > MAX_ACTIVE_PEDS)
		maxAPeds = MAX_ACTIVE_PEDS
	ENDIF
	
	fort.iMaxActivePeds = maxAPeds		
	fort.fRushFactor = rushFactor
ENDPROC

/// PURPOSE:
///    Generates a spawn position within a fortress
/// PARAMS:
///    fort - fortress reference
///    out - the resultant point
/// RETURNS:
///    true if we could generate one
FUNC BOOL GENERATE_FORTRESS_SPAWN_POINT(RAMPAGE_FORTRESS &fort, VECTOR &out)
	VECTOR v = GET_RANDOM_POINT_IN_RAMPAGE_AREA(fort.spawnArea)
	BOOL pvemnt = FALSE // (rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2)
	FLOAT z
	INT i 
	
	// check if point is safe
	#IF IS_DEBUG_BUILD
		VECTOR ppos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		CPRINTLN(DEBUG_RAMPAGE, "   POINT GRABBED FROM RAMPAGE SPAWN AREA:", v ," PLAYER POS:", ppos)
	#ENDIF
	
	IF NOT GET_SAFE_COORD_FOR_PED(v, pvemnt, out, GSC_FLAG_NOT_INTERIOR | GSC_FLAG_NOT_WATER)
		CPRINTLN(DEBUG_RAMPAGE, "   FAILED - CAN'T GET SAFE SPAWN POINT - POS:", v)
		RETURN FALSE
	ENDIF
	
	// do roof factor check - we check possiblity of people in roof
	// roof factor of 1 means if it can find a roof point it will put someone on it
	IF (fort.fRoofHeight > 0.0)
		IF (out.z > fort.fRoofHeight) AND (GET_RANDOM_FLOAT_IN_RANGE() > fort.fRoofFactor)
			CPRINTLN(DEBUG_RAMPAGE, "   FAILED - SPAWN POINT IS TOO HIGH - INPOS:", v, " OUTPOS:", out)
			RETURN FALSE
		ENDIF
	ENDIF
	
	out.z += 0.25
	IF GET_GROUND_Z_FOR_3D_COORD(out, z)
		out.z = z
	ENDIF
	
	// this stops peds from popping in from behind at very close range
	IF VDIST2(out, GET_ENTITY_COORDS(PLAYER_PED_ID())) < (rampageFortressSpawner.fMinSpawnRadius * rampageFortressSpawner.fMinSpawnRadius)
	
	
		IF (bDoPedSpawnBehindCheck)
			v = NORMALISE_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_GAMEPLAY_CAM_COORD())
			IF GET_PLANE_SIDE(v, GET_GAMEPLAY_CAM_COORD(), out) <= ON_PLANE
				CPRINTLN(DEBUG_RAMPAGE, "   FAILED - SPAWN POINT IS WITHIN MIN SPAWN DISTANCE:", rampageFortressSpawner.fMinSpawnRadius, " - INPOS:", v, " OUTPOS:", out)
				RETURN FALSE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_RAMPAGE, "   FAILED - SPAWN POINT IS WITHIN MIN SPAWN DISTANCE:", rampageFortressSpawner.fMinSpawnRadius, " - INPOS:", v, " OUTPOS:", out)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// check the point is actually in the fortress
	IF NOT IS_POINT_IN_ANGLED_AREA(out, fort.defenseArea.vPosition[0], fort.defenseArea.vPosition[1], fort.defenseArea.fWidth)
		CPRINTLN(DEBUG_RAMPAGE, "   SPAWN POINT ISN'T IN FORTRESS - OUTPOS:", out)
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_PED_NEAR_POINT(out, NO_PED_CHECK_RADIUS)
		CPRINTLN(DEBUG_RAMPAGE, "   FAILED - SPAWN POINT IS WITHIN ", NO_PED_CHECK_RADIUS, " OF SOMEONE - OUTPOS:", out)
		RETURN FALSE
	ENDIF
	
	// check point isnt in any used no spawn zone
	REPEAT iMaxActiveNoSpawnAreas i 
		IF IS_POINT_IN_ANGLED_AREA(out, noSpawnArea[i].vPosition[0], noSpawnArea[i].vPosition[1], noSpawnArea[i].fWidth)
			CPRINTLN(DEBUG_RAMPAGE, "   FAILED - SPAWN POINT IS NO SPAWN AREA ", i, " OUTPOS:", out)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	// check that we aren't spawning on top of a ped
	//IF IS_ANY_PED_IN_SPHERE(out, NO_PED_CHECK_RADIUS)

	
	// if the point is invisible it's okay but if we are at no spawn dist abandon
	IF IS_SPHERE_VISIBLE(out, 0.25)
		IF WOULD_ENTITY_BE_OCCLUDED(rampageInfo.pedModels[0], out)
			RETURN TRUE
		ENDIF
		
		CPRINTLN(DEBUG_RAMPAGE, "  FAILED - AT LEAST 0.25 METERS AROUND SPAWN POINT IS VISIBLE - OUTPOS:", out)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Updates the blocking of rampage spawning
/// RETURNS:
///    Return false if we aren't allowed to spawn
FUNC BOOL UPDATE_RAMPAGE_SPAWN_BLOCKER()
	IF (bBlockRampageSpawning = FALSE)
		RETURN TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
		IF (bPauseRampageSpawning)
			RETURN FALSE
		ENDIF
	#ENDIF
	
	iRampageSpawnBlockLimit = iMaxActivePeds / 2
	SWITCH (rampageSpawnState)
		CASE ERS_SPAWNING_ON
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Spawning is on, block camera movement - fix for bug 2201576
			
			IF (iAlivePedCounter >= iMaxActivePeds)
				rampageSpawnState = ERS_SPAWNING_LIMIT_BREAK
				RETURN FALSE
			ENDIF
		BREAK
		CASE ERS_SPAWNING_LIMIT_BREAK
			IF (iAlivePedCounter < iRampageSpawnBlockLimit)
				rampageSpawnState = ERS_SPAWNING_WAIT
				iRampageSpawnStateTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iMinRampageSpawnBlockTime, iMaxRampageSpawnBlockTime)
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE ERS_SPAWNING_WAIT
			IF (GET_GAME_TIMER() > iRampageSpawnStateTimer) OR (iAlivePedCounter < 4)
				rampageSpawnState = ERS_SPAWNING_ON
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check if a spawner is ready to spawn
/// PARAMS:
///    sptime - after this time spawner is ready
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL IS_SPAWNER_READY_TO_SPAWN(INT &sptime)
	IF (bUseGlobalPedSpawnTimer)
		RETURN (GET_GAME_TIMER() > iNextRampageSpawnTime)
	ENDIF
	
	RETURN (GET_GAME_TIMER() > sptime)
ENDFUNC

/// PURPOSE:
///    Updates the fortress spawning system
PROC UPDATE_RAMPAGE_ALLEY_SPAWNER(RAMPAGE_ALLEYSPAWNER &rs)
	INT i
	INT pedInd, spawnInd 
	VECTOR defPoint 
	FLOAT spawnU
	
	rs.iFrameCounter ++ 
	
	IF (rs.bIsActive = FALSE)
		EXIT
	ENDIF
	
	IF (iAlivePedCounter >= iMaxActivePeds)
		EXIT 
	ENDIF
	
	IF (rs.iFrameCounter % 4 <> 0)
		EXIT 
	ENDIF
	
	IF (bInBattleArea = FALSE)
		EXIT
	ENDIF
	
	IF NOT IS_SPAWNER_READY_TO_SPAWN(rs.iNextSpawnTime)
		EXIT 
	ENDIF
	
	// look for free ped
	IF NOT GET_FREE_RAMPAGE_PED_INDEX(rampagePed, pedInd)
		EXIT 
	ENDIF
	
	// update player u position - works out how far along the alley the player is 
	rs.fPlayerAlleyUValue = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), rs.vAlleyPoints[0], rs.vAlleyPoints[1])
	//GET_CLOSEST_POINT_ON_LINE_WITH_U_DEPRECIATED(GET_ENTITY_COORDS(PLAYER_PED_ID()), rs.vAlleyPoints[0], rs.vAlleyPoints[1], rs.fPlayerAlleyUValue)

	// get a spawn point check to see that further up the alley than the player is
	spawnInd = GET_RANDOM_INT_IN_RANGE(0, rs.iMaxActiveSpawnPoints)
	spawnU = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(rs.spawnPoint[spawnInd].vPosition, rs.vAlleyPoints[0], rs.vAlleyPoints[1])
	defPoint = rs.vAlleyPoints[0] + (spawnU * (rs.vAlleyPoints[1] - rs.vAlleyPoints[0]))

	//defPoint = GET_CLOSEST_POINT_ON_LINE_WITH_U_DEPRECIATED(rs.spawnPoint[spawnInd].vPosition, rs.vAlleyPoints[0], rs.vAlleyPoints[1], spawnU)
	IF (spawnU < (rs.fPlayerAlleyUValue + rs.fAlleyDiffAdjust))
		EXIT 
	ENDIF
	
	//@RJP Check spawn point is not too close to player
	IF VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), rs.spawnPoint[spawnInd].vPosition ) < 100
		EXIT
	ENDIF
	
	// check there isn't a ped right next to me
	IF (bUseIsAreaOccupied)
		IF IS_ANY_PED_IN_SPHERE(rs.spawnPoint[spawnInd].vPosition, 0.25)
			EXIT
		ENDIF
	ELSE
		IF IS_ANY_PED_NEAR_POINT(rs.spawnPoint[spawnInd].vPosition, 0.25)
			EXIT
		ENDIF
	ENDIF
	
	// check point isnt in any used no spawn zone
	REPEAT iMaxActiveNoSpawnAreas i 
		IF IS_POINT_IN_ANGLED_AREA(rs.spawnPoint[spawnInd].vPosition, noSpawnArea[i].vPosition[0], noSpawnArea[i].vPosition[1], noSpawnArea[i].fWidth)
			EXIT
		ENDIF
	ENDREPEAT
	
	IF NOT CREATE_RAMPAGER_PED_AT_SPAWN_POINT(rampagePed[pedInd], rs.spawnPoint[spawnInd], TRUE, TRUE)
		EXIT 
	ENDIF
	
	IF (bUseGlobalPedSpawnTimer)
		rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iMinRampagePedSpawnTime, iMaxRampagePedSpawnTime)	
		iNextRampageSpawnTime = rs.iNextSpawnTime
	ELSE
		rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(rs.iMinSpawnTime, rs.iMaxSpawnTime)	
	ENDIF
	
	//@RJP only doing this in rampage 4 for now
	IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE4
		defPoint = GET_ENTITY_COORDS(rampagePed[pedInd].pedID)
		IF (defPoint.z > 35.0)
			SET_PED_COMBAT_MOVEMENT(rampagePed[pedInd].pedID, CM_STATIONARY)
		ENDIF
	ENDIF
	
	TASK_COMBAT_PED(rampagePed[pedInd].pedID, PLAYER_PED_ID())
ENDPROC

/// PURPOSE:
///    Updates the car spawner
/// PARAMS:
///    rs - 
PROC UPDATE_RAMPAGE_CAR_SPAWNER(RAMPAGE_CARSPAWNER &rs)
	INT i 
	FLOAT head
	INT iCarInd
	VECTOR carpos, tmp
	MODEL_NAMES mdl 
	
	rs.iFrameCounter ++ 
	
	IF (rs.bIsActive = FALSE)
		EXIT
	ENDIF
	
	IF (iAliveCarCounter >= rs.iMinActiveCars)
		IF (GET_GAME_TIMER() < rs.iNextSpawnTime)
			EXIT
		ENDIF
	ENDIF
	
	IF (iAliveCarCounter >= rs.iMaxActiveCars)
		rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(rs.iMinSpawnTime, rs.iMaxSpawnTime)	
		EXIT 
	ENDIF
	
	IF (iAliveCarCounter >= COUNT_OF(rampageCar))
		EXIT 
	ENDIF
	
	IF (iAlivePedCounter >= COUNT_OF(rampagePed))
		EXIT 
	ENDIF
	
	IF (rs.iFrameCounter % 4 <> 0)
		EXIT 
	ENDIF
	
	// pick a car model
	mdl = GET_RANDOM_RAMPAGE_CAR_MODEL()
	IF (mdl = DUMMY_MODEL_FOR_SCRIPT)
		EXIT
	ENDIF

	IF NOT GET_FREE_RAMPAGE_CAR_INDEX(iCarInd)
		EXIT
	ENDIF
		
	// don't use spawn points
	IF (rs.bUseSpawnPoints = FALSE)	OR (bInBattleArea = FALSE)	
		VECTOR ResultLinkDir
		tmp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_RANDOM_FLOAT_IN_RANGE(0, 360), <<0, GET_RANDOM_FLOAT_IN_RANGE(rs.fMinSpawnRadius, rs.fMaxSpawnRadius), 0>>)
		IF NOT GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(tmp, carpos, ResultLinkDir)	
			EXIT 
		ENDIF
			
		head = GET_HEADING_FROM_COORDS(carpos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ELSE
		i = GET_RANDOM_INT_IN_RANGE(0, rs.iSpawnPointsUsed)
		carPos = rs.vSpawnPoints[i].vPosition
		head = rs.vSpawnPoints[i].fHeading
	ENDIF
		
	IF NOT CREATE_RAMPAGE_CAR(rampageCar[iCarInd], mdl, carpos, head)
		EXIT
	ENDIF
	
	//@RJP Check random chance that car should try to run over player
	IF GET_RANDOM_FLOAT_IN_RANGE() < rampageInfo.fCarRamChance
		SET_RAMPAGE_CAR_RAM(rampageCar[iCarInd], GET_RANDOM_FLOAT_IN_RANGE(35.0, 42.0))
	ELIF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2
		IF mdl = rampageInfo.carModels[0]
			IF GET_RANDOM_FLOAT_IN_RANGE() < 0.5
				SET_RAMPAGE_CAR_DRIVEBY( rampageCar[iCarInd], 20 )
			ELSE
				SET_RAMPAGE_CAR_DEPLOY(rampageCar[iCarInd], TRUE, 25)
			ENDIF
		ELSE
			SET_RAMPAGE_CAR_DEPLOY(rampageCar[iCarInd], TRUE, GET_RANDOM_FLOAT_IN_RANGE(35.0, 42.0))
		ENDIF
	ELSE
		SET_RAMPAGE_CAR_DEPLOY(rampageCar[iCarInd], TRUE, GET_RANDOM_FLOAT_IN_RANGE(35.0, 42.0))
	ENDIF
	
	rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(rs.iMinSpawnTime, rs.iMaxSpawnTime)		
ENDPROC

/// PURPOSE:
///    Updates the cloud spawner
/// PARAMS:
///    rs - 
PROC UPDATE_RAMPAGE_CLOUD_SPAWNER(RAMPAGE_CLOUDSPAWNER &rs)
	//FLOAT zrot
	INT ind, i
	VECTOR pos, out
	
	rs.iFrameCounter ++ 
	
	IF (rs.bIsActive = FALSE)
		EXIT
	ENDIF
	
	IF NOT IS_SPAWNER_READY_TO_SPAWN(rs.iNextSpawnTime)
		EXIT 
	ENDIF
	
	IF (iAlivePedCounter >= iMaxActivePeds)
		EXIT 
	ENDIF
	
	IF (iAlivePedCounter >= COUNT_OF(rampagePed))
		EXIT 
	ENDIF
	
	IF (rs.iFrameCounter % 4 <> 0)
		EXIT 
	ENDIF
	
	#IF IS_JAPANESE_BUILD
		IF (GET_RAMPAGE_REMAINING_TIME_IN_MILLISECONDS() < 2000)
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
		EXIT
	ENDIF
	
	pos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 
			GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0), <<0, GET_RANDOM_FLOAT_IN_RANGE(rs.fMinSpawnRadius, rs.fMaxSpawnRadius), 0>>)
	
	IF NOT GET_SAFE_COORD_FOR_PED(pos, rs.bPavementOnly, out, GSC_FLAG_NOT_ISOLATED | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_NOT_WATER)
		EXIT 
	ENDIF
	
	/*
	IF GET_GROUND_Z_FOR_3D_COORD(out, zrot)
		out.z = zrot
	ENDIF
	*/
	
	IF VDIST2(out, GET_ENTITY_COORDS(PLAYER_PED_ID())) < (rs.fMinSpawnRadius * rs.fMinSpawnRadius)
		EXIT 
	ENDIF
	
	// check point isnt in any used no spawn zone
	REPEAT iMaxActiveNoSpawnAreas i 
		IF IS_POINT_IN_ANGLED_AREA(out, noSpawnArea[i].vPosition[0], noSpawnArea[i].vPosition[1], noSpawnArea[i].fWidth)
			EXIT
		ENDIF
	ENDREPEAT
	
	// check that we aren't spawning on top of a ped
	IF (bUseIsAreaOccupied)
		IF IS_ANY_PED_IN_SPHERE(out, NO_PED_CHECK_RADIUS)
			EXIT
		ENDIF
	ELSE
		IF IS_ANY_PED_NEAR_POINT(out, NO_PED_CHECK_RADIUS)
			EXIT
		ENDIF
	ENDIF	
	
	// if the point is invisible it's okay but if we are at no spawn dist abandon
	IF IS_SPHERE_VISIBLE(out, 0.25)
		IF NOT WOULD_ENTITY_BE_OCCLUDED(rampageInfo.pedModels[0], out)
			EXIT
		ENDIF
	ENDIF	
	
	// create
	IF (GET_RANDOM_FLOAT_IN_RANGE() <= rs.fFleeChance)
		IF NOT CREATE_RAMPAGER_FLEE_PED_AT_POSITION(rampagePed[ind], out)
			EXIT 
		ENDIF
	ELIF NOT CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], out)
		EXIT 
	ENDIF
	
	IF (bUseGlobalPedSpawnTimer)
		rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iMinRampagePedSpawnTime, iMaxRampagePedSpawnTime)	
		iNextRampageSpawnTime = rs.iNextSpawnTime
	ELSE
		rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(rs.iMinSpawnTime, rs.iMaxSpawnTime)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the fortress spawning system
PROC UPDATE_RAMPAGE_FORTRESS_SPAWNER(RAMPAGE_FORTSPAWNER &rs)
//	INT i
	VECTOR tmp 
	
	rs.iFrameCounter ++ 
	
	IF (rs.bIsActive = FALSE)
		EXIT
	ENDIF
	

	IF NOT IS_SPAWNER_READY_TO_SPAWN(rs.iNextSpawnTime)
		EXIT 
	ENDIF
	
	IF (bInBattleArea = FALSE)
		EXIT
	ENDIF
	
	IF (rampageFort[rs.iFortressFrameCounter].bIsActive) AND (iAlivePedCounter < iMaxActivePeds)
		CPRINTLN(DEBUG_RAMPAGE, " ")
		CPRINTLN(DEBUG_RAMPAGE, "FORTRESS ", rs.iFortressFrameCounter, " IS TRYING TO SPAWN")
		IF GENERATE_FORTRESS_SPAWN_POINT(rampageFort[rs.iFortressFrameCounter], tmp)
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), tmp, rs.fFortressAllowRange)
				IF CREATE_RAMPAGER_PED_IN_FORTRESS_AT_POSITION(rampageFort[rs.iFortressFrameCounter], tmp, rs.iFortressFrameCounter)
				
					IF (bUseGlobalPedSpawnTimer)
						rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iMinRampagePedSpawnTime, iMaxRampagePedSpawnTime)	
						iNextRampageSpawnTime = rs.iNextSpawnTime
					ELSE
						rs.iNextSpawnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(rs.iMinSpawnTime, rs.iMaxSpawnTime)	
					ENDIF

				ENDIF
			ELSE
				CPRINTLN(DEBUG_RAMPAGE, "FORTRESS ", rs.iFortressFrameCounter, " PLAYER IS TOO FAR AWAY")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_RAMPAGE, "FORTRESS ", rs.iFortressFrameCounter, " COULDN'T GENERATE POINT")
		ENDIF
	ENDIF
	
	rs.iFortressFrameCounter ++
	IF rs.iFortressFrameCounter >= rs.iMaxActiveFortress
		rs.iFortressFrameCounter = 0
	ENDIF
ENDPROC

//----------------------
//	CHALLENGE FUNCTIONS
//----------------------

/// PURPOSE:
///    Resets the Challenge Array
/// PARAMS:
///    array - 
PROC RESET_CHALLENGE_ARRAY(RAMPAGE_CHALLENGE& array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		array[i].cType = CHALLENGE_NONE
		array[i].iRequiredAmount = 0
		array[i].sTxtKey = ""
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks to see if a challenge has been furfilled
/// PARAMS:
///    cg - Challenge Reference
/// RETURNS:
///    Success
FUNC BOOL CHECK_CHALLENGE(RAMPAGE_CHALLENGE &cg)
	SWITCH (cg.cType)
		CASE CHALLENGE_OVERKILL
			RETURN (rampageStats.iPedDeadCount >= cg.iRequiredAmount)
		CASE CHALLENGE_HEADSHOT
			RETURN (rampageStats.iHeadShots >= cg.iRequiredAmount)
		CASE CHALLENGE_KILLSTREAK
			RETURN (rampageStats.iMaxChain >= cg.iRequiredAmount)
		CASE CHALLENGE_VEHICLES
			RETURN (rampageStats.iCarDeadCount >= cg.iRequiredAmount)
		CASE CHALLENGE_OCCUPANT
			RETURN (rampageStats.iCarOccupantKills >= cg.iRequiredAmount)
		CASE CHALLENGE_CHOPPER
			RETURN (rampageStats.iChopperDeadCount >= cg.iRequiredAmount)
		CASE CHALLENGE_EXECUTION
			RETURN (rampageStats.iCloseRange >= cg.iRequiredAmount)
		CASE CHALLENGE_LONGSHOT
			RETURN (rampageStats.iLongRange >= cg.iRequiredAmount)
		CASE CHALLENGE_TANK
			RETURN (rampageStats.iTanksDestroyed >= cg.iRequiredAmount)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Count The Number Of Challenges
/// PARAMS:
///    array - 
FUNC INT COUNT_ACTIVE_CHALLENGES(RAMPAGE_CHALLENGE& array[])
	INT i
	INT cnt = 0
	
	REPEAT COUNT_OF(array) i
		IF array[i].cType <> CHALLENGE_NONE
			cnt ++
		ENDIF
	ENDREPEAT
	
	RETURN cnt
ENDFUNC

/// PURPOSE:
///    Count The Number Of Completed Challenges
/// PARAMS:
///    array - 
FUNC INT COUNT_COMPLETED_CHALLENGES(RAMPAGE_CHALLENGE& array[])
	INT i
	INT cnt = 0
	
	REPEAT COUNT_OF(array) i
		IF CHECK_CHALLENGE(array[i])
			cnt ++
		ENDIF
	ENDREPEAT
	
	RETURN cnt
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    cg - Challenge Reference
///    type - Challenge Type
///    cnt - Number of things to do to complete challenge
PROC SET_CHALLENGE(RAMPAGE_CHALLENGE &cg, eRAMPAGE_CHALLENGE type, INT cnt)
	cg.cType = type
	cg.iRequiredAmount = cnt
	SWITCH (cg.cType)
		CASE CHALLENGE_OVERKILL
			cg.sTxtKey = "CH_000"
		BREAK
		CASE CHALLENGE_HEADSHOT
			cg.sTxtKey = "CH_001"
		BREAK
		CASE CHALLENGE_KILLSTREAK
			cg.sTxtKey = "CH_002"
		BREAK
		CASE CHALLENGE_VEHICLES
			cg.sTxtKey = "CH_003"
		BREAK
		CASE CHALLENGE_OCCUPANT
			cg.sTxtKey = "CH_004"
		BREAK
		CASE CHALLENGE_CHOPPER
			cg.sTxtKey = "CH_005"
		BREAK
		CASE CHALLENGE_EXECUTION
			cg.sTxtKey = "CH_006"
		BREAK
		CASE CHALLENGE_LONGSHOT
			cg.sTxtKey = "CH_007"
		BREAK
		CASE CHALLENGE_TANK
			cg.sTxtKey = "CH_008"
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks to see if a challenge has been furfilled
/// PARAMS:
///    cg - Challenge Reference
/// RETURNS:
///    Success
FUNC FLOAT GET_CHALLENGE_PERCENTAGE(RAMPAGE_CHALLENGE &cg)

	IF (cg.iRequiredAmount = 0)
		RETURN 0.0
	ENDIF
	
	SWITCH (cg.cType)
		CASE CHALLENGE_HEADSHOT
			RETURN TO_FLOAT(rampageStats.iHeadShots) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_KILLSTREAK
			RETURN TO_FLOAT(rampageStats.iMaxChain) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_VEHICLES
			RETURN TO_FLOAT(rampageStats.iCarDeadCount) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_OCCUPANT
			RETURN TO_FLOAT(rampageStats.iCarOccupantKills) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_CHOPPER
			RETURN TO_FLOAT(rampageStats.iChopperDeadCount) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_EXECUTION
			RETURN TO_FLOAT(rampageStats.iCloseRange) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_LONGSHOT
			RETURN TO_FLOAT(rampageStats.iLongRange) / TO_FLOAT(cg.iRequiredAmount)
		CASE CHALLENGE_TANK
			RETURN TO_FLOAT(rampageStats.iTanksDestroyed) / TO_FLOAT(cg.iRequiredAmount)
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Gets combined challenge percentage
/// PARAMS:
///    array - 
FUNC FLOAT GET_TOTAL_CHALLENGE_PERCENTAGE(RAMPAGE_CHALLENGE& array[])
	INT cnt = COUNT_ACTIVE_CHALLENGES(array) 
	
	IF (cnt = 0)
		RETURN 0.0
	ENDIF
	
	RETURN TO_FLOAT(COUNT_COMPLETED_CHALLENGES(array)) / TO_FLOAT(cnt)
ENDFUNC


//----------------------
//	SETUP FUNCTIONS
//----------------------

/// PURPOSE:
///    Gets the position of a rampage blip from the rampage info struct
/// RETURNS:
///    Blip Coords
FUNC VECTOR GET_RAMPAGE_POSITION()
	INT iIndex = ENUM_TO_INT(rampageInfo.blipNameEnum)
	RETURN g_GameBlips[iIndex].vCoords[0]
ENDFUNC

/// PURPOSE:
///    Sets a rampage as failed used by the scaleform
FUNC BOOL IS_RAMPAGE_IN_FAIL_UPDATE_STATE()
	RETURN sFailState <> SP_LOADING
ENDFUNC

/// PURPOSE:
///    Sets a rampage as failed used by the scaleform
PROC SET_RAMPAGE_TO_FAIL_UPDATE_STATE()
	sFailState = SP_SETUP
ENDPROC

/// PURPOSE:
///    Zeros out the rampage info struct
PROC RESET_RAMPAGE_INFO()
	INT i 
	
	// reset misc data
	RESET_COMBAT_PARAMETERS(rampageCombatParams)
	rampageInfo.iTargetAmount = 30
	rampageInfo.iTimeLimit = 3 * ONE_MINUTE

	
	rampageInfo.vPlyrStartPos = <<0, 0, 0>>
	rampageInfo.fPlyrStartHeading = 0.0
	rampageInfo.vPlyrCarHidePos = <<0, 0, 0>>
	rampageInfo.fPlyrCarHideHeading = 0.0
	rampageInfo.bPassed = FALSE
	rampageInfo.fAmbientCarDensity = 0.0
	rampageInfo.fAmbientPedDensity = 0.0
	rampageInfo.iRandomSeed = 0
	rampageInfo.bExplosiveProps = FALSE
	rampageInfo.fCarRamChance = 0.0
	
	rampageInfo.sScenarioGroupName = ""
	rampageInfo.vScenarioBlockCenter = <<0, 0, 0>>
	rampageInfo.fScenarioBlockRadius = 0.0
	
	// reset model data
	rampageInfo.explosiveModel = DUMMY_MODEL_FOR_SCRIPT
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.pedModels) i 
		rampageInfo.pedModels[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	rampageInfo.iMaxPedModels = 0
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.carModels) i 
		rampageInfo.carModels[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	rampageInfo.iMaxCarModels = 0
	
	// reset weapon data
	i = 0
	REPEAT COUNT_OF(rampageInfo.plyrWeapon) i 
		rampageInfo.plyrWeapon[i] = WEAPONTYPE_INVALID
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.plyrWeaponComp) i 
		rampageInfo.plyrWeaponComp[i] = WEAPONCOMPONENT_INVALID
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.enemyWeapon) i 
		rampageInfo.enemyWeapon[i] = WEAPONTYPE_INVALID
	ENDREPEAT
	
	// reset voice names
	sVoiceContext = ""
	
	i = 0
	REPEAT COUNT_OF(sMaleVoiceNames) i 
		sMaleVoiceNames[i] = ""
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(sFemaleVoiceNames) i 
		sFemaleVoiceNames[i] = ""
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Release models and other things held by the rampage info struct
PROC RELEASE_RAMPAGE_INFO()
	INT i
	i = 0
	
	IF (rampageInfo.explosiveModel <> DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(rampageInfo.explosiveModel)
	ENDIF
	
	REPEAT COUNT_OF(rampageInfo.pedModels) i 
		IF (rampageInfo.pedModels[i] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(rampageInfo.pedModels[i])
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(rampageInfo.carModels) i 
		IF (rampageInfo.carModels[i] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(rampageInfo.carModels[i])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Populates the rampage info struct based on blip enum
/// PARAMS:
///    bp - rampage blip enum
PROC SET_RAMPAGE_INFO_FROM_STATIC_BLIP(STATIC_BLIP_NAME_ENUM bp)
	VECTOR vmin, vmax 
	
	// write default values in
	RESET_RAMPAGE_INFO()
	rampageInfo.blipNameEnum = bp 
	vBattleCenter = GET_RAMPAGE_POSITION()
	
	
	// then fill them in on a case by case basis
	SWITCH (bp)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			rcRampageMissionID = RC_RAMPAGE_1
			rampageInfo.iRampageIndex = 0
			rampageInfo.iTimeLimit = CEIL(2.0 * ONE_MINUTE)
			
			rampageInfo.sIntroAnim = "trvram_6_2h_intro"
			rampageInfo.sOutroAnim = "trvram_6_2h_outro"
			rampageInfo.sOutroRunAnim = "trvram_6_2h_run_outro"
			rampageInfo.fOutroPhase = 0.15
			rampageInfo.fOutroRunPhase = 0.37
			
			
			rampageInfo.plyrWeapon[0] = WEAPONTYPE_ASSAULTRIFLE
			rampageInfo.plyrWeaponComp[0] = WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01
			rampageInfo.plyrWeaponComp[1] = WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_02
	
			rampageInfo.enemyWeapon[0] = WEAPONTYPE_PISTOL
	
			rampageInfo.pedModels[0] = A_M_M_HILLBILLY_01
			rampageInfo.pedModels[1] = A_M_Y_SALTON_01
			rampageInfo.pedModels[2] = IG_RAMP_HIC
			rampageInfo.iMaxPedModels = 2
			
			rampageInfo.carModels[0] = BFINJECTION //REBEL
			rampageInfo.carModels[1] = BISON
//			rampageInfo.carModels[2] = BFINJECTION
			rampageInfo.iMaxCarModels = 2
			rampageInfo.sTargetName = "TR_RTGT1"	// Rednecks
			rampageInfo.sUIName = "TR_RAMP1"
			
			rampageInfo.fCarRamChance = 0.33		//Cars have a 1/3 chance to ram the player
			
			rampageInfo.bExplosiveProps = TRUE
			rampageInfo.explosiveModel = PROP_GASCYL_01A
			
			rampageInfo.sScenarioGroupName = "Rampage1"
			//rampageInfo.vScenarioBlockCenter = <<928, 3607, 33>>
			//rampageInfo.fScenarioBlockRadius = 140
			
			rampageInfo.vPlyrStartPos = <<908.7723, 3641.4531, 31.4889>>
			rampageInfo.fPlyrStartHeading = 270.3831
			
			rampageInfo.vPlyrCarHidePos = << 899.2652, 3649.5786, 31.7623 >>
			rampageInfo.fPlyrCarHideHeading = 269.0209 
			
			rampageInfo.iTargetAmount = 25
			
			sMaleVoiceNames[0] = "A_M_M_SALTON_02_WHITE_FULL_01"
			sMaleVoiceNames[1] = "A_M_O_SALTON_01_WHITE_FULL_01 "
			sMaleVoiceNames[2] = "A_M_M_HILLBILLY_01_WHITE_MINI_01"
			sMaleVoiceNames[3] = "A_M_M_HILLBILLY_02_WHITE_MINI_01"
			sMaleVoiceNames[4] = "A_M_Y_SALTON_01_WHITE_MINI_01"
			
			sVoiceContext = "GENERIC_INSULT_HIGH"
			sAudioScene = "RAMPAGE_REDNECKS_SCENE"
			iRampageMusicStartTime = 28000	// 38 seconds in
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			rcRampageMissionID = RC_RAMPAGE_2
			rampageInfo.iRampageIndex = 1
			rampageInfo.plyrWeapon[0] = WEAPONTYPE_SAWNOFFSHOTGUN
			
			rampageInfo.enemyWeapon[0] = WEAPONTYPE_PISTOL
			rampageInfo.enemyWeapon[1] = WEAPONTYPE_BAT
			
			rampageInfo.iTimeLimit = 2 * ONE_MINUTE
			
			rampageInfo.sIntroAnim = "trvram_6_2h_intro"
			rampageInfo.sOutroAnim = "trvram_6_2h_outro"
			rampageInfo.sOutroRunAnim = "trvram_6_2h_run_outro"
			rampageInfo.fOutroPhase = 0.15
			rampageInfo.fOutroRunPhase = 0.37
			
			rampageInfo.pedModels[0] = A_M_Y_HIPSTER_03
			rampageInfo.pedModels[1] = A_M_Y_HIPSTER_02 //A_F_Y_HIPSTER_01
			rampageInfo.pedModels[2] = A_M_Y_HIPSTER_01
			rampageInfo.pedModels[3] = IG_RAMP_HIPSTER
			rampageInfo.iMaxPedModels = 3	
			
			rampageInfo.carModels[0] = FAGGIO2
			rampageInfo.carModels[1] = FIXTER
			rampageInfo.carModels[2] = ISSI2
			rampageInfo.iMaxCarModels = 3
			
			rampageInfo.vPlyrStartPos = <<1181.1375, -400.7188, 66.9130>>
			rampageInfo.fPlyrStartHeading = 218.4670
			
			rampageInfo.vPlyrCarHidePos = << 1193.6074, -392.7852, 67.0623 >>
			rampageInfo.fPlyrCarHideHeading = 164.6903
			rampageInfo.sTargetName = "TR_RTGT2"	// Hipsters
			rampageInfo.sUIName = "TR_RAMP5"
			rampageInfo.fAmbientCarDensity = 1.0
			rampageInfo.fAmbientPedDensity = 0.2
			
			sMaleVoiceNames[0] = "A_M_Y_Hipster_03_White_Mini_01"
			sMaleVoiceNames[1] = "A_M_Y_Vinewood_02_White_Mini_01"
			sMaleVoiceNames[2] = "A_M_Y_Vinewood_03_White_Mini_01"
			sMaleVoiceNames[3] = "A_M_Y_Vinewood_04_White_Mini_01"
			
			sFemaleVoiceNames[0] = "A_F_Y_Hipster_01_White_Mini_01"
			sFemaleVoiceNames[1] = "A_F_Y_Vinewood_01_White_Mini_01"
			sFemaleVoiceNames[2] = "A_F_Y_Vinewood_02_White_Mini_01"
			sFemaleVoiceNames[3] = "A_F_Y_Vinewood_03_Chinese_Mini_01"
			sFemaleVoiceNames[4] = "A_F_Y_Vinewood_04_White_Mini_01"
			
			sVoiceContext = "RAMPAGEHIPSTER"
			sAudioScene = "RAMPAGE_HIPSTERS_SCENE"
			rampageInfo.iTargetAmount = 20
			fEnemySecondWeaponChance = 0.0
			fPissTakeRocketChance = 0.0
			iRampageMusicStartTime = 17000	// 19 seconds in
			fBattleRadius = 600.0
			sFormState = SFR_LEADIN
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			rcRampageMissionID = RC_RAMPAGE_3
			rampageInfo.iRampageIndex = 2
			rampageInfo.iTimeLimit = 2 * ONE_MINUTE
			
			rampageInfo.sIntroAnim = "trvram_6_1h_intro"
			rampageInfo.sOutroAnim = "trvram_6_1h_outro"
			rampageInfo.sOutroRunAnim = "trvram_6_1h_run_outro"
			rampageInfo.fOutroPhase = 0.19
			rampageInfo.fOutroRunPhase = 0.24
			
			rampageInfo.plyrWeapon[0] = WEAPONTYPE_MICROSMG
			rampageInfo.plyrWeaponComp[0] = WEAPONCOMPONENT_AT_SCOPE_MACRO
			rampageInfo.plyrWeaponComp[1] = WEAPONCOMPONENT_MICROSMG_CLIP_02	
			
			rampageInfo.enemyWeapon[0] = WEAPONTYPE_PISTOL
			rampageInfo.enemyWeapon[1] = WEAPONTYPE_ASSAULTRIFLE
			
			rampageInfo.pedModels[0] = A_M_M_MEXLABOR_01
			rampageInfo.pedModels[1] = G_M_Y_MEXGOON_03
			rampageInfo.pedModels[2] = A_M_Y_MEXTHUG_01
			rampageInfo.pedModels[3] = IG_RAMP_MEX
			rampageInfo.iMaxPedModels = 3
			
			rampageInfo.carModels[0] = CAVALCADE2
			rampageInfo.carModels[1] = VIGERO
			rampageInfo.iMaxCarModels = 2
			
			rampageInfo.vPlyrStartPos = <<463.9719, -1848.9276, 26.8640>>
			rampageInfo.fPlyrStartHeading = 135.7623
			
			rampageInfo.vPlyrCarHidePos = << 444.0720, -1861.6890, 26.5592 >>
			rampageInfo.fPlyrCarHideHeading = 131.0001 
			rampageInfo.sTargetName = "TR_RTGT3"	// Gang Members
			rampageInfo.sUIName = "TR_RAMP2"
			
			rampageInfo.iTargetAmount = 30	
			iRampageMusicStartTime = 15500	// 15.5 seconds in
			
			sVoiceContext = "GENERIC_INSULT_HIGH"
			sAudioScene = "RAMPAGE_VAGOS_SCENE"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			rcRampageMissionID = RC_RAMPAGE_4
			rampageInfo.iRampageIndex = 3
			rampageInfo.iTimeLimit = 2 * ONE_MINUTE
			rampageInfo.plyrWeapon[0] = WEAPONTYPE_ASSAULTRIFLE
			rampageInfo.enemyWeapon[0] = WEAPONTYPE_PISTOL
			
			rampageInfo.pedModels[0] = G_M_Y_BALLAEAST_01
			rampageInfo.pedModels[1] = G_M_Y_BALLAORIG_01
			rampageInfo.pedModels[2] = G_M_Y_BALLASOUT_01
			rampageInfo.pedModels[3] = IG_RAMP_GANG
			rampageInfo.iMaxPedModels = 3		

			rampageInfo.sIntroAnim = "trvram_6_2h_intro"
			rampageInfo.sOutroAnim = "trvram_6_2h_outro"
			rampageInfo.sOutroRunAnim = "trvram_6_2h_run_outro"
			rampageInfo.fOutroPhase = 0.15
			rampageInfo.fOutroRunPhase = 0.37
			
			rampageInfo.carModels[0] = ORACLE //TAILGATER
			rampageInfo.carModels[1] = FELON //DOMINATOR	
			rampageInfo.iMaxCarModels = 2
			
			rampageInfo.vPlyrStartPos = <<-162.3577, -1668.2686, 32.1239>>
			rampageInfo.fPlyrStartHeading = 327.5840
			
			rampageInfo.vPlyrCarHidePos = << -168.0764, -1633.8662, 32.6486 >>
			rampageInfo.fPlyrCarHideHeading = 262.9751 
			rampageInfo.sTargetName = "TR_RTGT4"	// Gang Members
			rampageInfo.sUIName = "TR_RAMP3"
			
			rampageInfo.iTargetAmount = 30	
			vBattleCenter = <<-129.0, -1622.7, 32.1>>
			iRampageMusicStartTime = 14100	// 14.1 seconds in
			sAudioScene = "RAMPAGE_BALLAS_SCENE"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			rcRampageMissionID = RC_RAMPAGE_5
			rampageInfo.iRampageIndex = 4
			
			rampageInfo.sIntroAnim = "trvram_6_2h_intro"
			rampageInfo.sOutroAnim = "trvram_6_2h_outro"
			rampageInfo.sOutroRunAnim = "trvram_6_2h_run_outro"
			rampageInfo.fOutroPhase = 0.15
			rampageInfo.fOutroRunPhase = 0.37
			
			rampageInfo.plyrWeapon[0] = WEAPONTYPE_GRENADELAUNCHER
			rampageInfo.plyrWeapon[1] = WEAPONTYPE_ASSAULTRIFLE
			
			rampageInfo.enemyWeapon[0] = WEAPONTYPE_PISTOL
			rampageInfo.enemyWeapon[1] = WEAPONTYPE_ASSAULTRIFLE
			
			rampageInfo.pedModels[0] = S_M_Y_MARINE_01
			rampageInfo.pedModels[1] = S_M_Y_MARINE_03
//			rampageInfo.pedModels[2] = S_M_Y_MARINE_02
			rampageInfo.iMaxPedModels = 2
			
			rampageInfo.carModels[0] = CRUSADER
			rampageInfo.carModels[1] = BARRACKS
			rampageInfo.carModels[2] = RHINO
			rampageInfo.iMaxCarModels = 2
			
			rampageInfo.vPlyrStartPos = <<-1295.7534, 2507.1167, 20.2824>>
			rampageInfo.fPlyrStartHeading = 336.2132
			
			rampageInfo.vPlyrCarHidePos = <<-1317.2107, 2482.0774, 22.8051>>
			rampageInfo.fPlyrCarHideHeading = 311.7142
			rampageInfo.sTargetName = "TR_RTGT5"	// Soldiers
			rampageInfo.sUIName = "TR_RAMP4"
			
			rampageInfo.iTargetAmount = 35	
			rampageInfo.iRandomSeed = 19780921
			fPissTakeRocketChance = 0.8
			iRampageMusicStartTime = 16000	// 19 seconds in
			sAudioScene = "RAMPAGE_MILITARY_SCENE"
		BREAK
		DEFAULT	
			SCRIPT_ASSERT("THIS BLIP IS NOT A RAMPAGE BLIP")
		BREAK
	ENDSWITCH
	
	//rampageInfo.iTimeLimit = CEIL(0.5 * ONE_MINUTE)
	PREPARE_RAMPAGE_ONE_SHOT()
	
	// scenario blocking
	IF NOT IS_VECTOR_ZERO(rampageInfo.vScenarioBlockCenter) AND (rampageInfo.fScenarioBlockRadius > 0.0)
		CALCULATE_MIN_MAX_FROM_POSITION_AND_RADIUS(rampageInfo.vScenarioBlockCenter, rampageInfo.fScenarioBlockRadius, vmin, vmax)
		scenarioBlockID = ADD_SCENARIO_BLOCKING_AREA(vmin, vmax)
		CPRINTLN(DEBUG_RAMPAGE, "BLOCKING SCENARIO AREA ", vmin, " TO ", vmax)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(rampageInfo.sScenarioGroupName)
		SET_SCENARIO_GROUP_ENABLED(rampageInfo.sScenarioGroupName, FALSE)
		CPRINTLN(DEBUG_RAMPAGE, "BLOCKING SCENARIO GROUP", rampageInfo.sScenarioGroupName)
	ENDIF
	
	CALCULATE_MIN_MAX_FROM_POSITION_AND_RADIUS(vBattleCenter, fBattleRadius, vmin, vmax)
	CPRINTLN(DEBUG_RAMPAGE, "RESETTING PED PATHS AND NAVMESH AROUND ", vmin, " TO ", vmax)
	DISABLE_NAVMESH_IN_AREA(vmin, vmax, FALSE)
	SET_PED_PATHS_BACK_TO_ORIGINAL(vmin, vmax)
	
//	BLOCK_SCENARIOS_AND_AMBIENT(worldScenarioBlockID, FALSE)
ENDPROC

/// PURPOSE:
///    Populates the rampage spawnsystem struct based on blip enum
/// PARAMS:
///    bp - rampage blip enum
PROC SET_RAMPAGE_COMBAT_PARAMS_FROM_STATIC_BLIP(STATIC_BLIP_NAME_ENUM bp)
	RESET_COMBAT_PARAMETERS(rampageCombatParams)
	
	// then fill them in on a case by case basis
	SWITCH (bp)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1	
			rampageCombatParams.fWeaponAccuracy1 = 0.12  // 0.06
			rampageCombatParams.fWeaponAccuracy2 = 0.12  // 0.06
			rampageCombatParams.fNearBlankChance = 0.1	 // 0.2
			rampageCombatParams.fFarBlankChance = 0.5	 // 0.6
			rampageCombatParams.iMinShootRate = 30		 // 20
			rampageCombatParams.iMaxShootRate = 55		 // 45
			rampageCombatParams.fPlayerWeaponDefenseModifier = 0.25  // 0.2
			//@RJP make peds always aggressive and more likely to flank
			rampageCombatParams.fAgressionChance = 1
			rampageCombatParams.fFlankChance = 0.5
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			rampageCombatParams.fWeaponAccuracy1 = 0.032
			rampageCombatParams.fWeaponAccuracy2 = 0.015
			rampageCombatParams.fNearBlankChance = 0.4
			rampageCombatParams.fFarBlankChance = 0.6
			rampageCombatParams.iMinShootRate = 20
			rampageCombatParams.iMaxShootRate = 40
			rampageCombatParams.fPlayerWeaponDefenseModifier = 0.25
			//@RJP making enemies in all rampages more aggressive
			rampageCombatParams.fAgressionChance = 1
			rampageCombatParams.fFlankChance = 0.5
		BREAK		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5	
			rampageCombatParams.fWeaponAccuracy1 = 0.12  // 0.06
			rampageCombatParams.fWeaponAccuracy2 = 0.09  // 0.06
			rampageCombatParams.fNearBlankChance = 0.2	 // 0.2
			rampageCombatParams.fFarBlankChance = 0.6	 // 0.6
			rampageCombatParams.iMinShootRate = 20		 // 20
			rampageCombatParams.iMaxShootRate = 50		 // 45
			rampageCombatParams.fPlayerWeaponDefenseModifier = 0.11  // 0.2
//			rampageCombatParams.fAgressionChance = 0.45
			//@RJP making enemies in all rampages more aggressive
			rampageCombatParams.fAgressionChance = 1
			rampageCombatParams.fFlankChance = 0.5
		BREAK
		DEFAULT
			rampageCombatParams.fWeaponAccuracy1 = 0.032
			rampageCombatParams.fWeaponAccuracy2 = 0.015
			rampageCombatParams.fNearBlankChance = 0.4
			rampageCombatParams.fFarBlankChance = 0.6
			rampageCombatParams.iMinShootRate = 20
			rampageCombatParams.iMaxShootRate = 40
			rampageCombatParams.fPlayerWeaponDefenseModifier = 0.2
			//@RJP making enemies in all rampages more aggressive
			rampageCombatParams.fAgressionChance = 1
			rampageCombatParams.fFlankChance = 0.5
		BREAK		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Populates no spawn areas based on blip enum
/// PARAMS:
///    bp - rampage blip enum
PROC SET_RAMPAGE_NO_SPAWN_AREAS_FROM_STATIC_BLIP(STATIC_BLIP_NAME_ENUM bp)

	iMaxActiveNoSpawnAreas = 0
	
	// then fill them in on a case by case basis
	SWITCH (bp)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			ADD_NO_SPAWN_AREA(<<899.080017,3616.289307,30.824312>>, <<889.898193,3616.469971,33.862087>>, 6.300000)
			ADD_NO_SPAWN_AREA(<<900.753662,3579.707275,30.141163>>, <<916.025208,3579.704102,37.618240>>, 13.000000)
			ADD_NO_SPAWN_AREA(<<908.902710,3571.474121,30.782627>>, <<908.867249,3554.104004,37.818115>>, 12.000000)
			ADD_NO_SPAWN_AREA(<<985.621765,3578.334961,30.448856>>, <<997.556702,3569.890137,41.186794>>, 9.750000)
			ADD_NO_SPAWN_AREA(<<954.739319,3617.572510,30.717031>>, <<960.034668,3617.539063,34.758904>>, 8.000000)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			ADD_NO_SPAWN_AREA(<<1145.495972,-368.676819,66.048904>>, <<1133.037842,-417.905365,77.887863>>, 16.500000)
			ADD_NO_SPAWN_AREA(<<1186.173950,-386.635620,66.453995>>, <<1156.464722,-379.281464,72.744225>>, 8.750000)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			ADD_NO_SPAWN_AREA(<<413.366638,-1746.958740,27.106783>>, <<405.916229,-1742.053589,33.078545>>, 7.500000)
			ADD_NO_SPAWN_AREA(<<444.245178,-1745.233398,26.727312>>, <<435.744415,-1741.421143,32.967648>>, 8.750000)
			ADD_NO_SPAWN_AREA(<<468.188049,-1802.802124,26.342609>>, <<449.523804,-1787.471436,33.342533>>, 13.000000)
			ADD_NO_SPAWN_AREA(<<531.593262,-1806.028564,26.562597>>, <<521.041504,-1806.093140,31.505949>>, 10.000000)
			ADD_NO_SPAWN_AREA(<<419.392212,-1845.889404,25.887005>>, <<425.954987,-1850.847412,28.781279>>, 7.000000)
			ADD_NO_SPAWN_AREA(<<481.791687,-1735.011353,30.859770>>, <<488.150574,-1737.297607,26.943594>>, 11.200000)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4	
			ADD_NO_SPAWN_AREA(<<-139.409958,-1648.285156,31.543270>>, <<-133.891083,-1653.293701,34.950226>>, 3.500000)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			ADD_NO_SPAWN_AREA(<<-1277.212524,2569.290527,22.069016>>, <<-1284.096802,2548.221680,28.853512>>, 19.250000)
		BREAK
		DEFAULT	
			SCRIPT_ASSERT("THIS BLIP IS NOT A RAMPAGE BLIP")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Populates no spawn areas based on blip enum
/// PARAMS:
///    bp - rampage blip enum
PROC SET_RAMPAGE_SPAWNSYSTEM_FROM_STATIC_BLIP(STATIC_BLIP_NAME_ENUM bp)
	
	// setup backup spawner
	rampageBackupCloudSpawner.bIsActive = FALSE
	rampageBackupCloudSpawner.bPavementOnly = FALSE
	rampageBackupCloudSpawner.iMinSpawnTime = 250
	rampageBackupCloudSpawner.iMaxSpawnTime = 1000
		
	rampageBackupCloudSpawner.fMinSpawnRadius = 30.0
	rampageBackupCloudSpawner.fMaxSpawnRadius = 80.0
	rampageBackupCloudSpawner.fFleeChance = 0.0
	
	// then fill them in on a case by case basis
	SWITCH (bp)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			iInitialSpawnDelay = 8000
			iInitialCarSpawnDelay = 8000
			
			// setup car spawner
			rampageCarSpawner.bIsActive = TRUE
			rampageCarSpawner.bUseSpawnPoints = TRUE
			rampageCarSpawner.iMaxActiveCars = 1
			rampageCarSpawner.iMinSpawnTime = 20000
			rampageCarSpawner.iMaxSpawnTime = 40000
	
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[0], <<638.8113, 3575.6160, 31.8078>>, 280.4006)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[1], <<677.1325, 3520.3567, 33.1989>>, 276.7284)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[2], <<1256.5873, 3540.4756, 34.2046>>, 90.8648)
			rampageCarSpawner.iSpawnPointsUsed = 3
			
			// setup fortress spawner
			rampageFortressSpawner.bIsActive = TRUE
			rampageFortressSpawner.iMaxActiveFortress = 7
			rampageFortressSpawner.iMinSpawnTime = 1000
			rampageFortressSpawner.iMaxSpawnTime = 3000
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[0], <<898.879761,3595.323242,31.918310>>, <<899.167908,3626.273682,32.514000>>, 30.750000, TRUE, 1)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[0], 0.0)
						
			SETUP_RAMPAGE_FORTRESS(rampageFort[1], <<887.112671,3583.712158,32.403027>>, <<922.399719,3584.079346,34.894115>>, 25.750000, TRUE, 2)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[1], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[2], <<921.673157,3558.021973,32.785378>>, <<878.298828,3557.901611,34.441288>>, 30.750000, TRUE, 3)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[2], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[3], <<1020.117737,3577.570557,31.758381>>, <<968.554504,3573.576172,32.805687>>, 35.250000, TRUE, 4)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[3], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[4], <<977.714905,3614.298828,31.531330>>, <<936.611206,3614.114014,32.604645>>, 29.500000, TRUE, 5)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[4], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[5], <<928.147705,3650.682861,31.363585>>, <<887.448364,3651.773682,32.843071>>, 32.000000, TRUE, 6)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[5], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[6], <<851.545593,3676.695313,31.115583>>, <<871.752380,3671.204346,32.785095>>, 13.500000, TRUE, 0)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[6], 0.0)
	
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			iInitialSpawnDelay = 2000
			iInitialCarSpawnDelay = 6000
			
			// setup car spawner
			rampageCarSpawner.bIsActive = TRUE
			rampageCarSpawner.bUseSpawnPoints = TRUE
			rampageCarSpawner.iMinActiveCars = 2
			rampageCarSpawner.iMaxActiveCars = 4
			rampageCarSpawner.iMinSpawnTime = 8000
			rampageCarSpawner.iMaxSpawnTime = 12000
			
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[0], <<1261.5217, -392.1491, 68.0716>>, 45.8832)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[1], <<1160.7268, -363.8029, 66.4686>>, 249.5359)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[2], <<1112.8073, -516.7747, 62.7259>>, 259.5641)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[3], <<1222.8971, -524.8954, 65.7801>>, 50.4901)
			rampageCarSpawner.iSpawnPointsUsed = 4
			
			// spawning
			rampageAlleySpawner.bIsActive = TRUE
			rampageAlleySpawner.vAlleyPoints[0] = <<1208.0143, -386.7501, 67.5128>>
			rampageAlleySpawner.vAlleyPoints[1] = <<1179.5604, -503.5638, 64.3207>>
			rampageAlleySpawner.fAlleyDiffAdjust = -1
			
			// ground based
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[0], <<1171.7067, -395.8017, 70.5896>>, 179.2177)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[1], <<1174.9569, -405.9748, 66.7132>>, 237.7915)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[2], <<1177.9843, -415.0459, 66.4166>>, 263.1743)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[3], <<1172.0057, -422.1147, 66.0902>>, 248.2870)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[4], <<1173.9624, -430.9555, 66.0785>>, 215.4900)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[5], <<1217.7241, -418.0172, 66.7731>>, 104.3705)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[6], <<1228.4182, -390.4437, 68.0662>>, 67.1844)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[7], <<1219.3175, -424.2363, 66.6526>>, 85.3891)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[8], <<1215.1510, -438.7675, 66.1798>>, 64.3428)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[9], <<1168.2654, -457.0743, 65.6949>>, 274.1016)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[10], <<1158.5686, -454.4118, 65.9844>>, 248.8965)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[11], <<1140.5071, -461.1952, 65.9288>>, 269.3017)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[12], <<1133.4406, -480.2752, 65.0497>>, 246.3703)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[13], <<1131.1036, -481.0168, 64.9569>>, 242.0116)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[14], <<1123.4287, -502.5286, 63.0634>>, 290.4138)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[15], <<1130.8507, -501.8386, 63.2847>>, 278.3421)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[16], <<1202.5520, -505.4180, 64.1416>>, 55.5955)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[17], <<1203.6964, -507.7457, 64.3037>>, 45.4403)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[18], <<1207.2788, -470.6629, 65.1517>>, 81.1791)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[19], <<1205.9600, -469.2520, 65.2501>>, 124.0248)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[20], <<1207.1526, -463.4705, 65.4440>>, 110.4368)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[21], <<1212.2612, -445.3408, 65.9532>>, 94.8838)
			
			rampageAlleySpawner.iMaxActiveSpawnPoints = 22

		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			iInitialSpawnDelay = 5000
			iInitialCarSpawnDelay = 10000
			
			// setup fortress spawner
			rampageFortressSpawner.bIsActive = TRUE
			rampageFortressSpawner.iMaxActiveFortress = 8
			rampageFortressSpawner.iMinSpawnTime = 1000
			rampageFortressSpawner.iMaxSpawnTime = 3000
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[0], <<491.036011,-1816.695068,26.366055>>, <<508.153839,-1831.315796,29.419441>>, 25.000000, TRUE, 1)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[0], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[1], <<433.541656,-1774.958252,29.387346>>, <<471.039612,-1807.620605,26.340366>>, 20.000000, TRUE, 2)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[1], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[2], <<507.713135,-1790.534058,26.500551>>, <<533.069275,-1790.829346,30.167883>>, 39.000000, TRUE, 3)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[2], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[3], <<484.440094,-1755.382446,29.064936>>, <<451.243195,-1742.952759,27.251459>>, 22.250000, TRUE, 4)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[3], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[4], <<458.570557,-1721.835205,29.504240>>, <<491.081787,-1734.426636,27.306875>>, 25.000000, TRUE, 5)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[4], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[5], <<519.821716,-1763.702393,26.695234>>, <<538.813416,-1722.355957,31.265846>>, 32.000000, TRUE, 6)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[5], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[6], <<480.949249,-1717.530273,29.864294>>, <<503.944916,-1684.640991,27.394945>>, 25.000000, TRUE, 7)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[6], 0.0)
			
			SETUP_RAMPAGE_FORTRESS(rampageFort[7], <<436.893097,-1841.786865,25.842968>>, <<413.346313,-1817.801025,29.323025>>, 40.000000, TRUE, 0)
			SETUP_RAMPAGE_FORTRESS_SPAWNING(rampageFort[7], 0.0)
			
			// spawning
			rampageAlleySpawner.bIsActive = FALSE
			
			// roof top molotov throwers
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[0], <<467.5966, -1853.2515, 32.0385>>, 15.2180, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[1], <<441.4879, -1827.1527, 30.6772>>, 269.2424, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[2], <<425.6849, -1850.3021, 29.6723>>, 232.5502, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[3], <<474.1450, -1861.8615, 32.0385>>, 304.1951, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[4], <<488.6247, -1827.9253, 31.0552>>, 44.0614, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[5], <<502.1079, -1814.2856, 32.7449>>, 43.2727, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[6], <<507.4474, -1797.3165, 30.3208>>, 65.9409, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[7], <<512.1399, -1788.1361, 31.2764>>, 106.2277, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[8], <<487.3958, -1730.4272, 31.0703>>, 211.2030, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[9], <<478.1194, -1740.2147, 31.7743>>, 232.2849, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[10], <<474.0694, -1760.0837, 31.5086>>, 250.7436, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[11], <<475.3656, -1772.2528, 31.1009>>, 267.3608, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[12], <<477.9729, -1792.3981, 31.2899>>, 235.5816, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[13], <<470.5758, -1797.9939, 31.4238>>, 264.4785, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[14], <<463.8697, -1805.9473, 31.4137>>, 177.1468, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[15], <<447.8933, -1792.6598, 31.3978>>, 141.8946, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[16], <<428.7633, -1813.7426, 30.6083>>, 297.5291, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[3], <<444.2840, -1867.4685, 30.3466>>, 53.9152, TRUE)
			
			rampageAlleySpawner.iMaxActiveSpawnPoints = 17
			
			// setup car spawner
			rampageCarSpawner.bIsActive = TRUE
			rampageCarSpawner.bUseSpawnPoints = TRUE
			rampageCarSpawner.iMaxActiveCars = 1
			rampageCarSpawner.iMinSpawnTime = 20000
			rampageCarSpawner.iMaxSpawnTime = 30000
			
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[0], <<406.0836, -1916.9851, 24.0788>>, 44.7007)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[1], <<533.4858, -1889.4647, 24.4219>>, 102.9549)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[2], <<543.5394, -1713.4049, 28.2633>>, 59.5502)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[3], <<413.8781, -1810.7837, 27.7617>>, 303.4869)
			rampageCarSpawner.iSpawnPointsUsed = 4
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4	
		
			iInitialSpawnDelay = 5000
			iInitialCarSpawnDelay = 8000	
			
			// spawning
			rampageAlleySpawner.bIsActive = TRUE
			rampageAlleySpawner.vAlleyPoints[0] = <<-162.145447,-1663.402344,32.8>>
			rampageAlleySpawner.vAlleyPoints[1] = <<-91.898834,-1579.666504,31.5>>			
			rampageAlleySpawner.fAlleyRadius = 10.0
			
			// ground based
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[0], <<-148.5, -1668.9, 32.3>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[1], <<-135.4, -1651.9, 32.3>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[2], <<-149.8, -1630.2, 33.1>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[3], <<-119.9, -1637.4, 32.2>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[4], <<-140.5, -1612.6, 33.3>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[5], <<-98.7, -1607.7, 31.9>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[6], <<-117.7, -1591.1, 33.3>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[7], <<-104.0, -1567.6, 32.7>>, 0.0)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[8], <<-77.9, -1588.7, 30.4>>, 0.0)	
			
			//roof top points
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[9], <<-95.6932, -1595.2693, 37.5381>>, 119.5596, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[10], <<-106.9553, -1586.2290, 39.4618>>, 231.9059, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[11], <<-125.7233, -1608.6276, 40.5382>>, 211.3573, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[12], <<-116.0610, -1620.4855, 38.8444>>, 48.9904, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[13], <<-144.6918, -1631.8112, 39.7488>>, 210.9559, TRUE)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[14], <<-138.2615, -1645.8605, 39.0303>>, 14.7954, TRUE)
			
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[15], <<-172.5470, -1649.6149, 32.4385>>, 173.3268)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[16], <<-175.8211, -1646.3540, 32.3339>>, 178.1288)
			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[17], <<-165.5250, -1690.7644, 31.2333>>, 20.2723)
			
			// air based
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[9], <<-150.9, -1662.3, 36.5>>, 0.0, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[10], <<-149.9, -1659.4, 36.5>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[11], <<-159.3, -1649.0, 37.7>>, 0.0, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[12], <<-161.1, -1652.2, 37.7>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[13], <<-155.2, -1645.0, 37.7>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[14], <<-143.8, -1653.6, 36.5>>, 0.0, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[15], <<-142.2, -1650.6, 36.5>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[16], <<-137.0, -1644.8, 36.5>>, 0.0, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[17], <<-136.1, -1642.7, 36.5>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[18], <<-131.1, -1636.8, 36.5>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[19], <<-124.0, -1628.9, 35.9>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[20], <<-119.4, -1623.3, 35.9>>, 0.0, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[21], <<-117.5, -1620.5, 35.9>>, 0.0, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[22], <<-127.7, -1611.2, 37.8>>, 0.0, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[23], <<-129.5, -1613.6, 37.8>>, 0.0, TRUE)
			
			//roof top points
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[24], <<-95.6932, -1595.2693, 37.5381>>, 119.5596, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[25], <<-106.9553, -1586.2290, 39.4618>>, 231.9059, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[26], <<-125.7233, -1608.6276, 40.5382>>, 211.3573, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[27], <<-116.0610, -1620.4855, 38.8444>>, 48.9904, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[28], <<-144.6918, -1631.8112, 39.7488>>, 210.9559, TRUE)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[29], <<-138.2615, -1645.8605, 39.0303>>, 14.7954, TRUE)
//			
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[30], <<-172.5470, -1649.6149, 32.4385>>, 173.3268)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[31], <<-175.8211, -1646.3540, 32.3339>>, 178.1288)
//			SET_RAMPAGE_SPAWN_POINT(rampageAlleySpawner.spawnPoint[32], <<-165.5250, -1690.7644, 31.2333>>, 20.2723)
			
			rampageAlleySpawner.iMaxActiveSpawnPoints = 18
			
			// setup car spawner
			rampageCarSpawner.bIsActive = TRUE
			rampageCarSpawner.bUseSpawnPoints = TRUE
			rampageCarSpawner.iMaxActiveCars = 1
			rampageCarSpawner.iMinSpawnTime = 20000
			rampageCarSpawner.iMaxSpawnTime = 30000
			
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[0], <<-102.4641, -1560.3872, 32.0852>>, 228.8770)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[1], <<-62.2571, -1593.2477, 28.5327>>, 43.1166)
				
			rampageCarSpawner.iSpawnPointsUsed = 2
			
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			iInitialSpawnDelay = 8000
			iInitialCarSpawnDelay = 5000	
			
			iMaxActivePeds = MAX_ACTIVE_PEDS
			rampageCarSpawner.bIsActive = TRUE
			rampageCarSpawner.bUseSpawnPoints = TRUE
			rampageCarSpawner.iMinActiveCars = 2
			rampageCarSpawner.iMaxActiveCars = 4
			rampageCarSpawner.iMinSpawnTime = 4000
			rampageCarSpawner.iMaxSpawnTime = 10000
	
			rampageCloudSpawner.fMinSpawnRadius = 20.0
			rampageCloudSpawner.fMaxSpawnRadius = 60.0
	
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[0], <<-1111.0688, 2673.1702, 17.5640>>, 134.8095)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[1], <<-1504.7275, 2397.6868, 25.2559>>, 269.3951)
			SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[2], <<-1536.2335, 2740.8357, 16.6437>>, 233.2830)
			
			rampageCarSpawner.iSpawnPointsUsed = 3
		BREAK
		DEFAULT	
			SCRIPT_ASSERT("THIS BLIP IS NOT A RAMPAGE BLIP")
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEANUP_EXPLOSIVE_PROPS()
	INT i
	REPEAT MAX_EXPLOSIVE_PROPS i
			IF DOES_ENTITY_EXIST( explosiveObject[i].objID )
				SET_OBJECT_AS_NO_LONGER_NEEDED( explosiveObject[i].objID )
				explosiveObject[i].bCreated = FALSE
			ENDIF
	ENDREPEAT
ENDPROC

PROC CREATE_EXLOSIVE_PROPS()
	IF NOT rampageInfo.bExplosiveProps
		EXIT
	ENDIF
	
	INT i
	REPEAT MAX_EXPLOSIVE_PROPS i
		IF NOT explosiveObject[i].bCreated
			IF NOT DOES_ENTITY_EXIST( explosiveObject[i].objID )
				explosiveObject[i].objID = CREATE_OBJECT( rampageInfo.explosiveModel, explosiveObject[i].pos )
				SET_ENTITY_ROTATION( explosiveObject[i].objID, explosiveObject[i].rot )
				explosiveObject[i].bCreated = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SETUP_EXPLOSIVE_PROP( RAMPAGE_EXPLOSIVE_PROP &expProp, VECTOR pos, VECTOR rot )
	expProp.pos = pos
	expProp.rot = rot
ENDPROC

PROC SETUP_RAMPAGE_EXPLOSIVE_PROPS( STATIC_BLIP_NAME_ENUM bp )
	SWITCH bp
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			SETUP_EXPLOSIVE_PROP( explosiveObject[0], <<917.5861, 3593.5764, 32.1551>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[1], <<899.7161, 3607.4707, 31.8142>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[2], <<889.1971, 3618.6641, 31.8218>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[3], <<901.7499, 3586.1833, 32.4256>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[4], <<915.1172, 3569.6101, 32.7876>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[5], <<979.5751, 3584.9543, 32.4930>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[6], <<999.9662, 3583.3921, 32.6675>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[7], <<956.1263, 3620.4758, 31.6291>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[8], <<864.4873, 3671.1313, 31.8982>>, <<0,0,0>> )
			SETUP_EXPLOSIVE_PROP( explosiveObject[9], <<921.76, 3644.44, 31.5245>>, <<0,0,0>> )
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Populates the rampage info struct based on blip enum
/// PARAMS:
///    bp - rampage blip enum
PROC SET_RAMPAGE_CHALLENGE_FROM_STATIC_BLIP(STATIC_BLIP_NAME_ENUM bp)

	RESET_CHALLENGE_ARRAY(rampageChallenge)
	
	// then fill them in on a case by case basis
	SWITCH (bp)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			SET_CHALLENGE(rampageChallenge[0], CHALLENGE_OVERKILL, 45)
			SET_CHALLENGE(rampageChallenge[1], CHALLENGE_HEADSHOT, 3)		
			SET_CHALLENGE(rampageChallenge[2], CHALLENGE_VEHICLES, 2)
			//SET_CHALLENGE(rampageChallenge[3], CHALLENGE_KILLSTREAK, 4)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			SET_CHALLENGE(rampageChallenge[0], CHALLENGE_OVERKILL, 30)
			SET_CHALLENGE(rampageChallenge[1], CHALLENGE_HEADSHOT, 10)
			SET_CHALLENGE(rampageChallenge[2], CHALLENGE_VEHICLES, 2)
			//SET_CHALLENGE(rampageChallenge[2], CHALLENGE_EXECUTION, 5)			
			//SET_CHALLENGE(rampageChallenge[3], CHALLENGE_KILLSTREAK, 5)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			SET_CHALLENGE(rampageChallenge[0], CHALLENGE_OVERKILL, 45)
			SET_CHALLENGE(rampageChallenge[1], CHALLENGE_HEADSHOT, 6)		
			SET_CHALLENGE(rampageChallenge[2], CHALLENGE_VEHICLES, 2)
			//SET_CHALLENGE(rampageChallenge[3], CHALLENGE_KILLSTREAK, 6)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			SET_CHALLENGE(rampageChallenge[0], CHALLENGE_OVERKILL, 50)
			SET_CHALLENGE(rampageChallenge[1], CHALLENGE_HEADSHOT, 6)
			SET_CHALLENGE(rampageChallenge[2], CHALLENGE_VEHICLES, 2)
			//SET_CHALLENGE(rampageChallenge[3], CHALLENGE_KILLSTREAK, 7)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			SET_CHALLENGE(rampageChallenge[0], CHALLENGE_OVERKILL, 45)
			SET_CHALLENGE(rampageChallenge[1], CHALLENGE_HEADSHOT, 6)	
			SET_CHALLENGE(rampageChallenge[2], CHALLENGE_TANK, 2)
			//SET_CHALLENGE(rampageChallenge[3], CHALLENGE_KILLSTREAK, 8)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Setups the rampage systems based on blip enum
/// PARAMS:
///    bp - rampage blip enum
PROC SET_RAMPAGE_FROM_STATIC_BLIP(STATIC_BLIP_NAME_ENUM bp)
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	SET_RAMPAGE_INFO_FROM_STATIC_BLIP(bp)
	SET_RAMPAGE_NO_SPAWN_AREAS_FROM_STATIC_BLIP(bp)
	SET_RAMPAGE_SPAWNSYSTEM_FROM_STATIC_BLIP(bp)
	SET_RAMPAGE_COMBAT_PARAMS_FROM_STATIC_BLIP(bp)
	SET_RAMPAGE_CHALLENGE_FROM_STATIC_BLIP(bp)
	
	IF rampageInfo.bExplosiveProps
		SETUP_RAMPAGE_EXPLOSIVE_PROPS(bp)
	ENDIF
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(rampageInfo.fAmbientPedDensity)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(rampageInfo.fAmbientCarDensity)
	
	// do check to see if we have beaten rampage score and write medal to 
	CPRINTLN(DEBUG_RAMPAGE, "-PRE RAMPAGE SAVE DATA CHECK----------------------")
	CPRINTLN(DEBUG_RAMPAGE, "  RAMPAGE #:", rampageInfo.iRampageIndex)
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, "  SAVED MEDAL INDEX :", g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex)
	CPRINTLN(DEBUG_RAMPAGE, "  SAVED HIGH SCORE  :", g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore)
	CPRINTLN(DEBUG_RAMPAGE, "--------------------------------------------------")
ENDPROC

/// PURPOSE:
///    Initializes the rampage 
/// PARAMS:
///    sData - script arg data (this passed in from the launcher)
PROC RAMPAGE_SETUP(g_structRCScriptArgs& sData)
	INT i 
	
	// fill in text for z skip menu
	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "INTRO"    
		sSkipMenu[1].sTxtLabel = "BATTLE"     
		sSkipMenu[2].sTxtLabel = "STAGE CLEAR"     
	#ENDIF  
		
	// fill in array for medal colors
	medalHudColor[0] = HUD_COLOUR_RED
	medalHudColor[1] = HUD_COLOUR_BRONZE
	medalHudColor[2] = HUD_COLOUR_SILVER
	medalHudColor[3] = HUD_COLOUR_GOLD
	
	// setup groups and script arguments
	g_bIsOnRampage = TRUE
	g_RestoreStoredWeaponsOnDeath = TRUE
	
	rampageScriptArgs = sData
	CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE VERSION", RAMPAGE_VERSION)
	
	#IF USE_TU_CHANGES
		CPRINTLN(DEBUG_RAMPAGE, "TU CHANGES ARE IN EFFECT")	
		CPRINTLN(DEBUG_RAMPAGE, "Disabling all Cheats - Current Cheat Variable:", g_iBitsetCheatsUsedThisSession)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
		iOldCheatStateVariable = g_iBitsetCheatsUsedThisSession
		g_iBitsetCheatsUsedThisSession = 0	
	#ENDIF
	
//	SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
//	SPECIAL_ABILITY_RESET(PLAYER_ID())
	
	ADD_RELATIONSHIP_GROUP("RamageGrp", activeGroup)
	ADD_RELATIONSHIP_GROUP("PassiveGrp", activePassiveGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, activeGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, activeGroup, RELGROUPHASH_PLAYER)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, activePassiveGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, activePassiveGroup, RELGROUPHASH_PLAYER)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, activePassiveGroup, activeGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, activeGroup, activePassiveGroup)
	
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_FAMILY)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_MEXICAN)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)

    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_AMBIENT_GANG_FAMILY, RELGROUPHASH_PLAYER)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_AMBIENT_GANG_MEXICAN, RELGROUPHASH_PLAYER)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_AMBIENT_GANG_LOST, RELGROUPHASH_PLAYER)

	SET_PLAYER_CAN_BE_HASSLED_BY_GANGS(PLAYER_ID(), FALSE)


	// turn off ambient things and cellphones
	DISABLE_CELLPHONE(TRUE)
	ENABLE_ALL_DISPATCH_SERVICES(FALSE)
	
	SUPPRESS_PICKUP_REWARD_TYPE(PICKUP_REWARD_TYPE_ALL, TRUE)
	STOP_ANY_PED_MODEL_BEING_SUPPRESSED()	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), fHPRegenMult)
	
	// turn off debug lines - just incase i forget
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	// clear wanted levels and cars and vehicles and stop scenario peds from messing about
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		SET_MAX_WANTED_LEVEL(0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
	ENDIF	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	// fill index values
	i = 0
	REPEAT COUNT_OF(rampagePed) i 
		rampagePed[i].iIndex = i 
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(rampageCar) i 
		rampageCar[i].iIndex = i 
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		START_RAMPAGE_DEBUG_WIDGET_GROUP()
	#ENDIF
ENDPROC

/// PURPOSE:
///    Cleanups the rampage and shuts down everything
/// PARAMS:
///    bPlayerDied - Call this if the player died
PROC RAMPAGE_CLEANUP(BOOL bPlayerDead = FALSE)
	
	INT i 
	WEAPON_TYPE currentWeapon
	
	// mark us as not being on rampage
	STOP_STREAM()
	IF (bPlayerDead)
		TRIGGER_MUSIC_EVENT("RAMPAGE_FAIL")
	ENDIF
	CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - START")
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAudioScene)
		IF IS_AUDIO_SCENE_ACTIVE(sAudioScene)
			STOP_AUDIO_SCENE(sAudioScene)
			CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - AUDIO SCENE STOPED")
		ENDIF
	ENDIF
	
	#IF USE_TU_CHANGES
		CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - RE ENABLING ANY CHEATS WHICH WERE ACTIVE BEFORE")
		CPRINTLN(DEBUG_RAMPAGE, "                    RESTORING CHEAT VARIABLE:", iOldCheatStateVariable)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
		g_iBitsetCheatsUsedThisSession = iOldCheatStateVariable
	#ENDIF

	STOP_RAMPAGE_FX()
//	CLEAR_TIMECYCLE_MODIFIER()
	
	STOP_SOUND(i_sound_countdown)
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2
		RELEASE_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
	ENDIF
	
	FORCE_REMOVE_ALL_BLIPS()
	RESET_PLAYER_RAMPAGE_INFINITE_AMMO()
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	#IF IS_DEBUG_BUILD
		CLEANUP_RAMPAGE_DEBUG_WIDGETS()
	#ENDIF
	
	SAFE_REMOVE_BLIP(battleBlip)
	SAFE_RELEASE_OBJECT(plyrGunObject)
	plyrGunObject = NULL
	
	// restore weapons - make him drop weapon when he dies
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID())
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeapon)
			plyrGunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(currentWeapon)
			SAFE_RELEASE_OBJECT(plyrGunObject)
			plyrGunObject = NULL
		ENDIF
	ENDIF
	
	IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
//	SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	Restore_Game_State_Snapshot_Weapons(g_startSnapshot)
	
	IF (NOT bPlayerDead)
		RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
	ENDIF
	
	IF (rampageInfo.bPassed = TRUE)
		REPEAT COUNT_OF(rampageInfo.plyrWeapon) i 
			IF (rampageInfo.plyrWeapon[i] <> WEAPONTYPE_INVALID) AND (rampageInfo.plyrWeapon[i] <> WEAPONTYPE_UNARMED)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), rampageInfo.plyrWeapon[i], GET_WEAPON_CLIP_SIZE(rampageInfo.plyrWeapon[i]) * 3, TRUE, TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	
	SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE )
	
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 1.0)
	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 1.0)
	SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), 1.0)
	
	// unload scaleform
	IF (sFormRampage <> NULL)
		CALL_SCALEFORM_MOVIE_METHOD(sFormRampage, "RESET_MOVIE")
//		DRAW_SCALEFORM_MOVIE(sFormRampage, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sFormRampage)
		sFormRampage = NULL
	ENDIF
	
	// unload scaleform
	IF (sFormRampageStats <> NULL)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sFormRampageStats)
		sFormRampageStats = NULL
	ENDIF
	
	// unload scaleform
	IF (sFormRampagePassMsg <> NULL)
		CALL_SCALEFORM_MOVIE_METHOD(sFormRampagePassMsg, "RESET_MOVIE")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sFormRampagePassMsg)
		sFormRampagePassMsg = NULL
	ENDIF
	
	//UNLOAD_LEADIN_ANIMDICT(rampageLeadIn)
	
	//cleanup entities
	CLEANUP_RAMPAGE_CAR_ARRAY(rampageCar)
	FLEE_RAMPAGE_PED_ARRAY(rampagePed)
	CLEANUP_RAMPAGE_TANK(rampageTank)
	CLEANUP_COVER_CARS()
	CLEANUP_EXPLOSIVE_PROPS()
	
	// restore sceanrios
	UNBLOCK_SCENARIOS_AND_AMBIENT(worldScenarioBlockID)
	IF (scenarioBlockID <> NULL)
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockID)
	ENDIF
	
	RESET_SCENARIO_GROUPS_ENABLED()
	// restore ambient stuff
	REMOVE_RELATIONSHIP_GROUP(activeGroup)
	REMOVE_RELATIONSHIP_GROUP(activePassiveGroup)
	CLEAR_ALL_PICKUP_REWARD_TYPE_SUPPRESSION()
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_DISPATCH_COPS_FOR_PLAYER(GET_PLAYER_INDEX(), TRUE)
	
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENABLE_AMBIENT_PEDS_AND_VEHICLES()
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	DISABLE_CELLPHONE(FALSE)
	
	SET_PLAYER_CAN_BE_HASSLED_BY_GANGS(PLAYER_ID(), TRUE)

	// handle passing completion percentage
	IF (rampageInfo.bPassed)
		SWITCH (rampageInfo.blipNameEnum)
		
			// Rednecks
			CASE STATIC_BLIP_MINIGAME_RAMPAGE1 
				Random_Character_Passed(CP_OJ_RAM1)
			BREAK

			// Ballers
			CASE STATIC_BLIP_MINIGAME_RAMPAGE3
				Random_Character_Passed(CP_OJ_RAM3)
			BREAK
			
			// Vagos
			CASE STATIC_BLIP_MINIGAME_RAMPAGE4
				Random_Character_Passed(CP_OJ_RAM4)
			BREAK
			
			// Army
			CASE STATIC_BLIP_MINIGAME_RAMPAGE5
				Random_Character_Passed(CP_OJ_RAM5)
			BREAK
			
			// Hipsters
			CASE STATIC_BLIP_MINIGAME_RAMPAGE2 
				//IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)
					CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - ALL RAMPAGES CLEAR") 
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED, TRUE)
					AWARD_ACHIEVEMENT(ACH18)	// Award achievement - Blast from the Past
				//ENDIF
				Random_Character_Passed(CP_OJ_RAM2)
			BREAK
			
			DEFAULT	
				SCRIPT_ASSERT("UNDEFINED RAMPAGE - CAN'T REGISTER 100%")
			BREAK
		ENDSWITCH
		
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)
			CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - ALL RAMPAGES UNLOCKED, REACTIVATING WORLD BRAIN")
			AWARD_ACHIEVEMENT(ACH18)	// Award achievement - Blast from the Past
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("launcher_rampage")
		ENDIF
	ENDIF
	
	IF (Random_Character_Cleanup_If_Triggered())
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_RAMPAGE, "Random Character Script was triggered so additional cleanup required")
		#ENDIF
	ENDIF
	
	// Clean up entities and release models
	g_bIsOnRampage = FALSE
	g_bCheckSolidGoldBabyAchievement = TRUE
	RC_CleanupSceneEntities(rampageScriptArgs, FALSE, FALSE)
	RELEASE_RAMPAGE_INFO()
	
	BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_RAMCOM )			//#1514495 stockmarket
	
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	CUTSCENE FUNCTIONS
//----------------------

/// PURPOSE:
///    Tells us the name of the intro cutscene
///    We need this just in case people try to use the z menu in a replay
PROC GET_NAME_OF_RAMPAGE_INTRO_CUTSCENE()
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			rampageScriptArgs.sIntroCutscene = "TRVRAM_1"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			rampageScriptArgs.sIntroCutscene = "TRVRAM_2_CONCAT"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			rampageScriptArgs.sIntroCutscene = "TRVRAM_3"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			rampageScriptArgs.sIntroCutscene = "TRVRAM_4"
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			rampageScriptArgs.sIntroCutscene = "TRVRAM_5_CON"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Rampage does not have an intial cutscene")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Registers entities we got from the script args with the mocaps
PROC REGISTER_INTRO_CUTSCENE_ENTITIES()

	MODEL_NAMES weaponModel
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		sPlayerSceneHandle = "Trevor"
		CPRINTLN(DEBUG_RAMPAGE, "REGISTER_INTRO_CUTSCENE_ENTITIES - PLAYER REGISTERED")
		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sPlayerSceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(PLAYER_PED_ID()),CEO_PRESERVE_FACE_BLOOD_DAMAGE|CEO_PRESERVE_BODY_BLOOD_DAMAGE)
	ENDIF
			
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "TriggerHappy_WhiteTrash"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[0], " - PED: 0")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[0], sSceneHandles[0], CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Nervous_WhiteTrash"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[1], " - PED: 1")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[1], sSceneHandles[1], CU_ANIMATE_EXISTING_SCRIPT_ENTITY) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))
			ENDIF	
			
			sWeaponSceneHandle = "Trevors_weapon"
			weaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[3])
				FREEZE_ENTITY_POSITION( rampageScriptArgs.objID[3], FALSE )
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[4])
				FREEZE_ENTITY_POSITION( rampageScriptArgs.objID[4], FALSE )
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Rampage_Hipster"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[0], " - PED: 0")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[0], sSceneHandles[0], CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[1])
				SAFE_DELETE_OBJECT(rampageScriptArgs.objID[1])
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[2])
				FREEZE_ENTITY_POSITION( rampageScriptArgs.objID[2], FALSE )
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[3])
				FREEZE_ENTITY_POSITION( rampageScriptArgs.objID[3], FALSE )
			ENDIF
			
			sWeaponSceneHandle = "Trevors_weapon"
			weaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_SAWNOFFSHOTGUN)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Armed_Mexican_Goon"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[0], " - PED: 0")  
				SET_PED_COMPONENT_VARIATION(rampageScriptArgs.pedID[0], PED_COMP_SPECIAL2, 0, 0)  //To fix dupe gun from B*2015853 - remove gun before registering for the cutsene 
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[0], sSceneHandles[0], CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))	
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Mexican_Goon"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[1], " - PED: 1")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[1], sSceneHandles[1], CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))
			ENDIF
			
			sWeaponSceneHandle = "Trevors_weapon"
			weaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_MICROSMG)
			
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Thug"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[0], " - PED: 0")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[0], sSceneHandles[0], CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Armed_Thug"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[1], " - PED: 1")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[1], sSceneHandles[1], CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]), CEO_PRESERVE_FACE_BLOOD_DAMAGE|CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_CLONE_DAMAGE_TO_CS_MODEL)
			ENDIF
			
			sWeaponSceneHandle = "Trevors_weapon"
			weaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)
			
			// we're now creating a weapon in the lead-in
			// was cleaner to just delete it and recreate here
			// @bsw 13/7/2013
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[0])	
				DELETE_OBJECT(rampageScriptArgs.objID[0])
			ENDIF
			
			IF NOT IS_ENTITY_ALIVE(rampageScriptArgs.objID[0])	
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER_INTRO_CUTSCENE_ENTITIES - PLAYER WEAPON REGISTERED - RAMPAGE 4 - CREATING NEW WEAPON")
				rampageScriptArgs.objID[0] = CREATE_WEAPON_OBJECT(WEAPONTYPE_ASSAULTRIFLE, 100, GET_ENTITY_COORDS(rampageScriptArgs.pedID[0]), TRUE)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[0])	
					CPRINTLN(DEBUG_RAMPAGE, "REGISTER_INTRO_CUTSCENE_ENTITIES - PLAYER WEAPON CREATED - RAMPAGE 4 - CREATING NEW WEAPON")
				ENDIF
			ENDIF	
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Army_Guy_Right"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[0], " - PED: 0")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[0], sSceneHandles[0], CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Army_Guy_Left"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[1], " - PED: 1")
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.pedID[1], sSceneHandles[1], CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.vehID[0])
				sSceneHandles[2] = "Army_Truck"
				CPRINTLN(DEBUG_RAMPAGE, "REGISTER ", sSceneHandles[2], " - VEHICLE")
				SET_VEHICLE_EXTRA(rampageScriptArgs.vehID[0], 1, TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.vehID[0], sSceneHandles[2], CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, GET_ENTITY_MODEL(rampageScriptArgs.vehID[0]))
			ENDIF

			
			sWeaponSceneHandle = "Trevors_weapon"	
			weaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_GRENADELAUNCHER)
		BREAK
	ENDSWITCH
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sWeaponSceneHandle)
		IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[0])	
			CPRINTLN(DEBUG_RAMPAGE, "REGISTER_INTRO_CUTSCENE_ENTITIES - PLAYER WEAPON REGISTERED - USING EXISTING OBJECT")
			REGISTER_ENTITY_FOR_CUTSCENE(rampageScriptArgs.objID[0], sWeaponSceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		ELSE
			CPRINTLN(DEBUG_RAMPAGE, "REGISTER_INTRO_CUTSCENE_ENTITIES - PLAYER WEAPON REGISTERED - USING NEW OBJECT")
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, sWeaponSceneHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, weaponModel)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates registered entities we got from the script args with the mocaps
PROC UPDATE_REGISTERED_INTRO_CUTSCENE_EXIT_STATE()

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sPlayerSceneHandle)
			CPRINTLN(DEBUG_RAMPAGE, "PLAYER EXIT STATE SET")
			set_ped_using_action_mode(PLAYER_PED_ID(),true,-1,"TREVOR_ACTION")
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ACTIONMODE_IDLE,FALSE,FAUS_CUTSCENE_EXIT)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
			TASK_PLAY_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sIntroAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT )
			TRIGGER_RAMPAGE_FX()
			bPlayerExitStateSet = TRUE
		ENDIF
	ENDIF
	
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "TriggerHappy_WhiteTrash"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[0]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[0], <<930.2522, 3681.3391, 30.6506>>) // GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(rampagePed[ind].pedID, <<0.0, 100.0, 0.0>>))
					FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[0], MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[0], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Nervous_WhiteTrash"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[1]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[1],<<920.2722, 3546.1018, 32.9231>>)// <<1049.8945, 3575.0146, 32.9901>>)
	               	FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[1], MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[1], PEDMOVE_RUN)
	            ENDIF
			ENDIF	
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Rampage_Hipster"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[0]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))		
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[0], <<1212.5776, -421.3977, 66.5951>>, TRUE)
	                FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[0], MS_ON_FOOT_SPRINT, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[0], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Armed_Mexican_Goon"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[0])//, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
					SET_PED_COMPONENT_VARIATION(rampageScriptArgs.pedID[0], PED_COMP_SPECIAL, 0, 0)
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[0], <<335.7316, -1887.2788, 24.8253>>)	
	                FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[0], MS_ON_FOOT_SPRINT, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[0], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Mexican_Goon"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[1]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))

					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[1], <<408.9685, -1923.8516, 24.0339>>)	
	                FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[1], MS_ON_FOOT_SPRINT, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[1], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Thug"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[0]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[0], <<-110.74, -1601.2, 31.7>>, FALSE)
	                FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[0], MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[0], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Armed_Thug"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[1]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[1], <<-142.6, -1713.4, 30.1>>, TRUE)//, TRUE)	
	               // FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[1], MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
	               // SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[1], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				sSceneHandles[0] = "Army_Guy_Left"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[0]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[0]))
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[0], <<-1209.9448, 2598.6895, 14.3906>>)
	               	FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[0], MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
	               	SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[0], PEDMOVE_RUN)
	            ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
				sSceneHandles[1] = "Army_Guy_Right"
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[1]) //, GET_ENTITY_MODEL(rampageScriptArgs.pedID[1]))
					RAMPAGE_SMART_FLEE_HACKERY(rampageScriptArgs.pedID[1], <<-1222.5686, 2587.6506, 15.1474>>)
	               	FORCE_PED_MOTION_STATE(rampageScriptArgs.pedID[1], MS_ON_FOOT_SPRINT, FALSE, FAUS_CUTSCENE_EXIT)
	                SET_PED_MIN_MOVE_BLEND_RATIO(rampageScriptArgs.pedID[1], PEDMOVE_SPRINT)
	            ENDIF
			ENDIF
			
			
			IF IS_ENTITY_ALIVE(rampageScriptArgs.vehID[0])
				IF CAN_SET_ENTER_STATE_FOR_REGISTERED_ENTITY(sSceneHandles[2])
					SET_VEHICLE_EXTRA(rampageScriptArgs.vehID[0], 1, FALSE)
					CPRINTLN(DEBUG_RAMPAGE, "VEHICLE ENTER STATE SET")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

	IF NOT IS_ENTITY_ALIVE(rampageScriptArgs.objID[0])
		IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sWeaponSceneHandle))
			rampageScriptArgs.objID[0] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sWeaponSceneHandle))
			IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[0])
				CPRINTLN(DEBUG_RAMPAGE, "PLAYER WEAPON CREATED")
			ENDIF
		ENDIF
	ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sWeaponSceneHandle) //, GET_ENTITY_MODEL(rampageScriptArgs.objID[0]))
		// @SBA - fix an issue with rampageScriptArgs.objID[0] not working when the corresponding
		// player weapon had no ammo (rampage 4 only)
		IF rampageScriptArgs.eMissionID = RC_RAMPAGE_4
			CPRINTLN(DEBUG_RAMPAGE, "PLAYER WEAPON EXIT STATE SET: Rampage 4")
			SAFE_DELETE_OBJECT(rampageScriptArgs.objID[0])
			GIVE_PLAYER_RAMPAGE_WEAPONS()
		ELSE
			CPRINTLN(DEBUG_RAMPAGE, "PLAYER WEAPON EXIT STATE SET")
			GIVE_WEAPON_OBJECT_TO_PED(rampageScriptArgs.objID[0], PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	// To fix B*2015853, yeah it's a bit of a bodge.
	IF rampageScriptArgs.eMissionID = RC_RAMPAGE_3
		IF GET_CUTSCENE_TIME() >= 23000	
			IF NOT bRampage3WeaponBodge	
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), rampageInfo.plyrWeapon[0], INFINITE_AMMO , TRUE, TRUE)
				INT i = 0
				REPEAT COUNT_OF(rampageInfo.plyrWeaponComp) i 
					IF (rampageInfo.plyrWeaponComp[i] <> WEAPONCOMPONENT_INVALID)
						GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), rampageInfo.plyrWeapon[0], rampageInfo.plyrWeaponComp[i])
					ENDIF
				ENDREPEAT
				bRampage3WeaponBodge = TRUE
			ENDIF
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    This places the player and any cars or peds we want there immediately after the mocap
PROC SETUP_INITIAL_PEDS()
	INT ind 
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			//@RJP set up cars to move in to attack player
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[0], <<930.9605, 3555.5437, 32.9354>>, 354.1474, TRUE, 1)
					SET_RAMPAGE_CAR_DEPLOY_TO_COORDINATES( rampageCar[ind], <<911.7063, 3625.5491, 31.3674>>, 40 )
//					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 40)
				ENDIF
			ENDIF
		BREAK
		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			/* setup some peds in combat positions */
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], <<1175.0933, -417.2925, 66.3285>>, 265.3943)
			ENDIF
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], <<1170.4524, -425.4240, 66.0957>>, 300.0205)
			ENDIF
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], <<1171.6316, -423.4090, 66.0957>>, 300.2360)
			ENDIF
			
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[2], <<1169.7002, -366.0976, 66.9855>>, 256.6603, TRUE, 1)
					SET_RAMPAGE_CAR_DEPLOY_TO_COORDINATES( rampageCar[ind], <<1188.0450, -407.0416, 66.8409>>, 30 )
//					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 30)
				ENDIF
			ENDIF
			
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[2], <<1154.9923, -474.4909, 65.5083>>, 245.5358, TRUE, 1)
					SET_RAMPAGE_CAR_DEPLOY_TO_COORDINATES( rampageCar[ind], <<1192.1339, -400.4286, 66.8566>>, 30 )
//					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 30)
				ENDIF
			ENDIF
		BREAK
		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			//@RJP set up cars to move in to attack player
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[0], <<405.9012, -1916.1853, 24.1725>>, 48.5025, TRUE, 1)
					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 40)
					SET_RAMPAGE_CAR_RAISED_COLLISION(rampageCar[ind])
				ENDIF
			ENDIF
			
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[1], <<351.6271, -1906.6952, 23.7255>>, 221.0698, TRUE, 1)
					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 40)
					SET_RAMPAGE_CAR_RAISED_COLLISION(rampageCar[ind])
				ENDIF
			ENDIF
		BREAK
		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			// setup some peds in combat positions
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				IF CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], <<-148.0109, -1632.1240, 32.0708>>, 230.0827)
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), <<0, 0, 0>>, 30.0)
				ENDIF
			ENDIF
			
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				IF CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], <<-122.8691, -1634.9785, 31.1487>>, 42.2112)
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), <<0, 0, 0>>, 30.0)
				ENDIF
			ENDIF
			
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				IF CREATE_RAMPAGER_PED_AT_POSITION(rampagePed[ind], <<-134.9283, -1614.3149, 31.6326>>, 243.3803)
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), <<0, 0, 0>>, 30.0)
					TASK_SEEK_COVER_FROM_PED(rampagePed[ind].pedID, PLAYER_PED_ID(), -1, TRUE)
				ENDIF
			ENDIF
		BREAK 
		DEFAULT
		BREAK
	ENDSWITCH
ENDPROC

PROC REPLAY_PLAYER_POSITION()
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<908.7723, 3641.4531, 31.4889>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 270.3831)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<1185.7426, -400.5164, 66.9606>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 159.1354)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<463.9719, -1848.9276, 26.8640>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 135.7623)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<-162.40, -1668.70, 33.14>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), -45.22)
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<-1295.7534, 2507.1167, 20.2824>>) 
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 336.2132) 
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    This places the player and any cars or peds we want there immediately after the mocap
PROC POST_INTRO_CUTSCENE_SETUP(BOOL bSkipped = FALSE)
	INT n 
	PED_INDEX ped
	INT ind0, ind1, ind
	BOOL bCreateInSwitch = FALSE 
	
	ENABLE_AMBIENT_PEDS_AND_VEHICLES(FALSE, FALSE, TRUE, TRUE)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(rampageInfo.fAmbientCarDensity) 
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(rampageInfo.fAmbientPedDensity)
	CPRINTLN(DEBUG_RAMPAGE, "POST_INTRO_CUTSCENE_SETUP - SKIPPED:", bSkipped)
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			
			// setup fleeing peds if they haven't been setup
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind0)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], rampageScriptArgs.pedID[0], TRUE, FALSE, FALSE)
					//SET_ENTITY_HEADING(rampageScriptArgs.pedID[0], -68.55)
					CPRINTLN(DEBUG_RAMPAGE, "GRABBED SCRIPT ARGS PED 0 - NEW INDEX:", ind0)
				ELSE
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_HIC, <<914.678894,3638.753174,31.896519>>, -68.557541) 
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<914.678894,3638.753174,31.896519>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], ped, TRUE, FALSE, FALSE)
					CPRINTLN(DEBUG_RAMPAGE, "CREATED FAKED ARGS PED 0 - NEW INDEX:", ind0)
				ENDIF
			ENDIF
					
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind1)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], rampageScriptArgs.pedID[1], TRUE, FALSE, FALSE)
					//SET_ENTITY_HEADING(rampageScriptArgs.pedID[1], -143.115036)
					CPRINTLN(DEBUG_RAMPAGE, "GRABBED SCRIPT ARGS PED 1 - NEW INDEX:", ind1)
				ELSE
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_HIC, <<915.581360,3637.608154,31.912495>>, -143.115036)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<915.581360,3637.608154,31.912495>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], ped, TRUE, FALSE, FALSE)
					CPRINTLN(DEBUG_RAMPAGE, "CREATED FAKED ARGS PED 1 - NEW INDEX:", ind1)
				ENDIF
			ENDIF
			
			IF (bSkipped = TRUE)
				CPRINTLN(DEBUG_RAMPAGE, "CUTSCENE SKIPPED")
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), rampageInfo.vPlyrStartPos) 
					SET_ENTITY_HEADING(PLAYER_PED_ID(), rampageInfo.fPlyrStartHeading) 
				ENDIF
				
				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind0].pedID, <<926.0913, 3639.4471,32.4406>>)
				SET_ENTITY_HEADING(rampagePed[ind0].pedID, RAD_TO_DEG(-1.38))
				
				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind1].pedID, <<923.4156, 3632.4092,32.4944>>)
				SET_ENTITY_HEADING(rampagePed[ind1].pedID, -143.115036)
				
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind0].pedID, <<1007.1674, 3634.4473, 31.5345>>) 
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind1].pedID, <<1049.8945, 3575.0146, 32.9901>>)
			ENDIF
			
			bCreateInSwitch = TRUE
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
		
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind0)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], rampageScriptArgs.pedID[0], TRUE, FALSE, FALSE)
				ELSE
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_HIPSTER, <<1186.44, -407.79, 67.72>>, RAD_TO_DEG(-2.04)) 
					SET_PED_DEFAULT_COMPONENT_VARIATION(ped)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<1186.44, -407.79, 67.72>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], ped, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
	
			IF (bSkipped = TRUE)
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), rampageInfo.vPlyrStartPos) 
					SET_ENTITY_HEADING(PLAYER_PED_ID(), rampageInfo.fPlyrStartHeading) 
				ENDIF
				
				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind0].pedID, <<1186.44, -407.79, 67.72>>)
				SET_ENTITY_HEADING(rampagePed[ind0].pedID, RAD_TO_DEG(-2.04)) 
				
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind0].pedID, <<1212.0, -421.3, 67.6>>)		
			ENDIF
			
			bCreateInSwitch = TRUE	
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
		
			// setup peds if they haven't been setup
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind0)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], rampageScriptArgs.pedID[0], TRUE, FALSE, FALSE)
				ELSE
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_MEX, <<454.1726, -1851.3158, 26.9211>>, 131.2600) 
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<454.1726, -1851.3158, 26.9211>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], ped, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
			
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind1)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], rampageScriptArgs.pedID[1], TRUE, FALSE, FALSE)
				ELSE 
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_MEX, <<457.3105, -1854.0668, 26.8747>>, 128.3050) 
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 1)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<457.3105, -1854.0668, 26.8747>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], ped, TRUE, FALSE, FALSE)
				ENDIF	
			ENDIF
			
			IF (bSkipped = TRUE)
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), rampageInfo.vPlyrStartPos) 
					SET_ENTITY_HEADING(PLAYER_PED_ID(), rampageInfo.fPlyrStartHeading) 
				ENDIF

				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind0].pedID, <<454.1726, -1851.3158, 26.9211>>)
				SET_ENTITY_HEADING(rampagePed[ind0].pedID, 131.2600) 
				
				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind1].pedID, <<457.3105, -1854.0668, 26.8747>>)
				SET_ENTITY_HEADING(rampagePed[ind1].pedID, 128.3050) 
				
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind1].pedID, <<408.9685, -1923.8516, 24.0339>>)	
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind0].pedID, <<335.7316, -1887.2788, 24.8253>>)	
			ENDIF
			
	
			bCreateInSwitch = TRUE	
		BREAK
		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			CLEAR_AREA_OF_OBJECTS(<<-152.6, -1665.1, 32.9>>, 1.0)
			CLEAR_AREA_OF_OBJECTS(<<-132.7, -1615.8, 32.6>>, 1.0)
			
			// this guy does not have the gun
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind0)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], rampageScriptArgs.pedID[0], TRUE, FALSE, FALSE)
				ELSE 
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_GANG,  <<-156.00, -1660.82, 32.88>>, -14.52) 
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 1)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 1)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<-156.90, -1662.03, 32.84>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], ped, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
			
			// setup peds if they haven't been setup - he ends up on the floor
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind1)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
					//SET_ENTITY_COORDS_GROUNDED(rampageScriptArgs.pedID[1], <<-164.50, -1672.90, 32.22>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], rampageScriptArgs.pedID[1], TRUE, FALSE, FALSE)
				ELSE 
					ped = CREATE_PED(PEDTYPE_MISSION, IG_RAMP_GANG,  <<-163.45, -1671.12, 32.38>>, -20.36) 
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 1)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					SET_ENTITY_COORDS_GROUNDED(ped, <<-163.45, -1671.12, 32.38>>)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], ped, TRUE, FALSE, FALSE)
				ENDIF	
			ENDIF
			 
			IF (bSkipped = TRUE)
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), rampageInfo.vPlyrStartPos) 
					SET_ENTITY_HEADING(PLAYER_PED_ID(), rampageInfo.fPlyrStartHeading) 
				ENDIF
				
				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind0].pedID, <<-156.90, -1662.03, 32.84>>)
				SET_ENTITY_HEADING(rampagePed[ind0].pedID, -14.52) 			
				// B*1034387
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind0].pedID, <<-110.74, -1601.2, 31.7>>, FALSE)	
				
				SET_ENTITY_COORDS_GROUNDED(rampagePed[ind1].pedID, <<-164.50, -1672.90, 32.22>>)
				SET_ENTITY_HEADING(rampagePed[ind1].pedID, -20.36) 
				RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind1].pedID, <<-142.6, -1713.4, 30.1>>, TRUE)//, TRUE)
			ENDIF
				
			bCreateInSwitch = TRUE	
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
		
			IF IS_ENTITY_ALIVE(rampageScriptArgs.vehID[0])
				FREEZE_ENTITY_POSITION(rampageScriptArgs.vehID[0], FALSE)
			ENDIF
			
			// setup fleeing peds if they haven't been setup
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind0)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], rampageScriptArgs.pedID[0], TRUE, FALSE, FALSE)
					//SET_ENTITY_HEADING(rampageScriptArgs.pedID[0], RAD_TO_DEG(-1.35))
				ELSE
					ped = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-1299.23, 2510.35, 21.26>>, RAD_TO_DEG(-1.31))
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					CLEAR_PED_PROP(ped, ANCHOR_HEAD)
					CLEAR_PED_PROP(ped, ANCHOR_EYES)
					CLEAR_PED_PROP(ped, ANCHOR_EARS)
					CLEAR_PED_PROP(ped, ANCHOR_MOUTH)
					CLEAR_PED_PROP(ped, ANCHOR_LEFT_HAND)
					CLEAR_PED_PROP(ped, ANCHOR_RIGHT_HAND)
					CLEAR_PED_PROP(ped, ANCHOR_LEFT_WRIST)
					CLEAR_PED_PROP(ped, ANCHOR_RIGHT_WRIST)
					CLEAR_PED_PROP(ped, ANCHOR_HIP)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind0], ped, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
					
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind1)
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[1])
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], rampageScriptArgs.pedID[1], TRUE, FALSE, FALSE)
					SET_ENTITY_HEADING(rampagePed[ind1].pedID, 8.44)
				ELSE
					ped = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_03, <<-1298.69, 2511.35, 21.17>>, RAD_TO_DEG(-1.42))
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 0, 0)
					CLEAR_PED_PROP(ped, ANCHOR_HEAD)
					CLEAR_PED_PROP(ped, ANCHOR_EYES)
					CLEAR_PED_PROP(ped, ANCHOR_EARS)
					CLEAR_PED_PROP(ped, ANCHOR_MOUTH)
					CLEAR_PED_PROP(ped, ANCHOR_LEFT_HAND)
					CLEAR_PED_PROP(ped, ANCHOR_RIGHT_HAND)
					CLEAR_PED_PROP(ped, ANCHOR_LEFT_WRIST)
					CLEAR_PED_PROP(ped, ANCHOR_RIGHT_WRIST)
					CLEAR_PED_PROP(ped, ANCHOR_HIP)
					CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind1], ped, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
				
			IF (bSkipped = TRUE)
				
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), rampageInfo.vPlyrStartPos) 
					SET_ENTITY_HEADING(PLAYER_PED_ID(), rampageInfo.fPlyrStartHeading) 
				ENDIF
	
				IF IS_ENTITY_ALIVE(rampagePed[ind0].pedID)
					CPRINTLN(DEBUG_RAMPAGE, "ENTITIES 0 REPOSITION")
					SET_ENTITY_COORDS_GROUNDED(rampagePed[ind0].pedID, <<-1288.9551, 2529.9951, 19.52>>) 
					SET_ENTITY_HEADING(rampagePed[ind0].pedID, RAD_TO_DEG(-0.83)) 
					RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind0].pedID, <<-1222.5686, 2587.6506, 15.1474>>)
				ENDIF
				
				IF IS_ENTITY_ALIVE(rampagePed[ind1].pedID)
					CPRINTLN(DEBUG_RAMPAGE, "ENTITIES 1 REPOSITION")
					SET_ENTITY_COORDS_GROUNDED(rampagePed[ind1].pedID, <<-1284.7990, 2535.8208, 18.7709>>) 
					SET_ENTITY_HEADING(rampagePed[ind1].pedID, RAD_TO_DEG(-0.15)) 
					RAMPAGE_SMART_FLEE_HACKERY(rampagePed[ind1].pedID, <<-1222.5686, 2587.6506, 15.1474>>)
				ENDIF
			ENDIF
						
			// setup some cars ready
			IF GET_FREE_RAMPAGE_CAR_INDEX(ind)
				IF CREATE_RAMPAGE_CAR(rampageCar[ind], rampageInfo.carModels[0], <<-1433.5477, 2650.1660, 16.6436>>, 228.0205, TRUE, 2)
					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 40.0)
					CPRINTLN(DEBUG_RAMPAGE, "AFTER CUTSCENE CAR 0 CREATED")
				ENDIF
			ENDIF
		
			IF GET_FREE_RAMPAGE_CAR_INDEX(ind)
				IF CREATE_RAMPAGE_CAR(rampageCar[ind], rampageInfo.carModels[0], <<-1478.0034, 2688.8289, 16.6436>>, 227.7704, TRUE, 2)
					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 40.0)
					CPRINTLN(DEBUG_RAMPAGE, "AFTER CUTSCENE CAR 1 CREATED")
				ENDIF
			ENDIF
			bCreateInSwitch = TRUE	
		BREAK
	ENDSWITCH
	
	GIVE_PLAYER_RAMPAGE_WEAPONS()
	//SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_SPRINT, 500)
	
	IF (bCreateInSwitch = TRUE)
		EXIT 
	ENDIF
	
	// grab any peds in the cutscene tell them to flee 
	// so the player has a first target and so they don't go into attack mode straight off
	// as it isn't fair if people are clearly behind the player on startup
	REPEAT COUNT_OF(rampageScriptArgs.pedID) n
		IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[n])
			IF GET_FREE_RAMPAGE_PED_INDEX(rampagePed, ind)
				CREATE_RAMPAGER_PED_FROM_PED_INDEX(rampagePed[ind], rampageScriptArgs.pedID[n], TRUE, FALSE, FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SET_GAMEPLAY_INTERPOLATE_FROM_INTRO()
	CAMERA_INDEX cam
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<907.5499, 3641.5085, 33.1808>>, <<-11.3754, 0.0004, -105.7755>>, 50.0000, TRUE)
		BREAK
		DEFAULT
			cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vFinalCamCoord, vFinalCamRot, 50.0000, TRUE)
		BREAK
	ENDSWITCH
	
	IF DOES_CAM_EXIST(cam)
		SET_CAM_ACTIVE(cam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		WAIT(0)
		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
		//RENDER_SCRIPT_CAMS(FALSE, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the intro
/// RETURNS:
///    Returns true when intro is finished
FUNC BOOL UPDATE_INTRO_CUTSCENE()
	
	VECTOR plyrPos 
	
	SWITCH (stageProgress)
		CASE SP_LOADING
		
			#IF IS_DEBUG_BUILD
				IF (bDebugShowTTY)
					CPRINTLN(DEBUG_RAMPAGE, "Requesting Cutscene ", rampageScriptArgs.sIntroCutscene)
				ENDIF
			#ENDIF
		
			CPRINTLN(DEBUG_RAMPAGE, "Loading Cutscene:", rampageScriptArgs.sIntroCutscene, " Game Time:", GET_GAME_TIMER())
			RC_TakeEntityOwnership(rampageScriptArgs)
			CPRINTLN(DEBUG_RAMPAGE, "Owner Ship Passed Across")
			
			IF NOT HAS_CUTSCENE_LOADED()
				RC_REQUEST_CUTSCENE(rampageScriptArgs.sIntroCutscene)
				PRESTREAM_RAMPAGE_INTRO_CUTSCENE_PED_VARIATIONS(rampageScriptArgs, sSceneHandles)
				
				// gun and holster
				IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE3
					IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
						SET_PED_COMPONENT_VARIATION(rampageScriptArgs.pedID[0], PED_COMP_SPECIAL, 0, 0)
						SET_PED_COMPONENT_VARIATION(rampageScriptArgs.pedID[0], PED_COMP_SPECIAL2, 1, 0)
					ENDIF
				ENDIF
			ENDIF
			
			
			stageProgress = SP_SETUP
		BREAK
		CASE SP_SETUP
			PRESTREAM_RAMPAGE_INTRO_CUTSCENE_PED_VARIATIONS(rampageScriptArgs, sSceneHandles)
			IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE3
				IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
					SET_PED_COMPONENT_VARIATION(rampageScriptArgs.pedID[0], PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(rampageScriptArgs.pedID[0], PED_COMP_SPECIAL2, 1, 0)
				ENDIF			
			ENDIF
			
			bPlayerExitStateSet = FALSE
			IF RC_IS_CUTSCENE_OK_TO_START()	
			
				#IF IS_DEBUG_BUILD
					IF (bDebugShowTTY)
						CPRINTLN(DEBUG_RAMPAGE, "Playing Cutscene ", rampageScriptArgs.sIntroCutscene)
					ENDIF
				#ENDIF
				
				/*In rampage2, request fake cell phone movie
				IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2
					REQUEST_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
					WHILE NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED( sFakeCellphoneData )
						WAIT(0)
					ENDWHILE
				ENDIF
				*/
				
				TASK_CLEAR_LOOK_AT( PLAYER_PED_ID() )
				
				CPRINTLN(DEBUG_RAMPAGE, "Starting Cutscene:", rampageScriptArgs.sIntroCutscene, " Game Time:", GET_GAME_TIMER())
				plyrPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				REQUEST_ACTION_MODE_ASSET("TREVOR_ACTION")
				REGISTER_INTRO_CUTSCENE_ENTITIES()
				SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
				RC_CLEANUP_LAUNCHER()
				START_CUTSCENE()
				//WAIT(0)
				
				UNLOAD_LEADIN_ANIMDICT(rampageLeadIn)
				
				CPRINTLN(DEBUG_RAMPAGE, "Cleared Vehicle For :", rampageScriptArgs.sIntroCutscene, " Game Time:", GET_GAME_TIMER())
				RC_START_CUTSCENE_MODE(plyrPos, TRUE, FALSE, TRUE, TRUE, FALSE, TRUE, TRUE) 
				
				//BLOCK_SCENARIOS_AND_AMBIENT(scenarioBlock)
				SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
				bCutsceneIsPlaying = FALSE
				LOAD_RAMPAGE_ASSETS(FALSE)
				stageProgress = SP_RUNNING
			ENDIF
		BREAK
		CASE SP_RUNNING
			IF ARE_RAMPAGE_ASSETS_LOADED()
				IF NOT bCutsceneCanBeSkipped
					bCutsceneCanBeSkipped = TRUE
					SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
				ENDIF
//				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				//@RJP IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY doesn't seem to work, WAS_CUTSCENE_SKIPPED seems to work though
				IF WAS_CUTSCENE_SKIPPED()
					stageProgress = SP_CSKIPPED
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING() AND (bCutsceneIsPlaying = FALSE) 
				BLOCK_SCENARIOS_AND_AMBIENT(worldScenarioBlockID, FALSE)
				SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), rampageCombatParams.fPlayerWeaponDamageModifier)
				SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), rampageCombatParams.fPlayerWeaponDefenseModifier)
				CPRINTLN(DEBUG_RAMPAGE, "*** REMOVING PLAYER WEAPONS, RESOLVING VEHICLES ***")
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
				RESOLVE_MISSION_VEHICLES_FOR_CUTSCENE()
				//If rampage2, move hipster's chair back into place
				IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2
					IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[2])
						SET_ENTITY_COORDS( rampageScriptArgs.objID[2], <<1179.778, -401.7374, 66.8697>> )
					ENDIF
					//DRAW_FAKE_CELLPHONE_SCREEN(sFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_GENERIC_CALL)
				ENDIF
//				ENABLE_AMBIENT_PEDS_AND_VEHICLES(FALSE, FALSE, TRUE, TRUE)
				
				IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE1
					// hide and move beer bottles
					IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[1])
						DETACH_ENTITY(rampageScriptArgs.objID[1])
						SET_ENTITY_COORDS( rampageScriptArgs.objID[1], <<909.8177, 3643.2930, 31.7073>> )
						SET_ENTITY_ROTATION( rampageScriptArgs.objID[1], <<0,0,140.60>> )
						FREEZE_ENTITY_POSITION(rampageScriptArgs.objID[1],TRUE)
						SET_ENTITY_VISIBLE(rampageScriptArgs.objID[1],FALSE)
					ENDIF
			
					IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[2])
						DETACH_ENTITY(rampageScriptArgs.objID[2])
						SET_ENTITY_COORDS( rampageScriptArgs.objID[2], <<907.6703, 3643.4170, 31.7073>> )
						SET_ENTITY_ROTATION( rampageScriptArgs.objID[2], <<0,0,-62.47>> )
						FREEZE_ENTITY_POSITION(rampageScriptArgs.objID[2],TRUE)
						SET_ENTITY_VISIBLE(rampageScriptArgs.objID[2],FALSE)
					ENDIF
				ENDIF
				
				bCutsceneIsPlaying = TRUE
			ENDIF
			
			IF (GET_CUTSCENE_TIME() > iRampageMusicStartTime)
				TRIGGER_RAMPAGE_MUSIC()
			ENDIF
			
			IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE4
				IF NOT bBloodDamage
					IF IS_ENTITY_ALIVE( rampageScriptArgs.pedID[1] )
						IF (GET_CUTSCENE_TIME() > 20100)
							CPRINTLN( DEBUG_RAMPAGE, "APPLYING BLOOD DAMAGE TO PED1" )
							APPLY_PED_BLOOD_SPECIFIC(rampageScriptArgs.pedID[1], ENUM_TO_INT(PDZ_HEAD), 0.500, 0.650, 307.546, 1.000, 3, 0.00, "BulletLarge") 
							bBloodDamage = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
				RC_END_CUTSCENE_MODE(TRUE, FALSE)
				POST_INTRO_CUTSCENE_SETUP()
				SETUP_INITIAL_PEDS()	
				
				IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE2
					RELEASE_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
				ENDIF
				
				IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE1
					// unhide beer bottles
					IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[1])
						FREEZE_ENTITY_POSITION(rampageScriptArgs.objID[1],FALSE)
						SET_ENTITY_VISIBLE(rampageScriptArgs.objID[1],TRUE)
					ENDIF
			
					IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[2])
						FREEZE_ENTITY_POSITION(rampageScriptArgs.objID[2],FALSE)
						SET_ENTITY_VISIBLE(rampageScriptArgs.objID[2],TRUE)
					ENDIF
				ENDIF
				
				IF (bHackInterpolate)
					SET_GAMEPLAY_INTERPOLATE_FROM_INTRO()
				ENDIF
				RETURN TRUE
			ELSE
				//vFinalCamCoord = GET_FINAL_RENDERED_CAM_COORD()
				//FinalCamRot = GET_FINAL_RENDERED_CAM_ROT()
				UPDATE_REGISTERED_INTRO_CUTSCENE_EXIT_STATE()
				IF NOT bPlayerExitStateSet
					//CPRINTLN(DEBUG_RAMPAGE, "UPDATING PLAYER CAMERA")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
			ENDIF
			
		BREAK
		CASE SP_CSKIPPED
			SET_CUTSCENE_FADE_VALUES(false, false, false, false)
			SAFE_FADE_SCREEN_OUT_TO_BLACK(250, FALSE)
			WAIT_FOR_CUTSCENE_TO_STOP()		
			RC_END_CUTSCENE_MODE()
			SAFE_DELETE_OBJECT(rampageScriptArgs.objID[0])
			IF rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE1
				// unhide beer bottles
				IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[1])
					FREEZE_ENTITY_POSITION(rampageScriptArgs.objID[1],FALSE)
					SET_ENTITY_VISIBLE(rampageScriptArgs.objID[1],TRUE)
				ENDIF
		
				IF IS_ENTITY_ALIVE(rampageScriptArgs.objID[2])
					FREEZE_ENTITY_POSITION(rampageScriptArgs.objID[2],FALSE)
					SET_ENTITY_VISIBLE(rampageScriptArgs.objID[2],TRUE)
				ENDIF
			ENDIF
			POST_INTRO_CUTSCENE_SETUP(TRUE)
			SETUP_INITIAL_PEDS()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			WAIT(250)		
			RETURN TRUE
		CASE SP_CLEANUP
			RC_END_CUTSCENE_MODE(TRUE, FALSE)
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//----------------------
//	SFORM FUNCTIONS
//----------------------

/// PURPOSE:
///    Tells us if the intro scaleform is active
/// RETURNS:
///    True or False
FUNC BOOL IS_RAMPAGE_SCALEFORM_INTRO_RUNNING()
	RETURN ENUM_TO_INT(sFormState) < ENUM_TO_INT(SFR_INTRODONE)
ENDFUNC

/// PURPOSE:
///    Tells us if the outro (results) scaleform is active
/// RETURNS:
///    True or False
FUNC BOOL IS_RAMPAGE_SCALEFORM_OUTRO_RUNNING()
	RETURN ENUM_TO_INT(sFormState) >= ENUM_TO_INT(SFR_OUTROSTART)
ENDFUNC

/// PURPOSE:
///    Tells us if any rampage scaleform is active
/// RETURNS:
///    True or False
FUNC BOOL IS_RAMPAGE_SCALEFORM_RUNNING()
	RETURN IS_RAMPAGE_SCALEFORM_INTRO_RUNNING() OR IS_RAMPAGE_SCALEFORM_INTRO_RUNNING()
ENDFUNC

/// PURPOSE:
///    Fills in data for the rampage complete endscreen
/// PARAMS:
///    cp - 
PROC ADD_CHALLENGE_TO_PASSED_ENDSCREEN_DATASET( RAMPAGE_CHALLENGE &cp )
	
	IF (cp.iRequiredAmount = 0)
		EXIT 
	ENDIF
	
	END_SCREEN_CHECK_MARK_STATUS check = ESCM_UNCHECKED
	IF (CHECK_CHALLENGE(cp))
		 check = ESCM_CHECKED
		 iCompletedChallenges ++
	ENDIF
	
	/*
	BOOL bInvalidated = TRUE
	IF (rampageInfo.bPassed)
		bInvalidated = FALSE
	ENDIF
	*/
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT( esd, ESEF_LEFT_SIDE_INT_SUBSTRING, cp.sTxtKey, "", cp.iRequiredAmount, 0, check)
	
ENDPROC

/// PURPOSE:
///    Fills in a line of the rampage complete score with an in
/// PARAMS
///    txtkey - the string we want on the left
///    val - the number we want on the right
PROC ADD_CHALLENGE_TO_PASSED_SCALEFORM(RAMPAGE_CHALLENGE &cp, INT &slot)

	IF (cp.iRequiredAmount = 0)
		EXIT 
	ENDIF
	
	INT chk = 0
	IF (CHECK_CHALLENGE(cp))
		 chk = 1
	ENDIF
	
	// headshots 
	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(chk)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		// headshots

		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(cp.sTxtKey)
			ADD_TEXT_COMPONENT_INTEGER(cp.iRequiredAmount)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	slot ++
ENDPROC

/// PURPOSE:
///    Fills in a line of the rampage complete score with an time
/// PARAMS
///    txtkey - the string we want on the left
///    val - the number we want on the right (in ms)
///    slot - the data slot we want to add it to
PROC ADD_INT_TO_PASSED_SCALEFORM(STRING txtkey, INT val, INT &slot)
	IF (val <= 0) AND (bShowZeroScores = FALSE)
		EXIT
	ENDIF
	
	// headshots 
	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(val)		// headshots
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(txtkey)	
	END_SCALEFORM_MOVIE_METHOD()
	
	slot ++
ENDPROC

/// PURPOSE:
///    Fills in a line of the rampage complete score with an time
/// PARAMS
///    txtkey - the string we want on the left
///    val - the number we want on the right (in ms)
///    slot - the data slot we want to add it to
PROC ADD_PERCENT_TO_PASSED_SCALEFORM(STRING txtkey, INT val, INT &slot)
	IF (val <= 0) AND (bShowZeroScores = FALSE)
		EXIT
	ENDIF
	
	// headshots 
	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(val)		// headshots
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(txtkey)	
	END_SCALEFORM_MOVIE_METHOD()
	slot ++
ENDPROC


/// PURPOSE:
///    Works out the final score
/// RETURNS:
///    Score
FUNC INT CALCULATE_RAMPAGE_FINAL_SCORE()
	INT i 
	INT sc 
	INT val
	INT m, s, t
	INT iTimeLeft = rampageInfo.iTimeLimit - (rampageStats.iEndTime - rampageStats.iStartTime)
	FLOAT cPercent
	
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, "-SCORE BREAKDOWN-----------------------------------")
	
	CONVERT_INT_TO_TIME(iTimeLeft, m, s, t)
	
	/* B*970451
	IF (iTimeLeft > 0)
		val = (iTimeLeft / 100) * FINAL_TIME_BONUS
		CPRINTLN(DEBUG_RAMPAGE, "  TIME LEFT:", m, ":", s, ".", t, " = ", val)
		sc += val
	ENDIF
	*/
	
	val = rampageStats.iCarDeadCount * FINAL_CAR_KILL_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  CARS KILLS:", rampageStats.iCarDeadCount, " = ", val)
	sc += val
	
	val = rampageStats.iMaxChain * FINAL_CHAIN_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  MAX CHAIN:", rampageStats.iMaxChain, " = ", val)
	sc += val
	
	val = rampageStats.iHeadShots * FINAL_HEADSHOT_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  HEADSHOTS:", rampageStats.iHeadShots, " = ", val)
	sc += val
	
	val = rampageStats.iPedDeadCount * FINAL_KILLS_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  KILLS:", rampageStats.iPedDeadCount, " = ", val)
	sc += val
	
	/*
	val = rampageStats.iCloseRange * FINAL_CLOSE_RANGE_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  CLOSE RANGE:", rampageStats.iCloseRange, " = ", val)
	sc += val
	
	val = rampageStats.iLongRange * FINAL_LONG_RANGE_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  LONG SHOT:", rampageStats.iLongRange, " = ", val)
	sc += val
	
	val = rampageStats.iInnocentsDead * FINAL_INNOCENT_BONUS
	CPRINTLN(DEBUG_RAMPAGE, "  INNOCENTS KILLS:", rampageStats.iInnocentsDead, " =  -", val)
	sc -= val
	*/
	
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, "  FINAL SCORE:", sc)
	CPRINTLN(DEBUG_RAMPAGE, "---------------------------------------------------")
	CPRINTLN(DEBUG_RAMPAGE, "")
	
	// do check to see if we have beaten rampage score and write medal to 
	CPRINTLN(DEBUG_RAMPAGE, "-CHALLENGE CHECK-----------------------------------")
	cPercent = GET_TOTAL_CHALLENGE_PERCENTAGE(rampageChallenge) * 50.0
	REPEAT COUNT_OF(rampageChallenge) i 
		IF (CHECK_CHALLENGE(rampageChallenge[i]))
			CPRINTLN(DEBUG_RAMPAGE, " CHALLENGE ", i, " COMPLETED")
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, " COMPLETED CHALLENGE PERCENTAGE: ", cPercent * 2.0, "%")
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, "---------------------------------------------------")
	CPRINTLN(DEBUG_RAMPAGE, "")
	
	// medal percents
	IF (rampageStats.iPedDeadCount >= rampageInfo.iTargetAmount)
		cPercent += 50.0
	ENDIF
	
	IF HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_SUPER_JUMP)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_SLIDEY_CARS)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_FAST_RUN)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_FAST_SWIM)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_GIVE_WEAPONS)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_GIVE_HEALTH_ARMOR)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_SPAWN_VEHICLE)
	OR HAS_CHEAT_BEEN_USED_THIS_SESSION(CHEAT_TYPE_GIVE_PARACHUTE)
		iCurrentMedalIndex = RAMPAGE_BRONZE
	ELIF (cPercent >= 100.0)
		iCurrentMedalIndex = RAMPAGE_GOLD
	ELIF (cPercent >= 75.0)
		iCurrentMedalIndex = RAMPAGE_SILVER
	ELIF (cPercent >= 50.0)
		iCurrentMedalIndex = RAMPAGE_BRONZE
	ELSE
		iCurrentMedalIndex = RAMPAGE_NOMEDAL
	ENDIF
	
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, " MEDAL PERCENT:", cPercent, "%")
	CPRINTLN(DEBUG_RAMPAGE, "")
	
	// do check to see if we have beaten rampage score and write medal to 
	CPRINTLN(DEBUG_RAMPAGE, "-SAVE DATA CHECK-----------------------------------")
	CPRINTLN(DEBUG_RAMPAGE, "  RAMPAGE #:", rampageInfo.iRampageIndex)
	CPRINTLN(DEBUG_RAMPAGE, "")
	CPRINTLN(DEBUG_RAMPAGE, "  SAVED MEDAL INDEX :", g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex)
	CPRINTLN(DEBUG_RAMPAGE, "  MEDAL THIS SESSION:", ENUM_TO_INT(iCurrentMedalIndex))
	CPRINTLN(DEBUG_RAMPAGE, "  SAVED HIGH SCORE  :", g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore)
	CPRINTLN(DEBUG_RAMPAGE, "  SCORE THIS SESSION:", sc)
	
	IF IS_REPEAT_PLAY_ACTIVE()
		IF (ENUM_TO_INT(iCurrentMedalIndex) > g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex)
		OR (sc > g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore)
			CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Repeat play is active, logging medal and/or high score for rampage ", rampageInfo.iRampageIndex, ".")
			CLEAR_REPLAY_STATS()
			BEGIN_REPLAY_STATS(13, 37)
				ADD_REPLAY_STAT_VALUE(rampageInfo.iRampageIndex)
				IF (ENUM_TO_INT(iCurrentMedalIndex) > g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex)
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Medal rank beaten. Passing new rank ", ENUM_TO_INT(iCurrentMedalIndex), ".")
					ADD_REPLAY_STAT_VALUE(ENUM_TO_INT(iCurrentMedalIndex))
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Medal rank not beaten. Passing old rank ", g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex, ".")
					ADD_REPLAY_STAT_VALUE(g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex)
				ENDIF
				IF (sc > g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore)
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> High score beaten. Passing new score ", sc, ".")
					ADD_REPLAY_STAT_VALUE(sc)
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> High score not beaten. Passing old score ", g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore, ".")
					ADD_REPLAY_STAT_VALUE(g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore)
				ENDIF
			END_REPLAY_STATS()
		ENDIF
	ENDIF
	
	IF (ENUM_TO_INT(iCurrentMedalIndex) > g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex)
		CPRINTLN(DEBUG_RAMPAGE, "")	
		CPRINTLN(DEBUG_RAMPAGE, "  MEDAL RECORD BEATEN!")
		g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iMedalIndex = ENUM_TO_INT(iCurrentMedalIndex)
	ENDIF
	
	IF (sc > g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore)
		CPRINTLN(DEBUG_RAMPAGE, "")	
		CPRINTLN(DEBUG_RAMPAGE, "  HIGH SCORE BEATEN!")
		g_savedGlobals.sRampageData.playerData[rampageInfo.iRampageIndex].iHighScore = sc
	ENDIF
	
	CPRINTLN(DEBUG_RAMPAGE, "---------------------------------------------------")
	CPRINTLN(DEBUG_RAMPAGE, "")
	
	RETURN sc
ENDFUNC

PROC LODGE_PERCENTAGE_FOR_CURRENT_RAMPAGE(FLOAT fperc)
	FLOAT fLastPerc = GET_RC_MS_PERCENTAGE(rcRampageMissionID)
	
	CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Lodging Percentage For RC Mission - Old %:", fLastPerc, " Current %:", fperc)
	IF (fLastPerc < 100.0)
		CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Previous Percentage is less than 100%")
		IF fperc >= 100.0
			++g_savedGlobals.sFlowCustom.iMissionGolds
		ENDIF
	ENDIF

	IF (fperc >= fLastPerc)
		SET_RC_MS_PERCENTAGE(rcRampageMissionID, fperc)
	ENDIF
ENDPROC


/// PURPOSE:
///    This starts up the rampage complete screen
PROC SETUP_RAMPAGE_COMPLETE_SCALEFORM()
	INT iTimeLeft = rampageInfo.iTimeLimit - (rampageStats.iEndTime - rampageStats.iStartTime)
//	INT iDataSlot = 0
	INT i
	//INT iTimeBonus 
	
	IF (iTimeLeft < 0)
		iTimeLeft = 0
	ENDIF
	
	CALCULATE_RAMPAGE_FINAL_SCORE()
	
	//Update Rampages Completed stat and achievement
	INT iNumRampagesCompleted = 0
	REPEAT NUM_OF_RAMPAGES i
		IF g_savedGlobals.sRampageData.playerData[i].iHighScore>0
			iNumRampagesCompleted++
		ENDIF
	ENDREPEAT
	
	//Check achievement and stat progress, update if necessary
	STAT_GET_INT(NUM_RAMPAGES_COMPLETED,i)
	IF I<iNumRampagesCompleted
		STAT_SET_INT(NUM_RAMPAGES_COMPLETED,iNumRampagesCompleted)
		SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH18),iNumRampagesCompleted)
	ENDIF
	
	SET_ENDSCREEN_DATASET_HEADER( esd, "RPASSED", rampageInfo.sUIName)
	
	esd.bShowSkipperPrompt = TRUE
	
	REPEAT COUNT_OF(rampageChallenge) i 
		ADD_CHALLENGE_TO_PASSED_ENDSCREEN_DATASET( rampageChallenge[i] )
	ENDREPEAT
	
	END_SCREEN_MEDAL_STATUS eMedal
	IF (iCurrentMedalIndex = RAMPAGE_GOLD)
		eMedal = ESMS_GOLD									// Gold
		LODGE_PERCENTAGE_FOR_CURRENT_RAMPAGE(100.0)
	ELIF (iCurrentMedalIndex = RAMPAGE_SILVER)
		eMedal = ESMS_SILVER								// Silver		
		LODGE_PERCENTAGE_FOR_CURRENT_RAMPAGE(75.0)
	ELSE
		eMedal = ESMS_BRONZE								// Bronze
		LODGE_PERCENTAGE_FOR_CURRENT_RAMPAGE(50.0)
	ENDIF
	
	SET_ENDSCREEN_COMPLETION_LINE_STATE( esd, rampageInfo.bPassed, "RO_RATE", iCompletedChallenges, COUNT_OF(rampageChallenge), ESC_FRACTION_COMPLETION, eMedal )
	
	IF rampageInfo.bPassed
		ENDSCREEN_PREPARE( esd )
	ENDIF
	
	IF rampageInfo.bPassed
		STOP_RAMPAGE_FX()
//		CLEAR_TIMECYCLE_MODIFIER()
	ENDIF
	
	// loade the scale forms
//	SECURE_REQUEST_AND_LOAD_SCALEFORM("MISSION_COMPLETE", sFormRampageStats)
//	SECURE_REQUEST_AND_LOAD_SCALEFORM("MP_BIG_MESSAGE_FREEMODE", sFormRampagePassMsg)		
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_DATA_SLOT_EMPTY")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		
//	END_SCALEFORM_MOVIE_METHOD()
//		
//	IF (rampageInfo.bPassed)
//	
//		CALCULATE_RAMPAGE_FINAL_SCORE()
//		
//		REPEAT COUNT_OF(rampageChallenge) i 
//			ADD_CHALLENGE_TO_PASSED_SCALEFORM(rampageChallenge[i], iDataSlot)
//		ENDREPEAT
//		
//		BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_TOTAL")
//		
//			IF (iCurrentMedalIndex = RAMPAGE_GOLD)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)				// Gold
//			ELIF (iCurrentMedalIndex = RAMPAGE_SILVER)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)				// Silver		
//			ELSE
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)				// Bronze
//			ENDIF
//			
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-99)	
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("RO_RATE")
//		
//		END_SCALEFORM_MOVIE_METHOD()
//		
//		BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampagePassMsg, "SHOW_CENTERED_MP_MESSAGE")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("RPASSED")
//			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TR_ROBJ1")		// Objective Kill ~1~ ~a~ in ~1~ seconds.
//				ADD_TEXT_COMPONENT_INTEGER(rampageInfo.iTargetAmount)
//				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(rampageInfo.sTargetName)
//			    ADD_TEXT_COMPONENT_INTEGER(rampageInfo.iTimeLimit / ONE_MINUTE)
//			END_TEXT_COMMAND_SCALEFORM_STRING()
//		END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
//	ELSE
//		BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampagePassMsg, "SHOW_CENTERED_MP_MESSAGE")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("RFAILED")
//			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TR_ROBJ1")		// Objective Kill ~1~ ~a~ in ~1~ seconds.
//				ADD_TEXT_COMPONENT_INTEGER(rampageInfo.iTargetAmount)
//				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(rampageInfo.sTargetName)
//			    ADD_TEXT_COMPONENT_INTEGER(rampageInfo.iTimeLimit / ONE_MINUTE)
//			END_TEXT_COMMAND_SCALEFORM_STRING()
//		END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
//	ENDIF
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_MISSION_TITLE")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("RAMPAGE")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("RAMPAGE")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(166)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)	
//	END_SCALEFORM_MOVIE_METHOD()
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "SET_MISSION_TITLE_COLOUR")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(166)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//	END_SCALEFORM_MOVIE_METHOD()
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampageStats, "DRAW_MENU_LIST")
//	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Updates the rampage complete screen
PROC DRAW_FROZEN_RAMPAGE_HUD(INT delay = 2000)
	INT time = rampageInfo.iTimeLimit - (rampageStats.iEndTime - rampageStats.iStartTime )
	IF (time < 0)
		time = 0
	ENDIF
	
	INT gt = GET_GAME_TIMER() + delay 
	
	WHILE (GET_GAME_TIMER() < gt)
		RAMPAGE_DISABLE_CONTROL_ACTIONS()
		DRAW_RAMPAGE_HUD(rampageStats.iPedDeadCount, "TR_HKILL", time, "TR_HTIME", 0, FALSE, -1, medalHudColor[iCurrentMedalIndex])
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Updates the rampage complete screen
PROC UPDATE_RAMPAGE_SCALEFORM()
	WEAPON_TYPE currentWeapon
	
	SWITCH (sFormState)
		CASE SFR_LEADIN
			RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
			sFormState = SFR_MOCAP
			stageProgress = SP_LOADING
			
			FALLTHRU
		BREAK
		CASE SFR_MOCAP
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			
			RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
			
			// if we are in a replay don't play the cutscene
			IF IS_REPEAT_RAMPAGE_IN_PROGRESS()
				CPRINTLN(DEBUG_RAMPAGE, "Repeat Play or All Rampage Unlocked")
				REMOVE_CUTSCENE()
				LOAD_RAMPAGE_ASSETS(TRUE)
				START_REPLAY_SETUP( rampageInfo.vPlyrStartPos, rampageInfo.fPlyrCarHideHeading, FALSE )
				
				CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Owner Ship Passed Across")
				CPRINTLN(DEBUG_RAMPAGE, "Owner Ship Passed Across")
				RC_TakeEntityOwnership(rampageScriptArgs)
				
				CLEAR_HELP()
				HIDE_HELP_TEXT_THIS_FRAME()
				sFormState = SFR_INTROSTART
				RC_CleanupScene_PEDS(rampageScriptArgs, TRUE)
				
				// we do this here so the players car is moved
				RESET_RAMPAGE_STATS(rampageStats)
				
				ENABLE_AMBIENT_PEDS_AND_VEHICLES(FALSE, FALSE, TRUE, TRUE)
				RESOLVE_MISSION_VEHICLES_FOR_CUTSCENE()	
				RC_START_CUTSCENE_MODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE) 
				RC_END_CUTSCENE_MODE()
				
				// clear all cars and peds after moving players car 
				CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 240.0)
				CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 240.0)
				BLOCK_SCENARIOS_AND_AMBIENT(worldScenarioBlockID, FALSE)
				
				CPRINTLN(DEBUG_RAMPAGE, "Repeat Play - Force Launcher For Mission To Terminate")
				CPRINTLN(DEBUG_RANDOM_CHAR, "Repeat Play - Force Launcher For Mission To Terminate")
				g_RandomChars[rampageScriptArgs.eMissionID].rcTerminateLauncher = TRUE
//				bFadeTransition = FALSE
				EXIT
			ENDIF
		
			IF UPDATE_INTRO_CUTSCENE()
				sFormState = SFR_INTROSTART
			ENDIF
		BREAK
		CASE SFR_INTROSTART
			sFormAlpha = 255
			
//			IF (bFadeTransition)
//				SET_TRANSITION_TIMECYCLE_MODIFIER("REDMIST", 1.0)
//			ELSE 
//				SET_TIMECYCLE_MODIFIER("REDMIST")
//			ENDIF
			
			RESET_RAMPAGE_STATS(rampageStats)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampage, "SHOW_SHARD_CENTERED_MP_MESSAGE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("RAMPAGE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_BLACK))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
			END_SCALEFORM_MOVIE_METHOD()
			
			isFormTimer = GET_GAME_TIMER() + 3500//DEFAULT_GOD_TEXT_TIME //B*2001997
			sFormState = SFR_INTRODONE

			IF (rampageInfo.iRandomSeed > 0)
				SET_RANDOM_SEED(rampageInfo.iRandomSeed)
			ENDIF
		BREAK
		CASE SFR_INTRODONE

			RESET_RAMPAGE_STATS(rampageStats)
			
			IF IS_REPEAT_RAMPAGE_IN_PROGRESS()
				IF IS_SCREEN_FADED_OUT()
					CPRINTLN(DEBUG_RAMPAGE, "SCREEN SHOULD BE FADED OUT")
				ELSE
					CPRINTLN(DEBUG_RAMPAGE, "SCREEN ISN'T FADED OUT")
				ENDIF
				
				IF IS_SCREEN_FADING_IN()
					CPRINTLN(DEBUG_RAMPAGE, "SCREEN IS FADING IN AND IT SHOULDN'T BE")				
				ENDIF

				CPRINTLN(DEBUG_RAMPAGE, "*** REMOVING PLAYER WEAPONS, REPEAT IN PROGRESS***")
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
				POST_INTRO_CUTSCENE_SETUP(TRUE)
				SETUP_INITIAL_PEDS()
			ENDIF
			
			sFormState = SFR_RAMPAGEON
			iCurrentMedalIndex = RAMPAGE_NOMEDAL
			iTimeLineEventIndex = 0
			
			rampageStats.iStartTime = GET_GAME_TIMER()
			rampageStats.iPredictedEndTime = rampageStats.iStartTime + rampageInfo.iTimeLimit 
			
			END_REPLAY_SETUP()
			
			PRINTLN(DEBUG_RAMPAGE, "FADING IN SCREEN")
			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
			
			// give weapons
			SETUP_RAMPAGE_BATTLE_RANT()
			FORCE_RAMPAGE_PED_ARRAY_COMBAT(rampagePed)
			TRIGGER_RAMPAGE_MUSIC()
			TRIGGER_RAMPAGE_ONE_SHOT()
			TRIGGER_RAMPAGE_FX()
			
			// force 0 wanted level
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				GIVE_PLAYER_RAMPAGE_WEAPONS()
				SET_MAX_WANTED_LEVEL(0)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
				SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), rampageCombatParams.fPlayerWeaponDamageModifier)
				SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), rampageCombatParams.fPlayerWeaponDefenseModifier)
				IF NOT bPlayerExitStateSet
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					TASK_PLAY_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sIntroAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT )
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sAudioScene)
				IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioScene)
					START_AUDIO_SCENE(sAudioScene)
					CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - AUDIO SCENE STARTED")
				ENDIF
			ENDIF
			
			BEGIN_TEXT_COMMAND_PRINT("TR_ROBJ1")
				ADD_TEXT_COMPONENT_INTEGER(rampageInfo.iTargetAmount)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(rampageInfo.sTargetName)
			END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			
//			iNextClockTickSoundTime = GET_GAME_TIMER() + 1000
			IF LOAD_STREAM( "RAMPAGE_STREAMING_BED_MASTER" )
				PLAY_STREAM_FRONTEND()
			ENDIF
		BREAK
		CASE SFR_RAMPAGEON
			
			IF NOT IS_ENTITY_PLAYING_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sIntroAnim )
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			// this is to make sure that the movie has to time to go away
			IF NOT bRampageShardAnimated
				IF (GET_GAME_TIMER() > (rampageStats.iStartTime + 2500))
					BEGIN_SCALEFORM_MOVIE_METHOD(sFormRampage, "SHARD_ANIM_OUT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_BLACK))//WHITE))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
					END_SCALEFORM_MOVIE_METHOD()
					bRampageShardAnimated = TRUE
				ENDIF
			ELIF (GET_GAME_TIMER() > (rampageStats.iStartTime + 3000))
			
				IF (bDisplayedSpecialHelp = FALSE)
					IF (rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE1)
						PRINT_HELP("RAMP_WPN_CHNGE")
						bDisplayedSpecialHelp = TRUE
					ENDIF
				ENDIF
				
				IF (sFormRampage <> NULL)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sFormRampage)
					sFormRampage = NULL
				ENDIF			
				
				EXIT 
			ENDIF
		BREAK
		CASE SFR_OUTROSTART
			sFormAlpha = 255
			
			rampageStats.iEndTime = GET_GAME_TIMER()
			TRIGGER_MUSIC_EVENT("RAMPAGE_STOP")
			
			STOP_SOUND( i_sound_countdown )
			
			SAFE_REMOVE_BLIP(battleBlip)
			CLEANUP_RAMPAGE_TANK(rampageTank)
			RAMPAGE_DISABLE_CONTROL_ACTIONS()
			
			STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30.0)
			RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, FALSE, TRUE, FALSE, FALSE)
			
			#IF IS_JAPANESE_BUILD
				 FORCE_DELETE_RAMPAGE_PED_ARRAY(rampagePed)
			#ENDIF
			
			FORCE_VEHICLE_OCCUPANTS_NEAR_PED_FLEE(PLAYER_PED_ID(), 10.0)
			FLEE_RAMPAGE_CAR_ARRAY(rampageCar)
			FLEE_RAMPAGE_PED_ARRAY(rampagePed)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			IF IS_PED_RAGDOLL( PLAYER_PED_ID() )
				GIVE_PLAYER_RAGDOLL_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			IF (rampageInfo.bPassed)
				DRAW_FROZEN_RAMPAGE_HUD()
				STOP_STREAM()
				PLAY_SOUND_FRONTEND(-1, "RAMPAGE_PASSED_MASTER")
				//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
			ELSE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sAudioScene)
				IF IS_AUDIO_SCENE_ACTIVE(sAudioScene)
					STOP_AUDIO_SCENE(sAudioScene)
					CPRINTLN(DEBUG_RAMPAGE, "RAMPAGE_CLEANUP() - AUDIO SCENE STOPPED")
					sAudioScene = NULL
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE( PLAYER_PED_ID() )
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeapon)
					IF currentWeapon = rampageInfo.plyrWeapon[0]
					OR currentWeapon = rampageInfo.plyrWeapon[1]
					AND NOT IS_PED_RAGDOLL( PLAYER_PED_ID() )
					AND NOT IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
						IF IS_PED_RUNNING( PLAYER_PED_ID() )
						OR IS_PED_SPRINTING( PLAYER_PED_ID() )
							bRunOutro = TRUE
							TASK_PLAY_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sOutroRunAnim, SLOW_BLEND_IN, NORMAL_BLEND_OUT )
						ELSE
							bRunOutro = FALSE
							TASK_PLAY_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sOutroAnim, SLOW_BLEND_IN, NORMAL_BLEND_OUT )
						ENDIF
						IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
						ENDIF
						SET_PLAYER_CONTROL( PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON )
					ELSE
						plyrGunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(currentWeapon)
						IF IS_ENTITY_OK(plyrGunObject) 
							IF IS_ENTITY_ATTACHED(plyrGunObject)
								DETACH_ENTITY(plyrGunObject)
							ENDIF
						ENDIF
						IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SETUP_RAMPAGE_COMPLETE_SCALEFORM()
			isFormTimer = GET_GAME_TIMER()
			sFormState = SFR_OUTROWAIT
		BREAK
		CASE SFR_OUTROWAIT
			RAMPAGE_DISABLE_CONTROL_ACTIONS()
			
			IF IS_ENTITY_ALIVE( PLAYER_PED_ID() )
				IF bRunOutro
					IF NOT IS_ENTITY_PLAYING_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sOutroRunAnim )
						SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
					ELIF GET_ENTITY_ANIM_CURRENT_TIME( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sOutroRunAnim ) > rampageInfo.fOutroRunPhase
						IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeapon)
							plyrGunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(currentWeapon)
						ENDIF
						IF IS_ENTITY_OK(plyrGunObject) 
							IF IS_ENTITY_ATTACHED(plyrGunObject)
								DETACH_ENTITY(plyrGunObject)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ENTITY_PLAYING_ANIM( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sOutroAnim )
						SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
					ELIF GET_ENTITY_ANIM_CURRENT_TIME( PLAYER_PED_ID(), "missrampageintrooutro", rampageInfo.sOutroAnim ) > rampageInfo.fOutroPhase
						IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeapon)
							plyrGunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(currentWeapon)
						ENDIF
						IF IS_ENTITY_OK(plyrGunObject) 
							IF IS_ENTITY_ATTACHED(plyrGunObject)
								DETACH_ENTITY(plyrGunObject)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			HIDE_HELP_TEXT_THIS_FRAME()			
			
			IF (rampageInfo.bPassed = FALSE)			
				sFormState = SFR_OUTRODONE_FAIL
				//DO_SCREEN_FADE_OUT(1000)
				isFormTimer = GET_GAME_TIMER() + 1500				
			ELIF (GET_GAME_TIMER() > (isFormTimer + DEFAULT_GOD_TEXT_TIME)) OR (IS_CUTSCENE_SKIP_BUTTON_PRESSED() AND (GET_GAME_TIMER() > isFormTimer + 2000))
				//Set end screen to animate out
				ENDSCREEN_START_TRANSITION_OUT(esd)
				sFormState = SFR_OUTRO_ANIM_OUT
				isFormTimer = GET_GAME_TIMER() + 1500
			ENDIF
			
		BREAK
		CASE SFR_OUTRODONE_FAIL
			RAMPAGE_DISABLE_CONTROL_ACTIONS()
			HIDE_HELP_TEXT_THIS_FRAME()
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 0)
			IF (GET_GAME_TIMER() > isFormTimer)	
				SAFE_DELETE_OBJECT(plyrGunObject)
				plyrGunObject = NULL
				//RC_END_CUTSCENE_MODE(TRUE, FALSE)
				SET_RAMPAGE_TO_FAIL_UPDATE_STATE()
				
				EXIT
			ENDIF
		BREAK
		
		//Wait for the pass screen to animate out
		CASE SFR_OUTRO_ANIM_OUT
			IF RENDER_ENDSCREEN( esd )
				sFormState = SFR_OUTRODONE
			ENDIF
			

		BREAK
		
		CASE SFR_OUTRODONE
			RAMPAGE_DISABLE_CONTROL_ACTIONS()
			CPRINTLN(DEBUG_RAMPAGE, "*** REMOVING PLAYER WEAPONS, OUTRO DONE***")
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			HIDE_HELP_TEXT_THIS_FRAME()
			SAFE_RELEASE_OBJECT(plyrGunObject)
//			SAFE_DELETE_OBJECT(plyrGunObject)
//			plyrGunObject = NULL
			//RC_END_CUTSCENE_MODE(TRUE, FALSE)
			ENDSCREEN_SHUTDOWN( esd )
			RAMPAGE_CLEANUP()
			EXIT
	ENDSWITCH
	
	IF (ENUM_TO_INT(sFormState) >= ENUM_TO_INT(SFR_OUTROSTART))
		IF (rampageInfo.bPassed = FALSE)			
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		ENDIF
	ENDIF
	
	// don't draw the scaleform if we're in fail state
	IF NOT IS_RAMPAGE_IN_FAIL_UPDATE_STATE()
		IF (ENUM_TO_INT(sFormState) >= ENUM_TO_INT(SFR_OUTROSTART))
		
			IF (rampageInfo.bPassed)
//				DISABLE_FRONTEND_THIS_FRAME()
				RENDER_ENDSCREEN( esd )
//				DRAW_SCALEFORM_MOVIE(sFormRampageStats, MISSION_PASSED_CENTER_X,MISSION_PASSED_CENTER_Y,MISSION_PASSED_SIZE_X,MISSION_PASSED_SIZE_Y,255,255,255,255)
//				DRAW_SCALEFORM_MOVIE(sFormRampagePassMsg, 0.5, 0.5, 1.0, 1.0, 100, 100, 100, 255 )
			ENDIF		
		ELIF (ENUM_TO_INT(sFormState) >= ENUM_TO_INT(SFR_INTROSTART))
			IF (sFormRampage <> NULL)
//				DISABLE_FRONTEND_THIS_FRAME()
				//SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sFormRampage, 255, 255, 255, sFormAlpha)
					//DRAW_SCALEFORM_MOVIE(sFormRampage, 0.5, 0.44, 1.0, 1.0, 255, 255, 255, sFormAlpha)
				//RESET_SCRIPT_GFX_ALIGN()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	UPDATE FUNCTIONS
//----------------------

/// PURPOSE:
///    This handle Z Skips
/// PARAMS:
///    iTargetStage - index of what stage we want to go to
PROC DO_Z_SKIP(INT iTargetStage)
	BOOL bFadeIn = FALSE
	RC_START_Z_SKIP()
	DELETE_RAMPAGE_PED_ARRAY(rampagePed)
	DELETE_RAMPAGE_CAR_ARRAY(rampageCar)
	
	SET_RAMPAGE_FROM_STATIC_BLIP(rampageInfo.blipNameEnum)
	WAIT_FOR_CUTSCENE_TO_STOP()
	STOP_STREAM()
	
	SWITCH iTargetStage
		CASE 0 // intro
			sFormState = SFR_MOCAP
			stageProgress = SP_LOADING 
			STOP_RAMPAGE_FX()
//			CLEAR_TIMECYCLE_MODIFIER()
			GET_NAME_OF_RAMPAGE_INTRO_CUTSCENE()
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), GET_RAMPAGE_POSITION())
			ENDIF
		BREAK
		CASE 1
			sFormState = SFR_INTROSTART
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), GET_RAMPAGE_POSITION())
			ENDIF
			POST_INTRO_CUTSCENE_SETUP(TRUE)
			SETUP_INITIAL_PEDS()
		BREAK		
		CASE 2
			sFormState = SFR_OUTRODONE
			rampageInfo.bPassed = TRUE
			bFadeIn = TRUE	
		BREAK
	ENDSWITCH
	
	RC_END_Z_SKIP(TRUE, bFadeIn)
ENDPROC

/// PURPOSE:
///    This handles the forced deletion of objects we don't want
///    Will be able to get rid of this once i can convince art to remove some stuff
PROC UPDATE_FORCED_OBJECT_DELETE()
	OBJECT_INDEX obj 
	SWITCH (rampageInfo.blipNameEnum)
//		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
//		
//			// this removes the fences so we can get in
//			obj = GET_CLOSEST_OBJECT_OF_TYPE(<<1214.3939,-431.5607,66.3090>>, 4.0, PROP_FNCLINK_02GATE4)
//			IF DOES_ENTITY_EXIST(obj)
//				SET_ENTITY_COORDS(obj, <<0, 0, -20>>)
//				SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
//				obj = NULL
//			ENDIF
//		BREAK
		
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
		
			// this removes the fences so we can get in
			obj = GET_CLOSEST_OBJECT_OF_TYPE(<<500.9570,-1805.2900,27.5551>>, 4.0, PROP_FNCLINK_02GATE4)
			IF DOES_ENTITY_EXIST(obj)
				SET_ENTITY_COORDS(obj, <<0, 0, -20>>)
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
				obj = NULL
			ENDIF
			
			obj = GET_CLOSEST_OBJECT_OF_TYPE(<<511.2028,-1798.3715,27.4122>>, 4.0, PROP_FNCRES_06GATER)
			IF DOES_ENTITY_EXIST(obj)
				SET_ENTITY_COORDS(obj, <<0, 0, -20>>)
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
				obj = NULL
			ENDIF
			
			obj = GET_CLOSEST_OBJECT_OF_TYPE(<<469.1499,-1805.6255,28.4358>>, 4.0, PROP_FNCLINK_06GATE2)
			IF DOES_ENTITY_EXIST(obj)
				SET_ENTITY_COORDS(obj, <<0, 0, -20>>)
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
				obj = NULL
			ENDIF
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			// this removes the fences so we can get in
			obj = GET_CLOSEST_OBJECT_OF_TYPE(<<-155.46, -1665.80, 32.17>>, 4.0, prop_fncsec_03c)
			IF DOES_ENTITY_EXIST(obj)
				SET_ENTITY_COORDS(obj, <<0, 0, -20>>)
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
				obj = NULL
			ENDIF
			
			obj = GET_CLOSEST_OBJECT_OF_TYPE(<<-133.76, -1617.43, 32.00>>, 4.0, prop_fncsec_03c)
			IF DOES_ENTITY_EXIST(obj)
				SET_ENTITY_COORDS(obj, <<0, 0, -20>>)
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
				obj = NULL
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    This handles S, F, J and Z skips
PROC UPDATE_RAMPAGE_FLOW_DEBUG_KEYS()
 
	#IF IS_DEBUG_BUILD
		INT iDbgStageJump
		IF (bDebugShowMissionVersion)
			SET_TEXT_SCALE(0.25, 0.25)
			DISPLAY_TEXT_WITH_NUMBER(0, 0, "NUMBER", RAMPAGE_VERSION)
		ENDIF
		
		//IF NOT IS_RAMPAGE_SCALEFORM_RUNNING()
		    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				FORCE_VEHICLE_OCCUPANTS_NEAR_PED_FLEE(PLAYER_PED_ID(), 15.0)
				WAIT_FOR_CUTSCENE_TO_STOP()
				sFormState = SFR_OUTROSTART
				//CLEAR_TIMECYCLE_MODIFIER()
				rampageInfo.bPassed = TRUE
		    ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				FORCE_VEHICLE_OCCUPANTS_NEAR_PED_FLEE(PLAYER_PED_ID(), 15.0)
				WAIT_FOR_CUTSCENE_TO_STOP()
				sFormState = SFR_OUTROSTART
				rampageInfo.bPassed = FALSE
			ENDIF
		//ENDIF	
		
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDbgStageJump)
			DO_Z_SKIP(iDbgStageJump)
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    This updates the rampage hud and shuts down the rampage if time runs out
PROC UPDATE_RAMPAGE_HUD()
	INT iValue = rampageStats.iPedDeadCount
	INT time = rampageInfo.iTimeLimit - (GET_GAME_TIMER() - rampageStats.iStartTime )

	IF (bInfiniteTime = FALSE)
		IF (time <= 0) 
			time = 0 
			sFormState = SFR_OUTROSTART
			IF (iValue >= rampageInfo.iTargetAmount) 
				rampageInfo.bPassed = TRUE
			ENDIF
		ENDIF		
	ENDIF
	
	//@RJP B*1267626 - stop ticking when time less than 10 seconds
//	IF time > 10000
//		IF (GET_GAME_TIMER() > iNextClockTickSoundTime)
//			iNextClockTickSoundTime = GET_GAME_TIMER() + 1000
//			PLAY_SOUND_FRONTEND(-1, "RAMPAGE_TIMER_COUNTDOWN_MASTER")
//			//CPRINTLN(DEBUG_RAMPAGE, " SOUND PLAYED - RAMPAGE_TIMER_COUNTDOWN_MASTER")
//		ENDIF
//	ENDIF
	
	IF NOT bTimerSoundStarted
		bTimerSoundStarted = TRUE
		
		IF (rampageInfo.blipNameEnum = STATIC_BLIP_MINIGAME_RAMPAGE5) // B*1843735 - Swap sound for military rampage
			PLAY_SOUND_FRONTEND(i_sound_countdown, "RAMPAGE_TIMER_COUNTDOWN_LOOP_3_MIN_MASTER")
		ELSE
			PLAY_SOUND_FRONTEND(i_sound_countdown, "RAMPAGE_TIMER_COUNTDOWN_LOOP_MASTER")
		ENDIF
	ENDIF
	
	IF (time <= 0)
		CPRINTLN(DEBUG_RAMPAGE, "SOUND PLAYED - TIMER STOP")
		PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
	ELIF (time < iNextBeepTime)
		CPRINTLN(DEBUG_RAMPAGE, "SOUND PLAYED - 10_SEC_WARNING - ", TO_FLOAT(iNextBeepTime) / 1000.0, " SECONDS")
		iNextBeepTime -= 1000
		PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
	ENDIF
	
	DRAW_RAMPAGE_HUD(iValue, "TR_HKILL", time, "TR_HTIME", 0, FALSE, -1, medalHudColor[iCurrentMedalIndex])
ENDPROC			

PROC UPDATE_RAMPAGE1_TIMELINE()
	INT ind
	SWITCH (iTimeLineEventIndex)
		CASE 0
			IF GET_GAME_TIMER() > rampageStats.iStartTime + 2000
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 1
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[1], <<978.3540, 3616.1819, 31.6477>>, 356.7491, TRUE, 1)
					SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind], TRUE, 60)
					iTimeLineEventIndex ++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_RAMPAGE2_TIMELINE()
	INT ind
	
	SWITCH (iTimeLineEventIndex)
		CASE 0
			IF IS_ENTITY_ALIVE(rampageScriptArgs.pedID[0])
				IF VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(rampageScriptArgs.pedID[0]) ) > 144
					GIVE_WEAPON_TO_PED( rampageScriptArgs.pedID[0], rampageInfo.enemyWeapon[0], INFINITE_AMMO, TRUE )
					SET_PED_COMBAT_ATTRIBUTES(rampageScriptArgs.pedID[0], CA_ALWAYS_FLEE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(rampageScriptArgs.pedID[0], CA_AGGRESSIVE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(rampageScriptArgs.pedID[0], CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_MOVEMENT(rampageScriptArgs.pedID[0], CM_WILLADVANCE )
					TASK_COMBAT_PED( rampageScriptArgs.pedID[0], PLAYER_PED_ID() )
				ENDIF
			ELSE
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 1
			IF rampageStats.iPedDeadCount > 15
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 2
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[0], <<1223.0145, -527.4169, 65.9266>>, 53.7151, TRUE, 1)
					SET_RAMPAGE_CAR_RAM( rampageCar[ind], 25 )
					iTimeLineEventIndex ++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF rampageStats.iPedDeadCount > 25
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 4
			IF GET_FREE_RAMPAGE_CAR_INDEX( ind )
				IF CREATE_RAMPAGE_CAR( rampageCar[ind], rampageInfo.carModels[0], <<1171.5835, -365.4520, 67.1268>>, 252.1326, TRUE, 1)
					SET_RAMPAGE_CAR_RAM( rampageCar[ind], 25 )
					iTimeLineEventIndex ++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL GET_RAMPAGE_MOLOTOV_THROWER_SPAWN_POINT(RAMPAGE_ALLEYSPAWNER &rs, INT &spawnInd)
	
	IF (iAlivePedCounter >= iMaxActivePeds)
		RETURN FALSE
	ENDIF
	
	IF (rs.iFrameCounter % 4 <> 0)
		RETURN FALSE
	ENDIF
	
	spawnInd = GET_RANDOM_INT_IN_RANGE(0, rs.iMaxActiveSpawnPoints)
	
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), rs.spawnPoint[spawnInd].vPosition) > 3600
		RETURN FALSE
	ENDIF
	
	// check that we aren't spawning on top of a ped
	IF (bUseIsAreaOccupied)
		IF IS_ANY_PED_IN_SPHERE(rs.spawnPoint[spawnInd].vPosition, 1.0)
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_ANY_PED_NEAR_POINT(rs.spawnPoint[spawnInd].vPosition, 1.0)
			RETURN FALSE
		ENDIF
	ENDIF	
	
	RETURN TRUE
	
ENDFUNC

PROC UPDATE_RAMPAGE3_TIMELINE()
	INT molotovPedInd, spawnInd
	
	SWITCH (iTimeLineEventIndex)
		CASE 0
			iMolotovRespawnTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE( 6000, 12000)
			iTimeLineEventIndex ++
		BREAK
		CASE 1
			IF GET_GAME_TIMER() > iMolotovRespawnTimer
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 2
			IF GET_FREE_RAMPAGE_PED_INDEX( rampagePed, molotovPedInd )
				IF GET_RAMPAGE_MOLOTOV_THROWER_SPAWN_POINT( rampageAlleySpawner, spawnInd )
					IF CREATE_RAMPAGER_PED_AT_SPAWN_POINT( rampagePed[molotovPedInd], rampageAlleySpawner.spawnPoint[spawnInd], TRUE, TRUE )
						rampagePed[molotovPedInd].bMolotovThrower = TRUE
						#IF IS_DEBUG_BUILD
							SET_PED_DEBUG_NAME( rampagePed[molotovPedInd].pedID, "RampMolotov", rampagePed[molotovPedInd].iIndex )
						#ENDIF
						iTimeLineEventIndex = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_RAMPAGE4_TIMELINE()
	
	INT rocketPedInd
	
	// events based on numbers of people killed
	SWITCH (iTimeLineEventIndex)
		CASE 0
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-155.1995, -1655.9540, 31.8369>>, 7.0)
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 1
			IF GET_FREE_RAMPAGE_PED_INDEX( rampagePed, rocketPedInd )
				IF CREATE_RAMPAGER_PED_AT_POSITION( rampagePed[rocketPedInd], <<-148.6551, -1631.6625, 32.0646>>, 237.4733 )
					iTimeLineEventIndex ++
					CPRINTLN( DEBUG_RAMPAGE, "UPDATE_RAMPAGE4_TIMELINE: CREATE ROCKET ENEMY" )
					GIVE_WEAPON_TO_PED( rampagePed[rocketPedInd].pedID, WEAPONTYPE_RPG, 1, TRUE )
					SET_PED_ACCURACY( rampagePed[rocketPedInd].pedID, 5 )
					SET_PED_DROPS_WEAPONS_WHEN_DEAD( rampagePed[rocketPedInd].pedID, TRUE )
					SET_PED_SPHERE_DEFENSIVE_AREA( rampagePed[rocketPedInd].pedID, <<-140.7438, -1637.3519, 31.3864>>, 5, TRUE )
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-132.3154, -1626.2484, 31.1810>>, 7.0)
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 3
			IF GET_FREE_RAMPAGE_PED_INDEX( rampagePed, rocketPedInd )
				IF CREATE_RAMPAGER_PED_AT_POSITION( rampagePed[rocketPedInd], <<-98.7502, -1607.0056, 30.8521>>, 45.6334 )
					iTimeLineEventIndex ++
					CPRINTLN( DEBUG_RAMPAGE, "UPDATE_RAMPAGE4_TIMELINE: CREATE ROCKET ENEMY" )
					GIVE_WEAPON_TO_PED( rampagePed[rocketPedInd].pedID, WEAPONTYPE_RPG, 1, TRUE )
					SET_PED_ACCURACY( rampagePed[rocketPedInd].pedID, 5 )
					SET_PED_DROPS_WEAPONS_WHEN_DEAD( rampagePed[rocketPedInd].pedID, TRUE )
					SET_PED_SPHERE_DEFENSIVE_AREA( rampagePed[rocketPedInd].pedID, <<-109.1352, -1599.4410, 30.6352>>, 5, TRUE )
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-110.1830, -1600.6589, 30.6615>>, 9.0)
				iTimeLineEventIndex ++
			ENDIF
		BREAK
		CASE 5
			IF GET_FREE_RAMPAGE_PED_INDEX( rampagePed, rocketPedInd )
				IF CREATE_RAMPAGER_PED_AT_POSITION( rampagePed[rocketPedInd], <<-98.9138, -1571.4451, 31.2628>>, 228.6146 )
					iTimeLineEventIndex ++
					CPRINTLN( DEBUG_RAMPAGE, "UPDATE_RAMPAGE4_TIMELINE: CREATE ROCKET ENEMY" )
					GIVE_WEAPON_TO_PED( rampagePed[rocketPedInd].pedID, WEAPONTYPE_RPG, 1, TRUE )
					SET_PED_ACCURACY( rampagePed[rocketPedInd].pedID, 5 )
					SET_PED_DROPS_WEAPONS_WHEN_DEAD( rampagePed[rocketPedInd].pedID, TRUE )
					SET_PED_SPHERE_DEFENSIVE_AREA( rampagePed[rocketPedInd].pedID, <<-91.4304, -1578.0559, 30.5151>>, 5, TRUE )
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF (rampageStats.iPedDeadCount > 20) OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-97.324738,-1585.606201,30.352205>>, 13.500000)
				iTimeLineEventIndex ++
				rampageCarSpawner.bIsActive = TRUE
				rampageCarSpawner.bUseSpawnPoints = TRUE
				rampageCarSpawner.iMaxActiveCars = 2
				rampageCarSpawner.iMinSpawnTime = 8000
				rampageCarSpawner.iMaxSpawnTime = 15000
				
				SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[0], <<-168.9869, -1697.6454, 30.9580>>, 38.6579)
				SET_RAMPAGE_SPAWN_POINT(rampageCarSpawner.vSpawnPoints[1], <<-186.1806, -1634.2001, 32.3377>>, 179.0611)
				
				rampageAlleySpawner.fAlleyDiffAdjust = -0.5
				CPRINTLN(DEBUG_RAMPAGE, "NEXT RAMPAGE EVENT", iTimeLineEventIndex)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC UPDATE_RAMPAGE5_TIMELINE()
	INT ind
	VECTOR tmp, pos, ResultLinkDir
	IF IS_ENTITY_ALIVE(rampageTank.vehicleID)
		rampageCarSpawner.iSpawnPointsUsed = 2
	ELSE
		rampageCarSpawner.iSpawnPointsUsed = 3
	ENDIF
	
	// generate tank in miltary base if player tries to run in there
	IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE) 
		OR IS_PLAYER_JUMPING_GATES() OR IS_PLAYER_IN_UNDERGROUND_TUNNEL()
		OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-1577.24854, 2779.56274, 16.07899>>, 60.0)
		rampageCloudSpawner.bIsActive = TRUE
		iMaxActivePeds = MAX_ACTIVE_PEDS
		
		IF NOT IS_ENTITY_ALIVE(rampageTank.vehicleID)
			tmp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_RANDOM_FLOAT_IN_RANGE(0, 360), <<0, GET_RANDOM_FLOAT_IN_RANGE(30.0, 100.0), 0>>)
			IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(tmp, pos, ResultLinkDir)	
				CREATE_RAMPAGE_TANK(rampageTank, pos)
				CPRINTLN(DEBUG_RAMPAGE, "TANK! TANK! TANK!")
				EXIT
			ENDIF
		ENDIF
	ELSE
		rampageCloudSpawner.bIsActive = FALSE
	ENDIF
	
	
	// events based on numbers of people killed
	SWITCH (iTimeLineEventIndex)
		CASE 0
			IF (rampageStats.iPedDeadCount > 3)
				IF GET_FREE_RAMPAGE_CAR_INDEX(ind)
					IF CREATE_RAMPAGE_CAR(rampageCar[ind], rampageInfo.carModels[0], <<-1111.0688, 2673.1702, 17.5640>>, 134.8095, TRUE)
						SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind])
						iTimeLineEventIndex ++
						CPRINTLN(DEBUG_RAMPAGE, "NEXT RAMPAGE EVENT", iTimeLineEventIndex)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF (rampageStats.iPedDeadCount > 10)
				IF GET_FREE_RAMPAGE_CAR_INDEX(ind)
					IF CREATE_RAMPAGE_CAR(rampageCar[ind], rampageInfo.carModels[0], <<-1536.2335, 2740.8357, 16.6437>>, 233.2830, TRUE)
						SET_RAMPAGE_CAR_DEPLOY(rampageCar[ind])
						iTimeLineEventIndex ++
						CPRINTLN(DEBUG_RAMPAGE, "NEXT RAMPAGE EVENT", iTimeLineEventIndex)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF (rampageStats.iPedDeadCount >= 15)
				
				IF CREATE_RAMPAGE_TANK(rampageTank, <<-1079.6121, 2690.5684, 19.3431>>, 132.9435) //<<-1544.3688, 2750.1809, 16.7780>>, 233.2830)
					CPRINTLN(DEBUG_RAMPAGE, "TANK! TANK! TANK!")
					//SET_RAMPAGE_CAR_DEPLOY_TO_COORDINATES(rampageTank, <<-1335.2, 2565.4, 17.7>>)
					iTimeLineEventIndex ++
					CPRINTLN(DEBUG_RAMPAGE, "NEXT RAMPAGE EVENT", iTimeLineEventIndex)
					iMaxActivePeds = MAX_ACTIVE_PEDS / 2
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_ENTITY_ALIVE(rampageTank.vehicleID)
				iMaxActivePeds = MAX_ACTIVE_PEDS / 2
			ELSE
				iMaxActivePeds = MAX_ACTIVE_PEDS
				
				//@RJP don't create a new rampage tank if one exists
				IF (rampageStats.iPedDeadCount >= 35)
					IF CREATE_RAMPAGE_TANK(rampageTank, <<-1353.2513, 2319.6099, 38.6243>>, 1.1871) //<<-1544.3688, 2750.1809, 16.7780>>, 233.2830)
						CPRINTLN(DEBUG_RAMPAGE, "TANK! TANK! TANK!")
						iTimeLineEventIndex ++
						CPRINTLN(DEBUG_RAMPAGE, "NEXT RAMPAGE EVENT", iTimeLineEventIndex)
						iMaxActivePeds = MAX_ACTIVE_PEDS / 2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_ENTITY_ALIVE(rampageTank.vehicleID)
				iMaxActivePeds = MAX_ACTIVE_PEDS / 2
			ELSE
				iMaxActivePeds = MAX_ACTIVE_PEDS
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_RAMPAGE_TIMELINE()
	#IF IS_DEBUG_BUILD
		IF (bPauseRampageSpawning)
			EXIT
		ENDIF
	#ENDIF
	
	SWITCH (rampageInfo.blipNameEnum)
		CASE STATIC_BLIP_MINIGAME_RAMPAGE1
			UPDATE_RAMPAGE1_TIMELINE()
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE2
			UPDATE_RAMPAGE2_TIMELINE()
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE3
			UPDATE_RAMPAGE3_TIMELINE()
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE4
			UPDATE_RAMPAGE4_TIMELINE()
		BREAK
		CASE STATIC_BLIP_MINIGAME_RAMPAGE5
			UPDATE_RAMPAGE5_TIMELINE()
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    This updates our RAG widgets
PROC UPDATE_RAMPAGE_DEBUG_WIDGETS()
	INT i
	FLOAT sx, sy
	
	VECTOR tmp
	SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), rampageCombatParams.fPlayerWeaponDamageModifier)
	SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), rampageCombatParams.fPlayerWeaponDefenseModifier)
	
	if (bDebugDrawSpot)
		DRAW_DEBUG_CIRCLE(vDebugDrawSpot, 1.0)
		DRAW_DEBUG_SPHERE(vDebugDrawSpot, 0.125)
	ENDIF
	
	i = 0 
	
	REPEAT COUNT_OF (coverCar) i 
		IF IS_ENTITY_ALIVE(coverCar[i])
			tmp = GET_ENTITY_COORDS(coverCar[i])
			DRAW_DEBUG_LINE(tmp, tmp + <<0, 0, 10>>, 255, 128, 0, 255)
		ENDIF
	ENDREPEAT
	
	IF (bInstaPassChallenges)
		i = 0
		REPEAT COUNT_OF(rampageChallenge) i 
			rampageChallenge[i].iRequiredAmount = 0
		ENDREPEAT
		bInstaPassChallenges = FALSE
	ENDIF
	
	iRampageSpawnStateDebug = ENUM_TO_INT(rampageSpawnState)
	
	IF (bDeletePedsAndCars) or (bDeletePeds)
		DELETE_RAMPAGE_PED_ARRAY(rampagePed)
		bDeletePeds = FALSE
	ENDIF
	
	IF (bDebugDeletePedsForJapan)
		FORCE_DELETE_RAMPAGE_PED_ARRAY(rampagePed)
		bDebugDeletePedsForJapan = FALSE
	ENDIF
	
	IF (bDeletePedsAndCars) or (bDeleteCars)
		DELETE_RAMPAGE_CAR_ARRAY(rampageCar)
		DELETE_RAMPAGE_TANK(rampageTank)
		bDeleteCars = FALSE
	ENDIF
	
	bDeletePedsAndCars = FALSE 
	
	IF (bCleanupPedsAndCars) or (bCleanupPeds)
		CLEANUP_RAMPAGE_PED_ARRAY(rampagePed)
		bCleanupPeds = FALSE
	ENDIF
	
	IF (bCleanupPedsAndCars) or (bCleanupCars)
		CLEANUP_RAMPAGE_CAR_ARRAY(rampageCar)
		CLEANUP_RAMPAGE_TANK(rampageTank)
		bCleanupCars = FALSE
	ENDIF
	
	bCleanupPedsAndCars = FALSE 	
	
	
	
	IF (bCreateTestVehicle)
		IF GET_FREE_RAMPAGE_CAR_INDEX(i)
			IF CREATE_RAMPAGE_CAR(rampageCar[i], GET_RANDOM_RAMPAGE_CAR_MODEL(), rampageCarSpawner.vSpawnPoints[iTestVehicleSpawnPointIndex].vPosition, rampageCarSpawner.vSpawnPoints[iTestVehicleSpawnPointIndex].fHeading)
				SET_RAMPAGE_CAR_DEPLOY(rampageCar[i], TRUE)
				bCreateTestVehicle = FALSE
			ENDIF
		ENDIF
	ENDIF

		
	IF (bCreateTestTank)
		DELETE_RAMPAGE_TANK(rampageTank)
		IF CREATE_RAMPAGE_TANK(rampageTank, rampageCarSpawner.vSpawnPoints[iTestVehicleSpawnPointIndex].vPosition, rampageCarSpawner.vSpawnPoints[iTestVehicleSpawnPointIndex].fHeading)
			bCreateTestTank = FALSE
		ENDIF
	ENDIF
	
	IF (bInstantPass)
		rampageStats.iPedDeadCount = rampageInfo.iTargetAmount
		bInstantPass = FALSE 
	ENDIF
	
	IF (bInstantFail)
		rampageStats.iStartTime = (GET_GAME_TIMER() - rampageInfo.iTimeLimit)
		bInstantFail = FALSE
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vBattleCenter)
		DRAW_DEBUG_CIRCLE(vBattleCenter, fBattleRadius)
		DRAW_DEBUG_SPHERE(vBattleCenter, 0.5)
	ENDIF
	
	// handle spawn point checker
	IF (bCheckSpawnPosition)
		bIsPointOkay = TRUE
		IF (bSetPlayerPosAsCheckPoint)
			vCheckSpawnPoint = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		i = 0
		REPEAT iMaxActiveNoSpawnAreas i 
			IF IS_POINT_IN_ANGLED_AREA(vCheckSpawnPoint, noSpawnArea[i].vPosition[0], noSpawnArea[i].vPosition[1], noSpawnArea[i].fWidth)
				bIsPointOkay = FALSE
				
				i = 99999
			ENDIF
		ENDREPEAT
		
		bCheckSpawnPosition = FALSE 
	ENDIF
	
	// no spawn debug
	i = 0
	REPEAT COUNT_OF(noSpawnArea) i
		IF (noSpawnArea[i].bRecalculate)
			SETUP_RAMPAGE_AREA(noSpawnArea[i], noSpawnArea[i].vPosition[0], noSpawnArea[i].vPosition[1], noSpawnArea[i].fWidth)
		ENDIF
		
		IF (bDebugShowTTY)
			IF (noSpawnArea[i].bShowDebug)
				DRAW_DEBUG_AREA(noSpawnArea[i].vPosition[0], noSpawnArea[i].vPosition[1], noSpawnArea[i].fWidth, 255, 0, 0, 255)
			ENDIF		
		ENDIF
	ENDREPEAT
	
	// fortress debug
	i = 0
	REPEAT COUNT_OF(rampageFort) i
		IF (rampageFort[i].bRecalculate)
			SETUP_RAMPAGE_FORTRESS(rampageFort[i], rampageFort[i].defenseArea.vPosition[0], rampageFort[i].defenseArea.vPosition[1], rampageFort[i].defenseArea.fWidth, rampageFort[i].bIsActive)
			SETUP_RAMPAGE_AREA(rampageFort[i].spawnArea, rampageFort[i].spawnArea.vPosition[0], rampageFort[i].spawnArea.vPosition[1], rampageFort[i].spawnArea.fWidth)
			rampageFort[i].bRecalculate = FALSE
		ENDIF

		tmp = INTERPOLATE_VECTOR(rampageFort[i].spawnArea.vPosition[0], rampageFort[i].spawnArea.vPosition[1], 0.5)
			
		IF (rampageFort[i].bGenerateTestSpawnPoint)
			IF GENERATE_FORTRESS_SPAWN_POINT(rampageFort[i], rampageFort[i].vTestSpawnPoint)
				rampageFort[i].bGenerateTestSpawnPoint = FALSE 
			ENDIF
		ENDIF
		
		IF (rampageFort[i].bShowDebugTestSpawnPoint)
			DRAW_DEBUG_LINE(rampageFort[i].vTestSpawnPoint, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			DRAW_DEBUG_SPHERE(rampageFort[i].vTestSpawnPoint, 0.5)
		ENDIF
				
		IF (rampageFort[i].bIsActive)
			DRAW_DEBUG_LINE(tmp - <<0, 0, 20>>, tmp + <<0, 0, 40>>)
			DRAW_DEBUG_RAMPAGE_FORTRESS(rampageFort[i])
		ENDIF
		
		IF (bDebugShowTTY)
			IF GET_SCREEN_COORD_FROM_WORLD_COORD(tmp, sx, sy) 
				SET_TEXT_SCALE(2.0, 2.0)
				DISPLAY_TEXT_WITH_NUMBER(sx, sy, "NUMBER", i)
			ENDIF
			
		
			IF (rampageFort[i].bIsActive) OR (rampageFort[i].bShowDebug)
				DRAW_DEBUG_RAMPAGE_FORTRESS(rampageFort[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (bShowScenarioBlockArea)
		IF NOT IS_VECTOR_ZERO(rampageInfo.vScenarioBlockCenter)
			DRAW_DEBUG_CIRCLE(rampageInfo.vScenarioBlockCenter, rampageInfo.fScenarioBlockRadius)
		ENDIF
	ENDIF
	
ENDPROC 
#ENDIF

/// PURPOSE:
///    This updates the entire rampage
PROC UPDATE_RAMPAGE_FAIL_STATE()
	RAMPAGE_DISABLE_CONTROL_ACTIONS()
	SWITCH (sFailState)
		CASE SP_LOADING // not actually a loading state this does nothing
		BREAK
		CASE SP_SETUP
			CPRINTLN(DEBUG_RAMPAGE, "Entering Failure State")
			CLEAR_PRINTS()
			CLEAR_HELP()
			STOP_RAMPAGE_FX()
//			CLEAR_TIMECYCLE_MODIFIER()
			// Remove all blips here
			IF (rampageStats.iPedDeadCount < rampageInfo.iTargetAmount)
				CPRINTLN(DEBUG_RAMPAGE, "Supposed to be setting failure reason here...")
				Random_Character_Failed_With_Reason("TR_FAIL", TRUE) 				
			ELSE
				CPRINTLN(DEBUG_RAMPAGE, "No Fail Reason...")
				Random_Character_Failed()
			ENDIF
			
			sFailState = SP_RUNNING
		BREAK	
		CASE SP_RUNNING
			CPRINTLN(DEBUG_RAMPAGE, "Running Failure State")
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				REMOVE_DECALS_IN_RANGE(GET_RAMPAGE_POSITION(), 500.0)
				CPRINTLN(DEBUG_RAMPAGE, "*** REMOVING PLAYER WEAPONS, CLEAN UP***")
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
				CPRINTLN(DEBUG_RAMPAGE, "Rampage Cleanup")
				RAMPAGE_CLEANUP()
			ENDIF			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    This updates the entire rampage
PROC UPDATE_RAMPAGE()
	
	iFrameCounter ++
	iGameTimer = GET_GAME_TIMER()

	//CPRINTLN(DEBUG_RAMPAGE, "---- RAMPAGE START FRAME: ", iFrameCounter, " ---- GAME TIMER:", iGameTimer, " ----")
	
	IF (iFrameCounter % 10 = 0)
		UPDATE_FORCED_OBJECT_DELETE()
	ENDIF
	
	// we now need to do this every frame
	
	NO_AMBIENT_MALARKY_THIS_FRAME()
	SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	IF IS_RAMPAGE_IN_FAIL_UPDATE_STATE()
		UPDATE_RAMPAGE_FAIL_STATE()
	ELSE
	
		#IF IS_DEBUG_BUILD
			UPDATE_RAMPAGE_FLOW_DEBUG_KEYS()
			UPDATE_RAMPAGE_DEBUG_WIDGETS()
		#ENDIF
	
		UPDATE_RAMPAGE_SCALEFORM()
	ENDIF
	
	// reset wanted rating
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	
	IF (sFormState = SFR_RAMPAGEON)
		PROCESS_RAMPAGE_DAMAGE_EVENT_QUEUE()
		PROCESS_ALL_PED_SCANNER_VEHICLE_EXPLOSION_CHECKS()
		
		CREATE_EXLOSIVE_PROPS()
		
		UPDATE_RAMPAGE_HUD()
		UPDATE_RAMPAGE_PEDS(rampagePed)
		UPDATE_RAMPAGE_CARS()
		UPDATE_RAMPAGE_TANK(rampageTank)
		UPDATE_COVER_CARS()
		UPDATE_RAMPAGE_TIMELINE()
		
		iAlivePedCounter = GET_ALIVE_RAMPAGE_PED_COUNT(rampagePed)
		iAliveCarCounter = GET_ALIVE_RAMPAGE_CAR_COUNT()
		
		IF UPDATE_RAMPAGE_SPAWN_BLOCKER()
			IF iGameTimer > (rampageStats.iStartTime + iInitialCarSpawnDelay)
				UPDATE_RAMPAGE_CAR_SPAWNER(rampageCarSpawner)
			ENDIF
			
			IF iGameTimer > (rampageStats.iStartTime + iInitialSpawnDelay)
				UPDATE_RAMPAGE_CLOUD_SPAWNER(rampageCloudSpawner)
				UPDATE_RAMPAGE_CLOUD_SPAWNER(rampageBackupCloudSpawner)
				UPDATE_RAMPAGE_FORTRESS_SPAWNER(rampageFortressSpawner)
				UPDATE_RAMPAGE_ALLEY_SPAWNER(rampageAlleySpawner)
			ENDIF
		ENDIF
		
		// here we blip the rampage point incase the player fucks off from the area - also turn on emergency spawner
		IF NOT DOES_BLIP_EXIST(battleBlip)
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vBattleCenter, FALSE) > fBattleRadius) 
				bInBattleArea = FALSE
				rampageBackupCloudSpawner.bIsActive = TRUE
				
				battleBlip = ADD_BLIP_FOR_COORD(vBattleCenter)
				IF DOES_BLIP_EXIST(battleBlip)
					SET_BLIP_AS_FRIENDLY(battleBlip, FALSE)
				ENDIF
			ENDIF
		ELSE
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vBattleCenter, FALSE) < fBattleRadius - 10) 
				rampageBackupCloudSpawner.bIsActive = FALSE
				bInBattleArea = TRUE
				SAFE_REMOVE_BLIP(battleBlip)
				battleBlip = NULL 
			ENDIF
		ENDIF
		
		IF (rampageStats.iLastKillTime > 0)
			IF (iGameTimer > (rampageStats.iLastKillTime + CHAIN_TIMER))
				
				IF (rampageStats.iChain > 1)
					CPRINTLN(DEBUG_RAMPAGE, "Chain Ended - Chain:", rampageStats.iChain)
					IF (rampageStats.iChain > rampageStats.iMaxChain)
						rampageStats.iMaxChain = rampageStats.iChain
						CPRINTLN(DEBUG_RAMPAGE, "New Max Kill Chain:", rampageStats.iMaxChain)
						//PLAY_SOUND_FRONTEND(GET_SOUND_ID(), "MULTIPLIER_AWARD_MASTER")
						
						IF (bShowNotifications)
							/*
							BEGIN_TEXT_COMMAND_THEFEED_POST("NOT_MCHN") // New Max Chain 
							ADD_TEXT_COMPONENT_INTEGER(rampageStats.iMaxChain)
							END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
							*/
						ENDIF
					ELSE
						CPRINTLN(DEBUG_RAMPAGE, "Kill Chain:", rampageStats.iChain)
						//PLAY_SOUND_FRONTEND(GET_SOUND_ID(), "MULTIPLIER_AWARD_MASTER")
						
						IF (bShowNotifications)
							/*
							BEGIN_TEXT_COMMAND_THEFEED_POST("NOT_CHN") // Kill Streak
							ADD_TEXT_COMPONENT_INTEGER(rampageStats.iChain)
							END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
							*/
						ENDIF
					ENDIF
				ENDIF
				
				rampageStats.iChain = 0
			ENDIF
		ENDIF
	ENDIF
	
	//CPRINTLN(DEBUG_RAMPAGE, "---- RAMPAGE END FRAME ----")
	//CPRINTLN(DEBUG_RAMPAGE, " ") 
ENDPROC




