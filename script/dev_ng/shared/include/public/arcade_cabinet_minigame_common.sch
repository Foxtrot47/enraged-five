USING "timer_public.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"
USING "freemode_header.sch"
USING "net_mp_tv.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"
USING "net_beam_hack_2d_vectors.sch"
USING "net_office_private.sch"

//##### CONSTANTS #####

CONST_FLOAT cfDEFAULT_ASPECT_RATIO_MODIFIER		1.778 	//How much the X values for the minigame need to be multiplied by to work with the default aspect ratio.
CONST_FLOAT cfSCREEN_CENTER						0.5
CONST_FLOAT cfARCADE_MINIGAME_FLOAT_TOLERANCE 	0.0001
CONST_FLOAT cfSTICK_THRESHOLD					0.65
CONST_FLOAT cfBASE_SCREEN_HEIGHT				1080.0
CONST_FLOAT cfBASE_SCREEN_WIDTH					1920.0
CONST_FLOAT MAX_FLOAT 340282300000000000000000000000000000000.0
CONST_FLOAT cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE 					0.001
CONST_INT ciARCADE_CABINET_LEADERBOARD_INITIALS 4
CONST_INT ciARCADE_CABINET_LEADERBOARD_BITS 6
CONST_INT ciARCADE_CABINET_LEADERBOARD_INITIALS_MAX 36

//##### STRUCTS #####

STRUCT RGBA_COLOUR_STRUCT
	INT iR
	INT iG
	INT iB
	INT iA = 255
ENDSTRUCT

//##### VARIABLES #####

FLOAT fAspectRatio
FLOAT fAspectRatioModifier
INT iScreenX
INT iScreenY

#IF IS_DEBUG_BUILD
INT iDebugLinesDrawnThisFrame
INT iRectsDrawnThisFrame
INT iSpritesDrawnThisFrame
#ENDIF

//##### METHODS #####

PROC INIT_RGBA_STRUCT(RGBA_COLOUR_STRUCT &Colour, INT iR, INT iG, INT iB, INT iA = 255)
	Colour.iR = iR
	Colour.iG = iG
	Colour.iB = iB
	Colour.iA = iA
ENDPROC

/// PURPOSE:
///    Calculates the screen aspect ratio based on the current resolution.
FUNC FLOAT GET_CUSTOM_SCREEN_ASPECT_RATIO()
	
	GET_ACTUAL_SCREEN_RESOLUTION(iScreenX, iScreenY) 
	
	fAspectRatio = GET_ASPECT_RATIO(FALSE)
	
	//Fix for Eyefinity triple monitors: the resolution returns as the full three-screen width but the game is only drawn on one screen.
	IF IS_PC_VERSION()
		IF fAspectRatio >= 4.0
			fAspectRatio = fAspectRatio / 3.0
		ENDIF
	ENDIF
	
	RETURN fAspectRatio
	
ENDFUNC

FUNC VECTOR_2D ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_PIXELSPACE(VECTOR_2D vScreenSpace)
	RETURN INIT_VECTOR_2D(vScreenSpace.x * cfBASE_SCREEN_WIDTH, vScreenSpace.y * cfBASE_SCREEN_HEIGHT)
ENDFUNC

FUNC VECTOR_2D ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(VECTOR_2D vPixelSpace)
	RETURN INIT_VECTOR_2D(vPixelSpace.x * (1 / cfBASE_SCREEN_WIDTH), vPixelSpace.y * (1 / cfBASE_SCREEN_HEIGHT))
ENDFUNC

FUNC FLOAT APPLY_ASPECT_MOD_TO_X(FLOAT fX)
	fX = ((fX * cfBASE_SCREEN_WIDTH) - ((cfBASE_SCREEN_WIDTH - cfBASE_SCREEN_HEIGHT) / 2)) / cfBASE_SCREEN_HEIGHT
	fX =  cfSCREEN_CENTER - ((cfSCREEN_CENTER - fX) / fAspectRatio)
	RETURN fX
ENDFUNC

FUNC FLOAT APPLY_ASPECT_MOD_TO_X_ROTATED(FLOAT fX)
	fX =  cfSCREEN_CENTER - ((cfSCREEN_CENTER - fX) / fAspectRatio)
	RETURN fX
ENDFUNC

FUNC VECTOR_2D APPLY_ASPECT_MOD_TO_VECTOR_2D(VECTOR_2D v)
	v.x = APPLY_ASPECT_MOD_TO_X(v.x)
	RETURN v
ENDFUNC

/// PURPOSE:
///    Calculates the modifer value used for the width of sprites based on the current aspect ratio and 
///    the default aspect modifier value.
/// PARAMS:
///    fAspectRatio	- The current aspect ratio value.
/// RETURNS:
///    FLOAT: The value the sprite needs to be multiplied by to keep the correct aspect ratio. Default / current aspect value
FUNC FLOAT GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	RETURN cfDEFAULT_ASPECT_RATIO_MODIFIER / fAspectRatio
ENDFUNC

/// PURPOSE:
///    Updates the current aspect ratio and aspect ratio modifier values.
PROC SET_CURRENT_ASPECT_RATIO_MODIFIER()
	fAspectRatio = GET_CUSTOM_SCREEN_ASPECT_RATIO()
	fAspectRatioModifier = GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
ENDPROC

/// PURPOSE:
///    Draws a rect from screenspace values
PROC DRAW_RECT_FROM_VECTOR_2D(VECTOR_2D vCenter, VECTOR_2D vScale, RGBA_COLOUR_STRUCT rgba)

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF
	
	vCenter = APPLY_ASPECT_MOD_TO_VECTOR_2D(vCenter)
	vScale.x = vScale.x * fAspectRatioModifier
	
#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_RECT(vCenter.x * g_fMinimizeArcadeCabinetCentreX, vCenter.y * g_fMinimizeArcadeCabinetCentreY, vScale.x * g_fMinimizeArcadeCabinetCentreScale, vScale.y * g_fMinimizeArcadeCabinetCentreScale, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	ELSE
#ENDIF
		DRAW_RECT(vCenter.x, vCenter.y, vScale.x, vScale.y, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF

#IF IS_DEBUG_BUILD
	iRectsDrawnThisFrame++
#ENDIF

ENDPROC

/// PURPOSE:
///    Draws a rect from pixelspace values (reference resolution: 1920x1080)
PROC ARCADE_DRAW_PIXELSPACE_RECT(VECTOR_2D vCenter, VECTOR_2D vScale, RGBA_COLOUR_STRUCT rgba)
	vCenter = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vCenter)
	vScale = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vScale)
	DRAW_RECT_FROM_VECTOR_2D(vCenter, vScale, rgba)
ENDPROC

/// PURPOSE:
///    Draws a sprite from screenspace values
///    Note: Use ARCADE_CABINET_DRAW_SPRITE_ROTATED for non-square rotated sprites
PROC ARCADE_CABINET_DRAW_SPRITE(STRING stTextureDict, STRING stTexture, FLOAT fCenterX, FLOAT fCenterY, FLOAT fScaleX, FLOAT fScaleY, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTexture)
		//ASSERTLN("[BAZ]ARCADE_CABINET_DRAW_SPRITE - Texture is null")
		EXIT
	ENDIF

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		////CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_CABINET] [JS] Invalid Dictionary string passed. SPRITE will not display")
		ASSERTLN("[ARCADE_CABINET] [JS] Invalid Dictionary string passed. SPRITE will not display.")
		ASSERTLN(stTexture)
		EXIT
	ENDIF

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX) * g_fMinimizeArcadeCabinetCentreX, fCenterY * g_fMinimizeArcadeCabinetCentreY, fScaleX * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, fScaleY * g_fMinimizeArcadeCabinetCentreScale, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, FALSE, TRUE)
	ELSE
#ENDIF
		INT iPixels = ROUND(fCenterY * iScreenY)
		fCenterY = TO_FLOAT(iPixels) * (1.0 / iScreenY)
		
		iPixels = ROUND((fScaleY * iScreenY) / 4) * 4
		fScaleY = TO_FLOAT(iPixels) * (1.0 / iScreenY)
		
		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX), fCenterY, fScaleX * fAspectRatioModifier, fScaleY, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, FALSE, TRUE)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

PROC ARCADE_CABINET_DRAW_SPRITE_WITH_UV(STRING stTextureDict, STRING stTexture, FLOAT fCenterX, FLOAT fCenterY, FLOAT fScaleX, FLOAT fScaleY, FLOAT ux, FLOAT uy, FLOAT vx, FLOAT vy, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		////CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_CABINET] [JS] Invalid Dictionary string passed. SPRITE will not display")
		ASSERTLN("[ARCADE_CABINET] [JS] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
//		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX) * g_fMinimizeArcadeCabinetCentreX, fCenterY * g_fMinimizeArcadeCabinetCentreY, fScaleX * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, fScaleY * g_fMinimizeArcadeCabinetCentreScale, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, FALSE, TRUE)
		DRAW_SPRITE_ARX_WITH_UV(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX) * g_fMinimizeArcadeCabinetCentreX, fCenterY * g_fMinimizeArcadeCabinetCentreY, fScaleX * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, fScaleY * g_fMinimizeArcadeCabinetCentreScale,ux,uy,vx,vy, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, TRUE)
	ELSE
#ENDIF
		INT iPixels = ROUND(fCenterY * iScreenY)
		fCenterY = TO_FLOAT(iPixels) * (1.0 / iScreenY)
		
		iPixels = ROUND((fScaleY * iScreenY) / 4) * 4
		fScaleY = TO_FLOAT(iPixels) * (1.0 / iScreenY)
		
//		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX), fCenterY, fScaleX * fAspectRatioModifier, fScaleY, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, FALSE, TRUE)
		DRAW_SPRITE_ARX_WITH_UV(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(fCenterX), fCenterY, fScaleX * fAspectRatioModifier, fScaleY,ux,uy,vx,vy, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, TRUE)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

PROC ARCADE_CABINET_DRAW_3D_SPRITE_TO_CAM(STRING stTextureDict, STRING stTexture, VECTOR vPosition, FLOAT fWidth, FLOAT fHeight, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		////CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_CABINET] [AC] Invalid Dictionary string passed. SPRITE will not display")
		ASSERTLN("[ARCADE_CABINET] [AC] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

	DRAW_SPRITE_3D(stTextureDict, stTexture, vPosition.x, vPosition.y, vPosition.z, fWidth, fHeight, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

PROC ARCADE_CABINET_DRAW_3D_SPRITE_TO_CAM_WITH_UV(STRING stTextureDict, STRING stTexture, VECTOR vPosition, FLOAT fWidth, FLOAT fHeight, VECTOR_2D vU, VECTOR_2D vV, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		////CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_CABINET] [AC] Invalid Dictionary string passed. SPRITE will not display")
		ASSERTLN("[ARCADE_CABINET] [AC] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

	DRAW_SPRITE_3D_WITH_UV(stTextureDict, stTexture, vPosition.x, vPosition.y, vPosition.z, fWidth, fHeight,vU.x,vU.y,vV.x,vV.y, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

PROC ARCADE_CABINET_DRAW_3D_SPRITE_WITH_UV(STRING stTextureDict, STRING stTexture, VECTOR vPosition, FLOAT fWidth, FLOAT fHeight, VECTOR vForward, VECTOR vUp, BOOL bTwoSided, VECTOR_2D vU, VECTOR_2D vV, RGBA_COLOUR_STRUCT rgba)
	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		////CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_CABINET] [AC] Invalid Dictionary string passed. SPRITE will not display")
		ASSERTLN("[ARCADE_CABINET] [AC] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF
	// Probably this I'm not sure if I am getting the right or left vector, but it works... change the variable name if you want.
	VECTOR vRight 	= NORMALISE_VECTOR(CROSS_PRODUCT(-vForward,vUp))
	VECTOR vPos 	= vPosition
	VECTOR VecCoorsFirst 	= vPos+(vRight*fWidth*0.5)+(vUp*fHeight*0.5)
	VECTOR VecCoorsSecond	= vPos+(vRight*fWidth*0.5)+(vUp*fHeight*-0.5)
	VECTOR VecCoorsThird 	= vPos+(vRight*fWidth*-0.5)+(vUp*fHeight*0.5)
	DRAW_TEXTURED_POLY(VecCoorsFirst, VecCoorsSecond, VecCoorsThird,rgba.iR,rgba.iG,rgba.iB,rgba.iA,stTextureDict,stTexture,<<vU.x,vU.y,1>>,<<vU.x,vV.y,1>>,<<vV.x,vU.y,1>>)
	VecCoorsFirst 	= vPos+(vRight*fWidth*0.5)+(vUp*fHeight*-0.5)
	VecCoorsSecond	= vPos+(vRight*fWidth*-0.5)+(vUp*fHeight*-0.5)
	VecCoorsThird 	= vPos+(vRight*fWidth*-0.5)+(vUp*fHeight*0.5)
	DRAW_TEXTURED_POLY(VecCoorsFirst, VecCoorsSecond, VecCoorsThird,rgba.iR,rgba.iG,rgba.iB,rgba.iA,stTextureDict,stTexture,<<vU.x,vV.y,1>>,<<vV.x,vV.y,1>>,<<vV.x,vU.y,1>>)
	IF bTwoSided
		vRight 	= NORMALISE_VECTOR(CROSS_PRODUCT(vForward,vUp))
		VecCoorsFirst 	= vPos+(vRight*fWidth*0.5)+(vUp*fHeight*0.5)
		VecCoorsSecond	= vPos+(vRight*fWidth*0.5)+(vUp*fHeight*-0.5)
		VecCoorsThird 	= vPos+(vRight*fWidth*-0.5)+(vUp*fHeight*0.5)
		DRAW_TEXTURED_POLY(VecCoorsFirst, VecCoorsSecond, VecCoorsThird,rgba.iR,rgba.iG,rgba.iB,rgba.iA,stTextureDict,stTexture,<<vV.x,vU.y,1>>,<<vV.x,vV.y,1>>,<<vU.x,vU.y,1>>)
		VecCoorsFirst 	= vPos+(vRight*fWidth*0.5)+(vUp*fHeight*-0.5)
		VecCoorsSecond	= vPos+(vRight*fWidth*-0.5)+(vUp*fHeight*-0.5)
		VecCoorsThird 	= vPos+(vRight*fWidth*-0.5)+(vUp*fHeight*0.5)
		DRAW_TEXTURED_POLY(VecCoorsFirst, VecCoorsSecond, VecCoorsThird,rgba.iR,rgba.iG,rgba.iB,rgba.iA,stTextureDict,stTexture,<<vV.x,vV.y,1>>,<<vU.x,vV.y,1>>,<<vU.x,vU.y,1>>)
	ENDIF
ENDPROC

PROC ARCADE_CABINET_DRAW_3D_SPRITE(STRING stTextureDict, STRING stTexture, VECTOR vPosition, FLOAT fWidth, FLOAT fHeight, VECTOR vForward, VECTOR vUp, BOOL bTwoSided, RGBA_COLOUR_STRUCT rgba)
	ARCADE_CABINET_DRAW_3D_SPRITE_WITH_UV(stTextureDict,stTexture,vPosition,fWidth,fHeight,vForward,vUp,bTwoSided,INIT_VECTOR_2D(0,0),INIT_VECTOR_2D(1,1),rgba)
ENDPROC

/// PURPOSE:
///    Draws a sprite from screenspace values
///    Note: Only use for non-square rotated sprites when ARCADE_CABINET_DRAW_SPRITE does not work
///    Speak to John Scott if you aren't sure on usage
PROC ARCADE_CABINET_DRAW_SPRITE_ROTATED(STRING stTextureDict, STRING stTexture, FLOAT fCenterX, FLOAT fCenterY, FLOAT fScaleX, FLOAT fScaleY, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)
#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X_ROTATED(fCenterX), fCenterY, fScaleX * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, fScaleY * g_fMinimizeArcadeCabinetCentreScale, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, FALSE, TRUE)
	ELSE
#ENDIF
		DRAW_SPRITE_ARX(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X_ROTATED(fCenterX), fCenterY, fScaleX * fAspectRatioModifier, fScaleY, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA, FALSE, TRUE)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

/// PURPOSE:
///    Draws a sprite from pixelspace (reference resolution: 1920x1080) values
PROC ARCADE_DRAW_PIXELSPACE_SPRITE(STRING stTextureDict, STRING stTexture, VECTOR_2D vCenter, VECTOR_2D vScale, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)
	vCenter = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vCenter)
	vScale = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vScale)

	ARCADE_CABINET_DRAW_SPRITE(stTextureDict, stTexture, vCenter.x, vCenter.y, vScale.x, vScale.y, fRotation, rgba)
ENDPROC

PROC ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(STRING stTextureDict, STRING stTexture, VECTOR_2D vCenter, VECTOR_2D vScale,VECTOR_2D vU,VECTOR_2D vV, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)
	vCenter = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vCenter)
	vScale = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vScale)
	ARCADE_CABINET_DRAW_SPRITE_WITH_UV(stTextureDict, stTexture, vCenter.x, vCenter.y, vScale.x, vScale.y,vU.x,vU.y,vV.x,vV.y, fRotation, rgba)
ENDPROC

/// PURPOSE:
///    Returns true if lines AB and CD intersect
///    Point of intersection is returned as vResult 
///    Note: vResult is set to (MAX_FLOAT, MAX_FLOAT) if no intersect
FUNC BOOL ARCADE_CABINET_GET_LINE_INTERSECT(VECTOR_2D vA, VECTOR_2D vB, VECTOR_2D vC, VECTOR_2D vD, VECTOR_2D &vResult)
	
	//Line AB equation: fA1x + fB1y = fC1 
	FLOAT fA1 = vB.y - vA.y
	FLOAT fB1 = vA.x - vB.x
	FLOAT fC1 = (fA1 * vA.x) + (fB1 * vA.y)
	
	//Line CD equation: fA2x + fB2y = fC2 
	FLOAT fA2 = vD.y - vC.y
	FLOAT fB2 = vC.x - vD.x
	FLOAT fC2 = (fA2 * vC.x) + (fB2 * vC.y)
	
	FLOAT fDeterminant = (fA1 * fB2) - (fA2 * fB1)
	
	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] ARCADE_CABINET_GET_LINE_INTERSECT - fDeterminant ", fDeterminant)
	
	//Lines are parallel if fDeterminant = 0
	IF NOT IS_FLOAT_IN_RANGE(fDeterminant, 0 - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 0 + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
		
		FLOAT fIntersectX = ((fB2 * fC1) - (fB1 * fC2)) / fDeterminant
		FLOAT fIntersectY = ((fA1 * fC2) - (fA2 * fC1)) / fDeterminant
		VECTOR_2D vPointOfIntersection = INIT_VECTOR_2D(fIntersectX, fIntersectY)
		
		//Check the intersection is within the given line segment
		IF FMIN(vA.x - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vB.x - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE) <= vPointOfIntersection.x 
		AND vPointOfIntersection.x <= FMAX(vA.x + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vB.x + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE)
		AND FMIN(vA.y - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vB.y - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE) <= vPointOfIntersection.y 
		AND vPointOfIntersection.y <= FMAX(vA.y + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vB.y + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE)
		AND  FMIN(vC.x - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vD.x - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE) <= vPointOfIntersection.x 
		AND vPointOfIntersection.x <= FMAX(vC.x + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vD.x + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE)
		AND FMIN(vC.y - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vD.y - cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE) <= vPointOfIntersection.y 
		AND vPointOfIntersection.y <= FMAX(vC.y + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE, vD.y + cfARCADE_CABINET_LINE_INTERSECTION_TOLERANCE)
			
			vResult = vPointOfIntersection
			RETURN TRUE
			
		ENDIF
		
	ENDIF
	
	vResult = INIT_VECTOR_2D(MAX_FLOAT,MAX_FLOAT)
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Returns a null string
/// RETURNS:
///    
FUNC STRING ARCADE_GET_NULL_STRING()
	STRING stString
	RETURN stString
ENDFUNC

#IF IS_DEBUG_BUILD

PROC DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(VECTOR_2D v0, VECTOR_2D v1, RGBA_COLOUR_STRUCT rgba)
#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF
	v0 = APPLY_ASPECT_MOD_TO_VECTOR_2D(v0)
	v1 = APPLY_ASPECT_MOD_TO_VECTOR_2D(v1)
#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_DEBUG_LINE_2D(<<v0.x, v0.y, 0>>, <<v1.x, v1.y, 0>>, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	ELSE
#ENDIF
		DRAW_DEBUG_LINE_2D(<<v0.x, v0.y, 0>>, <<v1.x, v1.y, 0>>, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF

	iDebugLinesDrawnThisFrame++

ENDPROC

/// PURPOSE:
///    Dumps all minigame data to the console.
PROC ARCADE_CABINET_DUMP_DATA_TO_CONSOLE()

	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] ARCADE_CABINET_DUMP_DATA_TO_CONSOLE - Dumping data...")
	UNUSED_PARAMETER(iDebugLinesDrawnThisFrame)
	UNUSED_PARAMETER(iRectsDrawnThisFrame)
	UNUSED_PARAMETER(iSpritesDrawnThisFrame)	
	//General data.
	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] iRectsDrawnThisFrame = ", iRectsDrawnThisFrame)
	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] iSpritesDrawnThisFrame = ", iSpritesDrawnThisFrame)
	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] iDebugLinesDrawnThisFrame = ", iDebugLinesDrawnThisFrame)
	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] fAspectRatio = ", fAspectRatio)
	////CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] fAspectRatioModifier = ", fAspectRatioModifier)
	
ENDPROC

#ENDIF

/// PURPOSE:
///    Call every frame before minigame processing to hide UI and disable features which shouldn't be shown during the minigame
/// PARAMS:
///    bPlaying - Whether or not the minigame is actually on screen and running. False when the script is running but the player has not pressed the prompt to play
PROC ARCADE_CABINET_COMMON_EVERY_FRAME_PROCESSING(BOOL bPlaying)

	//Ensure the aspect ratio values are kept up to date in case players change resolution mid-game.
	SET_CURRENT_ASPECT_RATIO_MODIFIER()

	//Control
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	IF IS_PC_VERSION()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	
	//UI
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	Pause_Objective_Text()
	DISABLE_SELECTOR_THIS_FRAME()
	//DISABLE_DPADDOWN_THIS_FRAME()
	
	THEFEED_HIDE_THIS_FRAME()
	
	IF bPlaying
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		IF NOT IS_PAUSE_MENU_ACTIVE()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		ENDIF
	ENDIF
	
	//Replay Recording
	#IF USE_REPLAY_RECORDING_TRIGGERS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame = 0
	iDebugLinesDrawnThisFrame = 0
	iRectsDrawnThisFrame = 0
#ENDIF
	
ENDPROC

/// PURPOSE:
///    Call when the minigame is started to remove player control and supress unwanted UI elements
PROC ARCADE_CABINET_COMMON_INITIALIZATION()
	
	//Prevent notifications from appearing during the minigame.
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
	g_bIsPlayerPlayingArcadeGame = TRUE

	SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
	
	//Disable player control
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		DISABLE_INTERACTION_MENU() 
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	//Turn on blinders for multihead displays.
	SET_MULTIHEAD_SAFE(TRUE, FALSE)
	
	//Block HUD elements
	DISABLE_SCRIPT_HUD(HUDPART_NONE, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_STATBOX, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_MISSIONBOX, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_DRUG_HUD, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_INVENTORY, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_TRANSITIONHUD, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_ROCKSTARTICKER, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_VEHICLE, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_MAP, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_OTHERPLAYERBOX, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_ENEMIESPLAYERBOX, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_MISSIONBRIEF, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_TODOBOX, TRUE)
	
ENDPROC

/// PURPOSE:
///    Call when the minigame is cleaned up to return player control and re-enable disabled UI
PROC ARCADE_CABINET_COMMON_CLEANUP()
	
	// Reset Arcade Playing bool
	g_bIsPlayerPlayingArcadeGame = FALSE

	//Unpause objective text
	Unpause_Objective_Text()
	
	//Re-enable phone/internet
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	MP_FORCE_TERMINATE_INTERNET_CLEAR()
	
	//Re-enable notifications
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	
	//Disable multiple displays/blinders.
	SET_MULTIHEAD_SAFE(FALSE, TRUE)
	
	//Re-enable the interaction menu.
	ENABLE_INTERACTION_MENU()
	
	//Return player control.
	IF IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	//Unblock HUD elements
	DISABLE_SCRIPT_HUD(HUDPART_NONE, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_STATBOX, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_MISSIONBOX, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_DRUG_HUD, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_INVENTORY, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_TRANSITIONHUD, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_ROCKSTARTICKER, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_VEHICLE, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_MAP, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_OTHERPLAYERBOX, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_ENEMIESPLAYERBOX, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_MISSIONBRIEF, FALSE)
	DISABLE_SCRIPT_HUD(HUDPART_TODOBOX, FALSE)
	
ENDPROC

//##### LEADERBOARD #####

/// PURPOSE:
///    Packs a leaderboard entry's initials into a single int
FUNC INT ARCADE_PACK_LEADERBOARD_INITIALS(INT &iInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS])
	
	INT i, iPackedInitials
	
	FOR i = 0 TO ciARCADE_CABINET_LEADERBOARD_INITIALS - 1
		SET_PACKED_BITFIELD_VALUE(iPackedInitials, i, ciARCADE_CABINET_LEADERBOARD_BITS, iInitials[i])
	ENDFOR
	
	RETURN iPackedInitials
	
ENDFUNC

/// PURPOSE:
///    Unpacks leaderboard initials stored as a signle int into an array
PROC ARCADE_UNPACK_LEADERBOARD_INITIALS(INT iPackedInitials, INT &iInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS])
	
	INT i
	FOR i = 0 TO ciARCADE_CABINET_LEADERBOARD_INITIALS - 1
		iInitials[i] = GET_PACKED_BITFILED_VALUE(iPackedInitials, i, ciARCADE_CABINET_LEADERBOARD_BITS)
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Returns the character from a leaderboard initial int
FUNC STRING ARCADE_GET_CHAR_FROM_INT(INT iChar)

	SWITCH iChar
		CASE 0
			RETURN "A"
		CASE 1
			RETURN "B"
		CASE 2
			RETURN "C"
		CASE 3
			RETURN "D"
		CASE 4
			RETURN "E"
		CASE 5
			RETURN "F"
		CASE 6
			RETURN "G"
		CASE 7
			RETURN "H"
		CASE 8
			RETURN "I"
		CASE 9
			RETURN "J"
		CASE 10
			RETURN "K"
		CASE 11
			RETURN "L"
		CASE 12
			RETURN "M"
		CASE 13
			RETURN "N"
		CASE 14
			RETURN "O"
		CASE 15
			RETURN "P"
		CASE 16
			RETURN "Q"
		CASE 17
			RETURN "R"
		CASE 18
			RETURN "S"
		CASE 19
			RETURN "T"
		CASE 20
			RETURN "U"
		CASE 21
			RETURN "V"
		CASE 22
			RETURN "W"
		CASE 23
			RETURN "X"
		CASE 24
			RETURN "Y"
		CASE 25
			RETURN "Z"
		CASE 26
			RETURN "0"
		CASE 27
			RETURN "1"
		CASE 28
			RETURN "2"
		CASE 29
			RETURN "3"
		CASE 30
			RETURN "4"
		CASE 31
			RETURN "5"
		CASE 32
			RETURN "6"
		CASE 33
			RETURN "7"
		CASE 34
			RETURN "8"
		CASE 35
			RETURN "9"
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC
