USING "commands_graphics.sch"
USING "commands_misc.sch"
USING "commands_ped.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "Overlay_Effects.sch"
USING "commands_task.sch"
USING "rgeneral_include.sch" //to avoid duplicate command problem



PROC DISPLAY_BEAM_WITH_ROTATION(vehicle_index vehBeamFrom, vector beamRotation)
	if IS_VEHICLE_DRIVEABLE(vehBeamFrom)
		if not IS_VEHICLE_SEARCHLIGHT_ON(vehBeamFrom)
			SET_VEHICLE_SEARCHLIGHT(vehBeamFrom,true,FALSE)
		ENDIF
				
		float beamRange = 250.0
		float tx = (COS(beamRotation.x) * beamRange) * sin(-beamRotation.z)
		float ty = (COS(beamRotation.x) * beamRange) * cos(-beamRotation.z)
		float tz = (sin(beamRotation.x) * beamRange)
			
		ped_index driver = GET_PED_IN_VEHICLE_SEAT(vehBeamFrom,VS_DRIVER)
		if not IS_PED_INJURED(driver)
			TASK_VEHICLE_AIM_AT_COORD(driver,GET_ENTITY_COORDS(vehBeamFrom)+<<tx,ty,tz>>)			
		ENDIF
	ENDIF
ENDPROC



proc DISPLAY_BEAM_AT_PED(vehicle_index vehBeamFrom,ped_index targetPed)//, float beamRange = 250.0, float beamRadius = 12.0, int r=255,int g=255,int b = 255, int a =100)			
	if IS_VEHICLE_DRIVEABLE(vehBeamFrom)
		if not IS_VEHICLE_SEARCHLIGHT_ON(vehBeamFrom)
			SET_VEHICLE_SEARCHLIGHT(vehBeamFrom,true,FALSE)
		ENDIF
		ped_index driver = GET_PED_IN_VEHICLE_SEAT(vehBeamFrom,VS_DRIVER)
		if not IS_PED_INJURED(driver)
			if GET_SCRIPT_TASK_STATUS(driver,SCRIPT_TASK_ANY) = VACANT_STAGE
				IF NOT IS_PED_INJURED(targetPed)
					TASK_VEHICLE_AIM_AT_PED(driver,targetPed)
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

proc DISPLAY_BEAM(vehicle_index vehBeamFrom, bool bDisplay=TRUE)//, float beamRange = 250.0, float beamRadius = 12.0, int r=255,int g=255,int b = 255, int a =100)
	
	
	
		if IS_VEHICLE_DRIVEABLE(vehBeamFrom)
			If bDisplay
				if not IS_VEHICLE_SEARCHLIGHT_ON(vehBeamFrom)
					SET_VEHICLE_SEARCHLIGHT(vehBeamFrom,true,FALSE)
					CPRINTLN(DEBUG_TREVOR3,"SET_VEHICLE_SEARCHLIGHT onb")
				ENDIF
				ped_index driver = GET_PED_IN_VEHICLE_SEAT(vehBeamFrom,VS_DRIVER)
				if not IS_PED_INJURED(driver)
					if GET_SCRIPT_TASK_STATUS(driver,SCRIPT_TASK_ANY) = VACANT_STAGE
						CPRINTLN(DEBUG_TREVOR3,"Aim task given")
						TASK_VEHICLE_AIM_USING_CAMERA(driver)				
					ENDIF
				ENDIF
			else
				if IS_VEHICLE_SEARCHLIGHT_ON(vehBeamFrom)
					SET_VEHICLE_SEARCHLIGHT(vehBeamFrom,FALSE,FALSE)
					CPRINTLN(DEBUG_TREVOR3,"SET_VEHICLE_SEARCHLIGHT off")
					ped_index driver = GET_PED_IN_VEHICLE_SEAT(vehBeamFrom,VS_DRIVER)
					if not IS_PED_INJURED(driver)
						if GET_SCRIPT_TASK_STATUS(driver,SCRIPT_TASK_VEHICLE_AIM_USING_CAMERA) = PERFORMING_TASK
							CLEAR_PED_TASKS(driver)				
						ENDIF
					ENDIF
				ENDIF
				
			endif
		ENDIF
	
ENDPROC
/*
FUNC FLOAT GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=FALSE)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC
*/
FUNC VECTOR GET_ROTATION_BETWEEN_VECTORS(vector firstVector, vector secondVector)
	vector vResult
	vResult.z = GET_HEADING_FROM_COORDS(firstVector,secondVector)
	vResult.x = ATAN2((secondVector.z - firstVector.z),SQRT(((secondVector.x - firstVector.x)*(secondVector.x - firstVector.x))+((secondVector.y - firstVector.y)*(secondVector.y - firstVector.y))))
	return vResult
ENDFUNC


func bool IS_PED_IN_VIEWING_ARC_FROM_PED(ped_index pedViewer, PED_INDEX pedToView, float viewingArc, float viewingRange, bool includeLOScheck=FALSE)
	vector vPedA, vPedB
	if not IS_ENTITY_DEAD(pedViewer)
		vPedA = GET_ENTITY_COORDS(pedViewer)
		if IS_ENTITY_DEAD(pedToView)
			vPedB = /*WITH DEADCHECK=FALSE*/ GET_ENTITY_COORDS(pedToView)
		ELSE
			vPedB = GET_ENTITY_COORDS(pedToView)
		ENDIF
		
		if ABSF(GET_ENTITY_HEADING(pedViewer) - GET_HEADING_FROM_COORDS(vPedA,vPedB,true)) < viewingArc/2
			if GET_DISTANCE_BETWEEN_COORDS(vPedB,vPedB) < viewingRange
				if includeLOScheck = TRUE					
					if HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedViewer,pedToView)
						return true
					ELSE
						return FALSE
					ENDIF
				ELSE
					return TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC


