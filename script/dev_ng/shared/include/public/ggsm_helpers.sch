//-------------------------------------------------
// File: ggsm_arcade_helper.sch
// Purpose: general functions required by system
//-------------------------------------------------

//-------------------------------------------------
// INCLUDES
//-------------------------------------------------
USING "ggsm_structs.sch"
USING "ggsm_maths.sch"
USING "ggsm_data.sch"
USING "arcade_games_help_text_flow.sch"
USING "net_arcade_cabinet.sch"

//-------------------------------------------------
// SOUND FUNCTIONS
//-------------------------------------------------
FUNC BOOL GGSM_IS_SOUND_LOOPED(ARCADE_GAMES_SOUND eSound)
	SWITCH (eSound)
		CASE ARCADE_GAMES_SOUND_GGSM_BOSS_GRAN_ATTACK_EYE_LASER_CHARGE_LOOP
		CASE ARCADE_GAMES_SOUND_GGSM_ABIL_PLAYER_DECOY_LOOP
			RETURN TRUE 
	ENDSWITCH 
	
	RETURN FALSE 
ENDFUNC 

PROC GGSM_START_AUDIO_SCENE(STRING str)
	IF NOT IS_AUDIO_SCENE_ACTIVE(str)
		START_AUDIO_SCENE(str)
	ENDIF 
ENDPROC 

PROC GGSM_STOP_AUDIO_SCENE(STRING str)
	IF IS_AUDIO_SCENE_ACTIVE(str)
		STOP_AUDIO_SCENE(str)
	ENDIF 
ENDPROC 

FUNC BOOL GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND eSound)
	IF (eSound = ARCADE_GAMES_SOUND_END)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF sGGSMData.bDebugDisableSFX
			RETURN FALSE
		ENDIF
	#ENDIF
	
	ARCADE_GAMES_SOUND_PLAY(eSound)
	ARCADE_GAMES_SOUND_SET_VARIABLE(eSound, "Screen_Position", 0.5)
	RETURN TRUE 
ENDFUNC 

FUNC BOOL GGSM_PLAY_SOUND_FROM_POSITION(ARCADE_GAMES_SOUND eSound, VECTOR_2D vPos)
	IF (eSound = ARCADE_GAMES_SOUND_END)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF sGGSMData.bDebugDisableSFX
			RETURN FALSE
		ENDIF
	#ENDIF
	
	FLOAT sp = ((vPos.x - GGSM_PX_OVERSCAN_MIN_X) / GGSM_PX_OVERSCAN_WIDTH)
	sp = CLAMP(sp, 0.0, 1.0)
	
	ARCADE_GAMES_SOUND_PLAY(eSound)
	ARCADE_GAMES_SOUND_SET_VARIABLE(eSound, "Screen_Position", sp)
	RETURN TRUE 
ENDFUNC 

FUNC BOOL GGSM_TRIGGER_MUSIC_EVENT(STRING str)
	BOOL bOk = TRIGGER_MUSIC_EVENT(str)
	CPRINTLN(DEBUG_MINIGAME, "GGSM_TRIGGER_MUSIC_EVENT:", str)
	RETURN bOk
ENDFUNC 

FUNC BOOL GGSM_TRIGGER_MUSIC_EVENT_FOR_STAGE(INT iStage)
	SWITCH (iStage)
		CASE 0 RETURN GGSM_TRIGGER_MUSIC_EVENT("ARCADE_SM_LVL_1_START")
		CASE 1 RETURN GGSM_TRIGGER_MUSIC_EVENT("ARCADE_SM_LVL_2_START")
		CASE 2 RETURN GGSM_TRIGGER_MUSIC_EVENT("ARCADE_SM_LVL_3_START")
		CASE 3 RETURN GGSM_TRIGGER_MUSIC_EVENT("ARCADE_SM_LVL_4_START")
		CASE 4 RETURN GGSM_TRIGGER_MUSIC_EVENT("ARCADE_SM_LVL_5_START")
		CASE 5 RETURN GGSM_TRIGGER_MUSIC_EVENT("ARCADE_SM_BOSS_START")
	ENDSWITCH 
	
	RETURN FALSE 
ENDFUNC 

//-------------------------------------------------
// FADE FUNCTIONS
//-------------------------------------------------
PROC GGSM_FADE_SCREEN_OUT(FLOAT fTime = 0.5)
	sGGSMData.fFadeTime = fTime 
ENDPROC 

PROC GGSM_FADE_SCREEN_OUT_INSTANT()
	sGGSMData.fFadeTime = 0.001 
	sGGSMData.fFadeAlpha = 1.0
ENDPROC 

PROC GGSM_FADE_SCREEN_IN(FLOAT fTime = 0.5)
	sGGSMData.fFadeTime = -fTime 
ENDPROC 

FUNC BOOL GGSM_IS_SCREEN_FADED_OUT()
	RETURN (sGGSMData.fFadeAlpha = 1.0)
ENDFUNC 

FUNC BOOL GGSM_IS_SCREEN_FADED_IN()
	RETURN (sGGSMData.fFadeAlpha = 0.0)
ENDFUNC 

PROC GGSM_FAKE_FADE_SCREEN_OUT(FLOAT fTime = 0.5)
	sGGSMData.fFakeFadeTime = fTime 
ENDPROC 

PROC GGSM_FAKE_FADE_SCREEN_IN(FLOAT fTime = 0.5)
	sGGSMData.fFakeFadeTime = -fTime 
ENDPROC 

FUNC BOOL GGSM_IS_SCREEN_FAKE_FADED_OUT()
	RETURN (sGGSMData.fFakeFadeAlpha = 1.0)
ENDFUNC 

FUNC BOOL GGSM_IS_SCREEN_FAKE_FADED_IN()
	RETURN (sGGSMData.fFakeFadeAlpha = 0.0)
ENDFUNC 

PROC GGSM_CLEAR_FAKE_FADE()
	sGGSMData.fFakeFadeAlpha = 0.0
	sGGSMData.fFakeFadeTime = 0.0
ENDPROC 

PROC GGSM_CLEAR_WHITE_OUT()
	sGGSMData.fWhiteOutAlpha = 0.0
	sGGSMData.fWhiteOutFadeTime = 0.0
ENDPROC 

PROC GGSM_FADE_TO_WHITE(FLOAT fTime = 0.5)
	sGGSMData.fWhiteOutFadeTime = fTime 
ENDPROC 

PROC GGSM_FADE_FROM_WHITE(FLOAT fTime = 0.5)
	sGGSMData.fWhiteOutFadeTime = -fTime 
ENDPROC 

FUNC BOOL GGSM_IS_SCREEN_WHITED_OUT()
	RETURN (sGGSMData.fWhiteOutAlpha = 1.0)
ENDFUNC 

FUNC BOOL GGSM_IS_SCREEN_WHITED_IN()
	RETURN (sGGSMData.fWhiteOutAlpha = 0.0)
ENDFUNC 

PROC GGSM_NUKE_WHITE_INSTANT()
	sGGSMData.fWhiteOutFadeTime = -1.0
	sGGSMData.fWhiteOutAlpha = 1.0
ENDPROC 


//-------------------------------------------------
// PC CONTROLS FUNCTIONS
//-------------------------------------------------

PROC GGSM_PC_CONTROLS_INIT()
	IF !sGGSMData.bHasInitPCControls
		INIT_PC_SCRIPTED_CONTROLS("Arcade_Go_Go_Space_Monkey")
		sGGSMData.bHasInitPCControls = TRUE
	ENDIF
ENDPROC

PROC GGSM_PC_CONTROLS_CLEANUP()
	IF sGGSMData.bHasInitPCControls
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
		sGGSMData.bHasInitPCControls = FALSE
	ENDIF
ENDPROC

//-------------------------------------------------
// ANIM FUNCTIONS
//-------------------------------------------------

PROC GGSM_PLAY_AVATAR_LOST_LIFE_ANIM()
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = FALSE
	
	sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_LOSE
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_PLAY_AVATAR_GAME_OVER_ANIM()
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = FALSE

	sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_LOSE_BIG
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_PLAY_AVATAR_BOSS_DEAD_ANIM()
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = FALSE

	sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_WIN
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_PLAY_AVATAR_ALL_CLEAR_ANIM()
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = FALSE

	sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_WIN_BIG
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_PLAY_AVATAR_IDLE_ANIM()
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = TRUE

	sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_IDLE
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_PLAY_AVATAR_PLAY_IDLE_ANIM(ARCADE_CABINET_ANIM_CLIPS eClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = FALSE

	sGGSMData.eAvatarClip = eClip
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_PLAY_AVATAR_PLAY_RANDOM_IDLE_ANIM()
	ARCADE_CABINET_ANIM_CLIPS eClip
	
	// If already doing PlayIdle, alternate variation
	IF sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
		eClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2
	
	ELIF sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2
		eClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
	
	// Otherwise, pick random variation
	ELSE
		IF GET_RANDOM_BOOL()
			eClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
		ELSE
			eClip = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2
		ENDIF
	ENDIF
	
	GGSM_PLAY_AVATAR_PLAY_IDLE_ANIM(eClip)

ENDPROC

PROC GGSM_PLAY_AVATAR_LEAVE_MACHINE_ANIM()
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent 
	sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
	sArcadeCabinetAnimEvent.bLoopAnim = FALSE
	
	sGGSMData.eAvatarClip = ARCADE_CABINET_ANIM_CLIP_EXIT
	sGGSMData.iAvatarClipFrames = 0
	PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, sGGSMData.eAvatarClip)
ENDPROC

PROC GGSM_UPDATE_AVATAR()
	
	sGGSMData.iAvatarClipFrames++
	
	// If current anim has finished...
	IF sGGSMData.iAvatarClipFrames > 5
		IF !IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(sGGSMData.eAvatarClip)
		OR HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(sGGSMData.eAvatarClip)
			SWITCH sGGSMData.eAvatarClip
				
				// PlayIdle proceeds to PlayIdle (randomises variation each time)
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2
					GGSM_PLAY_AVATAR_PLAY_RANDOM_IDLE_ANIM()
				BREAK
				
				// Lose proceeds to PlayIdle (as player is immediately returned to current stage)
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE
					GGSM_PLAY_AVATAR_PLAY_RANDOM_IDLE_ANIM()
				BREAK

				// Win proceeds to Idle (most bosses), or PlayIdle (boss rush - but not on final boss yet, because that would have done BigWin)
				CASE ARCADE_CABINET_ANIM_CLIP_WIN
					IF sGGSMData.sGameStats.level = GGSM_SECTOR_BANANASTAR
						GGSM_PLAY_AVATAR_PLAY_RANDOM_IDLE_ANIM()
					ELSE
						GGSM_PLAY_AVATAR_IDLE_ANIM()
					ENDIF
				BREAK
				
				// Win/BigWin/BigLose proceed to Idle (as game has ended, or between stages)
				CASE ARCADE_CABINET_ANIM_CLIP_WIN_BIG
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE_BIG
					GGSM_PLAY_AVATAR_IDLE_ANIM()
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

//-------------------------------------------------
// MISC FUNCTIONS
//-------------------------------------------------
PROC GGSM_RESET_GAME_STATS()
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_RESET_GAME_STATS")
	sGGSMData.sGameStats.gameType = HASH("ARCADE_CABINET_CH_GG_SPACE_MONKEY")
	sGGSMData.sGameStats.matchId = 0
	sGGSMData.sGameStats.numPlayers = 1
	sGGSMData.sGameStats.level = GGSM_SECTOR_FIRST
	sGGSMData.sGameStats.powerUps = 0
	sGGSMData.sGameStats.kills = 0
	sGGSMData.sGameStats.timePlayed = GET_GAME_TIMER()
	sGGSMData.sGameStats.score = 0
	sGGSMData.sGameStats.reward = 0
	sGGSMData.sGameStats.challenges = 0 
	sGGSMData.iPlayerLives = 3
ENDPROC 

PROC GGSM_SEND_PLAYSTATS()
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_SEND_PLAYSTATS")
	sGGSMData.sGameStats.level = sGGSMData.sGameStats.level
	sGGSMData.sGameStats.timePlayed = GET_GAME_TIMER() - sGGSMData.sGameStats.timePlayed
	sGGSMData.sGameStats.challenges = ENUM_TO_INT(sGGSMData.eChallengeFlags)
	
	IF sGGSMData.bTrophyUnlockedThisSession
		sGGSMData.sGameStats.reward = ARCADE_GAME_GET_TROPHY_ID_FOR_TELEMETRY(ARCADE_GAME_TROPHY_GGSM)
	ELSE
		sGGSMData.sGameStats.reward = 0
	ENDIf
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			sGGSMData.sGameStats.location = ENUM_TO_INT(GET_ARCADE_PROPERTY_ID_FROM_SIMPLE_INTERIOR_ID(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior))
		#IF FEATURE_FIXER
		ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			sGGSMData.sGameStats.location = HASH("SIMPLE_INTERIOR_TYPE_FIXER_HQ")
		#ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[GGSM_HELPERS] [GGSM_SEND_PLAYSTATS] Not in valid location, setting sGGSMData.sGameStats.location = 0")
			sGGSMData.sGameStats.location = 0		
		ENDIF						
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[GGSM_HELPERS] [GGSM_SEND_PLAYSTATS] Invalid Player ID, setting sGGSMData.sGameStats.location = 0")
		sGGSMData.sGameStats.location = 0
	ENDIF
	
	PLAYSTATS_ARCADE_CABINET(sGGSMData.sGameStats)
ENDPROC 

FUNC BOOL GGSM_IS_GAME_PLAYING()
	RETURN (sGGSMData.eState >= GGSM_STATE_PLAYING) AND (sGGSMData.eState <= GGSM_STATE_GAME_OVER)
ENDFUNC

FUNC BOOL GGSM_IS_GAME_ACTIVELY_PLAYING()
	RETURN (sGGSMData.eState >= GGSM_STATE_STAGE_SELECT) AND (sGGSMData.eState <= GGSM_STATE_GAME_OVER)
ENDFUNC

/// PURPOSE:
///    Updates the values used for animating sprite sequences
PROC GGSM_UPDATE_MAIN_TIMERS()
	sGGSMData.fDeltaTime = GET_FRAME_TIME()
	sGGSMData.fStateTimer += sGGSMData.fDeltaTime
ENDPROC 

PROC GGSM_INIT_GLOBALS()
	sGGSMData.sQuitTimer.control = FRONTEND_CONTROL
	sGGSMData.sQuitTimer.action = INPUT_FRONTEND_CANCEL
	
	sGGSMData.sStartBtnTimer.control = FRONTEND_CONTROL
	sGGSMData.sStartBtnTimer.action = INPUT_FRONTEND_SELECT
	
	INIT_RGBA_STRUCT(sGGSMData.rgbaBlack, 0, 0, 0, 255)
	INIT_RGBA_STRUCT(sGGSMData.rgbaBGSprite, 64, 64, 64, 255)
	INIT_RGBA_STRUCT(sGGSMData.rgbaSprite, 208, 208, 208, 255)
	INIT_RGBA_STRUCT(sGGSMData.rgbaWhite, 255, 255, 255, 255)
	
	GGSM_RESET_GAME_STATS()
	
	#IF IS_DEBUG_BUILD
		#IF GGSM_ENTITY_EDITOR_ACTIVE
			GGSM_ENTITY_DATA_INIT_DEBUG_ARRAY()
		#ENDIF 
	#ENDIF 
ENDPROC

PROC GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TYPE eType)
	SWITCH (eType)
		CASE GGSM_UNLOCK_TSHIRTS_GGSM_ASTEROID
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_GGSM_ASTEROID)
		BREAK
		
		CASE GGSM_UNLOCK_TSHIRTS_GGSM_CLOCKEDIT
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_GGSM_CLOCKEDIT)
		BREAK

		CASE GGSM_UNLOCK_TSHIRTS_GGSM_NUKE
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_GGSM_NUKE)
		BREAK
		
		CASE GGSM_UNLOCK_TSHIRTS_GGSM_SCORE
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_GGSM_SCORE)
		BREAK
		
		CASE GGSM_UNLOCK_TSHIRTS_GGSM_COMPLETE
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_GGSM_COMPLETE)
		BREAK
		
		CASE GGSM_UNLOCK_TSHIRTS_GGSM_NODAMAGE
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_GGSM_NODAMAGE)
		BREAK
					
		CASE GGSM_UNLOCK_TROPHY
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_GGSM, FALSE)
		BREAK
		
		CASE GGSM_UNLOCK_TROPHY_PENTHOUSE
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_GGSM, TRUE)
		BREAK
		
		DEFAULT
			EXIT 
		BREAK 
	ENDSWITCH 
	
	SET_BIT(sGGSMData.iUnlockedBitField, ENUM_TO_INT(eType))
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_UNLOCK_PRIZE:", GGSM_UNLOCK_TYPE_TO_STRING(eType), " UNLOCK BITSET:", sGGSMData.iUnlockedBitField)
ENDPROC

PROC GGSM_RESET_CHALLENGES()
	sGGSMData.eChallengeFlags = GGSM_CHALLENGE_BIT_NONE
	sGGSMData.iUnlockedBitField = 0
	SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_APEESCAPE, FALSE)
	SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MONKEYKIND, FALSE)
	SET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_ARCADE_HELP_BITSET, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_ARCADE_HELP_BITSETTWO, 0)
ENDPROC

FUNC BOOL GGSM_ARE_ALL_CHALLENGES_COMPLETE()
	IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MONKEYKIND)
		RETURN FALSE 
	ENDIF 
	
	IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_APEESCAPE)
		RETURN FALSE 
	ENDIF 
	
	IF (GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP) < g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_P_SCORE)
		RETURN FALSE 
	ENDIF 
	
	RETURN TRUE
ENDFUNC 

PROC GGSM_SET_CHALLENGE_BITS_FROM_AWARDS()
	sGGSMData.eChallengeFlags = GGSM_CHALLENGE_BIT_NONE
	IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MONKEYKIND)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, GGSM_CHALLENGE_BIT_MONKEY_MIND)
	ENDIF 
	
	IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_APEESCAPE)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, GGSM_CHALLENGE_BIT_APE_ESCAPE)
	ENDIF 
	
	IF (GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP) >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_B_SCORE)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, GGSM_CHALLENGE_BIT_ASTROCHIMP_B)
	ENDIF
	
	IF (GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP) >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_S_SCORE)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, GGSM_CHALLENGE_BIT_ASTROCHIMP_S)
	ENDIF
	
	IF (GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP) >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_G_SCORE)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, GGSM_CHALLENGE_BIT_ASTROCHIMP_G)
	ENDIF
	
	IF (GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP) >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_P_SCORE)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, GGSM_CHALLENGE_BIT_ASTROCHIMP_P)
	ENDIF
	
	INT i
	GGSM_CHALLENGE_BIT challengeBit
	
	CPRINTLN(DEBUG_MINIGAME, " 	--- CHALLENGE FLAGS ---")
	REPEAT 32 i
		challengeBit = INT_TO_ENUM(GGSM_CHALLENGE_BIT, SHIFT_LEFT(1, i))
		IF IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eChallengeFlags, challengeBit)
			CPRINTLN(DEBUG_MINIGAME,"  	", GGSM_CHALLENGE_BIT_TO_STRING(challengeBit))
		ENDIF 
	ENDREPEAT 
	CPRINTLN(DEBUG_MINIGAME, " ")
ENDPROC

PROC GGSM_SET_CHALLENGE_COMPLETED(GGSM_CHALLENGE_BIT eBit)
	IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eChallengeFlags, eBit) 
		CDEBUG1LN(DEBUG_MINIGAME, "GGSM_SET_CHALLENGE_COMPLETED:", GGSM_CHALLENGE_BIT_TO_STRING(eBit))
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eChallengeFlags, eBit)
		
		SWITCH (eBit)
			CASE GGSM_CHALLENGE_BIT_APE_ESCAPE
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_APEESCAPE, TRUE)
				GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TSHIRTS_GGSM_ASTEROID)
			BREAK 
			CASE GGSM_CHALLENGE_BIT_MONKEY_MIND
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MONKEYKIND, TRUE)
				GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TSHIRTS_GGSM_NODAMAGE)
			BREAK 
			CASE GGSM_CHALLENGE_BIT_ASTROCHIMP_P
				GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TSHIRTS_GGSM_SCORE)
			BREAK 
		ENDSWITCH
		
		BOOL bOwnsPenthouse = DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		IF GGSM_ARE_ALL_CHALLENGES_COMPLETE()
			sGGSMData.bTrophyUnlockedThisSession = TRUE
			GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TSHIRTS_GGSM_COMPLETE)
			IF (bOwnsPenthouse)
				GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TROPHY_PENTHOUSE)
			ELSE 
				GGSM_UNLOCK_PRIZE(GGSM_UNLOCK_TROPHY)
			ENDIF 
		ENDIF  
	ENDIF 
ENDPROC

FUNC BOOL GGSM_SHOULD_SHOW_TUTORIAL(GGSM_HELP_BIT eBit)
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eHelpFlags, eBit)
		RETURN FALSE 
	ENDIF 
	
	RETURN TRUE
ENDFUNC 

PROC GGSM_GIVE_EXTRA_LIFE()
	sGGSMData.iPlayerLives ++ 
	GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND_GGSM_PICKUP_EXTRA_LIFE)
	GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND_GGSM_MONKEY_VOICE_EXTRA_LIFE)
ENDPROC 

FUNC INT GGSM_CALCULATE_TAKEDOWN_PERCENTAGE()
	FLOAT pc = (TO_FLOAT(sGGSMData.iEnemiesDestroyed) / TO_FLOAT(sGGSMData.iEnemiesCreated)) * 100.0
	IF (pc >= 100.0)
		pc = 100.0
	ENDIF 
		
	RETURN CEIL(pc)
ENDFUNC 

FUNC INT GGSM_CALCULATE_TAKEDOWN_BONUS()
	INT pc = GGSM_CALCULATE_TAKEDOWN_PERCENTAGE()
	INT tb = pc * (GGSM_TAKEDOWN_BONUS / 100)
	tb -= (tb % 100) // gets rid of decimals 
	RETURN tb
ENDFUNC

FUNC BOOL GGSM_IS_LAST_STAGE()
	RETURN (sGGSMData.sGameStats.level >= GGSM_SECTOR_MAX)
ENDFUNC 

PROC GGSM_INCREASE_PLAYER_SCORE(INT iSc, BOOL bNoExtend = FALSE)
	sGGSMData.sGameStats.score += iSc 
	sGGSMData.iExtendPtsCounter += iSc 
	
	IF (bNoExtend = FALSE)
		IF (sGGSMData.iExtendPtsCounter >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_POINTS_FOR_EXTEND)
			GGSM_GIVE_EXTRA_LIFE()
			sGGSMData.iExtendPtsCounter -= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_POINTS_FOR_EXTEND
		ENDIF 
	ENDIF 
	
	// astrochimp challenge
	IF (sGGSMData.sGameStats.score > GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP))
		SET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASTROCHIMP, sGGSMData.sGameStats.score)
	ENDIF 
	
	IF (sGGSMData.sGameStats.score >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_P_SCORE)
		GGSM_SET_CHALLENGE_COMPLETED(GGSM_CHALLENGE_BIT_ASTROCHIMP_P)
	ELIF (sGGSMData.sGameStats.score >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_G_SCORE)
		GGSM_SET_CHALLENGE_COMPLETED(GGSM_CHALLENGE_BIT_ASTROCHIMP_G)
	ELIF (sGGSMData.sGameStats.score >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_S_SCORE)
		GGSM_SET_CHALLENGE_COMPLETED(GGSM_CHALLENGE_BIT_ASTROCHIMP_S)
	ELIF (sGGSMData.sGameStats.score >= g_sMPTunables.iCH_ARCADE_GAMES_GGSM_ASTROCHIMP_B_SCORE)
		GGSM_SET_CHALLENGE_COMPLETED(GGSM_CHALLENGE_BIT_ASTROCHIMP_B)
	ENDIF 
ENDPROC

FUNC BOOL GGSM_IS_DIALOG_ACTIVE()
	RETURN (sGGSMData.sDialogController.eState > GGSM_DIALOG_STATE_NONE) AND (sGGSMData.sDialogController.eState < GGSM_DIALOG_STATE_DONE)
ENDFUNC 

FUNC BOOL GGSM_IS_ANY_EDITOR_RUNNING()
	#IF IS_DEBUG_BUILD
		#IF GGSM_ENTITY_EDITOR_ACTIVE
			IF (sGGSMData.sDebugEntityEditor.sEditCore.bEditorActive)
				RETURN TRUE 
			ENDIF 
		#ENDIF 
		
		#IF GGSM_SPAWN_EDITOR_ACTIVE
			IF (sGGSMData.sDebugSpawnEditor.sEditCore.bEditorActive)
				RETURN TRUE 
			ENDIF 
		#ENDIF
	#ENDIF 
	
	RETURN FALSE 
ENDFUNC

FUNC BOOL GGSM_IS_ANY_EDITOR_INCLUDED()
	#IF IS_DEBUG_BUILD
		#IF GGSM_ENTITY_EDITOR_ACTIVE
			RETURN TRUE
		#ENDIF 	
		
		#IF GGSM_SPAWN_EDITOR_ACTIVE
			RETURN TRUE
		#ENDIF 	
	#ENDIF 
	
	RETURN FALSE 
ENDFUNC

PROC GGSM_RESET_SPECIAL_WEAPONS_ARRAY(GGSM_SPWEAPON_DATA &array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		array[i].eType = GGSM_SPECIAL_WEAPON_NONE
		array[i].eState = GGSM_SPWEAPON_INACTIVE 
		array[i].fTimer = 0
	ENDREPEAT
ENDPROC

PROC GGSM_SHUTDOWN_ALL_SPECIAL_WEAPONS(GGSM_SPWEAPON_DATA &array[])
	INT i
	
	REPEAT COUNT_OF(array) i
		IF (array[i].eType != GGSM_SPECIAL_WEAPON_NONE) 
			IF (array[i].eState != GGSM_SPWEAPON_INACTIVE) AND (array[i].eState != GGSM_SPWEAPON_COOLDOWN)
				array[i].eState = GGSM_SPWEAPON_COOLDOWN
			ENDIF
		ENDIF 
	ENDREPEAT
	
	// Player Ship
	IF (sGGSMData.iPlayerShipIndex != GGSM_INVALID_ENTITY)
		CLEAR_BITMASK_ENUM_AS_ENUM(sGGSMData.sEntities[sGGSMData.iPlayerShipIndex].eFlags, GGSM_ENTITY_BIT_REFLECT_BULLETS)
	ENDIF 
ENDPROC

FUNC BOOL GGSM_IS_STAGE_READY_TO_END()
	#IF IS_DEBUG_BUILD
		IF GGSM_IS_ANY_EDITOR_RUNNING()
			RETURN FALSE 
		ENDIF
		
		IF (sGGSMData.bDebugDisableEnemySpawn)
			RETURN FALSE 
		ENDIF
	#ENDIF 
	
	IF GGSM_IS_DIALOG_ACTIVE()
		RETURN FALSE 
	ENDIF 
	
	/*
	IF (GET_GAME_TIMER() < (sGGSMData.iLastEnemyDeathTimeMS + 5000))
		RETURN FALSE 
	ENDIF
	*/
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eStageFlags, GGSM_STAGE_BIT_EXPLOSIONS_ACTIVE)
		RETURN FALSE 
	ENDIF  
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eStageFlags, GGSM_STAGE_BIT_ENEMIES_ACTIVE)
		RETURN FALSE 
	ENDIF 
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BOSSES_ACTIVE)
		RETURN FALSE 
	ENDIF 
	
	RETURN (sGGSMData.iSpawnIndexCounter >= sGGSMData.iSpawnsUsed)
ENDFUNC

//-------------------------------------------------
// INPUT FUNCTIONS
//-------------------------------------------------

/// PURPOSE:
///    Returns true if the player has pressed start this frame
FUNC BOOL GGSM_HAS_PLAYER_PRESSED_START()
	CONTROL_ACTION CA_Start = INPUT_FRONTEND_PAUSE
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_Start = INPUT_FRONTEND_ACCEPT
	ENDIF

	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Start)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Returns true if the player has pressed quit this frame
FUNC BOOL GGSM_HAS_PLAYER_PRESSED_FIRE()
	CONTROL_ACTION CA_Fire = INPUT_SCRIPT_RDOWN
	CONTROL_ACTION CA_Fire2 = INPUT_SCRIPT_RT
	
	RETURN IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire2)
ENDFUNC

/// PURPOSE:
///    Returns true if the player has pressed quit this frame
FUNC BOOL GGSM_HAS_PLAYER_PRESSED_CANCEL()
	CONTROL_ACTION CA_Fire = INPUT_FRONTEND_CANCEL
	
	RETURN IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire)
ENDFUNC

/// PURPOSE:
///    Returns true if the player has pressed quit this frame
FUNC BOOL GGSM_HAS_PLAYER_HOLDING_FIRE()
	CONTROL_ACTION CA_Fire = INPUT_SCRIPT_RDOWN
	CONTROL_ACTION CA_Fire2 = INPUT_SCRIPT_RT
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Fire) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK))
	ENDIF

	RETURN IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Fire) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Fire2)
ENDFUNC

/// PURPOSE:
///    Returns true if the player has pressed quit this frame
FUNC BOOL GGSM_HAS_PLAYER_PRESSED_SPECIAL_WEAPON()
	CONTROL_ACTION CA_Fire = INPUT_SCRIPT_RUP
	CONTROL_ACTION CA_Fire2 = INPUT_SCRIPT_LB
	RETURN IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire2)
ENDFUNC

/// PURPOSE:
///    Returns true if the player has pressed quit this frame
FUNC BOOL GGSM_HAS_PLAYER_PRESSED_DEFENSE_WEAPON()
	CONTROL_ACTION CA_Fire = INPUT_SCRIPT_RRIGHT						// keep at as right because PC
	CONTROL_ACTION CA_Fire2 = INPUT_SCRIPT_RB
	CONTROL_ACTION CA_Fire3 = INPUT_SCRIPT_RLEFT 							
	
	RETURN IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire) 
		   OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire2)
		   OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Fire3)
ENDFUNC

FUNC BOOL GGSM_ALTER_POINT_WITH_INPUT(VECTOR_2D &vec, FLOAT fSpeed, GGSM_INPUT input = GGSM_INPUT_LSTICK)
	BOOL bMoved = FALSE 
	
	FLOAT fStickX, fStickY
	FLOAT spd = fSpeed * GET_FRAME_TIME()
	
	IF (input = GGSM_INPUT_DPAD)
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
			fStickY = -sGGSMData.fDPadScalar
		ENDIF 

		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
			fStickY = sGGSMData.fDPadScalar
		ENDIF 
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
			fStickX = -sGGSMData.fDPadScalar
		ENDIF 
	
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			fStickX = sGGSMData.fDPadScalar
		ENDIF 
	ELIF (input = GGSM_INPUT_RSTICK)
		fStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
		fStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	ELSE 
		fStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		fStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	ENDIF 
	
	IF (fStickX <= -sGGSMData.fAnalogDeadZone) OR (fStickX >= sGGSMData.fAnalogDeadZone)
		vec.x += (spd * fStickX)
		bMoved = TRUE
	ENDIF 

	IF (fStickY <= -sGGSMData.fAnalogDeadZone) OR (fStickY >= sGGSMData.fAnalogDeadZone)
		vec.y += (spd * fStickY) 
		bMoved = TRUE
	ENDIF 

	RETURN bMoved
ENDFUNC 

PROC GGSM_ALTER_FLOAT_WITH_INPUT(FLOAT &fValue, FLOAT fSpeed, GGSM_INPUT input)
	FLOAT fStickX, fStickY
	FLOAT spd = fSpeed * GET_FRAME_TIME()
	
	IF (input = GGSM_INPUT_DPAD)
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
			fStickY = -sGGSMData.fDPadScalar
		ENDIF 

		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
			fStickY = sGGSMData.fDPadScalar
		ENDIF 
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
			fStickX = -sGGSMData.fDPadScalar
		ENDIF 
	
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			fStickX = sGGSMData.fDPadScalar
		ENDIF 
	ELIF (input = GGSM_INPUT_RSTICK)
		fStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
		fStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	ELSE 
		fStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		fStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	ENDIF 
	
	IF (fStickX <= -sGGSMData.fAnalogDeadZone) OR (fStickX >= sGGSMData.fAnalogDeadZone)
		fValue += (spd * fStickX)
	ENDIF 

	IF (fStickY <= -sGGSMData.fAnalogDeadZone) OR (fStickY >= sGGSMData.fAnalogDeadZone)
		fValue += (spd * fStickY) 
	ENDIF 
ENDPROC

//-------------------------------------------------
// CAMERA FUNCTIONS
//-------------------------------------------------

/// PURPOSE:
///    Resets The Camera
/// PARAMS:
///    cam - 
PROC GGSM_CAMERA_RESET(GGSM_CAMERA &cam)
	cam.vWorldPosition = INIT_VECTOR_2D(-cam.vWorldViewportOffset.x, -cam.vWorldViewportOffset.y)
	cam.vLastWorldPosition = cam.vWorldPosition
	cam.vScrollDirection = INIT_VECTOR_2D(1, 0)
	cam.fDistTravelled = 0.0
	cam.fTileScrollSpeed = 5.0
	cam.fScrollPCController = 1.0
	CLEAR_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BRING_CAMERA_TO_STOP)
ENDPROC 

/// PURPOSE:
///    Initializes the camera
/// PARAMS:
///    cam - camera struct
///    vw - viewport width
///    vh - viewport height
PROC GGSM_CAMERA_INIT(GGSM_CAMERA &cam)
	cam.vWorldViewportOffset.x = (cfBASE_SCREEN_WIDTH - GGSM_PX_OVERSCAN_WIDTH) / 2.0
	cam.vWorldViewportOffset.y = (cfBASE_SCREEN_HEIGHT - GGSM_PX_OVERSCAN_HEIGHT) / 2.0
	
	cam.vViewportSize = INIT_VECTOR_2D(GGSM_PX_OVERSCAN_WIDTH, GGSM_PX_OVERSCAN_HEIGHT)
	GGSM_CAMERA_RESET(cam)
ENDPROC

PROC GGSM_CAMERA_JUMP_TO_DISTANCE(GGSM_CAMERA &cam, FLOAT fDistance)
	GGSM_CAMERA_RESET(cam)
	cam.vWorldPosition.x += fDistance
	cam.vLastWorldPosition = cam.vWorldPosition
	cam.fDistTravelled = fDistance
ENDPROC 

/// PURPOSE:
///    Updates the camera
/// PARAMS:
///    cam - camera struct 
PROC GGSM_CAMERA_UPDATE(GGSM_CAMERA &cam)
	#IF IS_DEBUG_BUILD
	IF (cam.bDebugMoveCamera)
		GGSM_CAMERA_JUMP_TO_DISTANCE(cam, cam.fDebugMoveCameraDist)
		cam.bDebugMoveCamera = FALSE 
	ENDIF
	#ENDIF 
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BRING_CAMERA_TO_STOP)
		cam.fScrollPCController -= (GET_FRAME_TIME() / 3.0)
		IF (cam.fScrollPCController < 0.0)
			cam.fScrollPCController = 0.0
		ENDIF 
	ELSE 
		cam.fScrollPCController += (GET_FRAME_TIME() / 3.0)
		IF (cam.fScrollPCController > 1.0)
			cam.fScrollPCController = 1.0
		ENDIF 	
	ENDIF 

	IF NOT GGSM_IS_DIALOG_ACTIVE()
		cam.fSpd = ((cam.fTileScrollSpeed * sGGSMData.fTimeScale * cam.fScrollPCController) / sGGSMData.fScrollTimePerFrame) * GET_FRAME_TIME()
		IF (NOT sGGSMData.bGamePaused) AND (sGGSMData.eState = GGSM_STATE_STAGE_ACTIVE)
			cam.vWorldPosition.x += cam.vScrollDirection.x * cam.fSpd
			cam.vWorldPosition.y += cam.vScrollDirection.y * cam.fSpd
		ENDIF
	ENDIF 
	
	cam.fDistTravelled += VECTOR_2D_DIST(cam.vLastWorldPosition, cam.vWorldPosition)
	cam.vLastWorldPosition = cam.vWorldPosition
ENDPROC

//-------------------------------------------------
// FX FUNCTIONS
//-------------------------------------------------	

FUNC BOOL GGSM_SPRITE_UPDATE_ANIMATION(GGSM_SPRITE_ANIM eAnim, FLOAT &fAnimFrame, BOOL bAffectedByTimeScale = TRUE)
	GGSM_SPRITE_ANIM_DATA data 
	FLOAT fDeltaTime = GET_FRAME_TIME()
	
	IF (bAffectedByTimeScale)
		fDeltaTime *= sGGSMData.fTimeScale
	ENDIF 
	
	IF NOT GGSM_GET_SPRITE_ANIM_DATA(eAnim, data)
		RETURN FALSE 
	ENDIF 

	IF (eAnim = GGSM_SPRITE_ANIM_EXPLOSION) OR (eAnim = GGSM_SPRITE_ANIM_PLYR_EXPLOSION)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_EXPLOSIONS_ACTIVE)
	ENDIF 
	
	// animate frame
	fAnimFrame += ((data.fEndFrame - data.fStartFrame) / data.fAnimTime) * fDeltaTime

	// loop anim if needed
	IF IS_BITMASK_ENUM_AS_ENUM_SET(data.eAnimFlags, GGSM_ANIM_BIT_LOOP)
		IF (fAnimFrame < data.fStartFrame)
			fAnimFrame = data.fEndFrame
		ENDIF 
		
		IF (fAnimFrame >= data.fEndFrame + 1.0)
			fAnimFrame = data.fStartFrame
		ENDIF 
	ELSE 
		IF (fAnimFrame >= data.fEndFrame + 1.0)
			fAnimFrame = data.fEndFrame
			RETURN TRUE 
		ENDIF 
	ENDIF 
	
	RETURN FALSE 
ENDFUNC

PROC GGSM_RESET_FXSPRITE(GGSM_FX_SPRITE &spr)
	spr.eFlags = GGSM_FX_SPRITE_BIT_NONE
	spr.eSpriteAnim = GGSM_SPRITE_ANIM_NONE
	spr.eSpriteType = GGSM_SPRITE_NONE
	spr.fAnimFrame = 0
ENDPROC 

/// PURPOSE:
///    Sets all explosions to be inactive
/// PARAMS:
///    array - array of projectiles
PROC GGSM_RESET_FXSPRITE_ARRAY(GGSM_FX_SPRITE &array[])
	INT i
	INT cnt = COUNT_OF(array)
		
	REPEAT cnt i
		GGSM_RESET_FXSPRITE(array[i])
	ENDREPEAT 
ENDPROC

/// PURPOSE:
///    Gets the next free projectile index
/// PARAMS:
///    array - projecile array
///    ind - Index of the next free index
///    iSearchIndex - Index to start searching from
/// RETURNS:
///    TRUE if projectile can be found
FUNC BOOL GGSM_GET_FREE_FXSPRITE(GGSM_FX_SPRITE &array[], INT &ind, INT &iSearchIndex)
	INT i
	INT cnt = COUNT_OF(array)
	
	// stops any overflow nonsense
	iSearchIndex = ABSI(iSearchIndex) % cnt
	
	ind = GGSM_INVALID_FX_SPRITE
	REPEAT cnt i
		IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(array[iSearchIndex].eFlags, GGSM_FX_SPRITE_BIT_ACTIVE)
			ind = iSearchIndex
			RETURN TRUE
		ENDIF 
		
		iSearchIndex = (iSearchIndex + 1) % cnt
	ENDREPEAT 
	
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Update the projectile array
/// PARAMS:
///    array - array
/// RETURNS:
///    # of active projectiles
FUNC INT GGSM_UPDATE_FXSPRITE_ARRAY(GGSM_FX_SPRITE &array[])
	INT i, n
	INT cnt = COUNT_OF(array)
	VECTOR_2D vPos, vSize 
	FLOAT fDeltaTime = GET_FRAME_TIME() * sGGSMData.fTimeScale
	
	CLEAR_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_EXPLOSIONS_ACTIVE)
	
	REPEAT cnt i
		IF IS_BITMASK_ENUM_AS_ENUM_SET(array[i].eFlags, GGSM_FX_SPRITE_BIT_ACTIVE)
		
			// update movement and do on screen checks
			vPos = GGSM_UNPACK_VECTOR_2D(array[i].iPackedPosition)
			vSize = GGSM_GET_SPRITE_SIZE(array[i].eSpriteType)
			vSize.x *= array[i].fScale
			vSize.y *= array[i].fScale
			
			IF GGSM_IS_POINT_ON_SCREEN(vPos, vSize)
				SET_BITMASK_ENUM_AS_ENUM(array[i].eFlags, GGSM_FX_SPRITE_BIT_ENTERED_PLAYFIELD)
				SET_BITMASK_ENUM_AS_ENUM(array[i].eFlags, GGSM_FX_SPRITE_BIT_ON_SCREEN)
			ELSE 
				CLEAR_BITMASK_ENUM_AS_ENUM(array[i].eFlags, GGSM_FX_SPRITE_BIT_ON_SCREEN)
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(array[i].eFlags, GGSM_FX_SPRITE_BIT_REMOVE_WHEN_OFFSCREEN | GGSM_FX_SPRITE_BIT_ENTERED_PLAYFIELD)
					array[i].eFlags = GGSM_FX_SPRITE_BIT_NONE 
					RELOOP 
				ENDIF 
			ENDIF 
			
			vPos.x += array[i].vVelocity.x * fDeltaTime
			vPos.y += array[i].vVelocity.y * fDeltaTime
			array[i].iPackedPosition = GGSM_PACK_VECTOR_2D_TO_INT(vPos)
			
			IF (array[i].eSpriteAnim != GGSM_SPRITE_ANIM_NONE)
				IF GGSM_SPRITE_UPDATE_ANIMATION(array[i].eSpriteAnim, array[i].fAnimFrame)
					array[i].eSpriteAnim = GGSM_SPRITE_ANIM_NONE
					array[i].eFlags = GGSM_FX_SPRITE_BIT_NONE
				ENDIF 
			ENDIF
			
			n ++
		ENDIF
	ENDREPEAT 	
	
	RETURN n
ENDFUNC 

PROC GGSM_FXSPRITE_SET_PHYSICAL_RADIUS(GGSM_FX_SPRITE &spr, FLOAT fRadius)
	VECTOR_2D size = GGSM_GET_SPRITE_SIZE(spr.eSpriteType)
	spr.fScale = fRadius / size.x
ENDPROC 

FUNC BOOL GGSM_FXSPRITE_INIT_METEOR(GGSM_FX_SPRITE &sprite, VECTOR_2D vPos, VECTOR_2D vVelocity, FLOAT fPhysicalRadius = 64.0)
	sprite.eFlags = GGSM_FX_SPRITE_BIT_ACTIVE | GGSM_FX_SPRITE_BIT_REMOVE_WHEN_OFFSCREEN | GGSM_FX_SPRITE_BIT_BG_SPRITE
	
	IF (fPhysicalRadius < 40.0)
		sprite.eSpriteType = GGSM_SPRITE_METEOR_SMALL
	ELIF (fPhysicalRadius < 80.0)
		sprite.eSpriteType = GGSM_SPRITE_METEOR_MEDIUM
	ELSE 
		sprite.eSpriteType = GGSM_SPRITE_METEOR_BIG
	ENDIF 
		
	sprite.eSpriteAnim = GGSM_SPRITE_ANIM_NONE 
	sprite.iPackedPosition = GGSM_PACK_VECTOR_2D_TO_INT(vPos)
	sprite.vVelocity = vVelocity
	sprite.fAnimFrame = 0
	sprite.iPackedColor = GGSM_PACK_RGBA_COLOUR(32, 32, 32, 255)
	
	GGSM_FXSPRITE_SET_PHYSICAL_RADIUS(sprite, fPhysicalRadius)
	RETURN TRUE 
ENDFUNC

FUNC BOOL GGSM_FXSPRITE_INIT_ANIMATED(GGSM_FX_SPRITE &sprite, VECTOR_2D vPos, GGSM_SPRITE_ANIM eAnim, FLOAT fScale = 1.0, GGSM_FX_SPRITE_BIT eFlags = GGSM_FX_SPRITE_BIT_NONE)
	GGSM_SPRITE_ANIM_DATA data 
	
	IF NOT GGSM_GET_SPRITE_ANIM_DATA(eAnim, data)
		RETURN FALSE 
	ENDIF 
	
	sprite.eFlags = (GGSM_FX_SPRITE_BIT_ACTIVE | eFlags)
	sprite.eSpriteAnim = eAnim
	sprite.eSpriteType = data.eSprite
	sprite.iPackedPosition = GGSM_PACK_VECTOR_2D_TO_INT(vPos)
	sprite.fAnimFrame = data.fStartFrame
	sprite.fScale = fScale
	sprite.iPackedColor = GGSM_PACK_RGBA_COLOUR(255, 255, 255, 255)
	GGSM_PLAY_SOUND_FROM_POSITION(data.eSound, vPos)
	RETURN TRUE 
ENDFUNC 

//-------------------------------------------------
// BG SCROLLER FUNCTIONS
//-------------------------------------------------

PROC GGSM_BGSCROLLER_RESET(GGSM_BGSCROLLER &scr, INT iStage)
	INT i
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_BG_SPACE)
	VECTOR_2D vPos
	
	// set positions for stars
	vPos.x = GGSM_PX_OVERSCAN_MIN_X - (vSize.x / 2.0)
	vPos.y = cfBASE_SCREEN_HALF_HEIGHT
	
	REPEAT GGSM_MAX_STAR_BG i
		scr.vSpaceBgPos[i] = vPos 
		scr.vSpaceBgPos[i].x += (i * vSize.x)
	ENDREPEAT
	
	// set positions for floor 
	vSize = GGSM_GET_SPRITE_BG_LAYER_SIZE_FOR_STAGE(iStage)
	vPos.x = GGSM_PX_OVERSCAN_MIN_X - (vSize.x / 2.0)
	vPos.y = GGSM_PX_OVERSCAN_MAX_Y - (vSize.y / 2.0)

	REPEAT GGSM_MAX_FLOOR_BG i
		scr.vFloorBgPos[i] = vPos 
		scr.vFloorBgPos[i].x += (i * vSize.x)
	ENDREPEAT
	
	GGSM_RESET_FXSPRITE_ARRAY(scr.sBgSprite)
	
	scr.fSpaceScrollRate = 64.0 + (12.8 * TO_FLOAT(iStage - 1))
	scr.fFloorScrollRate = (scr.fSpaceScrollRate * 2.0)
	
	scr.fObjScrollRate = 96.0
	
	scr.iStage = iStage
	scr.fTimer = 0
ENDPROC 

PROC GGSM_BGSCROLLER_UPDATE(GGSM_BGSCROLLER &scr)
	IF (sGGSMData.bGamePaused)
		EXIT 
	ENDIF 
	
	INT i, iSearch
	FLOAT r 
	
	FLOAT xlimit
	FLOAT spd = (sGGSMData.fDeltaTime * sGGSMData.fTimeScale)
	VECTOR_2D vSize = GGSM_GET_SPRITE_SIZE(GGSM_SPRITE_BG_SPACE)

	// scroll stars 
	scr.fTimer -= spd 
	xlimit = GGSM_PX_OVERSCAN_MIN_X - (vSize.x / 2.0)
	spd = (sGGSMData.fDeltaTime * sGGSMData.fTimeScale) * scr.fSpaceScrollRate
	
	REPEAT GGSM_MAX_STAR_BG i
		scr.vSpaceBgPos[i].x -= spd
		IF (scr.vSpaceBgPos[i].x <= xlimit)
			scr.vSpaceBgPos[i].x += (vSize.x * GGSM_MAX_STAR_BG)
		ENDIF 
	ENDREPEAT 
	
	// scroll floor bg 
	xlimit = GGSM_PX_OVERSCAN_MIN_X - (vSize.x / 2.0)
	spd = (sGGSMData.fDeltaTime * sGGSMData.fTimeScale) * scr.fFloorScrollRate
	vSize = GGSM_GET_SPRITE_BG_LAYER_SIZE_FOR_STAGE(scr.iStage)
	
	REPEAT GGSM_MAX_FLOOR_BG i
		scr.vFloorBgPos[i].x -= spd
		IF (scr.vFloorBgPos[i].x <= xlimit)
			scr.vFloorBgPos[i].x += (vSize.x * GGSM_MAX_FLOOR_BG)
		ENDIF 
	ENDREPEAT 	
	
	// update scrolling objects
	GGSM_UPDATE_FXSPRITE_ARRAY(scr.sBgSprite)	
	VECTOR_2D vPos, vVel  
	
	IF (scr.fTimer > 0.0)
		EXIT 
	ENDIF 
	
	IF (scr.iStage = GGSM_SECTOR_ASTEROID_BELT)
		IF GGSM_GET_FREE_FXSPRITE(scr.sBgSprite, i, iSearch)		
			vVel = INIT_VECTOR_2D(-GET_RANDOM_FLOAT_IN_RANGE(300.0, 700.0), 0)		
			r = GET_RANDOM_FLOAT_IN_RANGE(32.0, 512.0)
			vPos.x = GGSM_PX_OVERSCAN_MAX_X + (r / 2.0)
			vPos.y = GET_RANDOM_FLOAT_IN_RANGE(GGSM_PX_OVERSCAN_MIN_Y + 20, GGSM_PX_OVERSCAN_MAX_Y - 20)
			GGSM_FXSPRITE_INIT_METEOR(scr.sBgSprite[i], vPos, vVel, r)			
			scr.fTimer = GET_RANDOM_FLOAT_IN_RANGE(1.0, 5.0)
		ENDIF
	ENDIF 
ENDPROC 

//-------------------------------------------------
// DIALOG CONTROLLER FUNCTIONS
//-------------------------------------------------
PROC GGSM_DIALOG_CONTROLLER_SETUP_PICTURES(GGSM_DIALOG_CONTROLLER &con)
	ARCADE_GAMES_SOUND monkeyVoice = ARCADE_GAMES_SOUND_GGSM_MONKEY_VOICE_SCREECH_MID
	
	con.eLeftSpeak = ARCADE_GAMES_SOUND_END
	con.eRightSpeak = ARCADE_GAMES_SOUND_END
	
	SWITCH (con.eType)
		CASE GGSM_DIALOG_BOSS_SMOOTHIE_START
		CASE GGSM_DIALOG_BOSS_SMOOTHIE_FAILED
			con.eLeftPicture = GGSM_SPRITE_PIC_SMOOTHIE_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_SMOOTHIE_SPEAK
			con.eRightSpeak = monkeyVoice
		BREAK 

		CASE GGSM_DIALOG_BOSS_SMOOTHIE_DEFEATED
			con.eLeftPicture = GGSM_SPRITE_PIC_SMOOTHIE_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_SMOOTHIE_SPEAK_DEFEAT
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_MARINE_DEFEATED
			con.eLeftPicture = GGSM_SPRITE_PIC_MONKEY_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MARINE_MAIN		
			con.eLeftSpeak = monkeyVoice
			con.eRightSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_MARINE_SPEAK_DEFEAT
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_MARINE_START
		CASE GGSM_DIALOG_BOSS_MARINE_FAILED
			con.eLeftPicture = GGSM_SPRITE_PIC_MARINE_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_MARINE_SPEAK
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_GRANANA_START
		CASE GGSM_DIALOG_BOSS_GRANANA_FAILED
			con.eLeftPicture = GGSM_SPRITE_PIC_GRANANA_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN	
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_GRAN_SPEAK
			con.eRightSpeak = monkeyVoice
		BREAK 

		CASE GGSM_DIALOG_BOSS_GRANANA_DEFEATED
			con.eLeftPicture = GGSM_SPRITE_PIC_GRANANA_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN	
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_GRAN_SPEAK_DEFEAT
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_BREAD_START
		CASE GGSM_DIALOG_BOSS_BREAD_FAILED
			con.eLeftPicture = GGSM_SPRITE_PIC_BREAD_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_BREAD_SPEAK
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_BREAD_DEFEATED
			con.eLeftPicture = GGSM_SPRITE_PIC_BREAD_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_BREAD_SPEAK_DEFEAT
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_DR_DANK_START
		CASE GGSM_DIALOG_BOSS_DR_DANK_FAILED
			con.eLeftPicture = GGSM_SPRITE_PIC_DR_DANK_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_DR_DANK_SPEAK
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_DR_DANK_DEFEATED
			con.eLeftPicture = GGSM_SPRITE_PIC_DR_DANK_MAIN
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_DR_DANK_SPEAK_DEFEAT
			con.eRightSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_ASTEROID_ENTER
		CASE GGSM_DIALOG_ASTEROID_LEAVE
			con.eLeftPicture = GGSM_SPRITE_PIC_MONKEY_ASTEROID
			con.eRightPicture = GGSM_SPRITE_NONE
			con.eLeftSpeak = monkeyVoice
		BREAK 
		
		CASE GGSM_DIALOG_GAME_OVER
			con.eLeftPicture = GGSM_SPRITE_PIC_DR_DANK_FAIL
			con.eLeftSpeak = ARCADE_GAMES_SOUND_GGSM_BOSS_DR_DANK_SPEAK
			con.eRightPicture = GGSM_SPRITE_PIC_MONKEY_FAIL
			con.eRightSpeak = ARCADE_GAMES_SOUND_GGSM_MONKEY_VOICE_SCREECH_HIGH
		BREAK 
		
		DEFAULT 
			con.eLeftPicture = GGSM_SPRITE_PIC_MONKEY_MAIN		
			con.eRightPicture = GGSM_SPRITE_PIC_GRANANA_MAIN
		BREAK 
	ENDSWITCH 
ENDPROC 

FUNC BOOL GGSM_DIALOG_CONTROLLER_SETUP_MESSAGES(GGSM_DIALOG_CONTROLLER &con)
	con.iMsgTimerMS = GET_GAME_TIMER() + GGSM_DIALOG_READ_TIME
	
	SWITCH (con.eType)
		CASE GGSM_DIALOG_BOSS_SMOOTHIE_START
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL1_BOSS_START		
			con.eRightDialog = GGSM_SPRITE_BOX_LVL1_MONKEY_START
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_SMOOTHIE_DEFEATED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL1_BOSS_WIN

			con.eRightDialog = GGSM_SPRITE_BOX_LVL1_MONKEY_WIN
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_SMOOTHIE_FAILED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL1_BOSS_FAIL
			con.eRightDialog = GGSM_SPRITE_BOX_LVL1_MONKEY_FAIL
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		

		CASE GGSM_DIALOG_BOSS_MARINE_START
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL2_BOSS_START
			con.eRightDialog = GGSM_SPRITE_BOX_LVL2_MONKEY_START
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_MARINE_DEFEATED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL2_BOSS_WIN
			con.eRightDialog = GGSM_SPRITE_BOX_LVL2_MONKEY_WIN
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_MARINE_FAILED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL2_BOSS_FAIL
			con.eRightDialog = GGSM_SPRITE_BOX_LVL2_MONKEY_FAIL
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_GRANANA_START
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL3_BOSS_START
			con.eRightDialog = GGSM_SPRITE_BOX_LVL3_MONKEY_START
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_GRANANA_DEFEATED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL3_BOSS_WIN
			con.eRightDialog = GGSM_SPRITE_BOX_LVL3_MONKEY_WIN
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_GRANANA_FAILED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL3_BOSS_FAIL
			con.eRightDialog = GGSM_SPRITE_BOX_LVL3_MONKEY_FAIL
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_BREAD_START
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL4_BOSS_START
			con.eRightDialog = GGSM_SPRITE_BOX_LVL4_MONKEY_START
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_BREAD_DEFEATED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL4_BOSS_WIN
			con.eRightDialog = GGSM_SPRITE_BOX_LVL4_MONKEY_WIN
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_BREAD_FAILED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL4_BOSS_FAIL
			con.eRightDialog = GGSM_SPRITE_BOX_LVL4_MONKEY_FAIL
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_DR_DANK_START
			IF (con.iMsgNumber = 0)
				con.eLeftDialog = GGSM_SPRITE_BOX_LVL5_BOSS_START
				con.eRightDialog = GGSM_SPRITE_BOX_LVL5_MONKEY_START
			ELIF (con.iMsgNumber = 1)
				con.eLeftDialog = GGSM_SPRITE_BOX_LVL5_BOSS_START2
				con.eRightDialog = GGSM_SPRITE_BOX_LVL5_MONKEY_START2
			ELIF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_DR_DANK_DEFEATED
			IF (con.iMsgNumber = 0)
				con.eLeftDialog = GGSM_SPRITE_BOX_LVL5_BOSS_WIN
				con.eRightDialog = GGSM_SPRITE_BOX_LVL5_MONKEY_WIN
			ELIF (con.iMsgNumber = 1)
				con.eLeftDialog = GGSM_SPRITE_BOX_LVL5_BOSS_WIN2
				con.eRightDialog = GGSM_SPRITE_NONE
			ELIF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_BOSS_DR_DANK_FAILED
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL5_BOSS_FAIL
			con.eRightDialog = GGSM_SPRITE_BOX_LVL5_MONKEY_FAIL

			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_ASTEROID_ENTER
			con.eLeftDialog = GGSM_SPRITE_BOX_ASTEROID_NEARING
			con.eRightDialog = GGSM_SPRITE_NONE
			
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_ASTEROID_LEAVE
			con.eLeftDialog = GGSM_SPRITE_BOX_ASTEROID_LEAVING
			con.eRightDialog = GGSM_SPRITE_NONE
			
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_GAME_OVER
			con.eLeftDialog = INT_TO_ENUM(GGSM_SPRITE_TYPE, ENUM_TO_INT(GGSM_SPRITE_BOX_FAILSCRN_BOSS1) + con.iVariation)
			con.eRightDialog = INT_TO_ENUM(GGSM_SPRITE_TYPE, ENUM_TO_INT(GGSM_SPRITE_BOX_FAILSCRN_MONKEY_1) + con.iVariation)
			
			IF (con.iMsgNumber != 0)
				RETURN FALSE 
			ENDIF 
		BREAK 
		
		DEFAULT 
			con.eLeftDialog = GGSM_SPRITE_BOX_LVL3_MONKEY_START
			con.eRightDialog = GGSM_SPRITE_BOX_LVL3_BOSS_START
		BREAK 
	ENDSWITCH 
	
	RETURN TRUE 
ENDFUNC 

PROC GGSM_DIALOG_CONTROLLER_SET_STATE(GGSM_DIALOG_CONTROLLER &con, GGSM_DIALOG_STATE eState)
	con.eState = eState 
	con.bStateInit = FALSE 
	con.iMsgTimerMS = GET_GAME_TIMER() + GGSM_DIALOG_READ_TIME
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_DIALOG_CONTROLLER_SET_STATE:", GGSM_DIALOG_STATE_TO_STRING(eState))
ENDPROC 

PROC GGSM_DIALOG_CONTROLLER_START_DIALOG(GGSM_DIALOG_CONTROLLER &con, GGSM_DIALOG_TYPE eType, INT iVariation = -1)
	con.eType = eType
	
	IF (iVariation = -1) AND (con.eType = GGSM_DIALOG_GAME_OVER)
		iVariation = GET_RANDOM_INT_IN_RANGE(0, 6)
	ENDIF 
	
	con.iVariation = iVariation
	con.iMsgNumber = 0
	GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_INIT)
ENDPROC 

PROC GGSM_DIALOG_CONTROLLER_UPDATE(GGSM_DIALOG_CONTROLLER &con)
	IF NOT GGSM_IS_DIALOG_ACTIVE()
		EXIT 
	ENDIF 
	
	VECTOR_2D vPos, vSize  
	RGBA_COLOUR_STRUCT bgcol 
	INT iAlpha
	STRING sPortraitDict = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_PORTRAITS)
	STRING sDialogDict = GGSM_GET_TEXTURE_DICT(GGSM_TEXDICT_DIALOG)
	
	SWITCH (con.eState)
		CASE GGSM_DIALOG_STATE_INIT
			con.iMsgNumber = 0
			con.iSlideInOffset = GGSM_DIALOG_MAX_SLIDEIN
			//GGSM_RESET_PROJECTILE_ARRAY(sGGSMData.sProjectiles)
			GGSM_DIALOG_CONTROLLER_SETUP_PICTURES(con)
			GGSM_DIALOG_CONTROLLER_SETUP_MESSAGES(con)		
			GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_START)
			
			SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BRING_CAMERA_TO_STOP)
		BREAK 
		
		CASE GGSM_DIALOG_STATE_START
			IF NOT GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_PORTRAITS)
				EXIT 
			ENDIF 
			
			IF NOT GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_DIALOG)
				EXIT 
			ENDIF 
			
			IF (con.bStateInit = FALSE)
				GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND_GGSM_SPEECH_BUBBLE_APPEAR_SWEEP)
				con.bStateInit = TRUE
			ENDIF 
			
			con.iSlideInOffset -= GGSM_DIALOG_SLIDEIN_RATE
			IF (con.iSlideInOffset <= 0)
				GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND_GGSM_SPEECH_BUBBLE_APPEAR_SYNTH)
				GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_LEFT_SPEAK)
				con.iSlideInOffset = 0
			ENDIF 		
		BREAK 
		
		CASE GGSM_DIALOG_STATE_LEFT_SPEAK
			IF (con.bStateInit = FALSE)			
				IF ARCADE_GAMES_SOUND_HAS_FINISHED(ARCADE_GAMES_SOUND_GGSM_SPEECH_BUBBLE_APPEAR_SYNTH)					
					IF (con.eLeftSpeak != ARCADE_GAMES_SOUND_END)
						GGSM_PLAY_SOUND(con.eLeftSpeak)
					ENDIF 
					con.bStateInit = TRUE
				ENDIF 
			ELSE 
				IF (con.eLeftSpeak = ARCADE_GAMES_SOUND_END) OR ARCADE_GAMES_SOUND_HAS_FINISHED(con.eLeftSpeak)
					GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_RIGHT_SPEAK)
				ENDIF 
			ENDIF
		BREAK 
		
		CASE GGSM_DIALOG_STATE_RIGHT_SPEAK
			IF (con.bStateInit = FALSE)		
				IF (con.eRightSpeak != ARCADE_GAMES_SOUND_END)
					GGSM_PLAY_SOUND(con.eRightSpeak)
				ENDIF 
				con.bStateInit = TRUE
			ELSE 
				IF (con.eRightSpeak = ARCADE_GAMES_SOUND_END) OR ARCADE_GAMES_SOUND_HAS_FINISHED(con.eRightSpeak)
					GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_ACTIVE)
				ENDIF 
			ENDIF
		BREAK 
		
		CASE GGSM_DIALOG_STATE_ACTIVE
			// we jump to the next message if there isn't one we end the dialog
			IF GGSM_HAS_PLAYER_PRESSED_FIRE() OR (GET_GAME_TIMER() > con.iMsgTimerMS)
				con.iMsgNumber ++
				IF NOT GGSM_DIALOG_CONTROLLER_SETUP_MESSAGES(con)		
					GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_END)
				ELSE 
					GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND_GGSM_SPEECH_BUBBLE_APPEAR_SYNTH)
					GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_LEFT_SPEAK)
					CDEBUG1LN(DEBUG_MINIGAME, "GGSM_DIALOG_CONTROLLER_UPDATE - TYPE:", GGSM_DIALOG_TYPE_TO_STRING(con.eType), " MSG #:", con.iMsgNumber)
				ENDIF
			ENDIF 			
			
			IF GGSM_HAS_PLAYER_PRESSED_START()
				GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_END)
			ENDIF 
		BREAK 
		
		CASE GGSM_DIALOG_STATE_END
			IF (con.bStateInit = FALSE)
				GGSM_PLAY_SOUND(ARCADE_GAMES_SOUND_GGSM_SPEECH_BUBBLE_DISAPPEAR_SWEEP)
				con.bStateInit = TRUE
			ENDIF 
			
			con.iSlideInOffset += GGSM_DIALOG_SLIDEIN_RATE
			IF (con.iSlideInOffset >= GGSM_DIALOG_MAX_SLIDEIN)
				con.iSlideInOffset = GGSM_DIALOG_MAX_SLIDEIN
				
				con.eLeftDialog = GGSM_SPRITE_NONE
				con.eRightDialog = GGSM_SPRITE_NONE
				con.eLeftPicture = GGSM_SPRITE_NONE
				con.eRightPicture = GGSM_SPRITE_NONE
				GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_DONE)
				CLEAR_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BRING_CAMERA_TO_STOP)
				GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_DIALOG)
				EXIT
			ENDIF 
		BREAK 		
		
		CASE GGSM_DIALOG_STATE_DONE	
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_DIALOG)
			GGSM_DIALOG_CONTROLLER_SET_STATE(con, GGSM_DIALOG_STATE_SHUTDOWN)
			EXIT
		BREAK 
	ENDSWITCH 
	
	// draw transparent bg if in stage
	IF (con.eType > GGSM_DIALOG_GAME_OVER)
		iAlpha = FLOOR(128.0 * (TO_FLOAT(GGSM_DIALOG_MAX_SLIDEIN - con.iSlideInOffset) / TO_FLOAT(GGSM_DIALOG_MAX_SLIDEIN)))
		INIT_RGBA_STRUCT(bgcol, 0, 0, 0, iAlpha)
		ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_HALF_WIDTH, cfBASE_SCREEN_HALF_HEIGHT), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), bgcol)
	ENDIF 
	
	// draw left picture 
	IF (con.eLeftPicture != GGSM_SPRITE_NONE)
		vSize = GGSM_GET_SPRITE_SIZE(con.eLeftPicture)
		vPos.x = (GGSM_PX_HUD_MIN_X + (vSize.x / 2) + 120) - con.iSlideInOffset
		vPos.y = GGSM_PX_HUD_MIN_Y + (vSize.y / 2) + 120		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sPortraitDict, GGSM_GET_SPRITE_NAME(con.eLeftPicture), vPos, vSize, 0, sGGSMData.rgbaWhite)
		vPos.x += ((vSize.x / 2) + 32)
		
		// draw left text 
		IF (con.iSlideInOffset = 0) AND (con.eLeftDialog != GGSM_SPRITE_NONE)
			vSize = GGSM_GET_SPRITE_SIZE(con.eLeftDialog)
			
			IF (con.eType <= GGSM_DIALOG_GAME_OVER)
				vPos.x += (328.0 / 2.0)
			ELSE 
				vPos.x += (628.0 / 2.0)
			ENDIF 
			
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDialogDict, GGSM_GET_SPRITE_NAME(con.eLeftDialog), vPos, vSize, 0, sGGSMData.rgbaWhite)
		ENDIF
	ENDIF
	
	// draw right picture 
	IF (con.eRightPicture != GGSM_SPRITE_NONE)
		vSize = GGSM_GET_SPRITE_SIZE(con.eRightPicture)
		vPos.x = (GGSM_PX_HUD_MAX_X - (vSize.x / 2) - 120) + con.iSlideInOffset
		
		IF (con.eType <= GGSM_DIALOG_GAME_OVER)
			vPos.y = 420.0
		ELSE 
			vPos.y = GGSM_PX_HUD_MAX_Y - (vSize.y / 2) - 120
		ENDIF 
		ARCADE_DRAW_PIXELSPACE_SPRITE(sPortraitDict, GGSM_GET_SPRITE_NAME(con.eRightPicture), vPos, vSize, 0, sGGSMData.rgbaWhite)
		vPos.x -= ((vSize.x / 2) + 32)
		
		// draw right text 
		IF (con.iSlideInOffset = 0) AND (con.eRightDialog != GGSM_SPRITE_NONE)
			vSize = GGSM_GET_SPRITE_SIZE(con.eRightDialog)
		
			IF (con.eType <= GGSM_DIALOG_GAME_OVER)
				vPos.x -= (328.0 / 2.0)
			ELSE 
				vPos.x -= (628.0 / 2.0)
			ENDIF 
			
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDialogDict, GGSM_GET_SPRITE_NAME(con.eRightDialog), vPos, vSize, 0, sGGSMData.rgbaWhite)
		ENDIF
	ENDIF
ENDPROC 

//-------------------------------------------------
// POWER UP FUNCTIONS
//-------------------------------------------------

PROC GGSM_RESET_POWER_UP(GGSM_POWER_UP &pType)
	pType.eType = GGSM_SPRITE_NONE
	pType.iPackedPosition = GGSM_PACK_VECTOR_2D_TO_INT_EXPLICIT(1800, 540)
ENDPROC 

PROC GGSM_RESET_POWER_UP_ARRAY(GGSM_POWER_UP &pArray[])
	INT i
	
	REPEAT COUNT_OF(pArray) i
		GGSM_RESET_POWER_UP(pArray[i])
	ENDREPEAT 
ENDPROC 

FUNC INT GGSM_GET_FREE_POWER_UP(GGSM_POWER_UP &pArray[])
	INT i
	
	REPEAT COUNT_OF(pArray) i
		IF (pArray[i].eType = GGSM_SPRITE_NONE)
			RETURN i
		ENDIF 
	ENDREPEAT 
	
	RETURN -1
ENDFUNC 

FUNC BOOL GGSM_CREATE_POWER_UP(GGSM_SPRITE_TYPE eSprite, GGSM_POWER_UP &pArray[])
	IF (eSprite < GGSM_SPRITE_POWER_UP_BEAM) OR (eSprite > GGSM_SPRITE_POWER_UP_WAVE)
		RETURN FALSE 	
	ENDIF 

	INT ind = GGSM_GET_FREE_POWER_UP(pArray)
	IF (ind = -1)
		RETURN FALSE 
	ENDIF 
	
	pArray[ind].eType = eSprite 
	pArray[ind].iPackedPosition = GGSM_PACK_VECTOR_2D_TO_INT_EXPLICIT(1800, GET_RANDOM_FLOAT_IN_RANGE(200.0, 840.0))
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_CREATE_POWER_UP - TYPE:", GGSM_GET_SPRITE_NAME(eSprite))
	RETURN TRUE 
ENDFUNC 

FUNC BOOL GGSM_CREATE_POWER_UP_AT(GGSM_SPRITE_TYPE eSprite, VECTOR_2D vPos, GGSM_POWER_UP &pArray[])
	IF (eSprite < GGSM_SPRITE_POWER_UP_BEAM) OR (eSprite > GGSM_SPRITE_POWER_UP_WAVE)
		RETURN FALSE 	
	ENDIF 

	INT ind = GGSM_GET_FREE_POWER_UP(pArray)
	IF (ind = -1)
		RETURN FALSE 
	ENDIF 
	
	pArray[ind].eType = eSprite 
	pArray[ind].iPackedPosition = GGSM_PACK_VECTOR_2D_TO_INT(vPos)
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_CREATE_POWER_UP - TYPE:", GGSM_GET_SPRITE_NAME(eSprite))
	RETURN TRUE 
ENDFUNC 

//-------------------------------------------------
// ENTITY ACCESSOR FUNCTIONS
//-------------------------------------------------	

FUNC INT GGSM_ENTITY_GET_ARRAY_INDEX(GGSM_ENTITY &ent)
	RETURN ent.iArrayIndex
ENDFUNC 

FUNC INT GGSM_ENTITY_GET_GROUP_ID(GGSM_ENTITY &ent)
	RETURN ent.iGroupID
ENDFUNC 

FUNC INT GGSM_ENTITY_GET_PARENT_INDEX(GGSM_ENTITY &ent)
	RETURN ent.iEntityParentIndex
ENDFUNC 

PROC GGSM_ENTITY_SET_ARRAY_INDEX(GGSM_ENTITY &ent, INT i)
	ent.iArrayIndex = i
ENDPROC

PROC GGSM_ENTITY_SET_GROUP_ID(GGSM_ENTITY &ent, INT i)
	ent.iGroupID = i
ENDPROC 

PROC GGSM_ENTITY_SET_PARENT_INDEX(GGSM_ENTITY &ent, INT i)
	ent.iEntityParentIndex = i
ENDPROC

//-------------------------------------------------
// BOSS CONTROLLER FUNCTIONS
//-------------------------------------------------

PROC GGSM_BOSS_CONTROLLER_SET_STATE(GGSM_BOSS_CONTROLLER &con, GGSM_BOSS_STATE eState)
	con.eState = eState
	con.bStateInit = FALSE
	con.fStateTimer = 0.0
	con.iInternalCounter = 0
	con.iBossPatternState = 0
	
	CDEBUG1LN(DEBUG_MINIGAME, "GGSM_BOSS_CONTROLLER_SET_STATE - TYPE:", GGSM_BOSS_TYPE_TO_STRING(con.eType), " STATE:", GGSM_BOSS_STATE_TO_STRING(con.eState))
	
	IF (eState = GGSM_BOSS_STATE_LOADING)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BRING_CAMERA_TO_STOP)
	ENDIF
ENDPROC 

FUNC BOOL GGSM_BOSS_CONTROLLER_IS_BOSS_ACTIVE(GGSM_BOSS_CONTROLLER &boss)
	IF (boss.eState = GGSM_BOSS_STATE_NONE) OR (boss.eState = GGSM_BOSS_STATE_DONE)
		RETURN FALSE 
	ENDIF 
	
	RETURN FALSE
ENDFUNC 

FUNC INT GGSM_GET_FREE_BOSS_CONTROLLER_INDEX(GGSM_BOSS_CONTROLLER &boss[])
	INT i
	
	REPEAT COUNT_OF(boss) i
		IF (boss[i].iMaxTotalHP = 0)
			RETURN i
		ENDIF 
	ENDREPEAT 
	
	RETURN GGSM_INVALID_BOSS_CONTROLLER 
ENDFUNC

FUNC FLOAT GGSM_GET_BOSS_STATE_TIME_ELAPSED(GGSM_BOSS_CONTROLLER &boss)
	RETURN boss.fStateTimer
ENDFUNC 

FUNC INT GGSM_BOSS_CONTROLLER_GET_ENTITY_INDEX(GGSM_BOSS_CONTROLLER &boss, INT iSlot)
	IF (iSlot < 0) OR (iSlot >= GGSM_MAX_BOSS_PARTS)
		RETURN GGSM_INVALID_ENTITY
	ENDIF 
	
	RETURN boss.iEntityIndex[iSlot]
	//RETURN GET_PACKED_BITFIELD_VALUE_FROM_ARRAY(boss.iPackedEntityList, iSlot, GGSM_BOSS_LIST_BIT_WIDTH)
ENDFUNC 

PROC GGSM_BOSS_CONTROLLER_SET_ENTITY_INDEX(GGSM_BOSS_CONTROLLER &boss, INT iSlot, INT iEntityIndex)
	IF (iSlot < 0) OR (iSlot >= GGSM_MAX_BOSS_PARTS)
		EXIT
	ENDIF 
	
	boss.iEntityIndex[iSlot] = iEntityIndex
	//SET_PACKED_BITFIELD_VALUE_IN_ARRAY(boss.iPackedEntityList, iSlot, GGSM_BOSS_LIST_BIT_WIDTH, iEntityIndex)
ENDPROC

PROC GGSM_CLEAR_BOSS_CONTROLLER_USED_PATTERN_BITSET(GGSM_BOSS_CONTROLLER &boss)
	boss.iPatternUsedBitSet = 0
ENDPROC

FUNC BOOL GGSM_BOSS_CONTROLLER_ARE_ANY_PATTERNS_LEFT(GGSM_BOSS_CONTROLLER &boss)
	INT i
	
	REPEAT boss.iMaxPatterns i
		IF NOT IS_BIT_SET(boss.iPatternUsedBitSet, i)	
			RETURN TRUE
		ENDIF 
	ENDREPEAT 
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL GGSM_BOSS_CONTROLLER_PICK_UNIQUE_PATTERN(GGSM_BOSS_CONTROLLER &boss, GGSM_BOSS_STATE &newPattern)
	INT i, iRand
	
	REPEAT 30 i
		iRand = GET_RANDOM_INT_IN_RANGE(0, boss.iMaxPatterns)
		IF NOT IS_BIT_SET(boss.iPatternUsedBitSet, iRand)
			newPattern = INT_TO_ENUM(GGSM_BOSS_STATE, ENUM_TO_INT(GGSM_BOSS_STATE_PATTERN_A) + iRand)
			SET_BIT(boss.iPatternUsedBitSet, iRand)
			RETURN TRUE 
		ENDIF 
	ENDREPEAT 
	
	REPEAT boss.iMaxPatterns i
		IF NOT IS_BIT_SET(boss.iPatternUsedBitSet, i)
			newPattern = INT_TO_ENUM(GGSM_BOSS_STATE, ENUM_TO_INT(GGSM_BOSS_STATE_PATTERN_A) + i)
			SET_BIT(boss.iPatternUsedBitSet, i)
			RETURN TRUE 
		ENDIF
	ENDREPEAT 
	
	RETURN FALSE
ENDFUNC 

PROC GGSM_BOSS_CONTROLLER_SET_ATTACK_PATTERN(GGSM_BOSS_CONTROLLER &boss, GGSM_BOSS_STATE newState)
	GGSM_BOSS_CONTROLLER_SET_STATE(boss, newState)
	
	IF NOT GGSM_BOSS_CONTROLLER_ARE_ANY_PATTERNS_LEFT(boss)
		GGSM_CLEAR_BOSS_CONTROLLER_USED_PATTERN_BITSET(boss)
		SET_BIT(boss.iPatternUsedBitSet, ENUM_TO_INT(newState) - ENUM_TO_INT(GGSM_BOSS_STATE_PATTERN_A))
	ENDIF 
ENDPROC

PROC GGSM_BOSS_CONTROLLER_SELECT_ATTACK_PATTERN(GGSM_BOSS_CONTROLLER &boss)
	GGSM_BOSS_STATE newState 
	
	IF NOT GGSM_BOSS_CONTROLLER_ARE_ANY_PATTERNS_LEFT(boss)
		GGSM_CLEAR_BOSS_CONTROLLER_USED_PATTERN_BITSET(boss)
	ENDIF 
	
	GGSM_BOSS_CONTROLLER_PICK_UNIQUE_PATTERN(boss, newState)
	GGSM_BOSS_CONTROLLER_SET_ATTACK_PATTERN(boss, newState)
ENDPROC

PROC GGSM_RESET_BOSS_CONTROLLER(GGSM_BOSS_CONTROLLER &boss, GGSM_BOSS_TYPE eType = GGSM_BOSS_NONE)
	INT i 
	
	REPEAT GGSM_MAX_BOSS_PARTS i
		GGSM_BOSS_CONTROLLER_SET_ENTITY_INDEX(boss, i, GGSM_INVALID_ENTITY)
	ENDREPEAT 
	
	boss.eType = eType
	boss.iTotalHP = 0
	boss.iMaxTotalHP = 0
	boss.bFiftyPercent = FALSE 
	boss.iGroupID = GGSM_INVALID_GROUP_ID
	boss.iSubGroupID = GGSM_INVALID_GROUP_ID
	boss.eState = GGSM_BOSS_STATE_NONE
	boss.iSummedMaxHPBitSet = 0
	boss.iPatternUsedBitSet = 0
	boss.iMaxPatterns = 5
	
	CPRINTLN(DEBUG_MINIGAME, "GGSM_RESET_BOSS_CONTROLLER:", GGSM_BOSS_TYPE_TO_STRING(eType))
	DEBUG_PRINTCALLSTACK()
ENDPROC 

FUNC GGSM_SPRITE_TYPE GGSM_GET_BOSS_CONTROLLER_PORTRAIT(GGSM_BOSS_TYPE eType)
	SWITCH (eType)
		CASE GGSM_BOSS_BREAD				RETURN GGSM_SPRITE_PIC_BREAD_64
		CASE GGSM_BOSS_DR_DANK				RETURN GGSM_SPRITE_PIC_DR_DANK_64
		CASE GGSM_BOSS_GRANANA				RETURN GGSM_SPRITE_PIC_GRANANA_64
		CASE GGSM_BOSS_MARINE				RETURN GGSM_SPRITE_PIC_MARINE_64
		CASE GGSM_BOSS_SMOOTHIE				RETURN GGSM_SPRITE_PIC_SMOOTHIE_64
	ENDSWITCH 
	
	RETURN GGSM_SPRITE_BOSS_TEXT
ENDFUNC 

FUNC BOOL GGSM_LOAD_TEXTURE_DICTS_FOR_BOSS(GGSM_BOSS_TYPE eType)
	IF NOT GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_PORTRAITS)
		RETURN FALSE 
	ENDIF 
	
	SWITCH (eType)
		CASE GGSM_BOSS_BREAD
			RETURN GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_BOSS_BREAD)
		BREAK 
		
		CASE GGSM_BOSS_DR_DANK
			RETURN GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_BOSS_DR_DANK)
		BREAK 
		
		CASE GGSM_BOSS_GRANANA
			RETURN GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_BOSS_GRANANA)
		BREAK 
		
		CASE GGSM_BOSS_MARINE
			IF NOT GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_BOSS_MARINE)
				RETURN FALSE 
			ENDIF 
			
			RETURN GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_BOSS_ALIEN)
		BREAK 
		
		CASE GGSM_BOSS_SMOOTHIE
			RETURN GGSM_LOAD_TEXTURE_DICT(GGSM_TEXDICT_BOSS_SMOOTHIE)
		BREAK 			
	ENDSWITCH
	
	RETURN FALSE 
ENDFUNC 

PROC GGSM_RELEASE_TEXTURE_DICTS_FOR_BOSS(GGSM_BOSS_TYPE eType)
	SWITCH (eType)
		CASE GGSM_BOSS_BREAD
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_BOSS_BREAD)
		BREAK 
		
		CASE GGSM_BOSS_DR_DANK
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_BOSS_DR_DANK)
		BREAK 
		
		CASE GGSM_BOSS_GRANANA
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_BOSS_GRANANA)
		BREAK 
		
		CASE GGSM_BOSS_MARINE
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_BOSS_MARINE)
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_BOSS_ALIEN)
		BREAK 
		
		CASE GGSM_BOSS_SMOOTHIE
			GGSM_RELEASE_TEXTURE_DICT(GGSM_TEXDICT_BOSS_SMOOTHIE)
		BREAK 			
	ENDSWITCH 
ENDPROC

FUNC INT GGSM_BOSS_CONTROLLER_GET_ACTIVE_PART_COUNT(GGSM_BOSS_CONTROLLER &boss)
	INT i, cnt, ent 
	
	REPEAT GGSM_MAX_BOSS_PARTS i
		ent = GGSM_BOSS_CONTROLLER_GET_ENTITY_INDEX(boss, i)
		IF (ent != GGSM_INVALID_ENTITY) AND (sGGSMData.sEntities[ent].eState = GGSM_ENTITY_STATE_ALIVE)
			cnt ++
		ENDIF 
	ENDREPEAT 
	
	RETURN cnt
ENDFUNC 

PROC GGSM_BOSS_CONTROLLER_DEACTIVATE_ALL_WEAPONS(GGSM_BOSS_CONTROLLER &boss)
	INT i, ent 
	
	REPEAT GGSM_MAX_BOSS_PARTS i
		ent = GGSM_BOSS_CONTROLLER_GET_ENTITY_INDEX(boss, i)
		IF (ent != GGSM_INVALID_ENTITY) AND (sGGSMData.sEntities[ent].eState = GGSM_ENTITY_STATE_ALIVE)
			CLEAR_BITMASK_ENUM_AS_ENUM(sGGSMData.sEntities[ent].eFlags, GGSM_ENTITY_BIT_WPN_ACTIVE)
		ENDIF 
	ENDREPEAT 
ENDPROC

PROC GGSM_BOSS_CONTROLLER_DACTIVATE_ALL_WEAPONS(GGSM_BOSS_CONTROLLER &boss)
	INT i, ent 
	
	REPEAT GGSM_MAX_BOSS_PARTS i
		ent = GGSM_BOSS_CONTROLLER_GET_ENTITY_INDEX(boss, i)
		IF (ent != GGSM_INVALID_ENTITY) AND (sGGSMData.sEntities[ent].eState = GGSM_ENTITY_STATE_ALIVE)
			SET_BITMASK_ENUM_AS_ENUM(sGGSMData.sEntities[ent].eFlags, GGSM_ENTITY_BIT_WPN_ACTIVE)
		ENDIF 
	ENDREPEAT 
ENDPROC

FUNC INT GGSM_BOSS_CONTROLLER_GET_HP(GGSM_BOSS_CONTROLLER &boss)
	INT i, cnt, ent 
	
	REPEAT GGSM_MAX_BOSS_PARTS i
		ent = GGSM_BOSS_CONTROLLER_GET_ENTITY_INDEX(boss, i)
		IF IS_BIT_SET(boss.iSummedMaxHPBitSet, i)
			IF (ent != GGSM_INVALID_ENTITY) AND (sGGSMData.sEntities[ent].eState = GGSM_ENTITY_STATE_ALIVE)
				cnt += sGGSMData.sEntities[ent].iHP
			ENDIF 
		ENDIF
	ENDREPEAT 
	
	RETURN cnt
ENDFUNC 
	
FUNC FLOAT GGSM_BOSS_CONTROLLER_GET_HP_PERCENTAGE(GGSM_BOSS_CONTROLLER &boss)
	IF (boss.iMaxTotalHP = 0.0)
		RETURN 0.0
	ENDIF 
	
	RETURN TO_FLOAT(boss.iTotalHP) / TO_FLOAT(boss.iMaxTotalHP)
ENDFUNC 

PROC GGSM_ADD_ENTITY_TO_BOSS_CONTROLLER(GGSM_BOSS_CONTROLLER &boss, GGSM_ENTITY &ent, INT iIndex, BOOL bContributeToMaxHP = TRUE)
	INT iEntity = GGSM_BOSS_CONTROLLER_GET_ENTITY_INDEX(boss, iIndex) 
	
	IF (iEntity = GGSM_INVALID_ENTITY) AND NOT IS_BITMASK_ENUM_AS_ENUM_SET(ent.eFlags, GGSM_ENTITY_BIT_IS_PLAYER_ALLIED)
		GGSM_BOSS_CONTROLLER_SET_ENTITY_INDEX(boss, iIndex, GGSM_ENTITY_GET_ARRAY_INDEX(ent))
		
		IF (bContributeToMaxHP)
			boss.iMaxTotalHP += ent.iHP
			SET_BIT(boss.iSummedMaxHPBitSet, iIndex)
		ENDIF 
		
		IF (boss.eState = GGSM_BOSS_STATE_NONE)
			GGSM_BOSS_CONTROLLER_SET_STATE(boss, GGSM_BOSS_STATE_ARRIVE)
		ENDIF 
		
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BOSSES_ACTIVE)
		SET_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BRING_CAMERA_TO_STOP)
		
		IF (iIndex = 0)
			SET_BITMASK_ENUM_AS_ENUM(ent.eFlags, GGSM_ENTITY_BIT_FORCE_SCROLLING_TO_STOP)
		ENDIF 
		
		EXIT 
	ENDIF 
ENDPROC 

PROC GGSM_RESET_BOSS_CONTROLLER_ARRAY(GGSM_BOSS_CONTROLLER &boss[])
	INT i
	
	REPEAT COUNT_OF(boss) i
		GGSM_RESET_BOSS_CONTROLLER(boss[i])
	ENDREPEAT 
	
	CLEAR_BITMASK_ENUM_AS_ENUM(sGGSMData.eStageFlags, GGSM_STAGE_BIT_BOSSES_ACTIVE)
ENDPROC 

//-------------------------------------------------
// SCRIPT EXPORT FUNCTIONS
//-------------------------------------------------

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Exports a Tile Collison Definiation to script
/// PARAMS:
///    data - 
///    bOpenAndCloseFile - 
PROC GGSM_DEBUG_EXPORT_TILE_COLLISION_DEFINITION_TO_SCRIPT(GGSM_TILE_COLLISION_DATA &data, BOOL bOpenAndCloseFile = TRUE)
	INT i
	VECTOR_2D vec 
	
	IF (bOpenAndCloseFile)
		OPEN_DEBUG_FILE()
	ENDIF 
	
	SAVE_STRING_TO_DEBUG_FILE("    // Tile Collision Data")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("    data.eType = ")
	SAVE_STRING_TO_DEBUG_FILE(GGSM_TILE_COLLISION_TYPE_TO_STRING(data.eType))
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	REPEAT data.iNumPoints i
		vec = GGSM_TILE_COLLISION_DATA_GET_COLLISION_VECTOR(data, i)
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_TILE_COLLISION_DATA_SET_COLLISION_VECTOR(data, ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vec.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vec.y)		
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	ENDREPEAT 
	
	SAVE_STRING_TO_DEBUG_FILE("    data.iNumPoints = ")
	SAVE_INT_TO_DEBUG_FILE(data.iNumPoints)
	SAVE_NEWLINE_TO_DEBUG_FILE()	
		
	IF (bOpenAndCloseFile)
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC 

/// PURPOSE:
///    Exports Tile Collison Array to script
/// PARAMS:
///    array - 
///    iCount - # of elements to export
PROC GGSM_DEBUG_EXPORT_TILE_COLLISION_ARRAY_TO_SCRIPT(GGSM_TILE_COLLISION_DATA &array[], INT iCount = 0, BOOL bReturn = FALSE)
	INT i 
	
	IF (iCount = 0)
		iCount = COUNT_OF(array)
	ENDIF 
	
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("// Tile Colliders")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("SWITCH (iTileIndex)")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT iCount i
			SAVE_STRING_TO_DEBUG_FILE("CASE ")
			SAVE_INT_TO_DEBUG_FILE(i)
			SAVE_STRING_TO_DEBUG_FILE(" ")
			GGSM_DEBUG_EXPORT_TILE_COLLISION_DEFINITION_TO_SCRIPT(array[i], FALSE) 
			
			IF (bReturn) 
				SAVE_STRING_TO_DEBUG_FILE("RETURN TRUE")
			ELSE 
				SAVE_STRING_TO_DEBUG_FILE("BREAK")		
			ENDIF 
			
			SAVE_NEWLINE_TO_DEBUG_FILE()	
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT 
		
		SAVE_STRING_TO_DEBUG_FILE("ENDSWITCH")
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	CLOSE_DEBUG_FILE()
ENDPROC 

/// PURPOSE:
///    Export Tile Map Data to Script
/// PARAMS:
///    tm - 
PROC GGSM_DEBUG_EXPORT_TILE_MAP_DATA_TO_SCRIPT(GGSM_TILEMAP &tm)
	INT x, y, ind  
	BOOL bHFlip, bVFlip
	
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("// Tile Map Dimensions")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("data.iMapWidth = ")
		SAVE_INT_TO_DEBUG_FILE(tm.iMapWidth)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("data.iMapHeight = ")
		SAVE_INT_TO_DEBUG_FILE(tm.iMapHeight)
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		
		SAVE_STRING_TO_DEBUG_FILE("data.iMaxTiles = ")
		SAVE_INT_TO_DEBUG_FILE(tm.iMaxTiles)
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		
		SAVE_STRING_TO_DEBUG_FILE("data.fTileSize = ")
		SAVE_FLOAT_TO_DEBUG_FILE(tm.fTileSize)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("data.fTileDrawSize = ")
		SAVE_FLOAT_TO_DEBUG_FILE(tm.fTileDrawSize)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("data.bWrapped = ")
		IF (tm.bWrapped)
			SAVE_STRING_TO_DEBUG_FILE("TRUE")
		ELSE 
			SAVE_STRING_TO_DEBUG_FILE("FALSE")
		ENDIF 
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("// Tile Directories")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("data.sTileDirectory = \"") 
		SAVE_STRING_TO_DEBUG_FILE(tm.sTileDirectory)
		SAVE_STRING_TO_DEBUG_FILE("\"")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("data.sTileSetBaseName = \"") 
		SAVE_STRING_TO_DEBUG_FILE(tm.sTileSetBaseName)
		SAVE_STRING_TO_DEBUG_FILE("\"")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("// Tile Map Data")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT tm.iMapHeight y
			REPEAT tm.iMapWidth x
				IF GGSM_TILEMAP_GET_DATA(tm, x, y, ind, bHFlip, bVFlip)
					IF (ind != 0)
						SAVE_STRING_TO_DEBUG_FILE("GGSM_TILEMAP_SET_DATA(data, ")
						SAVE_INT_TO_DEBUG_FILE(x)
						SAVE_STRING_TO_DEBUG_FILE(", ")
						SAVE_INT_TO_DEBUG_FILE(y)
						SAVE_STRING_TO_DEBUG_FILE(", ")
						SAVE_INT_TO_DEBUG_FILE(ind)
						
						IF (bHFlip)
							SAVE_STRING_TO_DEBUG_FILE(", TRUE")
						ELSE 
							SAVE_STRING_TO_DEBUG_FILE(", FALSE")
						ENDIF 
	
						IF (bVFlip)
							SAVE_STRING_TO_DEBUG_FILE(", TRUE")
						ELSE 
							SAVE_STRING_TO_DEBUG_FILE(", FALSE")
						ENDIF 
						SAVE_STRING_TO_DEBUG_FILE(")")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF 
				ENDIF 
			ENDREPEAT 
		ENDREPEAT 
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
ENDPROC 

/// PURPOSE:
///    Exports a Spawn Definition to script
/// PARAMS:
///    data - 
///    bOpenAndCloseFile - 
PROC GGSM_DEBUG_EXPORT_FORMATION_TO_SCRIPT(GGSM_FORMATION_DATA &data, BOOL bOpenAndCloseFile = TRUE)
	//INT i
	VECTOR_2D vec
	
	IF (bOpenAndCloseFile)
		OPEN_DEBUG_FILE()
	ENDIF 
	
	SAVE_STRING_TO_DEBUG_FILE("    // Formation Data")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_TYPE(data, ")
	SAVE_STRING_TO_DEBUG_FILE(GGSM_FORMATION_TYPE_TO_STRING(GGSM_FORMATION_GET_TYPE(data)))
	SAVE_STRING_TO_DEBUG_FILE(")")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	vec = GGSM_FORMATION_GET_START_POSITION(data) 
	SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_START_POSITION(data, ")
	SAVE_FLOAT_TO_DEBUG_FILE(vec.x) 
	SAVE_STRING_TO_DEBUG_FILE(", ")
	SAVE_FLOAT_TO_DEBUG_FILE(vec.y) 
	SAVE_STRING_TO_DEBUG_FILE(")") 
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	vec = GGSM_FORMATION_GET_START_DIRECTION(data) 
	SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_START_DIRECTION(data, ")
	SAVE_FLOAT_TO_DEBUG_FILE(vec.x) 
	SAVE_STRING_TO_DEBUG_FILE(", ")
	SAVE_FLOAT_TO_DEBUG_FILE(vec.y) 
	SAVE_STRING_TO_DEBUG_FILE(")") 
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_SCROLL_DISTANCE(data, ")
	SAVE_FLOAT_TO_DEBUG_FILE(GGSM_FORMATION_GET_SCROLL_DISTANCE(data))
	SAVE_STRING_TO_DEBUG_FILE(")") 
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	IF (GGSM_FORMATION_GET_TYPE(data) = GGSM_FORMATION_POWER_UP)
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_POWER_UP_SPRITE(data, ")
		SAVE_STRING_TO_DEBUG_FILE(GGSM_SPRITE_TYPE_TO_STRING(GGSM_FORMATION_GET_POWER_UP_SPRITE(data)))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()	
	ELSE 
	
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_ENTITY_TYPE(data, ")
		SAVE_STRING_TO_DEBUG_FILE(GGSM_ENTITY_TYPE_TO_STRING(GGSM_FORMATION_GET_ENTITY_TYPE(data)))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_MOVEMENT_TYPE(data, ")
		SAVE_STRING_TO_DEBUG_FILE(GGSM_MOVEMENT_TYPE_TO_STRING(GGSM_FORMATION_GET_MOVEMENT_TYPE(data)))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_WEAPON_TYPE(data, ")
		SAVE_STRING_TO_DEBUG_FILE(GGSM_WEAPON_TYPE_TO_STRING(GGSM_FORMATION_GET_WEAPON_TYPE(data)))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_PATH_INDEX(data, ")
		SAVE_INT_TO_DEBUG_FILE(GGSM_FORMATION_GET_PATH_INDEX(data))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		
		vec = GGSM_FORMATION_GET_SPACING(data) 
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_SPACING(data, ")
		SAVE_FLOAT_TO_DEBUG_FILE(vec.x) 
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vec.y) 
		SAVE_STRING_TO_DEBUG_FILE(")") 
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF 
	
	SAVE_STRING_TO_DEBUG_FILE("    GGSM_FORMATION_SET_MEMBER_COUNT(data, ")
	SAVE_INT_TO_DEBUG_FILE(GGSM_FORMATION_GET_MEMBER_COUNT(data))
	SAVE_STRING_TO_DEBUG_FILE(")")
	SAVE_NEWLINE_TO_DEBUG_FILE()
		
	IF (bOpenAndCloseFile)
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

/// PURPOSE:
///    Exports Spawn Array to script
/// PARAMS:
///    array - 
///    iCount - # of elements to export
PROC GGSM_DEBUG_EXPORT_FORMATION_ARRAY_TO_SCRIPT(GGSM_FORMATION_DATA &array[], INT iCount = 0, BOOL bReturn = FALSE)
	INT i 
	INT n = 0

	IF (iCount <= 0)
		iCount = COUNT_OF(array)
	ENDIF 
	
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("// Formation Definitions")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("SWITCH (iSpawnIndex)")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT iCount i
			IF NOT GGSM_FORMATION_IS_VALID(array[i])
				RELOOP 
			ENDIF 
				
			SAVE_STRING_TO_DEBUG_FILE("CASE ")
			SAVE_INT_TO_DEBUG_FILE(n)
			SAVE_STRING_TO_DEBUG_FILE(" ")
			GGSM_DEBUG_EXPORT_FORMATION_TO_SCRIPT(array[n], FALSE) 
			
			IF (bReturn) 
				SAVE_STRING_TO_DEBUG_FILE("RETURN TRUE")
			ELSE 
				SAVE_STRING_TO_DEBUG_FILE("BREAK")		
			ENDIF 
			
			SAVE_NEWLINE_TO_DEBUG_FILE()	
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			n ++
		ENDREPEAT 
		
		SAVE_STRING_TO_DEBUG_FILE("ENDSWITCH")
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	CLOSE_DEBUG_FILE()
ENDPROC 

/// PURPOSE:
///    Exports a Spawn Definition to script
/// PARAMS:
///    data - 
///    bOpenAndCloseFile - 
PROC GGSM_DEBUG_EXPORT_PATH_TO_SCRIPT(GGSM_PATH_DATA &data, BOOL bOpenAndCloseFile = TRUE)
	INT i, numpts
	VECTOR_2D vec
	
	IF (bOpenAndCloseFile)
		OPEN_DEBUG_FILE()
	ENDIF 
	
	numpts = GGSM_PATH_DATA_GET_NUM_POINTS(data)
	SAVE_STRING_TO_DEBUG_FILE("    // Path Data")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("    GGSM_PATH_DATA_SET_NUM_POINTS(data, ")
	SAVE_INT_TO_DEBUG_FILE(GGSM_PATH_DATA_GET_NUM_POINTS(data))
	SAVE_STRING_TO_DEBUG_FILE(")")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	REPEAT numpts i
		vec = GGSM_PATH_DATA_GET_POINT(data, i)
		SAVE_STRING_TO_DEBUG_FILE("    GGSM_PATH_DATA_SET_POINT(data, ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vec.x) 
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vec.y) 
		SAVE_STRING_TO_DEBUG_FILE(")") 
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDREPEAT 
		
	IF (bOpenAndCloseFile)
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

/// PURPOSE:
///    Exports Spawn Array to script
/// PARAMS:
///    array - 
///    iCount - # of elements to export
PROC GGSM_DEBUG_EXPORT_PATH_ARRAY_TO_SCRIPT(GGSM_PATH_DATA &array[], INT iCount = 0, BOOL bReturn = FALSE)
	INT i 
	INT n = 0

	IF (iCount <= 0)
		iCount = COUNT_OF(array)
	ENDIF 
	
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("// Path Definitions")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("SWITCH (iPathIndex)")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT iCount i
			IF (GGSM_PATH_DATA_GET_NUM_POINTS(array[i]) = 0)
				RELOOP 
			ENDIF 
				
			SAVE_STRING_TO_DEBUG_FILE("CASE ")
			SAVE_INT_TO_DEBUG_FILE(n)
			SAVE_STRING_TO_DEBUG_FILE(" ")
			GGSM_DEBUG_EXPORT_PATH_TO_SCRIPT(array[n], FALSE) 
			
			IF (bReturn) 
				SAVE_STRING_TO_DEBUG_FILE("RETURN TRUE")
			ELSE 
				SAVE_STRING_TO_DEBUG_FILE("BREAK")		
			ENDIF 
			
			SAVE_NEWLINE_TO_DEBUG_FILE()	
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			n ++
		ENDREPEAT 
		
		SAVE_STRING_TO_DEBUG_FILE("ENDSWITCH")
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	CLOSE_DEBUG_FILE()
ENDPROC 

#ENDIF

