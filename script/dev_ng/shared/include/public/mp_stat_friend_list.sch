USING "globals.sch"
USING "rage_builtins.sch"

// Header support for MP_STAT_FRIEND_LIST.gfx


/// PURPOSE: Sets the colour to be used throughout the UI. Use before Display_stats
///    
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
///    colID - Colour ID. 0 = vagos orange, 1 = cops blue, 2 = bikers grey
PROC Set_Colour(SCALEFORM_INDEX si, INT colID)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "SET_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(colID)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE: Sets the Title String to be used by the UI
///    
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
///    titleStr - Title String
PROC Set_Title(SCALEFORM_INDEX si, STRING titleStr)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "SET_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(titleStr)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE: This stores the stat data in a list ready to be initialised and drawn to display with the command Display_stats command
/// 
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
///    index - Slot index, affects position. (Positions left to right - top to bottom)
///    statEnum - Stat enum, (Range 23 -44)
///    amount - The Value in percent for any stats with a bar (0-100) or Monetary/XP value e.g 200 XP, £300
///    label - String label of the stat
PROC Set_Stat(SCALEFORM_INDEX si, INT index, INT statEnum, INT amount, STRING label)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "SET_STAT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(statEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(amount)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(label)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC Update_Stat(SCALEFORM_INDEX si, INT index, INT statEnum, INT amount, STRING label)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "UPDATE_STAT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(statEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(amount)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(label)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: This draws the stats list to the screen once they have been setup with Set_Stat command(s)
///
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
PROC Display_Stats(SCALEFORM_INDEX si)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "DISPLAY_STATS")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE: This clears the stats list from the screen, and the Scaleform data array. Do this before you make calls to Set_Stat and Display_Stats commands.
///
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
PROC Clear_Stats(SCALEFORM_INDEX si)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "CLEAR_STATS")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE: This stores the friend data in an array ready to be initialised and drawn to display with the command Display_Friends command
/// 
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
///    index - Slot index, affects position. (Positions - top to bottom)
///    colourEnum - Colour ID. 0 = vagos orange, 1 = cops blue, 2 = bikers grey
///    label - Friend String label
PROC Set_Friend(SCALEFORM_INDEX si, INT index, INT colourEnum, STRING label, BOOL onMission = FALSE, INT arrest = ICON_EMPTY, INT role= ICON_EMPTY, INT mic= ICON_EMPTY  )

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "SET_FRIEND")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(colourEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(label)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(onMission)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(arrest)
	    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(role)
	    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(mic)


	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC UPDATE_Friend(SCALEFORM_INDEX si, INT index, INT colourEnum, STRING label, BOOL onMission = FALSE, INT arrest = ICON_EMPTY, INT role= ICON_EMPTY, INT mic= ICON_EMPTY  )

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "SET_FRIEND")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(colourEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(label)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(onMission)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(arrest)
	    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(role)
	    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(mic)


	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: This draws the friends list to the screen once the data has been setup with Set_Friend command(s)
///
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
PROC Display_Friends(SCALEFORM_INDEX si)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "DISPLAY_FRIENDS")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE: This clears the friends list from the screen, and the Scaleform data array. Do this before you make calls to Set_Friend and Display_Friends commands.
///
/// PARAMS:
///    si - Scaleform Movie Index (stored when MP_STAT_FRIEND_LIST.gfx is loaded)
PROC Clear_Friends(SCALEFORM_INDEX si)

	BEGIN_SCALEFORM_MOVIE_METHOD(si, "CLEAR_FRIENDS")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


