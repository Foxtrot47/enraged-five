//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Adam Westwood				Date: 22/01/14			    │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Heist Bags							                │
//│																				│
//│		Controls minigame and determines the fullness level of a heist bag      │
//|			                            										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


//if u use the original prop_gold_trolly_full. 
//I have added:
//Prop_Gold_Trolly_Half_Full
//Prop_Gold_Trolly_Empty
//
//and if u use the original PROP_CASH_CRATE_01
//I have added:
//Prop_Cash_Crate_Half_full
//Prop_Cash_Crate_Empty
//
//assetes added for next build
//
//CJ


USING "timer_public.sch"

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"

USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"

USING "LineActivation.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

CONST_INT	CASH_GRAB_BS_CASH_BAGGED				0
CONST_INT	CASH_GRAB_BS_BAG_REMOVED				1
CONST_INT	CASH_GRAB_BS_GRAB_ANIM_STOPPED			2
CONST_INT	CASH_GRAB_BS_MINIGAME_COMPLETED			3
CONST_INT	CASH_GRAB_BS_ANIM_EVENTS_STARTED		4
CONST_INT	CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS		5
CONST_INT	CASH_GRAB_BS_WALK_TASK_GIVEN_EARLY		6
CONST_INT	CASH_GRAB_BS_SKIP_WALK_AND_DO_INTRO 	7
CONST_INT	CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE 	8

ENUM HEIST_BAG_STATE
	HBS_NONE,
	HBS_DUFFLE_BAG_EMPTY,
	HBS_DUFFLE_BAG_FULL,
	HBS_DUFFLE_BAG_EMPTY_FOR_BODY_ARMOUR,
	HBS_DUFFLE_BAG_FULL_FOR_BODY_ARMOUR,
	HBS_SPORTS_BAG_EMPTY,
	HBS_SPORTS_BAG_FULL,
	HBS_SPORTS_BAG_EMPTY_FOR_BODY_ARMOUR,
	HBS_SPORTS_BAG_FULL_FOR_BODY_ARMOUR
ENDENUM

ENUM HEIST_BAG_GRAB_STATE

	HBGS_NONE,
	HBGS_APPROACH,
	HBGS_INTRO_ANIM,
	HBGS_IDLE_ANIM,
	HBGS_GRAB_ANIM,
	HBGS_EXIT_ANIM

ENDENUM

ENUM HEIST_BAG_TYPE
	HBT_DUFFLE_BAG,
	HBT_SPORTS_BAG
ENDENUM

ENUM CASH_TYPE
	CT_GOLD,
	CT_PAPER
ENDENUM

ENUM GRAB_DIFFICULTY
	GD_EASY,
	GD_MEDIUM,
	GD_HARD
ENDENUM

STRUCT PLAYER_HEIST_BAG 
	HEIST_BAG_STATE e_heist_bag_state = HBS_NONE
	FLOAT fAmountInBag
ENDSTRUCT

STRUCT S_CASH_GRAB_DATA 

	HEIST_BAG_GRAB_STATE 	eState
	GRAB_DIFFICULTY			eDifficulty
	CAMERA_INDEX 			cameraIndex
	
	INT						iCashGrabOngoingInteractionIndex = -1

	INT 					iGrabAmount
	INT						iBonusCashPiles
	INT						iTotalCashPiles
	INT						iBonusCashPilesGrabbed
	INT						iPlayerSceneID
	INT						iTrolleySceneID
	
	INT 					iBaseCashPileValue
	INT						iBonusCashPileValue
	
	INT						iBitSet

	FLOAT 					fGrabPhase
	FLOAT 					fGrabSpeed
	FLOAT 					fMaxGrabSpeed
		
	VECTOR 					vCamStartPos
	VECTOR 					vCamPointPos
	
	FLOAT 					fCamRotOffset
	FLOAT 					fDesiredCamRotOffset
	FLOAT 					fCamHeightOffset 			
	FLOAT 					fDesiredCamHeightOffset 	

	FLOAT					fMissionDifficultyModifier
	
	STRING					sBagAnimDict
ENDSTRUCT

#IF IS_DEBUG_BUILD

	DEBUGONLY FUNC STRING GET_HEIST_BAG_GRAB_STATE(S_CASH_GRAB_DATA &data)

		SWITCH data.eState
		
			CASE HBGS_NONE
				RETURN "HBGS_NONE"
			BREAK
		
			CASE HBGS_APPROACH
				RETURN "HBGS_APPROACH"
			BREAK
			
			CASE HBGS_INTRO_ANIM
				RETURN "HBGS_INTRO_ANIM"
			BREAK
			
			CASE HBGS_IDLE_ANIM
				RETURN "HBGS_IDLE_ANIM"
			BREAK
			
			CASE HBGS_GRAB_ANIM
				RETURN "HBGS_GRAB_ANIM"
			BREAK
			
			CASE HBGS_EXIT_ANIM
				RETURN "HBGS_EXIT_ANIM"
			BREAK
			
		ENDSWITCH
		
		RETURN ""

	ENDFUNC
	
	DEBUGONLY FUNC STRING GET_HEIST_BAG_GRAB_DIFFICULTY(S_CASH_GRAB_DATA &data)

		SWITCH data.eDifficulty
		
			CASE GD_EASY
				RETURN "GD_EASY"
			BREAK
			CASE GD_MEDIUM
				RETURN "GD_MEDIUM"
			BREAK
			CASE GD_HARD
				RETURN "GD_HARD"
			BREAK
			
		ENDSWITCH
		
		RETURN ""

	ENDFUNC

#ENDIF

FUNC BOOL IS_CASH_TROLLEY(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = HEI_PROP_HEI_CASH_TROLLY_01
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01A
	OR mnToCheck = CH_PROP_CH_CASH_TROLLY_01A
	OR mnToCheck = CH_PROP_CH_CASH_TROLLY_01B
	OR mnToCheck = CH_PROP_CH_CASH_TROLLY_01C
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01B
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01C
ENDFUNC
FUNC BOOL IS_LOW_CASH_TROLLEY(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = CH_PROP_CASH_LOW_TROLLY_01A
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01B
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01C
ENDFUNC
FUNC BOOL IS_DIAMOND_TROLLEY(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = CH_PROP_DIAMOND_TROLLY_01A
	OR mnToCheck = CH_PROP_DIAMOND_TROLLY_01B
	OR mnToCheck = CH_PROP_DIAMOND_TROLLY_01C
ENDFUNC
FUNC BOOL IS_GOLD_TROLLEY(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = PROP_GOLD_TROLLY_FULL
	OR mnToCheck = CH_PROP_GOLD_TROLLY_01A
	OR mnToCheck = CH_PROP_GOLD_TROLLY_01B
	OR mnToCheck = CH_PROP_GOLD_TROLLY_01C
ENDFUNC
FUNC BOOL IS_COKE_TROLLEY(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
ENDFUNC

FUNC BOOL IS_PAINTING(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01a"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01b"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01c"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01d"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01e"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01f"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01g"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01h"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01i"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("CH_prop_vault_painting_01j"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01a"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01b"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01c"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01d"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01e"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01f"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01g"))
	OR mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Painting_01h"))
ENDFUNC

FUNC BOOL IS_CASH_PILE(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Cash_Stack_01a"))
ENDFUNC

FUNC BOOL IS_COKE_PILE(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Coke_Stack_01a"))
ENDFUNC

FUNC BOOL IS_WEED_PILE(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Weed_Stack_01a"))
ENDFUNC

FUNC BOOL IS_GOLD_PILE(MODEL_NAMES mnToCheck)
	RETURN mnToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Gold_Stack_01a"))
ENDFUNC

FUNC BOOL IS_LOOT_PILE(MODEL_NAMES mnToCheck)
	RETURN IS_CASH_PILE(mnToCheck)
	OR IS_COKE_PILE(mnToCheck)
	OR IS_WEED_PILE(mnToCheck)
ENDFUNC

FUNC BOOL IS_GRAB_PILE(MODEL_NAMES mnToCheck)
	RETURN IS_LOOT_PILE(mnToCheck)
	OR IS_GOLD_PILE(mnToCheck)
ENDFUNC

/// PURPOSE:
///    Get the name of the animation dictionary for heist bag minigame depending on the mission prop's model. 
/// PARAMS:
///    tempObj - Mission prop object that the grab minigame animations are played for.
/// RETURNS:
///    The name of the animation dictionary specific for the mission prop.
FUNC STRING GET_HEIST_BAG_MINIGAME_ANIM_DICT(OBJECT_INDEX tempObj, BOOL bUseHighHeelsAnims)

	IF DOES_ENTITY_EXIST(tempObj)
		
		MODEL_NAMES mnObj = GET_ENTITY_MODEL(tempObj)
		
		IF GET_ENTITY_MODEL(tempObj) = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
		OR IS_DIAMOND_TROLLEY(mnObj)
		OR IS_CASH_TROLLEY(mnObj)
		OR IS_GOLD_TROLLEY(mnObj)
			IF bUseHighHeelsAnims
				RETURN "anim@heists@ornate_bank@grab_cash_heels"
			ELSE
				RETURN "anim@heists@ornate_bank@grab_cash"
			ENDIF
		ENDIF
		
		IF IS_PAINTING(mnObj)
			RETURN "ANIM_HEIST@HS3F@IG11_STEAL_PAINTING@MALE@"
		ENDIF
		
		IF IS_LOOT_PILE(mnObj)
			IF bUseHighHeelsAnims
				RETURN "ANIM@SCRIPTED@PLAYER@MISSION@TUN_TABLE_GRAB@CASH@HEELED@"
			ELSE
				RETURN "ANIM@SCRIPTED@PLAYER@MISSION@TUN_TABLE_GRAB@CASH@"
			ENDIF
		ENDIF
		
		IF IS_GOLD_PILE(mnObj)
			IF bUseHighHeelsAnims
				RETURN "ANIM@SCRIPTED@PLAYER@MISSION@TUN_TABLE_GRAB@GOLD@HEELED@"
			ELSE
				RETURN "ANIM@SCRIPTED@PLAYER@MISSION@TUN_TABLE_GRAB@GOLD@"
			ENDIF
		ENDIF
		
		SWITCH mnObj
			CASE HEI_PROP_HEI_CASH_TROLLY_01
				IF bUseHighHeelsAnims
					RETURN "anim@heists@ornate_bank@grab_cash_heels"
				ELSE
					RETURN "anim@heists@ornate_bank@grab_cash"
				ENDIF
			BREAK
			CASE PROP_GOLD_TROLLY_FULL
				RETURN "anim@heists@ornate_bank@ig_4_grab_gold"
			BREAK
			DEFAULT
				RETURN "anim@heists@money_grab@duffel"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""

ENDFUNC

PROC REQUEST_HEIST_BAG_ASSETS(MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	
	REQUEST_MODEL(mnBag)
	
ENDPROC

/// PURPOSE:
///    Checks if heist bag assets have loaded.
/// RETURNS:
///    Returns true if heist bag assets have loaded, false otherwise.
FUNC BOOL HAVE_HEIST_BAG_ASSETS_LOADED(MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	
	RETURN HAS_MODEL_LOADED(mnBag)

ENDFUNC

/// PURPOSE:
///    Request audio bank for heist bag minigame depending on the mission prop's model. 
/// PARAMS:
///    tempObj - Mission prop object that the grab minigame uses.
PROC REQUEST_HEIST_BAG_MINIGAME_AUDIO_BANK(OBJECT_INDEX tempObj)

	IF IS_PAINTING(GET_ENTITY_MODEL(tempObj))
		EXIT
	ENDIF

	IF DOES_ENTITY_EXIST(tempObj)
		SWITCH GET_ENTITY_MODEL(tempObj)
			CASE HEI_PROP_HEI_CASH_TROLLY_01
				// don't request any audio bank
			BREAK
			CASE PROP_GOLD_TROLLY_FULL
				REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_STASH_SWAG")
			BREAK
			DEFAULT
				REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_STASH_SWAG")
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC RELEASE_HEIST_BAG_MINIGAME_AUDIO_BANK()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_STASH_SWAG")
	PRINTLN("RELEASE_HEIST_BAG_MINIGAME_AUDIO_BANK - Releasing audio bank")
ENDPROC

FUNC BOOL HAS_HEIST_BAG_MINIGAME_AUDIO_BANK_LOADED(OBJECT_INDEX tempObj)

	IF DOES_ENTITY_EXIST(tempObj)
		SWITCH GET_ENTITY_MODEL(tempObj)
			CASE HEI_PROP_HEI_CASH_TROLLY_01
				RETURN TRUE
			BREAK
			CASE PROP_GOLD_TROLLY_FULL
				RETURN REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_STASH_SWAG")
			BREAK
			DEFAULT
				RETURN REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_STASH_SWAG")
			BREAK
		ENDSWITCH
		
		IF GET_ENTITY_MODEL(tempObj) = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC REQUEST_HEIST_BAG_MINIGAME_ASSETS(OBJECT_INDEX tempObj, BOOL bUseHighHeelsAnims, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	
	//request models
	REQUEST_MODEL(mnBag)
	MODEL_NAMES mnObj = GET_ENTITY_MODEL(tempObj)
	IF mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
	OR mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Coke_Stack_01a"))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")))
		
	ELIF IS_DIAMOND_TROLLEY(mnObj)
		REQUEST_MODEL(ch_Prop_Vault_Dimaondbox_01a)
		
	ELIF IS_GOLD_TROLLEY(mnObj)
	OR mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Gold_Stack_01a"))
		REQUEST_MODEL(CH_PROP_GOLD_BAR_01A)
	
	ELIF mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Weed_Stack_01a"))
		REQUEST_MODEL(hei_prop_heist_weed_block_01b)
		
	ELSE
		REQUEST_MODEL(HEI_PROP_HEIST_CASH_PILE)
	ENDIF
	
	//request animation dictionaries
	REQUEST_ANIM_DICT(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))
	
	//request audio banks
	REQUEST_HEIST_BAG_MINIGAME_AUDIO_BANK(tempObj)
	
ENDPROC

FUNC BOOL HAVE_HEIST_BAG_MINIGAME_ASSETS_LOADED(OBJECT_INDEX tempObj, BOOL bUseHighHeelsAnims, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	
	MODEL_NAMES mnObj = GET_ENTITY_MODEL(tempObj)
	
	IF mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
	OR mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Coke_Stack_01a"))
		IF 	HAS_MODEL_LOADED(mnBag)
		AND	HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")))
		AND	HAS_HEIST_BAG_MINIGAME_AUDIO_BANK_LOADED(tempObj)
		AND HAS_ANIM_DICT_LOADED(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))

			RETURN TRUE
		
		ENDIF
	ELIF IS_DIAMOND_TROLLEY(mnObj)
		IF 	HAS_MODEL_LOADED(mnBag)
		AND	HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a")))
		AND	HAS_HEIST_BAG_MINIGAME_AUDIO_BANK_LOADED(tempObj)
		AND HAS_ANIM_DICT_LOADED(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))

			RETURN TRUE
		
		ENDIF
	ELIF IS_GOLD_TROLLEY(mnObj)
	OR mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Gold_Stack_01a"))
		IF 	HAS_MODEL_LOADED(mnBag)
		AND	HAS_MODEL_LOADED(CH_PROP_GOLD_BAR_01A)
		AND	HAS_HEIST_BAG_MINIGAME_AUDIO_BANK_LOADED(tempObj)
		AND HAS_ANIM_DICT_LOADED(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))
			RETURN TRUE
		ENDIF
	ELIF IS_PAINTING(mnObj)
		IF 	HAS_MODEL_LOADED(mnBag)
		AND HAS_ANIM_DICT_LOADED(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))
			PRINTLN("[ML] Bag and anim dict for bag loaded")
			RETURN TRUE
		ENDIF
	ELIF mnObj = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Weed_Stack_01a"))
		IF 	HAS_MODEL_LOADED(mnBag)
		AND	HAS_MODEL_LOADED(hei_prop_heist_weed_block_01b)
		AND	HAS_HEIST_BAG_MINIGAME_AUDIO_BANK_LOADED(tempObj)
		AND HAS_ANIM_DICT_LOADED(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))
			RETURN TRUE
		ENDIF
		
	ELSE
		IF 	HAS_MODEL_LOADED(mnBag)
		AND	HAS_MODEL_LOADED(HEI_PROP_HEIST_CASH_PILE)
		AND	HAS_HEIST_BAG_MINIGAME_AUDIO_BANK_LOADED(tempObj)
		AND HAS_ANIM_DICT_LOADED(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))

			RETURN TRUE
		
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CLEANUP_HEIST_BAG_MINIGAME_ASSETS(OBJECT_INDEX tempObj, BOOL bUseHighHeelsAnims, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)

	//remove models
	SET_MODEL_AS_NO_LONGER_NEEDED(mnBag)
	IF GET_ENTITY_MODEL(tempObj) = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")))
	ELIF IS_DIAMOND_TROLLEY(GET_ENTITY_MODEL(tempObj))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a")))
	ELIF IS_GOLD_TROLLEY(GET_ENTITY_MODEL(tempObj))
		SET_MODEL_AS_NO_LONGER_NEEDED(CH_PROP_GOLD_BAR_01A)
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_CASH_PILE)
	ENDIF
	
	//request animation dictionaries
	REMOVE_ANIM_DICT(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, bUseHighHeelsAnims))
	
	RELEASE_HEIST_BAG_MINIGAME_AUDIO_BANK()

ENDPROC

FUNC BOOL IS_PED_PLAYING_ANY_CASH_GRAB_ANIM(PED_INDEX PedIndex)
	
	IF NOT IS_PED_INJURED(PedIndex)
		IF IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash", "intro")
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash", "grab_idle")
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash", "grab")
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash", "exit")
		//high heels version of anims
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash_heels", "intro")
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash_heels", "grab_idle")
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash_heels", "grab")
		OR IS_ENTITY_PLAYING_ANIM(PedIndex, "anim@heists@ornate_bank@grab_cash_heels", "exit")
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RESET_CASH_GRAB_MINIGAME_DATA(S_CASH_GRAB_DATA &data)
	
	data.fGrabSpeed 		= 1.0
	data.fMaxGrabSpeed 		= 1.0
	data.eState				= HBGS_NONE
	data.iCashGrabOngoingInteractionIndex = -1
	CLEAR_BIT(data.iBitSet, CASH_GRAB_BS_MINIGAME_COMPLETED)
	CLEAR_BIT(data.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
ENDPROC

PROC RESET_CASH_GRAB_SPEED(S_CASH_GRAB_DATA &data)
	
	data.fGrabSpeed 		= 1.0
	data.fMaxGrabSpeed 		= 1.0
	
ENDPROC

PROC CONVERGE_VALUE(FLOAT &fCurrentValue, FLOAT fDesiredValue, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF fCurrentValue != fDesiredValue
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF fCurrentValue - fDesiredValue > fConvergeAmountThisFrame
			fCurrentValue -= fConvergeAmountThisFrame
		ELIF fCurrentValue - fDesiredValue < -fConvergeAmountThisFrame
			fCurrentValue += fConvergeAmountThisFrame
		ELSE
			fCurrentValue = fDesiredValue
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CASH_GRAB_SPEED(S_CASH_GRAB_DATA &data, FLOAT fMinSpeed = 0.75, FLOAT fMaxSpeed = 1.50)

	//increase the max speed when player presses the button
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			SWITCH data.eDifficulty
				CASE GD_EASY
					data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed +(0.100) , fMinSpeed, fMaxSpeed)
				BREAK
				CASE GD_MEDIUM
					data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed +(0.090) , fMinSpeed, fMaxSpeed)
				BREAK
				CASE GD_HARD
					data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed +(0.080) , fMinSpeed, fMaxSpeed)
				BREAK
			ENDSWITCH		
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			SWITCH data.eDifficulty
				CASE GD_EASY
					data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed +(0.100) , fMinSpeed, fMaxSpeed)
				BREAK
				CASE GD_MEDIUM
					data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed +(0.090) , fMinSpeed, fMaxSpeed)
				BREAK
				CASE GD_HARD
					data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed +(0.080) , fMinSpeed, fMaxSpeed)
				BREAK
			ENDSWITCH		
		ENDIF
	ENDIF
	
	//make the max speed always decrease and clamp it between min and max speed values
	SWITCH data.eDifficulty
		CASE GD_EASY
			data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed -@(0.010 * 30) , fMinSpeed, fMaxSpeed)
		BREAK
		CASE GD_MEDIUM
			data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed -@(0.0125 * 30) , fMinSpeed, fMaxSpeed)
		BREAK
		CASE GD_HARD
			data.fMaxGrabSpeed = CLAMP(data.fMaxGrabSpeed -@(0.0135 * 30) , fMinSpeed, fMaxSpeed)
		BREAK
	ENDSWITCH
	
	//make the current speed achieve the max speed
	CONVERGE_VALUE(data.fGrabSpeed , data.fMaxGrabSpeed , 0.02, TRUE)
	
	//clamp the current speed between min and max speed values
	data.fGrabSpeed = CLAMP(data.fGrabSpeed , fMinSpeed, fMaxSpeed)

ENDPROC

FUNC STRING GET_ANIM_EVENT_NAME_FROM_INT(INT iValue)

	STRING sAnimEventName
	
	SWITCH iValue
			
		CASE 1
			sAnimEventName = "one"
		BREAK
		
		CASE 2
			sAnimEventName = "two"
		BREAK
		
		CASE 3
			sAnimEventName = "three"
		BREAK
		
		CASE 4
			sAnimEventName = "four"
		BREAK
		
		CASE 5
			sAnimEventName = "five"
		BREAK
		
		CASE 6
			sAnimEventName = "six"
		BREAK
		
		CASE 7
			sAnimEventName = "seven"
		BREAK
		
		CASE 8
			sAnimEventName = "eight"
		BREAK
		
		CASE 9
			sAnimEventName = "nine"
		BREAK
		
		CASE 10
			sAnimEventName = "ten"
		BREAK
		
		CASE 11
			sAnimEventName = "eleven"
		BREAK
		
		CASE 12
			sAnimEventName = "tweleve"
		BREAK
		
		CASE 13
			sAnimEventName = "thirteen"
		BREAK
		
		CASE 14
			sAnimEventName = "fourteen"
		BREAK
		
		CASE 15
			sAnimEventName = "fifteen"
		BREAK
		
		CASE 16
			sAnimEventName = "sixteen"
		BREAK
		
		CASE 17
			sAnimEventName = "seventeen"
		BREAK
		
		CASE 18
			sAnimEventName = "eighteen"
		BREAK
		
		CASE 19
			sAnimEventName = "nineteen"
		BREAK
		
		CASE 20
			sAnimEventName = "twenty"
		BREAK
		
		CASE 21
			sAnimEventName = "twentyone"
		BREAK
		
		CASE 22
			sAnimEventName = "twentytwo"
		BREAK
		
		CASE 23
			sAnimEventName = "twentythree"
		BREAK
		
		CASE 24
			sAnimEventName = "twentyfour"
		BREAK
		
		CASE 25
			sAnimEventName = "twentyfive"
		BREAK
		
		CASE 26
			sAnimEventName = "twentysix"
		BREAK
		
		CASE 27
			sAnimEventName = "twentyseven"
		BREAK
		
		CASE 28
			sAnimEventName = "twentyeight"
		BREAK
		
		CASE 29
			sAnimEventName = "twentynine"
		BREAK	
		
		CASE 30
			sAnimEventName = "thirty"
		BREAK
		
		CASE 31
			sAnimEventName = "thirtyone"
		BREAK
		
		CASE 32
			sAnimEventName = "thirtytwo"
		BREAK
		
		CASE 33
			sAnimEventName = "thirtythree"
		BREAK	
		
		CASE 34
			sAnimEventName = "thirtyfour"
		BREAK
		
		CASE 35
			sAnimEventName = "thirtyfive"
		BREAK
		
		CASE 36
			sAnimEventName = "thirtysix"
		BREAK
		
		CASE 37
			sAnimEventName = "thirtyseven"
		BREAK
		
		CASE 38
			sAnimEventName = "thirtyeight"
		BREAK
		
		CASE 39
			sAnimEventName = "thirtynine"
		BREAK
		
		CASE 40
			sAnimEventName = "forty"
		BREAK
		
		CASE 41
			sAnimEventName = "fortyone"
		BREAK
		
		CASE 42
			sAnimEventName = "fortytwo"
		BREAK
		
		CASE 43
			sAnimEventName = "fortythree"
		BREAK
		
		CASE 44
			sAnimEventName = "fortyfour"
		BREAK
		
		CASE 45
			sAnimEventName = "fortyfive"
		BREAK
	ENDSWITCH
	
	RETURN sAnimEventName

ENDFUNC

FUNC FLOAT GET_ANIMATION_RESUMPTION_PHASE_FROM_INT(INT iValue)

	FLOAT fResumePhase

	SWITCH iValue
			
		CASE 1
			fResumePhase = 0.022253
		BREAK
		
		CASE 2
			fResumePhase = 0.043811
		BREAK
		
		CASE 3
			fResumePhase = 0.061196
		BREAK
		
		CASE 4
			fResumePhase = 0.084145
		BREAK
		
		CASE 5
			fResumePhase = 0.102225
		BREAK
		
		CASE 6
			fResumePhase = 0.118915
		BREAK
		
		CASE 7
			fResumePhase = 0.138387
		BREAK
		
		CASE 8
			fResumePhase = 0.156467
		BREAK
		
		CASE 9
			fResumePhase = 0.182197
		BREAK
		
		CASE 10
			fResumePhase = 0.202364
		BREAK
		
		CASE 11
			fResumePhase = 0.226704
		BREAK
		
		CASE 12
			fResumePhase = 0.249652
		BREAK
		
		CASE 13
			fResumePhase = 0.270515
		BREAK
		
		CASE 14
			fResumePhase = 0.292768
		BREAK
		
		CASE 15
			fResumePhase = 0.315021
		BREAK
		
		CASE 16
			fResumePhase = 0.342142
		BREAK
		
		CASE 17
			fResumePhase = 0.363700
		BREAK
		
		CASE 18
			fResumePhase = 0.385257
		BREAK
		
		CASE 19
			fResumePhase = 0.406120
		BREAK
		
		CASE 20
			fResumePhase = 0.426287
		BREAK
		
		CASE 21
			fResumePhase = 0.454798
		BREAK
		
		CASE 22
			fResumePhase = 0.476356
		BREAK
		
		CASE 23
			fResumePhase = 0.503477
		BREAK
		
		CASE 24
			fResumePhase = 0.524339
		BREAK
		
		CASE 25
			fResumePhase = 0.541725
		BREAK
		
		CASE 26
			fResumePhase = 0.565369
		BREAK
		
		CASE 27
			fResumePhase = 0.586926
		BREAK
		
		CASE 28
			fResumePhase = 0.611961
		BREAK
		
		CASE 29
			fResumePhase = 0.634910
		BREAK	
		
		CASE 30
			fResumePhase = 0.655772
		BREAK
		
		CASE 31
			fResumePhase = 0.678025
		BREAK
		
		CASE 32
			fResumePhase = 0.698192
		BREAK
		
		CASE 33
			fResumePhase = 0.719750
		BREAK	
		
		CASE 34
			fResumePhase = 0.738526
		BREAK
		
		CASE 35
			fResumePhase = 0.760779
		BREAK
		
		CASE 36
			fResumePhase = 0.781641
		BREAK
		
		CASE 37
			fResumePhase = 0.812239
		BREAK
		
		CASE 38
			fResumePhase = 0.830320
		BREAK
		
		CASE 39
			fResumePhase = 0.853268
		BREAK
		
		CASE 40
			fResumePhase = 0.886648
		BREAK
		
		CASE 41
			fResumePhase = 0.913769
		BREAK
		
		CASE 42
			fResumePhase = 0.934631
		BREAK
		
		CASE 43
			fResumePhase = 0.953408
		BREAK
		
		CASE 44
			fResumePhase = 0.975661
		BREAK
		
		CASE 45
			fResumePhase = 1.000000
		BREAK
	ENDSWITCH
	
	RETURN fResumePhase

ENDFUNC

//SPECIAL 2
//1,2,3,4,5,6,7,8,9
//no bag - 3,8,9

FUNC BOOL IS_PLAYER_WEARING_VISIBLE_BODY_ARMOUR()
	PED_COMP_NAME_ENUM ePedComp
	INT iCompNum
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		ePedComp = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2)
	ENDIF
	iCompNum = ENUM_TO_INT(ePedComp)
	IF iCompNum = 1
	OR iCompNum = 2
	OR iCompNum = 4
	OR iCompNum = 5
	OR iCompNum = 6
	OR iCompNum = 7
	OR iCompNum = 8
	OR iCompNum = 9
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC INT GET_PLAYER_BAG_TYPE( PED_INDEX playerPed )
	PED_COMP_NAME_ENUM ePedComp
 	IF NOT IS_PED_INJURED( playerPed )
		ePedComp = GET_PED_COMP_ITEM_CURRENT_MP( playerPed, COMP_TYPE_HAND )
	ENDIF
	
	RETURN ENUM_TO_INT( ePedComp )
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_A_BAG( PED_INDEX playerPed  )
	IF GET_PLAYER_BAG_TYPE( playerPed ) = 9
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 10
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 11
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 12
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 13
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 14
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 15
	OR GET_PLAYER_BAG_TYPE( playerPed ) = 16
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_PLAYER_BAG()
	//IF DOES_PLAYER_HAVE_A_BAG( PLAYER_PED_ID() )
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	//ENDIF
ENDPROC

//MP_M_FREEMODE_01
//MP_F_FREEMODE_01

PROC FILL_PLAYERS_HEIST_BAG()
	
	//Duffel Bag Empty //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 9, 0)
	IF GET_PLAYER_BAG_TYPE( PLAYER_PED_ID() ) = 9
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 10, 0)
	ENDIF
	//Duffel Bag Empty player with armour //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 11, 0)
	IF IS_PLAYER_WEARING_VISIBLE_BODY_ARMOUR()
		IF GET_PLAYER_BAG_TYPE( PLAYER_PED_ID() ) = 11
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 12, 0)
		ENDIF
	ENDIF
	
	//Sports Bag Empty //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 14, 0)
	IF GET_PLAYER_BAG_TYPE( PLAYER_PED_ID() ) = 13
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 14, 0)
	ENDIF
	
	//Sports Bag Empty player with armour //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 15, 0)
	IF IS_PLAYER_WEARING_VISIBLE_BODY_ARMOUR()
		IF GET_PLAYER_BAG_TYPE( PLAYER_PED_ID() ) = 15
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 16, 0)
		ENDIF
	ENDIF
	
ENDPROC

PROC GIVE_PLAYER_COMPONENT_BAG()

	//Sports Bag Empty player with armour //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 15, 0)
	IF IS_PLAYER_WEARING_VISIBLE_BODY_ARMOUR()
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 16, 0)
	ELSE
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 14, 0)
	ENDIF
	
ENDPROC


PROC TOGGLE_PLAYER_HEIST_BAG(BOOL bShowBag)
	
	IF bShowBag
		IF GET_PLAYER_BAG_TYPE( PLAYER_PED_ID() ) = 13
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 14, 0)
		ENDIF
		//Sports Bag Empty player with armour //SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 15, 0)
		IF IS_PLAYER_WEARING_VISIBLE_BODY_ARMOUR()
			IF GET_PLAYER_BAG_TYPE( PLAYER_PED_ID() ) = 15
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 16, 0)
			ENDIF
		ENDIF
	ELSE
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	ENDIF
	
ENDPROC


FUNC BOOL NEW_CREATE_BAG_OBJECTS(NETWORK_INDEX niThisBagIndex, NETWORK_INDEX niThisStrapIndex)
	
	REQUEST_MODEL(P_LD_Heist_Bag_S_1)
	REQUEST_MODEL(P_CSH_Strap_01_S)
	IF HAS_MODEL_LOADED(P_LD_Heist_Bag_S_1)
	AND HAS_MODEL_LOADED(P_CSH_Strap_01_S)
		IF CAN_REGISTER_MISSION_OBJECTS(2)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE) + 2)
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niThisBagIndex)
				IF CREATE_NET_OBJ(niThisBagIndex,P_LD_Heist_Bag_S_1,GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niThisBagIndex), TRUE)
				ENDIF
			ELSE
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niThisStrapIndex)
					IF CREATE_NET_OBJ(niThisStrapIndex,P_CSH_Strap_01_S,GET_ENTITY_COORDS(PLAYER_PED_ID()))
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niThisStrapIndex), TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(P_CSH_Strap_01_S)
						SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_Heist_Bag_S_1)
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//INT iGrabTimer

FUNC BOOL MANAGE_GRAB_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MANAGE_THIS_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// FORCE_QUIT_FAIL_CASH_GRAB_MINIGAME_KEEP_DATA (pass in your own grab data struct)
/// NOTES: Force the termination of the hacking minigame and cause the player to fail but preserves variable data.
/// PURPOSE:
///   Force the termination of the hacking minigame and cause the player to fail.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No Returns
///  
PROC FORCE_QUIT_FAIL_CASH_GRAB_MINIGAME_KEEP_DATA(S_CASH_GRAB_DATA &cash_grab_data)
	SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	RESET_CASH_GRAB_MINIGAME_DATA(cash_grab_data)
ENDPROC

PROC FORCE_QUIT_PASS_CASH_GRAB_MINIGAME_KEEP_DATA(S_CASH_GRAB_DATA &cash_grab_data)
	SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	RESET_CASH_GRAB_MINIGAME_DATA(cash_grab_data)
ENDPROC

FUNC BOOL HAS_PLAYER_BEAT_CASH_GRAB(S_CASH_GRAB_DATA &cash_grab_data,BOOL bAndForceQuit = FALSE)

	IF IS_BIT_SET(cash_grab_data.iBitSet, CASH_GRAB_BS_MINIGAME_COMPLETED)
		IF bAndForceQuit 
			FORCE_QUIT_PASS_CASH_GRAB_MINIGAME_KEEP_DATA(cash_grab_data)
			
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_ROUGHLY_FACING_THIS_DIRECTION(PED_INDEX ped, FLOAT idealHeading, FLOAT acceptableRange = 30.0)
	FLOAT upperLimit, lowerLimit
	upperLimit = idealHeading + (acceptableRange/2)
	IF upperLimit > 360
		upperLimit -= 360.0
	ENDIF
	
	lowerLimit = idealHeading - (acceptableRange/2)
	IF lowerLimit < 0
		lowerLimit += 360.0
	ENDIF
	
	IF NOT IS_PED_INJURED(ped)
		IF upperLimit > lowerLimit
			IF GET_ENTITY_HEADING(ped) < upperLimit
			AND GET_ENTITY_HEADING(ped) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF GET_ENTITY_HEADING(ped) < upperLimit
			OR GET_ENTITY_HEADING(ped) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GRAB_OBJECT_CASH(OBJECT_INDEX tempGrabObj)
	
	IF GET_ENTITY_MODEL(tempGrabObj)  = PROP_CASH_CRATE_01
	OR GET_ENTITY_MODEL(tempGrabObj)  = PROP_CASH_TROLLY
	OR GET_ENTITY_MODEL(tempGrabObj) =  HEI_PROP_HEI_CASH_TROLLY_01
	OR GET_ENTITY_MODEL(tempGrabObj) =  HEI_PROP_HEI_CASH_TROLLY_02
	OR GET_ENTITY_MODEL(tempGrabObj) =  HEI_PROP_HEI_CASH_TROLLY_03
	OR GET_ENTITY_MODEL(tempGrabObj) =  INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01A"))
	OR GET_ENTITY_MODEL(tempGrabObj) =  INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01B"))
	OR GET_ENTITY_MODEL(tempGrabObj) =  INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01C"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CLEANUP_CASH_GRAB_SCRIPTED_CAMERA(S_CASH_GRAB_DATA &data)

	IF DOES_CAM_EXIST(data.cameraIndex)
		DESTROY_CAM(data.cameraIndex)
	ENDIF
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC

PROC SET_PED_PROOFS_FOR_MINIGAME(PED_INDEX PedIndex)
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_PED_INJURED(PedIndex)
			SET_ENTITY_PROOFS(PedIndex, FALSE, TRUE, TRUE, FALSE, TRUE, FALSE, FALSE, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_PED_PROOFS_FOR_MINIGAME(PED_INDEX PedIndex)
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_PED_INJURED(PedIndex)
			SET_ENTITY_PROOFS(PedIndex, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
		ENDIF
	ENDIF
ENDPROC

