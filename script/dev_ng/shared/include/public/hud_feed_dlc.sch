USING "globals.sch"
USING "rage_builtins.sch"

// Header support for HUD_FEED_DLC.gfx

/// PURPOSE: Displays the DLC feed image and localised DLC body string in the top left of the HUD
/// Make sure you have loaded the movie with REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE, 
/// and checked it has loaded with HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED before using this.
/// PARAMS:
///    sHUDFeedDLC - Scaleform movie ID for the DLC Feed UI
///    sDLCText - DLC body string for the DLC Feed Message
///    sTXD - The Texture Dictionary for the image used in the DLC Feed Message
///    sImageName - The Image name string for the image used in the DLC Feed Message
PROC Set_Feed_DLC(eSCRIPT_HUD_COMPONENT sHUDFeedDLC, STRING sDLCText, STRING sTXD, STRING sImageName)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedDLC, "SET_FEED_DLC")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sDLCText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sImageName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Removes DLC component from screen. Be sure to remove the scaleform movie when it is no longer required
///    
/// PARAMS:
///    sHUDFeedDLC - Scaleform movie ID for the DLC Feed UI
PROC Remove_Feed_DLC(eSCRIPT_HUD_COMPONENT sHUDFeedDLC)

	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(sHUDFeedDLC, "REMOVE_FEED_DLC")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC
