USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"

USING "globals.sch"

ENUM AREA_CHECK_AREAS
	AC_NONE=0,
	
	AC_GOLF_COURSE,
	
	AC_AIRPORT_AIRSIDE,
	AC_MILITARY_BASE,
	AC_PRISON,
	AC_BIOTECH,
	AC_MILITARY_DOCKS,
	AC_MOVIE_STUDIO,
	AC_DOWNTOWN_POLICE,
	
	AC_END
ENDENUM

//CONST_INT TOTAL_AC_AREAS  5 //ENUM_TO_INT(AC_END)
//STRING g_sAreaSuppressedByThisScript[TOTAL_AC_AREAS]

CONST_INT TOTAL_SUB_AREAS 15

// Debug
#IF IS_DEBUG_BUILD
	BOOL	bRADebugSpam = FALSE
#ENDIF

/// PURPOSE:
///    Get the nominal centre point of a restricted area
/// PARAMS:
///    eArea - Area check enum for area you want to check
/// RETURNS:
///    Centre point coordinates for the restricted area
FUNC VECTOR GET_AREACHECK_CENTRE_POINT(AREA_CHECK_AREAS eArea)
	SWITCH eArea
		CASE AC_GOLF_COURSE 		RETURN << -1155.8766, 48.3426, 52.4985 >>		BREAK
		CASE AC_AIRPORT_AIRSIDE 	RETURN << -1245.12, -2818.38, 12.94 >>			BREAK
		CASE AC_MILITARY_BASE 		RETURN << -2176.19, 3092.07, 31.81 >>			BREAK
		CASE AC_PRISON 				RETURN << 1701.6664, 2586.2612, 51.4925 >>		BREAK
		CASE AC_BIOTECH 			RETURN << 3525.0576, 3711.3232, 35.6423 >>		BREAK
		CASE AC_MILITARY_DOCKS 		RETURN << 548.1421, -3157.9612, 5.0696 >>		BREAK
		CASE AC_MOVIE_STUDIO 		RETURN << -1142.4111, -526.4487, 31.6878 >>		BREAK
		CASE AC_DOWNTOWN_POLICE 	RETURN << 456.8879, -985.2783, 35.6895 >>		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Get the proximity check range, squared, of a restricted area
/// PARAMS:
///    eArea - Area check enum for area you want to check
/// RETURNS:
///    Square of the check distance
FUNC FLOAT GET_AREACHECK_RANGE(AREA_CHECK_AREAS eArea)
	SWITCH eArea
		CASE AC_GOLF_COURSE			RETURN 1000000.0	BREAK
		CASE AC_AIRPORT_AIRSIDE		RETURN 1000000.0	BREAK
		CASE AC_MILITARY_BASE		RETURN 1500000.0	BREAK
		CASE AC_PRISON				RETURN 500000.0		BREAK
		CASE AC_BIOTECH				RETURN 500000.0		BREAK
		CASE AC_MILITARY_DOCKS		RETURN 500000.0		BREAK
		CASE AC_MOVIE_STUDIO		RETURN 500000.0		BREAK
		CASE AC_DOWNTOWN_POLICE		RETURN 500000.0		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Do a quick proximity check to prevent doing the full check when not needed.
///    You should then do an IS_COORD_IN_SPECIFIED_AREA check if this returns true.
/// PARAMS:
///    eArea - Area you want to check
///    vPlayerCoords - Coordinates you want to check - 
/// RETURNS:
///    TRUE if in active range for this area
FUNC BOOL IS_AREACHECK_IN_ACTIVE_RANGE(AREA_CHECK_AREAS eArea, VECTOR vPlayerCoords)
	RETURN VDIST2(vPlayerCoords, GET_AREACHECK_CENTRE_POINT(eArea)) < GET_AREACHECK_RANGE(eArea)
ENDFUNC

/// PURPOSE:
///    Checks to see if a given coord lies within the specified area.
/// PARAMS:
///    vInput - Vector of point to check
///    acArea - Area to check
///    iUpperZ - Additional Z to check within from the default z-value for the upper corner of the angled area.
/// RETURNS:
///    TRUE if vInput is within the area checked.
FUNC BOOL IS_COORD_IN_SPECIFIED_AREA(VECTOR vInput, AREA_CHECK_AREAS acArea, INT iUpperZ = 0 #IF USE_TU_CHANGES , BOOL bOverrideZForAll = FALSE #ENDIF )
	
	// USE VDIST2 (VDIST^2) prior to using this check for wanted levels.
	VECTOR vPos1[TOTAL_SUB_AREAS]
	VECTOR vPos2[TOTAL_SUB_AREAS]
	FLOAT  fWidth[TOTAL_SUB_AREAS]
	INT    numberOfSubAreas, i
	BOOL   bHighlight = FALSE

	SWITCH acArea

		CASE AC_GOLF_COURSE 
			vPos1[0] = <<-1332.211304,100.460831,40.384373>>
			vPos2[0] = <<-1094.238159,148.427444,73.0>> 
			fWidth[0] = 171.250000
			vPos1[1] = <<-999.734375,-110.223091,25.257057>>
			vPos2[1] = <<-1149.493774,109.255829,73.0>> 
			fWidth[1] = 132.000000
			vPos1[2] = <<-1035.112915,-84.958855,28.274601>>
			vPos2[2] = <<-1261.103149,50.081482,73.0>> 
			fWidth[2] = 132.000000
			
			numberOfSubAreas = 3
		BREAK

		CASE AC_AIRPORT_AIRSIDE
			vPos1[0] = <<-804.343872,-3346.500488,10>>
			vPos2[0] = <<-1816.953857,-2768.893311,250 + iUpperZ>> 
			fWidth[0] = 247.000000
			vPos1[1] = <<-1911.487915,-2934.197021,10>>
			vPos2[1] = <<-968.623596,-3477.747559,250 + iUpperZ>> 
			fWidth[1] = 149.000000
			vPos1[2] = <<-844.943298,-2802.785156,10>>
			vPos2[2] = <<-1011.081055,-3086.903809,250 + iUpperZ>> 
			fWidth[2] = 185.500000
			vPos1[3] = <<-1021.086121,-2952.277100,10>>
			vPos2[3] = <<-1599.008179,-2616.270508,250 + iUpperZ>> 
			fWidth[3] = 250.000000
			vPos1[4] = <<-1027.136353,-2436.457031,10>>
			vPos2[4] = <<-1392.610474,-2226.763428,250 + iUpperZ>> 
			fWidth[4] = 193.500000
			vPos1[5] = <<-1497.549316,-2408.712158,10>>
			vPos2[5] = <<-1136.917358,-2617.954590,250 + iUpperZ>> 
			fWidth[5] = 234.500000
			vPos1[6] = <<-982.792358,-2831.708740,12.933130>>
			vPos2[6] = <<-966.467651,-2803.458008,16.683130>> 
			fWidth[6] = 16.000000

			vPos1[7] = <<-1110.083130,-3496.805664,12>>
			vPos2[7] = <<-1955.298218,-3010.431396,250 + iUpperZ>> 
			fWidth[7] = 80.00
			vPos1[8] = <<-1886.899414,-3193.023682,12>>
			vPos2[8] = <<-1836.142944,-3105.268311,250 + iUpperZ>> 
			fWidth[8] = 142.000
			vPos1[9] = <<-1134.336670,-3535.648193,12>>
			vPos2[9] = <<-1259.648560,-3463.486328,250 + iUpperZ>> 
			fWidth[9] =30.750000
			
			vPos1[10] = <<-969.127869,-3463.899414,12>>
			vPos2[10] = <<-896.373413,-3505.714844,250 + iUpperZ>> 
			fWidth[10] = 150.000000
			
			vPos1[11] = <<-1369.491333,-2173.578857,10>>
			vPos2[11] = <<-1685.625610,-2720.363525,250 + iUpperZ>> 
			fWidth[11] = 29.25
			
			vPos1[12] = <<-1010.925598,-3550.943359,10>>
			vPos2[12] = <<-1110.197754,-3493.617188,250 + iUpperZ>>
			fWidth[12] = 43.00

			numberOfSubAreas = 13
		BREAK

		CASE AC_MILITARY_BASE
			vPos1[0] = <<-1773.943970,3287.334229,30>>
			vPos2[0] = <<-2029.776489,2845.083252,250 + iUpperZ>>
			fWidth[0] =255.000000
			vPos1[1] = <<-2725.889404,3291.098633,30>>
			vPos2[1] = <<-2009.181519,2879.835205,250 + iUpperZ>>
			fWidth[1] =180.000000
			vPos1[2] = <<-2442.026123,3326.698730,30>>
			vPos2[2] = <<-2033.927856,3089.048828,250 + iUpperZ>>
			fWidth[2] =205.000000
			vPos1[3] = <<-1917.165405,3374.208984,30>>
			vPos2[3] = <<-2016.790894,3195.058105,250 + iUpperZ>>
			fWidth[3] =86.250000
			vPos1[4] = <<-2192.752930,3373.277832,30>>
			vPos2[4] = <<-2191.544434,3150.416504,250 + iUpperZ>>
			fWidth[4] =150.500000
			vPos1[5] = <<-2077.663330,3344.514160,30>>
			vPos2[5] = <<-2191.544434,3150.416504,250 + iUpperZ>>
			fWidth[5] =140.500000
			vPos1[6] = <<-2861.755371,3352.660645,30>>
			vPos2[6] = <<-2715.870850,3269.915527,250 + iUpperZ>>
			fWidth[6] =90.000000
			vPos1[7] = <<-2005.574463,3364.532715,30>>
			vPos2[7] = <<-1977.568848,3330.888184,250 + iUpperZ>>
			fWidth[7] =100.000000
			vPos1[8] = <<-1682.234985,3004.285156,30>>
			vPos2[8] = <<-1942.746948,2947.441162,250 + iUpperZ>>
			fWidth[8] =248.750000
			
			// No iUpperZ on these - fringe areas so leniency allowed for cases like Epsilon6
			vPos1[9] = <<-2393.295410,2936.406006,31.680103>>
			vPos2[9] = <<-2453.036621,3006.863037,52.310028>>
			fWidth[9] = 128.00
			vPos1[10] = <<-2347.184814,3023.829834,31.565729>>
			vPos2[10] = <<-2517.329834,2989.063477,49.956444>>
			fWidth[10] = 140.00
			vPos1[11] = <<-2259.921875,3358.039795,29.999718>>
			vPos2[11] = <<-2299.771973,3385.790039,38.060143>>
			fWidth[11] = 16.00
			vPos1[12] = <<-2476.309326,3363.914063,31.679329>>
			vPos2[12] = <<-2431.980713,3287.669434,39.978264>>
			fWidth[12] = 214.25
			vPos1[13] = <<-2103.081299,2797.783447,29.378639>>
			vPos2[13] = <<-2096.821289,2874.423340,57.809891>>
			fWidth[13] = 65.75
			
			#IF USE_TU_CHANGES
			
			// Updated for DLC_Assassin3.sc - military base assassination - x:\gta5\script\dev_network\singleplayer\scripts\missions\DLC_Assassinations\DLC_Assassin3.sc
			// Needed to override z values of the fringe areas of the base
			
			INT iFringeUpperZ
			// No iUpperZ on these - fringe areas so leniency allowed for cases like Epsilon6
			IF bOverrideZForAll
				iFringeUpperZ = iUpperZ
			ELSE
				iFringeUpperZ = 0
			ENDIF
			
			vPos2[9].z += iFringeUpperZ
			vPos2[10].z += iFringeUpperZ
			vPos2[11].z += iFringeUpperZ
			vPos2[12].z += iFringeUpperZ
			vPos2[13].z += iFringeUpperZ
			
			#ENDIF

			numberOfSubAreas = 14
		BREAK

		CASE AC_PRISON
			vPos1[0] = <<1541.607178,2527.554932,40>>
			vPos2[0] = <<1815.575317,2535.059570,150 + iUpperZ>>
			fWidth[0] = 114.000000
			vPos1[1] = <<1788.878662,2445.727295,40>>
			vPos2[1] = <<1716.960327,2502.957031,150 + iUpperZ>> 
			fWidth[1] = 88.500000
			vPos1[2] = <<1601.157471,2436.244141,40>>
			vPos2[2] = <<1650.077637,2515.922607,150 + iUpperZ>> 
			fWidth[2] = 133.250000
			vPos1[3] = <<1706.330688,2407.597168,40>>
			vPos2[3] = <<1698.554565,2460.207764,150 + iUpperZ>> 
			fWidth[3] = 104.500000				  
			vPos1[4] = <<1712.451660,2756.217529,40>>
			vPos2[4] = <<1718.847656,2589.161621,150 + iUpperZ>> 
			fWidth[4] = 121.750000				 
			vPos1[5] = <<1830.227783,2661.240234,40>>
			vPos2[5] = <<1774.812378,2679.418701,150 + iUpperZ>> 
			fWidth[5] = 84.500000
			vPos1[6] = <<1559.050293,2632.220459,40>>
			vPos2[6] = <<1657.208252,2595.484375,150 + iUpperZ>> 
			fWidth[6] = 103.750000				 
			vPos1[7] = <<1612.020874,2716.868896,40>>
			vPos2[7] = <<1657.164673,2669.720947,150 + iUpperZ>> 
			fWidth[7] = 104.250000				 
			vPos1[8] = <<1809.872070,2729.826904,40>>
			vPos2[8] = <<1789.855103,2705.036865,150 + iUpperZ>> 
			fWidth[8] = 91.000000
			vPos1[9] = <<1818.788818,2605.947754,40>>
			vPos2[9] = <<1783.114258,2606.783203,150 + iUpperZ>> 
			fWidth[9] = 51.250000

			numberOfSubAreas = 10
		BREAK
		
		CASE AC_BIOTECH
			vPos1[0] 	= <<3411.001953,3663.184570,20>>
			vPos2[0] 	= <<3615.583008,3626.193604,40 + iUpperZ>>
			fWidth[0] 	= 45.750000
			vPos1[1] 	= <<3426.659912,3733.077881,20>>
			vPos2[1] 	= <<3643.800781,3694.361816,40 + iUpperZ>> 
			fWidth[1]	= 99.000000
			vPos1[2] 	= <<3446.036377,3795.688232,20>>
			vPos2[2] 	= <<3650.914307,3766.151611,40 + iUpperZ>> 
			fWidth[2] 	= 81.500000
			
			numberOfSubAreas = 3
		BREAK
		
		CASE AC_MILITARY_DOCKS
			vPos1[0] 	= <<526.053040,-3391.496826,-10>>
			vPos2[0] 	= <<523.228943,-3118.677979,10 + iUpperZ>>
			fWidth[0] 	= 120.0
			vPos1[1] 	= <<459.439697,-3199.989502,4.819676>>
			vPos2[1] 	= <<593.892761,-3199.998047,30.069256>>
			fWidth[1] 	= 170.00
			vPos1[2] 	= <<552.846680,-3111.053711,4.819394>>
			vPos2[2] 	= <<585.313721,-3111.844238,17.569231>>
			fWidth[2] 	= 12.50
			vPos1[3] 	= <<598.466553,-3140.147461,4.819257>>
			vPos2[3] 	= <<597.497314,-3117.062744,17.319258>>
			fWidth[3] 	= 9.75
			
			numberOfSubAreas = 4
		BREAK
		
		CASE AC_MOVIE_STUDIO
			vPos1[0] 	= <<-1108.549683,-570.879761,20>>
			vPos2[0] 	= <<-1187.810791,-477.503662,50 + iUpperZ>> 
			fWidth[0] 	= 162.000000
			vPos1[1] 	= <<-1201.377563,-485.967316,20>>
			vPos2[1] 	= <<-1215.795898,-464.828064,50 + iUpperZ>> 
			fWidth[1] 	= 124.000000
			vPos1[2] 	= <<-985.631104,-525.423340,20>>
			vPos2[2] 	= <<-1013.393188,-475.205750,50 + iUpperZ>> 
			fWidth[2] 	= 55.000000
			vPos1[3] = <<-1055.849243,-477.822601,20>>
			vPos2[3] = <<-1073.332520,-498.717010,50 + iUpperZ>> 
			fWidth[3] = 142.000000

			numberOfSubAreas = 4
		BREAK
		
		CASE AC_DOWNTOWN_POLICE
			// Stairwell to roof
			vPos1[0] 	= <<461.568390,-984.571960,29.439508>>
			vPos2[0] 	= <<471.170044,-984.429199,40.142120>>
			fWidth[0] 	= 7.750000
			// Locker room corridor
			vPos1[1] 	= <<457.340393,-984.756042,34.439507>>
			vPos2[1] 	= <<457.208374,-993.718933,29.389584>>
			fWidth[1] 	= 14.750000
			// Roof
			vPos1[2] 	= <<477.622681,-986.600037,40.008190>>
			vPos2[2] 	= <<424.868713,-986.327881,48.712406>>
			fWidth[2] 	= 31.500000
			vPos1[3] 	= <<474.388947,-974.461304,39.557606>>
			vPos2[3] 	= <<474.035797,-1021.972107,49.100330>>
			fWidth[3] 	= 30.500000
			// Behind front desk
			vPos1[4] 	= <<442.176849,-974.188782,29.689508>>
			vPos2[4] 	= <<442.185516,-979.863525,33.439507>>
			fWidth[4] 	= 6.750000
			
			numberOfSubAreas = 5
		BREAK
	ENDSWITCH
	
	REPEAT numberOfSubAreas i
		IF IS_POINT_IN_ANGLED_AREA(vInput, vPos1[i], vPos2[i], fWidth[i], bHighlight)
			#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "IS_COORD_IN_SPECIFIED_AREA: Passed point << ", vInput.x, ",", vInput.y, ",", vInput.z, " >> found in volume ", i, " of ", numberOfSubAreas, " for restricted area ", ENUM_TO_INT(acArea)) ENDIF #ENDIF
			RETURN TRUE
		ENDIF

	ENDREPEAT
	
	#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "IS_COORD_IN_SPECIFIED_AREA: Passed point << ", vInput.x, ",", vInput.y, ",", vInput.z, " >> not in area check for area ", ENUM_TO_INT(acArea)) ENDIF #ENDIF
	RETURN FALSE
ENDFUNC
// BOOL bAllowPlayerInActiveRestrictedArea will allow the player to remain in a restricted area after the suppressing script has terminated. 
// This is particularly useful in the transition from FBI5a to FBI5b where the player remains in a restricted area off mission.
PROC SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AREA_CHECK_AREAS acArea, BOOL bAllowPlayerInActiveRestrictedArea = FALSE)
	SET_BIT(g_iRestrictedAreaBitSet, ENUM_TO_INT(acArea))
	g_sAreaSuppressedByThisScript[ENUM_TO_INT(acArea)] = GET_THIS_SCRIPT_NAME()
	g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)] = bAllowPlayerInActiveRestrictedArea
	#if IS_DEBUG_BUILD
		PRINTNL()
		PRINTSTRING("SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL has been called by ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTNL()
	#ENDIF
ENDPROC

FUNC BOOL IS_RESTRICTED_AREA_WANTED_LEVEL_SUPPRESSED(AREA_CHECK_AREAS acArea)
	IF IS_BIT_SET(g_iRestrictedAreaBitSet, ENUM_TO_INT(acArea))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AREA_CHECK_AREAS acArea, BOOL bAllowPlayerInActiveRestrictedArea = FALSE)
	IF IS_BIT_SET(g_iRestrictedAreaBitSet, ENUM_TO_INT(acArea))
		IF NOT bAllowPlayerInActiveRestrictedArea
			#if IS_DEBUG_BUILD
				PRINTNL()
				PRINTSTRING("RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL cleared this bit = ")
				PRINTINT(ENUM_TO_INT(acArea))
				PRINTNL()
			#ENDIF
			CLEAR_BIT(g_iRestrictedAreaBitSet, ENUM_TO_INT(acArea))
			g_sAreaSuppressedByThisScript[ENUM_TO_INT(acArea)] = "NULL"
			g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)] = bAllowPlayerInActiveRestrictedArea
		ENDIF
	ELSE
		#if IS_DEBUG_BUILD
			PRINTNL()
			PRINTSTRING("RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL but this bit is not set = ")
			PRINTINT(ENUM_TO_INT(acArea))
			PRINTNL()
		#ENDIF
	ENDIF
ENDPROC

FUNC TEXT_LABEL_23 WANTED_LEVEL_SUPPRESSING_SCRIPT(AREA_CHECK_AREAS acArea)
	RETURN g_sAreaSuppressedByThisScript[ENUM_TO_INT(acArea)]
ENDFUNC




