///  Hud_Meter.sch
///    Author : John R. Diaz
///    This is a base class for any rect based bars or meters used in the HUD
///    SO PLEASE NOTE ANY FILES INCLUDING THIS HEADER BEFORE MAKING ANY CHANGES IN HERE
///  	Not all types have been fleshed out as the need hasn't arisen yet.

USING "minigames_helpers.sch"
USING "hud_text.sch"

CONST_FLOAT		HUD_METER_OUTLINE_THICKNESS			0.005			//How thick the border of the meter is
CONST_FLOAT		HUD_METER_OUTLINE_THICKNESS_2		0.01
CONST_FLOAT		HUD_METER_LABEL_SIZE				0.35	
CONST_FLOAT		HUD_METER_LABEL_POSY				0.205	
CONST_FLOAT		HUD_METER_LETTER_HEIGHT				0.03
CONST_FLOAT		HUD_METER_LETTER_CENTER				0.01

CONST_FLOAT		HUD_METER_VERTICAL_START_POSX		0.89			//A default value in screenspace for the vertical Left most point
CONST_FLOAT		HUD_METER_VERTICAL_START_POSY		0.235			//A default value in screenspace for the vertical Top most point
CONST_FLOAT		HUD_METER_VERTICAL_WIDTH			0.04			//A default value for the vertical width
CONST_FLOAT		HUD_METER_VERTICAL_HEIGHT			0.5				//A default value for the vertical height

CONST_FLOAT		HUD_METER_VERTICAL_LAST_CUTOFF		0.605			//A default value for the bottom most hashmark
CONST_FLOAT		HUD_METER_VERTICAL_MID_CUTOFF		0.48			//A default value for the middle most hashmark
CONST_FLOAT		HUD_METER_VERTICAL_FIRST_CUTOFF		0.355			//A default value for the top most hashmark
CONST_FLOAT		HUD_METER_VERTICAL_CO_DIV			0.125			//A default value for the amount of space between each hashmark
CONST_FLOAT		HUD_METER_VERTICAL_CO_HEIGHT		0.006			//A default value for the height of each hashmark
CONST_INT		HUD_METER_VERTICAL_CO_ALPHA			245			//A default value for the alpha value of each hashmark 

CONST_FLOAT		HUD_METER_HORIZONTAL_START_POSX		0.4
CONST_FLOAT		HUD_METER_HORIZONTAL_START_POSY		0.113
CONST_FLOAT		HUD_METER_HORIZONTAL_WIDTH			0.20
CONST_FLOAT		HUD_METER_HORIZONTAL_HEIGHT			0.05

ENUM HUD_METER_STATES
	HMS_INIT = 0,
	HMS_RUN,
	HMS_NUM_STATES
ENDENUM

ENUM HUD_METER_TYPE
	HMT_RISING= 0,					//Draws a vertical meter that rises upwards
	HMT_FALLING,					//Draws a vertical meter that increases downwards
	HMT_SCROLL_RIGHT,				//Draws a horizontal meter that grows from left to right
	HMT_SCROLL_LEFT,				//Draws a horizontal meter that grows from right to left
	HMT_NUM_STATES
ENDENUM

STRUCT HUD_METER

	FLOAT fTickOffsetX
	
	STRING sLabel			//The label if needed
	STRING sHashLabel1
	STRING sHashLabel2
	STRING sHashLabel3
	STRING sHashLabel4
	

	FLOAT fLabelPosX		// the x coord of the label
	FLOAT fLabelPosY		//the y coord of the label

		
	FLOAT fPosX				//Left most point in screen space
	FLOAT fPosY				//Top most point in screen space
	FLOAT fWidth			//The width - how much to add to PosX
	FLOAT fHeight			//The height - how much to add to PosY

	FLOAT fMaxValue			//The max value (height or width)to completely fill it up
	FLOAT fMinY				//The min value to completely fill up in screen space?
	FLOAT fMaxY
	
	INT iR 					//Red Value
	INT iG					//Green Value
	INT iB					//Blue Value
	INT iA					//Alpha Value
	
	FLOAT fBG_PosX			//Background - Left most point in screen space
	FLOAT fBG_PosY			//Background  Top most point in screen space
	FLOAT fBG_Width			//Background  The width - how much to add to PosX
	FLOAT fBG_Height		//Background  The height - how much to add to PosY
	
	INT iBG_R 				//Background Red Value
	INT iBG_G				//Background Green Value
	INT iBG_B				//Background Blue Value
	INT iBG_A				//Background Alpha Value
	
	FLOAT fTickValue		//How much each tick in the meter is worth
	
	INT iZoneValue			//The current zone the meter is in 1- Green... 4 - Red
	
	BOOL bActive			//Where this meter is being drawn or not.
	BOOL bAlreadySetupUp
		
	HUD_METER_STATES eHMS
	HUD_METER_TYPE eType

ENDSTRUCT
PROC HUD_METER_SET_LABELS(HUD_METER &thisMeter, STRING sLabel, STRING sHash1,STRING sHash2, STRING sHash3, STRING sHash4)
	
	thisMeter.sLabel = sLabel
	thisMeter.sHashLabel1 = sHash1
	thisMeter.sHashLabel2 = sHash2
	thisMeter.sHashLabel3 = sHash3
	thisMeter.sHashLabel4 = sHash4

ENDPROC

/// PURPOSE: Set up the initial coords/ colors of the meter. For now everything is DEFAULTED from CONSTS, in the future I want to make it take params for each variable
///    
/// PARAMS:
///    thisMeter - The hudmeter to initiliaze
///    eMeterType - The type of meter you want it to be
///    fValueToDraw - the value that we are trying to represent

PROC HUD_METER_SETUP_VERTICAL_MANUAL(HUD_METER &thisMeter,HUD_METER_TYPE eMeterType , FLOAT fbasePosX = HUD_METER_VERTICAL_START_POSX, FLOAT fBasePosY = HUD_METER_VERTICAL_START_POSY, FLOAT fbaseWidth = HUD_METER_VERTICAL_WIDTH, FLOAT fBaseHeight = HUD_METER_VERTICAL_HEIGHT, FLOAT fOutlineThickness = HUD_METER_OUTLINE_THICKNESS, INT r = 0, INT g = 255, INT b = 0 , INT iAlpha = 250, HUD_COLOURS hudBGColor = HUD_COLOUR_BLACK)
	
	thisMeter.bAlreadySetupUp = TRUE
	thisMeter.fBG_PosX = fbasePosX	
	thisMeter.fBG_PosY = fbasePosY	
	thisMeter.fBG_Width = fbaseWidth
	thisMeter.fBG_Height = fbaseHeight
	
	
	thisMeter.fLabelPosX = thisMeter.fBG_PosX - HUD_METER_LETTER_CENTER
	thisMeter.fLabelPosY = thisMeter.fBG_PosY - HUD_METER_LETTER_HEIGHT
	
	
	thisMeter.fWidth = thisMeter.fBG_Width - fOutlineThickness
	thisMeter.fPosY = (thisMeter.fBG_PosY + thisMeter.fBG_Height) - fOutlineThickness
	thisMeter.fWidth = thisMeter.fBG_Width - (fOutlineThickness * 2)
	//Set the max value and width just a little in from the background outline
	thisMeter.fMaxValue = (thisMeter.fBG_Height - (fOutlineThickness * 2.0))
	
	thisMeter.fTickValue = ((thisMeter.fBG_Height - (fOutlineThickness * 2.0))/1000)
	
	
	//Set the top point of the rect to the bottom of the background
	SWITCH eMeterType
		CASE HMT_RISING		
		
			//Set the top point of the rect to the bottom of the background
			thisMeter.fPosY = (thisMeter.fBG_PosY + thisMeter.fBG_Height) - fOutlineThickness
			
			//When the meter is completely empty this is my Y value
			thisMeter.fMinY = thisMeter.fPosY 
			
			//When the meter is completely full this is my Y value
			thisMeter.fMaxY = thisMeter.fBG_PosY + fOutlineThickness
			
			thisMeter.eType = HMT_RISING
		BREAK
		
		CASE HMT_FALLING
		
			
			//Set the top point of the rect to the top of the background
			thisMeter.fPosY = thisMeter.fBG_PosY + fOutlineThickness
		
			thisMeter.fHeight = thisMeter.fBG_Height - (fOutlineThickness * 2)
			
			//When the meter is completely empty this is my Y value
			thisMeter.fMinY = thisMeter.fPosY 
			
			//When the meter is completely full this is my Y value
			thisMeter.fMaxY = thisMeter.fMinY

			
			thisMeter.eType = HMT_FALLING
		BREAK
	ENDSWITCH
	
	//Set the x-coord of the rect just a little in from the background outline
	thisMeter.fPosX = thisMeter.fBG_PosX + fOutlineThickness
	
	//Set preset alpha and RGB values
	thisMeter.iR = r
	thisMeter.iG = g
	thisMeter.iB = b
	thisMeter.iA = iAlpha
	
	GET_HUD_COLOUR(hudBGColor,thisMeter.iBG_R,thisMeter.iBG_G,thisMeter.iBG_B,thisMeter.iBG_A)
	thisMeter.iBG_A = iAlpha
	
	thisMeter.fTickOffsetX = (thisMeter.fBG_PosX + thisMeter.fBG_Width + fOutlineThickness )
	
	thisMeter.iZoneValue = 1
	
//	PRINTLN("fValue To Draw - ","", fValueToDraw, "Tick Value Here should be reasonable","", thisMeter.fTickValue)
	DEBUG_MESSAGE("HUD_METER_SETUP - INIT SUCCESS")
ENDPROC

PROC HUD_METER_SETUP_DEFAULT_BY_TYPE(HUD_METER &thisMeter,HUD_METER_TYPE eMeterType , FLOAT fValueToDraw)
	//TODO: Make this so you can pass in whatever coords and params
	
	thisMeter.fLabelPosX 	=	HUD_METER_VERTICAL_START_POSX
	thisMeter.fLabelPosY	=	HUD_METER_VERTICAL_START_POSY - 0.03
	
	SWITCH eMeterType
		CASE HMT_SCROLL_RIGHT
		CASE HMT_SCROLL_LEFT //TODO - WRITE THIS, OTHERWISE FOR NOW IT's A SCROLL_RIGHT
		
			//So here the x,y, start regularly on the left end of the bar
			thisMeter.fBG_PosX = 		HUD_METER_HORIZONTAL_START_POSX	
			thisMeter.fBG_PosY = 		HUD_METER_HORIZONTAL_START_POSY	
			thisMeter.fBG_Width = 		HUD_METER_HORIZONTAL_WIDTH		
			thisMeter.fBG_Height = 		HUD_METER_HORIZONTAL_HEIGHT		
			
			//Set the max value and height just a little in from the background outline
			thisMeter.fPosY = 	thisMeter.fBG_PosY 	+ 	HUD_METER_OUTLINE_THICKNESS
			thisMeter.fHeight = 	thisMeter.fBG_Height 	- HUD_METER_OUTLINE_THICKNESS
		
			thisMeter.eType = HMT_SCROLL_RIGHT
		BREAK
		
		CASE HMT_RISING
			thisMeter.fBG_PosX 		= 	HUD_METER_VERTICAL_START_POSX
			thisMeter.fBG_PosY 		= 	HUD_METER_VERTICAL_START_POSY
			thisMeter.fBG_Width 	= 	HUD_METER_VERTICAL_WIDTH
			thisMeter.fBG_Height 	= 	HUD_METER_VERTICAL_HEIGHT
		
			//Set the max value and width just a little in from the background outline
			thisMeter.fWidth = thisMeter.fBG_Width - HUD_METER_OUTLINE_THICKNESS_2
			
			//Set the top point of the rect to the bottom of the background
			thisMeter.fPosY = (thisMeter.fBG_PosY + thisMeter.fBG_Height) - HUD_METER_OUTLINE_THICKNESS
			
			//determine how large each bar tick is. This should almost always be HUD_METER_VERTICAL_START_POSY + HUD_METER_VERTICAL_HEIGHT, but for the Taxi Pickup Meter I use HUD_METER_VERTICAL_CO_DIV
			thisMeter.fTickValue = (HUD_METER_VERTICAL_CO_DIV/fValueToDraw)
	
			thisMeter.eType = HMT_RISING
		BREAK
		
		CASE HMT_FALLING
			thisMeter.fBG_PosX = 		HUD_METER_VERTICAL_START_POSX
			thisMeter.fBG_PosY = 		HUD_METER_VERTICAL_START_POSY
			thisMeter.fBG_Width = 		HUD_METER_VERTICAL_WIDTH
			thisMeter.fBG_Height = 		HUD_METER_VERTICAL_HEIGHT
		
			//Set the max value and width just a little in from the background outline
			thisMeter.fWidth = thisMeter.fBG_Width - HUD_METER_OUTLINE_THICKNESS_2
		
			//Set the top point of the rect to the top of the background
			thisMeter.fPosY = thisMeter.fBG_PosY + HUD_METER_OUTLINE_THICKNESS
			thisMeter.fHeight = thisMeter.fBG_Height - HUD_METER_OUTLINE_THICKNESS_2
			
			thisMeter.fTickValue = ((HUD_METER_VERTICAL_HEIGHT - (HUD_METER_OUTLINE_THICKNESS * 2.0))/fValueToDraw)
		
			
			thisMeter.eType = HMT_FALLING
		BREAK
		
	ENDSWITCH
	
	
	//Set the x-coord of the rect just a little in from the background outline
	thisMeter.fPosX = thisMeter.fBG_PosX + HUD_METER_OUTLINE_THICKNESS
	
	//Set preset alpha and RGB values
	thisMeter.iR = 0
	thisMeter.iG = 255
	thisMeter.iB = 0
	thisMeter.iA = 250
	
	GET_HUD_COLOUR(HUD_COLOUR_BLACK,thisMeter.iBG_R,thisMeter.iBG_G,thisMeter.iBG_B,thisMeter.iBG_A)
	thisMeter.iBG_A = 200
	
	thisMeter.fTickOffsetX = (thisMeter.fBG_PosX + thisMeter.fBG_Width + HUD_METER_OUTLINE_THICKNESS )
	
	thisMeter.iZoneValue = 1
	
	PRINTLN("fValue To Draw - ","", fValueToDraw, "Tick Value Here should be reasonable","", thisMeter.fTickValue)
	DEBUG_MESSAGE("HUD_METER_SETUP - INIT SUCCESS")
ENDPROC
/// PURPOSE: Starts the meter and enables it to be drawn
///    
/// PARAMS:
///    thisMeter - Our meter data
PROC HUD_METER_ENABLE(HUD_METER &thisMeter)
	
	IF NOT thisMeter.bActive

		thisMeter.bActive = TRUE
		DEBUG_MESSAGE("TURNED ON")

	ENDIF
ENDPROC
/// PURPOSE: Stops the Meter from Drawing & Updating
///    
/// PARAMS:
///    thisMeter - our meter data
PROC HUD_METER_DISABLE(HUD_METER &thisMeter)
	
	IF thisMeter.bActive

		thisMeter.bActive = FALSE
		DEBUG_MESSAGE("HUD_METER TURNED OFF")
	ENDIF
ENDPROC
/// PURPOSE: Grabs the int value of what zone the meter is currently in
///    
/// PARAMS:
///    thisMeter - our meter data
/// RETURNS: 1- Green, 2 - Yellow , 3- Orange, 4 - Red
///    
FUNC INT HUD_METER_GET_ZONE_VALUE(HUD_METER &thisMeter)

	PRINTLN("Hud Meter was in zone # ", "",thisMeter.iZoneValue)
	RETURN thisMeter.iZoneValue
	
ENDFUNC

/// PURPOSE: Call this to draw the meter. This is automatically called for you in HUD_METER_UDPATE
///    
/// PARAMS:
///    thisMeter - The meter to draw
PROC HUD_METER_DRAW(HUD_METER &thisMeter, BOOL bHashes = TRUE)
	//Draw White Border First
	DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX - HUD_METER_OUTLINE_THICKNESS,thisMeter.fBG_PosY - HUD_METER_OUTLINE_THICKNESS ,thisMeter.fBG_Width + (HUD_METER_OUTLINE_THICKNESS * 2.0),thisMeter.fBG_Height + (HUD_METER_OUTLINE_THICKNESS * 2.0),255,255, 255,thisMeter.iBG_A)
	
	//Draw Background First
	DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,thisMeter.fBG_PosY,thisMeter.fBG_Width,thisMeter.fBG_Height,thisMeter.iBG_R,thisMeter.iBG_G, thisMeter.iBG_B,thisMeter.iBG_A + 20)
	
	//Draw Meter in Front
	DRAW_RECT_FROM_CORNER(thisMeter.fPosX,thisMeter.fPosY,thisMeter.fWidth,thisMeter.fHeight,thisMeter.iR,thisMeter.iG, thisMeter.iB,thisMeter.iA)
	
	//Hashmarks
	
	//Green band is last
	IF thisMeter.eType = HMT_RISING
		IF bHashes
			IF thisMeter.fPosY > HUD_METER_VERTICAL_LAST_CUTOFF
				DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,HUD_METER_VERTICAL_LAST_CUTOFF,thisMeter.fBG_Width,HUD_METER_VERTICAL_CO_HEIGHT,255,255,0,HUD_METER_VERTICAL_CO_ALPHA)
			ENDIF
			
			//Draw AVG Cutoff
			IF thisMeter.fPosY > HUD_METER_VERTICAL_MID_CUTOFF
				DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,HUD_METER_VERTICAL_MID_CUTOFF,thisMeter.fBG_Width,HUD_METER_VERTICAL_CO_HEIGHT,255,165,0,HUD_METER_VERTICAL_CO_ALPHA)
			ENDIF
			
			IF thisMeter.fPosY > HUD_METER_VERTICAL_FIRST_CUTOFF
				DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,HUD_METER_VERTICAL_FIRST_CUTOFF,thisMeter.fBG_Width,HUD_METER_VERTICAL_CO_HEIGHT,255,0,0,HUD_METER_VERTICAL_CO_ALPHA)
			ENDIF
		ENDIF
		
		IF NOT IS_STRING_EMPTY(thisMeter.sHashLabel4)
			SET_TEXT_SCALE(HUD_METER_LABEL_SIZE, HUD_METER_LABEL_SIZE)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			SET_TEXT_EDGE(0, 0, 0, 0, 0)
//			DISPLAY_TEXT_WITH_LITERAL_STRING(thisMeter.fTickOffsetX, (thisMeter.fBG_Height + thisMeter.fBG_PosY) -(HUD_METER_OUTLINE_THICKNESS * 3), "STRING", thisMeter.sHashLabel4)
		ENDIF
	//Flop the First & Last Cutoff
	ELIF thisMeter.eType = HMT_FALLING
		IF bHashes
			IF (thisMeter.fPosY ) < HUD_METER_VERTICAL_FIRST_CUTOFF
				DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,HUD_METER_VERTICAL_FIRST_CUTOFF,thisMeter.fBG_Width,HUD_METER_VERTICAL_CO_HEIGHT,255,255,0,HUD_METER_VERTICAL_CO_ALPHA)
			ENDIF
			
			IF (thisMeter.fPosY ) < HUD_METER_VERTICAL_MID_CUTOFF
				DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,HUD_METER_VERTICAL_MID_CUTOFF,thisMeter.fBG_Width,HUD_METER_VERTICAL_CO_HEIGHT,255,165,0,HUD_METER_VERTICAL_CO_ALPHA)
			ENDIF
			
			IF (thisMeter.fPosY ) < HUD_METER_VERTICAL_LAST_CUTOFF
				DRAW_RECT_FROM_CORNER(thisMeter.fBG_PosX,HUD_METER_VERTICAL_LAST_CUTOFF,thisMeter.fBG_Width,HUD_METER_VERTICAL_CO_HEIGHT,255,0,0,HUD_METER_VERTICAL_CO_ALPHA)
			ENDIF
		ENDIF
		//Draw AVG Cutoff
		IF NOT IS_STRING_EMPTY(thisMeter.sHashLabel4)
			SET_TEXT_SCALE(HUD_METER_LABEL_SIZE, HUD_METER_LABEL_SIZE)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			SET_TEXT_EDGE(0, 0, 0, 0, 0)
//			DISPLAY_TEXT_WITH_LITERAL_STRING(thisMeter.fTickOffsetX, thisMeter.fBG_PosY - HUD_METER_OUTLINE_THICKNESS_2, "STRING", thisMeter.sHashLabel4)
		ENDIF
		
	ENDIF
	
	//LABEL////////
	IF NOT IS_STRING_EMPTY(thisMeter.sLabel)
		SET_TEXT_SCALE(HUD_METER_LABEL_SIZE, HUD_METER_LABEL_SIZE)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(thisMeter.fLabelPosX , thisMeter.fLabelPosY, "STRING", thisMeter.sLabel)
	ENDIF
	
	//2nd Hash Mark - "$$"
	IF NOT IS_STRING_EMPTY(thisMeter.sHashLabel2)
		SET_TEXT_SCALE(HUD_METER_LABEL_SIZE, HUD_METER_LABEL_SIZE)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(thisMeter.fTickOffsetX , HUD_METER_VERTICAL_MID_CUTOFF - HUD_METER_OUTLINE_THICKNESS_2, "STRING", thisMeter.sHashLabel2)
	ENDIF
	
	//1st Hash Mark - "$" or "$$$"
	IF NOT IS_STRING_EMPTY(thisMeter.sHashLabel1)
		SET_TEXT_SCALE(HUD_METER_LABEL_SIZE, HUD_METER_LABEL_SIZE)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(thisMeter.fTickOffsetX , HUD_METER_VERTICAL_FIRST_CUTOFF - HUD_METER_OUTLINE_THICKNESS_2, "STRING",thisMeter.sHashLabel1)
	ENDIF		
	
	//3rd Hash Mark - "$" or "$$$"
	IF NOT IS_STRING_EMPTY(thisMeter.sHashLabel3)
		SET_TEXT_SCALE(HUD_METER_LABEL_SIZE, HUD_METER_LABEL_SIZE)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(thisMeter.fTickOffsetX , HUD_METER_VERTICAL_LAST_CUTOFF - HUD_METER_OUTLINE_THICKNESS_2, "STRING", thisMeter.sHashLabel3)
	ENDIF
	
ENDPROC
/// PURPOSE: This handles some basic color changing when the meter clears the hashmarks
///    
/// PARAMS:
///    thisMeter - The meter to update
PROC HUD_METER_COLOR_UPDATE(HUD_METER &thisMeter)
	
	SWITCH thisMeter.eType
	
		CASE HMT_RISING
			//Set to Yellow
			IF thisMeter.fPosY < HUD_METER_VERTICAL_LAST_CUTOFF
			AND thisMeter.fPosY >= HUD_METER_VERTICAL_MID_CUTOFF
			AND thisMeter.iR <> 255
				
				thisMeter.iR = 255
				thisMeter.iG = 255
				thisMeter.iB = 0
			
				thisMeter.iZoneValue = 2
			
			//Set to Orange
			ELIF thisMeter.fPosY < HUD_METER_VERTICAL_MID_CUTOFF
			AND thisMeter.fPosY >= HUD_METER_VERTICAL_FIRST_CUTOFF
			AND thisMeter.iG  <> 69
				
				thisMeter.iR = 255
				thisMeter.iG = 69
				thisMeter.iB = 0
				
				thisMeter.iZoneValue = 3
			
			//Set to Red
			ELIF thisMeter.fPosY < HUD_METER_VERTICAL_FIRST_CUTOFF
			AND thisMeter.iG <> 0
				
				thisMeter.iR = 255
				thisMeter.iG = 0
				thisMeter.iB = 0
				
				thisMeter.iZoneValue = 4

			ENDIF
			
		BREAK
	
		CASE HMT_FALLING
			
			//Yellow
			IF thisMeter.fPosY > HUD_METER_VERTICAL_FIRST_CUTOFF
			AND thisMeter.fPosY <= HUD_METER_VERTICAL_MID_CUTOFF
			
				IF thisMeter.iR < 255
				
					thisMeter.iR = 255
					thisMeter.iG = 255
					thisMeter.iB = 0
					
					thisMeter.iZoneValue = 2
				ENDIF

			//Orange
			ELIF thisMeter.fPosY > HUD_METER_VERTICAL_MID_CUTOFF
			AND thisMeter.fPosY <= HUD_METER_VERTICAL_LAST_CUTOFF
			
			 	IF thisMeter.iG  > 69
					
					thisMeter.iR = 255
					thisMeter.iG = 69
					thisMeter.iB = 0
					
					thisMeter.iZoneValue = 3
				
				ENDIF
			
			//Red
			ELIF thisMeter.fPosY > HUD_METER_VERTICAL_LAST_CUTOFF
			
			 	IF thisMeter.iG > 0
				
					thisMeter.iR = 255
					thisMeter.iG = 0
					thisMeter.iB = 0
					
					
					thisMeter.iZoneValue = 4
				ENDIF	
				
			ENDIF
			
		BREAK
	ENDSWITCH
	
	
	
ENDPROC
/// PURPOSE: Our main update loop, driver function if you will
///    
/// PARAMS:
///    myMeter - 
///    eMeterType - 
///    fETA - 
///    meterTimer - a ref to the timer that controls how howoften the meter updates
///    
///    NOTE* - Only have HMT_RISING & HMT_FALLING correctly working, other types will be supported when they become necessary
PROC HUD_METER_UDPATE(HUD_METER &myMeter, HUD_METER_TYPE eMeterType, FLOAT fValue2Draw, structTimer &meterTimer, BOOL bDraw = FALSE)
	IF myMeter.bActive
		SWITCH myMeter.eHMS
		
			//Set up our data to some presets
			CASE HMS_INIT
				RESTART_TIMER_NOW(meterTimer) //TAXI_RESET_TIMERS(TT_METER_TICK)
				IF NOT myMeter.bAlreadySetupUp
					HUD_METER_SETUP_DEFAULT_BY_TYPE(myMeter,eMeterType,fValue2Draw)
				ENDIF
				myMeter.eHMS = HMS_RUN
				
				DEBUG_MESSAGE("HUD_METER_UDPATE = HMS_RUN")
			BREAK
			
			CASE HMS_RUN
				SWITCH myMeter.eType
		
					CASE HMT_SCROLL_LEFT
					CASE HMT_SCROLL_RIGHT
			
						myMeter.fWidth = myMeter.fTickValue
			
					BREAK
				
					CASE HMT_RISING
						
						//Make it so that the bar is capped at the height of the background. 
						IF (myMeter.fPosY - myMeter.fTickValue) > (myMeter.fBG_PosY + HUD_METER_OUTLINE_THICKNESS)
						AND TIMER_DO_WHEN_READY(meterTimer,1.0) //TAXI_TIMER_DO_WHEN_READY(TT_METER_TICK)

							myMeter.fHeight += myMeter.fTickValue
							myMeter.fPosY -= myMeter.fTickValue
							
							RESTART_TIMER_NOW(meterTimer)	//TAXI_RESET_TIMERS(TT_METER_TICK)	
							
						ENDIF
					BREAK
						
					CASE HMT_FALLING
					
						IF (myMeter.fPosY + myMeter.fTickValue) < ((myMeter.fBG_PosY + myMeter.fBG_Height) - HUD_METER_OUTLINE_THICKNESS)
						AND TIMER_DO_WHEN_READY(meterTimer,1.0)	//TAXI_TIMER_DO_WHEN_READY(TT_METER_TICK)
							myMeter.fPosY += myMeter.fTickValue
							myMeter.fHeight -= myMeter.fTickValue
							
							RESTART_TIMER_NOW(meterTimer)	//TAXI_RESET_TIMERS(TT_METER_TICK)	
							//PRINTLN("Tick Value - ","", myMeter.fTickValue)
						ENDIF
					BREAK
					
				ENDSWITCH
				
				HUD_METER_COLOR_UPDATE(myMeter)
				IF bDraw
					HUD_METER_DRAW(myMeter)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
