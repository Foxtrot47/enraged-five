//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     : script_oddjob_queues                               		    //
//      AUTHOR          : Steven Messinger                                              //
//      DESCRIPTION     : Group of functions used to queue conersations or text.        //
//	               This is going to need a major overhall and is mostly temp			//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
///    

 USING "timer_public.sch"
 USING "dialogue_public.sch"
 USING "minigames_helpers.sch"
  
///    //TODO quick and dirty..9/7/2011 need to move this to it's own header soon. don't want other script getting this extra memory overhead
///    
structPedsForConversation convPedStruct
structTimer delayTimer
TEXT_LABEL_31 sQuickMessage
TEXT_LABEL_15 sOFConv
TEXT_LABEL_15 sOFLine

CONST_INT MAX_NUM_TRACKED_ENTITIES 12
CONST_INT UNBLIP_DISTANCE 200

STRUCT ENTITY_BLIP_DATA
	ENTITY_INDEX myEntity
	BLIP_INDEX myBlip
	BOOL bDistance   //we'll unblip on death but need to set this bool if you want to unblip on distance
ENDSTRUCT

ENTITY_BLIP_DATA trackedPeds[MAX_NUM_TRACKED_ENTITIES]

FUNC BOOL REGISTER_BLIPPED_ENTITY(ENTITY_INDEX myEntity, BLIP_INDEX myBlip, BOOL bUnblipDist = TRUE)
	INT idx
	
	REPEAT MAX_NUM_TRACKED_ENTITIES idx
		IF trackedPeds[idx].myEntity = myEntity AND NOT DOES_BLIP_EXIST(trackedPeds[idx].myBlip)
			SCRIPT_ASSERT("This entity is already registered.. this causes big problems")
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_TRACKED_ENTITIES idx
		IF NOT DOES_BLIP_EXIST(trackedPeds[idx].myBlip)
			//we've found a free spot
			trackedPeds[idx].myEntity = myEntity
			trackedPeds[idx].myBlip = myBlip
			trackedPeds[idx].bDistance = bUnblipDist
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("REGISTER_BLIPPED_ENTITY failed!  No free slots in trackedPeds array")
	RETURN FALSE
	
ENDFUNC

PROC UPDATE_BLIPPED_ENTITIES()
	INT idx
	
	REPEAT MAX_NUM_TRACKED_ENTITIES idx
		IF DOES_BLIP_EXIST(trackedPeds[idx].myBlip)
			//Should we remove the blip?
			IF IS_ENTITY_DEAD(trackedPeds[idx].myEntity)
				REMOVE_BLIP(trackedPeds[idx].myBlip)
			ELIF trackedPeds[idx].bDistance AND GET_PLAYER_DISTANCE_FROM_ENTITY(trackedPeds[idx].myEntity) > UNBLIP_DISTANCE
				REMOVE_BLIP(trackedPeds[idx].myBlip)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL REMOVE_BLIPPED_ENTITIY(BLIP_INDEX myBlip)
	INT idx
	REPEAT MAX_NUM_TRACKED_ENTITIES idx
		IF DOES_BLIP_EXIST(trackedPeds[idx].myBlip) AND DOES_BLIP_EXIST(myBlip)
			IF myBlip = trackedPeds[idx].myBlip
				REMOVE_BLIP(trackedPeds[idx].myBlip)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("Was unable to find a matching blip index so REMOVE_BLIPPED_ENTITIY FAILED")
	RETURN FALSE
ENDFUNC

FUNC BOOL REMOVE_BLIPPED_ENTITIY_I(ENTITY_INDEX myEntity)
	INT idx
	REPEAT MAX_NUM_TRACKED_ENTITIES idx
		IF DOES_ENTITY_EXIST(trackedPeds[idx].myEntity)
			IF myEntity = trackedPeds[idx].myEntity
				REMOVE_BLIP(trackedPeds[idx].myBlip)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("Was unable to find a matching blip index so REMOVE_BLIPPED_ENTITIY FAILED")
	RETURN FALSE
ENDFUNC

PROC CLEAR_BLIPPED_ENTITIES()
	INT idx
	
	REPEAT MAX_NUM_TRACKED_ENTITIES idx
		IF DOES_BLIP_EXIST(trackedPeds[idx].myBlip)
			REMOVE_BLIP(trackedPeds[idx].myBlip)
		ENDIF
	ENDREPEAT
ENDPROC

//TODO possibly move this into calling scripts so we don't un-intentially include this unneeded text label
//This stuff needs a major overhaul.  Very basic and not all that helpful ATM.  Move to a seperate header.
TEXT_LABEL_31 sMessage

/// PURPOSE:
///    Quick group of functions to delay a print
/// PARAMS:
/// 	myTimer - Timer used to delay the print
PROC CLEAR_PRINT_DELAY(structTimer& myTimer)
	CANCEL_TIMER(myTimer)
	sMessage = ""
ENDPROC

//for now this isn't a queue.  Just a simple delay so we can do a print help and a print objective seperate
PROC REGISTER_PRINT_DELAY(structTimer& myTimer, STRING printMessage)
	RESTART_TIMER_NOW(myTimer)
	sMessage = printMessage
ENDPROC

PROC REGISTER_QUICK_CONV_QUEUE(structPedsForConversation myPedStruct, STRING sConversation, STRING sMyLine, BOOL bStopCurrentLine = FALSE)
	DEBUG_MESSAGE("REGISTER_QUICK_CONV_QUEUE")
	IF bStopCurrentLine
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF
	sOFConv = sConversation
	sOFLine = sMyLine
	convPedStruct = myPedStruct
ENDPROC

PROC UPDATE_QUICK_CONV_QUEUE()
	IF NOT ARE_STRINGS_EQUAL(sOFConv, "")
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			DEBUG_MESSAGE("Playing quick queue conversation")
			CREATE_CONVERSATION(convPedStruct, sOFConv, sOFLine, CONV_PRIORITY_VERY_HIGH)
			sOFConv = ""
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_QUICK_CONV_QUEUE()
	sOFConv = ""
ENDPROC


PROC REGISTER_QUICK_DELAY(STRING printMessage)
	//IF IS_TIMER_STARTED(delayTimer)
	DEBUG_MESSAGE("REGISTERING A DELAY")
	START_TIMER_NOW(delayTimer)
	RESTART_TIMER_NOW(delayTimer)
	sQuickMessage = printMessage
	//ENDIF
ENDPROC

PROC UPDATE_QUICK_DELAY()
	IF NOT ARE_STRINGS_EQUAL(sQuickMessage, "")
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() AND  TIMER_DO_WHEN_READY(delayTimer, 0.2)) AND NOT IS_SCRIPTED_CONVERSATION_ONGOING() //TIMER_DO_ONCE_WHEN_READY(delayTimer, 15.0) OR 
			PRINT_NOW(sQuickMessage, DEFAULT_GOD_TEXT_TIME, 1)
			sQuickMessage = ""
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_QUICK_DELAY()
	CANCEL_TIMER(delayTimer)
	sQuickMessage = ""
ENDPROC

PROC UPDATE_PRINT_DELAY(structTimer& myTimer, FLOAT timeDleay = 8.0, BOOL bPrintHelp = FALSE)
	IF TIMER_DO_ONCE_WHEN_READY(myTimer, timeDleay)
		IF bPrintHelp
			PRINT_HELP(sMessage)
		ELSE
			PRINT_NOW(sMessage, DEFAULT_GOD_TEXT_TIME, 1)
		ENDIF
	ENDIF
ENDPROC
