USING "street_crime_using.sch"
USING "street_crime_drawing.sch"
USING "net_arcade_cabinet.sch"

// ------------------------------ HELPERS ------------------------------

/// PURPOSE:
///    Updates the player's global state
PROC SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE eState)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_GLOBAL_PLAYER_STATE - State changed from ", 
		SCGW_DEBUG_GET_CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_AS_STRING(g_sCasinoArcadeStreetCrimeVars.ePlayerState), " to ", SCGW_DEBUG_GET_CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_AS_STRING(eState))
	
	g_sCasinoArcadeStreetCrimeVars.ePlayerState = eState
	
ENDPROC

/// PURPOSE:
///    Updates the player's global state
PROC SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION eState)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_GLOBAL_PLAYER_STATE - State changed from ", 
		SCGW_DEBUG_GET_CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_AS_STRING(g_sCasinoArcadeStreetCrimeVars.eThisFrameAction), " to ", SCGW_DEBUG_GET_CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_AS_STRING(eState))
	
	g_sCasinoArcadeStreetCrimeVars.eThisFrameAction = eState
	
ENDPROC

FUNC BOOL SHOULD_ALLOW_STREET_CRIME_IN_PROPERTY()
	SWITCH GET_SIMPLE_INTERIOR_TYPE_LOCAL_PLAYER_IS_IN()
		CASE SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT
		CASE SIMPLE_INTERIOR_TYPE_ARCADE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns true if the game should quit
///    Processes holding a button to quit
/// RETURNS:
///    
FUNC BOOL SCGW_SHOULD_QUIT_NOW()

	IF g_bForceQuitPenthouseArcadeMachines
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - g_bForceQuitPenthouseArcadeMachines")
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_LEAVING_ANY_SIMPLE_INTERIOR_USING_THE_LIMO_SERVICE()
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - IS_PLAYER_LEAVING_ANY_SIMPLE_INTERIOR_USING_THE_LIMO_SERVICE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_DOING_HELI_DOCK_CUTSCENE(GET_PLAYER_INDEX())
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - IS_PLAYER_DOING_HELI_DOCK_CUTSCENE")
		RETURN TRUE
	ENDIF
	
	IF NOT SHOULD_ALLOW_STREET_CRIME_IN_PROPERTY()
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - Not allowed in this property")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME")
		RETURN TRUE
	ENDIF
	
	IF SCGW_ServerBD.eCurrentServerState = SCGW_SERVER_STATE_RUNNING
	AND SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_CANT_RESPAWN
		RETURN FALSE
	ENDIF
	
	IF sSCGWData.eCurrentState < SCGW_CLIENT_STATE_INTRO_VIDEO
		RETURN FALSE
	ENDIF

	CONTROL_ACTION CA_Quit =  INPUT_FRONTEND_CANCEL
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_Quit = INPUT_FRONTEND_DELETE
	ENDIF
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Quit)
	OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Quit))
		DRAW_GENERIC_METER(ciSCGW_HOLD_TO_QUIT_TIME - ABSI(NATIVE_TO_INT(GET_NETWORK_TIME()) - sSCGWData.iQuitTime), ciSCGW_HOLD_TO_QUIT_TIME, "DEG_GAME_QUIT")
	ELSE
		DRAW_GENERIC_METER(0, ciSCGW_HOLD_TO_QUIT_TIME, "DEG_GAME_QUIT")
	ENDIF

	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Quit)
		
		IF sSCGWData.iQuitTime = -HIGHEST_INT
			sSCGWData.iQuitTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + ciSCGW_HOLD_TO_QUIT_TIME
		ENDIF
		
		//DRAW_GENERIC_METER(ciSCGW_HOLD_TO_QUIT_TIME - (sSCGWData.iQuitTime - NATIVE_TO_INT(GET_NETWORK_TIME())), ciSCGW_HOLD_TO_QUIT_TIME, "SCGW_EXIT", HUD_COLOUR_RED, -1, HUDORDER_TOP)
		
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) > sSCGWData.iQuitTime
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - Button Held")
			RETURN TRUE
		ENDIF
		
	ELIF sSCGWData.iQuitTime != -HIGHEST_INT
		sSCGWData.iQuitTime = -HIGHEST_INT
	ENDIF
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		IF SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_QUIT_NOW - SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME true")
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

// ------------------------------ LEADERBOARD ------------------------------

/// PURPOSE:
///    Returns the default initials for a leaderboard position
FUNC INT SCGW_GET_DEFAULT_INITIALS_FOR_POSITION(INT iPosition)

	SWITCH iPosition
		CASE 0	RETURN 69644	//Green - MAR
		CASE 1	RETURN 356361	//Purple - JAX
		CASE 2	RETURN 533761	//Yellow - BUC
		CASE 3	RETURN 861443	//Blue - DUS
		CASE 4	RETURN 21638	//Green - GSF
		CASE 5	RETURN 336649	//Purple - JMS
		CASE 6	RETURN 586639	//Yellow - POP
		CASE 7	RETURN 795403	//Blue - LMC
		CASE 8	RETURN 38023	//Green - HSJ
		CASE 9	RETURN 341123	//Purple - DST
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

/// PURPOSE:
///    Initialise the leaderboard data in ServerBD
PROC SCGW_INITIALISE_SERVER_LEADERBOARD(INT &iInitials[ciCASINO_ARCADE_LEADERBOARD_POSITIONS], INT &iScores[ciCASINO_ARCADE_LEADERBOARD_POSITIONS])
	
	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO ciCASINO_ARCADE_LEADERBOARD_POSITIONS - 1
	
		IF iScores[i] <= 0
			//First time - Initialise
			SCGW_ServerBD.sLeaderboard[i].iScore = 50 - (i * 2)
			SCGW_ServerBD.sLeaderboard[i].iInitials = SCGW_GET_DEFAULT_INITIALS_FOR_POSITION(i)
			BROADCAST_CASINO_ARCADE_UPDATE_LEADERBOARD_EVENT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), CASINO_ARCADE_GAME_STREET_CRIMES, i, SCGW_ServerBD.sLeaderboard[i].iInitials, SCGW_ServerBD.sLeaderboard[i].iScore)
		ELSE
			SCGW_ServerBD.sLeaderboard[i].iScore = iScores[i]
			SCGW_ServerBD.sLeaderboard[i].iInitials = iInitials[i]
		ENDIF
		
	ENDFOR
	
	SCGW_ServerBD.bLeaderboardRecieved = TRUE
	
ENDPROC

PROC SCGW_PROCESS_INITIALS_ENTERED_EVENT(PLAYER_INDEX piFromPlayer)
	
	INT iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piFromPlayer))
	
	INT iPosition = SCGW_GET_PLAYER_LEADERBOARD_POSITION(iParticipant)
	
	IF iPosition < 0
	OR iPosition >= ciCASINO_ARCADE_LEADERBOARD_POSITIONS
		EXIT
	ENDIF
	
	SCGW_ServerBD.sLeaderboard[iPosition].iInitials = SCGW_PlayerBD[iParticipant].iLeaderboardInitials
	BROADCAST_CASINO_ARCADE_UPDATE_LEADERBOARD_EVENT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), CASINO_ARCADE_GAME_STREET_CRIMES, iPosition, SCGW_ServerBD.sLeaderboard[iPosition].iInitials, SCGW_ServerBD.sLeaderboard[iPosition].iScore)
	
ENDPROC

PROC QUICK_SORT_LEADERBOARD(SCGW_LEADERBOARD_STRUCT &sArray[], INT iLeft, INT iRight)

	INT i, j
	INT p = sArray[((iLeft + iRight) / 2)].iScore
	SCGW_LEADERBOARD_STRUCT q
	i = iLeft
	j = iRight

	WHILE (i <= j)
	
		WHILE ((sArray[i].iScore > p) AND (i < iRight))
			i++
		ENDWHILE
		
		WHILE ((p > sArray[j].iScore) AND (j > iLeft))
			j--
		ENDWHILE
		
		IF (i <= j)
		
			q.iInitials = sArray[i].iInitials
			q.iPlayer = sArray[i].iPlayer
			q.iScore = sArray[i].iScore
			
			sArray[i].iInitials = sArray[j].iInitials
			sArray[i].iPlayer = sArray[j].iPlayer
			sArray[i].iScore = sArray[j].iScore
			
			sArray[j].iInitials = q.iInitials
			sArray[j].iPlayer = q.iPlayer
			sArray[j].iScore = q.iScore
			
			i++
			j--
		ENDIF
	
	ENDWHILE
	
	IF (i < iRight)
		QUICK_SORT_LEADERBOARD(sArray, i, iRight)
	ENDIF
	
	IF (iLeft < j)
		QUICK_SORT_LEADERBOARD(sArray, iLeft, j)
	ENDIF

ENDPROC

/// PURPOSE:
///    Sorts the leaderboard once players' final scores are set
PROC SCGW_SORT_SERVER_LEADERBOARD()
	
	SCGW_LEADERBOARD_STRUCT sTempArray[ciCASINO_ARCADE_LEADERBOARD_POSITIONS + ciSCGW_MAX_PLAYERS]
	
	INT i
	
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
	
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT SCGW_Bot[i].bActive
			RELOOP
		ENDIF
		
		IF SCGW_PlayerBD[i].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		sTempArray[i].iInitials = SCGW_PlayerBD[i].iLeaderboardInitials
		sTempArray[i].iScore = SCGW_PlayerBD[i].iLeaderboardScore
		sTempArray[i].iPlayer = i
	
	ENDFOR
	
	FOR i = 0 TO ciCASINO_ARCADE_LEADERBOARD_POSITIONS - 1
		sTempArray[i + ciSCGW_MAX_PLAYERS].iInitials = SCGW_ServerBD.sLeaderboard[i].iInitials
		sTempArray[i + ciSCGW_MAX_PLAYERS].iPlayer = SCGW_ServerBD.sLeaderboard[i].iPlayer
		sTempArray[i + ciSCGW_MAX_PLAYERS].iScore = SCGW_ServerBD.sLeaderboard[i].iScore
	ENDFOR
	
	QUICK_SORT_LEADERBOARD(sTempArray, 0, ciCASINO_ARCADE_LEADERBOARD_POSITIONS + ciSCGW_MAX_PLAYERS - 1)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SORT_SERVER_LEADERBOARD - Sorting")

	FOR i = 0 TO ciCASINO_ARCADE_LEADERBOARD_POSITIONS - 1
		SCGW_ServerBD.sLeaderboard[i].iInitials = sTempArray[i].iInitials
		SCGW_ServerBD.sLeaderboard[i].iPlayer = sTempArray[i].iPlayer
		SCGW_ServerBD.sLeaderboard[i].iScore = sTempArray[i].iScore
		BROADCAST_CASINO_ARCADE_UPDATE_LEADERBOARD_EVENT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), CASINO_ARCADE_GAME_STREET_CRIMES, i, SCGW_ServerBD.sLeaderboard[i].iInitials, SCGW_ServerBD.sLeaderboard[i].iScore)
	ENDFOR
	
ENDPROC

// ------------------------------ HELP TEXT ------------------------------

/// PURPOSE:
///    Returns whether or not the help text is on display
FUNC BOOL SCGW_IS_THIS_HELP_BEING_DISPLAYED(STRING stHelp)

	IF sSCGWData.iDisplayedHelpText = GET_HASH_KEY(stHelp)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Prints a help text string and sets iDisplayedHelpText
/// PARAMS:
///    stHelp - 
PROC SCGW_PRINT_HELP_FOREVER(STRING stHelp)
	
	sSCGWData.iDisplayedHelpText = GET_HASH_KEY(stHelp)
	PRINT_HELP_FOREVER(stHelp)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PRINT_HELP_FOREVER - Printing ", stHelp, "(",sSCGWData.iDisplayedHelpText,")")
	
ENDPROC

/// PURPOSE:
///    Prints a help text string with a number and sets iDisplayedHelpText
/// PARAMS:
///    stHelp - 
PROC SCGW_PRINT_HELP_FOREVER_WITH_NUMBER(STRING stHelp, INT iValue)
	
	sSCGWData.iDisplayedHelpText = GET_HASH_KEY(stHelp)
	PRINT_HELP_WITH_NUMBER(stHelp, iValue)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PRINT_HELP_FOREVER_WITH_NUMBER - Printing ", stHelp, "(",sSCGWData.iDisplayedHelpText,")")
	
ENDPROC

/// PURPOSE:
///    Clears help text and resets stCurrentHelpText & iCurrentHelpTextExpiry to stop it being redrawn
PROC SCGW_CLEAR_HELP_TEXT()

	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_CLEAR_HELP_TEXT - Clearing")
	DEBUG_PRINTCALLSTACK()

	sSCGWData.iDisplayedHelpText = -1
	sSCGWData.bDoNotApplyPC = FALSE

	IF sSCGWData.iCurrentHelpTextExpiry != -HIGHEST_INT
		sSCGWData.iCurrentHelpTextExpiry = -HIGHEST_INT
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sSCGWData.stCurrentHelpText)
		sSCGWData.stCurrentHelpText = ARCADE_GET_NULL_STRING()
	ENDIF

	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Draws start/quit helptext on the gang  screen
PROC SCGW_PROCESS_GANG_SCREEN_HELP_TEXT()

	SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOWING_HELP_TEXT)
	
	TEXT_LABEL_23 stHelp
	IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState = SCGW_PLAYER_STATE_WAITING_TO_START
		stHelp = "SCGW_H_RDY1"
	ELSE
		stHelp = "SCGW_H_RDY0"
		IF sSCGWData.sLocalPlayer.bIsHost AND sSCGWData.bCanAddBots
			stHelp += "_H"
		ENDIF
		IF IS_PLAYSTATION_PLATFORM()
			stHelp += "_PS4"
		ENDIF
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		stHelp += "_PC"
	ENDIF
		
	IF SCGW_IS_THIS_HELP_BEING_DISPLAYED(stHelp)
		EXIT
	ENDIF
	
	SCGW_CLEAR_HELP_TEXT()
	SCGW_PRINT_HELP_FOREVER(stHelp)
	
ENDPROC

/// PURPOSE:
///    Draws helptext on the leaderboard entry screen
PROC SCGW_PROCESS_LBD_ENTRY_HELP_TEXT()

	SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOWING_HELP_TEXT)
	
	TEXT_LABEL_15 stHelp
	
	IF SCGW_GET_PLAYER_LEADERBOARD_POSITION(sSCGWData.sLocalPlayer.iLocalPart) = ciSCGW_LBD_DEFAULT_RANK
		stHelp = "SC_H_LBD_2"
	ELIF sSCGWData.iInitial = ciARCADE_CABINET_LEADERBOARD_INITIALS - 2
		stHelp = "SC_H_LBD_1"
	ELSE
		stHelp = "SC_H_LBD_0"
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		stHelp += "PC"
	ENDIF
	
	IF SCGW_IS_THIS_HELP_BEING_DISPLAYED(stHelp)
		EXIT
	ENDIF
	
	SCGW_CLEAR_HELP_TEXT()
	SCGW_PRINT_HELP_FOREVER(stHelp)
	
ENDPROC

/// PURPOSE:
///    Draws helptext on the leaderboard screen
PROC SCGW_PROCESS_LBD_HELP_TEXT()

	SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOWING_HELP_TEXT)
	
	TEXT_LABEL_15 stHelp = "SC_H_LBD_3"
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		stHelp += "PC"
	ENDIF
	
	IF SCGW_IS_THIS_HELP_BEING_DISPLAYED(stHelp)
		EXIT
	ENDIF
	
	SCGW_CLEAR_HELP_TEXT()
	SCGW_PRINT_HELP_FOREVER(stHelp)
	
ENDPROC


/// PURPOSE:
///    Draws help text
PROC SCGW_PROCESS_HELP_TEXT()

	IF sSCGWData.iCurrentHelpTextExpiry = -HIGHEST_INT
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sSCGWData.stCurrentHelpText)
		EXIT
	ENDIF
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME()) > sSCGWData.iCurrentHelpTextExpiry

		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_HELP_TEXT - Text: ", sSCGWData.stCurrentHelpText, " has expired")
		
		SCGW_CLEAR_HELP_TEXT()
		
		EXIT
		
	ENDIF
	
	SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOWING_HELP_TEXT)

	TEXT_LABEL_15 stHelp = sSCGWData.stCurrentHelpText
	IF sSCGWData.iCurrentHelpTextValue = -1
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		AND NOT sSCGWData.bDoNotApplyPC
			stHelp += "_PC"
		ENDIF
	ELSE
		IF SCGW_IS_THIS_HELP_BEING_DISPLAYED(stHelp)
			EXIT
		ENDIF
		
		SCGW_PRINT_HELP_FOREVER_WITH_NUMBER(stHelp, sSCGWData.iCurrentHelpTextValue)
		EXIT
	ENDIF
	
	IF SCGW_IS_THIS_HELP_BEING_DISPLAYED(stHelp)
		EXIT
	ENDIF
	
	SCGW_PRINT_HELP_FOREVER(stHelp)

ENDPROC

/// PURPOSE:
///    Triggers help text
PROC SCGW_TRIGGER_HELP_TEXT(STRING stHelpText, INT iDuration, BOOL bDoNotApplyPC = FALSE)
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		SCGW_CLEAR_HELP_TEXT()
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_TRIGGER_HELP_TEXT - Triggering: ", stHelpText, " Duration: ", iDuration)
	
	sSCGWData.iCurrentHelpTextExpiry = NATIVE_TO_INT(GET_NETWORK_TIME()) + iDuration
	sSCGWData.stCurrentHelpText = stHelpText
	sSCGWData.iCurrentHelpTextValue = -1
	sSCGWData.bDoNotApplyPC = bDoNotApplyPC
	
ENDPROC

/// PURPOSE:
///    Triggers help text
PROC SCGW_TRIGGER_HELP_TEXT_WITH_INT(STRING stHelpText, INT iValue, INT iDuration, BOOL bDoNotApplyPC = FALSE)
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		SCGW_CLEAR_HELP_TEXT()
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_TRIGGER_HELP_TEXT_WITH_INT - Triggering: ", stHelpText, " iValue: ", iValue, " Duration: ", iDuration)
	
	sSCGWData.iCurrentHelpTextExpiry = NATIVE_TO_INT(GET_NETWORK_TIME()) + iDuration
	sSCGWData.iCurrentHelpTextValue = iValue
	sSCGWData.stCurrentHelpText = stHelpText
	sSCGWData.bDoNotApplyPC = bDoNotApplyPC
	
ENDPROC

/// PURPOSE:
///    Returns whether or not help text is being drawn by the minigame this frame
FUNC BOOL SCGW_SHOULD_ALLOW_HELP_TEXT_THIS_FRAME()

	RETURN IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOWING_HELP_TEXT)
	
ENDFUNC

// ------------------------------ EXPLOSIONS ------------------------------

/// PURPOSE:
///    Adds an explosion at the coords
PROC SCGW_ADD_EXPLOSION_AT_COORDS(VECTOR_2D vCoords)

	sSCGWData.sExplosions[sSCGWData.iNextExplosionIndex].iAnimFrame = 0
	sSCGWData.sExplosions[sSCGWData.iNextExplosionIndex].vPosition = vCoords
	IF VECTOR_2D_DIST2(vCoords, sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].vActualPlayerPosition) < POW(cfSCGW_GRID_SPRITE_SCALE_Y * 2, 2)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_EXPLODE_DURATION, ciSCGW_VIB_EXPLODE_FREQ)
	ENDIF
	
	SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Rocket_Explode", vCoords)
	
	sSCGWData.iNextExplosionIndex ++
	IF sSCGWData.iNextExplosionIndex >= ciSCGW_MAX_EXPLOSIONS
		sSCGWData.iNextExplosionIndex = 0
	ENDIF
	
ENDPROC

// ------------------------------ FLYOVERS ------------------------------

PROC SCGW_INIT_FLYOVER_POSITION()
	
	SWITCH sSCGWData.iFlyoverType
		CASE ciSCGW_FLYOVER_TYPE_BIRDS
		CASE ciSCGW_FLYOVER_TYPE_PLANE
			sSCGWData.vFlyoverPos = INIT_VECTOR_2D(1.0 + GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.0), 1.0 + GET_RANDOM_FLOAT_IN_RANGE(0.0, cfSCREEN_CENTER))
		BREAK
		CASE ciSCGW_FLYOVER_TYPE_HELI
			sSCGWData.vFlyoverPos = INIT_VECTOR_2D(0.0 + GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.5), 1.0 + GET_RANDOM_FLOAT_IN_RANGE(0.0, cfSCREEN_CENTER))
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC FLOAT SCGW_GET_FLYOVER_SPEED_X()
	
	SWITCH sSCGWData.iFlyoverType
		CASE ciSCGW_FLYOVER_TYPE_BIRDS
			RETURN cfSCGW_FLYOVER_BIRDS_SPRITE_SPEED_X
		CASE ciSCGW_FLYOVER_TYPE_PLANE
			RETURN cfSCGW_FLYOVER_PLANE_SPRITE_SPEED_X
		CASE ciSCGW_FLYOVER_TYPE_HELI
			RETURN cfSCGW_FLYOVER_HELI_SPRITE_SPEED_X
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

FUNC FLOAT SCGW_GET_FLYOVER_SPEED_Y()
	
	SWITCH sSCGWData.iFlyoverType
		CASE ciSCGW_FLYOVER_TYPE_BIRDS
			RETURN cfSCGW_FLYOVER_BIRDS_SPRITE_SPEED_Y
		CASE ciSCGW_FLYOVER_TYPE_PLANE
			RETURN cfSCGW_FLYOVER_PLANE_SPRITE_SPEED_Y
		CASE ciSCGW_FLYOVER_TYPE_HELI
			RETURN cfSCGW_FLYOVER_HELI_SPRITE_SPEED_Y
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

PROC SCGW_PROCESS_FLYOVERS()

	IF sSCGWData.iFlyoverTime = -HIGHEST_INT
		sSCGWData.iFlyoverTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	ENDIF
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME()) - sSCGWData.iFlyoverTime > ciSCGW_FLYOVER_INTERVAL
		sSCGWData.iFlyoverTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		INT iNewType = GET_RANDOM_INT_IN_RANGE(0, ciSCGW_FLYOVER_TYPE_MAX)
		IF iNewType = sSCGWData.iFlyoverType
			iNewType = GET_RANDOM_INT_IN_RANGE(0, ciSCGW_FLYOVER_TYPE_MAX)
		ENDIF
		sSCGWData.iFlyoverType = iNewType
		SCGW_INIT_FLYOVER_POSITION()
	ENDIF
	
	sSCGWData.vFlyoverPos.x = sSCGWData.vFlyoverPos.x +@ SCGW_GET_FLYOVER_SPEED_X()
	sSCGWData.vFlyoverPos.y = sSCGWData.vFlyoverPos.y +@ SCGW_GET_FLYOVER_SPEED_Y()
	
ENDPROC

// ------------------------------ ROUNDS ------------------------------

/// PURPOSE:
///    Returns the winning participant index for the current round
///    Returns -1 if round is tied for sudden death
FUNC INT SCGW_GET_ROUND_WINNER()
	
	INT iWinner = -1
	INT iTopScore = -1
	INT iPlayerLoop = 0
	BOOL bTied = FALSE
	BOOL bAllDeadSuddenDeath = TRUE
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
		AND NOT SCGW_Bot[iPlayerLoop].bActive
			RELOOP
		ENDIF
		
		IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		IF NOT SCGW_ServerBD.bAllDeadSuddenDeath
			IF SCGW_ServerBD.bSuddenDeath
			AND SCGW_IS_PLAYER_DEAD(iPlayerLoop)
				RELOOP
			ENDIF
			
			IF SCGW_ServerBD.bSuddenDeath
				bAllDeadSuddenDeath = FALSE
			ENDIF
		ENDIF
		
		IF SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[iPlayerLoop].iGang] > iTopScore
			iWinner = iPlayerLoop
			iTopScore = SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[iPlayerLoop].iGang]
			bTied = FALSE
		ELIF SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[iPlayerLoop].iGang] = iTopScore
			bTied = TRUE
		ENDIF
	ENDFOR

	IF bTied
	AND NOT SCGW_ServerBD.bAllDeadSuddenDeath
		iWinner = -1
	ENDIF
	
	IF bAllDeadSuddenDeath
	AND SCGW_ServerBD.bSuddenDeath
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_GET_ROUND_WINNER - All players dead in sudden death")
		SCGW_ServerBD.bAllDeadSuddenDeath = TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_GET_ROUND_WINNER - Winner: ", iWinner, " Top score: ", iTopScore) 
	RETURN iWinner
	
ENDFUNC

/// PURPOSE:
///    Returns the winning participant index for the entire game
FUNC INT SCGW_GET_GAME_WINNER()
	
	INT iWinner = -1
	INT iTopScore = -1
	INT iPlayerLoop = 0
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
		AND NOT SCGW_Bot[iPlayerLoop].bActive
			RELOOP
		ENDIF
		
		IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		IF SCGW_ServerBD.iRoundScores[SCGW_PlayerBD[iPlayerLoop].iGang] > iTopScore
			iWinner = iPlayerLoop
			iTopScore = SCGW_ServerBD.iRoundScores[SCGW_PlayerBD[iPlayerLoop].iGang]
		ELIF iWinner > -1
		AND SCGW_ServerBD.iRoundScores[SCGW_PlayerBD[iPlayerLoop].iGang] = iTopScore
			IF (SCGW_PlayerBD[iPlayerLoop].iLeaderboardScore + SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[iPlayerLoop].iGang]) > (SCGW_PlayerBD[iWinner].iLeaderboardScore + SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[iWinner].iGang])
				iWinner = iPlayerLoop
			ENDIF
		ENDIF
		
	ENDFOR
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_GET_GAME_WINNER - Winner: ", iWinner, " Top score: ", iTopScore) 
	RETURN iWinner
	
ENDFUNC

/// PURPOSE:
///    Returns true if end conditions for the round are met
FUNC BOOL SCGW_SHOULD_CURRENT_ROUND_END()
	
	IF NOT SCGW_ServerBD.bSuddenDeath
	AND NATIVE_TO_INT(GET_NETWORK_TIME()) > SCGW_ServerBD.iStartTime + ciSCGW_MATCH_LENGTH
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CURRENT_ROUND_END - RETURNING TRUE - ROUND TIMER EXPIRED") 
		RETURN TRUE
	ENDIF
	
	IF sSCGWData.iActivePlayers <= 1	
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CURRENT_ROUND_END - RETURNING TRUE - NOT ENOUGH PLAYERS LEFT") 
		RETURN TRUE
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_NONE, "Host S Skip")
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CURRENT_ROUND_END - RETURNING TRUE - HOST PRESSED S") 
		RETURN TRUE
	ENDIF
#ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Returns whether or not the game should advance to another round
FUNC BOOL SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND()

	IF sSCGWData.iConnectedPlayers <= 1
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND - Returning FALSE - Not enough players left to play") 
		RETURN FALSE
	ENDIF

	IF SCGW_ServerBD.iCurrentRound >= SCGW_ServerBD.iNumRounds
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND - Returning FALSE - No more rounds to play") 
		RETURN FALSE
	ENDIF
	
	INT iRequiredWins = FLOOR(TO_FLOAT(SCGW_ServerBD.iNumRounds) / 2.0)
	IF SCGW_ServerBD.iCurrentRound > iRequiredWins
		INT iPlayerLoop
		FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
			AND NOT SCGW_Bot[iPlayerLoop].bActive
				RELOOP
			ENDIF
			
			IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
				RELOOP
			ENDIF
			
			IF SCGW_ServerBD.iRoundScores[SCGW_PlayerBD[iPlayerLoop].iGang] > iRequiredWins
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND - Returning FALSE gang: ", SCGW_PlayerBD[iPlayerLoop].iGang, " Has enough wins to win the game") 
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND - Returning TRUE More rounds to play") 
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Increments the win stat for the player's gang when they win
PROC SCGW_UPDATE_PLAYER_WINS()
	
	INT iWins
	
#IF IS_DEBUG_BUILD
	IF sSCGWData.sDebug.bTriggerChallenges = TRUE
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_WINS - bTriggerChallenges ON") 
	ENDIF
#ENDIF
	
	SWITCH SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang
		
		CASE ciSCGW_GANG_GREEN
			iWins = GET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_0) + 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_WINS - Updating wins as gang 0 to, ", iWins) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_0, iWins)
			IF iWins = g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT
#IF IS_DEBUG_BUILD
			OR sSCGWData.sDebug.bTriggerChallenges = TRUE
#ENDIF
				SET_PACKED_STAT_BOOL(PACKED_MP_THE_HOODS, TRUE)
				SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO, "CLO_VWM_D_1_3", "UNLOCK_NAME_SHIRT3","FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				SCGW_TRIGGER_HELP_TEXT_WITH_INT("SCGW_CH_0", g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT, ciSCGW_DEFAULT_HELP_TEXT_TIME, TRUE)
			ENDIF
		BREAK
		
		CASE ciSCGW_GANG_PURPLE	
			iWins = GET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_1) + 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_WINS - Updating wins as gang 1 to, ", iWins) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_1, iWins)
			IF iWins = g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT
#IF IS_DEBUG_BUILD
			OR sSCGWData.sDebug.bTriggerChallenges = TRUE
#ENDIF
				SET_PACKED_STAT_BOOL(PACKED_MP_THE_PUNKS, TRUE)
				SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO, "CLO_VWM_D_1_4", "UNLOCK_NAME_SHIRT3","FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				SCGW_TRIGGER_HELP_TEXT_WITH_INT("SCGW_CH_1", g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT, ciSCGW_DEFAULT_HELP_TEXT_TIME, TRUE)
			ENDIF
		BREAK
		
		CASE ciSCGW_GANG_YELLOW	
			iWins = GET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_2) + 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_WINS - Updating wins as gang 2 to, ", iWins) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_2, iWins)
			IF iWins = g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT
#IF IS_DEBUG_BUILD
			OR sSCGWData.sDebug.bTriggerChallenges = TRUE
#ENDIF			
				SET_PACKED_STAT_BOOL(PACKED_MP_THE_YOKELS, TRUE)
				SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO, "CLO_VWM_D_1_5", "UNLOCK_NAME_SHIRT3","FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				SCGW_TRIGGER_HELP_TEXT_WITH_INT("SCGW_CH_2", g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT, ciSCGW_DEFAULT_HELP_TEXT_TIME, TRUE)
			ENDIF
		BREAK
		
		CASE ciSCGW_GANG_BLUE
			iWins = GET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_3) + 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_WINS - Updating wins as gang 3 to, ", iWins) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SCGW_NUM_WINS_GANG_3, iWins)
			IF iWins = g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT
#IF IS_DEBUG_BUILD
			OR sSCGWData.sDebug.bTriggerChallenges = TRUE
#ENDIF			
				SET_PACKED_STAT_BOOL(PACKED_MP_THE_BIKERS, TRUE)
				SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO, "CLO_VWM_D_1_2", "UNLOCK_NAME_SHIRT3","FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				SCGW_TRIGGER_HELP_TEXT_WITH_INT("SCGW_CH_3", g_sMPtunables.iVC_SCGW_WIN_CHALLENGE_REQUIREMENT, ciSCGW_DEFAULT_HELP_TEXT_TIME, TRUE)
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

// ------------------------------ GRID OWNERSHIP ------------------------------

/// PURPOSE:
///    Returns the single int position of the building at iGridX, iGridY
FUNC INT SCGW_GET_BUILDING_CENTER(INT iGridX, INT iGridY)

	RETURN SCGW_GET_POSITION_FROM_COORDS(1 + (iGridX * 2), 1 + (iGridY * 2))
	
ENDFUNC

/// PURPOSE:
///    Returns the gang that owns the grid space iGridX, iGridY
///    Returns ciSCGW_GANG_NONE if no gang owns the grid space
FUNC INT SCGW_GET_GRID_BUILDING_GANG_OWNER(INT iGridX, INT iGridY)
	
	INT iGridCenter = SCGW_GET_BUILDING_CENTER(iGridX, iGridY)
	
	INT i
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
	
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT SCGW_Bot[i].bActive
			RELOOP
		ENDIF
		
		IF SCGW_PlayerBD[i].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		//Top Row - Horizontal
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X - 1)
			RELOOP
		ENDIF
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X)
			RELOOP
		ENDIF
		
		//Top Row - Vertical
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X - 1)
			RELOOP
		ENDIF
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X + 1)
			RELOOP
		ENDIF
		
		//Middle Row - Vertical
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter - 1)
			RELOOP
		ENDIF
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter + 1)
			RELOOP
		ENDIF
		
		//Bottom Row - Horizontal
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter + ciSCGW_MAX_PLAYER_POSITIONS_X - 1)
			RELOOP
		ENDIF
		IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[i].iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter + ciSCGW_MAX_PLAYER_POSITIONS_X)
			RELOOP
		ENDIF
		
		RETURN SCGW_PlayerBD[i].iGang
		
	ENDFOR
	
	RETURN ciSCGW_GANG_NONE
	
ENDFUNC

/// PURPOSE:
///    Marks the grid side as claimed by this gang
///    HOST ONLY
PROC SCGW_SET_GRID_SIDE_AS_CLAIMED_BY_GANG(INT iGridIndex, INT iSide, INT iGang)
			
	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	INT i
	
	FOR i = 0 TO ciSCGW_GANG_MAX - 1
		IF i = iGang
			//Set as claimed for the gang
			IF NOT IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[i][iSide], iGridIndex)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_GRID_SIDE_AS_CLAIMED_BY_GANG - Setting grid index: ", iGridIndex, "'s side: ", iSide, " as claimed by gang: ", i) 
				SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[i][iSide], iGridIndex)
			ENDIF
		ELSE
			//Set all other gangs as unclaimed
			IF IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[i][iSide], iGridIndex)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_GRID_SIDE_AS_CLAIMED_BY_GANG - Setting grid index: ", iGridIndex, "'s side: ", iSide, " as no longer claimed by gang: ", i) 
				CLEAR_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[i][iSide], iGridIndex)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Maintains the gang ownership of the territories and updates scores
///    HOST ONLY
PROC SCGW_PROCESS_SERVER_BUILDING_OWNERSHIP()

	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF

	INT iNewScores[ciSCGW_MAX_PLAYERS]

	INT i, j
	FOR i = 0 TO ciSCGW_MAX_GRID_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y - 1
			INT iGangOwner = SCGW_GET_GRID_BUILDING_GANG_OWNER(i, j)
			IF SCGW_ServerBD.iGridGangOwnership[i][j] != iGangOwner
				SCGW_ServerBD.iGridGangOwnership[i][j] = iGangOwner
			ENDIF
			IF iGangOwner != ciSCGW_GANG_NONE
				iNewScores[iGangOwner]++
			ENDIF
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciSCGW_GANG_MAX - 1
		IF SCGW_UF_ServerBD.iCurrentScore[i] != iNewScores[i]
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_SERVER_BUILDING_OWNERSHIP - Gang ", i, "'s Score updated from ", SCGW_UF_ServerBD.iCurrentScore[i], " to ", iNewScores[i])
			SCGW_UF_ServerBD.iCurrentScore[i] = iNewScores[i]
		ENDIF
	ENDFOR

ENDPROC

// ------------------------------ INPUT PROCESSING ------------------------------

/// PURPOSE:
///    Returns true if no input processing should be done this frame
FUNC BOOL SCGW_IS_INPUT_DISABLED_THIS_FRAME()

	IF IS_PAUSE_MENU_ACTIVE()
		RETURN TRUE
	ENDIF

	IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL SCGW_IS_INPUT_DISABLED_THIS_FRAME_BOT(INT iBot)

	IF IS_PAUSE_MENU_ACTIVE()
		RETURN TRUE
	ENDIF

	IF SCGW_Bot[iBot].SCGW_player.eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC INT SCGW_GET_FIRST_INPUT_FOR_GANG(INT iGang)
	
	SWITCH iGang
		CASE ciSCGW_GANG_GREEN RETURN ciSCGW_INPUT_MOVE_RIGHT
		CASE ciSCGW_GANG_PURPLE RETURN ciSCGW_INPUT_MOVE_LEFT
		CASE ciSCGW_GANG_YELLOW RETURN ciSCGW_INPUT_MOVE_RIGHT
		CASE ciSCGW_GANG_BLUE RETURN ciSCGW_INPUT_MOVE_LEFT
	ENDSWITCH
	
	RETURN ciSCGW_INPUT_NONE

ENDFUNC

/// PURPOSE:
///    Returns the movement input pressed this frame if any
FUNC INT SCGW_GET_MOVE_INPUT()
	
	IF SCGW_IS_INPUT_DISABLED_THIS_FRAME()
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	
	IF NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_FIRST_MOVE_DONE)
		RETURN SCGW_GET_FIRST_INPUT_FOR_GANG(SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang)
	ENDIF
	
	CONTROL_ACTION CA_MoveUp = MAX_INPUTS
	CONTROL_ACTION CA_MoveDown = MAX_INPUTS
	CONTROL_ACTION CA_MoveLeft = MAX_INPUTS
	CONTROL_ACTION CA_MoveRight = MAX_INPUTS
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_MoveUp = MAX_INPUTS
		CA_MoveDown = MAX_INPUTS
		CA_MoveLeft = MAX_INPUTS
		CA_MoveRight = MAX_INPUTS
	ENDIF

	FLOAT fLeftStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	
	IF fLeftStickY < -cfSTICK_THRESHOLD
	OR (CA_MoveUp != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_MoveUp))
		RETURN ciSCGW_INPUT_MOVE_UP
	ENDIF
	
	IF fLeftStickY > cfSTICK_THRESHOLD
	OR (CA_MoveDown != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_MoveDown))
		RETURN ciSCGW_INPUT_MOVE_DOWN
	ENDIF
	
	FLOAT fLeftStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
	
	IF fLeftStickX < -cfSTICK_THRESHOLD
	OR (CA_MoveLeft != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_MoveLeft))
		RETURN ciSCGW_INPUT_MOVE_LEFT
	ENDIF
	
	IF fLeftStickX > cfSTICK_THRESHOLD
	OR (CA_MoveRight != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_MoveRight))
		RETURN ciSCGW_INPUT_MOVE_RIGHT
	ENDIF
	
	RETURN ciSCGW_INPUT_NONE
	
ENDFUNC

FUNC BOOL SCGW_IS_VALID_MOVEMENT(INT iMoveInputThisFrame, INT iCurrentPlayerX, INT iCurrentPlayerY)
	BOOL bValidMovement = FALSE
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_INPUT_BOT - iMoveInputThisFrame ",iMoveInputThisFrame)
	SWITCH iMoveInputThisFrame
		
		CASE ciSCGW_INPUT_MOVE_UP
			bValidMovement = iCurrentPlayerY > 0 AND iCurrentPlayerX % 2 = 0
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_DOWN
			bValidMovement = iCurrentPlayerY < ciSCGW_MAX_PLAYER_POSITIONS_Y - 1 AND iCurrentPlayerX % 2 = 0
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_LEFT
			bValidMovement = iCurrentPlayerX > 0 AND iCurrentPlayerY % 2 = 0
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_RIGHT
			bValidMovement = iCurrentPlayerX < ciSCGW_MAX_PLAYER_POSITIONS_X - 1 AND iCurrentPlayerY % 2 = 0
		BREAK
		
	ENDSWITCH
	RETURN bValidMovement
ENDFUNC

FUNC INT SCGW_BEST_MOVE_TO_TARGET(INT iPosX,INT iPosY,INT iTargetPosX,INT iTargetPosY)
	INT iMoveX = iTargetPosX - iPosX
	INT iMoveY = iTargetPosY - iPosY
	IF iTargetPosX < 0 OR iTargetPosX > ciSCGW_MAX_PLAYER_POSITIONS_X
	OR iTargetPosY < 0 OR iTargetPosY > ciSCGW_MAX_PLAYER_POSITIONS_Y
	OR iMoveX = 0 AND iMoveY = 0
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_BEST_MOVE_TO_TARGET - iMoveX = iTargetPosX - iPosX ---- ",iMoveX," = ",iTargetPosX," - ",iPosX)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_BEST_MOVE_TO_TARGET - iMoveY = iTargetPosY - iPosY ---- ",iMoveY," = ",iTargetPosY," - ",iPosY)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_BEST_MOVE_TO_TARGET - SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_DOWN, iPosX, iPosY) ",SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_DOWN, iPosX, iPosY))
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_BEST_MOVE_TO_TARGET - SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_UP, iPosX, iPosY) ",SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_UP, iPosX, iPosY))
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_BEST_MOVE_TO_TARGET - SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_RIGHT, iPosX, iPosY) ",SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_RIGHT, iPosX, iPosY))
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_BEST_MOVE_TO_TARGET - SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_LEFT, iPosX, iPosY) ",SCGW_IS_VALID_MOVEMENT(ciSCGW_INPUT_MOVE_LEFT, iPosX, iPosY))
	
	INT hMove
	INT vMove
	IF iMoveY < 0
		vMove = ciSCGW_INPUT_MOVE_UP
	ELSE
		vMove = ciSCGW_INPUT_MOVE_DOWN
	ENDIF
	IF iMoveX < 0
		hMove = ciSCGW_INPUT_MOVE_LEFT
	ELSE
		hMove = ciSCGW_INPUT_MOVE_RIGHT
	ENDIF
	IF ABSI(iMoveX) < ABSI(iMoveY)
		IF iPosX % 2 = 0
			RETURN vMove
		ELSE
			RETURN hMove
		ENDIF
	ELSE
		IF iPosY % 2 = 0
			RETURN hMove
		ELSE
			RETURN vMove
		ENDIF
	ENDIF
	RETURN ciSCGW_INPUT_NONE
ENDFUNC

FUNC INT SCGW_GET_MOVE_INPUT_BOT(INT iBot)
	
	IF SCGW_IS_INPUT_DISABLED_THIS_FRAME_BOT(iBot)
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	
	IF NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_FIRST_MOVE_DONE)
		RETURN SCGW_GET_FIRST_INPUT_FOR_GANG(SCGW_Bot[iBot].SCGW_player.iGang)
	ENDIF

	INT iTargetPosX = SCGW_Bot[iBot].iTargetPosX
	INT iTargetPosY = SCGW_Bot[iBot].iTargetPosY
	INT iPosX
	INT iPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	INT iBestMove = SCGW_BEST_MOVE_TO_TARGET(iPosX, iPosY, iTargetPosX, iTargetPosY)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_GET_MOVE_INPUT_BOT - iBot ",iBot," SCGW_BEST_MOVE_TO_TARGET(",iPosX,", ",iPosY,", ",iTargetPosX,", ",iTargetPosY,") ",iBestMove )
	
	RETURN iBestMove
	
	RETURN ciSCGW_INPUT_NONE
	
ENDFUNC

/// PURPOSE:
///    Returns the turning input pressed this frame if any
FUNC INT SCGW_GET_TURN_INPUT()
	
	IF SCGW_IS_INPUT_DISABLED_THIS_FRAME()
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	
	CONTROL_ACTION CA_TurnUp = MAX_INPUTS
	CONTROL_ACTION CA_TurnDown = MAX_INPUTS 
	CONTROL_ACTION CA_TurnLeft = MAX_INPUTS
	CONTROL_ACTION CA_TurnRight = MAX_INPUTS
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_TurnUp = INPUT_FRONTEND_UP
		CA_TurnDown = INPUT_FRONTEND_DOWN 
		CA_TurnLeft = INPUT_FRONTEND_LEFT
		CA_TurnRight = INPUT_FRONTEND_RIGHT
	ENDIF

	FLOAT fRightStickY = 0
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		fRightStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	ENDIF
	
	IF fRightStickY < -cfSTICK_THRESHOLD
	OR (CA_TurnUp != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_TurnUp))
		RETURN ciSCGW_INPUT_TURN_UP
	ENDIF
	
	IF fRightStickY > cfSTICK_THRESHOLD
	OR (CA_TurnDown != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_TurnDown))
		RETURN ciSCGW_INPUT_TURN_DOWN
	ENDIF
	
	FLOAT fRightStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	
	IF fRightStickX < -cfSTICK_THRESHOLD
	OR (CA_TurnLeft != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_TurnLeft))
		RETURN ciSCGW_INPUT_TURN_LEFT
	ENDIF
	
	IF fRightStickX > cfSTICK_THRESHOLD
	OR (CA_TurnRight != MAX_INPUTS AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_TurnRight))
		RETURN ciSCGW_INPUT_TURN_RIGHT
	ENDIF

	RETURN ciSCGW_INPUT_NONE
	
ENDFUNC

FUNC INT SCGW_GET_TURN_INPUT_BOT(INT iBot)
	
	IF SCGW_IS_INPUT_DISABLED_THIS_FRAME_BOT(iBot)
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	
	RETURN SCGW_Bot[iBot].iFaceDirection
	
ENDFUNC
/// PURPOSE:
///    Returns the shooting input pressed this frame if any
FUNC INT SCGW_GET_SHOOT_INPUT()

	IF SCGW_IS_INPUT_DISABLED_THIS_FRAME()
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	
	CONTROL_ACTION CA_Shoot =  INPUT_SCRIPT_RT
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_Shoot = INPUT_FRONTEND_X
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, CA_Shoot)
		RETURN ciSCGW_INPUT_SHOOT
	ENDIF
	
	RETURN ciSCGW_INPUT_NONE
	
ENDFUNC

FUNC INT SCGW_GET_SHOOT_INPUT_BOT(INT iBot)

	IF SCGW_IS_INPUT_DISABLED_THIS_FRAME_BOT(iBot)
		RETURN ciSCGW_INPUT_NONE
	ENDIF
	
//	CONTROL_ACTION CA_Shoot =  INPUT_SCRIPT_RT
//	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//		CA_Shoot = INPUT_FRONTEND_X
//	ENDIF
//	
	IF SCGW_Bot[iBot].bShouldShoot
		RETURN ciSCGW_INPUT_SHOOT
	ENDIF
	
	RETURN ciSCGW_INPUT_NONE
	
ENDFUNC

/// PURPOSE:
///    Returns true if the player can shoot in iDirection with no immediate obstructions (screen edges and buildings)
FUNC BOOL SCGW_CAN_PLAYER_SHOOT_THIS_DIRECTION(INT iDirection)
	
	VECTOR_2D vMax = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_GET_POSITION_FROM_COORDS(ciSCGW_MAX_PLAYER_POSITIONS_X - 1, ciSCGW_MAX_PLAYER_POSITIONS_Y - 1))
	VECTOR_2D vMin = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_GET_POSITION_FROM_COORDS(0, 0))
	
	//Map Bounds
	SWITCH iDirection
	
		CASE ciSCGW_GRID_DIRECTION_UP
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].vActualPlayerPosition.y, 
			vMin.y - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMin.y + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_GRID_DIRECTION_DOWN
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].vActualPlayerPosition.y, 
			vMax.y - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMax.y + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_GRID_DIRECTION_LEFT
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].vActualPlayerPosition.x, 
			vMin.x - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMin.x + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_GRID_DIRECTION_RIGHT
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].vActualPlayerPosition.x, 
			vMax.x - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMax.x + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	
	//While Moving
	IF SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(sSCGWData.sLocalPlayer.iLocalPart) > NATIVE_TO_INT(GET_NETWORK_TIME()) 
		IF iCurrentPlayerY != iPreviousPlayerY
			//Vertical Movement
			IF iDirection = ciSCGW_GRID_DIRECTION_LEFT
			OR iDirection = ciSCGW_GRID_DIRECTION_RIGHT
				RETURN FALSE
			ENDIF
		ELIF iCurrentPlayerX != iPreviousPlayerX
			//Horizontal Movement
			IF iDirection = ciSCGW_GRID_DIRECTION_UP
			OR iDirection = ciSCGW_GRID_DIRECTION_DOWN
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SCGW_CAN_PLAYER_SHOOT_THIS_DIRECTION_BOT(INT iBot, INT iDirection)
	
	VECTOR_2D vMax = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_GET_POSITION_FROM_COORDS(ciSCGW_MAX_PLAYER_POSITIONS_X - 1, ciSCGW_MAX_PLAYER_POSITIONS_Y - 1))
	VECTOR_2D vMin = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_GET_POSITION_FROM_COORDS(0, 0))
	
	//Map Bounds
	SWITCH iDirection
	
		CASE ciSCGW_GRID_DIRECTION_UP
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iBot].vActualPlayerPosition.y, 
			vMin.y - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMin.y + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_GRID_DIRECTION_DOWN
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iBot].vActualPlayerPosition.y, 
			vMax.y - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMax.y + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_GRID_DIRECTION_LEFT
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iBot].vActualPlayerPosition.x, 
			vMin.x - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMin.x + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_GRID_DIRECTION_RIGHT
			IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iBot].vActualPlayerPosition.x, 
			vMax.x - cfARCADE_MINIGAME_FLOAT_TOLERANCE, 
			vMax.x + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	
	//While Moving
	IF SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(iBot) > NATIVE_TO_INT(GET_NETWORK_TIME()) 
		IF iCurrentPlayerY != iPreviousPlayerY
			//Vertical Movement
			IF iDirection = ciSCGW_GRID_DIRECTION_LEFT
			OR iDirection = ciSCGW_GRID_DIRECTION_RIGHT
				RETURN FALSE
			ENDIF
		ELIF iCurrentPlayerX != iPreviousPlayerX
			//Horizontal Movement
			IF iDirection = ciSCGW_GRID_DIRECTION_UP
			OR iDirection = ciSCGW_GRID_DIRECTION_DOWN
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Returns true if the player can turn towards iTurnInput
///    False if turning would face a building
FUNC BOOL SCGW_CAN_PLAYER_TURN_THIS_DIRECTION(INT iParticipant, INT iTurnInput)
	
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	
	//Mid Grid
	SWITCH iTurnInput
	
		CASE ciSCGW_INPUT_TURN_UP
		CASE ciSCGW_INPUT_TURN_DOWN
			IF iCurrentPlayerX % 2 != 0
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_INPUT_TURN_LEFT
		CASE ciSCGW_INPUT_TURN_RIGHT
			IF iCurrentPlayerY % 2 != 0
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//While Moving
	IF SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(iParticipant) > NATIVE_TO_INT(GET_NETWORK_TIME()) 
		IF iCurrentPlayerY != iPreviousPlayerY
			//Vertical Movement
			IF iTurnInput = ciSCGW_INPUT_TURN_LEFT
			OR iTurnInput = ciSCGW_INPUT_TURN_RIGHT
				RETURN FALSE
			ENDIF
		ELIF iCurrentPlayerX != iPreviousPlayerX
			//Horizontal Movement
			IF iTurnInput = ciSCGW_INPUT_TURN_UP
			OR iTurnInput = ciSCGW_INPUT_TURN_DOWN
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SCGW_CAN_PLAYER_TURN_THIS_DIRECTION_BOT(INT iBot, INT iTurnInput)
	
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	
	//Mid Grid
	SWITCH iTurnInput
	
		CASE ciSCGW_INPUT_TURN_UP
		CASE ciSCGW_INPUT_TURN_DOWN
			IF iCurrentPlayerX % 2 != 0
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_INPUT_TURN_LEFT
		CASE ciSCGW_INPUT_TURN_RIGHT
			IF iCurrentPlayerY % 2 != 0
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//While Moving
	IF SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(sSCGWData.sLocalPlayer.iLocalPart) > NATIVE_TO_INT(GET_NETWORK_TIME()) 
		IF iCurrentPlayerY != iPreviousPlayerY
			//Vertical Movement
			IF iTurnInput = ciSCGW_INPUT_TURN_LEFT
			OR iTurnInput = ciSCGW_INPUT_TURN_RIGHT
				RETURN FALSE
			ENDIF
		ELIF iCurrentPlayerX != iPreviousPlayerX
			//Horizontal Movement
			IF iTurnInput = ciSCGW_INPUT_TURN_UP
			OR iTurnInput = ciSCGW_INPUT_TURN_DOWN
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Processes moving the local player based on their input
PROC SCGW_PROCESS_MOVEMENT_INPUT()

	INT iMoveInputThisFrame = SCGW_GET_MOVE_INPUT()
	BOOL bDirectionChange = FALSE
	
	IF NOT (SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(sSCGWData.sLocalPlayer.iLocalPart) < NATIVE_TO_INT(GET_NETWORK_TIME())  
	AND IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED))
	AND NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementTime + ciSCGW_PLAYER_CHANGE_DIRECTION_TIME
	AND SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementDirection != iMoveInputThisFrame
	AND NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_DIRECTION_CHANGED)	
		bDirectionChange = TRUE
	ENDIF
	
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	IF bDirectionChange
		SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
		SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	ELSE
		SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
		SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	ENDIF
	
	BOOL bValidMovement = FALSE
	INT iPositionChangeY = 0
	INT iPositionChangeX = 0
	
	SWITCH iMoveInputThisFrame
		
		CASE ciSCGW_INPUT_MOVE_UP
			bValidMovement = iCurrentPlayerY > 0 AND iCurrentPlayerX % 2 = 0
			iPositionChangeY = -1
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_DOWN
			bValidMovement = iCurrentPlayerY < ciSCGW_MAX_PLAYER_POSITIONS_Y - 1 AND iCurrentPlayerX % 2 = 0
			iPositionChangeY = 1
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_LEFT
			bValidMovement = iCurrentPlayerX > 0 AND iCurrentPlayerY % 2 = 0
			iPositionChangeX = -1
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_RIGHT
			bValidMovement = iCurrentPlayerX < ciSCGW_MAX_PLAYER_POSITIONS_X - 1 AND iCurrentPlayerY % 2 = 0
			iPositionChangeX = 1
		BREAK
		
	ENDSWITCH
	
	IF bValidMovement
		
		IF (SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(sSCGWData.sLocalPlayer.iLocalPart) < NATIVE_TO_INT(GET_NETWORK_TIME())  
		AND IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED))
		OR bDirectionChange
		
			IF bDirectionChange
			OR (NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_FIRST_MOVE_DONE))
				SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_DIRECTION_CHANGED)
			ENDIF
		
			IF iPreviousPlayerX != iCurrentPlayerX
				iPreviousPlayerX = iCurrentPlayerX
			ENDIF
			
			IF iPreviousPlayerY != iCurrentPlayerY
				iPreviousPlayerY = iCurrentPlayerY
			ENDIF
			
			IF (iPositionChangeX != 0)
				iCurrentPlayerX += iPositionChangeX
			ENDIF
			
			IF (iPositionChangeY != 0)
				iCurrentPlayerY += iPositionChangeY
			ENDIF
			
			SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(iCurrentPlayerX, iCurrentPlayerY)
			SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(iPreviousPlayerX, iPreviousPlayerY)
			
			IF sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves > 0
				SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].fMoveSpeed = cfSCGW_BOOST_SPEED
				sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves--
			ELIF NOT IS_FLOAT_IN_RANGE(SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].fMoveSpeed, cfSCGW_BASE_SPEED - cfARCADE_MINIGAME_FLOAT_TOLERANCE, cfSCGW_BASE_SPEED + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].fMoveSpeed = cfSCGW_BASE_SPEED
			ENDIF
			
			SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			
			IF iPositionChangeX > 0
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_RIGHT
			ELIF iPositionChangeX < 0
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_LEFT
			ELIF iPositionChangeY < 0
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_UP
			ELIF iPositionChangeY > 0
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
			ENDIF
			
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_INPUT - MOVING - iPositionChangeX = ", iPositionChangeX, " iPositionChangeY = ", iPositionChangeY, " iPlayerPosition = (", SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition,")" )
			CLEAR_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)
			CLEAR_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
			SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastMovementDirection = iMoveInputThisFrame
			
			IF NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_FIRST_MOVE_DONE)
				SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_FIRST_MOVE_DONE)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_MOVEMENT_INPUT_BOT(INT iBot)

	INT iMoveInputThisFrame = SCGW_GET_MOVE_INPUT_BOT(iBot)
	BOOL bDirectionChange = FALSE
	
	IF NOT (SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(sSCGWData.sLocalPlayer.iLocalPart) < NATIVE_TO_INT(GET_NETWORK_TIME())  
	AND IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED))
	AND NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime + ciSCGW_PLAYER_CHANGE_DIRECTION_TIME
	AND SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementDirection != iMoveInputThisFrame
	AND NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_DIRECTION_CHANGED)	
		bDirectionChange = TRUE
	ENDIF
	
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	IF bDirectionChange
		SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
		SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	ELSE
		SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
		SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	ENDIF
	
	BOOL bValidMovement = FALSE
	INT iPositionChangeY = 0
	INT iPositionChangeX = 0
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_INPUT_BOT - iBot ",iBot," iMoveInputThisFrame ",iMoveInputThisFrame)
	SWITCH iMoveInputThisFrame
		
		CASE ciSCGW_INPUT_MOVE_UP
			bValidMovement = iCurrentPlayerY > 0 AND iCurrentPlayerX % 2 = 0
			iPositionChangeY = -1
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_DOWN
			bValidMovement = iCurrentPlayerY < ciSCGW_MAX_PLAYER_POSITIONS_Y - 1 AND iCurrentPlayerX % 2 = 0
			iPositionChangeY = 1
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_LEFT
			bValidMovement = iCurrentPlayerX > 0 AND iCurrentPlayerY % 2 = 0
			iPositionChangeX = -1
		BREAK
		
		CASE ciSCGW_INPUT_MOVE_RIGHT
			bValidMovement = iCurrentPlayerX < ciSCGW_MAX_PLAYER_POSITIONS_X - 1 AND iCurrentPlayerY % 2 = 0
			iPositionChangeX = 1
		BREAK
		
	ENDSWITCH
	
	IF bValidMovement
		
		IF (SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(iBot) < NATIVE_TO_INT(GET_NETWORK_TIME())  
		AND IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED))
		OR bDirectionChange
		
			IF bDirectionChange
			OR (NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_FIRST_MOVE_DONE))
				SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_DIRECTION_CHANGED)
			ENDIF
		
			IF iPreviousPlayerX != iCurrentPlayerX
				iPreviousPlayerX = iCurrentPlayerX
			ENDIF
			
			IF iPreviousPlayerY != iCurrentPlayerY
				iPreviousPlayerY = iCurrentPlayerY
			ENDIF
			
			IF (iPositionChangeX != 0)
				iCurrentPlayerX += iPositionChangeX
			ENDIF
			
			IF (iPositionChangeY != 0)
				iCurrentPlayerY += iPositionChangeY
			ENDIF
			
			SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(iCurrentPlayerX, iCurrentPlayerY)
			SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(iPreviousPlayerX, iPreviousPlayerY)
			
			IF sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves > 0
				SCGW_Bot[iBot].SCGW_player.fMoveSpeed = cfSCGW_BOOST_SPEED
				sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves--
			ELIF NOT IS_FLOAT_IN_RANGE(SCGW_Bot[iBot].SCGW_player.fMoveSpeed, cfSCGW_BASE_SPEED - cfARCADE_MINIGAME_FLOAT_TOLERANCE, cfSCGW_BASE_SPEED + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
				SCGW_Bot[iBot].SCGW_player.fMoveSpeed = cfSCGW_BASE_SPEED
			ENDIF
			
			SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			
			IF iPositionChangeX > 0
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_RIGHT
			ELIF iPositionChangeX < 0
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_LEFT
			ELIF iPositionChangeY < 0
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_UP
			ELIF iPositionChangeY > 0
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
			ENDIF
			
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_INPUT_BOT - MOVING - iBot ",iBot," iPositionChangeX = ", iPositionChangeX, " iPositionChangeY = ", iPositionChangeY, " iPlayerPosition = (", SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,")" )
			CLEAR_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)
			CLEAR_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
			SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementDirection = iMoveInputThisFrame
			
			IF NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_FIRST_MOVE_DONE)
				SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_FIRST_MOVE_DONE)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Processes changing the player's direction based on input
PROC SCGW_PROCESS_TURN_INPUT()
	
	INT iTurnInputThisFrame = SCGW_GET_TURN_INPUT()
	
	BOOL bValidTurn = SCGW_CAN_PLAYER_TURN_THIS_DIRECTION(sSCGWData.sLocalPlayer.iLocalPart,iTurnInputThisFrame)
	
	IF bValidTurn
		SWITCH iTurnInputThisFrame
			CASE ciSCGW_INPUT_TURN_UP
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_UP
			BREAK
			
			CASE ciSCGW_INPUT_TURN_DOWN
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
			BREAK
			
			CASE ciSCGW_INPUT_TURN_LEFT
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_LEFT
			BREAK
			
			CASE ciSCGW_INPUT_TURN_RIGHT
				SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_RIGHT
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_TURN_INPUT_BOT(INT iBot)
	
	INT iTurnInputThisFrame = SCGW_GET_TURN_INPUT_BOT(iBot)
	
	BOOL bValidTurn = SCGW_CAN_PLAYER_TURN_THIS_DIRECTION_BOT(iBot, iTurnInputThisFrame)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_TURN_INPUT_BOT - iTurnInputThisFrame ", iTurnInputThisFrame)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_TURN_INPUT_BOT - bValidTurn ", bValidTurn)
	IF bValidTurn
		SWITCH iTurnInputThisFrame
			CASE ciSCGW_INPUT_TURN_UP
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_UP
			BREAK
			
			CASE ciSCGW_INPUT_TURN_DOWN
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
			BREAK
			
			CASE ciSCGW_INPUT_TURN_LEFT
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_LEFT
			BREAK
			
			CASE ciSCGW_INPUT_TURN_RIGHT
				SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_RIGHT
			BREAK
			
		ENDSWITCH
	ENDIF
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_TURN_INPUT_BOT - SCGW_Bot[",iBot,"].SCGW_UF_Player.iFacingDirection ", SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection)

ENDPROC

/// PURPOSE:
///    Processes the player shooting
PROC SCGW_PROCESS_SHOOT_INPUT()
	
	IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_NONE
		EXIT
	ENDIF
	
	INT iShootInputThisFrame = SCGW_GET_SHOOT_INPUT()
	INT iShotDirection
	BOOL bValidShot = FALSE
	
	IF iShootInputThisFrame != ciSCGW_INPUT_NONE
		bValidShot = SCGW_CAN_PLAYER_SHOOT_THIS_DIRECTION(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection)
	ENDIF
	
	IF bValidShot
		
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) < sSCGWData.sLocalPlayer.iLastShotTime + sSCGWData.sWeaponData[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType].iShotInterval
			EXIT
		ENDIF
		
		iShotDirection = SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection
		

		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[sSCGWData.iNextShotIndex].iShotTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[sSCGWData.iNextShotIndex].iShotDirection = iShotDirection
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[sSCGWData.iNextShotIndex].iShotWeaponType = SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[sSCGWData.iNextShotIndex].vShotOrigin = sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].vActualPlayerPosition
		
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = iShotDirection
		
		sSCGWData.sLocalPlayer.iLastShotTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_SHOOT_INPUT - SHOOTING - iNextShotIndex = ", sSCGWData.iNextShotIndex, " iShotDirection = ", iShotDirection, " iShotOrigin = (", SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[sSCGWData.iNextShotIndex].vShotOrigin.x,", ", SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[sSCGWData.iNextShotIndex].vShotOrigin.y,")" )
		
		sSCGWData.iNextShotIndex ++
		IF sSCGWData.iNextShotIndex >= ciSCGW_MAX_PLAYER_SHOTS
			sSCGWData.iNextShotIndex = 0
		ENDIF
		
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_SHOOT_DURATION, ciSCGW_VIB_SHOOT_FREQ)
		
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_SHOOT_INPUT_BOT(INT iBot)
	
	IF SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_NONE
		EXIT
	ENDIF
	
	INT iShootInputThisFrame = SCGW_GET_SHOOT_INPUT_BOT(iBot)
	INT iShotDirection
	BOOL bValidShot = FALSE
	
	IF iShootInputThisFrame != ciSCGW_INPUT_NONE
		bValidShot = SCGW_CAN_PLAYER_SHOOT_THIS_DIRECTION_BOT(iBot, SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection)
	ENDIF
	
	IF bValidShot
		
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) < sSCGWData.sBotPlayer[iBot].iLastShotTime + sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType].iShotInterval
			EXIT
		ENDIF
		
		iShotDirection = SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection
		

		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[sSCGWData.iNextShotIndex].iShotTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[sSCGWData.iNextShotIndex].iShotDirection = iShotDirection
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[sSCGWData.iNextShotIndex].iShotWeaponType = SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[sSCGWData.iNextShotIndex].vShotOrigin = sSCGWData.sPlayers[iBot].vActualPlayerPosition
		
		SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = iShotDirection
		
		sSCGWData.sBotPlayer[iBot].iLastShotTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_SHOOT_INPUT_BOT - SHOOTING - iNextShotIndex = ", sSCGWData.iNextShotIndex, " iShotDirection = ", iShotDirection, " iShotOrigin = (", SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[sSCGWData.iNextShotIndex].vShotOrigin.x,", ", SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[sSCGWData.iNextShotIndex].vShotOrigin.y,")" )
		
		sSCGWData.iNextShotIndex ++
		IF sSCGWData.iNextShotIndex >= ciSCGW_MAX_PLAYER_SHOTS
			sSCGWData.iNextShotIndex = 0
		ENDIF
		
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_SHOOT_DURATION, ciSCGW_VIB_SHOOT_FREQ)
		
	ENDIF
	
ENDPROC

FUNC BOOL SCGW_PROCESS_BOT_AI_SHOULD_SHOOT(INT iBot)
	INT iPosX
	INT iPosY
	INT iPlayerPosX
	INT iPlayerPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	INT iPlayerLoop
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF iPlayerLoop != iBot AND (NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop)) OR SCGW_Bot[iPlayerLoop].bActive) AND (SCGW_PlayerBD[iPlayerLoop].iHealth > 0 OR SCGW_Bot[iPlayerLoop].SCGW_player.iHealth > 0)
			SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[iPlayerLoop].iPlayerPosition,iPlayerPosX,iPlayerPosY)
			IF iPlayerPosX = iPosX
				RETURN 		iPlayerPosY <= iPosY AND SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_UP
						OR	iPlayerPosY >= iPosY AND SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
			ELIF iPlayerPosY = iPosY
				RETURN 		iPlayerPosX <= iPosX AND SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_LEFT
						OR	iPlayerPosX >= iPosX AND SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_RIGHT
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC INT SCGW_PROCESS_BOT_AI_SHOULD_FACE_DIRECTION(INT iBot)
	INT iPosX
	INT iPosY
	INT iPlayerPosX
	INT iPlayerPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	INT iPlayerLoop
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF iPlayerLoop != iBot AND (NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop)) OR SCGW_Bot[iPlayerLoop].bActive)
			SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[iPlayerLoop].iPlayerPosition,iPlayerPosX,iPlayerPosY)
			IF iPlayerPosX = iPosX
				IF iPlayerPosY <= iPosY 
					RETURN ciSCGW_INPUT_TURN_UP
				ELIF iPlayerPosY >= iPosY 
					RETURN ciSCGW_INPUT_TURN_DOWN
				ENDIF
			ELIF iPlayerPosY = iPosY
				IF iPlayerPosX <= iPosX
					RETURN ciSCGW_INPUT_TURN_LEFT
				ELIF iPlayerPosX >= iPosX 
					RETURN ciSCGW_INPUT_TURN_RIGHT
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT SCGW_PROCESS_BOT_AI_GET_OWNER_GANG(INT iX, INT iY)
	IF  (iX >=0 AND iX <= ciSCGW_MAX_GRID_POSITIONS_X - 1)
	AND (iY >=0 AND iY <= ciSCGW_MAX_GRID_POSITIONS_Y - 1)
		RETURN sSCGWData.iPreviousGangOwnership[iX][iY]
	ENDIF
	RETURN -2
ENDFUNC
PROC SCGW_PROCESS_BOT_AI_GET_CLOSEST_BUILDINGS(INT iBot,INT &i00, INT &i01, INT &i10, INT &i11)
	INT iPosX
	INT iPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	i00 = SCGW_PROCESS_BOT_AI_GET_OWNER_GANG(iPosX/2-1, iPosY/2-1)
	i01 = SCGW_PROCESS_BOT_AI_GET_OWNER_GANG(iPosX/2-1, iPosY/2)
	i10 = SCGW_PROCESS_BOT_AI_GET_OWNER_GANG(iPosX/2, iPosY/2-1)
	i11 = SCGW_PROCESS_BOT_AI_GET_OWNER_GANG(iPosX/2, iPosY/2)
ENDPROC

PROC SCGW_PROCESS_BOT_AI_FIND_BEST_MOVE(INT iBot)
	INT iPosX
	INT iPosY
//	INT iPosTargetX
//	INT iPosTargetY
	INT i00
	INT i01
	INT i10
	INT i11
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	SCGW_PROCESS_BOT_AI_GET_CLOSEST_BUILDINGS(iBot,i00,i01,i10,i11)
	IF i00 != SCGW_Bot[iBot].SCGW_player.iGang AND i00 != -2
	ELIF i01 != SCGW_Bot[iBot].SCGW_player.iGang AND i01 != -2
	ELIF i10 != SCGW_Bot[iBot].SCGW_player.iGang AND i10 != -2
	ELIF i11 != SCGW_Bot[iBot].SCGW_player.iGang AND i11 != -2
	ELSE
	ENDIF
	
//	iGangToDraw = sSCGWData.iPreviousGangOwnership[iGridX][iGridY]
	
//	INT iPos = SCGW_GET_POSITION_FROM_COORDS(iPosTargetX, iPosTargetY)
//	iGangToDraw = sSCGWData.iPreviousGangOwnership[iGridX][iGridY]
ENDPROC

FUNC BOOL SCGW_PROCESS_BOT_AI_SHOULD_AVOID_VEHICLE(INT iBot)
	INT iPosX
	INT iPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	INT iPosTargetX
	INT iPosTargetY
	INT iVehicleLoop
	FOR iVehicleLoop = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
		IF SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleType = -1 
			RELOOP
		ENDIF
		SCGW_GET_POSITION_COORDS(SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleOrigin, iPosTargetX, iPosTargetY)
		IF iPosTargetX = iPosX
		OR iPosTargetY = iPosY
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_SHOULD_AVOID_VEHICLE - iBot ",iBot," iPosX ",iPosX," iPosY ",iPosY )
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_SHOULD_AVOID_VEHICLE - iVehicleLoop ",iVehicleLoop," iPosX ",iPosTargetX," iPosY ",iPosTargetY )
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_SHOULD_AVOID_VEHICLE - RETURN TRUE")
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC SCGW_PROCESS_BOT_AI_AVOID_VEHICLE(INT iBot)
	INT iPosX
	INT iPosY
	INT iTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosX ",iPosX," iPosY ",iPosY )
	BOOL bShouldMoveFromX
	BOOL bShouldMoveFromY
	BOOL bShouldMoveDown
	BOOL bShouldMoveRight
	INT iVehiclePosX
	INT iVehiclePosY
	INT iVehicleLoop
	FOR iVehicleLoop = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
		INT iVehicleMoveTime = PICK_INT(SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleDirection = ciSCGW_GRID_DIRECTION_DOWN OR SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleDirection = ciSCGW_GRID_DIRECTION_UP, ciSCGW_MAX_PLAYER_POSITIONS_Y + 2, ciSCGW_MAX_PLAYER_POSITIONS_X + 2) * ciSCGW_VEHICLE_GRID_SPEED
		IF SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleType = -1
		OR (iTime < SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleTime AND iTime > SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleTime + iVehicleMoveTime)
			RELOOP
		ENDIF
		SCGW_GET_POSITION_COORDS(SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleOrigin, iVehiclePosX, iVehiclePosY)
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iVehicleLoop ",iVehicleLoop," iVehiclePosX ",iVehiclePosX," iVehiclePosY ",iVehiclePosY )
		
		bShouldMoveFromX = bShouldMoveFromX OR (iVehiclePosX = iPosX)
		bShouldMoveFromY = bShouldMoveFromY OR (iVehiclePosY = iPosY)
		bShouldMoveDown  = bShouldMoveDown  OR (SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleDirection = ciSCGW_GRID_DIRECTION_DOWN AND iVehiclePosY = iPosY)
		bShouldMoveRight = bShouldMoveRight OR (SCGW_ServerBD.sVehicles[iVehicleLoop].iVehicleDirection = ciSCGW_GRID_DIRECTION_LEFT AND iVehiclePosX = iPosX)
	ENDFOR
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveFromX ",bShouldMoveFromX)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveFromY ",bShouldMoveFromY)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveDown  ",bShouldMoveDown)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveRight ",bShouldMoveRight)
	IF bShouldMoveFromX
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveFromX")
		IF bShouldMoveDown
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveDown")
			IF iPosY+2 > ciSCGW_MAX_PLAYER_POSITIONS_Y-1
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosY+2 > ciSCGW_MAX_PLAYER_POSITIONS_Y-1")
				SCGW_Bot[iBot].iTargetPosY = iPosY-2
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosY+2 <= ciSCGW_MAX_PLAYER_POSITIONS_Y-1")
				SCGW_Bot[iBot].iTargetPosY = iPosY+2
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," NOT bShouldMoveDown")
			IF iPosY-2 < 0
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosY-2 < 0")
				SCGW_Bot[iBot].iTargetPosY = iPosY+2
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosY-2 >= 0")
				SCGW_Bot[iBot].iTargetPosY = iPosY-2
			ENDIF
		ENDIF
	ENDIF
	IF bShouldMoveFromY
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveFromY")
		IF bShouldMoveRight
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," bShouldMoveRight")
			IF iPosX+2 > ciSCGW_MAX_PLAYER_POSITIONS_X-1
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosX+2 > ciSCGW_MAX_PLAYER_POSITIONS_X-1")
				SCGW_Bot[iBot].iTargetPosX = iPosX-2
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosX+2 <= ciSCGW_MAX_PLAYER_POSITIONS_X-1")
				SCGW_Bot[iBot].iTargetPosX = iPosX+2
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," NOT bShouldMoveRight")
			IF iPosX-2 < 0
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosX-2 < 0")
				SCGW_Bot[iBot].iTargetPosX = iPosX+2
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iPosX-2 >= 0")
				SCGW_Bot[iBot].iTargetPosX = iPosX-2
			ENDIF
		ENDIF
	ENDIF
	IF NOT bShouldMoveFromX AND NOT bShouldMoveFromY
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," NOT bShouldMoveFromX AND NOT bShouldMoveFromY ")
		SCGW_Bot[iBot].iTargetPosX = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYER_POSITIONS_X/2)*2
		SCGW_Bot[iBot].iTargetPosY = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYER_POSITIONS_Y/2)*2
	ENDIF

	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_BOT_AI_AVOID_VEHICLE - iBot ",iBot," iTargetPosX ",SCGW_Bot[iBot].iTargetPosX," iTargetPosY ",SCGW_Bot[iBot].iTargetPosY )
ENDPROC

PROC SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING_FIND_BUILDING(INT iBot)
	INT iPosX
	INT iPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	INT iTargetPosX
	INT iTargetPosY
	INT iRandom
	INT iIterator
	INT iBuildingToCaptureX
	INT iBuildingToCaptureY
	SWITCH GET_RANDOM_INT_IN_RANGE(0,6)
		CASE 5
			iTargetPosX = -1
			iTargetPosY = -1
			iRandom = GET_RANDOM_INT_IN_RANGE(0,4)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iRandom ",iRandom," iPosX ",iPosX," iPosY ",iPosY )
			FOR iIterator = 0 TO 3
				IF iPosX % 2 = 1 AND iPosY % 2 = 0
					iTargetPosX = iPosX
					IF (iIterator+iRandom)%2 = 0
						iTargetPosY = iPosY-1
					ELSE
						iTargetPosY = iPosY+1
					ENDIF
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - A iIterator ",iIterator," (iIterator+iRandom)%2 = ",(iIterator+iRandom)%2," iTargetPosX ",iTargetPosX," iTargetPosY ",iTargetPosY)
				ELIF iPosY % 2 = 1 AND iPosX % 2 = 0
					iTargetPosY = iPosY
					IF (iIterator+iRandom)%2 = 0
						iTargetPosX = iPosX-1
					ELSE
						iTargetPosX = iPosX+1
					ENDIF
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - B iIterator ",iIterator," (iIterator+iRandom)%2 = ",(iIterator+iRandom)%2," iTargetPosX ",iTargetPosX," iTargetPosY ",iTargetPosY)
				ELSE
					IF (iIterator+iRandom)%4 = 0
						iTargetPosX = iPosX-1
						iTargetPosY = iPosY-1
					ELIF (iIterator+iRandom)%4 = 1
						iTargetPosX = iPosX+1
						iTargetPosY = iPosY-1
					ELIF (iIterator+iRandom)%4 = 2
						iTargetPosX = iPosX-1
						iTargetPosY = iPosY+1
					ELSE
						iTargetPosX = iPosX+1
						iTargetPosY = iPosY+1
					ENDIF
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - C iIterator ",iIterator," (iIterator+iRandom)%4 = ",(iIterator+iRandom)%4," iTargetPosX ",iTargetPosX," iTargetPosY ",iTargetPosY)
				ENDIF
				IF iTargetPosX != -1 AND iTargetPosY != -1
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iGang ",SCGW_Bot[iBot].SCGW_player.iGang," iTargetPosX/2 ",iTargetPosX/2," iTargetPosY/2 ",iTargetPosY/2 )
					IF SCGW_ServerBD.iGridGangOwnership[(iTargetPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iTargetPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y] != SCGW_Bot[iBot].SCGW_player.iGang
						SCGW_Bot[iBot].iBuildingToCaptureX = (iTargetPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X
						SCGW_Bot[iBot].iBuildingToCaptureY = (iTargetPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y
						EXIT
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE 0
			FOR iBuildingToCaptureY = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y-1
				FOR iBuildingToCaptureX = 0 TO ciSCGW_MAX_GRID_POSITIONS_X-1
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iBuildingToCaptureX ",iBuildingToCaptureX," iBuildingToCaptureY ",iBuildingToCaptureY )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iPosX/2 ",iPosX/2," iPosY/2 ",iPosY/2 )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iGridGangOwnership ",SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y]," iGang ",SCGW_Bot[iBot].SCGW_player.iGang )
					IF SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y] != SCGW_Bot[iBot].SCGW_player.iGang
						SCGW_Bot[iBot].iBuildingToCaptureX = (iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X
						SCGW_Bot[iBot].iBuildingToCaptureY = (iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y
						EXIT
					ENDIF
				ENDFOR
			ENDFOR
		BREAK
		CASE 1
			FOR iBuildingToCaptureX = 0 TO ciSCGW_MAX_GRID_POSITIONS_X-1
				FOR iBuildingToCaptureY = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y-1
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iBuildingToCaptureX ",iBuildingToCaptureX," iBuildingToCaptureY ",iBuildingToCaptureY )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iPosX/2 ",iPosX/2," iPosY/2 ",iPosY/2 )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iGridGangOwnership ",SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y]," iGang ",SCGW_Bot[iBot].SCGW_player.iGang )
					IF SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y] != SCGW_Bot[iBot].SCGW_player.iGang
						SCGW_Bot[iBot].iBuildingToCaptureX = (iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X
						SCGW_Bot[iBot].iBuildingToCaptureY = (iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y
						EXIT
					ENDIF
				ENDFOR
			ENDFOR
		BREAK
		CASE 2
			FOR iBuildingToCaptureX = ciSCGW_MAX_GRID_POSITIONS_X-1 TO 0
				FOR iBuildingToCaptureY = ciSCGW_MAX_GRID_POSITIONS_Y-1 TO 0
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iBuildingToCaptureX ",iBuildingToCaptureX," iBuildingToCaptureY ",iBuildingToCaptureY )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iPosX/2 ",iPosX/2," iPosY/2 ",iPosY/2 )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iGridGangOwnership ",SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y]," iGang ",SCGW_Bot[iBot].SCGW_player.iGang )
					IF SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y] != SCGW_Bot[iBot].SCGW_player.iGang
						SCGW_Bot[iBot].iBuildingToCaptureX = (iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X
						SCGW_Bot[iBot].iBuildingToCaptureY = (iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y
						EXIT
					ENDIF
				ENDFOR
			ENDFOR
		BREAK
		CASE 3
			FOR iBuildingToCaptureY = ciSCGW_MAX_GRID_POSITIONS_Y-1 TO 0
				FOR iBuildingToCaptureX = ciSCGW_MAX_GRID_POSITIONS_X-1 TO 0
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iBuildingToCaptureX ",iBuildingToCaptureX," iBuildingToCaptureY ",iBuildingToCaptureY )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iPosX/2 ",iPosX/2," iPosY/2 ",iPosY/2 )
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iGridGangOwnership ",SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y]," iGang ",SCGW_Bot[iBot].SCGW_player.iGang )
					IF SCGW_ServerBD.iGridGangOwnership[(iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X][(iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y] != SCGW_Bot[iBot].SCGW_player.iGang
						SCGW_Bot[iBot].iBuildingToCaptureX = (iBuildingToCaptureX+iPosX/2)%ciSCGW_MAX_GRID_POSITIONS_X
						SCGW_Bot[iBot].iBuildingToCaptureY = (iBuildingToCaptureY+iPosY/2)%ciSCGW_MAX_GRID_POSITIONS_Y
						EXIT
					ENDIF
				ENDFOR
			ENDFOR
		BREAK
		CASE 4
			SCGW_Bot[iBot].iBuildingToCaptureX = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_GRID_POSITIONS_X)
			SCGW_Bot[iBot].iBuildingToCaptureY = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_GRID_POSITIONS_y)
		BREAK
	ENDSWITCH
ENDPROC

PROC SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING_FIND_BUILDING_FIND_CORNER(INT iBot)
	SCGW_Bot[iBot].iBuildingToCaptureCorner++
	SCGW_Bot[iBot].iBuildingToCaptureCorner = SCGW_Bot[iBot].iBuildingToCaptureCorner%6
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBuildingToCaptureCorner ",SCGW_Bot[iBot].iBuildingToCaptureCorner )
	IF SCGW_Bot[iBot].iBuildingToCaptureCorner = 0
		SCGW_Bot[iBot].iBuildingToCaptureCornerRandom = GET_RANDOM_INT_IN_RANGE(0,4)
	ELIF SCGW_Bot[iBot].iBuildingToCaptureCorner = 5
		SCGW_Bot[iBot].iBuildingToCaptureX = -1
		SCGW_Bot[iBot].iBuildingToCaptureY = -1
		SCGW_Bot[iBot].iTargetPosX = SCGW_Bot[iBot].iBuildingToCaptureX
		SCGW_Bot[iBot].iTargetPosY = SCGW_Bot[iBot].iBuildingToCaptureY
		EXIT
	ENDIF
	SWITCH ((SCGW_Bot[iBot].iBuildingToCaptureCorner+SCGW_Bot[iBot].iBuildingToCaptureCornerRandom)%6)
		CASE 0
		CASE 4
			SCGW_Bot[iBot].iTargetPosX = SCGW_Bot[iBot].iBuildingToCaptureX*2+1-1
			SCGW_Bot[iBot].iTargetPosY = SCGW_Bot[iBot].iBuildingToCaptureY*2+1-1
		BREAK
		CASE 1
			SCGW_Bot[iBot].iTargetPosX = SCGW_Bot[iBot].iBuildingToCaptureX*2+1+1
			SCGW_Bot[iBot].iTargetPosY = SCGW_Bot[iBot].iBuildingToCaptureY*2+1-1
		BREAK
		CASE 2
			SCGW_Bot[iBot].iTargetPosX = SCGW_Bot[iBot].iBuildingToCaptureX*2+1+1
			SCGW_Bot[iBot].iTargetPosY = SCGW_Bot[iBot].iBuildingToCaptureY*2+1+1
		BREAK
		CASE 3
			SCGW_Bot[iBot].iTargetPosX = SCGW_Bot[iBot].iBuildingToCaptureX*2+1-1
			SCGW_Bot[iBot].iTargetPosY = SCGW_Bot[iBot].iBuildingToCaptureY*2+1+1
		BREAK
		CASE 5
		BREAK
	ENDSWITCH
ENDPROC

PROC SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING(INT iBot)
	IF SCGW_Bot[iBot].iBuildingToCaptureX = -1 AND SCGW_Bot[iBot].iBuildingToCaptureY = -1
		SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING_FIND_BUILDING(iBot)
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," iBuildingToCaptureX ",SCGW_Bot[iBot].iBuildingToCaptureX," iBuildingToCaptureY ",SCGW_Bot[iBot].iBuildingToCaptureY )
	ENDIF
	INT iPosX
	INT iPosY
	TEXT_LABEL_63 tlLine = ""
	FOR iPosY = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y-1
		tlLine = ""
		FOR iPosX = 0 TO ciSCGW_MAX_GRID_POSITIONS_X-1
			tlLine += SCGW_ServerBD.iGridGangOwnership[iPosX][iPosY]
			tlLine += " "
		ENDFOR
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - ",tlLine )
	ENDFOR
	
	IF SCGW_Bot[iBot].iBuildingToCaptureX != -1 AND SCGW_Bot[iBot].iBuildingToCaptureY != -1
	AND SCGW_ServerBD.iGridGangOwnership[SCGW_Bot[iBot].iBuildingToCaptureX%ciSCGW_MAX_GRID_POSITIONS_X][SCGW_Bot[iBot].iBuildingToCaptureY%ciSCGW_MAX_GRID_POSITIONS_Y] = SCGW_Bot[iBot].SCGW_player.iGang
		SCGW_Bot[iBot].iBuildingToCaptureCorner = 4
	ENDIF
	
	IF SCGW_Bot[iBot].iBuildingToCaptureX != -1 AND SCGW_Bot[iBot].iBuildingToCaptureY != -1
		SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING_FIND_BUILDING_FIND_CORNER(iBot)
	ENDIF	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING - iBot ",iBot," SCGW_Bot[iBot].iTargetPosX ",SCGW_Bot[iBot].iTargetPosX," SCGW_Bot[iBot].iTargetPosY ",SCGW_Bot[iBot].iTargetPosY )
ENDPROC

PROC SCGW_PROCESS_BOT_AI_COLLECT_PICKUP(INT iBot)
	INT iPosX
	INT iPosY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition,iPosX,iPosY)
	INT iPickupX = -1
	INT iPickupY = -1
	INT iFinalPickupX = -1
	INT iFinalPickupY = -1
	INT iPickup = 0
	FOR iPickup = 0 TO ciSCGW_MAX_SPAWNED_PICKUPS-1
		IF SCGW_ServerBD.sPickups[iPickup].iSpawnedPickupType != -1 AND SCGW_ServerBD.sPickups[iPickup].iSpawnedPickupLocation != -1 AND SCGW_ServerBD.sPickups[iPickup].iSpawnedPickupSpawnTime != -HIGHEST_INT
			IF SCGW_ServerBD.sPickups[iPickup].iSpawnedPickupType = ciSCGW_PICKUPS_HEALTH
				SCGW_GET_POSITION_COORDS(SCGW_ServerBD.sPickups[iPickup].iSpawnedPickupLocation,iPickupX,iPickupY)
				IF iFinalPickupX = -1 AND iFinalPickupY = -1
				OR ABSI(iPickupX-iPosX)+ABSI(iPickupY-iPosY) < ABSI(iFinalPickupX-iPosX)+ABSI(iFinalPickupY-iPosY)
					iFinalPickupX = iPickupX
					iFinalPickupY = iPickupY
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	IF iFinalPickupX = -1
		SCGW_Bot[iBot].iTargetPosX = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYER_POSITIONS_X/2)*2
	ENDIF
	IF iFinalPickupY = -1
		SCGW_Bot[iBot].iTargetPosY = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYER_POSITIONS_Y/2)*2
	ENDIF
ENDPROC

PROC SCGW_PROCESS_BOT_AI_SUDDEN_DEATH(INT iBot)
	IF SCGW_Bot[iBot].iSuddenDeathTarget = -1
		SCGW_Bot[iBot].iSuddenDeathTarget = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYERS)
	ENDIF
	
	IF SCGW_Bot[iBot].iSuddenDeathTarget = iBot
		SCGW_Bot[iBot].iSuddenDeathTarget = -1
		EXIT
	ENDIF

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(SCGW_Bot[iBot].iSuddenDeathTarget))
	AND NOT SCGW_Bot[SCGW_Bot[iBot].iSuddenDeathTarget].bActive
		SCGW_Bot[iBot].iSuddenDeathTarget = -1
		EXIT
	ENDIF
	
	IF SCGW_PlayerBD[SCGW_Bot[iBot].iSuddenDeathTarget].iHealth = 0
		SCGW_Bot[iBot].iSuddenDeathTarget = -1
		EXIT
	ENDIF

	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[SCGW_Bot[iBot].iSuddenDeathTarget].iPlayerPosition,SCGW_Bot[iBot].iTargetPosX,SCGW_Bot[iBot].iTargetPosY)
ENDPROC

FUNC BOOL SCGW_PROCESS_BOT_AI_SHOULD_GO_SUDDEN_DEATH(INT iBot)
	UNUSED_PARAMETER(iBot)
	RETURN SCGW_ServerBD.bSuddenDeath
ENDFUNC

FUNC BOOL SCGW_PROCESS_BOT_AI_SHOULD_COLLECT_PICKUP(INT iBot)
	RETURN SCGW_Bot[iBot].SCGW_player.iHealth <= ciSCGW_MAX_PLAYER_HEALTH / 2
ENDFUNC

PROC SCGW_PROCESS_BOT_AI(INT iBot)
	SCGW_Bot[iBot].bShouldShoot = SCGW_PROCESS_BOT_AI_SHOULD_SHOOT(iBot)
	SCGW_Bot[iBot].iFaceDirection = SCGW_PROCESS_BOT_AI_SHOULD_FACE_DIRECTION(iBot)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_SHOOT_INPUT_BOT - iBot ",iBot," SCGW_Bot[iBot].eStateAI ",SCGW_Bot[iBot].eStateAI," SCGW_Bot[iBot].bTaskComplete ",SCGW_Bot[iBot].bTaskComplete )

	IF SCGW_Bot[iBot].bTaskComplete
		IF SCGW_Bot[iBot].eStateAI != SCGW_BOT_STATE_AI_SUDDEN_DEATH AND SCGW_PROCESS_BOT_AI_SHOULD_GO_SUDDEN_DEATH(iBot)
			SCGW_Bot[iBot].eStateAI = SCGW_BOT_STATE_AI_SUDDEN_DEATH
//			SCGW_Bot[iBot].bTaskComplete = TRUE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_SHOOT_INPUT_BOT - iBot ",iBot," SCGW_PROCESS_BOT_AI_SHOULD_GO_SUDDEN_DEATH")
		ELIF SCGW_Bot[iBot].eStateAI != SCGW_BOT_STATE_AI_AVOID_VEHICLE AND SCGW_PROCESS_BOT_AI_SHOULD_AVOID_VEHICLE(iBot)
			SCGW_Bot[iBot].eStateAI = SCGW_BOT_STATE_AI_AVOID_VEHICLE
	//		SCGW_Bot[iBot].bTaskComplete = TRUE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_SHOOT_INPUT_BOT - iBot ",iBot," SCGW_PROCESS_BOT_AI_SHOULD_AVOID_VEHICLE")
		ELIF SCGW_Bot[iBot].eStateAI != SCGW_BOT_STATE_AI_COLLECT_PICKUP AND SCGW_PROCESS_BOT_AI_SHOULD_COLLECT_PICKUP(iBot)
			SCGW_Bot[iBot].eStateAI = SCGW_BOT_STATE_AI_COLLECT_PICKUP
	//		SCGW_Bot[iBot].bTaskComplete = TRUE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_SHOOT_INPUT_BOT - iBot ",iBot," SCGW_PROCESS_BOT_AI_SHOULD_COLLECT_PICKUP")
		ELIF SCGW_Bot[iBot].eStateAI != SCGW_BOT_STATE_AI_CAPTURE_BUILDING AND SCGW_Bot[iBot].bTaskComplete
			SCGW_Bot[iBot].eStateAI = SCGW_BOT_STATE_AI_CAPTURE_BUILDING
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [AC] SCGW_PROCESS_SHOOT_INPUT_BOT - iBot ",iBot," SCGW_BOT_STATE_AI_CAPTURE_BUILDING")
		ENDIF

		SWITCH SCGW_Bot[iBot].eStateAI
			CASE SCGW_BOT_STATE_AI_CAPTURE_BUILDING
				SCGW_PROCESS_BOT_AI_CAPTURE_BUILDING(iBot)
			BREAK
			CASE SCGW_BOT_STATE_AI_AVOID_VEHICLE
				SCGW_PROCESS_BOT_AI_AVOID_VEHICLE(iBot)
			BREAK
			CASE SCGW_BOT_STATE_AI_COLLECT_PICKUP
				SCGW_PROCESS_BOT_AI_COLLECT_PICKUP(iBot)
			BREAK
			CASE SCGW_BOT_STATE_AI_SUDDEN_DEATH
				SCGW_PROCESS_BOT_AI_SUDDEN_DEATH(iBot)
			BREAK
		ENDSWITCH
		IF SCGW_Bot[iBot].iTargetPosX = -1
			SCGW_Bot[iBot].iTargetPosX = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYER_POSITIONS_X/2)*2
		ENDIF
		IF SCGW_Bot[iBot].iTargetPosY = -1
			SCGW_Bot[iBot].iTargetPosY = GET_RANDOM_INT_IN_RANGE(0,ciSCGW_MAX_PLAYER_POSITIONS_Y/2)*2
		ENDIF

		SCGW_Bot[iBot].bTaskComplete = FALSE
	ELSE
		IF (SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime + SCGW_GET_PLAYER_MOVEMENT_TIME(iBot) < NATIVE_TO_INT(GET_NETWORK_TIME())  
		AND IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED))
			IF SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(SCGW_Bot[iBot].iTargetPosX,SCGW_Bot[iBot].iTargetPosY)
			OR SCGW_Bot[iBot].iTargetPosX = -1 AND SCGW_Bot[iBot].iTargetPosY = -1
				SCGW_Bot[iBot].bTaskComplete = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Main processing for player input
PROC SCGW_PROCESS_INPUT()
	
	SCGW_PROCESS_MOVEMENT_INPUT()
	
	SCGW_PROCESS_TURN_INPUT()
	
	SCGW_PROCESS_SHOOT_INPUT()
	
ENDPROC

PROC SCGW_PROCESS_INPUT_BOT(INT iBot)
	
	SCGW_PROCESS_BOT_AI(iBot)
	
	SCGW_PROCESS_MOVEMENT_INPUT_BOT(iBot)
	
	SCGW_PROCESS_TURN_INPUT_BOT(iBot)
	
	SCGW_PROCESS_SHOOT_INPUT_BOT(iBot)
	
ENDPROC

// ------------------------------ PICKUP PROCESSING ------------------------------

/// PURPOSE:
///    Sets the pickup as being collected by a player
///    HOST ONLY
PROC SCGW_REMOVE_COLLECTED_PICKUP(INT iPickupIndex)

	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_REMOVE_COLLECTED_PICKUP - Removing collected pickup: ", iPickupIndex)
	SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupType = ciSCGW_PICKUPS_NONE
	SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupLocation = -1
	SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupSpawnTime = -HIGHEST_INT

ENDPROC

/// PURPOSE:
///    Processes the spawning of pickups
///    HOST ONLY
PROC SCGW_PROCESS_SERVER_PICKUPS(BOOL bForceSpawn = FALSE, BOOL bSuddenDeath = FALSE)
	
	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	IF SCGW_ServerBD.bSuddenDeath
	AND NOT bForceSpawn
		EXIT
	ENDIF
	
	INT i
	
	FOR i = 0 TO ciSCGW_MAX_SPAWNED_PICKUPS - 1
		IF SCGW_ServerBD.sPickups[i].iSpawnedPickupSpawnTime = -HIGHEST_INT
			RELOOP
		ENDIF
		
		IF (NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_ServerBD.sPickups[i].iSpawnedPickupSpawnTime + ciSCGW_PICKUP_DURATION)
			RELOOP
		ENDIF
		
		SCGW_ServerBD.sPickups[i].iSpawnedPickupLocation = -1
		SCGW_ServerBD.sPickups[i].iSpawnedPickupType = ciSCGW_PICKUPS_NONE
		SCGW_ServerBD.sPickups[i].iSpawnedPickupSpawnTime = -HIGHEST_INT
	ENDFOR	

	IF (NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_ServerBD.iLastPickupSpawnTime + ciSCGW_PICKUP_SPAWN_INTERVAL)
	AND NOT bForceSpawn
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_SERVER_PICKUPS - SPAWNING NEW PICKUPS")
		
	SCGW_ServerBD.iLastPickupSpawnTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		
	IF bSuddenDeath
		//Sudden Death
		SCGW_SPAWN_SUDDEN_DEATH_PICKUPS()
	ELIF NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.iStartTime < ciSCGW_PICKUP_SPAWN_INTERVAL
		//First Spawn
		SCGW_SPAWN_STARTING_PICKUPS()
	ELIF NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.iStartTime >= ciSCGW_PICKUP_SPAWN_INTERVAL
	AND NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.iStartTime < (ciSCGW_MATCH_LENGTH / 2)
		//Mid Game
		SCGW_SPAWN_MID_GAME_PICKUPS()
	ELIF NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.iStartTime < ciSCGW_MATCH_LENGTH - ciSCGW_PICKUP_DURATION
		//Late Game
		SCGW_SPAWN_LATE_GAME_PICKUPS()
	ENDIF
	
ENDPROC

// ------------------------------ PLAYER PROCESSING ------------------------------

/// PURPOSE:
///    Updates the player state
PROC SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE eNewState)
	
	IF sSCGWData.sLocalPlayer.iLocalPart = -1
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_PLAYER_STATE - State changed from ", 
		SCGW_DEBUG_GET_PLAYER_STATE_AS_STRING(SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState), " to ", SCGW_DEBUG_GET_PLAYER_STATE_AS_STRING(eNewState))
	
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState = eNewState
	
ENDPROC

PROC SCGW_SET_PLAYER_STATE_BOT(INT iBot, SCGW_PLAYER_STATE eNewState)
	
	IF iBot = -1
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_PLAYER_STATE_BOT - State changed from ", 
		SCGW_DEBUG_GET_PLAYER_STATE_AS_STRING(SCGW_Bot[iBot].SCGW_player.eCurrentPlayerState), " to ", SCGW_DEBUG_GET_PLAYER_STATE_AS_STRING(eNewState))
	
	SCGW_Bot[iBot].SCGW_player.eCurrentPlayerState = eNewState
	
ENDPROC

/// PURPOSE:
///    Updates the player's health by iHealthChange
PROC SCGW_UPDATE_PLAYER_HEALTH(INT iHealthChange, BOOL bHitByCar = FALSE)
	
	INT iNewHealth = SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth + iHealthChange
	
	IF iNewHealth > ciSCGW_MAX_PLAYER_HEALTH
		iNewHealth = ciSCGW_MAX_PLAYER_HEALTH
	ELIF iNewHealth <= 0
		iNewHealth = 0
		IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState = SCGW_PLAYER_STATE_ALIVE
			SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_DEAD)
			SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_DIED_THIS_ROUND)
			SCGW_PLAY_LOCAL_PLAYER_DEATH_SOUND("Death_Bleeps")
			g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_DEAD
		ENDIF
	ENDIF
	
	IF iNewHealth != SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth
		SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth = iNewHealth
	ENDIF
	
	IF iHealthChange < 0
		BROADCAST_SCRIPT_EVENT_SCGW_MINIGAME_DAMAGED(sSCGWData.sLocalPlayer.iLocalPart, SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE, bHitByCar)
	ENDIF
	
ENDPROC

PROC SCGW_UPDATE_PLAYER_HEALTH_BOT(INT iBot, INT iHealthChange, BOOL bHitByCar = FALSE)
	
	INT iNewHealth = SCGW_Bot[iBot].SCGW_player.iHealth + iHealthChange
	
	IF iNewHealth > ciSCGW_MAX_PLAYER_HEALTH
		iNewHealth = ciSCGW_MAX_PLAYER_HEALTH
	ELIF iNewHealth <= 0
		iNewHealth = 0
		IF SCGW_Bot[iBot].SCGW_player.eCurrentPlayerState = SCGW_PLAYER_STATE_ALIVE
			SCGW_SET_PLAYER_STATE_BOT(iBot,SCGW_PLAYER_STATE_DEAD)
//			SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_DIED_THIS_ROUND)
			SCGW_PLAY_LOCAL_PLAYER_DEATH_SOUND("Death_Bleeps")
//			g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_DEAD
		ENDIF
	ENDIF
	
	IF iNewHealth != SCGW_Bot[iBot].SCGW_player.iHealth
		SCGW_Bot[iBot].SCGW_player.iHealth = iNewHealth
	ENDIF
	
	IF iHealthChange < 0
		BROADCAST_SCRIPT_EVENT_SCGW_MINIGAME_DAMAGED(iBot, SCGW_Bot[iBot].SCGW_player.eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE, bHitByCar)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns whether or not the pickup can be used by the player
FUNC BOOL SCGW_CAN_PLAYER_USE_PICKUP(INT iPickupIndex)
	
	SWITCH SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupType
		
		CASE ciSCGW_PICKUPS_HEALTH
			IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth = ciSCGW_MAX_PLAYER_HEALTH
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_SPEED
		CASE ciSCGW_PICKUPS_GANG_CARS
			//NEVER BLOCKED
			RETURN TRUE
		
		CASE ciSCGW_PICKUPS_PISTOL
			IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_PISTOL
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_UZI
			IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_UZI
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_AK47
			IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_AK47
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_ROCKET
			IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_ROCKET
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_SHOTGUN
			IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_SHOTGUN
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SCGW_CAN_PLAYER_USE_PICKUP_BOT(INT iPickupIndex, INT iBot)
	
	SWITCH SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupType
		
		CASE ciSCGW_PICKUPS_HEALTH
			IF SCGW_Bot[iBot].SCGW_player.iHealth = ciSCGW_MAX_PLAYER_HEALTH
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_SPEED
		CASE ciSCGW_PICKUPS_GANG_CARS
			//NEVER BLOCKED
			RETURN TRUE
		
		CASE ciSCGW_PICKUPS_PISTOL
			IF SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_PISTOL
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_UZI
			IF SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_UZI
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_AK47
			IF SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_AK47
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_ROCKET
			IF SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_ROCKET
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciSCGW_PICKUPS_SHOTGUN
			IF SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_SHOTGUN
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Applies a collected pickup's effect to the player
/// PARAMS:
///    iPickupIndex - 
PROC SCGW_PROCESS_PICKUP_COLLECTED(INT iPickupIndex)

	VECTOR_2D vPickupPosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupLocation)

	sArcadePlayStats.powerUps ++
	
	SWITCH SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupType
		
		CASE ciSCGW_PICKUPS_HEALTH
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Health", vPickupPosition)
			SCGW_UPDATE_PLAYER_HEALTH(ciSCGW_HEALTH_PICKUP_VALUE)
		BREAK
		
		CASE ciSCGW_PICKUPS_SPEED
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Speed", vPickupPosition)
			sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves += ciSCGW_SPEED_PICKUP_VALUE
		BREAK
		
		CASE ciSCGW_PICKUPS_GANG_CARS
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Transport", vPickupPosition)
			BROADCAST_SCGW_MINIGAME_REQUEST_CARS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition, SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang)
		BREAK
		
		CASE ciSCGW_PICKUPS_PISTOL
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_PISTOL
		BREAK
		
		CASE ciSCGW_PICKUPS_UZI
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_UZI
		BREAK
		
		CASE ciSCGW_PICKUPS_AK47
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_AK47
		BREAK
		
		CASE ciSCGW_PICKUPS_ROCKET
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_ROCKET
		BREAK
		
		CASE ciSCGW_PICKUPS_SHOTGUN
			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_SHOTGUN
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOOTING_HELP_SHOWN)
	AND SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType != ciSCGW_WEAPON_NONE
		SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOOTING_HELP_SHOWN)
		SCGW_TRIGGER_HELP_TEXT("SCGW_H_SHOOT", ciSCGW_DEFAULT_HELP_TEXT_TIME)
	ENDIF

ENDPROC

PROC SCGW_PROCESS_PICKUP_COLLECTED_BOT(INT iPickupIndex, INT iBot)

//	VECTOR_2D vPickupPosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupLocation)

	sArcadePlayStats.powerUps ++
	
	SWITCH SCGW_ServerBD.sPickups[iPickupIndex].iSpawnedPickupType
		
		CASE ciSCGW_PICKUPS_HEALTH
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Health", vPickupPosition)
			SCGW_UPDATE_PLAYER_HEALTH_BOT(iBot,ciSCGW_HEALTH_PICKUP_VALUE)
		BREAK
		
		CASE ciSCGW_PICKUPS_SPEED
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Speed", vPickupPosition)
			sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves += ciSCGW_SPEED_PICKUP_VALUE
		BREAK
		
		CASE ciSCGW_PICKUPS_GANG_CARS
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Transport", vPickupPosition)
			BROADCAST_SCGW_MINIGAME_REQUEST_CARS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, SCGW_Bot[iBot].SCGW_player.iGang)
		BREAK
		
		CASE ciSCGW_PICKUPS_PISTOL
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_PISTOL
		BREAK
		
		CASE ciSCGW_PICKUPS_UZI
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_UZI
		BREAK
		
		CASE ciSCGW_PICKUPS_AK47
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_AK47
		BREAK
		
		CASE ciSCGW_PICKUPS_ROCKET
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_ROCKET
		BREAK
		
		CASE ciSCGW_PICKUPS_SHOTGUN
//			SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND("Pickup_Collect_Weapon", vPickupPosition)
			SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_SHOTGUN
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOOTING_HELP_SHOWN)
	AND SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType != ciSCGW_WEAPON_NONE
//		SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOOTING_HELP_SHOWN)
//		SCGW_TRIGGER_HELP_TEXT("SCGW_H_SHOOT", ciSCGW_DEFAULT_HELP_TEXT_TIME)
	ENDIF

ENDPROC

/// PURPOSE:
///    Initialises the player's starting position based on gang
PROC SCGW_SET_LOCAL_PLAYER_START_POSITION()
	
	SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(sSCGWData.sPositionData.iPlayerStartPositions[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang][ciSCGW_PLAYER_GRID_AXIS_X], sSCGWData.sPositionData.iPlayerStartPositions[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang][ciSCGW_PLAYER_GRID_AXIS_Y])
	SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition = SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition

	IF sSCGWData.sPositionData.iPlayerStartPositions[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang][ciSCGW_PLAYER_GRID_AXIS_Y] = 0
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
	ELSE
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iFacingDirection = ciSCGW_GRID_DIRECTION_UP
	ENDIF

ENDPROC

PROC SCGW_SET_LOCAL_PLAYER_START_POSITION_BOT(INT iBot)
	
	SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition = SCGW_GET_POSITION_FROM_COORDS(sSCGWData.sPositionData.iPlayerStartPositions[SCGW_Bot[iBot].SCGW_player.iGang][ciSCGW_PLAYER_GRID_AXIS_X], sSCGWData.sPositionData.iPlayerStartPositions[SCGW_Bot[iBot].SCGW_player.iGang][ciSCGW_PLAYER_GRID_AXIS_Y])
	SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition = SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition

	IF sSCGWData.sPositionData.iPlayerStartPositions[SCGW_Bot[iBot].SCGW_player.iGang][ciSCGW_PLAYER_GRID_AXIS_Y] = 0
		SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_DOWN
	ELSE
		SCGW_Bot[iBot].SCGW_UF_Player.iFacingDirection = ciSCGW_GRID_DIRECTION_UP
	ENDIF

ENDPROC

/// PURPOSE:
///    Returns the best respawn position for the local player
///    Picks a corner the player owns with no players or cars if possible
FUNC INT SCGW_GET_BEST_RESPAWN_POSITION()

	INT iBestPosition = -1
	INT iBestScore = -1
	
	INT i, j
	INT iPlayerXBitSet, iPlayerYBitSet, iCarXBitSet, iCarYBitSet
	INT iTempPosX, iTempPosY
	
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
		
		IF i = sSCGWData.sLocalPlayer.iLocalPart
			RELOOP
		ENDIF
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT SCGW_Bot[i].bActive
			RELOOP
		ENDIF
	
		IF SCGW_PlayerBD[i].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		IF SCGW_IS_PLAYER_DEAD(i)
			RELOOP
		ENDIF
	
		SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[i].iPlayerPosition, iTempPosX, iTempPosY)
		SET_BIT(iPlayerXBitSet, iTempPosX)
		SET_BIT(iPlayerYBitSet, iTempPosY)
	ENDFOR
	
	FOR i = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
	
		IF SCGW_ServerBD.sVehicles[i].iVehicleType = -1 
			RELOOP
		ENDIF
		
		SCGW_GET_POSITION_COORDS(SCGW_ServerBD.sVehicles[i].iVehicleOrigin, iTempPosX, iTempPosY)
		
		IF SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_UP
		OR SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_DOWN
			SET_BIT(iCarXBitSet, iTempPosX)
		ELIF SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_LEFT
		OR SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_RIGHT
			SET_BIT(iCarYBitSet, iTempPosY)
		ENDIF
		
	ENDFOR
	
	INT iCurrentPosition = -1
	INT iCurrentScore = 0
	FOR i = 0 TO ciSCGW_MAX_PLAYER_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_PLAYER_POSITIONS_Y - 1
		
			iCurrentScore = ciSCGW_RESPAWNING_TOTAL_SCORE
			iCurrentPosition =  SCGW_GET_POSITION_FROM_COORDS(i, j)
		
			IF NOT( IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iCurrentPosition)
			OR IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iCurrentPosition))
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(iCarXBitSet, i)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_CARS_X_SCORE
			ENDIF
			
			IF IS_BIT_SET(iCarYBitSet, j)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_CARS_Y_SCORE
			ENDIF
			
			IF IS_BIT_SET(iPlayerXBitSet, i)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_PLAYERS_X_SCORE
			ENDIF
			
			IF IS_BIT_SET(iPlayerYBitSet, j)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_PLAYERS_Y_SCORE
			ENDIF
			
			IF iCurrentScore = ciSCGW_RESPAWNING_TOTAL_SCORE
				RETURN iCurrentPosition
			ENDIF
			
			IF iCurrentScore > iBestScore
				iBestScore = iCurrentScore
				iBestPosition = iCurrentPosition
			ENDIF
			
		ENDFOR
	ENDFOR
	
	RETURN iBestPosition
	
ENDFUNC

FUNC INT SCGW_GET_BEST_RESPAWN_POSITION_BOT(INT iBot)

	INT iBestPosition = -1
	INT iBestScore = -1
	
	INT i, j
	INT iPlayerXBitSet, iPlayerYBitSet, iCarXBitSet, iCarYBitSet
	INT iTempPosX, iTempPosY
	
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
		
		IF i = iBot
			RELOOP
		ENDIF
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT SCGW_Bot[i].bActive
			RELOOP
		ENDIF
	
		IF SCGW_PlayerBD[i].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		IF SCGW_IS_PLAYER_DEAD(i)
			RELOOP
		ENDIF
	
		SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[i].iPlayerPosition, iTempPosX, iTempPosY)
		SET_BIT(iPlayerXBitSet, iTempPosX)
		SET_BIT(iPlayerYBitSet, iTempPosY)
	ENDFOR
	
	FOR i = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
	
		IF SCGW_ServerBD.sVehicles[i].iVehicleType = -1 
			RELOOP
		ENDIF
		
		SCGW_GET_POSITION_COORDS(SCGW_ServerBD.sVehicles[i].iVehicleOrigin, iTempPosX, iTempPosY)
		
		IF SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_UP
		OR SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_DOWN
			SET_BIT(iCarXBitSet, iTempPosX)
		ELIF SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_LEFT
		OR SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_RIGHT
			SET_BIT(iCarYBitSet, iTempPosY)
		ENDIF
		
	ENDFOR
	
	INT iCurrentPosition = -1
	INT iCurrentScore = 0
	FOR i = 0 TO ciSCGW_MAX_PLAYER_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_PLAYER_POSITIONS_Y - 1
		
			iCurrentScore = ciSCGW_RESPAWNING_TOTAL_SCORE
			iCurrentPosition =  SCGW_GET_POSITION_FROM_COORDS(i, j)
		
			IF NOT( IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_Bot[iBot].SCGW_player.iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iCurrentPosition)
			OR IS_LONG_BIT_SET(SCGW_ServerBD.iPositionOwnershipBS[SCGW_Bot[iBot].SCGW_player.iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iCurrentPosition))
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(iCarXBitSet, i)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_CARS_X_SCORE
			ENDIF
			
			IF IS_BIT_SET(iCarYBitSet, j)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_CARS_Y_SCORE
			ENDIF
			
			IF IS_BIT_SET(iPlayerXBitSet, i)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_PLAYERS_X_SCORE
			ENDIF
			
			IF IS_BIT_SET(iPlayerYBitSet, j)
				iCurrentScore -= ciSCGW_RESPAWNING_NO_PLAYERS_Y_SCORE
			ENDIF
			
			IF iCurrentScore = ciSCGW_RESPAWNING_TOTAL_SCORE
				RETURN iCurrentPosition
			ENDIF
			
			IF iCurrentScore > iBestScore
				iBestScore = iCurrentScore
				iBestPosition = iCurrentPosition
			ENDIF
			
		ENDFOR
	ENDFOR
	
	RETURN iBestPosition
	
ENDFUNC
/// PURPOSE:
///    Initialises the player's respawn position based on owned territory
PROC SCGW_SET_RESPAWN_POSITION()
	
	SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition = SCGW_GET_BEST_RESPAWN_POSITION()
	SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPreviousPlayerPosition = SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition
	
ENDPROC

PROC SCGW_SET_RESPAWN_POSITION_BOT(INT iBot)
	
	SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition = SCGW_GET_BEST_RESPAWN_POSITION_BOT(iBot)
	SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition = SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition
	
ENDPROC

/// PURPOSE:
///    Returns true if the player has terriroty they can respawn into
FUNC BOOL SCGW_CAN_PLAYER_RESPAWN(INT iParticipant)
	
	IF IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_STARTED_RESPAWNING)
		RETURN TRUE
	ENDIF
	
	IF SCGW_PlayerBD[iParticipant].iGang = -1
		RETURN FALSE
	ENDIF
	
	IF SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[iParticipant].iGang] = 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SCGW_CAN_PLAYER_RESPAWN_BOT(INT iBot)
	
	IF IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_STARTED_RESPAWNING)
		RETURN TRUE
	ENDIF
	
	IF SCGW_Bot[iBot].SCGW_player.iGang = -1
		RETURN FALSE
	ENDIF
	
	IF SCGW_UF_ServerBD.iCurrentScore[SCGW_Bot[iBot].SCGW_player.iGang] = 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Respawns the local player at owned territory
///    TODO: checks to make sure spawn location is good
PROC SCGW_RESPAWN_LOCAL_PLAYER()

	SCGW_SET_RESPAWN_POSITION()
	SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_ALIVE)
	SCGW_PLAY_LOCAL_PLAYER_DEATH_SOUND("Death_Resurrect")
	
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastRespawnTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth = ciSCGW_MAX_PLAYER_HEALTH
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_NONE
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].fMoveSpeed = cfSCGW_BASE_SPEED
	sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves = 0
	
	SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING)
	
	IF IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_STARTED_RESPAWNING)
		CLEAR_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_STARTED_RESPAWNING)
	ENDIF
	
ENDPROC

PROC SCGW_RESPAWN_LOCAL_PLAYER_BOT(INT iBot)

	SCGW_SET_RESPAWN_POSITION_BOT(iBot)
	SCGW_SET_PLAYER_STATE_BOT(iBot,SCGW_PLAYER_STATE_ALIVE)
	SCGW_PLAY_LOCAL_PLAYER_DEATH_SOUND("Death_Resurrect")
	
	SCGW_Bot[iBot].SCGW_player.iLastRespawnTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	SCGW_Bot[iBot].SCGW_player.iHealth = ciSCGW_MAX_PLAYER_HEALTH
	SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_NONE
	SCGW_Bot[iBot].SCGW_player.fMoveSpeed = cfSCGW_BASE_SPEED
	sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves = 0
	
//	SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING)
	
	IF IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_STARTED_RESPAWNING)
		CLEAR_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_STARTED_RESPAWNING)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Processes respawning or blocking respawning when the local player is dead
PROC SCGW_PROCESS_LOCAL_PLAYER_RESPAWNING()
	
	IF SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang] = 0
	AND (NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.iStartTime) > ciSCGW_ALLOW_TERRITORY_HELP_TIME
		IF IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_MOVEMENT_HELP_SHOWN)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_RESPAWN_HELP_SHOWN)
			SCGW_TRIGGER_HELP_TEXT("SCGW_H_RSPWN", ciSCGW_DEFAULT_HELP_TEXT_TIME)	
			SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_RESPAWN_HELP_SHOWN)
		ENDIF
	ENDIF
	
	IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_DEAD
		EXIT
	ENDIF
	
	SCGW_CLEAR_HELP_TEXT()
	
	IF SCGW_ServerBD.bSuddenDeath
		SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_CANT_RESPAWN)
		SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_DEAD_CANT_RESPAWN)
	ENDIF
	
	IF SCGW_CAN_PLAYER_RESPAWN(sSCGWData.sLocalPlayer.iLocalPart)
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) >= SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastDamagedTime + ciSCGW_WASTED_TIME
		AND NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_STARTED_RESPAWNING)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_LOCAL_PLAYER_RESPAWNING - ciSCGW_LOCALBS_STARTED_RESPAWNING")			
			SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_STARTED_RESPAWNING)
		ENDIF
		
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) >= SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastDamagedTime + ciSCGW_RESPAWN_TIME 
			SCGW_RESPAWN_LOCAL_PLAYER()
		ENDIF
	ELSE	
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) > SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastDamagedTime + ciSCGW_NO_RESPAWN_GRACE_PERIOD
		AND NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_STARTED_RESPAWNING)
			SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_CANT_RESPAWN)
			SCGW_PLAY_LOCAL_PLAYER_DEATH_SOUND("Death_Dead")
			SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_DEAD_CANT_RESPAWN)
		ENDIF
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_LOCAL_PLAYER_RESPAWNING_BOT(INT iBot)
	
	IF SCGW_UF_ServerBD.iCurrentScore[SCGW_Bot[iBot].SCGW_player.iGang] = 0
	AND (NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.iStartTime) > ciSCGW_ALLOW_TERRITORY_HELP_TIME
		IF IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_MOVEMENT_HELP_SHOWN)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_RESPAWN_HELP_SHOWN)
//			SCGW_TRIGGER_HELP_TEXT("SCGW_H_RSPWN", ciSCGW_DEFAULT_HELP_TEXT_TIME)	
//			SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_RESPAWN_HELP_SHOWN)
		ENDIF
	ENDIF
	
	IF SCGW_Bot[iBot].SCGW_player.eCurrentPlayerState != SCGW_PLAYER_STATE_DEAD
		EXIT
	ENDIF
	
//	SCGW_CLEAR_HELP_TEXT()
	
	IF SCGW_ServerBD.bSuddenDeath
		SCGW_SET_PLAYER_STATE_BOT(iBot,SCGW_PLAYER_STATE_CANT_RESPAWN)
//		SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_DEAD_CANT_RESPAWN)
	ENDIF
	
	IF SCGW_CAN_PLAYER_RESPAWN_BOT(iBot)
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) >= SCGW_Bot[iBot].SCGW_player.iLastDamagedTime + ciSCGW_WASTED_TIME
		AND NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_STARTED_RESPAWNING)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_LOCAL_PLAYER_RESPAWNING - ciSCGW_LOCALBS_STARTED_RESPAWNING")			
			SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_STARTED_RESPAWNING)
		ENDIF
		
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) >= SCGW_Bot[iBot].SCGW_player.iLastDamagedTime + ciSCGW_RESPAWN_TIME 
			SCGW_RESPAWN_LOCAL_PLAYER_BOT(iBot)
		ENDIF
	ELSE	
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) > SCGW_Bot[iBot].SCGW_player.iLastDamagedTime + ciSCGW_NO_RESPAWN_GRACE_PERIOD
		AND NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_STARTED_RESPAWNING)
			SCGW_SET_PLAYER_STATE_BOT(iBot,SCGW_PLAYER_STATE_CANT_RESPAWN)
			SCGW_PLAY_LOCAL_PLAYER_DEATH_SOUND("Death_Dead")
//			SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_DEAD_CANT_RESPAWN)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Processes the position of player iParticipant
PROC SCGW_PROCESS_PLAYER_POSITION(INT iParticipant)

	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITION - iParticipant ",iParticipant)			
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[iParticipant].iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
	SCGW_GET_POSITION_COORDS(SCGW_UF_PlayerBD[iParticipant].iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	VECTOR_2D vPlayerDestination = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_UF_PlayerBD[iParticipant].iPlayerPosition)
	VECTOR_2D vPreviousPlayerPosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_UF_PlayerBD[iParticipant].iPreviousPlayerPosition)
	FLOAT fAlpha = TO_FLOAT(NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_UF_PlayerBD[iParticipant].iLastMovementTime) /  (TO_FLOAT(SCGW_GET_PLAYER_MOVEMENT_TIME(iParticipant)))
	
	IF fAlpha < 0.0
		fAlpha = 0.0
	ENDIF
	
	IF iParticipant = sSCGWData.sLocalPlayer.iLocalPart
	AND ((fAlpha >= 0.65 AND NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF))
	OR (fAlpha >= 0.8 AND NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)))
		IF iPreviousPlayerX != iCurrentPlayerX
			IF iPreviousPlayerX > iCurrentPlayerX
				//Moving Left
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(SCGW_UF_PlayerBD[iParticipant].iPlayerPosition, ciSCGW_OWNERSHIP_LINK_RIGHT, SCGW_PlayerBD[iParticipant].iGang)
			ELSE
				//Moving Right
				INT iBit = SCGW_GET_POSITION_FROM_COORDS(iCurrentPlayerX - 1, iCurrentPlayerY)
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(iBit, ciSCGW_OWNERSHIP_LINK_RIGHT, SCGW_PlayerBD[iParticipant].iGang)
			ENDIF
		ELIF iPreviousPlayerY != iCurrentPlayerY
			IF iPreviousPlayerY > iCurrentPlayerY
				//Moving Up
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(SCGW_UF_PlayerBD[iParticipant].iPlayerPosition, ciSCGW_OWNERSHIP_LINK_DOWN, SCGW_PlayerBD[iParticipant].iGang)
			ELSE
				//Moving Down
				INT iBit = SCGW_GET_POSITION_FROM_COORDS(iCurrentPlayerX, iCurrentPlayerY - 1)
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(iBit, ciSCGW_OWNERSHIP_LINK_DOWN, SCGW_PlayerBD[iParticipant].iGang)
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
			SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
		ELSE
			CLEAR_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_DIRECTION_CHANGED)
			SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)
		ENDIF
	ENDIF
	
	IF fAlpha > 0.85
	OR fAlpha < 0.15
	
		//Pickups
		INT iPlayerPos
		IF fAlpha > 0.85
			iPlayerPos = SCGW_UF_PlayerBD[iParticipant].iPlayerPosition
		ELSE
			iPlayerPos = SCGW_UF_PlayerBD[iParticipant].iPreviousPlayerPosition
		ENDIF
		
		INT i
		FOR i = 0 TO ciSCGW_MAX_SPAWNED_PICKUPS - 1
			
			IF SCGW_ServerBD.sPickups[i].iSpawnedPickupType = ciSCGW_PICKUPS_NONE
				IF IS_BIT_SET(sSCGWData.iHiddenPickupsBS, i)
					CLEAR_BIT(sSCGWData.iHiddenPickupsBS, i)
				ENDIF
				RELOOP
			ENDIF
			
			IF NOT SCGW_CAN_PLAYER_USE_PICKUP(i)
				RELOOP
			ENDIF
			
			IF NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.sPickups[i].iSpawnedPickupSpawnTime  < ciSCGW_PICKUP_COLLECTION_TIME
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(sSCGWData.iHiddenPickupsBS, i)
				RELOOP
			ENDIF
			
			IF iPlayerPos = SCGW_ServerBD.sPickups[i].iSpawnedPickupLocation
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_UPDATE_PLAYER_POSITION - Player ", iParticipant, " picked up pickup ", i, " type ", SCGW_ServerBD.sPickups[i].iSpawnedPickupType, " at ", SCGW_ServerBD.sPickups[i].iSpawnedPickupLocation)			
				IF iParticipant = sSCGWData.sLocalPlayer.iLocalPart
					SET_BIT(sSCGWData.iHiddenPickupsBS, i)
					SCGW_PROCESS_PICKUP_COLLECTED(i)
					SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_PICKUP_DURATION, ciSCGW_VIB_PICKUP_FREQ)
					BROADCAST_SCGW_MINIGAME_PICKUP_COLLECTED(i)
					SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_COLLECTED_PICKUP)
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF	
	
	IF fAlpha < 1.0
		IF sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition.x < 0
		AND sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition.y < 0
			sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition = LERP_VECTOR_2D(vPreviousPlayerPosition, vPlayerDestination, fAlpha)
		ELSE
			sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition = sSCGWData.sPlayers[iParticipant].vActualPlayerPosition
		ENDIF
		sSCGWData.sPlayers[iParticipant].vActualPlayerPosition = LERP_VECTOR_2D(vPreviousPlayerPosition, vPlayerDestination, fAlpha)
		EXIT
	ENDIF
	
	IF sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition.x < 0
	AND sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition.y < 0
		sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition =  vPlayerDestination
	ELSE
		sSCGWData.sPlayers[iParticipant].vLastFramePlayerPosition = sSCGWData.sPlayers[iParticipant].vActualPlayerPosition
	ENDIF
	sSCGWData.sPlayers[iParticipant].vActualPlayerPosition = vPlayerDestination
	sSCGWData.sPlayers[iParticipant].iPlayerAnimFrame = 0
	
ENDPROC

PROC SCGW_PROCESS_PLAYER_POSITION_BOT(INT iBot)

	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITION_BOT - iBot ",iBot)
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITION_BOT - SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition ",SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition)
	INT iCurrentPlayerX, iCurrentPlayerY, iPreviousPlayerX, iPreviousPlayerY
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, iCurrentPlayerX, iCurrentPlayerY)
	SCGW_GET_POSITION_COORDS(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition, iPreviousPlayerX, iPreviousPlayerY)
	VECTOR_2D vPlayerDestination = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition)
	VECTOR_2D vPreviousPlayerPosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition)
	FLOAT fAlpha = TO_FLOAT(NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_Bot[iBot].SCGW_UF_Player.iLastMovementTime) /  (TO_FLOAT(SCGW_GET_PLAYER_MOVEMENT_TIME_BOT(iBot)))
	
	IF fAlpha < 0.0
		fAlpha = 0.0
	ENDIF
	
	IF ((fAlpha >= 0.65 AND NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF))
	OR (fAlpha >= 0.8 AND NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)))
		IF iPreviousPlayerX != iCurrentPlayerX
			IF iPreviousPlayerX > iCurrentPlayerX
				//Moving Left
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, ciSCGW_OWNERSHIP_LINK_RIGHT, SCGW_Bot[iBot].SCGW_player.iGang)
			ELSE
				//Moving Right
				INT iBit = SCGW_GET_POSITION_FROM_COORDS(iCurrentPlayerX - 1, iCurrentPlayerY)
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(iBit, ciSCGW_OWNERSHIP_LINK_RIGHT, SCGW_Bot[iBot].SCGW_player.iGang)
			ENDIF
		ELIF iPreviousPlayerY != iCurrentPlayerY
			IF iPreviousPlayerY > iCurrentPlayerY
				//Moving Up
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition, ciSCGW_OWNERSHIP_LINK_DOWN, SCGW_Bot[iBot].SCGW_player.iGang)
			ELSE
				//Moving Down
				INT iBit = SCGW_GET_POSITION_FROM_COORDS(iCurrentPlayerX, iCurrentPlayerY - 1)
				BROADCAST_SCGW_MINIGAME_SIDE_CLAIMED(iBit, ciSCGW_OWNERSHIP_LINK_DOWN, SCGW_Bot[iBot].SCGW_player.iGang)
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
			SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
		ELSE
			CLEAR_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_DIRECTION_CHANGED)
			SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)
		ENDIF
	ENDIF
	
	IF fAlpha > 0.85
	OR fAlpha < 0.15
		//Pickups
		INT iPlayerPos
		IF fAlpha > 0.85
			iPlayerPos = SCGW_Bot[iBot].SCGW_UF_Player.iPlayerPosition
		ELSE
			iPlayerPos = SCGW_Bot[iBot].SCGW_UF_Player.iPreviousPlayerPosition
		ENDIF
		
		INT i
		FOR i = 0 TO ciSCGW_MAX_SPAWNED_PICKUPS - 1
			
			IF SCGW_ServerBD.sPickups[i].iSpawnedPickupType = ciSCGW_PICKUPS_NONE
				IF IS_BIT_SET(sSCGWData.iHiddenPickupsBS, i)
					CLEAR_BIT(sSCGWData.iHiddenPickupsBS, i)
				ENDIF
				RELOOP
			ENDIF
			
			IF NOT SCGW_CAN_PLAYER_USE_PICKUP_BOT(i,iBot)
				RELOOP
			ENDIF
			
			IF NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_ServerBD.sPickups[i].iSpawnedPickupSpawnTime  < ciSCGW_PICKUP_COLLECTION_TIME
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(sSCGWData.iHiddenPickupsBS, i)
				RELOOP
			ENDIF
			
			IF iPlayerPos = SCGW_ServerBD.sPickups[i].iSpawnedPickupLocation
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITION_BOT - Bot ", iBot, " picked up pickup ", i, " type ", SCGW_ServerBD.sPickups[i].iSpawnedPickupType, " at ", SCGW_ServerBD.sPickups[i].iSpawnedPickupLocation)			
//				IF iParticipant = sSCGWData.sLocalPlayer.iLocalPart;
					SET_BIT(sSCGWData.iHiddenPickupsBS, i)
					SCGW_PROCESS_PICKUP_COLLECTED_BOT(i,iBot)
					BROADCAST_SCGW_MINIGAME_PICKUP_COLLECTED(i)
					SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_COLLECTED_PICKUP)
//				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF	
	
	IF fAlpha < 1.0
		IF sSCGWData.sPlayers[iBot].vLastFramePlayerPosition.x < 0
		AND sSCGWData.sPlayers[iBot].vLastFramePlayerPosition.y < 0
			sSCGWData.sPlayers[iBot].vLastFramePlayerPosition = LERP_VECTOR_2D(vPreviousPlayerPosition, vPlayerDestination, fAlpha)
		ELSE
			sSCGWData.sPlayers[iBot].vLastFramePlayerPosition = sSCGWData.sPlayers[iBot].vActualPlayerPosition
		ENDIF
		sSCGWData.sPlayers[iBot].vActualPlayerPosition = LERP_VECTOR_2D(vPreviousPlayerPosition, vPlayerDestination, fAlpha)
		EXIT
	ENDIF
	
	IF sSCGWData.sPlayers[iBot].vLastFramePlayerPosition.x < 0
	AND sSCGWData.sPlayers[iBot].vLastFramePlayerPosition.y < 0
		sSCGWData.sPlayers[iBot].vLastFramePlayerPosition =  vPlayerDestination
	ELSE
		sSCGWData.sPlayers[iBot].vLastFramePlayerPosition = sSCGWData.sPlayers[iBot].vActualPlayerPosition
	ENDIF
	sSCGWData.sPlayers[iBot].vActualPlayerPosition = vPlayerDestination
	sSCGWData.sPlayers[iBot].iPlayerAnimFrame = 0
	
ENDPROC

/// PURPOSE:
///    Processes the positions of all active players
PROC SCGW_PROCESS_PLAYER_POSITIONS()

	INT iPlayerLoop = 0
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
	
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
			RELOOP
		ENDIF
	
		IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		IF SCGW_IS_PLAYER_DEAD(iPlayerLoop)
			sSCGWData.sPlayers[iPlayerLoop].vLastFramePlayerPosition = INIT_VECTOR_2D(-1,-1)
			RELOOP
		ENDIF

		SCGW_PROCESS_PLAYER_POSITION(iPlayerLoop)
		
	ENDFOR
	
ENDPROC

PROC SCGW_PROCESS_PLAYER_POSITIONS_BOT()

	INT iBot = 0
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS - 1
	
		IF NOT SCGW_Bot[iBot].bActive
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITIONS_BOT - NOT SCGW_Bot[iBot].bActive iBot ",iBot)
			RELOOP
		ENDIF
	
		IF SCGW_Bot[iBot].SCGW_player.iGang = ciSCGW_GANG_NONE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITIONS_BOT - SCGW_Bot[iBot].SCGW_player.iGang = ciSCGW_GANG_NONE iBot ",iBot)
			RELOOP
		ENDIF
		
		IF SCGW_IS_PLAYER_DEAD(iBot)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_PROCESS_PLAYER_POSITIONS_BOT - SCGW_IS_PLAYER_DEAD(iBot) iBot ",iBot)
			sSCGWData.sPlayers[iBot].vLastFramePlayerPosition = INIT_VECTOR_2D(-1,-1)
			RELOOP
		ENDIF

		SCGW_PROCESS_PLAYER_POSITION_BOT(iBot)
		
	ENDFOR
	
ENDPROC

PROC SCGW_PROCESS_DAMAGED_EVENT(INT iParticipant, BOOL bDead, BOOL bHitByCar)
	
	IF bDead
		IF bHitByCar
			SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Hit_By_Car", iParticipant, sSCGWData.sPlayers[iParticipant].iPlayerSoundId)
		ELSE
			SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Death", iParticipant, sSCGWData.sPlayers[iParticipant].iPlayerSoundId)
		ENDIF
	ELSE
		SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Pain", iParticipant, sSCGWData.sPlayers[iParticipant].iPlayerSoundId)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Processes a player beign hit by a projectile
PROC SCGW_PROCESS_PLAYER_HIT_BY_BULLET(INT iShotLoop, INT iCollisionPlayerLoop, INT iParticipant)

	sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vActualBulletPosition = sSCGWData.sPlayers[iCollisionPlayerLoop].vActualPlayerPosition
	
	IF iCollisionPlayerLoop = sSCGWData.sLocalPlayer.iLocalPart
		SCGW_UPDATE_PLAYER_HEALTH(-sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iShotDamage)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_HIT_DURATION, ciSCGW_VIB_HIT_FREQ)
		SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastDamagedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Local player was hit, new health: ", SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth)
		IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE
			SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_1) + SCGW_PlayerBD[iParticipant].iGang))
		ENDIF
	ELIF iParticipant = sSCGWData.sLocalPlayer.iLocalPart
	AND SCGW_PlayerBD[iCollisionPlayerLoop].iHealth - sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iShotDamage <= 0
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_1) + SCGW_PlayerBD[iCollisionPlayerLoop].iGang))
		sArcadePlayStats.kills ++
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_BOT_HIT_BY_BULLET(INT iShotLoop, INT iBotLoop, INT iParticipant)

	sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vActualBulletPosition = sSCGWData.sPlayers[iBotLoop].vActualPlayerPosition
	
	SCGW_UPDATE_PLAYER_HEALTH_BOT(iBotLoop,-sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iShotDamage)
	SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_HIT_DURATION, ciSCGW_VIB_HIT_FREQ)
	SCGW_Bot[iBotLoop].SCGW_player.iLastDamagedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Local player was hit, new health: ", SCGW_Bot[iBotLoop].SCGW_player.iHealth)
	IF SCGW_Bot[iBotLoop].SCGW_player.eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_1) + SCGW_PlayerBD[iParticipant].iGang))
	ENDIF
	IF SCGW_Bot[iBotLoop].SCGW_player.iHealth <= 0
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_1) + SCGW_Bot[iBotLoop].SCGW_player.iGang))
		sArcadePlayStats.kills ++
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_PLAYER_HIT_BY_BULLET_BOT(INT iShotLoop, INT iCollisionPlayerLoop, INT iBot)

	sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vActualBulletPosition = sSCGWData.sPlayers[iCollisionPlayerLoop].vActualPlayerPosition
	
	IF iCollisionPlayerLoop = sSCGWData.sLocalPlayer.iLocalPart
		SCGW_UPDATE_PLAYER_HEALTH(-sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iShotDamage)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_HIT_DURATION, ciSCGW_VIB_HIT_FREQ)
		SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastDamagedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Local player was hit, new health: ", SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth)
		IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE
			SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_1) + SCGW_Bot[iBot].SCGW_player.iGang))
		ENDIF
	ELIF iBot = sSCGWData.sLocalPlayer.iLocalPart
	AND SCGW_PlayerBD[iCollisionPlayerLoop].iHealth - sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iShotDamage <= 0
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_1) + SCGW_PlayerBD[iCollisionPlayerLoop].iGang))
		sArcadePlayStats.kills ++
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_BOT_HIT_BY_BULLET_BOT(INT iShotLoop, INT iBotLoop, INT iBot)

	sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vActualBulletPosition = sSCGWData.sPlayers[iBotLoop].vActualPlayerPosition
	
	SCGW_UPDATE_PLAYER_HEALTH_BOT(iBotLoop,-sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iShotDamage)
	SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_HIT_DURATION, ciSCGW_VIB_HIT_FREQ)
	SCGW_Bot[iBotLoop].SCGW_player.iLastDamagedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Local player was hit, new health: ", SCGW_Bot[iBotLoop].SCGW_player.iHealth)
	IF SCGW_Bot[iBotLoop].SCGW_player.eCurrentPlayerState != SCGW_PLAYER_STATE_ALIVE
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_1) + SCGW_Bot[iBot].SCGW_player.iGang))
	ENDIF
	IF SCGW_Bot[iBotLoop].SCGW_player.iHealth <= 0
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_1) + SCGW_Bot[iBotLoop].SCGW_player.iGang))
		sArcadePlayStats.kills ++
	ENDIF

	
ENDPROC

/// PURPOSE:
///    Processes player iParticipant's active shots
PROC SCGW_PROCESS_PLAYER_SHOT_POSITIONS(INT iParticipant, INT &iShotsThisFrameBS)
	
	VECTOR_2D vPlayerCollision0, vPlayerCollision1, vBulletCollision0, vBulletCollision1, vCollisionOffset
	
	INT iShotLoop = 0
	FOR iShotLoop = 0 TO ciSCGW_MAX_PLAYER_SHOTS - 1
	
		INT iShotBit = SCGW_GET_PARTICIPANT_SHOT_BIT(iParticipant, iShotLoop)
		
		IF SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotTime = -HIGHEST_INT
			IF IS_BIT_SET(sSCGWData.iFinalizedShotsBS, iShotBit)
				CLEAR_BIT(sSCGWData.iFinalizedShotsBS, iShotBit)
			ENDIF
			RELOOP
		ENDIF
		
		SET_BIT(iShotsThisFrameBS, iShotBit)
		IF NOT IS_BIT_SET(sSCGWData.iShotsLastFrame, iShotBit)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - START MUZZLE FLASH - Player ", iParticipant," Shot ", iShotLoop)
			sSCGWData.sPlayers[iParticipant].iMuzzleFlashFrame = 0
			SCGW_PLAY_SHOOTING_SOUND_FOR_PLAYER(iParticipant, iShotLoop)
		ENDIF
		
		FLOAT fShotDistance = 0
		VECTOR_2D vBulletGridDestination = SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].vShotOrigin
		
		SWITCH SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotDirection
			CASE ciSCGW_GRID_DIRECTION_UP
				fShotDistance = - (sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_Y)
				vBulletGridDestination.y += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(0, cfSCGW_COLLISION_OFFSET_Y)
			BREAK
			CASE ciSCGW_GRID_DIRECTION_DOWN
				fShotDistance = (sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_Y)
				vBulletGridDestination.y += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(0, -cfSCGW_COLLISION_OFFSET_Y)
			BREAK
			CASE ciSCGW_GRID_DIRECTION_LEFT
				fShotDistance = -(sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_X)
				vBulletGridDestination.x += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(cfSCGW_COLLISION_OFFSET_X, 0)
			BREAK
			CASE ciSCGW_GRID_DIRECTION_RIGHT
				fShotDistance = (sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_X)
				vBulletGridDestination.x += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(-cfSCGW_COLLISION_OFFSET_X, 0)
			BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," Shot ", iShotLoop, " (",SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].vShotOrigin.x,", ", SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].vShotOrigin.y, ") -> (", vBulletGridDestination.x, ", ", vBulletGridDestination.y, ")")
		
		FLOAT fAlpha = TO_FLOAT(NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotTime) /  TO_FLOAT(sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iGridRange * sSCGWData.sWeaponData[SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType].iGridShotTime)
		
		IF IS_BIT_SET(sSCGWData.iFinalizedShotsBS, iShotBit)
			IF fAlpha > 1.0 
				sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vLastFrameBulletPosition = INIT_VECTOR_2D(-1, -1)
				IF iParticipant = sSCGWData.sLocalPlayer.iLocalPart
					SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[iShotLoop].iShotTime = -HIGHEST_INT
					SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[iShotLoop].vShotOrigin.x = -1
					SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[iShotLoop].vShotOrigin.y = -1
					SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[iShotLoop].iShotDirection = -1
					SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[iShotLoop].iShotWeaponType = -1
				ENDIF
			ENDIF
			RELOOP
		ENDIF
		
		VECTOR_2D vOrigin = SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].vShotOrigin
		VECTOR_2D vDestination = vBulletGridDestination
		
		IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vLastFrameBulletPosition.x, -1 - cfARCADE_MINIGAME_FLOAT_TOLERANCE, -1 + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
		AND IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vLastFrameBulletPosition.y, -1 - cfARCADE_MINIGAME_FLOAT_TOLERANCE, -1 + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
			sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vLastFrameBulletPosition = vOrigin
		ELSE
			sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vLastFrameBulletPosition = sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vActualBulletPosition
		ENDIF
		
		sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vActualBulletPosition = LERP_VECTOR_2D(vOrigin, vDestination, fAlpha)
		
		//Collision
		INT iCollisionPlayerLoop = 0
		BOOL bHitPlayer = FALSE
		
		//Set up bullet collision lines
		vBulletCollision0 = sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vActualBulletPosition
		vBulletCollision1 = ADD_VECTOR_2D(sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vLastFrameBulletPosition, vCollisionOffset)

		FOR iCollisionPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - iCollisionPlayerLoop ",iCollisionPlayerLoop)
			
			IF iCollisionPlayerLoop = iParticipant
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - iCollisionPlayerLoop = iParticipant ")
				RELOOP
			ENDIF
			
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCollisionPlayerLoop))
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCollisionPlayerLoop)) ")
				RELOOP
			ENDIF
			
			IF SCGW_PlayerBD[iCollisionPlayerLoop].iGang = ciSCGW_GANG_NONE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - SCGW_PlayerBD[iCollisionPlayerLoop].iGang = ciSCGW_GANG_NONE ")
				RELOOP
			ENDIF
			
			IF SCGW_IS_PLAYER_DEAD(iCollisionPlayerLoop)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - SCGW_IS_PLAYER_DEAD(iCollisionPlayerLoop) ")
				RELOOP
			ENDIF
			
			vPlayerCollision0 = sSCGWData.sPlayers[iCollisionPlayerLoop].vActualPlayerPosition
			vPlayerCollision1 = sSCGWData.sPlayers[iCollisionPlayerLoop].vLastFramePlayerPosition
			
			IF ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X
			OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y")
				RELOOP
			ENDIF
			
			IF SCGW_DO_LINES_INTERSECT_FOR_COLLISION(vBulletCollision0, vBulletCollision1, vPlayerCollision0, vPlayerCollision1)	
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," Shot ", iShotLoop, " Hit player ", iCollisionPlayerLoop)
				SCGW_PROCESS_PLAYER_HIT_BY_BULLET(iShotLoop, iCollisionPlayerLoop, iParticipant)
				bHitPlayer = TRUE
			ENDIF
			
		ENDFOR
		INT iBotLoop = 0
		FOR iBotLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - iBotLoop ",iBotLoop)
			
			IF iBotLoop = iParticipant
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - iBotLoop = iParticipant ")
				RELOOP
			ENDIF
			
			IF NOT SCGW_Bot[iBotLoop].bActive
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - NOT SCGW_Bot[iBotLoop].bActive ")
				RELOOP
			ENDIF
			
			IF SCGW_Bot[iBotLoop].SCGW_player.iGang = ciSCGW_GANG_NONE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - SCGW_Bot[iBotLoop].SCGW_player.iGang = ciSCGW_GANG_NONE ")
				RELOOP
			ENDIF
			
			IF SCGW_IS_PLAYER_DEAD_BOT(iBotLoop)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - SCGW_IS_PLAYER_DEAD_BOT(iBotLoop) ")
				RELOOP
			ENDIF
			
			vPlayerCollision0 = sSCGWData.sPlayers[iBotLoop].vActualPlayerPosition
			vPlayerCollision1 = sSCGWData.sPlayers[iBotLoop].vLastFramePlayerPosition
			
			IF ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X
			OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," - ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y")
				RELOOP
			ENDIF
			
			IF SCGW_DO_LINES_INTERSECT_FOR_COLLISION(vBulletCollision0, vBulletCollision1, vPlayerCollision0, vPlayerCollision1)	
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Player ", iParticipant," Shot ", iShotLoop, " Hit Bot ", iBotLoop)
				SCGW_PROCESS_BOT_HIT_BY_BULLET(iShotLoop, iBotLoop, iParticipant)
				bHitPlayer = TRUE
			ENDIF
			
		ENDFOR
		
		IF fAlpha >= 1.0 
		OR bHitPlayer
			IF NOT IS_BIT_SET(sSCGWData.iFinalizedShotsBS, iShotBit)
				IF SCGW_UF_PlayerBD[iParticipant].sProjectiles[iShotLoop].iShotWeaponType = ciSCGW_WEAPON_ROCKET
					SCGW_ADD_EXPLOSION_AT_COORDS(sSCGWData.sPlayers[iParticipant].sProjectiles[iShotLoop].vActualBulletPosition)
				ENDIF
			
				SET_BIT(sSCGWData.iFinalizedShotsBS, iShotBit)
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC SCGW_PROCESS_PLAYER_SHOT_POSITIONS_BOT(INT iBot, INT &iShotsThisFrameBS)
	
	VECTOR_2D vPlayerCollision0, vPlayerCollision1, vBulletCollision0, vBulletCollision1, vCollisionOffset
	
	INT iShotLoop = 0
	FOR iShotLoop = 0 TO ciSCGW_MAX_PLAYER_SHOTS - 1
	
		INT iShotBit = SCGW_GET_PARTICIPANT_SHOT_BIT(iBot, iShotLoop)
		
		IF SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotTime = -HIGHEST_INT
			IF IS_BIT_SET(sSCGWData.iFinalizedShotsBSBot, iShotBit)
				CLEAR_BIT(sSCGWData.iFinalizedShotsBSBot, iShotBit)
			ENDIF
			RELOOP
		ENDIF
		
		SET_BIT(iShotsThisFrameBS, iShotBit)
		IF NOT IS_BIT_SET(sSCGWData.iShotsLastFrameBot, iShotBit)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - START MUZZLE FLASH - Bot ", iBot," Shot ", iShotLoop)
			sSCGWData.sPlayers[iBot].iMuzzleFlashFrame = 0
			SCGW_PLAY_SHOOTING_SOUND_FOR_BOT(iBot, iShotLoop)
		ENDIF
		
		FLOAT fShotDistance = 0
		VECTOR_2D vBulletGridDestination = SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].vShotOrigin
		
		SWITCH SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotDirection
			CASE ciSCGW_GRID_DIRECTION_UP
				fShotDistance = - (sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_Y)
				vBulletGridDestination.y += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(0, cfSCGW_COLLISION_OFFSET_Y)
			BREAK
			CASE ciSCGW_GRID_DIRECTION_DOWN
				fShotDistance = (sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_Y)
				vBulletGridDestination.y += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(0, -cfSCGW_COLLISION_OFFSET_Y)
			BREAK
			CASE ciSCGW_GRID_DIRECTION_LEFT
				fShotDistance = -(sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_X)
				vBulletGridDestination.x += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(cfSCGW_COLLISION_OFFSET_X, 0)
			BREAK
			CASE ciSCGW_GRID_DIRECTION_RIGHT
				fShotDistance = (sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iGridRange * cfSCGW_GRID_SPRITE_SCALE_X)
				vBulletGridDestination.x += fShotDistance
				vCollisionOffset = INIT_VECTOR_2D(-cfSCGW_COLLISION_OFFSET_X, 0)
			BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," Shot ", iShotLoop, " (",SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].vShotOrigin.x,", ", SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].vShotOrigin.y, ") -> (", vBulletGridDestination.x, ", ", vBulletGridDestination.y, ")")
		
		FLOAT fAlpha = TO_FLOAT(NATIVE_TO_INT(GET_NETWORK_TIME()) - SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotTime) /  TO_FLOAT(sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iGridRange * sSCGWData.sWeaponData[SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType].iGridShotTime)
		
		IF IS_BIT_SET(sSCGWData.iFinalizedShotsBSBot, iShotBit)
			IF fAlpha > 1.0 
				sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vLastFrameBulletPosition = INIT_VECTOR_2D(-1, -1)
//				IF iParticipant = sSCGWData.sLocalPlayer.iLocalPart
					SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotTime = -HIGHEST_INT
					SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].vShotOrigin.x = -1
					SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].vShotOrigin.y = -1
					SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotDirection = -1
					SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType = -1
//				ENDIF
			ENDIF
			RELOOP
		ENDIF
		
		VECTOR_2D vOrigin = SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].vShotOrigin
		VECTOR_2D vDestination = vBulletGridDestination
		
		IF IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vLastFrameBulletPosition.x, -1 - cfARCADE_MINIGAME_FLOAT_TOLERANCE, -1 + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
		AND IS_FLOAT_IN_RANGE(sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vLastFrameBulletPosition.y, -1 - cfARCADE_MINIGAME_FLOAT_TOLERANCE, -1 + cfARCADE_MINIGAME_FLOAT_TOLERANCE)
			sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vLastFrameBulletPosition = vOrigin
		ELSE
			sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vLastFrameBulletPosition = sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vActualBulletPosition
		ENDIF
		
		sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vActualBulletPosition = LERP_VECTOR_2D(vOrigin, vDestination, fAlpha)
		
		//Collision
		INT iCollisionPlayerLoop = 0
		BOOL bHitPlayer = FALSE
		
		//Set up bullet collision lines
		vBulletCollision0 = sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vActualBulletPosition
		vBulletCollision1 = ADD_VECTOR_2D(sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vLastFrameBulletPosition, vCollisionOffset)

		FOR iCollisionPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - iCollisionPlayerLoop ",iCollisionPlayerLoop)
			
			IF iCollisionPlayerLoop = iBot
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - iCollisionPlayerLoop = iBot ")
				RELOOP
			ENDIF
			
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCollisionPlayerLoop))
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCollisionPlayerLoop)) ")
				RELOOP
			ENDIF
			
			IF SCGW_PlayerBD[iCollisionPlayerLoop].iGang = ciSCGW_GANG_NONE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - SCGW_PlayerBD[iCollisionPlayerLoop].iGang = ciSCGW_GANG_NONE ")
				RELOOP
			ENDIF
			
			IF SCGW_IS_PLAYER_DEAD(iCollisionPlayerLoop)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - SCGW_IS_PLAYER_DEAD(iCollisionPlayerLoop) ")
				RELOOP
			ENDIF
			
			vPlayerCollision0 = sSCGWData.sPlayers[iCollisionPlayerLoop].vActualPlayerPosition
			vPlayerCollision1 = sSCGWData.sPlayers[iCollisionPlayerLoop].vLastFramePlayerPosition
			
			IF ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X
			OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y")
				RELOOP
			ENDIF
			
			IF SCGW_DO_LINES_INTERSECT_FOR_COLLISION(vBulletCollision0, vBulletCollision1, vPlayerCollision0, vPlayerCollision1)	
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," Shot ", iShotLoop, " Hit player ", iCollisionPlayerLoop)
				SCGW_PROCESS_PLAYER_HIT_BY_BULLET_BOT(iShotLoop, iCollisionPlayerLoop, iBot)
				bHitPlayer = TRUE
			ENDIF
			
		ENDFOR
		INT iBotLoop = 0
		FOR iBotLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - iCollisionPlayerLoop ",iCollisionPlayerLoop)
			
			IF iBotLoop = iBot
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - iBotLoop = iBot ")
				RELOOP
			ENDIF
			
			IF NOT SCGW_Bot[iBotLoop].bActive
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - NOT SCGW_Bot[iBotLoop].bActive ")
				RELOOP
			ENDIF
			
			IF SCGW_Bot[iBotLoop].SCGW_player.iGang = ciSCGW_GANG_NONE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - SCGW_Bot[iBotLoop].SCGW_player.iGang = ciSCGW_GANG_NONE")
				RELOOP
			ENDIF
			
			IF SCGW_IS_PLAYER_DEAD_BOT(iBotLoop)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - SCGW_IS_PLAYER_DEAD_BOT(iBotLoop)")
				RELOOP
			ENDIF
			
			vPlayerCollision0 = sSCGWData.sPlayers[iBotLoop].vActualPlayerPosition
			vPlayerCollision1 = sSCGWData.sPlayers[iBotLoop].vLastFramePlayerPosition
			
			IF ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X
			OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," - ABSF(vPlayerCollision0.x - vBulletCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X OR ABSF(vPlayerCollision0.y - vBulletCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y")
				RELOOP
			ENDIF
			
			IF SCGW_DO_LINES_INTERSECT_FOR_COLLISION(vBulletCollision0, vBulletCollision1, vPlayerCollision0, vPlayerCollision1)	
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_PLAYER_SHOT_POSITIONS - Bot ", iBot," Shot ", iShotLoop, " Hit bot ", iBotLoop)
				SCGW_PROCESS_BOT_HIT_BY_BULLET_BOT(iShotLoop, iBotLoop, iBot)
				bHitPlayer = TRUE
			ENDIF
			
		ENDFOR
		
		IF fAlpha >= 1.0 
		OR bHitPlayer
			IF NOT IS_BIT_SET(sSCGWData.iFinalizedShotsBSBot, iShotBit)
				IF SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[iShotLoop].iShotWeaponType = ciSCGW_WEAPON_ROCKET
					SCGW_ADD_EXPLOSION_AT_COORDS(sSCGWData.sPlayers[iBot].sProjectiles[iShotLoop].vActualBulletPosition)
				ENDIF
			
				SET_BIT(sSCGWData.iFinalizedShotsBSBot, iShotBit)
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Processes the shot positions of all active players
PROC SCGW_PROCESS_SHOT_POSITIONS()

	INT iShotsThisFrameBS = 0
	
	INT iPlayerLoop = 0
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
	
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
			RELOOP
		ENDIF
	
		IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		SCGW_PROCESS_PLAYER_SHOT_POSITIONS(iPlayerLoop, iShotsThisFrameBS)
		
	ENDFOR
	
	sSCGWData.iShotsLastFrame = iShotsThisFrameBS
	
ENDPROC

PROC SCGW_PROCESS_SHOT_POSITIONS_BOT()

	INT iShotsThisFrameBS = 0
	
	INT iBot = 0
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS - 1
	
		IF NOT SCGW_Bot[iBot].bActive
			RELOOP
		ENDIF
	
		IF SCGW_Bot[iBot].SCGW_player.iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		SCGW_PROCESS_PLAYER_SHOT_POSITIONS_BOT(iBot, iShotsThisFrameBS)
		
	ENDFOR
	
	sSCGWData.iShotsLastFrameBot = iShotsThisFrameBS
	
ENDPROC

// ------------------------------ VEHICLE PROCESSING ------------------------------

/// PURPOSE:
///    Processes a player beign hit by a vehicle
PROC SCGW_PROCESS_PLAYER_HIT_BY_VEHICLE(INT iCollisionPlayerLoop)
	
	IF iCollisionPlayerLoop = sSCGWData.sLocalPlayer.iLocalPart
		SCGW_UPDATE_PLAYER_HEALTH(-ciSCGW_VEHICLE_DAMAGE, TRUE)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, ciSCGW_VIB_CAR_HIT_DURATION, ciSCGW_VIB_CAR_HIT_FREQ)
		SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastDamagedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_PLAYER_HIT_BY_VEHICLE - Local player was hit, new health: ", SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth)
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_VEHICLE)
	ENDIF
	
ENDPROC

PROC SCGW_PROCESS_PLAYER_HIT_BY_VEHICLE_BOT(INT iBotLoop)
	
	SCGW_UPDATE_PLAYER_HEALTH_BOT(iBotLoop,-ciSCGW_VEHICLE_DAMAGE, TRUE)
	SCGW_Bot[iBotLoop].SCGW_player.iLastDamagedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_PLAYER_HIT_BY_VEHICLE - Bot ",iBotLoop," was hit, new health: ", SCGW_Bot[iBotLoop].SCGW_player.iHealth)
	SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_VEHICLE)
	
ENDPROC

/// PURPOSE:
///    Processes spawned vehicle positions
PROC SCGW_PROCESS_VEHICLE_POSITIONS()
	
	VECTOR_2D vPlayerCollision0, vPlayerCollision1, vVehicleCollision0, vVehicleCollision1, vCollisionOffset
	
	BOOL bVehicleOnScreen = FALSE
	
	INT i
	INT iStartX, iStartY, iEndX, iEndY
	FOR i = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
		INT iTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		IF SCGW_ServerBD.sVehicles[i].iVehicleType != -1 
		AND iTime > SCGW_ServerBD.sVehicles[i].iVehicleTime
			INT iVehicleMoveTime = PICK_INT(SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_DOWN OR SCGW_ServerBD.sVehicles[i].iVehicleDirection = ciSCGW_GRID_DIRECTION_UP, ciSCGW_MAX_PLAYER_POSITIONS_Y + 2, ciSCGW_MAX_PLAYER_POSITIONS_X + 2) * ciSCGW_VEHICLE_GRID_SPEED
			IF iTime > SCGW_ServerBD.sVehicles[i].iVehicleTime + iVehicleMoveTime
				RELOOP
			ENDIF
			
			bVehicleOnScreen = TRUE
			
			FLOAT fAlpha = TO_FLOAT(iTime - SCGW_ServerBD.sVehicles[i].iVehicleTime) /  TO_FLOAT(iVehicleMoveTime)

			SCGW_GET_POSITION_COORDS(SCGW_ServerBD.sVehicles[i].iVehicleOrigin, iStartX, iStartY)
			
			SWITCH SCGW_ServerBD.sVehicles[i].iVehicleDirection
			
				CASE ciSCGW_GRID_DIRECTION_UP
					iEndX = iStartX
					iEndY = 0
					vCollisionOffset = INIT_VECTOR_2D(0, -(sSCGWData.sVehicleData[SCGW_ServerBD.sVehicles[i].iVehicleType].vSpriteScale.y / 2))
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_DOWN
					iEndX = iStartX
					iEndY = ciSCGW_MAX_PLAYER_POSITIONS_Y - 1
					vCollisionOffset = INIT_VECTOR_2D(0, (sSCGWData.sVehicleData[SCGW_ServerBD.sVehicles[i].iVehicleType].vSpriteScale.y / 2))
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_LEFT
					iEndX = 0
					iEndY = iStartY
					vCollisionOffset = INIT_VECTOR_2D(-(sSCGWData.sVehicleData[SCGW_ServerBD.sVehicles[i].iVehicleType].vSpriteScale.x / 2), 0)
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_RIGHT
					iEndX = ciSCGW_MAX_PLAYER_POSITIONS_X - 1
					iEndY = iStartY
					vCollisionOffset = INIT_VECTOR_2D((sSCGWData.sVehicleData[SCGW_ServerBD.sVehicles[i].iVehicleType].vSpriteScale.x / 2), 0)
				BREAK
				
			ENDSWITCH
			
			VECTOR_2D vStartPosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_ServerBD.sVehicles[i].iVehicleOrigin)
			VECTOR_2D vEndPosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_GET_POSITION_FROM_COORDS(iEndX, iEndY))
			
			SWITCH SCGW_ServerBD.sVehicles[i].iVehicleDirection
			
				CASE ciSCGW_GRID_DIRECTION_UP
					vStartPosition.y += cfSCGW_GRID_POS_Y_INCR
					vEndPosition.y += -cfSCGW_GRID_POS_Y_INCR
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_DOWN
					vStartPosition.y += -cfSCGW_GRID_POS_Y_INCR
					vEndPosition.y += cfSCGW_GRID_POS_Y_INCR
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_LEFT
					vStartPosition.x += cfSCGW_GRID_POS_X_INCR
					vEndPosition.x += -cfSCGW_GRID_POS_X_INCR
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_RIGHT
					vStartPosition.x += -cfSCGW_GRID_POS_X_INCR
					vEndPosition.x += cfSCGW_GRID_POS_X_INCR
				BREAK
				
			ENDSWITCH
			
			sSCGWData.sVehicles[i].vLastFrameVehiclePosition = sSCGWData.sVehicles[i].vActualVehiclePosition
			sSCGWData.sVehicles[i].vActualVehiclePosition = LERP_VECTOR_2D(vStartPosition, vEndPosition, fAlpha)
			
			IF NOT SCGW_IS_RECT_COMPLETELY_OUTSIDE_OF_GAME_AREA(sSCGWData.sVehicles[i].vActualVehiclePosition, sSCGWData.sVehicleData[SCGW_ServerBD.sVehicles[i].iVehicleType].vSpriteScale)
				IF SCGW_IS_RECT_COMPLETELY_OUTSIDE_OF_GAME_AREA(sSCGWData.sVehicles[i].vLastFrameVehiclePosition, sSCGWData.sVehicleData[SCGW_ServerBD.sVehicles[i].iVehicleType].vSpriteScale)
					IF SCGW_ServerBD.sVehicles[i].iVehicleType != ciSCGW_VEHICLE_TYPE_ICE_CREAM
						SCGW_PLAY_FIRE_AND_FORGET_GLOBAL_SOUND(SCGW_GET_VEHICLE_WARNING_SOUND(SCGW_ServerBD.sVehicles[i].iVehicleType), sSCGWData.sVehicles[i].vActualVehiclePosition)
					ENDIF
				ELSE
					IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_vehicle_drivethrough_scene")
						START_AUDIO_SCENE("dlc_vw_am_tw_vehicle_drivethrough_scene")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_gameplay_scene")
						STOP_AUDIO_SCENE("dlc_vw_am_tw_in_gameplay_scene")
					ENDIF
					IF i != 2 //Don't play sound for 2nd gang vehicle
						VECTOR_2D vVehPos = sSCGWData.sVehicles[i].vActualVehiclePosition
						IF i = 1
						AND SCGW_ServerBD.sVehicles[i].iVehicleDirection != SCGW_ServerBD.sVehicles[2].iVehicleDirection
							vVehPos = INIT_VECTOR_2D(cfSCREEN_CENTER, cfSCREEN_CENTER)
						ENDIF
						IF SCGW_ServerBD.sVehicles[i].iVehicleType >= ciSCGW_AMBIENT_VEHICLE_TYPES
							INT iGang = SCGW_ServerBD.sVehicles[i].iVehicleType - ciSCGW_AMBIENT_VEHICLE_TYPES 
							SCGW_START_LOOPING_GANG_SOUND(SCGW_GET_VEHICLE_LOOP_SOUND(SCGW_ServerBD.sVehicles[i].iVehicleType), sSCGWData.sVehicles[i].iVehicleLoopSoundId, vVehPos, iGang)
						ELSE
							SCGW_START_LOOPING_GLOBAL_SOUND(SCGW_GET_VEHICLE_LOOP_SOUND(SCGW_ServerBD.sVehicles[i].iVehicleType), sSCGWData.sVehicles[i].iVehicleLoopSoundId, vVehPos)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF sSCGWData.sVehicles[i].iVehicleLoopSoundId != -1
					SCGW_STOP_LOOPING_SOUND(sSCGWData.sVehicles[i].iVehicleLoopSoundId)
				ENDIF
			ENDIF
			
			//Set up vehicle collision lines
			vVehicleCollision0 = ADD_VECTOR_2D(sSCGWData.sVehicles[i].vActualVehiclePosition, vCollisionOffset)
			vVehicleCollision1 = SUBTRACT_VECTOR_2D(sSCGWData.sVehicles[i].vLastFrameVehiclePosition, vCollisionOffset)
			
			//Collision
			INT iCollisionPlayerLoop = 0
			FOR iCollisionPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
				
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCollisionPlayerLoop))
					RELOOP
				ENDIF
				
				IF SCGW_PlayerBD[iCollisionPlayerLoop].iGang = ciSCGW_GANG_NONE
					RELOOP
				ENDIF
				
				IF SCGW_IS_PLAYER_DEAD(iCollisionPlayerLoop)
					RELOOP
				ENDIF
				
				vPlayerCollision0 = sSCGWData.sPlayers[iCollisionPlayerLoop].vActualPlayerPosition
				vPlayerCollision1 = sSCGWData.sPlayers[iCollisionPlayerLoop].vLastFramePlayerPosition
				
				IF ABSF(vPlayerCollision0.x - vVehicleCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X
				OR ABSF(vPlayerCollision0.y - vVehicleCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y
					RELOOP
				ENDIF
				
				IF SCGW_DO_LINES_INTERSECT_FOR_COLLISION(vVehicleCollision0, vVehicleCollision1, vPlayerCollision0, vPlayerCollision1)	
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_VEHICLE_POSITIONS - Player ", iCollisionPlayerLoop, " hit by ", i)
					SCGW_PROCESS_PLAYER_HIT_BY_VEHICLE(iCollisionPlayerLoop)
				ENDIF
				
			ENDFOR
			INT iBotLoop = 0
			FOR iBotLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
				
				IF NOT SCGW_Bot[iBotLoop].bActive
					RELOOP
				ENDIF
				
				IF SCGW_Bot[iBotLoop].SCGW_player.iGang = ciSCGW_GANG_NONE
					RELOOP
				ENDIF
				
				IF SCGW_IS_PLAYER_DEAD_BOT(iBotLoop)
					RELOOP
				ENDIF
				
				vPlayerCollision0 = sSCGWData.sPlayers[iBotLoop].vActualPlayerPosition
				vPlayerCollision1 = sSCGWData.sPlayers[iBotLoop].vLastFramePlayerPosition
				
				IF ABSF(vPlayerCollision0.x - vVehicleCollision0.x) > cfSCGW_GRID_SPRITE_SCALE_X
				OR ABSF(vPlayerCollision0.y - vVehicleCollision0.y) > cfSCGW_GRID_SPRITE_SCALE_Y
					RELOOP
				ENDIF
				
				IF SCGW_DO_LINES_INTERSECT_FOR_COLLISION(vVehicleCollision0, vVehicleCollision1, vPlayerCollision0, vPlayerCollision1)	
					CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_VEHICLE_POSITIONS - Player ", iCollisionPlayerLoop, " hit by ", i)
					SCGW_PROCESS_PLAYER_HIT_BY_VEHICLE_BOT(iBotLoop)
				ENDIF
				
			ENDFOR

		ELSE
			IF sSCGWData.sVehicles[i].iVehicleLoopSoundId != -1
				SCGW_STOP_LOOPING_SOUND(sSCGWData.sVehicles[i].iVehicleLoopSoundId)
			ENDIF
		
			sSCGWData.sVehicles[i].vActualVehiclePosition = SCGW_GET_SCREEN_COORDS_FROM_PLAYER_GRID_POSITION(SCGW_ServerBD.sVehicles[i].iVehicleOrigin)
			
			SWITCH SCGW_ServerBD.sVehicles[i].iVehicleDirection
			
				CASE ciSCGW_GRID_DIRECTION_UP
					sSCGWData.sVehicles[i].vActualVehiclePosition.y += cfSCGW_GRID_POS_Y_INCR
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_DOWN
					sSCGWData.sVehicles[i].vActualVehiclePosition.y += -cfSCGW_GRID_POS_Y_INCR
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_LEFT
					sSCGWData.sVehicles[i].vActualVehiclePosition.x += cfSCGW_GRID_POS_X_INCR
				BREAK
				
				CASE ciSCGW_GRID_DIRECTION_RIGHT
					sSCGWData.sVehicles[i].vActualVehiclePosition.x += -cfSCGW_GRID_POS_X_INCR
				BREAK
				
			ENDSWITCH
			
			sSCGWData.sVehicles[i].vLastFrameVehiclePosition = sSCGWData.sVehicles[i].vActualVehiclePosition
		ENDIF
	ENDFOR
	
	IF NOT bVehicleOnScreen
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_gameplay_scene")
			START_AUDIO_SCENE("dlc_vw_am_tw_in_gameplay_scene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_vehicle_drivethrough_scene")
			STOP_AUDIO_SCENE("dlc_vw_am_tw_vehicle_drivethrough_scene")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Spawns gang backup vehicles for the gang near their position
///    HOST ONLY
PROC SCGW_SPAWN_GANG_BACKUP_VEHICLES(INT iGang, INT iPosition)

	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	INT i, iPlayerX, iPlayerY
	INT iDirections[ciSCGW_MAX_GANG_BACKUP_VEHICLES]
	INT iOrigins[ciSCGW_MAX_GANG_BACKUP_VEHICLES]
	INT iIncrement = 1
	SCGW_GET_POSITION_COORDS(iPosition, iPlayerX, iPlayerY)
	
	IF (GET_RANDOM_BOOL())
		//Vertical Vehicles
		IF iPlayerX % 2 = 0
			//Need to move 2 grid spaces over so the cars go either side of the buildings
			iIncrement = 2
		ENDIF
		
		FOR i = 0 TO ciSCGW_MAX_GANG_BACKUP_VEHICLES - 1
			BOOL bUp = GET_RANDOM_BOOL()
			iDirections[i] = PICK_INT(bUp, ciSCGW_GRID_DIRECTION_UP, ciSCGW_GRID_DIRECTION_DOWN)
			iOrigins[i] = SCGW_GET_POSITION_FROM_COORDS(iPlayerX + iIncrement, PICK_INT(bUp, ciSCGW_MAX_PLAYER_POSITIONS_Y - 1, 0))
			
			//Make increment negative for next vehicle
			iIncrement *= -1
		ENDFOR
	ELSE
		//Horizontal Vehicles
		IF iPlayerY % 2 = 0
			//Need to move 2 grid spaces over so the cars go either side of the buildings
			iIncrement = 2
		ENDIF
		
		FOR i = 0 TO ciSCGW_MAX_GANG_BACKUP_VEHICLES - 1
			BOOL bLeft = GET_RANDOM_BOOL()
			iDirections[i] = PICK_INT(bLeft, ciSCGW_GRID_DIRECTION_LEFT, ciSCGW_GRID_DIRECTION_RIGHT)
			iOrigins[i] = SCGW_GET_POSITION_FROM_COORDS(PICK_INT(bLeft, ciSCGW_MAX_PLAYER_POSITIONS_X - 1, 0), iPlayerY + iIncrement)
			
			//Make increment negative for next vehicle
			iIncrement *= -1
		ENDFOR
	ENDIF
	
	INT iVehicleOffset = (ciSCGW_MAX_ACTIVE_VEHICLES - ciSCGW_MAX_GANG_BACKUP_VEHICLES)
	FOR i = 0 TO ciSCGW_MAX_GANG_BACKUP_VEHICLES - 1
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SPAWN_GANG_BACKUP_VEHICLES - Spawning backup vehicle ", i, " for gang: ", iGang, " origin: ", iOrigins[i], " direction: ", iDirections[i])
		SCGW_ServerBD.sVehicles[i + iVehicleOffset].iVehicleOrigin = iOrigins[i]
		SCGW_ServerBD.sVehicles[i + iVehicleOffset].iVehicleTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + ciSCGW_VEHICLE_DELAY
		SCGW_ServerBD.sVehicles[i + iVehicleOffset].iVehicleDirection = iDirections[i]
		SCGW_ServerBD.sVehicles[i + iVehicleOffset].iVehicleType = ciSCGW_AMBIENT_VEHICLE_TYPES + iGang
	ENDFOR

	SCGW_ServerBD.iLastVehicleSpawnTime = NATIVE_TO_INT(GET_NETWORK_TIME())

ENDPROC

/// PURPOSE:
///    Processes the spawning of vehicles
///    HOST ONLY
///    TODO: Check for potential collisions
PROC SCGW_PROCESS_SERVER_VEHICLES()
	
	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	IF (NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_ServerBD.iLastVehicleSpawnTime + ciSCGW_VEHICLE_INTERVAL)
		EXIT
	ENDIF
		
	//Set on attempt to spawn so we don't spam this method if spawning is blocked somehow
	SCGW_ServerBD.iLastVehicleSpawnTime = NATIVE_TO_INT(GET_NETWORK_TIME())
		
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_SERVER_VEHICLES - Attempting to spawn vehicle")
		
	INT iVehicleX
	INT iVehicleY
	INT iDirection
	
	IF (GET_RANDOM_BOOL())
		//Vertical
		BOOL bUp = GET_RANDOM_BOOL()
		iVehicleX = GET_RANDOM_INT_IN_RANGE(0, ciSCGW_MAX_PLAYER_POSITIONS_X)
		IF iVehicleX % 2 != 0
			iVehicleX++
			IF iVehicleX >= ciSCGW_MAX_PLAYER_POSITIONS_X
				iVehicleX = 0
			ENDIF
		ENDIF
		iVehicleY = PICK_INT(bUp, ciSCGW_MAX_PLAYER_POSITIONS_Y - 1, 0) 
		iDirection = PICK_INT(bUp, ciSCGW_GRID_DIRECTION_UP, ciSCGW_GRID_DIRECTION_DOWN)
	ELSE
		//Horizontal
		BOOL bLeft = GET_RANDOM_BOOL()
		iVehicleX = PICK_INT(bLeft, ciSCGW_MAX_PLAYER_POSITIONS_X - 1, 0) 
		iVehicleY = GET_RANDOM_INT_IN_RANGE(0, ciSCGW_MAX_PLAYER_POSITIONS_Y)
		IF iVehicleY % 2 != 0
			iVehicleY++
			IF iVehicleY >= ciSCGW_MAX_PLAYER_POSITIONS_Y
				iVehicleY = 0
			ENDIF
		ENDIF
		iDirection = PICK_INT(bLeft, ciSCGW_GRID_DIRECTION_LEFT, ciSCGW_GRID_DIRECTION_RIGHT)
	ENDIF
		
	SCGW_ServerBD.sVehicles[0].iVehicleTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + ciSCGW_VEHICLE_DELAY
	SCGW_ServerBD.sVehicles[0].iVehicleOrigin = SCGW_GET_POSITION_FROM_COORDS(iVehicleX, iVehicleY)
	SCGW_ServerBD.sVehicles[0].iVehicleDirection = iDirection
	SCGW_ServerBD.sVehicles[0].iVehicleType = GET_RANDOM_INT_IN_RANGE(0, ciSCGW_AMBIENT_VEHICLE_TYPES)
		
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_SERVER_VEHICLES - spawning vehicle of type ", SCGW_ServerBD.sVehicles[0].iVehicleType, " at (", iVehicleX,", ", iVehicleY, ")")
	
ENDPROC


// ------------------------------ INITIALIZATION ------------------------------

/// PURPOSE:
///    Initializes a grid cell to be owned by a gang, including trails
/// PARAMS:
///    iGang - 
PROC SCGW_SET_GRID_OWNED_BY_GANG(INT iGang, INT iX, INT iY, BOOL bForIntro = FALSE)

	INT iGridCenter = SCGW_GET_BUILDING_CENTER(iX, iY)

	//Top Row - Horizontal
	IF NOT (bForIntro AND iGang = ciSCGW_GANG_BLUE)
		SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X - 1)
	ENDIF
	IF NOT (bForIntro AND iGang = ciSCGW_GANG_YELLOW)
		SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X)
	ENDIF
	//Top Row - Vertical
	SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X - 1)
	SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter - ciSCGW_MAX_PLAYER_POSITIONS_X + 1)
	//Middle Row - Vertical
	SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter - 1)
	SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_DOWN], iGridCenter + 1)
	//Bottom Row - Horizontal
	IF NOT (bForIntro AND iGang = ciSCGW_GANG_PURPLE)
		SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter + ciSCGW_MAX_PLAYER_POSITIONS_X - 1)
	ENDIF
	IF NOT (bForIntro AND iGang = ciSCGW_GANG_GREEN)
		SET_LONG_BIT(SCGW_ServerBD.iPositionOwnershipBS[iGang][ciSCGW_OWNERSHIP_LINK_RIGHT], iGridCenter + ciSCGW_MAX_PLAYER_POSITIONS_X)
	ENDIF
	//Ownership
	SCGW_ServerBD.iGridGangOwnership[iX][iY] = iGang

ENDPROC

/// PURPOSE:
///    Returns the X grid coord for the gang's start location
FUNC INT SCGW_GET_STARTING_GRID_FOR_GANG_X(INT iGang) 
	
	SWITCH iGang
		CASE ciSCGW_GANG_GREEN
			RETURN 0
		CASE ciSCGW_GANG_PURPLE
			RETURN ciSCGW_MAX_GRID_POSITIONS_X - 1
		CASE ciSCGW_GANG_YELLOW
			RETURN 0
		CASE ciSCGW_GANG_BLUE
			RETURN ciSCGW_MAX_GRID_POSITIONS_X - 1
	ENDSWITCH
	
	RETURN -1
	
ENDFUNC

/// PURPOSE:
///    Returns the Y grid coord for the gang's start location
FUNC INT SCGW_GET_STARTING_GRID_FOR_GANG_Y(INT iGang)

	SWITCH iGang
		CASE ciSCGW_GANG_GREEN
			RETURN 0
		CASE ciSCGW_GANG_PURPLE
			RETURN 0
		CASE ciSCGW_GANG_YELLOW
			RETURN ciSCGW_MAX_GRID_POSITIONS_Y - 1
		CASE ciSCGW_GANG_BLUE
			RETURN ciSCGW_MAX_GRID_POSITIONS_Y - 1
	ENDSWITCH
	
	RETURN -1

ENDFUNC

/// PURPOSE:
///    Resets all required server data for a new round
///    HOST ONLY
PROC INITIALIZE_SERVER_DATA_FOR_NEW_ROUND()

	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	INT i, j, k
	
	SCGW_ServerBD.bSuddenDeath = FALSE
	SCGW_ServerBD.bAllDeadSuddenDeath = FALSE
	
	//Gang Ownership
	FOR i = 0 TO ciSCGW_MAX_GRID_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y - 1
			SCGW_ServerBD.iGridGangOwnership[i][j] = ciSCGW_GANG_NONE
		ENDFOR
	ENDFOR
	
	//Trails & Score
	FOR i = 0 TO ciSCGW_GANG_MAX - 1
		SCGW_UF_ServerBD.iCurrentScore[i] = 0
		FOR j = 0 TO ciSCGW_OWNERSHIP_LINK_MAX - 1
			FOR k = 0 TO ciSCGW_GRIDBS_LENGTH - 1
				SCGW_ServerBD.iPositionOwnershipBS[i][j][k] = 0
			ENDFOR
		ENDFOR
	ENDFOR
	
	//Starting Territories
	FOR i = 0 TO ciSCGW_GANG_MAX - 1
		IF sSCGWData.iGangPlayers[i] = -1
			RELOOP
		ENDIF
		
		SCGW_SET_GRID_OWNED_BY_GANG(i, SCGW_GET_STARTING_GRID_FOR_GANG_X(i), SCGW_GET_STARTING_GRID_FOR_GANG_Y(i), TRUE)
	ENDFOR
	
	//Pickups
	FOR i = 0 TO ciSCGW_MAX_SPAWNED_PICKUPS - 1
		SCGW_ServerBD.sPickups[i].iSpawnedPickupLocation = -1
		SCGW_ServerBD.sPickups[i].iSpawnedPickupType = ciSCGW_PICKUPS_NONE
		SCGW_ServerBD.sPickups[i].iSpawnedPickupSpawnTime = -HIGHEST_INT
	ENDFOR
	
	//Vehicles
	FOR i = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
		SCGW_ServerBD.sVehicles[i].iVehicleTime = -HIGHEST_INT
		SCGW_ServerBD.sVehicles[i].iVehicleOrigin = -1
		SCGW_ServerBD.sVehicles[i].iVehicleDirection = -1
		SCGW_ServerBD.sVehicles[i].iVehicleType = -1
	ENDFOR
	
	//Round up to nearest second for nicer flashing <15 secs
	SCGW_ServerBD.iStartTime = CEIL(TO_FLOAT(NATIVE_TO_INT(GET_NETWORK_TIME()) + ciSCGW_START_TIME_OFFSET)/1000.0) * 1000
	
	//Run once to init scores
	SCGW_PROCESS_SERVER_BUILDING_OWNERSHIP()
	
ENDPROC

/// PURPOSE:
///    Resets all required client and local player broadcast data for a new round
PROC INITIALIZE_CLIENT_DATA_FOR_NEW_ROUND()

	SCGW_SET_LOCAL_PLAYER_START_POSITION()
	
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iHealth = ciSCGW_MAX_PLAYER_HEALTH
	SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_ALIVE)
	
	INT i,j
	FOR i = 0 TO ciSCGW_MAX_PLAYER_SHOTS - 1
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[i].iShotTime = -HIGHEST_INT
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[i].vShotOrigin.x = -1
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[i].vShotOrigin.y = -1
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[i].iShotDirection = -1
		SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].sProjectiles[i].iShotWeaponType = -1
		FOR j = 0 TO ciSCGW_MAX_PLAYERS - 1
			sSCGWData.sPlayers[j].sProjectiles[i].vLastFrameBulletPosition = INIT_VECTOR_2D(-1, -1)
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciSCGW_MAX_GRID_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y - 1
			sSCGWData.iGangOwnershipLastChanged[i][j] = -HIGHEST_INT
			sSCGWData.iPreviousGangOwnership[i][j] = -1
			sSCGWData.iLastFrameGangOwnership[i][j] = -1
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
		sSCGWData.sPlayers[i].iDisplayHealth = ciSCGW_MAX_PLAYER_HEALTH
	ENDFOR
	
	sSCGWData.iFlyoverTime = -HIGHEST_INT
	sSCGWData.vFlyoverPos = INIT_VECTOR_2D(0.0, 0.0)
	
	SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)
	SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
	CLEAR_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_DIED_THIS_ROUND)
	CLEAR_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_FIRST_MOVE_DONE)
	
	RESET_NET_TIMER(sSCGWData.tdSuddenDeathText)
	
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLastRespawnTime = -HIGHEST_INT
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].fMoveSpeed = cfSCGW_BASE_SPEED
	
	INT iUnpackedInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS]
	ARCADE_UNPACK_LEADERBOARD_INITIALS(SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardInitials, iUnpackedInitials)
	iUnpackedInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS - 1] = SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardInitials = ARCADE_PACK_LEADERBOARD_INITIALS(iUnpackedInitials)
	
	sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves = 0
	sSCGWData.iNextExplosionIndex = 0
	sSCGWData.iNextShotIndex = 0
	sSCGWData.iAudioBS = 0
	
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iEquippedWeaponType = ciSCGW_WEAPON_NONE
	
ENDPROC

PROC INITIALIZE_CLIENT_DATA_FOR_NEW_ROUND_BOT(INT iBot)

	SCGW_SET_LOCAL_PLAYER_START_POSITION_BOT(iBot)
	
	SCGW_Bot[iBot].SCGW_player.iHealth = ciSCGW_MAX_PLAYER_HEALTH
	SCGW_SET_PLAYER_STATE_BOT(iBot,SCGW_PLAYER_STATE_ALIVE)
	
	INT i,j
	FOR i = 0 TO ciSCGW_MAX_PLAYER_SHOTS - 1
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[i].iShotTime = -HIGHEST_INT
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[i].vShotOrigin.x = -1
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[i].vShotOrigin.y = -1
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[i].iShotDirection = -1
		SCGW_Bot[iBot].SCGW_UF_Player.sProjectiles[i].iShotWeaponType = -1
		FOR j = 0 TO ciSCGW_MAX_PLAYERS - 1
			sSCGWData.sPlayers[j].sProjectiles[i].vLastFrameBulletPosition = INIT_VECTOR_2D(-1, -1)
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciSCGW_MAX_GRID_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y - 1
			sSCGWData.iGangOwnershipLastChanged[i][j] = -HIGHEST_INT
			sSCGWData.iPreviousGangOwnership[i][j] = -1
			sSCGWData.iLastFrameGangOwnership[i][j] = -1
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
		sSCGWData.sPlayers[i].iDisplayHealth = ciSCGW_MAX_PLAYER_HEALTH
	ENDFOR
	
	sSCGWData.iFlyoverTime = -HIGHEST_INT
	sSCGWData.vFlyoverPos = INIT_VECTOR_2D(0.0, 0.0)
	
	SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED)
	SET_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_CURRENT_MOVE_PROCESSED_HALF)
	CLEAR_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_DIED_THIS_ROUND)
	CLEAR_BIT(sSCGWData.iBotBS[iBot], ciSCGW_LOCALBS_FIRST_MOVE_DONE)
	
	RESET_NET_TIMER(sSCGWData.tdSuddenDeathText)
	
	SCGW_Bot[iBot].SCGW_player.iLastRespawnTime = -HIGHEST_INT
	SCGW_Bot[iBot].SCGW_player.fMoveSpeed = cfSCGW_BASE_SPEED
		
	sSCGWData.sLocalPlayer.iSpeedBoostRemainingMoves = 0
	sSCGWData.iNextExplosionIndex = 0
	sSCGWData.iNextShotIndex = 0
	sSCGWData.iAudioBS = 0
	
	SCGW_Bot[iBot].SCGW_player.iEquippedWeaponType = ciSCGW_WEAPON_NONE
	
ENDPROC

/// PURPOSE:
///    Initializes the tile layout for the entire game
///    HOST ONLY
PROC SCGW_INITIALIZE_MAP_TILES()

	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	INT i, j 
	INT iTiles[ciSCGW_MAX_GRID_SPRITES]
	FOR i = 0 TO ciSCGW_MAX_GRID_SPRITES - 1
		iTiles[i] = i
	ENDFOR
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	FOR i = 0 TO ciSCGW_MAX_GRID_SPRITES - 1
		j = GET_RANDOM_INT_IN_RANGE(0, ciSCGW_MAX_GRID_SPRITES)
		INT iSwap = iTiles[i]
		iTiles[i] = iTiles[j]
		iTiles[j] = iSwap
	ENDFOR
	
	INT iTileIndex = 0
	FOR i = 0 TO ciSCGW_MAX_GRID_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y - 1
			SCGW_ServerBD.iGridSpriteIndexes[i][j] = iTiles[iTileIndex]
			iTileIndex++
		ENDFOR
	ENDFOR
	
ENDPROC

FUNC BOOL SCGW_IS_ANY_BOT_ACTIVE()
	INT iBot
	FOR iBot = 0 TO ciSCGW_GANG_MAX - 1
		IF SCGW_Bot[iBot].bActive
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC


// ------------------------------ CLIENT STATE MACHINE ------------------------------

/// PURPOSE:
///    Updates the client state
PROC SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE eNewState)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_CLIENT_STATE - State changed from ", 
		SCGW_DEBUG_GET_CLIENT_STATE_AS_STRING(sSCGWData.eCurrentState), " to ", SCGW_DEBUG_GET_CLIENT_STATE_AS_STRING(eNewState))
	
	sSCGWData.eCurrentState = eNewState
	
ENDPROC

FUNC INT GET_TELEMETRY_LOCATION()
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()	
		SWITCH GET_SIMPLE_INTERIOR_TYPE_LOCAL_PLAYER_IS_IN()
			CASE SIMPLE_INTERIOR_TYPE_ARCADE
				RETURN HASH("SIMPLE_INTERIOR_TYPE_ARCADE")
			BREAK
			CASE SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT
				RETURN HASH("SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT")
			BREAK
			
			DEFAULT
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] [GET_TELEMETRY_LOCATION] not in valid location, setting sArcadePlayStats.location = 0")
				RETURN 0
			BREAK
		ENDSWITCH
	ELSE
		CDEBUG2LN(DEBUG_MINIGAME, "[SCGW] [JS] [GET_TELEMETRY_LOCATION] Invalid Player ID, setting sArcadePlayStats.location = 0")
		RETURN 0
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Final cleanup procedure: removes all assets and resets player state.
PROC SCGW_CLEANUP_AND_EXIT_CLIENT()
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] - SCGW_CLEANUP_AND_EXIT_CLIENT - Cleaning up and terminating the script")
	
	IF SCGW_IS_ANY_BOT_ACTIVE()
		sArcadePlayStats.numPlayers = 1
	ENDIF
	
	sArcadePlayStats.location = GET_TELEMETRY_LOCATION()
	ARCADE_SEND_PLAY_STATS()
	
	ARCADE_CABINET_COMMON_CLEANUP()
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGrid")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGridTiles")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGridPlayerBlue")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGridPlayerGreen")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGridPlayerPurple")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGridPlayerYellow")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeCabinetGridHUD")
	
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT,TRUE)
	
	IF (sSCGWData.binkIntro != NULL)
		RELEASE_BINK_MOVIE(sSCGWData.binkIntro)
	ENDIF
	
	//Audio
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_menus_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_in_menus_scene")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_gameplay_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_in_gameplay_scene")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_vehicle_drivethrough_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_vehicle_drivethrough_scene")
	ENDIF
	SCGW_TRIGGER_MUSIC_EVENT(SCGW_GET_MUSIC_EVENT_STRING(SCGW_MUSIC_EVENT_STOP_MUSIC))
	INT i
	FOR i = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
		IF sSCGWData.sVehicles[i].iVehicleLoopSoundId >= 0
			STOP_SOUND(sSCGWData.sVehicles[i].iVehicleLoopSoundId)
			RELEASE_SOUND_ID(sSCGWData.sVehicles[i].iVehicleLoopSoundId)
		ENDIF
		IF sSCGWData.sVehicles[i].iVehicleWarningSoundId >= 0
			STOP_SOUND(sSCGWData.sVehicles[i].iVehicleWarningSoundId)
			RELEASE_SOUND_ID(sSCGWData.sVehicles[i].iVehicleWarningSoundId)
		ENDIF
	ENDFOR
	FOR i = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF sSCGWData.sPlayers[i].iWalkingSoundId >= 0
			STOP_SOUND(sSCGWData.sPlayers[i].iWalkingSoundId)
			RELEASE_SOUND_ID(sSCGWData.sPlayers[i].iWalkingSoundId)
		ENDIF
		IF sSCGWData.sPlayers[i].iPlayerSoundId >= 0
			STOP_SOUND(sSCGWData.sPlayers[i].iPlayerSoundId)
			RELEASE_SOUND_ID(sSCGWData.sPlayers[i].iPlayerSoundId)
		ENDIF
		IF sSCGWData.sPlayers[i].iShotSoundId >= 0
			STOP_SOUND(sSCGWData.sPlayers[i].iShotSoundId)
			RELEASE_SOUND_ID(sSCGWData.sPlayers[i].iShotSoundId)
		ENDIF
	ENDFOR
	IF sSCGWData.sLocalPlayer.iDeathSoundId >= 0
		STOP_SOUND(sSCGWData.sLocalPlayer.iDeathSoundId)
		RELEASE_SOUND_ID(sSCGWData.sLocalPlayer.iDeathSoundId)
	ENDIF
	IF sSCGWData.iAmbienceSoundId >= 0
		STOP_SOUND(sSCGWData.iAmbienceSoundId)
		RELEASE_SOUND_ID(sSCGWData.iAmbienceSoundId)
	ENDIF
	
	//Don't release unless launched from debug?
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/DLC_VW_AM_TW")
	
	SCGW_CLEAR_HELP_TEXT()
	
	//Set the state so calling the main update function does nothing.
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_FINISHED)
	
	g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_NOT_PLAYING
	g_sCasinoArcadeStreetCrimeVars.eThisFrameAction = CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_NONE
	
	SET_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(FALSE)

	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

//~~~~~ Client State Machine State Updates ~~~~~

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_INITIALISING
PROC SCGW_PROCESS_CLIENT_STATE_INITIALISING()
	
	IF SCGW_ServerBD.eCurrentServerState > SCGW_SERVER_STATE_LAUNCHING
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_INITIALISING - JOINED LATE, BAILING")
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			sSCGWData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			sSCGWData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			PLAY_ARCADE_CABINET_ANIMATION(sSCGWData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		ENDIF	
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_CLEANUP)
		EXIT
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang = ciSCGW_GANG_NONE
		SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang = 0
		sSCGWData.sDebug.bDebugOnePlayerTest = TRUE
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_INITIALISING - GAME LAUNCHED WITH ONE PLAYER FROM M MENU")
	ENDIF
#ENDIF

	ARCADE_CABINET_COMMON_INITIALIZATION()
	
	ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_STREET_CRIMES)
	SCGW_INITIALIZE_POSITION_DATA_STRUCT()
	SCGW_INITIALIZE_COLOURS()
	SCGW_INITIALIZE_GANGS()
	SCGW_INITIALIZE_VEHICLE_TYPES()
	SCGW_INITIALIZE_PICKUP_TYPES()
	SCGW_INITIALIZE_WEAPON_TYPES()
	
	INT i, j
	FOR i = 0 TO ciSCGW_MAX_GRID_POSITIONS_X - 1
		FOR j = 0 TO ciSCGW_MAX_GRID_POSITIONS_Y - 1
			sSCGWData.iGangOwnershipLastChanged[i][j] = -HIGHEST_INT
			sSCGWData.iPreviousGangOwnership[i][j] = -1
			sSCGWData.iLastFrameGangOwnership[i][j] = -1
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciSCGW_GANG_MAX - 1
		sSCGWData.iGangPlayerJoinTimes[i] = -HIGHEST_INT
	ENDFOR
	
	sSCGWData.binkIntro = SET_BINK_MOVIE("DegenSting_StreetCrime_1920x1080_auto")
	
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_REQUESTING_ASSETS)

ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_REQUESTING_ASSETS
PROC SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS()
	
	
	IF NOT ARCADE_GAMES_POSTFX_REQUESTING_ASSETS()
		CDEBUG1LN(DEBUG_MINIGAME, "[IAP] [JS] IAP_HAVE_ALL_ASSETS_LOADED - Loading PostFX")
		EXIT
	ENDIF
	
	//Textures
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGrid")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGrid")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGrid")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGridPlayerYellow")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGridPlayerYellow")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGridPlayerYellow")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGridPlayerPurple")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGridPlayerPurple")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGridPlayerPurple")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGridPlayerGreen")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGridPlayerGreen")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGridPlayerGreen")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGridPlayerBlue")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGridPlayerBlue")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGridPlayerBlue")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGridTiles")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGridTiles")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGridTiles")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPArcadeCabinetGridHUD")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeCabinetGridHUD")
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPArcadeCabinetGridHUD")
		EXIT
	ENDIF
	
	//Text
	REQUEST_ADDITIONAL_TEXT("SCGW", MINIGAME_TEXT_SLOT)
	IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("SCGW", MINIGAME_TEXT_SLOT)
		PRINTLN("[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading Text")
		EXIT
	ENDIF
	
	//Audio
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/DLC_VW_AM_TW")
		CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading Audio Bank DLC_VINEWOOD/DLC_VW_AM_TW")
		EXIT
	ENDIF
		
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_INTRO_VIDEO)
	PLAY_BINK_MOVIE(sSCGWData.binkIntro)
	SET_BINK_SHOULD_SKIP(sSCGWData.binkIntro, TRUE)
	SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Degenatron_Screen")
	
ENDPROC

PROC SCGW_PROCESS_CLIENT_STATE_INTRO_VIDEO()
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_menus_scene")
		START_AUDIO_SCENE("dlc_vw_am_tw_in_menus_scene")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_gameplay_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_in_gameplay_scene")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_vehicle_drivethrough_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_vehicle_drivethrough_scene")
	ENDIF
		
	SCGW_DRAW_BACKGROUND()
	
	DRAW_BINK_MOVIE(sSCGWData.binkIntro, cfSCREEN_CENTER, cfSCREEN_CENTER, 1.0 * fAspectRatioModifier, 1.0, 0.0, 255, 255, 255, 255)
	
	IF GET_BINK_MOVIE_TIME(sSCGWData.binkIntro) >= 30.0
	AND NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_TITLE_MUSIC_STARTED)
		SET_BIT(sSCGWData.iLocalBS, ciSCGW_LOCALBS_TITLE_MUSIC_STARTED)
		SCGW_TRIGGER_MUSIC_EVENT(SCGW_GET_MUSIC_EVENT_STRING(SCGW_MUSIC_EVENT_TITLE_START))
	ENDIF	
	
	IF GET_BINK_MOVIE_TIME(sSCGWData.binkIntro) >= 99.0
#IF IS_DEBUG_BUILD
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "Skip Intro")
#ENDIF
		STOP_BINK_MOVIE(sSCGWData.binkIntro)
		RELEASE_BINK_MOVIE(sSCGWData.binkIntro)
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_GANG_SCREEN)
		sSCGWData.iGangPlayerJoinTimes[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang] = NATIVE_TO_INT(GET_NETWORK_TIME())
		SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_READY_UP)
	ENDIF
	
	ARCADE_GAMES_POSTFX_DRAW()	
	SCGW_DRAW_FACADE()
	
ENDPROC

FUNC INT SCGW_GET_AVAILABLE_GANG()
	INT iPlayerLoop
	INT iGang
	BOOL bAvailable
	FOR iGang = 0 TO ciSCGW_MAX_PLAYERS - 1
		bAvailable = TRUE
		FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			IF SCGW_PlayerBD[iPlayerLoop].iGang = iGang
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
				bAvailable = FALSE
			ENDIF
			IF SCGW_Bot[iPlayerLoop].SCGW_player.iGang = iGang
				bAvailable = FALSE
			ENDIF
		ENDFOR
		IF bAvailable
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_GET_AVAILABLE_GANG - Gang ",iGang," available")
			RETURN iGang
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC SCGW_PROCESS_CLIENT_STATE_GANG_SCREEN()
	
	SCGW_DRAW_GANG_SCREEN()
	
	CONTROL_ACTION CA_Start = INPUT_FRONTEND_PAUSE
	CONTROL_ACTION CA_Back = INPUT_SCRIPT_RRIGHT
	CONTROL_ACTION CA_JoinAI = INPUT_FRONTEND_RLEFT
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_Start = INPUT_FRONTEND_ACCEPT
		CA_Back = INPUT_FRONTEND_DELETE
	ENDIF
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME()) > sSCGWData.iReadyPressedTime + ciSCGW_READY_SPAM_DELAY
		IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState != SCGW_PLAYER_STATE_WAITING_TO_START
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Start)
				SCGW_CLEAR_HELP_TEXT()
				SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_READY_UP)
				SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_WAITING_TO_START)
				SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Ready")
				sSCGWData.iReadyPressedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			ENDIF
			IF sSCGWData.bCanAddBots
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_JoinAI)
					INT iPlayerLoop
					FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
						IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
						AND NOT SCGW_Bot[iPlayerLoop].bActive
//							IF SCGW_Bot[iPlayerLoop].SCGW_player.iGang = -1
								SCGW_Bot[iPlayerLoop].SCGW_player.iGang = SCGW_GET_AVAILABLE_GANG()
								SCGW_Bot[iPlayerLoop].bActive = TRUE
								SCGW_Bot[iPlayerLoop].SCGW_player.eCurrentPlayerState = SCGW_PLAYER_STATE_WAITING_TO_START
								BREAKLOOP
//							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		IF SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].eCurrentPlayerState = SCGW_PLAYER_STATE_WAITING_TO_START
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_Back)
				SCGW_CLEAR_HELP_TEXT()
				SCGW_SET_PLAYER_STATE(SCGW_PLAYER_STATE_INITIALISING)
				SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Unready")
				sSCGWData.iReadyPressedTime = NATIVE_TO_INT(GET_NETWORK_TIME())
			ENDIF
		ENDIF
	ENDIF
	
	SCGW_PROCESS_GANG_SCREEN_HELP_TEXT()
	
	IF SCGW_ServerBD.eCurrentServerState >= SCGW_SERVER_STATE_INIT_ROUND
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_LAUNCHING)
		SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Start_Game")
		ARCADE_INITIALISE_PLAY_STATS(TRUE)
	ENDIF
		
ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_LAUNCHING
PROC SCGW_PROCESS_CLIENT_STATE_LAUNCHING()
	
	SCGW_DRAW_GAME_STATIC()
	SCGW_PROCESS_HELP_TEXT()

	IF SCGW_ServerBD.eCurrentServerState <= SCGW_SERVER_STATE_LAUNCHING
		EXIT
	ENDIF
	
	sArcadePlayStats.numPlayers = sSCGWData.iConnectedPlayers
				
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_INIT_ROUND)
	
ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_INIT_ROUND
PROC SCGW_PROCESS_CLIENT_STATE_INIT_ROUND()

	SCGW_DRAW_GAME_STATIC()
	SCGW_PROCESS_HELP_TEXT()
	
	IF SCGW_ServerBD.eCurrentServerState <= SCGW_SERVER_STATE_INIT_ROUND
		EXIT
	ENDIF
	
	INITIALIZE_CLIENT_DATA_FOR_NEW_ROUND()
	INT iBot
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS-1
		IF iBot != sSCGWData.sLocalPlayer.iLocalPart
		AND SCGW_Bot[iBot].bActive
			INITIALIZE_CLIENT_DATA_FOR_NEW_ROUND_BOT(iBot)
		ENDIF
	ENDFOR

	SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_COUNTDOWN)
	
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_COUNTDOWN)
	
ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_COUNTDOWN
PROC SCGW_PROCESS_CLIENT_STATE_COUNTDOWN()

	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_menus_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_in_menus_scene")
	ENDIF
	IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_gameplay_scene")
		START_AUDIO_SCENE("dlc_vw_am_tw_in_gameplay_scene")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_vehicle_drivethrough_scene")
		STOP_AUDIO_SCENE("dlc_vw_am_tw_vehicle_drivethrough_scene")
	ENDIF
	
	SCGW_START_LOOPING_GLOBAL_SOUND("Ambience", sSCGWData.iAmbienceSoundId, INIT_VECTOR_2D(-1, -1), FALSE)

	SCGW_PROCESS_PLAYER_POSITIONS()
	SCGW_PROCESS_PLAYER_POSITIONS_BOT()
	SCGW_PROCESS_HELP_TEXT()
	
	SCGW_DRAW_COUNTDOWN_SCREEN()
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_ServerBD.iStartTime
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_MOVEMENT_HELP_SHOWN)
		SET_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_MOVEMENT_HELP_SHOWN)
		SCGW_TRIGGER_HELP_TEXT("SCGW_H_MOVE", ciSCGW_DEFAULT_HELP_TEXT_TIME)
	ENDIF	
		
	SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING)
	SCGW_TRIGGER_MUSIC_EVENT(SCGW_GET_MUSIC_EVENT_STRING(SCGW_MUSIC_EVENT_GAMEPLAY))
	
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_RUNNING)
	
ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_RUNNING
PROC SCGW_PROCESS_CLIENT_STATE_RUNNING()
	
	SCGW_START_LOOPING_GLOBAL_SOUND("Ambience", sSCGWData.iAmbienceSoundId, INIT_VECTOR_2D(-1, -1), FALSE)
	
	SCGW_PROCESS_INPUT()
	INT iBot
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS-1
		IF iBot != sSCGWData.sLocalPlayer.iLocalPart
		AND SCGW_Bot[iBot].bActive
			SCGW_PROCESS_INPUT_BOT(iBot)
		ENDIF
	ENDFOR
	
	SCGW_PROCESS_PLAYER_POSITIONS()
	SCGW_PROCESS_PLAYER_POSITIONS_BOT()
	SCGW_PROCESS_VEHICLE_POSITIONS()
	SCGW_PROCESS_SHOT_POSITIONS()
	SCGW_PROCESS_SHOT_POSITIONS_BOT()
	
	SCGW_PROCESS_FLYOVERS()
	
	SCGW_PROCESS_LOCAL_PLAYER_RESPAWNING()
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS-1
		IF iBot != sSCGWData.sLocalPlayer.iLocalPart
		AND SCGW_Bot[iBot].bActive
			SCGW_PROCESS_LOCAL_PLAYER_RESPAWNING_BOT(iBot)
		ENDIF
	ENDFOR
		
	SCGW_DRAW_GAME_RUNNING()
	
	SCGW_PROCESS_HELP_TEXT()
	
	SCGW_PROCESS_ZERO_TERRITORIES_SOUND_LOOP()
	
	IF sSCGWData.iLastFrameWinningPart != sSCGWData.iWinningPart
		IF sSCGWData.iLastFrameWinningPart = sSCGWData.sLocalPlayer.iLocalPart
			SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Player_Loses_Lead")
		ELIF sSCGWData.iWinningPart = sSCGWData.sLocalPlayer.iLocalPart
			SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Player_Takes_Lead")
		ENDIF
	ENDIF
	
	IF SCGW_ServerBD.bSuddenDeath
	AND NOT HAS_NET_TIMER_STARTED(sSCGWData.tdSuddenDeathText)
		SCGW_TRIGGER_MUSIC_EVENT(SCGW_GET_MUSIC_EVENT_STRING(SCGW_MUSIC_EVENT_SUDDEN_DEATH))
		REINIT_NET_TIMER(sSCGWData.tdSuddenDeathText)
	ENDIF
	
	IF (SCGW_ServerBD.eCurrentServerState <= SCGW_SERVER_STATE_RUNNING)
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO ciSCGW_MAX_ACTIVE_VEHICLES - 1
		IF sSCGWData.sVehicles[i].iVehicleLoopSoundId != -1
			SCGW_STOP_LOOPING_SOUND(sSCGWData.sVehicles[i].iVehicleLoopSoundId)
		ENDIF
	ENDFOR
	
	SCGW_CLEAR_HELP_TEXT()
	sArcadePlayStats.score += SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang]
	sSCGWData.iGameWinnerAnimFrame = 0
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardScore += SCGW_UF_ServerBD.iCurrentScore[SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang]
	SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_ROUND_END_SCREEN)
	SCGW_TRIGGER_MUSIC_EVENT(SCGW_GET_MUSIC_EVENT_STRING(SCGW_MUSIC_EVENT_MID_ROUND))
	
ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_ROUND_END_SCREEN
PROC SCGW_PROCESS_CLIENT_STATE_ROUND_END_SCREEN()

	BOOL bRoundEnd = SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND()
	
	IF bRoundEnd
		SCGW_DRAW_ROUND_END_SCREEN()
	ELSE
		SCGW_DRAW_GAME_END_SCREEN()
	ENDIF
	
	SCGW_PROCESS_HELP_TEXT()
	
	IF SCGW_ServerBD.eCurrentServerState >= SCGW_SERVER_STATE_LEADERBOARD
		SCGW_STOP_LOOPING_SOUND(sSCGWData.iAmbienceSoundId)
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_menus_scene")
			START_AUDIO_SCENE("dlc_vw_am_tw_in_menus_scene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_in_gameplay_scene")
			STOP_AUDIO_SCENE("dlc_vw_am_tw_in_gameplay_scene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_am_tw_vehicle_drivethrough_scene")
			STOP_AUDIO_SCENE("dlc_vw_am_tw_vehicle_drivethrough_scene")
		ENDIF
		SCGW_TRIGGER_MUSIC_EVENT(SCGW_GET_MUSIC_EVENT_STRING(SCGW_MUSIC_EVENT_TITLE_START))
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_LBD_ENTRY)
		EXIT
	ENDIF
	
	IF SCGW_ServerBD.eCurrentServerState = SCGW_SERVER_STATE_INIT_ROUND
	OR SCGW_ServerBD.eCurrentServerState = SCGW_SERVER_STATE_COUNTDOWN
		SCGW_STOP_LOOPING_SOUND(sSCGWData.iAmbienceSoundId)
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_INIT_ROUND)
		EXIT
	ENDIF
	
	SCGW_START_LOOPING_GLOBAL_SOUND("Ambience", sSCGWData.iAmbienceSoundId, INIT_VECTOR_2D(-1, -1), FALSE)
	
	IF g_sCasinoArcadeStreetCrimeVars.ePlayerState != CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING
		//Waiting for host to move state
		EXIT
	ENDIF
	
	IF bRoundEnd
		//Set global state to Won/Lost Round
		IF SCGW_ServerBD.iRoundWinner[SCGW_ServerBD.iCurrentRound - 1] != -1
			IF SCGW_ServerBD.iRoundWinner[SCGW_ServerBD.iCurrentRound - 1] = sSCGWData.sLocalPlayer.iLocalPart
				SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_ROUND_END_WON)
				SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Win", sSCGWData.sLocalPlayer.iLocalPart, sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].iPlayerSoundId)
			ELSE
				SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_ROUND_END_LOST)
				SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Lose_Game", sSCGWData.sLocalPlayer.iLocalPart, sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].iPlayerSoundId)
			ENDIF
			SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_WON_PLAYER_1) + SCGW_PlayerBD[SCGW_ServerBD.iRoundWinner[SCGW_ServerBD.iCurrentRound - 1]].iGang))
		ENDIF
	ELSE
		//Set global state to Won/Lost Game
		IF SCGW_ServerBD.iGameWinner != -1
			IF SCGW_ServerBD.iGameWinner = sSCGWData.sLocalPlayer.iLocalPart
				SCGW_UPDATE_PLAYER_WINS()
				SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_GAME_END_WON)
				SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Win", sSCGWData.sLocalPlayer.iLocalPart, sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].iPlayerSoundId)
			ELSE
				SCGW_SET_GLOBAL_PLAYER_STATE(CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_GAME_END_LOST)
				SCGW_PLAY_GANG_SOUND_FROM_PLAYER("Lose_Game", sSCGWData.sLocalPlayer.iLocalPart, sSCGWData.sPlayers[sSCGWData.sLocalPlayer.iLocalPart].iPlayerSoundId)
			ENDIF
			SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_WON_PLAYER_1) + SCGW_PlayerBD[SCGW_ServerBD.iGameWinner].iGang))
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sSCGWData.iLocalBS, ciSCGW_LOCALBS_DIED_THIS_ROUND)
	AND SCGW_ServerBD.iRoundWinner[SCGW_ServerBD.iCurrentRound - 1] != -1
	AND SCGW_ServerBD.iRoundWinner[SCGW_ServerBD.iCurrentRound - 1] = sSCGWData.sLocalPlayer.iLocalPart
		IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_SCGW_WON_NO_DEATHS)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_SCGW_WON_NO_DEATHS, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO, "CLO_VWM_D_1_1", "UNLOCK_NAME_SHIRT3","FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_UNTOUCHABLE, TRUE)
			SCGW_TRIGGER_HELP_TEXT("SCGW_CH_4", ciSCGW_DEFAULT_HELP_TEXT_TIME, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_LBD_ENTRY
PROC SCGW_PROCESS_CLIENT_STATE_LBD_ENTRY()

	IF NOT IS_ACCOUNT_OVER_17_FOR_UGC()
		SCGW_CLEANUP_AND_EXIT_CLIENT()	
	ENDIF
	
	SCGW_DRAW_LEADERBOARD_ENTRIES()
	
	SCGW_PROCESS_LBD_ENTRY_HELP_TEXT()
	
	INT iUnpackedInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS]
	
	IF SC_PROFANITY_GET_CHECK_IS_VALID(sSCGWData.iLbdInitialProfanityToken)
           
		IF SC_PROFANITY_GET_CHECK_IS_PENDING(sSCGWData.iLbdInitialProfanityToken)
			EXIT
		ENDIF
		
		IF SC_PROFANITY_GET_STRING_PASSED(sSCGWData.iLbdInitialProfanityToken)
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_LBD_ENTRY - Profanity: Initials OK")
			BROADCAST_SCGW_INITIALS_ENTERED()
			SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_LBD)
			SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Change_Screen")
			sSCGWData.iLbdInitialProfanityToken = 0
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_LBD_ENTRY - Profanity: Initials BAD")
			SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Change_Letter")
			sSCGWData.iInitial = 0
			ARCADE_UNPACK_LEADERBOARD_INITIALS(0, iUnpackedInitials)
			iUnpackedInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS - 1] = SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang
			SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardInitials = ARCADE_PACK_LEADERBOARD_INITIALS(iUnpackedInitials)
			sSCGWData.iLbdInitialProfanityToken = 0
		ENDIF

		EXIT
		
	ENDIF
	
	CONTROL_ACTION CA_InitialUp = INPUT_SCRIPT_RB
	CONTROL_ACTION CA_InitialDown = INPUT_SCRIPT_LB
	CONTROL_ACTION CA_InitialAccept = INPUT_FRONTEND_RDOWN
	CONTROL_ACTION CA_InitialBack = INPUT_FRONTEND_RRIGHT
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CA_InitialAccept = INPUT_CURSOR_ACCEPT		
		CA_InitialBack = INPUT_CURSOR_CANCEL
		CA_InitialUp = INPUT_FRONTEND_UP
		CA_InitialDown = INPUT_FRONTEND_DOWN
	ENDIF
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_InitialAccept)
		sSCGWData.iInitial ++
		IF sSCGWData.iInitial = ciARCADE_CABINET_LEADERBOARD_INITIALS - 1
			ARCADE_UNPACK_LEADERBOARD_INITIALS(SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardInitials, iUnpackedInitials)
			TEXT_LABEL_3 tl3Initials = ARCADE_GET_CHAR_FROM_INT(iUnpackedInitials[0])
			tl3Initials += ARCADE_GET_CHAR_FROM_INT(iUnpackedInitials[1])
			tl3Initials += ARCADE_GET_CHAR_FROM_INT(iUnpackedInitials[2])
			IF SC_PROFANITY_CHECK_STRING(tl3Initials, sSCGWData.iLbdInitialProfanityToken)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_CLIENT_STATE_LBD_ENTRY - Profanity: Starting check")
				SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Entered_Score")
			ENDIF
		ELIF SCGW_GET_PLAYER_LEADERBOARD_POSITION(sSCGWData.sLocalPlayer.iLocalPart) = ciSCGW_LBD_DEFAULT_RANK
			SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Change_Screen")
			SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_LBD)
		ENDIF
		EXIT
	ENDIF
	
	IF SCGW_GET_PLAYER_LEADERBOARD_POSITION(sSCGWData.sLocalPlayer.iLocalPart) = ciSCGW_LBD_DEFAULT_RANK
		EXIT
	ENDIF
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_InitialBack)
		IF sSCGWData.iInitial > 0
			sSCGWData.iInitial --
		ENDIF
		EXIT
	ENDIF
	
	INT iDelta = 0
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_InitialUp)
		iDelta = 1
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, CA_InitialDown)
		iDelta = -1
	ENDIF
	
	IF iDelta = 0
		EXIT
	ENDIF
	
	SCGW_PLAY_FIRE_AND_FORGET_FRONTEND_SOUND("Frontend_Change_Letter")
	
	ARCADE_UNPACK_LEADERBOARD_INITIALS(SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardInitials, iUnpackedInitials)
	
	iUnpackedInitials[sSCGWData.iInitial] += iDelta
	
	IF iUnpackedInitials[sSCGWData.iInitial] >= ciARCADE_CABINET_LEADERBOARD_INITIALS_MAX
		iUnpackedInitials[sSCGWData.iInitial] = 0
	ELIF iUnpackedInitials[sSCGWData.iInitial] < 0
		iUnpackedInitials[sSCGWData.iInitial] = ciARCADE_CABINET_LEADERBOARD_INITIALS_MAX - 1
	ENDIF
	
	SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iLeaderboardInitials = ARCADE_PACK_LEADERBOARD_INITIALS(iUnpackedInitials)
	
ENDPROC	

/// PURPOSE:
///  Processing for the client state: SCGW_CLIENT_STATE_LBD
PROC SCGW_PROCESS_CLIENT_STATE_LBD()

	SCGW_PROCESS_LBD_HELP_TEXT()
	
	SCGW_DRAW_LEADERBOARD()
	
ENDPROC	

//~~~~~ Client State Machine Main Update ~~~~~

/// PURPOSE:
///    Processes the client state machine, calling the method to process the current state
PROC SCGW_PROCESS_CLIENT_STATE_MACHINE()
	
	SWITCH sSCGWData.eCurrentState
		
		CASE SCGW_CLIENT_STATE_INITIALISING
			
			SCGW_PROCESS_CLIENT_STATE_INITIALISING()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_REQUESTING_ASSETS
			
			SCGW_PROCESS_CLIENT_STATE_REQUESTING_ASSETS()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_INTRO_VIDEO
		
			SCGW_PROCESS_CLIENT_STATE_INTRO_VIDEO()
			
		BREAK
		
		CASE SCGW_CLIENT_STATE_GANG_SCREEN
		
			SCGW_PROCESS_CLIENT_STATE_GANG_SCREEN()
			
		BREAK
		
		CASE SCGW_CLIENT_STATE_LAUNCHING
		
			SCGW_PROCESS_CLIENT_STATE_LAUNCHING()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_INIT_ROUND
		
			SCGW_PROCESS_CLIENT_STATE_INIT_ROUND()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_COUNTDOWN
		
			SCGW_PROCESS_CLIENT_STATE_COUNTDOWN()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_RUNNING
		
			SCGW_PROCESS_CLIENT_STATE_RUNNING()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_ROUND_END_SCREEN
		
			SCGW_PROCESS_CLIENT_STATE_ROUND_END_SCREEN()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_LBD_ENTRY
		
			SCGW_PROCESS_CLIENT_STATE_LBD_ENTRY()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_LBD
		
			SCGW_PROCESS_CLIENT_STATE_LBD()
		
		BREAK
		
		CASE SCGW_CLIENT_STATE_CLEANUP
		
			SCGW_CLEANUP_AND_EXIT_CLIENT()
			
		BREAK
		
		CASE SCGW_CLIENT_STATE_FINISHED
			//Do nothing
		BREAK
		
	ENDSWITCH

ENDPROC

// ------------------------------ SERVER STATE MACHINE ------------------------------

/// PURPOSE:
///    Updates the server state
PROC SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE eNewState)
	
	IF !sSCGWData.sLocalPlayer.bIsHost
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_SET_SERVER_STATE - State changed from ", 
		SCGW_DEBUG_GET_SERVER_STATE_AS_STRING(SCGW_ServerBD.eCurrentServerState), " to ", SCGW_DEBUG_GET_SERVER_STATE_AS_STRING(eNewState))
	
	SCGW_ServerBD.eCurrentServerState = eNewState
	
ENDPROC

//~~~~~ Server State Machine State Updates ~~~~~

/// PURPOSE:
///  Processing for the server state: SCGW_SERVER_STATE_INITIALISING
PROC SCGW_PROCESS_SERVER_STATE_INITIALISING()
	
	SCGW_INITIALIZE_MAP_TILES()
	
	SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_LAUNCHING)

ENDPROC

/// PURPOSE:
///  Processing for the server state: SCGW_SERVER_STATE_LAUNCHING
PROC SCGW_PROCESS_SERVER_STATE_LAUNCHING()
	
	INT iPlayerLoop, iNumPlayers
	BOOL bAllLoaded = TRUE
	
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
		AND NOT SCGW_Bot[iPlayerLoop].bActive
			RELOOP
		ENDIF
		
		IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
			bAllLoaded = FALSE
			BREAKLOOP
		ENDIF
		
		IF SCGW_PlayerBD[iPlayerLoop].eCurrentPlayerState = SCGW_PLAYER_STATE_INITIALISING
			bAllLoaded = FALSE
			BREAKLOOP
		ENDIF
		
		iNumPlayers++
		
	ENDFOR
	
	IF !bAllLoaded
		IF HAS_NET_TIMER_STARTED(sSCGWData.tdIntroDelayTimer)
			RESET_NET_TIMER(sSCGWData.tdIntroDelayTimer)
		ENDIF
		
		//Waiting for players to load and ready up
		EXIT
	ENDIF
	
	IF iNumPlayers <= 1
#IF IS_DEBUG_BUILD
	AND sSCGWData.sDebug.bDebugOnePlayerTest = FALSE
#ENDIF
		IF HAS_NET_TIMER_STARTED(sSCGWData.tdIntroDelayTimer)
			RESET_NET_TIMER(sSCGWData.tdIntroDelayTimer)
		ENDIF
		
		//Not enough players to start
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sSCGWData.tdIntroDelayTimer)
		REINIT_NET_TIMER(sSCGWData.tdIntroDelayTimer)
		EXIT
	ELIF NOT HAS_NET_TIMER_EXPIRED(sSCGWData.tdIntroDelayTimer, ciSCGW_GANG_ALL_READY_DELAY)
		EXIT
	ENDIF
	
	SCGW_ServerBD.iNumRounds = iNumPlayers + 1
	
	SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_INIT_ROUND)
	
ENDPROC

/// PURPOSE:
///  Processing for the server state: SCGW_SERVER_STATE_INIT_ROUND
PROC SCGW_PROCESS_SERVER_STATE_INIT_ROUND()
	
	INITIALIZE_SERVER_DATA_FOR_NEW_ROUND()
	
	SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_COUNTDOWN)
	
ENDPROC

/// PURPOSE:
///  Processing for the server state: SCGW_SERVER_STATE_COUNTDOWN
PROC SCGW_PROCESS_SERVER_STATE_COUNTDOWN()
	
	IF NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_ServerBD.iStartTime
		EXIT
	ENDIF
	
	//Inititialize spawn times to the start time
	SCGW_ServerBD.iLastPickupSpawnTime = SCGW_ServerBD.iStartTime
	SCGW_ServerBD.iLastVehicleSpawnTime = SCGW_ServerBD.iStartTime
	
	SCGW_PROCESS_SERVER_PICKUPS(TRUE)
	SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_RUNNING)
	
ENDPROC

/// PURPOSE:
///  Processing for the server state: SCGW_SERVER_STATE_RUNNING
PROC SCGW_PROCESS_SERVER_STATE_RUNNING()
	
	SCGW_PROCESS_SERVER_BUILDING_OWNERSHIP()
	SCGW_PROCESS_SERVER_PICKUPS()
	SCGW_PROCESS_SERVER_VEHICLES()
	
	IF NOT SCGW_SHOULD_CURRENT_ROUND_END()
		EXIT
	ENDIF
	
	INT iWinner = SCGW_GET_ROUND_WINNER()
	
	IF iWinner = -1
		SCGW_PROCESS_SERVER_PICKUPS(TRUE, TRUE)
		SCGW_ServerBD.bSuddenDeath = TRUE
		EXIT
	ENDIF

	SCGW_ServerBD.iRoundScores[SCGW_PlayerBD[iWinner].iGang] ++
	SCGW_ServerBD.iRoundWinner[SCGW_ServerBD.iCurrentRound] = iWinner
	SCGW_ServerBD.iCurrentRound++
	SCGW_ServerBD.iEndTime = NATIVE_TO_INT(GET_NETWORK_TIME())
	
	IF NOT SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND()
		SCGW_ServerBD.iGameWinner = SCGW_GET_GAME_WINNER()
	ENDIF
	
	SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_ROUND_END_SCREEN)
	
ENDPROC

/// PURPOSE:
///  Processing for the server state: SCGW_SERVER_STATE_ROUND_END_SCREEN
PROC SCGW_PROCESS_SERVER_STATE_ROUND_END_SCREEN()
	
	IF (NATIVE_TO_INT(GET_NETWORK_TIME()) < SCGW_ServerBD.iEndTime + ciSCGW_ROUND_RESULT_TIME)
		//Waiting for end screen to finish displaying
		EXIT
	ENDIF
		
	IF SCGW_SHOULD_CONTINUE_TO_NEXT_ROUND()
		SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_INIT_ROUND)
		EXIT
	ENDIF
	
	SCGW_SORT_SERVER_LEADERBOARD()
	SCGW_SET_SERVER_STATE(SCGW_SERVER_STATE_LEADERBOARD)
	
ENDPROC

//~~~~~ Server State Machine Main Update ~~~~~

/// PURPOSE:
///    Processes the server state machine, calling the method to process the current state
PROC SCGW_PROCESS_SERVER_STATE_MACHINE()
	
	IF NOT sSCGWData.sLocalPlayer.bIsHost 
		EXIT
	ENDIF
	
	SWITCH SCGW_ServerBD.eCurrentServerState
		
		CASE SCGW_SERVER_STATE_INITIALISING
			SCGW_PROCESS_SERVER_STATE_INITIALISING()
		BREAK
		
		CASE SCGW_SERVER_STATE_LAUNCHING
			SCGW_PROCESS_SERVER_STATE_LAUNCHING()
		BREAK
		
		CASE SCGW_SERVER_STATE_INIT_ROUND
			SCGW_PROCESS_SERVER_STATE_INIT_ROUND()
		BREAK
		
		CASE SCGW_SERVER_STATE_COUNTDOWN
			SCGW_PROCESS_SERVER_STATE_COUNTDOWN()
		BREAK
		
		CASE SCGW_SERVER_STATE_RUNNING
			SCGW_PROCESS_SERVER_STATE_RUNNING()
		BREAK
		
		CASE SCGW_SERVER_STATE_ROUND_END_SCREEN
			SCGW_PROCESS_SERVER_STATE_ROUND_END_SCREEN()
		BREAK
		
		CASE SCGW_SERVER_STATE_LEADERBOARD
			
		BREAK
		
		CASE SCGW_SERVER_STATE_FINISHED
		
		BREAK
		
	ENDSWITCH

ENDPROC

// ------------------------------ EVENT PROCESSING ------------------------------

/// PURPOSE:
///    Processes all incoming events sent by players in the minigame
PROC SCGW_PROCESS_EVENTS()
	
	INT iCount
	SCRIPT_EVENT_DATA_SCGW_MINIGAME_PICKUP_COLLECTED EventDataPickupCollected
	SCRIPT_EVENT_DATA_SCGW_MINIGAME_SIDE_CLAIMED EventDataSideClaimed
	SCRIPT_EVENT_DATA_SCGW_MINIGAME_REQUEST_CARS EventDataRequestCars
	SCRIPT_EVENT_DATA_SCGW_MINIGAME_DAMAGED EventDataDamaged
	SCRIPT_EVENT_DATA_CASINO_ARCADE_SEND_LEADERBOARD_EVENT EventDataLeaderboardSend
	SCRIPT_EVENT_DATA_SCGW_INITIALS_ENTERED EventDataInitialsEntered
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		IF GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) != EVENT_NETWORK_SCRIPT_EVENT
			RELOOP
		ENDIF
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataPickupCollected, SIZE_OF(EventDataPickupCollected))
			IF EventDataPickupCollected.Details.Type = SCRIPT_EVENT_SCGW_MINIGAME_PICKUP_COLLECTED
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Found event: PICKUP COLLECTED")
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Player = ", NATIVE_TO_INT(EventDataPickupCollected.Details.FromPlayerIndex))
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - iPickupIndex = ", EventDataPickupCollected.iPickupIndex)
				SCGW_REMOVE_COLLECTED_PICKUP(EventDataPickupCollected.iPickupIndex)
				RELOOP
			ENDIF
		ENDIF
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataSideClaimed, SIZE_OF(EventDataSideClaimed))
			IF EventDataSideClaimed.Details.Type = SCRIPT_EVENT_SCGW_MINIGAME_SIDE_CLAIMED
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Found event: SIDE CLAIMED")
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Player = ", NATIVE_TO_INT(EventDataSideClaimed.Details.FromPlayerIndex))
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - iGridIndex = ", EventDataSideClaimed.iGridIndex)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - iSide = ", EventDataSideClaimed.iSide)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - iGang = ", EventDataSideClaimed.iGang)
				SCGW_SET_GRID_SIDE_AS_CLAIMED_BY_GANG(EventDataSideClaimed.iGridIndex, EventDataSideClaimed.iSide, EventDataSideClaimed.iGang)
				RELOOP
			ENDIF
		ENDIF
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataRequestCars, SIZE_OF(EventDataRequestCars))
			IF EventDataRequestCars.Details.Type = SCRIPT_EVENT_SCGW_MINIGAME_REQUEST_CARS
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Found event: REQUEST CARS")
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Player = ", NATIVE_TO_INT(EventDataRequestCars.Details.FromPlayerIndex))
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - iPosition = ", EventDataRequestCars.iPosition)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - iGang = ", EventDataRequestCars.iGang)
				SCGW_SPAWN_GANG_BACKUP_VEHICLES(EventDataRequestCars.iGang, EventDataRequestCars.iPosition)
				RELOOP
			ENDIF
		ENDIF
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataDamaged, SIZE_OF(EventDataDamaged))
			IF EventDataRequestCars.Details.Type = SCRIPT_EVENT_SCGW_MINIGAME_DAMAGED
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Found event: DAMAGED")
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Player = ", NATIVE_TO_INT(EventDataDamaged.Details.FromPlayerIndex))
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - bDead = ", EventDataDamaged.bDead)
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - bHitByCar = ", EventDataDamaged.bHitByCar)
				SCGW_PROCESS_DAMAGED_EVENT(EventDataDamaged.iParticipant, EventDataDamaged.bDead, EventDataDamaged.bHitByCar)
				RELOOP
			ENDIF
		ENDIF
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataLeaderboardSend, SIZE_OF(EventDataLeaderboardSend))
			IF EventDataLeaderboardSend.Details.Type = SCRIPT_EVENT_CASINO_ARCADE_SEND_LEADERBOARD_EVENT
				IF SCGW_ServerBD.bLeaderboardRecieved
					RELOOP
				ENDIF
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Found event: LEADERBOARD")
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Player = ", NATIVE_TO_INT(EventDataLeaderboardSend.Details.FromPlayerIndex))
				IF EventDataLeaderboardSend.eGameType != CASINO_ARCADE_GAME_STREET_CRIMES
					RELOOP
				ENDIF
				SCGW_INITIALISE_SERVER_LEADERBOARD(EventDataLeaderboardSend.iInitials, EventDataLeaderboardSend.iScores)
				RELOOP
			ENDIF
		ENDIF
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventDataInitialsEntered, SIZE_OF(EventDataInitialsEntered))
			IF EventDataInitialsEntered.Details.Type = SCRIPT_EVENT_SCGW_INITIALS_ENTERED
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Found event: INITIALS")
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_PROCESS_EVENTS - Player = ", NATIVE_TO_INT(EventDataInitialsEntered.Details.FromPlayerIndex))
				SCGW_PROCESS_INITIALS_ENTERED_EVENT(EventDataInitialsEntered.Details.FromPlayerIndex)
				RELOOP
			ENDIF
		ENDIF
		
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD
// ------------------------------ DEBUG ------------------------------

/// PURPOSE:
///    Sets the given widget group to be the parent for any widgets created by the minigame.
PROC SCGW_DEBUG_SET_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)

	sSCGWData.sDebug.widgetGridMinigame = parentWidgetGroup
	
ENDPROC

PROC SCGW_DEBUG_CREATE_PLAYERS_WIDGET()
	INT iPlayerLoop
	START_WIDGET_GROUP("iPlayerPosition")
		FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
			ADD_WIDGET_INT_READ_ONLY(CONVERT_INT_TO_STRING(iPlayerLoop),SCGW_UF_PlayerBD[iPlayerLoop].iPlayerPosition)
		ENDFOR
	STOP_WIDGET_GROUP()
ENDPROC
/// PURPOSE:
///    Creates the debug widgets for this minigame.
PROC SCGW_DEBUG_UPDATE_WIDGETS()
	IF sSCGWData.sDebug.bCallVehicles
		sSCGWData.sDebug.bCallVehicles = FALSE
		BROADCAST_SCGW_MINIGAME_REQUEST_CARS(SCGW_UF_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iPlayerPosition, SCGW_PlayerBD[sSCGWData.sLocalPlayer.iLocalPart].iGang)
	ENDIF
ENDPROC
/// PURPOSE:
///    Creates the debug widgets for this minigame.
PROC SCGW_DEBUG_CREATE_WIDGETS()

	IF sSCGWData.sDebug.bDebugCreatedWidgets
	OR sSCGWData.sDebug.widgetGridMinigame = NULL
		EXIT
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(sSCGWData.sDebug.widgetGridMinigame)
	
	START_WIDGET_GROUP("Grid Arcade Cabinet Minigame")
		
		ADD_WIDGET_BOOL("bBlockDrawing", g_bBlockArcadeCabinetDrawing)
		ADD_WIDGET_BOOL("g_bMinimizeArcadeCabinetDrawing", g_bMinimizeArcadeCabinetDrawing)
		ADD_WIDGET_BOOL("bDrawCollisionDebug", sSCGWData.sDebug.bDrawCollisionDebug)
		ADD_WIDGET_BOOL("bDisableSFX", sSCGWData.sDebug.bDisableSFX)
		ADD_WIDGET_BOOL("bDisableMusic", sSCGWData.sDebug.bDisableMusic)
		ADD_WIDGET_BOOL("bTriggerChallenges", sSCGWData.sDebug.bTriggerChallenges)
		ADD_WIDGET_BOOL("bCallVehicles", sSCGWData.sDebug.bCallVehicles)
		
		SCGW_DEBUG_CREATE_PLAYERS_WIDGET()
		ARCADE_GAMES_POSTFX_WIDGET_CREATE()
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(sSCGWData.sDebug.widgetGridMinigame)
	
	sSCGWData.sDebug.bDebugCreatedWidgets = TRUE
	
ENDPROC

/// PURPOSE:
///    Dumps the current state of the game to the logs
///    Automatically called when a bug is entered
PROC SCGW_DEBUG_DUMP_DATA_TO_LOGS()
	
	//INT i, j
	
	ARCADE_CABINET_DUMP_DATA_TO_CONSOLE()

	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_DEBUG_DUMP_DATA_TO_LOGS - Dumping data...")
	
	//TODO:
	
ENDPROC

/// PURPOSE:
///    Updates all debug functionality for the minigame.
PROC SCGW_DEBUG_PROCESSING()

	SCGW_DEBUG_CREATE_WIDGETS()
	SCGW_DEBUG_UPDATE_WIDGETS()
	ARCADE_GAMES_POSTFX_WIDGET_UPDATE()

	//Enable debug lines for debug
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	//If a bug is entered dump all data to the console.
	IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)	
		SCGW_DEBUG_DUMP_DATA_TO_LOGS()
	ENDIF
	
ENDPROC
#ENDIF

// ------------------------------ MAIN UPDATE ------------------------------

PROC SCGW_PLAYER_TO_BOT()
	INT iBot
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF SCGW_Bot[iBot].bActive
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_BOT_PLAYERS - SCGW_Bot[",iBot,"].SCGW_player.iGang ",SCGW_Bot[iBot].SCGW_player.iGang)
			SCGW_PlayerBD[iBot] = SCGW_Bot[iBot].SCGW_player
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_BOT_PLAYERS - SCGW_PlayerBD[iBot].iGang ",SCGW_PlayerBD[iBot].iGang)
		ENDIF
	ENDFOR
ENDPROC

PROC SCGW_BOT_TO_PLAYER()
	INT iBot
	FOR iBot = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF SCGW_Bot[iBot].bActive
			CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_BOT_PLAYERS - SCGW_Bot[",iBot,"].SCGW_player.iGang ",SCGW_Bot[iBot].SCGW_player.iGang)
			SCGW_PlayerBD[iBot] = SCGW_Bot[iBot].SCGW_player
			SCGW_UF_PlayerBD[iBot] = SCGW_Bot[iBot].SCGW_UF_Player
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Keeps the cached local participant index up to date and checks for host migrations
PROC SCGW_UPDATE_LOCAL_PLAYER_DATA()

	IF NETWORK_IS_GAME_IN_PROGRESS()
		sSCGWData.sLocalPlayer.iLocalPart = PARTICIPANT_ID_TO_INT()
		BOOL bIsHostThisFrame = NETWORK_IS_HOST_OF_THIS_SCRIPT()
#IF IS_DEBUG_BUILD
		IF sSCGWData.sLocalPlayer.bIsHost != bIsHostThisFrame
			IF bIsHostThisFrame
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_LOCAL_PLAYER_DATA - SCRIPT HOST MIGRATED TO LOCAL PLAYER")
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_LOCAL_PLAYER_DATA - SCRIPT HOST MIGRATED TO PARTICIPANT: ", NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
			ENDIF
		ENDIF
#ENDIF
		sSCGWData.sLocalPlayer.bIsHost = bIsHostThisFrame
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Maintains a count of all players taking part in the minigame
PROC SCGW_UPDATE_ACTIVE_PLAYERS()

	sSCGWData.iActivePlayers = 0
	sSCGWData.iConnectedPlayers = 0
	sSCGWData.iLastFrameWinningPart = sSCGWData.iWinningPart
	
	INT iHighScore = -1
	INT iWinner = -1
	
	INT iPlayerLoop = 0
	FOR iPlayerLoop = 0 TO ciSCGW_GANG_MAX - 1
		sSCGWData.iGangPlayers[iPlayerLoop] = -1
		IF SCGW_UF_ServerBD.iCurrentScore[iPlayerLoop] > iHighScore
			iHighScore = SCGW_UF_ServerBD.iCurrentScore[iPlayerLoop]
			iWinner = iPlayerLoop
		ELIF SCGW_UF_ServerBD.iCurrentScore[iPlayerLoop] = iHighScore
			//Draw no winner
			iWinner = -1
		ENDIF
	ENDFOR
	
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
		AND NOT SCGW_Bot[iPlayerLoop].bActive
			RELOOP
		ENDIF
		
		IF SCGW_PlayerBD[iPlayerLoop].iGang = ciSCGW_GANG_NONE
			RELOOP
		ENDIF
		
		sSCGWData.iGangPlayers[SCGW_PlayerBD[iPlayerLoop].iGang] = iPlayerLoop
		
		IF SCGW_PlayerBD[iPlayerLoop].iGang = iWinner
			iWinner = iPlayerLoop
		ENDIF
		
		IF sSCGWData.iGangPlayerJoinTimes[SCGW_PlayerBD[iPlayerLoop].iGang] = -HIGHEST_INT
		AND g_sCasinoArcadeStreetCrimeVars.eThisFrameAction = CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_NONE
			SCGW_SET_GLOBAL_THIS_FRAME_ACTION(INT_TO_ENUM(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION, ENUM_TO_INT(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_JOINED_PLAYER_1) + SCGW_PlayerBD[iPlayerLoop].iGang))
			sSCGWData.iGangPlayerJoinTimes[SCGW_PlayerBD[iPlayerLoop].iGang] = NATIVE_TO_INT(GET_NETWORK_TIME())
		ENDIF
		
		sSCGWData.iConnectedPlayers++
		
		IF sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth != SCGW_PlayerBD[iPlayerLoop].iHealth
			INT iDelta = SCGW_PlayerBD[iPlayerLoop].iHealth - sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth
			IF iDelta > 0
				sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth += (sSCGWData.iUpdateFrames)
				IF sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth > SCGW_PlayerBD[iPlayerLoop].iHealth
					sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth = SCGW_PlayerBD[iPlayerLoop].iHealth
				ENDIF
			ELSE
				sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth -= (sSCGWData.iUpdateFramesHalf)
				IF sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth < SCGW_PlayerBD[iPlayerLoop].iHealth
					sSCGWData.sPlayers[iPlayerLoop].iDisplayHealth = SCGW_PlayerBD[iPlayerLoop].iHealth
				ENDIF
			ENDIF
		ENDIF
		
		IF SCGW_PlayerBD[iPlayerLoop].eCurrentPlayerState = SCGW_PLAYER_STATE_CANT_RESPAWN
			RELOOP
		ENDIF
		
		sSCGWData.iActivePlayers ++
		
	ENDFOR
	
	sSCGWData.iWinningPart = iWinner
	
	FOR iPlayerLoop = 0 TO ciSCGW_GANG_MAX - 1
		IF sSCGWData.iGangPlayers[iPlayerLoop] = -1
		AND sSCGWData.iGangPlayerJoinTimes[iPlayerLoop] != -HIGHEST_INT
			sSCGWData.iGangPlayerJoinTimes[iPlayerLoop] = -HIGHEST_INT
		ENDIF
	ENDFOR
	
#IF IS_DEBUG_BUILD
	IF sSCGWData.sDebug.bDebugOnePlayerTest
		sSCGWData.iActivePlayers ++
		sSCGWData.iConnectedPlayers++
	ENDIF
#ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[SCGW] [JS] SCGW_UPDATE_ACTIVE_PLAYERS - iConnectedPlayers = ", sSCGWData.iConnectedPlayers, " iActivePlayers = ", sSCGWData.iActivePlayers)
	
ENDPROC

/// PURPOSE:
///    Run every frame before the state machines are processed
PROC SCGW_PRE_UPDATE()
	
	//Reset This frame action if required
	IF g_sCasinoArcadeStreetCrimeVars.eThisFrameAction != CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_NONE
		SCGW_SET_GLOBAL_THIS_FRAME_ACTION(CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_NONE)
	ENDIF
	
	sSCGWData.iFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
	sSCGWData.iUpdateFrames = FLOOR(sSCGWData.iFrameTimeCounter / cfSCGW_ANIM_FRAME_TIME)
	sSCGWData.iFrameTimeCounter = sSCGWData.iFrameTimeCounter - ROUND(cfSCGW_ANIM_FRAME_TIME * sSCGWData.iUpdateFrames)
	
	sSCGWData.iFrameTimeCounterHalf += ROUND(GET_FRAME_TIME() * 1000)
	sSCGWData.iUpdateFramesHalf = FLOOR(sSCGWData.iFrameTimeCounterHalf / (cfSCGW_ANIM_FRAME_TIME*3))
	sSCGWData.iFrameTimeCounterHalf = sSCGWData.iFrameTimeCounterHalf - ROUND((cfSCGW_ANIM_FRAME_TIME*3) * sSCGWData.iUpdateFramesHalf)
	
	sSCGWData.iPickupAnimFrame += sSCGWData.iUpdateFrames
	
	sSCGWData.iRocketAnimFrame += sSCGWData.iUpdateFramesHalf
	IF sSCGWData.iRocketAnimFrame >= ciSCGW_ROCKET_SPRITE_FRAMES
		sSCGWData.iRocketAnimFrame = 0
	ENDIF
	
	SCGW_PLAYER_TO_BOT()
	SCGW_UPDATE_LOCAL_PLAYER_DATA()
	SCGW_UPDATE_ACTIVE_PLAYERS()
	
	IF sSCGWData.eCurrentState >= SCGW_CLIENT_STATE_CLEANUP
		// Cleaning up, don't do anything else
		EXIT
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_CLEANUP)
		EXIT
	ENDIF
	
	IF SCGW_SHOULD_QUIT_NOW()
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			sSCGWData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			sSCGWData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			PLAY_ARCADE_CABINET_ANIMATION(sSCGWData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		ENDIF	
		SCGW_SET_CLIENT_STATE(SCGW_CLIENT_STATE_CLEANUP)
		EXIT
	ENDIF
	
	IF sSCGWData.sLocalPlayer.bIsHost
	AND NOT SCGW_ServerBD.bLeaderboardRecieved
	AND (sSCGWData.iLastLeaderboardRequest = -HIGHEST_INT
	OR NATIVE_TO_INT(GET_NETWORK_TIME()) - sSCGWData.iLastLeaderboardRequest > ciSCGW_RESEND_LEADERBOARD_REQUEST_TIME)
		BROADCAST_CASINO_ARCADE_REQUEST_LEADERBOARD_EVENT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), CASINO_ARCADE_GAME_STREET_CRIMES)
		sSCGWData.iLastLeaderboardRequest = NATIVE_TO_INT(GET_NETWORK_TIME())
	ENDIF
	
	SCGW_PROCESS_EVENTS()
	
	CLEAR_BIT(sSCGWData.iHelpTextBitset, ciSCGW_HELP_BS_SHOWING_HELP_TEXT)
	
	BOOL bIsPlaying = sSCGWData.eCurrentState >= SCGW_CLIENT_STATE_INTRO_VIDEO
	ARCADE_CABINET_COMMON_EVERY_FRAME_PROCESSING(bIsPlaying)
	
	sSCGWData.bCanAddBots = TRUE
	INT iPlayerLoop
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF sSCGWData.sLocalPlayer.iLocalPart != iPlayerLoop
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
			sSCGWData.bCanAddBots = FALSE
		ENDIF
	ENDFOR
	FOR iPlayerLoop = 0 TO ciSCGW_MAX_PLAYERS - 1
		IF NOT sSCGWData.bCanAddBots
			SCGW_Bot[iPlayerLoop].bActive = FALSE
			SET_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(FALSE)
		ENDIF
		IF SCGW_Bot[iPlayerLoop].bActive
			SET_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(TRUE)
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Run every frame after the state machines are processed
PROC SCGW_POST_UPDATE()
	SCGW_BOT_TO_PLAYER()
	IF NOT SCGW_SHOULD_ALLOW_HELP_TEXT_THIS_FRAME()
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Call every frame to run the minigame
PROC PROCESS_STREET_CRIME_GANG_WARS()

	SCGW_PRE_UPDATE()
	
	SCGW_PROCESS_CLIENT_STATE_MACHINE()
	
	SCGW_PROCESS_SERVER_STATE_MACHINE()
	
	SCGW_POST_UPDATE()
		
#IF IS_DEBUG_BUILD
	SCGW_DEBUG_PROCESSING()
#ENDIF
	
ENDPROC
