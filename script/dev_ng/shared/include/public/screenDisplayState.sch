USING "rage_builtins.sch"
USING "globals.sch"


FUNC BOOL IS_RESULT_SCREEN_DISPLAYING()
	IF g_bResultScreenDisplaying
		RETURN TRUE
	ELIF g_bMissionOverStatTrigger AND NOT g_bMissionStatSystemBlocker
		//GET_NUMBER_OF_THREADS_WITH_NAME
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_COLLECTED_SCREEN_DISPLAYING()
	RETURN g_bCollectedScreenDisplaying
ENDFUNC

/// PURPOSE:
///    Sets the state of the global g_bResultScreenDisplaying to track down issues with it not being cleaned up correctly.
PROC SET_RESULT_SCREEN_DISPLAYING_STATE(BOOL bState)
	CDEBUG1LN(DEBUG_MISSION_STATS, "(CDM)SET_RESULT_SCREEN_DISPLAYING_STATE; Script ", GET_THIS_SCRIPT_NAME()," is setting the state of g_bResultScreenDisplaying to be ", bState)
	g_bResultScreenDisplaying	= bState
	//B* 1913692: Also reset the Prepared BOOL
	g_bResultScreenPrepared = bState
ENDPROC

PROC SET_COLLECTED_SCREEN_DISPLAYING(BOOL bState)
	IF g_bCollectedScreenDisplaying <> bState
		CDEBUG1LN(DEBUG_MISSION_STATS, "SET_COLLECTED_SCREEN_DISPLAYING; Script ", GET_THIS_SCRIPT_NAME()," is setting the state of g_bCollectedScreenDisplaying to be ", bState)
		g_bCollectedScreenDisplaying = bState
	ELSE
		CDEBUG3LN(DEBUG_MISSION_STATS, "SET_COLLECTED_SCREEN_DISPLAYING; Script ", GET_THIS_SCRIPT_NAME()," TRIED setting the state of g_bCollectedScreenDisplaying to be ", bState, " but it already set")
	ENDIF
ENDPROC
//EOF
