USING "rage_builtins.sch"
USING "globals.sch"
using "script_player.sch"
USING "commands_camera.sch"
USING "commands_script.sch"
USING "net_include.sch"
using "Usefulcommands.sch"

#if IS_DEBUG_BUILD

// =========================================== CONSTS ==============================================
CONST_INT MAX_RESOURCES 20
CONST_INT MAX_VECTORS 30
CONST_INT MAX_INTS 60
CONST_INT MAX_FLOATS 30
CONST_INT MAX_BOOLS 30
CONST_INT MAX_TEXT_WIDGETS 10
CONST_INT MAX_MODELS 10


// ===================================================== xml hash coees ================================
CONST_INT HASH_menuType 1958669326
CONST_INT HASH_subType 253941193
CONST_INT HASH_startV -1811416473
CONST_INT HASH_startF -1347571282
CONST_INT HASH_startI -1527866320
CONST_INT HASH_startB -55587979
CONST_INT HASH_startTw -2043871425
CONST_INT HASH_startMod 99217238
CONST_INT HASH_vX 2049010486
CONST_INT HASH_vY 199626429
CONST_INT HASH_vZ 461352432
CONST_INT HASH_f 1208324078 
CONST_INT HASH_i 1573272431
CONST_INT HASH_b 14385563
CONST_INT HASH_mod -1905913153
CONST_INT HASH_tw -211853340



// ============================================ SET UP RESOURCES ==================================

STRUCT strResourceStarts
	int startV
	int startF
	int startI
	int startB
	int startTw
	int startMod
ENDSTRUCT

STRUCT strResource
	int menuType
	int type
	WIDGET_GROUP_ID widget
	strResourceStarts varStart
ENDSTRUCT

strResource resource[MAX_RESOURCES]

vector v[MAX_VECTORS]
int i[MAX_INTS]
float f[MAX_FLOATS]
bool b[MAX_BOOLS]
TEXT_WIDGET_ID tw[MAX_TEXT_WIDGETS]
MODEL_NAMES mod[MAX_MODELS]



// ==================================================== bools ================================
bool bExit
bool bDebug
bool bAddResource
bool bJustCounting
bool bAddToScenario
bool bAddTrigger
bool bPlayScenario
bool bPlaybackTriggered
bool bSaveData
bool bLoadData

//================================================== ints =======================================
int iResourceCount
int vCount,fCount,iCount,bCount,twCount,modCount
int iCamTime, itemp, itempb

// ===================================================== floats ===================================



// ======================================================= vectors =============================
vector vTemp, vTempb

// ================================================= WIDGETS ===================================
WIDGET_GROUP_ID parentWidget
WIDGET_GROUP_ID resourceWidget
WIDGET_GROUP_ID scenarioWidget
WIDGET_GROUP_ID TriggerWidget

//========================================== camera ==================================
CAMERA_INDEX aCam, bCam


// ========================================== strings =========================================
string sVehRecording
STRING sfilename = "sceneBuild"

// =========================================== text labels ==========================================
TEXT_LABEL_31 tempTl

// ============================================= vehicle index =================================
vehicle_index tempVeh

// ============================================= ped_index =====================================
ped_index tempPed

// ==================================================== model names ================================
MODEL_NAMES tempModel

//================================================ object index =====================================
object_index tempObj


//SAVE / LOAD

string path="X:/GTA5/build/dev/"
TEXT_LABEL_63 filename

proc saveString(string s,string saveAtt)
	TEXT_LABEL_31 tx = ""
	tx += saveAtt
	tx += "=\""
	tx += s
	tx += "\" "
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tx,path,filename)
ENDPROC

proc saveBool(bool bBool,string saveAtt)
	TEXT_LABEL_31 tx = ""
	tx += saveAtt
	tx += "=\""
	IF bBool = TRUE
		tx += "TRUE"
	ELSE
		tx += "FALSE"
	ENDIF
	tx += "\" " 
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tx,path,filename)
ENDPROC

PROC saveInt(int j,string saveAtt)//,bool ignoreComma=FALSE)
	TEXT_LABEL_31 tx = ""
	tx += saveAtt
	tx += "=\""
	tx += j
	tx += "\" " 
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tx,path,filename)
ENDPROC

proc saveFloat(float fl,string saveAtt)//,bool ignoreComma=FALSE)
	fl=fl
	TEXT_LABEL_31 tx = ""
	tx += saveAtt
	tx += "=\""
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tx,path,filename) 
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fl,path,filename)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ",path,filename)
ENDPROC


proc saveResourceData(int j)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		<Resource ",path,filename)	
	
	saveInt(resource[j].menuType,"menuType")
	saveInt(resource[j].type,"subType")
	saveInt(resource[j].varStart.startV,"startV")
	saveInt(resource[j].varStart.startF,"startF")
	saveInt(resource[j].varStart.startI,"startI")
	saveInt(resource[j].varStart.startB,"startB")
	saveInt(resource[j].varStart.startTw,"startTw")
	saveInt(resource[j].varStart.startMod,"startMod")
			
	SAVE_STRING_TO_NAMED_DEBUG_FILE(" />",path,filename)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
ENDPROC

proc checkSaveData()	
	
	if bSaveData=TRUE	
		bSaveData=FALSE
		int j	
		filename = sfilename
		filename += ".xml"
		CLEAR_NAMED_DEBUG_FILE(path,filename)
		//
		
		OPEN_NAMED_DEBUG_FILE(path, filename)
		
		PRINTLN(path,filename)
		
					
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<loadData>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		PRINTLN("path: ",path," filename: ",filename)
		
		if not IS_PED_INJURED(PLAYER_PED_ID())
			vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	<PlayerCoord ",path,filename)
			saveFloat(vTemp.x,"vX")
			saveFloat(vTemp.y,"vY")
			saveFloat(vTemp.z,"vZ")
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" />",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDIF
		
		for j = 0 to iResourceCount-1		
			saveResourceData(j)			
		ENDFOR	
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Vectors ",path,filename)
		saveInt(resource[iResourceCount].varStart.startV,"vCount")
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		
		for j = 0 to resource[iResourceCount].varStart.startV-1
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<saveVector ",path,filename)	
			saveFloat(v[j].x,"vX")
			saveFloat(v[j].y,"vY")
			saveFloat(v[j].z,"vZ")					
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		/>",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDFOR		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Vectors>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Floats ",path,filename)
		saveInt(resource[iResourceCount].varStart.startF,"fCount")
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)		
		for j = 0 to resource[iResourceCount].varStart.startF-1
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<saveFloat ",path,filename)	
			saveFloat(f[j],"f")				
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		/>",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDFOR		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Floats>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Ints ",path,filename)
		saveInt(resource[iResourceCount].varStart.startI,"iCount")
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		
		for j = 0 to resource[iResourceCount].varStart.startI-1
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<saveInt ",path,filename)	
			saveInt(i[j],"i")				
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		/>",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDFOR		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Ints>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Bools ",path,filename)
		saveInt(resource[iResourceCount].varStart.startB,"bCount")
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)		
		for j = 0 to resource[iResourceCount].varStart.startB-1
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<saveBool ",path,filename)	
			saveBool(b[j],"b")				
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		/>",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDFOR		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Bools>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)				
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Mod ",path,filename)
		saveInt(resource[iResourceCount].varStart.startMod,"ModCount")
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)		
		for j = 0 to resource[iResourceCount].varStart.startMod-1
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<saveMod ",path,filename)	
			saveint(ENUM_TO_INT(mod[j]),"Mod")				
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		/>",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDFOR		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Mod>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	<Tw ",path,filename)
		saveInt(resource[iResourceCount].varStart.startTw,"twCount")
		SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)		
		PRINTLN("Saving text widget data")
		for j = 0 to resource[iResourceCount].varStart.startTw-1
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		<saveTw ",path,filename)	
			if DOES_TEXT_WIDGET_EXIST(tw[j])
				saveString(GET_CONTENTS_OF_TEXT_WIDGET(tw[j]),"tw")				
				PRINTLN("tw[",j,"]=",GET_CONTENTS_OF_TEXT_WIDGET(tw[j]))
			ELSE
				saveString("null","tw")
			ENDIF
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		/>",path,filename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
		ENDFOR		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	</Tw>",path,filename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)
						
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path,filename)	
		SAVE_STRING_TO_NAMED_DEBUG_FILE("</loadData>",path,filename)
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC





func int FIND_NTH_RESOURCE_ENTRY(int nthEntry, int menutype, int subtype=-1)
	int iCounter,j
	//PRINTLN("Find entry: nthEntry: ",nthEntry," menutype: ",menutype," subtype: ",subtype)
	for j = 0 to COUNT_OF(resource)-1
	//	PRINTLN("resource: ",j)
	//	PRINTLN("menu type: ",resource[j].menuType)
	//	PRINTLN("sub type: ",resource[j].type)
		if resource[j].menuType = menutype
			if subtype != -1
				if resource[j].type = subtype
					if nthEntry = iCounter
						return j
					ENDIF
					iCounter++
				ENDIF
			ELSE
				if nthEntry = iCounter
					return j
				ENDIF
				iCounter++
			ENDIF
		ENDIF
	ENDFOR
	return -1
ENDFUNC

proc getBezCoord(int resEntry, int line, float t, vector &vReturn)  //vector v0, vector v1, vector v2, vector v3, float t)	
	//outputDebugVector("v0:",bez1.vPos)
	//outputDebugVector("v1:",bez2.vPos)
	vector v0,v1,v2,v3,vR	
	
	int istartVectorEntry = resource[resEntry].varStart.startV

	
	if not are_vectors_equal(v[istartVectorEntry+(line*3)],v[istartVectorEntry+(line*3)+3])		
		v0 = v[istartVectorEntry+(line*3)]
		v1 = v[istartVectorEntry+(line*3)] + v[istartVectorEntry+(line*3)+1] //bezVcontrol[bezNode]
		v2 = v[istartVectorEntry+(line*3)+3] - v[istartVectorEntry+(line*3)+4]
		v3 = v[istartVectorEntry+(line*3)+3]
		vR.x = (((1-t)*(1-t)*(1-t))*v0.x) + (3*((1-t)*(1-t))*t*v1.x) + (3*(1-t)*(t*t)*v2.x) + ((t*t*t)*v3.x)
		vR.y = (((1-t)*(1-t)*(1-t))*v0.y) + (3*((1-t)*(1-t))*t*v1.y) + (3*(1-t)*(t*t)*v2.y) + ((t*t*t)*v3.y)
		vR.z = (((1-t)*(1-t)*(1-t))*v0.z) + (3*((1-t)*(1-t))*t*v1.z) + (3*(1-t)*(t*t)*v2.z) + ((t*t*t)*v3.z)			
		vReturn =  vR		
	ELSE	

		vReturn = v[resEntry+(line*3)]		
	ENDIF
endproc

proc getBezRot(int resEntry, int line, float t, vector &vReturn)
	vector v0,v1,vchange
	if not are_vectors_equal(v[resEntry+(line*3)+2],v[resEntry+(line*3)+5])		
		v0 = v[resEntry+(line*3)+2]
		v1 = v[resEntry+(line*3)+5]
		vChange = v1-v0
		if vChange.x < -180.0 vChange.x += 360.0 ENDIF
		if vChange.x > 180.0 vChange.x -= 360.0 ENDIF
		if vChange.y < -180.0 vChange.y += 360.0 ENDIF
		if vChange.y > 180.0 vChange.y -= 360.0 ENDIF
		if vChange.z < -180.0 vChange.z += 360.0 ENDIF
		if vChange.z > 180.0 vChange.z -= 360.0 ENDIF
		vreturn = v0 + (t*vChange)
	endif
ENDPROC

proc useBezier(int bezNo, int currentTime, int totalTime, vector &vPos, vector &vRot)
	int iResEntry = FIND_NTH_RESOURCE_ENTRY(bezNo,0,5) //bezier resource entry
	
	float fPropOfTotalTime = to_float(currentTime) / to_float(totalTime)
	int iBezStart = floor(fPropOfTotalTime * (i[resource[iResEntry].varStart.startI+1]-1)) //no of nodes
	float timeAlongLineSegment = (fPropOfTotalTime * (i[resource[iResEntry].varStart.startI+1]-1)) - to_float(iBezStart)
	getBezCoord(iResEntry,iBezStart,timeAlongLineSegment,vPos)
	getBezRot(iResEntry,iBezStart,timeAlongLineSegment,vRot)
ENDPROC


func bool GET_MODEL_FOR_PED(MODEL_NAMES &aModel, string sModel)
	int j
	
	string aString
	while GET_PED_MODEL_FROM_INDEX(j,amodel)		
		aString = GET_MODEL_NAME_FOR_DEBUG(amodel)
		if not Is_String_Null_Or_Empty(aString)
			if not Is_String_Null_Or_Empty(sModel)
				if ARE_STRINGS_EQUAL(aString,sModel)
					return true
				ENDIF
			ENDIF
		ENDIF
		j++
	ENDWHILE
	aModel = DUMMY_MODEL_FOR_SCRIPT
	return FALSE
ENDFUNC

func bool set_model_for_Vehicle(MODEL_NAMES &setmod,string sModel)
	int j
	MODEL_NAMES modReturn
	
	while GET_VEHICLE_MODEL_FROM_INDEX(j,modReturn)
		if are_Strings_equal(GET_MODEL_NAME_FOR_DEBUG(modReturn),sModel)
			setmod = modReturn
			return true
		ENDIF
		j++
	endwhile
	return FALSE
ENDFUNC

func vehicle_index eventToVeh(int iEvent)
	//coun through events finding vehicle from index stored in iEvent
	if iEvent != -1
		return INT_TO_NATIVE(vehicle_index,i[resource[iEvent].varStart.startI+1])
	ENDIF
	return null
ENDFUNC

func ped_index eventToPed(int iEvent)
	//coun through events finding vehicle from index stored in iEvent
	if iEvent != -1
		return INT_TO_NATIVE(ped_index,i[resource[iEvent].varStart.startI+1])
	ENDIF
	return null
ENDFUNC


proc VehToEvent(vehicle_index veh, int iTrigger)
	i[resource[iTrigger].varStart.startI+1] = NATIVE_TO_INT(veh)
ENDPROC

func string GET_RESOURCE_NAME(int MainType, int entry, int subType=-1, STRING name=NULL )
	string sReturn
	TEXT_LABEL_31 txt
	txt = name
	txt += " "
	int iTriggerCount
	int j
	for j = 0 to entry
		if j = entry
			txt += iTriggerCount
			sReturn = GET_STRING_FROM_STRING(txt,0,GET_LENGTH_OF_LITERAL_STRING(txt))
			//sReturn = txt
			return sReturn
		ENDIF
		if resource[j].menuType=MainType
			if subType != -1
				if resource[j].type = subType
					iTriggerCount++
				ENDIF
			ELSE
				iTriggerCount++
			ENDIF
		ENDIF		
	ENDFOR
	
	return "Empty"
ENDFUNC

proc checkIntResourceOverflow(int iResource,int varEntry)
	int j
	if iResource < iResourceCount-1
		if resource[iResource+1].varStart.startI = resource[iResource].varStart.startI + varEntry
			for j = iResource+1 to COUNT_OF(resource)-1
				resource[j].varStart.startI++
			ENDFOR
			for j = COUNT_OF(i)-1 to resource[iResource+1].varStart.startI step -1
				i[j] = i[j-1]
			ENDFOR			
			i[resource[iResource+1].varStart.startI-1] = 0 //reset the new variable
		ENDIF
	ENDIF
ENDPROC

proc checkFloatResourceOverflow(int iResource,int varEntry)
	int j
	if iResource < iResourceCount-1
		if resource[iResource+1].varStart.startF = resource[iResource].varStart.startF + varEntry
			for j = iResource+1 to COUNT_OF(resource)-1
				resource[j].varStart.startF++
			ENDFOR
			for j = COUNT_OF(f)-1 to resource[iResource+1].varStart.startF step -1
				f[j] = f[j-1]
			ENDFOR			
			f[resource[iResource+1].varStart.startf-1] = 0 //reset the new variable
		ENDIF
	ENDIF
ENDPROC

proc checkVectorResourceOverflow(int iResource,int varEntry)
	int j
	if iResource < iResourceCount-1
		if resource[iResource+1].varStart.startV = resource[iResource].varStart.startV + varEntry
			for j = iResource+1 to COUNT_OF(resource)-1
				resource[j].varStart.startV++
			ENDFOR
			for j = COUNT_OF(v)-1 to resource[iResource+1].varStart.startV step -1
				v[j] = v[j-1]
			ENDFOR			
			v[resource[iResource+1].varStart.startV-1] = <<0,0,0>> //reset the new variable
		ENDIF
	ENDIF
ENDPROC

proc checkTwResourceOverflow(int iResource,int varEntry)
	int j
	if iResource < iResourceCount-1
		if resource[iResource+1].varStart.startTw = resource[iResource].varStart.startTw + varEntry
			for j = iResource+1 to COUNT_OF(resource)-1
				resource[j].varStart.startTw++
			ENDFOR
			for j = COUNT_OF(tw)-1 to resource[iResource+1].varStart.startTw step -1
				tw[j] = tw[j-1]
			ENDFOR		
	
	//		tw[resource[iResource+1].varStart.startTw-1] = 0 //reset the new variable
		ENDIF
	ENDIF
ENDPROC

proc checkModResourceOverflow(int iResource,int varEntry)
	int j
	if iResource < iResourceCount-1
		if resource[iResource+1].varStart.startMod = resource[iResource].varStart.startMod + varEntry
			for j = iResource+1 to COUNT_OF(resource)-1
				resource[j].varStart.startMod++
			ENDFOR
			for j = COUNT_OF(mod)-1 to resource[iResource+1].varStart.startMod step -1
				mod[j] = mod[j-1]
			ENDFOR			
			mod[resource[iResource+1].varStart.startMod-1] = DUMMY_MODEL_FOR_SCRIPT //reset the new variable
		ENDIF
	ENDIF
ENDPROC

proc checkBoolResourceOverflow(int iResource,int varEntry)
	int j
	if iResource < iResourceCount-1
		if resource[iResource+1].varStart.startB = resource[iResource].varStart.startB + varEntry
			for j = iResource+1 to COUNT_OF(resource)-1
				resource[j].varStart.startB++
			ENDFOR
			for j = COUNT_OF(b)-1 to resource[iResource+1].varStart.startB step -1
				b[j] = b[j-1]
			ENDFOR			
			b[resource[iResource+1].varStart.startB-1] = FALSE //reset the new variable
		ENDIF
	ENDIF
ENDPROC

proc addWidgetCombo(int iResource, int varEntry, string label, string tl1,string tl2=null,string tl3=null,string tl4=null,string tl5=null,string tl6=null, string tl7=null, string tl8=null,string tl9=null, string tTwenty=null, string t21=null, string t22=null, string t23=null)
	PRINTLN("addWidgetCombo()")
	checkIntResourceOverflow(iResource,varEntry)
	if bJustCounting=FALSE
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO(tl1)
			if not is_string_null(tl2) ADD_TO_WIDGET_COMBO(tl2) ENDIF
			if not is_string_null(tl3) ADD_TO_WIDGET_COMBO(tl3) ENDIF
			if not is_string_null(tl4) ADD_TO_WIDGET_COMBO(tl4) ENDIF
			if not is_string_null(tl5) ADD_TO_WIDGET_COMBO(tl5) ENDIF
			if not is_string_null(tl6) ADD_TO_WIDGET_COMBO(tl6)	ENDIF
			if not is_string_null(tl7) ADD_TO_WIDGET_COMBO(tl7) ENDIF
			if not is_string_null(tl8) ADD_TO_WIDGET_COMBO(tl8) ENDIF
			if not is_string_null(tl9) ADD_TO_WIDGET_COMBO(tl9) ENDIF
			if not is_string_null(tTwenty) ADD_TO_WIDGET_COMBO(tTwenty) ENDIF
			if not is_string_null(t21) ADD_TO_WIDGET_COMBO(t21) ENDIF
			if not is_string_null(t22) ADD_TO_WIDGET_COMBO(t22) ENDIF
			if not is_string_null(t23) ADD_TO_WIDGET_COMBO(t23) ENDIF
		STOP_WIDGET_COMBO(label,i[resource[iResource].varStart.startI+varEntry])
	ELSE
		iCount++
	ENDIF
ENDPROC

proc addWidgetComboStorage(int iResource, int varEntry)
	checkIntResourceOverflow(iResource,varEntry)
	PRINTLN("addWidgetComboStorage()")
	if bJustCounting=FALSE
	ELSE
		iCount++
	ENDIF
ENDPROC

PROC addWidgetTextEntry(int iResource, int varEntry,string title)
	checkTwResourceOverflow(iResource,varEntry)
	PRINTLN("addWidgetTextEntry()")
	if bJustCounting=FALSE
		tw[resource[iResource].varStart.startTw+varEntry] = ADD_TEXT_WIDGET(title)		
	ELSE
		twCount++
	ENDIF
ENDPROC

proc addBool(int iResource, int varEntry, string title)
	checkBoolResourceOverflow(iResource,varEntry)
	PRINTLN("addBool()")
	if bJustCounting=FALSE
		ADD_WIDGET_BOOL(title,b[resource[iResource].varStart.startB+varEntry])
	ELSE
		bCount++
	ENDIF
ENDPROC

proc addBoolStorage(int iResource, int varEntry)
	checkIntResourceOverflow(iResource,varEntry)
	PRINTLN("addBoolStorage()")
	if bJustCounting=FALSE
	ELSE
		bCount++
	ENDIF
ENDPROC

proc addFloat(int iResource, int varEntry, string title,float min, float max, float slider_step)
	checkFloatResourceOverflow(iResource,varEntry)
	PRINTLN("addFloat()")
	if bJustCounting=FALSE
		ADD_WIDGET_float_SLIDER(title,f[resource[iResource].varStart.startF+varEntry],min,max,slider_step)
	ELSE
		fCount++
	ENDIF
ENDPROC

proc addInt(int iResource, int varEntry, string title,int min, int max, int slider_step)
	checkIntResourceOverflow(iResource,varEntry)
	PRINTLN("addInt()")
	if bJustCounting=FALSE
		ADD_WIDGET_INT_SLIDER(title,i[resource[iResource].varStart.startI+varEntry],min,max,slider_step)
	ELSE
		iCount++
	ENDIF
ENDPROC

proc addIntStorage(int iResource, int varEntry)
	checkIntResourceOverflow(iResource,varEntry)
	PRINTLN("addIntStorage()")
	if bJustCounting=FALSE
	ELSE
		iCount++
	ENDIF
ENDPROC

proc addVector(int iResource, int varEntry, string title, float min, float max, float slider_step)
	PRINTLN("addVector()")
	checkVectorResourceOverflow(iResource,varEntry)
	if bJustCounting=FALSE
		ADD_WIDGET_VECTOR_SLIDER(title,v[resource[iResource].varStart.startV+varEntry],min,max,slider_step)
	ELSE
		vCount++
	ENDIF
ENDPROC


proc addVectorStorage(int iResource, int varEntry)
	PRINTLN("addVectorStorage()")
	checkVectorResourceOverflow(iResource,varEntry)
	if bJustCounting=FALSE
	ELSE
		vCount++
	ENDIF
ENDPROC

proc addModelStorage(int iResource, int varEntry)
	PRINTLN("addModelStorage()")
	checkModResourceOverflow(iResource,varEntry)
	if bJustCounting=FALSE
	ELSE
		modCount++
	ENDIF
ENDPROC



proc addResourceWidgetType(int iEntry, string stxtWidget=null)
	//int fdfdf = COUNT_OF(MODEL_NAMES)
	//fdfdf = fdfdf
	//IS_MODEL_A_PED()
	//int ji

	int ji
/*
	PRINTLN("addResourceWidgetType()")
		
		for j = 0 to iResourceCount-1
			PRINTLN("resource[",j,"].starti: ",resource[j].varStart.startI," to ",resource[j+1].varStart.startI-1)
			for jj = resource[j].varStart.startI to resource[j+1].varStart.startI-1
				PRINTLN("i[",jj,"]",i[jj])
			ENDFOR
		ENDFOR
*/		
		
		
		TEXT_LABEL_23 txtlbl = stxtWidget
		if resource[iEntry].menuType = 0 //asset
			addWidgetCombo(iEntry,0,"Resource Type","None","Vehicle","Ped","Force","Vehicle Damage","Bezier Path","Explosion","Veh Recording","object","Cam Setup","Anim Dict")	
		ELIF  resource[iEntry].menuType = 1 //trigger
			addBoolStorage(iEntry,0) //set to true when trigger conditions are met.
			addWidgetCombo(iEntry,0,"Trigger Type","None","entity in area","Time in recording","distance between entities","time in anim")
		ELIF  resource[iEntry].menuType = 2 //event
			addWidgetCombo(iEntry,0,"Resource Type","None","Ped actions","ped anims","Vehicle Actions","Entity on Bezier")	
			addintStorage(iEntry,1) //playing state
		endif
		SWITCH resource[iEntry].type
			case 1 //vehicle			
				addWidgetTextEntry(iEntry,0,"Model")
				if bJustCounting=FALSE
					if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw])
						SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw],txtlbl)
					ENDIF
				ENDIF
				addBool(iEntry,0,"Spawn vehicle")
				addModelStorage(iEntry,0) //mod[resource[iEntry].varStart.startMod]
				addintStorage(iEntry,1) //vehicle index  1
			BREAK
			case 2 //ped
				//GET_PED_MODEL_FROM_INDEX()
				addWidgetTextEntry(iEntry,0,"Model")
				if bJustCounting=FALSE
					if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw])
						SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw],txtlbl)
					ENDIF
				ENDIF
				addBool(iEntry,0,"Spawn ped")				
				addIntStorage(iEntry,1) //store ped index as int
			BREAK
			CASE 3 //force
							
				//int drop-down combo force type
				addWidgetCombo(iEntry,1,"FORCE","IMPULSE","EXTERNAL FORCE","EXTERNAL IMPULSE","TORQUE","ANGULAR IMPULSE")
				
				//vector force
				addVector(iEntry,0,"Force",-1000.0,1000.0,0.1)
				//vector offset
				addVector(iEntry,1,"Offset",-15.0,15.0,0.1)
				//bool local force
				addBool(iEntry,0,"Local Force")
				//bool local offset
				addBool(iEntry,1,"Local Offset")
				//bool scale by mass
				addBool(iEntry,2,"scale by mass")
				addint(iEntry,2,"Duration",0,60000,1) //time
				addIntStorage(iEntry,3) //keeps track of time
				addBool(iEntry,3,"APPLY FORCE TO USED PLAYER/VEHICLE")
				//APPLY_FORCE_TO_ENTITY(vehicle[VEH_PLAYER].id,APPLY_TYPE_IMPULSE,<<0,fImpulse,0>>,<<0,0,0>>,0,true,true,true)
				
				
			BREAK
			case 4 //vehicle damage
				addBool(iEntry,0,"Apply Damage")
				addBool(iEntry,1,"Local Damage")
				addVector(iEntry,0,"Force",-20.0,20.0,0.05)
				addfloat(iEntry,0,"Damage",-1000,10000,1)
				addfloat(iEntry,1,"Deformation",-1000,10000,1)				
			BREAK
			case 5 //bezier
				addBool(iEntry,0,"Show Bezier")
				addint(iEntry,1,"No of nodes",0,20,1)
				addIntStorage(iEntry,2) //store number of nodes to detect when player changes above value
				addint(iEntry,3,"Set node to edit",0,20,1)
				addBool(iEntry,1,"Set node pos to debug cam")
				addBool(iEntry,2,"Set control to debug cam")
				
				for ji = 0 to i[resource[iEntry].varStart.startI+1]-1
					addVectorStorage(iEntry,0) //pos
					addVectorStorage(iEntry,1) //control
					addVectorStorage(iEntry,2) //rot
				ENDFOR
				/*
				
				STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
				*/
				/*
				SET_CURRENT_WIDGET_GROUP(resource[iEntry].widget)
				for ji = 0 to i[resource[iEntry].varStart.startI+1]-1
					tl = "node "
					tl += ji
					START_WIDGET_GROUP(tl)
					addVector(v[resource[iEntry].varStart.startV+(ji*3)],"Pos",-8000.0,8000.0,0.1)
					addVector(v[resource[iEntry].varStart.startV+1+(ji*3)],"Control",-200.0,200.0,0.1)
					addVector(v[resource[iEntry].varStart.startV+2+(ji*3)],"Rot",-180.0,180.0,0.1)
					STOP_WIDGET_GROUP()
				ENDFOR
				CLEAR_CURRENT_WIDGET_GROUP(resource[iEntry].widget)
				*/

			BREAK
			case 6 //explosion
			BREAK
			case 7 //recording
				addWidgetTextEntry(iEntry,0,"Recording Name")
				if bJustCounting=FALSE
					if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw])
						SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw],txtlbl)
					ENDIF
				ENDIF
				addint(iEntry,1,"rec to test?",0,999,1)
				addBool(iEntry,0,"Test Now")
			break
			case 8 //object
				addint(iEntry,1,"object enum value",-2147483647,2147483647,1)
				addIntStorage(iEntry,2) // 2: store to check if id changed by user
				addIntStorage(iEntry,3) // 3:store id of object
				addBool(iEntry,0,"Test Now")
			BREAK
			case 9 //cam setup
				addWidgetCombo(iEntry,1,"Cam type","Static","pan","follow bez")
				addIntStorage(iEntry,2) //store previous state of above
				addint(iEntry,3,"Time duration",0,120000,10)
				addBool(iEntry,0,"Test cam")
				if i[resource[iEntry].varStart.startI+1] = 0
					addBool(iEntry,1,"Pos cam start to debug")
					addVector(iEntry,0,"Pos",-8000.0,8000.0,0.1)
					addVector(iEntry,1,"Rot",-180.0,180.0,0.1)
				ELIF i[resource[iEntry].varStart.startI+1] = 1
					addBool(iEntry,1,"Pos cam start to debug")
					addBool(iEntry,2,"Pos cam end to debug")
					addVector(iEntry,0,"Cam start pos",-8000.0,8000.0,0.1)
					addVector(iEntry,1,"Cam start rot",-180.0,180.0,0.1)
					addVector(iEntry,2,"cam end pos",-8000.0,8000.0,0.1)
					addVector(iEntry,3,"cam end rot",-180.0,180.0,0.1)
				ELIF i[resource[iEntry].varStart.startI+1] = 2
					addint(iEntry,4,"Select Bezier",0,10,1)
					addIntStorage(iEntry,5) //store bezier to use
				ENDIF
			BREAK
			
			case 10 //animation bank
				addWidgetTextEntry(iEntry,0,"Anim Dict")
				if bJustCounting=FALSE
					if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw])
						SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw],txtlbl)
					ENDIF
				ENDIF
			BREAK
			
			case 101 //entity in area
				addWidgetCombo(iEntry,1,"Entity type","Ped","Vehicle","Object")
				addint(iEntry,2,"Entity entry",0,20,1)
				addBool(iEntry,0,"Set Location from player")
				addVector(iEntry,0,"Check location",-8000.0,8000.0,0.1)
				PRINTLN("Define readius of check:",f[resource[iEntry].varStart.startF])
				PRINTLN("resource[4].varStart.startF",resource[iEntry].varStart.startF)
				PRINTLN("iEntry:",iEntry)
				addFloat(iEntry,0,"Check Radius",0.0,100.0,0.01)
				addBool(iEntry,1,"Show locate")
			BREAK
			
			case 102 //trigger time in recording
				//vehicle to watch
				addint(iEntry,1,"vehicle check",0,20,1)
				//recording number played
				addint(iEntry,2,"Check rec no.",0,999,1)
				//time in recording
				addFloat(iEntry,0,"Time in rec",0.0,120000.0,10.0)				
			break
			
			case 104 //time in anim
				addint(iEntry,1,"Ped to check",0,20,1)
				addint(iEntry,2,"Anim dict entry",0,20,1)
				addWidgetTextEntry(iEntry,0,"Anim Name")
				if bJustCounting=FALSE
					if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw])
						SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw],txtlbl)
					ENDIF
				ENDIF
				
				addFloat(iEntry,0,"Time in anim (-1.0=finised)",-1.0,120000.0,0.01)	
				addBoolStorage(iEntry,0) //store when required anim is playing
			BREAK
			
			case 201 //create ped and give actions
				//trigger
				addint(iEntry,2,"Trigger (-1 = on Start)",-1,30,1)
				//action
				addint(iEntry,3,"Ped to use",0,20,1)
				addWidgetCombo(iEntry,4,"Action","Spawn","Go to coord")
				addIntStorage(iEntry,5) //store previous state of above
				if i[resource[iEntry].varStart.startI+4] = 0 //spawn
					addBool(iEntry,0,"Set spawn pos from player")
					addVector(iEntry,0,"Spawn pos",-8000.0,8000.0,0.1)
					addFloat(iEntry,0,"Heading",-180.0,180.0,0.1)
				ELIF i[resource[iEntry].varStart.startI+4] = 1 //go to coord
					addBool(iEntry,0,"Set goto pos from player")
					addVector(iEntry,0,"Go to coord",-8000.0,8000.0,0.1)
					addWidgetCombo(iEntry,6,"Speed","Walk","Run","Sprint")					
				ENDIF				
			BREAK
			
			case 202 //ped anims
				addint(iEntry,2,"Trigger (-1 = on Start)",-1,30,1)
				addint(iEntry,3,"Ped to use",0,20,1)
				addint(iEntry,4,"Anim dict to use",0,20,1)
				addWidgetTextEntry(iEntry,0,"Anim to play")
				if bJustCounting=FALSE
					if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw])
						SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw],txtlbl)
					ENDIF
				ENDIF
				addBool(iEntry,0,"Set start location")
				addVector(iEntry,0,"Start pos",-8000.0,8000.0,0.1)
				addFloat(iEntry,0,"Heading",-180.0,180.0,0.1)
				addFloat(iEntry,1,"Blend in",0.0,10.0,0.01)
				addFloat(iEntry,2,"Blend out",0.0,10.0,0.01)
				addBool(iEntry,1,"Stores no of anim flags")
				addIntStorage(iEntry,5) //stores no of anim flags
				if i[resource[iEntry].varStart.startI+5] != 0
					for ji = 0 to i[resource[iEntry].varStart.startI+5]-1
						addWidgetCombo(iEntry,6+ji,"Anim flag","AF_DEFAULT",
						"AF_EXIT_AFTER_INTERRUPTED",
						"AF_EXTRACT_INITIAL_OFFSET",
						"AF_HOLD_LAST_FRAME",
						"AF_IGNORE_GRAVITY",
						"AF_LOOPING",
						"AF_NOT_INTERRUPTABLE",
						"AF_OVERRIDE_PHYSICS", 
						"AF_REORIENT_WHEN_FINISHED",
						"AF_REPOSITION_WHEN_FINISHED",
						"AF_SECONDARY",
						"AF_TURN_OFF_COLLISION",
						"AF_UPPERBODY")

					ENDFOR		
				ENDIF				
			BREAK
			
			case 203 //Vehicle Actions
				addint(iEntry,2,"Trigger (-1 = on Start)",-1,30,1)
				addint(iEntry,3,"Veh to use",0,20,1)
				addWidgetCombo(iEntry,4,"Action","Spawn","Trigger Recording")
				addIntStorage(iEntry,5) //store previous state of above
				if i[resource[iEntry].varStart.startI+4] = 0 //spawn
					addBool(iEntry,0,"Set spawn pos from player")
					addVector(iEntry,0,"Spawn pos",-8000.0,8000.0,0.1)
					addFloat(iEntry,0,"Heading",-180.0,180.0,0.1)
				ELIF i[resource[iEntry].varStart.startI+4] = 1 //trigger recording
					addint(iEntry,6,"Recording to play",0,999,1)				
					//skip duration in recording
					addFloat(iEntry,1,"Skip time in rec (ms)",0,120000,1)
				ENDIF
			BREAK
			
			case 204 //entity follow bezier curve
				addint(iEntry,2,"Trigger (-1 = on Start)",-1,30,1)
				addWidgetCombo(iEntry,3,"Entity type","Ped","Vehicle","Object")
				addint(iEntry,4,"Entity entry",0,20,1)
				addint(iEntry,5,"Bezier entry",0,20,1)
				addint(iEntry,6,"Time Duration",0,120000,1)
				addIntStorage(iEntry,7) // 7: current time playing back
				addBool(iEntry,0,"Set first point to entity pos")
			BREAK
		ENDSWITCH
ENDPROC

proc initResourceWidget()
	PRINTLN("initResourceWidget()")
	int j
	//int ji
	int ji
	TEXT_LABEL_23 tl

	if not DOES_WIDGET_GROUP_EXIST(resourceWidget)
		SET_CURRENT_WIDGET_GROUP(parentWidget)
		resourceWidget = START_WIDGET_GROUP("Resources")
		ADD_WIDGET_BOOL("Add resource",bAddResource)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(parentWidget)
	ENDIF

		
	
	
		
		string sTxlbl 
		for j = 0 to iResourceCount-1
			sTxlbl=null
			if resource[j].menuType = 0
				SET_CURRENT_WIDGET_GROUP(resourceWidget)
				if DOES_WIDGET_GROUP_EXIST(resource[j].widget)
					if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
						sTxlbl = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw])
					ENDIF
					
					DELETE_WIDGET_GROUP(resource[j].widget)
					PRINTLN("DELETE_WIDGET_GROUP(resource[j(",j,")].widget)")
				ENDIF
				CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
				
				SWITCH resource[j].type
					case 0 //not set
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP("New Resource")
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 1 		//vehicle
					
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Vehicle"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j,sTxlbl)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 2 
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Ped"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j,sTxlbl)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 3
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Force"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 4 
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Vehicle Damage"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 5 
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Bezier Path"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
						SET_CURRENT_WIDGET_GROUP(resource[j].widget)
							for ji = 0 to i[resource[j].varStart.startI+1]-1
								tl = "node "
								tl += ji
								START_WIDGET_GROUP(tl)
								addVector(j,(ji*3),"Pos",-8000.0,8000.0,0.1)
								addVector(j,1+(ji*3),"Control",-200.0,200.0,0.1)
								addVector(j,2+(ji*3),"Rot",-180.0,180.0,0.1)
								STOP_WIDGET_GROUP()
							ENDFOR
						CLEAR_CURRENT_WIDGET_GROUP(resource[j].widget)
					BREAK
					case 6 
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Explosion"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK		
					case 7
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Vehicle Recording"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j,sTxlbl)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 8
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Object"))					
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 9
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Camera Cut"))
						PRINTLN("START_WIDGET_GROUP()",j)
						addResourceWidgetType(j)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
					case 10
						SET_CURRENT_WIDGET_GROUP(resourceWidget)
						resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(0,j,resource[j].type,"Anim dict"))
						addResourceWidgetType(j,sTxlbl)
						STOP_WIDGET_GROUP()	
						CLEAR_CURRENT_WIDGET_GROUP(resourceWidget)
					BREAK
				ENDSWITCH
			ENDIF	
			
			
		ENDFOR
	
	
ENDPROC

proc initTriggerWidget()
PRINTLN("initTriggerWidget()")
	int j

	if not DOES_WIDGET_GROUP_EXIST(TriggerWidget)
		SET_CURRENT_WIDGET_GROUP(parentWidget)
		TriggerWidget = START_WIDGET_GROUP("Triggers")
		ADD_WIDGET_BOOL("Add to Triggers",bAddTrigger)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(parentWidget)
	ENDIF
	
		
	string sTxlbl 
	for j = 0 to iResourceCount-1
		sTxlbl=null
		if resource[j].menuType = 1
			SET_CURRENT_WIDGET_GROUP(TriggerWidget)
		
			if DOES_WIDGET_GROUP_EXIST(resource[j].widget)
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
					sTxlbl = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw])
					sTxlbl=sTxlbl
				ENDIF
					
				DELETE_WIDGET_GROUP(resource[j].widget)
			ENDIF
			CLEAR_CURRENT_WIDGET_GROUP(TriggerWidget)
			
			SWITCH resource[j].type
				case 100 //not set
					SET_CURRENT_WIDGET_GROUP(TriggerWidget)
					resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(1,j,-1,"Trigger "))
					//resource[j].type = 200
					addResourceWidgetType(j)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(TriggerWidget)					
					
				BREAK	
				case 101 //entity in area
					SET_CURRENT_WIDGET_GROUP(TriggerWidget)
					resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(1,j,-1,"Entity in area "))
					addResourceWidgetType(j)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(TriggerWidget)	
				BREAK
				case 102 //time in recording
					SET_CURRENT_WIDGET_GROUP(TriggerWidget)
					resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(1,j,-1,"time in recording "))
					addResourceWidgetType(j)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(TriggerWidget)	
				BREAK
				case 103 //distance between entities
					SET_CURRENT_WIDGET_GROUP(TriggerWidget)
					resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(1,j,-1,"Distance from entity "))
					addResourceWidgetType(j)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(TriggerWidget)
				BREAK
				case 104 //time in anim
					SET_CURRENT_WIDGET_GROUP(TriggerWidget)
					resource[j].widget = START_WIDGET_GROUP(GET_RESOURCE_NAME(1,j,-1,"Time in anim "))
					addResourceWidgetType(j,sTxlbl)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(TriggerWidget)
				BREAK
				/*
				case 201 //not set
					SET_CURRENT_WIDGET_GROUP(scenarioWidget)
					resource[j].widget = START_WIDGET_GROUP("Ped actions")
					addResourceWidgetType(j)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)										
				BREAK
			*/
			ENDSWITCH			
		ENDIF						
	ENDFOR
ENDPROC



func bool HAS_EVENT_TRIGGER_RULE_BEEN_MET(int trigger)
	INT iResourceB
	string sTemp, sTempB
	ped_index thisPed
	if trigger != -1
		PRINTLN("Find resource entry for trigger: ",trigger)
		int iResource = FIND_NTH_RESOURCE_ENTRY(trigger,1)
		
		PRINTLN("iResource entry: ",iResource)	
		if iResource != -1
			switch resource[iResource].type
				case 101 //entity in area
					if i[resource[iResource].varStart.startI+1] = 0 //entity is ped
						tempPed = eventToPed(FIND_NTH_RESOURCE_ENTRY(i[resource[iResource].varStart.startI+2],0,2))
						if not IS_PED_INJURED(tempPed)
							if IS_ENTITY_AT_COORD(tempPed,v[resource[iResource].varStart.startV],<<f[resource[iResource].varStart.startF],f[resource[iResource].varStart.startF],10.0>>)
								return TRUE
							ENDIF
						ENDIF
					ELIF i[resource[iResource].varStart.startI+1] = 1 //entity is veh
						tempVeh = eventToVeh(FIND_NTH_RESOURCE_ENTRY(i[resource[iResource].varStart.startI+2],0,1))
						if IS_VEHICLE_DRIVEABLE(tempVeh)
							if IS_ENTITY_AT_COORD(tempVeh,v[resource[iResource].varStart.startV],<<f[resource[iResource].varStart.startF],f[resource[iResource].varStart.startF],10.0>>)
								return TRUE
							ENDIF
						ENDIF
					ELIF i[resource[iResource].varStart.startI+1] = 2 //entity is object
					ENDIF
				BREAK
				
				case 102 //trigger time in recording
					PRINTLN("102")	
					PRINTLN("resource entry for vehicle 0: ",FIND_NTH_RESOURCE_ENTRY(i[resource[iResource].varStart.startI+1],0,1))
					tempVeh = eventToVeh(FIND_NTH_RESOURCE_ENTRY(i[resource[iResource].varStart.startI+1],0,1))
					
					if IS_VEHICLE_DRIVEABLE(tempVeh)
						PRINTLN("veh does exist")
						if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(tempVeh)
							if GET_PLAYBACK_NUMBER_FOR_VEHICLE_VIA_RECID(tempVeh) = i[resource[iResource].varStart.startI+2]
								if GET_TIME_POSITION_IN_RECORDING(tempVeh) >= f[resource[iResource].varStart.startF]
									return TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					 //vehicle
				BREAK
				
				case 104 //check anim time
					iResourceB = FIND_NTH_RESOURCE_ENTRY(i[resource[iResource].varStart.startI+1],0,2) //resource entry for ped
					if i[resource[iResourceB].varStart.startI+1] != 0
						thisPed = int_to_native(ped_index,i[resource[iResourceB].varStart.startI+1])
						if not IS_PED_INJURED(thisPed)
							PRINTLN("ped alive")
							iResourceB = FIND_NTH_RESOURCE_ENTRY(i[resource[iResource].varStart.startI+2],0,10) //entry for anim dict
							if DOES_TEXT_WIDGET_EXIST(tw[resource[iResourceB].varStart.startTw])
								stemp = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iResourceB].varStart.startTw])
								PRINTLN("anim dict to check: ",stemp)
								if not Is_String_Null_Or_Empty(stemp)
									stempb = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iResource].varStart.startTw])
									PRINTLN("ani name: ",stempb)
									if not Is_String_Null_Or_Empty(stemp)
										if IS_ENTITY_PLAYING_ANIM(thisPed,stemp,stempb)
											PRINTLN("anim playing")
											b[resource[iResource].varStart.startb] = TRUE
											if f[resource[iResource].varStart.startF] != -1.0 
												if GET_ENTITY_ANIM_CURRENT_TIME(thisPed,stemp,stempb) >= f[resource[iResource].varStart.startf]
													return TRUE
												ENDIF
											ENDIF
										ELSE
											PRINTLN("anim finished playing")
											if b[resource[iResource].varStart.startb] = TRUE and f[resource[iResource].varStart.startF] = -1.0
												return TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				break
			ENDSWITCH
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

proc initScenarioWidget()
	PRINTLN("initScenarioWidget()")
	int j


	if not DOES_WIDGET_GROUP_EXIST(scenarioWidget)
		SET_CURRENT_WIDGET_GROUP(parentWidget)
		scenarioWidget = START_WIDGET_GROUP("Scenario Builder")
		ADD_WIDGET_BOOL("Add to scenario",bAddToScenario)
		ADD_WIDGET_BOOL("Play Scenario",bPlayScenario)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(parentWidget)
	ENDIF
	
		
	string sTxlbl 
	for j = 0 to iResourceCount-1
		sTxlbl=null
		if resource[j].menuType = 2
			SET_CURRENT_WIDGET_GROUP(scenarioWidget)
		
			if DOES_WIDGET_GROUP_EXIST(resource[j].widget)
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
					sTxlbl = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw])
					sTxlbl=sTxlbl
				ENDIF					
				DELETE_WIDGET_GROUP(resource[j].widget)
			ENDIF
			CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)
			SWITCH resource[j].type
				case 200 //not set
					SET_CURRENT_WIDGET_GROUP(scenarioWidget)
					resource[j].widget = START_WIDGET_GROUP("New Element")
					//resource[j].type = 200
					addResourceWidgetType(j,sTxlbl)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)					
					
				BREAK	
				case 201 //ped actions
					SET_CURRENT_WIDGET_GROUP(scenarioWidget)
					resource[j].widget = START_WIDGET_GROUP("Ped actions")
					addResourceWidgetType(j,sTxlbl)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)										
				BREAK
				CASE 202 // ped anims
					SET_CURRENT_WIDGET_GROUP(scenarioWidget)
					resource[j].widget = START_WIDGET_GROUP("Ped anim")
					addResourceWidgetType(j,sTxlbl)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)	
				BREAK
				case 203 //nvehicle actions
					SET_CURRENT_WIDGET_GROUP(scenarioWidget)
					resource[j].widget = START_WIDGET_GROUP("Veh actions")
					addResourceWidgetType(j,sTxlbl)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)										
				BREAK
				case 204 //entity follow bezier
					SET_CURRENT_WIDGET_GROUP(scenarioWidget)
					resource[j].widget = START_WIDGET_GROUP("Entity follow bezier")
					addResourceWidgetType(j,sTxlbl)
					STOP_WIDGET_GROUP()	
					CLEAR_CURRENT_WIDGET_GROUP(scenarioWidget)	
				BREAK
				
				case 205 //vehicle damage
				BREAK
				case 206 //apply force to entity
				BREAK
				case 207 //explosion
				BREAK
			ENDSWITCH			
		ENDIF						
	ENDFOR		
ENDPROC

PROC initWidgets()
	PRINTLN("initWidgets()")

	if not DOES_WIDGET_GROUP_EXIST(parentWidget)	
		parentWidget = START_WIDGET_GROUP("Scene Builder")
			ADD_WIDGET_BOOL("EXIT",bEXIT)		
			ADD_WIDGET_BOOL("dEBUG",bDebug)
			ADD_WIDGET_BOOL("Load data",bloadData)
			ADD_WIDGET_BOOL("Save data",bSaveData)
		STOP_WIDGET_GROUP()
		initResourceWidget()
		initTriggerWidget()
		initScenarioWidget()

	ENDIF
	
	v[0] = v[0]
	i[0] =i[0] 
	f[0] =f[0]

	b[0] =b[0]
	tw[0] =tw[0]
	
ENDPROC

proc COUNT_VARS(int iResEntry)
	PRINTLN("COUNT_VARS()")
	vCount=0
	fCount=0
	iCount=0

	bCount=0
	twCount=0
	modCount=0
	bJustCounting=true
	addResourceWidgetType(iResEntry)
	bJustCounting=FALSE
ENDPROC

proc GET_NEW_START_VALUES(int iResourceEntry)
	PRINTLN("GET_NEW_START_VALUES(",iResourceEntry,")")
	int j
	COUNT_VARS(iResourceEntry)	
	for j= iResourceEntry+1 to COUNT_OF(resource)-1
		resource[j].varStart.startV = resource[iResourceEntry].varStart.startV+vCount
		resource[j].varStart.startF = resource[iResourceEntry].varStart.startF+fCount
		resource[j].varStart.startI = resource[iResourceEntry].varStart.startI+iCount

		resource[j].varStart.startB = resource[iResourceEntry].varStart.startB+bCount
		resource[j].varStart.startTw = resource[iResourceEntry].varStart.startTw+twCount
		resource[j].varStart.startMod = resource[iResourceEntry].varStart.startMod+modCount
	ENDFOR
ENDPROC

proc AMEND_VAR_ENTRIES(int iEntry)
	int J
	COUNT_VARS(iEntry)
	if TwCount > 0
		for j = 0 to TwCount-1
			if DOES_TEXT_WIDGET_EXIST(tw[resource[iEntry].varStart.startTw+j])
				SET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iEntry].varStart.startTw+j],"")
			ENDIF
		ENDFOR
	ENDIF
	
	//find difference between existing var count and new var count, in order to add or remove vars accordingly.
	vCount = vCount - (resource[iEntry+1].varStart.startV - resource[iEntry].varStart.startV)
	fCount = fCount - (resource[iEntry+1].varStart.startF - resource[iEntry].varStart.startF)
	iCount = iCount - (resource[iEntry+1].varStart.startI - resource[iEntry].varStart.startI)
	bCount = bCount - (resource[iEntry+1].varStart.startB - resource[iEntry].varStart.startB)
	TwCount = TwCount - (resource[iEntry+1].varStart.startTw - resource[iEntry].varStart.startTw)		
	modCount = modCount - (resource[iEntry+1].varStart.startMod - resource[iEntry].varStart.startMod)

	
	if vCount > 0
		for j = COUNT_OF(v)-1 TO resource[iEntry+1].varStart.startV+vCount STEP -1
			IF j-vCount >= 0
				v[j] = v[j-vCount]
			ENDIF
		ENDFOR	
		for j = resource[iEntry].varStart.startv to resource[iEntry].varStart.startv+vCount-1
			v[j] = <<0,0,0>>
		ENDFOR
	ENDIF	
	if vCount < 0
		for j =  resource[iEntry+1].varStart.startV+vCount to COUNT_OF(v)-1
			if j - vCount < COUNT_OF(v)
				v[j] = v[j-vCount]
			ENDIF
		ENDFOR
	ENDIF
	
	// int i
	
	if iCount > 0
		
		for j = COUNT_OF(i)-1 TO resource[iEntry+1].varStart.startI+iCount STEP -1
			
			IF j-iCount >= 0
				
				i[j] = i[j-iCount]
			ENDIF			
		ENDFOR	
		for j = resource[iEntry].varStart.starti+1 to resource[iEntry].varStart.starti+iCount
			//don't reset the first entry for each widget as that stores the widget type.
			i[j] = 0
		ENDFOR
	ENDIF	
	if iCount < 0
		
		for j =  resource[iEntry+1].varStart.startI+iCount to COUNT_OF(i)-1
			if j - iCount < COUNT_OF(i)
				i[j] = i[j-iCount]
			ENDIF
		ENDFOR
	ENDIF

	//float f
	if fcount > 0
		for j = COUNT_OF(f)-1 TO resource[iEntry+1].varStart.startf+fCount STEP -1
			IF j-fcount >= 0
				f[j] = f[j-fcount]
			ENDIF			
		ENDFOR	
		for j = resource[iEntry].varStart.startf to resource[iEntry].varStart.startf+fCount-1
			f[j] = 0
		ENDFOR
	ENDIF	
	if fcount < 0
		for j =  resource[iEntry+1].varStart.startf+fCount to COUNT_OF(f)-1
			if j - fCount < COUNT_OF(f)
				f[j] = f[j-fcount]
			ENDIF
		ENDFOR
	ENDIF
	
	
	//bools
	PRINTLN("bcount for change ",bCount)
	PRINTLN("b[0] before change = ",b[0])
	PRINTLN("b[1] before change = ",b[1])
	PRINTLN("b[2] before change = ",b[2])
	PRINTLN("b[3] before change = ",b[3])
	PRINTLN("b[4] before change = ",b[4])
	PRINTLN("b[5] before change = ",b[5])
	if bcount > 0
		PRINTLN("NUDGE VALUES: for j = ",COUNT_OF(b)-1," to ",resource[iEntry+1].varStart.startb+bCount," step -1")
		for j = COUNT_OF(b)-1 TO resource[iEntry+1].varStart.startb+bCount STEP -1
			IF j-bcount >= 0
				PRINTLN("b[",j,"] = ",b[j-bCount])
				b[j] = b[j-bcount]
			ENDIF
		ENDFOR	
		for j = resource[iEntry].varStart.startB to resource[iEntry].varStart.startb+bCount
			PRINTLN("b[",j,"] = false")
			b[j] = FALSE
		ENDFOR
	ENDIF	
	if bcount < 0
		PRINTLN("SET TO ZERO: for j = ",resource[iEntry+1].varStart.startb+BCount," to ",COUNT_OF(b)-1)
		
		for j =  resource[iEntry+1].varStart.startB+BCount to COUNT_OF(b)-1
			IF j-BCount < COUNT_OF(B)
				PRINTLN("b[",j,"] = ",b[j-bcount])
				b[j] = b[j-bcount]
			ENDIF
		ENDFOR
	ENDIF
	PRINTLN("b[0] after change = ",b[0])
	PRINTLN("b[1] after change = ",b[1])
	PRINTLN("b[2] after change = ",b[2])
	PRINTLN("b[3] after change = ",b[3])
	PRINTLN("b[4] after change = ",b[4])
	PRINTLN("b[5] after change = ",b[5])
	
	//Text widgets
	if twCount > 0
		for j = COUNT_OF(tw)-1 TO resource[iEntry+1].varStart.startTw+twCount STEP -1			
			IF j-twCount >= 0
				tw[j] = tw[j-twCount]
			ENDIF
		ENDFOR		
		//reset newly created vars
		PRINTLN("RESET TEXT WIDGETS: for j = ",resource[iEntry].varStart.startTw," to ",resource[iEntry].varStart.startTw+twCount-1)
		for j = resource[iEntry].varStart.startTw to resource[iEntry].varStart.startTw+twCount-1			
			tw[j]=null
		ENDFOR
	ENDIF			
	if twcount < 0
		for j =  resource[iEntry+1].varStart.startTw+twCount to COUNT_OF(tw)-1
			IF j-twCount < COUNT_OF(tw)
				tw[j] = tw[j-twCount]
			ENDIF			
		ENDFOR
	ENDIF
	
	
	
	if modCount > 0		
		for j = COUNT_OF(mod)-1 TO resource[iEntry+1].varStart.startMod+modCount STEP -1
			IF j-modCount >= 0
				mod[j] = mod[j-modCount]
			ENDIF
		ENDFOR	
		//reset newly created models
		for j = resource[iEntry].varStart.startMod to resource[iEntry].varStart.startMod+modCount-1
			mod[j]=DUMMY_MODEL_FOR_SCRIPT
		ENDFOR
	ENDIF	
	if modcount < 0	
		for j =  resource[iEntry+1].varStart.startMod+modCount to COUNT_OF(mod)-1
			IF j-modCount < COUNT_OF(mod)
				mod[j] = mod[j-modCount]
			ENDIF
		ENDFOR
	ENDIF

	for j = iEntry+1 to COUNT_OF(resource)-1
		resource[j].varStart.startV += vCount
		resource[j].varStart.startf += fCount
		resource[j].varStart.starti += iCount
	
		resource[j].varStart.startB += bCount
		resource[j].varStart.startTw += TwCount
		resource[j].varStart.startMod += modCount
	ENDFOR

ENDPROC

proc CHECK_EXIT_SCRIPT()
	if bEXIT = TRUE
		DELETE_WIDGET_GROUP(parentWidget)
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC



func int GET_VEH_RESOURCE_ENTRY(int iEventCaller)
	int iCounter,j
	for j = 0 to COUNT_OF(resource)-1
		if resource[j].menuType = 0
			if resource[j].type = 1 //vehicle
				if iCounter = i[resource[iEventCaller].varStart.startI+3] //vehicel to use
					return j
				ENDIF
				iCounter++
			ENDIF
		ENDIF
	ENDFOR
	return -1
ENDFUNC

func ANIMATION_FLAGS get_anim_flag(int iValue)
	switch iValue
		case 0 RETURN AF_DEFAULT BREAK
		case 1 RETURN AF_EXIT_AFTER_INTERRUPTED BREAK
		case 2 RETURN AF_EXTRACT_INITIAL_OFFSET BREAK
		case 3 RETURN AF_HOLD_LAST_FRAME BREAK
		case 4 RETURN AF_IGNORE_GRAVITY BREAK
		case 5 RETURN AF_LOOPING BREAK
		case 6 RETURN AF_NOT_INTERRUPTABLE BREAK
		case 7 RETURN AF_OVERRIDE_PHYSICS BREAK
		case 8 RETURN AF_REORIENT_WHEN_FINISHED BREAK
		case 9 RETURN AF_REPOSITION_WHEN_FINISHED BREAK
		case 10 RETURN AF_SECONDARY BREAK
		case 11 RETURN AF_TURN_OFF_COLLISION BREAK
		case 12 RETURN AF_UPPERBODY BREAK		
	ENDSWITCH
	
	RETURN AF_DEFAULT
ENDFUNC

proc TRIGGER_THIS_EVENT(int ievent)
	int ji
	ANIMATION_FLAGS AF = AF_DEFAULT
	vehicle_index thisVeh	
	PED_INDEX thisPed
	MODEL_NAMES thisModel
	int iVehResourceEntry,iResource,iResourceB
	i[resource[ievent].varStart.startI+1] = 1 //track event triggered state = 1: playing back
	string sAnimDict,aAnimToPlay
	PRINTLN("b")
	switch resource[ievent].type
		
		case 201 //ped action
			iResource = FIND_NTH_RESOURCE_ENTRY(i[resource[ievent].varStart.startI+3],0,2) //resource entry for ped
			if i[resource[ievent].varStart.startI+4] = 0 //spawn	
				if i[resource[iResource].varStart.startI+1] != 0
					thisPed = int_to_native(ped_index,i[resource[iResource].varStart.startI+1])
					if DOES_ENTITY_EXIST(thisPed)
						DELETE_PED(thisPed)
					ENDIF
				ENDIF

				 GET_MODEL_FOR_PED(thisModel,GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iResource].varStart.startTw]))				 

				thisPed = CREATE_PED(PEDTYPE_MISSION,thisModel,v[resource[ievent].varStart.startV],f[resource[ievent].varStart.startF])				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed,TRUE)
				i[resource[iResource].varStart.startI+1] = NATIVE_TO_INT(thisPed)
			ELIF i[resource[ievent].varStart.startI+4] = 1 //go to coord
				thisPed = int_to_native(ped_index,i[resource[iResource].varStart.startI+1])
				if not IS_PED_INJURED(thisPed)
					if i[resource[ievent].varStart.startI+6] = 0
						TASK_GO_STRAIGHT_TO_COORD(thisPed,v[resource[ievent].varStart.startV],PEDMOVE_WALK)
					ELIF i[resource[ievent].varStart.startI+6] = 1
						TASK_GO_STRAIGHT_TO_COORD(thisPed,v[resource[ievent].varStart.startV],PEDMOVE_RUN)
					ELIF i[resource[ievent].varStart.startI+6] = 2
						TASK_GO_STRAIGHT_TO_COORD(thisPed,v[resource[ievent].varStart.startV],PEDMOVE_SPRINT)
					ENDIF						
				ENDIF
			ENDIF
		BREAK
		
		case 202 //ped anims
			iResource = FIND_NTH_RESOURCE_ENTRY(i[resource[ievent].varStart.startI+3],0,2) //resource entry for ped
			iResourceB = FIND_NTH_RESOURCE_ENTRY(i[resource[ievent].varStart.startI+4],0,10) //resource for anim dict
			
			sAnimDict = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[iResourceB].varStart.startTw])			
			aAnimToPlay = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[ievent].varStart.startTw])
			
			PRINTLN("sAnimDict: ",sAnimDict," aAnimToPlay: ",aAnimToPlay)
			
			thisPed = int_to_native(ped_index,i[resource[iResource].varStart.startI+1])
			if HAS_ANIM_DICT_LOADED(sAnimDict)
				if not IS_PED_INJURED(thisPed)
				
					if i[resource[ievent].varStart.startI+5] != 0
						for ji = 0 to i[resource[ievent].varStart.startI+5]-1
							AF += GET_ANIM_FLAG(i[resource[ievent].varStart.startI+6+ji])						
						ENDFOR
					ENDIF		
					PRINTLN("blend in: ",f[resource[ievent].varStart.startF+1])
					TASK_PLAY_ANIM(thisPed,sAnimDict,aAnimToPlay,f[resource[ievent].varStart.startF+1],f[resource[ievent].varStart.startF+2],-1,AF)
				ENDIF
			ELSE
				SCRIPT_ASSERT("SCRIPT: anim dict not pre loaded")
			ENDIF
		BREAK
		
		case 203 //vehicle action
			//get action to perform
		
			if i[resource[ievent].varStart.startI+4] = 0 //spawn vehicle
				iVehResourceEntry = GET_VEH_RESOURCE_ENTRY(ievent)
				thisVeh = eventToVeh(iVehResourceEntry)
				if DOES_ENTITY_EXIST(thisVeh)
					DELETE_VEHICLE(thisVeh)
				ENDIF
				thisVeh = CREATE_VEHICLE(mod[resource[iVehResourceEntry].varStart.startMod],v[resource[ievent].varStart.startV],f[resource[ievent].varStart.startF])
				VehToEvent(thisVeh,iVehResourceEntry)
			ELIF i[resource[ievent].varStart.startI+4] = 1 //play recording
				PRINTLN("c")
				iVehResourceEntry = GET_VEH_RESOURCE_ENTRY(ievent)
				thisVeh = eventToVeh(iVehResourceEntry)				
				if not DOES_ENTITY_EXIST(thisVeh)					
					PRINTLN("d")
					if i[resource[ievent].varStart.startI+6] != 0 and not Is_String_Null_Or_Empty(sVehRecording)						
						PRINTLN("e")
						if DOES_VEHICLE_RECORDING_FILE_EXIST_IN_CDIMAGE(i[resource[ievent].varStart.startI+6],sVehRecording)						
							PRINTLN("f")
							vTemp = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(i[resource[ievent].varStart.startI+6],f[resource[iEvent].varStart.startF+1],sVehRecording)
							thisVeh = CREATE_VEHICLE(mod[resource[iVehResourceEntry].varStart.startMod],GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(i[resource[ievent].varStart.startI+6],f[resource[iEvent].varStart.startF+1],sVehRecording),vtemp.z)
							VehToEvent(thisVeh,iVehResourceEntry)
						ENDIF
					ENDIF
				ENDIF	
				if IS_VEHICLE_DRIVEABLE(thisVeh)
					PRINTLN("g")
					if i[resource[ievent].varStart.startI+6] != 0 and not Is_String_Null_Or_Empty(sVehRecording)					
						PRINTLN("h")
						START_PLAYBACK_RECORDED_VEHICLE(thisVeh,i[resource[ievent].varStart.startI+6],sVehRecording)
						PRINTLN("playback skip: ",f[resource[iEvent].varStart.startF+1])
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisVeh,f[resource[iEvent].varStart.startF+1])
					ENDIF
				ENDIF
			ENDIF
		BREAK
		case 204 //entity follow bezier curve
			//find if entity is ped/vehicle/object
			if i[resource[ievent].varStart.startI+3] = 0 //entity is ped
				tempPed = eventToPed(FIND_NTH_RESOURCE_ENTRY(i[resource[ievent].varStart.startI+4],0,2))
				if not IS_PED_INJURED(tempPed)
					useBezier(i[resource[ievent].varStart.startI+5],i[resource[ievent].varStart.startI+7],i[resource[ievent].varStart.startI+6],vtemp,vTempb)
					SET_ENTITY_COORDS_NO_OFFSET(tempPed,vtemp)
					SET_ENTITY_ROTATION(tempPed,vtempb)
					i[resource[ievent].varStart.startI+7] += floor(TIMESTEP()*1000.0)
					if i[resource[ievent].varStart.startI+7] > i[resource[ievent].varStart.startI+6]
						i[resource[ievent].varStart.startI+1] = 2
					ENDIF
				ENDIF	
			ELIF i[resource[ievent].varStart.startI+2] = 1 //entity is veh
				tempVeh = eventToVeh(FIND_NTH_RESOURCE_ENTRY(i[resource[ievent].varStart.startI+4],0,1))
				if IS_VEHICLE_DRIVEABLE(tempVeh)
					
				ELIF i[resource[ievent].varStart.startI+1] = 2 //entity is object
				
				ENDIF
			
			ENDIF
		break
	ENDSWITCH
ENDPROC

proc CLEANUP_RESOURCE(int ievent)
	vehicle_index vehTemp
	switch resource[ievent].type
		case 1
			vehTemp = INT_TO_NATIVE(vehicle_index,i[resource[ievent].varStart.startI+1])
			if DOES_ENTITY_EXIST(vehTemp)
				if IS_VEHICLE_DRIVEABLE(vehTemp)
					if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTemp)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTemp)
					ENDIF
				ENDIF
				
				DELETE_VEHICLE(vehTemp)
				i[resource[ievent].varStart.startI+1] = 0
			ENDIF
		BREAK
		case 204 //reset bezier timer
			i[resource[ievent].varStart.startI+7] = 0
		BREAK
		case 103
			b[resource[ievent].varStart.startI] = FALSE
		BREAK
	ENDSWITCH	
ENDPROC

proc playbackScenario()
	int j
	int iTrigger
	if bPlayScenario = TRUE	
		for j = 0 to count_of(resource)-1	
			if resource[j].menuType = 2 //event			
				iTrigger = i[resource[j].varStart.startI+2]			
				if i[resource[j].varStart.startI+1] = 0				
					if bPlaybackTriggered = FALSE and iTrigger=-1						
						TRIGGER_THIS_EVENT(j)
					ENDIF
					if HAS_EVENT_TRIGGER_RULE_BEEN_MET(iTrigger)
						TRIGGER_THIS_EVENT(j)
					ENDIF
				ELIF i[resource[j].varStart.startI+1] = 1
					if resource[j].type = 204 //keep udating for bezier
						TRIGGER_THIS_EVENT(j)
					ENDIF					
				ENDIF
			ENDIF
		endfor
		bPlaybackTriggered = TRUE
	ELSE
		if bPlaybackTriggered = TRUE 
			bPlaybackTriggered = FALSE
			for j = 0 to count_of(resource)-1	
				if resource[j].menuType = 2 //event							
					i[resource[j].varStart.startI+1] = 0
					CLEANUP_RESOURCE(j)
				ENDIF
				if resource[j].menuType = 0 //resource	
					CLEANUP_RESOURCE(j)
				ENDIF
				if resource[j].menuType = 1 //resource	
					b[resource[j].varStart.startB] = FALSE
				ENDIF
			endfor
			
			
			
		ENDIF
	ENDIF
ENDPROC

proc INSERT_RESOURCE_INT_ENTRY(int resourceEntry)
	int j
	for j = COUNT_OF(i)-1 to resource[resourceEntry+1].varStart.startI+1 step-1
		i[j] = i[j-1]
	ENDFOR
	i[resource[resourceEntry+1].varStart.startI] = 0
	for j = resourceEntry+1 to COUNT_OF(resource)-1
		PRINTLN("resource[",j,"].varStart.startI = ",resource[j].varStart.startI)
		resource[j].varStart.startI++
		PRINTLN("resource[",j,"].varStart.startI = ",resource[j].varStart.startI)
	ENDFOR
ENDPROC

proc REBUILD_WIDGETS(int j, bool recountVars=TRUE)
	if recountVars
		GET_NEW_START_VALUES(j)						
	ENDIF
	initResourceWidget()
	initTriggerWidget()
	initScenarioWidget()
ENDPROC

proc manageResources()
	int j
	int ii
	MODEL_NAMES aModel
	ped_index aPed
	string sTemp
	float Ft
	float ftemp
	
	if bAddResource
		bAddResource = FALSE	
		resource[iResourceCount].menuType = 0		
		
		iResourceCount++
		REBUILD_WIDGETS(iResourceCount-1)
		//GET_NEW_START_VALUES(iResourceCount-1)						
		//initResourceWidget()
	ENDIF
	
	if bAddTrigger
		bAddTrigger = FALSE
		resource[iResourceCount].menuType = 1
		resource[iResourceCount].type = 100
		iResourceCount++
		//GET_NEW_START_VALUES(iResourceCount-1)
		//initTriggerWidget()
		REBUILD_WIDGETS(iResourceCount-1)
	ENDIF
	if bAddToScenario
		bAddToScenario = FALSE
		resource[iResourceCount].menuType = 2
		resource[iResourceCount].type = 200
		
		iResourceCount++
		//GET_NEW_START_VALUES(iResourceCount-1)
		//initScenarioWidget()	
		REBUILD_WIDGETS(iResourceCount-1)
	ENDIF
	
	
	
	vector vPlot, vNewPlot
	
	bool bMakeChange = FALSE
	
	VEHICLE_INDEX thisveh
	for j = 0 to iResourceCount-1
		if (resource[j].menuType = 0 and resource[j].type != i[resource[j].varStart.startI]) or (resource[j].menuType = 1 and resource[j].type-100 != i[resource[j].varStart.startI]) or (resource[j].menuType = 2 and resource[j].type-200 != i[resource[j].varStart.startI]) //type
			PRINTLN("******************************************************************************")
			PRINTLN("change in widget type for j = ",j)
			PRINTLN("Stored j type = ",resource[j].type," i[resource[j].varStart.startI] = ",i[resource[j].varStart.startI]," where resource[j].varStart.startI = ",resource[j].varStart.startI)
			
			if resource[j].menuType = 0 resource[j].type = i[resource[j].varStart.startI] ENDIF
			if resource[j].menuType = 1
				resource[j].type = i[resource[j].varStart.startI]+100			
			ENDIF
			if resource[j].menuType = 2
				resource[j].type = i[resource[j].varStart.startI]+200			
			ENDIF
			AMEND_VAR_ENTRIES(j)
		
			//update widget and variable entries
			bMakeChange = TRUE
		ENDIF						
		
		
		switch resource[j].type
			case 1 //vehicle
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
					if not Is_String_Null_Or_Empty(GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
						if IS_STRING_A_VEHICLE_MODEL(GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
							if mod[resource[j].varStart.startMod] = DUMMY_MODEL_FOR_SCRIPT
								set_model_for_Vehicle(mod[resource[j].varStart.startMod],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
								REQUEST_MODEL(mod[resource[j].varStart.startMod])
							ELSE
								if not are_strings_equal(GET_MODEL_NAME_FOR_DEBUG(mod[resource[j].varStart.startMod]),GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
									set_model_for_Vehicle(mod[resource[j].varStart.startMod],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
									REQUEST_MODEL(mod[resource[j].varStart.startMod])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//check spawn vehicle
				if b[resource[j].varStart.startB] = TRUE
					b[resource[j].varStart.startB] = FALSE
					if IS_MODEL_A_VEHICLE(mod[resource[j].varStart.startMod])
						if not IS_PED_INJURED(PLAYER_PED_ID())
							if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								thisveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								if IS_VEHICLE_DRIVEABLE(thisveh)
									SET_ENTITY_COORDS(PLAYER_PED_ID(),GET_ENTITY_COORDS(thisveh))
									DELETE_VEHICLE(thisveh)
								ENDIF
							ENDIF
							REQUEST_MODEL(mod[resource[j].varStart.startMod])
							while not HAS_MODEL_LOADED(mod[resource[j].varStart.startMod])	
								PRINTLN("Loading model")
								WAIT(0)
							ENDWHILE
							if not IS_PED_INJURED(PLAYER_PED_ID())
								i[resource[j].varStart.startI+1] = native_to_int(CREATE_VEHICLE(mod[resource[j].varStart.startMod],GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0,0.0,1.0>>))
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),INT_TO_NATIVE(VEHICLE_INDEX,i[resource[j].varStart.startI+1]))
							ENDIF
						ENDIF
					ENDIF
				ENDIF

			BREAK
			case 2 //ped
			/*
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
					if not Is_String_Null_Or_Empty(GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))						
						if IS_STRING_A_PED(GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
						
							
						
							if mod[resource[j].varStart.startMod] = DUMMY_MODEL_FOR_SCRIPT
								set_model_for_Vehicle(mod[resource[j].varStart.startMod],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
								REQUEST_MODEL(mod[resource[j].varStart.startMod])
							ELSE
								if not are_strings_equal(GET_MODEL_NAME_FOR_DEBUG(mod[resource[j].varStart.startMod]),GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
									set_model_for_Vehicle(mod[resource[j].varStart.startMod],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
									REQUEST_MODEL(mod[resource[j].varStart.startMod])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF*/
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
					if not Is_String_Null_Or_Empty(GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))						
						GET_MODEL_FOR_PED(aModel,GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))	
						if IS_MODEL_A_PED(aModel)
							if mod[resource[j].varStart.startMod] = DUMMY_MODEL_FOR_SCRIPT
								mod[resource[j].varStart.startMod] = aModel
								REQUEST_MODEL(aModel)
							ELSE
								if not are_strings_equal(GET_MODEL_NAME_FOR_DEBUG(mod[resource[j].varStart.startMod]),GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
									mod[resource[j].varStart.startMod] = aModel
									REQUEST_MODEL(aModel)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//check spawn vehicle
				if b[resource[j].varStart.startB] = TRUE
					b[resource[j].varStart.startB] = FALSE
					aPed = INT_TO_NATIVE(ped_index,i[resource[j].varStart.startI+1])
					if DOES_ENTITY_EXIST(aPed)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(aPed))
						DELETE_PED(aPed)
					ENDIF
					if GET_MODEL_FOR_PED(aModel,GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))						
						REQUEST_MODEL(aModel)
						while not HAS_MODEL_LOADED(aModel)	
							WAIT(0)
							PRINTLN("Loading model b")
						ENDWHILE
						if not IS_PED_INJURED(PLAYER_PED_ID())
							aPed = CREATE_PED(pedtype_mission,aModel,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,2.0,0.0>>))								
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(aPed,TRUE)
							i[resource[j].varStart.startI+1] = NATIVE_TO_INT(aPed)
						ENDIF
					
					ENDIF
				ENDIF
			BREAK
			case 3 //force
				if b[resource[j].varStart.startB+3] = TRUE
					if i[resource[j].varStart.startI+3] = 0
						i[resource[j].varStart.startI+3] = i[resource[j].varStart.startI+2] + GET_GAME_TIMER()
					ENDIF
					if GET_GAME_TIMER()<=i[resource[j].varStart.startI+3]						
						if not IS_PED_INJURED(PLAYER_PED_ID())
							if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								thisveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								if IS_VEHICLE_DRIVEABLE(thisveh)
									APPLY_FORCE_TO_ENTITY(thisveh,INT_TO_ENUM(APPLY_FORCE_TYPE,i[resource[j].varStart.startI+1]),v[resource[j].varStart.startv],v[resource[j].varStart.startv+1],0,b[resource[j].varStart.startb],b[resource[j].varStart.startb+1],b[resource[j].varStart.startb+2])
								ENDIF
							ELSE
								APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(),INT_TO_ENUM(APPLY_FORCE_TYPE,i[resource[j].varStart.startI+1]),v[resource[j].varStart.startv],v[resource[j].varStart.startv+1],0,b[resource[j].varStart.startb],b[resource[j].varStart.startb+1],b[resource[j].varStart.startb+2])
							ENDIF
						ENDIF
					ELSE
						b[resource[j].varStart.startB+3] = FALSE
						i[resource[j].varStart.startI+3] = 0
					ENDIF
				ENDIF
			BREAK
			case 4 //damage
				if b[resource[j].varStart.startB] = TRUE 
					b[resource[j].varStart.startB] = FALSE
					if not IS_PED_INJURED(PLAYER_PED_ID())
						if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							thisveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							if IS_VEHICLE_DRIVEABLE(thisveh)
								SET_VEHICLE_DAMAGE(thisveh,v[resource[j].varStart.startV],f[resource[j].varStart.startf],f[resource[j].varStart.startf+1],b[resource[j].varStart.startB+1])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 5 //bezier
				if i[resource[j].varStart.startI+1] != i[resource[j].varStart.startI+2] //stored no of points != no of points in widget
					i[resource[j].varStart.startI+2] = i[resource[j].varStart.startI+1]
					REBUILD_WIDGETS(j)
				ENDIF
				if b[resource[j].varStart.startB] //draw bez line
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
					for ii = 0 to i[resource[j].varStart.startI+1]-1
						DRAW_DEBUG_BOX(v[resource[j].varStart.startV+(ii*3)]-<<0.5,0.5,0.5>>,v[resource[j].varStart.startV+(ii*3)]+<<0.5,0.5,0.5>>,255,0,0,128)
						DRAW_DEBUG_BOX(v[resource[j].varStart.startV+(ii*3)]+v[resource[j].varStart.startV+1+(ii*3)]-<<0.35,0.35,0.35>>,v[resource[j].varStart.startV+(ii*3)]+v[resource[j].varStart.startV+1+(ii*3)]+<<0.35,0.35,0.35>>,255,255,0,128)
						DRAW_DEBUG_LINE(v[resource[j].varStart.startV+(ii*3)],v[resource[j].varStart.startV+(ii*3)]+v[resource[j].varStart.startV+1+(ii*3)],255,0,255,80)
						
						if i[resource[j].varStart.startI+1] > 1 and ii < i[resource[j].varStart.startI+1]-1
						 	getBezCoord(j,ii,0,vPlot) // getBezCoord(resource[j].varStart.startV,ii,0,vPlot)
							
							ft = 0.1
							while ft <= 0.95
								getBezCoord(j,ii,ft,vNewPlot)
								DRAW_DEBUG_LINE(vplot,vnewplot,0,0,255,128)
								vplot=vnewplot
								ft+= 0.1
							ENDWHILE
							DRAW_DEBUG_LINE(vplot,v[resource[j].varStart.startV+(ii*3)+3],0,0,255,128)
						ENDIF
					ENDFOR					
				ENDIF
				if b[resource[j].varStart.startB+1]
					 b[resource[j].varStart.startB+1] = FALSE
					v[resource[j].varStart.startV+(i[resource[j].varStart.startI+3]*3)] = GET_CAM_COORD(GET_DEBUG_CAM())
					v[resource[j].varStart.startV+2+(i[resource[j].varStart.startI+3]*3)] = GET_CAM_ROT(GET_DEBUG_CAM())					 
				ENDIF
				if b[resource[j].varStart.startB+2]
					 b[resource[j].varStart.startB+2] = FALSE
					v[resource[j].varStart.startV+1+(i[resource[j].varStart.startI+3]*3)] = GET_CAM_COORD(GET_DEBUG_CAM()) - v[resource[j].varStart.startV+(i[resource[j].varStart.startI+3]*3)]										 
				ENDIF
			BREAK
			case 7 //veh recording
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])				
					if Is_String_Null_Or_Empty(sVehRecording)					
						sVehRecording = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw])
					ELSE
						if not ARE_STRINGS_EQUAL(sVehRecording,GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
							sVehRecording = null
						ENDIF
					ENDIF
					if b[resource[j].varStart.startB] = TRUE //trigger recording
						//b[resource[j].varStart.startB] = FALSE
						if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							thisveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							if IS_VEHICLE_DRIVEABLE(thisveh)
								if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisveh)
									if DOES_VEHICLE_RECORDING_FILE_EXIST_IN_CDIMAGE(i[resource[j].varStart.startI+1],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
										REQUEST_VEHICLE_RECORDING(i[resource[j].varStart.startI+1],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
										while not HAS_VEHICLE_RECORDING_BEEN_LOADED(i[resource[j].varStart.startI+1],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
											REQUEST_VEHICLE_RECORDING(i[resource[j].varStart.startI+1],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
											PRINTLN("has veh rec loaded ",i[resource[j].varStart.startI+1],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
											WAIT(0)
										ENDWHILE
										if IS_VEHICLE_DRIVEABLE(thisveh)
											START_PLAYBACK_RECORDED_VEHICLE(thisveh,i[resource[j].varStart.startI+1],GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw]))
										ENDIF
									ENDIF
								ENDIF
							ENDIF								
						ENDIF
					ELSE
						if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							thisveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							if IS_VEHICLE_DRIVEABLE(thisveh)
								if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisveh)
									STOP_PLAYBACK_RECORDED_VEHICLE(thisveh)
								ENDIF
							ENDIF								
						ENDIF
					ENDIF
				ENDIF
			BREAK
			case 8
				if i[resource[j].varStart.startI+1] != i[resource[j].varStart.startI+2]
					i[resource[j].varStart.startI+2] = i[resource[j].varStart.startI+1]
					tempModel = INT_TO_ENUM(model_names,i[resource[j].varStart.startI+1])
					if IS_MODEL_IN_CDIMAGE(tempModel)
						REQUEST_MODEL(tempModel)
						while not HAS_MODEL_LOADED(tempModel)
							PRINTLN("Load model")
							REQUEST_MODEL(tempModel)
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
				if b[resource[j].varStart.startB]
					b[resource[j].varStart.startB]=FALSE
					tempModel = INT_TO_ENUM(model_names,i[resource[j].varStart.startI+1])
					if IS_MODEL_IN_CDIMAGE(tempModel)
						if HAS_MODEL_LOADED(tempModel)
							if not IS_PED_INJURED(player_ped_id())
								if DOES_ENTITY_EXIST(INT_TO_NATIVE(object_index,i[resource[j].varStart.startI+3]))
									tempObj = INT_TO_NATIVE(object_index,i[resource[j].varStart.startI+3])
									DELETE_OBJECT(tempObj)
								ENDIF
								i[resource[j].varStart.startI+3] = native_to_int(CREATE_OBJECT(tempModel,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,1.5,0.0>>)))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			case 9 //cams
				if i[resource[j].varStart.startI+1] != i[resource[j].varStart.startI+2] //stored no of points != no of points in widget
					i[resource[j].varStart.startI+2] = i[resource[j].varStart.startI+1]
					REBUILD_WIDGETS(j)
				ENDIF
				if i[resource[j].varStart.startI+1] = 0 //static 
					if b[resource[j].varStart.startB+1] //position to debug
						b[resource[j].varStart.startB+1] = FALSE
						v[resource[j].varStart.startV] = GET_CAM_COORD(GET_DEBUG_CAM())
						v[resource[j].varStart.startV+1] = GET_CAM_ROT(GET_DEBUG_CAM())	
					ENDIF
				ELIF i[resource[j].varStart.startI+1] = 1 //pan
					if b[resource[j].varStart.startB+1] //position to debug
						b[resource[j].varStart.startB+1] = FALSE
						v[resource[j].varStart.startV] = GET_CAM_COORD(GET_DEBUG_CAM())
						v[resource[j].varStart.startV+1] = GET_CAM_ROT(GET_DEBUG_CAM())	
					ENDIF
					if b[resource[j].varStart.startB+2] //position to debug
						b[resource[j].varStart.startB+2] = FALSE
						v[resource[j].varStart.startV+2] = GET_CAM_COORD(GET_DEBUG_CAM())
						v[resource[j].varStart.startV+3] = GET_CAM_ROT(GET_DEBUG_CAM())	
					ENDIF
				ENDIF
				
				if b[resource[j].varStart.startB] = TRUE
					if not DOES_CAM_EXIST(aCam)
						aCam = CREATE_CAMERA()
					
						iCamTime = GET_GAME_TIMER()+ i[resource[j].varStart.startI+3]
						if i[resource[j].varStart.startI+1] = 0 //static
							SET_CAM_COORD(aCam,v[resource[j].varStart.startV])
							SET_CAM_ROT(aCam,v[resource[j].varStart.startV+1])
							SET_CAM_ACTIVE(aCam,true)
						ELIF i[resource[j].varStart.startI+1] = 1 //pan
							bCam = CREATE_CAMERA()
							SET_CAM_COORD(aCam,v[resource[j].varStart.startV])
							SET_CAM_ROT(aCam,v[resource[j].varStart.startV+1])
							SET_CAM_COORD(bCam,v[resource[j].varStart.startV+2])
							SET_CAM_ROT(bCam,v[resource[j].varStart.startV+3])
							SET_CAM_ACTIVE_WITH_INTERP(bCam,acam,i[resource[j].varStart.startI+3])
						ELIF i[resource[j].varStart.startI+1] = 2 //bezier
						itempb=0
						for itemp = 0 to COUNT_OF(resource)-1
							if resource[itemp].type = 5 //bezier
								if itempb = i[resource[j].varStart.startI+4]
									i[resource[j].varStart.startI+5] = itemp
									itemp = COUNT_OF(resource)									
								ELSE
									itempb++
								ENDIF								
							ENDIF
						ENDFOR
						itemp = i[resource[j].varStart.startI+5]
							SET_CAM_COORD(aCam,v[resource[itemp].varStart.startV])
							SET_CAM_ROT(aCam,v[resource[itemp].varStart.startV+1])
							SET_CAM_ACTIVE(aCam,true)
						ENDIF
						
						RENDER_SCRIPT_CAMS(true,FALSE)
					ENDIF
						
						
						
					if GET_GAME_TIMER() < iCamTime
						if i[resource[j].varStart.startI+1] = 2 //bezier
							useBezier(i[resource[j].varStart.startI+5],GET_GAME_TIMER() - (iCamTime - i[resource[j].varStart.startI+3]),i[resource[j].varStart.startI+3],vTemp,vTempB)
							//itemp = currentTime
	//itemp =  //3 = total duration
	//ftemp = to_float(itemp)/(TO_FLOAT(i[resource[j].varStart.startI+3])/TO_FLOAT(i[resource[itempb].varStart.startI+1]-1))
	//itemp = FLOOR(ftemp)
							
							SET_CAM_COORD(aCam,vTemp)	
							SET_CAM_ROT(aCam,vTempb)
						ENDIF
					ELSE
						RENDER_SCRIPT_CAMS(FALSE,FALSE)
						SET_CAM_ACTIVE(aCam,FALSE)
						DESTROY_CAM(aCam)
						if DOES_CAM_EXIST(bCam)
							DESTROY_CAM(bCam)
						ENDIF
						b[resource[j].varStart.startB] = FALSE
					ENDIF
				ELSE
					if DOES_CAM_EXIST(aCam)
						RENDER_SCRIPT_CAMS(FALSE,FALSE)
						SET_CAM_ACTIVE(aCam,FALSE)
						DESTROY_CAM(aCam)
						if DOES_CAM_EXIST(bCam)
							DESTROY_CAM(bCam)
						ENDIF
					ENDIF
				ENDIF
			break
			
			case 10 //anim dictFget
				//load anim dict if required
				if DOES_TEXT_WIDGET_EXIST(tw[resource[j].varStart.startTw])
					stemp = GET_CONTENTS_OF_TEXT_WIDGET(tw[resource[j].varStart.startTw])					
					if not Is_String_Null_Or_Empty(stemp)
						if not HAS_ANIM_DICT_LOADED(stemp)
							REQUEST_ANIM_DICT(stemp)
							
							PRINTLN("************************ request anim dict ",stemp," **************************************")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			case 100 //Triggers
			
			BREAK
			
			case 101 //entity in area
				if b[resource[j].varStart.startB] = TRUE
					b[resource[j].varStart.startB] = FALSE
					if not IS_PED_INJURED(PLAYER_PED_ID())
						v[resource[j].varStart.startV] = GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0,0.0,1.0>>
					ENDIF					
				ENDIF
				if b[resource[j].varStart.startB+1] = TRUE
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					ftemp = f[resource[j].varStart.startF]
					DRAW_DEBUG_BOX(v[resource[j].varStart.startV]-<<ftemp,ftemp,3>>,v[resource[j].varStart.startV]+<<ftemp,ftemp,3>>,0,0,255,100)
				ENDIF	
			BREAK
			
			case 201 //ped actions
			
				if i[resource[j].varStart.startI+4] != i[resource[j].varStart.startI+5]
					i[resource[j].varStart.startI+5] = i[resource[j].varStart.startI+4]
					REBUILD_WIDGETS(j)
				ENDIF
			
				if i[resource[j].varStart.startI+4] = 0 //spawn
					if b[resource[j].varStart.startB] //set coords from player
						b[resource[j].varStart.startB] = FALSE
						if not IS_PED_INJURED(PLAYER_PED_ID())
							v[resource[j].varStart.startV] = GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0,0.0,1.0>>
							f[resource[j].varStart.startF] = GET_ENTITY_HEADING(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ELIF i[resource[j].varStart.startI+4] = 1 //go to coord
					if b[resource[j].varStart.startB] //set coords from player
						b[resource[j].varStart.startB] = FALSE
						if not IS_PED_INJURED(PLAYER_PED_ID())
							v[resource[j].varStart.startV] = GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0,0.0,1.0>>
							f[resource[j].varStart.startF] = GET_ENTITY_HEADING(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			BREAK
/*
			case 201 //create ped and give actions
				//trigger
				addint(i[resource[iEntry].varStart.startI+2],"Trigger (-1 = on Start)",-1,30,1)
				//action
				addint(i[resource[iEntry].varStart.startI+3],"Ped to use",0,20,1)
				addWidgetCombo(i[resource[iEntry].varStart.startI+4],"Action","Spawn","Go to coord")
				addIntStorage() //store previous state of above
				if i[resource[iEntry].varStart.startI+4] = 0 //spawn
					addBool(b[resource[iEntry].varStart.startB],"Set spawn pos from player")
					addVector(v[resource[iEntry].varStart.startV],"Spawn pos",-8000.0,8000.0,0.1)
					addFloat(f[resource[iEntry].varStart.startF],"Heading",-180.0,180.0,0.1)
				ELIF i[resource[iEntry].varStart.startI+4] = 1 //go to coord
					addVector(v[resource[iEntry].varStart.startV],"Go to coord",-8000.0,8000.0,0.1)
					addWidgetCombo(i[resource[iEntry].varStart.startI+6],"Speed","Walk","Run","Sprint")					
				ENDIF				
			BREAK
*/			
			case 202 //anims	
				if b[resource[j].varStart.startB+1]
					b[resource[j].varStart.startB+1]=FALSE
					i[resource[j].varStart.startI+5]++
					REBUILD_WIDGETS(j)
				//if i[resource[j].varStart.startI+6] != 0
					
				//	if not IS_BIT_set(i[resource[j].varStart.startI+5],i[resource[j].varStart.startI+6])
				//		SET_BIT(i[resource[j].varStart.startI+5],i[resource[j].varStart.startI+6])
				//		INSERT_RESOURCE_INT_ENTRY(j)
						
				//	ENDIF
			
					
				ENDIF
						
			BREAK
			
			case 203 //veh actions
				if i[resource[j].varStart.startI+4] != i[resource[j].varStart.startI+5] //stored no of points != no of points in widget
					i[resource[j].varStart.startI+5] = i[resource[j].varStart.startI+4]
					REBUILD_WIDGETS(j)
				ENDIF
				if i[resource[j].varStart.startI+4] = 0 //spawn
					if b[resource[j].varStart.startB] //set coords from player
						b[resource[j].varStart.startB] = FALSE
						if not IS_PED_INJURED(PLAYER_PED_ID())
							v[resource[j].varStart.startV] = GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0,0.0,1.0>>
						ENDIF
					ENDIF
				ELIF i[resource[j].varStart.startI+4] = 1 //veh rec
					if not Is_String_Null_Or_Empty(sVehRecording)
						if i[resource[j].varStart.startI+6] != i[resource[j].varStart.startI+7]
							if i[resource[j].varStart.startI+7] != 0
								if DOES_VEHICLE_RECORDING_FILE_EXIST_IN_CDIMAGE(i[resource[j].varStart.startI+7],sVehRecording)
									REMOVE_VEHICLE_RECORDING(i[resource[j].varStart.startI+7],sVehRecording)									
								ENDIF
							ENDIF
							if DOES_VEHICLE_RECORDING_FILE_EXIST_IN_CDIMAGE(i[resource[j].varStart.startI+6],sVehRecording)
								REQUEST_VEHICLE_RECORDING(i[resource[j].varStart.startI+6],sVehRecording)
								while not HAS_VEHICLE_RECORDING_BEEN_LOADED(i[resource[j].varStart.startI+6],sVehRecording)
									PRINTLN("has veh rec loaded b")
									WAIT(0)
								ENDWHILE															
							ENDIF
							i[resource[j].varStart.startI+7] = i[resource[j].varStart.startI+6]
						ENDIF
					ENDIF							
				ENDIF
			BREAK
		ENDSWITCH		
		
	ENDFOR
	

	
	if bMakeChange = TRUE
	
		initResourceWidget()
		initTriggerWidget()
		initScenarioWidget()
	

		PRINTLN("finished revising widgets")
		PRINTLN("i[1] = ",i[1])
		PRINTLN("------------------------------------------------------------------------------")
	ENDIF
	
ENDPROC

PROC loadData()
	bool bReachedTw=FALSE
	int j, eachNode

	filename = path
	filename += sfilename
	filename += ".xml"
	
	LOAD_XML_FILE(filename)
	

	iResourceCount=0
	vCount=0
	iCount=0
	fCount=0
	modCount=0
	twCount=0
	bCount=0
	
	IF GET_NUMBER_OF_XML_NODES() <> 0
		FOR eachNode = 0 TO (GET_NUMBER_OF_XML_NODES()-1)
			if are_strings_equal(GET_XML_NODE_NAME(),"PlayerCoord")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_vX
							vTemp.x = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break	
						case HASH_vY
							vTemp.y = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
						case HASH_vZ
							vTemp.z = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
					ENDSWITCH								
				ENDFOR	
				if not IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),vTemp)
				ENDIF
			ENDIF
			
			if are_strings_equal(GET_XML_NODE_NAME(),"Resource")
				PRINTLN("loadresource")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
				
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_menuType
							resource[iResourceCount].menuType = GET_INT_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
						case HASH_subType
							resource[iResourceCount].type = GET_INT_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
						case HASH_startV 
							resource[iResourceCount].varStart.startV = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)  																 
						break
						case HASH_startF 
							resource[iResourceCount].varStart.startF = GET_INT_FROM_XML_NODE_ATTRIBUTE(j) 		
							PRINTLN("Load data: resource[",iResourceCount,"].varStart.startF = ",resource[iResourceCount].varStart.startF)
						break
						case HASH_startI 
							resource[iResourceCount].varStart.startI = GET_INT_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
						case HASH_startB 
							resource[iResourceCount].varStart.startB = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)																 
						break
						CASE HASH_startTw 
							resource[iResourceCount].varStart.startTw = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						BREAK
						case HASH_startMod
							resource[iResourceCount].varStart.startMod = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						BREAK
					ENDSWITCH								
				ENDFOR	
				iResourceCount++
			ENDIF
			
		
			if are_strings_equal(GET_XML_NODE_NAME(),"saveVector")
				PRINTLN("loadvector")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
			
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_vX
							v[vCount].x = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break	
						case HASH_vY
							v[vCount].y = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
						case HASH_vZ
							v[vCount].z = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break
					ENDSWITCH								
				ENDFOR	
				vCount++
			ENDIF
	
			if are_strings_equal(GET_XML_NODE_NAME(),"saveFloat")
				PRINTLN("loadfloat")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1			
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_f
							f[fCount] = GET_float_FROM_XML_NODE_ATTRIBUTE(j) 	
							PRINTLN("f[",fCount,"] = ",f[fCount])
						break	
					ENDSWITCH								
				ENDFOR	
				fCount++
			ENDIF
	
			if are_strings_equal(GET_XML_NODE_NAME(),"saveInt")
				PRINTLN("loadint")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_i
							i[iCount] = GET_int_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break	
					ENDSWITCH								
				ENDFOR	
				iCount++
			ENDIF
			
			if are_strings_equal(GET_XML_NODE_NAME(),"saveBool")
				PRINTLN("loadbool")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_b
							b[bCount] = GET_bool_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break	
					ENDSWITCH								
				ENDFOR	
				bCount++
			ENDIF
			/*
			if are_strings_equal(GET_XML_NODE_NAME(),"saveTw")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_tw
							tw[twCount] = GET_bool_FROM_XML_NODE_ATTRIBUTE(j) 																 
						break	
					ENDSWITCH								
				ENDFOR	
				twCount++
			ENDIF
			*/
			if are_strings_equal(GET_XML_NODE_NAME(),"saveMod")
				PRINTLN("loadMod")
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_mod
							itemp = GET_int_FROM_XML_NODE_ATTRIBUTE(j)
							mod[modCount] = INT_TO_ENUM(model_names,itemp)
						break	
					ENDSWITCH								
				ENDFOR	
				modCount++
			ENDIF
			
			if are_strings_equal(GET_XML_NODE_NAME(),"saveTw")
				PRINTLN("Loading text widget data")
				if breachedtw = FALSE
					breachedtw=TRUE
					if DOES_WIDGET_GROUP_EXIST(resourceWidget) DELETE_WIDGET_GROUP(resourceWidget) ENDIF
					if DOES_WIDGET_GROUP_EXIST(TriggerWidget) DELETE_WIDGET_GROUP(TriggerWidget) ENDIF
					if DOES_WIDGET_GROUP_EXIST(scenarioWidget) DELETE_WIDGET_GROUP(scenarioWidget) ENDIF
					REBUILD_WIDGETS(iResourceCount-1)
					twCount=0
				ENDIF
				FOR j = 0 to GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1
	
					SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						case HASH_tw
						
							//tempstring = GET_string_FROM_XML_NODE_ATTRIBUTE(j)
							tempTl = GET_string_FROM_XML_NODE_ATTRIBUTE(j)
							if DOES_TEXT_WIDGET_EXIST(tw[twCount])
								PRINTLN("tw[",twCount,"]=",tempTl)
								SET_CONTENTS_OF_TEXT_WIDGET(tw[twCount],tempTl)
							ENDIF 																 
						break	
					ENDSWITCH								
				ENDFOR	
				twCount++
			ENDIF
			
			if eachNode < GET_NUMBER_OF_XML_NODES()-1
				GET_NEXT_XML_NODE()	
			ENDIF
		ENDFOR
	ENDIF		
	
	

				
	DELETE_XML_FILE()
	
ENDPROC

proc checkLoadData()
	if bLoadData
		loadData()
		bLoadData = FALSE
	ENDIF
ENDPROC

proc debug()
	int j
	j=j
	
	if bDebug = TRUE
	
		for j = 0 to 10			
			showIntOnScreen(0.1,0.1+(j*0.03),resource[j].varStart.startV,"startv ")
			showIntOnScreen(0.25,0.1+(j*0.03),resource[j].varStart.starti,"starti ")
			showIntOnScreen(0.4,0.1+(j*0.03),resource[j].varStart.startf,"startf ")
			showIntOnScreen(0.55,0.1+(j*0.03),resource[j].varStart.startB,"startb ")
			showIntOnScreen(0.7,0.1+(j*0.03),resource[j].varStart.startTw,"starttw ")
		//for j = 0 to iResourceCount-1
			
		//ENDFOR
		ENDFOR
	ENDIF
	
ENDPROC

proc sceneBuilder()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		manageResources()
		playbackScenario()
		CHECK_EXIT_SCRIPT()
		checkSaveData()
		checkLoadData()
		debug()
	
	ENDIF
ENDPROC

#ENDIF


