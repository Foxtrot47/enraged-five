// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___                                                                                      
// ___                    	Author: Adam Westwoood & Ross Wallace
// ___								Date: 07/12/2009                                             
// _________________________________________________________________________________________
// ___                                                                                        
// ___                                  Overlay Effects                                                                                                         
// ___                   	A collection of analogue and digital 
// ___         		  									
// ___                                                                                                                                                   
// ___                                
// ___               			Notes: Placeholder effects	                                                          
// ___                                                                                   
// _________________________________________________________________________________________

// ____________________________________ INCLUDES ___________________________________________


USING "commands_graphics.sch"

CONST_FLOAT videoCameraZoomMax 45.0
CONST_FLOAT videoCameraZoomMin 10.00

BOOL bFlipHoriz
BOOL bFlipVert
BOOL bInterlaceFlip
BOOL bFXHasFadedIn
BOOL bHasRecFadedIn = FALSE

INT iNoSignal
INT iGrime
INT iGrimeLastValue
INT iFXTimer
INT iFXRandom
INT iRecAlpha = 0

FLOAT scuzzYpos1
FLOAT scuzzYpos2
FLOAT scuzzYpos3

INT iOverlaysAssigned


ENUM LOAD_OVERLAY_ENUM
	FX_LOAD_SCANLINES,
	FX_LOAD_NO_SIGNAL,
	FX_LOAD_STATIC,
	FX_LOAD_SCUZZ,
	FX_LOAD_SET_SECURITY_CAM,
	FX_LOAD_SET_LOSING_SIGNAL
ENDENUM


ENUM TEXTURE_OVERLAY_LIST
	TX_NSCANLINE1,
	TX_NSCANLINE2,
	TX_NSCANLINE3,
	TX_STATIC1,
	TX_STATIC2,
	TX_STATIC3,
	TX_STATIC4,
	TX_STATIC5,
	TX_FATSCAN1,
	TX_SIGNAL1,
	TX_SIGNAL2,
	TX_SIGNAL3,
	TX_SIGNAL4,
	TX_LINE1,
	TX_LINE2,
	TX_LIGHT_LEAK1,
	TX_LIGHT_LEAK2
ENDENUM


PROC MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TEXTURE_OVERLAY_LIST e_text)

	IF ENUM_TO_INT(e_text) < 32
		SET_BIT(iOverlaysAssigned, ENUM_TO_INT(e_text))
	ELSE
		SCRIPT_ASSERT("EXTEND TEXTURE ASSIGNED ARRAY!- SEE ADAM W")
	ENDIF
ENDPROC


FUNC BOOL HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TEXTURE_OVERLAY_LIST e_text)
	IF IS_BIT_SET(iOverlaysAssigned, ENUM_TO_INT(e_text))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
	

//Load all the overlay effects
FUNC BOOL LOAD_OVERLAY_EFFECTS()
	REQUEST_STREAMED_TEXTURE_DICT("digitalOverlay")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("digitalOverlay")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
	
	
// flipping and
FUNC INT GET_RANDOM_GRIME_VALUES(INT iMaxTexture)

	IF GET_RANDOM_INT_IN_RANGE(0,50) > 25
		bFlipHoriz = TRUE
	ELSE
		bFlipHoriz = FALSE
	ENDIF
	
	IF GET_RANDOM_INT_IN_RANGE(0,50) > 25
		bFlipVert = TRUE
	ELSE
		bFlipVert = FALSE
	ENDIF
	
	RETURN GET_RANDOM_INT_IN_RANGE(0, iMaxTexture)
	
ENDFUNC
	
	
FUNC FLOAT FLIP_IF_TRUE(BOOL bFlipThis)

	IF bFlipThis
		RETURN (-1.0)
	ELSE
		RETURN 1.0
	ENDIF
	
	RETURN 1.0
	
ENDFUNC

	
//////////////////////////////////
//DIGITAL EFFECTS
//////////////////////////////////

//Draw Scan lines on the screen
PROC OVERLAY_SCAN_LINES(BOOL bEnableScanLines,INT iScanLineAlpha = 200)
	IF NOT LOAD_OVERLAY_EFFECTS()
		LOAD_OVERLAY_EFFECTS()
	ELSE
		IF bEnableScanLines
			//HUD EFFECTS 
			IF bInterlaceFlip
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_FATSCAN1)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_FATSCAN1)
				ELSE
					DRAW_SPRITE("digitalOverlay","nscanline1", 0.5,0.5,1.0,1.0,0.0,255,255,255,iScanLineAlpha)
				ENDIF
				bInterlaceFlip = FALSE
			ELSE
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_FATSCAN1)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_FATSCAN1)
				ELSE
					DRAW_SPRITE("digitalOverlay","nscanline1", 0.5,0.5,-1.0,-1.0,0.0,255,255,255,iScanLineAlpha)
				ENDIF
				bInterlaceFlip = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//Draw the no signal overlay to the screen, use with noise and scuzz lines for full effect
PROC OVERLAY_NO_SIGNAL(BOOL bEnableNoSignal,INT iNoSignalAlpha = 255)
	IF NOT LOAD_OVERLAY_EFFECTS(FX_LOAD_NO_SIGNAL)
		LOAD_OVERLAY_EFFECTS(FX_LOAD_NO_SIGNAL)
	ELSE
		IF bEnableNoSignal  = TRUE
			iNoSignal = GET_RANDOM_INT_IN_RANGE(0,4)
			IF iNoSignal = 0
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_SIGNAL1)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_SIGNAL1)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Signal1", 0.5,0.5,1.0,1.0,0.0,255,255,255,iNoSignalAlpha)	
				ENDIF
			ENDIF
			
			IF iNoSignal = 1
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_SIGNAL2)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_SIGNAL2)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Signal2", 0.5,0.5,1.0,1.0,0.0,255,255,255,iNoSignalAlpha)	
				ENDIF
			ENDIF
			
			IF iNoSignal = 2
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_SIGNAL3)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_SIGNAL3)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Signal3", 0.5,0.5,1.0,1.0,0.0,255,255,255,iNoSignalAlpha)	
				ENDIF
			ENDIF
			
			IF iNoSignal = 3
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_SIGNAL4)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_SIGNAL4)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Signal4", 0.5,0.5,1.0,1.0,0.0,255,255,255,iNoSignalAlpha)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//Draw grime effect on the screen.
PROC OVERLAY_STATIC(BOOL bEnableStatic,INT iStaticStrength = 26)
	IF NOT LOAD_OVERLAY_EFFECTS()
		LOAD_OVERLAY_EFFECTS()
	ELSE
		IF bEnableStatic
			iGrime = GET_RANDOM_GRIME_VALUES(5)
			iNoSignal = GET_RANDOM_INT_IN_RANGE(0,4)
			WHILE iGrimeLastValue = iGrime
				iGrime = GET_RANDOM_GRIME_VALUES(5)
			ENDWHILE
			
			IF iGrime = 0
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_STATIC1)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_STATIC1)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Static1", 0.5,0.5,1.0*FLIP_IF_TRUE(bFlipHoriz),1.0*FLIP_IF_TRUE(bFlipVert),0.0,255,255,255,iStaticStrength)
				ENDIF
			ENDIF
			
			IF iGrime = 1
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_STATIC2)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_STATIC2)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Static2", 0.5,0.5,1.0*FLIP_IF_TRUE(bFlipHoriz),1.0*FLIP_IF_TRUE(bFlipVert),0.0,255,255,255,iStaticStrength)
				ENDIF
			ENDIF
			
			IF iGrime = 2
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_STATIC3)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_STATIC3)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Static3", 0.5,0.5,1.0*FLIP_IF_TRUE(bFlipHoriz),1.0*FLIP_IF_TRUE(bFlipVert),0.0,255,255,255,iStaticStrength)
				ENDIF
			ENDIF
			
			IF iGrime = 3
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_STATIC4)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_STATIC4)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Static4", 0.5,0.5,1.0*FLIP_IF_TRUE(bFlipHoriz),1.0*FLIP_IF_TRUE(bFlipVert),0.0,255,255,255,iStaticStrength)
				ENDIF
			ENDIF
			
			IF iGrime = 4
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_STATIC5)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_STATIC5)
				ELSE
					DRAW_SPRITE("digitalOverlay", "Static5", 0.5,0.5,1.0*FLIP_IF_TRUE(bFlipHoriz),1.0*FLIP_IF_TRUE(bFlipVert),0.0,255,255,255,iStaticStrength)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC


//Draw scuzz effects to the screen
PROC OVERLAY_SCUZZ(BOOL bEnableScuzz, INT iScuzzAlpha = 128, BOOL bEnabledScuzz1 = TRUE,BOOL bEnabledScuzz2= TRUE,BOOL bEnabledScuzz3= TRUE)
	IF NOT LOAD_OVERLAY_EFFECTS()
		LOAD_OVERLAY_EFFECTS()
	ELSE
		IF bEnableScuzz 
			IF bEnabledScuzz1
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_NSCANLINE1)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_NSCANLINE1)
				ELSE
					DRAW_SPRITE("digitalOverlay","nscuzz1", 0.5,scuzzYpos1,1.0,0.2,0.0,255,255,255,iScuzzAlpha)
				ENDIF

				IF scuzzYpos1 < 1.0
					scuzzYpos1 += 0.01	
				ELSE
					scuzzYpos1 = 0.0
				ENDIF
			ENDIF
				
			IF bEnabledScuzz2
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_NSCANLINE2)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_NSCANLINE2)
				ELSE
					DRAW_SPRITE("digitalOverlay", "nscuzz2", 0.5,scuzzYpos2,1.0,0.2,0.0,255,255,255,iScuzzAlpha)
				ENDIF
				
				IF scuzzYpos2 < 1.0
					scuzzYpos2 += 0.02
				ELSE
					scuzzYpos2 = 0.0
				ENDIF
			ENDIF
				
			IF bEnabledScuzz3
				IF NOT HAS_TEXTURE_OVERLAY_BEEN_ASSIGNED(TX_NSCANLINE3)
					MARK_TEXTURE_OVERLAY_AS_ASSIGNED(TX_NSCANLINE3)
				ELSE
					DRAW_SPRITE("digitalOverlay", "nscuzz3", 0.5,scuzzYpos3,1.0,0.2,0.0,255,255,255,iScuzzAlpha)
				ENDIF
				
				IF scuzzYpos3 < 1.0
					scuzzYpos3 += 0.028	
				ELSE
					scuzzYpos3 = 0.0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


////Enables and sets the game noise value, default value is moderate screen noise
//PROC OVERLAY_GAME_NOISE(BOOL bEnableGameNoise,FLOAT fNoise = 0.38)
//	SET_NOISEOVERIDE(bEnableGameNoise)
//	SET_NOISINESSOVERIDE(fNoise)
//ENDPROC


/// PURPOSE:
/// To be called every frame, enables the security camera overlay. Ensure you call LOAD_OVERLAY_EFFECTS(FX_LOAD_SET_SECURITY_CAM) before you call this.
/// PARAMS:
/// bEnableSecurityCam - Enable or disable the camera
/// bEnableLowTech - If enabled allows the display of scuzz lines
/// iMaxStatic, Sets the maximum alpha of static level. (MAX 255) - Default 30
/// iMinStatic, Sets the mimimum alpha of static level. (MIN 0)   - Default 5
/// bShowRec, Displays the recording symbol, - Default - FALSE
///   
PROC OVERLAY_SET_SECURITY_CAM(BOOL bEnableSecurityCam,BOOL bEnableLowTech = FALSE, INT iMaxStatic = 30,  INT iMinStatic = 5, BOOL bShowRec = FALSE)
	IF NOT LOAD_OVERLAY_EFFECTS()
		LOAD_OVERLAY_EFFECTS()
	ELSE	
		IF bEnableSecurityCam
			IF bShowRec = TRUE
				IF iRecAlpha <= 225 AND bHasRecFadedIn = FALSE
					iRecAlpha = iRecAlpha + 30
					
				 	//DRAW_SPRITE(txdRec, 0.5,0.5,1.0,1.0,0.0,255,255,255,iRecAlpha)
					IF iRecAlpha >= 225
						bHasRecFadedIn = TRUE
					ENDIF
				ELSE
					IF iRecAlpha >= 30
						iRecAlpha = iRecAlpha - 30
						//DRAW_SPRITE(txdRec, 0.5,0.5,1.0,1.0,0.0,255,255,255,iRecAlpha)
						IF iRecAlpha <= 30
							bHasRecFadedIn = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			OVERLAY_SCAN_LINES(TRUE)		
		
			IF iFXTimer <= iMaxStatic AND bFXHasFadedIn = FALSE
				iFXTimer ++
				OVERLAY_STATIC(TRUE,iFXTimer)
				IF iFXTimer = iMaxStatic
					bFXHasFadedIn = TRUE
					iFXRandom = GET_RANDOM_INT_IN_RANGE(iMinStatic,iMaxStatic)
				ENDIF
			ELSE
				IF iFXTimer > 0
					IF iFXTimer > iFXRandom
						iFXTimer --
						OVERLAY_STATIC(TRUE,iFXTimer)
						IF iFXTimer = iFXRandom
							bFXHasFadedIn = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bEnableLowTech 
				OVERLAY_SCUZZ(TRUE,40)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//Clean up all the effects
PROC CLEAN_UP_OVERLAYS()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("digitalOverlay")
ENDPROC
