// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   ply_to_ply_calls.sch
//      CREATED         :   Conor McGuire
//      DESCRIPTION     :   Contains common functions for player to player calls used in MP and SP
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
USING "globals.sch"
USING "cellphone_public.sch"
USING "dialogue_public.sch"

CONST_INT PLY_TO_PLY_CALL_RESPONSE_ACCEPTED     1
CONST_INT PLY_TO_PLY_CALL_RESPONSE_ENGAGED      2
CONST_INT PLY_TO_PLY_CALL_RESPONSE_REJECTED     3

FUNC BOOL CAN_PLAYER_TALK_TO_PLAYER(GAMER_HANDLE &gamerhandle)

    /*
    IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Player not in playing state.")
        PRINTNL()
        RETURN FALSE
    ENDIF 
    */                   


    IF g_bInMultiplayer
        IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() = FALSE //#1601899
            
            PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE as player is in MP but has not completed tutorial.")
            PRINTNL()

            RETURN FALSE

        ENDIF

        IF IS_NET_PLAYER_OK(PLAYER_ID())
            IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
                IF GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()) >= 1.0

                    PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE as player is in MP and underwater. #1472049")
                    PRINTNL()

                    RETURN FALSE

                ENDIF
            ENDIF
        ENDIF

    ENDIF



    IF NETWORK_IS_CLOUD_AVAILABLE() = FALSE
    
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE as cloud is unavailable. #1627099")
        PRINTNL()

        RETURN FALSE

    ENDIF 



    //Criteria here.. 
    IF voiceSession.bInVoiceSession //Put this back in Sunday 1.9.13
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER - bInVoiceSession = TRUE")
        PRINTNL()
        //RETURN FALSE //But this always returns, correctly now //Put this back in Sunday 1.9.13
    ENDIF


    IF g_b_PreloadOngoing = TRUE //Potential fix for 1618431
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Preload Ongoing")
        PRINTNL()
        RETURN FALSE
    ENDIF
        

    IF g_b_On_TEAM_RACE
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - on TEAM RACE")
        PRINTNL()
        RETURN FALSE
    ENDIF

    IF voiceSession.bBusyThisFrame
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Busy this frame!")
        PRINTNL()
        RETURN FALSE
    ENDIF
    IF IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Cellphone disabled or disabled this frame only.")
        PRINTNL()
        RETURN FALSE
    ENDIF
    IF   g_OnMissionState != MISSION_TYPE_FRIEND_ACTIVITY
    and  g_OnMissionState != MISSION_TYPE_FRIEND_SQUAD
    and  g_OnMissionState != MISSION_TYPE_EXILE
    and  g_OnMissionState != MISSION_TYPE_OFF_MISSION
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - On some form of mission 1.")
        PRINTNL()
        RETURN FALSE
    ENDIF   
    IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
    and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE)
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - On some form of mission 2.")
        PRINTNL()
        RETURN FALSE
    ENDIF
    
    IF IS_MOBILE_PHONE_CALL_ONGOING()
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - IS_MOBILE_PHONE_CALL_ONGOING.")
        PRINTNL()
        RETURN FALSE
    ENDIF
    IF IS_CUTSCENE_ACTIVE()
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Cutscene Active!")
        PRINTNL()
        RETURN FALSE
    ENDIF



    IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Under 17 account!")
        PRINTNL()
        RETURN FALSE
    ENDIF


    if IS_PAUSE_MENU_ACTIVE()
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Pause Menu Active!")
        PRINTNL()
        RETURN FALSE
    endif

    if g_bResultScreenDisplaying
        PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - Result Screen Displaying!")
        PRINTNL()
        RETURN FALSE
    endif

    IF NETWORK_IS_SESSION_ACTIVE()
        IF NOT NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
            PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - No net Headset!")
            PRINTNL()
            RETURN FALSE
        ENDIF
    ENDIF
    
    IF NOT NETWORK_IS_SIGNED_ONLINE()
        RETURN FALSE
    ENDIF
	
	IF NETWORK_IS_GAMER_MUTED_BY_ME(gamerhandle)
    OR NETWORK_IS_GAMER_BLOCKED_BY_ME(gamerhandle)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_AM_I_MUTED_BY_GAMER(gamerhandle)
    OR NETWORK_AM_I_BLOCKED_BY_GAMER(gamerhandle)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_PLATFORM_PARTY()
		IF IS_XBOX_PLATFORM()
			IF NETWORK_IS_IN_PLATFORM_PARTY_CHAT()
				 PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - in party chat xbox one")
				RETURN FALSE
			ENDIF
		ELIF IS_PLAYSTATION_PLATFORM()
			 PRINTLN("CAN_PLAYER_TALK_TO_PLAYER returning FALSE - in party PS4")
			RETURN FALSE
		ENDIF
	ENDIF
	
    
    RETURN TRUE
ENDFUNC

PROC CLEAR_VOICE_SESSION_STRUCT()
    PRINTLN("CLEAR_VOICE_SESSION_STRUCT: called from script: ", GET_THIS_SCRIPT_NAME())
    voiceSession.bInVoiceSession = FALSE
    voiceSession.bRequestedVoiceSession = FALSE
    voiceSession.bClearVoiceSession = FALSE
    voiceSession.iScriptResponse = -1
    voiceSession.iCodeResponse = -1
    voiceSession.gamerName = ""
	voiceSession.displayName[0] = ""
	voiceSession.bAwaitingEndEvent = FALSE
	voiceSession.bInitDisplayName = FALSE
	voiceSession.bReturnedDisplayName = FALSE
	voiceSession.iDisplayRequest = -2
	voiceSession.iIncomingPhonecallStage = 0
    CLEAR_GAMER_HANDLE_STRUCT(voiceSession.gamerHandle)
ENDPROC

PROC SCRIPT_START_A_VOICE_SESSION()
    PRINTLN("SCRIPT_START_A_VOICE_SESSION: called from script: ", GET_THIS_SCRIPT_NAME())
    NETWORK_SESSION_VOICE_HOST()
    voiceSession.bRequestedVoiceSession = TRUE
    voiceSession.bClearVoiceSession = FALSE
    voiceSession.iScriptResponse = -1
    voiceSession.iCodeResponse = -1
    voiceSession.bAwaitingEndEvent = FALSE
ENDPROC

PROC SCRIPT_END_A_VOICE_SESSION()
    PRINTLN("SCRIPT_END_A_VOICE_SESSION: called from script: ", GET_THIS_SCRIPT_NAME())
    voiceSession.bClearVoiceSession = TRUE
	voiceSession.bAwaitingEndEvent = TRUE
    IF NETWORK_IS_SIGNED_ONLINE()
        IF NETWORK_SESSION_IS_IN_VOICE_SESSION()



            #if IS_DEBUG_BUILD

                cdPrintString ("Proc SCRIPT_END_A_VOICE_SESSION called. Will try to leave voice session...")
                cdPrintnl()
                cdPrintnl()

            #endif


            WHILE NETWORK_SESSION_IS_VOICE_SESSION_BUSY()

                WAIT (0)

                #if IS_DEBUG_BUILD

                    cdPrintString ("Trying to end voice session but it is still reporting that it's busy!")
                    cdPrintnl()
     
                #endif

            ENDWHILE


            #if IS_DEBUG_BUILD

                cdPrintString ("Proc SCRIPT_END_A_VOICE_SESSION  - Left voice session.")
                cdPrintnl()
                cdPrintnl()

            #endif


            NETWORK_SESSION_VOICE_LEAVE()   //This should terminate the voice session EVENT_VOICE_CONNECTION_TERMINATED 
                                            //and PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT should set bVoiceSession to false.
            voiceSession.bRequestedVoiceSession = FALSE
            voiceSession.bClearVoiceSession = FALSE



        ELSE 
            IF IS_GAMER_HANDLE_VALID(voiceSession.gamerHandle)
                IF NETWORK_IS_FRIEND(voiceSession.gamerHandle)
                    IF NOT NETWORK_IS_FRIEND_HANDLE_ONLINE(voiceSession.gamerHandle)
						PRINTLN("SCRIPT_END_A_VOICE_SESSION: friend not online")
                        CLEAR_VOICE_SESSION_STRUCT()
                    ENDIF
                ENDIF
            ELSE
				PRINTLN("SCRIPT_END_A_VOICE_SESSION: not valid handle")
                CLEAR_VOICE_SESSION_STRUCT()
            ENDIF
        ENDIF
    ELSE
//      IF NETWORK_SESSION_IS_IN_VOICE_SESSION()
//          NETWORK_SESSION_VOICE_LEAVE()
//          PRINTLN("SCRIPT_END_A_VOICE_SESSION: calling NETWORK_SESSION_VOICE_LEAVE")
//      ENDIF
		PRINTLN("SCRIPT_END_A_VOICE_SESSION: not signed in")
        CLEAR_VOICE_SESSION_STRUCT()
    ENDIF


    IF voiceSession.bConnectedCallOnGoing

        #if IS_DEBUG_BUILD

            cdPrintString ("PLY_TO_PLY calls reports that bConnectedCallOnGoing is still TRUE! - Hanging up phone as a precaution.")
            cdPrintnl()
            cdPrintnl()

        #endif


        HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
    ENDIF

    voiceSession.bConnectedCallOnGoing = FALSE

    voiceSession.iIncomingPhonecallStage = 0   //Added for work on 1602347

    //g_IsThisAnMPChatCall = FALSE //This has been moved to Cellphone_flashhand for 2111525. It was required so that the hands free handler is definitely reset.

    
    #if IS_DEBUG_BUILD

        //Was CHAT_CALL assignment 1
        cdPrintString ("SCRIPT_END_A_VOICE_SESSION() concluded...  2111525")
        cdPrintnl()
        cdPrintnl()

    #endif

    //This might be dangerous as it means the player can keep on attempting voice sessions before the terminate event has come through.   
    //CLEAR_VOICE_SESSION_STRUCT() //Precautionary measure for #1622767 to make sure this definitely gets done when ending a voice session? Moved to cellphone_controller


           
ENDPROC




PROC PROCESS_VOICE_SESSION_STARTED_EVENT()

    PRINTLN("PROCESS_VOICE_SESSION_STARTED_EVENT setting player in voice session")

    voiceSession.bInVoiceSession = TRUE

    voiceSession.iScriptResponse = -1
    voiceSession.iCodeResponse = -1

    #if IS_DEBUG_BUILD
    
        cdPrintString ("Ply_to_Ply calls - PROCESS_VOICE_SESSION_STARTED_EVENT has set bInVoiceSession = TRUE")
        cdPrintnl()
        cdPrintnl()

    #endif

    IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) = TRUE

        //Adding this on a trial basis to prevent quitting before PROCESS_VOICE_SESSION_STARTED_EVENT() has fired. 1602347
        IF g_b_ToggleButtonLabels
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_202") //"END CALL" - Negative

                IF g_Use_Prologue_Cellphone //No End Call in prologue regardless of labels. #844402
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                    6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                ENDIF

        ELSE
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
        ENDIF


    ENDIF

ENDPROC




PROC PROCESS_VOICE_SESSION_ENDED_EVENT()
    PRINTLN("PROCESS_VOICE_SESSION_END_EVENT setting player is not in a voice session")
    CLEAR_VOICE_SESSION_STRUCT()
ENDPROC







PROC PROCESS_VOICE_CONNECTION_REQUESTED_EVENT(INT iCount)
    
    STRUCT_CONNECTION_REQUEST_EVENT sei

    NET_PRINT("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT - Received event")NET_NL()

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        NET_PRINT("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT  - Successfully got event data")NET_NL()
        NET_PRINT_STRINGS("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT received call from: ", sei.szGamerTag) NET_NL()
        IF voiceSession.iIncomingPhonecallStage = 0
            IF NOT CAN_PLAYER_TALK_TO_PLAYER(sei.hGamer)
                //PRINT_HELP("BOYAH")
                NETWORK_SESSION_VOICE_RESPOND_TO_REQUEST(FALSE, PLY_TO_PLY_CALL_RESPONSE_ENGAGED)
                PRINTLN("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT - player can't talk responding busy")
            ELSE
                voiceSession.iIncomingPhonecallStage = 1
                voiceSession.gamerName = sei.szGamerTag
                voiceSession.gamerHandle = sei.hGamer
				voiceSession.displayName[0] = ""
				voiceSession.iDisplayRequest = -2
				voiceSession.bInitDisplayName = FALSE
				voiceSession.bReturnedDisplayName = FALSE
                PRINTLN("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT: voiceSession.iIncomingPhonecallStage = 0 Setting voiceSession.iIncomingPhonecallStage = 1 for ", voiceSession.gamerName)
            ENDIF
        ELSE
            IF NOT CAN_PLAYER_TALK_TO_PLAYER(sei.hGamer)
                //PRINT_HELP("BOYAH")
                NETWORK_SESSION_VOICE_RESPOND_TO_REQUEST(FALSE, PLY_TO_PLY_CALL_RESPONSE_ENGAGED)
                PRINTLN("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT - player can't talk responding busy")
            ELSE
                voiceSession.iIncomingPhonecallStage = 1
                voiceSession.gamerName = sei.szGamerTag
                voiceSession.gamerHandle = sei.hGamer
				voiceSession.displayName[0] = ""
				voiceSession.iDisplayRequest = -2
				voiceSession.bInitDisplayName = FALSE
				voiceSession.bReturnedDisplayName = FALSE
                PRINTLN("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT Setting voiceSession.iIncomingPhonecallStage = 1 for ", voiceSession.gamerName)
            ENDIF
        ENDIF
    ELSE
        NET_SCRIPT_ASSERT("PROCESS_VOICE_CONNECTION_REQUESTED_EVENT  - could not get event data!")
    ENDIF
ENDPROC

PROC PROCESS_VOICE_CONNECTION_RESPONSE_EVENT(INT iCount)
    
    STRUCT_CONNECTION_RESPONSE_EVENT sei

    NET_PRINT("PROCESS_VOICE_CONNECTION_RESPONSE_EVENT - Received event")NET_NL()

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        
        NET_PRINT("PROCESS_VOICE_CONNECTION_RESPONSE_EVENT  - Successfully got event data")NET_NL()
        //added 1 as 0 is valid response and I want to wait for a non-zero response
        voiceSession.iScriptResponse = sei.nReponseCode
        voiceSession.iCodeResponse =  ENUM_TO_INT(sei.nResponse)
        
        PRINTLN("voiceSession.iScriptResponse = ", voiceSession.iScriptResponse)
        PRINTLN("voiceSession.iCodeResponse = ", voiceSession.iCodeResponse)

    ELSE
        NET_SCRIPT_ASSERT("PROCESS_VOICE_CONNECTION_RESPONSE_EVENT  - could not get event data!")
    ENDIF
ENDPROC




PROC PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT(INT iCount)
    STRUCT_CONNECTION_TERMINATED_EVENT sei

    NET_PRINT("PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT - Received event")NET_NL()


    IF g_bInMultiplayer

        PLAY_SOUND_FRONTEND (-1, "Hang_Up", "Phone_SoundSet_Michael")

    ELSE
    
        PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)

    ENDIF

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        
        PRINTLN("SCRIPT_END_A_VOICE_SESSION: PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT received")
        SCRIPT_END_A_VOICE_SESSION()
        NET_PRINT("PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT  - Successfully got event data")NET_NL()
        //added 1 as 0 is valid response and I want to wait for a non-zero response
    ELSE
        NET_SCRIPT_ASSERT("PROCESS_EVENT_VOICE_CONNECTION_TERMINATED_EVENT - could not get event data!")
    ENDIF
    voiceSession.bInVoiceSession = FALSE
    voiceSession.iScriptResponse = -1
    voiceSession.iCodeResponse = -1
    voiceSession.gamerName = ""
    CLEAR_GAMER_HANDLE_STRUCT(voiceSession.gamerHandle)
ENDPROC




PROC SET_PLAYER_BUSY_FOR_PLAYER_CALL_THIS_FRAME()
    voiceSession.bBusyNextFrame = TRUE
ENDPROC




FUNC BOOL IS_VOICE_SESSION_ONGOING()
    if voiceSession.iIncomingPhonecallStage != 0
    OR voiceSession.bRequestedVoiceSession
    OR voiceSession.bInVoiceSession
        RETURN TRUE
    ENDIF
    RETURN FALSE    
ENDFUNC

FUNC BOOL GET_PLAYER_DISPLAY_NAME_FOR_PHONECALLS(GAMER_HANDLE& gamerHandle)
	IF NOT voiceSession.bInitDisplayName
		voiceSession.bReturnedDisplayName = FALSE
		GAMER_HANDLE displayGamer[1]
		displayGamer[0] = gamerHandle
		voiceSession.iDisplayRequest = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(displayGamer,1)
		PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS getting display name for incoming call")
		voiceSession.bInitDisplayName = TRUE
		RETURN FALSE
	ELSE
		IF NOT voiceSession.bReturnedDisplayName
			INT iResult = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(voiceSession.iDisplayRequest,voiceSession.displayName,1)
			IF iResult = 0
				//already filled
			ELIF iResult = -1
				voiceSession.displayName[0] = voiceSession.gamerName
			ELSE
				 PRINTLN("IS_INITIAL_CALL_PROCESSING_COMPLETE: waiting for display name request: iResult = ",iResult)
				 RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	voiceSession.bReturnedDisplayName = TRUE
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_INCOMING_PLAYER_CALLS()
    //IF NOT bAddedTempPedStruct
    //  ADD_PED_FOR_DIALOGUE(tempPedStruct ,3,NULL,"MP_FM_BOUNTY")
    //  bAddedTempPedStruct = TRUE
    //ENDIF
    IF voiceSession.bRequestedVoiceSession
        IF voiceSession.bClearVoiceSession
            IF NETWORK_IS_SIGNED_ONLINE()
                IF NETWORK_SESSION_IS_IN_VOICE_SESSION()
                    NETWORK_SESSION_VOICE_LEAVE()  
                    voiceSession.bClearVoiceSession = FALSE
                    PRINTLN("SCRIPT_END_A_VOICE_SESSION: voiceSession.bClearVoiceSession")
                    SCRIPT_END_A_VOICE_SESSION()
                //ELSE
                    //PRINTLN("Player requested voice session, now clear session, but not in a voice session")
                ENDIF
//          ELSE
//              voiceSession.bClearVoiceSession = FALSE
//              PRINTLN("SCRIPT_END_A_VOICE_SESSION: voiceSession.bClearVoiceSession")
//              SCRIPT_END_A_VOICE_SESSION()
            ENDIF
        ENDIF
    ENDIF
//  IF voiceSession.bInVoiceSession
//      IF NOT IS_BIT_SET(BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
//          IF IS_CELLPHONE_ON_HOMESCREEN()
//
//               LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
//                                              7, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) // Other
//
//                                          SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
//
//
//          ENDIF
//      ELSE
//          IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT))
//              PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)
//              PRINTLN("SCRIPT_END_A_VOICE_SESSION: Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT))")
//              SCRIPT_END_A_VOICE_SESSION()
//          ENDIF
//      ENDIF
//  ELSE
//      IF IS_BIT_SET(BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
//          IF IS_CELLPHONE_ON_HOMESCREEN()
//
//              LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
//                                                  7, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) // Other
//
//                                              CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
//
//
//          ENDIF
//
//      ENDIF
//  ENDIF
    

 


    IF voiceSession.bConnectedCallOnGoing
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT))
            PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)
            PRINTLN("SCRIPT_END_A_VOICE_SESSION: Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT))")
            SCRIPT_END_A_VOICE_SESSION()
        ENDIF
        IF NOT NETWORK_IS_SIGNED_ONLINE()
            PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS: killing voice call local player is not online")
            SCRIPT_END_A_VOICE_SESSION()
        ELSE
            IF IS_GAMER_HANDLE_VALID(voiceSession.gamerHandle)
                IF NETWORK_IS_FRIEND(voiceSession.gamerHandle)
                    IF NOT NETWORK_IS_FRIEND_HANDLE_ONLINE(voiceSession.gamerHandle)
                        PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS: killing voice call ", voiceSession.gamerName, " is not online")
                        SCRIPT_END_A_VOICE_SESSION()
                    ENDIF
                ENDIF
            ELSE
                PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS: killing voice call gamer handle isn't valid")
                SCRIPT_END_A_VOICE_SESSION()
            ENDIF
        ENDIF



        //IF g_bInMultiplayer

            //Fix for TCR #1650388
            
            IF NETWORK_IS_GAMER_MUTED_BY_ME(voiceSession.gamerHandle)
            OR NETWORK_IS_GAMER_BLOCKED_BY_ME(voiceSession.gamerHandle)
            //OR g_DumpDisableEveryFrameCaller //include this to test.
                        
                PRINTLN("SCRIPT_END_A_VOICE_SESSION: Player who created this log has either blocked or muted the remote gamer during on ongoing call. ")
                SCRIPT_END_A_VOICE_SESSION()

            ENDIF

            IF NETWORK_AM_I_MUTED_BY_GAMER(voiceSession.gamerHandle)
            OR NETWORK_AM_I_BLOCKED_BY_GAMER(voiceSession.gamerHandle)

                PRINTLN("SCRIPT_END_A_VOICE_SESSION: Player who created this log has been blocked or muted by remote gamer during on ongoing call. ")
                SCRIPT_END_A_VOICE_SESSION()

            ENDIF


            //NETWORK_IS_GAMER_IN_MY_SESSION(GAMER_HANDLE& gamerHandle)

        //ENDIF





    ENDIF

    SWITCH voiceSession.iIncomingPhonecallStage
        CASE 1
            IF CAN_PLAYER_TALK_TO_PLAYER(voiceSession.gamerHandle)
				IF IS_XBOX_PLATFORM()
					IF GET_PLAYER_DISPLAY_NAME_FOR_PHONECALLS(voiceSession.gamerHandle)
		                IF INITIATE_INCOMING_CHAT_CALL(voiceSession.displayName[0])
		                    voiceSession.iIncomingPhonecallStage = 2
							PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS- moving to stage 2 ")
		                ELSE
		                    PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS - waiting for INITIATE_INCOMING_CHAT_CALL")
		                ENDIF
					ELSE
						PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS - waiting for GET_PLAYER_DISPLAY_NAME_FOR_PHONECALLS")
					ENDIF
				ELSE
	                IF INITIATE_INCOMING_CHAT_CALL(voiceSession.gamerName)
	                    voiceSession.iIncomingPhonecallStage = 2
						PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS- moving to stage 2 ")
	                ELSE
	                    PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS - waiting for INITIATE_INCOMING_CHAT_CALL")
	                ENDIF
				ENDIF
            ELSE
                //PRINT_HELP("BOYAH")
                NETWORK_SESSION_VOICE_RESPOND_TO_REQUEST(FALSE, PLY_TO_PLY_CALL_RESPONSE_ENGAGED)
                NET_PRINT("MAINTAIN_INCOMING_PLAYER_CALLS - player can't talk responding busy") NET_NL()
                voiceSession.iIncomingPhonecallStage = 0
            ENDIF
        BREAK
        CASE 2
            IF NETWORK_IS_SIGNED_ONLINE()
                IF IS_GAMER_HANDLE_VALID(voiceSession.gamerHandle)
                    IF NETWORK_IS_FRIEND(voiceSession.gamerHandle)
                        IF NOT NETWORK_IS_FRIEND_HANDLE_ONLINE(voiceSession.gamerHandle)
                            PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS: killing voice call ", voiceSession.gamerName, " is not online")
							NETWORK_SESSION_VOICE_RESPOND_TO_REQUEST(FALSE, PLY_TO_PLY_CALL_RESPONSE_REJECTED)
                            SCRIPT_END_A_VOICE_SESSION()
                        ENDIF
                    ENDIF
                ELSE
                    PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS: killing voice call gamer handle isn't valid")
                    SCRIPT_END_A_VOICE_SESSION()
                ENDIF
                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
                    NETWORK_SESSION_VOICE_RESPOND_TO_REQUEST(TRUE, PLY_TO_PLY_CALL_RESPONSE_ACCEPTED)
                    NET_PRINT("MAINTAIN_INCOMING_PLAYER_CALLS - player said yes") NET_NL()
                    voiceSession.iIncomingPhonecallStage = 0
                    SET_PLAYER_CHAT_CALL_AS_CONNECTED ()
                    
                    TitleUpdate_Post_Set_Player_Chat_Call_As_Connected()

                    //HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
                ELIF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT ))
                    
                    NETWORK_SESSION_VOICE_RESPOND_TO_REQUEST(FALSE, PLY_TO_PLY_CALL_RESPONSE_REJECTED)
                    NET_PRINT("MAINTAIN_INCOMING_PLAYER_CALLS - player said no") NET_NL()
                    voiceSession.iIncomingPhonecallStage = 0
                    //HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
                ENDIF
            ELSE
                PRINTLN("MAINTAIN_INCOMING_PLAYER_CALLS: killing voice call Local player is not online")
                SCRIPT_END_A_VOICE_SESSION()
            ENDIF
        BREAK
    ENDSWITCH
ENDPROC
//EOF
