//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Adam Westwood				Date: 07/02/14			    │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						      Charm Minigame						            │
//│																				│
//│		              Minigame to for charming a ped                            │
//│                                                                             │
//│			                            										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "timer_public.sch"

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"

USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"

USING "LineActivation.sch"

USING "clothes_shop_private.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

CONST_INT bs_gest_knuckle_crunch  0
CONST_INT bs_gest_slow_clap_face_palm  1
CONST_INT bs_gest_thumbs_up  2
CONST_INT bs_gest_wanking  3
CONST_INT bs_gest_thrusting  4
CONST_INT bs_gest_salute 5
CONST_INT bs_gest_fuck_you  6
CONST_INT bs_gest_air_guitar  7
CONST_INT bs_gest_rock  8
CONST_INT bs_gest_no_way  9
CONST_INT bs_gest_photos  10
CONST_INT bs_gest_dancing  11
CONST_INT bs_gest_blow_kiss  12
CONST_INT bs_gest_jazz_hands 13
CONST_INT bs_gest_nose_pick  14
CONST_INT bs_gest_wave  15
CONST_INT bs_gest_hands_up  16
CONST_INT bs_gest_shh  17
CONST_INT bs_gest_dock  18

ENUM CHARM_PED_STATE
	CPS_NONE,
	CPS_WAITING_FOR_STATE,
	CPS_WAITING,
	CPS_COMMENTING_ON_VEHICLE,
	CPS_COMMENTING_ON_NO_VEHICLE,
	CPS_COMMENTING_ON_CLOTHES,
	CPS_BEING_CHARMED,
	CPS_BEING_CHARMED_NO_SPEECH,
	CPS_CHARMED
ENDENUM

STRUCT S_CHARM_MINIGAME_DATA 
	CHARM_PED_STATE e_charm_ped_state = CPS_NONE
	FLOAT fCharmLevel
	FLOAT fDesiredCharmLevel
	BOOL b_charm_minigame_active
	BOOL b_quit_charm_minigame
	BOOL b_failed_charm_minigame
	BOOL b_beat_charm_minigame
	INT  i_charm_timer
	INT  i_this_charm_minigame
	MODEL_NAMES ePlayerVehicleModel
	VEHICLE_INDEX vehVehicleTemp
	BOOL bPlayerHasNoVeh
	INT iOutfitCost
	INT iTaskDelayTimer
	STRING sThisDialogue
	INT iSpeechTimer
	INT iIdleTimer
	BOOL bShownHelp
	BOOL showHUD
	INT bsGesturesPlayed
ENDSTRUCT

CONST_FLOAT MAX_CHARM_BAR 100.0

STRING sCharmAnimDictAdditional = "gestures@f@standing@casual"
STRING sCharmAnimDict = "anim@heists@ornate_bank@chat_manager"

STRING sCharmAnimCharmed = "charm"

STRING sCharmAnimBeingCharmed1 = "gesture_pleased"
STRING sCharmAnimBeingCharmed2 = "gesture_i_will"
STRING sCharmAnimBeingCharmed3 = "gesture_nod_yes_soft"

STRING sCharmAnimCommentingOnVehicleG = "nice_car"
STRING sCharmAnimCommentingOnVehicleA = "average_car"
STRING sCharmAnimCommentingOnVehicleB = "not_nice_car"

STRING sCharmAnimCommentingOnNoVehicle = "on_foot"

STRING sCharmAnimCommentingOnClothesG= "nice_clothes"
STRING sCharmAnimCommentingOnClothesA= "average_clothes"
STRING sCharmAnimCommentingOnClothesB= "poor_clothes"

STRING sCharmAnimNoSpeak= "no_speak"

//"disrupt"
//"fail"
//"not_charm"
//"wear_weird"

STRING sMinigameDialogue = "OBHSMAU"

structPedsForConversation sSpeech


VEHICLE_VALUE_DATA sVVData

/// RESET_CROWD_CONTROL_MINIGAME_DATA(S_CROWD_CONTROL_DATA &crowd_control_data)
///    
PROC RESET_CHARM_MINIGAME_DATA(S_CHARM_MINIGAME_DATA &charm_minigame_data)
	charm_minigame_data.b_charm_minigame_active = FALSE
	charm_minigame_data.b_quit_charm_minigame = FALSE
	charm_minigame_data.b_failed_charm_minigame = FALSE
	charm_minigame_data.b_beat_charm_minigame = FALSE
	charm_minigame_data.i_charm_timer = 0
	charm_minigame_data.i_this_charm_minigame = 0
	charm_minigame_data.e_charm_ped_state = CPS_NONE
	charm_minigame_data.fCharmLevel = 0
	charm_minigame_data.bShownHelp = FALSE
ENDPROC

/// RESET_CROWD_CONTROL_MINIGAME_DATA(S_CROWD_CONTROL_DATA &crowd_control_data)
///
FUNC BOOL MANAGE_CHARM_MINIGAME_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// FORCE_QUIT_FAIL_CROWD_CONTROL_MINIGAME_KEEP_DATA (pass in your own crowd control struct)
/// NOTES: Force the termination of the hacking minigame and cause the player to fail but preserves variable data.
/// PURPOSE:
///   Force the termination of the hacking minigame and cause the player to fail.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No Returns
///  
PROC FORCE_QUIT_FAIL_CHARM_MINIGAME_KEEP_DATA(S_CHARM_MINIGAME_DATA &charm_minigame_data)
	SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	charm_minigame_data.b_failed_charm_minigame = TRUE
	RESET_CHARM_MINIGAME_DATA(charm_minigame_data)
ENDPROC

/// FORCE_QUIT_PASS_CROWD_CONTROL_MINIGAME_KEEP_DATA (pass in your own crowd control struct)
/// NOTES: Force the termination of the hacking minigame and cause the player to fail but preserves variable data.
/// PURPOSE:
///   Force the termination of the hacking minigame and cause the player to fail.
/// PARAMS:
///    S_HACKING_DATA &hacking_data - hacking data
/// RETURNS:
///  No Returns
///  
PROC FORCE_QUIT_PASS_CHARM_MINIGAME_KEEP_DATA(S_CHARM_MINIGAME_DATA &charm_minigame_data)
	SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	charm_minigame_data.b_failed_charm_minigame = TRUE
	RESET_CHARM_MINIGAME_DATA(charm_minigame_data)
ENDPROC

FUNC BOOL HAS_PLAYER_BEAT_CHARM_MINIGAME(S_CHARM_MINIGAME_DATA &charm_minigame_data,BOOL bAndForceQuit = FALSE)

	IF charm_minigame_data.b_beat_charm_minigame = TRUE
		IF bAndForceQuit 
			FORCE_QUIT_PASS_CHARM_MINIGAME_KEEP_DATA(charm_minigame_data)
			
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
RETURN FALSE
ENDFUNC

//PURPOSE: Return True if the Player is in front of the Ped
FUNC BOOL IS_PLAYER_IN_FRONT_OF_CHARM_PED(PLAYER_INDEX Player, PED_INDEX TargetPed)

	VECTOR posA		// Player position
	VECTOR posB 	// Ped position
	VECTOR posC
	VECTOR vecAB 	// vector from Player to Ped
	VECTOR vecPed	// unit vector pointing in the direction the Ped is facing
	FLOAT fTemp
	
	// get positions
	IF IS_NET_PLAYER_OK(Player)
		posA = GET_PLAYER_COORDS(Player)
	ENDIF
	
	IF NOT IS_PED_INJURED(TargetPed)
		posB = GET_ENTITY_COORDS(TargetPed)
	ENDIF

	// get vec from Player to Ped
	vecAB = posB - posA
	
	// get unit vector pointing forwards from Ped
	IF NOT IS_PED_INJURED(TargetPed)
		posC = get_offset_from_entity_in_world_coords(TargetPed, <<0.0, 7.0, 0.0>>)
		vecPed = posC - posB
	ENDIF

	// take the z out
	vecAB.z = 0.0
	vecPed.z = 0.0

	// calculate dot product of vecAB and vecPed
	fTemp = DOT_PRODUCT(vecAB, vecPed)
	IF (fTemp < -0.1)
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

//PUSRPOSE: HAS_PLAYER_JUST_SHOT_NEAR_TO_CROWD_CONTROL_PED
FUNC BOOL HAS_PLAYER_JUST_SHOT_NEAR_TO_CHARM_PED(PED_INDEX pedCharmPed)
	
	IF NOT IS_PED_INJURED(pedCharmPed)
	AND IS_NET_PLAYER_OK(PLAYER_ID())
		WEAPON_TYPE CurrentWeapon
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
			IF CurrentWeapon != WEAPONTYPE_UNARMED
			AND CurrentWeapon != WEAPONTYPE_OBJECT
			AND CurrentWeapon != WEAPONTYPE_FLARE
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedCharmPed), 5.0)
				OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(pedCharmPed), 10.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PUSRPOSE: IS_PLAYER_THREATENING_CHARM_PED
FUNC BOOL IS_PLAYER_THREATENING_CHARM_PED(PED_INDEX pedCharmPed)
	
	//Check if player has aimed a gun at the Staff
	IF NOT IS_PED_INJURED(pedCharmPed)
	AND IS_NET_PLAYER_OK(PLAYER_ID())
		WEAPON_TYPE CurrentWeapon
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
			IF CurrentWeapon != WEAPONTYPE_UNARMED
			AND CurrentWeapon != WEAPONTYPE_OBJECT
			AND CurrentWeapon != WEAPONTYPE_FLARE
				IF ( IS_PLAYER_FREE_AIMING(PLAYER_ID()) AND IS_PED_FACING_PED(PLAYER_PED_ID(), pedCharmPed, 45.0) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),  pedCharmPed, <<5.0, 5.0, 2.0>>) )
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),  pedCharmPed)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),  pedCharmPed)
				OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedCharmPed), 5.0)
				OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(pedCharmPed), 10.0) 
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

//PUSRPOSE: IS_PLAYER_CHARMING_PED
FUNC BOOL IS_PLAYER_CHARMING_PED(PED_INDEX pedCharmPed)

	IF NOT IS_PED_INJURED(pedCharmPed)
	AND IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PLAYER_IN_FRONT_OF_PED(PLAYER_ID(), pedCharmPed)
		AND NETWORK_IS_PLAYER_TALKING(PLAYER_ID())					
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GET_PLAYER_CLOTHES_VALUE(S_CHARM_MINIGAME_DATA &charm_minigame_data)
INT iCompType
INT iPedProp
PED_COMP_NAME_ENUM eCurrentItem
PED_COMP_ITEM_DATA_STRUCT sTempItemData

	REPEAT NUMBER_OF_PED_COMP_TYPES iCompType
	      IF iCompType = ENUM_TO_INT(COMP_TYPE_OUTFIT)
	            // skip
	      ELIF iCompType = ENUM_TO_INT(COMP_TYPE_PROPGROUP)
	            // skip
	      ELIF iCompType = ENUM_TO_INT(COMP_TYPE_PROPS)
	          // get individual props
	          REPEAT NUMBER_OF_PED_PROP_TYPES iPedProp
	              eCurrentItem = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iCompType), iPedProp)
				  
	              IF eCurrentItem != DUMMY_PED_COMP
		              sTempItemData = GET_PED_COMP_DATA_FOR_ITEM_MP (GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, eCurrentItem)
		              charm_minigame_data.iOutfitCost += sTempItemData.iCost
	              ENDIF
				  
	          ENDREPEAT
	      ELSE
		  
	      	  eCurrentItem = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iCompType))
		  
		      IF eCurrentItem != DUMMY_PED_COMP
				sTempItemData = GET_PED_COMP_DATA_FOR_ITEM_MP (GET_ENTITY_MODEL(PLAYER_PED_ID()), INT_TO_ENUM(PED_COMP_TYPE_ENUM,iCompType), eCurrentItem)
		      	charm_minigame_data.iOutfitCost += sTempItemData.iCost
		      ENDIF
			  
	      ENDIF
	ENDREPEAT

ENDPROC

PROC GET_PLAYER_VEHICLE_VALUE(PED_INDEX pedCharmPed,S_CHARM_MINIGAME_DATA &charm_minigame_data)
	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        charm_minigame_data.vehVehicleTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        PRINTLN("PLAYER IS IN VEHICLE")
	ELSE
        charm_minigame_data.vehVehicleTemp = GET_LAST_DRIVEN_VEHICLE()
        PRINTLN("PLAYER IS NOT IN VEHICLE")
	ENDIF
	
	IF NOT IS_PED_INJURED(pedCharmPed)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(charm_minigame_data.vehVehicleTemp)
				IF IS_ENTITY_AT_ENTITY(pedCharmPed,charm_minigame_data.vehVehicleTemp,<<15,15,15>>)
					charm_minigame_data.ePlayerVehicleModel = GET_ENTITY_MODEL( charm_minigame_data.vehVehicleTemp)
					GET_VEHICLE_VALUE(sVVData,charm_minigame_data.ePlayerVehicleModel)
				ELSE
					charm_minigame_data.bPlayerHasNoVeh = TRUE
				ENDIF
			ELSE
				charm_minigame_data.bPlayerHasNoVeh = TRUE
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(charm_minigame_data.vehVehicleTemp)
				charm_minigame_data.ePlayerVehicleModel = GET_ENTITY_MODEL( charm_minigame_data.vehVehicleTemp)
				GET_VEHICLE_VALUE(sVVData,charm_minigame_data.ePlayerVehicleModel)
				PRINTLN("Vehicle Value:",sVVData.iStandardPrice)
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

/// REQUEST_CHARM_MINIGAME_ASSETS()
/// NOTES: called in the charm minigame script but can be precalled and checked by the scripter if necessary
/// PURPOSE: 
/// checking assets are loaded
/// PARAMS: 
/// RETURNS: 
///    
PROC REQUEST_CHARM_MINIGAME_ASSETS()
	REQUEST_ANIM_DICT(sCharmAnimDict)
	REQUEST_ANIM_DICT(sCharmAnimDictAdditional)
	REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
ENDPROC

/// HAVE_CHARM_MINIGAME_ASSETS_LOADED()
/// NOTES: called in the charm minigame script but can be precalled and checked by the scripter if necessary
/// PURPOSE:
/// checking assets are loaded
/// PARAMS:
/// RETURNS: 

FUNC BOOL HAVE_CHARM_MINIGAME_ASSETS_LOADED()
	REQUEST_ANIM_DICT(sCharmAnimDict)
	REQUEST_ANIM_DICT(sCharmAnimDictAdditional)
	IF HAS_ANIM_DICT_LOADED(sCharmAnimDict)
	AND HAS_ANIM_DICT_LOADED(sCharmAnimDictAdditional)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_NOT_COOL(S_CHARM_MINIGAME_DATA &charm_minigame_data)

	IF charm_minigame_data.ePlayerVehicleModel = TITAN
	OR charm_minigame_data.ePlayerVehicleModel = LUXOR
	OR charm_minigame_data.ePlayerVehicleModel = DUMP
	OR charm_minigame_data.ePlayerVehicleModel = DUNE2
	OR charm_minigame_data.ePlayerVehicleModel = ZTYPE
	OR charm_minigame_data.ePlayerVehicleModel = BLIMP
	OR charm_minigame_data.ePlayerVehicleModel = SUBMERSIBLE
	OR charm_minigame_data.ePlayerVehicleModel = FIRETRUK
	OR charm_minigame_data.ePlayerVehicleModel = PBUS
	OR charm_minigame_data.ePlayerVehicleModel = TACO
	OR charm_minigame_data.ePlayerVehicleModel = SPEEDO2
	OR charm_minigame_data.ePlayerVehicleModel = ROMERO
	OR charm_minigame_data.ePlayerVehicleModel = TOURBUS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_VEHICLE_A_POLICE_CAR(S_CHARM_MINIGAME_DATA &charm_minigame_data)

	IF charm_minigame_data.ePlayerVehicleModel = POLICE2
	OR charm_minigame_data.ePlayerVehicleModel = POLICE3
	OR charm_minigame_data.ePlayerVehicleModel = POLICE4
	OR charm_minigame_data.ePlayerVehicleModel = PRANGER
	OR charm_minigame_data.ePlayerVehicleModel = SHERIFF
	OR charm_minigame_data.ePlayerVehicleModel = DILETTANTE2
	OR charm_minigame_data.ePlayerVehicleModel = FBI 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_CHARM_LEVEL_FOR_VEHICLE_VALUE(S_CHARM_MINIGAME_DATA &charm_minigame_data)

	IF NOT IS_VEHICLE_A_POLICE_CAR(charm_minigame_data)
		IF sVVData.iStandardPrice > 1000000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 15
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 40
			ENDIF
		ELIF sVVData.iStandardPrice < 100000 AND sVVData.iStandardPrice > 500000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 10
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 30
			ENDIF
		ELIF sVVData.iStandardPrice < 500000 AND sVVData.iStandardPrice > 250000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 8
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 25
			ENDIF
		ELIF sVVData.iStandardPrice < 250000 AND sVVData.iStandardPrice > 100000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 5
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 20
			ENDIF
		ELIF sVVData.iStandardPrice < 100000 AND sVVData.iStandardPrice > 50000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 5
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 18
			ENDIF
		ELIF sVVData.iStandardPrice < 50000 AND sVVData.iStandardPrice > 25000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 5
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 15
			ENDIF
		ELIF sVVData.iStandardPrice < 25000 AND sVVData.iStandardPrice > 10000
			IF IS_VEHICLE_NOT_COOL(charm_minigame_data)
				charm_minigame_data.fDesiredCharmLevel = 2
			ELSE
				charm_minigame_data.fDesiredCharmLevel = 10
			ENDIF
		ELSE
			charm_minigame_data.fDesiredCharmLevel = 5
		ENDIF
	ELSE
		charm_minigame_data.fDesiredCharmLevel = 1
	ENDIF
	
	RETURN charm_minigame_data.fDesiredCharmLevel
	
ENDFUNC

FUNC FLOAT GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(S_CHARM_MINIGAME_DATA &charm_minigame_data)
	FLOAT fCharmLevel
	IF charm_minigame_data.iOutfitCost < 2500  
		fCharmLevel = 1
		PRINTLN("[RUN_CHARM_PED_MINIGAME]GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING fCharmLevel = 1 ")
	ELIF charm_minigame_data.iOutfitCost < 5000
		fCharmLevel = 10
		PRINTLN("[RUN_CHARM_PED_MINIGAME]GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING fCharmLevel = 10 ")
	ELIF charm_minigame_data.iOutfitCost < 10000
		fCharmLevel = 20
		PRINTLN("[RUN_CHARM_PED_MINIGAME]GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING fCharmLevel = 20 ")
	ELIF charm_minigame_data.iOutfitCost < 20000 
		fCharmLevel = 25
		PRINTLN("[RUN_CHARM_PED_MINIGAME]GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING fCharmLevel = 25 ")
	ELIF charm_minigame_data.iOutfitCost < 30000 
		fCharmLevel =  30
		PRINTLN("[RUN_CHARM_PED_MINIGAME]GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING fCharmLevel = 30 ")
	ELSE
		fCharmLevel =  40
		PRINTLN("[RUN_CHARM_PED_MINIGAME]GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING fCharmLevel = 40 ")
	ENDIF
	
	RETURN fCharmLevel
ENDFUNC

FUNC BOOL HAS_PLAYER_CHARMED_PED(S_CHARM_MINIGAME_DATA &charm_minigame_data)
	IF charm_minigame_data.b_beat_charm_minigame = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_CHARM_PED_ACTIONS(PED_INDEX pedCharmPed ,S_CHARM_MINIGAME_DATA &charm_minigame_data)
	
	INT iTemptRand
	
	PRINTLN("[RUN_CHARM_PED_MINIGAME] charm_minigame_data.e_charm_ped_state: ")PRINTLN(ENUM_TO_INT(charm_minigame_data.e_charm_ped_state))
	
	SWITCH charm_minigame_data.e_charm_ped_state
		
		
		CASE CPS_WAITING_FOR_STATE
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF MANAGE_CHARM_MINIGAME_TIMER(charm_minigame_data.iTaskDelayTimer,2000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_SCRIPT_TASK_STATUS(pedCharmPed,SCRIPT_TASK_PERFORM_SEQUENCE)<> PERFORMING_TASK
							charm_minigame_data.e_charm_ped_state = CPS_WAITING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//holding state
		CASE CPS_WAITING
			
		BREAK
		
		CASE CPS_COMMENTING_ON_VEHICLE
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,sMinigameDialogue, charm_minigame_data.sThisDialogue, CONV_PRIORITY_MEDIUM) 
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),20000,SLF_WHILE_NOT_IN_FOV)
							IF charm_minigame_data.fDesiredCharmLevel >= 30
								TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnVehicleG,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							ELIF charm_minigame_data.fDesiredCharmLevel < 30 AND charm_minigame_data.fDesiredCharmLevel > 14
								TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnVehicleA,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							ELSE
								TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnVehicleB,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							ENDIF
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(pedCharmPed, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						charm_minigame_data.iTaskDelayTimer = GET_GAME_TIMER()
						charm_minigame_data.e_charm_ped_state = CPS_WAITING_FOR_STATE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CPS_COMMENTING_ON_NO_VEHICLE
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,sMinigameDialogue, charm_minigame_data.sThisDialogue, CONV_PRIORITY_MEDIUM) 
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),20000,SLF_WHILE_NOT_IN_FOV)
							TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnNoVehicle,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(pedCharmPed, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						charm_minigame_data.iTaskDelayTimer = GET_GAME_TIMER()
						charm_minigame_data.e_charm_ped_state = CPS_WAITING_FOR_STATE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CPS_COMMENTING_ON_CLOTHES
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,sMinigameDialogue, charm_minigame_data.sThisDialogue, CONV_PRIORITY_MEDIUM) 
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),20000,SLF_WHILE_NOT_IN_FOV)
							IF GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) >= 30
								TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnClothesG,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							ELIF GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) < 30 AND GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) > 10
								TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnClothesA,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							ELIF GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) < 10
								TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCommentingOnClothesB,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							ENDIF
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(pedCharmPed, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						charm_minigame_data.iTaskDelayTimer = GET_GAME_TIMER()
						charm_minigame_data.sThisDialogue = "OBHSM16"
						charm_minigame_data.e_charm_ped_state = CPS_WAITING_FOR_STATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CPS_BEING_CHARMED
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,sMinigameDialogue, charm_minigame_data.sThisDialogue, CONV_PRIORITY_MEDIUM) 
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),20000,SLF_WHILE_NOT_IN_FOV)
							iTemptRand = GET_RANDOM_INT_IN_RANGE(0,3)
							IF iTemptRand = 0
								TASK_PLAY_ANIM(NULL,sCharmAnimDictAdditional,sCharmAnimBeingCharmed1,SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							ELIF iTemptRand = 1
								TASK_PLAY_ANIM(NULL,sCharmAnimDictAdditional,sCharmAnimBeingCharmed2,SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							ELIF iTemptRand = 2
								TASK_PLAY_ANIM(NULL,sCharmAnimDictAdditional,sCharmAnimBeingCharmed3,SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							ENDIF
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(pedCharmPed, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						charm_minigame_data.iTaskDelayTimer = GET_GAME_TIMER()
						charm_minigame_data.e_charm_ped_state = CPS_WAITING_FOR_STATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CPS_BEING_CHARMED_NO_SPEECH
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,sMinigameDialogue, charm_minigame_data.sThisDialogue, CONV_PRIORITY_MEDIUM) 
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),20000,SLF_WHILE_NOT_IN_FOV)
							TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimNoSpeak,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(pedCharmPed, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						charm_minigame_data.iTaskDelayTimer = GET_GAME_TIMER()
						charm_minigame_data.e_charm_ped_state = CPS_WAITING_FOR_STATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CPS_CHARMED
			IF NOT IS_PED_INJURED(pedCharmPed)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,sMinigameDialogue, charm_minigame_data.sThisDialogue, CONV_PRIORITY_MEDIUM) 
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),20000,SLF_WHILE_NOT_IN_FOV)
							TASK_PLAY_ANIM(NULL,sCharmAnimDict,sCharmAnimCharmed,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(pedCharmPed, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						charm_minigame_data.iTaskDelayTimer = GET_GAME_TIMER()
						charm_minigame_data.e_charm_ped_state = CPS_WAITING_FOR_STATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE CPS_NONE
		
		BREAK
		
	ENDSWITCH

ENDPROC

//INTERACTION_ANIM_TYPE_IN_CAR,
//INTERACTION_ANIM_TYPE_CREW,			
//INTERACTION_ANIM_TYPE_PLAYER,			
//INTERACTION_ANIM_TYPE_SPECIAL,	
///#IF FEATURE_POINTING_INTERACTIONS
//INTERACTION_ANIM_TYPE_POINT,
//#ENDIF
//MAX_NUM_PLAYER_INTERACTION_TYPES 

INT iGestureType
INT iGestureAnim

CREW_INTERACTIONS eCrewGesture
PLAYER_INTERACTIONS eInteractionGesture

FUNC BOOL IS_PLAYER_GESTURE_SUITABLE()
	
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		GET_CURRENTLY_PLAYING_INTERACTION(iGestureType,iGestureAnim)
		IF iGestureType = 0 OR iGestureType = 1 OR iGestureType = 2 OR iGestureType = 3 OR iGestureType = 4 OR iGestureType = 5
			//CREW INTERACTIONS
			IF iGestureType = 1
				eCrewGesture = INT_TO_ENUM(CREW_INTERACTIONS,iGestureAnim)
					
				IF eCrewGesture != CREW_INTERACTION_UP_YOURS
				//OR eCrewGesture != CREW_INTERACTION_FINGER
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF iGestureType = 2
				eInteractionGesture = INT_TO_ENUM(PLAYER_INTERACTIONS,iGestureAnim)
				IF eInteractionGesture != PLAYER_INTERACTION_FINGER
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF iGestureType = 3
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_GESTURE_SPECIFIC()
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		GET_CURRENTLY_PLAYING_INTERACTION(iGestureType,iGestureAnim)
		IF iGestureType = 2
			eInteractionGesture = INT_TO_ENUM(PLAYER_INTERACTIONS,iGestureAnim)
			IF eInteractionGesture = PLAYER_INTERACTION_KNUCKLE_CRUNCH
			OR eInteractionGesture = PLAYER_INTERACTION_SLOW_CLAP
			OR eInteractionGesture = PLAYER_INTERACTION_FACE_PALM
			OR eInteractionGesture = PLAYER_INTERACTION_THUMBS_UP
			OR eInteractionGesture = PLAYER_INTERACTION_WANK
			OR eInteractionGesture = PLAYER_INTERACTION_AIR_SHAGGING
			OR eInteractionGesture = PLAYER_INTERACTION_SALUTE
			OR eInteractionGesture = PLAYER_INTERACTION_AIR_GUITAR
			OR eInteractionGesture = PLAYER_INTERACTION_ROCK
			OR eInteractionGesture = PLAYER_INTERACTION_PHOTOGRAPHY
			OR eInteractionGesture = PLAYER_INTERACTION_BLOW_KISS
			OR eInteractionGesture = PLAYER_INTERACTION_JAZZ_HANDS
			OR eInteractionGesture = PLAYER_INTERACTION_NOSE_PICK
			OR eInteractionGesture = PLAYER_INTERACTION_WAVE
			OR eInteractionGesture = PLAYER_INTERACTION_SURRENDER
			OR eInteractionGesture = PLAYER_INTERACTION_SHUSH
			OR eInteractionGesture = PLAYER_INTERACTION_DOCK
				RETURN TRUE
			ENDIF
			
			#IF FEATURE_INTERACTIONS_ARMY
			IF eInteractionGesture = PLAYER_INTERACTION_UP_YOURS
			OR eInteractionGesture = PLAYER_INTERACTION_NO_WAY
			//OR eInteractionGesture = PLAYER_INTERACTION_DANCE
				RETURN TRUE
			ENDIF
			#ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RUN_CHARM_PED_MINIGAME(PED_INDEX pedCharmPed ,S_CHARM_MINIGAME_DATA &charm_minigame_data)
	
	VEHICLE_INDEX vehTemp
	
	PRINTLN("[RUN_CHARM_PED_MINIGAME] i_this_charm_minigame: ")PRINTLN(charm_minigame_data.i_this_charm_minigame)
	
	MANAGE_CHARM_PED_ACTIONS(pedCharmPed,charm_minigame_data)
	
	// Speirs commented out to fix NT (1865642), showHUD doesn't seem to be set anywhere
//	IF charm_minigame_data.showHUD
		IF charm_minigame_data.i_this_charm_minigame > 2
			IF charm_minigame_data.fCharmLevel < charm_minigame_data.fDesiredCharmLevel
				charm_minigame_data.fCharmLevel = charm_minigame_data.fCharmLevel + 0.8 //0.2
				PRINTLN("[RUN_CHARM_PED_MINIGAME] matching fCharmLevel to fDeseiredCharmLevel")
			ENDIF
			DRAW_GENERIC_METER(ROUND(charm_minigame_data.fCharmLevel), 100, "MC_LIKE_L", HUD_COLOUR_YELLOW, -1, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE)
			PRINTLN("[RUN_CHARM_PED_MINIGAME] charm_minigame_data.fCharmLevel: ")PRINTLN(charm_minigame_data.fCharmLevel)
			PRINTLN("[RUN_CHARM_PED_MINIGAME] charm_minigame_data.fDesiredCharmLevel: ")PRINTLN(charm_minigame_data.fDesiredCharmLevel)
		ENDIF
//	ENDIF

	SWITCH charm_minigame_data.i_this_charm_minigame
		
		//SET UP DATA AND ADD PED FOR DIALOGUE
		CASE 0
			REQUEST_CHARM_MINIGAME_ASSETS()
			IF HAVE_CHARM_MINIGAME_ASSETS_LOADED()
				charm_minigame_data.e_charm_ped_state = CPS_WAITING
				sVVData.iStandardPrice = 0
				ADD_PED_FOR_DIALOGUE(sSpeech, 6, pedCharmPed, "HEISTMANAGER")
				charm_minigame_data.bShownHelp = FALSE
				charm_minigame_data.i_this_charm_minigame ++
			ENDIF
		BREAK
		
		CASE 1
			
			IF charm_minigame_data.bShownHelp = FALSE
				IF NOT IS_PED_INJURED(pedCharmPed)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedCharmPed)) < 20
						PRINT_HELP("MC_CHARM_1")
						charm_minigame_data.bShownHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//CHECK PLAYER IN VEHICLE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedCharmPed)
					IF IS_ENTITY_AT_ENTITY(vehTemp, pedCharmPed,<<8,8,8>>)
						PRINT_HELP("MC_CHARM_2")
						charm_minigame_data.i_this_charm_minigame ++
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedCharmPed)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCharmPed,<<5,5,5>>)
						IF IS_PLAYER_IN_FRONT_OF_CHARM_PED(PLAYER_ID(),pedCharmPed)
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_CHARM_2")
								PRINT_HELP("MC_CHARM_2")
							ENDIF
							charm_minigame_data.i_this_charm_minigame ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			GET_PLAYER_VEHICLE_VALUE(pedCharmPed,charm_minigame_data)
			IF sVVData.iStandardPrice > 0
			OR charm_minigame_data.bPlayerHasNoVeh = TRUE
				charm_minigame_data.i_this_charm_minigame ++
			ENDIF
		BREAK
		
		CASE 3
			PRINTLN("Vehicle Value:",sVVData.iStandardPrice)
			IF charm_minigame_data.bPlayerHasNoVeh = TRUE
				charm_minigame_data.sThisDialogue = "OBHSM11"
				charm_minigame_data.e_charm_ped_state = CPS_COMMENTING_ON_NO_VEHICLE
				charm_minigame_data.iIdleTimer = GET_GAME_TIMER()
				charm_minigame_data.i_this_charm_minigame ++
				//Where your wheels at bro, dialogue.
			ELSE
				charm_minigame_data.fDesiredCharmLevel = GET_CHARM_LEVEL_FOR_VEHICLE_VALUE(charm_minigame_data)
				IF charm_minigame_data.fDesiredCharmLevel >= 30
					charm_minigame_data.sThisDialogue = "OBHSM8"
				ELIF charm_minigame_data.fDesiredCharmLevel < 30 AND charm_minigame_data.fDesiredCharmLevel > 14
					charm_minigame_data.sThisDialogue = "OBHSM9"	
				ELSE
					charm_minigame_data.sThisDialogue = "OBHSM10"
				ENDIF
				
				charm_minigame_data.e_charm_ped_state = CPS_COMMENTING_ON_VEHICLE
				charm_minigame_data.iIdleTimer = GET_GAME_TIMER()
				charm_minigame_data.i_this_charm_minigame ++
			ENDIF
		BREAK
		
		CASE 4
			IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
			OR MANAGE_CHARM_MINIGAME_TIMER(charm_minigame_data.iIdleTimer,10000) //time out timer
				IF NOT IS_PED_INJURED(pedCharmPed)
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedCharmPed,<<5,5,5>>)
							IF IS_PLAYER_IN_FRONT_OF_CHARM_PED(PLAYER_ID(),pedCharmPed)
								GET_PLAYER_CLOTHES_VALUE(charm_minigame_data)
								charm_minigame_data.iOutfitCost = 10000
								charm_minigame_data.e_charm_ped_state = CPS_WAITING
								charm_minigame_data.i_this_charm_minigame ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		CASE 5
			PRINTLN("[RUN_CHARM_PED_MINIGAME] charm_minigame_data.iOutfitCost")PRINTLN(charm_minigame_data.iOutfitCost)
			GET_PLAYER_CLOTHES_VALUE(charm_minigame_data)
			IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
				IF charm_minigame_data.iOutfitCost > 0
					IF GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) >= 30
						charm_minigame_data.sThisDialogue = "OBHSM11"
					ELIF GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) < 30 AND GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) > 10
						charm_minigame_data.sThisDialogue = "OBHSM12"
					ELIF GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data) < 10
						charm_minigame_data.sThisDialogue = "OBHSM13"
					ENDIF
					
					//Add in mask / naked or weird later "OBHSM14"
					
					charm_minigame_data.e_charm_ped_state = CPS_COMMENTING_ON_CLOTHES
					charm_minigame_data.fDesiredCharmLevel = charm_minigame_data.fDesiredCharmLevel + GET_CHARM_LEVEL_FOR_CURRENT_CLOTHING(charm_minigame_data)
					charm_minigame_data.iSpeechTimer = GET_GAME_TIMER()
					charm_minigame_data.i_this_charm_minigame ++
				ENDIF
			ENDIF
		BREAK
	
		
		CASE 6
			IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
				IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
					charm_minigame_data.sThisDialogue = "OBHSM16"
					charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
				ENDIF
				PRINTLN("[RUN_CHARM_PED_MINIGAME] player talking")
				charm_minigame_data.iSpeechTimer = GET_GAME_TIMER()
				charm_minigame_data.fDesiredCharmLevel = charm_minigame_data.fDesiredCharmLevel + 0.112
			ELSE
				IF IS_ANY_INTERACTION_ANIM_PLAYING()
					IF IS_PLAYER_GESTURE_SPECIFIC()
						IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
							IF eInteractionGesture = PLAYER_INTERACTION_AIR_GUITAR
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_air_guitar)
									charm_minigame_data.sThisDialogue = "OBHSM51"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_air_guitar)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_BLOW_KISS
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_blow_kiss )
									charm_minigame_data.sThisDialogue = "OBHSM56"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_blow_kiss )
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
//							#IF FEATURE_INTERACTIONS_ARMY 
//								IF eInteractionGesture = PLAYER_INTERACTION_DANCE
//									IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_dancing )
//										charm_minigame_data.sThisDialogue = "OBHSM55"
//										charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
//										SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_dancing)
//									ELSE
//										charm_minigame_data.sThisDialogue = "OBHSM64"
//										charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
//									ENDIF
//								ENDIF
//							#ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_DOCK
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_dock )
									charm_minigame_data.sThisDialogue = "OBHSM62"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_dock)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							#IF FEATURE_INTERACTIONS_ARMY 
							IF eInteractionGesture = PLAYER_INTERACTION_UP_YOURS
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_fuck_you )
									charm_minigame_data.sThisDialogue = "OBHSM50"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_fuck_you)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							#ENDIF
							
							//
							IF eInteractionGesture = PLAYER_INTERACTION_SURRENDER
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_hands_up )
									charm_minigame_data.sThisDialogue = "OBHSM60"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_hands_up)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_JAZZ_HANDS
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_jazz_hands )
									charm_minigame_data.sThisDialogue = "OBHSM57"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_jazz_hands)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_KNUCKLE_CRUNCH
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_knuckle_crunch)
									charm_minigame_data.sThisDialogue = "OBHSM44"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_knuckle_crunch)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							#IF FEATURE_INTERACTIONS_ARMY 
							IF eInteractionGesture = PLAYER_INTERACTION_NO_WAY
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_no_way)
									charm_minigame_data.sThisDialogue = "OBHSM53"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_no_way)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							#ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_NOSE_PICK
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_nose_pick)
									charm_minigame_data.sThisDialogue = "OBHSM58"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_nose_pick)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_PHOTOGRAPHY
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_photos)
									charm_minigame_data.sThisDialogue = "OBHSM54"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_photos)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_ROCK
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_rock)
									charm_minigame_data.sThisDialogue = "OBHSM52"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_rock)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_SALUTE
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_salute)
									charm_minigame_data.sThisDialogue = "OBHSM49"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_salute)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_SHUSH
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_shh)
									charm_minigame_data.sThisDialogue = "OBHSM61"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_shh)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_SLOW_CLAP
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_slow_clap_face_palm)
									charm_minigame_data.sThisDialogue = "OBHSM45"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_slow_clap_face_palm)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_AIR_SHAGGING
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_thrusting)
									charm_minigame_data.sThisDialogue = "OBHSM48"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_thrusting)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_THUMBS_UP
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_thumbs_up)
									charm_minigame_data.sThisDialogue = "OBHSM46"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_thumbs_up)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_WANK
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_wanking)
									charm_minigame_data.sThisDialogue = "OBHSM47"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_wanking)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
							IF eInteractionGesture = PLAYER_INTERACTION_WAVE
								IF NOT IS_BIT_SET(charm_minigame_data.bsGesturesPlayed,bs_gest_wave)
									charm_minigame_data.sThisDialogue = "OBHSM59"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
									SET_BIT(charm_minigame_data.bsGesturesPlayed,bs_gest_wave)
								ELSE
									charm_minigame_data.sThisDialogue = "OBHSM64"
									charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
								ENDIF
							ENDIF
							
						ENDIF
						
						charm_minigame_data.fDesiredCharmLevel = charm_minigame_data.fDesiredCharmLevel + 0.09
					ELSE
						IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
							charm_minigame_data.sThisDialogue = "OBHSM63"
							charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED
						ENDIF
						charm_minigame_data.fDesiredCharmLevel = charm_minigame_data.fDesiredCharmLevel + 0.09
					ENDIF
				ENDIF
				//Gradually increase over time while the player isn't doing anything
				IF MANAGE_CHARM_MINIGAME_TIMER(charm_minigame_data.iIdleTimer,1000)
					PRINTLN("[RUN_CHARM_PED_MINIGAME] increasing desire level after non action")
					charm_minigame_data.fDesiredCharmLevel = charm_minigame_data.fDesiredCharmLevel + 0.7
					charm_minigame_data.iIdleTimer = GET_GAME_TIMER()
				ENDIF
				IF MANAGE_CHARM_MINIGAME_TIMER(charm_minigame_data.iSpeechTimer,GET_RANDOM_INT_IN_RANGE(15000,25000))
					IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
						charm_minigame_data.sThisDialogue = "OBHSM17"
						charm_minigame_data.iSpeechTimer = GET_GAME_TIMER()
						charm_minigame_data.e_charm_ped_state = CPS_BEING_CHARMED_NO_SPEECH
					ENDIF
				ENDIF
			ENDIF
			
			IF charm_minigame_data.fCharmLevel >= 100
				charm_minigame_data.sThisDialogue = "OBHSM29"
				charm_minigame_data.e_charm_ped_state = CPS_CHARMED
				charm_minigame_data.i_this_charm_minigame ++
			ENDIF
		BREAK
		
		CASE 7
			IF charm_minigame_data.e_charm_ped_state = CPS_WAITING
				charm_minigame_data.bShownHelp = FALSE
				charm_minigame_data.b_beat_charm_minigame = TRUE
				charm_minigame_data.i_this_charm_minigame ++
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC
