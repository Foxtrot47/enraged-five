USING "gunslinger_arcade_using.sch"
USING "gunslinger_arcade_drawing.sch"
USING "gunslinger_arcade_levels.sch"
USING "degenatron_games_movie.sch"
USING "net_arcade_cabinet.sch"


/// PURPOSE:
///    Updates the values used for animating sprite sequences
PROC TLG_UPDATE_ANIM_FRAMES()
	
	INT p, i
	FOR p = 0 TO ciTLG_MAX_PLAYERS - 1
	
		//Blast Slow anim update
		FOR i = 0  TO ciTLG_MAX_PROJECTILES - 1
			sTLGData.sPlayerData[p].sBulletData[i].iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
			sTLGData.sPlayerData[p].sBulletData[i].iSlowUpdateFrames = FLOOR(sTLGData.sPlayerData[p].sBulletData[i].iSlowFrameTimeCounter / cfTLG_SLOW_ANIM_FRAME_TIME)
			sTLGData.sPlayerData[p].sBulletData[i].iSlowFrameTimeCounter -= ROUND(cfTLG_SLOW_ANIM_FRAME_TIME * sTLGData.sPlayerData[p].sBulletData[i].iSlowUpdateFrames)
		ENDFOR
		
	ENDFOR
	
	// Enemies
	FOR i = 0 TO ciTLG_MAX_ENEMIES  -1

		//Slow anim update
		sTLGData.sEnemyData[i].iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
		sTLGData.sEnemyData[i].iSlowUpdateFrames = FLOOR(sTLGData.sEnemyData[i].iSlowFrameTimeCounter / cfTLG_SLOW_ANIM_FRAME_TIME)
		sTLGData.sEnemyData[i].iSlowFrameTimeCounter -= ROUND(cfTLG_SLOW_ANIM_FRAME_TIME * sTLGData.sEnemyData[i].iSlowUpdateFrames)
		
		//Default anim update
		sTLGData.sEnemyData[i].iDefaultFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
		sTLGData.sEnemyData[i].iDefaultUpdateFrames = FLOOR(sTLGData.sEnemyData[i].iDefaultFrameTimeCounter / cfTLG_DEFAULT_ANIM_FRAME_TIME)
		sTLGData.sEnemyData[i].iDefaultFrameTimeCounter -= ROUND(cfTLG_DEFAULT_ANIM_FRAME_TIME * sTLGData.sEnemyData[i].iDefaultUpdateFrames)
		
		IF sTLGData.sEnemyData[i].iEnemyType = TLG_OUTHOUSE_ENEMY
		OR sTLGData.sEnemyData[i].iEnemyType = TLG_COFFIN_ENEMY
			IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
				IF sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES -2
					sTLGData.sEnemyData[i].iSlowFrameTimeCounter = 0
					sTLGData.sEnemyData[i].iSlowFrameTimeCounter = 0
					sTLGData.sEnemyData[i].iSlowUpdateFrames = 0
					
					sTLGData.sEnemyData[i].iDefaultFrameTimeCounter = 0
					sTLGData.sEnemyData[i].iDefaultUpdateFrames = 0
					sTLGData.sEnemyData[i].iDefaultFrameTimeCounter = 0
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO ciTLG_MAX_HOSTAGES  -1

		//Slow anim update
		sTLGData.sHostageData[i].iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
		sTLGData.sHostageData[i].iSlowUpdateFrames = FLOOR(sTLGData.sHostageData[i].iSlowFrameTimeCounter / cfTLG_SLOW_ANIM_FRAME_TIME)
		sTLGData.sHostageData[i].iSlowFrameTimeCounter -= ROUND(cfTLG_SLOW_ANIM_FRAME_TIME * sTLGData.sHostageData[i].iSlowUpdateFrames)
		
		//Default anim update
		sTLGData.sHostageData[i].iDefaultFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
		sTLGData.sHostageData[i].iDefaultUpdateFrames = FLOOR(sTLGData.sHostageData[i].iDefaultFrameTimeCounter / cfTLG_DEFAULT_ANIM_FRAME_TIME)
		sTLGData.sHostageData[i].iDefaultFrameTimeCounter -= ROUND(cfTLG_DEFAULT_ANIM_FRAME_TIME * sTLGData.sHostageData[i].iDefaultUpdateFrames)
		
	ENDFOR
	
	FOR i = 0 TO ciTLG_MAX_FX  -1

		//Slow anim update
		sTLGData.sFXData[i].iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
		sTLGData.sFXData[i].iSlowUpdateFrames = FLOOR(sTLGData.sFXData[i].iSlowFrameTimeCounter / cfTLG_SLOW_ANIM_FRAME_TIME)
		sTLGData.sFXData[i].iSlowFrameTimeCounter -= ROUND(cfTLG_SLOW_ANIM_FRAME_TIME * sTLGData.sFXData[i].iSlowUpdateFrames)

	ENDFOR
	
	FOR i = 0 TO ciTLG_MAX_COVER - 1
		IF sTLGData.sCoverData[i].bIsAnimated
		AND sTLGData.sCoverData[i].iHitCount > 0
			//Slow anim update
			sTLGData.sCoverData[i].iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
			sTLGData.sCoverData[i].iSlowUpdateFrames = FLOOR(sTLGData.sCoverData[i].iSlowFrameTimeCounter / cfTLG_SLOW_ANIM_FRAME_TIME)
			sTLGData.sCoverData[i].iSlowFrameTimeCounter -= ROUND(cfTLG_SLOW_ANIM_FRAME_TIME * sTLGData.sCoverData[i].iSlowUpdateFrames)
			
			//Default anim update
			sTLGData.sCoverData[i].iDefaultFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
			sTLGData.sCoverData[i].iDefaultUpdateFrames = FLOOR(sTLGData.sCoverData[i].iDefaultFrameTimeCounter / cfTLG_DEFAULT_ANIM_FRAME_TIME)
			sTLGData.sCoverData[i].iDefaultFrameTimeCounter -= ROUND(cfTLG_DEFAULT_ANIM_FRAME_TIME * sTLGData.sCoverData[i].iDefaultUpdateFrames)

		ENDIF
	ENDFOR
	
	FOR i = 0 TO ciTLG_MAX_ENEMY_PROJECTILES - 1
		sTLGData.sEnemyProjectiles[i].iSlowFrameTimeCounter +=  ROUND(GET_FRAME_TIME() * 1000)
		sTLGData.sEnemyProjectiles[i].iSlowUpdateFrames = FLOOR(sTLGData.sEnemyProjectiles[i].iSlowFrameTimeCounter / cfTLG_SLOW_ANIM_FRAME_TIME)
		sTLGData.sEnemyProjectiles[i].iSlowFrameTimeCounter -= ROUND(cfTLG_SLOW_ANIM_FRAME_TIME * sTLGData.sEnemyProjectiles[i].iSlowUpdateFrames)
	ENDFOR
	
ENDPROC



PROC TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE eNewState)
	
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] {DSW} TLG_SET_LOCAL_STATE - State changed from ", 
	TLG_DEBUG_GET_TLG_ARCADE_CLIENT_STATE_AS_STRING(sTLGData.eClientState), " to ", TLG_DEBUG_GET_TLG_ARCADE_CLIENT_STATE_AS_STRING(eNewState))
	
	sTLGData.eClientState = eNewState
	
//	IF eNewState = TLG_ARCADE_CLIENT_STATE_LEVEL_SELECTION_MENU	// relaced by TLG_AUDIO_EFFECT_PLAYER_START
//		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SELECT_STAGE)
//	ENDIF
ENDPROC




PROC TLG_RESET_ALL_ENEMY_THROWN_PROJECTLES()
	INT i
	REPEAT ciTLG_MAX_ENEMY_PROJECTILES i
		TLG_RESET_ENEMY_THROWN_PROJECTILE(i)
	ENDREPEAT
	
	sTLGData.iNumberOfThrownEnemyProjectiles = 0
ENDPROC

PROC RESET_ENEMIES()
	INT i = 0
	FOR i = 0 TO ciTLG_MAX_ENEMIES  -1
		RESET_ENEMY(i)
	ENDFOR
ENDPROC


PROC TLG_RESET_ALL_COVER()
	INT i
	FOR i = 0 TO ciTLG_MAX_COVER -1
		TLG_RESET_COVER(i)
	ENDFOR
	sTLGData.iNumberOfProps = 0
ENDPROC



PROC TLG_RESET_ITEMS()
	INT i
	FOR i = 0 TO ciTLG_MAX_ITEMS -1
		TLG_RESET_ITEM(i)
	ENDFOR
	
	sTLGData.iNumberOfItems = 0
ENDPROC
PROC TLG_CLEANUP_VARIABLES()
	
	sTLGData.sPlayerData[0].fHealth = 1.0
//	sTLGData.sPlayerData[0].iWeapon = 0
//	sTLGData.sPlayerData[0].iAmmo = ciTLG_PLAYER_START_AMMO
	
	TLG_INIT_PLAYER_WEAPON()
	
	sTLGData.sPlayerData[0].iHitCountdown = 0
	
	sTLGData.iCurrentActiveEnemies[0] = 0
	sTLGData.iCurrentActiveEnemies[1] = 0
	sTLGData.iCurrentActiveEnemies[2] = 0
	
	sTLGData.bFadingOut = FALSE
	
	sTLGData.eLevelStage = TLG_FADING_IN
	
	INT i
	TLG_RESET_ALL_COVER()
	
	RESET_ENEMIES()

	TLG_RESET_ALL_ENEMY_THROWN_PROJECTLES()
	
	sTLGData.iNumberOfEnemies = 0
	
	TLG_RESET_HOSTAGES()
	
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
		sTLGData.sBackgroundData[i].bRendering = FALSE
	ENDFOR
	sTLGData.iNumberOfBackgroundElements = 0
	
	TLG_RESET_ITEMS()
	
	
	FOR i = 0 TO ciTLG_MAX_FX - 1
		sTLGData.sFXData[i].bActive = FALSE
	ENDFOR
	
	TLG_RESET_LOCAL_PLAYER_SCORE_FOR_CURRENT_LEVEL()
ENDPROC


PROC TLG_CHECK_PLAYER_RETURNING_TO_MENU(BOOL bReturnToTitle = TRUE, BOOL bAllowAccept = FALSE, BOOL bIsLeaderboard = FALSE, BOOL bUseTimeOut = FALSE)
	BOOL bShouldReturn
	BOOL bTimeExpired
	IF bUseTimeOut
		IF sTLGData.iReturnToMenuTimeout = 0
			 sTLGData.iReturnToMenuTimeout = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
	bTimeExpired = (bUseTimeOut AND (GET_GAME_TIMER() - sTLGData.iReturnToMenuTimeout > 10000))
	IF NOT bIsLeaderboard
		bShouldReturn = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR (bAllowAccept AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	ELSE
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			bShouldReturn = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ELSE
			bShouldReturn = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ENDIF
	ENDIF
	
	IF bShouldReturn
	OR bTimeExpired
		#IF IS_DEBUG_BUILD
			IF bTimeExpired
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_CHECK_PLAYER_RETURNING_TO_MENU] {dsw} Time expired - bReturnToTitle: ", bReturnToTitle, " bAllowAccept: ", bAllowAccept, " bIsLeaderboard: ",bIsLeaderboard, " bUseTimeOut: ",bUseTimeOut)
				DEBUG_PRINTCALLSTACK()
			ENDIF
		#ENDIF
		

		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_MENU_BACK)
		
		
		TLG_CLEANUP_VARIABLES()
		
		IF bReturnToTitle
			TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_TITLE_SCREEN)
			sTLGData.iReturnToMenuTimeout = 0
		ELSE
			TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_LEVEL_SELECTION_MENU)
			sTLGData.iReturnToMenuTimeout = 0
		ENDIF
		
		
	ENDIF
	
ENDPROC

FUNC INT TLG_GET_NEXT_FX_INDEX()
	
	INT i
	FOR i = 0 TO ciTLG_MAX_FX  -1
		IF sTLGData.sFXData[i].bActive = FALSE
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN 0
	
ENDFUNC

PROC TWS_CREATE_FX(VECTOR_2D vPos, TLG_FX_TYPE eType, INT iLane)

	INT iIndex = TLG_GET_NEXT_FX_INDEX()
	
	sTLGData.sFXData[iIndex].vSpritePos = vPos
	sTLGData.sFXData[iIndex].eFXType = eType
	IF iLane = ciTLG_LANE_POPUP_LHS
	OR iLane = ciTLG_LANE_POPUP_RHS
		iLane= ciTLG_LANE_BACKGROUND
	ENDIF
	sTLGData.sFXData[iIndex].iLane = iLane
	sTLGData.sFXData[iIndex].iSpriteBlastAnimFrame = 0
	sTLGData.sFXData[iIndex].bActive = TRUE
	
ENDPROC

PROC TLG_ACTIVATE_ITEM(INT iIndex)
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ACTIVATE_ITEM] {DSW} Called. Item: ", iIndex, " Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[iIndex].eItemType), " Pos: ", sTLGData.sItemData[iIndex].vSpritePos.x, ", ", sTLGData.sItemData[iIndex].vSpritePos.y)
	#ENDIF
//	INT iInClip
//	INT iTotal
	INT iRand
	TLG_PLAYER_WEAPON_DATA weaponData
	SWITCH sTLGData.sItemData[iIndex].eItemType
	
		CASE TLG_MACHINE_GUN_AMMO
	//		sTLGData.sPlayerData[0].iWeapon = 1
		//	sTLGData.sPlayerData[0].iAmmo = 1000
			TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_GATLING)
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_GATLING_GUN)
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
		BREAK
		
		CASE TLG_HEALTH
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PICKUP_HEALTH)
			sTLGData.sPlayerData[0].fHealth = CLAMP(sTLGData.sPlayerData[0].fHealth + 0.5, 0.0, 1.0)
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
		BREAK
		
		CASE TLG_REVOLVER_AMMO
			weaponData = TLG_GET_PLAYER_WEAPON_DATA(TLG_PLAYER_WEAPON_PISTOL)
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_NEW_GUN)

			
			sTLGData.sPlayerData[0].weaponData[TLG_PLAYER_WEAPON_TYPE_DEFAULT].iAmmoInClip = weaponData.iClipSize
			sTLGData.sPlayerData[0].weaponData[TLG_PLAYER_WEAPON_TYPE_DEFAULT].iTotalAmmo = weaponData.iMaxAmmo
			
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
		BREAK
		
		CASE TLG_RIFLE_AMMO
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_RIFLE)
			TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_RIFLE)
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
		BREAK
		
		CASE TLG_SHOTGUN_AMMO
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SHOTGUN)
			TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_SHOTGUN)
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
		BREAK
		
		CASE TLG_SHERIFF
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PICKUP_SHERIFF)
			TLG_SET_PLAYER_BIT(TLG_PLAYER_BIT_INVULNERABILE)
			sTLGData.sPlayerData[0].iInvulnerableTimer = GET_GAME_TIMER()
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
			
			TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
			TLG_PLAY_SOUND_NO_POSITION(TLG_AUDIO_EFFECT_POWERUP)
		BREAK
		
		CASE TLG_SNAKE_OIL
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PICKUP_SNAKE_OIL)
			iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_ACTIVATE_ITEM] TLG_SNAKE_OIL - ", iRand)
			SWITCH iRand
				CASE 0
					//-- Restore health
					sTLGData.sPlayerData[0].fHealth = 1.0
				BREAK
				
				CASE 1
					//-- Damage player
					TLG_DAMAGE_PLAYER(TLG_DAMAGE_PLAYER_TYPE_SNAKE_OIL)
				BREAK
				
				CASE 2
					//-- Hide reticule
					TLG_SET_PLAYER_BIT(TLG_PLAYER_BIT_HIDE_RETICULE)
					sTLGData.sPlayerData[0].iHideReticuleTimer = GET_GAME_TIMER()
					
					TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
					TLG_PLAY_SOUND_NO_POSITION(TLG_AUDIO_EFFECT_POWERDOWN)
				BREAK
				
				CASE 3
					//-- Invulnerable
					TLG_SET_PLAYER_BIT(TLG_PLAYER_BIT_INVULNERABILE)
					TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
					TLG_PLAY_SOUND_NO_POSITION(TLG_AUDIO_EFFECT_POWERUP)
					sTLGData.sPlayerData[0].iInvulnerableTimer = GET_GAME_TIMER()
				BREAK
			ENDSWITCH
			
			sTLGData.sItemData[iIndex].bRendering = FALSE
			sTLGData.sItemData[iIndex].iRenderAfterWarpCount = 0
		BREAK
	ENDSWITCH
	
	TLG_INCREASE_POWERUPS_COLLECTED_FOR_CURRENT_LEVEL()
ENDPROC

FUNC BOOL TLG_CHECK_FOR_HEADSHOT(VECTOR_2D vSpriteTL, VECTOR_2D vBullet, INT iLane, TLG_ENEMY_TYPES etype)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_CHECK_FOR_HEADSHOT] ")
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_CHECK_FOR_HEADSHOT] Lane: ", iLane)
	
	FLOAT fAbsfDist = ABSF(vBullet.Y - vSpriteTL.y)
	FLOAT fYMin, fYMax
	
	IF etype != TLG_OUTHOUSE_ENEMY
	AND etype != TLG_COFFIN_ENEMY
		SWITCH iLane
			CASE 0 // Back of screen
				fYMin = cfTLG_HEADSHOT_LANE0_MIN_Y
				fYMax = cfTLG_HEADSHOT_LANE0_MAX_Y
			BREAK
			
			CASE 1 // Middle of screen
				fYMin = cfTLG_HEADSHOT_LANE1_MIN_Y
				fYMax = cfTLG_HEADSHOT_LANE1_MAX_Y
			BREAK
			
			CASE 2 // Front of screen
				fYMin = cfTLG_HEADSHOT_LANE2_MIN_Y
				fYMax = cfTLG_HEADSHOT_LANE2_MAX_Y
			BREAK
		ENDSWITCH
	ELSE
		IF etype = TLG_OUTHOUSE_ENEMY
			fYMin = cfTLG_HEADSHOT_OUTHOUSE_MIN_Y
			fyMax = cfTLG_HEADSHOT_OUTHOUSE_MAX_Y
		ELSE
			fYMin = cfTLG_HEADSHOT_COFFIN_MIN_Y
			fyMax = cfTLG_HEADSHOT_COFFIN_MAX_Y
		ENDIF
	ENDIF
	
	fAbsfDist = ABSF(vBullet.Y - vSpriteTL.y)
	IF fAbsfDist >= fYMin 
	AND fAbsfDist <= fYMax
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_CHECK_FOR_HEADSHOT] HEADSHOT!")
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_CHECK_FOR_HEADSHOT] No Headshot - Diff: ", fAbsfDist, " Min: ", fYMin, " Max : ", fYMax)
	
	
	RETURN FALSE
ENDFUNC

PROC TLG_PLAY_AUDIO_FOR_SHOT_COVER(INT iCover)
	TLG_ASSET_TYPES eType = sTLGData.sCoverData[iCover].eType
	VECTOR_2D vPos = sTLGData.sCoverData[iCover].vSpritePos
	
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[TLG_PLAY_AUDIO_FOR_SHOT_COVER] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] {dsw} Cover ", iCover, " Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[iCover].eType))
	#ENDIF
	
	SWITCH eType
		
		CASE TLG_ASSET_CACTUS_1
		CASE TLG_ASSET_CACTUS_2
		CASE TLG_ASSET_CACTUS_3
		CASE TLG_ASSET_CACTUS_4
		CASE TLG_ASSET_CACTUS_5
		CASE TLG_ASSET_CACTUS_6
		CASE TLG_ASSET_CACTUS_7
		CASE TLG_ASSET_CACTUS_8
		CASE TLG_ASSET_CACTUS_9
		CASE TLG_ASSET_CACTUS_10
		CASE TLG_ASSET_CACTUS_11
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_CACTUS, vPos)
		BREAK
		
		CASE TLG_ASSET_CRATE_1
		CASE TLG_ASSET_BUILDING_1
		CASE TLG_ASSET_BUILDING_2
		CASE TLG_ASSET_BUILDING_3
		CASE TLG_ASSET_BUILDING_4
		CASE TLG_ASSET_BUILDING_5
		CASE TLG_ASSET_BUILDING_6
		CASE TLG_ASSET_BUILDING_7
		CASE TLG_ASSET_BUILDING_8
		CASE TLG_ASSET_FENCE
		CASE TLG_ASSET_FENCE_SMALL
		CASE TLG_ASSET_FOREST_TREE_1
		CASE TLG_ASSET_FOREST_TREE_2
		CASE TLG_ASSET_STACKED_CRATES_1
		CASE TLG_ASSET_STACKED_CRATES_2
		CASE TLG_ASSET_SPECIAL_TNT_CRATE
		CASE TLG_ASSET_SPECIAL_TNT_BARREL_1
		CASE TLG_ASSET_SPECIAL_TNT_BARREL_2
		CASE TLG_ASSET_BARRELS
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_BARREL, vPos)
		BREAK
		
		CASE TLG_ASSET_GRASS
		CASE TLG_ASSET_BUSH_1
		CASE TLG_ASSET_BUSH_2
		
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_BUSH, vPos)
		BREAK
		
		CASE TLG_ASSET_BIG_TOMB
		CASE TLG_ASSET_TOMBSTONE_1
		CASE TLG_ASSET_TOMBSTONE_2
		CASE TLG_ASSET_TOMBSTONE_3
		CASE TLG_ASSET_TOMBSTONE_4
		CASE TLG_ASSET_TOMBSTONE_5
		CASE TLG_ASSET_ROCK_SMALL
		CASE TLG_ASSET_ROCK_LARGE
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_ROCK, vPos)
		BREAK
		
		CASE TLG_ASSET_COFFIN
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_COFFIN, vPos)
		BREAK
		
		CASE TLG_ASSET_OUTHOUSE
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_LATRINE, vPos)
		BREAK
		
		DEFAULT
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_GENERIC, vPos)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT TLG_GET_NUMBER_OF_PICKUPS_PLACED_INSIDE_COFFINS_FOR_CURRENT_SCREEN()
	INT i
	INT iCount
	FOR i = 0 TO ciTLG_MAX_COVER - 1
		IF sTLGData.sCoverData[i].bRendering
			IF sTLGData.sCoverData[i].eType = TLG_ASSET_COFFIN
				IF TLG_IS_POSITION_ON_SCREEN(sTLGData.sCoverData[i].vSpritePos)
					IF sTLGData.sCoverData[i].bSpawnedPickup
						iCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iCount
ENDFUNC
PROC TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER(	INT iCover, INT iPlayer, INT iProjectile, BOOL &bProcessedThisFrame  )
	IF bProcessedThisFrame 
		EXIT
	ENDIF
	
	INT iRandom
	INT iPlaced
	VECTOR_2D vPickupLoc
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].bHitObject = TRUE
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].iTimeOfHit = GET_GAME_TIMER()
	sTLGData.sCoverData[iCover].iHitCount++
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] {dsw} Cover ", iCover, " Shot. Hit count: ", sTLGData.sCoverData[iCover].iHitCount, " Lane: ", sTLGData.sCoverData[iCover].iCoverLane, " Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[iCover].eType), " Special? ",  sTLGData.sCoverData[iCover].bIsSpecialCover)
	#ENDIF
	
	IF sTLGData.sCoverData[iCover].bIsSpecialCover
		sTLGData.sCoverData[iCover].bRendering = FALSE
		TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_EXP_GENERIC, sTLGData.sCoverData[iCover].vSpritePos)
		
		TWS_CREATE_FX(sTLGData.sCoverData[iCover].vSpritePos, TLG_FX_EXPLOSION, sTLGData.sCoverData[iCover].iCoverLane)
		
		//-- When we explode a TNT barrel, replace it with another at same spot, but don't render until after it's warped around again
		IF sTLGData.sCoverData[iCover].eType = TLG_ASSET_SPECIAL_TNT_BARREL_1
		OR sTLGData.sCoverData[iCover].eType = TLG_ASSET_SPECIAL_TNT_CRATE
			TLG_ADD_COVER_TO_LIST(sTLGData.sCoverData[iCover].vSpritePos, sTLGData.sCoverData[iCover].eType, 	sTLGData.sCoverData[iCover].iCoverLane, DEFAULT, sTLGData.sCoverData[iCover].iScreen, DEFAULT, 1)
		ENDIF
		
		TLG_KILL_ALL_ENEMIES_IN_RANGE(sTLGData.sCoverData[iCover].vSpritePos)
		TLG_ADD_TO_LOCAL_PLAYER_SCORE(TLG_SCORE_TYPE_SCENERY)
		
	ELIF sTLGData.sCoverData[iCover].bIsAnimated
		
		IF sTLGData.sCoverData[iCover].bRendering
			IF sTLGData.sCoverData[iCover].eType = TLG_ASSET_CRATE_1
				IF sTLGData.sCoverData[iCover].iHitCount > 2
					sTLGData.sCoverData[iCover].bRendering = FALSE
					TWS_CREATE_FX(sTLGData.sCoverData[iCover].vSpritePos, TLG_FX_LOOT_CRATE, 1)
					CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] Crate ", iCover, " Spawing pickup here: ", sTLGData.sCoverData[iCover].vSpritePos.x, ", ", sTLGData.sCoverData[iCover].vSpritePos.y)
					IF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_DESERT
						TLG_ADD_ITEM_TO_LIST(INIT_VECTOR_2D(sTLGData.sCoverData[iCover].vSpritePos.x, sTLGData.sCoverData[iCover].vSpritePos.y), TLG_HEALTH, ciTLG_LANE_FOREGROUND)
					ELIF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_TOWN
						TLG_ADD_ITEM_TO_LIST(INIT_VECTOR_2D(sTLGData.sCoverData[iCover].vSpritePos.x, sTLGData.sCoverData[iCover].vSpritePos.y), TLG_SHERIFF, sTLGData.sCoverData[iCover].iCoverLane)
					ENDIF
				ENDIF			
			ELIF sTLGData.sCoverData[iCover].eType = TLG_ASSET_OUTHOUSE
				IF NOT sTLGData.sCoverData[iCover].bSpawnedPickup
					IF sTLGData.sCoverData[iCover].iHitCount > 0
						sTLGData.sCoverData[iCover].bSpawnedPickup = TRUE
						vPickupLoc = sTLGData.sCoverData[iCover].vSpritePos
						vPickupLoc.x += cfTLG_PICKUP_OUTHOUSE_OFFSET_X
						vPickupLoc.y += cfTLG_PICKUP_OUTHOUSE_OFFSET_Y
						TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_REVOLVER_AMMO, ciTLG_LANE_FOREGROUND)
						
						CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] Spawned ammo inside outhouse ", iCover)
						sTLGData.sCoverData[iCover].bSpawnedPickup = TRUE
					ENDIF
				ENDIF
			ELIF sTLGData.sCoverData[iCover].eType = TLG_ASSET_COFFIN
				IF NOT sTLGData.sCoverData[iCover].bSpawnedPickup
					IF sTLGData.sCoverData[iCover].iHitCount > 0
					//	sTLGData.sCoverData[iCover].bSpawnedPickup = TRUE
						vPickupLoc = sTLGData.sCoverData[iCover].vSpritePos
						vPickupLoc.x += cfTLG_PICKUP_COFFIN_OFFSET_X
						vPickupLoc.y += cfTLG_PICKUP_COFFIN_OFFSET_Y
						
						// N.B Pickups in foreground lane (i.e. those in coffins / outhouses) wil not warp, and will vanish when off screne
						
						iRandom = GET_RANDOM_INT_IN_RANGE(0, 8)
						
						//-- If we're stopping on screen 2 in the graveyard level, want to force either a gatling gun pickup or a health
						//-- pickup
						IF TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_GRAVEYARD
							IF TLG_IS_LEVEL_STOPPED()
							OR TLG_IS_MOVING_LEVEL_ABOUT_TO_STOP()
								iPlaced = TLG_GET_NUMBER_OF_PICKUPS_PLACED_INSIDE_COFFINS_FOR_CURRENT_SCREEN()
								IF iPlaced = 0
									CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] Graveyard level - stopped - haven't placed any pickups inside coffin forcing gatling gun")
									iRandom = 8
								ELIF iPlaced = 1
									CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] Graveyard level - stopped - placed 1 pickupinside coffin forcing health")
									iRandom = 5
								ELSE
									CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER]  Placed too many inside coffin - iPlaced: ", iPlaced) 
								ENDIF
							ENDIF
						ENDIF
						
						SWITCH iRandom
							CASE 0		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_RIFLE_AMMO, 	ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
							CASE 1		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_SHOTGUN_AMMO, 	ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
							CASE 2		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_REVOLVER_AMMO, ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
							CASE 3		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_HEALTH, 		ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
							CASE 4		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_SHERIFF, 		ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
							CASE 5		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_SNAKE_OIL, 	ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
							CASE 8		TLG_ADD_ITEM_TO_LIST(vPickupLoc, TLG_MACHINE_GUN_AMMO, 	ciTLG_LANE_FOREGROUND, DEFAULT, 0.75)	BREAK
						ENDSWITCH
						
						
						
						CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER] Spawned ammo inside outhouse ", iCover)
						sTLGData.sCoverData[iCover].bSpawnedPickup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	TLG_PLAY_AUDIO_FOR_SHOT_COVER(iCover)
	
	bProcessedThisFrame = TRUE
ENDPROC

FUNC INT TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED(INT iEnemy, BOOL bHeadshot)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} ")
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} Enemy ", iEnemy, " Shot at least once. Lane: ", sTLGData.sEnemyData[iEnemy].iEnemyLane, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType), " Headshot? ", bHeadshot)
	#ENDIF
		
	INT iBaseDamage
	INT iRand
	IF bHeadshot
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} was headshot. Damage: ", TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[iEnemy].iEnemyType))
		RETURN TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[iEnemy].iEnemyType)
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_OUTHOUSE_ENEMY
	OR sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_COFFIN_ENEMY
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} Coffinb / outhouse enemy. Damage: ", TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[iEnemy].iEnemyType))
		RETURN TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[iEnemy].iEnemyType)
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].iEnemyLane = ciTLG_LANE_FOREGROUND
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} Foreground enemy. Damage: ", TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[iEnemy].iEnemyType))
		RETURN TLG_GET_ENEMY_START_HEALTH_FOR_TYPE(sTLGData.sEnemyData[iEnemy].iEnemyType)
	ENDIF
	
	TLG_PLAYER_WEAPON weapon = TLG_GET_PLAYER_CURRENT_WEAPON()
	TLG_PLAYER_WEAPON_DATA data = TLG_GET_PLAYER_WEAPON_DATA(weapon)
	iBaseDamage = data.iMinDamagePerShot
	
	IF sTLGData.sPlayerData[0].weaponType = TLG_PLAYER_WEAPON_TYPE_DEFAULT
		iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		iBaseDamage += iRand
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} Player has pistol. Damage: ", iBaseDamage)
		RETURN iBaseDamage
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} Player has weapon: ", TLG_GET_PLAYER_WEAPON_AS_STRING(weapon), " Returning: ", data.iMinDamagePerShot)
	#ENDIF
	
	RETURN data.iMinDamagePerShot
ENDFUNC

PROC TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(	INT iEnemy, INT iPlayer, INT iProjectile, VECTOR_2D vSpriteTopLeft, VECTOR_2D vBulletPos )
	BOOL bHeadshot
	
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].bHit = TRUE
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].iTimeOfHit = GET_GAME_TIMER()
	
	IF NOT sTLGData.sEnemyData[iEnemy].bShotAtLeastOnce
		sTLGData.sEnemyData[iEnemy].bShotAtLeastOnce = TRUE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY] {dsw} Enemy ", iEnemy, " Shot at least once. Lane: ", sTLGData.sEnemyData[iEnemy].iEnemyLane, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType))
		#ENDIF
	ENDIF
		
	IF sTLGData.sEnemyData[iEnemy].iEnemyType != TLG_OUTHOUSE_ENEMY
	AND sTLGData.sEnemyData[iEnemy].iEnemyType != TLG_COFFIN_ENEMY
		bHeadshot = TLG_CHECK_FOR_HEADSHOT( vSpriteTopLeft, vBulletPos, sTLGData.sEnemyData[iEnemy].iEnemyLane, sTLGData.sEnemyData[iEnemy].iEnemyType )
		sTLGData.sEnemyData[iEnemy].iHealth -= TLG_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED(iEnemy, bHeadshot)
		IF sTLGData.sEnemyData[iEnemy].iHealth > 0
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_BODY, sTLGData.sEnemyData[iEnemy].vSpritePos)
			TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_INJURED, sTLGData.sEnemyData[iEnemy].vSpritePos)
			TLG_INCREASE_ENEMY_HIT_COUNT_FOR_CURRENT_LEVEL()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Enemy ", iEnemy, "  Has been injured. New Health: ", sTLGData.sEnemyData[iEnemy].iHealth, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType))
			#ENDIF
		ELSE
			
			IF bHeadshot
			AND NOT TLG_IS_BEAR(iEnemy)
				IF NOT TLG_IS_HELP_TEXT_BIT_SET(TLG_HELP_TEXT_BIT_AT_LEAST_ONE_HEADSHOT)
					TLG_SET_HELP_TEXT_BIT(TLG_HELP_TEXT_BIT_AT_LEAST_ONE_HEADSHOT)
				ENDIF
				TLG_ADD_TO_LOCAL_PLAYER_SCORE( TLG_SCORE_TYPE_ENEMY_HEADSHOT )
				TLG_INCREASE_ENEMY_HEADSHOT_COUNT_FOR_CURRENT_LEVEL()
				TLG_INCREASE_ENEMY_HIT_COUNT_FOR_CURRENT_LEVEL()
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SHOOT_HEADSHOT)
			ELSE
				TLG_ADD_TO_LOCAL_PLAYER_SCORE( TLG_SCORE_TYPE_ENEMY )
				TLG_INCREASE_ENEMY_HIT_COUNT_FOR_CURRENT_LEVEL()
			ENDIF
			
			IF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_CART_1
				TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_EXP_CART, sTLGData.sEnemyData[iEnemy].vSpritePos)
			ELIF TLG_IS_BEAR(iEnemy)
				TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BEAR_DEATH, sTLGData.sEnemyData[iEnemy].vSpritePos)
			ELSE
				TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_BODY, sTLGData.sEnemyData[iEnemy].vSpritePos)
				TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_DEAD, sTLGData.sEnemyData[iEnemy].vSpritePos)
			ENDIF
			sTLGData.sEnemyData[iEnemy].eState = TLG_DYING
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Enemy ", iEnemy, "  set as Dying as iHealth <= 0. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType))
			#ENDIF
		ENDIF
	ELSE  
		IF NOT sTLGData.sEnemyData[iEnemy].bOuthouseAudio
			IF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_OUTHOUSE_ENEMY
				TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_PLAYER_SHOOT_OUTHOUSE, sTLGData.sEnemyData[iEnemy].vSpritePos)
				TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_FART, sTLGData.sEnemyData[iEnemy].vSpritePos)
			ELIF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_COFFIN_ENEMY
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SHOOT_COFFIN)
			ENDIF
			sTLGData.sEnemyData[iEnemy].bOuthouseAudio = TRUE
		ENDIF
		
		//-- Need to handle enemies hiding in outhouses differently
		IF sTLGData.sEnemyData[iEnemy].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES - 2
			sTLGData.sEnemyData[iEnemy].iHealth -= 10
			bHeadshot = TLG_CHECK_FOR_HEADSHOT( vSpriteTopLeft, vBulletPos, sTLGData.sEnemyData[iEnemy].iEnemyLane, sTLGData.sEnemyData[iEnemy].iEnemyType )
			IF bHeadshot
				TLG_ADD_TO_LOCAL_PLAYER_SCORE( TLG_SCORE_TYPE_ENEMY_HEADSHOT )
				TLG_INCREASE_ENEMY_HIT_COUNT_FOR_CURRENT_LEVEL()
				TLG_INCREASE_ENEMY_HEADSHOT_COUNT_FOR_CURRENT_LEVEL()
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SHOOT_HEADSHOT)
			ELSE
				TLG_ADD_TO_LOCAL_PLAYER_SCORE( TLG_SCORE_TYPE_ENEMY )
				TLG_INCREASE_ENEMY_HIT_COUNT_FOR_CURRENT_LEVEL()
			ENDIF
			IF sTLGData.sEnemyData[iEnemy].iHealth <= 0
				sTLGData.sEnemyData[iEnemy].eState = TLG_DYING
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} OUTHOUSE Enemy ", iEnemy, "  set as Dying as iHealth <= 0. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType))
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE( INT iHostage, INT iPlayer, INT iProjectile, BOOL &bProcessedThisFrame  )
	IF bProcessedThisFrame
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE] {dsw}  Hit hostage", iHostage, " Type: ", TLG_GET_HOSTAGE_TYPE_AS_STRING(sTLGData.sHostageData[iHostage].eHostageType))
	#ENDIF
	
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].bHit = TRUE
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].iTimeOfHit = GET_GAME_TIMER()
	
	sTLGData.sHostageData[iHostage].iHealth -= 10
	
	IF NOT sTLGData.sHostageData[iHostage].bDying = TRUE
		IF sTLGData.sHostageData[iHostage].iHealth <= 0
			sTLGData.sHostageData[iHostage].bDying = TRUE
			
			TLG_PLAY_HOSTAGE_DEAD_AUDIO(sTLGData.sHostageData[iHostage].eHostageType, sTLGData.sHostageData[iHostage].vSpritePos)
			
			IF sTLGData.sHostageData[iHostage].eHostageType = TLG_EAGLE
			OR sTLGData.sHostageData[iHostage].eHostageType = TLG_VULTURE
				TLG_INCREASE_BIRDS_SHOT_COUNT_FOR_CURRENT_LEVEL()
				TLG_ADD_TO_LOCAL_PLAYER_SCORE(TLG_SCORE_TYPE_ANIMAL)
				TWS_CREATE_FX( sTLGData.sHostageData[iHostage].vSpritePos, TLG_FX_FEATHERS,  0)
			ELIF sTLGData.sHostageData[iHostage].eHostageType = TLG_BAT
				TLG_INCREASE_BIRDS_SHOT_COUNT_FOR_CURRENT_LEVEL()
				TLG_ADD_TO_LOCAL_PLAYER_SCORE(TLG_SCORE_TYPE_ANIMAL)

			ELIF sTLGData.sHostageData[iHostage].eHostageType = TLG_BAT
			OR sTLGData.sHostageData[iHostage].eHostageType = TLG_MOUSE
			OR sTLGData.sHostageData[iHostage].eHostageType = TLG_SCORPION
				TLG_ADD_TO_LOCAL_PLAYER_SCORE(TLG_SCORE_TYPE_ANIMAL)
			ENDIF
		ENDIF
	ENDIF
	
	bProcessedThisFrame = TRUE
ENDPROC

PROC TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM( INT iItem, INT iPlayer, INT iProjectile,BOOL &bProcessedThisFrame )
	IF bProcessedThisFrame
		EXIT
	ENDIF
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM] {dsw}  Hit item", iItem, " Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[iItem].eItemType))
	#ENDIF
	
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].bHit = TRUE
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].iTimeOfHit = GET_GAME_TIMER()
	
	sTLGData.sItemData[iItem].bShot = TRUE
	
	TLG_ACTIVATE_ITEM(iItem)
	
	bProcessedThisFrame = TRUE
ENDPROC

PROC TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_THROWN_WEAPON( INT iWeapon, INT iPlayer, INT iProjectile , BOOL &bProcessedThisFrame)
	IF bProcessedThisFrame
		EXIT
	ENDIF
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_THROWN_WEAPON] {dsw}  Hit thrown weapon ", iWeapon, " Type: ", TLG_GET_ENEMY_THROWN_PROJECTILE_TYPE_AS_STRING(sTLGData.sEnemyProjectiles[iWeapon].eType))
	#ENDIF
				
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].bHit = TRUE
	sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].iTimeOfHit = GET_GAME_TIMER()
	
	sTLGData.sEnemyProjectiles[iWeapon].eState = 	TLG_ENEMY_PROJECTILE_STATE_EXPLODE
	TWS_CREATE_FX(sTLGData.sEnemyProjectiles[iWeapon].vSpritePos, TLG_FX_ENEMY_TNT_EXP, 1)
	SET_CONTROL_SHAKE(FRONTEND_CONTROL, 100, 20)
	TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_EXP_DYNAMITE, sTLGData.sEnemyProjectiles[iWeapon].vSpritePos)
	
	//TLG_KILL_ALL_ENEMIES_IN_AREA(sTLGData.sEnemyProjectiles[iWeapon].vSpritePos, INIT_VECTOR_2D(cfTLG_SPRITE_ENEMY_THROWN_EXPLOSION_LARGE_WIDTH, cfTLG_SPRITE_ENEMY_THROWN_EXPLOSION_LARGE_HEIGHT), 2.0)
	TLG_KILL_ALL_ENEMIES_IN_RANGE(sTLGData.sEnemyProjectiles[iWeapon].vSpritePos)
	
	bProcessedThisFrame = TRUE		
ENDPROC


PROC HANDLE_PLAYER_BULLET_COLLISION(INT iPlayer, INT iProjectile)
	
	IF NOT sTLGData.sPlayerData[iPlayer].bHasJustShot
		EXIT
	ENDIF
	
	
	INT i
	TLG_PLAYER_WEAPON_DATA weaponData
	weaponData = TLG_GET_PLAYER_WEAPON_DATA(TLG_GET_PLAYER_CURRENT_WEAPON())
	
	FLOAT fSpreadModifier = weaponData.fSpreadModifier
	
	INT iMaxEnemyImpacts = weaponData.iMaxEnemyImpacts
	
	
	FLOAT fDistance, fDistanceX, fDistanceY, x1, x2, y1, y2 // Top-left & Bottom-right corners of collision
	VECTOR_2D vBulletPos = INIT_VECTOR_2D(sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X, sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y)
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] Called. Player Weapon: ", TLG_GET_PLAYER_WEAPON_AS_STRING(TLG_GET_PLAYER_CURRENT_WEAPON()), " Bullet Pos: ", vBulletPos.x, ", ", vBulletPos.y)
	#ENDIF
						
	BOOL bHitSomething = FALSE
	
	INT iEnemyHit[ciTLG_MAX_ENEMY_SHOT_AT_ONCE]
	INT iCurrentNumberOfEnemyHit = 0
	
	FOR i = 0 TO ciTLG_MAX_ENEMY_SHOT_AT_ONCE -1
		iEnemyHit[i] = -1
	ENDFOR
	
	INT iCoverHit = -1
	INT iHostageHit = -1
	INT iItemHit = -1
	INT iThrownHit = -1
	
	FLOAT fModifier
	FLOAT fWidth
	FLOAT fHeight
	FOR i = 0 TO ciTLG_MAX_COVER -1
	
		IF sTLGData.sCoverData[i].bRendering
			fModifier = TLG_GET_COVER_SIZE_MODIFIER_FOR_COLLISION_CHECK(sTLGData.sCoverData[i].eType)
			fWidth = sTLGData.sCoverData[i].fCoverWidth * fModifier
			fHeight = sTLGData.sCoverData[i].fCoverHeight * fModifier
			
			x1 = sTLGData.sCoverData[i].vSpritePos.x - (fWidth/2)
			y1 = sTLGData.sCoverData[i].vSpritePos.y - (fHeight/2)
			
			x2 = sTLGData.sCoverData[i].vSpritePos.x + (fWidth/2)
			y2 = sTLGData.sCoverData[i].vSpritePos.y + (fHeight/2)
			
			IF (sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X > x1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X < x2
			AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y > y1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y < y2)
				IF iCoverHit = -1
				OR sTLGData.sCoverData[i].iCoverLane >  sTLGData.sCoverData[iCoverHit].iCoverLane
				OR (sTLGData.sCoverData[i].bIsSpecialCover AND NOT sTLGData.sCoverData[iCoverHit].bIsSpecialCover)
				OR (sTLGData.sCoverData[i].bIsAnimated AND NOT sTLGData.sCoverData[iCoverHit].bIsAnimated)
					bHitSomething = TRUE
					iCoverHit = i
					
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Cover hit: ",i, " Lane: ", sTLGData.sCoverData[i].iCoverLane, "  Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType), " Modifier: ", fModifier)
						CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Cover hit: ",i, " Bullet: ",vBulletPos.x, ", ", vBulletPos.y, " Point1: ", x1, ", ", y1, " Point 2: ",x2, ", ", y2) 
					#ENDIF
				//	TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER(i,iPlayer, iProjectile )
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
		
	BOOL bBulletHitEnemyCoordCheck
	FLOAT fSpreadDistanceX
	FLOAT fSpreadDistanceY
	FOR i = 0 TO ciTLG_MAX_ENEMIES  -1
	
		bBulletHitEnemyCoordCheck = FALSE
		IF sTLGData.sEnemyData[i].eState = TLG_SHOOTING
		OR sTLGData.sEnemyData[i].eState = TLG_ACTIVE
			IF sTLGData.sEnemyData[i].iHealth > 0

				x1 = sTLGData.sEnemyData[i].vSpritePos.x - (sTLGData.sEnemyData[i].vSize.x/2)
				y1 = sTLGData.sEnemyData[i].vSpritePos.y - (sTLGData.sEnemyData[i].vSize.y/2)
				
				x2 = sTLGData.sEnemyData[i].vSpritePos.x + (sTLGData.sEnemyData[i].vSize.x/2)
				y2 = sTLGData.sEnemyData[i].vSpritePos.y + (sTLGData.sEnemyData[i].vSize.y/2)
				
				IF fSpreadModifier = 1.0
					IF (vBulletPos.X > x1 AND vBulletPos.X < x2
					AND vBulletPos.Y > y1 AND vBulletPos.Y < y2)
						bBulletHitEnemyCoordCheck = TRUE
					ENDIF
				ELSE
					fSpreadDistanceX = sTLGData.sEnemyData[i].vSpritePos.x - vBulletPos.X	
					fSpreadDistanceY = sTLGData.sEnemyData[i].vSpritePos.Y - vBulletPos.Y	
					IF ((fSpreadDistanceX * fSpreadDistanceX) + (fSpreadDistanceY * fSpreadDistanceY ) < (fSpreadModifier * fSpreadModifier))
						bBulletHitEnemyCoordCheck = TRUE
						CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Enemy SPREAD hit: ",i, " fSpreadModifier: ", fSpreadModifier, " Current weapon: ", TLG_GET_PLAYER_WEAPON_AS_STRING(TLG_GET_PLAYER_CURRENT_WEAPON()))
					ENDIF
				ENDIF
				
				IF bBulletHitEnemyCoordCheck
					IF iMaxEnemyImpacts = 1
						IF iEnemyHit[iCurrentNumberOfEnemyHit] = -1 
						OR sTLGData.sEnemyData[i].iEnemyLane >  sTLGData.sEnemyData[iEnemyHit[iCurrentNumberOfEnemyHit]].iEnemyLane
						OR sTLGData.sEnemyData[iEnemyHit[iCurrentNumberOfEnemyHit]].iEnemyType = TLG_OUTHOUSE_ENEMY // update to enemy in front of enemy in outshouse
						OR sTLGData.sEnemyData[iEnemyHit[iCurrentNumberOfEnemyHit]].iEnemyType = TLG_COFFIN_ENEMY
							bHitSomething = TRUE
							iEnemyHit[iCurrentNumberOfEnemyHit] = i
							iCurrentNumberOfEnemyHit++
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Enemy hit: ",i, " Lane: ", sTLGData.sEnemyData[i].iEnemyLane, "  Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
								CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Enemy hit: ",i, " Bullet: ",vBulletPos.x, ", ", vBulletPos.y, " Point1: ", x1, ", ", y1, " Point2: ",x2, ", ", y2) 
								CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Number of Potential Enemy hits: ", iCurrentNumberOfEnemyHit, " Max with weapon: ", iMaxEnemyImpacts, " Weapon: ", TLG_GET_PLAYER_WEAPON_AS_STRING(TLG_GET_PLAYER_CURRENT_WEAPON()))
							#ENDIF
						//	TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(i,iPlayer, iProjectile, INIT_VECTOR_2D(x1, y1), vBulletPos )
						ENDIF
					ELSE
						IF iCurrentNumberOfEnemyHit < iMaxEnemyImpacts
							IF iEnemyHit[iCurrentNumberOfEnemyHit] = -1
								bHitSomething = TRUE
								iEnemyHit[iCurrentNumberOfEnemyHit] = i
								iCurrentNumberOfEnemyHit++
							
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Enemy hit: ",i, " Lane: ", sTLGData.sEnemyData[i].iEnemyLane, "  Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
									CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Enemy hit: ",i, " Bullet: ",vBulletPos.x, ", ", vBulletPos.y, " Point1: ", x1, ", ", y1, " Point2: ",x2, ", ", y2) 
									CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Number of Potential Enemy hits: ", iCurrentNumberOfEnemyHit, " Max with weapon: ", iMaxEnemyImpacts, " Weapon: ", TLG_GET_PLAYER_WEAPON_AS_STRING(TLG_GET_PLAYER_CURRENT_WEAPON()))
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
		
	FOR i = 0 TO ciTLG_MAX_HOSTAGES  -1
	
		IF sTLGData.sHostageData[i].iHealth > 0
			x1 = sTLGData.sHostageData[i].vSpritePos.x - (sTLGData.sHostageData[i].vSize.x/2)
			y1 = sTLGData.sHostageData[i].vSpritePos.y - (sTLGData.sHostageData[i].vSize.y/2)
			
			x2 = sTLGData.sHostageData[i].vSpritePos.x + (sTLGData.sHostageData[i].vSize.x/2)
			y2 = sTLGData.sHostageData[i].vSpritePos.y + (sTLGData.sHostageData[i].vSize.y/2)
			
			IF (sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X > x1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X < x2
			AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y > y1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y < y2)
				IF iHostageHit = -1
				OR sTLGData.sHostageData[i].iHostageLane > sTLGData.sHostageData[iHostageHit].iHostageLane
				//	TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(i,iPlayer, iProjectile )
					iHostageHit = i
					
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Hostage hit: ",i, " Lane: ", sTLGData.sHostageData[i].iHostageLane, "  Type: ", TLG_GET_HOSTAGE_TYPE_AS_STRING(sTLGData.sHostageData[i].eHostageType))
					#ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO ciTLG_MAX_ITEMS  -1
		
		IF NOT sTLGData.sItemData[i].bShot
			IF sTLGData.sItemData[i].bRendering
				fDistanceX = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.x - sTLGData.sItemData[i].vSpritePos.x
				fDistanceY = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.y - sTLGData.sItemData[i].vSpritePos.y
				fDistance = SQRT(fDistanceX * fDistanceX + fDistanceY * fDistanceY)
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Checking item: ", i, " Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[i].eItemType), " Pos: ", sTLGData.sItemData[i].vSpritePos.x, ", ", sTLGData.sItemData[i].vSpritePos.y, " Rendering: ", sTLGData.sItemData[i].bRendering, " Dist from bullet : ", fDistance)
				#ENDIF
				
				IF fDistance < 60
					IF iItemHit = -1
					OR sTLGData.sItemData[i].iLane > sTLGData.sItemData[iItemHit].iLane
				//		TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM(i,iPlayer, iProjectile )
						iItemHit = i
						bHitSomething = TRUE
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential Item hit: ",i, " Lane: ", sTLGData.sItemData[i].iLane, "  Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[i].eItemType))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Not checking item as bShot Item: ",i, " Lane: ", sTLGData.sItemData[i].iLane, "  Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[i].eItemType),  " Pos: ", sTLGData.sItemData[i].vSpritePos.x, ", ", sTLGData.sItemData[i].vSpritePos.y)
			#ENDIF
		ENDIF
	ENDFOR
	
	VECTOR_2D v2ProjSize
	FOR i = 0 TO ciTLG_MAX_ENEMY_PROJECTILES  -1
		IF sTLGData.sEnemyProjectiles[i].eType = TLG_ENEMY_PROJECTILE_DYNAMITE
			IF sTLGData.sEnemyProjectiles[i].eState = TLG_ENEMY_PROJECTILE_STATE_AIRBORNE
				SWITCH sTLGData.sEnemyProjectiles[i].iSpriteAnimFrame
					CASE 0			v2ProjSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_SMALL_WIDTH, ciTLG_DYNAMITE_SMALL_HEIGHT)		BREAK
					CASE 1			v2ProjSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_SMALL_WIDTH, ciTLG_DYNAMITE_SMALL_HEIGHT)		BREAK
					CASE 2			v2ProjSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_MEDIUM_WIDTH, ciTLG_DYNAMITE_MEDIUM_HEIGHT)		BREAK
					CASE 3			v2ProjSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_MEDIUM_WIDTH, ciTLG_DYNAMITE_MEDIUM_HEIGHT)		BREAK
					CASE 4			v2ProjSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_LARGE_WIDTH, ciTLG_DYNAMITE_LARGE_HEIGHT)		BREAK
					CASE 5			v2ProjSize = INIT_VECTOR_2D(ciTLG_DYNAMITE_LARGE_WIDTH, ciTLG_DYNAMITE_LARGE_HEIGHT)		BREAK
				ENDSWITCH
				
				x1 = sTLGData.sEnemyProjectiles[i].vSpritePos.x - (v2ProjSize.x/2)
				y1 = sTLGData.sEnemyProjectiles[i].vSpritePos.y - (v2ProjSize.y/2)
				
				x2 = sTLGData.sEnemyProjectiles[i].vSpritePos.x + (v2ProjSize.x/2)
				y2 = sTLGData.sEnemyProjectiles[i].vSpritePos.y + (v2ProjSize.y/2)
				
				IF (sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X > x1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X < x2
				AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y > y1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y < y2)
					IF iThrownHit = -1
					//	TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_THROWN_WEAPON(i, iPlayer, iProjectile)
						iThrownHit = i
						bHitSomething = TRUE
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Potential thrown weapon hit: ",i, " Lane: ", sTLGData.sEnemyProjectiles[i].iLane, "  Type: ", TLG_GET_ENEMY_THROWN_PROJECTILE_TYPE_AS_STRING(sTLGData.sEnemyProjectiles[i].eType))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	

	
	//-- Figure out what we actually hit.
	
	
	IF iThrownHit = -1 AND iCoverHit = -1 AND iItemHit = -1 AND iEnemyHit[0] = -1 AND iHostageHit = -1
		//-- Didn't hit anything
		TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BULLET_HIT_GENERIC, vBulletPos)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION]")
	CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Something's been shot....")
	IF iThrownHit = -1
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iThrownHit: ", iThrownHit)
	ELSE
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iThrownHit: ", iThrownHit, " Lane: N/A Type: ", TLG_GET_ENEMY_THROWN_PROJECTILE_TYPE_AS_STRING(sTLGData.sEnemyProjectiles[iThrownHit].eType))
	ENDIF
	
	IF iCoverHit = -1
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iCoverHit: ", iCoverHit)
	ELSE
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iCoverHit: ", iCoverHit, " Lane: ", sTLGData.sCoverData[iCoverHit].iCoverLane, " Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[iCoverHit].eType))
	ENDIF
	
	IF iItemHit = -1
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iItemHit: ", iItemHit)
	ELSE
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iItemHit: ", iItemHit, " Lane: ", sTLGData.sItemData[iItemHit].iLane, " Type: ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[iItemHit].eItemType))
	ENDIF
	
	i = 0
	FOR i = 0 TO iCurrentNumberOfEnemyHit - 1
		IF iEnemyHit[i] = -1
			CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iEnemyHit[", i, "]: ", iEnemyHit[i])
		ELSE
			CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iEnemyHit[", i, "]: ", iEnemyHit[i], " Lane: ", sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemyHit[i]].iEnemyType))
		ENDIF
	ENDFOR
	
	IF iHostageHit = -1
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iHostageHit: ", iHostageHit)
	ELSE
		CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} 		iHostageHit: ", iHostageHit, " Lane: ", sTLGData.sHostageData[iHostageHit].iHostageLane, " Type: ", TLG_GET_HOSTAGE_TYPE_AS_STRING(sTLGData.sHostageData[iHostageHit].eHostageType))
	ENDIF
	
	CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION]")
	
	#ENDIF
	
	BOOL bProcessedHitCoverThisFrame = FALSE
	BOOL bProcessedHitItemsThisFrame = FALSE
	BOOL bProcessedHitHostagesThisFrame = FALSE
	BOOL bProcessedHitThrownObjectsThisFrame = FALSE
	i = 0
	
	
	// For any given lane, Cover > Items > Enemy > Hostages
	
	IF iThrownHit >= 0
		//-- Hit thrown explosive
		TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_THROWN_WEAPON(iThrownHit, iPlayer, iProjectile, bProcessedHitThrownObjectsThisFrame)
	ENDIF
	
	IF iCurrentNumberOfEnemyHit = 0
		iCurrentNumberOfEnemyHit = 1
	ENDIF
	
	
	FOR i = 0 TO iCurrentNumberOfEnemyHit - 1
		
		
		//-- If we've hit a foreground enemy, go with that
		IF iEnemyHit[i] > -1 AND sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane = ciTLG_LANE_FOREGROUND
			TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
		ELIF iHostageHit > -1 AND sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_FOREGROUND
			//-- Hit a mouse or a scorpion - go with that
			TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
		ELIF iItemHit > -1 AND sTLGData.sItemData[iItemHit].iLane = ciTLG_LANE_FOREGROUND
			//-- Hit a foreground item - generally a pickup hidden in an out house
			TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM(iItemHit,iPlayer, iProjectile, bProcessedHitItemsThisFrame )
		ELIF iEnemyHit[i] > -1 AND sTLGData.sEnemyData[iEnemyHit[i]].bIsPopup
			//-- Enemy peds that 'pop up' behind buildings can always be hit
			TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
		ELSE
			IF iCoverHit > -1
				IF sTLGData.sCoverData[iCoverHit].iCoverLane = ciTLG_LANE_MIDDLE
				AND NOT TLG_CAN_SHOOT_THROUGH_COVER(iCoverHit)
					//-- Hit cover in middle lane
					TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER(iCoverHit, iPlayer, iProjectile, bProcessedHitCoverThisFrame)
				ELIF sTLGData.sCoverData[iCoverHit].iCoverLane = ciTLG_LANE_BACKGROUND
				OR TLG_CAN_SHOOT_THROUGH_COVER(iCoverHit)
					//-- Background cover hit, check if anything in front of it
					IF iItemHit > -1 AND sTLGData.sItemData[iItemHit].iLane = ciTLG_LANE_MIDDLE 
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM(iItemHit,iPlayer, iProjectile, bProcessedHitItemsThisFrame )
					ELIF iEnemyHit[i] > -1 AND ( sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane = ciTLG_LANE_MIDDLE OR TLG_CAN_SHOOT_THROUGH_COVER(iCoverHit))
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
					ELIF iEnemyHit[i] > -1 AND ( sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane = ciTLG_LANE_BACKGROUND)
						//-- Be generous with peds sticking their heads above cover
						fModifier = TLG_GET_COVER_SIZE_MODIFIER_FOR_COLLISION_CHECK(sTLGData.sCoverData[iCoverHit].eType)
						fHeight = sTLGData.sCoverData[iCoverHit].fCoverHeight * fModifier
						IF sTLGData.sCoverData[iCoverHit].vSpritePos.y - (fHeight /2.0) < (sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y + (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y / 2.0))
							CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Setting Enemy ", iEnemyHit[i], " as hit instead of cover ", iCoverHit, " because...") 
							CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] Cover Y offset ", sTLGData.sCoverData[iCoverHit].vSpritePos.y - (fHeight /2.0), " >  Enemy Y offset ", sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y / 2.0))
							TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
						ELSE 
							CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] {dsw} Not overriding cover ", iCoverHit, " with Enemy ", iEnemyHit[i], " Val 1 ",sTLGData.sCoverData[iCoverHit].vSpritePos.y - (fHeight /2.0), " > Val 2 ", sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y / 2.0))
						ENDIF
						
					ELIF iHostageHit > -1 AND sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_MIDDLE
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
					ELSE
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_COVER(iCoverHit, iPlayer, iProjectile, bProcessedHitCoverThisFrame)
					ENDIF
				ENDIF
			ELSE
				IF iItemHit > -1
					IF sTLGData.sItemData[iItemHit].iLane = ciTLG_LANE_MIDDLE 
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM(iItemHit,iPlayer, iProjectile, bProcessedHitItemsThisFrame )
					ELIF sTLGData.sItemData[iItemHit].iLane = ciTLG_LANE_BACKGROUND
						IF iEnemyHit[i] > -1 AND sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane = ciTLG_LANE_MIDDLE
							TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
						ELIF iHostageHit > -1 AND sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_MIDDLE
							TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
						ELSE
							TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ITEM(iItemHit,iPlayer, iProjectile, bProcessedHitItemsThisFrame )
						ENDIF
					ENDIF
				ELIF iEnemyHit[i] > -1
					IF sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane = ciTLG_LANE_MIDDLE
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
					ELIF sTLGData.sEnemyData[iEnemyHit[i]].iEnemyLane = ciTLG_LANE_BACKGROUND
						IF iHostageHit > -1 AND sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_MIDDLE
							TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
						ELSE
							TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_ENEMY(iEnemyHit[i], iPlayer, iProjectile, INIT_VECTOR_2D(sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.x - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.x/2), sTLGData.sEnemyData[iEnemyHit[i]].vSpritePos.y - (sTLGData.sEnemyData[iEnemyHit[i]].vSize.y/2)), vBulletPos)
						ENDIF
					ENDIF
				ELIF iHostageHit > -1
					IF sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_FOREGROUND
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
					ELIF sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_MIDDLE
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
					ELIF sTLGData.sHostageData[iHostageHit].iHostageLane = ciTLG_LANE_BACKGROUND
						TLG_HANDLE_PLAYER_BULLET_COLLISION_WITH_HOSTAGE(iHostageHit,iPlayer, iProjectile, bProcessedHitHostagesThisFrame )
					ENDIF
				ENDIF
						
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF NOT bHitSomething
		IF TLG_GET_PLAYER_SUCESSIVE_HITS() != 1
			TLG_RESET_PLAYER_SUCCESSIVE_HITS()
		ENDIF
	ENDIF
ENDPROC

PROC TWS_ACTIVATE_MENU_OPTION(TLG_MENU_OPTIONS eOption)
	/*
	TLG_LEVEL_DESERT
	TLG_LEVEL_TOWN,
	TLG_LEVEL_FOREST,
	TLG_LEVEL_MINE,
	TLG_LEVEL_GRAVEYARD,
	*/
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] TWS_ACTIVATE_MENU_OPTION ")
	
	SWITCH eOption
	CASE TLG_START
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_START)
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_BULLET_HIT_START)
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_LEVEL_SELECTION_MENU)
	BREAK
	
	CASE TLG_SCORES
		IF NOT TLG_SHOULD_SKIP_LEADERBOARD()
			TLG_INIT_BACKGROUND(TLG_LEVEL_DESERT)
			TLG_INIT_COVER_POINTS(TLG_LEVEL_DESERT)
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_BULLET_HIT_OPTION)
			TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_VIEW_LEADERBOARD)
		//	TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_EDIT_LEADERBOARD)
		ENDIF
	BREAK
	
	CASE TLG_WANTED_1
	
		sTLGData.eCurrentLevel = TLG_LEVEL_DESERT
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SELECT_STAGE)
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_CHOOSE_LVL1_GOSPEL_GANGSTERS)
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INIT_LEVEL)
	
	BREAK
	CASE TLG_WANTED_2
	
		sTLGData.eCurrentLevel = TLG_LEVEL_TOWN	
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SELECT_STAGE)
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_CHOOSE_LVL2_BUCKLE_BUNNIES)
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INIT_LEVEL)
	
	
	BREAK
	CASE TLG_WANTED_3

		sTLGData.eCurrentLevel = TLG_LEVEL_FOREST
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SELECT_STAGE)
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_CHOOSE_LVL3_HILLBILLY_HOODLUMS)
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INIT_LEVEL)

	
	BREAK
	CASE TLG_WANTED_4
	
		sTLGData.eCurrentLevel = TLG_LEVEL_MINE
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SELECT_STAGE)
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_CHOOSE_LVL4_RUCKUS_RANGERS)
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INIT_LEVEL)
	
	
	BREAK
	CASE TLG_WANTED_5
	
		sTLGData.eCurrentLevel = TLG_LEVEL_GRAVEYARD
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SELECT_STAGE)
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_CHOOSE_LVL5_BARREL_FEVER_BOYS)
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INIT_LEVEL)
	
	
	BREAK
  	ENDSWITCH
ENDPROC

PROC HANDLE_PLAYER_BULLET_IN_MENU(INT iPlayer, INT iProjectile)
	
	IF NOT sTLGData.sPlayerData[iPlayer].bHasJustShot
		EXIT
	ENDIF
	
	INT i
	FLOAT x1, x2, y1, y2 // Top-left & Bottom-right corners of collision
	

	BOOL bHitSomething
	FOR i = 0 TO ciTLG_MAX_MENU_OPTIONS-1

		IF sTLGData.sMenuOptionsData[i].eClientState = sTLGData.eClientState
	
			sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].bHit = TRUE
			
			x1 = sTLGData.sMenuOptionsData[i].vSpritePos.x - (sTLGData.sMenuOptionsData[i].vSize.x/2)
			y1 = sTLGData.sMenuOptionsData[i].vSpritePos.y - (sTLGData.sMenuOptionsData[i].vSize.y/2)
			
			x2 = sTLGData.sMenuOptionsData[i].vSpritePos.x + (sTLGData.sMenuOptionsData[i].vSize.x/2)
			y2 = sTLGData.sMenuOptionsData[i].vSpritePos.y + (sTLGData.sMenuOptionsData[i].vSize.y/2)
			
//			IF sTLGData.sMenuOptionsData[i].eMenuOption = TLG_START
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] ")
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] Sprite Pos X: ", sTLGData.sMenuOptionsData[i].vSpritePos.x, " Pos Y: ",sTLGData.sMenuOptionsData[i].vSpritePos.y)
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] Sprite Size X:  ", sTLGData.sMenuOptionsData[i].vSize.x, " Size Y: ", sTLGData.sMenuOptionsData[i].vSize.y)
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] Sprite (x1, y1) : ", x1, " , ", y1, " (x2, y2): ", x2, ", ", y2)
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] Bullet x: ", sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X, " y: ", sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.y)
//				
//				
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [HANDLE_PLAYER_BULLET_IN_MENU] ")
//			ENDIF
			
			IF (sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X > x1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.X < x2
			AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y > y1 AND sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].vSpritePos.Y < y2)
				bHitSomething = TRUE
				sTLGData.sPlayerData[iPlayer].sBulletData[iProjectile].iTimeOfHit = GET_GAME_TIMER()
				IF TLG_IS_MENU_OPTION_UNLOCKED(sTLGData.sMenuOptionsData[i].eMenuOption)
					TWS_ACTIVATE_MENU_OPTION(sTLGData.sMenuOptionsData[i].eMenuOption)
				ELSE
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_STAGE_LOCKED)
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF NOT bHitSomething
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_BULLET_HIT_MISS)
	ENDIF
ENDPROC

PROC TLG_CHECK_PLAYER_HOVERING_ENEMY()
	
	FLOAT distanceX, distanceY, fDistance
	INT iPlayer, i
	
	FOR iPlayer = 0 TO ciTLG_MAX_PLAYERS - 1
	
		sTLGData.sPlayerData[iPlayer].bHoveringEnemy = FALSE
		
		FOR i = 0 TO ciTLG_MAX_ENEMIES  -1

			IF (sTLGData.sEnemyData[i].eState = TLG_ACTIVE OR sTLGData.sEnemyData[i].eState = TLG_SHOOTING) AND sTLGData.sEnemyData[i].iHealth > 0
				distanceX = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.x - sTLGData.sEnemyData[i].vSpritePos.x
				distanceY = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.y - sTLGData.sEnemyData[i].vSpritePos.y
				fDistance = SQRT(distanceX * distanceX + distanceY * distanceY)
			
				IF fDistance < 150
					sTLGData.sPlayerData[iPlayer].bHoveringEnemy = TRUE
					EXIT
				ENDIF
			ENDIF
		ENDFOR
		
		FOR i = 0 TO ciTLG_MAX_ITEMS  -1
	
		IF NOT sTLGData.sItemData[i].bShot
			IF sTLGData.sItemData[i].bRendering
				distanceX = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.x - sTLGData.sItemData[i].vSpritePos.x
				distanceY = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.y - sTLGData.sItemData[i].vSpritePos.y
				fDistance = SQRT(distanceX * distanceX + distanceY * distanceY)
			
				IF fDistance < 150
					sTLGData.sPlayerData[iPlayer].bHoveringEnemy = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	ENDFOR
ENDPROC

PROC TLG_CHECK_PLAYER_HOVERING_MENU_OPTION()
	
	FLOAT x1, x2, y1, y2 // Top-left & Bottom-right corners of collision
		
	INT iPlayer, i
	FOR iPlayer = 0 TO ciTLG_MAX_PLAYERS - 1

		FOR i = 0 TO ciTLG_MAX_MENU_OPTIONS-1

			IF sTLGData.sMenuOptionsData[i].eClientState = sTLGData.eClientState
				
				x1 = sTLGData.sMenuOptionsData[i].vSpritePos.x - (sTLGData.sMenuOptionsData[i].vSize.x/2)
				y1 = sTLGData.sMenuOptionsData[i].vSpritePos.y - (sTLGData.sMenuOptionsData[i].vSize.y/2)
				
				x2 = sTLGData.sMenuOptionsData[i].vSpritePos.x + (sTLGData.sMenuOptionsData[i].vSize.x/2)
				y2 = sTLGData.sMenuOptionsData[i].vSpritePos.y + (sTLGData.sMenuOptionsData[i].vSize.y/2)
				
				IF (sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.x > x1 AND sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.x < x2
				AND sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.y > y1 AND sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos.Y < y2)

					sTLGData.sPlayerData[iPlayer].bHoveringEnemy = TRUE
					
				ENDIF
			ENDIF
			
		ENDFOR
	ENDFOR
	
ENDPROC


PROC TLG_HANDLE_RETICULE_MOVEMENT()
	IF TLG_IS_PLAYER_PLAYING()
		IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.0
			EXIT
		ENDIF
	ENDIF
	FLOAT fAxisThresholdMod = cfIAP_CONTROLLER_AXIS_THRESHOLD_MOD
	 
	FLOAT fLeftLimit = 305.0.0
	FLOAT fRightLimit = cfRIGHT_SCREEN_LIMIT
	FLOAT fDownLimit = 80.0
	FLOAT fUpLimit = 995.0
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		// Controller
		FLOAT fRightStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
		FLOAT fRightStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		FLOAT fLeftStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		FLOAT fLeftStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		
		fAxisThresholdMod = 0.5
		
		BOOL bMovingLeft 	= (fRightStickX < -cfSTICK_THRESHOLD * fAxisThresholdMod) OR (fLeftStickX < -cfSTICK_THRESHOLD * fAxisThresholdMod)
		BOOL bMovingRight 	= (fRightStickX > cfSTICK_THRESHOLD * fAxisThresholdMod) OR (fLeftStickX > cfSTICK_THRESHOLD * fAxisThresholdMod)
		BOOL bMovingDown	= (fRightStickY < -cfSTICK_THRESHOLD * fAxisThresholdMod) OR fLeftStickY < -cfSTICK_THRESHOLD * fAxisThresholdMod
		BOOL bMovingUp 		= (fRightStickY > cfSTICK_THRESHOLD * fAxisThresholdMod) OR (fLeftStickY > cfSTICK_THRESHOLD * fAxisThresholdMod)
		
		
		//Reticule 1
		IF bMovingLeft AND sTLGData.sPlayerData[0].vSpriteMarkerPos.x >= fLeftLimit // 0.0
			sTLGData.sPlayerData[0].vSpriteMarkerPos.x -= (30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15)) * TLG_GET_DEFAULT_TIMESTEP()
			IF sTLGData.sPlayerData[0].vSpriteMarkerPos.x < fLeftLimit
				sTLGData.sPlayerData[0].vSpriteMarkerPos.x = fLeftLimit
			ENDIF
		ELIF bMovingRight AND sTLGData.sPlayerData[0].vSpriteMarkerPos.x <= fRightLimit
			sTLGData.sPlayerData[0].vSpriteMarkerPos.x += (30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15)) * TLG_GET_DEFAULT_TIMESTEP()
			IF sTLGData.sPlayerData[0].vSpriteMarkerPos.x > fRightLimit
				sTLGData.sPlayerData[0].vSpriteMarkerPos.x = fRightLimit
			ENDIF
		ENDIF
		
		IF bMovingDown AND sTLGData.sPlayerData[0].vSpriteMarkerPos.y >= fDownLimit //20.0
			sTLGData.sPlayerData[0].vSpriteMarkerPos.y -= (30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15))  * TLG_GET_DEFAULT_TIMESTEP()
			IF sTLGData.sPlayerData[0].vSpriteMarkerPos.y < fDownLimit
				sTLGData.sPlayerData[0].vSpriteMarkerPos.y = fDownLimit
			ENDIF
		ELIF bMovingUp AND sTLGData.sPlayerData[0].vSpriteMarkerPos.y <= fUpLimit
			sTLGData.sPlayerData[0].vSpriteMarkerPos.y += (30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15))  * TLG_GET_DEFAULT_TIMESTEP()
			IF sTLGData.sPlayerData[0].vSpriteMarkerPos.y > fUpLimit
				sTLGData.sPlayerData[0].vSpriteMarkerPos.y = fUpLimit
			ENDIF
		ENDIF
	ELSE
		
		//-- Mouse
		FLOAT fMouseX= GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
		FLOAT fMouseY= GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		
		VECTOR_2D vTopLeft = INIT_VECTOR_2D(fLeftLimit, fUpLimit)
		VECTOR_2D vBottomRight = INIT_VECTOR_2D(fRightLimit, fDownLimit)
		
		FLOAT fXAxisPlay = vBottomRight.x - vTopLeft.x
		FLOAT fYAxisPlay = vTopLeft.Y - vBottomRight.y
		
		sTLGData.sPlayerData[0].vSpriteMarkerPos = INIT_VECTOR_2D(vTopLeft.x + (fMouseX * fXAxisPlay), vBottomRight.y + (fMouseY * fYAxisPlay))
	ENDIF
	
ENDPROC

PROC TLG_HANDLE_PLAYER_MOVEMENT()

	
	
	TLG_CHECK_PLAYER_HOVERING_ENEMY()
	
	TLG_HANDLE_RETICULE_MOVEMENT()
	
ENDPROC

PROC TLG_HANDLE_PLAYER_ABILITIES()
	IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_INVULNERABILE)
		IF GET_GAME_TIMER() - sTLGData.sPlayerData[0].iInvulnerableTimer > ciTLG_INVULNERABILTY_TIME
			TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_INVULNERABILE)
			TLG_STOP_SOUND(TLG_AUDIO_EFFECT_POWERUP)
			TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PICKUP_RUN_OUT)
		ENDIF
	ENDIF
	
	IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_HIDE_RETICULE)
		IF GET_GAME_TIMER() - sTLGData.sPlayerData[0].iHideReticuleTimer > ciTLG_HIDE_RETICULE_TIME
			TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_HIDE_RETICULE)
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PICKUP_RUN_OUT)
			TLG_STOP_SOUND(TLG_AUDIO_EFFECT_POWERDOWN)
			TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_POWERUP_ACTIVE)
		ENDIF
	ENDIF
	
	
	
ENDPROC

FUNC BOOL TLG_CAN_ENEMY_SHOOT_GUN(INT iEnemy)
	IF sTLGData.sEnemyData[iEnemy].vSpritePos.x >= cfRIGHT_SCREEN_LIMIT
		RETURN FALSE
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].vSpritePos.x <= 300.0
		RETURN FALSE
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_THROWING_ENEMY_1
		RETURN FALSE
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_THROWING_ENEMY_1
		RETURN FALSE
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_FOREGROUND_ENEMY_BEAR
		RETURN FALSE
	ENDIF
	// TLG_ADD_ENEMY_TO_LIST(1398.0, 446.875, 357.875,
	
	FLOAT fPosY
	FLOAT fLocToCheck
	IF sTLGData.sEnemyData[iEnemy].bIsPopup
		fPosY 		= sTLGData.sEnemyData[iEnemy].vSpritePos.y
		fLocToCheck	= sTLGData.sEnemyData[iEnemy].fStoppingX + 10.0
		
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_CAN_ENEMY_SHOOT_GUN] [TLG_ACTIVE] [dsw] Enemy ", iEnemy, " Is popup Y Pos: ",fPosY, "> ? ", fLocToCheck)
		IF fPosY > fLocToCheck
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_CAN_ENEMY_SHOOT_GUN] [TLG_ACTIVE] [dsw] Enemy ", iEnemy, " FALSE")
			RETURN FALSE
		ENDIF
		
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_CAN_ENEMY_SHOOT_GUN] [TLG_ACTIVE] [dsw] Enemy ", iEnemy, " TRUE")
	ENDIF
	
	
	RETURN TRUE
ENDFUNC

PROC TLG_HANDLE_POPUP_ENEMY(INT iEnemy)
	IF NOT sTLGData.sEnemyData[iEnemy].bIsPopup
		EXIT
	ENDIF
	

	IF sTLGData.sEnemyData[iEnemy].iPopUpTime = 0
		IF sTLGData.sEnemyData[iEnemy].vSpritePos.y > sTLGData.sEnemyData[iEnemy].fStoppingX
			sTLGData.sEnemyData[iEnemy].vSpritePos.y -= 4.0
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [TLG_HANDLE_POPUP_ENEMY] Enemy ", iEnemy, " Going up Y pos ", sTLGData.sEnemyData[iEnemy].vSpritePos.y, " Stop ", sTLGData.sEnemyData[iEnemy].fStoppingX)
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [TLG_HANDLE_POPUP_ENEMY] Enemy ", iEnemy, " Finished up, grabbing time")
			sTLGData.sEnemyData[iEnemy].iPopUpTime = GET_GAME_TIMER()
		ENDIF
	ELSE
		IF GET_GAME_TIMER() - sTLGData.sEnemyData[iEnemy].iPopUpTime > 5000
			IF sTLGData.sEnemyData[iEnemy].vSpritePos.y < sTLGData.sEnemyData[iEnemy].vStartPos.y
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [TLG_HANDLE_POPUP_ENEMY] Enemy ", iEnemy, " Going dowm Y pos ", sTLGData.sEnemyData[iEnemy].vSpritePos.y, " Stop ",  sTLGData.sEnemyData[iEnemy].vStartPos.y)
				sTLGData.sEnemyData[iEnemy].vSpritePos.y += 4.0
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [TLG_HANDLE_POPUP_ENEMY] Enemy ", iEnemy, " FINISHED Going dowm Y pos ", sTLGData.sEnemyData[iEnemy].vSpritePos.y, " Stop ",  sTLGData.sEnemyData[iEnemy].vStartPos.y)
				IF GET_GAME_TIMER() - sTLGData.sEnemyData[iEnemy].iPopUpTime > 8000
					sTLGData.sEnemyData[iEnemy].iPopUpTime = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TLG_INIT_THROWN_ENEMY_PROJECTILE(TLG_ENEMY_PROJECTILE_TYPES eType, VECTOR_2D vInitialPos)
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_INIT_THROWN_ENEMY_PROJECTILE] Called, type: ", TLG_GET_ENEMY_THROWN_PROJECTILE_TYPE_AS_STRING(eType), " Initial pos: ",vInitialPos.x, ", ", vInitialPos.y)
	#ENDIF
	
	IF sTLGData.iNumberOfThrownEnemyProjectiles > ciTLG_MAX_ENEMY_PROJECTILES - 1
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_INIT_THROWN_ENEMY_PROJECTILE] Too many projectiles - exiting ")
		EXIT
	ENDIF
	
	sTLGData.sEnemyProjectiles[sTLGData.iNumberOfThrownEnemyProjectiles].eState = TLG_ENEMY_PROJECTILE_STATE_AIRBORNE
	sTLGData.sEnemyProjectiles[sTLGData.iNumberOfThrownEnemyProjectiles].eType = eType
	sTLGData.sEnemyProjectiles[sTLGData.iNumberOfThrownEnemyProjectiles].vSpritePos = vInitialPos
ENDPROC

PROC TLG_HANDLE_THROWN_ENEMY_PROJECTILES()
	INT i
	
	
	REPEAT ciTLG_MAX_ENEMY_PROJECTILES i
		SWITCH sTLGData.sEnemyProjectiles[i].eState
			CASE TLG_ENEMY_PROJECTILE_STATE_AIRBORNE
				sTLGData.sEnemyProjectiles[i].iSpriteAnimFrame += sTLGData.sEnemyProjectiles[i].iSlowUpdateFrames
				IF sTLGData.sEnemyProjectiles[i].iSpriteAnimFrame >= ciTLG_MAX_ENEMY_THROWN_DYNAMITE_ANIM_FRAMES 
					sTLGData.sEnemyProjectiles[i].eState = 	TLG_ENEMY_PROJECTILE_STATE_EXPLODE
					TLG_DAMAGE_PLAYER(TLG_DAMAGE_PLAYER_TYPE_DYNAMITE)
					TWS_CREATE_FX(sTLGData.sEnemyProjectiles[i].vSpritePos, TLG_FX_ENEMY_TNT_EXP, 1)
					SET_CONTROL_SHAKE(FRONTEND_CONTROL, 100, 20)
					TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_EXP_DYNAMITE, sTLGData.sEnemyProjectiles[i].vSpritePos)
				ENDIF
				
			BREAK
			
			CASE TLG_ENEMY_PROJECTILE_STATE_EXPLODE
				TLG_RESET_ENEMY_THROWN_PROJECTILE(i)
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

FUNC BOOL TLG_CAN_ENEMY_STOP_TO_CHECK_FOR_COVER(INT iEnemy)
	IF sTLGData.sEnemyData[iEnemy].bIsPopup
		RETURN FALSE
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].bThrownProjectile
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN(INT iEnemy)
	VECTOR_2D vPos = sTLGData.sEnemyData[iEnemy].vSpritePos
	VECTOR_2D vSize = sTLGData.sEnemyData[iEnemy].vSize
	FLOAT fScale = TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(sTLGData.sEnemyData[iEnemy].iEnemyType, sTLGData.sEnemyData[iEnemy].iEnemyLane)
	BOOL bFromLeft = sTLGData.sEnemyData[iEnemy].bReverse
	BOOL bThrownProjectile = sTLGData.sEnemyData[iEnemy].bThrownProjectile
	
	TLG_ENEMY_TYPES eType = sTLGData.sEnemyData[iEnemy].iEnemyType
	
	
	IF eType = TLG_COFFIN_ENEMY
		IF TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_STOPPED)
			IF NOT sTLGData.sEnemyData[iEnemy].bAppearedOnscreen
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] {dsw} True Enemy ", iEnemy, " Is coffin, level stopped, not on screen. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType))
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF sTLGData.sEnemyData[iEnemy].eState = TLG_ACTIVE
		IF NOT sTLGData.sEnemyData[iEnemy].bAppearedOnscreen
			RETURN FALSE
		ELIF sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_OUTHOUSE_ENEMY
		OR sTLGData.sEnemyData[iEnemy].iEnemyType = TLG_COFFIN_ENEMY
			IF NOT sTLGData.sEnemyData[iEnemy].bShotAtLeastOnce
				IF NOT TLG_IS_LEVEL_STOPPED()
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF vPos.x <( cfLEFT_SCREEN_LIMIT - ( vSize.x * fScale) )
		//-- Enemy is off the left hand side of the screen
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] {dsw} True Enemy ", iEnemy, " Off LHS Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType), " Pos x ", vPos.x)
		#ENDIF
		RETURN TRUE
	ENDIF
	
	
	IF bFromLeft AND bThrownProjectile AND vPos.x > ( cfRIGHT_SCREEN_LIMIT +  ( vSize.x * fScale) )
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] {dsw} True Enemy ", iEnemy, " Thrown projectile, off rhs Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType), " Pos x ", vPos.x)
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF bFromLeft AND sTLGData.sEnemyData[iEnemy].bAppearedOnscreen AND vPos.x > cfBASE_SCREEN_WIDTH * 2.0
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] {dsw} True Enemy ", iEnemy, " From lhs, far off rhs Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType), " Pos x ", vPos.x)
		#ENDIF
		RETURN TRUE
	ENDIF
//	#IF IS_DEBUG_BUILD
//		IF eType = TLG_COFFIN_ENEMY OR eType = TLG_OUTHOUSE_ENEMY
//			CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN]")
//			CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] Enemy ", iEnemy, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(eType), " State: ", TLG_GET_ENEMY_STATE_AS_STRING(sTLGData.sEnemyData[iEnemy].eState) )
//			CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] Pos: ", vPos.x, ", ", vPos.y, " Ever on screen? ", sTLGData.sEnemyData[iEnemy].bAppearedOnscreen, " Shot? ", sTLGData.sEnemyData[iEnemy].bShotAtLeastOnce)
//			CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] Level stopped? ", TLG_IS_LEVEL_STOPPED())
//			CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN]")
//			
//		ENDIF
//	#ENDIF
	
	IF TLG_IS_LEVEL_STOPPED()
		IF sTLGData.sEnemyData[iEnemy].eState = TLG_SHOOTING
		OR (( eType = TLG_COFFIN_ENEMY OR eType = TLG_OUTHOUSE_ENEMY)
			AND sTLGData.sEnemyData[iEnemy].eState = TLG_ACTIVE)
		
			IF vPos.x > ( cfRIGHT_SCREEN_LIMIT )
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] {dsw} True Enemy ", iEnemy, " Level stopped, off rhs Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType), " Pos x ", vPos.x)
				#ENDIF
				RETURN TRUE
			ELIF vPos.x <= ( 300.0 )
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN] {dsw} True Enemy ", iEnemy, " Level stopped, off lhs Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[iEnemy].iEnemyType), " Pos x ", vPos.x)
				#ENDIF
				RETURN TRUE
			
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TLG_SHOULD_FORCE_DOORS_OPEN(INT iEnemyCover)
	IF sTLGData.sEnemyData[iEnemyCover].iRenderAfterWarpedCount = 0
		IF sTLGData.sEnemyData[iEnemyCover].iWarpedForMovingLevelCount > 0
			RETURN TRUE
		ENDIF
	ELSE
		IF sTLGData.sEnemyData[iEnemyCover].iWarpedForMovingLevelCount > sTLGData.sEnemyData[iEnemyCover].iRenderAfterWarpedCount
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




PROC TLG_HANDLE_ENEMIES()
	
	INT iCurrentGameTime = GET_GAME_TIMER()
	INT iBlockingCover
	INT i//, j
	VECTOR_2D vThrow
	FLOAT fXpos
	
//	FLOAT fCoverWidth
//	FLOAT fScale
	FOR i = 0 TO ciTLG_MAX_ENEMIES - 1
		iBlockingCover = -1
		
		#IF IS_DEBUG_BUILD
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw]")
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw] Processing Enemy: ",i)
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw] 				State: ", TLG_GET_ENEMY_STATE_AS_STRING(sTLGData.sEnemyData[i].eState), " Lane: ",sTLGData.sEnemyData[i].iEnemyLane, " Type:", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw]				Active in stage: ", sTLGData.sEnemyData[i].iLevelStage, " Current stage:", TLG_GET_CURRENT_LEVEL_STAGE())
		#ENDIF
		
		IF NOT sTLGData.sEnemyData[i].bAppearedOnscreen
			IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
			OR sTLGData.sEnemyData[i].eState = TLG_SHOOTING
			OR sTLGData.sEnemyData[i].eState = TLG_DYING
				fXpos = sTLGData.sEnemyData[i].vSpritePos.x
				IF fXPos > cfLEFT_SCREEN_LIMIT AND fXPos < cfRIGHT_SCREEN_LIMIT	
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw] Enemy: ",i, " Appeared on screen. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " State: ", TLG_GET_ENEMY_STATE_AS_STRING(sTLGData.sEnemyData[i].eState), " Lane: ", sTLGData.sEnemyData[i].iEnemyLane)
					#ENDIF
					
					sTLGData.sEnemyData[i].bAppearedOnscreen = TRUE
					
					IF TLG_IS_BEAR(i)
						TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BEAR_APPEAR, sTLGData.sEnemyData[i].vSpritePos)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH sTLGData.sEnemyData[i].eState
		
		CASE TLG_ACTIVE
		
			#IF IS_DEBUG_BUILD
		//		CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_IS_COVER_SUITABLE_FOR_ENEMY_TO_USE] {dsw} Start of Active Enemy ", i, " Staggered count: ", sTLGData.iStaggeredOnScreenViableCoverCount)
			#ENDIF
			
			IF sTLGData.sEnemyData[i].iEnemyType = TLG_CART_1
				sTLGData.sEnemyData[i].iSpriteAnimFrame = 0
			ELIF sTLGData.sEnemyData[i].iEnemyType = TLG_OUTHOUSE_ENEMY  
			OR sTLGData.sEnemyData[i].iEnemyType = TLG_COFFIN_ENEMY
				IF sTLGData.sEnemyData[i].bShotAtLeastOnce
					sTLGData.sEnemyData[i].iSpriteAnimFrame += sTLGData.sEnemyData[i].iSlowUpdateFrames
					IF sTLGData.sEnemyData[i].iSpriteAnimFrame > ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES - 2
						sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES - 2
					ENDIF
				ENDIF
			ELSE
				sTLGData.sEnemyData[i].iSpriteAnimFrame += sTLGData.sEnemyData[i].iSlowUpdateFrames
				IF sTLGData.sEnemyData[i].iSpriteAnimFrame >= ciTLG_MAX_ANIM_FRAMES
					sTLGData.sEnemyData[i].iSpriteAnimFrame = 0
				ENDIF
			ENDIF
			
//			IF sTLGData.sEnemyData[i].iEnemyType != TLG_CART_1 AND sTLGData.sEnemyData[i].iSpriteAnimFrame >= ciTLG_MAX_ANIM_FRAMES
//				sTLGData.sEnemyData[i].iSpriteAnimFrame = 0
//			ELIF sTLGData.sEnemyData[i].iEnemyType = TLG_CART_1
//				sTLGData.sEnemyData[i].iSpriteAnimFrame = 0
//			ENDIF

			IF sTLGData.sEnemyData[i].iHealth <= 0
				sTLGData.sEnemyData[i].eState = TLG_DYING
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, "  set as Dying as iHealth <= 0. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
				#ENDIF
			ENDIF
			
			IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
			AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
				IF NOT sTLGData.sEnemyData[i].bIsPopup
				//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [dsw] [TLG_IS_COVER_SUITABLE_FOR_ENEMY_TO_USE] Enemy ",i , " Comparing sprite x ", sTLGData.sEnemyData[i].vSpritePos.x, " wih stopping x ", sTLGData.sEnemyData[i].fStoppingX) 
					IF sTLGData.sEnemyData[i].vSpritePos.x >= sTLGData.sEnemyData[i].fStoppingX -10
					AND sTLGData.sEnemyData[i].vSpritePos.x <= sTLGData.sEnemyData[i].fStoppingX +10
						//-- If the enemy is about to stop and shoot then check the stop location isn't behind a big bit of cover
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [dsw] [TLG_IS_COVER_SUITABLE_FOR_ENEMY_TO_USE] Enemy ",i , " Stopping position ",sTLGData.sEnemyData[i].fStoppingX, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
						#ENDIF
						
						IF TLG_IS_COORD_BLOCKED_BY_COVER(sTLGData.sEnemyData[i].iEnemyLane, sTLGData.sEnemyData[i].fStoppingX, iBlockingCover)
						AND NOT sTLGData.sEnemyData[i].bThrownProjectile
							
							SET_ENEMY_NEW_STOPPING_POINT_AFTER_BLOCKED_COVER(i, iBlockingCover)

							
						ELSE
							CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [dsw] Not blocked. Enemy ", i, " Stopping at x pos ", sTLGData.sEnemyData[i].vSpritePos.x)
							sTLGData.sEnemyData[i].eState = TLG_SHOOTING
							sTLGData.sEnemyData[i].iTimeOfLastShot = GET_GAME_TIMER()
						ENDIF
					ELSE
						//-- If the ped is stiil on their way to their stopping loc, stop if they happen to pass some suitable cover
						IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
						AND NOT sTLGData.sEnemyData[i].bThrownProjectile
							IF TLG_IS_COVER_SUITABLE_FOR_ENEMY_TO_USE(sTLGData.iViableOnScreenEnemyCoverPoints[sTLGData.iStaggeredOnScreenViableCoverCount], i)
								sTLGData.sEnemyData[i].eState = TLG_SHOOTING
								sTLGData.sEnemyData[i].iTimeOfLastShot = GET_GAME_TIMER()
							ENDIF
						ENDIF

					ENDIF

				ENDIF
						
				SWITCH sTLGData.sEnemyData[i].iEnemyLane
					CASE 2
						IF sTLGData.sEnemyData[i].bReverse
							sTLGData.sEnemyData[i].vSpritePos.x += TLG_GET_ENEMY_FOREGROUND_UPDATE(TRUE)
						ELSE
							sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_ENEMY_FOREGROUND_UPDATE(FALSE)
						ENDIF
					BREAK
					CASE 1
						IF sTLGData.sEnemyData[i].bReverse
							sTLGData.sEnemyData[i].vSpritePos.x += TLG_GET_ENEMY_MIDDLE_UPDATE(TRUE)
						ELSE
							sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_ENEMY_MIDDLE_UPDATE(FALSE)
						ENDIF
					BREAK
					CASE 0
						IF sTLGData.sEnemyData[i].bReverse
							sTLGData.sEnemyData[i].vSpritePos.x += TLG_GET_ENEMY_BACKGROUND_UPDATE(TRUE)
						ELSE
							sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_ENEMY_BACKGROUND_UPDATE(FALSE)
						ENDIF
					BREAK
					
					
				ENDSWITCH
				
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw] Enemy: ",i, " Lane: ",sTLGData.sEnemyData[i].iEnemyLane, " Type:", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " New Pos: ", sTLGData.sEnemyData[i].vSpritePos.x)
//				#ENDIF
			ELSE
//				#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [OUTHOUSE_WARP] [dsw] Enemy: ",i, " Is outhouse Lane: ",sTLGData.sEnemyData[i].iEnemyLane, " Type:", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " State: ", TLG_GET_ENEMY_STATE_AS_STRING(sTLGData.sEnemyData[i].eState))
//				#ENDIF
		
				//-- Outhouse enemies - wait until door is off before shooting
				IF sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES - 2 
					sTLGData.sEnemyData[i].eState = TLG_SHOOTING
					sTLGData.sEnemyData[i].iTimeOfLastShot = GET_GAME_TIMER()
				ENDIF
				
				BOOL bForceOpenDoors
				IF IS_MOVING_LEVEL() AND sTLGData.sEnemyData[i].iEnemyLane != 2
					sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_OUTHOUSE_UPDATE()
					
					FLOAT fEnemyScaleMulti
					
					//-- If the outhouse doors haven't been shot off, and it's gonbe off the screen LHS, warp it back so 
					//-- It'll appear
					fEnemyScaleMulti = TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(sTLGData.sEnemyData[i].iEnemyType, sTLGData.sEnemyData[i].iEnemyLane)
					
					IF sTLGData.sEnemyData[i].vSpritePos.x <= 0
						IF sTLGData.sEnemyData[i].iEnemyType = TLG_OUTHOUSE_ENEMY
							IF sTLGData.sEnemyData[i].vSpritePos.x < 200 	
								sTLGData.sEnemyData[i].vSpritePos.x = cfBASE_SCREEN_WIDTH + (( sTLGData.sEnemyData[i].vSize.x * fEnemyScaleMulti)/2.0)
								sTLGData.sEnemyData[i].iWarpedForMovingLevelCount++
							ENDIF
						ELIF sTLGData.sEnemyData[i].iEnemyType = TLG_COFFIN_ENEMY	
							sTLGData.sEnemyData[i].vSpritePos.x = TLG_GET_OFFSET_FOR_ELEMENT_WARP_FOR_CURRENT_LEVEL()
							CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [OUTHOUSE_WARP] [dsw] Enemy: ",i, " Warped")
							sTLGData.sEnemyData[i].iWarpedForMovingLevelCount++
						ENDIF
					ELSE
						
						IF NOT sTLGData.sEnemyData[i].bShotAtLeastOnce
							IF TLG_SHOULD_FORCE_DOORS_OPEN(i)
							
								//-- Have the outhouse enemies shoot their doors off if the player hasn't done it
								//-- Will happen if they've gone off the screen LHS at least once
								//-- Wait until they're halfway between the centre of the screen and the rhs limit
								FLOAT fPosToCheck
								fPosToCheck = (cfBASE_SCREEN_WIDTH / 2.0) +  ((cfBASE_SCREEN_WIDTH - cfRIGHT_SCREEN_LIMIT)) 
								IF sTLGData.sEnemyData[i].vSpritePos.x - ((sTLGData.sEnemyData[i].vSize.x * TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(TLG_OUTHOUSE_ENEMY, sTLGData.sEnemyData[i].iEnemyLane)/2.0)) < fPosToCheck
									bForceOpenDoors = TRUE
									CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_HANDLE_ENEMIES] Outhouse enemy ", i, " Forcing doors open. Sprite x: ",  sTLGData.sEnemyData[i].vSpritePos.x, " < ", fPosToCheck)
								ENDIF
							ENDIF

						ENDIF
							
					ENDIF
				ELSE
					//-- If it's a stationary stage, wait until the outhouse is on screen and start a timer to determine
					//-- If the doors should be forced open
					IF NOT sTLGData.sEnemyData[i].bShotAtLeastOnce
						IF TLG_IS_CURRENT_LEVEL_STAGE_STATIONARY()
							IF sTLGData.sEnemyData[i].vSpritePos.x < cfRIGHT_SCREEN_LIMIT
								IF sTLGData.sEnemyData[i].iOnScreenTime = 0
									sTLGData.sEnemyData[i].iOnScreenTime = GET_GAME_TIMER()
								ELSE
									IF GET_GAME_TIMER() -  sTLGData.sEnemyData[i].iOnScreenTime > 15000
										bForceOpenDoors = TRUE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_HANDLE_ENEMIES] Outhouse enemy ", i, " Forcing doors open as stationary & time expired")
									ELIF TLG_GET_FINAL_STAGE_IN_CURRENT_LEVEL() = TLG_GET_CURRENT_LEVEL_STAGE()
										IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 1
											bForceOpenDoors = TRUE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_HANDLE_ENEMIES] Outhouse enemy ", i, " Forcing doors open as stationary, final stage and everyone else dead")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bForceOpenDoors
					sTLGData.sEnemyData[i].bShotAtLeastOnce = TRUE
					TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_RIFLE_FIRE, sTLGData.sEnemyData[i].vSpritePos)
				ENDIF
			ENDIF
			
			IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
				IF TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN(i)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " Active but went outside the limits and is being set as inactive. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
					#ENDIF
					sTLGData.sEnemyData[i].eState = TLG_INACTIVE
					
					IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
					AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
						sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]--
					ENDIF
					
					IF sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] < 0
						sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] = 0
						CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} iCurrentActiveEnemies in lane ", sTLGData.sEnemyData[i].iEnemyLane, " < 0 (1)")
					ENDIF
						
				ENDIF
			ENDIF
		BREAK
		
		CASE TLG_SHOOTING
				
			IF IS_MOVING_LEVEL() AND sTLGData.sEnemyData[i].iEnemyLane != 2
				sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_ENEMY_SHOOTING_UPDATE()
			ENDIF
			
			IF TLG_CAN_ENEMY_SHOOT_GUN(i)
				IF iCurrentGameTime - sTLGData.sEnemyData[i].iTimeOfLastShot > ciTLG_ENEMY_TIME_TO_SHOOT - (ENUM_TO_INT(sTLGData.eCurrentLevel) * 200)
					sTLGData.sEnemyData[i].iTimeOfLastShot = GET_GAME_TIMER()
					
					IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
					AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
						IF sTLGData.sEnemyData[i].iEnemyLane = 2
							TWS_CREATE_FX(INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSpritePos.x + cfTLG_FG_ENEMY_1_X_OFFSET, sTLGData.sEnemyData[i].vSpritePos.Y + cfTLG_FG_ENEMY_1_Y_OFFSET), TLG_FX_ENEMY_SHOT, sTLGData.sEnemyData[i].iEnemyLane)
						ELSE
							IF sTLGData.sEnemyData[i].iEnemyType != TLG_THROWING_ENEMY_1
								TWS_CREATE_FX(INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSpritePos.x + cfTLG_BG_ENEMY_1_X_OFFSET, sTLGData.sEnemyData[i].vSpritePos.Y + cfTLG_BG_ENEMY_1_Y_OFFSET), TLG_FX_ENEMY_SHOT, sTLGData.sEnemyData[i].iEnemyLane)
							ENDIF
						ENDIF
					ELSE
						TWS_CREATE_FX(INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSpritePos.x, sTLGData.sEnemyData[i].vSpritePos.Y), TLG_FX_ENEMY_SHOT, sTLGData.sEnemyData[i].iEnemyLane)
					ENDIF
					
					IF GET_PLAYER_TOTAL_AMMO_FOR_ALL_WEAPON_TYPES() <= 0
						IF sTLGData.sPlayerData[0].iNoAmmoTimer = 0
							sTLGData.sPlayerData[0].iNoAmmoTimer = GET_GAME_TIMER()
							CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} Start iNoAmmoTimer")
						ELSE
							IF GET_GAME_TIMER() - sTLGData.sPlayerData[0].iNoAmmoTimer >= ciTLG_PLAYER_CRITICAL_DAMAGE_NO_AMMO_TIME 
								IF sTLGData.sPlayerData[0].fHealth > 0.05
									CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} Forcing hit critical as ammo <=0")
									sTLGData.sPlayerData[0].fHealth = 0.05
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF sTLGData.sPlayerData[0].iNoAmmoTimer != 0
							sTLGData.sPlayerData[0].iNoAmmoTimer = 0
						ENDIF
					ENDIF
					
					
					TLG_DAMAGE_PLAYER(TLG_DAMAGE_PLAYER_TYPE_BULLET)
					//-- Enemy firing audio
					TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_RIFLE_FIRE, sTLGData.sEnemyData[i].vSpritePos)
					
					
					
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] Player has been hit by enemy ", i, " Ped Pos: ", sTLGData.sEnemyData[i].vSpritePos.x, ", ", sTLGData.sEnemyData[i].vSpritePos.y, " lane: ", sTLGData.sEnemyData[i].iEnemyLane, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " Popup? ",  sTLGData.sEnemyData[i].bIsPopup)
					#ENDIF
				ELSE
					//-- Enemies 'laugh' just before they fire a critical shot
					IF NOT sTLGData.sEnemyData[i].bPlayedCriticalWarningAudio
						IF iCurrentGameTime - sTLGData.sEnemyData[i].iTimeOfLastShot > (ciTLG_ENEMY_TIME_TO_SHOOT - (ENUM_TO_INT(sTLGData.eCurrentLevel) * 200)) - 1000
							IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.1 AND TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0
								sTLGData.sEnemyData[i].bPlayedCriticalWarningAudio = TRUE
								TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_CRIT_WARNING, sTLGData.sEnemyData[i].vSpritePos)
							ENDIF
						ENDIF
					ELSE
						IF TLG_GET_LOCAL_PLAYER_HEALTH() > 0.4
							sTLGData.sEnemyData[i].bPlayedCriticalWarningAudio = FALSE
							CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {DSW} reset bPlayedCriticalWarningAudio for enemy ", i, " as player restored health")
						ENDIF
					ENDIF
					
					//-- Audio for enemy cocking weapon
					IF NOT sTLGData.sEnemyData[i].bPlayedAimAudio
						TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_RIFLE_AIM,sTLGData.sEnemyData[i].vSpritePos)
						sTLGData.sEnemyData[i].bPlayedAimAudio = TRUE
					ENDIF
				ENDIF
			ELIF TLG_IS_BEAR(i)
				IF iCurrentGameTime - sTLGData.sEnemyData[i].iTimeOfLastShot > ciTLG_ENEMY_TIME_TO_SHOOT - (ENUM_TO_INT(sTLGData.eCurrentLevel) * 200)
					sTLGData.sEnemyData[i].iTimeOfLastShot = GET_GAME_TIMER()
					CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {DSW} Bear ", i, " attacking ")
					TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_DAMAGED_BY_BEAR)
					TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_BEAR_ATTACK, sTLGData.sEnemyData[i].vSpritePos)
					sTLGData.iBearAttackTime = GET_GAME_TIMER()
					TLG_DAMAGE_PLAYER(TLG_DAMAGE_PLAYER_TYPE_BEAR)
				ENDIF
			ENDIF
			
	//		IF sTLGData.sEnemyData[i].vSpritePos.x < cfLEFT_SCREEN_LIMIT - ( sTLGData.sEnemyData[i].vSize.x * TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(sTLGData.sEnemyData[i].iEnemyType, sTLGData.sEnemyData[i].iEnemyLane))
	//		OR (sTLGData.sEnemyData[i].bReverse AND sTLGData.sEnemyData[i].bThrownProjectile AND sTLGData.sEnemyData[i].vSpritePos.x > cfRIGHT_SCREEN_LIMIT -  ( sTLGData.sEnemyData[i].vSize.x * TLG_GET_ENEMY_TYPE_SCALE_FOR_LANE(sTLGData.sEnemyData[i].iEnemyType, sTLGData.sEnemyData[i].iEnemyLane)))
			IF TLG_SHOULD_CLEANUP_ENEMY_FOR_BEING_OFF_SCREEN(i)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " went outside the limits and is being set as inactive. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
				#ENDIF
				
				sTLGData.sEnemyData[i].eState = TLG_INACTIVE
				
				IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
				AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
					sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]--
				ENDIF
				
				IF sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] < 0
					sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] = 0
					CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} iCurrentActiveEnemies in lane ", sTLGData.sEnemyData[i].iEnemyLane, " < 0 (2)")
				ENDIF
			ENDIF
			
			IF sTLGData.sEnemyData[i].iEnemyType = TLG_THROWING_ENEMY_1
				IF NOT sTLGData.sEnemyData[i].bThrownProjectile
					sTLGData.sEnemyData[i].iSpriteAnimFrame += sTLGData.sEnemyData[i].iSlowUpdateFrames
					IF sTLGData.sEnemyData[i].iSpriteAnimFrame > ciTLG_MAX_THROWING_FRAMES -2
						sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_THROWING_FRAMES -2
						sTLGData.sEnemyData[i].bThrownProjectile = TRUE
						sTLGData.sEnemyData[i].iTimeOfLastShot = GET_GAME_TIMER()
						vThrow = sTLGData.sEnemyData[i].vSpritePos
						vThrow.x -= 23.75
						vThrow.y -= 95.125
						TLG_INIT_THROWN_ENEMY_PROJECTILE(TLG_ENEMY_PROJECTILE_DYNAMITE, vThrow)
						TLG_PLAY_SOUND_AND_RELEASE(TLG_AUDIO_EFFECT_ENEMY_THROW_DYNAMITE, sTLGData.sEnemyData[i].vSpritePos)
						CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " Throwing projectile")
					ENDIF
				ELSE
					IF GET_GAME_TIMER() - sTLGData.sEnemyData[i].iTimeOfLastShot > 1000
						IF sTLGData.sEnemyData[i].bReverse 
							sTLGData.sEnemyData[i].fStoppingX = 1700.0
						ELSE
							sTLGData.sEnemyData[i].fStoppingX = 0.0
						ENDIF
						
						CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " has thrown projectile and time expired. Running off screen")
						sTLGData.sEnemyData[i].eState = TLG_ACTIVE
					ENDIF
				ENDIF
			ENDIF
			
			TLG_HANDLE_POPUP_ENEMY(i)
		BREAK
		
		CASE TLG_DYING
			IF IS_MOVING_LEVEL()
				sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_ENEMY_DYING_UPDATE()
			ENDIF
			
			VECTOR_2D vOuthousePos
			IF sTLGData.sEnemyData[i].iEnemyType = TLG_CART_1
				
				sTLGData.sEnemyData[i].iSpriteAnimFrame += sTLGData.sEnemyData[i].iSlowUpdateFrames

				IF sTLGData.sEnemyData[i].iSpriteAnimFrame >= ciTLG_MAX_CART_ANIM_FRAMES
					sTLGData.sEnemyData[i].eState = TLG_INACTIVE
					
					IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
					AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
						sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]--
					ENDIF
					
					sTLGData.sEnemyData[i].iSpriteAnimFrame = 0
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " Cart set as inactive. Destroyed. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
					#ENDIF
					
					IF sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] < 0
						sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] = 0
						CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} iCurrentActiveEnemies in lane ", sTLGData.sEnemyData[i].iEnemyLane, " < 0 (3)")
					ENDIF
				ENDIF
			ELSE
			
				IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
				AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
					SWITCH sTLGData.sEnemyData[i].iEnemyLane
						CASE ciTLG_LANE_FOREGROUND
							sTLGData.sEnemyData[i].vSpritePos.y -= 10
							sTLGData.sEnemyData[i].iJumpDistance += 5
						BREAK
						CASE ciTLG_LANE_MIDDLE
							sTLGData.sEnemyData[i].vSpritePos.y -= 10
							sTLGData.sEnemyData[i].iJumpDistance += 10
						BREAK
						CASE ciTLG_LANE_BACKGROUND
						CASE ciTLG_LANE_POPUP_LHS
						CASE ciTLG_LANE_POPUP_RHS
							sTLGData.sEnemyData[i].vSpritePos.y -= 2
							sTLGData.sEnemyData[i].iJumpDistance += 5
						BREAK
					ENDSWITCH
				ENDIF
				
//				IF sTLGData.sEnemyData[i].iEnemyLane = 2
//					#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_DYING] {dsw} Enemy ", i, " Lane 2. Jump Dist ",sTLGData.sEnemyData[i].iJumpDistance , ". Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
//					#ENDIF
//				ENDIF
				IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
				AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
					IF sTLGData.sEnemyData[i].iJumpDistance > 50
						sTLGData.sEnemyData[i].eState = TLG_INACTIVE
						
						
						IF sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] > 0
							sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]--
						ENDIF
						
						//-- If it's a 'pop up' guy and the level is stationary, start a timer
						//-- before the next 'pop up' ped can appear in the same spot
						IF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_POPUP_LHS
						OR sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_POPUP_RHS
							IF sTLGData.sEnemyData[i].bWaitUntilLevelStationary
								CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " Starting active timer for lane ",sTLGData.sEnemyData[i].iEnemyLane)
								sTLGData.iTimeSinceEnemyActive[sTLGData.sEnemyData[i].iEnemyLane] = GET_GAME_TIMER()
							ENDIF
						ENDIF

						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " set as inactive as jump distamce ",sTLGData.sEnemyData[i].iJumpDistance , "> 50. Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
						#ENDIF
					ENDIF
				ELSE
					sTLGData.sEnemyData[i].iSpriteAnimFrame += sTLGData.sEnemyData[i].iSlowUpdateFrames
					IF sTLGData.sEnemyData[i].iSpriteAnimFrame > ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES 
						sTLGData.sEnemyData[i].iSpriteAnimFrame = ciTLG_MAX_OUTHOUSE_ENEMY_ANIM_FRAMES
					ENDIF
					
					IF !TLG_IS_CURRENT_LEVEL_STAGE_STATIONARY()
						IF (sTLGData.sEnemyData[i].vSpritePos.x + sTLGData.sEnemyData[i].vSize.x) < cfLEFT_SCREEN_LIMIT
							sTLGData.sEnemyData[i].eState = TLG_INACTIVE
							IF sTLGData.sEnemyData[i].iEnemyType = TLG_OUTHOUSE_ENEMY
								vOuthousePos = INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSpritePos.x + (cfBASE_SCREEN_WIDTH*2.0), 676.125)
								TLG_ADD_COVER_TO_LIST(vOuthousePos, TLG_ASSET_OUTHOUSE, sTLGData.sEnemyData[i].iEnemyLane)
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] {dsw} Enemy ", i, " set as inactive as Outhouse and off screen ")
								#ENDIF
							ELSE
								IF NOT TLG_IS_MOVING_LEVEL_ABOUT_TO_STOP()
								AND NOT TLG_IS_LEVEL_STOPPED()
									TLG_ADD_ENEMY_TO_LIST(sTLGData.sEnemyData[i].vSpritePos.x, sTLGData.sEnemyData[i].vSpritePos.y , sTLGData.sEnemyData[i].vSpritePos.x, 1, FALSE, TLG_COFFIN_ENEMY, DEFAULT, DEFAULT, DEFAULT, 1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE TLG_WAITING
//			IF sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] != 1
//				CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_ENEMIES] Active in lane ", sTLGData.sEnemyData[i].iEnemyLane, ": ", sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane])
//			ENDIF
			
			//-- Set enemy peds as active, depending on their parameters
			IF sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane] < 1
			OR sTLGData.sEnemyData[i].iEnemyType = TLG_OUTHOUSE_ENEMY
			OR sTLGData.sEnemyData[i].iEnemyType = TLG_COFFIN_ENEMY
			OR (sTLGData.sEnemyData[i].iEnemyType = TLG_CART_1 AND TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_MINE)

			
				IF sTLGData.sEnemyData[i].iLevelStage <= TLG_GET_CURRENT_LEVEL_STAGE()
					IF sTLGData.sEnemyData[i].bWaitUntilLevelStationary
						IF IS_MOVING_LEVEL()
							//-- The ped only appears when the level is stationary, but it is still moving
							RELOOP
						ENDIF
					ENDIF
					
					IF sTLGData.sEnemyData[i].bIsPopup
						IF sTLGData.sEnemyData[i].bWaitUntilLevelStationary

							IF sTLGData.iTimeSinceEnemyActive[sTLGData.sEnemyData[i].iEnemyLane] = 0
							OR GET_GAME_TIMER() - sTLGData.iTimeSinceEnemyActive[sTLGData.sEnemyData[i].iEnemyLane] > 4000
								//-- This ped 'pops-up' from behind scenery, but only when the level has stopped scrolling
									
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_ENEMIES] Setting as SHOOTING stationary pop up enemy ", i, " Lane: ", sTLGData.sEnemyData[i].iEnemyLane, " Is popup? ", sTLGData.sEnemyData[i].bIsPopup," Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
								#ENDIF
								
								sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]++
								sTLGData.sEnemyData[i].eState = TLG_SHOOTING

							ENDIF
						ELSE

							//-- This ped pops up behind scenery while the level is moving
							IF TOG_IS_ENEMY_ON_SCREEN(i, 10.0)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_ENEMIES] Setting as SHOOTING moving pop up enemy ", i, " Lane: ", sTLGData.sEnemyData[i].iEnemyLane, " Is popup? ", sTLGData.sEnemyData[i].bIsPopup," Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType))
								#ENDIF
								
								
								sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]++
								sTLGData.sEnemyData[i].eState = TLG_SHOOTING
								
							ENDIF
							
							
						ENDIF
					ELSE
						IF sTLGData.sEnemyData[i].iRenderAfterWarpedCount = 0
						OR sTLGData.sEnemyData[i].iRenderAfterWarpedCount = sTLGData.sEnemyData[i].iWarpedForMovingLevelCount
							//-- This is a normal ped who runs into position from either the left or the right of the screev
							IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY
							AND sTLGData.sEnemyData[i].iEnemyType != TLG_COFFIN_ENEMY
								sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane]++
							ENDIF
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_ENEMIES] Setting as TLG_ACTIVE enemy ", i, " Lane: ", sTLGData.sEnemyData[i].iEnemyLane, " Is popup? ", sTLGData.sEnemyData[i].bIsPopup," Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " Active in lane: ", sTLGData.iCurrentActiveEnemies[sTLGData.sEnemyData[i].iEnemyLane])
							#ENDIF
							sTLGData.sEnemyData[i].eState = TLG_ACTIVE
						ENDIF
					ENDIF
					
					
				ENDIF
			ENDIF
			
			//-- Still want to update the positions of some 'inactive' enemies. generally for when we don't want them to appear from the start
			IF IS_MOVING_LEVEL()
				IF sTLGData.sEnemyData[i].bIsPopup
				OR (TLG_GET_CURRENT_LEVEL() = TLG_LEVEL_MINE AND TLG_IS_GUN_CART(i))
				OR sTLGData.sEnemyData[i].iRenderAfterWarpedCount > 0
					IF NOT sTlgData.sEnemyData[i].bWaitUntilLevelStationary
						sTLGData.sEnemyData[i].vSpritePos.x -= TLG_GET_ENEMY_WAITING_UPDATE()
					ENDIF
					
					IF sTLGData.sEnemyData[i].iRenderAfterWarpedCount > 0
						IF sTLGData.sEnemyData[i].vSpritePos.x <= 0
							sTLGData.sEnemyData[i].vSpritePos.x = TLG_GET_OFFSET_FOR_ELEMENT_WARP_FOR_CURRENT_LEVEL()
							CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_WAITING] [OUTHOUSE_WARP] [dsw] Enemy: ",i, " Warped")
							sTLGData.sEnemyData[i].iWarpedForMovingLevelCount++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		ENDSWITCH
		
//		#IF IS_DEBUG_BUILD
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_HANDLE_ENEMIES] [TLG_ACTIVE] [dsw] Processing ENDFOR Enemy: ",i, " Max: ", ciTLG_MAX_ENEMIES, " Lane: ",sTLGData.sEnemyData[i].iEnemyLane, " Type:", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " State: ", TLG_GET_ENEMY_STATE_AS_STRING(sTLGData.sEnemyData[i].eState))
//		#ENDIF
	ENDFOR
	
	
	//-- Need to clear that we've been attacked by a bear if all bears are dead (i,e, if there hasn't been an attack for a while)
	//-- TLG_LEVELS_BIT_DAMAGED_BY_BEAR is used to determine if we should display the claw-attack sprite in TLG_DRAW_SHOT_FLASH
	IF TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_DAMAGED_BY_BEAR)
		IF GET_GAME_TIMER() - sTLGData.iBearAttackTime > 3000
			TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(TLG_GET_CURRENT_LEVEL(),TLG_LEVELS_BIT_DAMAGED_BY_BEAR)
		ENDIF
	ENDIF
ENDPROC

PROC TLG_HANDLE_HOSTAGES()

	FLOAT fXPos
	INT i
	
	FOR i = 0 TO ciTLG_MAX_HOSTAGES - 1
		IF sTLGData.sHostageData[i].bRendering
			sTLGData.sHostageData[i].iSpriteAnimFrame += sTLGData.sHostageData[i].iSlowUpdateFrames
			
			IF sTLGData.sHostageData[i].iHealth > 0
				IF sTLGData.sHostageData[i].iSpriteAnimFrame >= ciTLG_MAX_ANIM_FRAMES
					sTLGData.sHostageData[i].iSpriteAnimFrame = 0
				ENDIF
			
				IF sTLGData.sHostageData[i].bGoingBack = FALSE AND sTLGData.sHostageData[i].fStoppingX != 0.0
					IF sTLGData.sHostageData[i].vSpritePos.x < sTLGData.sHostageData[i].fStoppingX
						sTLGData.sHostageData[i].bGoingBack = TRUE
					ELSE
						sTLGData.sHostageData[i].vSpritePos.x -= TLG_GET_HOSTAGE_UPDATE()
					ENDIF
				ELSE
					sTLGData.sHostageData[i].vSpritePos.x += TLG_GET_HOSTAGE_UPDATE()
				ENDIF
				
				
				fXPos = sTLGData.sHostageData[i].vSpritePos.x	
				IF NOT TLG_IS_HOSTAGE_BIT_SET(i, TLG_HOSTAGE_BIT_APPEARED_ONSCREEN)
					IF fXPos > cfLEFT_SCREEN_LIMIT AND fXPos < cfRIGHT_SCREEN_LIMIT	
						//-- Appeared on screen
						TLG_SET_HOSTAGE_BIT(i, TLG_HOSTAGE_BIT_APPEARED_ONSCREEN)
						TLG_PLAY_HOSTAGE_HELP_AUDIO(sTLGData.sHostageData[i].eHostageType, sTLGData.sHostageData[i].vSpritePos)
					ENDIF
				ELSE
					//-- Hostage is 'saved' if they appear on screen and then leaves the screen without being shot
					IF sTLGData.sHostageData[i].eHostageType = TLG_GIRL
					OR sTLGData.sHostageData[i].eHostageType = TLG_GUY

						IF NOT TLG_IS_HOSTAGE_BIT_SET(i, TLG_HOSTAGE_BIT_SAVED)
							IF fXPos < cfLEFT_SCREEN_LIMIT OR fXPos > cfRIGHT_SCREEN_LIMIT
								//-- Left the screen
								TLG_SET_HOSTAGE_BIT(i, TLG_HOSTAGE_BIT_SAVED)
								TLG_PLAY_HOSTAGE_ESCAPE_AUDIO(sTLGData.sHostageData[i].eHostageType, sTLGData.sHostageData[i].vSpritePos)
								TLG_INCREASE_HOSTAGES_SAVED_COUNT_FOR_CURRENT_LEVEL()
								TLG_RESET_HOSTAGE(i)
								
								RELOOP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELIF sTLGData.sHostageData[i].bDying
				
				IF IS_MOVING_LEVEL()
					sTLGData.sHostageData[i].vSpritePos.x -= TLG_GET_HOSTAGE_DYING_UPDATE()
				ENDIF
					
				IF sTLGData.sHostageData[i].iSpriteAnimFrame >= ciTLG_EXPLOSION_MAX_ANIM_FRAMES
					sTLGData.sHostageData[i].bDying = FALSE
					
				ENDIF
				
			ENDIF	
		ENDIF
	ENDFOR
	
ENDPROC


PROC TLG_HANDLE_FX()

	INT i
	FOR i = 0  TO ciTLG_MAX_FX - 1
		IF sTLGData.sFXData[i].bActive
			
			sTLGData.sFXData[i].iSpriteBlastAnimFrame += sTLGData.sFXData[i].iSlowUpdateFrames
		//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_DRAW_BULLETS_AND_EFFECTS] {DSW} Frame: ", sTLGData.sFXData[i].iSpriteBlastAnimFrame)
			IF IS_MOVING_LEVEL() AND sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_PLAYING
				sTLGData.sFXData[i].vSpritePos.x -= TLG_GET_FX_UPDATE()
			ENDIF
			
			SWITCH sTLGData.sFXData[i].eFXType
			CASE TLG_FX_ENEMY_SHOT
			
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame >= ciTLG_ENEMY_SHOT_MAX_ANIM_FRAMES
					sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
					sTLGData.sFXData[i].bActive = FALSE
				ENDIF
			BREAK
			CASE TLG_FX_EXPLOSION
			
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame >= ciTLG_EXPLOSION_MAX_ANIM_FRAMES
					sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
					sTLGData.sFXData[i].bActive = FALSE
				ENDIF
			BREAK
			
			CASE TLG_FX_ENEMY_TNT_EXP
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame >= ciTLG_MAX_ENEMY_THROWN_EXPLOSION_ANIM_FRAMES
					sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
					sTLGData.sFXData[i].bActive = FALSE
				ENDIF
			BREAK 
			
			CASE TLG_FX_LOOT_CRATE
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame >= ciTLG_MAX_LOOT_CRATE_FX_ANIM_FRAMES
					sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
					sTLGData.sFXData[i].bActive = FALSE
				ENDIF 
			BREAK
			
			CASE TLG_FX_FEATHERS
				IF sTLGData.sFXData[i].iSpriteBlastAnimFrame >= ciTLG_FEATHERS_MAX_ANIM_FRAMES
					sTLGData.sFXData[i].iSpriteBlastAnimFrame = 0
					sTLGData.sFXData[i].bActive = FALSE
				ENDIF 
			BREAK
			
			ENDSWITCH
			
		ENDIF
	ENDFOR
	
ENDPROC

//-- Handle gatling gun overheating
PROC TLG_PROCESS_GATLING_GUN_TEMP()
	FLOAT fTimeOverHeat = cfTLG_GATLING_OVERHEAT_TIME_SECONDS
	FLOAT fTimeCooldown = cfTLG_GATLING_COOLDOWN_TIME_SECONDS
	
	BOOL bShooting
	FLOAT fFrameTime = GET_FRAME_TIME()
	
	IF TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_GATLING
	
		bSHooting = (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						AND  TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0)
						
		IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_GATLING_OVERHEAT)
			IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_GATLING_STOPPED_SHOOTING)
				IF NOT bSHooting
					TLG_SET_PLAYER_BIT(TLG_PLAYER_BIT_GATLING_STOPPED_SHOOTING)
				ENDIF
			ENDIF
			
			IF bSHooting
				IF sTLGData.sPlayerData[0].iGatlingOverheatTime = 0
				OR (GET_GAME_TIMER() - sTLGData.sPlayerData[0].iGatlingOverheatTime) > 100
					sTLGData.sPlayerData[0].iGatlingOverheatTime = GET_GAME_TIMER()
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SHOOT_GATLING_OVERHEAT)
				ENDIF
				
				IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_GATLING_STOPPED_SHOOTING)
					sTLGData.sPlayerData[0].fGatlingGunTemp = 1.0
					EXIT
				ENDIF
			ENDIF
			
			
			
			IF sTLGData.sPlayerData[0].fGatlingGunTemp > 0.0
				sTLGData.sPlayerData[0].fGatlingGunTemp -=  (1.0 / (fTimeCooldown/fFrameTime))
			//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_GATLING_GUN_TEMP] DECREASE sTLGData.sPlayerData[0].fGatlingGunTemp = ", sTLGData.sPlayerData[0].fGatlingGunTemp)
			ELSE
				sTLGData.sPlayerData[0].fGatlingGunTemp = 0.0
				TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_GATLING_OVERHEAT)
				TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_GATLING_STOPPED_SHOOTING)
			ENDIF
			
			EXIT
		ENDIF
		
		
						
		IF bSHooting
			IF sTLGData.sPlayerData[0].fGatlingGunTemp < 1.0
								
				IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_GATLING_OVERHEAT)
					sTLGData.sPlayerData[0].fGatlingGunTemp +=  (1.0 / (fTimeOverHeat/fFrameTime))
					
					IF sTLGData.sPlayerData[0].fGatlingGunTemp >= 1.0
						TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_GATLING_OVERHEAT)
						TLG_SET_PLAYER_BIT(TLG_PLAYER_BIT_GATLING_OVERHEAT)
					ENDIF
//				ELSE
//					IF sTLGData.sPlayerData[0].iGatlingOverheatTime = 0
//					OR (GET_GAME_TIMER() - sTLGData.sPlayerData[0].iGatlingOverheatTime) > 100
//						sTLGData.sPlayerData[0].iGatlingOverheatTime = GET_GAME_TIMER()
//						TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_SHOOT_GATLING_OVERHEAT)
//					ENDIF
				ENDIF
			ELSE
				sTLGData.sPlayerData[0].fGatlingGunTemp = 1.0
			ENDIF
		ELSE
			IF sTLGData.sPlayerData[0].fGatlingGunTemp > 0.0
				sTLGData.sPlayerData[0].fGatlingGunTemp -=  (1.0 / (fTimeCooldown/fFrameTime))
			//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_GATLING_GUN_TEMP] DECREASE sTLGData.sPlayerData[0].fGatlingGunTemp = ", sTLGData.sPlayerData[0].fGatlingGunTemp)
			ELSE
				sTLGData.sPlayerData[0].fGatlingGunTemp = 0.0
				TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_GATLING_OVERHEAT)
				TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_GATLING_STOPPED_SHOOTING)
			ENDIF
		ENDIF
	ELSE
		IF sTLGData.sPlayerData[0].fGatlingGunTemp != 0.0
			sTLGData.sPlayerData[0].fGatlingGunTemp = 0.0
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TLG_PLAYER_SHOT(INT iPlayer, BOOL bIsMenu)
	TLG_PLAYER_WEAPON_DATA weaponData
	IF TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() > 0
	OR bIsMenu
		IF  TLG_IS_PLAYER_SHOOTING()
			//-- Shoot pistol
			IF TLG_GET_PLAYER_CURRENT_WEAPON() = GET_PLAYER_DEFAULT_WEAPON()
				IF NOT bIsMenu
					sTLGData.sPlayerData[0].weaponData[TLG_PLAYER_WEAPON_TYPE_DEFAULT].iLastShotTime = GET_GAME_TIMER()
				ENDIF
				RETURN TRUE
			ELSE
				
				IF TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_SHOTGUN
				OR TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_RIFLE
					weaponData = TLG_GET_PLAYER_WEAPON_DATA(TLG_GET_PLAYER_CURRENT_WEAPON())
					IF sTLGData.sPlayerData[0].weaponData[TLG_PLAYER_WEAPON_TYPE_SPECIAL].iLastShotTime = 0
					OR GET_GAME_TIMER() - sTLGData.sPlayerData[0].weaponData[TLG_PLAYER_WEAPON_TYPE_SPECIAL].iLastShotTime >= weaponData.iMinTimeBetweenShots
						sTLGData.sPlayerData[0].weaponData[TLG_PLAYER_WEAPON_TYPE_SPECIAL].iLastShotTime = GET_GAME_TIMER()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//--SHoot gatling gun
		IF (TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_GATLING 
		AND (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT) 
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) )
		AND GET_GAME_TIMER() - sTLGData.sPlayerData[iPlayer].iLastProjectileTime > 100)
			IF NOT TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_GATLING_OVERHEAT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TLG_HANDLE_PLAYER_RELOADING()
	TLG_PLAYER_WEAPON_TYPE weaponType = sTLGData.sPlayerData[0].weaponType
	
	//-- Exit if the gun clip is full
	IF NOT TLG_CAN_PLAYER_CURRNT_WEAPON_BE_RELOADED()
		IF sTLGData.sPlayerData[0].bIsReloading[weaponType]
			sTLGData.sPlayerData[0].bIsReloading[weaponType] = FALSE
		ENDIF
		
		IF sTLGData.sPlayerData[0].weaponData[weaponType].iReloadSingleBulletTime != 0
			sTLGData.sPlayerData[0].weaponData[weaponType].iReloadSingleBulletTime = 0
		ENDIF
		
		IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_RELOADED_FIRST_BULLET)
			TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_RELOADED_FIRST_BULLET)
		ENDIF
		
		EXIT
	ENDIF
	
	//-- Stop reloading if the player shoots after starting to reload.
	IF TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() > 0
		IF TLG_PLAYER_SHOT(0, FALSE)
			IF sTLGData.sPlayerData[0].bIsReloading[weaponType]
				sTLGData.sPlayerData[0].bIsReloading[weaponType] = FALSE
			ENDIF
			
			IF sTLGData.sPlayerData[0].weaponData[weaponType].iReloadSingleBulletTime != 0
				sTLGData.sPlayerData[0].weaponData[weaponType].iReloadSingleBulletTime = 0
			ENDIF
			
			IF TLG_IS_PLAYER_BIT_SET(TLG_PLAYER_BIT_RELOADED_FIRST_BULLET)
				TLG_CLEAR_PLAYER_BIT(TLG_PLAYER_BIT_RELOADED_FIRST_BULLET)
			ENDIF
			EXIT
		ENDIF
	ENDIF
	
	IF sTLGData.sPlayerData[0].bIsReloading[weaponType]
		TLG_RELOAD_PLAYER_WEAPON()
	ENDIF
	
ENDPROC


PROC TLG_HANDLE_PLAYER_SHOOTING(INT iPlayer, BOOL bIsMenu = FALSE)
	
	sTLGData.sPlayerData[iPlayer].bHasJustShot = FALSE
		
	// PROCESS BULLETS
//	IF ((TLG_GET_PLAYER_CURRENT_WEAPON() = GET_PLAYER_DEFAULT_WEAPON() AND TLG_IS_PLAYER_SHOOTING())
//		OR (TLG_GET_PLAYER_CURRENT_WEAPON() = TLG_PLAYER_WEAPON_GATLING AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT) AND GET_GAME_TIMER() - sTLGData.sPlayerData[iPlayer].iLastProjectileTime > 100))
//	AND (TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() > 0 AND iPlayer = 0 OR bIsMenu)
	IF TLG_PLAYER_SHOT(iPlayer, bIsMenu)
		IF bIsMenu
		OR TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0
		 
			sTLGData.sPlayerData[iPlayer].iLastProjectileTime = GET_GAME_TIMER()
				
			CPRINTLN(DEBUG_MINIGAME, "TLG - Player shot.")
			
			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 100, 20)
			sTLGData.sPlayerData[iPlayer].bHasJustShot = TRUE
			
			sTLGData.sPlayerData[iPlayer].sBulletData[sTLGData.sPlayerData[iPlayer].iProjectileIndex].vSpritePos = sTLGData.sPlayerData[iPlayer].vSpriteMarkerPos
			
			sTLGData.sPlayerData[iPlayer].sBulletData[sTLGData.sPlayerData[iPlayer].iProjectileIndex].iSpriteBlastAnimFrame = 0
			sTLGData.sPlayerData[iPlayer].sBulletData[sTLGData.sPlayerData[iPlayer].iProjectileIndex].bShot = TRUE

			sTLGData.sPlayerData[iPlayer].iProjectileIndex = (sTLGData.sPlayerData[iPlayer].iProjectileIndex + 1) % ciTLG_MAX_PROJECTILES
		ENDIF
		
		IF NOT bIsMenu
			IF TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0
				TLG_INCREASE_SHOT_FIRED_FOR_CURRENT_LEVEL()
				TLG_HANDLE_PLAYER_SHOOTING_AUDIO()
				TLG_REDUCE_PLAYER_AMMO_IN_CLIP()
			ENDIF
		ELSE
			TLG_HANDLE_PLAYER_SHOOTING_AUDIO()
		ENDIF
	ENDIF
	
	IF sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_PLAYING
		IF TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0
			IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
			
				//-- Check for player reloading
				IF NOT sTLGData.sPlayerData[0].bIsReloading[sTLGData.sPlayerData[0].weaponType]
					sTLGData.sPlayerData[0].bIsReloading[sTLGData.sPlayerData[0].weaponType] = TRUE
				ENDIF
			ELSE
				IF TLG_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() = 0 
				AND NOT TLG_DOES_PLAYER_CURRENT_WEAPON_HAVE_INFINITE_AMMO()
					IF TLG_GET_PLAYER_TOTAL_AMMO_IN_CURRENT_WEAPON(FALSE) > 0
						//-- Check for player shootig empty weapon
						IF TLG_IS_PLAYER_SHOOTING()
							TLG_HANDLE_PLAYER_FIRING_EMPTY_WEAPON_AUDIO()
						ENDIF
					ELSE
						IF sTLGData.sPlayerData[0].weaponType = TLG_PLAYER_WEAPON_TYPE_SPECIAL
							TLG_SET_PLAYER_WEAPON_TYPE(TLG_PLAYER_WEAPON_TYPE_DEFAULT)
							TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_NONE, FALSE)
							TLG_INIT_PLAYER_SPECIAL_WEAPON()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT i
	FOR i = 0  TO ciTLG_MAX_PROJECTILES - 1
	
		IF sTLGData.sPlayerData[iPlayer].sBulletData[i].bShot

			sTLGData.sPlayerData[iPlayer].sBulletData[i].iSpriteBlastAnimFrame += sTLGData.sPlayerData[iPlayer].sBulletData[i].iSlowUpdateFrames
			
			IF bIsMenu
				HANDLE_PLAYER_BULLET_IN_MENU(iPlayer, i)
			ELSE
				HANDLE_PLAYER_BULLET_COLLISION(iPlayer, i)
				
				IF IS_MOVING_LEVEL()
					sTLGData.sPlayerData[iPlayer].sBulletData[i].vSpritePos.X -= TLG_GET_PROJECTILE_UPDATE()
				ENDIF
			ENDIF
			
			IF sTLGData.sPlayerData[iPlayer].sBulletData[i].iSpriteBlastAnimFrame >= ciTLG_PLAYER_SHOT_MAX_ANIM_FRAMES
				
				sTLGData.sPlayerData[iPlayer].sBulletData[i].bShot = FALSE
				sTLGData.sPlayerData[iPlayer].sBulletData[i].bHit = FALSE
				sTLGData.sPlayerData[iPlayer].sBulletData[i].bHitObject = FALSE 
				sTLGData.sPlayerData[iPlayer].sBulletData[i].bPlayedAudio = FALSE
				sTLGData.sPlayerData[iPlayer].sBulletData[i].iSpriteBlastAnimFrame = 0
				
			ENDIF
			
		ENDIF
	ENDFOR
	
	TLG_PROCESS_GATLING_GUN_TEMP()
ENDPROC

PROC TLG_PROCESS_MENU_CONTROLS()

//	FLOAT fAxisThresholdMod = cfIAP_CONTROLLER_AXIS_THRESHOLD_MOD
//	
//	FLOAT fRightStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
//	FLOAT fRightStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
	TLG_CHECK_PLAYER_HOVERING_MENU_OPTION()
	
//	fAxisThresholdMod = 0.5
//	
//	//Reticule 1
//	IF fRightStickX < -cfSTICK_THRESHOLD * fAxisThresholdMod AND sTLGData.sPlayerData[0].vSpriteMarkerPos.x > 0.0
//		sTLGData.sPlayerData[0].vSpriteMarkerPos.x -= 30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15)
//	ELIF fRightStickX > cfSTICK_THRESHOLD * fAxisThresholdMod AND sTLGData.sPlayerData[0].vSpriteMarkerPos.x < (cfBASE_SCREEN_WIDTH - 40)
//		sTLGData.sPlayerData[0].vSpriteMarkerPos.x += 30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15)
//	ENDIF
//	
//	IF fRightStickY < -cfSTICK_THRESHOLD * fAxisThresholdMod AND sTLGData.sPlayerData[0].vSpriteMarkerPos.y > 20.0
//		sTLGData.sPlayerData[0].vSpriteMarkerPos.y -= 30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15)
//	ELIF fRightStickY > cfSTICK_THRESHOLD * fAxisThresholdMod AND sTLGData.sPlayerData[0].vSpriteMarkerPos.y < (cfBASE_SCREEN_HEIGHT - 40)
//		sTLGData.sPlayerData[0].vSpriteMarkerPos.y += 30 - (BOOL_TO_INT(sTLGData.sPlayerData[0].bHoveringEnemy) * 15)
//	ENDIF
	
	TLG_HANDLE_RETICULE_MOVEMENT()
	
	CPRINTLN(DEBUG_MINIGAME, "[TLG_ARCADE] RETICULE ", sTLGData.sPlayerData[0].vSpriteMarkerPos.x, ",", sTLGData.sPlayerData[0].vSpriteMarkerPos.y)
ENDPROC

/// PURPOSE:
///    Levels are split up into stages. Generally when the level stops / starts moving a new stage ends / begins
/// RETURNS: True when all stages complete
///    
FUNC BOOL TLG_MAINTAIN_PLAYER_CURRENT_LEVEL_PROGRESSION()
	INT iNextScreen
	SWITCH sTLGData.eCurrentLevel
		CASE TLG_LEVEL_DESERT
		//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [GD] TLG_GET_CURRENT_LEVEL_STAGE() = ", TLG_GET_CURRENT_LEVEL_STAGE(), " TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1 = ", TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1)
			IF TLG_GET_CURRENT_LEVEL_STAGE() = TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1
				IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_COMPLETE_LEVEL)
					RETURN TRUE
				ENDIF
			ELSE
				SWITCH TLG_GET_CURRENT_LEVEL_STAGE()
					CASE 0
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() = 0
							IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_INCREASE_CURRENT_LEVEL_STAGE()
								TLG_INIT_ENEMIES(1)
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			ENDIF
		BREAK
		
			CASE TLG_LEVEL_TOWN
			IF TLG_GET_CURRENT_LEVEL_STAGE() = TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1
		//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [GD] TLG_GET_CURRENT_LEVEL_STAGE() = ", TLG_GET_CURRENT_LEVEL_STAGE(), " TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1 = ", TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1)
				IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_COMPLETE_LEVEL)
					RETURN TRUE
				ENDIF
			ELSE
				SWITCH TLG_GET_CURRENT_LEVEL_STAGE()
					CASE 0
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() <= 3
							IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								
							ENDIF
						ENDIF
					BREAK
					
					CASE 1
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() = 0
							TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(TLG_LEVEL_TOWN, TLG_LEVELS_BIT_SHOULD_STOP)
							TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(TLG_LEVEL_TOWN, TLG_LEVELS_BIT_STOPPED)
							TLG_RESET_ALL_COLLECTIBLE_ITEMS_NOT_ON_SCREEN()
							iNextScreen = TLG_GET_SCREEN_TO_STOP_ON()
							iNextScreen++
							IF iNextScreen > 4
								iNextScreen = 1
							ENDIF
							
							TLG_ADD_COVER_TO_LIST_FOR_SCREEN(TLG_ASSET_CRATE_1, iNextScreen)
							
//							iNextScreen++
//							IF iNextScreen > 4
//								iNextScreen = 1
//							ENDIF
//							
//							TLG_ADD_ITEM_TO_LIST_FOR_SCREEN(TLG_HEALTH, 1, iNextScreen)
//							
//							iNextScreen++
//							IF iNextScreen > 4
//								iNextScreen = 1
//							ENDIF
//							
//							TLG_ADD_ITEM_TO_LIST_FOR_SCREEN(TLG_REVOLVER_AMMO, 1, iNextScreen)
							
							TLG_RESET_SCREEN_TO_STOP_ON()
							TLG_INCREASE_CURRENT_LEVEL_STAGE()
							TLG_INIT_ENEMIES(2)
						ENDIF
					BREAK
					
					CASE 2
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() = 0
							IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE TLG_LEVEL_FOREST
			IF TLG_GET_CURRENT_LEVEL_STAGE() = TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1
		//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [GD] TLG_GET_CURRENT_LEVEL_STAGE() = ", TLG_GET_CURRENT_LEVEL_STAGE(), " TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1 = ", TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1)
				IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_COMPLETE_LEVEL)
					RETURN TRUE
				ENDIF
			ELSE
				SWITCH TLG_GET_CURRENT_LEVEL_STAGE()
					CASE 0
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() <= 5
							IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
					
			ENDIF
		BREAK
		
		CASE TLG_LEVEL_MINE
			IF TLG_GET_CURRENT_LEVEL_STAGE() = TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1
		//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [GD] TLG_GET_CURRENT_LEVEL_STAGE() = ", TLG_GET_CURRENT_LEVEL_STAGE(), " TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1 = ", TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1)
				IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_COMPLETE_LEVEL)
					RETURN TRUE
				ENDIF
			ELSE
				SWITCH TLG_GET_CURRENT_LEVEL_STAGE()
					CASE 0
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() <= 5
							IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
					
			ENDIF
		BREAK
		

		CASE TLG_LEVEL_GRAVEYARD
			IF TLG_GET_CURRENT_LEVEL_STAGE() = TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [GD] Enemies alive: ", TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() , " TLG_GET_CURRENT_LEVEL_STAGE() = ", TLG_GET_CURRENT_LEVEL_STAGE(), " TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1 = ", TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES() - 1)
				IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0
					TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_COMPLETE_LEVEL)
					RETURN TRUE
				ENDIF
			ELSE
				SWITCH TLG_GET_CURRENT_LEVEL_STAGE()
					CASE 0
						IF TLG_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_STAGE() <= 5
							IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
								
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
					
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC TLG_CHECK_PLAYER_PROGRESS()
	
	IF sTLGData.eLevelStage = TLG_PLAY
		
		IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.0 // OR sTLGData.sPlayerData[0].iAmmo <= 0
			TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_HEALTH_DEAD)
			//TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_DEATH)
		//	sTLGData.bFadingOut = TRUE
			sTLGData.eLevelStage = TLG_WAIT_FOR_DEAD
			
			IF NOT IS_BIT_SET(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_STOPPED))
				TLG_TRIGGER_MUSIC_EVENT(TLG_MUSIC_EVENT_STOP)
				SET_BIT(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_STOPPED))
			ENDIF
	
			TLG_MAINTAIN_PLAYER_AWARDS()
			
		ELSE 
			
			IF TLG_MAINTAIN_PLAYER_CURRENT_LEVEL_PROGRESSION()
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [GD] Level completed")

				sTLGData.bLevelsCompleted[ENUM_TO_INT(sTLGData.eCurrentLevel)] = TRUE
				sTLGData.bFadingOut = TRUE
				sTLGData.eLevelStage = TLG_FADING_OUT
							
				#IF IS_DEBUG_BUILD
					TLG_OUTPUT_LEVEL_COMPLETE_DATA()
					
					IF TLG_ARE_ALL_LEVELS_COMPLETE()
						TLG_OUTPUT_ALL_LEVELS_COMPLETE_DATA()
					ENDIF
				#ENDIF
				

				TLG_MAINTAIN_PLAYER_AWARDS()

			ENDIF
		ENDIF
	ELIF sTLGData.eLevelStage = TLG_WAIT_FOR_DEAD
		IF sTLGData.sPlayerData[0].iDeadTime = 0
			sTLGData.sPlayerData[0].iDeadTime = GET_GAME_TIMER()
		ELSE
			IF GET_GAME_TIMER() - sTLGData.sPlayerData[0].iDeadTime >= ciTLG_WAIT_FOR_DEAD_TIME
				sTLGData.bFadingOut = TRUE
				sTLGData.eLevelStage = TLG_FADING_OUT
			ENDIF
		ENDIF
	ELIF sTLGData.eLevelStage = TLG_FADING_OUT
		
		IF sTLGData.iFade >= 255
			TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_GAMEPLAY)
			TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_MENUS)
			TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_RESULT_SCREEN)
		ENDIF
		
	ENDIF
	
ENDPROC

//
// PROCESS STATES
//



// Process INIT
PROC TLG_PROCESS_CLIENT_STATE_INIT()

	ARCADE_CABINET_COMMON_INITIALIZATION()
	
	TLG_INIT_COLOUR_STRUCTS()
	TLG_INIT_MENU()
	
	TLG_INIT_PLAYER_WEAPON()
	
	TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_REQUESTING_ASSETS)

ENDPROC

// Process REQUESTING ASSETS
PROC TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS()


//
//	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31BaseTextDict)
//	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31BaseTextDict)
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading MPLastGunslingers")
//		EXIT
//	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31TitleTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31HudTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31LevelSelectTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31EnemiesTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31GameplayCommonTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31ForegroundEnemiesATextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31ForegroundEnemiesBTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31ForegroundEnemiesCTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31HostagesTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31CommonPropsTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31LevelCompTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31ResultsTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31DeadTextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31Level1TextDict)
	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31FacadeTextDict)
	
	IF NOT ARCADE_GAMES_POSTFX_REQUESTING_ASSETS()
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS Loading POSTFX assets")
		EXIT
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31TitleTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31TitleTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31HudTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31HudTextDict)
		EXIT
	ENDIF
	

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31FacadeTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31FacadeTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31LevelSelectTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31LevelSelectTextDict)
		EXIT
	ENDIF

	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31LevelCompTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31LevelCompTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31Level1TextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31Level1TextDict)
		EXIT
	ENDIF
//	
//	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31Level2TextDict)
//	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31Level2TextDict)
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31Level2TextDict)
//		EXIT
//	ENDIF
//	
//	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31Level3TextDict)
//	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31Level3TextDict)
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31Level3TextDict)
//		EXIT
//	ENDIF
//	
//	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31Level4TextDict)
//	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31Level4TextDict)
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31Level4TextDict)
//		EXIT
//	ENDIF
//	
//	REQUEST_STREAMED_TEXTURE_DICT(sTLGData.tl31Level5TextDict)
//	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31Level5TextDict)
//		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31Level5TextDict)
//		EXIT
//	ENDIF
	

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31EnemiesTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31EnemiesTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31GameplayCommonTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31GameplayCommonTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31ForegroundEnemiesATextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31ForegroundEnemiesATextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31ForegroundEnemiesBTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31ForegroundEnemiesBTextDict)
		EXIT
	ENDIF
	
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31ForegroundEnemiesCTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31ForegroundEnemiesCTextDict)
		EXIT
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31HostagesTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31HostagesTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31CommonPropsTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31CommonPropsTextDict)
		EXIT
	ENDIF
	

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31ResultsTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31ResultsTextDict)
		EXIT
	ENDIF
	
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTLGData.tl31DeadTextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Loading ", sTLGData.tl31DeadTextDict)
		EXIT
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3/H3_ArcMac_BR2")
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS - Waiting for audio")
		EXIT
	ENDIF
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_BadlandsSkipIntro")
			TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_MENUS)
			TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_TITLE_SCREEN)
			EXIT
		ENDIF
	#ENDIF
	

	TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_FIB)
			

//	DG_START_MOVIE(sTLGData.bmIdTlgIntro, "TLG_Intro")  // _1992_DegenatronLogo_720_auto
////	DG_START_MOVIE(sTLGData.bmIdTlgIntro, "_Pixtro_Intro_80s_1")
//	
//	TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INTRO)

ENDPROC

//-- Process intro Bink movie
PROC TLG_PROCESS_CLIENT_INTRO()
	
	//Wait for the intro movie to finish
	TLG_DRAW_MENU_BACKGROUND()
	
	FLOAT fRatioWidth = 1.0 * GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	FLOAT fRatioHeight = 1.0
	BOOL bMovieFinished = DG_DRAW_MOVIE(sTLGData.bmIdTlgIntro, fRatioWidth, fRatioHeight)
//	ARCADE_GAMES_POSTFX_DRAW()
	
	TWS_DRAW_FRONT_FX()
	
	IF NOT bMovieFinished
//		CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [TLG_PROCESS_CLIENT_INTRO] Movie Time: ", GET_BINK_MOVIE_TIME(sTLGData.bmIdTlgIntro))
		IF NOT IS_BIT_SET(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_STARTED_INTRO))
			IF GET_BINK_MOVIE_TIME(sTLGData.bmIdTlgIntro) >= 34.0
				TLG_TRIGGER_MUSIC_EVENT(TLG_MUSIC_EVENT_INTRO)
				SET_BIT(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_STARTED_INTRO))
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_MENUS)
	TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_TITLE_SCREEN)
	
ENDPROC


PROC TLG_PROCESS_CLIENT_STATE_FIB
	TLG_DRAW_MENU_BACKGROUND()
	
//	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31BaseTextDict, "ARCADE_FIB", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTLG_FIB_SCREEN_WIDTH, cfTLG_FIB_SCREEN_HEIGHT), 0, sTLGData.rgbaSprite)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31TitleTextDict, "BADLANDS_PSA_SCREEN", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfTLG_FIB_SCREEN_WIDTH, cfTLG_FIB_SCREEN_HEIGHT), 0, sTLGData.rgbaSprite)
	
	TWS_DRAW_FRONT_FX()
	
	IF sTLGData.iTimer = 0
		sTLGData.iTimer = GET_GAME_TIMER()
	ELSE
		IF GET_GAME_TIMER() - sTLGData.iTimer > 5000
		OR TLG_IS_PLAYER_SHOOTING()
			DG_START_MOVIE(sTLGData.bmIdTlgIntro, "TLG_Intro")  // _1992_DegenatronLogo_720_auto
	
			TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_INTRO)

		ENDIF
	ENDIF
	
ENDPROC

PROC TLG_TRIGGER_TITLE_MUSIC()
	IF NOT IS_BIT_SET(sTLGData.iMusicBitset,  ENUM_TO_INT(TLG_MUSIC_BIT_STARTED_TITLE))
		CPRINTLN(DEBUG_MINIGAME, "[TLG_TRIGGER_TITLE_MUSIC] Called. CallstacK:")
		DEBUG_PRINTCALLSTACK()
		TLG_TRIGGER_MUSIC_EVENT(TLG_MUSIC_EVENT_TITLE)
		SET_BIT(sTLGData.iMusicBitset,  ENUM_TO_INT(TLG_MUSIC_BIT_STARTED_TITLE))
	ENDIF
ENDPROC

// Process TITLE SCREEN
PROC TLG_PROCESS_CLIENT_STATE_TITLE_SCREEN()
	
	TLG_TRIGGER_TITLE_MUSIC()
	
	TLG_PROCESS_MENU_CONTROLS()
	TLG_HANDLE_PLAYER_SHOOTING(0, TRUE)
	TLG_DRAW_CLIENT_STATE_TITLE_SCREEN()
	TLG_MAINTAIN_HELP_TEXT()

ENDPROC

// Process STATE MENU
PROC TLG_PROCESS_CLIENT_STATE_MENU()
	TLG_TRIGGER_TITLE_MUSIC()
	
	TLG_PROCESS_MENU_CONTROLS()
	TLG_HANDLE_PLAYER_SHOOTING(0, TRUE)
	TLG_DRAW_CLIENT_STATE_MENU()
	TLG_CHECK_PLAYER_RETURNING_TO_MENU(TRUE)
	TLG_MAINTAIN_HELP_TEXT()
ENDPROC

// Process INIT LEVEL
PROC TLG_PROCESS_CLIENT_STATE_INIT_LEVEL()
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_CLIENT_STATE_INIT_LEVEL] {DSW} Called. Level: ", TLG_GET_LEVEL_NAME_AS_STRING(TLG_GET_CURRENT_LEVEL()))
	#ENDIF
	
	//-- Need to still draw the menu during this frame
	TLG_DRAW_CLIENT_STATE_MENU()
	
	TEXT_LABEL_31 tl31TextDict = TLG_GET_TEXTURE_DICTIONARY_FOR_LEVEL(sTLGData.eCurrentLevel)
	REQUEST_STREAMED_TEXTURE_DICT(tl31TextDict)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl31TextDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {dsw} TLG_PROCESS_CLIENT_STATE_INIT_LEVEL - Loading ", tl31TextDict)
		EXIT
	ENDIF
	
	INT i
	
	i = 0
	REPEAT ciTLG_MAX_SCREENS i	
		sTLGData.iPropToStopOn[i] = -1
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_CLIENT_STATE_INIT_LEVEL] {DSW} sTLGData.iPropToStopOn[", i, "] = ", sTLGData.iPropToStopOn[i])
	ENDREPEAT
	
	
	
	TLG_INIT_COVER_POINTS(sTLGData.eCurrentLevel)
	TLG_INIT_ENEMIES()
	TLG_INIT_HOSTAGES()
	TLG_INIT_PLAYERS()
	TLG_INIT_BACKGROUND(sTLGData.eCurrentLevel)
	TLG_INIT_COLLECTIBLE_POINTS()
	TLG_INIT_PLAY_VARIABLES()
	TLG_INIT_SOUND()
	TLG_RESET_LOCAL_PLAYER_SCORE_FOR_CURRENT_LEVEL()
	
	TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_MENUS)
	TLG_START_AUDIO_SCENE(TLG_AUDIO_SCENE_GAMEPLAY)
	
//	TLG_PLAY_SOUND_NO_POSITION(TLG_AUDIO_EFFECT_GET_READY)
	
	TLG_HANDLE_PLAYER_WEAPON_COCKED_AUDIO()
	
	sTLGData.iLocalLevelRunningTime = GET_GAME_TIMER()
//	sTLGData.iLevelBitset[ENUM_TO_INT(sTLGData.eCurrentLevel)] = 0
	sTLGData.iLevelCurrentStage[ENUM_TO_INT(sTLGData.eCurrentLevel)] = 0
	sTLGData.iTotalLevelShots[sTLGData.eCurrentLevel] = 0
	sTLGData.iTotalLevelEnemyHits[sTLGData.eCurrentLevel] = 0
	sTLGData.iTotalLevelEnemyHeadShots[sTLGData.eCurrentLevel] = 0
	sTLGData.iTotalPowerupsCollected[sTLGData.eCurrentLevel] = 0
	sTLGData.iStaggeredOnScreenViableCoverCount = 0
	TLG_RESET_SCREEN_TO_STOP_ON()
	stlgData.iCurrentScreen = 1
	stlgData.fRightMostCoverX = 99999999.0
	sTLGData.iRightMostCover = -1
	REPEAT ciTLG_MAX_LANES i
		sTLGData.iCurrentActiveEnemies[i] = 0
		sTLGData.iTimeSinceEnemyActive[i] = 0
	ENDREPEAT
	
	sTLGData.iResultsScreenTimer = 0
	sTLGData.iResultsScreenTimeout = 0
	sTLGData.iReturnToMenuTimeout = 0
	
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_SHOULD_STOP)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_STOPPED)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_DONE_RESULTS_SCREEN_AUDIO)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_ADDED_ON_ACCURACY_BONUS)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_ADDED_ON_HOSTAGE_BONUS) 
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_PLAYED_DEATH_SCREEN_AUDIO)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_PLAYED_HOSTAGE_SCORE_AUDIO)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_PLAYED_DEATH_SCREEN_AUDIO)
	TLG_CLEAR_LEVEL_BIT_FOR_LEVEL(sTLGData.eCurrentLevel, TLG_LEVELS_BIT_PLAYED_RESULTS_DEAD_AUDIO)
	
	IF TLG_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT()) > 0
		TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_REPLAYING_LEVEL)
	ENDIF
	
	CLEAR_BIT(sTLGData.iMusicBitset,  ENUM_TO_INT(TLG_MUSIC_BIT_STARTED_TITLE))
	CLEAR_BIT(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_STOPPED))
	CLEAR_BIT(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_DEATH))
	ARCADE_GAMES_HELP_TEXT_CLEAR()
	TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_PLAYING)
	
	TLG_PREPARE_MUSIC_EVENT(TLG_MUSIC_EVENT_DEATH)
	TLG_TRIGGER_MUSIC_EVENT(TLG_GET_MUSIC_EVENT_FOR_LEVEL(TLG_GET_CURRENT_LEVEL()))
ENDPROC

// Process PLAYING
PROC TLG_PROCESS_CLIENT_STATE_PLAYING()
	
	TLG_HANDLE_ENEMIES()
	
	TLG_HANDLE_THROWN_ENEMY_PROJECTILES()
	
	TLG_HANDLE_HOSTAGES()
	
	TLG_HANDLE_PLAYER_SHOOTING(0)
	
	TLG_HANDLE_PLAYER_RELOADING()
	
	TLG_HANDLE_PLAYER_MOVEMENT()
	
	TLG_HANDLE_PLAYER_ABILITIES()
	
	TLG_HANDLE_FX()

	TLG_CHECK_PLAYER_PROGRESS()
	
	TLG_DRAW_CLIENT_STATE_PLAYING()
	
	TLG_HANDLE_LEVEL_INTRO_AUDIO()
	
	TLG_MAINTAIN_HELP_TEXT()
	
#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BadlandsReturnToMenuInGame")	
		TLG_CHECK_PLAYER_RETURNING_TO_MENU(TRUE)
	ENDIF
#ENDIF

ENDPROC

FUNC BOOL TLG_IS_SCORE_POOR_FOR_CURRENT_LEVEL()
	INT iScore = TLG_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT())
	
	SWITCH TLG_GET_CURRENT_LEVEL()
		CASE TLG_LEVEL_DESERT
			RETURN iScore < 1000
		BREAK
		
		CASE TLG_LEVEL_TOWN
			RETURN iScore < 1000
		BREAK
		
		CASE TLG_LEVEL_FOREST
			RETURN iScore < 1000
		BREAK
		
		CASE TLG_LEVEL_MINE
			RETURN iScore < 1000
		BREAK
		
		CASE TLG_LEVEL_GRAVEYARD
			RETURN iScore < 1000
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL TLG_IS_SCORE_AVERAGE_FOR_CURRENT_LEVEL()
	INT iScore = TLG_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT())
	
	SWITCH TLG_GET_CURRENT_LEVEL()
		CASE TLG_LEVEL_DESERT
			RETURN iScore < 1500
		BREAK
		
		CASE TLG_LEVEL_TOWN
			RETURN iScore < 1500
		BREAK
		
		CASE TLG_LEVEL_FOREST
			RETURN iScore < 1500
		BREAK
		
		CASE TLG_LEVEL_MINE
			RETURN iScore < 1500
		BREAK
		
		CASE TLG_LEVEL_GRAVEYARD
			RETURN iScore < 1500
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TLG_IS_SCORE_GOOD_FOR_CURRENT_LEVEL()
	INT iScore = TLG_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT())
	
	SWITCH TLG_GET_CURRENT_LEVEL()
		CASE TLG_LEVEL_DESERT
			RETURN iScore < 2000
		BREAK
		
		CASE TLG_LEVEL_TOWN
			RETURN iScore < 2000
		BREAK
		
		CASE TLG_LEVEL_FOREST
			RETURN iScore < 2000
		BREAK
		
		CASE TLG_LEVEL_MINE
			RETURN iScore < 2000
		BREAK
		
		CASE TLG_LEVEL_GRAVEYARD
			RETURN iScore < 2000
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TLG_IS_SCORE_AMAZING_FOR_CURRENT_LEVEL()
	INT iScore = TLG_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(TLG_GET_CURRENT_LEVEL_AS_INT())
	
	SWITCH TLG_GET_CURRENT_LEVEL()
		CASE TLG_LEVEL_DESERT
			RETURN iScore >= 2000
		BREAK
		
		CASE TLG_LEVEL_TOWN
			RETURN iScore >= 2000
		BREAK
		
		CASE TLG_LEVEL_FOREST
			RETURN iScore >= 2000
		BREAK
		
		CASE TLG_LEVEL_MINE
			RETURN iScore >= 2000
		BREAK
		
		CASE TLG_LEVEL_GRAVEYARD
			RETURN iScore >= 2000
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC TLG_INIT_GOTO_LEADERBOARD()
	INT i
	FOR i = 0 TO ciTLG_MAX_BACKGROUND_ELEMS - 1
		sTLGData.sBackgroundData[i].bRendering = FALSE
	ENDFOR
	
	TLG_RESET_ALL_COVER()
	
	sTLGData.iNumberOfBackgroundElements = 0

	TLG_INIT_BACKGROUND(TLG_LEVEL_DESERT)
	TLG_INIT_COVER_POINTS(TLG_LEVEL_DESERT)
ENDPROC
// Process RESULT SCREEN
PROC TLG_PROCESS_CLIENT_STATE_RESULT_SCREEN()
	BOOL bDoFinalScene
	
	IF sTLGData.iResultsScreenTimer = 0
		sTLGData.iResultsScreenTimer = GET_GAME_TIMER()
	ENDIF
	IF TLG_ARE_ALL_LEVELS_COMPLETE() AND NOT sTLGData.bPlayedOutro
		bDoFinalScene = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_badlandsForceOutro")
			bDoFinalScene = TRUE
		ENDIF
	#ENDIF
	
	TLG_MAINTAIN_HELP_TEXT()
	
	TLG_DRAW_CLIENT_STATE_RESULT_SCREEN(bDoFinalScene)
	
	BOOL bShouldGoToLeaderboard
//	INT i
	
	IF sTLGData.sPlayerData[0].fHealth <= 0.0
		bShouldGoToLeaderboard = TRUE
		IF NOT IS_BIT_SET(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_DEATH))
			TLG_TRIGGER_MUSIC_EVENT(TLG_MUSIC_EVENT_DEATH)
			SET_BIT(sTLGData.iMusicBitset, ENUM_TO_INT(TLG_MUSIC_BIT_DEATH))
		ENDIF
		
	//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_CLIENT_STATE_RESULT_SCREEN] {DSW} bShouldGoToLeaderboard = TRUE Player health: ", sTLGData.sPlayerData[0].fHealth)
	ELSE
		//IF TLG_ARE_ALL_LEVELS_COMPLETE() AND NOT sTLGData.bPlayedOutro
		IF bDoFinalScene
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR (GET_GAME_TIMER() - sTLGData.iResultsScreenTimer ) > 10000
				DG_START_MOVIE(sTLGData.bmIdTlgOutro, "TLG_Outro")
				TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_OUTRO_MOVIE)
			ENDIF
		ELSE
			TLG_CHECK_PLAYER_RETURNING_TO_MENU(FALSE, TRUE, DEFAULT, TRUE)
		ENDIF
		
		//-- Audio for results screen
		IF NOT TLG_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_DONE_RESULTS_SCREEN_AUDIO)
			IF TLG_IS_SCORE_POOR_FOR_CURRENT_LEVEL()
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SCORE_BAD)
			ELIF TLG_IS_SCORE_AVERAGE_FOR_CURRENT_LEVEL()
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SCORE_AVERAGE)
			ELIF TLG_IS_SCORE_GOOD_FOR_CURRENT_LEVEL()
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SCORE_GOOD)
			ELIF TLG_IS_SCORE_AMAZING_FOR_CURRENT_LEVEL()
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SCORE_AMAZING)
			ENDIF
			
			TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_DONE_RESULTS_SCREEN_AUDIO)
		ENDIF
	ENDIF
	

	
	IF bShouldGoToLeaderboard
		IF TLG_SHOULD_SKIP_LEADERBOARD()
			TLG_CHECK_PLAYER_RETURNING_TO_MENU(FALSE, TRUE, DEFAULT, TRUE)
		ELSE
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR (GET_GAME_TIMER() - sTLGData.iResultsScreenTimer ) > 10000
				TLG_INIT_GOTO_LEADERBOARD()
				TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_EDIT_LEADERBOARD)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TLG_PROCESS_CLIENT_STATE_OUTRO_MOVIE()
	TLG_DRAW_MENU_BACKGROUND()
	
	FLOAT fRatioWidth = 1.0 * GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	FLOAT fRatioHeight = 1.0
	BOOL bMovieFinished = DG_DRAW_MOVIE(sTLGData.bmIdTlgOutro, fRatioWidth, fRatioHeight)
	
	TWS_DRAW_FRONT_FX()
	
	IF NOT bMovieFinished
		//CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [TLG_PROCESS_CLIENT_STATE_OUTRO_MOVIE] Movie Time: ", GET_BINK_MOVIE_TIME(sTLGData.bmIdTlgOutro))
		IF NOT sTLGData.bPlayedOutroAudio
			IF GET_BINK_MOVIE_TIME(sTLGData.bmIdTlgOutro) >= 76.0
				TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_FINISH_GAME)
				sTLGData.bPlayedOutroAudio = TRUE
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	sTLGData.bPlayedOutro = TRUE
	
	IF NOT TLG_SHOULD_SKIP_LEADERBOARD()
		TLG_INIT_GOTO_LEADERBOARD()
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_EDIT_LEADERBOARD)
	ELSE
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_TITLE_SCREEN)
	ENDIF
ENDPROC

PROC TLG_PROCESS_CLIENT_STATE_VIEW_LEADERBOARD()
	TLG_DRAW_CLIENT_STATE_LEADERBOARD()
	
	TLG_CHECK_PLAYER_RETURNING_TO_MENU(TRUE, TRUE, TRUE)
ENDPROC

PROC TLG_PROCESS_CLIENT_STATE_EDIT_LEADERBOARD()
	TLG_TRIGGER_TITLE_MUSIC()
	INT iPlayerScore
//	INT iPlayerScore = 50000//TLG_GET_LOCAL_PLAYER_TOTAL_SCORE()
//	playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardScore = iPlayerScore

	IF playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardScore != TLG_GET_LOCAL_PLAYER_TOTAL_SCORE()
		playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardScore = TLG_GET_LOCAL_PLAYER_TOTAL_SCORE()
	ENDIF
	
	iPlayerScore = playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardScore
	BOOL bServerReady = IS_BIT_SET(tlgServerBd.iSortLbForPart, PARTICIPANT_ID_TO_INT())
	
	//ARCADE_GAMES_LEADERBOARD_PROCESS_ENTRY(sTLGData.sLbData, tlgServerBd.sLeaderboard, playerBd, playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardInitials, iPlayerScore, bServerReady)
	ARCADE_GAMES_LEADERBOARD_PROCESS_ENTRY(sTLGData.sLbData, tlgServerBd.sLeaderboard, playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardInitials, iPlayerScore, bServerReady)
	
	TLG_DRAW_CLIENT_STATE_LEADERBOARD()
	
	IF NOT sTLGData.sLbData.bEditing
		TLG_SEND_TELEMETRY()
		TLG_RESET_LOCAL_PLAYER_SCORE_FOR_CURRENT_LEVEL()
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_VIEW_LEADERBOARD)
	ENDIF
ENDPROC

// Process CLEANUP
PROC TLG_CLEANUP_AND_EXIT_CLIENT()
	
	CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_CLEANUP_AND_EXIT_CLIENT] - Cleaning up and terminating the script")
	
	TLG_CANCEL_MUSIC_EVENT(TLG_MUSIC_EVENT_DEATH)
	TLG_TRIGGER_MUSIC_EVENT(TLG_MUSIC_EVENT_STOP)
	
	//TLG_SEND_TELEMETRY()
	
	sTLGData.sArcadeAnimData.bHoldLastFrame = FALSE
	sTLGData.sArcadeAnimData.bLoopAnim = FALSE 
	IF sTLGData.ePlayerPedAnimState = TLG_PLAYER_PED_ANIM_IDLE
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_CLEANUP_AND_EXIT_CLIENT] - ARCADE_CABINET_ANIM_CLIP_EXIT")
		PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_EXIT) 
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_CLEANUP_AND_EXIT_CLIENT] - ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM")
		PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM)
	ENDIF
	ARCADE_CABINET_COMMON_CLEANUP()
	
	ARCADE_GAMES_HELP_TEXT_CLEAR()
	
	TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_MENUS)
	TLG_STOP_AUDIO_SCENE(TLG_AUDIO_SCENE_GAMEPLAY)
	DG_CLEAN_UP_MOVIE(sTLGData.bmIdTlgIntro) 
	DG_CLEAN_UP_MOVIE(sTLGData.bmIdTlgOutro) 
	//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sTLGData.tl31BaseTextDict)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

FUNC ARCADE_CABINET_ANIM_CLIPS TLG_GET_PLAY_IDLE_CLIP_FROM_COUNT(INT iCount)
	ARCADE_CABINET_ANIM_CLIPS ePlayIdle
	SWITCH iCount
		CASE 0		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE			BREAK
		CASE 1		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2		BREAK
		CASE 2		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V3		BREAK
		CASE 3		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V4		BREAK
	ENDSWITCH
	
	RETURN ePlayIdle
ENDFUNC

FUNC BOOL TLG_SHOULD_PLAY_NEXT_PLAY_IDLE_ANIM()
	IF sTLGData.iIdleAnimCount = -1
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_SHOULD_PLAY_NEXT_PLAY_IDLE_ANIM]  True - iIdleAnimCount: ", sTLGData.iIdleAnimCount)
		RETURN TRUE
	ENDIF
	
	INT iCurrent = sTLGData.iIdleAnimCount - 1
	IF iCurrent < 0
		iCurrent = 0
	ENDIF
	ARCADE_CABINET_ANIM_CLIPS ePlayIdle = TLG_GET_PLAY_IDLE_CLIP_FROM_COUNT(sTLGData.iIdleAnimCount)
	
	IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ePlayIdle)
		IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ePlayIdle)
			CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_SHOULD_PLAY_NEXT_PLAY_IDLE_ANIM]  True - Previous idle finished COunt:: ", iCurrent)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC TLG_PROCESS_PLAYER_PED_ANIMS()
	/*
	url:bugstar:6022461
	PLAY_ARCADE_CABINET_ANIMATION(eAnimEvent, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LEVEL_COMPLETE) 
PLAY_ARCADE_CABINET_ANIMATION(eAnimEvent, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LOSE_LIFE) 
	PLAY_ARCADE_CABINET_ANIMATION(eAnimEvent, ARCADE_CABINET_ANIM_CLIP_WIN) 
	PLAY_ARCADE_CABINET_ANIMATION(eAnimEvent, ARCADE_CABINET_ANIM_CLIP_LOSE)
	*/ 
	ARCADE_CABINET_ANIM_CLIPS ePlayIdle
	SWITCH sTLGData.ePlayerPedAnimState
		//-- Idle (stand)
		CASE TLG_PLAYER_PED_ANIM_IDLE
			IF sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_LEVEL_SELECTION_MENU
				sTLGData.sArcadeAnimData.bHoldLastFrame = TRUE
				sTLGData.sArcadeAnimData.bLoopAnim = FALSE
				sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
				sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
				PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_IDLE_TO_AIM) 
				
				TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_IDLE_TO_AIM)
			ENDIF
		BREAK
		
		//-- Idle (stand) -> Aiming
		CASE TLG_PLAYER_PED_ANIM_IDLE_TO_AIM
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_IDLE_TO_AIM)
				IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_IDLE_TO_AIM)
					sTLGData.sArcadeAnimData.bHoldLastFrame = FALSE
					sTLGData.sArcadeAnimData.bLoopAnim = TRUE
					sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
					sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
					PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_AIM_IDLE) 
					
					sTLGData.iIdleAnimCount = -1
					TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE)
				ELSE
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_PLAYER_PED_ANIMS] {DSW} Waiting for HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED - ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_IDLE_TO_AIM")
				ENDIF
			ENDIF
		BREAK
		
		//-- Idle (Aiming) -> Idle (playing)
		CASE TLG_PLAYER_PED_ANIM_AIM_IDLE
			IF sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_PLAYING
				IF TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0 
				AND TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() > 0
					IF TLG_SHOULD_PLAY_NEXT_PLAY_IDLE_ANIM()
						sTLGData.iIdleAnimCount++
						
						IF sTLGData.iIdleAnimCount > 3
							sTLGData.iIdleAnimCount = 0
						ENDIF
						
												
						ePlayIdle = TLG_GET_PLAY_IDLE_CLIP_FROM_COUNT(sTLGData.iIdleAnimCount)
						SWITCH sTLGData.iIdleAnimCount
							CASE 0		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE			BREAK
							CASE 1		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2		BREAK
							CASE 2		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V3		BREAK
							CASE 3		ePlayIdle = ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V4		BREAK
						ENDSWITCH
					
					
						CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PROCESS_PLAYER_PED_ANIMS] {DSW} Starting play_idle, var: ",  sTLGData.iIdleAnimCount)
						
						
						sTLGData.sArcadeAnimData.fBlendInDelta = REALLY_SLOW_BLEND_IN
						sTLGData.sArcadeAnimData.fBlendOutDelta = REALLY_SLOW_BLEND_OUT
						PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ePlayIdle) 
						sTLGData.bDoneReactionAnim = FALSE	
				//		TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_PLAY_IDLE)
					ENDIF
				ENDIF

			ENDIF
			
			IF NOT sTLGData.bDoneReactionAnim
				IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.0
					sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
					sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
					sTLGData.sArcadeAnimData.bLoopAnim = FALSE
					sTLGData.sArcadeAnimData.bHoldLastFrame = TRUE
					PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LOSE_LIFE)
					
					TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_LOSE_LIFE)
					sTLGData.bDoneReactionAnim = TRUE
				ELSE 
					IF sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_PLAYING
					OR sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_RESULT_SCREEN
						IF (TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0 AND TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0)

							sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
							sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
							sTLGData.sArcadeAnimData.bLoopAnim = FALSE
							sTLGData.sArcadeAnimData.bHoldLastFrame = TRUE
							
							IF TLG_ARE_ALL_LEVELS_COMPLETE()
							AND NOT sTLGData.bPlayedWinningAnim
								sTLGData.bPlayedWinningAnim = TRUE
								PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_WIN)
								
								TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_COMPLETE_GAME)
							ELSE
								PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LEVEL_COMPLETE)
								
								TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_COMPLETE_STAGE)

							ENDIF
							
							sTLGData.bDoneReactionAnim = TRUE 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
//		CASE TLG_PLAYER_PED_ANIM_PLAY_IDLE
//			IF TLG_GET_LOCAL_PLAYER_HEALTH() <= 0.0
//				sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
//				sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
//				sTLGData.sArcadeAnimData.bLoopAnim = FALSE
//				sTLGData.sArcadeAnimData.bHoldLastFrame = TRUE
//				PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LOSE_LIFE)
//				
//				TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_LOSE_LIFE)
//			ELSE 
//				IF sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_PLAYING
//				OR sTLGData.eClientState = TLG_ARCADE_CLIENT_STATE_RESULT_SCREEN
//					IF (TLG_GET_LOCAL_PLAYER_HEALTH() > 0.0 AND TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE() = 0)
//
//						sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
//						sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
//						sTLGData.sArcadeAnimData.bLoopAnim = FALSE
//						sTLGData.sArcadeAnimData.bHoldLastFrame = TRUE
//						
//						IF TLG_ARE_ALL_LEVELS_COMPLETE()
//						AND NOT sTLGData.bPlayedWinningAnim
//							sTLGData.bPlayedWinningAnim = TRUE
//							PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_WIN)
//							
//							TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_COMPLETE_GAME)
//						ELSE
//							PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LEVEL_COMPLETE)
//							
//							TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_COMPLETE_STAGE)
//
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//		BREAK
		
		//-- Idle (Playing) -> Lose life
		CASE TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_LOSE_LIFE
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LOSE_LIFE)
				IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LOSE_LIFE)
					sTLGData.sArcadeAnimData.bHoldLastFrame = FALSE
					sTLGData.sArcadeAnimData.bLoopAnim = TRUE
					sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
					sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
					PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_AIM_IDLE) 
					sTLGData.iIdleAnimCount = -1
					TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE)
				ENDIF
			ENDIF
		BREAK
		
		//-- Idle (Aiming) -> Complete stage
		CASE TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_COMPLETE_STAGE
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LEVEL_COMPLETE)
				IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LEVEL_COMPLETE)
						sTLGData.sArcadeAnimData.fBlendInDelta = SLOW_BLEND_IN
						sTLGData.sArcadeAnimData.fBlendOutDelta = SLOW_BLEND_OUT
						sTLGData.sArcadeAnimData.bHoldLastFrame = FALSE
						sTLGData.sArcadeAnimData.bLoopAnim = TRUE
						
						PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_AIM_IDLE) 
						sTLGData.iIdleAnimCount = -1
						TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_AIM_IDLE)
					
				ENDIF
			ENDIF
		BREAK
		
		//-- Idle (Playing) -> Complete game
		CASE TLG_PLAYER_PED_ANIM_AIM_IDLE_TO_COMPLETE_GAME
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_WIN)
				IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_WIN)
					IF sTLGData.eClientState != TLG_ARCADE_CLIENT_STATE_PLAYING
						sTLGData.sArcadeAnimData.bHoldLastFrame = FALSE
						sTLGData.sArcadeAnimData.bLoopAnim = TRUE
						sTLGData.sArcadeAnimData.fBlendInDelta = REALLY_SLOW_BLEND_IN
						sTLGData.sArcadeAnimData.fBlendOutDelta = REALLY_SLOW_BLEND_OUT
						PLAY_ARCADE_CABINET_ANIMATION(sTLGData.sArcadeAnimData, ARCADE_CABINET_ANIM_CLIP_IDLE) 
						
						TLG_SET_PLAYER_PED_ANIM_STATE(TLG_PLAYER_PED_ANIM_IDLE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC TLG_PROCESS_CLIENT_STATE_MACHINE()
	
	SWITCH sTLGData.eClientState
		
		CASE TLG_ARCADE_CLIENT_STATE_INIT
		
			TLG_PROCESS_CLIENT_STATE_INIT()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_REQUESTING_ASSETS
		
			TLG_PROCESS_CLIENT_STATE_REQUESTING_ASSETS()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_FIB
			TLG_PROCESS_CLIENT_STATE_FIB()
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_INTRO
			
			TLG_PROCESS_CLIENT_INTRO()
			
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_TITLE_SCREEN
		
			TLG_PROCESS_CLIENT_STATE_TITLE_SCREEN()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_LEVEL_SELECTION_MENU
		
			TLG_PROCESS_CLIENT_STATE_MENU()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_INIT_LEVEL
		
			TLG_PROCESS_CLIENT_STATE_INIT_LEVEL()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_PLAYING
			/*
			#IF IS_DEBUG_BUILD
				INT i
			*/	
				/*
					On PC, ~ 1 sec ~38 frames ~ 0.028170 ms avg frame time
					Middle cover (3) 600.0 -> 452 = 148 px
					
					Middle Enemy: (5) 2600.0 -> 1945.0 = 555.0 px
					
					Background enemy: (19) 2500 -> 1760.0 = 740.0 px
					
					On Ps4 debug ~ 1 sec ~20 frames ~ 0.053854s avg frame time
					Middle cover (3): 600 -> 524 = 76 px
					Middle ebemy (5): 2500 -> 2230 = 270 px
					Background enemy (19): 2500 -> 2140  = 360 px
					
				
				*/
				/*
				CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Start of frame. Frame time: ", GET_FRAME_TIME())
				IF sTLGData.iDebugTimer	= 0
					sTLGData.iDebugTimer = GET_GAME_TIMER()
					CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Started timer")
					
					REPEAT ciTLG_MAX_COVER i
						IF sTLGData.iDebugCover = -1
							IF sTLGData.sCoverData[i].iCoverLane = ciTLG_LANE_MIDDLE
								CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using cover ", i, " Start pos x: ", sTLGData.sCoverData[i].vSpriteInitialPos.x)
								sTLGData.iDebugCover = i
							ENDIF
						ENDIF
					ENDREPEAT
					
					REPEAT ciTLG_MAX_ENEMIES i
						IF sTLGData.sEnemyData[i].iEnemyType != TLG_OUTHOUSE_ENEMY  
							IF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_BACKGROUND
								IF stlgData.iDebugBackgroundEnemy = -1
									IF NOT sTLGData.sEnemyData[i].bReverse
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Background enemy ", i, " Start pos x: ", sTLGData.sEnemyData[i].vStartPos.x) 
										stlgData.iDebugBackgroundEnemy  = i
									ENDIF
								ENDIF
							ELIF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_MIDDLE
								IF stlgData.iDebugMiddleEnemy = -1
									IF NOT sTLGData.sEnemyData[i].bReverse
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Middle enemy ", i, " Start pos x: ", sTLGData.sEnemyData[i].vStartPos.x) 
										stlgData.iDebugMiddleEnemy = i
									ENDIF
								ENDIF
							ELIF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_FOREGROUND
								IF stlgData.iDebugForegroundEnemy = -1
									IF NOT sTLGData.sEnemyData[i].bReverse
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Foreground enemy ", i, " Start pos x: ", sTLGData.sEnemyData[i].vStartPos.x) 
										stlgData.iDebugForegroundEnemy = i
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Time Passed: ", GET_GAME_TIMER() - sTLGData.iDebugTimer)
					
					REPEAT ciTLG_MAX_COVER i
						IF i =  sTLGData.iDebugCover
							IF sTLGData.sCoverData[i].iCoverLane = ciTLG_LANE_MIDDLE
								CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using cover ", i, " Current Pos: ", sTLGData.sCoverData[i].vSpritePos.x, " X Diff: ", sTLGData.sCoverData[i].vSpritePos.x - sTLGData.sCoverData[i].vSpriteInitialPos.x ," Start pos x: ", sTLGData.sCoverData[i].vSpriteInitialPos.x)
						
							ENDIF
						ENDIF
					ENDREPEAT
					
					
					REPEAT ciTLG_MAX_ENEMIES i
						IF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_BACKGROUND
							IF i =  sTLGData.iDebugBackgroundEnemy
								IF NOT sTLGData.sEnemyData[i].bReverse
									IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Background enemy ", i, " Current Pos: ", sTLGData.sEnemyData[i].vSpritePos.x, " Diff from start: ", sTLGData.sEnemyData[i].vSpritePos.x - sTLGData.sEnemyData[i].vStartPos.x, " Start pos x: ", sTLGData.sEnemyData[i].vStartPos.x) 
								
									ELSE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Background enemy ", i, " NO LONGER ACTIVE")
									ENDIF
								ENDIF
							ENDIF
						ELIF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_MIDDLE
							IF i =  sTLGData.iDebugMiddleEnemy
								IF NOT sTLGData.sEnemyData[i].bReverse
									IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Middle enemy ", i, " Current Pos: ", sTLGData.sEnemyData[i].vSpritePos.x, " Diff from start: ", sTLGData.sEnemyData[i].vSpritePos.x - sTLGData.sEnemyData[i].vStartPos.x, " Start pos x: ", sTLGData.sEnemyData[i].vStartPos.x) 
										
									ELSE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Middle enemy ", i, " NO LONGER ACTIVE")
									ENDIF
								ENDIF
							ENDIF
						ELIF sTLGData.sEnemyData[i].iEnemyLane = ciTLG_LANE_FOREGROUND
							IF i =  sTLGData.iDebugForegroundEnemy
								IF NOT sTLGData.sEnemyData[i].bReverse
									IF sTLGData.sEnemyData[i].eState = TLG_ACTIVE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Foreground enemy ", i, " Current Pos: ", sTLGData.sEnemyData[i].vSpritePos.x, " Diff from start: ", sTLGData.sEnemyData[i].vSpritePos.x - sTLGData.sEnemyData[i].vStartPos.x, " Start pos x: ", sTLGData.sEnemyData[i].vStartPos.x) 
									
									ELSE
										CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] Using Foreground enemy ", i, " NO LONGER ACTIVE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				
				
				
			#ENDIF
			*/
			
			TLG_PROCESS_CLIENT_STATE_PLAYING()
			
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
					TLG_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(TLG_LEVELS_BIT_SHOULD_STOP)
			//		TLG_INCREASE_CURRENT_LEVEL_STAGE()
				ENDIF
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_BadlandsOnScreen")
					TLG_DO_ON_SCREEN_DEBUG()
				ENDIF
			#ENDIF
			
			/*
			#IF IS_DEBUG_BUILD
				sTLGData.iDebugFrameCount++
				sTLGData.fDebugTotalFrameTime += GET_FRAME_TIME()
				CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST] End of frame. Total frames: ", sTLGData.iDebugFrameCount, " Average: ", sTLGData.fDebugTotalFrameTime / (TO_FLOAT(sTLGData.iDebugFrameCount)))
				CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [UPDATE_TEST]")
				
				
			#ENDIF
			*/
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_RESULT_SCREEN
		
			TLG_PROCESS_CLIENT_STATE_RESULT_SCREEN()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_OUTRO_MOVIE
			TLG_PROCESS_CLIENT_STATE_OUTRO_MOVIE()
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_EDIT_LEADERBOARD
		
			TLG_PROCESS_CLIENT_STATE_EDIT_LEADERBOARD()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_VIEW_LEADERBOARD
		
			TLG_PROCESS_CLIENT_STATE_VIEW_LEADERBOARD()
		
		BREAK
		
		CASE TLG_ARCADE_CLIENT_STATE_CLEANUP
		
			TLG_CLEANUP_AND_EXIT_CLIENT()
		
		BREAK
		
	ENDSWITCH
	
	
	//-- Handle player ped anims while playing
	TLG_PROCESS_PLAYER_PED_ANIMS()
ENDPROC

#IF IS_DEBUG_BUILD
PROC TLG_DEBUG_OUTPUT_ALL_ENEMY_POSITIONS()
	INT i
	CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [OUTPUT_ENEMY_POSITIONS] ")
	FOR i = 0 TO ciTLG_MAX_ENEMIES  -1
		IF sTLGData.sEnemyData[i].eState != TLG_INACTIVE
			CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [OUTPUT_ENEMY_POSITIONS] Enemy ", i, " lane:", sTLGData.sEnemyData[i].iEnemyLane, " Type: ", TLG_GET_ENEMY_TYPE_AS_STRING(sTLGData.sEnemyData[i].iEnemyType), " State: ", TLG_GET_ENEMY_STATE_AS_STRING(sTLGData.sEnemyData[i].eState) , " Active in stage: ", sTLGData.sEnemyData[i].iLevelStage)
			CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [OUTPUT_ENEMY_POSITIONS] Reverse? ", sTLGData.sEnemyData[i].bReverse, " Pos: ", sTLGData.sEnemyData[i].vSpritePos.x, ", ", sTLGData.sEnemyData[i].vSpritePos.y, " Stopping: ", sTLGData.sEnemyData[i].fStoppingX, " Start: ", sTLGData.sEnemyData[i].vStartPos.x, ", ", sTLGData.sEnemyData[i].vStartPos.y)
			CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [OUTPUT_ENEMY_POSITIONS] Ever on screen? ", sTLGData.sEnemyData[i].bAppearedOnscreen)
			CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [OUTPUT_ENEMY_POSITIONS]")
		ENDIF
	ENDFOR
ENDPROC

PROC TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS()
	INT i
	CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] ")
	
	/*
	sTLGData.sItemData[iItem].bRendering = FALSE
	sTLGData.sItemData[iItem].iMinActiveStage = 0
	sTLGData.sItemData[iItem].iRenderAfterWarpCount = 0
	sTLGData.sItemData[iItem].iWarpCount = 0
	sTLGData.sItemData[iItem].bShot = FALSE
	*/
	FOR i = 0 TO ciTLG_MAX_ITEMS  -1
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "]..eItemType = ", TLG_GET_COLLECTIBLE_TYPE_AS_STRING(sTLGData.sItemData[i].eItemType))
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].bRendering = ", sTLGData.sItemData[i].bRendering) 
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].vSpritePos = ", sTLGData.sItemData[i].vSpritePos.x, ", ", sTLGData.sItemData[i].vSpritePos.y)
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].iMinActiveStage = ", sTLGData.sItemData[i].iMinActiveStage)
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].iRenderAfterWarpCount = ", sTLGData.sItemData[i].iRenderAfterWarpCount)
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].iMinActiveStage = ", sTLGData.sItemData[i].iMinActiveStage)
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].iWarpCount = ", sTLGData.sItemData[i].iWarpCount)
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] sTLGData.sItemData[", i, "].bShot = ", sTLGData.sItemData[i].bShot)
		CDEBUG1LN(DEBUG_MINIGAME, "{dsw} [TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS] ")
		
	ENDFOR
ENDPROC
PROC TLG_DEBUG_SET_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)

	sTLGData.widgetGroupExample = parentWidgetGroup
	
ENDPROC
	
PROC TLG_DEBUG_CREATE_WIDGETS()
	
	IF sTLGData.bDebugCreatedWidgets
		EXIT
	ENDIF
	
	IF sTLGData.widgetGroupExample = NULL
		EXIT
	ENDIF
	
	
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] [DSW] Creating widgets...")
	SET_CURRENT_WIDGET_GROUP(sTLGData.widgetGroupExample)
	START_WIDGET_GROUP("TLG Arcade Widgets")
		ADD_WIDGET_INT_READ_ONLY("iRectsDrawnThisFrame", iRectsDrawnThisFrame)
		ADD_WIDGET_INT_READ_ONLY("iSpritesDrawnThisFrame", iSpritesDrawnThisFrame)
		ADD_WIDGET_INT_READ_ONLY("iDebugLinesDrawnThisFrame", iDebugLinesDrawnThisFrame)
		ADD_WIDGET_FLOAT_READ_ONLY("vSpriteMarkerPos X", sTLGData.sPlayerData[0].vSpriteMarkerPos.x)
		ADD_WIDGET_FLOAT_READ_ONLY("vSpriteMarkerPos Y", sTLGData.sPlayerData[0].vSpriteMarkerPos.y)
		ADD_WIDGET_FLOAT_READ_ONLY("Prop X", sTLGData.sCoverData[8].vSpritePos.x)
		ADD_WIDGET_FLOAT_READ_ONLY("Prop X", sTLGData.sCoverData[9].vSpritePos.x)
		ADD_WIDGET_FLOAT_READ_ONLY("Prop X", sTLGData.sCoverData[10].vSpritePos.x)
		ADD_WIDGET_FLOAT_READ_ONLY("Enemy X", sTLGData.sEnemyData[0].vSpritePos.x)
//		ADD_WIDGET_FLOAT_SLIDER("Reticule Screen Sp X", sTLGData.fDebugEntityWorldSpaceX, -cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_WIDTH, 0.5)
//		ADD_WIDGET_FLOAT_SLIDER("Reticule Screen Sp Y", sTLGData.fDebugEntityWorldSpaceY, -cfGAME_SCREEN_HEIGHT, cfGAME_SCREEN_HEIGHT, 0.5)
		START_WIDGET_GROUP("Texture Pos")
		ADD_WIDGET_FLOAT_SLIDER("Texture 1 X", sTLGData.fTexturePosX[0], -10000.0, 10000.0, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("Texture 1 Y", sTLGData.fTexturePosY[0], -10000.0, 10000.0, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("Texture 1 Scale", sTLGData.fTextureScale[0], 0.0, 1.0, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("Texture 2 X", sTLGData.fTexturePosX[1], -10000.0, 10000.0, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("Texture 2 Y", sTLGData.fTexturePosY[1], -10000.0, 10000.0, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("Texture 3 X", sTLGData.fTexturePosX[2], -10000.0, 10000.0, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("Texture 3 Y", sTLGData.fTexturePosY[2], -10000.0, 10000.0, 0.5)
		ADD_WIDGET_INT_SLIDER("Value", sTLGDATA.iDebugIntegerValue,0, 100000, 1)
		ADD_WIDGET_FLOAT_SLIDER("Scale",sTLGData.fDebugFloatValue, -10.0, 10.0, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Enemy Hover")
			ADD_WIDGET_BOOL("Enable Enemy Hover", sTLGData.bEnableEnemyHover)
			ADD_WIDGET_FLOAT_READ_ONLY("Marker World Px Pos X", sTLGData.sPlayerData[0].vSpriteMarkerPos.x)
			ADD_WIDGET_FLOAT_READ_ONLY("Marker World Px Pos Y", sTLGData.sPlayerData[0].vSpriteMarkerPos.y)
			ADD_WIDGET_FLOAT_READ_ONLY("Reticule-Entity Dist", sTLGData.fReticuleSpriteDist)
			ADD_WIDGET_INT_READ_ONLY("Entity ID", sTLGData.iDebugEntityId )
			ADD_WIDGET_INT_READ_ONLY("Entity Type", sTLGData.iDebugType )
			ADD_WIDGET_INT_READ_ONLY("Entity State", sTLGData.iDebugEntityState )
			ADD_WIDGET_FLOAT_READ_ONLY("Entity stopping X", sTLGData.fEntityStoppingX)
			ADD_WIDGET_INT_READ_ONLY("Lane", sTLGData.iDebugLane) 
			ADD_WIDGET_INT_READ_ONLY("Anim frame", sTLGData.iDebugAnimFrame)
			ADD_WIDGET_FLOAT_READ_ONLY("Entity World X", sTLGData.fDebugEntityWorldX)
			ADD_WIDGET_FLOAT_READ_ONLY("Entity World Y", sTLGData.fDebugEntityWorldY)
//			ADD_WIDGET_FLOAT_READ_ONLY("Entity TL X", sTLGData.fDebugEntityCornerX[0])
//			ADD_WIDGET_FLOAT_READ_ONLY("Entity TL Y", sTLGData.fDebugEntityCornerY[0])
//			ADD_WIDGET_FLOAT_READ_ONLY("Entity BR X", sTLGData.fDebugEntityCornerX[1])
//			ADD_WIDGET_FLOAT_READ_ONLY("Entity BR Y", sTLGData.fDebugEntityCornerY[1])
//			ADD_WIDGET_FLOAT_READ_ONLY("Ent-Ret TL X Diff", sTLGData.fDebugReticuleEntityTLDiffX)
//			ADD_WIDGET_FLOAT_READ_ONLY("Ent-Ret TL Y Diff", sTLGData.fDebugReticuleEntityTLDiffY)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Enemy Projectiles")
			ADD_WIDGET_BOOL("Launch Projectile", sTLGData.bDebugLaunchDynamite)
			ADD_WIDGET_BOOL("Clear Projectile", sTLGData.bDebugClearDynamite)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("My Scores")
			ADD_WIDGET_INT_READ_ONLY("Desert ", sTLGData.iMyScores[0])
			ADD_WIDGET_INT_READ_ONLY("Town ", sTLGData.iMyScores[1])
			ADD_WIDGET_INT_READ_ONLY("Forest ", sTLGData.iMyScores[2])
			ADD_WIDGET_INT_READ_ONLY("Mine ", sTLGData.iMyScores[3])
			ADD_WIDGET_INT_READ_ONLY("Graveyard ", sTLGData.iMyScores[4])
			ADD_WIDGET_INT_READ_ONLY("Successive Hits", sTLGData.sPlayerData[0].iSuccessiveHits)
			ADD_WIDGET_INT_READ_ONLY("Chain", sTLGData.sPlayerData[0].iChainedScore)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Level") 
			ADD_WIDGET_INT_READ_ONLY("Stage", sTLGData.iLevelCurrentStage[TLG_GET_CURRENT_LEVEL()])
			ADD_WIDGET_INT_READ_ONLY("# of Stages", sTLGData.iDebugTotalStages)
			ADD_WIDGET_INT_READ_ONLY("Hostages Saved", sTLGData.iTotalLevelHostagesSaved[TLG_GET_CURRENT_LEVEL()])
			ADD_WIDGET_INT_READ_ONLY("Shots", sTLGData.iTotalLevelShots[TLG_GET_CURRENT_LEVEL()])
			ADD_WIDGET_INT_READ_ONLY("Enemy Kills", sTLGData.iTotalLevelEnemyHits[TLG_GET_CURRENT_LEVEL()])
			ADD_WIDGET_INT_READ_ONLY("HeadShots", sTLGData.iTotalLevelEnemyHeadShots[TLG_GET_CURRENT_LEVEL()])
			ADD_WIDGET_INT_READ_ONLY("Birds", sTLGData.iTotalLevelBirdsShot[TLG_GET_CURRENT_LEVEL()])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Enemies Count")
			ADD_WIDGET_INT_READ_ONLY("Lane 0 Active ", sTLGData.iCurrentActiveEnemies[0])
			ADD_WIDGET_INT_READ_ONLY("Lane 1 Active ", sTLGData.iCurrentActiveEnemies[1])
			ADD_WIDGET_INT_READ_ONLY("Lane 2 Active ", sTLGData.iCurrentActiveEnemies[2])
			ADD_WIDGET_INT_READ_ONLY("Total rem ", sTLGData.iDebugEnemiesRemaining)
			ADD_WIDGET_BOOL("Output all pos", sTLGData.bDebugOutputAliveEnemies)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Background") 
			ADD_WIDGET_BOOL("Stop moving", sTLGData.bStopMovingLevel)
			ADD_WIDGET_BOOL("Hide Facade", sTLGData.bDisableFacade)
			ADD_WIDGET_BOOL("Shift props left", sTLGData.bDebugShiftPropsLeft)
			ADD_WIDGET_BOOL("Shift props right", sTLGData.bDebugShiftPropsRight)
			ADD_WIDGET_FLOAT_READ_ONLY("Tile 0 X", sTLGData.sBackgroundTilesData[0].vSpritePos.x)
			ADD_WIDGET_FLOAT_READ_ONLY("Tile 1 X", sTLGData.sBackgroundTilesData[1].vSpritePos.x)
//			ADD_WIDGET_FLOAT_READ_ONLY("Tile 2 X", sTLGData.sBackgroundTilesData[2].vSpritePos.x)
//			ADD_WIDGET_FLOAT_READ_ONLY("Tile 3 X", sTLGData.sBackgroundTilesData[3].vSpritePos.x)
//			ADD_WIDGET_FLOAT_READ_ONLY("Tile 4 X", sTLGData.sBackgroundTilesData[4].vSpritePos.x)
//			ADD_WIDGET_FLOAT_READ_ONLY("Tile 5 X", sTLGData.sBackgroundTilesData[5].vSpritePos.x)
//			ADD_WIDGET_FLOAT_READ_ONLY("Tile 6 X", sTLGData.sBackgroundTilesData[6].vSpritePos.x)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Props / Cover")  
			ADD_WIDGET_BOOL("Enable Hover", sTLGData.bEnableCoverHover)
			ADD_WIDGET_BOOL("Stop moving", sTLGData.bStopMovingLevel)
			ADD_WIDGET_INT_READ_ONLY("Index", sTLGDATA.iDebugIntegerValue)
			sTLGData.twDebugId = ADD_TEXT_WIDGET("Prop Type") 
			ADD_WIDGET_FLOAT_READ_ONLY("Prop X", sTLGData.fDebugEntityWorldX)
			ADD_WIDGET_FLOAT_READ_ONLY("Prop Y", sTLGData.fDebugEntityWorldY)
			ADD_WIDGET_INT_READ_ONLY("Prop Lane", sTLGData.iDebugLane)
			ADD_WIDGET_INT_READ_ONLY("Prop Screen", sTLGData.iDebugCoverScreen)
			ADD_WIDGET_FLOAT_READ_ONLY("Prop Scale", sTLGData.fDebugEntityScale)
			ADD_WIDGET_BOOL("Edit", sTLGData.bDebugEnableEdit)
			ADD_WIDGET_BOOL("Hide Foreground", sTLGData.bDebugHideForegroundProps)
			ADD_WIDGET_INT_READ_ONLY("Editing ID", sTLGData.iEntityEdit)
			sTLGData.twDebugId = ADD_TEXT_WIDGET("Editing Type") 
			ADD_WIDGET_FLOAT_SLIDER("Pos X", sTLGData.fTexturePosX[0], -10000.0, 10000.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Pos Y", sTLGData.fTexturePosY[0], -10000.0, 10000.0, 0.5)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Controls")
//			ADD_WIDGET_FLOAT_READ_ONLY("Stick X", sTLGData.fDebugStickX)
//			ADD_WIDGET_FLOAT_READ_ONLY("Stick Y", sTLGData.fDebugStickX)
//			ADD_WIDGET_FLOAT_READ_ONLY("Mouse X", sTLGData.fDebugMouseX)
//			ADD_WIDGET_FLOAT_READ_ONLY("Mouse Y", sTLGData.fDebugMouseY)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Awards")
			ADD_WIDGET_BOOL("Award Deadeye T-Shirt", sTLGData.bDebugAwardDeadeye)
			ADD_WIDGET_INT_SLIDER("Sharshooter award", sTLGData.iDebugSharpShooter, 0, 50, 1)
			ADD_WIDGET_BOOL("Set birds shot", sTLGData.bDebugAwardSetSharpshooter)
			ADD_WIDGET_BOOL("Award Sharpshooter T-Shirt", sTLGData.bDebugAwardSharpshooter)
			ADD_WIDGET_BOOL("Award Pistols at dawn T-Shirt", sTLGData.bDebugAwardPistols)
			ADD_WIDGET_BOOL("Award Complete T-Shirt", sTLGData.bDebugAwardComplete)
			ADD_WIDGET_BOOL("Award Complete Trophy", sTLGData.bDebugAwardTrophy)
			ADD_WIDGET_BOOL("Clear stats", sTLGData.bClearAwardStats)
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Pickups")
			ADD_WIDGET_BOOL("Enable", sTLGData.bDebugEnablePickup)
			ADD_WIDGET_INT_SLIDER("Show Screen Pickups", sTLGData.iDebugPickupScreen, -1, ciTLG_MAX_SCREENS, 1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Update")
			ADD_WIDGET_INT_SLIDER("Base FPS", ciBASE_FPS, 0, 1000, 1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Server BD")
			ADD_WIDGET_INT_READ_ONLY("Number of players ", tlgServerBd.iNumPlayers)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	ARCADE_GAMES_LEADERBOARD_WIDGET_CREATE(sTLGData.sLbData)
	
	ARCADE_GAMES_POSTFX_WIDGET_CREATE()
	
	
	CLEAR_CURRENT_WIDGET_GROUP(sTLGData.widgetGroupExample)
	
	sTLGData.fTexturePosX[0] = 1000.0 //cfBASE_SCREEN_WIDTH/6*2
	sTLGData.fTexturePosY[0] =  315.0 //152.500
	sTLGData.fTextureScale[0] = 1.0 
	sTLGData.fTexturePosX[1] = cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_X 
	sTLGData.fTexturePosY[1] = cfTLG_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_Y
	sTLGData.fTexturePosX[2] = 500.0
	sTLGData.fTexturePosY[2] = 500.0
	
	sTLGDATA.iDebugIntegerValue = 7250
	sTLGDATA.fDebugFloatValue = 1.0
	
	sTLGData.iDebugCover = -1
	stlgData.iDebugForegroundEnemy = -1
	stlgData.iDebugMiddleEnemy = -1
	stlgData.iDebugBackgroundEnemy = -1
	
	sTLGData.bDebugCreatedWidgets = TRUE

ENDPROC

PROC TLG_UPDATE_WIDGETS()
	INT i
	
	IF PARTICIPANT_ID_TO_INT() >= 0
		
		IF sTLGData.iMyScores[sTLGData.iStaggeredDebugCount] != playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[sTLGData.iStaggeredDebugCount]
			sTLGData.iMyScores[sTLGData.iStaggeredDebugCount] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[sTLGData.iStaggeredDebugCount]
		ENDIF
		
	ENDIF
	
	sTLGData.iDebugEnemiesRemaining = TLG_GET_TOTAL_NUMBER_OF_ENEMIES_ALIVE()
	sTLGData.iDebugTotalStages = TLG_GET_CURRENT_LEVEL_NUMBER_OF_STAGES()
//	VECTOR_2D vScreenSpace = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(INIT_VECTOR_2D(sTLGData.sPlayerData[0].vSpriteMarkerPos.x, sTLGData.sPlayerData[0].vSpriteMarkerPos.y))
//	sTLGData.fDebugEntityWorldSpaceX = vScreenSpace.x
//	sTLGData.fDebugEntityWorldSpacey = vScreenSpace.y
	
	
	VECTOR_2D vReticule = TLG_GET_PLAYER_RETICULE_POSITON()
	FLOAT distanceX, distanceY, fDistance, fDistanceToCheck
	BOOL bIsHovering
	IF sTLGData.bEnableEnemyHover
		FOR i = 0 TO ciTLG_MAX_ENEMIES  -1

			IF (sTLGData.sEnemyData[i].eState = TLG_ACTIVE OR sTLGData.sEnemyData[i].eState = TLG_SHOOTING) AND sTLGData.sEnemyData[i].iHealth > 0
				distanceX = vReticule.x - sTLGData.sEnemyData[i].vSpritePos.x
				distanceY = vReticule.y - sTLGData.sEnemyData[i].vSpritePos.y
				fDistance = SQRT(distanceX * distanceX + distanceY * distanceY)
				
				IF (sTLGData.sEnemyData[i].iEnemyLane = 0 OR sTLGData.sEnemyData[i].iEnemyLane = 1)
					fDistanceToCheck = 120.0
				ELSE
					fDistanceToCheck = 320.0 // 280.0
				ENDIF
				
				IF fDistance < fDistanceToCheck
					sTLGData.fReticuleSpriteDist = fDistance
					sTLGData.iDebugEntityId = i
					sTLGData.iDebugType = ENUM_TO_INT(sTLGData.sEnemyData[i].iEnemyType)
					sTLGData.iDebugLane = sTLGData.sEnemyData[i].iEnemyLane
					sTLGData.iDebugEntityState  = ENUM_TO_INT(sTLGData.sEnemyData[i].eState)
					sTLGData.fEntityStoppingX = sTLGData.sEnemyData[i].fStoppingX
					sTLGData.fDebugEntityWorldX = sTLGData.sEnemyData[i].vSpritePos.x
					sTLGData.fDebugEntityWorldY = sTLGData.sEnemyData[i].vSpritePos.Y
					
					sTLGData.iDebugAnimFrame = sTLGData.sEnemyData[i].iSpriteAnimFrame
					
					VECTOR_2D vCorner0 = INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSpritePos.x - (sTLGData.sEnemyData[i].vSize.x/2), sTLGData.sEnemyData[i].vSpritePos.y - (sTLGData.sEnemyData[i].vSize.y/2))
					VECTOR_2D vCorner1 = INIT_VECTOR_2D(sTLGData.sEnemyData[i].vSpritePos.x + (sTLGData.sEnemyData[i].vSize.x/2), sTLGData.sEnemyData[i].vSpritePos.y + (sTLGData.sEnemyData[i].vSize.y/2)) 
					
					sTLGData.fDebugEntityCornerX[0] = vCorner0.x
					sTLGData.fDebugEntityCornerY[0] = vCorner0.y
					sTLGData.fDebugEntityCornerX[1] = vCorner1.x
					sTLGData.fDebugEntityCornerY[1] = vCorner1.y
					
					sTLGData.fDebugReticuleEntityTLDiffX = vReticule.x - vCorner0.x
					sTLGData.fDebugReticuleEntityTLDiffY = vReticule.y - vCorner0.y
					bIsHovering = TRUE
				ENDIF
			ENDIF
		ENDFOR
		
		IF NOT bIsHovering
			sTLGData.iDebugEntityId = -1
			sTLGData.fDebugEntityWorldX = -1.0
			sTLGData.fDebugEntityWorldY = -1.0
			sTLGData.fDebugEntityCornerX[0] = -1.0
			sTLGData.fDebugEntityCornerY[0] = -1.0
			sTLGData.fDebugEntityCornerX[1] = -1.0
			sTLGData.fDebugEntityCornerY[1] = -1.0
			sTLGData.iDebugLane = -1
			sTLGData.fReticuleSpriteDist = -1.0
			sTLGData.fDebugReticuleEntityTLDiffX = -1.0
			sTLGData.fDebugReticuleEntityTLDiffY = -1.0
		ENDIF
	ENDIF
	STRING sCover
	IF sTLGData.bEnableCoverHover
		INT iClosest = TLG_GET_CLOSEST_PROP_TO_COORD(vReticule)
		IF iClosest > -1
			sCover = TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[iClosest].eType)
			sTLGData.fDebugEntityWorldX = sTLGData.sCoverData[iClosest].vSpritePos.x
			sTLGData.fDebugEntityWorldY = sTLGData.sCoverData[iClosest].vSpritePos.Y
			sTLGData.iDebugLane = sTLGData.sCoverData[iClosest].iCoverLane
			sTLGDATA.iDebugIntegerValue = iClosest
			sTLGData.iDebugCoverScreen = sTLGData.sCoverData[iClosest].iScreen
			sTLGData.fDebugEntityScale = TLG_GET_COVER_TYPE_SCALE_FOR_LANE(sTLGData.sCoverData[iClosest].eType, sTLGData.sCoverData[iClosest].iCoverLane)
		ELSE
			sCover = TLG_GET_ASSET_TYPE_AS_STRING(TLG_ASSET_INVALID)
			sTLGData.fDebugEntityWorldX = -9999999.0
			sTLGData.fDebugEntityWorldY = -9999999.0
			sTLGDATA.iDebugIntegerValue = -1
		ENDIF
		SET_CONTENTS_OF_TEXT_WIDGET(sTLGData.twDebugId, sCover)
	ENDIF
	
	IF sTLGData.bDebugEnableEdit
	AND sTLGData.bEnableCoverHover
		IF NOT sTLGData.bDebugInitEdit
		AND sTLGDATA.iDebugIntegerValue > -1
			sTLGData.iEntityEdit = sTLGDATA.iDebugIntegerValue
			sTLGData.bDebugInitEdit = TRUE
			sTLGData.fTexturePosX[0] = sTLGData.sCoverData[sTLGData.iEntityEdit].vSpritePos.x
			sTLGData.fTexturePosY[0] = sTLGData.sCoverData[sTLGData.iEntityEdit].vSpritePos.y
			sTLGData.bDebugHideForegroundProps = TRUE
		ELIF sTLGData.bDebugInitEdit
			sCover = TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[sTLGData.iEntityEdit].eType)
			SET_CONTENTS_OF_TEXT_WIDGET(sTLGData.twDebugId, sCover)
			sTLGData.sCoverData[sTLGData.iEntityEdit].vSpritePos = GET_DEBUG_TEXTURE_POS(0)
		ENDIF
	ELSE
		IF sTLGData.bDebugInitEdit
			sTLGData.bDebugInitEdit = FALSE
			sTLGData.iEntityEdit = -1
		ENDIF
	ENDIF
	
	IF sTLGData.bDebugHideForegroundProps
		 sTLGData.bDebugHideForegroundProps = FALSE
		i = 0
		REPEAT ciTLG_MAX_COVER i
			IF sTLGData.sCoverData[i].bRendering
				IF sTLGData.sCoverData[i].iCoverLane = 2
					sTLGData.sCoverData[i].bRendering = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF sTLGData.bDebugShiftPropsLeft
		sTLGData.bDebugShiftPropsLeft = FALSE
		i = 0
		REPEAT ciTLG_MAX_COVER i
			IF sTLGData.sCoverData[i].bRendering
				sTLGData.sCoverData[i].vSpritePos.x -= (cfBASE_SCREEN_WIDTH / 2.0)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF sTLGData.bDebugShiftPropsRight
		sTLGData.bDebugShiftPropsRight = FALSE
		i = 0
		REPEAT ciTLG_MAX_COVER i
			IF sTLGData.sCoverData[i].bRendering
				sTLGData.sCoverData[i].vSpritePos.x += (cfBASE_SCREEN_WIDTH / 2.0)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF sTLGData.bDebugOutputAliveEnemies
		sTLGData.bDebugOutputAliveEnemies = FALSE
		
		TLG_DEBUG_OUTPUT_ALL_ENEMY_POSITIONS()
	ENDIF
	
	IF sTLGData.bDebugAwardDeadeye
		sTLGData.bDebugAwardDeadeye = FALSE
		TLG_AWARD_DEADEYE()
	ENDIF
	
	IF sTLGData.bDebugAwardPistols
		sTLGData.bDebugAwardPistols = FALSE
		TLG_AWARD_PISTOLS_AT_DAWN()
	ENDIF
	
	IF sTLGData.bDebugAwardSharpshooter
		sTLGData.bDebugAwardSharpshooter = FALSE
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_BADLANDS_SHARPSHOOTER)
	ENDIF
	
	IF sTLGData.bDebugAwardSetSharpshooter
		sTLGData.bDebugAwardSetSharpshooter = FALSE
		sTLGData.iTotalLevelBirdsShot[1] = sTLGData.iDebugSharpShooter
		IF sTLGData.iDebugSharpShooter >= TLG_GET_SHARPSHOOTER_AWARD_PLATINUM_TARGET_SCORE()
			TLG_AWARD_SHARPSHOOTER(sTLGData.iDebugSharpShooter, TRUE)
		ELSE
			TLG_AWARD_SHARPSHOOTER(sTLGData.iDebugSharpShooter, FALSE)
		ENDIF
	ENDIF
	IF sTLGData.bDebugAwardComplete
		sTLGData.bDebugAwardComplete = FALSE
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_BADLANDS_COMPLETE)
	ENDIF
	
	IF sTLGData.bDebugAwardTrophy
		sTLGData.bDebugAwardTrophy = FALSE
	//	ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_FORTUNE_TELLER, TRUE)
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_BADLANDS, TRUE)
	ENDIF
	
	IF sTLGData.bClearAwardStats 
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ARCADE_HELP_BITSET, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ARCADE_HELP_BITSETTWO, 0)
		SET_MP_INT_CHARACTER_AWARD(MP_AWARD_SHARPSHOOTER,0)
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_DEADEYE, FALSE)
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_PISTOLSATDAWN, FALSE)
		sTLGData.bClearAwardStats = FALSE
	ENDIF
	IF sTLGData.bDebugLaunchDynamite
		sTLGData.bDebugLaunchDynamite = FALSE
		TLG_INIT_THROWN_ENEMY_PROJECTILE(TLG_ENEMY_PROJECTILE_DYNAMITE, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH / 2.0, cfBASE_SCREEN_HEIGHT / 2.0))
	ENDIF
	
	IF sTLGData.bDebugClearDynamite
		sTLGData.bDebugClearDynamite = FALSE
		TLG_RESET_ALL_ENEMY_THROWN_PROJECTLES()
	ENDIF
	
	VECTOR_2D vInitialPos
	VECTOR_2D vOffset
	IF sTLGData.bDebugEnablePickup
		IF sTLGData.iDebugPickupScreen != -1
			TLG_GET_ITEM_INITIAL_POS_AND_OFFSET_FOR_SCREEN(TLG_HEALTH, sTLGData.iDebugPickupScreen, vInitialPos, vOffset)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "POWERUP_HEALTH",  vInitialPos, INIT_VECTOR_2D(cfTLG_ITEM_HEALTH_WIDTH, cfTLG_ITEM_HEALTH_HEIGHT), 0, sTLGData.rgbaSprite)
			
			TLG_GET_ITEM_INITIAL_POS_AND_OFFSET_FOR_SCREEN(TLG_RIFLE_AMMO, sTLGData.iDebugPickupScreen, vInitialPos, vOffset)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "ammo3",  vInitialPos, INIT_VECTOR_2D(cfTLG_SPRITE_HUD_PISTOL_AMMO_CLIP_WIDTH, cfTLG_SPRITE_HUD_PISTOL_AMMO_CLIP_HEIGHT), 0, sTLGData.rgbaSprite)
			
			TLG_GET_ITEM_INITIAL_POS_AND_OFFSET_FOR_SCREEN(TLG_SHOTGUN_AMMO, sTLGData.iDebugPickupScreen, vInitialPos, vOffset)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "ammo2",  vInitialPos, INIT_VECTOR_2D(cfTLG_SPRITE_HUD_SHOTGUN_AMMO_CLIP_WIDTH, cfTLG_SPRITE_HUD_SHOTGUN_AMMO_CLIP_HEIGHT), 0, sTLGData.rgbaSprite)
			
			TLG_GET_ITEM_INITIAL_POS_AND_OFFSET_FOR_SCREEN(TLG_MACHINE_GUN_AMMO, sTLGData.iDebugPickupScreen, vInitialPos, vOffset)
			ARCADE_DRAW_PIXELSPACE_SPRITE(sTLGData.tl31HudTextDict, "ammo4",  vInitialPos, INIT_VECTOR_2D(cfTLG_SPRITE_HUD_AMMO_WIDTH, cfTLG_SPRITE_HUD_AMMO_HEIGHT), 0, sTLGData.rgbaSprite)
		ENDIF
	ELSE
		IF sTLGData.iDebugPickupScreen != -1
			sTLGData.iDebugPickupScreen = -1
		ENDIF
	ENDIF
//	sTLGData.fDebugStickX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
//	sTLGData.fDebugStickY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
//	sTLGData.fDebugMouseX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
//	sTLGData.fDebugMouseY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	sTLGData.iStaggeredDebugCount++
	IF sTLGData.iStaggeredDebugCount >= ENUM_TO_INT(TLG_MAX_LEVELS)
		sTLGData.iStaggeredDebugCount = 0
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_CTRL, "Debug Rifle")
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_RIFLE)
		TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_RIFLE)
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_2, KEYBOARD_MODIFIER_CTRL, "Debug Shotgun") 
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_SHOTGUN)
		TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_SHOTGUN)
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_3, KEYBOARD_MODIFIER_CTRL, "Debug Gatling") 
		TLG_PLAY_SOUND_AND_RELEASE_NO_POSITION(TLG_AUDIO_EFFECT_PLAYER_GATLING_GUN)
		TLG_SET_PLAYER_SPECIAL_WEAPON(TLG_PLAYER_WEAPON_GATLING)
	ENDIF
	
	ARCADE_GAMES_POSTFX_WIDGET_UPDATE()
	
	ARCADE_GAMES_LEADERBOARD_WIDGET_UPDATE(sTLGData.sLbData, playerBd, tlgServerBd.sLeaderboard)
ENDPROC

/// PURPOSE:
///    Dumps the current state of the game to the logs
///    Automatically called when a bug is entered
PROC TLG_DEBUG_DUMP_DATA_TO_LOGS()

	ARCADE_CABINET_DUMP_DATA_TO_CONSOLE()

	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] {dsw} TLG_DEBUG_DUMP_DATA_TO_LOGS - Dumping data...")
	
	//General
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] ----- GENERAL -----")
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] eClientState = ", TLG_DEBUG_GET_TLG_ARCADE_CLIENT_STATE_AS_STRING(sTLGData.eClientState))
	
	TLG_DEBUG_OUTPUT_ALL_ENEMY_POSITIONS()
	
	TLG_DEBUG_OUTPUT_ALL_PICKUP_POSITIONS()
	
ENDPROC

PROC TLG_DEBUG_PROCESSING()

	TLG_DEBUG_CREATE_WIDGETS()
	
	//If a bug is entered dump all data to the console.
	IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)
		TLG_DEBUG_DUMP_DATA_TO_LOGS()
	ENDIF
	
ENDPROC

#ENDIF

/// PURPOSE:
///    Run every frame before the client state machine is processed
PROC TLG_PRE_UPDATE()
	
	IF sTLGData.eClientState >= TLG_ARCADE_CLIENT_STATE_CLEANUP
		// Cleaning up, don't do anything
		EXIT
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND() 
	OR SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
		TLG_SET_LOCAL_STATE(TLG_ARCADE_CLIENT_STATE_CLEANUP)
		EXIT
	ENDIF
	
	ARCADE_GAMES_HELP_TEXT_PRE_UPDATE()
	
	ARCADE_GAMES_LEADERBOARD_PROCESS_EVENTS(sTLGData.sLbData, tlgServerBd.sLeaderboard, playerBd, tlgServerBd.bLeaderboardRecieved)
	
	ARCADE_GAMES_LEADERBOARD_LOAD(sTLGData.sLbData, CASINO_ARCADE_GAME_TLG, tlgServerBd.bLeaderboardRecieved)
	
	TLG_UPDATE_ANIM_FRAMES()
	
	BOOL bIsPlaying = sTLGData.eClientState >= TLG_ARCADE_CLIENT_STATE_INTRO
	
	INT i
	IF bIsPlaying
	
		//-- Construct an array of all the onscreen cover locations which enemy peds can use to hide behind
		//-- Peds can loop through this to see if there's any nearby cover.
		//-- Avoids having every enemy loop through every cover location, every frame.
		IF sTLGData.iStaggeredOnScreenViableCoverCount = 0
			sTLGData.iNumberOfViableOnScreenCoverPoints = 0
			
			REPEAT ciTLG_MAX_COVER i
				IF sTLGData.iNumberOfViableOnScreenCoverPoints < ciTLG_MAX_SUITABLE_HIDE_BEHIND_COVER
					IF TLG_IS_COVER_ASSET_TYPE_SUITABLE_FOR_COVER(sTLGData.sCoverData[i].eType)
						IF TLG_IS_COVER_ON_SCREEN(i)
//							#IF IS_DEBUG_BUILD
//							CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PRE_UPDATE] [TLG_IS_COVER_SUITABLE_FOR_ENEMY_TO_USE] [dsw] ")
//							CDEBUG1LN(DEBUG_MINIGAME, "[TLG_PRE_UPDATE] [TLG_IS_COVER_SUITABLE_FOR_ENEMY_TO_USE] [dsw] Adding cover ", i, " To viable cover locations. Type: ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[i].eType), " Tot Viable: ", sTLGData.iNumberOfViableOnScreenCoverPoints + 1  )
//							#ENDIF
							sTLGData.iViableOnScreenEnemyCoverPoints[sTLGData.iNumberOfViableOnScreenCoverPoints] = i
							sTLGData.iNumberOfViableOnScreenCoverPoints++
							
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	ARCADE_CABINET_COMMON_EVERY_FRAME_PROCESSING(bIsPlaying)
	
	TLG_SET_CURRENT_TIMESTEP()
ENDPROC
	
/// PURPOSE:
///    Run every frame after the client state machine is processed
PROC TLG_POST_UPDATE()
	
	//Hide help text every frame unless this minigame wants to show some
	//See x:\gta5\script\dev_ng\shared\include\public\invade_persuade_main.sch IAP_SHOULD_ALLOW_HELP_TEXT_THIS_FRAME()
	
	IF NOT ARCADE_GAMES_HELP_TEXT_SHOULD_ALLOW_THIS_FRAME()
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
	
	IF sTLGData.eClientState >= TLG_ARCADE_CLIENT_STATE_PLAYING
		IF sTLGData.iRightMostCover >= 0
			IF sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen > stlgData.iCurrentScreen
				stlgData.iCurrentScreen = sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_POST_UPDATE] {DSW} Current screen updated to ", stlgData.iCurrentScreen, " as right most cover ", sTLGData.iRightMostCover, " of type ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[sTLGData.iRightMostCover].eType), " is for screen ", sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen)
				#ENDIF
			ELIF sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen = 1 AND stlgData.iCurrentScreen = 4
				stlgData.iCurrentScreen = sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_POST_UPDATE] {DSW} Current screen updated to ", stlgData.iCurrentScreen, " as right most cover ", sTLGData.iRightMostCover, " of type ", TLG_GET_ASSET_TYPE_AS_STRING(sTLGData.sCoverData[sTLGData.iRightMostCover].eType), " is for screen ", sTLGData.sCoverData[sTLGData.iRightMostCover].iScreen)
				#ENDIF
			ENDIF
		ENDIF
		
		sTLGData.iStaggeredOnScreenViableCoverCount++
		IF sTLGData.iStaggeredOnScreenViableCoverCount >= sTLGData.iNumberOfViableOnScreenCoverPoints
			sTLGData.iStaggeredOnScreenViableCoverCount = 0
		//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_POST_UPDATE] {DSW} Resetting sTLGData.iStaggeredOnScreenViableCoverCount, num viable: ", sTLGData.iNumberOfViableOnScreenCoverPoints)
		ENDIF
	ENDIF
	//HIDE_HELP_TEXT_THIS_FRAME()
	
ENDPROC

/// PURPOSE:
///    Call every frame to run the minigame
PROC PROCESS_TLG_ARCADE()
	
	TLG_PRE_UPDATE()
	
	TLG_PROCESS_CLIENT_STATE_MACHINE()
	
	TLG_POST_UPDATE()
	
	#IF IS_DEBUG_BUILD
		TLG_DEBUG_PROCESSING()
	#ENDIF
	
ENDPROC
