//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_cam_public.sch																		//
// Description: Enums, structs, consts, and look-up tables for the turret_cam system.						//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		12/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "types.sch"
USING "weapon_enums.sch"
USING "MP_globals_script_timers.sch"
USING "commands_pad.sch"
USING "commands_shapetest.sch"
USING "turret_cam_public_def.sch"

#IF IS_DEBUG_BUILD
USING "commands_debug.sch"
#ENDIF

/// PURPOSE: Resource names required for a turret cam.
///   	These should derived from config settings and 
///    	be set during CONFIG_LOAD.
STRUCT TURRET_CAM_RESOURCE_NAMES
	STRING sStreamedHudTex
	STRING sHudScaleformName
ENDSTRUCT

/// PURPOSE: Audio required for normal HUD implementations.
///    Derived from TURRET_CAM_HUD_SOUNDS config.
STRUCT TURRET_CAM_AUDIO_HUD
	STRING sAudioBank
	STRING sHudAudioScene
	STRING sHudSoundSet
	
	STRING sPan
	STRING sZoom
	STRING sBackground
	
	STRING sTakeDamage
	STRING sLowHealthLoop
	
	INT iPanId
	INT iZoomId
	INT iBackgroundId
	
	INT iTakeDamageId 
	INT iLowHealthLoopId
ENDSTRUCT

/// PURPOSE: Audio required for normal weapon implementations.
///    Derived from TURRET_CAM_WEAPON_SOUNDS config.
STRUCT TURRET_CAM_AUDIO_WEAPON
	STRING sWeaponSoundSet	
	STRING sFire
	INT iFireId
ENDSTRUCT

/// PURPOSE: Audio required for turret cam.
///    Derived from config settings.
STRUCT TURRET_CAM_AUDIO_STRUCT
	TURRET_CAM_AUDIO_HUD hud
	TURRET_CAM_AUDIO_WEAPON weapon
ENDSTRUCT

/// PURPOSE: Used to store turret local config, args, and config derived data/resource handles.
STRUCT TURRET_SETTINGS
	TURRET_CAM_CONFIG cfg
	
	// Resource names (derived from config)
	TURRET_CAM_RESOURCE_NAMES res
	
	// Placement data
	VECTOR vTurretCoords
	VECTOR vTurretRotation

	// Parenting
	TURRET_CAM_ATTACH_TYPE eAttachType
	ENTITY_INDEX entParent
ENDSTRUCT

/// PURPOSE: Turret cam internal state enum
ENUM TURRET_CAM_STATE
	TCS_LOAD,
	TCS_UPDATE,
	
	TCS_COUNT
ENDENUM


/// PURPOSE: Lock-on missile consts (could be config settings in future if required)
CONST_FLOAT cf_TARGET_ACQUISITION_RANGE_2 	0.005 			// Screen space radius ^2 from centre after which potential targets are ignored.
CONST_FLOAT cf_TARGET_VALIDITY_RANGE_2 		0.04 			// Screen space radius ^2 from centre after which an aquired target is lost.
CONST_INT 	ci_TARGET_ACQUISITION_TIME_MS	2000			// Time (ms) after LOS confirmation before lockon is "complete".

/// PURPOSE: PFX impact scale while in turret
CONST_FLOAT cf_TURRET_CAM_FULL_AUTO_IMPACT_SCALE 1.5

STRUCT HOMING_MISSILE_DATA
	ENTITY_INDEX entShapetestHit		// Entity we have clear LOS to.
	SHAPETEST_INDEX shapetestId			// Id of ongoing LOS shapetest (0 = no test).
	SCRIPT_TIMER targetLockedTimer 		// This restarts when target is found and in LOS.
	ENTITY_INDEX entTargetAttempt 		// Check this entity for LOS.
	INT iAcquiringSoundId = -1
	INT iLockedSoundId = -1
ENDSTRUCT

STRUCT PILOTED_MISSILE_DATA
	BOOL bDoingMissileTransition		// Doing transition to missile cam?
	BOOL bRequestedMissileStart			// Have we asked the missile script to start?
	VECTOR vStartCoord					// Coord to fire missile from
	BOOL bLaunchedDroneScriptFromHere	// Did we launch the drone script from this script?
ENDSTRUCT

STRUCT FULL_AUTO_DATA
	SCRIPT_TIMER reloadTimer 	// duration of a reload
	INT iAmmoTime				// how long player has been shooting for
ENDSTRUCT

/// PURPOSE: Used for TURRET_CAM_LOCAL_DATA.iBsLocal
CONST_INT ci_TURRET_CAM_LOCAL_BS_LOAD_SCENE 							0 
CONST_INT ci_TURRET_CAM_LOCAL_BS_HIDE_HUD_THIS_FRAME	 				1
CONST_INT ci_TURRET_CAM_LOCAL_BS_FREEZE_CAM_THIS_FRAME					2
CONST_INT ci_TURRET_CAM_LCOAL_BS_SUPPRESS_HELP_THIS_FRAME				3
CONST_INT ci_TURRET_CAM_LOCAL_BS_DISABLED_PIM							4 // Did this script disable the interaction menu?
CONST_INT ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS						5 // Loaded PC input map?
CONST_INT ci_TURRET_CAM_LOCAL_BS_SUPPRESS_PC_CONTROLS_THIS_FRAME 		6 // Forcing input map to be disabled?
CONST_INT ci_TURRET_CAM_LOCAL_BS_SUPPRESS_TIMECYCLE_RESET_THIS_FRAME	7 // Don't reload the timecycle modifier even if there's a mismatch

STRUCT TURRET_CAM_TRANSFORM_DATA
	// Neither of these are updated if the attachement type is
	// TCAT_NONE so you can safely use these values in all calculations
	// without checking for parent...
	VECTOR vParentWorldCoords // Cached parent world coords 
	VECTOR vParentWorldRot	  // Cached parent world rotation 

	VECTOR vCamWorldCoords 
	VECTOR vCamWorldRot 
ENDSTRUCT

STRUCT TURRET_CAM_LOCAL_DATA
	// Camera
	CAMERA_INDEX turretCam
	VECTOR vRelativeRotation	// Relative offset to m_settings.vTurretRotation
	
	TURRET_CAM_TRANSFORM_DATA transformData

	// General
	TURRET_CAM_STATE eState
	
	// Weapon
	SCRIPT_TIMER lastShotTime
	FULL_AUTO_DATA fullAuto
	HOMING_MISSILE_DATA homingMissile
	PILOTED_MISSILE_DATA pilotedMissile
	
	// Rendering
	BOOL bHaveSetInstButtons
	SCALEFORM_INDEX hudScaleformId
	BOOL bHaveSetArenaHudWeaponType = FALSE // Arena Hud specific check

	// Minimap
	BLIP_INDEX blipForeground
	BLIP_INDEX blipBackground
	
	TURRET_CAM_AUDIO_STRUCT audio
	
	INT iTimecycleModId = -1
	BOOL bPopTimecycle = FALSE
	
	// Loading
	SCRIPT_TIMER loadSceneTimer
	
	// Uses ci_TURRET_CAM_LOCAL_BS_
	INT iBsLocal
	
	TURRET_CAM_DAMAGE_ACCUMULATOR damageTaken
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT ARENA_TURRET_DEBUG_DATA
	BOOL bCreatedWidgets
	INT iState
	BOOL bDrawFiringLines = FALSE
	VECTOR vFireStart
	VECTOR vFireEndPoint
	
	TEXT_WIDGET_ID tw_hudAudioBank
	TEXT_WIDGET_ID tw_hudSoundSet
	TEXT_WIDGET_ID tw_hudAudioScene
	TEXT_WIDGET_ID tw_hudAudioZoom
	TEXT_WIDGET_ID tw_hudAudioPan
	TEXT_WIDGET_ID tw_hudAudioBackground
	TEXT_WIDGET_ID tw_weaponAudioSoundSet
	TEXT_WIDGET_ID tw_weaponAudioFire
	
	INT iHomingMissileTargetEnt
	INT iHomingMissileShapetestEnt
	INT iHomingMissileShapetestId
ENDSTRUCT
#ENDIF // IS_DEBUG_BUILD
