//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Barclay					Date: 	8/11/2010		│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						    Drag Race Scripted Header							│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "Script_player.sch"
USING "script_maths.sch"


FLOAT fOffsetY
FLOAT fCarCamFOV
FLOAT fRevGuage
FLOAT fPointing_Cam_FOV
FLOAT fNew_Dest_FOV
FLOAT fNew_Init_FOV

INT GearShiftStage
INT i_cutscene_stage

VECTOR vInterp_From_Car_Cam_Coords
VECTOR vCrash_Cam_Init_Coords
VECTOR vCrash_Cam_Dest_Coords
VECTOR vPointing_Cam_Coords

BOOL bHandShakeCamRequired
BOOL bRoadShakeCamRequired
BOOL b_Is_Cutscene_Running
BOOL bshakeCamActivated
BOOL bForceApplied
BOOL bvehicleExploded

/// PURPOSE:
///    Attaches a racing camera to the players car during a race. 
/// PARAMS:
///    PlayerCar - The vehicle the player is going to be racing in.
///    DragCam- The camera to be attached.
///    FLOAT fOffsetX - Is the offset.x from the vehicle that the DragCam should be attached too.
///    FLOAT fMaxOffsetY - The maximum offset.y can go to from the vehicle that the DragCam should be attached too.
///    FLOAT fMinOffsetY - The minimym offset.y can go to from the vehicle that the DragCam should be attached too.
///    FLOAT fOffsetZ - Is the offset.z from the vehicle that the DragCam should be attached too.
///    FLOAT fMaxCamFOV - The maximum the camera can FOV out too.
///    FLOAT fMinCamFOV - The minimum the camera can FOV in too.
PROC ATTACH_DRAG_RACE_CAM(VEHICLE_INDEX PlayerCar, CAMERA_INDEX &DragCam, FLOAT fOffsetX, FLOAT fMaxOffsetY, FLOAT fMinOffsetY, FLOAT fOffsetZ, FLOAT fMaxCamFOV, FLOAT fMinCamFOV)
	IF IS_VEHICLE_DRIVEABLE(PlayerCar)
	
		fOffsetY = -((fMaxOffsetY* -1) + ((fMaxCamFOV-fCarCamFOV)/((fMaxCamFOV-fMinCamFOV)*100))*((fMaxOffsetY-fMinOffsetY)*100))		
		
		//General rule for fOffsetY
		IF fOffsetY > fMaxOffsetY
			fOffsetY = fMaxOffsetY
		ELIF fOffsetY < fMinOffsetY
			fOffsetY = fMinOffsetY
		ENDIF
		
		
		IF DOES_CAM_EXIST(DragCam)
			IF NOT IS_CAM_ACTIVE(DragCam)
				SET_CAM_ACTIVE(DragCam, TRUE)
			ENDIF	
			
			//Increment FOV if car is accelorating. 
			IF IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER2)
				PRINTSTRING("RIGHTSHOULDER2 BUTTON IS BEING PRESSED") PRINTNL()
				fCarCamFOV = fCarCamFOV + 0.4
			ELSE
				fCarCamFOV = fCarCamFOV - 1.5
			ENDIF
			
			//General rule for fcarCamFOV
			IF fCarCamFOV > fMaxCamFOV
				fCarCamFOV = fMaxCamFOV
			ELIF fCarCamFOV < fMinCamFOV
				fCarCamFOV = fMinCamFOV
			ENDIF			
			
			//PRINTSTRING("fCarCamFOV =") PRINTFLOAT(fCarCamFOV) PRINTNL()

			IF IS_VEHICLE_STOPPED(PlayerCar)
				IF bHandShakeCamRequired = TRUE
					SHAKE_CAM(DragCam, "HAND_SHAKE", 1)
					bHandShakeCamRequired = FALSE
					bRoadShakeCamRequired = TRUE
				ENDIF
			ELSE
				IF bRoadShakeCamRequired = TRUE	
					SHAKE_CAM(DragCam, "ROAD_VIBRATION_SHAKE", 2)
					bRoadShakeCamRequired = FALSE
					bHandShakeCamRequired = TRUE
				ENDIF
			ENDIF

			//ATTACH_CAM_TO_ENTITY(DragCam, PlayerCar, <<fOffsetX, fOffsetY, fOffsetZ>>, TRUE)
			IF fCarCamFOV > 0
				SET_CAM_FOV(DragCam, fCarCamFOV)
			ENDIF
			
		ELSE
			DragCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ATTACH_CAM_TO_ENTITY(DragCam, PlayerCar, <<fOffsetX, fOffsetY, fOffsetZ>>)
			POINT_CAM_AT_ENTITY(DragCam, PlayerCar,  <<0,0, 0>>)
			RENDER_SCRIPT_CAMS(TRUE,FALSE)
		ENDIF
		
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Draws a racing hud and guage on screen, for drag races. 
/// PARAMS:
///    PlayerCar - The vehicle the player is going to be racing in.
///    FLOAT fSpeedGear1 - The speed at which gear 1 changes.
///    FLOAT fSpeedGear1 - The speed at which gear 2 changes.
///    FLOAT fSpeedGear1 - The speed at which gear 3 changes.
///    FLOAT fSpeedGear1 - The speed at which gear 4 changes.
///    FLOAT fSpeedGear1 - The speed at which gear 5 changes.
///    FLOAT fSpeedGear1 - The speed at which gear 6 can go to.
PROC DRAW_RACING_HUD_FOR_VEHICLE(VEHICLE_INDEX PlayerCar, FLOAT fSpeedGear1, FLOAT fSpeedGear2, FLOAT fSpeedGear3, FLOAT fSpeedGear4, FLOAT fSpeedGear5, FLOAT fSpeedGear6limit)
	
	FLOAT playerVehSpeed
	IF IS_VEHICLE_DRIVEABLE(PlayerCar)
		playerVehSpeed = GET_ENTITY_SPEED(PlayerCar)
		IF  playerVehSpeed > 0
		AND playerVehSpeed < fSpeedGear1
			GearShiftStage = 0
		ELIF playerVehSpeed > fSpeedGear1
		AND playerVehSpeed < fSpeedGear2
			GearShiftStage = 1
		ELIF playerVehSpeed > fSpeedGear2
		AND playerVehSpeed < fSpeedGear3
			GearShiftStage = 2
		ELIF playerVehSpeed > fSpeedGear3
		AND playerVehSpeed < fSpeedGear4
			GearShiftStage = 3
		ELIF playerVehSpeed > fSpeedGear4
		AND playerVehSpeed < fSpeedGear5
			GearShiftStage = 4
		ELIF playerVehSpeed > fSpeedGear5
		AND playerVehSpeed < fSpeedGear6limit
			GearShiftStage = 5
		ENDIF
	
	
		SWITCH GearShiftStage
		
			CASE 0
			
				fRevGuage = (0.716 -((playerVehSpeed/(fSpeedGear1*1000))*354))	
				
			BREAK
			
			CASE 1
			
				fRevGuage = (0.680 -(((playerVehSpeed-12.8)/((fSpeedGear2-fSpeedGear1)*1000))*318))
				
			BREAK
			
			CASE 2

				fRevGuage = (0.640 -(((playerVehSpeed-17)/((fSpeedGear3-fSpeedGear2)*1000))*278))
				
			BREAK
			
			CASE 3

				fRevGuage = (0.583 -(((playerVehSpeed-23)/((fSpeedGear4-fSpeedGear3)*1000))*221))
				
			BREAK
			
			CASE 4

				fRevGuage = (0.553 -(((playerVehSpeed-31)/((fSpeedGear5-fSpeedGear4)*1000))*191))
				
			BREAK
		
			CASE 5

				fRevGuage = (0.517 -(((playerVehSpeed-41)/((fSpeedGear6limit-fSpeedGear5)*1000))*155))
				
			BREAK	
		ENDSWITCH
	ENDIF
	
	IF fRevGuage > 0.716 
		fRevGuage = 0.716
	ELIF fRevGuage < 0.362
		fRevGuage = 0.362
	ENDIF

	//Black Background Box
  	DRAW_RECT(0.108,0.539,0.031,0.364,0,0,0,255)
	//Red Box
  	DRAW_RECT(0.108,0.376,0.029,0.036,200,20,20,255)
	//Green Box
  	DRAW_RECT(0.108,0.409,0.028,0.036,20,200,20,255)
	//Orange Box Sweet Spot
  	DRAW_RECT(0.108,0.571,0.028,0.296,100,100,0,255)	
	
	//Draw Black Rev counter guage
  	DRAW_RECT(0.108,fRevGuage,0.030,0.006,0,0,0,255)
	
ENDPROC

/// PURPOSE:
///    Performs a crash cutscene on the specified players vehicle.
/// PARAMS:
///    PlayerCar - The vehicle the player is racing in.
///    CAMERA_INDEX CamAttachedToCar - The camera that is attached to the car during the race at the point of the crash.
///    CAMERA_INDEX InterpFromCarCam - The camera that will interp from the CamAttachedToCar.
///    CAMERA_INDEX CrashCamDest - The destination camera that the high up camera will interp to.
///    CAMERA_INDEX CrashCamInit - The initial camera that the high up camera will interp from.
///    CAMERA_INDEX PointingCam - The camera that will point at the vehicle during the interp.
PROC DRAG_RACE_CRASH_CUTSCENE(VEHICLE_INDEX PlayerCar, CAMERA_INDEX CamAttachedToCar, CAMERA_INDEX InterpFromCarCam, CAMERA_INDEX CrashCamDest, CAMERA_INDEX CrashCamInit, CAMERA_INDEX PointingCam)

		b_Is_Cutscene_Running = TRUE
		i_cutscene_stage = 0
		
		//Create cams  (4 cams needed)
		IF NOT DOES_CAM_EXIST(InterpFromCarCam)
			InterpFromCarCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ENDIF		
		
		IF NOT DOES_CAM_EXIST(CrashCamDest)
			CrashCamDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ENDIF
		
		IF NOT DOES_CAM_EXIST(CrashCamInit)
			CrashCamInit = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ENDIF		
		
		IF NOT DOES_CAM_EXIST(PointingCam)
			PointingCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
			vInterp_From_Car_Cam_Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<-1.8, -5.7, 2>>)
		ENDIF		
		
		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
			vCrash_Cam_Init_Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<-7, 27, 20>>)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
			vCrash_Cam_Dest_Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<7, 17, 20>>)
		ENDIF
		
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	
		WHILE b_Is_Cutscene_Running = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING("Case = ") PRINTINT(i_cutscene_stage) PRINTNL()
				PRINTSTRING("TimerA = ") PRINTINT(TIMERA()) PRINTNL()
				PRINTSTRING("TimerB = ") PRINTINT(TIMERB()) PRINTNL()
			#ENDIF
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			WAIT(0)
			
			SWITCH i_cutscene_stage 
			
				CASE 0
					
					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
						IF DOES_CAM_EXIST(InterpFromCarCam)
							SET_CAM_COORD(InterpFromCarCam, vInterp_From_Car_Cam_Coords)
							POINT_CAM_AT_ENTITY(InterpFromCarCam, PlayerCar, <<0, 0, 0>>)
							SET_CAM_FOV(InterpFromCarCam, 20)
							SHAKE_CAM(InterpFromCarCam, "ROAD_VIBRATION_SHAKE", 1)
							SET_TIME_SCALE(0.0005)
							SET_CAM_ACTIVE_WITH_INTERP(InterpFromCarCam, CamAttachedToCar, 3000)
							SETTIMERA(0)
							i_cutscene_stage ++
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 1
					
					IF TIMERA() > 100
						IF IS_VEHICLE_DRIVEABLE(PlayerCar)
							IF DOES_CAM_EXIST(CrashCamDest)
								SET_CAM_COORD(CrashCamDest, vCrash_Cam_Dest_Coords)
								POINT_CAM_AT_ENTITY(CrashCamDest, PlayerCar, <<0, 0, 0>>)
								SET_CAM_FOV(CrashCamDest, 30)
								SHAKE_CAM(CrashCamDest, "HAND_SHAKE", 1)
							ENDIF
							IF DOES_CAM_EXIST(CrashCamInit)
								SET_CAM_COORD(CrashCamInit, vCrash_Cam_Init_Coords)
								POINT_CAM_AT_ENTITY(CrashCamInit, PlayerCar, <<0, 0, 0>>)
								SET_CAM_FOV(CrashCamInit, 30)
								SHAKE_CAM(CrashCamInit, "HAND_SHAKE", 1)
							ENDIF
								
								SET_CAM_ACTIVE_WITH_INTERP(CrashCamDest, CrashCamInit, 4000)
								SET_TIME_SCALE(0.3)
								SETTIMERA(0)
								i_cutscene_stage++
							
						ENDIF
					ENDIF
					
				BREAK
				
				CASE 2
					
					//Get interping cams coords and fov
					IF DOES_CAM_EXIST(CrashCamDest)
						vPointing_Cam_Coords =	GET_CAM_COORD(CrashCamDest)
						fPointing_Cam_FOV = GET_CAM_FOV(CrashCamDest)
					ENDIF
					
					SET_CAM_PARAMS(PointingCam, vPointing_Cam_Coords, <<0,0,0>>, fPointing_Cam_FOV)
					POINT_CAM_AT_ENTITY(PointingCam, PlayerCar, <<0,0,0>>)
					SET_CAM_ACTIVE(PointingCam, TRUE)
					
					IF DOES_CAM_EXIST(PointingCam)
						IF bshakeCamActivated = FALSE
							SHAKE_CAM(PointingCam, "HAND_SHAKE", 1)
							bshakeCamActivated = TRUE
						ENDIF
					ENDIF
					
					IF bForceApplied = FALSE
						IF IS_VEHICLE_DRIVEABLE(PlayerCar)
							APPLY_FORCE_TO_ENTITY(PlayerCar, APPLY_TYPE_FORCE, <<0, 0, 1350>>, <<-0.4, -1.1, -1.5>>, 0, TRUE, TRUE, TRUE)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), PlayerCar, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
							SETTIMERB(0)
							bForceApplied = TRUE
						ENDIF
					ENDIF					
					
					IF TIMERA() > 450
						IF IS_CAM_ACTIVE(CrashCamDest)
							fNew_Dest_FOV = GET_CAM_FOV(CrashCamDest) 
						ENDIF
						IF IS_CAM_ACTIVE(CrashCamInit)
							fNew_Init_FOV = GET_CAM_FOV(CrashCamInit) 
						ENDIF					
						fNew_Dest_FOV = fNew_Dest_FOV -@ 20
						fNew_Init_FOV = fNew_Init_FOV -@ 20
					
						IF fNew_Dest_FOV < 10
							fNew_Dest_FOV = 10
						ENDIF
						IF fNew_Init_FOV < 10
							fNew_Init_FOV = 10
						ENDIF
						
						IF DOES_CAM_EXIST(CrashCamDest)
							SET_CAM_FOV(CrashCamDest, fNew_Dest_FOV)
						ENDIF
						IF DOES_CAM_EXIST(CrashCamInit)
							SET_CAM_FOV(CrashCamInit, fNew_Init_FOV)
						ENDIF
					ENDIF
					
					IF bvehicleExploded = FALSE
						IF bForceApplied = TRUE
							IF TIMERB() > 500
								IF IS_VEHICLE_DRIVEABLE(PlayerCar)
									IF NOT IS_ENTITY_IN_AIR(PlayerCar)
										EXPLODE_VEHICLE(PlayerCar, TRUE)
										SET_TIME_SCALE(0.4)
										SETTIMERB(0)
										bvehicleExploded = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bvehicleExploded = TRUE
						IF TIMERB() > 1400
							SET_TIME_SCALE(1)
							SETTIMERB(0)
							i_cutscene_stage++
						ENDIF
					ELSE
						IF TIMERB() > 4000
							SET_TIME_SCALE(1)
							i_cutscene_stage ++
						ENDIF
					ENDIF
					
				BREAK
				
				CASE 3

					i_cutscene_stage = 0
					b_Is_Cutscene_Running = FALSE

				BREAK
				
			ENDSWITCH
			
		ENDWHILE
		
		IF b_Is_Cutscene_Running = FALSE
		
			RENDER_SCRIPT_CAMS(FALSE, FALSE)

			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			
		ENDIF
		
ENDPROC

/// PURPOSE:
///    Function to handle just the basic explode stuff
/// PARAMS:
///    i_crash_stage - int for the stage of crash
///    PlayerCar - the vehicle to explode/apply force to
PROC HANDLE_CRASH(INT &i_crash_stage, VEHICLE_INDEX PlayerCar)
		
	
	SWITCH i_crash_stage 
		
		CASE 0
			SETTIMERA(0)
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			i_crash_stage++
		BREAK
			
		CASE 1
			IF TIMERA() > 100
				IF bForceApplied = FALSE
					APPLY_FORCE_TO_ENTITY(PlayerCar, APPLY_TYPE_FORCE, <<0, 0, 150>>, <<-0.4, -1.1, -1.5>>, 0, TRUE, TRUE, TRUE)
					SETTIMERB(0)
					bForceApplied = TRUE
				ENDIF				
				i_crash_stage++
			ENDIF
		BREAK
			
		CASE 2				
			IF bvehicleExploded = FALSE
				IF bForceApplied = TRUE
					IF TIMERB() > 500
						EXPLODE_VEHICLE(PlayerCar, TRUE)
						bvehicleExploded = TRUE
					ENDIF
				ENDIF
			ENDIF	
		BREAK
			
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Check to see if the player is crashing
FUNC BOOL CHECK_PLAYER_CRASHING(INT &iRaceZone, VEHICLE_INDEX PlayerCar)
	FLOAT fPlayerHeading
	FLOAT fPlayerSpeed
	
	PED_INDEX PedIndex
	PedIndex = PLAYER_PED_ID()
	IF IS_VEHICLE_DRIVEABLE(PlayerCar)
		fPlayerHeading = GET_ENTITY_HEADING(PlayerCar)
		fPlayerSpeed = GET_ENTITY_SPEED(PlayerCar)
	
		IF IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-403.617249,21.196581,44.900120>>, <<-407.236359,-19.419012,48.892529>>, 10.000000)
			iRaceZone = 1
		ELIF IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-691.598572,-46.563717,35.820042>>, <<-714.964661,3.306605,39.831947>>, 10.000000)
			iRaceZone = 2
		ELIF IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1159.647095,-299.341919,35.832703>>, <<-1178.641602,-259.096588,39.832699>>, 10.000000)
			iRaceZone = 3
		ELIF IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-1420.035156,-436.437195,38.974415>>, <<-1441.095337,-401.565948,38.210201>>, 10.000000)
			iRaceZone = 4
		ENDIF
	//PRINTSTRING("HEALTH OF VEHICLE IS") PRINTINT(GET_ENTITY_HEALTH(PlayerCar)) PRINTNL()
// Angled Area Info
		IF GET_ENTITY_HEALTH(PlayerCar) <= 850
			PRINTSTRING("Player crashing due to low health") PRINTNL()
			RETURN TRUE
		ENDIF
	ENDIF
	IF fPlayerSpeed > 20
		
		SWITCH iRaceZone
			
			CASE 0	
				IF fPlayerHeading > 108
				OR fPlayerHeading < 10
					PRINTSTRING("Player crashed when in zone 0") PRINTNL()
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE 1	
				IF fPlayerHeading > 140
				OR fPlayerHeading < 30
					PRINTSTRING("Player crashed when in zone 1") PRINTNL()
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE 2
				IF fPlayerHeading > 175
				OR fPlayerHeading < 40
					PRINTSTRING("Player crashed when in zone 2") PRINTNL()
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE 3
				IF fPlayerHeading > 175
				OR fPlayerHeading < 60
					PRINTSTRING("Player crashed when in zone 3") PRINTNL()
					RETURN TRUE
				ENDIF
			BREAK	
			
			CASE 4
				IF fPlayerHeading < -150
				OR (fPlayerHeading < 38 AND fPlayerHeading > 0)
					PRINTSTRING("Player crashed when in zone 4") PRINTNL()
					RETURN TRUE
				ENDIF
			BREAK		

		ENDSWITCH
		
	ENDIF
	RETURN FALSE
ENDFUNC
