USING "camhedz_arcade_using.sch"
USING "camhedz_arcade_levels.sch"
USING "camhedz_arcade_leaderboard.sch"
USING "camhedz_arcade_sprites.sch"
USING "arcade_games_postfx.sch"
USING "degenatron_games_movie.sch"

//-------------------------------------------------
// HUD CHAR FUNCTIONS
//-------------------------------------------------
FUNC STRING CMHDZ_GET_HUD_CHAR_SPRITE(CMHDZ_HUD_CHAR eChar)
	SWITCH (eChar)
		CASE CMHDZ_HUD_CHAR_0				RETURN "FONT_0"
		CASE CMHDZ_HUD_CHAR_1				RETURN "FONT_1"
		CASE CMHDZ_HUD_CHAR_2				RETURN "FONT_2"
		CASE CMHDZ_HUD_CHAR_3				RETURN "FONT_3"
		CASE CMHDZ_HUD_CHAR_4				RETURN "FONT_4"
		CASE CMHDZ_HUD_CHAR_5				RETURN "FONT_5"
		CASE CMHDZ_HUD_CHAR_6				RETURN "FONT_6"
		CASE CMHDZ_HUD_CHAR_7				RETURN "FONT_7"
		CASE CMHDZ_HUD_CHAR_8				RETURN "FONT_8"
		CASE CMHDZ_HUD_CHAR_9				RETURN "FONT_9"
		CASE CMHDZ_HUD_CHAR_A				RETURN "FONT_A"
		CASE CMHDZ_HUD_CHAR_B				RETURN "FONT_B"
		CASE CMHDZ_HUD_CHAR_C				RETURN "FONT_C"
		CASE CMHDZ_HUD_CHAR_D				RETURN "FONT_D"
		CASE CMHDZ_HUD_CHAR_E				RETURN "FONT_E"
		CASE CMHDZ_HUD_CHAR_F				RETURN "FONT_F"
		CASE CMHDZ_HUD_CHAR_G				RETURN "FONT_G"
		CASE CMHDZ_HUD_CHAR_H				RETURN "FONT_H"
		CASE CMHDZ_HUD_CHAR_I				RETURN "FONT_I"
		CASE CMHDZ_HUD_CHAR_J				RETURN "FONT_J"
		CASE CMHDZ_HUD_CHAR_K				RETURN "FONT_K"
		CASE CMHDZ_HUD_CHAR_L				RETURN "FONT_L"
		CASE CMHDZ_HUD_CHAR_M				RETURN "FONT_M"
		CASE CMHDZ_HUD_CHAR_N				RETURN "FONT_N"
		CASE CMHDZ_HUD_CHAR_O				RETURN "FONT_O"
		CASE CMHDZ_HUD_CHAR_P				RETURN "FONT_P"
		CASE CMHDZ_HUD_CHAR_Q				RETURN "FONT_Q"
		CASE CMHDZ_HUD_CHAR_R				RETURN "FONT_R"
		CASE CMHDZ_HUD_CHAR_S				RETURN "FONT_S"
		CASE CMHDZ_HUD_CHAR_T				RETURN "FONT_T"
		CASE CMHDZ_HUD_CHAR_U				RETURN "FONT_U"
		CASE CMHDZ_HUD_CHAR_V				RETURN "FONT_V"
		CASE CMHDZ_HUD_CHAR_W				RETURN "FONT_W"
		CASE CMHDZ_HUD_CHAR_X				RETURN "FONT_X"
		CASE CMHDZ_HUD_CHAR_Y				RETURN "FONT_Y"
		CASE CMHDZ_HUD_CHAR_Z				RETURN "FONT_Z"
		
		CASE CMHDZ_HUD_CHAR_APOSTROPHE	RETURN "FONT_APOSTOPHE" //RETURN "FONT_SPECAILCHARACTER_APOSTROPHE"
		CASE CMHDZ_HUD_CHAR_BACKSLASH		RETURN "FONT_BKSLASH" //RETURN "FONT_SPECAILCHARACTER_SLASH"
		CASE CMHDZ_HUD_CHAR_CLOSEBRACKET	RETURN "FONT_]" //RETURN "FONT_SPECAILCHARACTER_CLOSE_BRACKET"
		CASE CMHDZ_HUD_CHAR_COLON			RETURN "FONT_COLON" //RETURN "FONT_SPECAILCHARACTER_COLON"
		CASE CMHDZ_HUD_CHAR_COMMA			RETURN "FONT_," //RETURN "FONT_SPECAILCHARACTER_,"
		CASE CMHDZ_HUD_CHAR_DOLLAR		RETURN "FONT_DOLLAR"
		CASE CMHDZ_HUD_CHAR_EXCLAMATION	RETURN "FONT_!" //RETURN "FONT_SPECAILCHARACTER_!"
		CASE CMHDZ_HUD_CHAR_FULL_STOP		RETURN "FONT_." //RETURN "FONT_SPECAILCHARACTER_."
		CASE CMHDZ_HUD_CHAR_MINUS			RETURN "FONT_-" //RETURN "FONT_SPECAILCHARACTER_-COPY"
		CASE CMHDZ_HUD_CHAR_OPEN_BRACKET	RETURN "FONT_[" //RETURN "FONT_SPECAILCHARACTER_OPEN_BRACKET"
		CASE CMHDZ_HUD_CHAR_PLUS			RETURN "FONT_+" //RETURN "FONT_SPECAILCHARACTER_PLUS"
		CASE CMHDZ_HUD_CHAR_QUESTION		RETURN "FONT_QUESTION" //RETURN "FONT_SPECAILCHARACTER_QUESTIONMARK"
		CASE CMHDZ_HUD_CHAR_SEMI_COLON	RETURN "FONT_;" // RETURN "FONT_SPECAILCHARACTER_SEMI_COLON"
		CASE CMHDZ_HUD_CHAR_SPEECH		RETURN "FONT_SPEECH" // RETURN "FONT_SPECAILCHARACTER_SEMI_COLON"
		CASE CMHDZ_HUD_CHAR_STAR			RETURN "FONT_ASTERIX" // SPECAILCHARACTER_STAR"
		CASE CMHDZ_HUD_CHAR_PERCENT			RETURN "FONT_%"
	ENDSWITCH 
	
	RETURN ""
ENDFUNC 

FUNC VECTOR_2D CMHDZ_GET_HUD_CHAR_SIZE(CMHDZ_HUD_CHAR eChar, FLOAT sx = 1.0, FLOAT sy = 1.0)
	VECTOR_2D vScale 
	
	vScale.y = 40 * sy 
	vScale.x = 24 * sx
	RETURN vScale
	
	SWITCH (eChar)
		CASE CMHDZ_HUD_CHAR_APOSTROPHE
		//CASE CMHDZ_HUD_CHAR_COLON
		CASE CMHDZ_HUD_CHAR_COMMA
		CASE CMHDZ_HUD_CHAR_EXCLAMATION
		CASE CMHDZ_HUD_CHAR_FULL_STOP
		CASE CMHDZ_HUD_CHAR_SEMI_COLON
			vScale.x = CMHDZ_PX_HUD_THIN_CHAR_WIDTH * sx
		BREAK 
		
		CASE CMHDZ_HUD_CHAR_COLON
			vScale.x = 4 * sx
		BREAK
		
		DEFAULT 
			vScale.x = CMHDZ_PX_HUD_CHAR_WIDTH * sx
		BREAK
	ENDSWITCH 
	
	RETURN vScale
ENDFUNC

PROC CMHDZ_PACK_HUD_CHAR_TO_ARRAY(CMHDZ_HUD_CHAR eChar, INT iIndex, INT &iCArray[])
	INT iIntArrayIndex = iIndex / 5
	
	IF (iIntArrayIndex >= COUNT_OF(iCArray))
		EXIT 
	ENDIF 
	
	INT iBitIntIndex = (iIndex % 5) * 6
	SET_BITS_IN_RANGE(iCArray[iIntArrayIndex], iBitIntIndex, iBitIntIndex + 5, ENUM_TO_INT(eChar))
ENDPROC 

PROC CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(INT iDigit, INT iIndex, INT &iCArray[])
	INT iIntArrayIndex = iIndex / 5
	
	IF (iIntArrayIndex >= COUNT_OF(iCArray))
		EXIT 
	ENDIF 
	
	INT iBitIntIndex = (iIndex % 5) * 6
	SET_BITS_IN_RANGE(iCArray[iIntArrayIndex], iBitIntIndex, iBitIntIndex + 5, ENUM_TO_INT(CMHDZ_HUD_CHAR_0) + iDigit)
ENDPROC 

FUNC CMHDZ_HUD_CHAR CMHDZ_UNPACK_HUD_CHAR_FROM_ARRAY(INT &iCArray[], INT iIndex)
	INT iIntArrayIndex = iIndex / 5
	INT iBitIntIndex = (iIndex % 5) * 6
	IF (iIntArrayIndex >= COUNT_OF(iCArray))
		RETURN CMHDZ_HUD_CHAR_INVALID
	ENDIF 
	
	RETURN INT_TO_ENUM(CMHDZ_HUD_CHAR, GET_BITS_IN_RANGE(iCArray[iIntArrayIndex], iBitIntIndex, iBitIntIndex + 5))
ENDFUNC 

PROC CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT eTextPivot)
	sCMHDZData.eTextPivot = eTextPivot
ENDPROC 

PROC CMHDZ_SET_HUD_CHAR_SCALE(FLOAT sx = 1.0, FLOAT sy = 1.0)
	sCMHDZData.vTextScale.x = sx
	sCMHDZData.vTextScale.y = sy
ENDPROC 

FUNC INT CMHDZ_GET_NUMBER_OF_DIGITS_IN_NUMBER(INT iNumber)
	RETURN FLOOR(LOG10(TO_FLOAT(iNumber))) + 1
ENDFUNC 

PROC CMHDZ_DRAW_HUD_CHAR_POSITIVE_NUMBER_AT_POSITION(FLOAT fX, FLOAT fY, INT iIntToDisplay, INT iNumLength = 8)
	INT iMaxDigits = CMHDZ_GET_NUMBER_OF_DIGITS_IN_NUMBER(iIntToDisplay)	
	IF (iMaxDigits >= CMHDZ_HUD_MAX_NUM_LENGTH)
		EXIT 
	ENDIF 
	
	INT i
	INT iCurValue = iIntToDisplay
	TEXT_LABEL_15 str 
	str += iIntToDisplay
	INT iStrLen = GET_LENGTH_OF_LITERAL_STRING(str)
	CMHDZ_HUD_CHAR mChars[CMHDZ_HUD_MAX_NUM_LENGTH]

	VECTOR_2D vPos = INIT_VECTOR_2D(fx, fy)
	VECTOR_2D vScale = INIT_VECTOR_2D(CMHDZ_PX_HUD_CHAR_WIDTH * sCMHDZData.vTextScale.x, CMHDZ_PX_HUD_CHAR_HEIGHT * sCMHDZData.vTextScale.y)
	
	IF (iStrLen < iNumLength)
		vPos.x -= ((iNumLength - iStrLen) * vScale.x)
	ENDIF 
	
	STRING txtDir = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
	REPEAT iMaxDigits i
		mChars[i] = INT_TO_ENUM(CMHDZ_HUD_CHAR, ENUM_TO_INT(CMHDZ_HUD_CHAR_0) + (iCurValue % 10))
		iCurValue /= 10
		
		CMHDZ_DRAW_GAME_SPRITE(txtDir, CMHDZ_GET_HUD_CHAR_SPRITE(mChars[i]), vPos, vScale, 0, sCMHDZData.rgbaWhite)
		vPos.x -= vScale.x
	ENDREPEAT 
ENDPROC 


PROC CMHDZ_WRITE_STRING_USING_TEXT_SPRITES(STRING sTextToDisplay, VECTOR_2D vDisplayPos, FLOAT fSizeMultiplier = 1.0, BOOL bCenterText = FALSE, INT iLengthOverride = 0, INT iRed = 255, INT iGreen = 255, Int iBlue = 255, INT iAlpha = 255)
	
	// Moddable variable
	VECTOR_2D vDisplayFinalPos = vDisplayPos	
	VECTOR_2D vBaseLetterScale = INIT_VECTOR_2D(24, 40)
	
	RGBA_COLOUR_STRUCT rgbaStruct
	INIT_RGBA_STRUCT(rgbaStruct, iRed, iGreen, iBlue, iAlpha)
		
	INT iLengthOfString = GET_LENGTH_OF_LITERAL_STRING(sTextToDisplay)
	
	IF iLengthOverride > 0
		iLengthOfString = iLengthOverride
	ENDIF
	
	IF bCenterText
		vDisplayFinalPos.X = (cfBASE_SCREEN_WIDTH/2.0)
		vDisplayFinalPos.X -= ROUND((24.0*fSizeMultiplier)*((iLengthOfString-1)/2.0))
		//vDisplayFinalPos.X += 18 // Fixer
	ENDIF
		
	FLOAT fCarryOverX = 0
		
	INT i = 0
	FOR i = 0 TO iLengthOfString-1
		
		TEXT_LABEL_63 sLetterTexture = "FONT_"
		TEXT_LABEL_63 sCharacter = "!"

		sCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sTextToDisplay, i, i+1)	
		
		VECTOR_2D vScaleFinal = MULTIPLY_VECTOR_2D(vBaseLetterScale, fSizeMultiplier)
		
		IF ARE_STRINGS_EQUAL(sCharacter, " ")
			vDisplayFinalPos.x += vBaseLetterScale.x // Add the size of a sprite Before displaying the next one.
			RELOOP
		ENDIF
		
		// Smaller and Wider font
//		IF ARE_STRINGS_EQUAL(sCharacter, "M")
//		OR ARE_STRINGS_EQUAL(sCharacter, "W")
//			vScaleFinal.x = (48.0*fSizeMultiplier)
//			vDisplayFinalPos.x += (vScaleFinal.x/8)
//		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ".")
		OR ARE_STRINGS_EQUAL(sCharacter, ":")
		OR ARE_STRINGS_EQUAL(sCharacter, "!")
		OR ARE_STRINGS_EQUAL(sCharacter, "'")
			vScaleFinal.x = (20.0*fSizeMultiplier)
			vDisplayFinalPos.x -= (vScaleFinal.x/8)
		ENDIF
		
		// Check if Character is a misc
//		IF ARE_STRINGS_EQUAL(sCharacter, "!")
//			sCharacter = "!"
//		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, "?")
			sCharacter = "Question"
		ENDIF

		IF ARE_STRINGS_EQUAL(sCharacter, ".")
			sCharacter = "DOT"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ":")
			sCharacter = "Colon"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, "'")
			sCharacter = "Apostrophe"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, "^") // Fallback as we can't specify " in code
			sCharacter = "Speech"
		ENDIF
		
//		IF ARE_STRINGS_EQUAL(sCharacter, "%")
//			sCharacter = "%"
//		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, "*")
			sCharacter = "Asterix"
		ENDIF
		
//		IF ARE_STRINGS_EQUAL(sCharacter, "+")
//			sCharacter = "+"
//		ENDIF
		
		// WIDER CHARACTERS
//		IF ARE_STRINGS_EQUAL(sCharacter, "M")
//			sCharacter = "M_WIDE"
//		ENDIF
//		
//		IF ARE_STRINGS_EQUAL(sCharacter, "W")
//			sCharacter = "W_WIDE"
//		ENDIF
				
		sLetterTexture += sCharacter

		vDisplayFinalPos.x += (fCarryOverX)
		fCarryOverX = 0
				
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), sLetterTexture, vDisplayFinalPos, vScaleFinal, 0.0, rgbaStruct)
			
//		IF ARE_STRINGS_EQUAL(sCharacter, "M_WIDE")
//		OR ARE_STRINGS_EQUAL(sCharacter, "W_WIDE")
//			fCarryOverX = (vScaleFinal.x/8)
//		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ".")
		OR ARE_STRINGS_EQUAL(sCharacter, ":")
		OR ARE_STRINGS_EQUAL(sCharacter, "!")
		OR ARE_STRINGS_EQUAL(sCharacter, "'")
			fCarryOverX = -(vScaleFinal.x/8)
		ENDIF
		
		IF vScaleFinal.x  > (vBaseLetterScale.x*fSizeMultiplier)
			vScaleFinal.x = (vBaseLetterScale.x*fSizeMultiplier)
		ENDIF
		
		vDisplayFinalPos.x += vScaleFinal.x // Add the size of a sprite Before displaying the next one.
		
		
	ENDFOR
	
ENDPROC

PROC CMHDZ_DRAW_DOLLAR_SYMBOL(VECTOR_2D vPos, FLOAT fScale = 1.0, INT iR = 255, INT iG = 255, INT iB = 255, INT iA = 255)
	//CMHDZ_DRAW_GAME_SPRITE(txTexture, vPos, vScale, 0, sCMHDZData.rgbaSprite)
	 CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("$", vPos, fScale, DEFAULT, DEFAULT, iR, iG, iB, iA)
ENDPROC

PROC CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(FLOAT fX, FLOAT fY, INT &iCArray[], INT iNumCharactersInArray, RGBA_COLOUR_STRUCT col)
	INT i
	CMHDZ_HUD_CHAR eChar
	VECTOR_2D vPos = INIT_VECTOR_2D(fx, fy)
	VECTOR_2D vScale = INIT_VECTOR_2D(CMHDZ_PX_HUD_CHAR_WIDTH * sCMHDZData.vTextScale.x, CMHDZ_PX_HUD_CHAR_HEIGHT * sCMHDZData.vTextScale.y)
	FLOAT sWidth 
	
	// get string len
	REPEAT iNumCharactersInArray i
		eChar = CMHDZ_UNPACK_HUD_CHAR_FROM_ARRAY(iCArray, i)	
		vScale = CMHDZ_GET_HUD_CHAR_SIZE(eChar, sCMHDZData.vTextScale.x, sCMHDZData.vTextScale.y)
		sWidth += vScale.x
	ENDREPEAT 
	
	vPos.y += (vScale.y / 2)
	SWITCH (sCMHDZData.eTextPivot)
		CASE CMHDZ_HUD_TEXTPIVOT_LEFT
			vPos.x += (vScale.x / 2)
		BREAK 
		
		CASE CMHDZ_HUD_TEXTPIVOT_CENTER
			vPos.x -= (sWidth / 2.0)
			vPos.x += (CMHDZ_PX_HUD_CHAR_WIDTH / 2.0)
		BREAK 
		
		CASE CMHDZ_HUD_TEXTPIVOT_RIGHT
			vPos.x -= (sWidth)
		BREAK 
	ENDSWITCH
			
	STRING txtDir = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
	
	REPEAT iNumCharactersInArray i
		eChar = CMHDZ_UNPACK_HUD_CHAR_FROM_ARRAY(iCArray, i)
		IF (eChar != CMHDZ_HUD_CHAR_SPACE) AND (eChar != CMHDZ_HUD_CHAR_INVALID)
			vScale = CMHDZ_GET_HUD_CHAR_SIZE(eChar, sCMHDZData.vTextScale.x, sCMHDZData.vTextScale.y)
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(txtDir, CMHDZ_GET_HUD_CHAR_SPRITE(eChar), vPos, vScale, 0, col)
			
			vPos.x += vScale.x
		ELSE 
			vPos.x += (CMHDZ_PX_HUD_CHAR_WIDTH * sCMHDZData.vTextScale.x)
		ENDIF 
	ENDREPEAT 
ENDPROC 

PROC CMHDZ_DRAW_REC_TEXT(VECTOR_2D vPos, RGBA_COLOUR_STRUCT col)
	INT iCArray[1] //REC: - # Chars:4
	iCArray[0] = 10806301

	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_LEFT)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(vPos.x, vPos.y, iCArray, 4, col)
ENDPROC

PROC CMHDZ_DRAW_SIGNAL_FAILURE_TEXT()
	
//	TEXT_LABEL_31 tl31Sprite = "LEVEL_0"
//	tl31Sprite += ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1
//	tl31Sprite += "_OUTRO_FAIL"
//	
//	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_RESULTS), tl31Sprite, vPos, INIT_VECTOR_2D(320, 104), 0, sCMHDZData.rgbaSprite)

	VECTOR_2D vPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH / 2, cfBASE_SCREEN_HEIGHT * 0.3)
	VECTOR_2D vLeftPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH / 3, cfBASE_SCREEN_HEIGHT * 0.5)

	SWITCH sCMHDZData.eCurrentLevel
		
		CASE CMHDZ_LEVEL_CITY
			ARCADE_DRAW_PIXELSPACE_RECT(vPos, INIT_VECTOR_2D(600, 80), sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("BUTCHERED!", INIT_VECTOR_2D(vPos.x, vPos.y), 1.0, TRUE, 10, 0, 0, 0)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("[The Hoodz shot you down]", vLeftPos, DEFAULT, TRUE)
		BREAK

		CASE CMHDZ_LEVEL_SCRAPYARD
			ARCADE_DRAW_PIXELSPACE_RECT(vPos, INIT_VECTOR_2D(600, 80), sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("EXECUTED!", INIT_VECTOR_2D(vPos.x, vPos.y), 1.0, TRUE, 9, 0,0,0)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("THE INNOCENTZ CORRUPTED YOUR HEAD.", vLeftPos, DEFAULT, TRUE)
		BREAK	
		
		CASE CMHDZ_LEVEL_ASYLUM
			ARCADE_DRAW_PIXELSPACE_RECT(vPos, INIT_VECTOR_2D(600, 80), sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("MASSACRED!", INIT_VECTOR_2D(vPos.x, vPos.y), 1.0, TRUE, 10, 0,0,0)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("THERE WAS NO GETTING PAST THESE GUARDZ.", vLeftPos, DEFAULT, TRUE)
		BREAK
	
		CASE CMHDZ_LEVEL_PARK
			ARCADE_DRAW_PIXELSPACE_RECT(vPos, INIT_VECTOR_2D(600, 80), sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("MURDERED!", INIT_VECTOR_2D(vPos.x, vPos.y), 1.0, TRUE, 9, 0, 0, 0)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("THE CLOWNZ SPLIT YOUR SIDES.", vLeftPos, DEFAULT, TRUE)
		BREAK
	
		CASE CMHDZ_LEVEL_SLAUGHTERHOUSE
			ARCADE_DRAW_PIXELSPACE_RECT(vPos, INIT_VECTOR_2D(600, 80), sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("OBLITERATED!", INIT_VECTOR_2D(vPos.x, vPos.y), 1.0, TRUE, 12, 0, 0, 0)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("You got trampled by the Hurd.", vLeftPos, DEFAULT, TRUE)
		BREAK
	
	ENDSWITCH

ENDPROC



PROC CMHDZ_DRAW_EJECT_OPTION_TEXT(VECTOR_2D vPos, RGBA_COLOUR_STRUCT col)
	INT iCArray[1] //EJECT - # Chars:5
	iCArray[0] = 523830608

	CMHDZ_SET_HUD_CHAR_SCALE(1.0, 1.0)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_CENTER)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(vPos.x, vPos.y, iCArray, 5, col)
ENDPROC

PROC CMHDZ_DRAW_REWIND_OPTION_TEXT(VECTOR_2D vPos, RGBA_COLOUR_STRUCT col)
	INT iCArray[2] //REWIND - # Chars:6
	iCArray[0] = 424813597
	iCArray[1] = 15

	CMHDZ_SET_HUD_CHAR_SCALE(1.0, 1.0)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_CENTER)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(vPos.x, vPos.y, iCArray, 6, col)
ENDPROC

PROC CMHDZ_DRAW_NO_AMMO_TEXT(RGBA_COLOUR_STRUCT col)
	INT iCArray[2] //NO AMMO! - # Chars:8
	iCArray[0] = 405804697
	iCArray[1] = 181912

	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_CENTER)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(cfBASE_SCREEN_WIDTH / 2, cfBASE_SCREEN_HEIGHT / 2, iCArray, 8, col)
ENDPROC

PROC CMHDZ_DRAW_RELOAD_TEXT(RGBA_COLOUR_STRUCT col)
	INT iCArray[2] //RELOAD! - # Chars:7
	iCArray[0] = 208237597
	iCArray[1] = 2831

	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_CENTER)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(cfBASE_SCREEN_WIDTH / 2, cfBASE_SCREEN_HEIGHT / 2, iCArray, 7, col)
ENDPROC

PROC CMHDZ_DRAW_ROUND_START_TEXT(RGBA_COLOUR_STRUCT col)
	INT iCArray[3] //TAPE 1 START - # Chars:12
	iCArray[0] = 21082911
	iCArray[1] = 209576003
	iCArray[2] = 2013

	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(ENUM_TO_INT(sCMHDZData.eCurrentLevel) + 1, 5, iCArray)
	
	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_CENTER)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(cfBASE_SCREEN_WIDTH / 2, cfBASE_SCREEN_HEIGHT / 2, iCArray, 12, col)
ENDPROC

PROC CMHDZ_DRAW_ROUND_CLEAR_TEXT(RGBA_COLOUR_STRUCT col)
	INT iCArray[2] //TAPE 1 END - # Chars:10
	iCArray[0] = 21082911
	iCArray[1] = 258277443
	
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(ENUM_TO_INT(sCMHDZData.eCurrentLevel) + 1, 5, iCArray)
	
	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	CMHDZ_SET_HUD_CHAR_PIVOT(CMHDZ_HUD_TEXTPIVOT_CENTER)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(cfBASE_SCREEN_WIDTH / 2, cfBASE_SCREEN_HEIGHT / 2, iCArray, 10, col)
ENDPROC

PROC CMHDZ_DRAW_TIME_TEXT(VECTOR_2D vPos, INT iMilliSeconds, RGBA_COLOUR_STRUCT col, CMHDZ_HUD_TEXTPIVOT just = CMHDZ_HUD_TEXTPIVOT_RIGHT)
	INT iCArray[2] //00:00:00 - # Chars:8
	iCArray[0] = 34246786
	iCArray[1] = 8361

	// break milliseconds into time 
	INT iHths = (iMilliSeconds / 10) % 100
	INT iSecs = (iMilliSeconds / 1000) % 60
	INT iMins = (iMilliSeconds / 60000) % 60
	
	// replace chars 
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iHths % 10), 7, iCArray)
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(((iHths / 10) % 10), 6, iCArray)
	
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iSecs % 10), 4, iCArray)
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(((iSecs / 10) % 10), 3, iCArray)
	
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iMins % 10), 1, iCArray)
	CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY(((iMins / 10) % 10), 0, iCArray)
	
	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	CMHDZ_SET_HUD_CHAR_PIVOT(just)
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(vPos.x, vPos.y, iCArray, 8, col)
ENDPROC

PROC CMHDZ_DRAW_PLAYER_SCORE_TEXT(INT sc, RGBA_COLOUR_STRUCT col, FLOAT fx = 680.0, FLOAT fy = 95.0, CMHDZ_HUD_TEXTPIVOT ePivot = CMHDZ_HUD_TEXTPIVOT_USE_STORED)
	INT iMaxDigits, i, n
	INT iCurValue
	
	INT iCArray[2] //00:00:00 - # Chars:8
	iCArray[0] = 34246786
	iCArray[1] = 8361
	
	// get number bit
	IF (sc > 0)
		iCurValue = sc
		iMaxDigits = CMHDZ_GET_NUMBER_OF_DIGITS_IN_NUMBER(sc)
		n = 0
		REPEAT iMaxDigits i
			CMHDZ_PACK_SINGLE_DIGIT_TO_HUD_CHAR_ARRAY((iCurValue % 10), 7 - n, iCArray)
			iCurValue /= 10
			
			// seperate every second number with :
			IF (i % 2 != 0)
				n ++
			ENDIF
			
			n ++
		ENDREPEAT 
	ENDIF 

	CMHDZ_SET_HUD_CHAR_SCALE(1.5, 1.5)
	IF (ePivot != CMHDZ_HUD_TEXTPIVOT_USE_STORED) 
		CMHDZ_SET_HUD_CHAR_PIVOT(ePivot)
	ELIF (ePivot != sCMHDZData.eTextPivot)
		CMHDZ_SET_HUD_CHAR_PIVOT(ePivot)
	ENDIF
	
	CMHDZ_DRAW_HUD_CHAR_PACKED_ARRAY_AT_POSITION(fx, fy, iCArray, 8, col)
ENDPROC

PROC CMHDZ_DRAW_HORIZONTAL_MASK_SPRITE(STRING sTexDict, STRING sTexName, VECTOR_2D vCenter, VECTOR_2D vScale, FLOAT maskRatio, FLOAT rot, RGBA_COLOUR_STRUCT col)
	VECTOR_2D vNewScale = vScale 
	
	vNewScale.y *= maskRatio
	vCenter.y -= (vScale.y / 2.0)
	vCenter.y += (vNewScale.y / 2.0)
	
	vCenter.y += sCMHDZData.fSyncOffsetY
	
	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(sTexDict, sTexName, vCenter, vNewScale, INIT_VECTOR_2D(0, 0), INIT_VECTOR_2D(1, maskRatio), rot, col)
ENDPROC 

PROC CMHDZ_DRAW_GAME_OVER_EFFECT()

	VECTOR_2D vu, vy	
	RGBA_COLOUR_STRUCT col 
	VECTOR_2D vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
//	
//	FLOAT fDesiredFrameTime = 48.0
//	FLOAT fFrameTime = (0.0+@1000)
//	fFrameTime += (fFrameTime%8.0)
//	CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_DRAW_GAME_OVER_EFFECT - fFrameTime mod: ", fFrameTime)
	
//	vu.x = 1.0
//	vu.y = 1.0
	
	// If fc is 242
	// resired ft is 32
	// Reality ft 4
	
//	INT iFrameCount = ROUND(GET_FRAME_COUNT() / (fDesiredFrameTime/fFrameTime))
//	CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_DRAW_GAME_OVER_EFFECT - iFrameCount: ", iFrameCount)	
	 // If frameTime = 4 we get 8 by *2
	 // If frameTime = 8 do nothing
	 // If frameTime = 24 we get 8 by / 3
	 // If frameTime = 32 we get 8 by / 4

	SWITCH (GET_FRAME_COUNT() % 8)
		CASE 0
		CASE 1
			vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
		BREAK 

		CASE 2
		CASE 3
			vNoise = INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
		BREAK 
		
		CASE 4
		CASE 5
			vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, -cfGAME_SCREEN_HEIGHT)
		BREAK 
		
		CASE 6
		CASE 7
			vNoise = INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH, -cfGAME_SCREEN_HEIGHT)
		BREAK 
	ENDSWITCH
	
	INIT_RGBA_STRUCT(col, 190, 190, 170, 150)

	vu.x = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vu.y = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vy.x = vu.x + 0.75
	vy.y = vu.y + 0.75
	
	vy.x *= 2.0
	vy.y *= 2.0

	// Testing doubling effect
	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "critical_damage_fx_001",
		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), vNoise, vu, vy, 0, col)

//	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "critical_damage_fx_001",
//		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), vNoise, vu, vy, 0, col)

ENDPROC

PROC CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
	VECTOR_2D vu, vy	
	RGBA_COLOUR_STRUCT col 
	VECTOR_2D vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)

	SWITCH (GET_FRAME_COUNT() % 8)
		CASE 0
		CASE 1
			vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
		BREAK 

		CASE 2
		CASE 3
			vNoise = INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
		BREAK 
		
		CASE 4
		CASE 5
			vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, -cfGAME_SCREEN_HEIGHT)
		BREAK 
		
		CASE 6
		CASE 7
			vNoise = INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH, -cfGAME_SCREEN_HEIGHT)
		BREAK 
	ENDSWITCH
	
	col = sCMHDZData.rgbaWhiteSafe
	col.iA = 145
	//INIT_RGBA_STRUCT(col, 255, 255, 255, 145)
	
	vu.x = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vu.y = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vy.x = vu.x + 0.75
	vy.y = vu.y + 0.75
	
	//vy.x *= 2.0
	vy.y *= 2.0

	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "heavy_damage_fx_overlay_001", 
										  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), vNoise, vu, vy, 0, col)

ENDPROC 


PROC CMHDZ_DRAW_REWIND_SYMBOL()

	
	VECTOR_2D vPos1 = INIT_VECTOR_2D(420, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 48)
	VECTOR_2D vPos2 = INIT_VECTOR_2D(vPos1.x + 48,vPos1.y)
	VECTOR_2D vScaleRev = INIT_VECTOR_2D(-48,80)
	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS("MPCamhedzVHSFont", "PLAY", vPos1, vScaleRev, 0.0, sCMHDZData.rgbaSprite)
	CMHDZ_DRAW_GAME_SPRITE_DETAILS("MPCamhedzVHSFont", "PLAY", vPos2, vScaleRev, 0.0, sCMHDZData.rgbaSprite)

ENDPROC

PROC CMHDZ_DRAW_PLAY_SYMBOL()
	
	IF (sCMHDZData.iFade < 10.0)
		EXIT
	ENDIF
	
	CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("PLY:", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 20, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28), 1.5)

	VECTOR_2D vPos1 = INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 160, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28)
	VECTOR_2D vScaleRev = INIT_VECTOR_2D(48,40)
	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS("MPCamhedzVHSFont", "PLAY", vPos1, vScaleRev, 0.0, sCMHDZData.rgbaGreen)
	
ENDPROC

PROC CMHDZ_DRAW_STOP_SYMBOL()
	CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("STP:", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 20, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28), 1.5)
	
	VECTOR_2D vPos1 = INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 160, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28)
	VECTOR_2D vScaleRev = INIT_VECTOR_2D(48,48)
	vPos1.y += sCMHDZData.fSyncOffsetY
	ARCADE_DRAW_PIXELSPACE_RECT(vPos1, vScaleRev, sCMHDZData.rgbaRedText)
ENDPROC

PROC CMHDZ_DRAW_SELECT_SYMBOL(VECTOR_2D vPos1)

	VECTOR_2D vScaleRev = INIT_VECTOR_2D(24,40)
	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS("MPCamhedzVHSFont", "PLAY", vPos1, vScaleRev, 0.0, sCMHDZData.rgbaSprite)
	
ENDPROC

PROC CMHDZ_DRAW_REWIND_EFFECT()
	VECTOR_2D vu, vy	

	vu.x = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vu.y = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vy.x = vu.x + 0.75
	vy.y = vu.y + 0.75
	
	//vy.x *= 2.0
	vy.y *= 2.0
	
	RGBA_COLOUR_STRUCT col = sCMHDZData.rgbaWhiteSafe
	col.iA -= 100

	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "damage_fx_overlay_001", 
										  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, (cfBASE_SCREEN_HEIGHT/2) - 180), 
										  INIT_VECTOR_2D(1500, 200),
										  vu, vy, 0, col)

	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "damage_fx_overlay_001", 
										  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, (cfBASE_SCREEN_HEIGHT/2) + 220), 
										  INIT_VECTOR_2D(1500, 200), 
										  vu, vy, 0, col)
										  
//	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2,cfBASE_SCREEN_HEIGHT/2),
//								INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH,cfBASE_SCREEN_HEIGHT),
//								sCMHDZData.rgbaBlack)

	
ENDPROC 

PROC CMHDZ_DRAW_DAMAGE_NOISE(INT iAlpha = 255)
	VECTOR_2D vu, vy	
	RGBA_COLOUR_STRUCT col 
	VECTOR_2D vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
	
	SWITCH (GET_FRAME_COUNT() % 8)
		CASE 0
		CASE 1
			vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
		BREAK 

		CASE 2
		CASE 3
			vNoise = INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT)
		BREAK 
		
		CASE 4
		CASE 5
			vNoise = INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, -cfGAME_SCREEN_HEIGHT)
		BREAK 
		
		CASE 6
		CASE 7
			vNoise = INIT_VECTOR_2D(-cfGAME_SCREEN_WIDTH, -cfGAME_SCREEN_HEIGHT)
		BREAK 
	ENDSWITCH
	
	col = sCMHDZData.rgbaWhiteSafe
	col.iA = iAlpha
	//INIT_RGBA_STRUCT(col, 255, 255, 255, iAlpha)

	vu.x = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vu.y = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
	vy.x = vu.x + 0.75
	vy.y = vu.y + 0.75
	
	//vy.x *= 2.0
	vy.y *= 2.0
	
	// Testing doubling effect
	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "damage_fx_overlay_001", 
										  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), vNoise, vu, vy, 0, col)
	
//	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "damage_fx_overlay_001", 
//										  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), vNoise, vu, vy, 0, col)

ENDPROC 

///////////////////////////////
///    Star Rating System	///
///////////////////////////////

PROC CMHDZ_DRAW_STAR_RATING(VECTOR_2D vPos, FLOAT rating, FLOAT starSize = 64.0)
	INT i
	RGBA_COLOUR_STRUCT col 
	VECTOR_2D starPos = vPos 
	
	VECTOR_2D size = INIT_VECTOR_2D(starSize, starSize)
	STRING textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
		
	// draw full stars
	REPEAT 5 i 
		IF (i <= (ROUND(rating)))
			col = sCMHDZData.rgbaYellow
		ELSE 
			col = sCMHDZData.rgbaWhite
		ENDIF
		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(textDict, "STAR", starPos, size, 0, col) 
		starPos.y -= (size.y + 5)
	ENDREPEAT 
	
	// draw section star
	IF (rating > 5)
		EXIT 
	ENDIF 
	
//	IF ((pc = 0.0) OR (pc = 1.0))
//		EXIT 
//	ENDIF
	
//	VECTOR_2D vu, vy	
//	
//	starPos = vPos 
//	starPos.x += ((size.x + 5) * FLOOR(rating))
//	starPos.x -= (starSize / 2.0)
//	//starPos.x += ((starSize * pc) / 2.0)
//	vu = INIT_VECTOR_2D(0, 0)
//	vy = INIT_VECTOR_2D(pc, 1)
//	size = INIT_VECTOR_2D(starSize * pc, starSize)
//	
//	col = sCMHDZData.rgbaYellow
//	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(textDict, "STAR", starPos, size, vu, vy, 0, col)
ENDPROC 


PROC CMHDZ_DRAW_STAR_RATING_RESULTS(VECTOR_2D vPos, FLOAT rating, FLOAT starSize = 64.0)
	INT i
	RGBA_COLOUR_STRUCT col 
	VECTOR_2D starPos = vPos 
	
	VECTOR_2D size = INIT_VECTOR_2D(starSize, starSize)
	STRING textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
		
	// draw full stars
	REPEAT 5 i 
		IF (i <= (ROUND(rating)))
			col = sCMHDZData.rgbaYellow
		ELSE 
			col = sCMHDZData.rgbaWhite
		ENDIF
		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(textDict, "STAR", starPos, size, 0, col) 
		starPos.x += (size.x + 5)
	ENDREPEAT 
	
	// draw section star
	IF (rating > 5)
		EXIT 
	ENDIF 
	
//	IF ((pc = 0.0) OR (pc = 1.0))
//		EXIT 
//	ENDIF
	
//	VECTOR_2D vu, vy	
//	
//	starPos = vPos 
//	starPos.x += ((size.x + 5) * FLOOR(rating))
//	starPos.x -= (starSize / 2.0)
//	//starPos.x += ((starSize * pc) / 2.0)
//	vu = INIT_VECTOR_2D(0, 0)
//	vy = INIT_VECTOR_2D(pc, 1)
//	size = INIT_VECTOR_2D(starSize * pc, starSize)
//	
//	col = sCMHDZData.rgbaYellow
//	ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(textDict, "STAR", starPos, size, vu, vy, 0, col)
ENDPROC 

PROC CMHDZ_ASIGN_STAR_RATING(CMHDZ_LEVELS level)

	INT iMaxScore = 0
	FLOAT fRating = 0.0
	FLOAT pc = 0.0

	// Per level score ceiling based on overall enemy count for each stage
	// Level 5 scores should be based on total enemy count * default enemy score.
	// Scale accordingly
	SWITCH(level)
	
		CASE CMHDZ_LEVEL_CITY
			iMaxScore = 10000			
		BREAK
		
		CASE CMHDZ_LEVEL_SCRAPYARD
			iMaxScore = 10000
		BREAK
		
		CASE CMHDZ_LEVEL_ASYLUM
			iMaxScore = 10000
		BREAK
		
		CASE CMHDZ_LEVEL_PARK
			iMaxScore = 20000
		BREAK
		
		CASE CMHDZ_LEVEL_SLAUGHTERHOUSE
			iMaxScore = 100000
		BREAK
			
	ENDSWITCH
	
	CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_ASIGN_STAR_RATING - iStageScore: ", CMHDZ_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(ENUM_TO_INT(level)))
	CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_ASIGN_STAR_RATING - MaxScore: ", iMaxScore)
	
	fRating = (TO_FLOAT(CMHDZ_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(ENUM_TO_INT(level))))
	fRating += (TO_FLOAT(playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[level]) * ((CMHDZ_GET_PLAYER_ACCURACY_FOR_LEVEL(level)) / 100.0))
	fRating /= iMaxScore // Percentage
	// Round up
	fRating = (fRating*5.0)
	pc = fRating - TO_FLOAT(FLOOR(fRating))
	fRating -= pc
	fRating -= 1 //Correction for rounding
	playerBd[PARTICIPANT_ID_TO_INT()].fPlayerStarRating[level] = fRating
ENDPROC

PROC CMHDZ_DRAW_STAR_RATING_FOR_STAGE(VECTOR_2D pos, CMHDZ_LEVELS level, FLOAT fSpriteSize = 24.0)

	CMHDZ_DRAW_STAR_RATING(pos, playerBd[PARTICIPANT_ID_TO_INT()].fPlayerStarRating[level], fSpriteSize)

ENDPROC

PROC CMHDZ_DRAW_STAR_RATING_RESULTS_FOR_STAGE(VECTOR_2D pos, CMHDZ_LEVELS level, FLOAT fSpriteSize = 24.0)

	CMHDZ_DRAW_STAR_RATING_RESULTS(pos, playerBd[PARTICIPANT_ID_TO_INT()].fPlayerStarRating[level], fSpriteSize)

ENDPROC

///////////////////////////////

PROC TWS_DRAW_FRONT_FX()


	ARCADE_GAMES_POSTFX_DRAW()
	
	#IF IS_DEBUG_BUILD
		IF sCMHDZData.bDisableFacade
			EXIT
		ENDIF
	#ENDIF
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_FACADE), "Badlands_Bezel", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0, sCMHDZData.rgbaWhite)	
	
ENDPROC


/// PURPOSE:
///    Different scores (Hi-score, current score, rival's score) are different colours. Need to construct the texture name
/// PARAMS:
///    iInsertValue - The integer value represented by the texture
/// RETURNS:
///    
FUNC TEXT_LABEL_63 CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME(INT iInsertValue)//CMHDZ_SCORE_INTEGER_TYPE type, INT iInsertValue)
	TEXT_LABEL_63 tlTextureName
	tlTextureName = "FONT_"
	tlTextureName += iInsertValue

	RETURN tlTextureName
ENDFUNC

FUNC RGBA_COLOUR_STRUCT CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE type)//CMHDZ_SCORE_INTEGER_TYPE type, INT iInsertValue)	
	RGBA_COLOUR_STRUCT colReturn = sCMHDZData.rgbaWhite
	
	SWITCH type
		CASE CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE
			INIT_RGBA_STRUCT(colReturn, 0,0,255,255)
		BREAK
			
		CASE CMHDZ_SCORE_INTEGER_TYPE_MY_SCORE
			INIT_RGBA_STRUCT(colReturn, 255,171,0,255)
		BREAK
		
		CASE CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE
			INIT_RGBA_STRUCT(colReturn, 255,0,0,255)
		BREAK	
	ENDSWITCH
	
	
	RETURN colReturn
ENDFUNC

// See Alex's DEGENATRON_GAMES_DRAW_INTEGER
PROC CMHDZ_DRAW_SCORE_INTEGER(INT iValue, INT iDigits, VECTOR_2D vCenter,RGBA_COLOUR_STRUCT rgba, FLOAT fScale = 1.0)
	VECTOR_2D vCharCenter = vCenter
	VECTOR_2D vCharSize = INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_X_DIGIT_SIZE * fScale, cfCMHDZ_MY_HISCORE_Y_DIGIT_SIZE * fScale)
	BOOL bZero = (iValue = 0)
	INT iChar
	IF iDigits = 0
		INT currentValue
		currentValue = iValue
		IF iValue > 0
			iDigits = FLOOR(LOG10(TO_FLOAT(iValue)))+1
		ELIF iValue = 0
			iDigits = 1
		ENDIF
		

//		//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SCORE_INTEGER] {DSW} ")
//		//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SCORE_INTEGER] {DSW} iValue: ", iValue, " iDigits: ", iDigits, " Orig vCenter.x: ", vCenter.x, " vCharSize.x: ", vCharSize.x)
		WHILE currentValue > 0 OR bZero
			bZero = FALSE
			vCharCenter.x = vCenter.x + (vCharSize.x * (iDigits-1)/2.0) - (vCharSize.x * iChar)
//			//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SCORE_INTEGER] {DSW} currentValue: ", currentValue, " vCharCenter.x: ", vCharCenter.x, " iChar: ", iChar, " Drawing value: ",currentValue % 10)
			TEXT_LABEL_63 tlTextureName = CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME(currentValue % 10)
			currentValue /= 10
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), tlTextureName,vCharCenter,vCharSize,0.0, rgba)
			iChar++
	  ENDWHILE

	ELSE
		REPEAT iDigits iChar
			vCharCenter.x = vCenter.x + vCharSize.x * (iDigits-1)/2.0 - vCharSize.x * iChar
			TEXT_LABEL_63 tlTextureName = CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME((iValue / ROUND(POW(10.0,to_float(iChar)))) % 10)

			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), tlTextureName,vCharCenter,vCharSize,0.0, rgba)
		ENDREPEAT
	ENDIF
ENDPROC

PROC CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(INT iValue, VECTOR_2D vCenter, RGBA_COLOUR_STRUCT rgba, FLOAT fScale = 1.0) //CMHDZ_SCORE_INTEGER_TYPE scoreType, FLOAT fScale = 1.0)
//	VECTOR_2D vCharCenter = vCenter
//	VECTOR_2D vCharSize = INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_X_DIGIT_SIZE * fScale, cfCMHDZ_MY_HISCORE_Y_DIGIT_SIZE * fScale)
//	BOOL bZero = (iValue = 0)
	//INT iChar
//	INT iDigits = 0
	
	TEXT_LABEL_15 tl16Score
	
	tl16Score += "$"
	tl16Score += iValue
	CMHDZ_WRITE_STRING_USING_TEXT_SPRITES(tl16Score, vCenter, fScale, DEFAULT, DEFAULT, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	
	
//	CMHDZ_DRAW_DOLLAR_SYMBOL(vCenter, fScale, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
//	
//	IF iDigits = 0
//		INT currentValue
//		currentValue = iValue
//		IF iValue > 0
//			iDigits = FLOOR(LOG10(TO_FLOAT(iValue)))+1
//		ELIF iValue = 0
//			iDigits = 1
//		ENDIF
//		
//
//		//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SCORE_INTEGER] {DSW} ")
//		//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SCORE_INTEGER] {DSW} iValue: ", iValue, " iDigits: ", iDigits, " Orig vCenter.x: ", vCenter.x, " vCharSize.x: ", vCharSize.x)
//		WHILE currentValue > 0 OR bZero
//			bZero = FALSE
//			//vCharCenter.x = vCenter.x + (vCharSize.x * (iDigits-1)/2.0) - (vCharSize.x * iChar)
//			vCharCenter.x = vCenter.x + (vCharSize.x * (iChar + 1))
//			//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SCORE_INTEGER] {DSW} currentValue: ", currentValue, " vCharCenter.x: ", vCharCenter.x, " iChar: ", iChar, " Drawing value: ",currentValue % 10)
//			TEXT_LABEL_63 tlTextureName = CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME(currentValue % 10)
//			currentValue /= 10
//					CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), tlTextureName,vCharCenter,vCharSize,0.0, rgba)
//			iChar++
//	  ENDWHILE
//	  	
//	ENDIF
ENDPROC

//FUNC TEXT_LABEL_63 CMHDZ_GET_PERCENTAGE_SIGN_TEXTURE_FOR_SCORE_TYPE(CMHDZ_SCORE_INTEGER_TYPE type)
//	TEXT_LABEL_63 tlTextureName
//	
//	
//	SWITCH type
//
//		CASE CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE	tlTextureName = "FONT_%"			BREAK
//		CASE CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE	tlTextureName = "SCORE_TEXT_PERCENTAGE_RED"			BREAK	
//		
//	ENDSWITCH
//	
//	
//	RETURN tlTextureName
//ENDFUNC
//FUNC TEXT_LABEL_63 CMHDZ_GET_CROSS_CHAR_TEXTURE_FOR_SCORE_TYPE(CMHDZ_SCORE_INTEGER_TYPE type)
//	TEXT_LABEL_63 tlTextureName
//	
//	
//	SWITCH type
//
//		CASE CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE	tlTextureName = "SCORE_TEXT_X_BLUE"			BREAK
//		CASE CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE	tlTextureName = "SCORE_TEXT_X_RED"			BREAK	
//		
//	ENDSWITCH
//	
//	
//	RETURN tlTextureName
//ENDFUNC

PROC GET_SCORE_DIGITS_AS_ARRAY(INT iScore, INT iDigits, INT &ScoreArray[])

//	//CDEBUG2LN(DEBUG_MINIGAME, "[GET_SCORE_DIGITS_AS_ARRAY] {DSW} ")
//	//CDEBUG2LN(DEBUG_MINIGAME, "[GET_SCORE_DIGITS_AS_ARRAY] {DSW} iScore: ", iScore, " iDigits:", iDigits )
	INT iChar = 0
	INT i = 0
	FOR iChar = (iDigits-1) to 0 STEP - 1
		ScoreArray[i] = (iScore / ROUND(POW(10.0,to_float(iChar)))) % 10
	//	//CDEBUG2LN(DEBUG_MINIGAME, "[GET_SCORE_DIGITS_AS_ARRAY] {DSW} ScoreArray[", i, "] = ",ScoreArray[i] )
		i++
	ENDFOR	
	
ENDPROC

/// PURPOSE:
///    Draw the value specified where the 'center' value is the position of the 'left-most' (i.e. first) digit
/// PARAMS:
///    iValue - 
///    vLeftMostCenter - 
///    rgba - 
///    scoreType - 
///    fScale - 
PROC CMHDZ_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(INT iValue, VECTOR_2D vLeftMostCenter,RGBA_COLOUR_STRUCT rgba, FLOAT fScale = 1.0, BOOL bWithDollar = FALSE, BOOL bWithPercentage = FALSE)

	VECTOR_2D vCharSize = INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_X_DIGIT_SIZE * fScale, cfCMHDZ_MY_HISCORE_Y_DIGIT_SIZE * fScale)
	
	INT iScoreDigits[10]
	INT i
	INT iNumDigits = FLOOR(LOG10(TO_FLOAT(iValue))) + 1
	GET_SCORE_DIGITS_AS_ARRAY(iValue, iNumDigits, iScoreDigits)
	TEXT_LABEL_63 tlTextureName
	VECTOR_2D vCenter
	REPEAT iNumDigits i
		tlTextureName = CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME(iScoreDigits[i])
		vCenter.y = vLeftMostCenter.y
		vCenter.x = vLeftMostCenter.x + (i * vCharSize.x)
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),tlTextureName,vCenter,vCharSize,0.0, rgba)
	ENDREPEAT
	
	IF bWithDollar
		vCenter.x = vLeftMostCenter.x - (i * vCharSize.x)
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("$", vCenter)
	ENDIF
	
	// Ad percentage onto end
	IF bWithPercentage
	AND !bWithDollar
		vCenter.x += (vCharSize.x)
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("%", vCenter)
	ENDIF
	
ENDPROC

PROC CMHDZ_DRAW_SCORE_ZERO( VECTOR_2D vLeftMostCenter,RGBA_COLOUR_STRUCT rgba, FLOAT fScale = 1.0)//CMHDZ_SCORE_INTEGER_TYPE scoreType, FLOAT fScale = 1.0)
	VECTOR_2D vCharSize = INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_X_DIGIT_SIZE * fScale, cfCMHDZ_MY_HISCORE_Y_DIGIT_SIZE * fScale)

	TEXT_LABEL_63 tlTextureName = CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME(0)
	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),tlTextureName,vLeftMostCenter,vCharSize,0.0, rgba)
ENDPROC

//PROC CMHDZ_DRAW_SCORE_INTEGER_CENTER_RIGHT_MOST(INT iValue, VECTOR_2D vRightMostCenter,RGBA_COLOUR_STRUCT rgba, FLOAT fScale = 1.0, BOOL bWithDollar = FALSE)
//
//	VECTOR_2D vCharSize = INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_X_DIGIT_SIZE * fScale, cfCMHDZ_MY_HISCORE_Y_DIGIT_SIZE * fScale)
//	
//	INT iScoreDigits[10]
//	INT i
//	INT iNumDigits = FLOOR(LOG10(TO_FLOAT(iValue))) + 1
//	GET_SCORE_DIGITS_AS_ARRAY(iValue, iNumDigits, iScoreDigits)
//	TEXT_LABEL_63 tlTextureName
//	VECTOR_2D vCenter
//	REPEAT iNumDigits i
//		tlTextureName = CMHDZ_GET_SCORE_INTEGER_TEXTURE_NAME(iScoreDigits[i])
//		vCenter.y = vRightMostCenter.y
//		vCenter.x = vRightMostCenter.x - (i * vCharSize.x)
//		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),tlTextureName,vCenter,vCharSize,0.0, rgba)
//	ENDREPEAT
//	
//	IF bWithDollar
//		vCenter.x = vRightMostCenter.x - (i * vCharSize.x)
//		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("$", vCenter)
//	ENDIF
//	
//ENDPROC


//-------------------------------------------------
// CREATE FX
//-------------------------------------------------
FUNC INT CMHDZ_GET_NEXT_FX_INDEX()
	
	INT i
	FOR i = 0 TO ciCMHDZ_MAX_FX  -1
		IF sCMHDZData.sFXData[i].bActive = FALSE
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN 0
	
ENDFUNC

PROC TWS_CREATE_FX(VECTOR_2D vPos, CMHDZ_FX_TYPE eType, INT iLane)

	INT iIndex = CMHDZ_GET_NEXT_FX_INDEX()
	
	sCMHDZData.sFXData[iIndex].vSpritePos = vPos
	sCMHDZData.sFXData[iIndex].eFXType = eType
	IF iLane = ciCMHDZ_LANE_POPUP_LHS
	OR iLane = ciCMHDZ_LANE_POPUP_RHS
		iLane= ciCMHDZ_LANE_BACKGROUND
	ENDIF
	sCMHDZData.sFXData[iIndex].iLane = iLane
	sCMHDZData.sFXData[iIndex].iSpriteBlastAnimFrame = 0
	sCMHDZData.sFXData[iIndex].bActive = TRUE
	sCMHDZData.sFXData[iIndex].bFinished = FALSE
	sCMHDZData.sFXData[iIndex].fAlpha = TO_FLOAT(sCMHDZData.rgbaSprite.iA) // Standard
	
ENDPROC


PROC CMHDZ_HANDLE_FX()

	INT i
	FOR i = 0  TO ciCMHDZ_MAX_FX - 1
		IF sCMHDZData.sFXData[i].bActive
			
			sCMHDZData.sFXData[i].iSpriteBlastAnimFrame += sCMHDZData.sFXData[i].iSlowUpdateFrames
		//	//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_BULLETS_AND_EFFECTS] {DSW} Frame: ", sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
			IF IS_MOVING_LEVEL() AND sCMHDZData.eClientState = CMHDZ_ARCADE_CLIENT_STATE_PLAYING
				sCMHDZData.sFXData[i].vSpritePos.x -= CMHDZ_GET_FX_UPDATE()
			ENDIF
			
			SWITCH sCMHDZData.sFXData[i].eFXType
				CASE CMHDZ_FX_ENEMY_SHOT
				
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_ENEMY_SHOT_MAX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF
				BREAK
				CASE CMHDZ_FX_EXPLOSION
				
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_EXPLOSION_MAX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF
				BREAK
				
				CASE CMHDZ_FX_ENEMY_TNT_EXP
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_MAX_ENEMY_THROWN_EXPLOSION_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF
				BREAK 
				
				CASE CMHDZ_FX_LOOT_CRATE
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_MAX_LOOT_CRATE_FX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF 
				BREAK
				
//				CASE CMHDZ_FX_BLOOD_SPLAT
//					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_BLOOD_SPLAT_MAX_ANIM_FRAMES
//						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
//						sCMHDZData.sFXData[i].bActive = FALSE
//					ENDIF 
//				BREAK
				
				CASE CMHDZ_FX_BEARSY_FOREGROUND_PAIN_BLOOD
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_BEARSY_BLEED_MAX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF
				BREAK
				
				
				CASE CMHDZ_FX_BEARSY_FOREGROUND_SPARKS
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_SPARKS_MAX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF
				BREAK		
				
				CASE CMHDZ_FX_OUCH
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= 3
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bActive = FALSE
					ENDIF
				BREAK	
				
//				CASE CMHDZ_FX_SLASH_EFFECT
//					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_BLOOD_SPLAT_MAX_ANIM_FRAMES
//						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
//						sCMHDZData.sFXData[i].bActive = FALSE
//					ENDIF
//				BREAK
			ENDSWITCH	
		ENDIF
	ENDFOR
	
ENDPROC


//-------------------------------------------------
// BACKGROUND FUNCTIONS
//-------------------------------------------------

FUNC BOOL CMHDZ_IS_CLOUD(STRING sCloud)
	RETURN ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_DESERT_CLOUDS_TILE")
	
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_01")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_02")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_03")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_04")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_05")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_06")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_SCRAPYARD_CLOUD_07")
			
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_01")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_02")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_03")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_04")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_05")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_06")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_02_TOWN_BACKGROUND_CLOUDS_07")
			
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_01")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_02")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_03")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_04")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_05")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_06")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_03_FOREST_BACKGROUND_CLOUDS_07")
ENDFUNC

FUNC BOOL CMHDZ_IS_CITY_BACKGROUND_BUILDING(STRING sCloud)
	RETURN ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_CITY_BUILDING_01")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_CITY_BUILDING_02")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_CITY_BUILDING_03")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_CITY_BUILDING_04")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_CITY_BUILDING_05")
			OR ARE_STRINGS_EQUAL(sCloud, "LEVEL_01_CITY_BUILDING_06")
ENDFUNC

FUNC BOOL CMHDZ_IS_THEMEPARK_BACKGROUND_SKYLINE(STRING sTexture)
	RETURN ARE_STRINGS_EQUAL(sTexture, "LEVEL_04_THEMEPARK_BACKGROUND_SKYLINE")
ENDFUNC

FUNC INT CMHDZ_GET_FIRST_BACKGROUND_ELEMENT_OF_TYPE(STRING sType)
	INT i
	FOR i = 0 TO ciCMHDZ_MAX_BACKGROUND_ELEMS - 1
		IF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, sType)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC


FUNC INT CMHDZ_GET_NUMBER_OF_BACKGROUND_ELEMENTS_OF_TYPE(STRING sType, INT iFirstElement)
	INT iCount
	INT i
	FOR i = iFirstElement TO ciCMHDZ_MAX_BACKGROUND_ELEMS - 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sType)
			IF NOT IS_STRING_NULL_OR_EMPTY(sCMHDZData.sBackgroundData[i].sSprite)
				IF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, sType)
					iCount++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
//	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_GET_NUMBER_OF_BACKGROUND_ELEMENTS_OF_TYPE] Type: ", sType, " Count: ", iCount)
	RETURN iCount
ENDFUNC 

/// PURPOSE:
///    Figure out where to warp the background element specified when it disappears off the screen LHS
/// PARAMS:
///    iElement - 
/// RETURNS:
///    
FUNC FLOAT GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(INT iElement)
	FLOAT fWidth = sCMHDZData.sBackgroundData[iElement].vSpriteSize.x
	INT iFirst = CMHDZ_GET_FIRST_BACKGROUND_ELEMENT_OF_TYPE(sCMHDZData.sBackgroundData[iElement].sSprite)
	INT iTotal = CMHDZ_GET_NUMBER_OF_BACKGROUND_ELEMENTS_OF_TYPE(sCMHDZData.sBackgroundData[iElement].sSprite, iFirst)
	INT iPrevious = iElement -1 
	IF iPrevious < iFirst
		iPrevious = iFirst + (iTotal - 1) //(ciCMHDZ_MAX_BACKGROUND_TILES - 1)
	ENDIF
	
	RETURN sCMHDZData.sBackgroundData[iPrevious].vSpritePos.X + fWidth
ENDFUNC

PROC CMHDZ_DRAW_FAR_BACKGROUND(BOOL bBackwards = FALSE)//, BOOL bLeaderBoard = FALSE)
	TEXT_LABEL_63 tl23BackgroundSprite
	TEXT_LABEL_31 tl31textDict //= CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_CITY)
	
	SWITCH sCMHDZData.eCurrentLevel
		CASE CMHDZ_LEVEL_CITY
			tl23BackgroundSprite = "LEVEL_01_CITY_BACKGROUND_TILE" // "bg-tile1" //
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_CITY)
		BREAK
		
		CASE CMHDZ_LEVEL_SCRAPYARD
			tl23BackgroundSprite = "LEVEL_02_SCRAPYARD_BACKGROUND_TILE" //"bg-tile2"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_SCRAPYARD)
		BREAK
		
		CASE CMHDZ_LEVEL_ASYLUM
			tl23BackgroundSprite = "LEVEL_03_ASYLUM_BACKGROUND_TILE" //"bg-tile3"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_ASYLUM)
		BREAK
		
		CASE CMHDZ_LEVEL_PARK
			tl23BackgroundSprite = "LEVEL_04_THEMEPARK_BACKGROUND_TILE" //"bg-tile4"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_THEMEPARK)
		BREAK
		
		CASE CMHDZ_LEVEL_SLAUGHTERHOUSE
			tl23BackgroundSprite = "LEVEL_05_SLAUGHTERHOUSE_BACKGROUND_TILE" //"bg-tile5"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL5)
		BREAK
	ENDSWITCH
	
//	IF (bLeaderBoard)
//		tl23BackgroundSprite = "LEVEL_01_CITY_BACKGROUND_TILE" // "bg-tile1" //
//		tl31textDict =  CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_CITY)	
//	ENDIF
	
	INT i
	INT iPrevious
	VECTOR_2D vDrawPos
	FLOAT xFlip
	
	// bg
	FOR i = 0 TO ciCMHDZ_MAX_BACKGROUND_TILES - 1
			
		vDrawPos = INIT_VECTOR_2D(sCMHDZData.sBackgroundTilesData[i].vSpritePos.x, cfBASE_SCREEN_HEIGHT/2)
		IF (bBackwards)
			xFlip = -(vDrawPos.x - (cfBASE_SCREEN_WIDTH / 2)) 
			vDrawPos.x = (cfBASE_SCREEN_WIDTH / 2) + xFlip
		ENDIF
		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl23BackgroundSprite, vDrawPos, INIT_VECTOR_2D(276.0, cfGAME_SCREEN_HEIGHT), 0, sCMHDZData.rgbaSprite)
				
		IF sCMHDZData.sBackgroundTilesData[i].vSpritePos.x < cfCMHDZ_BCAKGROUND_TILE_WIDTH/2
			iPrevious = i - 1
			IF iPrevious < 0
				iPrevious = ciCMHDZ_MAX_BACKGROUND_TILES - 1
			ENDIF

			sCMHDZData.sBackgroundTilesData[i].vSpritePos.x = sCMHDZData.sBackgroundTilesData[iPrevious].vSpritePos.x + cfCMHDZ_BCAKGROUND_TILE_WIDTH
		ENDIF
		
		IF IS_MOVING_LEVEL()
		AND CMHDZ_GET_LOCAL_PLAYER_HEALTH() > 0.0
			sCMHDZData.sBackgroundTilesData[i].vSpritePos.x -= CMHDZ_GET_BACKGROUND_TILE_UPDATE()	
		ENDIF
	ENDFOR
	
	// lane 0
	FLOAT fCloudSpace, fClouds 
	
	REPEAT ciCMHDZ_MAX_BACKGROUND_ELEMS i
		IF ((sCMHDZData.sBackgroundData[i].iLane = ciCMHDZ_LANE_BACKGROUND) AND CMHDZ_BACKGROUND_DATA_IS_RENDERING(i))
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, sCMHDZData.sBackgroundData[i].sSprite, sCMHDZData.sBackgroundData[i].vSpritePos, sCMHDZData.sBackgroundData[i].vSpriteSize, 0, sCMHDZData.rgbaSprite)
		
			IF CMHDZ_IS_THEMEPARK_BACKGROUND_SKYLINE(sCMHDZData.sBackgroundData[i].sSprite)
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X < (cfGAME_OVERSCAN_MIN_X) - (cfCMHDZ_BG_THEMEPARK_SKYLINE_SPRITE_WIDTH / 2.0)
					sCMHDZData.sBackgroundData[i].vSpritePos.X += (cfCMHDZ_BG_THEMEPARK_SKYLINE_SPRITE_WIDTH * 2.0)
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()				
				ENDIF			
			ELIF CMHDZ_IS_CLOUD(sCMHDZData.sBackgroundData[i].sSprite)
				IF sCMHDZData.eCurrentLevel = CMHDZ_LEVEL_SCRAPYARD
					fCloudSpace = cfCMHDZ_BG_SCRAPYARD_CLOUD_GAP
					fClouds = cfCMHDZ_BG_SCRAPYARD_CLOUD1_WIDTH + fCloudSpace + cfCMHDZ_BG_SCRAPYARD_CLOUD2_WIDTH + fCloudSpace + cfCMHDZ_BG_SCRAPYARD_CLOUD3_WIDTH + fCloudSpace + cfCMHDZ_BG_SCRAPYARD_CLOUD4_WIDTH + fCloudSpace + cfCMHDZ_BG_SCRAPYARD_CLOUD5_WIDTH + fCloudSpace + cfCMHDZ_BG_SCRAPYARD_CLOUD6_WIDTH + fCloudSpace + cfCMHDZ_BG_SCRAPYARD_CLOUD7_WIDTH
					IF sCMHDZData.sBackgroundData[i].vSpritePos.X + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 ) <= 300.0
						sCMHDZData.sBackgroundData[i].vSpritePos.X = 300.0 + fClouds + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CLOUD_UPDATE()
					ENDIF			
				ENDIF 
			ELSE
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X < (cfGAME_OVERSCAN_MIN_X) - cfCMHDZ_BCAKGROUND_TILE_WIDTH/2
					sCMHDZData.sBackgroundData[i].vSpritePos.X += cfBASE_SCREEN_WIDTH
					
					IF (sCMHDZData.eCurrentLevel = CMHDZ_LEVEL_CITY)
						IF CMHDZ_IS_CITY_BACKGROUND_BUILDING(sCMHDZData.sBackgroundData[i].sSprite)
							CMHDZ_SET_RANDOM_BG_CITY_BACKGROUND_BUILDING_ELEMENT(i)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()				
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT 
	
	// lane 1
	REPEAT ciCMHDZ_MAX_BACKGROUND_ELEMS i
		IF ((sCMHDZData.sBackgroundData[i].iLane = ciCMHDZ_LANE_MIDDLE) AND CMHDZ_BACKGROUND_DATA_IS_RENDERING(i))
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, sCMHDZData.sBackgroundData[i].sSprite, sCMHDZData.sBackgroundData[i].vSpritePos, sCMHDZData.sBackgroundData[i].vSpriteSize, 0, sCMHDZData.rgbaSprite)
			
			IF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_04_THEMEPARK_BACKGROUND_PROP_MINECART_01")
				IF (sCMHDZData.sBackgroundData[i].vSpritePos.x < (cfGAME_OVERSCAN_MIN_X) - (cfCMHDZ_THEMEPARK_MINECART_TRACK_SPRITE_WIDTH / 2.0))
					sCMHDZData.sBackgroundData[i].vSpritePos.X += (cfCMHDZ_THEMEPARK_MINECART_TRACK_SPRITE_WIDTH * 6.0)
				ENDIF				
				
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CUSTOM_FAST_NEAR_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			// Scrapyard rock objects.
			ELIF ARE_STRINGS_EQUAL("LEVEL_02_SCRAPYARD_ROCKS", sCMHDZData.sBackgroundData[i].sSprite)
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = 1980 //fClouds// + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_GENERAL_UPDATE()
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC 

PROC CMHDZ_DRAW_BACKGROUND(BOOL bBackwards = FALSE)
	IF (sCMHDZData.eCurrentLevel <= CMHDZ_LEVEL_PARK)
		CMHDZ_DRAW_FAR_BACKGROUND(bBackwards)
		EXIT
	ENDIF
	
	VECTOR_2D vDrawPos
	FLOAT xFlip
	
	TEXT_LABEL_63 tl23BackgroundSprite
	TEXT_LABEL_31 tl31textDict //= sCMHDZData.tl31BaseTextDict
	SWITCH sCMHDZData.eCurrentLevel
		CASE CMHDZ_LEVEL_CITY
			tl23BackgroundSprite = "LEVEL_01_DESERT_BACKGROUND_TILE" // "bg-tile1" //
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL1)
		BREAK
		
		CASE CMHDZ_LEVEL_SCRAPYARD
			tl23BackgroundSprite =  "LEVEL_02_TOWN_BACKGROUND_TILE" //"bg-tile2"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL2)
		BREAK
		
		CASE CMHDZ_LEVEL_ASYLUM
			tl23BackgroundSprite = "LEVEL_03_FOREST_BACKGROUND_TILE" //"bg-tile3"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL3)
		BREAK
		
		CASE CMHDZ_LEVEL_PARK
			tl23BackgroundSprite = "LEVEL_04_GOLDMINE_BACKGROUND_TILE" //"bg-tile4"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL4)
		BREAK
		
		CASE CMHDZ_LEVEL_SLAUGHTERHOUSE
			tl23BackgroundSprite = "LEVEL_05_SLAUGHTERHOUSE_BACKGROUND_TILE" //"bg-tile5"
			tl31textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL5)
		BREAK
	ENDSWITCH
	
	INT i
	INT iPrevious
	//INT iFirst
	FOR i = 0 TO ciCMHDZ_MAX_BACKGROUND_TILES - 1
		vDrawPos = INIT_VECTOR_2D(sCMHDZData.sBackgroundTilesData[i].vSpritePos.x, cfBASE_SCREEN_HEIGHT/2)
		IF (bBackwards)
			xFlip = -(vDrawPos.x -(cfBASE_SCREEN_WIDTH / 2)) 
			vDrawPos.x = (cfBASE_SCREEN_WIDTH / 2) + xFlip
		ENDIF
		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl23BackgroundSprite, vDrawPos, INIT_VECTOR_2D(cfCMHDZ_BCAKGROUND_TILE_WIDTH, cfGAME_SCREEN_HEIGHT), 0, sCMHDZData.rgbaSprite)

		IF sCMHDZData.sBackgroundTilesData[i].vSpritePos.x < cfCMHDZ_BCAKGROUND_TILE_WIDTH/2
			iPrevious = i - 1
			IF iPrevious < 0
				iPrevious =  ciCMHDZ_MAX_BACKGROUND_TILES - 1
			ENDIF
			//sCMHDZData.sBackgroundTilesData[i].vSpritePos.x = (cfCMHDZ_BCAKGROUND_TILE_WIDTH * 7) - cfCMHDZ_BCAKGROUND_TILE_WIDTH/2 - (4.0 * CMHDZ_GET_PREVIOUS_TIMESTEP())
			sCMHDZData.sBackgroundTilesData[i].vSpritePos.x = sCMHDZData.sBackgroundTilesData[iPrevious].vSpritePos.x + cfCMHDZ_BCAKGROUND_TILE_WIDTH
		ENDIF
		
		IF IS_MOVING_LEVEL()
		AND CMHDZ_GET_LOCAL_PLAYER_HEALTH() > 0.0 
			sCMHDZData.sBackgroundTilesData[i].vSpritePos.x -= CMHDZ_GET_BACKGROUND_TILE_UPDATE()
		ENDIF
		
	ENDFOR
	
	FLOAT fClouds
	FLOAT fCloudSpace
	
	sCMHDZData.iNumberOfBackgroundElementsDrawn = 0
	// Far canyons
	FOR i = 0 TO ciCMHDZ_MAX_BACKGROUND_ELEMS - 1
//		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVL2), "mountains", GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfCMHDZ_MOUNTAIN_1_SPRITE_WIDTH, cfCMHDZ_MOUNTAIN_1_SPRITE_HEIGHT), 0, sCMHDZData.rgbaSprite)
//		CMHDZ_DRAW_GAME_SPRITE(sCMHDZData.tl31BaseTextDict, "LEVEL_05_GRAVEYARD_BACKGROUND_TILE_TREES", GET_DEBUG_TEXTURE_POS(1), INIT_VECTOR_2D(cfCMHDZ_SLAUGHTERHOUSE_LIGHTS_WIDTH, cfCMHDZ_SLAUGHTERHOUSE_LIGHTS_HEIGHT), 0, sCMHDZData.rgbaSprite)
//		CMHDZ_DRAW_GAME_SPRITE(sCMHDZData.tl31BaseTextDict, "LEVEL_05_GRAVEYARD_BACKGROUND_TILE_GATE", GET_DEBUG_TEXTURE_POS(2), INIT_VECTOR_2D(cfCMHDZ_SLAUGHTERHOUSE_FENCE_WIDTH, cfCMHDZ_SLAUGHTERHOUSE_FENCE_HEIGHT), 0, sCMHDZData.rgbaSprite)
				
		IF (sCMHDZData.sBackgroundData[i].iLane = ciCMHDZ_LANE_BACKGROUND) AND CMHDZ_BACKGROUND_DATA_IS_RENDERING(i)
			sCMHDZData.iNumberOfBackgroundElementsDrawn ++
			
			vDrawPos = sCMHDZData.sBackgroundData[i].vSpritePos
			IF (bBackwards)
				xFlip = -(vDrawPos.x -(cfBASE_SCREEN_WIDTH / 2)) 
				vDrawPos.x = (cfBASE_SCREEN_WIDTH / 2) + xFlip
			ENDIF
					
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, sCMHDZData.sBackgroundData[i].sSprite, vDrawPos, sCMHDZData.sBackgroundData[i].vSpriteSize, 0, sCMHDZData.rgbaSprite)
			
			IF CMHDZ_IS_CLOUD(sCMHDZData.sBackgroundData[i].sSprite)
				IF CMHDZ_GET_CURRENT_LEVEL() = CMHDZ_LEVEL_CITY
					IF sCMHDZData.sBackgroundData[i].vSpritePos.X + (cfCMHDZ_LEVEL1_CLOUD_WIDTH / 2.0 ) <= 0
						sCMHDZData.sBackgroundData[i].vSpritePos.X = cfCMHDZ_LEVEL1_CLOUD_WIDTH + cfCMHDZ_LEVEL1_CLOUD_WIDTH + (cfCMHDZ_LEVEL1_CLOUD_WIDTH / 2.0 )
						//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] {DSW} Warping cloud ", i, " New Pos x: ", sCMHDZData.sBackgroundData[i].vSpritePos.X)
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CLOUD_UPDATE() 
					ENDIF
				ELIF CMHDZ_GET_CURRENT_LEVEL() = CMHDZ_LEVEL_SCRAPYARD
					fCloudSpace = cfCMHDZ_LEVEL2_CLOUD_GAP
					fClouds = cfCMHDZ_LEVEL2_CLOUD1_WIDTH + fCloudSpace + cfCMHDZ_LEVEL2_CLOUD2_WIDTH + fCloudSpace + cfCMHDZ_LEVEL2_CLOUD3_WIDTH + fCloudSpace + cfCMHDZ_LEVEL2_CLOUD4_WIDTH + fCloudSpace + cfCMHDZ_LEVEL2_CLOUD5_WIDTH + fCloudSpace + cfCMHDZ_LEVEL2_CLOUD6_WIDTH + fCloudSpace + cfCMHDZ_LEVEL2_CLOUD7_WIDTH
					IF sCMHDZData.sBackgroundData[i].vSpritePos.X + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 ) <= 300.0
						sCMHDZData.sBackgroundData[i].vSpritePos.X = 300.0 + fClouds + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CLOUD_UPDATE()
					ENDIF
				ELIF CMHDZ_GET_CURRENT_LEVEL() = CMHDZ_LEVEL_ASYLUM
					fCloudSpace = cfCMHDZ_LEVEL3_CLOUD_GAP
					fClouds = cfCMHDZ_LEVEL3_CLOUD1_WIDTH + fCloudSpace + cfCMHDZ_LEVEL3_CLOUD2_WIDTH + fCloudSpace + cfCMHDZ_LEVEL3_CLOUD3_WIDTH + fCloudSpace + cfCMHDZ_LEVEL3_CLOUD4_WIDTH + fCloudSpace + cfCMHDZ_LEVEL3_CLOUD5_WIDTH + fCloudSpace + cfCMHDZ_LEVEL3_CLOUD6_WIDTH + fCloudSpace + cfCMHDZ_LEVEL3_CLOUD7_WIDTH
					IF sCMHDZData.sBackgroundData[i].vSpritePos.X + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 ) <= 300.0
						sCMHDZData.sBackgroundData[i].vSpritePos.X = 300.0 + fClouds + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
					ENDIF
					
					IF IS_MOVING_LEVEL()
						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CLOUD_UPDATE()
					ENDIF
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "mountains")
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X + (cfCMHDZ_MOUNTAIN_1_SPRITE_WIDTH / 2.0 ) <= 0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = cfCMHDZ_MOUNTAIN_1_SPRITE_WIDTH + cfCMHDZ_MOUNTAIN_1_SPRITE_WIDTH + (cfCMHDZ_MOUNTAIN_1_SPRITE_WIDTH / 2.0 )
					//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] {DSW} Warping mountains ", i, " New Pos x: ", sCMHDZData.sBackgroundData[i].vSpritePos.X)
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF				
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_03_FOREST_BACKGROUND_TREES")
				fClouds = 7 * cfCMHDZ_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds
				ENDIF
				
			//	//CPRINTLN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] Level 3 trees : ", i)
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_04_GOLDMINE_CAVE_BACKGROUND_CAVE")
				fClouds = 7 * cfCMHDZ_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CLOUD_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_04_GOLDMINE_CAVE_TILE_02")
				fClouds = 7 * cfCMHDZ_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_05_SLAUGHTERHOUSE_PROP_DOOR_02")
				fClouds = 7 * cfCMHDZ_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds// + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CLOUD_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_05_SLAUGHTERHOUSE_PROP_DOOR_01")
				fClouds = 7 * cfCMHDZ_LEVEL4_BACKGROUND_SURFACE_WIDTH
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds// + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
		
			ELSE
				
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X < cfCMHDZ_BCAKGROUND_TILE_WIDTH/2
					sCMHDZData.sBackgroundData[i].vSpritePos.X = cfBASE_SCREEN_WIDTH
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	// Close canyons
	FOR i = 0 TO ciCMHDZ_MAX_BACKGROUND_ELEMS - 1
		
		IF (sCMHDZData.sBackgroundData[i].iLane = ciCMHDZ_LANE_MIDDLE) AND CMHDZ_BACKGROUND_DATA_IS_RENDERING(i)
		
			sCMHDZData.iNumberOfBackgroundElementsDrawn ++
			//sCMHDZData.sBackgroundData[i].vSpritePos.y = sCMHDZData.fTexturePosY[0]

			vDrawPos = sCMHDZData.sBackgroundData[i].vSpritePos
			IF (bBackwards)
				xFlip = -(vDrawPos.x -(cfBASE_SCREEN_WIDTH / 2)) 
				vDrawPos.x = (cfBASE_SCREEN_WIDTH / 2) + xFlip
			ENDIF
			
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, sCMHDZData.sBackgroundData[i].sSprite, vDrawPos, sCMHDZData.sBackgroundData[i].vSpriteSize, 0, sCMHDZData.rgbaSprite)
			
			IF ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_CAVE_TILE", sCMHDZData.sBackgroundData[i].sSprite)
			OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_ASSETS_10", sCMHDZData.sBackgroundData[i].sSprite) 
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CUSTOM_FAST_NEAR_BACKGROUND_ELEMENT_UPDATE()
				ENDIF				
			ELIF ARE_STRINGS_EQUAL("fence-small", sCMHDZData.sBackgroundData[i].sSprite)
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) 
				ENDIF
				
				IF IS_MOVING_LEVEL()
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_DEFAULT_NEAR_BACKGROUND_ELEMENT_UPDATE()
				ENDIF
			ELIF ARE_STRINGS_EQUAL(sCMHDZData.sBackgroundData[i].sSprite, "LEVEL_05_SLAUGHTERHOUSE_LIGHT")
				fClouds = 1 * cfCMHDZ_LEVEL4_BACKGROUND_SURFACE_WIDTH
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X  <= 0.0
					sCMHDZData.sBackgroundData[i].vSpritePos.X = GET_WARP_POSITION_FOR_BACKGROUND_ELEMENT(i) //fClouds// + (sCMHDZData.sBackgroundData[i].vSpriteSize.x / 2.0 )
				ENDIF
				IF IS_MOVING_LEVEL()
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] LEVEL_05_SLAUGHTERHOUSE_LIGHT - X Position update")
					sCMHDZData.sBackgroundData[i].vSpritePos.X -= (CMHDZ_GET_GENERAL_UPDATE())
				ENDIF
				
			ELSE
				
				IF sCMHDZData.sBackgroundData[i].vSpritePos.X < cfCMHDZ_BCAKGROUND_TILE_WIDTH/2
					sCMHDZData.sBackgroundData[i].vSpritePos.X = cfBASE_SCREEN_WIDTH
				ENDIF
				
				IF IS_MOVING_LEVEL()
					IF ARE_STRINGS_EQUAL("TERRAIN_ROCKS", sCMHDZData.sBackgroundData[i].sSprite) 
					OR ARE_STRINGS_EQUAL("FLOOR_GRASS", sCMHDZData.sBackgroundData[i].sSprite)
					//OR ARE_STRINGS_EQUAL("LEVEL_02_SCRAPYARD_ROCKS", sCMHDZData.sBackgroundData[i].sSprite)
					OR ARE_STRINGS_EQUAL("LEVEL_03_FOREST_ROCKS", sCMHDZData.sBackgroundData[i].sSprite)
					OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_ROCK", sCMHDZData.sBackgroundData[i].sSprite)
					OR ARE_STRINGS_EQUAL("WOODENSTRUCTURE", sCMHDZData.sBackgroundData[i].sSprite)
			//		OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_ASSETS_10", sCMHDZData.sBackgroundData[i].sSprite)
			//		OR ARE_STRINGS_EQUAL("LEVEL_04_GOLDMINE_CAVE_TILE", sCMHDZData.sBackgroundData[i].sSprite)

						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CUSTOM_FAST_NEAR_BACKGROUND_ELEMENT_UPDATE()
					ELIF ARE_STRINGS_EQUAL("mountain-small1", sCMHDZData.sBackgroundData[i].sSprite) 
					OR ARE_STRINGS_EQUAL("mountain-small2", sCMHDZData.sBackgroundData[i].sSprite) 
						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_CUSTOM_SLOW_NEAR_BACKGROUND_ELEMENT_UPDATE()
					ELSE
						sCMHDZData.sBackgroundData[i].vSpritePos.X -= CMHDZ_GET_DEFAULT_NEAR_BACKGROUND_ELEMENT_UPDATE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
		

ENDPROC
	
//-------------------------------------------------
// HOSTAGE FUNCTIONS
//-------------------------------------------------
//PROC CMHDZ_DRAW_HOSTAGES(INT iLane)
//
//	CMHDZ_SPRITE eSprite 
//	
//	INT i
//	
//	FOR i = 0 TO ciCMHDZ_MAX_HOSTAGES -1
//		
//		IF sCMHDZData.sHostageData[i].iHostageLane = iLane
//			IF sCMHDZData.sHostageData[i].iHealth > 0
//				SWITCH sCMHDZData.sHostageData[i].eHostageType
//				CASE CMHDZ_HOSTAGE_GIRL
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_HOSTAGE11, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				CASE CMHDZ_HOSTAGE_GUY
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_HOSTAGE21, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				CASE CMHDZ_HOSTAGE_EAGLE
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_EAGLE1, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				CASE CMHDZ_HOSTAGE_VULTURE
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_VULTURE_01, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				CASE CMHDZ_HOSTAGE_BAT
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_BAT_01, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				CASE CMHDZ_HOSTAGE_SCORPION
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_ANIMALBONUS_SCORPION_01, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				CASE CMHDZ_HOSTAGE_MOUSE
//					eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_ANIMALBONUS_RAT_01, sCMHDZData.sHostageData[i].iSpriteAnimFrame)
//				BREAK
//				ENDSWITCH
//				
//				IF sCMHDZData.sHostageData[i].bGoingBack AND (sCMHDZData.sHostageData[i].eHostageType != CMHDZ_HOSTAGE_EAGLE AND sCMHDZData.sHostageData[i].eHostageType != CMHDZ_HOSTAGE_VULTURE)
//					CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(-sCMHDZData.sHostageData[i].vSize.x, sCMHDZData.sHostageData[i].vSize.y), 0, sCMHDZData.rgbaSprite)
//				ELSE
//					CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(sCMHDZData.sHostageData[i].vSize.x, sCMHDZData.sHostageData[i].vSize.y), 0, sCMHDZData.rgbaSprite)
//				ENDIF
//					
//			ELIF sCMHDZData.sHostageData[i].bDying
//				SWITCH sCMHDZData.sHostageData[i].eHostageType
//				CASE CMHDZ_HOSTAGE_GIRL
//					eSprite = CMHDZ_SPRITE_OUCH1
//					CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_SPEECH_SPRITE_WIDTH, cfCMHDZ_SPEECH_SPRITE_HEIGHT), 0, sCMHDZData.rgbaSprite)
//				BREAK
//				CASE CMHDZ_HOSTAGE_GUY
//					eSprite = CMHDZ_SPRITE_OUCH1
//					CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sHostageData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_SPEECH_SPRITE_WIDTH, cfCMHDZ_SPEECH_SPRITE_HEIGHT), 0, sCMHDZData.rgbaSprite)
//				BREAK
//				ENDSWITCH
//			ENDIF
//		ENDIF
//		
//	ENDFOR
//		
//ENDPROC

//-------------------------------------------------
// COVER FUNCTIONS
//-------------------------------------------------

FUNC CMHDZ_SPRITE CMHDZ_COVER_UTIL_GET_SPRITE_FOR_TYPE(CMHDZ_ASSET_TYPES eType, INT iHitCount = 0)
	CMHDZ_SPRITE eSprite = CMHDZ_SPRITE_NONE 
	
	SWITCH (eType)
		CASE CMHDZ_ASSET_CACTUS_1
			eSprite = CMHDZ_SPRITE_CACTUS_1
		BREAK
		CASE CMHDZ_ASSET_CACTUS_2
			eSprite = CMHDZ_SPRITE_CACTUS_2
		BREAK
		CASE CMHDZ_ASSET_CACTUS_3
			eSprite = CMHDZ_SPRITE_CACTUS_3
		BREAK
		CASE CMHDZ_ASSET_ASYLUM_MD_BED
			eSprite = CMHDZ_SPRITE_ASYLUM_PROP_MD_BED
		BREAK
		
		CASE CMHDZ_ASSET_ASYLUM_MD_WHEELCHAIR			
			eSprite = CMHDZ_SPRITE_ASYLUM_PROP_MD_WHEELCHAIR
		BREAK
		CASE CMHDZ_ASSET_ASYLUM_MD_MATTRESS			
			eSprite = CMHDZ_SPRITE_ASYLUM_PROP_MD_MATTRESS
		BREAK
		CASE CMHDZ_ASSET_ASYLUM_LIGHT_1			
			eSprite = CMHDZ_SPRITE_ASYLUM_PROP_LIGHT_STATE_1
		BREAK
		CASE CMHDZ_ASSET_ASYLUM_LIGHT_2			
			eSprite = CMHDZ_SPRITE_ASYLUM_PROP_LIGHT_STATE_2
		BREAK
		
        CASE CMHDZ_ASSET_ASYLUM_BG_RADIATOR	
			eSprite = CMHDZ_SPRITE_ASYLUM_RADIATOR_PROP
		BREAK
		
        CASE CMHDZ_ASSET_ASYLUM_BG_DOOR 				
			eSprite = CMHDZ_SPRITE_ASYLUM_RADIATOR_DOOR
		BREAK
		
        CASE CMHDZ_ASSET_ASYLUM_GRAFFITI_1
			eSprite = CMHDZ_SPRITE_ASYLUM_GRAFFITI_1
		BREAK
		
        CASE CMHDZ_ASSET_ASYLUM_GRAFFITI_2				
			eSprite = CMHDZ_SPRITE_ASYLUM_GRAFFITI_2
		BREAK
		
        CASE CMHDZ_ASSET_ASYLUM_GRAFFITI_3				
			eSprite = CMHDZ_SPRITE_ASYLUM_GRAFFITI_3
		BREAK

		CASE CMHDZ_ASSET_CITY_BUILDING_1
			eSprite = CMHDZ_SPRITE_CITY_BUILDING_06
		BREAK
		CASE CMHDZ_ASSET_CITY_BUILDING_2
			eSprite = CMHDZ_SPRITE_CITY_BUILDING_07
		BREAK
		CASE CMHDZ_ASSET_CITY_BUILDING_3
			eSprite = CMHDZ_SPRITE_CITY_BUILDING_08
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_VAN_1
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_VAN_01
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_VAN_2
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_VAN_02
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_VAN_3
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_VAN_03
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_VAN_4
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_VAN_04
		BREAK		
		CASE CMHDZ_ASSET_THEMEPARK_BUILDING_1
			eSprite = CMHDZ_SPRITE_THEMEPARK_BUILDING_1
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_BUILDING_2
			eSprite = CMHDZ_SPRITE_THEMEPARK_BUILDING_2
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_BUILDING_3
			eSprite = CMHDZ_SPRITE_THEMEPARK_BUILDING_3
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_BUILDING_4
			eSprite = CMHDZ_SPRITE_THEMEPARK_BUILDING_4
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_CLOWNBOARD_1
			eSprite = CMHDZ_SPRITE_THEMEPARK_CLOWNBOARD_1
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_CLOWNBOARD_2
			eSprite = CMHDZ_SPRITE_THEMEPARK_CLOWNBOARD_2
		BREAK		
		CASE CMHDZ_ASSET_FENCE
			eSprite = CMHDZ_SPRITE_FENCE
		BREAK
//		CASE CMHDZ_ASSET_FENCE_SMALL
//			eSprite = CMHDZ_SPRITE_FENCE_SMALL
//		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_CAR_1
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_CAR_01
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_CAR_2
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_CAR_02
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_CAR_3
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_CAR_03
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_MD_CAR_4
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_MD_CAR_04
		BREAK
		CASE CMHDZ_ASSET_CACTUS_9
			eSprite = CMHDZ_SPRITE_PROP_CACTUS_07
		BREAK
		CASE CMHDZ_ASSET_SCRAPYARD_PROP_FENCE
			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_FENCE
		BREAK
//		CASE CMHDZ_ASSET_SCRAPYARD_BG_CARS
//			eSprite = CMHDZ_SPRITE_SCRAPYARD_PROP_BG_CARS
//		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_TREE_1
			eSprite = CMHDZ_SPRITE_TREE_01
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_TREE_2
			eSprite = CMHDZ_SPRITE_TREE_02
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_TREE_3
			eSprite = CMHDZ_SPRITE_TREE_01
		BREAK
		CASE CMHDZ_ASSET_THEMEPARK_TREE_4
			eSprite = CMHDZ_SPRITE_TREE_02
		BREAK
		CASE CMHDZ_ASSET_GRASS
			eSprite = CMHDZ_SPRITE_FLOOR_GRASS
		BREAK
		CASE CMHDZ_ASSET_BUSH_1
			eSprite = CMHDZ_SPRITE_PROP_BUSH_SMALL
		BREAK
		CASE CMHDZ_ASSET_BUSH_2
			eSprite = CMHDZ_SPRITE_PROP_BUSH_LARGE
		BREAK
		CASE CMHDZ_ASSET_ROCK_SMALL
			eSprite = CMHDZ_SPRITE_PROP_ROCK_SMALL
		BREAK
		CASE CMHDZ_ASSET_ROCK_LARGE
			eSprite = CMHDZ_SPRITE_PROP_ROCK_LARGE
		BREAK
		CASE CMHDZ_ASSET_FOREGROUND_TRASH
			eSprite = CMHDZ_SPRITE_PROP_FOREGROUND_TRASH
		BREAK
		CASE CMHDZ_ASSET_BARRIER
			eSprite = CMHDZ_SPRITE_PROP_BARRIER
		BREAK
		CASE CMHDZ_ASSET_BARRELS
			eSprite = CMHDZ_SPRITE_PROP_BARRELS
		BREAK
		CASE CMHDZ_ASSET_MEATGRINDER_1
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PROP_MEATGRINDER_01
		BREAK
		CASE CMHDZ_ASSET_MEATGRINDER_2
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PROP_MEATGRINDER_02
		BREAK
		CASE CMHDZ_ASSET_BODYBAG_1
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_BODYBAG_01
		BREAK
		CASE CMHDZ_ASSET_BODYBAG_2
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_BODYBAG_02
		BREAK
		CASE CMHDZ_ASSET_BODYBAG_3
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_BODYBAG_03
		BREAK
		CASE CMHDZ_ASSET_BODYBAG_FLOOR_1
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_FLOOR_BODYBAG_01
		BREAK
		
		CASE CMHDZ_ASSET_BODYBAG_FLOOR_2
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_FLOOR_BODYBAG_02
		BREAK
				
		CASE CMHDZ_ASSET_BODYBAG_FLOOR_3
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_FLOOR_BODYBAG_03
		BREAK
		
		CASE CMHDZ_ASSET_BODYBAG_FLOOR_4
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_FLOOR_BODYBAG_04
		BREAK
				
		CASE CMHDZ_ASSET_MEATHOOK
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_MEATHOOK
		BREAK
				
		CASE CMHDZ_ASSET_PIG
			IF iHitCount = 0
				eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PIG_01
			ELSE
				eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PIG_02
			ENDIF
		BREAK
		
		CASE CMHDZ_ASSET_PIG_BATCH
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PIG_BATCH
		BREAK
		
		CASE CMHDZ_ASSET_WINDOW
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PROP_WINDOW
		BREAK
		
		CASE CMHDZ_ASSET_DOOR_1
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PROP_DOOR_01
		BREAK
		
		CASE CMHDZ_ASSET_DOOR_2
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PROP_DOOR_02
		BREAK
		
		CASE CMHDZ_ASSET_OVERHEAD_LIGHT
			eSprite = CMHDZ_SPRITE_SLAUGHTERHOUSE_PROP_LIGHT
		BREAK
		
		CASE CMHDZ_ASSET_WATER_TOWER
			eSprite = CMHDZ_SPRITE_WATER_TANK
		BREAK
//		CASE CMHDZ_ASSET_SPECIAL_TNT_CRATE
//			eSprite = CMHDZ_SPRITE_PROP_TNT_CRATE_STATE_01
//		BREAK
		CASE CMHDZ_ASSET_SPECIAL_TNT_BARREL_1
			eSprite = CMHDZ_SPRITE_BARREL_HORIZONTAL
		BREAK
		CASE CMHDZ_ASSET_SPECIAL_TNT_BARREL_2
			//tl23Sprite = "BARREL_VERTICAL"
			// ?!?
		BREAK
		CASE CMHDZ_ASSET_TRASHCAN_BIG_1
			eSprite = CMHDZ_SPRITE_TRASHCAN_BIG_01
		BREAK
		CASE CMHDZ_ASSET_TRASHCAN_BIG_2
			eSprite = CMHDZ_SPRITE_TRASHCAN_BIG_02
		BREAK		
		CASE CMHDZ_ASSET_TRASHCAN_SMALL_1
			eSprite = CMHDZ_SPRITE_TRASHCAN_SMALL_01
		BREAK		
		CASE CMHDZ_ASSET_TRASHCAN_SMALL_2
			eSprite = CMHDZ_SPRITE_TRASHCAN_SMALL_02
		BREAK			
		CASE CMHDZ_ASSET_TRASHBAG_BIG
			eSprite = CMHDZ_SPRITE_TRASHBAG_BIG
		BREAK			
		CASE CMHDZ_ASSET_TRASHBAG_SMALL
			eSprite = CMHDZ_SPRITE_TRASHBAG_SMALL
		BREAK			
	
	ENDSWITCH
				
	RETURN eSprite
ENDFUNC 

FUNC BOOL CMHDZ_DRAW_COVER_PROCESS_CHECK(INT i, INT iDistance, BOOL bIsSuperFarBackGround = FALSE, BOOL bIsEditor = FALSE)

	IF (bIsSuperFarBackGround = TRUE)
		IF NOT CMHDZ_COVER_DATA_IS_BITMASK_SET(i, CMHDZ_COVER_DATA_BITS_IS_FAR_BG)
			RETURN FALSE 
		ENDIF
	ELIF CMHDZ_COVER_DATA_IS_BITMASK_SET(i, CMHDZ_COVER_DATA_BITS_IS_FAR_BG)
		RETURN FALSE 
	ENDIF
	
	// Invalid or empty data
	IF sCMHDZData.sCoverData[i].fActiveAtPoint = 0
		RETURN FALSE
	ENDIF
	
	// Off screen to the right
	IF !bIsEditor
	AND (sCMHDZData.fOverallLevelProgression + cfBASE_SCREEN_WIDTH + 400) <= sCMHDZData.sCoverData[i].fActiveAtPoint
		//CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_DRAW_COVER_PROCESS_CHECK - Cover number:", i, ", Active point: ", sCMHDZData.sCoverData[i].fActiveAtPoint, ", RESULT:- Returning false as it's off screen to Right")
		RETURN FALSE
	ENDIF
	
	// Not even on screen
//	IF sCMHDZData.sCoverData[i].vSpritePos.x <= -(sCMHDZData.sCoverData[i].vSpriteSize.x/2.0)
//		RETURN FALSE
//	ENDIF
//
//	IF sCMHDZData.sCoverData[i].vSpritePos.X >= (1920.0 + (sCMHDZData.sCoverData[i].vSpriteSize.X/2.0))
//		RETURN FALSE
//	ENDIF
		
	RETURN sCMHDZData.sCoverData[i].iCoverLane = iDistance
ENDFUNC 

FUNC BOOL CMHDZ_IS_COVER_OUT_OF_LIMITS(INT iCoverID)
	RETURN sCMHDZData.sCoverData[iCoverID].vSpritePos.x <= -(cfRIGHT_SCREEN_LIMIT + cfLEFT_SCREEN_LIMIT)
ENDFUNC

PROC CMHDZ_DRAW_COVER(INT iDistance, BOOL bForLeaderboard = FALSE, BOOL bIsSuperFarBackGround = FALSE)

	CMHDZ_SPRITE eSprite
	FLOAT fScale = 1.0
	VECTOR_2D vScale
	
	BOOL bRepositionWhenOffScreenForMovingLevel
	BOOL bUpdatePosition
	
	INT i
	INT iNumRemove = 0
	INT iRemoveWhenOffScreen[ciCMHDZ_MAX_COVER]
	// Loop thrugh all cover
	REPEAT ciCMHDZ_MAX_COVER i
		IF NOT CMHDZ_DRAW_COVER_PROCESS_CHECK(i, iDistance, bIsSuperFarBackGround)
			RELOOP 
		ENDIF
		
		// draw the cover on the current screen and the next
		fScale = CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(sCMHDZData.sCoverData[i].eType, iDistance)
		eSprite = CMHDZ_COVER_UTIL_GET_SPRITE_FOR_TYPE(sCMHDZData.sCoverData[i].eType, sCMHDZData.sCoverData[i].iHitCount)
		vScale = CMHDZ_COVER_UTIL_GET_TYPE_SIZE(sCMHDZData.sCoverData[i].eType)
		IF (eSprite = CMHDZ_SPRITE_NONE)
			RELOOP
		ENDIF	
		
		//IF sCMHDZData.sCoverData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH AND sCMHDZData.sCoverData[i].vSpritePos.x > 0
		
		IF bForLeaderboard
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_SPRITE_GET_TEXTURE_DICT_NAME(eSprite), CMHDZ_SPRITE_GET_TEXTURE_NAME(eSprite),  sCMHDZData.sCoverData[i].vSpritePos, INIT_VECTOR_2D(vScale.x*fScale, vScale.y*fScale), 0, sCMHDZData.rgbaSprite)
		ELSE 
			CMHDZ_DRAW_GAME_SPRITE(eSprite,  sCMHDZData.sCoverData[i].vSpritePos, INIT_VECTOR_2D(vScale.x*fScale, vScale.y*fScale), 0, sCMHDZData.rgbaSprite)
			
//#IF IS_DEBUG_BUILD
//			// Identfy cover numbers and size
//			TEXT_LABEL_31 sDebug = "Cover: "
//			sDebug += i
//			sDebug += ", Active: "
//			sDebug += ROUND(sCMHDZData.sCoverData[i].fActiveAtPoint)
//									
//			DRAW_DEBUG_TEXT_2D(sDebug,
//				<<(sCMHDZData.sCoverData[i].vSpritePos.x / 1920),
//				(sCMHDZData.sCoverData[i].vSpritePos.y / 1080),
//				0.0>>,
//				15, 15, 250)
//#ENDIF
//			
		ENDIF
		
	
		bUpdatePosition = TRUE	
		bRepositionWhenOffScreenForMovingLevel = !CMHDZ_COVER_DATA_IS_BITMASK_SET(i, CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN)
		
		IF bForLeaderboard
			
			//-- If it's the LB background, always update
			bRepositionWhenOffScreenForMovingLevel = TRUE
		ELIF IS_MOVING_LEVEL() 
			
			IF CMHDZ_IS_MOVING_LEVEL_ABOUT_TO_STOP()
				//IF CMHDZ_GET_CURRENT_LEVEL() = CMHDZ_LEVEL_CITY
				IF sCMHDZData.sCoverData[i].iCoverLane = ciCMHDZ_LANE_FOREGROUND
					//-- Don't want to reprosition if we're about to stop scrolling as otherwise they'll appear again on
					//-- the static screen
					bRepositionWhenOffScreenForMovingLevel = FALSE
					
					IF CEIL(sCMHDZData.sCoverData[i].fActiveAtPoint/1920.0) <= (CMHDZ_GET_CURRENT_LEVEL_SCREEN() + 1)
						FLOAT fLeftEdge
						fLeftEdge = sCMHDZData.sCoverData[i].vSpritePos.x - (sCMHDZData.sCoverData[i].fCoverWidth /2.0)
						IF fLeftEdge > cfBASE_SCREEN_WIDTH - 250
							//-- If we're about to stop, don't update anything that's about to appear from the right
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] {DSW} Not updating cover from right - about to stop ", i, " Lane: ", sCMHDZData.sCoverData[i].iCoverLane ," Type: ", CMHDZ_ASSET_TYPES_TO_STRING(sCMHDZData.sCoverData[i].eType), " Left edge Pos: ", fLeftEdge)
							#ENDIF
							bUpdatePosition = FALSE
						ENDIF
					ENDIF
				ELSE
					//-- Level is moving, fine to update
					bRepositionWhenOffScreenForMovingLevel = TRUE
				ENDIF
			ELSE
				//-- Level is moving, fine to update
				bRepositionWhenOffScreenForMovingLevel = TRUE
			ENDIF
			
		ELSE
			
			//-- Stationary, don't move anything
			bRepositionWhenOffScreenForMovingLevel = FALSE
			bUpdatePosition = FALSE
		ENDIF
		
		IF iDistance = ciCMHDZ_LANE_FOREGROUND
			IF CMHDZ_IS_COVER_OUT_OF_LIMITS(i)
				IF bRepositionWhenOffScreenForMovingLevel
					//sCMHDZData.sCoverData[i].vSpritePos.x += (cfRIGHT_SCREEN_LIMIT*3) //+ ((sCMHDZData.sCoverData[i].fCoverWidth * CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(sCMHDZData.sCoverData[i].eType, sCMHDZData.sCoverData[i].iCoverLane)) /2)
					IF !CMHDZ_SHOULD_REMOVE_COVER_WHEN_OFF_SCREEN(i)
						sCMHDZData.sCoverData[i].vSpritePos.x += ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
						sCMHDZData.sCoverData[i].fActiveAtPoint += ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
					ENDIF
				ENDIF
			ELSE
				IF bUpdatePosition
					sCMHDZData.sCoverData[i].vSpritePos.x -= CMHDZ_GET_COVER_FOREGROUND_UPDATE()
				ENDIF
			ENDIF
		ELSE // Distance = 0 or 1. Foreground and background?
			IF CMHDZ_IS_COVER_OUT_OF_LIMITS(i)
				
				IF bRepositionWhenOffScreenForMovingLevel
					IF CMHDZ_SHOULD_REMOVE_COVER_WHEN_OFF_SCREEN(i)
						//-- When some cover has finished animating, don't want to reposition
						IF iNumRemove < ciCMHDZ_MAX_COVER
							iRemoveWhenOffScreen[iNumRemove] = i
							iNumRemove++
//							
//							IF sCMHDZData.sCoverData[i].eType = CMHDZ_ASSET_PIG
//								CMHDZ_ADD_COVER_TO_LIST(sCMHDZData.sCoverData[i].vSpritePos, CMHDZ_ASSET_PIG, 	ciCMHDZ_LANE_BACKGROUND,  sCMHDZData.sCoverData[i].iScreen)
//							ENDIF
//							
//							#IF IS_DEBUG_BUILD
//							CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] {DSW} Want to remove when offscreen ", i, " Lane: ", sCMHDZData.sCoverData[i].iCoverLane ," Type: ", CMHDZ_ASSET_TYPES_TO_STRING(sCMHDZData.sCoverData[i].eType))
//							#ENDIF
//						ELSE
//							#IF IS_DEBUG_BUILD
//							CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] {DSW} Want to remove when offscreen BUT ARRAY FULL! ", i, " Lane: ", sCMHDZData.sCoverData[i].iCoverLane ," Type: ", CMHDZ_ASSET_TYPES_TO_STRING(sCMHDZData.sCoverData[i].eType))
//							#ENDIF
						ENDIF
					ELSE
						//sCMHDZData.sCoverData[i].vSpritePos.x += (cfRIGHT_SCREEN_LIMIT*3) //+ ((sCMHDZData.sCoverData[i].fCoverWidth * CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(sCMHDZData.sCoverData[i].eType, sCMHDZData.sCoverData[i].iCoverLane)) /2)
						sCMHDZData.sCoverData[i].vSpritePos.x += ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
						sCMHDZData.sCoverData[i].fActiveAtPoint += ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
						sCMHDZData.sCoverData[i].vSpritePos.x *= CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(sCMHDZData.sCoverData[i].eType, sCMHDZData.sCoverData[i].iCoverLane)						
						
						#IF IS_DEBUG_BUILD
							IF sCMHDZData.sCoverData[i].eType = CMHDZ_ASSET_SPECIAL_TNT_BARREL_1
								//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_COVER] {DSW} Found TNT Barrel Cover: ", i, " Type: ", CMHDZ_ASSET_TYPES_TO_STRING(sCMHDZData.sCoverData[i].eType))
							ENDIF
						#ENDIF
					ENDIF			
				ENDIF
			ELSE
				IF bUpdatePosition
					//sCMHDZData.sCoverData[i].vSpritePos.x -= CMHDZ_GET_GENERAL_UPDATE()
					sCMHDZData.sCoverData[i].vSpritePos.x = (sCMHDZData.sCoverData[i].fActiveAtPoint - sCMHDZData.fOverallLevelProgression)
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT 
	
	//-- Delete / reset any cover we don't want to re-appear next time the screen scrolls
	IF iNumRemove > 0
		FOR i = 0 TO iNumRemove - 1
			CMHDZ_RESET_COVER(iRemoveWhenOffScreen[i])
		ENDFOR
	ENDIF

ENDPROC


// =============================================
// BOSS Functions
// =============================================
PROC CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE eNewState, CMHDZ_BOSS_STATE oldState = CMHDZ_BOSS_STATE_INACTIVE)
	CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_CHANGE_BOSS_STATE - New state: ", CMHDZ_BOSS_STATE_TO_STRING(eNewState))
	INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
	sCMHDZData.sBossData.bShotThisFrame = FALSE
	sCMHDZData.sBossData.fStateChangeTimer = 0.0
	
	IF oldState = CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN
		IF sCMHDZData.sBossData.fHealth <= (sCMHDZData.sBossData.fDamageThreshold_1)
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sCMHDZData.sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)
				eNewState = (CMHDZ_BOSS_STATE_SPAWN_HELPERS_1)
//				sCMHDZData.sBossData.eCurrentBossState = eNewState
//				EXIT
			ENDIF
		ENDIF
	
		IF sCMHDZData.sBossData.fHealth <= (sCMHDZData.sBossData.fDamageThreshold_2)
			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sCMHDZData.sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
				eNewState = (CMHDZ_BOSS_STATE_SPAWN_HELPERS_2)
//				sCMHDZData.sBossData.eCurrentBossState = eNewState
//				EXIT
			ENDIF
		ENDIF
		
		IF sCMHDZData.sBossData.fHealth <= (sCMHDZData.sBossData.fDamageThreshold_3)
//			IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sCMHDZData.sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
				eNewState = (CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
//				sCMHDZData.sBossData.eCurrentBossState = eNewState
//				EXIT
//			ENDIF
		ENDIF
		
		// Stop the chainsaw sound if Bearsy is leaving the screen.
		CMHDZ_STOP_SOUND(CMHDZ_AUDIO_EFFECT_BEARSY_CHAINSAW_LOOP)
	ENDIF
	
	IF eNewState = CMHDZ_BOSS_STATE_WALKING
	AND oldState != CMHDZ_BOSS_STATE_THROWING
		sCMHDZData.sBossData.fTimeToThrow = GET_RANDOM_FLOAT_IN_RANGE(1800.0, 2600.0)
		SET_BITMASK_ENUM_AS_ENUM_BY_BOOL(sCMHDZData.sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_REVERSE, INT_TO_BOOL(GET_RANDOM_INT_IN_RANGE(0, 2)))
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(sCMHDZData.sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_REVERSE)
			sCMHDZData.sBossData.vSpritePos.X = -200
		ELSE // Appear from right
			sCMHDZData.sBossData.vSpritePos.X = 1920 + 200
		ENDIF
		
		// Middle lane
		sCMHDZData.sBossData.iActiveLane = ciCMHDZ_LANE_MIDDLE
	
		IF sCMHDZData.sBossData.fHealth <= (sCMHDZData.sBossData.fDamageThreshold_3)
			eNewState = (CMHDZ_BOSS_STATE_FOREGROUND_POP_UP) // Will hit the next state
		ENDIF
	ENDIF
	
	IF eNewState = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP
		sCMHDZData.sBossData.iActiveLane = ciCMHDZ_LANE_FOREGROUND
		sCMHDZData.sBossData.vSpritePos.x = GET_RANDOM_FLOAT_IN_RANGE(600.0, 1200.0)
		sCMHDZData.sBossData.vSpritePos.y = (1080.0 + (sCMHDZData.sBossData.vScale.y/2.0))
		CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_BEARSY_REVEAL)

		// First time appearance.
		IF oldState = CMHDZ_BOSS_STATE_WAITING
			sCMHDZData.sBossData.vSpritePos.x = cfBASE_SCREEN_WIDTH/2.0
		ENDIF
	ENDIF
	
	
	IF eNewState = CMHDZ_BOSS_STATE_STUNNED_BACKGROUND
		sCMHDZData.iStaticScreenPhase = 0
	ENDIF
	
	IF eNewState = CMHDZ_BOSS_STATE_DYING
		CMHDZ_TRIGGER_MUSIC_EVENT(CMHDZ_MUSIC_EVENT_FINAL_BOSS_DEFEATED)
	ENDIF
		
	sCMHDZData.sBossData.eCurrentBossState = eNewState
ENDPROC

FUNC FLOAT CMHDZ_GET_AMOUNT_BOSS_HAS_BEEN_DAMAGED(BOOL bHeadshot, CMHDZ_BOSS_STATE eBossState, INT iActiveLane)

	IF iActiveLane = ciCMHDZ_LANE_FOREGROUND
		IF eBossState != CMHDZ_BOSS_STATE_STUNNED_FOREGROUND
		AND eBossState != CMHDZ_BOSS_STATE_ATTACK_READY
			RETURN 0.0
		ENDIF
	ENDIF
								
	IF iActiveLane = ciCMHDZ_LANE_BACKGROUND
		
		// Scale damage if in the background
		IF eBossState = CMHDZ_BOSS_STATE_STUNNED_BACKGROUND
		OR eBossState = CMHDZ_BOSS_STATE_WALKING_INV
			RETURN 0.0
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		//CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [CMHDZ_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} ")
		//CPRINTLN(DEBUG_MINIGAME, "[HANDLE_PLAYER_BULLET_COLLISION] [CMHDZ_GET_AMOUNT_ENEMY_HAS_BEEN_DAMAGED] {dsw} Enemy ", iEnemy, " Shot at least once. Lane: ", sCMHDZData.sEnemyData[iEnemy].iEnemyLane, " Type: ", CMHDZ_ENEMY_TYPES_TO_STRING(sCMHDZData.sEnemyData[iEnemy].iEnemyType), " Headshot? ", bHeadshot)
	#ENDIF
		
	FLOAT fBaseDamage
	INT iRand
	
	CMHDZ_PLAYER_WEAPON weapon = CMHDZ_GET_PLAYER_CURRENT_WEAPON()
	CMHDZ_PLAYER_WEAPON_DATA data = CMHDZ_GET_PLAYER_WEAPON_DATA(weapon)
	fBaseDamage = TO_FLOAT(data.iMinDamagePerShot)
	
	IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_QUAD_DAMAGE)
		fBaseDamage *= 2.0 // Bearsy dying too quickly. Upping defense.
	ENDIF
		
	IF sCMHDZData.sPlayerData[0].weaponType = CMHDZ_PLAYER_WEAPON_TYPE_DEFAULT
		iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		fBaseDamage += iRand
	ENDIF
		
	IF bHeadshot
		fBaseDamage	*= 2.0
	ENDIF
	
	// Scale for background
	IF iActiveLane = ciCMHDZ_LANE_BACKGROUND
		fBaseDamage /= 2.0
	ENDIF
			
	RETURN fBaseDamage
	
ENDFUNC

PROC CMHDZ_BOSS_PROCESS_ANIM_DATA(CMHDZ_BOSS_DATA &sBossData, CMHDZ_SPRITE &eSprite,  INT &iAnimLength, INT &iFrameTime)

	BOOL bLoopingAnim = TRUE

	SWITCH sBossData.eCurrentBossState

		CASE CMHDZ_BOSS_STATE_WAITING
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_WALK_FRAME_IDLE
			iAnimLength = 1
			iFrameTime = 1000
		BREAK
	
		CASE CMHDZ_BOSS_STATE_WALKING
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_WALK_FRAME_01
			iAnimLength = 5
			iFrameTime = 150
		BREAK

		CASE CMHDZ_BOSS_STATE_WALKING_INV
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_WALK_FRAME_01
			iAnimLength = 5
			iFrameTime = 75
		BREAK

		CASE CMHDZ_BOSS_STATE_STUNNED_BACKGROUND
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_BACKGROUND_STAGGERED
			iAnimLength = 1
			iFrameTime = 150
		BREAK
		
		CASE CMHDZ_BOSS_STATE_THROWING
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_THROWING_01
			iAnimLength = 3
			iFrameTime = 200
			bLoopingAnim = FALSE
		BREAK
		
		CASE CMHDZ_BOSS_STATE_PAIN
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_BACK_PAIN
			iAnimLength = 6
			iFrameTime = 135
		BREAK
		
		CASE CMHDZ_BOSS_STATE_FOREGROUND_PAIN
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_DEATH_01
			iAnimLength = 2
			iFrameTime = 500
		BREAK

		CASE CMHDZ_BOSS_STATE_FOREGROUND_IDLE
		CASE CMHDZ_BOSS_STATE_FOREGROUND_POP_UP
		CASE CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_IDLE_04
			iAnimLength = 2
			iFrameTime = 400
		BREAK
					
		CASE CMHDZ_BOSS_STATE_ATTACKING
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_ATTACKING_01
			iAnimLength = 5
			iFrameTime = 150
			bLoopingAnim = FALSE
		BREAK
		
		CASE CMHDZ_BOSS_STATE_STUNNED_FOREGROUND
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_STAGGERED
			iAnimLength = 1
			iFrameTime = 150
		BREAK
				
		CASE CMHDZ_BOSS_STATE_ATTACKING_COUNTER
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_ATTACKING_01
			iAnimLength = 5
			iFrameTime = 100
			bLoopingAnim = FALSE
		BREAK
	
		CASE CMHDZ_BOSS_STATE_BLOCKING
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_IDLE_01
			iAnimLength = 3
			iFrameTime = 200
		BREAK
		
		CASE CMHDZ_BOSS_STATE_ATTACK_READY
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_IDLE_01
			iAnimLength = 2
			iFrameTime = 400
		BREAK
		
		CASE CMHDZ_BOSS_STATE_DYING
			sBossData.sStartingSprite = CMHDZ_SPRITE_BEARSY_DEATH_01
			iAnimLength = 2
			iFrameTime = 500
			bLoopingAnim = TRUE
		BREAK

				
	ENDSWITCH

	IF (sBossData.fHealth > sBossData.fDamageThreshold_1)
		// Do nothing - x1
	ELIF (sBossData.fHealth > sBossData.fDamageThreshold_2)
		iFrameTime = ROUND(iFrameTime*0.9)
	ELIF (sBossData.fHealth > sBossData.fDamageThreshold_3)
		iFrameTime = ROUND(iFrameTime*0.8)
	ELSE
		iFrameTime = ROUND(iFrameTime*0.7)
	ENDIF
	
	// Local frame timer
	sBossData.fAnimTimer +=  (0.0+@1000)
	sBossData.iAnimFrame = FLOOR(sBossData.fAnimTimer / iFrameTime)
	
	IF sBossData.iAnimFrame >= iAnimLength
		
		IF NOT bLoopingAnim
			sBossData.iAnimFrame = iAnimLength-1
			SET_BITMASK_ENUM_AS_ENUM(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_ANIM_FINISHED_ONCE)
		ELSE
			sBossData.fAnimTimer = 0.0
			sBossData.iAnimFrame = 0
		ENDIF
	ENDIF
	
	//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_PROCESS_ANIM_DATA - iAnimFrame: ", sBossData.iAnimFrame)
	
	// Get anim frame timer update
//	IF sBossData.fAnimTimer >= (iFrameTime)
//		
//		//sBossData.fAnimTimer = 0.0
//	ENDIF
	
	// Reset data if we're beyond the length
//	IF sBossData.iAnimFrame >= iAnimLength
//		sBossData.iAnimFrame = 0
//		sBossData.fAnimTimer = 0.0
//	ENDIF
	
	eSprite = INT_TO_ENUM(CMHDZ_SPRITE, ENUM_TO_INT(sBossData.sStartingSprite)+ sBossData.iAnimFrame)
	sBossData.vScale = CMHDZ_BOSS_GET_SPRITE_SIZES(eSprite)
	
	IF sBossData.iActiveLane = ciCMHDZ_LANE_BACKGROUND
		sBossData.vScale = MULTIPLY_VECTOR_2D(sBossData.vScale, 0.8)
	ENDIF

ENDPROC

PROC CMHDZ_BOSS_MIDBACK_WALKING_UPDATE(CMHDZ_BOSS_DATA &sBossData, INT &iFrameTime)

	// Position update
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_REVERSE)
		SWITCH (sBossData.iActiveLane)
						
			CASE ciCMHDZ_LANE_MIDDLE
				
				//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_DATA_BITS_REVERSE - Go to screens right")
				
				sBossData.vSpritePos.x += CMHDZ_GET_BOSS_MIDDLE_UPDATE(iFrameTime)
				sBossData.vSpritePos.y = 500
				IF sBossData.vSpritePos.x >= cfRIGHT_SCREEN_LIMIT + 100
					sBossData.fTimeToThrow = GET_RANDOM_FLOAT_IN_RANGE(1800.0, 2600.0)
					sBossData.iActiveLane = ciCMHDZ_LANE_BACKGROUND
				ENDIF			
			BREAK 
			
			CASE ciCMHDZ_LANE_BACKGROUND
						
				sBossData.vSpritePos.x -= CMHDZ_GET_BOSS_BACKGROUND_UPDATE(iFrameTime)
				sBossData.vScale.x = -sBossData.vScale.x
				sBossData.vSpritePos.y = 320
				IF sBossData.vSpritePos.x <= cfLEFT_SCREEN_LIMIT - 100
					sBossData.iActiveLane = ciCMHDZ_LANE_FOREGROUND
					sBossData.vSpritePos.x = GET_RANDOM_FLOAT_IN_RANGE(600.0, 1200.0)
					sBossData.vSpritePos.y = (1080.0 + (sBossData.vScale.y/2.0))
					//sBossData.vSpritePos.x = sBossData.eBossPhases[sBossData.iPhaseIndex].fForegroundXAppear
					sBossData.fStateChangeTimer = 0.0
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_MIDBACK_WALKING_UPDATE - sBossData.eCurrentBossState - CMHDZ_BOSS_DATA_BITS_REVERSE = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP")
				ENDIF
			BREAK
		
		ENDSWITCH
	ELSE // Appear from right
		SWITCH (sBossData.iActiveLane)
						
			CASE ciCMHDZ_LANE_MIDDLE
				//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] Appear from right - ciCMHDZ_LANE_MIDDLE - iFrameTime: ", iFrameTime)
				sBossData.vSpritePos.x -= CMHDZ_GET_BOSS_MIDDLE_UPDATE(iFrameTime)
				sBossData.vScale.x = -sBossData.vScale.x
				sBossData.vSpritePos.y = 500
				IF sBossData.vSpritePos.x <= cfLEFT_SCREEN_LIMIT - 100
					sBossData.fTimeToThrow = GET_RANDOM_FLOAT_IN_RANGE(1800.0, 2600.0)
					sBossData.iActiveLane = ciCMHDZ_LANE_BACKGROUND
				ENDIF
			BREAK 
			
			CASE ciCMHDZ_LANE_BACKGROUND
				sBossData.vSpritePos.x += CMHDZ_GET_BOSS_BACKGROUND_UPDATE(iFrameTime)
				//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] Appear from right - ciCMHDZ_LANE_BACKGROUND - sBossData.vSpritePos.x: ", sBossData.vSpritePos.x)
				sBossData.vSpritePos.y = 320
				IF sBossData.vSpritePos.x >= cfRIGHT_SCREEN_LIMIT + 100
					sBossData.iActiveLane = ciCMHDZ_LANE_FOREGROUND
					sBossData.vSpritePos.x = GET_RANDOM_FLOAT_IN_RANGE(600.0, 1200.0)
					//sBossData.vSpritePos.x = sBossData.eBossPhases[sBossData.iPhaseIndex].fForegroundXAppear
					sBossData.vSpritePos.y = (1080.0 + (sBossData.vScale.y/2.0))
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_MIDBACK_WALKING_UPDATE - sBossData.eCurrentBossState - NOT CMHDZ_BOSS_DATA_BITS_REVERSE = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP")
					sBossData.fStateChangeTimer = 0.0
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
				ENDIF
			BREAK
						
		ENDSWITCH
	ENDIF

ENDPROC


PROC CMHDZ_BOSS_INVINCIBLE_WALKING_UPDATE(CMHDZ_BOSS_DATA &sBossData, INT &iFrameTime)

	// Position update
	IF IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_REVERSE)
		SWITCH (sBossData.iActiveLane)
						
			CASE ciCMHDZ_LANE_MIDDLE
				
				//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_DATA_BITS_REVERSE - Go to screens right")
				
				sBossData.vSpritePos.x += (CMHDZ_GET_BOSS_MIDDLE_UPDATE(iFrameTime)*2)
				sBossData.vSpritePos.y = 500
				IF sBossData.vSpritePos.x >= cfRIGHT_SCREEN_LIMIT + 100
					sBossData.iActiveLane = ciCMHDZ_LANE_FOREGROUND
					sBossData.vSpritePos.x = GET_RANDOM_FLOAT_IN_RANGE(600.0, 1200.0)
					sBossData.vSpritePos.y = (1080.0 + (sBossData.vScale.y/2.0))
					//sBossData.vSpritePos.x = sBossData.eBossPhases[sBossData.iPhaseIndex].fForegroundXAppear
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
					sBossData.fStateChangeTimer = 0.0
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_MIDBACK_WALKING_UPDATE - sBossData.eCurrentBossState - CMHDZ_BOSS_DATA_BITS_REVERSE = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP")
					
					IF sBossData.fHealth <= sBossData.fDamageThreshold_1
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)
							sBossData.fHealth = sBossData.fDamageThreshold_1
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_1)
							EXIT
						ENDIF
					ENDIF
				
					IF sBossData.fHealth <= sBossData.fDamageThreshold_2
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
							sBossData.fHealth = sBossData.fDamageThreshold_2
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_2)
							EXIT
						ENDIF
					ENDIF
					
//					IF sBossData.fHealth <= sBossData.fDamageThreshold_3
//						EXIT
//					ENDIF
										
				ENDIF			
			BREAK 
			
			CASE ciCMHDZ_LANE_BACKGROUND
						
				sBossData.vSpritePos.x -= (CMHDZ_GET_BOSS_BACKGROUND_UPDATE(iFrameTime)*2)
				sBossData.vScale.x = -sBossData.vScale.x
				sBossData.vSpritePos.y = 320
				IF sBossData.vSpritePos.x <= cfLEFT_SCREEN_LIMIT - 100
					sBossData.iActiveLane = ciCMHDZ_LANE_FOREGROUND
					sBossData.vSpritePos.x = GET_RANDOM_FLOAT_IN_RANGE(600.0, 1200.0)
					sBossData.vSpritePos.y = (1080.0 + (sBossData.vScale.y/2.0))
					//sBossData.vSpritePos.x = sBossData.eBossPhases[sBossData.iPhaseIndex].fForegroundXAppear
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
					sBossData.fStateChangeTimer = 0.0
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_MIDBACK_WALKING_UPDATE - sBossData.eCurrentBossState - CMHDZ_BOSS_DATA_BITS_REVERSE = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP")
					
					IF sBossData.fHealth <= sBossData.fDamageThreshold_1
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_1)
							sBossData.fHealth = sBossData.fDamageThreshold_1
							EXIT
						ENDIF
					ENDIF
				
					IF sBossData.fHealth <= sBossData.fDamageThreshold_2
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_2)
							sBossData.fHealth = sBossData.fDamageThreshold_2
							EXIT
						ENDIF
					ENDIF
						
//					IF sBossData.fHealth <= sBossData.fDamageThreshold_3
//
//						EXIT
//					ENDIF
				ENDIF
			BREAK
		
		ENDSWITCH
	ELSE // Appear from right
		SWITCH (sBossData.iActiveLane)
						
			CASE ciCMHDZ_LANE_MIDDLE
				//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] Appear from right - ciCMHDZ_LANE_MIDDLE - iFrameTime: ", iFrameTime)
				sBossData.vSpritePos.x -= (CMHDZ_GET_BOSS_MIDDLE_UPDATE(iFrameTime)*2)
				sBossData.vScale.x = -sBossData.vScale.x
				sBossData.vSpritePos.y = 500
				IF sBossData.vSpritePos.x <= cfLEFT_SCREEN_LIMIT - 100
					//sBossData.vSpritePos.x = sBossData.eBossPhases[sBossData.iPhaseIndex].fForegroundXAppear
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
					sBossData.fStateChangeTimer = 0.0
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_MIDBACK_WALKING_UPDATE - sBossData.eCurrentBossState - CMHDZ_BOSS_DATA_BITS_REVERSE = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP")
					
					IF sBossData.fHealth <= sBossData.fDamageThreshold_1
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_1)
							sBossData.fHealth = sBossData.fDamageThreshold_1
							EXIT
						ENDIF
					ENDIF
				
					IF sBossData.fHealth <= sBossData.fDamageThreshold_2
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_2)
							sBossData.fHealth = sBossData.fDamageThreshold_2
							EXIT
						ENDIF
					ENDIF
										
//					IF sBossData.fHealth <= sBossData.fDamageThreshold_3
//						EXIT
//					ENDIF					
				ENDIF
			BREAK 
			
			CASE ciCMHDZ_LANE_BACKGROUND
				sBossData.vSpritePos.x += (CMHDZ_GET_BOSS_BACKGROUND_UPDATE(iFrameTime)*2)
				//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] Appear from right - ciCMHDZ_LANE_BACKGROUND - sBossData.vSpritePos.x: ", sBossData.vSpritePos.x)
				sBossData.vSpritePos.y = 320
				IF sBossData.vSpritePos.x >= cfRIGHT_SCREEN_LIMIT + 100
//					sBossData.iActiveLane = ciCMHDZ_LANE_FOREGROUND
//					sBossData.vSpritePos.x = GET_RANDOM_FLOAT_IN_RANGE(600.0, 1200.0)
//					//sBossData.vSpritePos.x = sBossData.eBossPhases[sBossData.iPhaseIndex].fForegroundXAppear
//					sBossData.vSpritePos.y = (1080.0 + (sBossData.vScale.y/2.0))
//					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_MIDBACK_WALKING_UPDATE - sBossData.eCurrentBossState - NOT CMHDZ_BOSS_DATA_BITS_REVERSE = CMHDZ_BOSS_STATE_FOREGROUND_POP_UP")
//					sBossData.fStateChangeTimer = 0.0
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
					
					IF sBossData.fHealth <= sBossData.fDamageThreshold_1
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_1)
							sBossData.fHealth = sBossData.fDamageThreshold_1
							EXIT
						ENDIF
					ENDIF
				
					IF sBossData.fHealth <= sBossData.fDamageThreshold_2
						IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
							CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_SPAWN_HELPERS_2)
							sBossData.fHealth = sBossData.fDamageThreshold_2
							EXIT
						ENDIF
					ENDIF
					
//					IF sBossData.fHealth <= sBossData.fDamageThreshold_3
//						//sBossData.fHealth = sBossData.fDamageThreshold_3
//						EXIT
//					ENDIF
				ENDIF
			BREAK
						
		ENDSWITCH
	ENDIF

ENDPROC

PROC CMHDZ_INIT_THROWN_BEARSY_PROJECTILE()
	#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_INIT_THROWN_ENEMY_PROJECTILE] Called, type: ", CMHDZ_PROJECTILE_TYPES_TO_STRING(eType), " Initial pos: ",vInitialPos.x, ", ", vInitialPos.y)
	#ENDIF
	
	IF sCMHDZData.iNumberOfThrownEnemyProjectiles > ciCMHDZ_MAX_ENEMY_PROJECTILES - 1
		//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_INIT_THROWN_BEARSY_PROJECTILE] Too many projectiles - exiting ")
		EXIT
	ENDIF
	
	sCMHDZData.sEnemyProjectiles[sCMHDZData.iNumberOfThrownEnemyProjectiles].eState = CMHDZ_PROJECTILE_STATE_AIRBORNE
	
	INT min = ENUM_TO_INT(CMHDZ_PROJECTILE_BONE)
	INT max = ENUM_TO_INT(CMHDZ_PROJECTILE_TROTTER)+1// Non inclusive maximum
	
	sCMHDZData.sEnemyProjectiles[sCMHDZData.iNumberOfThrownEnemyProjectiles].eType = INT_TO_ENUM(CMHDZ_PROJECTILE_TYPES, GET_RANDOM_INT_IN_RANGE(min,max))
	sCMHDZData.sEnemyProjectiles[sCMHDZData.iNumberOfThrownEnemyProjectiles].vSpritePos = sCMHDZData.sBossData.vSpritePos
	
	CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BEARSY_MEAT_THROW, sCMHDZData.sEnemyProjectiles[sCMHDZData.iNumberOfThrownEnemyProjectiles].vSpritePos)
ENDPROC

PROC CMHDZ_HANDLE_THROWN_BEARSY_PROJECTILES()
	INT i
	
	
	REPEAT ciCMHDZ_MAX_ENEMY_PROJECTILES i
		SWITCH sCMHDZData.sEnemyProjectiles[i].eState
			CASE CMHDZ_PROJECTILE_STATE_AIRBORNE
				//sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame += sCMHDZData.sEnemyProjectiles[i].iSlowUpdateFrames
				IF sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame >= ciCMHDZ_MAX_ENEMY_THROWN_DYNAMITE_ANIM_FRAMES 
					sCMHDZData.sEnemyProjectiles[i].eState = 	CMHDZ_PROJECTILE_STATE_IMPACT
					CMHDZ_DAMAGE_PLAYER(CMHDZ_DAMAGE_PLAYER_TYPE_BULLET)
					SET_CONTROL_SHAKE(FRONTEND_CONTROL, 100, 20)
					TWS_CREATE_FX(sCMHDZData.sEnemyProjectiles[i].vSpritePos, CMHDZ_FX_BLOOD_SPLAT, ciCMHDZ_LANE_FOREGROUND)
					CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_RAT_DEATH, sCMHDZData.sEnemyProjectiles[i].vSpritePos)
				ENDIF				
			BREAK
			
			CASE CMHDZ_PROJECTILE_STATE_IMPACT
				CMHDZ_RESET_ENEMY_THROWN_PROJECTILE(i)
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC


PROC CMHDZ_DRAW_THROWN_BEARSY_PROJECTILES()
	INT i
	CMHDZ_SPRITE eSprite
	VECTOR_2D vSize
	
	CMHDZ_HANDLE_THROWN_BEARSY_PROJECTILES()
	
	REPEAT ciCMHDZ_MAX_ENEMY_PROJECTILES i
		IF sCMHDZData.sEnemyProjectiles[i].eState = CMHDZ_PROJECTILE_STATE_INACTIVE
		ELIF sCMHDZData.sEnemyProjectiles[i].eState = CMHDZ_PROJECTILE_STATE_AIRBORNE
		
			//CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_DRAW_THROWN_BEARSY_PROJECTILES - Projectile to draw: ", CMHDZ_PROJECTILE_TYPES_TO_STRING(sCMHDZData.sEnemyProjectiles[i].eType))
		
			SWITCH sCMHDZData.sEnemyProjectiles[i].eType
				
				CASE CMHDZ_PROJECTILE_BONE
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_BEARSY_BONE_SMALL_01
							vSize = INIT_VECTOR_2D(212, 204)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_BEARSY_BONE_MED_02
							vSize = INIT_VECTOR_2D(316, 304)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_BEARSY_BONE_LARGE_01
							vSize = INIT_VECTOR_2D(472, 452)
						BREAK
						
					ENDSWITCH	
				BREAK
				
				CASE CMHDZ_PROJECTILE_JOWL
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_BEARSY_JOWL_SMALL_01
							vSize = INIT_VECTOR_2D(212, 204)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_BEARSY_JOWL_MED_02
							vSize = INIT_VECTOR_2D(316, 304)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_BEARSY_JOWL_LARGE_01
							vSize = INIT_VECTOR_2D(472, 452)
						BREAK
						
					ENDSWITCH	
				BREAK
				
				CASE CMHDZ_PROJECTILE_LEG
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_BEARSY_LEG_SMALL_01
							vSize = INIT_VECTOR_2D(212, 204)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_BEARSY_LEG_MED_02
							vSize = INIT_VECTOR_2D(316, 304)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_BEARSY_LEG_LARGE_01
							vSize = INIT_VECTOR_2D(472, 452)	
						BREAK
						
					ENDSWITCH	
				BREAK
				
				CASE CMHDZ_PROJECTILE_TROTTER
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_BEARSY_TROTTER_SMALL_01
							vSize = INIT_VECTOR_2D(212, 204)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_BEARSY_TROTTER_MED_02
							vSize = INIT_VECTOR_2D(316, 304)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_BEARSY_TROTTER_LARGE_01
							vSize = INIT_VECTOR_2D(472, 452)
						BREAK
						
					ENDSWITCH	
				BREAK
				
				DEFAULT	
					RELOOP
				BREAK
			ENDSWITCH
			
			////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_DRAW_THROWN_ENEMY_PROJECTILES] Sprite: ", CMHDZ_SPRITE_TO_STRING(eSprite))	
			CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sEnemyProjectiles[i].vSpritePos, vSize, 0, sCMHDZData.rgbaSprite)
		ELIF sCMHDZData.sEnemyProjectiles[i].eState = CMHDZ_PROJECTILE_STATE_IMPACT
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC CMHDZ_HANDLE_BEARSY_DAMAGE(CMHDZ_BOSS_DATA &sBossData, BOOL bHeadshot)

	// Handle bearsy damage
	IF sBossData.bShotThisFrame
		EXIT
	ENDIF
	
	sBossData.bShotThisFrame = TRUE
	FLOAT fDamage = CMHDZ_GET_AMOUNT_BOSS_HAS_BEEN_DAMAGED(bHeadshot, sBossData.eCurrentBossState, sBossData.iActiveLane)
		
	IF (fDamage = 0.0)
		//sBossData.bShotThisFrame = FALSE
	ELSE
		CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BULLET_HIT_BEARSY, sBossData.vSpritePos)
		CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BEARSY_PAIN, sBossData.vSpritePos)
		
		IF bHeadshot
			CMHDZ_INCREASE_ENEMY_HEADSHOT_COUNT_FOR_CURRENT_LEVEL()
			CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_AT_LEAST_ONE_HEADSHOT)
		ENDIF

		CMHDZ_ADD_TO_LOCAL_PLAYER_SCORE( CMHDZ_SCORE_TYPE_BEARSY )
		CMHDZ_INCREASE_ENEMY_HIT_COUNT_FOR_CURRENT_LEVEL(FALSE)
		
		sBossData.fHealth -= fDamage
	ENDIF


ENDPROC


PROC CMHDZ_BOSS_STATE_UPDATE(CMHDZ_BOSS_DATA &sBossData, CMHDZ_SPRITE &eSprite)

	CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_BOSS_STATE_UPDATE - Boss State INT: ", sBossData.eCurrentBossState)

	INT iLastFrame = 6
	INT iFrameTime = 150
	
	FLOAT fEnemyPositionToAppear = TO_FLOAT(sCMHDZData.iCurrentScreen) * (cfCMHDZ_SCREEN_LENGTH)
	
	// GET Frame based upon lane.
	CMHDZ_BOSS_PROCESS_ANIM_DATA(sBossData, eSprite, iLastFrame, iFrameTime)
	
	// State change timer
	sBossData.fStateChangeTimer +=  (0.0+@1000)
	
	IF (sBossData.fStateChangeTimer%100.0) <= 10.0
		CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_CHANGE_BOSS_STATE - fStateChangeTimer: ", sBossData.fStateChangeTimer)
	ENDIF
	
	// Get enemy weapon data
	CMHDZ_PLAYER_WEAPON_TYPE weaponType = sCMHDZData.sPlayerData[0].weaponType
	INT iClipSize = sCMHDZData.sPlayerData[0].weaponData[weaponType].iAmmoInClip

	SWITCH sBossData.eCurrentBossState

		CASE CMHDZ_BOSS_STATE_INACTIVE
			IF CMHDZ_IS_LEVEL_STOPPED()
				CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_BEARSY_APPEAR)
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WAITING)
				CMHDZ_TRIGGER_MUSIC_EVENT(CMHDZ_MUSIC_EVENT_FINAL_BOSS)
				
				sBossData.fStateChangeTimer = 0.0
				
				// Initialising here
				sBossData.fDamageThreshold_1 = (sBossData.fHealth * 0.75)
				sBossData.fDamageThreshold_2 = (sBossData.fHealth * 0.5)
				sBossData.fDamageThreshold_3 = (sBossData.fHealth * 0.25)				
			ENDIF
		BREAK

		CASE CMHDZ_BOSS_STATE_WAITING
			IF sBossData.fStateChangeTimer >= (3500)
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP, CMHDZ_BOSS_STATE_WAITING)
			ENDIF
		BREAK

		CASE CMHDZ_BOSS_STATE_FOREGROUND_POP_UP
//			eSprite = CMHDZ_SPRITE_BEARSY_FOREGROUND_IDLE_01
//			sCMHDZData.CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WALKING
			IF sBossData.vSpritePos.y > 780 // Have Bearsy pop-up on screen
				sBossData.vSpritePos.y -= (1.0*(0.0+@1000))
			ELSE
				sBossData.vSpritePos.y = 780
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_IDLE)
			ENDIF
		BREAK
	
		CASE CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN
			
			IF sBossData.vSpritePos.y < (1080 + (sBossData.vScale.y/2.0))
				sBossData.vSpritePos.y += (1.0*(0.0+@1000))
			ELSE
				CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_BEARSY)							
//				sBossData.fTimeToThrow = GET_RANDOM_FLOAT_IN_RANGE(1800.0, 2600.0)
//				SET_BITMASK_ENUM_AS_ENUM_BY_BOOL(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_REVERSE, INT_TO_BOOL(GET_RANDOM_INT_IN_RANGE(0, 2)))
//				
//				IF IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_REVERSE)
//					sBossData.vSpritePos.X = -200
//				ELSE // Appear from right
//					sBossData.vSpritePos.X = 1920 + 200
//				ENDIF
//				
//				// Middle lane
//				sBossData.iActiveLane = ciCMHDZ_LANE_MIDDLE
//				
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WALKING)
			ENDIF
		BREAK
	
		CASE CMHDZ_BOSS_STATE_FOREGROUND_IDLE
			IF sBossData.fStateChangeTimer >= (((cfBOSS_MaxHealth+sBossData.fHealth)*15)/4.0)
				sBossData.fStateChangeTimer = 0.0
				//sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_ATTACK_READY)
			ENDIF
			
			IF sBossData.bShotThisFrame
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_BLOCKING)
				CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BULLET_HIT_BEARSY_DEFLECT, sBossData.vSpritePos)
				CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BULLET_HIT_METAL, sBossData.vSpritePos)
				
				TWS_CREATE_FX(INIT_VECTOR_2D(sBossData.vSpritePos.x + 60, sBossData.vSpritePos.Y - 20), CMHDZ_FX_ENEMY_SHOT, ciCMHDZ_LANE_FOREGROUND)

			ENDIF
		BREAK

		CASE CMHDZ_BOSS_STATE_BLOCKING
			
			IF iClipSize = 0
			OR sBossData.fStateChangeTimer >= (1500.0)
				CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BEARSY_ATTACK, sBossData.vSpritePos)
				//sCMHDZData.iBearAttackTime = GET_GAME_TIMER()
				CMHDZ_DAMAGE_PLAYER(CMHDZ_DAMAGE_PLAYER_TYPE_SLASH)
				sBossData.fAnimTimer = 0.0
				sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				
				TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_BEARSY_FOREGROUND_ATTACKING_BLOOD, ciCMHDZ_LANE_FOREGROUND)
				TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_BEARSY_FOREGROUND_SPARKS, ciCMHDZ_LANE_FOREGROUND)
				TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_SLASH_EFFECT, ciCMHDZ_LANE_FOREGROUND)
				
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_ATTACKING_COUNTER)
				BREAK
			ENDIF
			
			IF sBossData.bShotThisFrame
//				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_BLOCKING)
				CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BULLET_HIT_BEARSY_DEFLECT, sBossData.vSpritePos)
				CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BULLET_HIT_METAL, sBossData.vSpritePos)
				
				TWS_CREATE_FX(INIT_VECTOR_2D(sBossData.vSpritePos.x + 60, sBossData.vSpritePos.Y - 20), CMHDZ_FX_ENEMY_SHOT, ciCMHDZ_LANE_FOREGROUND)

				BREAK
			ENDIF
			
			IF sBossData.fStateChangeTimer >= (1250.0)
				//sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_IDLE)
			ENDIF
			
		BREAK
		
		CASE CMHDZ_BOSS_STATE_ATTACK_READY
			// Flash green
			IF (sBossData.fStateChangeTimer % 300) <= 150
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 100, 255, 100)
			ELSE
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
			ENDIF
			
			IF sBossData.bShotThisFrame
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_STUNNED_FOREGROUND)
			ENDIF
			
			IF sBossData.fStateChangeTimer >= (((cfBOSS_MaxHealth+sBossData.fHealth)*10)/4.0)
				sBossData.fStateChangeTimer = 0.0
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)

				CMHDZ_PLAY_SOUND_AND_RELEASE(CMHDZ_AUDIO_EFFECT_BEARSY_ATTACK, sBossData.vSpritePos)
				//sCMHDZData.iBearAttackTime = GET_GAME_TIMER()
				CMHDZ_DAMAGE_PLAYER(CMHDZ_DAMAGE_PLAYER_TYPE_SLASH)
				sBossData.fAnimTimer = 0.0
				sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				
				TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_BEARSY_FOREGROUND_ATTACKING_BLOOD, ciCMHDZ_LANE_FOREGROUND)
				TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_BEARSY_FOREGROUND_SPARKS, ciCMHDZ_LANE_FOREGROUND)
				TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_SLASH_EFFECT, ciCMHDZ_LANE_FOREGROUND)
						
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_ATTACKING)
			ENDIF

		BREAK
		
		CASE CMHDZ_BOSS_STATE_ATTACKING
		CASE CMHDZ_BOSS_STATE_ATTACKING_COUNTER
			IF IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_ANIM_FINISHED_ONCE)// If anim is finished transition
				sBossData.fStateChangeTimer = 0.0
				sBossData.fAnimTimer = 0.0
				sBossData.iAnimFrame = 0
				CLEAR_BITMASK_ENUM_AS_ENUM(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_ANIM_FINISHED_ONCE)// Clear the bit
								
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN)
			ENDIF
		BREAK
				
		CASE CMHDZ_BOSS_STATE_STUNNED_FOREGROUND
			// Flash yellow and lightly shake
			IF (sBossData.fStateChangeTimer % 300) <= 150
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 200, 100)
				sCMHDZData.sBossData.vSpritePos.x += 0.05*(0.0+@1000)
			ELSE
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
				
				IF sBossData.bShotThisFrame
					INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 50, 50)
					TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_BEARSY_FOREGROUND_PAIN_BLOOD, ciCMHDZ_LANE_FOREGROUND)
				ENDIF
				
				sCMHDZData.sBossData.vSpritePos.x -= 0.05*(0.0+@1000)
			ENDIF
		
			IF sBossData.fStateChangeTimer >= 2000
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
				sBossData.fStateChangeTimer = 0.0
				//sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				//TWS_CREATE_FX(sCMHDZData.sBossData.vSpritePos, CMHDZ_FX_BEARSY_FOREGROUND_PAIN_BLOOD, ciCMHDZ_LANE_FOREGROUND)
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN)
			ENDIF
			
			// Health ran out. Boss is dead.
			IF sBossData.fHealth <= 0.0
				CMHDZ_KILL_ALL_ENEMIES_IN_RANGE_NO_SCORE(sCMHDZData.sBossData.vSpritePos)
				CMHDZ_ADD_TO_LOCAL_PLAYER_SCORE( CMHDZ_SCORE_TYPE_BEARSY_KILL )
				
				CMHDZ_STOP_SOUND(CMHDZ_AUDIO_EFFECT_BEARSY_CHAINSAW_LOOP)
				CMHDZ_PLAY_SOUND(CMHDZ_AUDIO_EFFECT_BEARSY_DIE, sCMHDZData.sBossData.vSpritePos)
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_DYING)
			ENDIF
		BREAK
		
		CASE CMHDZ_BOSS_STATE_FOREGROUND_PAIN
			// Flash red
			IF (sBossData.fStateChangeTimer % 300) <= 150
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 50, 50)
			ELSE
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
			ENDIF
		
			IF sBossData.fStateChangeTimer >= 1000.0
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
				sBossData.fStateChangeTimer = 0.0
				//sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN)
			ENDIF
		BREAK
	
		CASE CMHDZ_BOSS_STATE_WALKING
		
			CMHDZ_BOSS_MIDBACK_WALKING_UPDATE(sBossData, iFrameTime)
			
			IF sBossData.bShotThisFrame
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 50, 50)
			ELSE
				INIT_RGBA_STRUCT(sBossData.rgbaColor, 255, 255, 255)
			ENDIF
		
			IF (sBossData.fStateChangeTimer >= sBossData.fTimeToThrow)
			AND IS_FLOAT_IN_RANGE(sBossData.vSpritePos.x, 400.0, 1400.0)
			AND sBossData.fStateChangeTimer >= sBossData.fTimeToThrow
			AND sBossData.fTimeToThrow > 0.0 // If time to throw doesn't have a value don't do it.
				sBossData.fStateChangeTimer = 0.0
				sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				sBossData.fAnimTimer = 0.0
				CMHDZ_INIT_THROWN_BEARSY_PROJECTILE()
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_THROWING)
			ENDIF
			
			IF sBossData.bShotThisFrame

				IF sBossData.fHealth <= sBossData.fDamageThreshold_1
				AND NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)
					sBossData.fHealth = sBossData.fDamageThreshold_1
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_STUNNED_BACKGROUND)
				ENDIF
				
				IF sBossData.fHealth <= sBossData.fDamageThreshold_2
				AND NOT IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
					sBossData.fHealth = sBossData.fDamageThreshold_2
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_STUNNED_BACKGROUND)		
				ENDIF
				
				IF sBossData.fHealth <= sBossData.fDamageThreshold_3
					CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_STUNNED_BACKGROUND)		
				ENDIF

			ENDIF

		BREAK
		
		CASE CMHDZ_BOSS_STATE_WALKING_INV
			// Flash red
			IF (sBossData.fStateChangeTimer % 500) <= 150
				INIT_RGBA_STRUCT(sBossData.rgbaColor, 255, 50, 50)
			ELSE
				INIT_RGBA_STRUCT(sBossData.rgbaColor, 255, 255, 255)
			ENDIF
		
			CMHDZ_BOSS_INVINCIBLE_WALKING_UPDATE(sBossData, iFrameTime)
		BREAK
		
		CASE CMHDZ_BOSS_STATE_STUNNED_BACKGROUND
		
			// Flash red
			IF (sBossData.fStateChangeTimer % 500) <= 150
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 50, 50)
			ELSE
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
			ENDIF
				
			IF sBossData.fStateChangeTimer >= 1500
				INIT_RGBA_STRUCT(sCMHDZData.sBossData.rgbaColor, 255, 255, 255)
				sBossData.fStateChangeTimer = 0.0
				//sBossData.iAnimFrame = 0 // Reset anim frames if using timers
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WALKING_INV)
			ENDIF
		
		BREAK
	
		CASE CMHDZ_BOSS_STATE_THROWING
			IF IS_BITMASK_ENUM_AS_ENUM_SET(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_ANIM_FINISHED_ONCE)// If anim is finished transition
				sBossData.fTimeToThrow = GET_RANDOM_FLOAT_IN_RANGE(1800.0, 2600.0)
				sBossData.iAnimFrame = 0 // Reset anim frame here
				sBossData.fAnimTimer = 0.0
				sBossData.fStateChangeTimer = 0.0
				CLEAR_BITMASK_ENUM_AS_ENUM(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_ANIM_FINISHED_ONCE)// Clear the bit
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WALKING, CMHDZ_BOSS_STATE_THROWING)
				CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] Check the boss health: ", sBossData.fHealth)				
			ENDIF
		BREAK
		
		CASE CMHDZ_BOSS_STATE_SPAWN_HELPERS_1
			CMHDZ_ADD_ENEMY_TO_LIST(1850.0000, 430.0000, 1100.0000, ciCMHDZ_LANE_BACKGROUND, FALSE, CMHDZ_ENEMY_5, fEnemyPositionToAppear, TRUE, FALSE, 0, 1)
			CMHDZ_ADD_ENEMY_TO_LIST(100, 680, 900, ciCMHDZ_LANE_MIDDLE, TRUE, CMHDZ_ENEMY_5, fEnemyPositionToAppear, TRUE, FALSE, 0, 1)
			CMHDZ_ADD_ENEMY_TO_LIST(1850, 950, 1050, ciCMHDZ_LANE_FOREGROUND, FALSE, CMHDZ_FOREGROUND_ENEMY_T5_2, fEnemyPositionToAppear, TRUE, FALSE, 0, 1)

			SET_BITMASK_ENUM_AS_ENUM(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_1)

			CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WAIT_FOR_ALL_ENEMIES_DEAD)
			
		BREAK
	
		CASE CMHDZ_BOSS_STATE_SPAWN_HELPERS_2
			CMHDZ_ADD_ENEMY_TO_LIST(100, 340, 500, ciCMHDZ_LANE_BACKGROUND, TRUE, CMHDZ_ENEMY_5, fEnemyPositionToAppear, TRUE, FALSE, 0, 1)
			CMHDZ_ADD_ENEMY_TO_LIST(1850, 675, 780, ciCMHDZ_LANE_MIDDLE, FALSE, CMHDZ_ENEMY_5, fEnemyPositionToAppear, TRUE, FALSE, 0, 1)
			CMHDZ_ADD_ENEMY_TO_LIST(1850, 430, 1500, ciCMHDZ_LANE_BACKGROUND, FALSE, CMHDZ_ENEMY_5, fEnemyPositionToAppear, TRUE, FALSE, 0, 2)
			CMHDZ_ADD_ENEMY_TO_LIST(1850, 950, 560, ciCMHDZ_LANE_FOREGROUND, FALSE, CMHDZ_FOREGROUND_ENEMY_T5_1, fEnemyPositionToAppear, TRUE, FALSE, 0, 2)
			
			SET_BITMASK_ENUM_AS_ENUM(sBossData.eDataBits, CMHDZ_BOSS_DATA_BITS_SPAWNED_ENEMIES_2)
			
			CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_WAIT_FOR_ALL_ENEMIES_DEAD)
		
		BREAK
		
		CASE CMHDZ_BOSS_STATE_WAIT_FOR_ALL_ENEMIES_DEAD
		
			IF CMHDZ_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_SCREEN() <= 1
				sCMHDZData.iStaticScreenPhase = 0
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FOREGROUND_POP_UP)
			ENDIF
		
		BREAK
		
		CASE CMHDZ_BOSS_STATE_DYING
		
			IF sBossData.vSpritePos.y < (1080 + (sBossData.vScale.y/2.0))
				sBossData.vSpritePos.y += (1.0*(0.0+@400))
			ELSE
				CMHDZ_CHANGE_BOSS_STATE(CMHDZ_BOSS_STATE_FINAL)
			ENDIF
						
		BREAK
		
		CASE CMHDZ_BOSS_STATE_FINAL
			
		BREAK
	
	ENDSWITCH
	
	// Handle looping chainsaw sound
	SWITCH sBossData.eCurrentBossState

		CASE CMHDZ_BOSS_STATE_FOREGROUND_IDLE
		CASE CMHDZ_BOSS_STATE_FOREGROUND_PAIN
		CASE CMHDZ_BOSS_STATE_FOREGROUND_POP_DOWN
		CASE CMHDZ_BOSS_STATE_FOREGROUND_POP_UP
		CASE CMHDZ_BOSS_STATE_BLOCKING
		CASE CMHDZ_BOSS_STATE_ATTACK_READY
		CASE CMHDZ_BOSS_STATE_ATTACKING
		CASE CMHDZ_BOSS_STATE_ATTACKING_COUNTER
		CASE CMHDZ_BOSS_STATE_STUNNED_FOREGROUND
			CMHDZ_PLAY_SOUND_LOOPING(CMHDZ_AUDIO_EFFECT_BEARSY_CHAINSAW_LOOP, sCMHDZData.sBossData.vSpritePos)
		BREAK
	ENDSWITCH
	
	sBossData.bShotThisFrame = FALSE
ENDPROC

PROC CMHDZ_DRAW_BOSS_SPRITE_AND_EFFECTS(CMHDZ_SPRITE eSprite, INT iLane)

	IF sCMHDZData.sBossData.iActiveLane = iLane
		CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sBossData.vSpritePos, sCMHDZData.sBossData.vScale, 0.0, sCMHDZData.sBossData.rgbaColor)
	ENDIF

//	CMHDZ_HANDLE_FX()
//	CMHDZ_DRAW_BULLETS_AND_EFFECTS(ciCMHDZ_LANE_FOREGROUND)
//	CMHDZ_DRAW_GORE_EFFECTS()
	
ENDPROC


PROC CMHDZ_DRAW_BOSS(INT iLane = ciCMHDZ_LANE_FOREGROUND)

	IF (sCMHDZData.eCurrentLevel != CMHDZ_LEVEL_SLAUGHTERHOUSE)
		EXIT 
	ENDIF
	
	IF sCMHDZData.iCurrentScreen < CMHDZ_GET_FINAL_SCREEN_IN_CURRENT_LEVEL()
		EXIT
	ENDIF
	
	CMHDZ_DRAW_BOSS_SPRITE_AND_EFFECTS(sCMHDZData.sBossData.eActiveSprite, iLane)
		
ENDPROC

FUNC BOOL CMHDZ_BEARSY_UPDATE()

	IF sCMHDZData.eCurrentLevel != CMHDZ_LEVEL_SLAUGHTERHOUSE
		RETURN FALSE // Don't process if we aren't in the right stage.
	ENDIF

	IF sCMHDZData.iCurrentScreen < CMHDZ_GET_FINAL_SCREEN_IN_LEVEL(CMHDZ_LEVEL_SLAUGHTERHOUSE)
		RETURN FALSE // Don't process if we aren't in the right screen.
	ENDIF
	
	IF sCMHDZData.sBossData.eCurrentBossState = CMHDZ_BOSS_STATE_FINAL
		RETURN TRUE
	ENDIF
	
	CMHDZ_BOSS_STATE_UPDATE(sCMHDZData.sBossData, sCMHDZData.sBossData.eActiveSprite)

	CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_BEARSY_UPDATE - fHealth: ", sCMHDZData.sBossData.fHealth)

	RETURN FALSE
ENDFUNC

FUNC BOOL CMHDZ_IS_BEARSY_ON_SCREEN()

	IF sCMHDZData.sBossData.vSpritePos.x <= 120.0
		RETURN FALSE
	ENDIF

	IF sCMHDZData.sBossData.vSpritePos.x >= 1820.0
		RETURN FALSE
	ENDIF
	
	IF sCMHDZData.sBossData.vSpritePos.y >= 900.0
		RETURN FALSE
	ENDIF
	
	IF sCMHDZData.sBossData.vSpritePos.y <= 150.0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//-------------------------------------------------
// PROJECTILE FUNCTIONS
//-------------------------------------------------

PROC CMHDZ_DRAW_THROWN_PLAYER_PROJECTILES(INT iPlayer)
	INT i
	TEXT_LABEL_31 tl31Sprite
	STRING sTextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_COMMON_PROPS)
	VECTOR_2D vSize
	
//	CMHDZ_DRAW_GAME_SPRITE(sCMHDZData.tl31BaseTextDict, "THROWING_DYNAMITE_SMALL_01" ,GET_DEBUG_TEXTURE_POS(0),  INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_SMALL_WIDTH, ciCMHDZ_DYNAMITE_SMALL_HEIGHT),0, sCMHDZData.rgbaSprite)
	
	REPEAT ciCMHDZ_MAX_PLAYER_THROWN_PROJECTILES i
		IF sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].eState = CMHDZ_PROJECTILE_STATE_INACTIVE
		ELIF sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].eState = CMHDZ_PROJECTILE_STATE_AIRBORNE
			SWITCH sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].eType
				CASE CMHDZ_PROJECTILE_DYNAMITE
					SWITCH sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].iSpriteAnimFrame
						CASE 5		tl31Sprite = "THROWING_DYNAMITE_SMALL_01"	vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_SMALL_WIDTH, ciCMHDZ_DYNAMITE_SMALL_HEIGHT)		BREAK
						CASE 4		tl31Sprite = "THROWING_DYNAMITE_SMALL_02"	vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_SMALL_WIDTH, ciCMHDZ_DYNAMITE_SMALL_HEIGHT)		BREAK
						CASE 3		tl31Sprite = "THROWING_DYNAMITE_MEDIUM_01"	vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_MEDIUM_WIDTH, ciCMHDZ_DYNAMITE_MEDIUM_HEIGHT)	BREAK
						CASE 2		tl31Sprite = "THROWING_DYNAMITE_MEDIUM_02"	vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_MEDIUM_WIDTH, ciCMHDZ_DYNAMITE_MEDIUM_HEIGHT)	BREAK
						CASE 1		tl31Sprite = "THROWING_DYNAMITE_LARGE_01"	vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_LARGE_WIDTH, ciCMHDZ_DYNAMITE_LARGE_HEIGHT)		BREAK
						CASE 0		tl31Sprite = "THROWING_DYNAMITE_LARGE_02"	vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_LARGE_WIDTH, ciCMHDZ_DYNAMITE_LARGE_HEIGHT)		BREAK		
					ENDSWITCH
				BREAK
				
				CASE CMHDZ_PROJECTILE_MOLOTOV
					SWITCH sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].iSpriteAnimFrame
						CASE 5		tl31Sprite = "THROWING_FIREBOTTLE_SMALL"	vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_SMALL_WIDTH, ciCMHDZ_MOLOTOV_SMALL_HEIGHT)		BREAK
						CASE 4		tl31Sprite = "THROWING_FIREBOTTLE_SMALL"	vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_SMALL_WIDTH, ciCMHDZ_MOLOTOV_SMALL_HEIGHT)		BREAK
						CASE 3		tl31Sprite = "THROWING_FIREBOTTLE_MEDIUM"	vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_MEDIUM_WIDTH, ciCMHDZ_MOLOTOV_MEDIUM_HEIGHT)		BREAK
						CASE 2		tl31Sprite = "THROWING_FIREBOTTLE_MEDIUM"	vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_MEDIUM_WIDTH, ciCMHDZ_MOLOTOV_MEDIUM_HEIGHT)		BREAK
						CASE 1		tl31Sprite = "THROWING_FIREBOTTLE_LARGE"	vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_LARGE_WIDTH, ciCMHDZ_MOLOTOV_LARGE_HEIGHT)		BREAK
						CASE 0		tl31Sprite = "THROWING_FIREBOTTLE_LARGE"	vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_LARGE_WIDTH, ciCMHDZ_MOLOTOV_LARGE_HEIGHT)		BREAK
					ENDSWITCH									
				BREAK
				
				CASE CMHDZ_PROJECTILE_NAILBOMB
					SWITCH sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].iSpriteAnimFrame
						CASE 5		tl31Sprite = "THROWING_ENEMY_NAILBOMB_SMALL"	vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_SMALL_WIDTH, ciCMHDZ_NAILBOMB_SMALL_HEIGHT)		BREAK
						CASE 4		tl31Sprite = "THROWING_ENEMY_NAILBOMB_SMALL"	vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_SMALL_WIDTH, ciCMHDZ_NAILBOMB_SMALL_HEIGHT)		BREAK
						CASE 3		tl31Sprite = "THROWING_ENEMY_NAILBOMB_MEDIUM"	vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_MEDIUM_WIDTH, ciCMHDZ_NAILBOMB_MEDIUM_HEIGHT)		BREAK
						CASE 2		tl31Sprite = "THROWING_ENEMY_NAILBOMB_MEDIUM"	vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_MEDIUM_WIDTH, ciCMHDZ_NAILBOMB_MEDIUM_HEIGHT)		BREAK
						CASE 1		tl31Sprite = "THROWING_ENEMY_NAILBOMB_LARGE"	vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_LARGE_WIDTH, ciCMHDZ_NAILBOMB_LARGE_HEIGHT)		BREAK
						CASE 0		tl31Sprite = "THROWING_ENEMY_NAILBOMB_LARGE"	vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_LARGE_WIDTH, ciCMHDZ_NAILBOMB_LARGE_HEIGHT)		BREAK
					ENDSWITCH									
				BREAK
				
				CASE CMHDZ_PROJECTILE_BRICK
					SWITCH sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].iSpriteAnimFrame
						CASE 5		tl31Sprite = "THROWING_ENEMY_BRICK_SMALL"	vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_SMALL_WIDTH, ciCMHDZ_BRICK_SMALL_HEIGHT)		BREAK
						CASE 4		tl31Sprite = "THROWING_ENEMY_BRICK_SMALL"	vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_SMALL_WIDTH, ciCMHDZ_BRICK_SMALL_HEIGHT)		BREAK
						CASE 3		tl31Sprite = "THROWING_ENEMY_BRICK_MEDIUM"	vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_MEDIUM_WIDTH, ciCMHDZ_BRICK_MEDIUM_HEIGHT)		BREAK
						CASE 2		tl31Sprite = "THROWING_ENEMY_BRICK_MEDIUM"	vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_MEDIUM_WIDTH, ciCMHDZ_BRICK_MEDIUM_HEIGHT)		BREAK
						CASE 1		tl31Sprite = "THROWING_ENEMY_BRICK_LARGE"	vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_LARGE_WIDTH, ciCMHDZ_BRICK_LARGE_HEIGHT)		BREAK
						CASE 0		tl31Sprite = "THROWING_ENEMY_BRICK_LARGE"	vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_LARGE_WIDTH, ciCMHDZ_BRICK_LARGE_HEIGHT)		BREAK
					ENDSWITCH									
				BREAK				
			ENDSWITCH
			
			//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_DRAW_THROWN_PLAYER_PROJECTILES] Player:", iPlayer, " Sprite: ", tl31Sprite)
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(sTextDict, tl31Sprite, sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].vSpritePos, vSize, 0, sCMHDZData.rgbaSprite)
		ELIF sCMHDZData.sPlayerData[iPlayer].sThrownProjectile[i].eState = CMHDZ_PROJECTILE_STATE_IMPACT
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC CMHDZ_DRAW_THROWN_ENEMY_PROJECTILES()
	INT i
	CMHDZ_SPRITE eSprite
	VECTOR_2D vSize
	
	REPEAT ciCMHDZ_MAX_ENEMY_PROJECTILES i
		IF sCMHDZData.sEnemyProjectiles[i].eState = CMHDZ_PROJECTILE_STATE_INACTIVE
		ELIF sCMHDZData.sEnemyProjectiles[i].eState = CMHDZ_PROJECTILE_STATE_AIRBORNE
			SWITCH sCMHDZData.sEnemyProjectiles[i].eType
				CASE CMHDZ_PROJECTILE_DYNAMITE
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0
						CASE 1
							eSprite = CMHDZ_SPRITE_THROWING_DYNAMITE_SMALL_01
							vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_SMALL_WIDTH, ciCMHDZ_DYNAMITE_SMALL_HEIGHT)		
						BREAK
	
//							eSprite = CMHDZ_SPRITE_THROWING_DYNAMITE_SMALL_02
//							vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_SMALL_WIDTH, ciCMHDZ_DYNAMITE_SMALL_HEIGHT)		
//						BREAK
						
						
//							eSprite = CMHDZ_SPRITE_THROWING_DYNAMITE_MEDIUM_01
//							vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_MEDIUM_WIDTH, ciCMHDZ_DYNAMITE_MEDIUM_HEIGHT)	
//						BREAK
						
						CASE 2
						CASE 3		
							eSprite = CMHDZ_SPRITE_THROWING_DYNAMITE_MEDIUM_02
							vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_MEDIUM_WIDTH, ciCMHDZ_DYNAMITE_MEDIUM_HEIGHT)	
						BREAK
						
						CASE 4	
						CASE 5
							eSprite = CMHDZ_SPRITE_THROWING_DYNAMITE_LARGE_01
							vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_LARGE_WIDTH, ciCMHDZ_DYNAMITE_LARGE_HEIGHT)		
						BREAK
						
						
//							eSprite = CMHDZ_SPRITE_THROWING_DYNAMITE_LARGE_02
//							vSize = INIT_VECTOR_2D(ciCMHDZ_DYNAMITE_LARGE_WIDTH, ciCMHDZ_DYNAMITE_LARGE_HEIGHT)	
//						BREAK
						
					ENDSWITCH			
				BREAK
				
				CASE CMHDZ_PROJECTILE_BRICK
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_THROWING_BRICK_SMALL
							vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_SMALL_WIDTH, ciCMHDZ_BRICK_SMALL_HEIGHT)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_THROWING_BRICK_MEDIUM
							vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_MEDIUM_WIDTH, ciCMHDZ_BRICK_MEDIUM_HEIGHT)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_THROWING_BRICK_LARGE
							vSize = INIT_VECTOR_2D(ciCMHDZ_BRICK_LARGE_WIDTH, ciCMHDZ_BRICK_LARGE_HEIGHT)	
						BREAK
						
					ENDSWITCH			
				BREAK
				
				CASE CMHDZ_PROJECTILE_NAILBOMB
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_THROWING_NAILBOMB_SMALL
							vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_SMALL_WIDTH, ciCMHDZ_NAILBOMB_SMALL_HEIGHT)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_THROWING_NAILBOMB_MEDIUM
							vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_MEDIUM_WIDTH, ciCMHDZ_NAILBOMB_MEDIUM_HEIGHT)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_THROWING_NAILBOMB_LARGE
							vSize = INIT_VECTOR_2D(ciCMHDZ_NAILBOMB_LARGE_WIDTH, ciCMHDZ_NAILBOMB_LARGE_HEIGHT)	
						BREAK
						
					ENDSWITCH	
				BREAK
				
				CASE CMHDZ_PROJECTILE_MOLOTOV
					
					SWITCH sCMHDZData.sEnemyProjectiles[i].iSpriteAnimFrame
						CASE 0	
						CASE 1		
							eSprite = CMHDZ_SPRITE_THROWING_MOLOTOV_SMALL
							vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_SMALL_WIDTH, ciCMHDZ_MOLOTOV_SMALL_HEIGHT)		
						BREAK
						
						CASE 2		
						CASE 3		
							eSprite = CMHDZ_SPRITE_THROWING_MOLOTOV_MEDIUM
							vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_MEDIUM_WIDTH, ciCMHDZ_MOLOTOV_MEDIUM_HEIGHT)	
						BREAK
						
						CASE 4		
						CASE 5
							eSprite = CMHDZ_SPRITE_THROWING_MOLOTOV_LARGE
							vSize = INIT_VECTOR_2D(ciCMHDZ_MOLOTOV_LARGE_WIDTH, ciCMHDZ_MOLOTOV_LARGE_HEIGHT)	
						BREAK
						
					ENDSWITCH	
				BREAK
				
				DEFAULT	
					RELOOP
				BREAK
			ENDSWITCH
			
			////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_DRAW_THROWN_ENEMY_PROJECTILES] Sprite: ", CMHDZ_SPRITE_TO_STRING(eSprite))	
			CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sEnemyProjectiles[i].vSpritePos, vSize, 0, sCMHDZData.rgbaSprite)
		ELIF sCMHDZData.sEnemyProjectiles[i].eState = CMHDZ_PROJECTILE_STATE_IMPACT
		ENDIF
		
	ENDREPEAT
	
	// Bearsy projectiles
	IF CMHDZ_IS_BEARSY_ON_SCREEN()
		CMHDZ_DRAW_THROWN_BEARSY_PROJECTILES()
	ENDIF
ENDPROC

//-------------------------------------------------
// ITEM FUNCTIONS
//-------------------------------------------------

FUNC CMHDZ_SPRITE CMHDZ_GET_SPRITE_FOR_ITEM_TYPE(CMHDZ_ITEM_TYPES eType)
	CMHDZ_SPRITE eSprite = CMHDZ_SPRITE_NONE 
	
	SWITCH (eType)
		CASE CMHDZ_ITEM_HEALTH
			eSprite = CMHDZ_SPRITE_ITEM_HEALTH
		BREAK
		
//		CASE CMHDZ_ITEM_REVOLVER_AMMO
//			eSprite = CMHDZ_SPRITE_ITEM_REVOLVER_AMMO
//		BREAK
		
		CASE CMHDZ_ITEM_NAIL_GUN_AMMO
			eSprite = CMHDZ_SPRITE_ITEM_MACHINE_GUN_AMMO
		BREAK
		
		CASE CMHDZ_ITEM_SHOTGUN_AMMO
			eSprite = CMHDZ_SPRITE_ITEM_SHOTGUN_AMMO
		BREAK
		
		CASE CMHDZ_ITEM_RIFLE_AMMO
			eSprite = CMHDZ_SPRITE_ITEM_RIFLE_AMMO
		BREAK
		
		CASE CMHDZ_ITEM_SHIELD
			eSprite = CMHDZ_SPRITE_ITEM_SHIELD
		BREAK
		
		CASE CMHDZ_ITEM_BUFF
			eSprite = CMHDZ_SPRITE_ITEM_BUFF
		BREAK
		
		CASE CMHDZ_ITEM_MOLOTOV
			eSprite = CMHDZ_SPRITE_ITEM_MOLOTOV_AMMO
		BREAK
		
		CASE CMHDZ_ITEM_NAILBOMB
			eSprite = CMHDZ_SPRITE_ITEM_NAILBOMB_AMMO
		BREAK
		
		CASE CMHDZ_ITEM_CASH
			eSprite = CMHDZ_SPRITE_ITEM_CASH
		BREAK
		
		CASE CMHDZ_ITEM_VIDEOTAPE
			eSprite = CMHDZ_SPRITE_ITEM_VIDEOTAPE
		BREAK
		
		
	ENDSWITCH
	
	RETURN eSprite
ENDFUNC 

PROC CMHDZ_DRAW_ITEMS(INT iDistance)
		
	CMHDZ_SPRITE eSprite
	FLOAT fScale = 1.0

	SWITCH iDistance
	CASE 0
		fScale = 0.75
	BREAK
	CASE 2
		//fScale = 2.5
	BREAK
	ENDSWITCH
	
//	IF iDistance = 2
//		CMHDZ_DRAW_GAME_SPRITE("ammo1",  GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PISTOL_AMMO_CLIP_WIDTH*1.0, cfCMHDZ_SPRITE_HUD_PISTOL_AMMO_CLIP_HEIGHT*1.0), 0, sCMHDZData.rgbaSprite)
//		CMHDZ_DRAW_GAME_SPRITE("POWERUP_PAINKILLERS",  GET_DEBUG_TEXTURE_POS(1), INIT_VECTOR_2D(cfCMHDZ_ITEM_HEALTH_WIDTH*1.0, cfCMHDZ_ITEM_HEALTH_HEIGHT*1.0), 0, sCMHDZData.rgbaSprite)
//		CMHDZ_DRAW_GAME_SPRITE("ammo4",  GET_DEBUG_TEXTURE_POS(2), INIT_VECTOR_2D(cfCMHDZ_ITEM_HEALTH_WIDTH*1.0, cfCMHDZ_ITEM_HEALTH_HEIGHT*1.0), 0, sCMHDZData.rgbaSprite)
//	ENDIF
	
	
	INT i
	FOR i = 0 TO ciCMHDZ_MAX_ITEMS -1

		IF sCMHDZData.sItemData[i].iLane = iDistance 
		AND (sCMHDZData.sItemData[i].bRendering OR sCMHDZData.sItemData[i].iRenderAfterWarpCount > 0 OR sCMHDZData.sItemData[i].iMinScreen > 0)
						
		//	//CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [CMHDZ_DRAW_ITEMS] Drawing item ", i, " Type:", ENUM_TO_INT(sCMHDZData.sItemData[i].eItemType), " At pos: ", sCMHDZData.sItemData[i].vSpritePos.x, ", ", sCMHDZData.sItemData[i].vSpritePos.y)
			IF sCMHDZData.sItemData[i].bRendering
				eSprite = CMHDZ_GET_SPRITE_FOR_ITEM_TYPE(sCMHDZData.sItemData[i].eItemType)
				CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sItemData[i].vSpritePos, MULTIPLY_VECTOR_2D(sCMHDZData.sItemData[i].vSize, fScale), 0, sCMHDZData.rgbaSprite)
			ENDIF
				
			IF IS_MOVING_LEVEL()
//				IF iDistance = 2

					sCMHDZData.sItemData[i].vSpritePos.x -= CMHDZ_GET_ITEM_UPDATE()
					IF sCMHDZData.sItemData[i].vSpritePos.x < -200
						sCMHDZData.sItemData[i].bRendering = FALSE
					ENDIF
//				ELSE
//					sCMHDZData.sItemData[i].vSpritePos.x -= CMHDZ_GET_ITEM_UPDATE()
//					IF CMHDZ_GET_CURRENT_LEVEL() = CMHDZ_LEVEL_CITY
//						IF sCMHDZData.sItemData[i].vSpritePos.x < -200
//							sCMHDZData.sItemData[i].vSpritePos.x = cfBASE_SCREEN_WIDTH + sCMHDZData.sItemData[i].vSize.X/2
//							sCMHDZData.sItemData[i].iWarpCount++
//						ENDIF
//					ELSE
					IF sCMHDZData.sItemData[i].iRenderAfterWarpCount > 0
						IF sCMHDZData.sItemData[i].vSpritePos.x < 0
							sCMHDZData.sItemData[i].vSpritePos.x = CMHDZ_GET_OFFSET_FOR_ELEMENT_WARP_FOR_CURRENT_LEVEL()
							sCMHDZData.sItemData[i].iWarpCount++
						ENDIF
					ENDIF
//					ENDIF
//				ENDIF
			ENDIF
			
			IF NOT sCMHDZData.sItemData[i].bRendering
				IF sCMHDZData.sItemData[i].iRenderAfterWarpCount > 0
					IF sCMHDZData.sItemData[i].iWarpCount = sCMHDZData.sItemData[i].iRenderAfterWarpCount
						
						sCMHDZData.sItemData[i].bRendering = TRUE
						#IF IS_DEBUG_BUILD
							//CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [CMHDZ_DRAW_ITEMS] Setting to render item ", i, " Type: ", CMHDZ_ITEM_TYPES_TO_STRING(sCMHDZData.sItemData[i].eItemType), " As warped this num times: ", sCMHDZData.sItemData[i].iRenderAfterWarpCount) 
						#ENDIF
					ENDIF
				ENDIF
				
				IF sCMHDZData.sItemData[i].iMinScreen > 0 AND sCMHDZData.sItemData[i].iMinScreen = CMHDZ_GET_CURRENT_LEVEL_SCREEN()
					IF NOT CMHDZ_IS_POSITION_ON_SCREEN(sCMHDZData.sItemData[i].vSpritePos)
										
						sCMHDZData.sItemData[i].bRendering = TRUE
						#IF IS_DEBUG_BUILD
							//CDEBUG1LN(DEBUG_MINIGAME, "{DSW} [CMHDZ_DRAW_ITEMS] Setting to render item ", i, " Type: ", CMHDZ_ITEM_TYPES_TO_STRING(sCMHDZData.sItemData[i].eItemType), " iMinScreen ", sCMHDZData.sItemData[i].iMinScreen, " = Current stage: ", CMHDZ_GET_CURRENT_LEVEL_SCREEN()) 
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF CMHDZ_IS_POSITION_ON_SCREEN(sCMHDZData.sItemData[i].vSpritePos)
		
			// Don't process if the timer isn't 0.
			IF(sAGHelpText.iCurrentHelpTextExpiry - NATIVE_TO_INT(GET_NETWORK_TIME())) > 0.0
				RELOOP
			ENDIF
			
			SWITCH sCMHDZData.sItemData[i].eItemType		
				CASE CMHDZ_ITEM_HEALTH
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_PAINKILLERS)
				BREAK
				
				CASE CMHDZ_ITEM_SHIELD
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_PILLS)
				BREAK
				
				CASE CMHDZ_ITEM_BUFF
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_BUFF)
				BREAK
				
				CASE CMHDZ_ITEM_VIDEOTAPE
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_VIDEO)
				BREAK
				
				CASE CMHDZ_ITEM_NAIL_GUN_AMMO
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_NAIL)
				BREAK
				
				CASE CMHDZ_ITEM_RIFLE_AMMO
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_RIFLE)
				BREAK
				
				CASE CMHDZ_ITEM_SHOTGUN_AMMO
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_SHOTGUN)
				BREAK
				
				CASE CMHDZ_ITEM_MOLOTOV
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_MOLOTOV)
				BREAK
				
				CASE CMHDZ_ITEM_NAILBOMB
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_NAILBOMB)
				BREAK
				
				CASE CMHDZ_ITEM_CASH
					CMHDZ_SET_HELP_TEXT_BIT(CMHDZ_HELP_TEXT_BIT_CASH)
				BREAK
				
			ENDSWITCH
		ENDIF

	ENDFOR
	
ENDPROC


//-------------------------------------------------
// ENEMY FUNCTIONS
//-------------------------------------------------
PROC CMHDZ_DRAW_ENEMIES(INT iLane = ciCMHDZ_LANE_BACKGROUND)

	CMHDZ_SPRITE eSprite 
	
	INT i
	FLOAT fScale
	VECTOr_2D v2Scale
	RGBA_COLOUR_STRUCT rgbaEnemy = sCMHDZData.rgbaSprite
	
//#IF IS_DEBUG_BUILD	
//	INT iBlockingCover
//#ENDIF

	FOR i = 0 TO ciCMHDZ_MAX_ENEMIES -1
		
		fScale = CMHDZ_GET_ENEMY_TYPE_SCALE_FOR_LANE(iLane) 

#IF IS_DEBUG_BUILD	
//	IF sCMHDZData.sEnemyData[i].eState != CMHDZ_ENEMY_STATE_DYING
//	AND sCMHDZData.sEnemyData[i].eState != CMHDZ_ENEMY_STATE_INACTIVE
//			TEXT_LABEL_23 sDebug = "Enemy number: "
//			sDebug += i
//			
//			DRAW_DEBUG_TEXT_2D(sDebug,
//				<<(sCMHDZData.sEnemyData[i].vSpritePos.x / 1920),
//				(sCMHDZData.sEnemyData[i].vSpritePos.y / 1080),
//				0.0>>,
//				250, 15, 15)
//			
//			
//			IF CMHDZ_IS_COORD_BLOCKED_BY_COVER(sCMHDZData.sEnemyData[i].iEnemyLane, sCMHDZData.sEnemyData[i].fStoppingX, iBlockingCover)
//				sDebug = "BLOCKED BY COVER"
//				DRAW_DEBUG_TEXT_2D(sDebug,
//					<<(sCMHDZData.sEnemyData[i].vSpritePos.x / 1920),
//					(sCMHDZData.sEnemyData[i].vSpritePos.y + 100 / 1080),
//					0.0>>,
//					250, 15, 15)
//			ENDIF
//			UNUSED_PARAMETER(iBlockingCover)	
//	ENDIF
#ENDIF
		
		IF (sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING 
		OR sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_STUNNED
		OR sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_HEADSHOT
		OR sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_BURNING)
		AND (sCMHDZData.sEnemyData[i].iEnemyLane = iLane 
		OR (iLane = ciCMHDZ_LANE_BACKGROUND AND(sCMHDZData.sEnemyData[i].iEnemyLane = ciCMHDZ_LANE_POPUP_LHS OR sCMHDZData.sEnemyData[i].iEnemyLane = ciCMHDZ_LANE_POPUP_RHS)))
		
			SWITCH sCMHDZData.sEnemyData[i].iEnemyType
			CASE CMHDZ_ENEMY_1
				eSprite = CMHDZ_SPRITE_GANG_01_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_ENEMY_2
				eSprite = CMHDZ_SPRITE_GANG_02_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_ENEMY_3
				eSprite = CMHDZ_SPRITE_GANG_03_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_ENEMY_4
				eSprite = CMHDZ_SPRITE_GANG_04_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_ENEMY_5
				eSprite = CMHDZ_SPRITE_GANG_05_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			
			CASE CMHDZ_THROWING_ENEMY_1
				eSprite = CMHDZ_SPRITE_GANG_01_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_THROWING_ENEMY_2
				eSprite = CMHDZ_SPRITE_GANG_02_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK 
			CASE CMHDZ_THROWING_ENEMY_3
				eSprite = CMHDZ_SPRITE_GANG_03_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_THROWING_ENEMY_4
				eSprite = CMHDZ_SPRITE_GANG_04_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			CASE CMHDZ_THROWING_ENEMY_5
				eSprite = CMHDZ_SPRITE_GANG_05_DEAD
				v2Scale = INIT_VECTOR_2D(cfCMHDZ_ENEMY_DEAD_SPRITE_WIDTH*fScale, cfCMHDZ_ENEMY_DEAD_SPRITE_HEIGHT*fScale)
			BREAK
			
			CASE CMHDZ_RARE_ENEMY_BUNNY
				eSprite = CMHDZ_SPRITE_GANG_RARE_BUNNY_DEAD
				v2Scale = INIT_VECTOR_2D(116*fScale, 196*fScale)
			BREAK
			
			CASE CMHDZ_RARE_ENEMY_MONKEY
				eSprite = CMHDZ_SPRITE_GANG_RARE_MONKEY_DEAD
				v2Scale = INIT_VECTOR_2D(116*fScale, 196*fScale)
			BREAK

			
			ENDSWITCH
			
			// Was it a headshot?
			IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_HEADSHOT
			
				//Default anim update
				//CDEBUG2LN(DEBUG_MINIGAME, " CMHDZ_DRAW_ENEMIES sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_HEADSHOT iDefaultFrameTimeCounter: ", sCMHDZData.sEnemyData[i].iDefaultFrameTimeCounter)
				//CDEBUG2LN(DEBUG_MINIGAME, " CMHDZ_DRAW_ENEMIES sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_HEADSHOT iDefaultUpdateFrames: ", sCMHDZData.sEnemyData[i].iDefaultUpdateFrames)
			
				IF sCMHDZData.sEnemyData[i].iHeadshotAnimFrame < 8 
					sCMHDZData.sEnemyData[i].iHeadshotAnimFrame += sCMHDZData.sEnemyData[i].iDefaultUpdateFrames
					//sCMHDZData.sEnemyData[i].iHeadshotAnimFrame = 0
				ENDIF
				
				IF sCMHDZData.sEnemyData[i].iGoreAnimFrame < 9
					sCMHDZData.sEnemyData[i].iGoreAnimFrame += (sCMHDZData.sEnemyData[i].iDefaultUpdateFrames*2)
				ENDIF
				
				IF (sCMHDZData.sEnemyData[i].iGoreAnimFrame > 10)
					sCMHDZData.sEnemyData[i].iGoreAnimFrame = 9
				ENDIF
				
				CDEBUG2LN(DEBUG_MINIGAME, " CMHDZ_DRAW_ENEMIES sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_HEADSHOT iHeadshotAnimFrame: ", sCMHDZData.sEnemyData[i].iHeadshotAnimFrame)
			
				SWITCH sCMHDZData.sEnemyData[i].iEnemyType
					CASE CMHDZ_ENEMY_1
					CASE CMHDZ_THROWING_ENEMY_1
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_01_HEADSHOT_01, sCMHDZData.sEnemyData[i].iHeadshotAnimFrame)
					BREAK
					CASE CMHDZ_ENEMY_2
					CASE CMHDZ_THROWING_ENEMY_2
						eSprite= CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_02_HEADSHOT_01, sCMHDZData.sEnemyData[i].iHeadshotAnimFrame)
					BREAK
					CASE CMHDZ_ENEMY_3
					CASE CMHDZ_THROWING_ENEMY_3
						eSprite= CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_03_HEADSHOT_01, sCMHDZData.sEnemyData[i].iHeadshotAnimFrame)
					BREAK
					CASE CMHDZ_ENEMY_4
					CASE CMHDZ_THROWING_ENEMY_4
						eSprite= CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_04_HEADSHOT_01, sCMHDZData.sEnemyData[i].iHeadshotAnimFrame)
					BREAK
					CASE CMHDZ_ENEMY_5
					CASE CMHDZ_THROWING_ENEMY_5
						eSprite= CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_05_HEADSHOT_01, sCMHDZData.sEnemyData[i].iHeadshotAnimFrame)
					BREAK					
				ENDSWITCH
			
				VECTOR_2D pos = sCMHDZData.sEnemyData[i].vSpritePos
				pos.y -= (0.35*(0.0+@1000))
				
			
				CMHDZ_DRAW_GAME_SPRITE(CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GORE_BACKGROUND_1, sCMHDZData.sEnemyData[i].iGoreAnimFrame),pos,
					INIT_VECTOR_2D(472.0,372.0),0.0, sCMHDZData.rgbaSprite)
						
			ENDIF
			
			IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_BURNING
			
				IF sCMHDZData.sEnemyData[i].iHeadshotAnimFrame < 4
					sCMHDZData.sEnemyData[i].iHeadshotAnimFrame += sCMHDZData.sEnemyData[i].iDefaultUpdateFrames
					//sCMHDZData.sEnemyData[i].iHeadshotAnimFrame = 0
				ENDIF
			
				SWITCH sCMHDZData.sEnemyData[i].iEnemyType
					CASE CMHDZ_ENEMY_1
					CASE CMHDZ_ENEMY_2
					CASE CMHDZ_ENEMY_3
					CASE CMHDZ_ENEMY_4
					CASE CMHDZ_ENEMY_5
					CASE CMHDZ_RARE_ENEMY_BUNNY
					CASE CMHDZ_RARE_ENEMY_MONKEY
//					CASE CMHDZ_RARE_ENEMY_RAT
					CASE CMHDZ_THROWING_ENEMY_1
					CASE CMHDZ_THROWING_ENEMY_2
					CASE CMHDZ_THROWING_ENEMY_3
					CASE CMHDZ_THROWING_ENEMY_4
					CASE CMHDZ_THROWING_ENEMY_5					
						
						// Use the enemy flash timer
						IF sCMHDZData.sEnemyData[i].iEnemyFlashTimer = 0
							sCMHDZData.sEnemyData[i].iEnemyFlashTimer = GET_GAME_TIMER()
							eSprite = CMHDZ_SPRITE_GANG_BURNING_01
						ELIF GET_GAME_TIMER() - sCMHDZData.sEnemyData[i].iEnemyFlashTimer >= 50
							eSprite = CMHDZ_SPRITE_GANG_BURNING_01
						ELSE
							IF (ROUND(TO_FLOAT(GET_GAME_TIMER() - sCMHDZData.sEnemyData[i].iEnemyFlashTimer)/ 100.0)) % 2 = 0
								eSprite = CMHDZ_SPRITE_GANG_BURNING_02
							ELSE
								eSprite = CMHDZ_SPRITE_GANG_BURNING_01
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			ENDIF
						
			IF sCMHDZData.sEnemyData[i].vSpritePos.x > 0 AND sCMHDZData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH
				CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sEnemyData[i].vSpritePos, v2Scale, 0, sCMHDZData.rgbaSprite)
			ENDIF
			
		ElIF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ACTIVE 
		OR 	 sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
			IF sCMHDZData.sEnemyData[i].iEnemyLane = iLane
			OR (iLane = ciCMHDZ_LANE_BACKGROUND  AND (sCMHDZData.sEnemyData[i].iEnemyLane = ciCMHDZ_LANE_POPUP_LHS OR sCMHDZData.sEnemyData[i].iEnemyLane = ciCMHDZ_LANE_POPUP_RHS))
				
				//-- Flash injured peds
				IF sCMHDZData.sEnemyData[i].iHealth < CMHDZ_GET_ENEMY_START_HEALTH_FOR_TYPE()
					IF sCMHDZData.sEnemyData[i].iEnemyFlashTimer = 0
						sCMHDZData.sEnemyData[i].iEnemyFlashTimer = GET_GAME_TIMER()
						rgbaEnemy = sCMHDZData.rgbaSprite
					ELIF GET_GAME_TIMER() - sCMHDZData.sEnemyData[i].iEnemyFlashTimer >= 1000
						rgbaEnemy = sCMHDZData.rgbaSprite
					ELSE
						IF (ROUND(TO_FLOAT(GET_GAME_TIMER() - sCMHDZData.sEnemyData[i].iEnemyFlashTimer)/ 100.0)) % 2 = 0
							rgbaEnemy = sCMHDZData.rgbaEnemyShot
						ELSE
							rgbaEnemy = sCMHDZData.rgbaSprite
						ENDIF
					ENDIF
				ENDIF
					
				// Process pop up peds at same time as background peds

				SWITCH sCMHDZData.sEnemyData[i].iEnemyType
				CASE CMHDZ_ENEMY_1
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GANG_01_ATTACKING
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_01_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_ENEMY_2
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GANG_02_ATTACKING
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_02_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_ENEMY_3
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GANG_03_ATTACKING
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_03_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_ENEMY_4
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GANG_04_ATTACKING
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_04_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_ENEMY_5
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GANG_05_ATTACKING
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_05_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
//				CASE CMHDZ_CART_1
//					eSprite = CMHDZ_SPRITE_CART1
//				BREAK
				
				CASE CMHDZ_THROWING_ENEMY_1
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_01_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame + 2)
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_01_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_THROWING_ENEMY_2
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_02_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame + 2)
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_02_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_THROWING_ENEMY_3
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_03_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame + 2)
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_03_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_THROWING_ENEMY_4
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_04_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame + 2)
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_04_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK
				CASE CMHDZ_THROWING_ENEMY_5
					IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_05_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame + 2)
					ELSE
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_05_THROWING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
					ENDIF
				BREAK

				ENDSWITCH
				
				// Rare hostage enemies
				
				SWITCH sCMHDZData.sEnemyData[i].iEnemyType
					CASE CMHDZ_RARE_ENEMY_BUNNY
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_RARE_BUNNY_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
						v2Scale = INIT_VECTOR_2D(116*fScale, 196*fScale)
					BREAK
					
					CASE CMHDZ_RARE_ENEMY_MONKEY
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_RARE_MONKEY_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
						v2Scale = INIT_VECTOR_2D(116*fScale, 196*fScale)
					BREAK
					
					CASE CMHDZ_RARE_ENEMY_RAT
						eSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GANG_RARE_RAT_RUNNING_01, sCMHDZData.sEnemyData[i].iSpriteAnimFrame)
						v2Scale = INIT_VECTOR_2D(140*fScale, 60*fScale)
						rgbaEnemy = sCMHDZData.rgbaSprite
					BREAK
				ENDSWITCH
				

				IF sCMHDZData.sEnemyData[i].vSpritePos.x > 0 
				AND sCMHDZData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH
					
					IF  CMHDZ_ENEMY_DATA_IS_BITMASK_SET(i, CMHDZ_ENEMY_DATA_BITS_IS_BONUS)
					AND CMHDZ_ENEMY_DATA_IS_BITMASK_SET(i, CMHDZ_ENEMY_DATA_BITS_REVERSE)
						sCMHDZData.sEnemyData[i].vSize.x = -ABSF(sCMHDZData.sEnemyData[i].vSize.x)
						CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_DRAW_ENEMIES - REversing the scale factor if coming from left vSize.x: ", sCMHDZData.sEnemyData[i].vSize.x)
					ENDIF

				//	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_DRAW_ENEMIES] Drawing enemy ",i, " At position X ", sCMHDZData.sEnemyData[i].vSpritePos.x, " Size: ", sCMHDZData.sEnemyData[i].vSize.X, ", ", sCMHDZData.sEnemyData[i].vSize.y, " Scale: ", fScale, " Lane: ",sCMHDZData.sEnemyData[i].iEnemyLane) 
					CMHDZ_DRAW_GAME_SPRITE(eSprite ,sCMHDZData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(sCMHDZData.sEnemyData[i].vSize.X*fScale, sCMHDZData.sEnemyData[i].vSize.y*fScale), 0, rgbaEnemy)
//					ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(sCMHDZData.sEnemyData[i].fStoppingX,  sCMHDZData.sEnemyData[i].vSpritePos.y),
//						sCMHDZData.sEnemyData[i].vSize, sCMHDZData.rgbaRed)
				//ELSE
				//	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_DRAW_ENEMIES] NOT Drawing enemy ", i, " As X Pos: ", sCMHDZData.sEnemyData[i].vSpritePos.x, " Lane: ",sCMHDZData.sEnemyData[i].iEnemyLane) 
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
		
ENDPROC

PROC CMHDZ_DRAW_FOREGROUND_ENEMIES()
	
	FLOAT fScale = 1.2
	CMHDZ_SPRITE eSprite
	VECTOR_2D vSize
	 
	INT i
	FOR i = 0 TO ciCMHDZ_MAX_ENEMIES -1
	
		IF sCMHDZData.sEnemyData[i].iEnemyLane != ciCMHDZ_LANE_FOREGROUND
			RELOOP
		ENDIF
			
		// Still alive and not stunned
		IF sCMHDZData.sEnemyData[i].iHealth > 0
		AND sCMHDZData.sEnemyData[i].eState != CMHDZ_ENEMY_STATE_STUNNED
		AND (sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ACTIVE
		OR sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING)
			vSize = sCMHDZData.sEnemyData[i].vSize
	
			SWITCH sCMHDZData.sEnemyData[i].iEnemyType
				CASE CMHDZ_FOREGROUND_ENEMY_T1_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_A
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T1_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_B
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T2_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_A
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T2_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_B
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T3_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_03_A
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T3_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_03_B
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T4_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_A
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T4_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_B
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T5_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_A
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T5_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_B
				BREAK

			ENDSWITCH
			
		
			// Is the enemy attacking
			IF CMHDZ_ENEMY_DATA_IS_BITMASK_SET(i, CMHDZ_ENEMY_DATA_BITS_WILL_INFLICT_MELEE_DAMAGE)
			AND sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_ATTACKING

				IF sCMHDZData.sEnemyData[i].fAnimTimer > 0
					SWITCH sCMHDZData.sEnemyData[i].iEnemyType
						CASE CMHDZ_FOREGROUND_ENEMY_T1_1
							eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_A_ATTACK
							vSize = INIT_VECTOR_2D(540, 537)
						BREAK
						
						CASE CMHDZ_FOREGROUND_ENEMY_T2_1
							eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_A_ATTACK
							vSize = INIT_VECTOR_2D(540, 537)
						BREAK
						
						CASE CMHDZ_FOREGROUND_ENEMY_T4_1
							eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_A_ATTACK
							vSize = INIT_VECTOR_2D(540, 537)
						BREAK
						
						CASE CMHDZ_FOREGROUND_ENEMY_T5_2
							eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_B_ATTACK
						BREAK
					ENDSWITCH
					
					sCMHDZData.sEnemyData[i].fAnimTimer -= (0.0+@1000)
				ENDIF
			ENDIF

			CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(vSize.x *fScale, vSize.Y*fScale), 0, sCMHDZData.rgbaSprite)
		ENDIF
		
		IF (sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING
		OR sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_STUNNED)
		
			SWITCH sCMHDZData.sEnemyData[i].iEnemyType
				CASE CMHDZ_FOREGROUND_ENEMY_T1_1
				CASE CMHDZ_FOREGROUND_ENEMY_T1_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_DEAD
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T2_1
				CASE CMHDZ_FOREGROUND_ENEMY_T2_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_DEAD
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T3_1
				CASE CMHDZ_FOREGROUND_ENEMY_T3_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_03_DEAD
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T4_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_A_DEAD
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T4_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_B_DEAD
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T5_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_A_DEAD
				BREAK			
				CASE CMHDZ_FOREGROUND_ENEMY_T5_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_B_DEAD
				BREAK
	//			CASE CMHDZ_FOREGROUND_ENEMY_BEAR
	//				eSprite = CMHDZ_SPRITE_LEVEL_03_FOREST_BEAR_02
	//			BREAK
			ENDSWITCH
			
			CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_ENEMY_FOREGROUND_DEAD_SPRITE_WIDTH*1.2, cfCMHDZ_ENEMY_FOREGROUND_DEAD_SPRITE_HEIGHT*1.2), 0, sCMHDZData.rgbaSprite)
		ENDIF
		
		IF (sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_BURNING)
		
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_BURNING_01
			
			IF sCMHDZData.sEnemyData[i].iSpriteAnimFrame < 4
				sCMHDZData.sEnemyData[i].iSpriteAnimFrame += (sCMHDZData.sEnemyData[i].iDefaultUpdateFrames)
				IF sCMHDZData.sEnemyData[i].iSpriteAnimFrame >= 4
					sCMHDZData.sEnemyData[i].iSpriteAnimFrame = 0
				ENDIF
			ENDIF
			
			VECTOR_2D pos = sCMHDZData.sEnemyData[i].vSpritePos
			pos.y -= (0.55*(0.0+@1000))
			//SCRIPT_ASSERT("[BAZ] CMHDZ_DRAW_FOREGROUND_ENEMIES - Foreground burning")
			CMHDZ_DRAW_GAME_SPRITE(CMHDZ_SPRITE_GET_NEXT_ENUM(eSprite, sCMHDZData.sEnemyData[i].iSpriteAnimFrame), pos, 
					INIT_VECTOR_2D(cfCMHDZ_ENEMY_FOREGROUND_DEAD_SPRITE_WIDTH*1.2, cfCMHDZ_ENEMY_FOREGROUND_DEAD_SPRITE_HEIGHT*1.2), 0, sCMHDZData.rgbaSprite)
		ENDIF
		
		IF sCMHDZData.sEnemyData[i].eState = CMHDZ_ENEMY_STATE_DYING_HEADSHOT
			SWITCH sCMHDZData.sEnemyData[i].iEnemyType
				CASE CMHDZ_FOREGROUND_ENEMY_T1_1
				CASE CMHDZ_FOREGROUND_ENEMY_T1_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_HEADSHOT
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T2_1
				CASE CMHDZ_FOREGROUND_ENEMY_T2_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_HEADSHOT
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T3_1
				CASE CMHDZ_FOREGROUND_ENEMY_T3_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_03_HEADSHOT
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T4_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_A_HEADSHOT
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T4_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_B_HEADSHOT
				BREAK
				CASE CMHDZ_FOREGROUND_ENEMY_T5_1
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_A_HEADSHOT
				BREAK			
				CASE CMHDZ_FOREGROUND_ENEMY_T5_2
					eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_B_HEADSHOT
				BREAK
	//			CASE CMHDZ_FOREGROUND_ENEMY_BEAR
	//				eSprite = CMHDZ_SPRITE_LEVEL_03_FOREST_BEAR_02
	//			BREAK
			ENDSWITCH
			
			IF sCMHDZData.sEnemyData[i].iGoreAnimFrame < 9 // Can sometimes see bearsy
				sCMHDZData.sEnemyData[i].iGoreAnimFrame += (sCMHDZData.sEnemyData[i].iDefaultUpdateFrames*2)
			ENDIF
			
			VECTOR_2D pos = sCMHDZData.sEnemyData[i].vSpritePos

			CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sEnemyData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_ENEMY_FOREGROUND_DEAD_SPRITE_WIDTH*1.2, cfCMHDZ_ENEMY_FOREGROUND_DEAD_SPRITE_HEIGHT*1.2), 0, sCMHDZData.rgbaSprite)

			pos.y -= (0.65*(0.0+@1000))
			// Draw the gore effects.
			CMHDZ_DRAW_GAME_SPRITE(CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_GORE_BACKGROUND_1, sCMHDZData.sEnemyData[i].iGoreAnimFrame),pos,
				INIT_VECTOR_2D(472.0,372.0),0.0, sCMHDZData.rgbaSprite)
				
		ENDIF
		
	ENDFOR
	
ENDPROC

//-------------------------------------------------
// FX FUNCTIONS
//-------------------------------------------------

PROC CMHDZ_FLASH_SCREEN_FOR_ACTIVE_EFFECT(RGBA_COLOUR_STRUCT colour, INT iMaxAlpha = 50)

	
	FLOAT fTimeToMaxAlpha = 5.0
	FLOAT fFrameTime = GET_FRAME_TIME()
	IF NOT sCMHDZData.bEffectHitmaxAlpha
		sCMHDZData.fEffectFlashAlpha += (TO_FLOAT(iMaxAlpha) / (fTimeToMaxAlpha/fFrameTime))
	//	sCMHDZData.fTotalTimeTaken += fFrameTime
		IF sCMHDZData.fEffectFlashAlpha > TO_FLOAT(iMaxAlpha)
			sCMHDZData.fEffectFlashAlpha = TO_FLOAT(iMaxAlpha)
			sCMHDZData.bEffectHitmaxAlpha = TRUE
		ENDIF
	ELSE
//		sCMHDZData.fTotalTimeTaken = 0
		sCMHDZData.fEffectFlashAlpha -= (TO_FLOAT(iMaxAlpha) / (fTimeToMaxAlpha/fFrameTime))
		IF sCMHDZData.fEffectFlashAlpha < 0
			sCMHDZData.fEffectFlashAlpha = 0
			sCMHDZData.bEffectHitmaxAlpha = FALSE
		ENDIF
	ENDIF
	
	colour.iA = ROUND(sCMHDZData.fEffectFlashAlpha)
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), colour)	
	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] [CMHDZ_FLASH_SCREEN_FOR_ACTIVE_EFFECT] Alpha: ", sCMHDZData.fEffectFlashAlpha, " Frame time: ", fFrameTime, " Delta: ", ROUND(TO_FLOAT(iMaxAlpha) / (fTimeToMaxAlpha/fFrameTime)))
ENDPROC

PROC CMHDZ_DRAW_BULLETS_AND_EFFECTS(INT iDistance)
	
	TEXT_LABEL_63 tl23Sprite
	CMHDZ_SPRITE eSprite
	INT p,i
	
	// Bullets
	FOR p = 0 TO ciCMHDZ_MAX_PLAYERS - 1
	
		FOR i = 0  TO ciCMHDZ_MAX_PROJECTILES - 1
			IF sCMHDZData.sPlayerData[p].sBulletData[i].bShot
					
				IF sCMHDZData.sPlayerData[p].sBulletData[i].bHitObject
					tl23Sprite = "bullet-hole"
				
				ELIF sCMHDZData.sPlayerData[p].sBulletData[i].vSpritePos.y < 300
					tl23Sprite = "smoke"
				
				ELIF !sCMHDZData.sPlayerData[p].sBulletData[i].bHit
					tl23Sprite = "bullet"
				ENDIF
				
				
				CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_HUD), tl23Sprite, sCMHDZData.sPlayerData[p].sBulletData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_SPRITE_IMPACT_WIDTH, cfCMHDZ_SPRITE_IMPACT_HEIGHT), 0, sCMHDZData.rgbaSprite)
				
				IF NOT sCMHDZData.sPlayerData[p].sBulletData[i].bPlayedAudio
				//	CMHDZ_PLAY_SOUND_FRONT_END(CMHDZ_AUDIO_EFFECT_BULLET_HIT_GENERIC)
					
					sCMHDZData.sPlayerData[p].sBulletData[i].bPlayedAudio = TRUE
				ENDIF

			ENDIF
		ENDFOR
		
	ENDFOR
	
	// FX
	VECTOR_2D vSize
	
	FOR i = 0  TO ciCMHDZ_MAX_FX - 1
		IF sCMHDZData.sFXData[i].bActive AND sCMHDZData.sFXData[i].iLane = iDistance
			
			SWITCH sCMHDZData.sFXData[i].eFXType
				CASE CMHDZ_FX_ENEMY_SHOT
				
					tl23Sprite = "FOREGROUND_ENEMY_GUNFIRE"
					CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_COMMON_PROPS), tl23Sprite, sCMHDZData.sFXData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_SPRITE_GANG_SHOT_WIDTH, cfCMHDZ_SPRITE_GANG_SHOT_HEIGHT), 0, sCMHDZData.rgbaSprite)
				
				BREAK
	//			CASE CMHDZ_FX_EXPLOSION
	//			
	//				tl23Sprite = "PROP_TNT_CRATE_STATE_0"
	//				tl23Sprite += (sCMHDZData.sFXData[i].iSpriteBlastAnimFrame + 2)
	//				CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_COMMON_PROPS), tl23Sprite, sCMHDZData.sFXData[i].vSpritePos, INIT_VECTOR_2D(cfCMHDZ_SPRITE_EXPLOSION_WIDTH, cfCMHDZ_SPRITE_EXPLOSION_HEIGHT), 0, sCMHDZData.rgbaSprite)
	//			
	//			BREAK
				
				CASE CMHDZ_FX_ENEMY_TNT_EXP
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
					OR sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 1
						tl23Sprite = "THROWING_ENEMY_WEAPON_EXPLOSION_01"
						vSize = INIT_VECTOR_2D(cfCMHDZ_SPRITE_GANG_THROWN_EXPLOSION_SMALL_WIDTH, cfCMHDZ_SPRITE_GANG_THROWN_EXPLOSION_SMALL_HEIGHT)
					ELSE
						tl23Sprite = "THROWING_ENEMY_WEAPON_EXPLOSION_02"
						vSize = INIT_VECTOR_2D(cfCMHDZ_SPRITE_GANG_THROWN_EXPLOSION_LARGE_WIDTH, cfCMHDZ_SPRITE_GANG_THROWN_EXPLOSION_LARGE_HEIGHT)
					ENDIF
				//	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_BULLETS_AND_EFFECTS] {DSW} SPrite: ", tl23Sprite, " Frame: ", sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
					CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_COMMON_PROPS), tl23Sprite, sCMHDZData.sFXData[i].vSpritePos, vSize, 0, sCMHDZData.rgbaSprite)
				BREAK
				
				CASE CMHDZ_FX_LOOT_CRATE
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						tl23Sprite = "CRATE_04"
					ELIF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 1
						tl23Sprite = "CRATE_05"
					ELIF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 2
						tl23Sprite = "CRATE_06"
					ENDIF
					vSize = INIT_VECTOR_2D(cfCMHDZ_SPRITE_LOOT_CRATE_EXPLOSION_WIDTH, cfCMHDZ_SPRITE_LOOT_CRATE_EXPLOSION_HEIGHT)
					//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_BULLETS_AND_EFFECTS] {DSW} SPrite: ", tl23Sprite, " Frame: ", sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
					CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_COMMON_PROPS), tl23Sprite, sCMHDZData.sFXData[i].vSpritePos, vSize, 0, sCMHDZData.rgbaSprite)
				BREAK

				CASE CMHDZ_FX_BEARSY_FOREGROUND_PAIN_BLOOD
					eSprite = INT_TO_ENUM(CMHDZ_SPRITE, ENUM_TO_INT(CMHDZ_SPRITE_BEARSY_FOREGROUND_BLOOD_01) + sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
					CMHDZ_DRAW_GAME_SPRITE(eSprite,sCMHDZData.sFXData[i].vSpritePos, CMHDZ_BOSS_GET_SPRITE_SIZES(eSprite), 0, sCMHDZData.rgbaSprite)
					//CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_FX_BEARSY_FOREGROUND_PAIN_BLOOD - Drawing")
				BREAK
				
				CASE CMHDZ_FX_BEARSY_FOREGROUND_SPARKS
					eSprite = INT_TO_ENUM(CMHDZ_SPRITE, ENUM_TO_INT(CMHDZ_SPRITE_BEARSY_FOREGROUND_ATTACKING_SPARKS_01) + sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
					CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sFXData[i].vSpritePos, CMHDZ_BOSS_GET_SPRITE_SIZES(eSprite), 0, sCMHDZData.rgbaSprite)
					//CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_FX_BEARSY_FOREGROUND_SPARKS - Drawing this frame")
				BREAK
				
				CASE CMHDZ_FX_OUCH
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame <= 2
						eSprite = INT_TO_ENUM(CMHDZ_SPRITE, ENUM_TO_INT(CMHDZ_SPRITE_OUCH1))
						CMHDZ_DRAW_GAME_SPRITE(eSprite, sCMHDZData.sFXData[i].vSpritePos, INIT_VECTOR_2D(116.0,96.0), 0, sCMHDZData.rgbaSprite)
					ENDIF
					//CDEBUG2LN(DEBUG_MINIGAME, "CMHDZ_FX_BEARSY_FOREGROUND_SPARKS - Drawing this frame")
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDFOR
	
	//CMHDZ_FLASH_SCREEN_FOR_ACTIVE_EFFECT(sCMHDZData.rgbaWhite)
	
//	RGBA_COLOUR_STRUCT rgbaDead = sCMHDZData.rgbaRed
//	rgbaDead.iA = 255
//	IF iDistance = ciCMHDZ_LANE_FOREGROUND // only once
//		
//	ENDIF
	
ENDPROC

PROC CMHDZ_DRAW_GORE_EFFECTS()

	//TEXT_LABEL_63 tl23Sprite
	//CMHDZ_SPRITE eSprite
	INT i
	
	// FX
	//VECTOR_2D vSize
	
	FOR i = 0  TO ciCMHDZ_MAX_FX - 1
		// We determine whther draw function gets hit by assessing Alpha levels.
		IF sCMHDZData.sFXData[i].bActive// AND sCMHDZData.sFXData[i].iLane = iDistance
			
			IF sCMHDZData.sFXData[i].bFinished
				sCMHDZData.sFXData[i].fAlpha -= (0.0+@1000)
				sCMHDZData.sFXData[i].vSpritePos.y += (0.0+@1000)/2
				
				// Prevent invalid values
				IF sCMHDZData.sFXData[i].fAlpha < 0
					sCMHDZData.sFXData[i].fAlpha = 0
					sCMHDZData.sFXData[i].bActive = FALSE
					EXIT
				ENDIF
			ELSE // bFinished = FALSE

				IF sCMHDZData.sFXData[i].eFXType = CMHDZ_FX_BEARSY_FOREGROUND_PAIN_BLOOD
				OR sCMHDZData.sFXData[i].eFXType = CMHDZ_FX_SLASH_EFFECT
					sCMHDZData.sFXData[i].iSpriteBlastAnimFrame += sCMHDZData.sFXData[i].iSlowUpdateFrames
				ELSE
					sCMHDZData.sFXData[i].iSpriteBlastAnimFrame += sCMHDZData.sFXData[i].iMedUpdateFrames
				ENDIF
			ENDIF
		//	//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_BULLETS_AND_EFFECTS] {DSW} Frame: ", sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
//			IF IS_MOVING_LEVEL() AND sCMHDZData.eClientState = CMHDZ_ARCADE_CLIENT_STATE_PLAYING
//				sCMHDZData.sFXData[i].vSpritePos.x -= CMHDZ_GET_FX_UPDATE()
//			ENDIF
			
			SWITCH sCMHDZData.sFXData[i].eFXType
				CASE CMHDZ_FX_BLOOD_SPLAT
					//eSprite = INT_TO_ENUM(CMHDZ_SPRITE, ENUM_TO_INT(CMHDZ_SPRITE_SCREEN_DAMAGE_BLOOD_SPLATTER) + sCMHDZData.sFXData[i].iSpriteBlastAnimFrame)
					CMHDZ_DRAW_GAME_SPRITE_ALPHA_CONTROL(CMHDZ_SPRITE_SCREEN_DAMAGE_BLOOD_SPLATTER, sCMHDZData.sFXData[i].vSpritePos, CMHDZ_BOSS_GET_SPRITE_SIZES(CMHDZ_SPRITE_SCREEN_DAMAGE_BLOOD_SPLATTER), sCMHDZData.sFXData[i].fAlpha)
				BREAK
				
				CASE CMHDZ_FX_BEARSY_FOREGROUND_ATTACKING_BLOOD
					//eSprite = INT_TO_ENUM(CMHDZ_SPRITE, (ENUM_TO_INT(CMHDZ_SPRITE_BEARSY_FOREGROUND_ATTACKING_BLOOD_01) + sCMHDZData.sFXData[i].iSpriteBlastAnimFrame))
					//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_DRAW_GORE_EFFECTS() - CMHDZ_FX_BEARSY_FOREGROUND_ATTACKING_BLOOD - fAlpha: ", sCMHDZData.sFXData[i].fAlpha)
					CMHDZ_DRAW_GAME_SPRITE_ALPHA_CONTROL(CMHDZ_GET_SPRITE_FROM_ANIM_FRAME(i, CMHDZ_SPRITE_BEARSY_FOREGROUND_ATTACKING_BLOOD_01), sCMHDZData.sFXData[i].vSpritePos, CMHDZ_BOSS_GET_SPRITE_SIZES(CMHDZ_SPRITE_BEARSY_FOREGROUND_ATTACKING_BLOOD_01), sCMHDZData.sFXData[i].fAlpha)
				BREAK
				
				CASE CMHDZ_FX_SLASH_EFFECT
					//eSprite = INT_TO_ENUM(CMHDZ_SPRITE, ENUM_TO_INT(CMHDZ_SPRITE_SCREEN_DAMAGE_RIP)) // Only one frame	
					CMHDZ_DRAW_GAME_SPRITE_ALPHA_CONTROL(CMHDZ_SPRITE_SCREEN_DAMAGE_RIP, sCMHDZData.sFXData[i].vSpritePos, CMHDZ_BOSS_GET_SPRITE_SIZES(CMHDZ_SPRITE_SCREEN_DAMAGE_RIP), sCMHDZData.sFXData[i].fAlpha)
				BREAK
			ENDSWITCH
			
			// Reset effects on finish
			SWITCH sCMHDZData.sFXData[i].eFXType
				CASE CMHDZ_FX_BLOOD_SPLAT
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= 30
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bFinished = TRUE
					ENDIF 
				BREAK
				
				CASE CMHDZ_FX_BEARSY_FOREGROUND_ATTACKING_BLOOD
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_BEARSY_CHAINSAW_BLOOD_MAX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bFinished = TRUE
					ENDIF
				BREAK

				CASE CMHDZ_FX_SLASH_EFFECT
					IF sCMHDZData.sFXData[i].iSpriteBlastAnimFrame >= ciCMHDZ_SCREEN_SLASH_MAX_ANIM_FRAMES
						sCMHDZData.sFXData[i].iSpriteBlastAnimFrame = 0
						sCMHDZData.sFXData[i].bFinished = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
		ENDIF
	ENDFOR

ENDPROC

PROC CMHDZ_DRAW_RETICLES(BOOL bIgnoreDead = FALSE, BOOL bInMenu = FALSE)
	// What kind of reticle is needed
	TEXT_LABEL_31 tl15Reticle
	tl15Reticle = "RETICLE_REVOLVER_DEFAULT"

	//RGBA_COLOUR_STRUCT col 
	IF (CMHDZ_GET_LOCAL_PLAYER_HEALTH() <= 0.0) AND (bIgnoreDead = FALSE)
		EXIT
	ENDIF
	
	IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_HIDE_RETICULE)
		EXIT
	ENDIF
	
//	IF CMHDZ_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
//		INIT_RGBA_STRUCT(col, 255, 32, 32, 255)
//	ELSE
//		INIT_RGBA_STRUCT(col, 20, 80, 255, 255) 
//	ENDIF

	SWITCH sCMHDZData.sPlayerData[0].activeWeapon
	
		CASE CMHDZ_PLAYER_WEAPON_REVOLVER
		CASE CMHDZ_PLAYER_WEAPON_NONE
			tl15Reticle = "RETICLE_REVOLVER"
		BREAK

		CASE CMHDZ_PLAYER_WEAPON_NAILGUN
			tl15Reticle = "RETICLE_NAIL_GUN"	
		BREAK
	
		CASE CMHDZ_PLAYER_WEAPON_RIFLE
			tl15Reticle = "RETICLE_RPG"
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_SHOTGUN
			tl15Reticle = "RETICLE_SHOTGUN"	
		BREAK
	
	ENDSWITCH
	
	CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
	
//	IF CMHDZ_IS_PLAYER_PLAYING()
//	AND NOT bInMenu
	IF NOT bInMenu
		IF CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() <= 0
			IF CMHDZ_IS_PLAYER_SHOOTING()
		 		tl15Reticle += "_NO_AMMO_FIRING"
			ELSE 
				tl15Reticle += "_NO_AMMO"
			ENDIF
		ELSE 

			IF CMHDZ_IS_PLAYER_SHOOTING()
				tl15Reticle += "_SHOOTING"
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(sCMHDZData.sPlayerData[0].eReticuleBits, CMHDZ_RETICLE_BITS_DESTROYED_TARGET)
				OR IS_BITMASK_ENUM_AS_ENUM_SET(sCMHDZData.sPlayerData[0].eReticuleBits, CMHDZ_RETICLE_BITS_HIT_TARGET)
					tl15Reticle += "_HIT"
				ENDIF
			ELSE
				tl15Reticle += "_DEFAULT"
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF bInMenu
		IF CMHDZ_IS_PLAYER_SHOOTING()
			tl15Reticle += "_SHOOTING"
		ELSE
			tl15Reticle += "_DEFAULT"
		ENDIF
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	//CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_RETICLE_TYPES), tl15Reticle, sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos, INIT_VECTOR_2D(cfCMHDZ_SPRITE_MARKER_WIDTH, cfCMHDZ_SPRITE_MARKER_HEIGHT), 0, sCMHDZData.rgbaSprite)
	ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_RETICLE_TYPES), tl15Reticle, sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos, INIT_VECTOR_2D(cfCMHDZ_SPRITE_MARKER_WIDTH, cfCMHDZ_SPRITE_MARKER_HEIGHT), 0, sCMHDZData.rgbaWhite)
ENDPROC

PROC CMHDZ_DRAW_SHOT_DISTORTION()
	//TEXT_LABEL_23 tl23Sprite

//	INT p
//	FOR p = 0 TO ciCMHDZ_MAX_PLAYERS - 1
//		
//		IF sCMHDZData.sPlayerData[p].bHasJustShot
//			/*
//				ON PC
//				Pistol shot diff ~ 450ms
//				NAILGUN gun ~ 120 ms
//			*/
//			
//			IF sCMHDZData.sPlayerData[p].iLastShotTime = 0
//			OR GET_GAME_TIMER() - sCMHDZData.sPlayerData[p].iLastShotTime > 200
//				ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sCMHDZData.rgbaSprite)
//				sCMHDZData.sPlayerData[p].iLastShotTime = GET_GAME_TIMER()
//			ENDIF
//			
////			IF sCMHDZData.sPlayerData[p].iLastShotTime = 0
////				sCMHDZData.sPlayerData[p].iLastShotTime = GET_GAME_TIMER()
////				//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SHOT_DISTORTION] {DSW} Grabbed last shot time")
////			ELSE
////				//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_SHOT_DISTORTION] {DSW} Time since last shot ",  GET_GAME_TIMER() - sCMHDZData.sPlayerData[p].iLastShotTime)
////				sCMHDZData.sPlayerData[p].iLastShotTime = GET_GAME_TIMER()
////			ENDIF
//		ENDIF
//		
//	ENDFOR
	
	BOOL bShowOffset = TRUE
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_BadlandsNoDeath") OR (CMHDZ_CMDLINE_NO_DEATH = 1)
		//OR bShowOffset
			bShowOffset = FALSE
		ENDIF
	#ENDIF
	
	//FLOAT fScreenJumpPoint = GET_RANDOM_FLOAT_IN_RANGE(90.0, 125.0) // Point in the timer span that the screen starts returning to neutral.
	FLOAT fScreenJumpPoint = 10.0 // Point in the timer span that the screen starts returning to neutral.

	
	IF sCMHDZData.iFade > 0
		bShowOffset = FALSE
	ENDIF
	
	IF bShowOffset
	
		IF sCMHDZData.sPlayerData[0].fHitCountdown > 0.0
		AND sCMHDZData.sPlayerData[0].fHealth > 0 // Only process if you still have health.
			sCMHDZData.sPlayerData[0].fHitCountdown -= (0.0+@1000.0)
		ENDIF
	
	
		IF sCMHDZData.sPlayerData[0].fHitCountdown > fScreenJumpPoint
//			IF sCMHDZData.sPlayerData[0].fHealth > 0 // Only process if you still have health.
//				sCMHDZData.sPlayerData[0].fHitCountdown -= (0.0+@1000.0)
//			ENDIF
			
			IF sCMHDZData.sPlayerData[0].bHeavyDamageState
			OR sCMHDZData.sPlayerData[0].fHealth <= 0.0
				INIT_RGBA_STRUCT(sCMHDZData.rgbaSprite, 230, 220, 230, 255)
				sCMHDZData.sPlayerData[0].bHeavyDamageState = TRUE
				sCMHDZData.fSyncOffsetY += (0.0+@GET_RANDOM_FLOAT_IN_RANGE(300.0,400.0)) // Testing
			
				// Only heavy damage on bool toggle
				IF sCMHDZData.sPlayerData[0].bHeavyDamageState
					CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
				ENDIF
			ELSE // Regular damage
				CMHDZ_DRAW_DAMAGE_NOISE(85)
				// Adjust the offset
				IF (sCMHDZData.fSyncOffsetY > 0.0)
				AND sCMHDZData.sPlayerData[0].fHealth > 0
					sCMHDZData.fSyncOffsetY += (0.0+@GET_RANDOM_FLOAT_IN_RANGE(50.0,100.0))
				ENDIF
			ENDIF
		ELIF sCMHDZData.sPlayerData[0].fHitCountdown > 0.0

			sCMHDZData.fSyncOffsetY -= (0.0+@GET_RANDOM_FLOAT_IN_RANGE(150.0,200.0))//*(1.0+(sCMHDZData.fSyncOffsetY/100))
			
			IF (sCMHDZData.sPlayerData[0].fHealth <= 0.0)
				CMHDZ_SET_PLAYER_BIT(CMHDZ_PLAYER_BIT_DYING)
			ENDIF
			
		ELSE // fHitCountdown <= 0.0
		
			IF sCMHDZData.fSyncOffsetY < -(fScreenJumpPoint*2.0)
				sCMHDZData.fSyncOffsetY = -(fScreenJumpPoint*2.0)
			ENDIF
		
			IF sCMHDZData.fSyncOffsetY < 0.0
				sCMHDZData.fSyncOffsetY += (0.0+@GET_RANDOM_FLOAT_IN_RANGE(200.0,300.0))
			ELSE
				sCMHDZData.fSyncOffsetY = 0.0	
			ENDIF
		
			sCMHDZData.sPlayerData[0].fHitCountdown = 0.0
			INIT_RGBA_STRUCT(sCMHDZData.rgbaSprite, 255, 255, 255, 255)
			sCMHDZData.sPlayerData[0].bHeavyDamageState = FALSE
			
		ENDIF
	ENDIF
	
	IF sCMHDZData.sPlayerData[0].bHeavyDamageState
		sCMHDZData.fSoundGlitchAdditional += 0.25
	ENDIF

	IF  (sCMHDZData.fSyncOffsetY != 0.0)
//	AND (sCMHDZData.fSyncOffsetY < 0.0)
	//AND (sCMHDZData.fSyncOffsetY < (cfBASE_SCREEN_HEIGHT/4.0))
	
		
		VECTOR_2D vu, vy
		vu.x = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
		vu.y = GET_RANDOM_FLOAT_IN_RANGE(0.0, 0.25)
		vy.x = vu.x + 0.75
		vy.y = vu.y + 0.75
		
		ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "damage_fx_overlay_001", 
									  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, sCMHDZData.fSyncOffsetY), 
									  INIT_VECTOR_2D(1500, 200),
									  vu, vy, 0, sCMHDZData.rgbaWhiteSafe)

		ARCADE_DRAW_PIXELSPACE_SPRITE_WITH_UV(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_DAMAGE), "damage_fx_overlay_001", 
									  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, sCMHDZData.fSyncOffsetY + 877.0),
									  INIT_VECTOR_2D(1500, 200), 
									  vu, vy, 0, sCMHDZData.rgbaWhiteSafe)
		
	ENDIF
	
	// Old damage assessment. Effect will fade over the gameplay
	CMHDZ_DRAW_DAMAGE_NOISE(ROUND(90 * (1 - CMHDZ_GET_LOCAL_PLAYER_HEALTH())))

ENDPROC

//-------------------------------------------------
// HUD FUNCTIONS
//-------------------------------------------------
PROC CMHDZ_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(CMHDZ_PLAYER_WEAPON weapon, INT iClipPos, VECTOR_2D &v2Pos, VECTOR_2D &v2Size)

	SWITCH weapon
		CASE CMHDZ_PLAYER_WEAPON_REVOLVER	
			v2Size 	= INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PISTOL_AMMO_SINGLE_WIDTH, cfCMHDZ_SPRITE_HUD_PISTOL_AMMO_SINGLE_HEIGHT)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_0_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_1_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_2_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_3_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_4_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_5_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_RIFLE	
			v2Size 	= INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_RIFLE_AMMO_SINGLE_WIDTH, cfCMHDZ_SPRITE_HUD_RIFLE_AMMO_SINGLE_HEIGHT)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_0_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_1_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_2_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_3_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_4_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_5_X, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_SHOTGUN	
			v2Size 	= INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_SHOTGUN_AMMO_SINGLE_WIDTH, cfCMHDZ_SPRITE_HUD_SHOTGUN_AMMO_SINGLE_HEIGHT)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_0_X+22, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_1_X+16, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_2_X+10, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_3_X+4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_4_X+4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_5_X+4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_NAILGUN
			v2Size 	= INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_SHOTGUN_AMMO_SINGLE_WIDTH, 80)
			
			SWITCH iClipPos
				CASE 0		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_0_X - 4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 1		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_1_X - 4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 2		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_2_X - 4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 3		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_3_X - 4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 4		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_4_X - 4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
				CASE 5		v2Pos 	= INIT_VECTOR_2D(cfCMHDZ_SINGLE_PISTOL_AMMO_5_X - 4, cfCMHDZ_SINGLE_PISTOL_AMMO_0_Y)	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CMHDZ_DRAW_SINGLE_AMMO(STRING texture, VECTOR_2D pos, VECTOR_2D size, INT iClipPos, INT iAmmoInClip, INT iClipSize)
	RGBA_COLOUR_STRUCT rgba = sCMHDZData.rgbaWhite
	INT iBulletsFired = iClipSize - iAmmoInClip

	IF iBulletsFired > iClipPos 

		rgba = sCMHDZData.rgbaSpriteUsedBullet
	ENDIF
	
//	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_DRAW_SINGLE_AMMO] iClipPos: ", iClipPos, " Fired: ", iBulletsFired, " iAmmoInClip: ",iAmmoInClip, " iClipSize: ", iClipSize, " Should fade? ", bShouldFade)
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_HUD), texture, pos, size, 0, rgba)
ENDPROC

PROC CMHDZ_DRAW_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
	TEXT_LABEL_31 tl23Sprite
	
	INT iAmmoInClip
	INT iClipSize
	INT i
	VECTOR_2D v2AmmoPos
	VECTOR_2D v2AmmoSize
	
	INT iMaxAmmoSize = 6
	
	SWITCH CMHDZ_GET_PLAYER_CURRENT_WEAPON()
		CASE CMHDZ_PLAYER_WEAPON_REVOLVER
			tl23Sprite = "ammo_pistol1"
			iAmmoInClip = CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = CMHDZ_GET_PLAYER_CURRENT_WEAPON_CLIP_SIZE()
			
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				CMHDZ_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(CMHDZ_PLAYER_WEAPON_REVOLVER, i, v2AmmoPos, v2AmmoSize)				
				CMHDZ_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_SHOTGUN
			tl23Sprite = "INDIVIDUAL_AMMO_ICON_SHOTGUN"
			iAmmoInClip = CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = CMHDZ_GET_PLAYER_CURRENT_WEAPON_CLIP_SIZE()
			
			
			// Fixing position from right of screen.
			// This system doesn't draw objects in the correct place unless manually corrected.
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				CMHDZ_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(CMHDZ_PLAYER_WEAPON_SHOTGUN, i, v2AmmoPos, v2AmmoSize)
				v2AmmoPos.x -= 28.0 * (iMaxAmmoSize - iClipSize)
				CMHDZ_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_RIFLE
			tl23Sprite = "INDIVIDUAL_AMMO_ICON_RIFLE"
			iAmmoInClip = CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = CMHDZ_GET_PLAYER_CURRENT_WEAPON_CLIP_SIZE()
			
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				CMHDZ_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(CMHDZ_PLAYER_WEAPON_RIFLE, i, v2AmmoPos, v2AmmoSize)
				v2AmmoPos.x -= 24.0 * (iMaxAmmoSize - iClipSize)
				CMHDZ_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK
		
		CASE CMHDZ_PLAYER_WEAPON_NAILGUN
			tl23Sprite = "INDIVIDUAL_AMMO_NAIL"
			iAmmoInClip = CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
			iClipSize = 5 // Hard lock as ammo doesn't reload.
			
			//-- Right to left
			FOR i = 0 TO iClipSize - 1
				CMHDZ_GET_SINGLE_AMMO_HUD_POSITION_DETAILS(CMHDZ_PLAYER_WEAPON_NAILGUN, i, v2AmmoPos, v2AmmoSize)
				v2AmmoPos.x -= 24.0 * (iMaxAmmoSize - iClipSize)
				CMHDZ_DRAW_SINGLE_AMMO(tl23Sprite, v2AmmoPos, v2AmmoSize, i, iAmmoInClip, iClipSize)
			ENDFOR
		BREAK

	ENDSWITCH
ENDPROC

PROC CMHDZ_DRAW_AMMO_QUANTITY(VECTOR_2D vPos, INT iTotalAmmo, RGBA_COLOUR_STRUCT col)
	TEXT_LABEL_23 n1, n2
	STRING txtStr // = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
	txtStr = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
	
	IF iTotalAmmo > 99
		n1 = "FONT_INF"
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(txtStr, n1, INIT_VECTOR_2D(vPos.x + 50, vPos.y + 6), INIT_VECTOR_2D(72, 60), 0, col)

		EXIT
		//n2 = "FONT_9"
	ELSE
		n1 = "FONT_"
		n1 += iTotalAmmo / 10
		n2 = "FONT_"
		n2 += iTotalAmmo % 10
	ENDIF

	txtStr = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)

	CMHDZ_DRAW_GAME_SPRITE_DETAILS(txtStr, n1, INIT_VECTOR_2D(vPos.x, vPos.y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_AMMO_QTY_WIDTH, cfCMHDZ_SPRITE_HUD_AMMO_QTY_HEIGHT), 0, col)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(n2)
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(txtStr, n2, INIT_VECTOR_2D(vPos.x + cfCMHDZ_SPRITE_HUD_AMMO_QTY_WIDTH, vPos.y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_AMMO_QTY_WIDTH, cfCMHDZ_SPRITE_HUD_AMMO_QTY_HEIGHT), 0, col)
	ENDIF
ENDPROC 


PROC CMHDZ_DRAW_THROWN_PROJECTILE_RELOAD_METER()
	FLOAT fReloadRatio = 1.0
	FLOAT t1, t2 
	TEXT_LABEL_31 tl31Sprite
	CMHDZ_PROJECTILE_TYPES eType = sCMHDZData.sPlayerData[0].projectileType
	FLOAT fXpos = cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_X // Some sprites don't line up correctly without case correction.
	
	IF (sCMHDZData.sPlayerData[0].iLastThrownProjectileTime != 0) AND (sCMHDZData.sProjectileInfo[eType].iReloadTimeMS > 0)
		t1 = TO_FLOAT(GET_GAME_TIMER())
		t2 = TO_FLOAT(sCMHDZData.sPlayerData[0].iLastThrownProjectileTime + sCMHDZData.sProjectileInfo[eType].iReloadTimeMS)
		fReloadRatio = (t2 - t1) / TO_FLOAT(sCMHDZData.sProjectileInfo[eType].iReloadTimeMS)
	ENDIF

	fReloadRatio = CLAMP(fReloadRatio, 0.0, 1.0)
	
	SWITCH eType
		CASE CMHDZ_PROJECTILE_DYNAMITE
			tl31Sprite = "AMMO_BOMB"
		BREAK 
		
		CASE CMHDZ_PROJECTILE_MOLOTOV
			tl31Sprite = "AMMO_MOLOTOV"
			fXpos += 4.0
		BREAK 
		
		CASE CMHDZ_PROJECTILE_NAILBOMB
			tl31Sprite = "AMMO_NAILBOMB"
			fXpos += 4.0
		BREAK

		CASE CMHDZ_PROJECTILE_BRICK
			tl31Sprite = "AMMO_BRICK"
			fXpos -= 4.0 // Correct for smaller sprite.
		BREAK 
	ENDSWITCH 
	
	STRING sTextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_HUD)
		
	
	CMHDZ_DRAW_HORIZONTAL_MASK_SPRITE(sTextDict, tl31Sprite, INIT_VECTOR_2D(fXpos, cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_Y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_WIDTH, cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_HEIGHT), 1.0, 0, sCMHDZData.rgbaSpriteUsedBullet)
	CMHDZ_DRAW_HORIZONTAL_MASK_SPRITE(sTextDict, tl31Sprite, INIT_VECTOR_2D(fXpos, cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_Y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_WIDTH, cfCMHDZ_SPRITE_HUD_PROJECTILE_AMMO_HEIGHT), 1.0 - fReloadRatio, 0, sCMHDZData.rgbaSprite)	
	
	
	IF (eType = CMHDZ_PROJECTILE_BRICK)
		EXIT 
	ENDIF
	
	CMHDZ_DRAW_AMMO_QUANTITY(INIT_VECTOR_2D(cfCMHDZ_PROJECTILE_AMMO_QTY_1_X_POS, cfCMHDZ_PROJECTILE_AMMO_QTY_1_Y_POS), sCMHDZData.sPlayerData[0].iThrownProjectilesAmmo, sCMHDZData.rgbaSprite)
ENDPROC 
	
PROC CMHDZ_DRAW_LEVEL_PROGRESS_MARKER()

	FLOAT fMarkerY = cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_Y + (cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_HEIGHT/2.0)
	fMarkerY -= 84.0
	fMarkerY -= (sCMHDZData.iCurrentScreen - 1) * (cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_HEIGHT / CMHDZ_GET_FINAL_SCREEN_IN_CURRENT_LEVEL())
//	fMarkerY = (1.0 - fMarkerY) + cfCMHDZ_SPRITE_HUD_PROGRESS_MARKER_Y  // because it needs to move from bottom to top

	// Apply the sync offset.
	FLOAT fYpos = (cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_Y + sCMHDZData.fSyncOffsetY)
	fMarkerY += sCMHDZData.fSyncOffsetY
	
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_X, fYpos), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_WIDTH, cfCMHDZ_SPRITE_HUD_PROGRESS_BAR_HEIGHT), sCMHDZData.rgbaWhite)
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
		"HEXAGON",
		INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PROGRESS_MARKER_X, fMarkerY), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PROGRESS_MARKER_WIDTH, cfCMHDZ_SPRITE_HUD_PROGRESS_MARKER_HEIGHT), 0, sCMHDZData.rgbaWhite)
ENDPROC

PROC CMHDZ_DRAW_PLAYER_HEALTH_BAR()
	INT icon = 4
	FLOAT hp = CMHDZ_GET_LOCAL_PLAYER_HEALTH()
	RGBA_COLOUR_STRUCT color = sCMHDZData.rgbaWhite
	STRING textDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT)
	
	IF (sCMHDZData.iFade = 0.0)
	AND (hp != 0.0)
		CMHDZ_DRAW_REC_TEXT(INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X, cfCMHDZ_SPRITE_HUD_TIME_Y), sCMHDZData.rgbaWhite)
		//CMHDZ_DRAW_STOP_SYMBOL()
		//CMHDZ_DRAW_PLAY_SYMBOL()
	ENDIF
	
	IF (hp = 0.0)
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(textDict, "BATTERY_DEAD", 
		INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 244, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28), 
		INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_W, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_H), 0, color)	

		CMHDZ_DRAW_STOP_SYMBOL()

//		IF (sCMHDZData.iFade = 0.0)
//			CMHDZ_DRAW_GAME_SPRITE_DETAILS(textDict, "REDCIRCLE", 
//				INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 135, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 24), 
//				INIT_VECTOR_2D(48, 48), 0, color)
//		ENDIF
		
		EXIT
	ENDIF 
	
	
	IF ((GET_GAME_TIMER() % 1000) < 500)
	AND (sCMHDZData.iFade = 0.0)
	AND (hp != 0.0)
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(textDict, "REDCIRCLE", 
			INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 162, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28), 
			INIT_VECTOR_2D(48, 48), 0, color)	
	ENDIF
	
	IF (hp < 0.2) AND ((GET_GAME_TIMER() % 1000) > 500)
		EXIT 
	ENDIF 
	
	IF (hp < 0.2)
		icon = 0
	ELIF (hp < 0.4)
		icon = 1
	ELIF (hp < 0.6)
		icon = 2
	ELIF (hp < 0.8)
		icon = 3
	ENDIF
	
	TEXT_LABEL_63 txtName = "BATTERY_"	
	txtName += icon 
	
	IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_INVULNERABILE)
		color = sCMHDZData.rgbaYellow
	ENDIF
	
	IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_QUAD_DAMAGE)
		color = sCMHDZData.rgbaBlue
	ENDIF
		
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(textDict, txtName, 
		INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_X + 244, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_Y + 28), 
		INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_HP_INDICATOR_W, cfCMHDZ_SPRITE_HUD_HP_INDICATOR_H), 0, color)
	
	
ENDPROC

PROC CMHDZ_DRAW_HUD()

	// Fade
	IF NOT sCMHDZData.bFadingOut AND sCMHDZData.iFade > 0
		
//		RGBA_COLOUR_STRUCT rgbaFade
//		INIT_RGBA_STRUCT(rgbaFade, 255, 255, 255, sCMHDZData.iFade)
		//CMHDZ_DRAW_DAMAGE_NOISE(sCMHDZData.iFade)	
				
		sCMHDZData.iFade -= ROUND(GET_FRAME_TIME() * 100)
		//CDEBUG2LN(DEBUG_MINIGAME, "[BAZ][CMHDZ] CMHDZ_DRAW_HUD - bFadingIn - iFade time: ", (GET_FRAME_TIME() * 150))
		
		SET_BITMASK_AS_ENUM(sCMHDZData.iDirectorAudioBits, CMHDZ_DIRECTOR_BITS_Level_Start_TRIGGER)
		
		IF (sCMHDZData.iFade > 130)
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sCMHDZData.rgbaBlack)			
			sCMHDZData.fSoundGlitchLevel = 1.0
			
			IF ((sCMHDZData.iFade)%50) <= 25
				CMHDZ_DRAW_ROUND_START_TEXT(sCMHDZData.rgbaWhite)
			ENDIF
			
			//sCMHDZData.fSyncOffsetY = GET_RANDOM_FLOAT_IN_RANGE(-10.0,10.0)
			INT i = (255 - (sCMHDZData.iFade+30))
			IF (i < 0)
				i = 0
			ENDIF
			CMHDZ_DRAW_DAMAGE_NOISE(i)
			//sCMHDZData.fSyncOffsetY += (0.0+@(80*(sCMHDZData.iFade)))
		ELIF(sCMHDZData.iFade > 75)
			CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
			sCMHDZData.fSoundGlitchLevel = 0.5
			//sCMHDZData.fSyncOffsetY += (0.0+@(80*(sCMHDZData.iFade)))
//			sCMHDZData.fSyncOffsetY = GET_RANDOM_FLOAT_IN_RANGE(-sCMHDZData.iFade/3.0,sCMHDZData.iFade/3.0)
		ELIF (sCMHDZData.fSyncOffsetY > 0)
			//sCMHDZData.fSyncOffsetY -= (0.0+@GET_RANDOM_FLOAT_IN_RANGE(2500.0,4000.0))
			sCMHDZData.fSyncOffsetY = 0.0
		ELIF (sCMHDZData.fSyncOffsetY < 0)
			sCMHDZData.fSyncOffsetY = 0.0
		ENDIF
		
		IF (sCMHDZData.iFade < 55)
			CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_MUSIC_TRANSITION)
			sCMHDZData.fSoundGlitchLevel = 0.0
			CMHDZ_TRIGGER_MUSIC_EVENT(CMHDZ_GET_MUSIC_EVENT_FOR_LEVEL(CMHDZ_GET_CURRENT_LEVEL()))
			sCMHDZData.iFade = 0
		ENDIF
		
		// Draw the play symbol
		IF (sCMHDZData.iFade > 75)
		
			IF (sCMHDZData.iFade < 130)
				CMHDZ_DRAW_PLAY_SYMBOL()
			ENDIF
			
			EXIT
		ELSE
			CMHDZ_HANDLE_DIRECTOR_ONE_OFF_DIALOGUE(CMHDZ_AUDIO_EFFECT_DIRECTOR_Level_Start)
			CMHDZ_DRAW_PLAY_SYMBOL()
		ENDIF
		
		
	ELIF sCMHDZData.bFadingOut //AND sCMHDZData.iFade < 255
		
//		RGBA_COLOUR_STRUCT rgbaFade
//		INIT_RGBA_STRUCT(rgbaFade, 255, 255, 255, sCMHDZData.iFade)
		//CMHDZ_DRAW_DAMAGE_NOISE(sCMHDZData.iFade)	
		//sCMHDZData.iFade += 4
		sCMHDZData.iFade += ROUND(GET_FRAME_TIME() * 100)
		CDEBUG2LN(DEBUG_MINIGAME, "[BAZ][CMHDZ] CMHDZ_DRAW_HUD - bFadingOut - iFade time: ", (GET_FRAME_TIME() * 150))
		//SET_VARIABLE_ON_SOUND(sCMHDZData.iSoundId[CMHDZ_AUDIO_EFFECT_LEVEL_BACKGROUND_LOOP],"GlitchLevel",(sCMHDZData.iFade/255.0))

//		IF (sCMHDZData.iFade > 150)
//			CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
//			sCMHDZData.fSyncOffsetY += (0.0+@(80*(sCMHDZData.iFade)))
//		ELIF (sCMHDZData.fSyncOffsetY > 0)
//			sCMHDZData.fSyncOffsetY -= (0.0+@GET_RANDOM_FLOAT_IN_RANGE(2500.0,4000.0))
//		ELIF (sCMHDZData.fSyncOffsetY < 0)
//			sCMHDZData.fSyncOffsetY = 0.0
//		ENDIF

		IF (sCMHDZData.iFade > 150)
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sCMHDZData.rgbaBlack)			
			
			IF CMHDZ_GET_LOCAL_PLAYER_HEALTH() > 0.0	
				IF ((sCMHDZData.iFade)%50) <= 25
					CMHDZ_DRAW_ROUND_CLEAR_TEXT(sCMHDZData.rgbaWhite)
				ENDIF
			ENDIF	
			
			//sCMHDZData.fSyncOffsetY = GET_RANDOM_FLOAT_IN_RANGE(-10,10)
			CMHDZ_TRIGGER_MUSIC_EVENT(CMHDZ_GET_MUSIC_EVENT_FOR_LEVEL(CMHDZ_GET_CURRENT_LEVEL()))
			CMHDZ_DRAW_DAMAGE_NOISE(FLOOR(sCMHDZData.iFade/1.5))
			sCMHDZData.fSoundGlitchLevel = 1.0
			//sCMHDZData.fSyncOffsetY += (0.0+@(80*(sCMHDZData.iFade)))
		ELIF(sCMHDZData.iFade > 75)
			CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
			sCMHDZData.fSoundGlitchLevel = 0.5
			//sCMHDZData.fSyncOffsetY += (0.0+@(80*(sCMHDZData.iFade)))
//			sCMHDZData.fSyncOffsetY = (GET_RANDOM_FLOAT_IN_RANGE(-sCMHDZData.iFade/3.0,sCMHDZData.iFade/3.0))
		ELIF (sCMHDZData.fSyncOffsetY > 0)
			//sCMHDZData.fSyncOffsetY -= (0.0+@GET_RANDOM_FLOAT_IN_RANGE(2500.0,4000.0))
			sCMHDZData.fSyncOffsetY = 0.0
		ELIF (sCMHDZData.fSyncOffsetY < 0)
			sCMHDZData.fSyncOffsetY = 0.0
		ENDIF
		
		// Draw the play symbol
		IF (sCMHDZData.iFade > 75)
			CMHDZ_TRIGGER_MUSIC_EVENT(CMHDZ_MUSIC_EVENT_STOP)
			EXIT
		ELSE
			CMHDZ_DRAW_STOP_SYMBOL()
		ENDIF
		
//		IF (sCMHDZData.iFade < 5)
//			sCMHDZData.iFade = 0
//		ENDIF
		
		//ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaFade)
		//sCMHDZData.fSyncOffsetY += (0.0+@(2000*sCMHDZData.iFade))
		
	ENDIF
	
	// No hud if we're out of health.
	IF sCMHDZData.sPlayerData[0].fHealth <= 0.0
		EXIT
	ENDIF
	
	CMHDZ_DRAW_PLAYER_HEALTH_BAR()

	// Regular score display
	IF CMHDZ_GET_NUMBER_OF_PLAYERS() <= 1
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("SCR:", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_TIME_X - cfCMHDZ_SPRITE_HUD_TIME_TEXT_X, cfCMHDZ_SPRITE_HUD_TIME_Y + cfCMHDZ_SPRITE_HUD_TIME_TEXT_Y), 1.5)
		CMHDZ_DRAW_PLAYER_SCORE_TEXT(GET_LOCAL_PLAYER_SCORE(), sCMHDZData.rgbaWhite, cfCMHDZ_SPRITE_HUD_TIME_X, cfCMHDZ_SPRITE_HUD_TIME_Y, CMHDZ_HUD_TEXTPIVOT_RIGHT)
	ENDIF

	
	IF CMHDZ_GET_NUMBER_OF_PLAYERS() > 1
	 	// 'Red' (1P) Score
		//CMHDZ_DRAW_PLAYER_SCORE_TEXT(CMHDZ_GET_RED_PLAYER_SCORE_FOR_LEVEL(CMHDZ_GET_CURRENT_LEVEL_AS_INT()),sCMHDZData.rgbaRed, cfCMHDZ_SPRITE_HUD_TIME_X, cfCMHDZ_SPRITE_HUD_TIME_Y + 100, CMHDZ_HUD_TEXTPIVOT_RIGHT)
		
		RGBA_COLOUR_STRUCT rgbaPlayerColor = sCMHDZData.rgbaWhite
		
		// Is rival Blue?
		IF CMHDZ_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
			rgbaPlayerColor = sCMHDZData.rgbaRedText
		ELSE // Rival is red
			rgbaPlayerColor = sCMHDZData.rgbaBlue
		ENDIF
		
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("SCR:", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_TIME_X - cfCMHDZ_SPRITE_HUD_TIME_TEXT_X, cfCMHDZ_SPRITE_HUD_TIME_Y + cfCMHDZ_SPRITE_HUD_TIME_TEXT_Y), 1.5, DEFAULT, DEFAULT, rgbaPlayerColor.iR, rgbaPlayerColor.iG, rgbaPlayerColor.iB)
		CMHDZ_DRAW_PLAYER_SCORE_TEXT(GET_LOCAL_PLAYER_SCORE(), rgbaPlayerColor, cfCMHDZ_SPRITE_HUD_TIME_X, cfCMHDZ_SPRITE_HUD_TIME_Y, CMHDZ_HUD_TEXTPIVOT_RIGHT)

		
		
		INT iPart = CMHDZ_GET_PARTICIPANT_WITH_HIGHEST_HI_SCORE_FOR_CURRENT_LEVEL()
		INT iCurrentLevel = CMHDZ_GET_CURRENT_LEVEL_AS_INT()
		
		INT iHighestScore = CMHDZ_GET_PARTICIPANT_HI_SCORE_FOR_LEVEL_FROM_HOST(iPart,iCurrentLevel )
		CMHDZ_SCORE_INTEGER_TYPE eHiScoreType
		
		IF iHighestScore > 0
			IF iPart = PARTICIPANT_ID_TO_INT()
				//-- I've got the highest score
				IF CMHDZ_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
					//-- I'm in the left hand cabinet - Hi score should be red
					eHiScoreType = CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE
				ELSE
					//-- I'm in the right hand cabinbet - Hi score should be blue
					eHiScoreType = CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE
				ENDIF
			ELSE
				//--Rival has highest score
				IF CMHDZ_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
					//-- Rival in right hand cabinet - score should be blue
					eHiScoreType = CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE
				ELSE
					//-- Rival in left hand cabinet - score should be red
					eHiScoreType = CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE
				ENDIF
			ENDIF
		ELSE
			//-- Just make the hi score white if nobody has a score yet
			eHiScoreType = CMHDZ_SCORE_INTEGER_TYPE_HI_SCORE
		ENDIF
		UNUSED_PARAMETER(eHiScoreType)
		//CMHDZ_DRAW_SCORE_INTEGER(iHighestScore, ciCMHDZ_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_X_POS, cfCMHDZ_MY_HISCORE_Y_POS), sCMHDZData.rgbaSprite, eHiScoreType) //
		
		RGBA_COLOUR_STRUCT rgbaRivalColor = sCMHDZData.rgbaWhite
		
		// Is rival Blue?
		IF CMHDZ_IS_LOCAL_PLAYER_IN_LEFT_HAND_CABINET()
			rgbaRivalColor = sCMHDZData.rgbaBlue
		ELSE // Rival is red
			rgbaRivalColor = sCMHDZData.rgbaRedText
		ENDIF
		// (2P) Score
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("RVL:", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_TIME_X - cfCMHDZ_SPRITE_HUD_TIME_TEXT_X, cfCMHDZ_SPRITE_HUD_TIME_Y + (cfCMHDZ_SPRITE_HUD_TIME_TEXT_Y + 60)), 1.5, DEFAULT, DEFAULT, rgbaRivalColor.iR, rgbaRivalColor.iG, rgbaRivalColor.iB)
		CMHDZ_DRAW_PLAYER_SCORE_TEXT(CMHDZ_GET_RIVAL_SCORE_FOR_LEVEL(iCurrentLevel), rgbaRivalColor, cfCMHDZ_SPRITE_HUD_TIME_X, cfCMHDZ_SPRITE_HUD_TIME_Y + 60, CMHDZ_HUD_TEXTPIVOT_RIGHT)

		//CMHDZ_DRAW_SCORE_INTEGER(CMHDZ_GET_RIVAL_SCORE_FOR_LEVEL(iCurrentLevel), ciCMHDZ_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfCMHDZ_BLUE_SCORE_X_POS, cfCMHDZ_BLUE_SCORE_Y_POS), sCMHDZData.rgbaSprite, CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE)
		
		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel) > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel)
			//-- Red 'Win'
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPCamhedzVHSFont", "SCORE_TEXT_WIN_GRAPHIC_RED", INIT_VECTOR_2D(960.0, cfCMHDZ_SCORE_WIN_RED_Y_POS ), INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE, cfCMHDZ_SCORE_WIN_Y_SIZE), 0, sCMHDZData.rgbaSprite)
		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel) < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iCurrentLevel)	
			//-- Blue 'Win'
			ARCADE_DRAW_PIXELSPACE_SPRITE("MPCamhedzVHSFont", "SCORE_TEXT_WIN_GRAPHIC_BLUE", INIT_VECTOR_2D(960.0, cfCMHDZ_SCORE_WIN_BLUE_Y_POS), INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE, cfCMHDZ_SCORE_WIN_Y_SIZE), 0, sCMHDZData.rgbaSprite)
		ENDIF
	
	ENDIF
	
	
	CMHDZ_DRAW_THROWN_PROJECTILE_RELOAD_METER()
	
	INT iTotalAmmo = CMHDZ_GET_PLAYER_TOTAL_AMMO_IN_CURRENT_WEAPON(FALSE) 
	CMHDZ_DRAW_AMMO_IN_CLIP_FOR_CURRENT_WEAPON()
	CMHDZ_DRAW_AMMO_QUANTITY(INIT_VECTOR_2D(cfCMHDZ_AMMO_QTY_1_X_POS, cfCMHDZ_AMMO_QTY_1_Y_POS), iTotalAmmo, sCMHDZData.rgbaWhite)

//	IF CMHDZ_SHOULD_SHOW_RELOAD_PROMPT() AND NOT (sCMHDZData.bFadingOut)
//		IF CMHDZ_GET_PLAYER_TOTAL_AMMO_IN_CURRENT_WEAPON() = 0
//		AND sCMHDZData.sPlayerData[0].weaponType = CMHDZ_PLAYER_WEAPON_TYPE_DEFAULT
//			CMHDZ_DRAW_NO_AMMO_TEXT(sCMHDZData.rgbaWhite)
//			//CMHDZ_DRAW_GAME_SPRITE("TYPE_PROMPT_NO_AMMO", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_X, cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_Y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_WIDTH, cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_HEIGHT), 0, sCMHDZData.rgbaSprite)
//		ELSE
//			CMHDZ_DRAW_RELOAD_TEXT(sCMHDZData.rgbaWhite)
//			//CMHDZ_DRAW_GAME_SPRITE("TYPE_PROMPT_RELOAD", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_RELOAD_PROMPT_X, cfCMHDZ_SPRITE_HUD_RELOAD_PROMPT_Y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_RELOAD_PROMPT_WIDTH, cfCMHDZ_SPRITE_HUD_RELOAD_PROMPT_HEIGHT), 0, sCMHDZData.rgbaSprite)
//		ENDIF		
//	ENDIF
	
	IF CMHDZ_SHOULD_SHOW_RELOAD_PROMPT() 
	AND NOT (sCMHDZData.bFadingOut)
		IF CMHDZ_GET_PLAYER_AMMO_IN_CLIP_FOR_CURRENT_WEAPON() = 0
//			IF sCMHDZData.sPlayerData[0].weaponType != CMHDZ_PLAYER_WEAPON_TYPE_DEFAULT
//				//CMHDZ_DRAW_GAME_SPRITE("TYPE_PROMPT_NO_AMMO", INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_X, cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_Y), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_WIDTH, cfCMHDZ_SPRITE_HUD_NO_AMMO_PROMPT_HEIGHT), 0, sCMHDZData.rgbaSprite)
//				// Revert to original weapon
//				CMHDZ_INIT_PLAYER_WEAPON()
//			ELSE
				CMHDZ_DRAW_RELOAD_TEXT(sCMHDZData.rgbaWhite)
//			ENDIF		
		ENDIF
	ENDIF
	
	CMHDZ_DRAW_LEVEL_PROGRESS_MARKER()
	
	IF CMHDZ_IS_PLAYER_PLAYING()
		IF NOT CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_DYING)
		
			RGBA_COLOUR_STRUCT rgbaColour
			FLOAT fTimerDiv
							
			// Asign the correct colour
			IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_INVULNERABILE)
				rgbaColour = sCMHDZData.rgbaYellow
				fTimerDiv = 12
				//fTimerPercent = 100.0 - (sCMHDZData.fPowerRunTimer / ciCMHDZ_ITEM_SHIELD_TIME)
			ELIF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_HIDE_RETICULE)
				rgbaColour = sCMHDZData.rgbaGreen
			ELIF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_QUAD_DAMAGE)
				//fTimerPercent = 100.0 - (sCMHDZData.fQuadDamageRunTimer / ciCMHDZ_ITEM_BUFF_TIME)
				fTimerDiv = 10
				rgbaColour = sCMHDZData.rgbaBlue	
			ELSE
				IF sCMHDZData.fEffectFlashAlpha != 0
					sCMHDZData.fEffectFlashAlpha = 0
				ENDIF
			ENDIF
			
			IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_INVULNERABILE)
			OR CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_QUAD_DAMAGE)
				CMHDZ_FLASH_SCREEN_FOR_ACTIVE_EFFECT(rgbaColour)
						
				//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_IS_PLAYER_PLAYING powerup active - fTimerPercent: ", fTimerPercent)
						
				CMHDZ_SPRITE csSprite = CMHDZ_SPRITE_GET_NEXT_ENUM(CMHDZ_SPRITE_POWER_UP_LINE_01, ROUND((sCMHDZData.fPowerRunTimer % 200.0)/50.0))
				//csSprite = CMHDZ_SPRITE_POWER_UP_LINE_01
						
				// Power up tracking line
				CMHDZ_DRAW_GAME_SPRITE(csSprite, 
					INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0), (1000.0 - (sCMHDZData.fPowerRunTimer/fTimerDiv))),
					INIT_VECTOR_2D(1440, 80),
					0.0,
					rgbaColour)
			ENDIF
		ELSE
			
			//ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaDead)
			//IF sCMHDZData.fSyncOffsetY >= 125.0
			//CMHDZ_DRAW_GAME_OVER_EFFECT()
			//ENDIF
			
		ENDIF	
	ELSE
	
		IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_INVULNERABILE)
			CMHDZ_CLEAR_PLAYER_BIT(CMHDZ_PLAYER_BIT_INVULNERABILE)
			CMHDZ_STOP_AUDIO_SCENE(CMHDZ_AUDIO_SCENE_POWERUP_ACTIVE)
			CMHDZ_STOP_SOUND(CMHDZ_AUDIO_EFFECT_PICKUP_SHIELD)
		ENDIF
	
		IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_QUAD_DAMAGE)
			CMHDZ_CLEAR_PLAYER_BIT(CMHDZ_PLAYER_BIT_QUAD_DAMAGE)
			CMHDZ_STOP_AUDIO_SCENE(CMHDZ_AUDIO_SCENE_POWERUP_ACTIVE)
			CMHDZ_STOP_SOUND(CMHDZ_AUDIO_EFFECT_PICKUP_BUFF)
		ENDIF
		
		IF CMHDZ_IS_PLAYER_BIT_SET(CMHDZ_PLAYER_BIT_HIDE_RETICULE)
			CMHDZ_CLEAR_PLAYER_BIT(CMHDZ_PLAYER_BIT_HIDE_RETICULE)
			CMHDZ_STOP_AUDIO_SCENE(CMHDZ_AUDIO_SCENE_POWERUP_ACTIVE)
		ENDIF
		
		// Toggle special effects off.
		IF sAGPostFXData.sFXLayers[ARCADE_GAMES_POSTFX_LAYER_FLICKERING].bShow
			//sAGPostFXData.sFXLayers[ARCADE_GAMES_POSTFX_LAYER_SCAN_LINE].bShow = FALSE
			sAGPostFXData.sFXLayers[ARCADE_GAMES_POSTFX_LAYER_FLICKERING].bShow = FALSE
			INIT_RGBA_STRUCT(sAGPostFXData.sFXLayers[ARCADE_GAMES_POSTFX_LAYER_SCAN_LINE].rgbaColor,255,255,255,3)
		ENDIF
	ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
//-------------------------------------------------
// DEBUG FUNCTIONS
//-------------------------------------------------
PROC DEBUG_TEXT_FLOAT_IN_SCREEN(STRING text, FLOAT fValue,FLOAT x, FLOAT y, INT r = 255, INT g = 255, INT b = 255)
	
	TEXT_PLACEMENT textPlacement
	textPlacement.x = x
	textPlacement.y = y
	TEXT_STYLE textStyle
	SET_TITLE_SMALL_TEXT(textStyle)
	textStyle.XScale = 0.2
	textStyle.YScale = 0.2
	
	TEXT_LABEL_63 output
	output = text
	output += " = "
	output += FLOAT_TO_STRING(fValue)
	textStyle.r = r
	textStyle.g = g
	textStyle.b = b

	DRAW_TEXT_WITH_STRING(textPlacement, textStyle, "DBG_STRING ",TEXT_LABEL_TO_STRING(output))
	
ENDPROC 
PROC DEBUG_TEXT_INT_IN_SCREEN(STRING text, INT iValue,FLOAT x, FLOAT y, INT r = 255, INT g = 255, INT b = 255)
	TEXT_PLACEMENT textPlacement
	textPlacement.x = x
	textPlacement.y = y
	TEXT_STYLE textStyle
	SET_TITLE_SMALL_TEXT(textStyle)
	textStyle.XScale = 0.2
	textStyle.YScale = 0.2
	
	TEXT_LABEL_63 output
	output = text
	output += " = "
	output += iValue
	textStyle.r = r
	textStyle.g = g
	textStyle.b = b

	DRAW_TEXT_WITH_STRING(textPlacement, textStyle, "DBG_STRING",TEXT_LABEL_TO_STRING(output)) 
ENDPROC

PROC CMHDZ_DO_ON_SCREEN_DEBUG()
	//	DEBUG_TEXT_FLOAT_IN_SCREEN("fTravelRate ",sDGMonkeyData.sLevelData.fTravelRate,0.16,0.05+0.01*iLine)
	FLOAT fYpos =  0.2
	DEBUG_TEXT_INT_IN_SCREEN("Stage: ", CMHDZ_GET_CURRENT_LEVEL_SCREEN(), 0.16, fYpos)
	fYpos+=0.025
	DEBUG_TEXT_INT_IN_SCREEN("Enemies: ", CMHDZ_GET_TOTAL_NUMBER_OF_ENEMIES_LEFT_ALIVE_FOR_CURRENT_SCREEN(), 0.16, fYpos)
	fYpos+=0.025
	DEBUG_TEXT_INT_IN_SCREEN("Screen: ", sCMHDZData.iCurrentScreen, 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_INT_IN_SCREEN("Stop screen: ", sCMHDZData.iScreenToStopOn, 0.16, fYpos)
//	DEBUG_TEXT_FLOAT_IN_SCREEN("RetX", sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos.x, 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_FLOAT_IN_SCREEN("RetY", sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos.y, 0.16, fYpos)
//	DEBUG_TEXT_INT_IN_SCREEN("Lane 0: ", sCMHDZData.iCurrentActiveEnemies[0], 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_INT_IN_SCREEN("Lane 1: ", sCMHDZData.iCurrentActiveEnemies[1], 0.16, fYpos)
//	fYpos+=0.025
//	DEBUG_TEXT_INT_IN_SCREEN("Lane 2: ", sCMHDZData.iCurrentActiveEnemies[2], 0.16, fYpos)
ENDPROC
#ENDIF


//-------------------------------------------------
// STATE FUNCTIONS
//-------------------------------------------------

PROC CMHDZ_DRAW_CLIENT_STATE_PLAYING()
	// Draw black and grey background
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sCMHDZData.rgbaBlack)
	//ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), sCMHDZData.rgbaRed)

	sCMHDZData.fBackgroundSpeedMult = 1.0
	
	CMHDZ_DRAW_BACKGROUND()
	
	CMHDZ_DRAW_COVER(ciCMHDZ_LANE_BACKGROUND, DEFAULT, TRUE)
	
	//CMHDZ_DRAW_HOSTAGES(ciCMHDZ_LANE_BACKGROUND)
	
	CMHDZ_DRAW_ENEMIES(ciCMHDZ_LANE_BACKGROUND)
	
	CMHDZ_DRAW_BOSS(ciCMHDZ_LANE_BACKGROUND)	
	
	CMHDZ_DRAW_COVER(ciCMHDZ_LANE_BACKGROUND, DEFAULT, FALSE)

	CMHDZ_DRAW_ITEMS(ciCMHDZ_LANE_BACKGROUND)
	
	
	
//	FLOAT fScale = CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(CMHDZ_ASSET_SCRAPYARD_MD_VAN_3, 0)
//	CMHDZ_DRAW_GAME_SPRITE(sCMHDZData.tl31BaseTextDict, "LEVEL_02_TOWN_PROP_08",  GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfCMHDZ_BUILDING_2_SPRITE_WIDTH*fScale, cfCMHDZ_BUILDING_2_SPRITE_HEIGHT*fScale), 0, sCMHDZData.rgbaSprite)
//	
	CMHDZ_DRAW_BULLETS_AND_EFFECTS(ciCMHDZ_LANE_BACKGROUND)

	CMHDZ_DRAW_ENEMIES(ciCMHDZ_LANE_MIDDLE)
	
	CMHDZ_DRAW_BOSS(ciCMHDZ_LANE_MIDDLE)	
	
	CMHDZ_DRAW_COVER(ciCMHDZ_LANE_MIDDLE)
	
	CMHDZ_DRAW_ITEMS(ciCMHDZ_LANE_MIDDLE)
		
	CMHDZ_DRAW_BULLETS_AND_EFFECTS(ciCMHDZ_LANE_MIDDLE)
	
	//CMHDZ_DRAW_HOSTAGES(ciCMHDZ_LANE_FOREGROUND)
	
	CMHDZ_DRAW_ITEMS(ciCMHDZ_LANE_FOREGROUND)
	
	CMHDZ_DRAW_FOREGROUND_ENEMIES()
	
	CMHDZ_DRAW_BOSS(ciCMHDZ_LANE_FOREGROUND)	
	
	CMHDZ_DRAW_THROWN_PLAYER_PROJECTILES(0)
	
	CMHDZ_DRAW_COVER(ciCMHDZ_LANE_FOREGROUND)
	
	CMHDZ_DRAW_THROWN_ENEMY_PROJECTILES()
	
	CMHDZ_DRAW_BULLETS_AND_EFFECTS(ciCMHDZ_LANE_FOREGROUND)
	
	CMHDZ_DRAW_GORE_EFFECTS()
			
	BOOL bInMenu = FALSE
#IF IS_DEBUG_BUILD 
#IF CMHDZ_CMDLINE_EDITOR_NEW
		bInMenu = sCMHDZData.sEditor.bPlayStage		
#ENDIF
#ENDIF
	//UNUSED_PARAMETER(bInMenu)
	CMHDZ_DRAW_RETICLES(DEFAULT, bInMenu)
		
	
	CMHDZ_DRAW_SHOT_DISTORTION()
	
	CMHDZ_DRAW_HUD()
	
	TWS_DRAW_FRONT_FX()
ENDPROC

//-------------------------------------------------
// MENU FUNCTIONS
//-------------------------------------------------

PROC CMHDZ_DRAW_MENU_BACKGROUND()

	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sCMHDZData.rgbaBlack)
	
ENDPROC

//PROC CMHDZ_DRAW_LEADERBOARD_BACKGROUND()
//	CMHDZ_DRAW_FAR_BACKGROUND(FALSE, TRUE)
//ENDPROC

PROC CMHDZ_DRAW_MENU_LEVELS()

	
	FLOAT fFactor = 1.0
	TEXT_LABEL_31 tl31Sprite
	TEXT_LABEL_31 tl31TextDict
	//FLOAT fStarOffset = 90
	
	INT iNumPlayers = CMHDZ_GET_NUMBER_OF_PLAYERS()
	INT iLevel
	
	CMHDZ_SCORE_INTEGER_TYPE eLeftHandCabScoreType = CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE
	
	
	//Placeholder

//	CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/8), INIT_VECTOR_2D(328, 52), 0, sCMHDZData.rgbaSprite)
	CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("- LEVEL SELECT -", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/8), DEFAULT, TRUE)
		
	//-- Level 1	
	//tl31Sprite = "CAMHEDZ_LEVEL_SELECT_TEXT"
	tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVLSELECT)
	tl31Sprite = "LEVEL_01_THUMBNAIL"
	IF sCMHDZData.bLevelsCompleted[0]
		tl31Sprite += "_COMPLETED"
		//tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE)
	ENDIF

	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*3, cfBASE_SCREEN_HEIGHT/8*3), INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH*fFactor, cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sCMHDZData.rgbaSprite)
							
	//-- Level 2
	tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVLSELECT)
	tl31Sprite = "LEVEL_02_THUMBNAIL"
	IF sCMHDZData.bLevelsCompleted[1]
		tl31Sprite += "_COMPLETED"
		//tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE)
	ENDIF
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*5, cfBASE_SCREEN_HEIGHT/8*3), INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH*fFactor, cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sCMHDZData.rgbaSprite)
	
							
	//-- Level 3
	tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVLSELECT)
	tl31Sprite = "LEVEL_03_THUMBNAIL"
	IF sCMHDZData.bLevelsCompleted[2]
		tl31Sprite += "_COMPLETED"
		//tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE)
//	ELIF IS_LEVEL_UNLOCKED(CMHDZ_LEVEL_ASYLUM)
//		tl31Sprite = "LVL03"
//	ELSE
//		tl31Sprite = "LVL03_LOCKED"
	ENDIF
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*7, cfBASE_SCREEN_HEIGHT/8*3), INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH*fFactor, cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sCMHDZData.rgbaSprite)
	
							
	//-- Level 4
	tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVLSELECT)
	tl31Sprite = "LEVEL_04_THUMBNAIL"
	IF sCMHDZData.bLevelsCompleted[3]
		tl31Sprite += "_COMPLETED"
		//tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE)
//	ELIF IS_LEVEL_UNLOCKED(CMHDZ_LEVEL_PARK)
//		tl31Sprite = "LVL04"
//	ELSE
//		tl31Sprite = "LVL04_LOCKED"
	ENDIF
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*4, cfBASE_SCREEN_HEIGHT/8*6), INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH*fFactor, cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sCMHDZData.rgbaSprite)
	
	//-- Level 5
	tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_LVLSELECT)
	IF sCMHDZData.bLevelsCompleted[4]
		tl31Sprite = "LEVEL_05_THUMBNAIL_COMPLETED"
		//tl31TextDict = CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE)
	ELIF IS_LEVEL_UNLOCKED(CMHDZ_LEVEL_SLAUGHTERHOUSE)
		tl31Sprite = "LEVEL_05_THUMBNAIL"
	ELSE
		tl31Sprite = "LEVEL_05_THUMBNAIL_UNCOMPLETED"
	ENDIF
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(tl31textDict, tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/10*6, cfBASE_SCREEN_HEIGHT/8*6), INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH*fFactor, cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT*fFactor), 0, sCMHDZData.rgbaSprite)
	
	IF iNumPlayers = 1
		//-- 1 PLAYER
		
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_CITY)
		INT iMyScore = CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL1_SCORE_X_POS, cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS), sCMHDZData.rgbaSprite,
									cfCMHDZ_LEVEL_SCORE_SCALE)
			
			// Display a star rating based on overall high scores for each level.
			CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL1_STAR_X_POS, cfCMHDZ_SOLO_LEVEL1_STAR_Y_POS), CMHDZ_LEVEL_CITY)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_SCRAPYARD)
		iMyScore = CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL2_SCORE_X_POS, cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS), sCMHDZData.rgbaSprite,
									cfCMHDZ_LEVEL_SCORE_SCALE)
									
			// Display a star rating based on overall high scores for each level.
			CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL2_STAR_X_POS, cfCMHDZ_SOLO_LEVEL1_STAR_Y_POS), CMHDZ_LEVEL_SCRAPYARD)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_ASYLUM)
		iMyScore = CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL3_SCORE_X_POS, cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS), sCMHDZData.rgbaSprite,
									cfCMHDZ_LEVEL_SCORE_SCALE)
									
			// Display a star rating based on overall high scores for each level.
			CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL3_STAR_X_POS, cfCMHDZ_SOLO_LEVEL1_STAR_Y_POS), CMHDZ_LEVEL_ASYLUM)
			
		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_PARK)
		iMyScore = CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL4_SCORE_X_POS, cfCMHDZ_SOLO_LEVEL4_SCORE_Y_POS), sCMHDZData.rgbaSprite,
									cfCMHDZ_LEVEL_SCORE_SCALE)
									
			// Display a star rating based on overall high scores for each level.
			CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL4_STAR_X_POS, cfCMHDZ_SOLO_LEVEL4_STAR_Y_POS), CMHDZ_LEVEL_PARK)
									
		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_SLAUGHTERHOUSE)
		iMyScore = CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_LEVEL(ilevel)
		
		IF iMyScore > 0
			
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iMyScore, INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL5_SCORE_X_POS, cfCMHDZ_SOLO_LEVEL4_SCORE_Y_POS), sCMHDZData.rgbaSprite,
									cfCMHDZ_LEVEL_SCORE_SCALE)
									
			// Display a star rating based on overall high scores for each level.
			CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL5_STAR_X_POS, cfCMHDZ_SOLO_LEVEL4_STAR_Y_POS), CMHDZ_LEVEL_SLAUGHTERHOUSE)
									
		ENDIF
		
		INT iTotalScore = CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_ALL_LEVELS()
		IF iTotalScore > 0
			//CMHDZ_DRAW_GAME_SPRITE("FONT", INIT_VECTOR_2D(cfCMHDZ_LEVEL_RED_SCORE_DOLLAR_X_POS, cfCMHDZ_SOLO_LEVEL_SCORE_Y_POS), INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_DOLLAR_X_SIZE, cfCMHDZ_MY_HISCORE_DOLLAR_Y_SIZE), 0, sCMHDZData.rgbaSprite)
			CMHDZ_DRAW_DOLLAR_SYMBOL(INIT_VECTOR_2D(cfCMHDZ_LEVEL_RED_SCORE_DOLLAR_X_POS, cfCMHDZ_RED_SCORE_DOLLAR_Y_POS))
			CMHDZ_DRAW_SCORE_INTEGER(CMHDZ_GET_LOCAL_PLAYER_HI_SCORE_FOR_ALL_LEVELS(), ciCMHDZ_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfCMHDZ_LEVEL_RED_HI_SCORE_X_POS, cfCMHDZ_LEVEL_RED_HI_SCORE_Y_POS), sCMHDZData.rgbaSprite)
		ENDIF
			
	ELIF iNumPlayers > 1
		//-- 2 PLAYERS
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_CITY)
		INT iRedScore = CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		INT iBlueScore = CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		eLeftHandCabScoreType = CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE
		
		//Red Hi-Score - Level 1
		IF iRedScore > iBlueScore
		AND iRedScore > 0

			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iRedScore,
									INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL1_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS),
									CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE), 
									cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
		
		//Blue Hi-Score - Level 1
		IF iBlueScore > iRedScore
		AND iBlueScore > 0			
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iBlueScore,
							INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL1_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS),
							CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE),
							cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
		
//		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			// Red 'Win' - Level 1
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), 
//										"SCORE_TEXT_WIN_GRAPHIC_RED", 
//										INIT_VECTOR_2D(cfCMHDZ_RED_LEVEL1_WIN_X_POS, cfCMHDZ_RED_LEVEL1_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			
//			// Blue 'Win' - Level 1
//
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
//										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
//										INIT_VECTOR_2D(cfCMHDZ_BLUE_LEVEL1_WIN_X_POS, cfCMHDZ_BLUE_LEVEL1_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ENDIF
		
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_SCRAPYARD)
		iRedScore = CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		// Red hi-score - Level 2
		IF iRedScore > iBlueScore
		AND iRedScore > 0

			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iRedScore, 
									INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL2_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS),
									CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE),
									cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
			
		// Blue hi-score - Level 2
		IF iBlueScore > iRedScore
		AND iBlueScore > 0
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL2_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS),
								CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE),
								cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF	

//		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			// Red 'Win' - Level 2
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
//										"SCORE_TEXT_WIN_GRAPHIC_RED", 
//										INIT_VECTOR_2D(cfCMHDZ_RED_LEVEL2_WIN_X_POS, cfCMHDZ_RED_LEVEL1_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			// Blue 'Win' - Level 2
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
//										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
//										INIT_VECTOR_2D(cfCMHDZ_BLUE_LEVEL2_WIN_X_POS, cfCMHDZ_BLUE_LEVEL1_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_ASYLUM)
		iRedScore = CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		// Red Hi-score - level 3
		IF iRedScore > iBlueScore
		AND iRedScore > 0

			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iRedScore,
									INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL3_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS),
									CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE),
									cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
				
		
		
		// Blue Hi-score - level 3
		IF iBlueScore > iRedScore
		AND iBlueScore > 0
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL3_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL1_SCORE_Y_POS),
								CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE),
								cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
		
//		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			// Red 'Win' - Level 3
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
//										"SCORE_TEXT_WIN_GRAPHIC_RED", 
//										INIT_VECTOR_2D(cfCMHDZ_RED_LEVEL3_WIN_X_POS, cfCMHDZ_RED_LEVEL1_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//										
//		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)							
//			// Blue 'Win' - Level 3
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), 
//										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
//										INIT_VECTOR_2D(cfCMHDZ_BLUE_LEVEL3_WIN_X_POS, cfCMHDZ_BLUE_LEVEL1_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_PARK)
		iRedScore = CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		// Red Hi-score - level 4
		IF iRedScore > iBlueScore
		AND iRedScore > 0

			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iRedScore,
									INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL4_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL4_SCORE_Y_POS),
									CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE),
									cfCMHDZ_LEVEL_SCORE_SCALE)
							
		ENDIF
		
		// Blue Hi-score - level 4
		IF iBlueScore > iRedScore
		AND iBlueScore > 0
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL4_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL4_SCORE_Y_POS),
								CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE),
								cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
//		
//		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			// Red 'Win' - Level 4
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
//										"SCORE_TEXT_WIN_GRAPHIC_RED", 
//										INIT_VECTOR_2D(cfCMHDZ_RED_LEVEL4_WIN_X_POS, cfCMHDZ_RED_LEVEL4_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//		
//			// Blue 'Win' - Level 4
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT),
//										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
//										INIT_VECTOR_2D(cfCMHDZ_BLUE_LEVEL4_WIN_X_POS, cfCMHDZ_RED_LEVEL4_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ENDIF
		
		iLevel = ENUM_TO_INT(CMHDZ_LEVEL_SLAUGHTERHOUSE)
		iRedScore = CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(ENUM_TO_INT(iLevel))
		iBlueScore = CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
		
		//-- Red Hi-score - level 5
		IF iRedScore > iBlueScore
		AND iRedScore > 0

			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iRedScore,
									INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL5_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL4_SCORE_Y_POS),
									CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE),
									cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF	
	
		//-- Blue Hi-score - level 5
		IF iBlueScore > iRedScore
		AND iBlueScore > 0
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iBlueScore,
								INIT_VECTOR_2D(cfCMHDZ_SOLO_LEVEL5_SCORE_X_POS,cfCMHDZ_SOLO_LEVEL4_SCORE_Y_POS),
								CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE),
								cfCMHDZ_LEVEL_SCORE_SCALE)
		ENDIF
							
//		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)
//			// Red 'Win' - Level 5
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), 
//										"SCORE_TEXT_WIN_GRAPHIC_RED", 
//										INIT_VECTOR_2D(cfCMHDZ_RED_LEVEL5_WIN_X_POS, cfCMHDZ_RED_LEVEL4_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)	
//		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel) < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_LEVEL_FROM_HOST(iLevel)	
//			// Blue 'Win' - Level 5
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), 
//										"SCORE_TEXT_WIN_GRAPHIC_BLUE", 
//										INIT_VECTOR_2D(cfCMHDZ_BLUE_LEVEL5_WIN_X_POS, cfCMHDZ_RED_LEVEL4_WIN_Y_POS), 
//										INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE * cfCMHDZ_LEVEL_SCORE_SCALE, cfCMHDZ_SCORE_WIN_Y_SIZE* cfCMHDZ_LEVEL_SCORE_SCALE), 
//										0, 
//										sCMHDZData.rgbaSprite)
//		ENDIF
		
		// 'Red' (1P) Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), "FONT_$", INIT_VECTOR_2D(cfCMHDZ_LEVEL_RED_SCORE_DOLLAR_X_POS, cfCMHDZ_LEVEL_RED_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_DOLLAR_X_SIZE, cfCMHDZ_MY_HISCORE_DOLLAR_Y_SIZE), 0, CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_RED_SCORE))
		CMHDZ_DRAW_SCORE_INTEGER(CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST(), ciCMHDZ_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfCMHDZ_LEVEL_RED_HI_SCORE_X_POS, cfCMHDZ_LEVEL_RED_HI_SCORE_Y_POS), CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(eLeftHandCabScoreType))
	
		// 'Blue' (2P) Score
		ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), "FONT_$", INIT_VECTOR_2D(cfCMHDZ_LEVEL_BLUE_SCORE_DOLLAR_X_POS, cfCMHDZ_LEVEL_BLUE_SCORE_DOLLAR_Y_POS), INIT_VECTOR_2D(cfCMHDZ_MY_HISCORE_DOLLAR_X_SIZE, cfCMHDZ_MY_HISCORE_DOLLAR_Y_SIZE), 0, CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE))
		CMHDZ_DRAW_SCORE_INTEGER(CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST(), ciCMHDZ_MAX_SCORE_DIGITS, INIT_VECTOR_2D(cfCMHDZ_LEVEL_BLUE_HI_SCORE_X_POS, cfCMHDZ_LEVEL_BLUE_HI_SCORE_Y_POS), CMHDZ_GET_SCORE_COLOUR_FROM_TYPE(CMHDZ_SCORE_INTEGER_TYPE_BLUE_SCORE))
		
//		IF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST() > CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST()
//		
//			// Red 'Win'
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), "SCORE_TEXT_WIN_GRAPHIC_RED", INIT_VECTOR_2D(cfCMHDZ_LEVEL_SCORE_WIN_RED_X_POS, cfCMHDZ_LEVEL_SCORE_WIN_RED_Y_POS), INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE, cfCMHDZ_SCORE_WIN_Y_SIZE), 0, sCMHDZData.rgbaSprite)
//		ELIF CMHDZ_GET_RED_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST() < CMHDZ_GET_BLUE_PLAYER_HI_SCORE_FOR_ALL_LEVELS_FROM_HOST()
//	
//			// Blue 'Win'
//			ARCADE_DRAW_PIXELSPACE_SPRITE(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_VHSFONT), "SCORE_TEXT_WIN_GRAPHIC_BLUE", INIT_VECTOR_2D(cfCMHDZ_LEVEL_SCORE_WIN_BLUE_X_POS, cfCMHDZ_LEVEL_SCORE_WIN_BLUE_Y_POS), INIT_VECTOR_2D(cfCMHDZ_SCORE_WIN_X_SIZE, cfCMHDZ_SCORE_WIN_Y_SIZE), 0, sCMHDZData.rgbaSprite)
//		ENDIF
	ENDIF
		
	//ENDIF
ENDPROC

PROC CMHDZ_DRAW_CLIENT_STATE_MENU()
	
//	IF sCMHDZData.fSyncOffsetY > 0.0
//	 	sCMHDZData.fSyncOffsetY -= (0.0+@GET_RANDOM_FLOAT_IN_RANGE(2500.0,4000.0))
//	ELSE
//		sCMHDZData.fSyncOffsetY = 0.0
//	ENDIF
	
	CMHDZ_DRAW_MENU_BACKGROUND()
	CMHDZ_DRAW_MENU_LEVELS()
	CMHDZ_DRAW_BULLETS_AND_EFFECTS(1)
	CMHDZ_DRAW_RETICLES(DEFAULT, TRUE)
	TWS_DRAW_FRONT_FX()
	
ENDPROC


//-------------------------------------------------
// TITLE SCREEN FUNCTIONS
//-------------------------------------------------
PROC CMHDZ_DRAW_TITLE_SCREEN_ELEMENTS()
	BOOL bHighlight

//
//	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_TITLES), "CAMHEDZ_TITLE_SCREEN_LOGO", 
//		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, (cfBASE_SCREEN_HEIGHT/2)+100), INIT_VECTOR_2D(908, 316), 0, sCMHDZData.rgbaSprite)
	
	CMHDZ_DRAW_MENU_BACKGROUND()
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_TITLES), "CAMHEDZ_TITLE_SCREEN_CHARACTER", 
		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, (cfBASE_SCREEN_HEIGHT/2)), INIT_VECTOR_2D(782, 1080), 0, sCMHDZData.rgbaSprite)
		
	// Draw the looping Bink for the title screen.
//	FLOAT fRatioWidth = 1.0 * GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
//	FLOAT fRatioHeight = 1.0
//	IF GET_BINK_MOVIE_TIME(sCMHDZData.bmIdCmhdzIntro) > 99.5
//		SET_BINK_MOVIE_TIME(sCMHDZData.bmIdCmhdzIntro, 99.5)
//	ENDIF
	//DRAW_BINK_MOVIE(sCMHDZData.bmIdCmhdzIntro, cfSCREEN_CENTER, cfSCREEN_CENTER, fRatioWidth, fRatioHeight, 0.0, 255,255,255,255)
	CMHDZ_DRAW_MENU_BACKGROUND()
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_TITLES), "CAMHEDZ_TITLE_SCREEN_CHARACTER", 
		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, (cfBASE_SCREEN_HEIGHT/2)), INIT_VECTOR_2D(782, 1080), 0, sCMHDZData.rgbaSprite)
		
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_TITLES), "CAMHEDZ_TITLE_SCREEN_PIXTRO_LOGO", 
		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, (cfBASE_SCREEN_HEIGHT/2)+388), INIT_VECTOR_2D(200, 44), 0, sCMHDZData.rgbaSprite)

	IF CMHDZ_IS_POSITION_OVER_MENU_OPTION(sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos, CMHDZ_START)
		bHighlight = TRUE
	ENDIF
	//CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE), "CAMHEDZ_TITLE_SCREEN_START_GAME_BUTTON", sCMHDZData.sMenuOptionsData[CMHDZ_START].vSpritePos, sCMHDZData.sMenuOptionsData[CMHDZ_START].vSize, 0, sCMHDZData.rgbaSprite)		
	
	//IF NOT 
	IF bHighlight
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("START", sCMHDZData.sMenuOptionsData[CMHDZ_START].vSpritePos, DEFAULT, TRUE, DEFAULT, sCMHDZData.rgbaYellow.iR, sCMHDZData.rgbaYellow.iG, sCMHDZData.rgbaYellow.iB, sCMHDZData.rgbaYellow.iA)
	ELSE
		CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("START", sCMHDZData.sMenuOptionsData[CMHDZ_START].vSpritePos, DEFAULT, TRUE, DEFAULT, sCMHDZData.rgbaWhite.iR, sCMHDZData.rgbaWhite.iG, sCMHDZData.rgbaWhite.iB, sCMHDZData.rgbaWhite.iA)
	ENDIF
	
	bHighlight = FALSE
	IF NOT sCMHDZData.bSkipLeaderboard

		IF CMHDZ_IS_POSITION_OVER_MENU_OPTION(sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos, CMHDZ_SCORES)
			bHighlight = TRUE
		ENDIF
		
		IF bHighlight
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("LEADERBOARD", sCMHDZData.sMenuOptionsData[CMHDZ_SCORES].vSpritePos, DEFAULT, TRUE, DEFAULT, sCMHDZData.rgbaYellow.iR, sCMHDZData.rgbaYellow.iG, sCMHDZData.rgbaYellow.iB, sCMHDZData.rgbaYellow.iA)
		ELSE
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("LEADERBOARD", sCMHDZData.sMenuOptionsData[CMHDZ_SCORES].vSpritePos, DEFAULT, TRUE, DEFAULT, sCMHDZData.rgbaWhite.iR, sCMHDZData.rgbaWhite.iG, sCMHDZData.rgbaWhite.iB, sCMHDZData.rgbaWhite.iA)
		ENDIF
	ENDIF
	
//	tl31Sprite = "PIXTRO_LOGO"
//	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE), tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/6*5 + 60), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PIXTRO_LOGO_WIDTH, cfCMHDZ_SPRITE_HUD_PIXTRO_LOGO_HEIGHT), 0, sCMHDZData.rgbaSprite)
		
		
#IF IS_DEBUG_BUILD
	//
#ENDIF
	
ENDPROC

PROC CMHDZ_DRAW_CLIENT_STATE_TITLE_SCREEN()

	CMHDZ_DRAW_TITLE_SCREEN_ELEMENTS()
	CMHDZ_DRAW_BULLETS_AND_EFFECTS(1)
	CMHDZ_DRAW_RETICLES(DEFAULT, TRUE)
	TWS_DRAW_FRONT_FX()
	//CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("HELLO WORLD!", INIT_VECTOR_2D(1000, 700), 2, DEFAULT, 1)
ENDPROC


//===========================================================
//					NEW REWIND SYSTEM
//===========================================================
PROC CMHDZ_REWIND_GET_REWIND_ANIMATION(TEXT_LABEL_63 &sRewindFrame)
	sRewindFrame = "CMHDZ_REWIND_FRAME_"

	SWITCH sCMHDZData.eCurrentLevel
	
		CASE CMHDZ_LEVEL_CITY
			sRewindFrame += "CITY"
		BREAK
		
		CASE CMHDZ_LEVEL_SCRAPYARD
			sRewindFrame += "SCRAPYARD"
		BREAK
		
		CASE CMHDZ_LEVEL_ASYLUM
			sRewindFrame += "ASYLUM"
		BREAK
		
		CASE CMHDZ_LEVEL_PARK
			sRewindFrame += "THEMEPARK"
		BREAK
					
		CASE CMHDZ_LEVEL_SLAUGHTERHOUSE
			sRewindFrame += "SLAUGHTERHOUSE"
		BREAK
	
	ENDSWITCH
		
ENDPROC

FUNC BOOL CMHDZ_IS_REWIND_SEQUENCE_FINISHED()

	TEXT_LABEL_63 tl63RewindFrame
	sCMHDZData.iTimer += CEIL(0.0+@1000)
	CMHDZ_REWIND_GET_REWIND_ANIMATION(tl63RewindFrame)
	
	RGBA_COLOUR_STRUCT rgbaRandomisedColour
	
	INIT_RGBA_STRUCT(rgbaRandomisedColour,
		GET_RANDOM_INT_IN_RANGE(240,255),
		GET_RANDOM_INT_IN_RANGE(240,255),
		GET_RANDOM_INT_IN_RANGE(240,255))
	
	//SCRIPT_ASSERT(CMHDZ_GET_TEXTURE_DICTIONARY_FOR_LEVEL(sCMHDZData.eCurrentLevel))
	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICTIONARY_FOR_LEVEL(sCMHDZData.eCurrentLevel),
						tl63RewindFrame,
						INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0) + sCMHDZData.fSyncOffsetX, cfBASE_SCREEN_HEIGHT/2.0),
						INIT_VECTOR_2D(2048, 944),
						0.0,
						rgbaRandomisedColour)

	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICTIONARY_FOR_LEVEL(sCMHDZData.eCurrentLevel),
						tl63RewindFrame,
						INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0 + (sCMHDZData.fSyncOffsetX - 2048), cfBASE_SCREEN_HEIGHT/2.0),
						INIT_VECTOR_2D(2048, 944),
						0.0,
						rgbaRandomisedColour)

	//CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
	CMHDZ_DRAW_REWIND_EFFECT()
	
	// Blink rewind
	IF (sCMHDZData.iTimer%500) < 250
		// Rewind symbol appears on screen
		CMHDZ_DRAW_REWIND_SYMBOL()
	ENDIF
	
	CMHDZ_DRAW_DAMAGE_NOISE(100)

	IF sCMHDZData.iTimer < 1850
	
		sCMHDZData.fSyncOffsetX += (0.0+@4000)
		
		IF (sCMHDZData.fSyncOffsetX >= 2048)
			sCMHDZData.fSyncOffsetX = 0.0
		ENDIF
		
		sCMHDZData.fSyncOffsetY = GET_RANDOM_FLOAT_IN_RANGE(30,90)
		
	ELSE
		sCMHDZData.iTimer = 0 // Reset timer
		sCMHDZData.fSyncOffsetX = 0.0
		sCMHDZData.fSyncOffsetY = 0.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//-------------------------------------------------
// GAME OVER FUNCTIONS
//-------------------------------------------------
PROC CMHDZ_DRAW_GAME_OVER_SCREEN()
	BOOL bHighlight 
	RGBA_COLOUR_STRUCT col 
	
	DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
	CMHDZ_DRAW_GAME_OVER_EFFECT()
	CMHDZ_DRAW_PLAYER_HEALTH_BAR()
	
	TEXT_LABEL_15 sSubtitles
	
	SWITCH (sCMHDZData.eResultState)
		CASE CMHDZ_RESULT_SCREEN_STATE_INIT
			sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/3, cfBASE_SCREEN_HEIGHT/3)
			//CMHDZ_REPLAY_SYSTEM_STOP_RECORDING()
			CMHDZ_SET_RESULT_SCREEN_STATE(CMHDZ_RESULT_SCREEN_STATE_DEAD_SCREEN)
			
			// Display the subtitle if language isn't English				
			IF GET_CURRENT_LANGUAGE() != LANGUAGE_ENGLISH
				sSubtitles = "CMHDZ_LVD1"
				
				SWITCH sCMHDZData.eCurrentLevel
					
					CASE CMHDZ_LEVEL_CITY
						sSubtitles = "CMHDZ_LVD1"
					BREAK 
					
					CASE CMHDZ_LEVEL_SCRAPYARD
						sSubtitles = "CMHDZ_LVD2"
					BREAK 

					CASE CMHDZ_LEVEL_ASYLUM
						sSubtitles = "CMHDZ_LVD3"
					BREAK 
					
					CASE CMHDZ_LEVEL_PARK
						sSubtitles = "CMHDZ_LVD4"
					BREAK 
					
					CASE CMHDZ_LEVEL_SLAUGHTERHOUSE
						sSubtitles = "CMHDZ_LVD5"
					BREAK 
					
				ENDSWITCH
				
				PRINT_NOW(sSubtitles, 8000, 1)

			ENDIF
		BREAK
		
		CASE CMHDZ_RESULT_SCREEN_STATE_DEAD_SCREEN
			ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_CMHDZ_HELP_MENU)
			CMHDZ_DRAW_SIGNAL_FAILURE_TEXT()//, sCMHDZData.rgbaWhite)
			
			col = sCMHDZData.rgbaYellow
			bHighlight = CMHDZ_IS_POSITION_OVER_MENU_OPTION(sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos, CMHDZ_REWIND)			
			IF NOT bHighlight
				col = sCMHDZData.rgbaWhite
//			ELSE // Draw the symbol
//				posX = 600.0
//				CMHDZ_DRAW_SELECT_SYMBOL(INIT_VECTOR_2D(posX, sCMHDZData.sMenuOptionsData[CMHDZ_REWIND].vSpritePos.y + 12))
			ENDIF
			CMHDZ_DRAW_REWIND_OPTION_TEXT(sCMHDZData.sMenuOptionsData[CMHDZ_REWIND].vSpritePos, col)
			
			col = sCMHDZData.rgbaYellow
			bHighlight = CMHDZ_IS_POSITION_OVER_MENU_OPTION(sCMHDZData.sPlayerData[0].vSpriteFinalReticlePos, CMHDZ_EJECT)			
			IF NOT bHighlight
				col = sCMHDZData.rgbaWhite
//			ELSE
//				posX = 600.0
//				CMHDZ_DRAW_SELECT_SYMBOL(INIT_VECTOR_2D(posX, sCMHDZData.sMenuOptionsData[CMHDZ_EJECT].vSpritePos.y + 12))
			ENDIF
			CMHDZ_DRAW_EJECT_OPTION_TEXT(sCMHDZData.sMenuOptionsData[CMHDZ_EJECT].vSpritePos, col)		
			CMHDZ_DRAW_RETICLES(TRUE, TRUE)
		BREAK 
		
		CASE CMHDZ_RESULT_SCREEN_STATE_START_REWIND
			ARCADE_GAMES_HELP_TEXT_CLEAR()
			CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_REWIND_START)
			CMHDZ_PLAY_SOUND_LOOPING_NO_POSITION(CMHDZ_AUDIO_EFFECT_REWIND_LOOP)
			sCMHDZData.iTimer = 0
			CMHDZ_SET_RESULT_SCREEN_STATE(CMHDZ_RESULT_SCREEN_STATE_REWIND)
		BREAK
		
		CASE CMHDZ_RESULT_SCREEN_STATE_REWIND
			IF CMHDZ_IS_REWIND_SEQUENCE_FINISHED()
				sCMHDZData.fBackgroundSpeedMult = 0.0
				CMHDZ_PLAY_DIRECTOR_DIALOGUE(CMHDZ_AUDIO_EFFECT_DIRECTOR_Restart_Level)
				CMHDZ_STOP_SOUND(CMHDZ_AUDIO_EFFECT_REWIND_LOOP)
				CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_REWIND_STOP)
				CMHDZ_SET_RESULT_SCREEN_STATE(CMHDZ_RESULT_SCREEN_STATE_COMPLETE)			
			ELSE  
				sCMHDZData.fBackgroundSpeedMult = 8.0
			ENDIF 
			//CMHDZ_DRAW_BACKGROUND(TRUE)
			//CMHDZ_REPLAY_SYSTEM_DRAW()

		BREAK
		
		
		CASE CMHDZ_RESULT_SCREEN_STATE_GOTO_LEADERBOARD
			ARCADE_GAMES_HELP_TEXT_CLEAR()
			sCMHDZData.fBackgroundSpeedMult = 1.0
			//CMHDZ_REPLAY_SYSTEM_RESET()
		BREAK

	ENDSWITCH
ENDPROC

PROC CMHDZ_DRAW_CLIENT_STATE_GAME_OVER()
	
	CMHDZ_DRAW_MENU_BACKGROUND()
	CMHDZ_DRAW_GAME_OVER_SCREEN()
	TWS_DRAW_FRONT_FX()
ENDPROC

PROC CMHDZ_DRAW_RESULT_SCREEN_ELEMENTS(BOOL bDoOutro)
	
	TEXT_LABEL_31 tl31Sprite
	INT iScore
	INT iAccuracy
	INT iAccuracyBonus
	INT iLevel = CMHDZ_GET_CURRENT_LEVEL_AS_INT()
	
	BOOL bShouldShowScore = TRUE
	BOOL bShouldShowAccuracy = FALSE
	
	BOOL bPlayerDead
	
	IF CMHDZ_GET_LOCAL_PLAYER_HEALTH() <= 0.0
		bPlayerDead = TRUE
	ENDIF
	
	IF sCMHDZData.iResultsScreenTimer = 0
		sCMHDZData.iResultsScreenTimer = GET_GAME_TIMER()
	ENDIF
	
	
	IF NOT bPlayerDead
		IF GET_GAME_TIMER() - sCMHDZData.iResultsScreenTimer > 1500
			bShouldShowAccuracy = TRUE
			
	//     NEEDS TO TAKE INTO ACCOUNT ANIMALS SHOT
			//iAccuracy = ROUND(100.0 * (TO_FLOAT(CMHDZ_GET_TOTAL_ENEMY_HITS_FOR_CURRENT_LEVEL()) / TO_FLOAT(CMHDZ_GET_TOTAL_SHOTS_FIRED_IN_CURRENT_LEVEL())))
			iAccuracy = ROUND(CMHDZ_GET_PLAYER_ACCURACY_FOR_CURRENT_LEVEL())
			
			IF iAccuracy > 100
				iAccuracy = 100
			ENDIF
			
			IF NOT CMHDZ_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_ADDED_ON_ACCURACY_BONUS)
				//-- Accuracy bonus is accuracy * level score
				iAccuracyBonus = ROUND(TO_FLOAT(playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]) * (TO_FLOAT(iAccuracy) / 100.0))
				playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel] += iAccuracyBonus
				
				//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} Accuracy Bonus: ", iAccuracyBonus)
				//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel])
				
				
				IF playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] < playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
		 			playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[iLevel]
					//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_RESULT_SCREEN_ELEMENTS] {DSW} New Hi-score: ", playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[iLevel])
				ENDIF
				
				CMHDZ_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_ADDED_ON_ACCURACY_BONUS)
			ENDIF
		ENDIF
		
	ENDIF
	

		
#IF IS_DEBUG_BUILD
		BOOL bDoOutroDebug = FALSE
#ENDIF

	//bDoOutro = TRUE
	IF bDoOutro
#IF IS_DEBUG_BUILD
	OR bDoOutroDebug
#ENDIF
	
		//-- Bearsy
		tl31Sprite = "BEARSY_DEAD_STATE"
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(
			CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE), 
			tl31Sprite, 
			INIT_VECTOR_2D(cfCMHDZ_SPRITE_FINAL_SCENE_POSTER_X, cfCMHDZ_SPRITE_FINAL_SCENE_POSTER_Y),
			//INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/6*2, (cfBASE_SCREEN_HEIGHT/3*2)-100),
			INIT_VECTOR_2D(cfCMHDZ_SPRITE_FINAL_SCENE_POSTER_WIDTH, cfCMHDZ_SPRITE_FINAL_SCENE_POSTER_HEIGHT), 
			0, sCMHDZData.rgbaSprite)
				
		// --- Level complete text
		tl31Sprite = "LEVEL_0"
		tl31Sprite += ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1
		tl31Sprite += "_OUTRO_WIN"
		
		VECTOR_2D vec = INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_RESULT_TEXT_WIDTH, cfCMHDZ_SPRITE_HUD_RESULT_TEXT_HEIGHT)
		
		SWITCH (ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1)
		
			CASE 1
				vec = INIT_VECTOR_2D(376, 76)
			BREAK
		
			CASE 2
				vec = INIT_VECTOR_2D(436, 104)
			BREAK
			
			CASE 3
				vec = INIT_VECTOR_2D(324, 104)
			BREAK
			
			CASE 4
				vec = INIT_VECTOR_2D(400, 104)
			BREAK
			
			CASE 5
				vec = INIT_VECTOR_2D(580, 104)
			BREAK
		
		ENDSWITCH

		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_RESULTS), 
			tl31Sprite, 
			INIT_VECTOR_2D(cfCMHDZ_SPRITE_FINAL_SCENE_TEXT_X, cfCMHDZ_SPRITE_FINAL_SCENE_TEXT_Y), 
			vec,
			0, sCMHDZData.rgbaSprite)
				
		// --- Level complete Thumbnail
		tl31Sprite = "LEVEL_0"
		tl31Sprite += ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1
		tl31Sprite += "_THUMBNAIL_COMPLETED"
		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE), 
			tl31Sprite, 
			INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/6*2)-8, (cfBASE_SCREEN_HEIGHT/2)-20), 
			INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH, 
			cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT), 
			0, sCMHDZData.rgbaSprite)

		
		IF bShouldShowScore
		
			//-- Score
//			tl31Sprite = "SCORE_SCREEN_TEXT_01"
//			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_RESULTS), 
//				tl31Sprite, 
//				INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_X, cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_Y), 
//				INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_Y), 
//				0, sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("SCORE", 
				INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_X, cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_Y), 
				DEFAULT, DEFAULT, DEFAULT, 
				sCMHDZData.rgbaYellow.iR, 
				sCMHDZData.rgbaYellow.iG, 
				sCMHDZData.rgbaYellow.iB, 
				sCMHDZData.rgbaYellow.iA)
			
			//CMHDZ_DRAW_DOLLAR_SYMBOL(INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_DOLLAR_POS_X, cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_DOLLAR_POS_Y))

			iScore = CMHDZ_GET_LOCAL_PLAYER_TOTAL_SCORE()
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iScore,
													INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_VAL_POS_X, cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_SCORE_POS_Y),
													sCMHDZData.rgbaSprite, 1)
			
			
			IF NOT CMHDZ_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
				CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				CMHDZ_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
			ENDIF
		ENDIF
		

		
		IF bShouldShowAccuracy
			//-- Accuracy
//			tl31Sprite = "SCORE_SCREEN_TEXT_02"
//	//		INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_Y)
//			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_RESULTS), tl31Sprite, INIT_VECTOR_2D(cfCMHDZ_RESULTS_GAME_SCREEN_COMPLETE_ACCURACY_POS_X, cfCMHDZ_RESULTS_GAME_SCREEN_COMPLETE_ACCURACY_POS_Y), INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_Y), 0, sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("ACCURACY", 
				INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_ACCURACY_POS_X, cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_ACCURACY_POS_Y), 
				DEFAULT, DEFAULT, DEFAULT, 
				sCMHDZData.rgbaYellow.iR, 
				sCMHDZData.rgbaYellow.iG, 
				sCMHDZData.rgbaYellow.iB, 
				sCMHDZData.rgbaYellow.iA)
			
			//CMHDZ_DRAW_SCORE_INTEGER_WITH_PERCENTAGE(
			CMHDZ_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iAccuracy,
													INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_ACCURACY_VAL_POS_X, cfCMHDZ_RESULTS_SCREEN_GAME_COMPLETE_ACCURACY_POS_Y),
													sCMHDZData.rgbaSprite,
													1, DEFAULT, TRUE)//, TRUE)
			
			//CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_GAME_RESULTS_STAR_X_POS, cfCMHDZ_SOLO_GAME_RESULTS_STAR_Y_POS), sCMHDZData.eCurrentLevel)
			CMHDZ_DRAW_STAR_RATING_RESULTS_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_GAME_RESULTS_STAR_X_POS, cfCMHDZ_SOLO_GAME_RESULTS_STAR_Y_POS), sCMHDZData.eCurrentLevel, 48)
						
			IF NOT CMHDZ_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
				CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				
				SWITCH (ROUND(playerBd[PARTICIPANT_ID_TO_INT()].fPlayerStarRating[sCMHDZData.eCurrentLevel]))
					CASE 0
					CASE 1
					CASE 2
						CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_PLAYER_SCORE_BAD)
					BREAK
				
					CASE 3
						CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_PLAYER_SCORE_AVERAGE)
					BREAK
					
					CASE 4
						CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_PLAYER_SCORE_GOOD)
					BREAK
					
					CASE 5
						CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_PLAYER_SCORE_AMAZING)
					BREAK
				ENDSWITCH
				
				CMHDZ_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
			ENDIF
		ENDIF
		
	// Regular stage clear data
	ELIF NOT bPlayerDead

	
		tl31Sprite = "LEVEL_0"
		tl31Sprite += ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1
		tl31Sprite += "_THUMBNAIL_COMPLETED"
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_TITLE), tl31Sprite, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/6*2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfCMHDZ_SPRITE_POSTER_MENU_WIDTH, cfCMHDZ_SPRITE_POSTER_MENU_HEIGHT), 0, sCMHDZData.rgbaSprite)
		
		tl31Sprite = "LEVEL_0"
		tl31Sprite += ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1
		tl31Sprite += "_OUTRO_WIN"
		VECTOR_2D vec = INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_RESULT_TEXT_WIDTH, cfCMHDZ_SPRITE_HUD_RESULT_TEXT_HEIGHT)
		
		SWITCH (ENUM_TO_INT(sCMHDZData.eCurrentLevel)+1)
		
			CASE 1
				vec = INIT_VECTOR_2D(376, 76)
			BREAK
		
			CASE 2
				vec = INIT_VECTOR_2D(436, 104)
			BREAK
			
			CASE 3
				vec = INIT_VECTOR_2D(324, 104)
			BREAK
			
			CASE 4
				vec = INIT_VECTOR_2D(400, 104)
			BREAK
			
			CASE 5
				vec = INIT_VECTOR_2D(580, 104)
			BREAK
		
		ENDSWITCH
		
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_RESULTS), tl31Sprite, INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/8*4)+152, (cfBASE_SCREEN_HEIGHT/3)+57), vec, 0, sCMHDZData.rgbaSprite)

		IF bShouldShowScore
		
			//-- Score
//			tl31Sprite = "SCORE_SCREEN_TEXT_01"
//			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_RESULTS), tl31Sprite, INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_POS_Y), INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_SIZE_Y), 0, sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("SCORE", 
				INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_POS_Y), 
				DEFAULT, DEFAULT, DEFAULT, 
				sCMHDZData.rgbaYellow.iR, 
				sCMHDZData.rgbaYellow.iG, 
				sCMHDZData.rgbaYellow.iB, 
				sCMHDZData.rgbaYellow.iA)
			
			//CMHDZ_DRAW_DOLLAR_SYMBOL(INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_DOLLAR_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_Y))

			iScore = CMHDZ_GET_LOCAL_PLAYER_SCORE_FOR_LEVEL(CMHDZ_GET_CURRENT_LEVEL_AS_INT())
			CMHDZ_DRAW_SCORE_INTEGER_WITH_DOLLAR(iScore,
													INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_SCORE_VAL_POS_Y),
													sCMHDZData.rgbaSprite,
													1)
						
			IF NOT CMHDZ_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
				CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)
				CMHDZ_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_RESULTS_SCORE_AUDIO)
			ENDIF
		ENDIF
		
		
		IF bShouldShowAccuracy
			//-- Accuracy
//			tl31Sprite = "SCORE_SCREEN_TEXT_02"
//			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_RESULTS), tl31Sprite, INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_Y), INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_SIZE_Y), 0, sCMHDZData.rgbaSprite)
			CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("ACCURACY", 
				INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_POS_Y), 
				DEFAULT, DEFAULT, DEFAULT, 
				sCMHDZData.rgbaYellow.iR, 
				sCMHDZData.rgbaYellow.iG, 
				sCMHDZData.rgbaYellow.iB, 
				sCMHDZData.rgbaYellow.iA)			

			
		//	//CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_DRAW_RESULT_SCREEN_ELEMENTS] iAccuracy: ", iAccuracy)
			CMHDZ_DRAW_SCORE_INTEGER_CENTER_LEFT_MOST(iAccuracy,
													INIT_VECTOR_2D(cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_VAL_POS_X, cfCMHDZ_RESULTS_SCREEN_COMPLETE_ACCURACY_VAL_POS_Y),
													sCMHDZData.rgbaSprite,
													1, DEFAULT, TRUE)//, TRUE)
													
			//CMHDZ_DRAW_STAR_RATING_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_RESULTS_STAR_X_POS, cfCMHDZ_SOLO_RESULTS_STAR_Y_POS), sCMHDZData.eCurrentLevel)
			CMHDZ_DRAW_STAR_RATING_RESULTS_FOR_STAGE(INIT_VECTOR_2D(cfCMHDZ_SOLO_RESULTS_STAR_X_POS, cfCMHDZ_SOLO_RESULTS_STAR_Y_POS), sCMHDZData.eCurrentLevel, 48)

											
			IF NOT CMHDZ_IS_LEVEL_BIT_SET_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
				CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_RESULTS_SCORE_UPDATE)	
				CMHDZ_SET_LEVEL_BIT_FOR_CURRENT_LEVEL(CMHDZ_LEVELS_BIT_PLAYED_ACCURACY_SCORE_AUDIO)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC CMHDZ_DRAW_CLIENT_STATE_RESULT_SCREEN(BOOL bDoOutro)
	
	CMHDZ_DRAW_MENU_BACKGROUND()
	CMHDZ_DRAW_RESULT_SCREEN_ELEMENTS(bDoOutro)
	TWS_DRAW_FRONT_FX()
	
ENDPROC


//-------------------------------------------------
// DEBUG FUNCTIONS
//-------------------------------------------------
#IF IS_DEBUG_BUILD
#IF CMHDZ_CMDLINE_EDITOR_NEW

//FUNC CMHDZ_SPRITE CMHDZ_DEBUG_GET_SPRITE_FOR_HOSTAGE_TYPE(CMHDZ_HOSTAGE_TYPES eType)
//	CMHDZ_SPRITE eSprite
//	
//	SWITCH eType
//		CASE CMHDZ_HOSTAGE_GIRL
//			eSprite = CMHDZ_SPRITE_HOSTAGE11
//		BREAK
//		CASE CMHDZ_HOSTAGE_GUY
//			eSprite = CMHDZ_SPRITE_HOSTAGE21
//		BREAK
//		CASE CMHDZ_HOSTAGE_EAGLE
//			eSprite = CMHDZ_SPRITE_EAGLE1
//		BREAK
//		CASE CMHDZ_HOSTAGE_VULTURE
//			eSprite = CMHDZ_SPRITE_VULTURE_01
//		BREAK
//		CASE CMHDZ_HOSTAGE_BAT
//			eSprite = CMHDZ_SPRITE_BAT_01
//		BREAK
//		CASE CMHDZ_HOSTAGE_SCORPION
//			eSprite = CMHDZ_SPRITE_ANIMALBONUS_SCORPION_01
//		BREAK
//		CASE CMHDZ_HOSTAGE_MOUSE
//			eSprite = CMHDZ_SPRITE_ANIMALBONUS_RAT_01
//		BREAK
//	ENDSWITCH
//		
//	RETURN eSprite
//ENDFUNC

FUNC CMHDZ_SPRITE CMHDZ_DEBUG_GET_SPRITE_FOR_ENEMY_TYPE(CMHDZ_ENEMY_TYPES eType)
	CMHDZ_SPRITE eSprite
	
	SWITCH eType
		CASE CMHDZ_ENEMY_1
			eSprite = CMHDZ_SPRITE_GANG_01_ATTACKING
		BREAK
		CASE CMHDZ_ENEMY_2
			eSprite = CMHDZ_SPRITE_GANG_02_ATTACKING
		BREAK
		CASE CMHDZ_ENEMY_3
			eSprite = CMHDZ_SPRITE_GANG_03_ATTACKING
		BREAK
		CASE CMHDZ_ENEMY_4
			eSprite = CMHDZ_SPRITE_GANG_04_ATTACKING
		BREAK
		CASE CMHDZ_ENEMY_5
			eSprite = CMHDZ_SPRITE_GANG_05_ATTACKING
		BREAK
//		CASE CMHDZ_CART_1
//			eSprite = CMHDZ_SPRITE_CART1
//		BREAK
		
		CASE CMHDZ_THROWING_ENEMY_1
			eSprite = CMHDZ_SPRITE_GANG_01_THROWING_04
		BREAK
		CASE CMHDZ_THROWING_ENEMY_2
			eSprite = CMHDZ_SPRITE_GANG_02_THROWING_04
		BREAK
		CASE CMHDZ_THROWING_ENEMY_3
			eSprite = CMHDZ_SPRITE_GANG_03_THROWING_04
		BREAK
		CASE CMHDZ_THROWING_ENEMY_4
			eSprite = CMHDZ_SPRITE_GANG_04_THROWING_04
		BREAK
		CASE CMHDZ_THROWING_ENEMY_5
			eSprite = CMHDZ_SPRITE_GANG_05_THROWING_04
		BREAK
		
		CASE CMHDZ_FOREGROUND_ENEMY_T1_1
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_A
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T1_2
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_01_B
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T2_1
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_A
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T2_2
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_02_B
		BREAK
		
		CASE CMHDZ_FOREGROUND_ENEMY_T3_1
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_03_A
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T3_2
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_03_B
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T4_1
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_A
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T4_2
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_04_B
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T5_1
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_A
		BREAK
		CASE CMHDZ_FOREGROUND_ENEMY_T5_2
			eSprite = CMHDZ_SPRITE_FOREGROUND_ENEMY_05_B
		BREAK

//		CASE CMHDZ_FOREGROUND_ENEMY_BEAR
//			eSprite = CMHDZ_SPRITE_LEVEL_03_FOREST_BEAR_01
//		BREAK
	ENDSWITCH
	
	RETURN eSprite
ENDFUNC

PROC CMHDZ_DRAW_DEBUG_LINE(VECTOR_2D pos1, VECTOR_2D pos2, RGBA_COLOUR_STRUCT rgba)
	pos1 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(pos1)
	pos2 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(pos2)
	
	DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(pos1, pos2, rgba)
ENDPROC

PROC CMHDZ_DRAW_DEBUG_BOX(VECTOR_2D vPoint, VECTOR_2D vSize, RGBA_COLOUR_STRUCT col)
	VECTOR_2D vMin, vMax 
	
	vMin.x = vPoint.x - (vSize.x / 2.0)
	vMin.y = vPoint.y - (vSize.y / 2.0)
	vMax.x = vPoint.x + (vSize.x / 2.0)
	vMax.y = vPoint.y + (vSize.y / 2.0)	
	
	CMHDZ_DRAW_DEBUG_LINE(INIT_VECTOR_2D(vMin.x, vMin.y), INIT_VECTOR_2D(vMax.x, vMin.y), col)
	CMHDZ_DRAW_DEBUG_LINE(INIT_VECTOR_2D(vMax.x, vMin.y), INIT_VECTOR_2D(vMax.x, vMax.y), col)
	CMHDZ_DRAW_DEBUG_LINE(INIT_VECTOR_2D(vMax.x, vMax.y), INIT_VECTOR_2D(vMin.x, vMax.y), col)
	CMHDZ_DRAW_DEBUG_LINE(INIT_VECTOR_2D(vMin.x, vMax.y), INIT_VECTOR_2D(vMin.x, vMin.y), col)
ENDPROC 
 
PROC CMHDZ_DRAW_DEBUG_RECT(VECTOR_2D vPoint, VECTOR_2D vSize, RGBA_COLOUR_STRUCT col)
	ARCADE_DRAW_PIXELSPACE_RECT(vPoint, vSize, col)
ENDPROC 

PROC CMHDZ_DRAW_DEBUG_LINE_MINIMAP(VECTOR_2D vPoint1, VECTOR_2D vPoint2, RGBA_COLOUR_STRUCT rgba, VECTOR_2D vMiniMapPos, VECTOR_2D vMiniMapSize)
	VECTOR_2D scaleFactor = INIT_VECTOR_2D(vMiniMapSize.x / cfBASE_SCREEN_WIDTH, vMiniMapSize.y / cfBASE_SCREEN_HEIGHT)
	VECTOR_2D pos1, pos2
	
	pos1.x = (vPoint1.x * scaleFactor.x) + vMiniMapPos.x
	pos1.y = (vPoint1.y * scaleFactor.y) + vMiniMapPos.y	
	
	pos2.x = (vPoint2.x * scaleFactor.x) + vMiniMapPos.x
	pos2.y = (vPoint2.y * scaleFactor.y) + vMiniMapPos.y
	
	pos1 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(pos1)
	pos2 = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(pos2)
	
	DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(pos1, pos2, rgba)
ENDPROC

PROC CMHDZ_DRAW_DEBUG_BOX_MINIMAP(VECTOR_2D vPoint, VECTOR_2D vSize, RGBA_COLOUR_STRUCT col, VECTOR_2D vMiniMapPos, VECTOR_2D vMiniMapSize)
	VECTOR_2D vMin, vMax 
	
	vMin.x = vPoint.x - (vSize.x / 2.0)
	vMin.y = vPoint.y - (vSize.y / 2.0)
	vMax.x = vPoint.x + (vSize.x / 2.0)
	vMax.y = vPoint.y + (vSize.y / 2.0)	
	
	CMHDZ_DRAW_DEBUG_LINE_MINIMAP(INIT_VECTOR_2D(vMin.x, vMin.y), INIT_VECTOR_2D(vMax.x, vMin.y), col, vMiniMapPos, vMiniMapSize)
	CMHDZ_DRAW_DEBUG_LINE_MINIMAP(INIT_VECTOR_2D(vMax.x, vMin.y), INIT_VECTOR_2D(vMax.x, vMax.y), col, vMiniMapPos, vMiniMapSize)
	CMHDZ_DRAW_DEBUG_LINE_MINIMAP(INIT_VECTOR_2D(vMax.x, vMax.y), INIT_VECTOR_2D(vMin.x, vMax.y), col, vMiniMapPos, vMiniMapSize)
	CMHDZ_DRAW_DEBUG_LINE_MINIMAP(INIT_VECTOR_2D(vMin.x, vMax.y), INIT_VECTOR_2D(vMin.x, vMin.y), col, vMiniMapPos, vMiniMapSize)
ENDPROC 

PROC CMHDZ_DRAW_DEBUG_RECT_MINIMAP(VECTOR_2D vPoint, VECTOR_2D vSize, RGBA_COLOUR_STRUCT col, VECTOR_2D vMiniMapPos, VECTOR_2D vMiniMapSize)
	VECTOR_2D pos, size
	VECTOR_2D scaleFactor = INIT_VECTOR_2D(vMiniMapSize.x / cfBASE_SCREEN_WIDTH, vMiniMapSize.y / cfBASE_SCREEN_HEIGHT)
	
	pos.x = (vPoint.x * scaleFactor.x) + vMiniMapPos.x
	pos.y = (vPoint.y * scaleFactor.y) + vMiniMapPos.y
	size.x = (vSize.x * scaleFactor.x)
	size.y = (vSize.y * scaleFactor.y)
	
	ARCADE_DRAW_PIXELSPACE_RECT(pos, size, col)
ENDPROC 

PROC CMHDZ_DRAW_DEBUG_SPRITE_MINIMAP(CMHDZ_SPRITE eSprite, VECTOR_2D vPoint, VECTOR_2D vSize, RGBA_COLOUR_STRUCT col, VECTOR_2D vMiniMapPos, VECTOR_2D vMiniMapSize)
	VECTOR_2D pos, size
	VECTOR_2D scaleFactor = INIT_VECTOR_2D(vMiniMapSize.x / cfBASE_SCREEN_WIDTH, vMiniMapSize.y / cfBASE_SCREEN_HEIGHT)
	
	pos.x = (vPoint.x * scaleFactor.x) + vMiniMapPos.x
	pos.y = (vPoint.y * scaleFactor.y) + vMiniMapPos.y
	size.x = (vSize.x * scaleFactor.x)
	size.y = (vSize.y * scaleFactor.y)

	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_SPRITE_GET_TEXTURE_DICT_NAME(eSprite), CMHDZ_SPRITE_GET_TEXTURE_NAME(eSprite), pos, size, 0, col)
ENDPROC

//-------------------------------------------------
// EDITOR DRAWING
//-------------------------------------------------
PROC CMHDZ_EDITOR_DRAW_ENEMIES(CMHDZ_EDITOR &w)
	IF (w.iSelectedData = -1) 
	OR (w.iSelectedData >= ciCMHDZ_MAX_ENEMIES)
	OR (w.bPlayStage)
	OR (w.iEditMode != CMHDZ_EDITOR_MODE_ENEMY)
		EXIT 
	ENDIF 

	FLOAT fScale
	VECTOR_2D pos, vSize 
	CMHDZ_SPRITE spr 
	RGBA_COLOUR_STRUCT transColor = sCMHDZData.rgbaWhite
		
	// draw selection overlays
	transColor.iA = 192
	
	CMHDZ_DRAW_DEBUG_BOX(pos, vSize, sCMHDZData.rgbaWhite)
	
	//IF (w.sEnemyMapData[w.iSelectedData].fPointToAppear != w.iActiveLevel)
		//transColor.iA = 128
	//ENDIF
	
	pos = w.sEnemyMapData[w.iSelectedData].vSpritePos
	fScale = CMHDZ_GET_ENEMY_TYPE_SCALE_FOR_LANE(w.sEnemyMapData[w.iSelectedData].iLane) 
	vSize = MULTIPLY_VECTOR_2D(CMHDZ_GET_ENEMY_SIZE_FOR_TYPE(w.sEnemyMapData[w.iSelectedData].eEnemyType, w.sEnemyMapData[w.iSelectedData].iLane != ciCMHDZ_LANE_FOREGROUND), fScale)	
	spr = CMHDZ_DEBUG_GET_SPRITE_FOR_ENEMY_TYPE(w.sEnemyMapData[w.iSelectedData].eEnemyType)
	
	IF (spr != CMHDZ_SPRITE_NONE)
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_SPRITE_GET_TEXTURE_DICT_NAME(spr), CMHDZ_SPRITE_GET_TEXTURE_NAME(spr), pos, vSize, 0, transColor)
		
		pos.x = w.sEnemyMapData[w.iSelectedData].fStoppingPos
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(w.sEnemyMapData[w.iSelectedData].eDataBits, CMHDZ_ENEMY_DATA_BITS_IS_POPUP)
			pos.x = w.sEnemyMapData[w.iSelectedData].vSpritePos.x
			pos.y = w.sEnemyMapData[w.iSelectedData].fStoppingPos
		ENDIF
		
		INIT_RGBA_STRUCT(transColor, 255, 0, 0, transColor.iA)
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_SPRITE_GET_TEXTURE_DICT_NAME(spr), CMHDZ_SPRITE_GET_TEXTURE_NAME(spr), pos, vSize, 0, transColor)
		CMHDZ_DRAW_DEBUG_LINE(pos, w.sEnemyMapData[w.iSelectedData].vSpritePos, transColor)
	ENDIF

ENDPROC

PROC CMHDZ_EDITOR_DRAW_BOSS(CMHDZ_EDITOR &w, INT iLane)

	IF (w.iEditMode != CMHDZ_EDITOR_MODE_BOSS)
		EXIT 
	ENDIF 
	
	// Including for damage effects
	CMHDZ_DRAW_SHOT_DISTORTION()
		
	CMHDZ_SPRITE eSprite
	CMHDZ_BOSS_STATE_UPDATE(sCMHDZData.sBossData, eSprite)
	//CMHDZ_DRAW_BOSS_SPRITE_AND_EFFECTS(eSprite, sCMHDZData.sEnemyProjectiles)
	CMHDZ_DRAW_BOSS(iLane)	
ENDPROC

FUNC BOOL CMHDZ_EDITOR_IS_COVER_OUT_OF_LIMITS(INT iCoverID, BOOL bToRight = FALSE)
	
	IF bToRight	
		RETURN sCMHDZData.sEditor.sCoverMapData[iCoverID].vSpritePos.x >= (cfRIGHT_SCREEN_LIMIT + cfLEFT_SCREEN_LIMIT)
	ENDIF
	
	RETURN sCMHDZData.sEditor.sCoverMapData[iCoverID].vSpritePos.x <= -(cfRIGHT_SCREEN_LIMIT + cfLEFT_SCREEN_LIMIT)
ENDFUNC

PROC CMHDZ_EDITOR_DRAW_COVER(INT iDistance, BOOL bIsSuperFarBackGround = FALSE)

	CMHDZ_SPRITE eSprite
	FLOAT fScale = 1.0
	VECTOR_2D vScale
	RGBA_COLOUR_STRUCT rgbaToUse
	//BOOL bRepositionWhenOffScreenForMovingLevel
	//BOOL bUpdatePosition
	
	INT i
	// Loop thrugh all cover
	REPEAT ciCMHDZ_MAX_COVER i
	
		IF iDistance = 2
		AND sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x <= (1920.0 + 200)
		AND sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x >= -200

			sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x = ((sCMHDZData.sEditor.sCoverMapData[i].fActiveAtPoint - sCMHDZData.fOverallLevelProgression) * 12)
				
		ELSE // Distance = 0 or 1. Foreground and background?

			sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x = (sCMHDZData.sEditor.sCoverMapData[i].fActiveAtPoint - sCMHDZData.fOverallLevelProgression)

		ENDIF
			
		// Too far to Left
		IF CMHDZ_EDITOR_IS_COVER_OUT_OF_LIMITS(i, FALSE)
		AND sCMHDZData.sEditor.sCoverMapData[i].vSpriteInitialPos.x <= ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*4
			sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x += ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
			sCMHDZData.sEditor.sCoverMapData[i].fActiveAtPoint += ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
		ENDIF
	
		// Too far to Right
		IF CMHDZ_EDITOR_IS_COVER_OUT_OF_LIMITS(i, TRUE)
		AND sCMHDZData.sEditor.sCoverMapData[i].vSpriteInitialPos.x <= ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*4
			sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x -= ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
			sCMHDZData.sEditor.sCoverMapData[i].fActiveAtPoint -= ((cfRIGHT_SCREEN_LIMIT+cfRIGHT_SCREEN_LIMIT)/2.0)*3
		ENDIF
		
		IF NOT CMHDZ_DRAW_COVER_PROCESS_CHECK(i, iDistance, bIsSuperFarBackGround, TRUE)		
			RELOOP 
		ENDIF
		// draw the cover on the current screen and the next
		fScale = CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(sCMHDZData.sEditor.sCoverMapData[i].eType, iDistance)
		eSprite = CMHDZ_COVER_UTIL_GET_SPRITE_FOR_TYPE(sCMHDZData.sEditor.sCoverMapData[i].eType)
		vScale = CMHDZ_COVER_UTIL_GET_TYPE_SIZE(sCMHDZData.sEditor.sCoverMapData[i].eType)

		IF (eSprite = CMHDZ_SPRITE_NONE)
			RELOOP
		ENDIF	
				
		IF sCMHDZData.sEditor.iEditMode = CMHDZ_EDITOR_MODE_COVER
		AND i = sCMHDZData.sEditor.iSelectedData
			// Draw the selected dat in the center of the screen
			CMHDZ_DRAW_GAME_SPRITE(eSprite,  INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.y), INIT_VECTOR_2D(vScale.x*fScale, vScale.y*fScale), 0, sCMHDZData.rgbaSprite)			
			RELOOP
		ENDIF
		
		IF sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x <=	(1920 + 200)
		AND sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x >= (0.0 - 200)
		
			rgbaToUse = sCMHDZData.rgbaSprite
						
			// Cover that removes will be transparent
			IF CMHDZ_COVER_MAP_DATA_IS_BITMASK_SET(sCMHDZData.sEditor.sCoverMapData[i], CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN)
			AND sCMHDZData.fOverallLevelProgression >=  sCMHDZData.sEditor.sCoverMapData[i].vSpriteInitialPos.x
				rgbaToUse.iA /= 3
			ENDIF
			
//			IF sCMHDZData.iCurrentScreen + 3 >= CMHDZ_GET_FINAL_SCREEN_IN_CURRENT_LEVEL()
//				////CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] CMHDZ_EDITOR_DRAW_COVER - iCurrentScreen: ", sCMHDZData.iCurrentScreen)
//				rgbaToUse.iA /= 3
//			ENDIF
								
//			IF CMHDZ_COVER_DATA_IS_BITMASK_SET(i, CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN)
//				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_EDITOR_DRAW_COVER] CMHDZ_COVER_DATA_IS_BITMASK_SET - CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN: i = ", i)
//				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_EDITOR_DRAW_COVER] CMHDZ_COVER_DATA_IS_BITMASK_SET - CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN: sCMHDZData.sEditor.sCoverMapData[i].vSpriteInitialPos.x = ", sCMHDZData.sEditor.sCoverMapData[i].vSpriteInitialPos.x)
//				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_EDITOR_DRAW_COVER] CMHDZ_COVER_DATA_IS_BITMASK_SET - CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN: sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x = ", sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x)
//				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_EDITOR_DRAW_COVER] CMHDZ_COVER_DATA_IS_BITMASK_SET - CMHDZ_COVER_DATA_BITS_REMOVE_OFF_SCREEN: sCMHDZData.sEditor.sCoverMapData[i].fActiveAtPoint = ", sCMHDZData.sEditor.sCoverMapData[i].fActiveAtPoint)
//			ENDIF

			CMHDZ_DRAW_GAME_SPRITE(eSprite, INIT_VECTOR_2D(sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.x,  sCMHDZData.sEditor.sCoverMapData[i].vSpritePos.y), INIT_VECTOR_2D(vScale.x*fScale, vScale.y*fScale), 0, rgbaToUse)		
						
		ENDIF
				
	ENDREPEAT 
	
//	IF iDistance = ciCMHDZ_LANE_FOREGROUND
//	
//		IF sCMHDZData.sEditor.iEditMode != CMHDZ_EDITOR_MODE_COVER
//			EXIT
//		ENDIF
//	
//		VECTOR_2D pos, vSize 
//		CMHDZ_SPRITE spr 
//		RGBA_COLOUR_STRUCT transColor = sCMHDZData.rgbaWhite
//		
//		transColor.iA = 125
//		fScale = 1.0
//		//IF sCMHDZData.fOverallLevelProgression >= (w.sEnemyMapData[w.iSelectedData].fPointToAppear - 720.0)
//		
//		i= 0
//		FOR i = 0 TO ciCMHDZ_MAX_ENEMIES  -1
//		
//			IF sCMHDZData.sEditor.sEnemyMapData[i].eEnemyType = CMHDZ_ENEMY_INVALID
//				RELOOP
//			ENDIF
//		
//			pos = sCMHDZData.sEditor.sEnemyMapData[i].vSpritePos
//			
////			IF  sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear + 1355 >= (sCMHDZData.fOverallLevelProgression - 1355.0)
////			AND sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear + 1355 <= (sCMHDZData.fOverallLevelProgression + 1355.0)
////				//pos.x = (sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear - pos.x)
////				pos.x -= sCMHDZData.fOverallLevelProgression
//
////			IF i = 0
////				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_ENEMIES - Enemy Index relooped: ", i)
////				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_ENEMIES - Point to appear: ", sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear)
////				CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_ENEMIES - X coord to draw: ", pos.x)
////			ENDIF
//
////			IF sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear < (sCMHDZData.fOverallLevelProgression - 400.0)
////			OR sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear > (sCMHDZData.fOverallLevelProgression  + 400)
////				RELOOP
////			ENDIF
//
//			IF CMHDZ_ENEMY_DATA_IS_BITMASK_SET(i, CMHDZ_ENEMY_DATA_BITS_REVERSE)
//				//pos.x += ((sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear - sCMHDZData.fOverallLevelProgression) - sCMHDZData.sEditor.sEnemyMapData[i].fStoppingPos)
//				//pos.x += ((sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear) - sCMHDZData.sEditor.sEnemyMapData[i].fStoppingPos)
//				pos.x -= (sCMHDZData.fOverallLevelProgression - (sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear))
//				pos.x += (sCMHDZData.sEditor.sEnemyMapData[i].fStoppingPos - sCMHDZData.sEditor.sEnemyMapData[i].vSpritePos.x)
//				
//			ELSE
//				pos.x -= sCMHDZData.fOverallLevelProgression
//				pos.x -= ((sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear) - sCMHDZData.sEditor.sEnemyMapData[i].fStoppingPos)
//				pos.x -= (sCMHDZData.sEditor.sEnemyMapData[i].vSpritePos.x - sCMHDZData.sEditor.sEnemyMapData[i].fStoppingPos)
//				//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_COVER - Space difference from right: ",(sCMHDZData.sEditor.sEnemyMapData[i].vSpritePos.x - sCMHDZData.sEditor.sEnemyMapData[i].fStoppingPos))
//			ENDIF
//							
//			//pos.x += 80.0
//		
//			//pos.x = (sCMHDZData.sEditor.sEnemyMapData[sCMHDZData.sEditor.iSelectedData].fPointToAppear - sCMHDZData.fOverallLevelProgression)
//			IF pos.x < cfLEFT_SCREEN_LIMIT
//			OR pos.x > cfRIGHT_SCREEN_LIMIT
//				RELOOP
//			ENDIF
//			
//			//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_ENEMIES - Enemy Index to draw: ", i)
//			//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_ENEMIES - Point to appear: ", sCMHDZData.sEditor.sEnemyMapData[i].fPointToAppear)		
//
//			fScale = CMHDZ_GET_ENEMY_TYPE_SCALE_FOR_LANE(sCMHDZData.sEditor.sEnemyMapData[i].iLane) 
//			vSize = MULTIPLY_VECTOR_2D(CMHDZ_GET_ENEMY_SIZE_FOR_TYPE(sCMHDZData.sEditor.sEnemyMapData[i].eEnemyType, sCMHDZData.sEditor.sEnemyMapData[i].iLane != ciCMHDZ_LANE_FOREGROUND), fScale)	
//			spr = CMHDZ_DEBUG_GET_SPRITE_FOR_ENEMY_TYPE(sCMHDZData.sEditor.sEnemyMapData[i].eEnemyType)
//			
//			IF CMHDZ_ENEMY_DATA_IS_BITMASK_SET(i, CMHDZ_ENEMY_DATA_BITS_IS_POPUP)
//				INIT_RGBA_STRUCT(transColor, 255, 100, 100, transColor.iA)
//			ENDIF
//			
//			//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ][BAZ] CMHDZ_EDITOR_DRAW_ENEMIES - Enemy index drawn: ", i)
//
//			CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_SPRITE_GET_TEXTURE_DICT_NAME(spr), CMHDZ_SPRITE_GET_TEXTURE_NAME(spr), pos, vSize, 0, transColor)
//		ENDFOR
//		
//		EXIT
//	ENDIF
ENDPROC

PROC CMHDZ_EDITOR_DRAW_ITEMS(INT iDistance)
	
	IF sCMHDZData.sEditor.iEditMode != CMHDZ_EDITOR_MODE_ITEM
		EXIT
	ENDIF
		
	CMHDZ_SPRITE eSprite
	FLOAT fScale = 1.0

	SWITCH iDistance
	CASE 0
		fScale = 0.75
	BREAK
	CASE 2
		//fScale = 2.5
	BREAK
	ENDSWITCH
	
//	IF iDistance = 2
//		CMHDZ_DRAW_GAME_SPRITE("ammo1",  GET_DEBUG_TEXTURE_POS(0), INIT_VECTOR_2D(cfCMHDZ_SPRITE_HUD_PISTOL_AMMO_CLIP_WIDTH*1.0, cfCMHDZ_SPRITE_HUD_PISTOL_AMMO_CLIP_HEIGHT*1.0), 0, sCMHDZData.rgbaSprite)
//		CMHDZ_DRAW_GAME_SPRITE("POWERUP_PAINKILLERS",  GET_DEBUG_TEXTURE_POS(1), INIT_VECTOR_2D(cfCMHDZ_ITEM_HEALTH_WIDTH*1.0, cfCMHDZ_ITEM_HEALTH_HEIGHT*1.0), 0, sCMHDZData.rgbaSprite)
//		CMHDZ_DRAW_GAME_SPRITE("ammo4",  GET_DEBUG_TEXTURE_POS(2), INIT_VECTOR_2D(cfCMHDZ_ITEM_HEALTH_WIDTH*1.0, cfCMHDZ_ITEM_HEALTH_HEIGHT*1.0), 0, sCMHDZData.rgbaSprite)
//	ENDIF
	
//	INT i
//	FOR i = 0 TO ciCMHDZ_MAX_ITEMS -1
	
		// Don't draw other items if in Editor mode. Only selected one
	INT i = sCMHDZData.sEditor.iSelectedData	

	IF i >= 0
	AND i < ciCMHDZ_MAX_ITEMS
		eSprite = CMHDZ_GET_SPRITE_FOR_ITEM_TYPE(sCMHDZData.sEditor.sItemMapData[i].eItemType)
		CMHDZ_DRAW_GAME_SPRITE(eSprite, INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0), sCMHDZData.sEditor.sItemMapData[i].vSpritePos.y), MULTIPLY_VECTOR_2D(sCMHDZData.sItemData[i].vSize, fScale), 0, sCMHDZData.rgbaSprite)
	ENDIF
		
ENDPROC

PROC CMHDZ_EDITOR_DRAW_COVER_MAP_DATA_ON_MINIMAP(CMHDZ_EDITOR &w, CMHDZ_COVER_MAP_DATA &array[])
	INT i
	VECTOR_2D vSize
	FLOAT fScale 
	RGBA_COLOUR_STRUCT rcol
	CMHDZ_SPRITE eSprite 
	
	INIT_RGBA_STRUCT(rcol, 0, 255, 0, 255)
	
	REPEAT COUNT_OF(array) i
		IF (array[i].eType != CMHDZ_ASSET_INVALID)
			eSprite = CMHDZ_COVER_UTIL_GET_SPRITE_FOR_TYPE(array[i].eType)
			IF (eSprite = CMHDZ_SPRITE_NONE)
				RELOOP
			ENDIF
			
			fScale = CMHDZ_COVER_UTIL_GET_TYPE_SCALE_FOR_LANE(array[i].eType, array[i].iCoverLane)
			vSize = MULTIPLY_VECTOR_2D(CMHDZ_COVER_UTIL_GET_TYPE_SIZE(array[i].eType), fScale)
			
			IF (i = w.iSelectedData) AND (w.iEditMode = CMHDZ_EDITOR_MODE_COVER)
				CMHDZ_DRAW_DEBUG_RECT_MINIMAP(array[i].vSpriteInitialPos, vSize, sCMHDZData.rgbaRed, w.vMiniMapPos, w.vMiniMapSize)
				rcol = sCMHDZData.rgbaWhite
			ELSE 
				INIT_RGBA_STRUCT(rcol, 200, 200, 200, 255)
			ENDIF
			
//			IF ((CEIL(sCMHDZData.fOverallLevelProgression/((cfRIGHT_SCREEN_LIMIT + cfLEFT_SCREEN_LIMIT)/2))) != 0) AND ((CEIL(sCMHDZData.fOverallLevelProgression/((cfRIGHT_SCREEN_LIMIT + cfLEFT_SCREEN_LIMIT)/2))) != array[i].iScreen)
//				INIT_RGBA_STRUCT(rcol, 200, 200, 200, 64)
//			ENDIF
			
			CMHDZ_DRAW_DEBUG_SPRITE_MINIMAP(eSprite, array[i].vSpriteInitialPos, vSize, rcol, w.vMiniMapPos, w.vMiniMapSize)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CMHDZ_EDITOR_DRAW_ITEM_MAP_DATA_ON_MINIMAP(CMHDZ_EDITOR &w, CMHDZ_ITEM_MAP_DATA &array[])
	INT i
	VECTOR_2D vSize
	RGBA_COLOUR_STRUCT rcol
	CMHDZ_SPRITE eSprite 
	
	INIT_RGBA_STRUCT(rcol, 0, 255, 0, 255)
	
	REPEAT COUNT_OF(array) i
		IF (array[i].eItemType = CMHDZ_ITEM_NONE)
			RELOOP
		ENDIF
			
		eSprite = CMHDZ_GET_SPRITE_FOR_ITEM_TYPE(array[i].eItemType)
		IF (eSprite = CMHDZ_SPRITE_NONE)
			RELOOP
		ENDIF
			
		vSize = CMHDZ_GET_ITEM_SIZE_FOR_TYPE(array[i].eItemType)
		
		IF (i = w.iSelectedData) AND (w.iEditMode = CMHDZ_EDITOR_MODE_ITEM)
			CMHDZ_DRAW_DEBUG_RECT_MINIMAP(array[i].vSpritePos, vSize, sCMHDZData.rgbaRed, w.vMiniMapPos, w.vMiniMapSize)
			rcol = sCMHDZData.rgbaWhite
		ELSE 
			INIT_RGBA_STRUCT(rcol, 200, 200, 200, 255)
		ENDIF
		
		CMHDZ_DRAW_DEBUG_SPRITE_MINIMAP(eSprite, array[i].vSpritePos, vSize, rcol, w.vMiniMapPos, w.vMiniMapSize)
	ENDREPEAT
ENDPROC

PROC CMHDZ_EDITOR_DRAW_ENEMY_MAP_DATA_ON_MINIMAP(CMHDZ_EDITOR &w, CMHDZ_ENEMY_MAP_DATA &array[])
	INT i
	VECTOR_2D vSize
	FLOAT fScale 
	RGBA_COLOUR_STRUCT rcol
	CMHDZ_SPRITE eSprite 
	VECTOR_2D vPos 
	
	INIT_RGBA_STRUCT(rcol, 0, 255, 0, 255)
	
	REPEAT COUNT_OF(array) i
		IF (array[i].eEnemyType != CMHDZ_ENEMY_INVALID)
			eSprite = CMHDZ_DEBUG_GET_SPRITE_FOR_ENEMY_TYPE(array[i].eEnemyType)
			IF (eSprite = CMHDZ_SPRITE_NONE)
				RELOOP
			ENDIF
		
			fScale = CMHDZ_GET_ENEMY_TYPE_SCALE_FOR_LANE(array[i].iLane) 
			vSize = MULTIPLY_VECTOR_2D(CMHDZ_GET_ENEMY_SIZE_FOR_TYPE(array[i].eEnemyType, array[i].iLane != ciCMHDZ_LANE_FOREGROUND), fScale)	
			vPos = array[i].vSpritePos
			
			IF (i = w.iSelectedData) AND (w.iEditMode = CMHDZ_EDITOR_MODE_ENEMY)
				CMHDZ_DRAW_DEBUG_RECT_MINIMAP(vPos, vSize, sCMHDZData.rgbaRed, w.vMiniMapPos, w.vMiniMapSize)
				rcol = sCMHDZData.rgbaWhite
			ELSE 
				INIT_RGBA_STRUCT(rcol, 200, 200, 200, 255)
			ENDIF
			
			CMHDZ_DRAW_DEBUG_SPRITE_MINIMAP(eSprite, vPos, vSize, rcol, w.vMiniMapPos, w.vMiniMapSize)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CMHDZ_EDITOR_DRAW_MINIMAP(CMHDZ_EDITOR &w)
	IF (!w.bShowMiniMap) OR (w.bPlayStage)
		EXIT 
	ENDIF
	
	// Do not draw the Minimap over the Editor if we are still on the title screen or selection menu.
	IF (sCMHDZData.eClientState = CMHDZ_ARCADE_CLIENT_STATE_TITLE_SCREEN)
		EXIT
	ENDIF

	IF (sCMHDZData.eClientState = CMHDZ_ARCADE_CLIENT_STATE_LEVEL_SELECTION_MENU)
		EXIT
	ENDIF

	VECTOR_2D v, origin
	RGBA_COLOUR_STRUCT rcol
	
	// draw bg
	INIT_RGBA_STRUCT(rcol, 0, 0, 0, 200)
	v = CMHDZ_GET_CENTER_FROM_CORNER_RECT(w.vMiniMapPos.x, w.vMiniMapPos.y, w.vMiniMapSize.x, w.vMiniMapSize.y)
	ARCADE_DRAW_PIXELSPACE_RECT(v, INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, w.vMiniMapSize.y), rcol)
	
	// Screens aren't actually screens - they are half screens?!?
	IF (CEIL(sCMHDZData.fOverallLevelProgression/(cfCMHDZ_SCREEN_LENGTH)) > 0)
	
		// draw actual screen boundary
		INIT_RGBA_STRUCT(rcol, 255, 255, 255, 90)
		origin = CMHDZ_GET_ORIGIN_FOR_SCREEN(ROUND(sCMHDZData.fOverallLevelProgression/cfCMHDZ_SCREEN_LENGTH))
		v = CMHDZ_GET_CENTER_FROM_CORNER_RECT(origin.x, origin.y, (cfBASE_SCREEN_WIDTH / 2.0), cfBASE_SCREEN_HEIGHT)
		CMHDZ_DRAW_DEBUG_BOX_MINIMAP(v, INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH / 2.0), cfBASE_SCREEN_HEIGHT), rcol, w.vMiniMapPos, w.vMiniMapSize)
		
		// draw viewable screen
		INIT_RGBA_STRUCT(rcol, 0, 0, 255, 64)
		v = CMHDZ_GET_CENTER_FROM_CORNER_RECT(origin.x + cfGAME_OVERSCAN_MIN_X, cfGAME_OVERSCAN_MIN_Y, cfGAME_OVERSCAN_WIDTH, cfGAME_OVERSCAN_HEIGHT)
		CMHDZ_DRAW_DEBUG_RECT_MINIMAP(v, INIT_VECTOR_2D(cfGAME_OVERSCAN_WIDTH, cfGAME_OVERSCAN_HEIGHT), rcol, w.vMiniMapPos, w.vMiniMapSize)	
	ENDIF
	
	// draw map things
	CMHDZ_EDITOR_DRAW_COVER_MAP_DATA_ON_MINIMAP(w, w.sCoverMapData)
	CMHDZ_EDITOR_DRAW_ITEM_MAP_DATA_ON_MINIMAP(w, w.sItemMapData)
	CMHDZ_EDITOR_DRAW_ENEMY_MAP_DATA_ON_MINIMAP(w, w.sEnemyMapData)
//	CMHDZ_EDITOR_DRAW_HOSTAGE_MAP_DATA_ON_MINIMAP(w, w.sHostageMapData)
ENDPROC

PROC CMHDZ_EDITOR_DRAW_ENTITY_INDICES(CMHDZ_EDITOR &w)
	INT i
	VECTOR_2D vScreenCoord
	
	IF (w.bShowEntityIndicies[CMHDZ_EDITOR_MODE_ENEMY])
		REPEAT ciCMHDZ_MAX_ENEMIES i
			IF (sCMHDZData.sEnemyData[i].eState != CMHDZ_ENEMY_STATE_INACTIVE) 
			AND (sCMHDZData.sEnemyData[i].eState != CMHDZ_ENEMY_STATE_WAITING)
				IF sCMHDZData.sEnemyData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH AND sCMHDZData.sEnemyData[i].vSpritePos.x > 0
					vScreenCoord = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(sCMHDZData.sEnemyData[i].vSpritePos)
					DISPLAY_TEXT_WITH_NUMBER(vScreenCoord.x, vScreenCoord.y, "NUMBER", i)
				ENDIF		
			ENDIF
		ENDREPEAT
	ENDIF 
	
//	IF (w.bShowEntityIndicies[CMHDZ_EDITOR_MODE_HOSTAGE])
//		REPEAT ciCMHDZ_MAX_HOSTAGES i
//			IF sCMHDZData.sHostageData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH AND sCMHDZData.sHostageData[i].vSpritePos.x > 0
//				IF (sCMHDZData.sHostageData[i].bRendering)
//					vScreenCoord = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(sCMHDZData.sHostageData[i].vSpritePos)
//					DISPLAY_TEXT_WITH_NUMBER(vScreenCoord.x, vScreenCoord.y, "NUMBER", i)
//				ENDIF
//			ENDIF		
//		ENDREPEAT
//	ENDIF 
	
	IF (w.bShowEntityIndicies[CMHDZ_EDITOR_MODE_ITEM])
		REPEAT ciCMHDZ_MAX_ITEMS i
			IF sCMHDZData.sItemData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH AND sCMHDZData.sItemData[i].vSpritePos.x > 0
				vScreenCoord = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(sCMHDZData.sItemData[i].vSpritePos)
				DISPLAY_TEXT_WITH_NUMBER(vScreenCoord.x, vScreenCoord.y, "NUMBER", i)
			ENDIF	
		ENDREPEAT
	ENDIF 
	
	IF (w.bShowEntityIndicies[CMHDZ_EDITOR_MODE_COVER])
		REPEAT ciCMHDZ_MAX_COVER i
			IF sCMHDZData.sCoverData[i].vSpritePos.x < cfBASE_SCREEN_WIDTH AND sCMHDZData.sCoverData[i].vSpritePos.x > 0
				vScreenCoord = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(sCMHDZData.sCoverData[i].vSpritePos)
				DISPLAY_TEXT_WITH_NUMBER(vScreenCoord.x, vScreenCoord.y, "NUMBER", i)
			ENDIF		
		ENDREPEAT
	ENDIF 
ENDPROC 

PROC CMHDZ_DRAW_CLIENT_STATE_EDITOR_ACTIVE()
	// Draw black and grey background
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), sCMHDZData.rgbaBlack)
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), INIT_VECTOR_2D(cfGAME_SCREEN_WIDTH, cfGAME_SCREEN_HEIGHT), sCMHDZData.rgbaRed)

	CMHDZ_DRAW_BACKGROUND()
	CMHDZ_EDITOR_DRAW_COVER(ciCMHDZ_LANE_BACKGROUND, TRUE)
	CMHDZ_EDITOR_DRAW_BOSS(sCMHDZData.sEditor, ciCMHDZ_LANE_BACKGROUND)
	CMHDZ_EDITOR_DRAW_ITEMS(ciCMHDZ_LANE_BACKGROUND)	
	CMHDZ_EDITOR_DRAW_COVER(ciCMHDZ_LANE_BACKGROUND, FALSE)
	
	CMHDZ_EDITOR_DRAW_BOSS(sCMHDZData.sEditor, ciCMHDZ_LANE_MIDDLE)
	CMHDZ_EDITOR_DRAW_ITEMS(ciCMHDZ_LANE_MIDDLE)
	CMHDZ_EDITOR_DRAW_COVER(ciCMHDZ_LANE_MIDDLE)	
		
	CMHDZ_EDITOR_DRAW_BOSS(sCMHDZData.sEditor, ciCMHDZ_LANE_FOREGROUND)
	CMHDZ_EDITOR_DRAW_ITEMS(ciCMHDZ_LANE_FOREGROUND)	
	CMHDZ_EDITOR_DRAW_COVER(ciCMHDZ_LANE_FOREGROUND)	
	
	CMHDZ_EDITOR_DRAW_ENEMIES(sCMHDZData.sEditor)	
	
	//CMHDZ_EDITOR_DRAW_BOSS(sCMHDZData.sEditor)
	
	#IF IS_DEBUG_BUILD
		#IF CMHDZ_CMDLINE_EDITOR_NEW
			IF (sCMHDZData.sEditor.bPlayStage)
				CMHDZ_DRAW_HUD()
			ENDIF
		#ENDIF 
	#ENDIF 
	CMHDZ_DRAW_RETICLES(TRUE, TRUE)
	TWS_DRAW_FRONT_FX()
ENDPROC
#ENDIF
#ENDIF

//-------------------------------------------------
// LEADERBOARD FUNCTIONS
//-------------------------------------------------

PROC  ARCADE_GAMES_DRAW_INTEGER(INT iValue, INT iDigits, VECTOR_2D vCenter,RGBA_COLOUR_STRUCT rgba)
	VECTOR_2D vCharCenter = vCenter
	VECTOR_2D vCharSize = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SIZE()
	VECTOR_2D vCharSeparation = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SEPARATION()
	BOOL bZero = (iValue = 0)
	INT iChar
	
	IF iDigits = 0
		INT currentValue
		currentValue = iValue
		WHILE currentValue > 0 OR bZero
			bZero = FALSE
			vCharCenter.x = vCenter.x + vCharSeparation.x * (iDigits-1)/2.0 - vCharSeparation.x * iChar
			TEXT_LABEL_63 tlTextureName
			tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_NUMBER()
			tlTextureName += currentValue % 10
			currentValue /= 10
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, rgba)
			iChar++
	  ENDWHILE
	ELSE
		REPEAT iDigits iChar
			vCharCenter.x = vCenter.x + vCharSeparation.x * (iDigits-1)/2.0 - vCharSeparation.x * iChar
			TEXT_LABEL_63 tlTextureName
			tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_NUMBER()
			tlTextureName += (iValue / ROUND(POW(10.0,to_float(iChar)))) % 10
			CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, rgba)
		ENDREPEAT
	ENDIF
ENDPROC

PROC ARCADE_GAMES_LEADERBOARD_DRAW_UNPACKED_INITIALS(ARCADE_GAMES_LEADERBOARD_DATA &lbData, INT &iUnpackedInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS], VECTOR_2D vCenter,RGBA_COLOUR_STRUCT rgba,RGBA_COLOUR_STRUCT rgbaHighlight, BOOL editing)
	VECTOR_2D vCharCenter = vCenter
	VECTOR_2D vCharSize = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SIZE()
	VECTOR_2D vCharSeparation = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SEPARATION()

	RGBA_COLOUR_STRUCT color
	
	INT iChar

	REPEAT 3 iChar
		vCharCenter.x = vCenter.x + vCharSeparation.x * (3-1)/2.0 + vCharSeparation.x * iChar
		TEXT_LABEL_63 tlTextureName
		IF iUnpackedInitials[iChar] < 26
			tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_LETTER()

		ELSE
			tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_NUMBER()

		ENDIF
		tlTextureName += ARCADE_GET_CHAR_FROM_INT(iUnpackedInitials[iChar])
		IF editing 
			IF  lbData.iCurrentInitial = iChar
				IF lbData.iTickTimer = 0
					color = rgba
				ELSE
					color = rgbaHighlight
				ENDIF
			ELSE
				color = rgbaHighlight
			ENDIF
		ELSE
			color = rgba
		ENDIF

		CMHDZ_DRAW_GAME_SPRITE_DETAILS("MPCamhedzVHSFont",tlTextureName,vCharCenter,vCharSize,0.0, color)
	ENDREPEAT
ENDPROC

PROC ARCADE_GAMES_LEADERBOARD_DRAW_RANK(INT iPosition, VECTOR_2D vCenter,RGBA_COLOUR_STRUCT rgba,RGBA_COLOUR_STRUCT rgbaHighlight, BOOL editing)
//	//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW_RANK][LEADERBOARD] - ARCADE_GAMES_LEADERBOARD_DRAW_RANK ")
	VECTOR_2D vCharCenter = vCenter
	VECTOR_2D vCharSize = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SIZE()
	VECTOR_2D vCharSeparation = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SEPARATION()
	RGBA_COLOUR_STRUCT color
	IF editing 
		color = rgbaHighlight
	ELSE
		color = rgba
	ENDIF
	
	TEXT_LABEL_63 tlTextureName
	IF iPosition = 10
		tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_NUMBER()
		tlTextureName += 1
		vCharCenter.x = vCharCenter.x - vCharSeparation.x
		CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)
		vCharCenter = vCenter
		iPosition = 0
	ENDIF
	tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_NUMBER()
	tlTextureName += iPosition
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)

	vCharCenter.x = vCenter.x + vCharSeparation.x * 1
	tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_LETTER()
	IF iPosition = 1
		tlTextureName += "S"
	ELIF iPosition = 2
		tlTextureName += "N"
	ELIF iPosition = 3
		tlTextureName += "R"
	ELSE
		tlTextureName += "T"
	ENDIF
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)

	vCharCenter.x = vCenter.x + vCharSeparation.x * 2
	tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_LETTER()
	IF iPosition = 1
		tlTextureName += "T"
	ELIF iPosition = 2
		tlTextureName += "D"
	ELIF iPosition = 3
		tlTextureName += "D"
	ELSE
		tlTextureName += "H"
	ENDIF
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)
ENDPROC

PROC ARCADE_GAMES_LEADERBOARD_DRAW_SCORE(INT iScore, VECTOR_2D vCenter,RGBA_COLOUR_STRUCT rgba,RGBA_COLOUR_STRUCT rgbaHighlight, BOOL editing)
	VECTOR_2D vCharCenter = vCenter
	VECTOR_2D vCharSize = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SIZE()
	VECTOR_2D vCharSeparation = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SEPARATION()
	RGBA_COLOUR_STRUCT color
	IF editing 
		color = rgbaHighlight
	ELSE
		color = rgba
	ENDIF
	
	ARCADE_GAMES_DRAW_INTEGER(iScore,ARCADE_GAMES_LEADERBOARD_GET_SCORE_MAX_CHARS(),vCenter,color)
	//PTS
	TEXT_LABEL_63 tlTextureName
	vCharCenter.x = vCenter.x + vCharSeparation.x * (ARCADE_GAMES_LEADERBOARD_GET_SCORE_MAX_CHARS()/2 + 1)
	tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_LETTER()
	tlTextureName += "P"
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)
	vCharCenter.x = vCenter.x + vCharSeparation.x * (ARCADE_GAMES_LEADERBOARD_GET_SCORE_MAX_CHARS()/2 + 2)
	tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_LETTER()
	tlTextureName += "T"
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)
	vCharCenter.x = vCenter.x + vCharSeparation.x * (ARCADE_GAMES_LEADERBOARD_GET_SCORE_MAX_CHARS()/2 + 3)
	tlTextureName = ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_LETTER()
	tlTextureName += "S"
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(ARCADE_GAMES_LEADERBOARD_GET_TEXTURE_DICTIONARY(),tlTextureName,vCharCenter,vCharSize,0.0, color)
ENDPROC

/// PURPOSE:
///    Draws the leaderboard
PROC ARCADE_GAMES_LEADERBOARD_DRAW(ARCADE_GAMES_LEADERBOARD_DATA &lbData, INT iLocalInitials, ARCADE_GAMES_LEADERBOARD_STRUCT &lbHost[ciCASINO_ARCADE_LEADERBOARD_POSITIONS], BOOL bIsPlayerTwo = FALSE)
//	//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] - ARCADE_GAMES_LEADERBOARD_DRAW ")

	TEXT_LABEL_63 tl63RewindFrame
	sCMHDZData.iTimer += CEIL(0.0+@1000)
	
	IF sCMHDZData.eCurrentLevel >= CMHDZ_MAX_LEVELS
	OR sCMHDZData.eCurrentLevel < CMHDZ_LEVEL_CITY
		sCMHDZData.eCurrentLevel = CMHDZ_LEVEL_CITY
	ENDIF
	
	CMHDZ_REWIND_GET_REWIND_ANIMATION(tl63RewindFrame)
	
	RGBA_COLOUR_STRUCT rgbaRandomisedColour
	
	INIT_RGBA_STRUCT(rgbaRandomisedColour,
		GET_RANDOM_INT_IN_RANGE(240,255),
		GET_RANDOM_INT_IN_RANGE(240,255),
		GET_RANDOM_INT_IN_RANGE(240,255))
	
	
	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICTIONARY_FOR_LEVEL(sCMHDZData.eCurrentLevel),
						tl63RewindFrame,
						INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0) + sCMHDZData.fSyncOffsetX, cfBASE_SCREEN_HEIGHT/2.0),
						INIT_VECTOR_2D(2048, 944),
						0.0,
						rgbaRandomisedColour)

	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICTIONARY_FOR_LEVEL(sCMHDZData.eCurrentLevel),
						tl63RewindFrame,
						INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0 + (sCMHDZData.fSyncOffsetX - 2048), cfBASE_SCREEN_HEIGHT/2.0),
						INIT_VECTOR_2D(2048, 944),
						0.0,
						rgbaRandomisedColour)

	CMHDZ_DRAW_HEAVY_DAMAGE_EFFECT()
	//CMHDZ_DRAW_REWIND_EFFECT()
	
	// Blink rewind
//	IF (sCMHDZData.iTimer%2000) < 1000
//		// Rewind symbol appears on screen
//		CMHDZ_DRAW_REWIND_SYMBOL()
//	ENDIF
	
	//CMHDZ_DRAW_DAMAGE_NOISE(100)
	
	sCMHDZData.fSyncOffsetX += (0.0+@175)
	
	IF (sCMHDZData.fSyncOffsetX >= 2048)
		sCMHDZData.fSyncOffsetX = 0.0
	ENDIF
	
	// Darken background
	RGBA_COLOUR_STRUCT col 
	INIT_RGBA_STRUCT(col, 0,0,0,125)
	
	ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0),
					INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH,cfBASE_SCREEN_HEIGHT),
					col)


	IF lbData.bEditing
		ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_LEADERBOARD_ENTRY)
	ELSE
		ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_LEADERBOARD)
	ENDIF

	VECTOR_2D vCharSeparation = ARCADE_GAMES_LEADERBOARD_GET_CHAR_TEXTURE_SEPARATION()
	lbData.iTickTimer = ((lbData.iTickTimer + ROUND(GET_FRAME_TIME() * 1000))% 1000) / 500
	
//	CMHDZ_DRAW_GAME_SPRITE_DETAILS(CMHDZ_GET_TEXTURE_DICT_NAME(CMHDZ_TEXDICT_CH_TITLES),
//		ARCADE_GAMES_LEADERBOARD_GET_TITLE_TEXTURE_NAME(),
//		ARCADE_GAMES_LEADERBOARD_GET_TITLE_TEXTURE_POSITION(),
//		ARCADE_GAMES_LEADERBOARD_GET_TITLE_TEXTURE_SIZE(),0.0,
//		ARCADE_GAMES_LEADERBOARD_GET_DEFAULT_COLOR())
	CMHDZ_WRITE_STRING_USING_TEXT_SPRITES("LEADERBOARD", ARCADE_GAMES_LEADERBOARD_GET_TITLE_TEXTURE_POSITION(), DEFAULT, TRUE)
//	//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] My Initials: ", iLocalInitials)
	
	INT iUnpackedInitials[ciARCADE_CABINET_LEADERBOARD_INITIALS]
	INT iPositionLoop = 0
	BOOL bEditing = FALSE
	FOR iPositionLoop = 0 TO ciCASINO_ARCADE_LEADERBOARD_POSITIONS - 1
		
		bEditing = lbHost[iPositionLoop].iPlayer = PARTICIPANT_ID_TO_INT() AND lbData.bEditing
	//	bEditing = lbData.bEditing
	
		//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD]")
		//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] iPositionLoop: ",iPositionLoop, " bEditing: ", bEditing, "  lbData.bEditing: ",  lbData.bEditing, " Local part: ", PARTICIPANT_ID_TO_INT())
		//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] lbHost[",iPositionLoop, "].iPlayer = ",  lbHost[iPositionLoop].iPlayer)
		//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] lbHost[",iPositionLoop, "].iScore = ",  lbHost[iPositionLoop].iScore)
		//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] lbHost[",iPositionLoop, "].iInitials = ",  lbHost[iPositionLoop].iInitials)
		
		
		IF (lbData.iCurrentInitial < ciARCADE_CABINET_LEADERBOARD_INITIALS - 1	OR SC_PROFANITY_GET_CHECK_IS_VALID(lbData.iLbdInitialProfanityToken))
		AND bEditing
			//CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_GAMES_LEADERBOARD_DRAW][LEADERBOARD] Editing, iInitials: ", iLocalInitials)
			ARCADE_UNPACK_LEADERBOARD_INITIALS(iLocalInitials, iUnpackedInitials)
		ELSE
			ARCADE_UNPACK_LEADERBOARD_INITIALS(lbHost[iPositionLoop].iInitials, iUnpackedInitials)
		ENDIF
		
	//	ARCADE_UNPACK_LEADERBOARD_INITIALS(lbHost[iPositionLoop].iInitials, iUnpackedInitials)
		
		//Rank
		ARCADE_GAMES_LEADERBOARD_DRAW_RANK(iPositionLoop+1,ADD_VECTOR_2D(ARCADE_GAMES_LEADERBOARD_GET_RANK_COLUMN_POSITION(),INIT_VECTOR_2D(0.0,vCharSeparation.y*iPositionLoop)),ARCADE_GAMES_LEADERBOARD_GET_DEFAULT_COLOR(),ARCADE_GAMES_LEADERBOARD_GET_ENTRY_COLOR(bIsPlayerTwo),bEditing)

		//Initials
		ARCADE_GAMES_LEADERBOARD_DRAW_UNPACKED_INITIALS(lbData, iUnpackedInitials,ADD_VECTOR_2D(ARCADE_GAMES_LEADERBOARD_GET_INITIALS_COLUMN_POSITION(),INIT_VECTOR_2D(0.0,vCharSeparation.y*iPositionLoop)),ARCADE_GAMES_LEADERBOARD_GET_DEFAULT_COLOR(),ARCADE_GAMES_LEADERBOARD_GET_ENTRY_COLOR(bIsPlayerTwo),bEditing)

		//Score
		ARCADE_GAMES_LEADERBOARD_DRAW_SCORE(lbHost[iPositionLoop].iScore,ADD_VECTOR_2D(ARCADE_GAMES_LEADERBOARD_GET_SCORE_COLUMN_POSITION(),INIT_VECTOR_2D(0.0,vCharSeparation.y*iPositionLoop)),ARCADE_GAMES_LEADERBOARD_GET_DEFAULT_COLOR(),ARCADE_GAMES_LEADERBOARD_GET_ENTRY_COLOR(bIsPlayerTwo),bEditing)

	ENDFOR

ENDPROC

PROC ARCADE_GAMES_LEADERBOARD_LOAD(ARCADE_GAMES_LEADERBOARD_DATA &lbData, CASINO_ARCADE_GAME eGame, BOOL &bReceived)
	UNUSED_PARAMETER(eGame)
	
	IF NOT bReceived
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND (lbData.iLastLeaderboardRequest = -HIGHEST_INT
	OR NATIVE_TO_INT(GET_NETWORK_TIME()) - lbData.iLastLeaderboardRequest > ciARCADE_GAMES_RESEND_LEADERBOARD_REQUEST_TIME)
		BROADCAST_CASINO_ARCADE_REQUEST_LEADERBOARD_EVENT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), ARCADE_GAMES_LEADERBOARD_GET_CASINO_ARCADE_GAME())
		lbData.iLastLeaderboardRequest = NATIVE_TO_INT(GET_NETWORK_TIME())
	ENDIF

ENDPROC

PROC CMHDZ_DRAW_CLIENT_STATE_LEADERBOARD()

	//sCMHDZData.fOverallLevelProgression = 0.01
			
	// 1 is the minimum screen value so we count from there.
	
	//CMHDZ_DRAW_CLIENT_STATE_PLAYING()
	
	ARCADE_GAMES_LEADERBOARD_DRAW(sCMHDZData.sLbData, playerBd[PARTICIPANT_ID_TO_INT()].iLeaderboardInitials, cmhdzServerBd.sLeaderboard)
	
	TWS_DRAW_FRONT_FX()
ENDPROC

