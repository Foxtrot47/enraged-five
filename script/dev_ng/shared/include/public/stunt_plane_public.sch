//****************************************************************************************************
//
//	Author: 		Somebody that probably doesn't work here anymore.
//	Date: 			11/16/11
//	Description:	Shared consts for stunt plane stuff (stunt plane races, pilot school, etc.)
//	
//****************************************************************************************************


//Values for checking stunts 
//CONST_FLOAT 			STUNT_PLANE_KNIFE_LEFT_MIN 						70.0
//CONST_FLOAT 			STUNT_PLANE_KNIFE_LEFT_MAX 						110.0

//CONST_FLOAT 			STUNT_PLANE_KNIFE_RIGHT_MIN 					-70.0
//CONST_FLOAT 			STUNT_PLANE_KNIFE_RIGHT_MAX 					-110.0

CONST_FLOAT				STUNT_PLANE_KNIFE_THRESHOLD						0.3420  // sin(20)

CONST_FLOAT				STUNT_PLANE_INVERT_MIN							-135.0
CONST_FLOAT				STUNT_PLANE_INVERT_MAX							135.0

CONST_FLOAT				STUNT_PLANE_INVERT_THRESHOLD                    -0.7071	// -sin(45)

CONST_FLOAT				STUNT_PLANE_ROLL_MIN							45.0			
CONST_FLOAT				STUNT_PLANE_ROLL_MAX							135.0

CONST_FLOAT				STUNT_PLANE_ROLL_THRESHOLD						0.7071	// sin(45)

//Locate sizes
CONST_FLOAT				STUNT_PLANE_LOCATE_SIZE_CHECKPOINT				30.0


//Scale sizes
CONST_FLOAT 			STUNT_PLANE_NEXT_BLIP_SCALE						0.7	//radar blip size for the "next" checkpoint 
CONST_FLOAT 			STUNT_PLANE_CHECKPOINT_SCALE					15.0

//timers 
CONST_INT				STUNT_PLANE_VEH_STUCK_ROOF_TIME					1000	// Vehicle stuck time (roof) (ms).
CONST_INT				STUNT_PLANE_VEH_STUCK_SIDE_TIME					3000	// Vehicle stuck time (side) (ms).
CONST_INT				STUNT_PLANE_VEH_STUCK_HUNG_TIME					2000	// Vehicle stuck time (hung) (ms).
CONST_INT				STUNT_PLANE_VEH_STUCK_JAM_TIME					6000	// Vehicle stuck time (jam) (ms).

//sounds
STRING					STUNT_PLANE_SFX_CHECKPOINT_CLEAR				= "CHECKPOINT_NORMAL"	//sound played upon clearing a checkpoint
STRING					STUNT_PLANE_SFX_CHECKPOINT_MISS					= "CHECKPOINT_MISSED"	//sound played upon missing a checkpoint
STRING					STUNT_PLANE_SFX_CHECKPOINT_PERFECT				= "CHECKPOINT_PERFECT"	//sound with "extra flare" upon doing something special at a checkpoint

//player coords for being at the menu
VECTOR 					SPTT_MENU_PLAYER_COORDS 						= <<1694.7395, 3276.5024, 41.2796>>
VECTOR 					SPTT_MENU_PLAYER_ROTATION						= << 8.79494, 0.59893, 154.8464 >>
CONST_FLOAT				SPTT_MENU_PLAYER_HEADING						154.8464

PROC Stunt_Plane_Unreferenced_Variable_Hack()
	SPTT_MENU_PLAYER_COORDS = SPTT_MENU_PLAYER_COORDS
	SPTT_MENU_PLAYER_ROTATION = SPTT_MENU_PLAYER_ROTATION
	STUNT_PLANE_SFX_CHECKPOINT_CLEAR = STUNT_PLANE_SFX_CHECKPOINT_CLEAR
	STUNT_PLANE_SFX_CHECKPOINT_MISS = STUNT_PLANE_SFX_CHECKPOINT_MISS
	STUNT_PLANE_SFX_CHECKPOINT_PERFECT = STUNT_PLANE_SFX_CHECKPOINT_PERFECT
ENDPROC

