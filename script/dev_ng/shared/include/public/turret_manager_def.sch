//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_manager_def.sch																		//
// Description: Enums, structs, consts, and look-up tables for the turret_manager system.					//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		05/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "MP_globals_script_timers.sch"
USING "commands_debug.sch"

// How long to wait for a server response before sending another request
CONST_INT ci_TURRET_RQST_SETTINGS_TIMEOUT_MS 	1000
// How many times should the player request use of turret before giving up?
CONST_INT ci_TURRET_RQST_SETTINGS_TRIES			5

CONST_INT ci_TURRET_SCRIPT_HASH 				HASH("turret_cam_script")

CONST_INT ci_TURRET_LOCK_BS_DO_NOT_KICK	0 // If set: the player will not be kicked from any existing turret if the lock request fails

// Max number of turrets in any turret_group_type. Never decrease this number.
CONST_INT ci_MAX_TURRET_GROUP_SIZE 6

/// PURPOSE: Define groups of turrets which are conceptually related.
///    Each TURRET_GROUP_TYPE will have its own processing protocols which
///    determine how instances are handled and how each turret in the group
///    is relevant.
///    
///    E.g. Using one TGT_ARENA_SPECTATOR turret gives you access to the other
///    turrets with LB/RB so long as they are not in use.
ENUM TURRET_GROUP_TYPE
	TGT_NONE = 0,
	TGT_ARENA_SPECTATOR,
	TGT_ARENA_CONTESTANT
	
	// If you add a new TURRET_GROUP_TYPE you will need to add an entry to
	// "FUNC INT TURRET_GROUP_TURRET_COUNT" (below) ensure that 
	// "ci_MAX_TURRET_GROUP_SIZE" (above) is up to date.
ENDENUM

/// PURPOSE:
///    Look up table for number of turrets used in a given TURRET_GROUP_TYPE
FUNC INT TURRET_GROUP_TURRET_COUNT(TURRET_GROUP_TYPE eType)
	INT iResult
	SWITCH eType
		CASE TGT_ARENA_SPECTATOR 	iResult = 6 BREAK
		DEFAULT 					iResult = 1 BREAK
	ENDSWITCH
		
	#IF IS_DEBUG_BUILD
		IF iResult > ci_MAX_TURRET_GROUP_SIZE
			CASSERTLN(DEBUG_NET_TURRET, "INCREASE ci_MAX_TURRET_GROUP_SIZE in 'turret_manager_def.sch'")
		ENDIF
	#ENDIF
	RETURN iResult
ENDFUNC

/// PURPOSE: A player can only occupy one turret at a time. This is the data required to track
///    that for one player.
STRUCT TURRET_OCCUPANCY_DATA		
	INT iTurretId					
	TURRET_GROUP_TYPE groupType		
	INT iInstanceId = -1			// For example a vehicle network_index may be used for vehicle turret types
ENDSTRUCT

/// PURPOSE: Describes the state of the turret_manager system.
ENUM TURRET_MANAGER_LOCK_STATE
	TLLS_UNLOCKED,	// No turret is in use.
	TLLS_LOCKING,	// Waiting for server to confirm that we can use a particular turret.
	TLLS_LOCKED,	// Player has locked in a turret with the server.
	TLLS_UNLOCKING	// Waiting for server to confirm it knows we're not using a turret.
ENDENUM

/// PURPOSE: The server response status for a turret lock request.
ENUM TURRET_LOCK_RESPONSE
	TLR_PENDING,	// Waiting for a server response
	TLR_SUCCESS,	// Turret locked
	TLR_FAILED		// Failed to lock turret (including if a request has been overwritten by a more recent one)
ENDENUM

/// PURPOSE: Defines how to search for a free turretId server side.
ENUM TURRET_LOCK_SEARCH_TYPE
	TLST_THIS_TURRET_ONLY = 0,	// Only lock the turret specified in the request (if available)
	TLST_ANY_TURRET, 			// Lock any turret with a matching group and instance
	TLST_ANY_TURRET_FORWARDS,	// Lock the next free turretId forwards
	TLST_ANY_TURRET_BACKWARDS	// Lock the next free turretId backwards
ENDENUM

/// PURPOSE: Special purpose enum to represent a turret lock request id.
ENUM TURRET_LOCK_REQUEST_ID
	TLRI_NONE = 0
ENDENUM

#IF IS_DEBUG_BUILD
STRUCT TURRET_MANAGER_DEBUG
	BOOL bDumpServerData 		// Set to dump server data
	WIDGET_GROUP_ID widgetGroup 
ENDSTRUCT
#ENDIF //IS_DEBUG_BUILD

/// PURPOSE: Context for the turret launcher system.
STRUCT TURRET_MANAGER_DATA
	TURRET_OCCUPANCY_DATA turretLockRequestCopy
	PLAYER_INDEX pHost			// Cache host of freemode
	
	SCRIPT_TIMER requestSentTime
	INT iRequestAttempts // How many times have we sent the server a reqest?
	
	#IF IS_DEBUG_BUILD
	TURRET_MANAGER_DEBUG debug
	#ENDIF
ENDSTRUCT

/// PURPOSE: These indices represent how data is ordered
///    in the request and responses sent to and from server.
CONST_INT ci_TURRET_RQST_UID	0
CONST_INT ci_TURRET_RQST_TURRET	1
CONST_INT ci_TURRET_RQST_INST 	2
CONST_INT ci_TURRET_RQST_GRP  	3
CONST_INT ci_TURRET_RQST_SEARCH	4
CONST_INT ci_TURRET_RQST_BS		5 // Uses bits prefixed with ci_TURRET_LOCK_BS_
// If any more INTs are added ci_TURRET_RQST_COUNT (below) must be updated!

CONST_INT ci_TURRET_RQST_COUNT  6
