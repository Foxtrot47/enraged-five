USING "arcade_road/arcade_road_helpers.sch"

#IF IS_DEBUG_BUILD

PROC ROAD_DEBUG_PROCESSING()
	
	ROAD_DEBUG_CREATE_WIDGETS()
	BACKGROUND_DEBUG_CREATE_WIDGETS()
	CABINET_DEBUG_CREATE_WIDGETS()
	
ENDPROC

#ENDIF

PLAYER_VEHICLE pvPlayer
RIVAL_PLAYER_VEHICLE rpvRival

STAGES_SETS stgFirstStage
STAGES_SETS stgCurrentStage
STAGES_SETS stgNextStage
STAGES_SETS stgFinalStage

PROC ROAD_CLEANUP_AND_EXIT_CLIENT()
	CDEBUG1LN(DEBUG_MINIGAME, "[ROAD] [JS] - ROAD_CLEANUP_AND_EXIT_CLIENT - Cleaning up and terminating the script")
	ARCADE_GAMES_HELP_TEXT_CLEAR()
	ARCADE_CABINET_COMMON_CLEANUP()
	RELEASE_MENU_ASSETS()
	ROAD_ARCADE_RELEASE_ALL_SOUNDS(FALSE)
	RELEASE_INGAME_ASSETS()
	RELEASE_TITLE_ASSETS(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stgCurrentStage))
	
	ARCADE_GAMES_AUDIO_CLEAN_UP()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_ScreenOverlay)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeGamesFX00")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeGamesFX01")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeGamesFX02")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeGamesFX03")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeGamesFX04")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPArcadeGamesFX05")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_HUDGeneric)
	
	ARCADE_GAMES_SOUND_RELEASE_AUDIO_BANK("DLC_HEIST3/H3_RaceNChase")
	ARCADE_GAMES_SOUND_RELEASE_AUDIO_BANK("DLC_HEIST3/H3_RaceNChase_Music_Previews")
	ARCADE_GAMES_AUDIO_SCENE_STOP(roadArcadeAudioScene_Active)
	TRIGGER_MUSIC_EVENT("ARCADE_RACE_N_CHASE_STOP_MUSIC")	
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC ROAD_PROTOTYPE_RACE_SETUP()

	CDEBUG1LN(DEBUG_MINIGAME, "ROAD_PROTOTYPE_RACE_SETUP - Setting up this frame")
	
	// Intialise Street Legal vehicle

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets
			pvPlayer.sSpriteFrame = "VEHICLE_CAR_01_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets
			pvPlayer.sSpriteFrame = "VEHICLE_CAR_02_FRAME_01"
		ENDIF
		
		// Same width and heigh across both sprites
		pvPlayer.iWidth = 676
		pvPlayer.iHeight = 260
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets_Bike
			pvPlayer.sSpriteFrame = "VEHICLE_BIKE_01_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets_Bike
			pvPlayer.sSpriteFrame = "VEHICLE_BIKE_02_FRAME_01"
		ENDIF
		
		// Same width and heigh across both sprites
		pvPlayer.iWidth = 288
		pvPlayer.iHeight = 288
		
		pvPlayer.fAcceleration = 60
		pvPlayer.fMaxSpeed = 110
		pvPlayer.fPenaltyMod = 4.5
		pvPlayer.fPerfectBite = 0.35
		
		pvPlayer.fOverdriveTime = 300.0
		pvPlayer.fDrag = 15.0
		pvPlayer.fHandling = 20.0
		
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets_Truck
			pvPlayer.sSpriteFrame = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets_Truck
			pvPlayer.sSpriteFrame = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01"
		ENDIF
		
		pvPlayer.iWidth = 752
		pvPlayer.iHeight = 288
		
		pvPlayer.fAcceleration = 30
		pvPlayer.fPerfectBite = 0.8
		pvPlayer.fMaxSpeed = 80
		pvPlayer.fPenaltyMod = 1.5
		
		pvPlayer.fOverdriveTime = 600.0
		pvPlayer.fDrag = 30.0
		pvPlayer.fHandling = 7.5

	ENDIF
	
	pvPlayer.sDriverFrameNo = "01"
	pvPlayer.psActiveState = ps_countdown
	pvPlayer.iPlayerScore = 0
	pvPlayer.iSpeedMultiplier = 1
	pvPlayer.pgPlayerGear = pgLOW
	pvPlayer.fPerfectBite = 0.6
	pvPlayer.fPenaltyMod = 2.5
	pvPlayer.fPlayerSpeed = 0
	pvPlayer.bCrashedOnce = FALSE
	
	// Debug clear the awards.
#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_J) 
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TRAFFICAVOI, FALSE)
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_CANTCATCHBRA, FALSE)	
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDTRUCK, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDTRUCK, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDTRUCK, FALSE)
	ENDIF
#ENDIF
	fXCarOffset = 0.0

	ciROAD_HORIZON_POINT = 640
	cfROAD_HillScale = 0.0
	cfROAD_HorizonScale = 0.99
	cfROAD_SideScaleMod =	0.6
	fROAD_RoadWidthMultiplier = 2.0

	stgFirstStage = ROAD_ARCADE_GET_FIRST_STAGE_FROM_SELECT()
	
	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
	AND roadArcadeServerBd.stageToStart != STAGE_INVALID
		stgFirstStage = roadArcadeServerBd.stageToStart
	ENDIF
	
	stgCurrentStage = stgFirstStage
	
	stgNextStage = ROAD_ARCADE_GET_NEXT_STAGE(stgCurrentStage, FALSE)
	stgFinalStage = ROAD_ARCADE_GET_NEXT_STAGE(stgCurrentStage, TRUE)
	
	// Initiate level backgrounds
	ROAD_ARCADE_INIT_BACKGROUND_DATA(stgCurrentStage)

	// Initiate over road objects
	GENERIC_OV_INIT(stgCurrentStage, stgFirstStage)
	
	// Initialise Vice City objects
	ROAD_ARCADE_INIT_STAGE_SIDE_OBJ(stgCurrentStage)
	ROAD_ARCADE_INIT_BACKGROUND_DATA(stgCurrentStage)
	ROAD_ARCADE_INIT_GENERIC_VEH(stgCurrentStage)
	ROAD_ARCADE_INIT_STAGE_CROSSING_OBJ(stgCurrentStage)
	ROAD_ARCADE_INIT_RIVAL_VEH(stgCurrentStage)
	STAGE_OV_INIT(stgCurrentStage)
	
	RA_INIT_FLAGGER(stgFirstStage)
	
	pfasCurrentFinishState = FS0_INIT
	transitionData.transition_ActiveState = T_INACTIVE
	
	bFinishedRace = FALSE
	bCloseGame = FALSE
	bGameOVER = FALSE

	SETTIMERA(0)
	SETTIMERB(0)
		
	// Reset the network variables
	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
		roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iScore = -1 // Set final score invalid
		roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].fPlayerProgress = 0.0
		roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].bFinished = FALSE
		
		CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_AGREED)
		CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_DECLINED)

		// Reset tag display
		sPlayerOneTag = ""
		sPlayerTwoTag = ""

		RESET_lbCurrentEntry()
				
//	ELSE
//		sPlayerOneTag = "BAZ"
//		sPlayerTwoTag = "CBT"
	
	ENDIF
	
	// Reset the variables
	pvPlayer.iPlayerPosX = 960
	pvPlayer.iPlayerPosY = 844
	fJOURNEY_DistanceTravelled = 0.01
	fGameTimerElapsed = 0
	fCountDownTimer = 0
	fGameTIMER = 40.0
	fFinalZValue = 0
		
	ROAD_ARCADE_AUDIO_START_AUDIO_SCENE(ROAD_ARCADE_AUDIO_SCENE_MAIN_GAME)
	ROAD_ARCADE_RELEASE_ALL_SOUNDS()
	ROAD_ARCADE_START_SONG_FROM_SELECTION(muMenuSelection.iSongSelection)
			
	// Initialise structures
	ROAD_ARCADE_RESET_WARNING_OBJ()
	ROAD_ARCADE_RESET_TICKER_OBJ()
			
ENDPROC


// Moved to main loop script as it's the only place it's used.
PROC ROAD_ARCADE_UPDATE_IN_GAME_TIMER(PLAYER_VEHICLE& player)

	UNUSED_PARAMETER(player) // False unref error

	IF fCountDownTimer > 4000
	AND !bFinishedRace
	AND transitionData.transition_ActiveState = T_INACTIVE
		fGameTimerElapsed += 0.0+@(1000)
	ENDIF
	
	IF fGameTimerElapsed < 70000
		fGameTIMER = 60.0 - (fGameTimerElapsed/1000.0) // Millisecs corrected to seconds.
	ENDIF
	
	// Final Countdown Effect
	IF fGameTIMER < 10
		IF CEIL(fGameTimer) != iGameTimer_LastWholeNum
			IF fGameTIMER > 0
				IF !bGameOVER
				AND !bFinishedRace
					ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_TIMER_COUNTDOWN)
				ENDIF
			ENDIF
			iGameTimer_LastWholeNum =  CEIL(fGameTimer)
		ENDIF
		
		IF fGameTIMER < 5
		AND fGameTIMER > 0
			IF !bGameOVER
			AND !bFinishedRace
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_TIMER_WARNING)
			ENDIF
		ELSE
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_TIMER_WARNING)
		ENDIF
	ENDIF
		
	IF fGameTIMER <= 0
	
		fGameTIMER = 0 // Cap off at 0 exactly
	
		IF !bGameOVER
			player.ePlayerPedAnimState = RA_PLAYER_PED_ANIM_LOSE
			
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
			ENDIF
			
			bGameOVER = TRUE
			
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_TIMER_COUNTDOWN)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_TIMER_WARNING)
			
			ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_LOSE)
									
			IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
			
				CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_UPDATE_IN_GAME_TIMER - Game Over set as we have timed out.")
						
				SET_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_TIME_OVER)
				ROAD_ARCADE_NET_SET_PARTICIPANT_FINAL_SCORE(pvPlayer, PARTICIPANT_ID_TO_INT())
			ENDIF
						
		ENDIF
		
	ENDIF
		
	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
		IF ROAD_ARCADE_NET_IS_MULTIPLAYER_RACE_OVER()
			IF !bGameOVER
			AND !roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].bFinished
				CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_UPDATE_IN_GAME_TIMER - Game Over as OPPONENT has timed out.")
				
				IF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(PARTICIPANT_ID_TO_INT())].iNetBitSet, RA_NET_PLAYER_TIME_OVER)
				AND fGameTIMER > 0
					pvPlayer.iPlayerScore += 50000 // Increase still active player's score if they didn't time out!
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
				ENDIF
				
				ROAD_ARCADE_NET_SET_PARTICIPANT_FINAL_SCORE(pvPlayer, PARTICIPANT_ID_TO_INT())
				bGameOVER = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bGameOVER
			
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
			ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR, "Speed", 0.0)
			ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR, "Gear", TO_FLOAT(ENUM_TO_INT(player.pgPlayerGear)))
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
			ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE, "Speed", 0.0)
			ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE, "Gear", TO_FLOAT(ENUM_TO_INT(player.pgPlayerGear)))
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
			ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK, "Speed", 0.0)
			ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK, "Gear", TO_FLOAT(ENUM_TO_INT(player.pgPlayerGear)))
		ENDIF
	
	ENDIF
ENDPROC

PROC ROAD_PROTOTYPE_MAIN_UPDATE()
			
	IF pvPlayer.psActiveState = ps_Drive
	AND !bGameOVER
		PLAYER_VEHICLE_CONTROLS(pvPlayer) // Process inputs first
		pvPlayer.ePlayerPedAnimState = RA_PLAYER_PED_ANIM_IN_GAME
	ENDIF
	
	FLOAT fRoadCurve = SCALING_ROAD_CURVATURE(stgCurrentStage, pvPlayer.fZValue)*(pvPlayer.fHandling/2.0)
	// Final offset for the player's X based on the Road Curvature
	fXCarOffset -= (0+@(fRoadCurve))*(pvPlayer.fPlayerSpeed/pvPlayer.fMaxSpeed)
	
	SCALING_ROAD_HILL_MOD(stgCurrentStage, fFinalZValue)
	cfROAD_HillScale += cfROAD_HillScaleMod
	fROAD_RoadWidthMultiplier += cfROAD_WidthMultiplierMod
	ciROAD_HORIZON_POINT += ciROAD_HorizonDebug

	// Determine object draw order based on 
	SIDELINE_SIDES sideToDrawFirst = SIDE_LEFT
	SIDELINE_SIDES sideToDrawLast = SIDE_RIGHT
	
	// Determine drawing side based on X axis
	IF (pvPlayer.fRoadCenter + SCALING_ROAD_CURVATURE(stgCurrentStage, pvPlayer.fZValue)) > (cfBASE_SCREEN_WIDTH/2.0)
		sideToDrawFirst = SIDE_RIGHT
		sideToDrawLast = SIDE_LEFT
	ENDIF
		
	// Draw the transtition tunnel when we go to the next stage
	ROAD_ARCADE_HANDLE_STAGE_TRANSITION(pvPlayer, stgCurrentStage, stgNextStage, stgFirstStage, stgFinalStage)
	
	ROAD_ARCADE_DRAW_BACKGROUND(stgCurrentStage, pvPlayer)

	ROAD_ARCADE_SCALING_GROUND_DRAW(stgCurrentStage)
	
	INT objectCount
	INT i
	
	// Sideline objects 1
	IF transitionData.transition_ActiveState = T_INACTIVE
		objectCount = ROAD_ARCADE_GET_SIDELINE_OBJ_COUNT(stgCurrentStage)
		i = 0
		FOR i = 0 TO objectCount
			ROAD_ARCADE_DRAW_SIDELINE_OBJECTS(pvPlayer, sideToDrawFirst, stgCurrentStage, i)
		ENDFOR
	ENDIF

	ROAD_ARCADE_SCALING_ROAD_DRAW(pvPlayer, stgCurrentStage, stgNextStage)
	
	// Sideline objects 2
	IF transitionData.transition_ActiveState = T_INACTIVE
		i = 0
		FOR i = 0 TO objectCount
			ROAD_ARCADE_DRAW_SIDELINE_OBJECTS(pvPlayer, sideToDrawLast, stgCurrentStage, i)
		ENDFOR
	ENDIF
		
	ROAD_ARCADE_DRAW_OVERROAD_OBJS(pvPlayer, rpvRival, stgCurrentStage)

	IF pvPlayer.psActiveState != ps_Crashed
	AND pvPlayer.psActiveState != ps_Swerving
	AND pvPlayer.psActiveState != ps_Finish
		PLAYER_VEHICLE_DRAW(pvPlayer)
	ENDIF

	IF pvPlayer.psActiveState = ps_Crashed
	
		BOOL bJustReset = FALSE
	
		IF pvPlayer.bOffRoad
			IF fXCarOffset >= (ciROAD_DEFAULT_SPRITE_WIDTH * 1.5)
			OR fXCarOffset <= -(ciROAD_DEFAULT_SPRITE_WIDTH * 1.5)
				bJustReset = TRUE
				SETTIMERB(0)
			ENDIF
		ENDIF
	
		IF ROAD_ARCADE_HANDLE_PLAYER_CRASH_ANIM(pvPlayer, bJustReset)
			pvPlayer.psActiveState = ps_Drive // Revert to driving if we have finished the Crash anim
		ENDIF
	ENDIF
	
	IF pvPlayer.psActiveState = ps_Swerving
		IF ROAD_ARCADE_HANDLE_PLAYER_SWERVE(pvPlayer)
			pvPlayer.psActiveState = ps_Drive // Revert to driving if we have finished the Crash anim
		ENDIF
	ENDIF
		
	IF bFinishedRace
	AND !bGameOVER
		ROAD_ARCADE_HANDLE_PLAYER_FINISH_ANIM(pvPlayer, stgCurrentStage)
	ENDIF
	
	ROAD_ARCADE_HUD_DRAW(pvPlayer)

	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
		ROAD_ARCADE_NET_RENDER_PLAYER_RANK()
	ENDIF
	
	ROAD_ARCADE_HANDLE_TICKER_OBJECT()
	ROAD_ARCADE_HANDLE_WARNING_OBJECT()
		
	// Calculate the player's curve value
	pvPlayer.fCurveMultiplier = SCALING_ROAD_CURVATURE(stgCurrentStage, pvPlayer.fZValue)

	// Draw the lights seen at the start.
	//CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PLAYER ACTIVE STATE", pvPlayer.psActiveState)
	IF fCountDownTimer < 5000
		ROAD_ARCADE_HUD_DRAW_STARTING_LIGHTS(pvPlayer) // Draw traffic lights at the start.
		
		// Perfect start control
		// Normal deteriorates
		
		IF GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT) > 0.1
			pvPlayer.fAccelerationNormal = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
		ENDIF
		
		// Perfect bite is within limits
		IF fCountDownTimer < 3000
			IF pvPlayer.fAccelerationNormal >= pvPlayer.fPerfectBite - 0.25
				IF !pvPlayer.bPerfectStart
				
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_CAR)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_BIKE)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_TRUCK)
					ENDIF
				ENDIF
				
				pvPlayer.bPerfectStart = TRUE
			ENDIF
						
			// Too little
			IF pvPlayer.fAccelerationNormal < pvPlayer.fPerfectBite - 0.25
				pvPlayer.bPerfectStart = FALSE
			ENDIF		
			
			// Too far
			IF pvPlayer.fAccelerationNormal > pvPlayer.fPerfectBite + 0.25
				pvPlayer.bPerfectStart = FALSE			
			ENDIF
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				IF RA_INPUT_IS_ACCEL_DOWN(FALSE)
					IF !pvPlayer.bPerfectStart
						IF eRoadArcade_ActiveGameType = RA_GT_CAR
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_CAR)
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_BIKE
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_BIKE)
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_TRUCK)
						ENDIF
						pvPlayer.bPerfectStart = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF pvPlayer.bPerfectStart
				SET_CONTROL_SHAKE(FRONTEND_CONTROL, 100, 90)
			ENDIF
		
		ENDIF
	ENDIF
			
	ROAD_ARCADE_UPDATE_IN_GAME_TIMER(pvPlayer)
	
	
	IF bGameOVER

		pvPlayer.fPlayerSpeed = 0

		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_GAME_OVER", 
		INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), 
		INIT_VECTOR_2D(520, 100)
		, 0, rgbaOriginal)
		
		IF RA_INPUT_IS_ACCEL_JUST_PRESSED()

			ROAD_ARCADE_GO_TO_LEADERBOARD(pvPlayer)
						
		ENDIF
	ENDIF

	
	ROAD_ARCADE_JOURNEY_UPDATE(pvPlayer)
		
#if IS_DEBUG_BUILD


	IF widgetArcadeCabinetSupport != NULL
		ARCADE_GAMES_POSTFX_WIDGET_UPDATE()
		ARCADE_GAMES_SOUND_WIDGET_UPDATE()
		ARCADE_GAMES_LEADERBOARD_WIDGET_UPDATE()
	ENDIF

#ENDIF
	
ENDPROC

FUNC STAGES_SETS ROAD_ARCADE_GET_RANDOM_STAGE()

	STAGES_SETS stgReturn = STAGE_VICE_CITY
	
	INT iRandomNum = GET_RANDOM_INT_IN_RANGE(0, 5)
	
	SWITCH(iRandomNum)
	
		CASE 0
			stgReturn = STAGE_VICE_CITY
		BREAK
	
		CASE 1
			stgReturn = STAGE_SAN_FIERRO
		BREAK
	
		CASE 2
			stgReturn = STAGE_SAN_ANDREAS
		BREAK
	
		CASE 3
			stgReturn = STAGE_LAS_VENTURAS
		BREAK
	
		CASE 4
			stgReturn = STAGE_LIBERTY_CITY
		BREAK
	
	ENDSWITCH

	RETURN stgReturn

ENDFUNC

PROC ROAD_ARCADE_TITLE_SETUP(BOOL bMpVersion = FALSE)
	
	pvPlayer.sSpriteDict = sDICT_PlayerAssets
	pvPlayer.sSpriteFrame = "VEHICLE_CAR_01_FRAME_01"
	pvPlayer.fTurningAngle = 0.0
	pvPlayer.fPlayerLeftStickX = 0.0
	pvPlayer.fPlayerSpeed = 0.0
	pvPlayer.iPlayerPosX = 960
	fJOURNEY_DistanceTravelled = 0.01

	stgCurrentStage = ROAD_ARCADE_GET_RANDOM_STAGE()
	ROAD_ARCADE_INIT_STAGE_SIDE_OBJ_TITLE(stgCurrentStage)
	ROAD_ARCADE_INIT_BACKGROUND_DATA(stgCurrentStage)
		
	transitionData.transition_ActiveState = T_INACTIVE
	fJOURNEY_DistanceTravelled = 0.1
	
	// Intialise Street Legal vehicle

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets
			pvPlayer.sSpriteFrame = "VEHICLE_CAR_01_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets
			pvPlayer.sSpriteFrame = "VEHICLE_CAR_02_FRAME_01"
		ENDIF
		
		// Same width and heigh across both sprites
		pvPlayer.iWidth = 676
		pvPlayer.iHeight = 260
	ENDIF
		
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets_Bike
			pvPlayer.sSpriteFrame = "VEHICLE_BIKE_01_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets_Bike
			pvPlayer.sSpriteFrame = "VEHICLE_BIKE_02_FRAME_01"
		ENDIF
		
		// Same width and heigh across both sprites
		pvPlayer.iWidth = 288
		pvPlayer.iHeight = 288
		
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets_Truck
			pvPlayer.sSpriteFrame = "VEHICLE_TRUCK_01_FRAME_01"
			pvPlayer.sDriverFrameNo = "01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets_Truck
			pvPlayer.sSpriteFrame = "VEHICLE_TRUCK_02_FRAME_01"
		ENDIF
		
		pvPlayer.iWidth = 752
		pvPlayer.iHeight = 288
	ENDIF
	
	pvPlayer.sDriverFrameNo = "01"
	pvPlayer.psActiveState = ps_Drive
	pvPlayer.iPlayerScore = 0
	pvPlayer.iSpeedMultiplier = 1
	pvPlayer.pgPlayerGear = pgHigh
	pvPlayer.fPlayerSpeed = 60.0 // Reset player's speed
	fXCarOffset = 0.0
	
	// Reset road height variables
	ciROAD_HORIZON_POINT = 640
	cfROAD_HillScale = 0.0
	cfROAD_HorizonScale = 0.99
	cfROAD_SideScaleMod =	0.6
	fROAD_RoadWidthMultiplier = 2.0
	
	IF !bMpVersion
		//IF NOT AUDIO_IS_MUSIC_PLAYING()
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				TRIGGER_MUSIC_EVENT("ARCADE_RC_LS_NIGHTS_START")
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				TRIGGER_MUSIC_EVENT("ARCADE_RC_RED_VELVET_START")
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				TRIGGER_MUSIC_EVENT("ARCADE_RC_RENEGADE_START")
			ENDIF
		//ENDIF
	ELSE
	
		// Play the new challenger jingle if available
	
	ENDIF
	
ENDPROC

// Stripped down version of the above
PROC ROAD_ARCADE_TITLE_UPDATE(BOOL bMpVersion = FALSE)

	// Reset progress if we are well beyond a large number
	IF fJOURNEY_DistanceTravelled >= 100000
		fJOURNEY_DistanceTravelled = 0.01
	ENDIF

	// Lock speed at 60
	IF !bMpVersion
		pvPlayer.fPlayerSpeed = 60.0
		pvPlayer.fTurningAngle = 0.0
		pvPlayer.fPlayerLeftStickX = 0.0
	ENDIF
	
	// Determine object draw order based on 
	SIDELINE_SIDES sideToDrawFirst = SIDE_LEFT
	SIDELINE_SIDES sideToDrawLast = SIDE_RIGHT
	
	// Determine drawing side based on X axis
	IF SCALING_ROAD_CURVATURE(stgCurrentStage, fJOURNEY_DistanceTravelled) > 0.0
		sideToDrawFirst = SIDE_RIGHT
		sideToDrawLast = SIDE_LEFT
	ENDIF
	
	ROAD_ARCADE_DRAW_BACKGROUND(stgCurrentStage, pvPlayer, TRUE)
	ROAD_ARCADE_SCALING_GROUND_DRAW(stgCurrentStage, TRUE)
	
	INT objectCount
	INT i
	
	// Don't draw the following elements if we are in a tunnel
	// Sideline objects 1
	objectCount = ROAD_ARCADE_GET_SIDELINE_OBJ_COUNT(stgCurrentStage)
	i = 0
	FOR i = 0 TO objectCount
		ROAD_ARCADE_DRAW_SIDELINE_OBJECTS(pvPlayer, sideToDrawFirst, stgCurrentStage, i, TRUE)
	ENDFOR

	ROAD_ARCADE_SCALING_ROAD_DRAW(pvPlayer, stgCurrentStage, stgNextStage, TRUE)

	// Sideline objects 2
	i = 0
	FOR i = 0 TO objectCount
		ROAD_ARCADE_DRAW_SIDELINE_OBJECTS(pvPlayer, sideToDrawLast, stgCurrentStage, i, TRUE)
	ENDFOR

	IF bMpVersion

		FLOAT fRivalX = 0.0
		
		pvPlayer.fPlayerSpeed = 0 // static for now
		
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_GenericAssets,
			"GENERIC_FRAME_01_START_BANNER_01",
			INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0) + 60.0),
			INIT_VECTOR_2D(1264, 632),
			0.0,
			rgbaOriginal)
		
		IF iPlayerSeatLocalID = 0
			pvPlayer.iPlayerPosX = 740
			fRivalX = 1180.0
		ELSE
			pvPlayer.iPlayerPosX = 1180
			fRivalX = 740.0
		ENDIF

		TEXT_LABEL_63 sRival = ROAD_ARCADE_GET_RIVAL_TEXTURE_FROM_PLAYER_ID(iPlayerSeatLocalID)

		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_RivalVehDistant,
			TEXT_LABEL_TO_STRING(sRival), 
			INIT_VECTOR_2D(fRivalX, TO_FLOAT(pvPlayer.iPlayerPosY)),
			INIT_VECTOR_2D(352.0, 256.0),
			0.0,
			rgbaOriginal)
					
	ENDIF

	PLAYER_VEHICLE_DRAW(pvPlayer)
		
	//ROAD_ARCADE_UPDATE_IN_GAME_TIMER(pvPlayer)
	
	ROAD_ARCADE_JOURNEY_UPDATE(pvPlayer)
	
	//ROAD_ARCADE_RENDER_Z_BUFFER()
	
	IF !bMpVersion
		RA_DRAW_TITLE_SCREEN_OBJ()
	ELSE
		RA_DRAW_CHALLENGE_SCREEN_OBJ()
	ENDIF

#if IS_DEBUG_BUILD

	IF widgetArcadeCabinetSupport != NULL
		ARCADE_GAMES_POSTFX_WIDGET_UPDATE()
		ARCADE_GAMES_SOUND_WIDGET_UPDATE()
		ARCADE_GAMES_LEADERBOARD_WIDGET_UPDATE()
	ENDIF

#ENDIF

ENDPROC

