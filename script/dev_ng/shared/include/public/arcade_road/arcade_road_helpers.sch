USING "arcade_cabinet_minigame_common.sch"
USING "net_arcade_cabinet.sch"
USING "arcade_games_leaderboard.sch"
USING "arcade_games_postfx.sch"
USING "arcade_games_help_text.sch"
USING "arcade_games_help_text_flow.sch"
USING "arcade_road/arcade_road_audio.sch"

STRUCT_ARCADE_CABINET sRoadArcadeTelemetry

ENUM ROAD_ARCADE_LOCAL_CHALLENGE_TRACKERS

	RA_CHALLENGE_CANTCATCHBRA = BIT0,
	RA_CHALLENGE_RACECHAMP_PLAT = BIT1,
	RA_CHALLENGE_TRAFFICAVOI= BIT2,
	RA_CHALLENGE_RACECHAMP_GOLD = BIT3,
	RA_CHALLENGE_RACECHAMP_SILVER = BIT4,
	RA_CHALLENGE_RACECHAMP_BRONZE = BIT5

ENDENUM

ENUM ROAD_ARCADE_GAME_TYPE
	RA_GT_INVALID = -1,
	RA_GT_CAR,
	RA_GT_BIKE,
	RA_GT_TRUCK,
	RA_GT_MUTANT
ENDENUM

ROAD_ARCADE_GAME_TYPE eRoadArcade_ActiveGameType = RA_GT_CAR
INT iPlayerSeatLocalID = 0 // Player ID. 0 = Left player, 1 = Right Player

STRING sPrefixDebugRoadArcade = "[ROAD] [LB] "

STRING sDICT_GenericAssets = "MPRoadArcade_Generic"

// car
STRING sDICT_PlayerAssets = "MPRoadPlayer_Up"
STRING sDICT_PlayerCrash = "mproadplayer_crash"

// bike
STRING sDICT_PlayerAssets_Bike = "MPRoadPlayer_Bike"
STRING sDICT_PlayerCrash_Bike = "mproadplayer_crash_bike"

// truck
STRING sDICT_PlayerAssets_Truck = "MPRoadPlayer_Truck"
STRING sDICT_PlayerCrash_Truck = "mproadplayer_crash_truck"

STRING sDICT_PlayerTwoAssets = "MPRoadArcade_RivalVehicleFull"
STRING sDICT_PlayerTwoCrash = "MPRoadArcade_RivalVehicleCrash"

STRING sDICT_PlayerTwoAssets_Bike = "MPRoadArcade_RivalVehicleFull_Bike"
STRING sDICT_PlayerTwoCrash_Bike = "MPRoadArcade_RivalVehicleCrash_Bike"

STRING sDICT_PlayerTwoAssets_Truck = "MPRoadArcade_RivalVehicleFull_Truck"
STRING sDICT_PlayerTwoCrash_Truck = "MPRoadArcade_RivalVehicleCrash_Truck"

STRING sDICT_HUDGeneric = "MPRoadArcade_HUD_Gen"

STRING sDICT_EffectsAssets = "MPRoadArcade_Effects"

STRING sDICT_FinishLine = "MPRoadArcade_FinishLine"
STRING sDICT_FinishLine_Bike = "MPRoadArcade_FinishLine_Bike"
STRING sDICT_FinishLine_Truck = "MPRoadArcade_FinishLine_Truck"

STRING sDICT_FinishCrowd = "MPRoadArcade_FinishCrowd"
STRING sDICT_FinishGroups = "MPRoadArcade_FinishGroups"

STRING sDICT_TitleAssets = "MPRoadArcade_TitleAssets"

STRING sDICT_SelectMenu = "MPRoadArcade_SelectMenu"
STRING sDICT_SelectMenu_Bike = "MPRoadArcade_SelectMenu_Bike"
STRING sDICT_SelectMenu_Truck = "MPRoadArcade_SelectMenu_Truck"

STRING sDICT_SelectMenuRadio = "MPRoadArcade_SelectMenuRadio"

STRING sDICT_ScreenOverlay = "MPRoadArcade_ScreenEffects"

// Car
STRING sDICT_LeaderBoard = "MPRoadArcade_LeaderBoard"
STRING sDICT_LeaderBoard_Rival = "MPRoadArcade_LeaderBoard_Rival"

// Bike
STRING sDICT_LeaderBoard_Bike = "MPRoadArcade_LeaderBoard_Bike"
STRING sDICT_LeaderBoard_RivalBike = "MPRoadArcade_LeaderBoard_BikeRival"

// Truck
STRING sDICT_LeaderBoard_Truck = "MPRoadArcade_LeaderBoard_Truck"
STRING sDICT_LeaderBoard_RivalTruck = "MPRoadArcade_LeaderBoard_TruckRival"

STRING sDICT_EasterEggs = "MPRoadArcade_EasterEggs"

STRING sDICT_TrackVice = "MPRoadArcade_ViceTrack"
STRING sDICT_TrackAndreas = "MPRoadArcade_AndreasTrack"
STRING sDICT_TrackFeirro = "MPRoadArcade_FeirroTrack"
STRING sDICT_TrackLiberty = "MPRoadArcade_LibertyTrack"
STRING sDICT_TrackVenturas = "MPRoadArcade_VenturasTrack"

// Vehicle dictionaries
STRING sDICT_GenericCar = "MPRoadArcade_VehCar"
STRING sDICT_GenericPickUp = "MPRoadArcade_VehPickUp"
STRING sDICT_GenericBike = "MPRoadArcade_VehBike"
STRING sDICT_GenericTruck = "MPRoadArcade_VehTruck"

STRING sDICT_RivalVehDistant = "MPRoadArcade_RivalVehicleUp"

// Tweakers
TWEAK_FLOAT cfROAD_HillScaleMod 0.0
TWEAK_FLOAT cfROAD_WidthMultiplierMod 0.0
TWEAK_INT ciROAD_HorizonDebug 0

FLOAT fJOURNEY_DistanceTravelled = 0.01
FLOAT fFinalZValue = 0.0
FLOAT fZTotalLengthOfRoad = 0.0
FLOAT fXCarOffset = 0.0

// Timers
FLOAT fGameTIMER
FLOAT fGameTimerElapsed
FLOAT fCountDownTimer = 0.0
FLOAT fRunTimer = 0.0

CONST_INT ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM			0



BOOL bFinishedRace
BOOL bGameOVER
BOOL bCloseGame
BINK_MOVIE_ID splashBink

ENUM GAME_STATES
	GS_INVALID = -1,
	GS_INIT,
	GS_STREAM_OVERLAYS,
	GS_STREAM_WARNINGS,
	GS_FIB_SCREEN,
	GS_SOBERUP_SCREEN,
	GS_OPENING_BINK,
	GS_STREAM_MENU_ASSETS,
	GS_STREAM_TITLE_ASSETS,
	GS_TITLE_SCREEN,
	GS_MUSIC_SELECT,
	GS_TRACK_SELECT,
	GS_MULTIPLAYER_STREAM_CHALLENGER_ASSETS,
	GS_MULTIPLAYER_CHALLENGER_SCREEN,
	GS_MUTLIPLAYER_ENTER_TAG,
	GS_MULTIPLAYER_WAIT_FOR_NETWORK, // Only used in 2p mode.
	GS_STREAM_RACE_ASSETS,
	GS_RACE_SETUP,
	GS_MAIN_GAME_UPDATE,
	GS_CLEANUP_GAME_ASSETS,
	GS_LEADERBOARD_NAME_ENTRY,
	GS_LEADERBOARD_DISPLAY,
	GS_MULTIPLAYER_RESULTS_SCREEN,
	GS_MULTIPLAYER_REMATCH,
	GS_MULTIPLAYER_AWAITING_REMATCH,
	GS_MULTIPLAYER_REMATCH_DECLINED,
	GS_STREAM_MULTIPLAYER_LOST_CONNECTION_ASSETS,
	GS_MULTIPLAYER_LOST_CONNECTION,
	GS_CLEANUP_TO_TITLE
	
ENDENUM

GAME_STATES gameCurrentState

STRUCT MENU_SELECTIONS

	INT iSongSelection = 2 // Init to 2 as it's the middle value. (0-4)
	INT iTrackSelection = 2
	BOOL bEastWest = FALSE

ENDSTRUCT

MENU_SELECTIONS muMenuSelection

// Enum descriptions of each background layer in each stage
ENUM STAGES_SETS
	STAGE_INVALID = -1,
	STAGE_VICE_CITY,
	STAGE_LIBERTY_CITY,
	STAGE_SAN_ANDREAS,
	STAGE_LAS_VENTURAS,
	STAGE_SAN_FIERRO
ENDENUM

// Process Player Ped Animations
ENUM RA_PLAYER_PED_ANIM_STATE

	RA_PLAYER_PED_ANIM_IDLE,
	RA_PLAYER_PED_ANIM_START_GAME,
	RA_PLAYER_PED_ANIM_IN_GAME,
	RA_PLAYER_PED_ANIM_TAKE_LEAD,
	RA_PLAYER_PED_ANIM_HIT_CHECKPOINT,
	RA_PLAYER_PED_ANIM_LOSE_LEAD,
	RA_PLAYER_PED_ANIM_CRASH_VEH,
	RA_PLAYER_PED_ANIM_WIN,
	RA_PLAYER_PED_ANIM_LOSE,
	RA_PLAYER_PED_ANIM_BACK_TO_IDLE
		
ENDENUM

FUNC STAGES_SETS ROAD_ARCADE_GET_FIRST_STAGE_FROM_SELECT()

	IF muMenuSelection.iTrackSelection = 0
		RETURN STAGE_LIBERTY_CITY
	ENDIF

	IF muMenuSelection.iTrackSelection = 1
		RETURN STAGE_LAS_VENTURAS
	ENDIF

	IF muMenuSelection.iTrackSelection = 2
		RETURN STAGE_VICE_CITY
	ENDIF

	IF muMenuSelection.iTrackSelection = 3
		RETURN STAGE_SAN_ANDREAS
	ENDIF

	IF muMenuSelection.iTrackSelection = 4
		RETURN STAGE_SAN_FIERRO
	ENDIF
	
	RETURN STAGE_VICE_CITY

ENDFUNC

STRUCT CURVE_DATA
	
	FLOAT fZValueRef
	INT fCurvePixels
	FLOAT fCurveInStart
	FLOAT fCurveInEnd
	FLOAT fCurveOutStart
	FLOAT fCurveOutEnd

ENDSTRUCT

STRUCT HILL_DATA
	
	FLOAT fZValueRef
	
	INT iHorizonLine
	FLOAT fRoadWidth
	FLOAT fHillScale
	FLOAT fSideScale
	
	FLOAT fHillInStart
	FLOAT fHillInEnd
	FLOAT fHillOutStart
	FLOAT fHillOutEnd

ENDSTRUCT

ENUM SIDELINE_SIDES

	SIDE_LEFT,
	SIDE_RIGHT

ENDENUM

// Over road object dictionaries
// Struct detailing info on an overroad object.
STRUCT OVER_ROAD_OBJECT

	INT objID = -1
	STRING sObjTexture
	STRING sObjDict
	//FLOAT fObjectFrequency // Set to 0 to have it not appear on the given side
	VECTOR_2D vSpriteScale
	
	// Position along route to draw the object
	FLOAT fZDrawAt = -1.0
	FLOAT fZWasDrawnAt = 0.0 // Write to value 
	
	FLOAT fYOffset = 0.0
	
	BOOL bIsCheckpoint = FALSE
	BOOL bIsStart = FALSE
	BOOL bIsFinish = FALSE

ENDSTRUCT

ENUM OVER_OBJECTS_GEN_ID

	G_START,
	G_TRANSITION_CHECK,
	G_FINISH,
	G_CROWD,
	G_CHECKPOINT_01,
	G_CHECKPOINT_02
	
ENDENUM

OVER_ROAD_OBJECT genericObjects[COUNT_OF(OVER_OBJECTS_GEN_ID)]

SCRIPT_CONTROL_HOLD_TIMER schtQuitGame
SCRIPT_CONTROL_HOLD_TIMER schtQuitGamePC

//---------------PLAYER VEHICLE SUB-STATES--------------

ENUM PLAYER_STATES

	ps_countdown,
	ps_Drive,
	ps_Swerving,
	ps_Crashed,
	ps_Finish
	
ENDENUM

ENUM PLAYER_GEARS

	pgLow,
	pgHigh

ENDENUM

ENUM SLIP_STREAM_STATE

	slipINACTIVE,
	slipACTIVE,
	slipMAXED,
	slipOVERDRIVE

ENDENUM

// --------------- PLAYER OBJECT ---------------
STRUCT PLAYER_VEHICLE

	// Animations
	string sSpriteDict
	string sSpriteFrame
	
	// Sprite resolution
	int iWidth = 676
	int iHeight = 258
			
	// Location
	int iPlayerPosX = 960
	int iPlayerPosY = 844
			
	// Variables
	float 	fPlayerSpeed = 0.0
	float 	fPlayerLeftStickX = 0.0 // Store the current Left Stick X value
	float 	fMaxSpeed = 90.0
	float 	fMaxSpeedMod = 1.0
	float 	fAcceleration = 40.0
	float 	fAccelerationMod = 1.0
	float 	fAccelerationNormal = 0.0 // Need to track acceleration strength
	float 	fBrakingNormal = 0.0 // Need to track acceleration strength
	FLOAT 	fSlipTime = 0.0
	FLOAT 	fOverdriveTime = 500.0
	float 	fDrag = 25.0
	float	fPenaltyMod = 2.5
	float 	fHandling = 10.0
	FLOAT 	fTurningAngle = 0.0
	
	// Scoring
	INT iPlayerScore = 0
	INT iSpeedMultiplier = 1
	
	// Play states
	PLAYER_STATES psActiveState
	PLAYER_GEARS pgPlayerGear = pgLow
	
	// slip streaming
	SLIP_STREAM_STATE slipState = slipINACTIVE
	BOOL bWillSlipStream = FALSE // Toggle off at end of the frame
	
	// Bool to determine whther to draw the player shadow
	bool bDrawShadow = FALSE
	
	// Car is off road
	bool bOffRoad = FALSE
	
	// Variable to control the Animation of the driver
	string sDriverFrameNo
	float fAnimationSpeed = 5.0
	float fAnimationCounter = 0.0
	
	// Perfect start?
	Bool bPerfectStart = FALSE
	FLOAT fPerfectBite = 0.6
	
	// PLayer's on screen Z value and Instance count
	FLOAT fZValue = 0.0
	INT   iSpriteInstance = 0
	FLOAT fRoadCenter = 0.0
	FLOAT fCurveOffset = 0.0 // Add to World offset while a curve is happening to simulate the effect of steering
	FLOAT fCurveMultiplier = 0.0
	
	// Control player ped animation
	RA_PLAYER_PED_ANIM_STATE ePlayerPedAnimState = RA_PLAYER_PED_ANIM_IDLE
	ARCADE_CABINET_ANIM_EVENT_STRUCT ae_PedAnimData
	ARCADE_CABINET_ANIM_CLIPS animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
	ARCADE_CABINET_ANIM_CLIPS previousIdleAnim
	INT iAnimBitSet
	SCRIPT_TIMER sIdleAnimTimer
	
	// Award trackers
	Bool bCrashedOnce = FALSE
	
ENDSTRUCT

STRUCT RIVAL_PLAYER_VEHICLE

	// Position along route to draw the object
	FLOAT fZDrawAt
	BOOL bDrawThisFrame
	FLOAT fYOffset = 0.5
	
ENDSTRUCT

// Trnasition effect
ENUM TUNNEL_TRANS_STATE

	T_INACTIVE = 0,
	T_WAIT_FOR_BACKGROUND,
	T_LOAD,
	T_WAIT_FOR_RETURN,
	T_DRAW_FINISH

ENDENUM

STRUCT ROAD_ARCADE_TRANSITION_DATA

	FLOAT fTransitionOffset = 0.0
	FLOAT fTransitionOffset_Final = 0.0
	FLOAT fChangeTimer = 0.0
	
	// Cut off any side objects 
	FLOAT fZTransitionCutOff = 0.0
	
	// For Curvature calculation - between 0.0 and 1.0
	FLOAT fTransitionProgress = 0.0
	
	BOOL bSwitchCurve = FALSE
	
	TUNNEL_TRANS_STATE transition_ActiveState
	
ENDSTRUCT

ROAD_ARCADE_TRANSITION_DATA transitionData

// ANIMATION TIMERS
FLOAT challengerIconFlash
FLOAT trophyGirlAnim
FLOAT particleFxAnim
FLOAT slipStreamFxAnim

// Anim states - Finish
ENUM PLAYER_FINISH_ANIM_STATE

	FS0_INIT = 0,
	FS1_CORRECT_X,
	FS2_BEGIN_BRAKE,
	FS3_Dift_To_Stop,
	FS4_Dog_Loop,
	FS5_Trophy
	
ENDENUM

PLAYER_FINISH_ANIM_STATE pfasCurrentFinishState

ENUM DOG_ANIM_FRAMES

	// Starting from 2 as frames
	DOG_FRAME_02,
	DOG_FRAME_03,
	DOG_FRAME_04,
	DOG_FRAME_05
	
ENDENUM

DOG_ANIM_FRAMES dafCurrentDogFrame

ENUM TROPHY_GIVE_ANIM_STATE

	TROPHY_WALK,
	TROPHY_STOP
	
ENDENUM

TROPHY_GIVE_ANIM_STATE trophyGive

// ---------------- COLOUR DATA ---------------
// RGBA descriptions of different lighting styles.
RGBA_COLOUR_STRUCT rgbaOriginal
RGBA_COLOUR_STRUCT rgbaBlackOut

RGBA_COLOUR_STRUCT rgbaSanAndreasEvening
RGBA_COLOUR_STRUCT rgbaLibertyCityEvening

RGBA_COLOUR_STRUCT rgbaNightContrast

RGBA_COLOUR_STRUCT rgbaVicePurple
RGBA_COLOUR_STRUCT rgbaFeirroRed
RGBA_COLOUR_STRUCT rgbaAndreasOrange
RGBA_COLOUR_STRUCT rgbaVenturasBlue
RGBA_COLOUR_STRUCT rgbaLibertyTaxi

RGBA_COLOUR_STRUCT rgbaAkedo

RGBA_COLOUR_STRUCT rgbaFrameColour

PROC ROAD_ARCADE_TOD_COLOUR_STRUCTS()

	INIT_RGBA_STRUCT(rgbaOriginal, 255, 255, 255)
	INIT_RGBA_STRUCT(rgbaBlackOut, 0, 0, 0)
	
	INIT_RGBA_STRUCT(rgbaSanAndreasEvening, 255, 170, 100)
	INIT_RGBA_STRUCT(rgbaLibertyCityEvening, 199, 102, 70)

	INIT_RGBA_STRUCT(rgbaNightContrast, 110, 84, 70)
	
	INIT_RGBA_STRUCT(rgbaAkedo,141, 239, 255)
	
	// Road Colours
	INIT_RGBA_STRUCT(rgbaVicePurple, 500, 160, 500)
	INIT_RGBA_STRUCT(rgbaFeirroRed, 439, 14, 33)
	INIT_RGBA_STRUCT(rgbaAndreasOrange, 230, 156, 51)
	INIT_RGBA_STRUCT(rgbaVenturasBlue, 161, 231, 241)
	INIT_RGBA_STRUCT(rgbaLibertyTaxi, 218, 219, 39)
	
ENDPROC

// ------------INPUT HELPERS ------------
FUNC BOOL RA_INPUT_IS_ACCEL_JUST_PRESSED(ARCADE_GAMES_SOUND soundToPlay = ROAD_ARCADE_AUDIO_EFFECT_MENU_CONFIRM)

	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB) // Used in menus so we can use the gear change prompt too.
		ARCADE_GAMES_SOUND_PLAY(soundToPlay)
	
		RETURN TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			ARCADE_GAMES_SOUND_PLAY(soundToPlay)
	
			RETURN TRUE
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL RA_INPUT_IS_ACCEL_DOWN(BOOL bIncludeTrigger = TRUE)

	BOOL bReturn = FALSE

	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		bReturn = TRUE
	ENDIF
	
	IF bIncludeTrigger
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "RA_INPUT_IS_ACCEL_DOWN - Cellphone up is being held on keyboard")
			bReturn = TRUE
		ENDIF
	
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL RA_INPUT_IS_BRAKE_JUST_PRESSED()

	BOOL bReturn = FALSE

	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LB)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		bReturn = TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)

		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DUCK)
			bReturn = TRUE
		ENDIF
	
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL RA_INPUT_IS_BRAKE_DOWN(BOOL bIncludeTrigger = TRUE)

	BOOL bReturn = FALSE
		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	
	IF bIncludeTrigger
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT)
	OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LB)
		bReturn = TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		
		IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DUCK)
		OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			bReturn = TRUE
		ENDIF

	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL RA_INPUT_IS_STEER_LEFT_PRESSED(BOOL bHeld = FALSE)
	
	BOOL bSteer = FALSE

	// Check if just pressed
	IF !bHeld

		bSteer = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
		bSteer = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
		
		IF !bSteer
		AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_LEFT_ONLY)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
				bSteer = TRUE
			ENDIF
		ENDIF

		RETURN bSteer
	ENDIF
	
	// Check if held
	bSteer = IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
		
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "RA_INPUT_IS_STEER_LEFT_PRESSED - Dpad LEFT down: ", bSteer)
		
	IF !bSteer
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_LEFT_ONLY)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
			bSteer = TRUE
		ENDIF
	ENDIF

	RETURN bSteer

ENDFUNC

FUNC BOOL RA_INPUT_IS_STEER_RIGHT_PRESSED(BOOL bHeld = FALSE)
	
	BOOL bSteer = FALSE

	// Check if just pressed
	IF !bHeld

		bSteer = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
		bSteer = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
		
		IF !bSteer
		AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_RIGHT_ONLY)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
				bSteer = TRUE
			ENDIF
		ENDIF

		RETURN bSteer
	ENDIF
	
	// Check if held
	bSteer = IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "RA_INPUT_IS_STEER_LEFT_PRESSED - Dpad RIGHT down: ", bSteer)
	
	IF !bSteer
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_RIGHT_ONLY)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
			bSteer = TRUE
		ENDIF
	ENDIF

	RETURN bSteer

ENDFUNC

FUNC BOOL ROAD_ARCADE_IS_QUIT_BEING_HELD()

	INT iQuitTime = 1000

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

		
		IF IS_CONTROL_PRESSED(schtQuitGamePC.control, schtQuitGamePC.action)
		OR (IS_DISABLED_CONTROL_PRESSED(schtQuitGamePC.control, schtQuitGamePC.action))
			DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), schtQuitGamePC.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
		ELSE
			DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
		ENDIF

	ELSE
		
		
		IF IS_CONTROL_PRESSED(schtQuitGame.control, schtQuitGame.action)
		OR (IS_DISABLED_CONTROL_PRESSED(schtQuitGame.control, schtQuitGame.action))
			DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), schtQuitGame.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
		ELSE
			DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
		ENDIF
	
	ENDIF

	IF IS_CONTROL_HELD(schtQuitGame, DEFAULT, iQuitTime)
		RETURN TRUE
	ENDIF
	
	IF IS_PC_VERSION()
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_HELD(schtQuitGamePC, DEFAULT, iQuitTime)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC 

STRUCT ROAD_ARCADE_ANALOGUE_STICK_MENU_NAV

	BOOL bLeftStickMovedLeft
	BOOL bLeftStickMovedRight
	BOOL bLeftStickMovedUp
	BOOL bLeftStickMovedDown

ENDSTRUCT

ROAD_ARCADE_ANALOGUE_STICK_MENU_NAV roadArcadeStickNavData

PROC ROAD_ARCADE_CHECK_LEFT_STICK_NEUTRAL()
	
	IF roadArcadeStickNavData.bLeftStickMovedLeft
	AND GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > -0.1
		roadArcadeStickNavData.bLeftStickMovedLeft = FALSE
	ENDIF
	
	IF roadArcadeStickNavData.bLeftStickMovedRight
	AND GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < 0.1
		roadArcadeStickNavData.bLeftStickMovedRight = FALSE
	ENDIF
	
	IF roadArcadeStickNavData.bLeftStickMovedUp
	AND GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > -0.1
		roadArcadeStickNavData.bLeftStickMovedUp = FALSE
	ENDIF
	
	IF roadArcadeStickNavData.bLeftStickMovedDown
	AND GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0.1
		roadArcadeStickNavData.bLeftStickMovedDown = FALSE
	ENDIF
	
ENDPROC

FUNC BOOL RA_INPUT_IS_UP_PRESSED()

	Bool bReturn = FALSE
	
	bReturn = IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)

	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < -0.1
	AND !roadArcadeStickNavData.bLeftStickMovedUp
		roadArcadeStickNavData.bLeftStickMovedUp = TRUE
		bReturn = true
	ENDIF

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_UP_ONLY)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
			bReturn = true
		ENDIF
		
	ENDIF
	
	return bReturn
	
ENDFUNC

FUNC BOOL RA_INPUT_IS_RIGHT_PRESSED()

	BOOL bReturn = FALSE

	bReturn = IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	
	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.1
	AND !roadArcadeStickNavData.bLeftStickMovedRight
		roadArcadeStickNavData.bLeftStickMovedRight = TRUE
		bReturn = true
	ENDIF
	
	IF !bReturn
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_RIGHT_ONLY)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
			bReturn = TRUE
		ENDIF
	ENDIF

	RETURN bReturn

ENDFUNC

FUNC BOOL RA_INPUT_IS_DOWN_PRESSED()

	BOOL bReturn = FALSE

	bReturn = IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
	
	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.1
	AND !roadArcadeStickNavData.bLeftStickMovedDown
		roadArcadeStickNavData.bLeftStickMovedDown = TRUE
		bReturn = true
	ENDIF
	
	IF !bReturn
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_DOWN_ONLY)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
			bReturn = TRUE
		ENDIF
	ENDIF

	RETURN bReturn
	
ENDFUNC

FUNC BOOL RA_INPUT_IS_LEFT_PRESSED()

	BOOL bReturn = FALSE

	bReturn = IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
	
	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.1
	AND !roadArcadeStickNavData.bLeftStickMovedLeft
		roadArcadeStickNavData.bLeftStickMovedLeft = TRUE
		bReturn = true
	ENDIF
	
	IF !bReturn
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_LEFT_ONLY)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
			bReturn = TRUE
		ENDIF
		
	ENDIF

	RETURN bReturn
		
ENDFUNC

FUNC BOOL RA_INPUT_IS_LEFT_DOWN()

	BOOL bReturn = FALSE

	bReturn = IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
	
	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.1
		bReturn = true
	ENDIF
	
	IF !bReturn
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_LEFT_ONLY)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
			bReturn = TRUE
		ENDIF
		
	ENDIF

	RETURN bReturn
		
ENDFUNC

FUNC BOOL RA_INPUT_IS_RIGHT_DOWN()

	BOOL bReturn = FALSE

	bReturn = IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	
	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.1
		bReturn = true
	ENDIF
	
	IF !bReturn
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_RIGHT_ONLY)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
			bReturn = TRUE
		ENDIF
	ENDIF

	RETURN bReturn
ENDFUNC

//---------------Rendering Helpers-----------------
STRUCT ROAD_ARCADE_SPRITE

	STRING sDict
	TEXT_LABEL_63 sTexture
	VECTOR_2D vPos
	VECTOR_2D vScale
	RGBA_COLOUR_STRUCT rgbaColour
	FLOAT fRotation = 0.0
	FLOAT fZvalue = 0.0
	BOOL  bBeenDrawn = FALSE
	
ENDSTRUCT

PROC ARCADE_CABINET_REAL_RESOLUTION_CORRECTED(VECTOR_2D &vPos, VECTOR_2D &vScale)

	vPos = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vPos)
	vScale = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vScale)

	vPos.x = APPLY_ASPECT_MOD_TO_X(vPos.x)
	vScale.X *= fAspectRatioModifier

	vPos.x *= iScreenX
	vPos.y *= iScreenY
	
	vScale.x *= iScreenX
	vScale.y *= iScreenY
	
	vPos.x = TO_FLOAT(FLOOR(vPos.x))
	vPos.y = TO_FLOAT(FLOOR(vPos.y))
	
	vScale.x = TO_FLOAT(FLOOR(vScale.x))
	vScale.y = TO_FLOAT(FLOOR(vScale.y))
	
//	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ARCADE_CABINET_REAL_RESOLUTION_CORRECTED - Sprite pos x: ", vPos.x)
//	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ARCADE_CABINET_REAL_RESOLUTION_CORRECTED - Sprite pos y: ", vPos.y)
//	
//	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ARCADE_CABINET_REAL_RESOLUTION_CORRECTED - Sprite Scale x: ", vScale.x)
//	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ARCADE_CABINET_REAL_RESOLUTION_CORRECTED - Sprite Scale y: ", vScale.y)

	vPos.x /= iScreenX
	vPos.y /= iScreenY
	
	vScale.x /= iScreenX
	vScale.y /= iScreenY
	
ENDPROC

/// PURPOSE:
///    Draws a sprite from screenspace values
///    Note: Use ARCADE_CABINET_DRAW_SPRITE_ROTATED for non-square rotated sprites
PROC ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(STRING stTextureDict, STRING stTexture, VECTOR_2D vCenter, VECTOR_2D vScale, FLOAT fRotation, RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[GRID_ARCADE] [JS] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF

	

#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_SPRITE(stTextureDict, stTexture, APPLY_ASPECT_MOD_TO_X(vCenter.X) * g_fMinimizeArcadeCabinetCentreX, vCenter.Y * g_fMinimizeArcadeCabinetCentreY, vScale.X * fAspectRatioModifier * g_fMinimizeArcadeCabinetCentreScale, vScale.Y * g_fMinimizeArcadeCabinetCentreScale, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	ELSE
#ENDIF
		
		ARCADE_CABINET_REAL_RESOLUTION_CORRECTED(vCenter, vScale)
		
		DRAW_SPRITE(stTextureDict, stTexture, vCenter.X, vCenter.Y, vScale.X, vScale.Y, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
#IF IS_DEBUG_BUILD
	iSpritesDrawnThisFrame++
#ENDIF
ENDPROC

FUNC BOOL ROAD_ARCADE_DRAW_FORMATTED_SPRITE(ROAD_ARCADE_SPRITE &spriteToDraw)

	IF !spriteToDraw.bBeenDrawn
		VECTOR_2D vPos = spriteToDraw.vPos
		VECTOR_2D vScale = spriteToDraw.vScale

		vPos.x -= (vPos.x%4.0)
		vPos.y -= (vPos.y%4.0)

		vScale.x -= (vScale.x%4.0)
		vScale.y -= (vScale.y%4.0)
		
		// Off to the sides cull them
		IF vPos.x < 200
			RETURN FALSE
		ENDIF
	
		IF vPos.x > cfBASE_SCREEN_WIDTH - 200
			RETURN FALSE
		ENDIF
	
		// Safety check
		IF NOT IS_STRING_NULL_OR_EMPTY(spriteToDraw.sDict)
		AND NOT IS_STRING_NULL_OR_EMPTY(spriteToDraw.sTexture)
					
			ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(
					spriteToDraw.sDict,
					TEXT_LABEL_TO_STRING(spriteToDraw.sTexture),
					vPos,
					vScale,
					spriteToDraw.fRotation,
					spriteToDraw.rgbaColour)
					
		ENDIF
				
		spriteToDraw.bBeenDrawn	= TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(STRING sDict, STRING sTexture, VECTOR_2D vPos, VECTOR_2D vScale, FLOAT fRotation, RGBA_COLOUR_STRUCT rgbaColour)

	// Fix subpixel drawing
	vPos.x -= (vPos.x%4.0)
	vPos.y -= (vPos.y%4.0)

	// Fix subpixel scaling
	vScale.x -= (vScale.x%4.0)
	vScale.y -= (vScale.y%4.0)
		
	// Off to the sides cull them
	IF vPos.x < 150
		RETURN FALSE
	ENDIF

	IF vPos.x > cfBASE_SCREEN_WIDTH - 150
		RETURN FALSE
	ENDIF

	// Safety check
	IF NOT IS_STRING_NULL_OR_EMPTY(sDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(sTexture)

		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(
				sDict,
				sTexture,
				vPos,
				vScale,
				fRotation,
				rgbaColour)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

CONST_INT iRA_MaxNumberOfSprites 6

STRUCT ROAD_ARCADE_Z_BUFFER
	ROAD_ARCADE_SPRITE rasSprites[iRA_MaxNumberOfSprites]
	INT iInstanceCount = 0	
ENDSTRUCT

PROC ROAD_ARCADE_Z_BUFFER_INIT(ROAD_ARCADE_Z_BUFFER &roadArcade_ZBuffer)

	roadArcade_ZBuffer.iInstanceCount = 0
	
	INT i
	FOR i = 0 TO iRA_MaxNumberOfSprites-1
		roadArcade_ZBuffer.rasSprites[i].fZvalue = 0.0
		roadArcade_ZBuffer.rasSprites[i].sDict = ""
		roadArcade_ZBuffer.rasSprites[i].sTexture = ""
		roadArcade_ZBuffer.rasSprites[i].rgbaColour = rgbaOriginal
		roadArcade_ZBuffer.rasSprites[i].fRotation = 0.0
		roadArcade_ZBuffer.rasSprites[i].vPos = INIT_VECTOR_2D(0.0,0.0)
		roadArcade_ZBuffer.rasSprites[i].vScale = INIT_VECTOR_2D(0.0,0.0)
	ENDFOR

ENDPROC

PROC ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(ROAD_ARCADE_Z_BUFFER &roadArcade_ZBuffer, 
	STRING sDict, 
	TEXT_LABEL_63 sTexture,
	VECTOR_2D vPos,
	VECTOR_2D vScale,
	RGBA_COLOUR_STRUCT rgbaColour, 
	FLOAT fRotation = 0.0,
	FLOAT fZvalue = 0.0,
	BOOL  bBeenDrawn = FALSE)
	
	IF IS_STRING_NULL_OR_EMPTY(sDict)
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sTexture)
		EXIT
	ENDIF
	
	ROAD_ARCADE_SPRITE spriteToAdd

	spriteToAdd.sDict = sDict
	spriteToAdd.sTexture = sTexture
	spriteToAdd.vPos = vPos
	spriteToAdd.vScale = vScale
	spriteToAdd.rgbaColour = rgbaColour
	spriteToAdd.fRotation = fRotation
	spriteToAdd.fZvalue = fZvalue
	spriteToAdd.bBeenDrawn = bBeenDrawn

	IF roadArcade_ZBuffer.iInstanceCount < iRA_MaxNumberOfSprites
		roadArcade_ZBuffer.rasSprites[roadArcade_ZBuffer.iInstanceCount] = spriteToAdd
		roadArcade_ZBuffer.iInstanceCount++
	ENDIF

ENDPROC

// --------------- TICKER OBJECTS ---------------
// HUD Ticker control

STRUCT HUD_TICKER_OBJECT
	STRING sDict
	STRING sTexture
	FLOAT xOffset = 0.0
	FLOAT fDelayTimer = 0.0
	FLOAT fFlashTimer = 0.0
	
	BOOL bActive
	VECTOR_2D vScale
	
ENDSTRUCT

HUD_TICKER_OBJECT tickerCurrentObject
HUD_TICKER_OBJECT warningCurrentObject

PROC ROAD_ARCADE_RESET_TICKER_OBJ()
	HUD_TICKER_OBJECT empty
	tickerCurrentObject = empty
ENDPROC

PROC ROAD_ARCADE_RESET_WARNING_OBJ()
	HUD_TICKER_OBJECT empty
	warningCurrentObject = empty
ENDPROC

PROC ROAD_ARCADE_INIT_TICKER_OBJ(STRING sDict, STRING sTextureToUse, VECTOR_2D vScale)

	HUD_TICKER_OBJECT empty
	tickerCurrentObject = empty
	
	tickerCurrentObject.bActive = TRUE
	tickerCurrentObject.sDict = sDict
	tickerCurrentObject.sTexture = sTextureToUse
	tickerCurrentObject.vScale = vScale
	tickerCurrentObject.fDelayTimer = 0
	tickerCurrentObject.xOffset = (0.0 - vScale.x/2.0)
	
ENDPROC

PROC ROAD_ARCADE_INIT_WARNING_OBJ(STRING sDict, STRING sTextureToUse, VECTOR_2D vScale)

	HUD_TICKER_OBJECT empty
	warningCurrentObject = empty
	
	warningCurrentObject.bActive = TRUE
	warningCurrentObject.sDict = sDict
	warningCurrentObject.sTexture = sTextureToUse
	warningCurrentObject.vScale = vScale
	warningCurrentObject.fDelayTimer = 0
	warningCurrentObject.xOffset = cfBASE_SCREEN_WIDTH/2.0
	
ENDPROC

PROC ROAD_ARCADE_HANDLE_TICKER_OBJECT()

	IF tickerCurrentObject.bActive

		IF tickerCurrentObject.xOffset >= (cfBASE_SCREEN_WIDTH/2.0)
			tickerCurrentObject.fDelayTimer += (0.0+@5000)		
		ENDIF
	
		IF tickerCurrentObject.fDelayTimer = 0.0
		AND tickerCurrentObject.xOffset <= (cfBASE_SCREEN_WIDTH/2.0)
			tickerCurrentObject.xOffset += (0.0+@1000)
		ENDIF
	
		IF tickerCurrentObject.fDelayTimer > 1500.0
		AND tickerCurrentObject.xOffset >= (cfBASE_SCREEN_WIDTH/2.0)
			tickerCurrentObject.xOffset += (0.0+@1000)
		ENDIF
	
		IF tickerCurrentObject.xOffset > cfBASE_SCREEN_WIDTH + tickerCurrentObject.vScale.x
			tickerCurrentObject.xOffset = -tickerCurrentObject.vScale.x
			tickerCurrentObject.sTexture = ""
			tickerCurrentObject.fDelayTimer = 0.0
			tickerCurrentObject.bActive = FALSE
			
			ROAD_ARCADE_RESET_TICKER_OBJ()
			EXIT
		ENDIF
										
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tickerCurrentObject.sTexture)
	
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(
			tickerCurrentObject.sDict,
			tickerCurrentObject.sTexture,
			INIT_VECTOR_2D(tickerCurrentObject.xOffset, cfBASE_SCREEN_HEIGHT/2.0),
			tickerCurrentObject.vScale,
			0.0,
			rgbaOriginal)
			
	ENDIF

ENDPROC

PROC ROAD_ARCADE_HANDLE_WARNING_OBJECT()
	// Flashing version

	warningCurrentObject.xOffset = (cfBASE_SCREEN_WIDTH/2.0)
	warningCurrentObject.fDelayTimer += (0.0+@1000)
	
	IF (warningCurrentObject.fDelayTimer%1000) <= 500
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(
			warningCurrentObject.sDict,
			warningCurrentObject.sTexture,
			INIT_VECTOR_2D(warningCurrentObject.xOffset, cfBASE_SCREEN_HEIGHT/2.0),
			warningCurrentObject.vScale,
			0.0,
			rgbaFeirroRed)
	ENDIF
	
	IF warningCurrentObject.fDelayTimer >= 1750
		warningCurrentObject.xOffset = -warningCurrentObject.vScale.x
		warningCurrentObject.sTexture = ""
		warningCurrentObject.fDelayTimer = 0.0
		warningCurrentObject.bActive = FALSE
		
		ROAD_ARCADE_RESET_WARNING_OBJ()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Take an array of floats an perform divide & conquer quick sort.
///    Not stable
///	   O(log(n))
///	   O(n2) time, but typically O(n-log(n)) time
/// PARAMS:
///    fArray - Passed in array
///    iLeft - Start element
///    iRight - End element (length)
PROC QUICK_SORT_FLOAT_INV(FLOAT &fArray[], INT iLeft, INT iRight)

	INT i, j
	FLOAT p = fArray[((i + j) / 2)]
	FLOAT q
	i = iRight
	j = iLeft

	WHILE (i > j)
	
		// Ignore 0.0 values
		WHILE ((fArray[i] > p) AND (i > iRight))

			i--
		ENDWHILE
		
		WHILE ((p > fArray[j]) AND (j < iLeft))

			j++
		ENDWHILE
		
		IF (i > j)

			IF (fArray[i] > fArray[j])
				q = fArray[i]
				fArray[i] = fArray[j]
				fArray[j] = q
			ENDIF
			

			i--
			j++
		ENDIF
	
	ENDWHILE
	
	IF (i >= 0 AND i > iRight)
		QUICK_SORT_FLOAT_INV(fArray, i, iRight)
	ENDIF
	
	IF (j >= 0 AND iLeft > j)
		QUICK_SORT_FLOAT_INV(fArray, iLeft, j)
	ENDIF

ENDPROC

PROC ROAD_ARCADE_Z_BUFFER_SORT_ARRAY_PIECES(ROAD_ARCADE_Z_BUFFER &roadArcade_ZBuffer, FLOAT &spriteZValues[])
	
	int i = 0 // Loop counter
	FOR i = 0 to (iRA_MaxNumberOfSprites-1) // iRA_MaxNumberOfSprites acts as a sprite limiter
		spriteZValues[i] = roadArcade_ZBuffer.rasSprites[i].fZValue
	ENDFOR

	QUICK_SORT_FLOAT_INV(spriteZValues, 0, roadArcade_ZBuffer.iInstanceCount-1)
			
ENDPROC

PROC ROAD_ARCADE_Z_BUFFER_RENDER(ROAD_ARCADE_Z_BUFFER &roadArcade_ZBuffer)

	// Don't sort if the value at Index 0 is uninitialised
	IF roadArcade_ZBuffer.rasSprites[0].fZvalue = 0.0
		EXIT
	ENDIF

	FLOAT fZvaluesSorted[iRA_MaxNumberOfSprites]

	ROAD_ARCADE_Z_BUFFER_SORT_ARRAY_PIECES(roadArcade_ZBuffer, fZvaluesSorted)

	INT i
	INT j
	FOR i = 0 TO iRA_MaxNumberOfSprites-1
		
		IF roadArcade_ZBuffer.rasSprites[i].fZvalue = fZvaluesSorted[j]
			IF !roadArcade_ZBuffer.rasSprites[i].bBeenDrawn
				ROAD_ARCADE_DRAW_FORMATTED_SPRITE(roadArcade_ZBuffer.rasSprites[i])
				j++
				i = -1
			ENDIF
		ENDIF
		
		IF j >= iRA_MaxNumberOfSprites
			BREAKLOOP
		ENDIF
	
	ENDFOR

ENDPROC

// ------------------------- MENU SUPPORT  --------------------------------
PROC RA_WRITE_STRING_USING_TEXT_SPRITES(STRING sTextToDisplay, VECTOR_2D vDisplayPos, INT iSizeMultiplier = 1, BOOL bCenterText = FALSE, INT iThinCharacters = 0, INT iWideCharacters = 0, INT iLengthOverride = 0)
	
	// Moddable variable
	VECTOR_2D vDisplayFinalPos = vDisplayPos	
	VECTOR_2D vBaseLetterScale = INIT_VECTOR_2D(32, 64)
	
	INT iLengthOfString = GET_LENGTH_OF_LITERAL_STRING(sTextToDisplay)
	
	IF iLengthOverride > 0
		iLengthOfString = iLengthOverride
	ENDIF
	
	IF bCenterText
		vDisplayFinalPos.X = (cfBASE_SCREEN_WIDTH/2.0)
		vDisplayFinalPos.X -= ROUND((32.0*iSizeMultiplier)*((iLengthOfString)/2.0))
		
		IF iThinCharacters > 0
			vDisplayFinalPos.X += (iThinCharacters*20.0)
		ENDIF
		
		IF iWideCharacters > 0
			vDisplayFinalPos.X -= (iWideCharacters*16.0)
		ENDIF
		
	ENDIF
		
	FLOAT fCarryOverX = 0
		
	INT i = 0
	FOR i = 0 TO iLengthOfString-1
		
		TEXT_LABEL_63 sLetterTexture = "HUD_LETTER_"
		TEXT_LABEL_63 sCharacter = "EXCLAMATION"

		sCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sTextToDisplay, i, i+1)	
		
		VECTOR_2D vScaleFinal = MULTIPLY_VECTOR_2D(vBaseLetterScale, TO_FLOAT(iSizeMultiplier))
		
		IF ARE_STRINGS_EQUAL(sCharacter, " ")
			vDisplayFinalPos.x += vBaseLetterScale.x // Add the size of a sprite Before displaying the next one.
			RELOOP
		ENDIF
		
		// Smaller and Wider font
		IF ARE_STRINGS_EQUAL(sCharacter, "M")
		OR ARE_STRINGS_EQUAL(sCharacter, "W")
			vScaleFinal.x = (48.0*iSizeMultiplier)
			vDisplayFinalPos.x += (vScaleFinal.x/8)
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ".")
		OR ARE_STRINGS_EQUAL(sCharacter, ":")
		OR ARE_STRINGS_EQUAL(sCharacter, "!")
			vScaleFinal.x = (20.0*iSizeMultiplier)
			vDisplayFinalPos.x -= (vScaleFinal.x/8)
		ENDIF
		
		// Check if Character is a misc
		IF ARE_STRINGS_EQUAL(sCharacter, "!")
			sCharacter = "EXCLAMATION_SHORT"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, "?")
			sCharacter = "QUESTION_MARK"
		ENDIF

		IF ARE_STRINGS_EQUAL(sCharacter, ".")
			sCharacter = "FULL_STOP"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ":")
			sCharacter = "COLON_SHORT"
		ENDIF
		
		// WIDER CHARACTERS
		IF ARE_STRINGS_EQUAL(sCharacter, "M")
			sCharacter = "M_WIDE"
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, "W")
			sCharacter = "W_WIDE"
		ENDIF
				
		sLetterTexture += sCharacter

		vDisplayFinalPos.x += (fCarryOverX)
		fCarryOverX = 0
				
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, 
			sLetterTexture, vDisplayFinalPos, vScaleFinal, 0.0, rgbaOriginal)
			
		IF ARE_STRINGS_EQUAL(sCharacter, "M_WIDE")
		OR ARE_STRINGS_EQUAL(sCharacter, "W_WIDE")
			fCarryOverX = (vScaleFinal.x/8)
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCharacter, ".")
		OR ARE_STRINGS_EQUAL(sCharacter, ":")
		OR ARE_STRINGS_EQUAL(sCharacter, "!")
			fCarryOverX = -(vScaleFinal.x/8)
		ENDIF
		
		IF vScaleFinal.x  > (vBaseLetterScale.x*iSizeMultiplier)
			vScaleFinal.x = (vBaseLetterScale.x*iSizeMultiplier)
		ENDIF
		
		vDisplayFinalPos.x += vScaleFinal.x // Add the size of a sprite Before displaying the next one.
		
		
	ENDFOR
	
ENDPROC

PROC RA_DRAW_TITLE_SCREEN_OBJ()

	// Draw Title
	STRING sTitle = "HUD_INTRO_TITLE_02"
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		sTitle = "HUD_INTRO_TITLE_02_BIKE"
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sTitle = "HUD_INTRO_TITLE_02_TRUCK"
	ENDIF
	
	VECTOR_2D vTitlePos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0-88)
	VECTOR_2D vTitleScale = INIT_VECTOR_2D(1024,376)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_TitleAssets, sTitle, vTitlePos, vTitlescale, 0.0, rgbaOriginal)
	
	// Draw Start prompt
	STRING sStart = "HUD_BUTTON_START"
	
	VECTOR_2D vStartPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+156)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(520, 100)
	
	IF TIMERA() >= 1000
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_TitleAssets, sStart, vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 2000
		SETTIMERA(0)
	ENDIF

ENDPROC

PROC RA_DRAW_CHALLENGE_SCREEN_OBJ()

	// Draw message
	RA_WRITE_STRING_USING_TEXT_SPRITES("HERE COMES A NEW CHALLENGER!!",
		INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT/2.0)-272), 1, TRUE, 2, 2)
	
	// Draw Start prompt
	STRING sStart = "HUD_BUTTON_START"
	
	VECTOR_2D vStartPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+124)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(520, 100)
	
	IF TIMERA() >= 1000
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_TitleAssets, sStart, vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 2000
		SETTIMERA(0)
	ENDIF

ENDPROC

PROC RA_UPDATE_SELECT(INT &iToUpdate, ARCADE_GAMES_SOUND navSound = ROAD_ARCADE_AUDIO_EFFECT_MENU_NAV)
	
	ROAD_ARCADE_CHECK_LEFT_STICK_NEUTRAL()
	
	// Update off left stick Gait.	
	IF RA_INPUT_IS_LEFT_PRESSED()
		ARCADE_GAMES_SOUND_PLAY(navSound)
		iToUpdate++
	ENDIF


	IF RA_INPUT_IS_RIGHT_PRESSED()
		ARCADE_GAMES_SOUND_PLAY(navSound)
		iToUpdate--
	ENDIF
	
ENDPROC


FUNC STRING RA_GET_SELECTED_SONG_TEX()

	STRING strSong = "HUD_BUTTON_AUDIO_SONG_ONE"

	// Prevent selection going outside limits
	IF muMenuSelection.iSongSelection > 8
		muMenuSelection.iSongSelection = 0
	ENDIF

	IF muMenuSelection.iSongSelection < 0
		muMenuSelection.iSongSelection = 8
	ENDIF

	IF muMenuSelection.iSongSelection = 0
		strSong = "HUD_BUTTON_AUDIO_SONG_FIVE"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_OVERDRIVE)
	ENDIF

	IF muMenuSelection.iSongSelection = 1
		strSong = "HUD_BUTTON_AUDIO_SONG_FOUR"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_AFTERBURN)
	ENDIF

	IF muMenuSelection.iSongSelection = 2
		strSong = "HUD_BUTTON_AUDIO_SONG_ONE"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_VICE_LIGHTS)
	ENDIF

	IF muMenuSelection.iSongSelection = 3
		strSong = "HUD_BUTTON_AUDIO_SONG_TWO"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_LS_NIGHTS)
	ENDIF

	IF muMenuSelection.iSongSelection = 4
		strSong = "HUD_BUTTON_AUDIO_SONG_THREE"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_RENEGADE)
	ENDIF

	IF muMenuSelection.iSongSelection = 5
		strSong = "HUD_BUTTON_AUDIO_SONG_SIX"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_HOT_PURSUIT)
	ENDIF

	IF muMenuSelection.iSongSelection = 6
		strSong = "HUD_BUTTON_AUDIO_SONG_SEVEN"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_RED_VELVET)
	ENDIF

	IF muMenuSelection.iSongSelection = 7
		strSong = "HUD_BUTTON_AUDIO_SONG_EIGHT"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_TURBO_BOOST)
	ENDIF

	IF muMenuSelection.iSongSelection = 8
		strSong = "HUD_BUTTON_AUDIO_SONG_NINE"
		ROAD_ARCADE_AUDIO_START_RADIO_LOOP(ROAD_ARCADE_AUDIO_EFFECT_RADIO_UNDERCOVER)
	ENDIF
	
	RETURN strSong

ENDFUNC

FUNC STRING RA_GET_SELECTED_TRACK_TEX()

	STRING strTrack = "HUD_TRACK_SELECT_TRACK_04"

	// Prevent selection going outside limits
	IF muMenuSelection.iTrackSelection > 4
		muMenuSelection.iTrackSelection = 0
	ENDIF

	IF muMenuSelection.iTrackSelection < 0
		muMenuSelection.iTrackSelection = 4
	ENDIF

	IF muMenuSelection.iTrackSelection = 0
		strTrack = "HUD_TRACK_SELECT_TRACK_04"
	ENDIF

	IF muMenuSelection.iTrackSelection = 1
		strTrack = "HUD_TRACK_SELECT_TRACK_05"
	ENDIF

	IF muMenuSelection.iTrackSelection = 2
		strTrack = "HUD_TRACK_SELECT_TRACK_01"
	ENDIF

	IF muMenuSelection.iTrackSelection = 3
		strTrack = "HUD_TRACK_SELECT_TRACK_02"
	ENDIF

	IF muMenuSelection.iTrackSelection = 4
		strTrack = "HUD_TRACK_SELECT_TRACK_03"
	ENDIF

	RETURN strTrack

ENDFUNC

FUNC STRING RA_GET_SELECTED_TRACK_TAG_TEX()

	STRING strTrack = "HUD_INTRO_SELECT_MENU_CITY_TAG_04"

	// Prevent selection going outside limits
	IF muMenuSelection.iTrackSelection > 4
		muMenuSelection.iTrackSelection = 0
	ENDIF

	IF muMenuSelection.iTrackSelection < 0
		muMenuSelection.iTrackSelection = 4
	ENDIF

	IF muMenuSelection.iTrackSelection = 0
		strTrack = "HUD_INTRO_SELECT_MENU_CITY_TAG_04"
	ENDIF

	IF muMenuSelection.iTrackSelection = 1
		strTrack = "HUD_INTRO_SELECT_MENU_CITY_TAG_05"
	ENDIF

	IF muMenuSelection.iTrackSelection = 2
		strTrack = "HUD_INTRO_SELECT_MENU_CITY_TAG_01"
	ENDIF

	IF muMenuSelection.iTrackSelection = 3
		strTrack = "HUD_INTRO_SELECT_MENU_CITY_TAG_02"
	ENDIF

	IF muMenuSelection.iTrackSelection = 4
		strTrack = "HUD_INTRO_SELECT_MENU_CITY_TAG_03"
	ENDIF

	RETURN strTrack

ENDFUNC

STRUCT RA_SCOOTIN_DOG

	STRING sDict 
	STRING sTexture
	STRING sActiveFrame
	
	SIDELINE_SIDES scootDirection
	
	VECTOR_2D vScale
	VECTOR_2D vPos
	
	FLOAT fAnimSpeed = 750.0
	FLOAT fAnimTimer = 0.0

	FLOAT fXOffset = 0.0
	FLOAT fMaxAnimationOffset = 120.0
	FLOAT fFrameChangeStep = 12.0
	
ENDSTRUCT

RA_SCOOTIN_DOG titleScootinDog

PROC RA_INIT_DOG_MENU()

	titleScootinDog.sDict = sDICT_SelectMenu
	titleScootinDog.sTexture = "HUD_INTRO_SELECT_MENU_DOG_"
	titleScootinDog.sActiveFrame = "FRAME_01"
	titleScootinDog.fAnimTimer = 0
	titleScootinDog.scootDirection = SIDE_LEFT
	
	titleScootinDog.vScale = INIT_VECTOR_2D(112.0, 108.0)
	titleScootinDog.vPos = INIT_VECTOR_2D(1260.0, 500.0)
ENDPROC

PROC RA_SCOOTIN_DOG_UPDATE()

	titleScootinDog.fAnimTimer += 0.0+@(1000)
	
	IF titleScootinDog.fAnimTimer >= titleScootinDog.fAnimSpeed
	
		titleScootinDog.fAnimTimer = 0
	
		IF ARE_STRINGS_EQUAL(titleScootinDog.sActiveFrame, "FRAME_01")
			titleScootinDog.sActiveFrame = "FRAME_02"
		ELSE
			titleScootinDog.sActiveFrame = "FRAME_01"
		ENDIF
		
		IF titleScootinDog.scootDirection = SIDE_RIGHT
			titleScootinDog.fXOffset += titleScootinDog.fFrameChangeStep
			
			IF titleScootinDog.fXOffset >= titleScootinDog.fMaxAnimationOffset
				titleScootinDog.vScale.x = ABSF(titleScootinDog.vScale.x)
				titleScootinDog.scootDirection = SIDE_LEFT
			ENDIF
		ENDIF
		
		IF titleScootinDog.scootDirection = SIDE_LEFT
			titleScootinDog.fXOffset -= titleScootinDog.fFrameChangeStep
			
			IF titleScootinDog.fXOffset <= -titleScootinDog.fMaxAnimationOffset
				titleScootinDog.vScale.x = -ABSF(titleScootinDog.vScale.x)
				titleScootinDog.scootDirection = SIDE_RIGHT
			ENDIF
		ENDIF
	ENDIF
	
	TEXT_LABEL_63 fFinalTexture = titleScootinDog.sTexture
	fFinalTexture += titleScootinDog.sActiveFrame
	
	FLOAT fFinalXPos = titleScootinDog.vPos.x + titleScootinDog.fXOffset
	
	VECTOR_2D vFinalPos = INIT_VECTOR_2D(fFinalXPos, titleScootinDog.vPos.y)
	
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(
			titleScootinDog.sDict,
			fFinalTexture,
			vFinalPos,
			titleScootinDog.vScale,
			0.0,
			rgbaOriginal)

ENDPROC

PROC RA_DRAW_SONG_SCREEN()

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_SCOOTIN_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-552,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)-32)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_MUSIC_NEUTRAL"
	
	IF RA_INPUT_IS_LEFT_DOWN()
		sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_MUSIC_LEFT"
	ENDIF
	
	IF RA_INPUT_IS_RIGHT_DOWN()
		sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_MUSIC_RIGHT"
	ENDIF
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(360,360)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-24,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+220)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenuRadio, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+8,(cfBASE_SCREEN_HEIGHT/2.0) - 24)
	
	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
		
	// Draw selection HUD
	STRING sSongTitle = RA_GET_SELECTED_SONG_TEX()
	
	VECTOR_2D vSongScale = INIT_VECTOR_2D(520,100)
	VECTOR_2D vSongPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-32)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSongTitle, vSongPos, vSongScale, 0.0, rgbaOriginal)
	
	vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) - (50 + (vSongScale.x/2.0) + 4)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_AUDIO_SELECT_LEFT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
	vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) + (50 + (vSongScale.x/2.0) + 4)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_AUDIO_SELECT_RIGHT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
	
ENDPROC


PROC RA_DRAW_TRACK_SCREEN(BOOL bWaitingForNetwork = false)

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_SCOOTIN_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-552,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)-32)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_01_LEVEL"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-298,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-28)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+8,(cfBASE_SCREEN_HEIGHT/2.0) - 24)
	
	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	// Draw tapping finger
	STRING sStart = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_02_LEVEL"
	FLOAT fXOffset = -310
	
	VECTOR_2D vStartPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0+fXOffset,cfBASE_SCREEN_HEIGHT/2.0 + 20.0)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(32, 100)
	
	IF TIMERA() >= 500
	
		vStartPos.x += (vStartPos.x%4)
		vStartPos.y += (vStartPos.y%4)
	
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu, sStart, vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 1000
		SETTIMERA(0)
	ENDIF
	
	// Draw a special message if the other player hasn't finished selecting yet.
	IF (bWaitingForNetwork)
	
		RA_WRITE_STRING_USING_TEXT_SPRITES("WAITING FOR OTHER PLAYER", INIT_VECTOR_2D(592, 520), 1, TRUE, 0, 1)
	
		EXIT
	ELSE	
		// Draw selection HUD
		STRING sSongTitle = RA_GET_SELECTED_TRACK_TEX()
		
		VECTOR_2D vSongScale = INIT_VECTOR_2D(520,100)
		VECTOR_2D vSongPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+(360.0/2.0)-32.0)
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSongTitle, vSongPos, vSongScale, 0.0, rgbaOriginal)
		
		vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) - (50 + (vSongScale.x/2.0) + 4)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_TRACK_SELECT_LEFT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
		vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) + (50 + (vSongScale.x/2.0) + 4)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_TRACK_SELECT_RIGHT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
	ENDIF
	
	
ENDPROC


PROC ROAD_ARCADE_DRAW_NETWORK_SCREEN()

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_SCOOTIN_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-552,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)-32)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_01_LEVEL"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-298,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-32)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+8,(cfBASE_SCREEN_HEIGHT/2.0) - 28)
		
	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("WAITING FOR CHALLENGER", INIT_VECTOR_2D(604, (cfBASE_SCREEN_HEIGHT/2.0)), 1, TRUE)

ENDPROC


PROC ROAD_ARCADE_NET_DRAW_TAG_ENTRY()
	
	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_SCOOTIN_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-552,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)-32)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_01_LEVEL"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-298,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-28)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+8,(cfBASE_SCREEN_HEIGHT/2.0) - 24)
	
	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	// Draw tapping finger
	STRING sStart = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_02_LEVEL"
	FLOAT fXOffset = -309
	
	VECTOR_2D vStartPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0+fXOffset,cfBASE_SCREEN_HEIGHT/2.0 + 17.0)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(32, 100)
	
	IF TIMERA() >= 500
	
		//vStartPos.x += (vStartPos.x%4)
		vStartPos.y += (vStartPos.y%4)
	
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu, sStart, vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 1000
		SETTIMERA(0)
	ENDIF
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("ENTER YOUR ON SCREEN TAG", INIT_VECTOR_2D(637,540), 1, TRUE)
			
ENDPROC

RA_SCOOTIN_DOG titleWhizingDog

PROC RA_INIT_DOG_MENU_TRUCK()

	titleWhizingDog.sDict = sDICT_SelectMenu_Truck
	titleWhizingDog.sTexture = "HUD_INTRO_SELECT_MENU_DOG_"
	titleWhizingDog.sActiveFrame = "FRAME_01"
	titleWhizingDog.fAnimTimer = 0
	titleWhizingDog.scootDirection = SIDE_RIGHT // Not actually used here.
	
	titleWhizingDog.vScale = INIT_VECTOR_2D(112.0, 108.0)
	titleWhizingDog.vPos = INIT_VECTOR_2D(1304.0, 468.0)
ENDPROC

PROC RA_WHIZING_DOG_UPDATE()

	titleWhizingDog.fAnimTimer += 0.0+@(1000)
	
	IF titleWhizingDog.fAnimTimer >= titleWhizingDog.fAnimSpeed
	
		titleWhizingDog.fAnimTimer = 0
	
		IF ARE_STRINGS_EQUAL(titleWhizingDog.sActiveFrame, "FRAME_01")
			titleWhizingDog.sActiveFrame = "FRAME_02"
		ELSE
			titleWhizingDog.sActiveFrame = "FRAME_01"
		ENDIF

	ENDIF
	
	TEXT_LABEL_63 fFinalTexture = titleWhizingDog.sTexture
	fFinalTexture += titleWhizingDog.sActiveFrame
	
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(
			titleWhizingDog.sDict,
			fFinalTexture,
			titleWhizingDog.vPos,
			titleWhizingDog.vScale,
			0.0,
			rgbaOriginal)

ENDPROC

PROC RA_DRAW_SONG_SCREEN_TRUCK()

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_WHIZING_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
		
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-572,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)+20)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_MUSIC_NEUTRAL_TRUCK"
	
	IF RA_INPUT_IS_LEFT_DOWN()
		sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_MUSIC_LEFT_TRUCK"
	ENDIF
	
	IF RA_INPUT_IS_RIGHT_DOWN()
		sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_MUSIC_RIGHT_TRUCK"
	ENDIF
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(360,360)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-44,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+144)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenuRadio, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+24,(cfBASE_SCREEN_HEIGHT/2.0) - 30)

	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Truck, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
		
	// Draw selection HUD
	STRING sSongTitle = RA_GET_SELECTED_SONG_TEX()
	
	VECTOR_2D vSongScale = INIT_VECTOR_2D(520,100)
	VECTOR_2D vSongPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+(360.0/2.0)-32)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSongTitle, vSongPos, vSongScale, 0.0, rgbaOriginal)
	
	vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) - (50 + (vSongScale.x/2.0) + 4)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_AUDIO_SELECT_LEFT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
	vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) + (50 + (vSongScale.x/2.0) + 4)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_AUDIO_SELECT_RIGHT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
	
	
ENDPROC

PROC RA_DRAW_TRACK_SCREEN_TRUCK(BOOL bWaitingForNetwork = false)

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_WHIZING_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
		
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-572,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)+20)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_01_LEVEL"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-292,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+10)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+24,(cfBASE_SCREEN_HEIGHT/2.0) - 30)

	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Truck, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	// Draw tapping finger
	STRING sStart = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_02_LEVEL"
	FLOAT fXOffset = -15
	
	VECTOR_2D vStartPos = INIT_VECTOR_2D(vRightArmPos.x + fXOffset,cfBASE_SCREEN_HEIGHT/2.0 + 52.0)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(32, 100)
	
	IF TIMERA() >= 500
	
		vStartPos.x += (vStartPos.x%4)
		vStartPos.y += (vStartPos.y%4)
	
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStart, vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 1000
		SETTIMERA(0)
	ENDIF
		
	// Draw a special message if the other player hasn't finished selecting yet.
	IF (bWaitingForNetwork)
	
		RA_WRITE_STRING_USING_TEXT_SPRITES("WAITING FOR OTHER PLAYER", INIT_VECTOR_2D(592, 520), 1, TRUE)
	
		EXIT
	ELSE	
		// Draw selection HUD
		STRING sSongTitle = RA_GET_SELECTED_TRACK_TEX()
		
		VECTOR_2D vSongScale = INIT_VECTOR_2D(520,100)
		VECTOR_2D vSongPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+(360.0/2.0)-32)
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSongTitle, vSongPos, vSongScale, 0.0, rgbaOriginal)
		
		vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) - (50 + (vSongScale.x/2.0) + 4)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_TRACK_SELECT_LEFT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
			
		vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) + (50 + (vSongScale.x/2.0) + 4)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_TRACK_SELECT_RIGHT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
	ENDIF
	
	
ENDPROC

PROC ROAD_ARCADE_DRAW_NETWORK_SCREEN_TRUCK()

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_WHIZING_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-552,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)-32)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_01_LEVEL"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-298,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-32)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+8,(cfBASE_SCREEN_HEIGHT/2.0) - 28)

	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Truck, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("WAITING FOR CHALLENGER", INIT_VECTOR_2D(604, (cfBASE_SCREEN_HEIGHT/2.0)))

ENDPROC


PROC ROAD_ARCADE_NET_DRAW_TAG_ENTRY_TRUCK()
	
	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF iPlayerSeatLocalID = 0
		RA_WHIZING_DOG_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-552,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)-32)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_01_LEVEL"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(160,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-292,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+10)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(380,480)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+24,(cfBASE_SCREEN_HEIGHT/2.0) - 30)

	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Truck, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	// Draw tapping finger
	STRING sStart = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_FRAME_02_LEVEL"
	FLOAT fXOffset = -15
	
	VECTOR_2D vStartPos = INIT_VECTOR_2D(vRightArmPos.x + fXOffset,cfBASE_SCREEN_HEIGHT/2.0 + 52.0)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(32, 100)
	
	IF TIMERA() >= 500
	
		vStartPos.x += (vStartPos.x%4)
		vStartPos.y += (vStartPos.y%4)
	
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Truck, sStart, vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 1000
		SETTIMERA(0)
	ENDIF
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("ENTER YOUR ON SCREEN TAG", INIT_VECTOR_2D(637,540))
			
ENDPROC

RA_SCOOTIN_DOG titleCatBike

PROC RA_INIT_CAT_MENU()

	titleCatBike.sDict = sDICT_SelectMenu_Bike
	titleCatBike.sTexture = "HUD_INTRO_SELECT_MENU_CAT_"
	titleCatBike.sActiveFrame = "FRAME_01"
	titleCatBike.fAnimTimer = 0
	titleCatBike.scootDirection = SIDE_RIGHT // Not actually used here.
	
	titleCatBike.vScale = INIT_VECTOR_2D(112.0, 108.0)
	titleCatBike.vPos = INIT_VECTOR_2D(1232.0, 326.0)
ENDPROC

PROC RA_CAT_TITLE_UPDATE()

	titleCatBike.fAnimTimer += 0.0+@(1000)
	
	IF titleCatBike.fAnimTimer >= titleCatBike.fAnimSpeed
	
		titleCatBike.fAnimTimer = 0
	
		IF ARE_STRINGS_EQUAL(titleCatBike.sActiveFrame, "FRAME_01")
			titleCatBike.sActiveFrame = "FRAME_02"
		ELSE
			titleCatBike.sActiveFrame = "FRAME_01"
		ENDIF

	ENDIF
	
	TEXT_LABEL_63 fFinalTexture = titleCatBike.sTexture
	fFinalTexture += titleCatBike.sActiveFrame
	
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(
			titleCatBike.sDict,
			fFinalTexture,
			titleCatBike.vPos,
			titleCatBike.vScale,
			0.0,
			rgbaOriginal)

ENDPROC

PROC ROAD_ARCADE_NET_DRAW_TAG_ENTRY_BIKE()
	
	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Cat
	IF iPlayerSeatLocalID = 0
		RA_CAT_TITLE_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-508,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)+28)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_01"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+436,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+20)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Radio toggle finger
	String sStaticRightThumb = "HUD_INTRO_SELECT_MENU_DRIVER_THUMB_FRAME_01"
	
	IF RA_INPUT_IS_LEFT_DOWN()
	OR RA_INPUT_IS_RIGHT_DOWN()
		sStaticRightThumb = "HUD_INTRO_SELECT_MENU_DRIVER_THUMB_FRAME_02"
	ENDIF
	
	VECTOR_2D vRightThumbScale = INIT_VECTOR_2D(148,156)
	VECTOR_2D vRightThumbPos = INIT_VECTOR_2D(vRightArmPos.x - 83,vRightArmPos.y - 15)
	
	vRightThumbPos.x += (vRightThumbPos.x%4)
	vRightThumbPos.y += (vRightThumbPos.y%4)
	
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_SelectMenu_Bike, sStaticRightThumb, vRightThumbPos, vRightThumbScale, 0.0, rgbaOriginal)
		
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(316,292)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-8,208)
	
	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Bike, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("ENTER YOUR ON SCREEN TAG", INIT_VECTOR_2D(637,540), 1, TRUE)
			
ENDPROC

PROC RA_DRAW_SONG_SCREEN_BIKE()

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	IF (iPlayerSeatLocalID = 0)
		RA_CAT_TITLE_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
		
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-508,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)+28)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_01"
		
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+436,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+20)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Radio toggle finger
	String sStaticRightThumb = "HUD_INTRO_SELECT_MENU_DRIVER_THUMB_FRAME_01"
	
	IF RA_INPUT_IS_LEFT_DOWN()
	OR RA_INPUT_IS_RIGHT_DOWN()
		sStaticRightThumb = "HUD_INTRO_SELECT_MENU_DRIVER_THUMB_FRAME_02"
	ENDIF
	
	VECTOR_2D vRightThumbScale = INIT_VECTOR_2D(148,156)
	VECTOR_2D vRightThumbPos = INIT_VECTOR_2D(vRightArmPos.x - 83,vRightArmPos.y - 15)
	
	vRightThumbPos.x += (vRightThumbPos.x%4)
	vRightThumbPos.y += (vRightThumbPos.y%4)
	
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_SelectMenu_Bike, sStaticRightThumb, vRightThumbPos, vRightThumbScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(316,292)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-8,208)
	
	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Bike, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
		
	// Draw selection HUD
	STRING sSongTitle = RA_GET_SELECTED_SONG_TEX()
	
	VECTOR_2D vSongScale = INIT_VECTOR_2D(520,100)
	VECTOR_2D vSongPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-32)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSongTitle, vSongPos, vSongScale, 0.0, rgbaOriginal)
	
	vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) - (50 + (vSongScale.x/2.0) + 4)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_AUDIO_SELECT_LEFT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
	vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) + (50 + (vSongScale.x/2.0) + 4)
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_AUDIO_SELECT_RIGHT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
	
ENDPROC

PROC RA_DRAW_TRACK_SCREEN_BIKE(BOOL bWaitingForNetwork = false)

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Cat
	IF (iPlayerSeatLocalID = 0)
		RA_CAT_TITLE_UPDATE()
	ENDIF

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
		
	// Draw Arms
	String sStaticLeftArm = "HUD_INTRO_SELECT_MENU_DRIVER_LEFT_ARM_01"
	
	VECTOR_2D vLeftArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vLeftArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-508,cfBASE_SCREEN_HEIGHT/2.0+(vLeftArmScale.y/2.0)+28)
	
	vLeftArmPos.x += (vLeftArmPos.x%4)
	vLeftArmPos.y += (vLeftArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticLeftArm, vLeftArmPos, vLeftArmScale, 0.0, rgbaOriginal)
	
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_01"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+436,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)+20)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Radio toggle finger
	String sStaticRightThumb = "HUD_INTRO_SELECT_MENU_DRIVER_THUMB_FRAME_01"
	
	IF RA_INPUT_IS_LEFT_DOWN()
	OR RA_INPUT_IS_RIGHT_DOWN()
		sStaticRightThumb = "HUD_INTRO_SELECT_MENU_DRIVER_THUMB_FRAME_02"
	ENDIF
	
	VECTOR_2D vRightThumbScale = INIT_VECTOR_2D(148,156)
	VECTOR_2D vRightThumbPos = INIT_VECTOR_2D(vRightArmPos.x - 83,vRightArmPos.y - 15)
	
	vRightThumbPos.x += (vRightThumbPos.x%4)
	vRightThumbPos.y += (vRightThumbPos.y%4)
	
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_SelectMenu_Bike, sStaticRightThumb, vRightThumbPos, vRightThumbScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(316,292)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-8,208)

	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Bike, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	// Draw a special message if the other player hasn't finished selecting yet.
	IF (bWaitingForNetwork)
	
		RA_WRITE_STRING_USING_TEXT_SPRITES("WAITING FOR OTHER PLAYER", INIT_VECTOR_2D(592, 520), 1, TRUE)
	
		EXIT
	ELSE	
		// Draw selection HUD
		STRING sSongTitle = RA_GET_SELECTED_TRACK_TEX()
		
		VECTOR_2D vSongScale = INIT_VECTOR_2D(520,100)
		VECTOR_2D vSongPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-32)
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSongTitle, vSongPos, vSongScale, 0.0, rgbaOriginal)
		
		vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) - (50 + (vSongScale.x/2.0) + 4)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_TRACK_SELECT_LEFT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
			
		vSongPos.x = (cfBASE_SCREEN_WIDTH/2.0) + (50 + (vSongScale.x/2.0) + 4)
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_TRACK_SELECT_RIGHT", vSongPos, INIT_VECTOR_2D(100,100), 0.0, rgbaOriginal)
		
	ENDIF
	
ENDPROC

PROC ROAD_ARCADE_DRAW_NETWORK_SCREEN_BIKE()

	// Draw background
	STRING sBackDrop = "HUD_INTRO_SELECT_MENU_BACKGROUND_01"
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundScale = INIT_VECTOR_2D(1264,932)
	
	vBackgroundPos.x += (vBackgroundPos.x%4)
	vBackgroundPos.y += (vBackgroundPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sBackDrop, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)

	// Draw Dog
	RA_CAT_TITLE_UPDATE()

	// Draw Dashboard
	STRING sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_01"
	
	IF (iPlayerSeatLocalID = 1)
		sDashboard = "HUD_INTRO_SELECT_MENU_DASHBOARD_02"
	ENDIF
	
	VECTOR_2D vDashboardPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+40)
	VECTOR_2D vDashboardScale = INIT_VECTOR_2D(1264,860)
	
	vDashboardPos.x += (vDashboardPos.x%4)
	vDashboardPos.y += (vDashboardPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sDashboard, vDashboardPos, vDashboardScale, 0.0, rgbaOriginal)
	
	// Draw Arms
	String sStaticRightArm = "HUD_INTRO_SELECT_MENU_DRIVER_RIGHT_ARM_01"
	
	VECTOR_2D vRightArmScale = INIT_VECTOR_2D(344,500)
	VECTOR_2D vRightArmPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+100,cfBASE_SCREEN_HEIGHT/2.0+(vRightArmScale.y/2.0)-32)
	
	vRightArmPos.x += (vRightArmPos.x%4)
	vRightArmPos.y += (vRightArmPos.y%4)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_SelectMenu_Bike, sStaticRightArm, vRightArmPos, vRightArmScale, 0.0, rgbaOriginal)
	
	// Draw tag
	STRING sTagTitle = RA_GET_SELECTED_TRACK_TAG_TEX()
	
	VECTOR_2D vTagScale = INIT_VECTOR_2D(316,292)
	VECTOR_2D vTagPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+8,146)

	ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_SelectMenu_Bike, sTagTitle, vTagPos, vTagScale, 0.0, rgbaOriginal)
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("WAITING FOR CHALLENGER", INIT_VECTOR_2D(604, (cfBASE_SCREEN_HEIGHT/2.0)))

ENDPROC



//------------------- SCORE MANAGEMENT --------------------
// Draw the score
PROC ROAD_ARCADE_GET_SCORE(INT &scoreArray[], INT iScore)

	FLOAT iLocalScore = TO_FLOAT(iScore)
	
	Int iHundThs = FLOOR((iLocalScore/POW(10,5)))
	iLocalScore -= (iHundThs*POW(10,5))
	scoreArray[0] = iHundThs // Add the number to the return string
	
	Int iTenThs = FLOOR((iLocalScore/POW(10,4)))
	iLocalScore -= (iTenThs*POW(10,4))
	scoreArray[1] = iTenThs // Add the number to the return string
	
	Int iThs = FLOOR((iLocalScore/POW(10,3)))
	iLocalScore -= (iThs*POW(10,3))
	scoreArray[2] = iThs // Add the number to the return string
	
	Int iHund = FLOOR((iLocalScore/POW(10,2)))
	iLocalScore -= (iHund*POW(10,2))
	scoreArray[3] = iHund // Add the number to the return string
	
	Int iTen = FLOOR((iLocalScore/10))
	iLocalScore -= (iTen*10)
	scoreArray[4] = iTen // Add the number to the return string
	
	Int iSing = FLOOR((iLocalScore))
	iLocalScore -= iSing
	scoreArray[5] = iSing // Add the number to the return string
	
ENDPROC

PROC ROAD_ARCADE_DRAW_SCORE(INT iScore, VECTOR_2D vScoreNumStart)

	INT iScoreArray[6] 
	ROAD_ARCADE_GET_SCORE(iScoreArray, iScore)

	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, "HUD_BUTTON_SCORE", 
				vScoreNumStart, 
				INIT_VECTOR_2D(128.0, 64.0)
				, 0, rgbaOriginal)
	
	vScoreNumStart.x += 96
	
	INT i	
	FOR i = 0 TO 5
	
	
		VECTOR_2D vPos = vScoreNumStart
		VECTOR_2D vScale = INIT_VECTOR_2D(32, 64)
		
		TEXT_LABEL_63 sScoreNumber = "HUD_LETTER_"
		sScoreNumber += iScoreArray[i]
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sScoreNumber, 
				vPos, 
				vScale
				, 0.0, rgbaOriginal)
		
		vScoreNumStart.x += 48
	
	ENDFOR

ENDPROC

STRUCT RA_TAG_DATA

	INT iCurrentSelection = 0

	STRING sFirstLetter
	STRING sSecondLetter
	STRING sThirdLetter
	
	TEXT_LABEL_63 sCompleteEntry
	
	INT iScore = 0
ENDSTRUCT

//RA_TAG_DATA leaderBoardEntries[20]

FUNC STRING RA_GET_LETTER_FROM_SELECTION(INT i)
	IF i = 0
		RETURN "A"
	ENDIF

	IF i = 1
		RETURN "B"
	ENDIF

	IF i = 2
		RETURN "C"
	ENDIF

	IF i = 3
		RETURN "D"
	ENDIF

	IF i = 4
		RETURN "E"
	ENDIF

	IF i = 5
		RETURN "F"
	ENDIF
	
	IF i = 6
		RETURN "G"
	ENDIF
	
	IF i = 7
		RETURN "H"
	ENDIF
	
	IF i = 8
		RETURN "I"
	ENDIF
	
	IF i = 9
		RETURN "J"
	ENDIF
	
	IF i = 10
		RETURN "K"
	ENDIF
	
	IF i = 11
		RETURN "L"
	ENDIF
	
	IF i = 12
		RETURN "M"
	ENDIF
	
	IF i = 13
		RETURN "N"
	ENDIF
	
	IF i = 14
		RETURN "O"
	ENDIF
	
	IF i = 15
		RETURN "P"
	ENDIF
	
	IF i = 16
		RETURN "Q"
	ENDIF
	
	IF i = 17
		RETURN "R"
	ENDIF
	
	IF i = 18
		RETURN "S"
	ENDIF
	
	IF i = 19
		RETURN "T"
	ENDIF
	
	IF i = 20
		RETURN "U"
	ENDIF
	
	IF i = 21
		RETURN "V"
	ENDIF
	
	IF i = 22
		RETURN "W"
	ENDIF
	
	IF i = 23
		RETURN "X"
	ENDIF
	
	IF i = 24
		RETURN "Y"
	ENDIF
	
	IF i = 25
		RETURN "Z"
	ENDIF
	
	IF i = 26
		RETURN "0"
	ENDIF
	
	IF i = 27
		RETURN "1"
	ENDIF
	
	IF i = 28
		RETURN "2"
	ENDIF
	
	IF i = 29
		RETURN "3"
	ENDIF
	
	IF i = 30
		RETURN "4"
	ENDIF
	
	IF i = 31
		RETURN "5"
	ENDIF
	
	IF i = 32
		RETURN "6"
	ENDIF
	
	IF i = 33
		RETURN "7"
	ENDIF
	
	IF i = 34
		RETURN "8"
	ENDIF
	
	IF i = 35
		RETURN "9"
	ENDIF
	
	IF i = 36
		RETURN "!"
	ENDIF
	
	IF i = 37
		RETURN "."
	ENDIF
	
	IF i = 38
		RETURN "$"
	ENDIF
	
	IF i = 39
		RETURN ":"
	ENDIF
	
	IF i = 40
		RETURN " "
	ENDIF
	
	RETURN " "
ENDFUNC

ENUM NAME_ENTRY_STAGES

	NE_FIRST_LETTER = 0,
	NE_SECOND_LETTER,
	NE_THIRD_LETTER,
	NE_CONFIRM

ENDENUM

NAME_ENTRY_STAGES eCurrentEntryStage

FUNC STRING ROAD_ARCADE_GET_LEADERBOARD_DICT()

	IF iPlayerSeatLocalID = 0
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN sDICT_LeaderBoard
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN sDICT_LeaderBoard_Bike
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN sDICT_LeaderBoard_Truck
		ENDIF
		
	ELSE
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN sDICT_LeaderBoard_Rival
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN sDICT_LeaderBoard_RivalBike
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN sDICT_LeaderBoard_RivalTruck
		ENDIF
		
	ENDIF

	RETURN sDICT_LeaderBoard

ENDFUNC

PROC RA_DRAW_SCORE_ENTRY_BACKDROP(BOOL bIsOutOfRank)

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
		EXIT
	ENDIF

	TEXT_LABEL_63 sBackground = "HUD_WIN_BACKGROUND_01"
		
	IF iPlayerSeatLocalID = 1
		sBackground = "HUD_WIN_BACKGROUND_02"
	ENDIF
		
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundSCALE = INIT_VECTOR_2D(1264, 932)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackground, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)
	
	IF iPlayerSeatLocalID = 0
	
		TEXT_LABEL_63 sBackgroundTail1 = "HUD_WIN_DOG_TAIL_FRAME_01"
		TEXT_LABEL_63 sBackgroundTail2 = "HUD_WIN_DOG_TAIL_FRAME_02"
		
		VECTOR_2D vBackgroundTailPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-392, (cfBASE_SCREEN_HEIGHT/2.0)+40)
		VECTOR_2D vBackgroundTailSCALE = INIT_VECTOR_2D(316, 400)
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackgroundTail1 = "HUD_WIN_CAT_TAIL_FRAME_01"
			sBackgroundTail2 = "HUD_WIN_CAT_TAIL_FRAME_02"
			
			vBackgroundTailPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-504, (cfBASE_SCREEN_HEIGHT/2.0)+62)
			
			vBackgroundTailSCALE = INIT_VECTOR_2D(236, 224)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackgroundTail1 = "HUD_WIN_DOG_TONGUE_FRAME_01"
			sBackgroundTail2 = "HUD_WIN_DOG_TONGUE_FRAME_02"
			
			vBackgroundTailSCALE = INIT_VECTOR_2D(136, 152)
			
			vBackgroundTailPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-152, (cfBASE_SCREEN_HEIGHT/2.0)-152)
		ENDIF
		
		IF TIMERA() < 400		
			ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackgroundTail1, vBackgroundTailPos, vBackgroundTailScale, 0.0, rgbaOriginal)
		ELSE
			ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackgroundTail2, vBackgroundTailPos, vBackgroundTailScale, 0.0, rgbaOriginal)
		ENDIF

		IF TIMERA() >= 800
			SETTIMERA(0)
		ENDIF
		
	ENDIF
	
	IF !bIsOutOfRank
		RA_WRITE_STRING_USING_TEXT_SPRITES("ENTER YOUR NAME!!", INIT_VECTOR_2D(744,140), 1, TRUE, 2, 1)
	ENDIF
				
ENDPROC

FUNC BOOL RA_DRAW_SCORE_TABLE()

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
		RETURN FALSE
	ENDIF

	STRING sBackground = "HUD_LEADERBOARD_FRAME_03"
	
	ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_HELP_LEADERBOARD)
	
	IF iPlayerSeatLocalID = 0
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
		OR eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_FRAME_02"
		ENDIF
		
	ELSE
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			sBackground = "HUD_LEADERBOARD_TORERO_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_OPPONENT_FRAME_02"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_MESA_FRAME_03"
		ENDIF
		
	ENDIF
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundSCALE = INIT_VECTOR_2D(1280, 1112)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackground, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("TOP RACERS AND CHASERS!!", INIT_VECTOR_2D(604, 140), 1, TRUE, 2)
	ARCADE_GAMES_LEADERBOARD_DRAW(iPlayerSeatLocalID = 1, FALSE)
	
	IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
	
ENDFUNC

// --------------------- NETWORK SUPPORT ---------------------

// Network Flags
ENUM ROAD_ARCADE_NET_FLAGS

	RA_NET_PLAYER_CHALLENGE = 1,
	RA_NET_GAME_INITIALISED = 2,
	RA_NET_PLAYER_TIME_OVER = 4,
	RA_NET_PLAYER_REMATCH_AGREED = 8,
	RA_NET_PLAYER_REMATCH_DECLINED = 16

ENDENUM

// Game state for the multiplayer version
ENUM ROAD_ARCADE_MULTI_GS

	MULTI_SERVER_INIT,
	MULTI_SERVER_SOLO,
	MULTI_SERVER_CHALLENGER, // Solo player can hit cutton to quit current game and nenter a Multiplayer game
	MULTI_SERVER_INGAME,
	MULTI_SERVER_RACING,
	MULTI_SERVER_REMATCH,
	MULTI_SERVER_LOST_CONNECTION

ENDENUM

FUNC STRING ROAD_ARCADE_GET_SERVER_STATE_AS_STRING(INT iState)

	SWITCH (INT_TO_ENUM(ROAD_ARCADE_MULTI_GS, iState))
		CASE MULTI_SERVER_SOLO 					RETURN "GAME_STATE_SOLO"
		CASE MULTI_SERVER_CHALLENGER 			RETURN "GAME_STATE_CHALLENGER"
		CASE MULTI_SERVER_INGAME				RETURN "GAME_STATE_MP"
		CASE MULTI_SERVER_REMATCH				RETURN "GAME_REMATCH"
		CASE MULTI_SERVER_LOST_CONNECTION		RETURN "GAME_LOST_CONNECTION"	
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC

FUNC STRING ROAD_ARCADE_GET_GAME_STATE_AS_STRING(GAME_STATES iState)

	SWITCH iState
		CASE GS_TRACK_SELECT 							RETURN "GS_TRACK_SELECT"
		CASE GS_MUSIC_SELECT 							RETURN "GS_MUSIC_SELECT"
		CASE GS_MUTLIPLAYER_ENTER_TAG 					RETURN "GS_MUTLIPLAYER_ENTER_TAG"
		CASE GS_MULTIPLAYER_WAIT_FOR_NETWORK 			RETURN "GS_MULTIPLAYER_WAIT_FOR_NETWORK"
		CASE GS_MULTIPLAYER_CHALLENGER_SCREEN 			RETURN "GS_MULTIPLAYER_CHALLENGER_SCREEN"
		CASE GS_MAIN_GAME_UPDATE			 			RETURN "GS_MAIN_GAME_UPDATE"
		CASE GS_MULTIPLAYER_STREAM_CHALLENGER_ASSETS	RETURN "GS_MULTIPLAYER_STREAM_CHALLENGER_ASSETS"
		CASE GS_CLEANUP_GAME_ASSETS						RETURN "GS_CLEANUP_GAME_ASSETS"
		CASE GS_CLEANUP_TO_TITLE						RETURN "GS_CLEANUP_TO_TITLE"
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC

STRUCT ROAD_ARCADE_PLAYER_BROADCAST_DATA
	GAME_STATES activeState
	TEXT_LABEL_63 sPlayerID // HS stle name tag entry. Int will be interpreted as a string.
	INT iScore = -1
	FLOAT fPlayerProgress
	BOOL bFinished
	
	INT iNetBitSet
	STAGES_SETS sCurrentStage
ENDSTRUCT

ROAD_ARCADE_PLAYER_BROADCAST_DATA roadArcadePlayerBd[2]
INT iBroadcastFrameCounter

//ROAD_ARCADE_PLAYER_BROADCAST_DATA roadArcadePlayerBd[2]

// Everyone can read this data, only the server can update it. 
STRUCT ROAD_ARCADE_SERVER_BROADCAST_DATA
    INT iNumPlayers
	ROAD_ARCADE_MULTI_GS serverGameState
	
	STAGES_SETS stageToStart = STAGE_INVALID
	
	INT iMatchId = 0
ENDSTRUCT

ROAD_ARCADE_SERVER_BROADCAST_DATA roadArcadeServerBd

FUNC BOOL ROAD_ARCADE_NET_IS_MULTIPLAYER_AVAILABLE()

	INT i
	INT iNumPlayers
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			iNumPlayers++
		ENDIF
	ENDREPEAT
	
	IF iNumPlayers > 1
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC ROAD_ARCADE_MULTI_GS ROAD_ARCADE_NET_GET_SERVER_STATE()
	RETURN roadArcadeServerBd.serverGameState
ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()

	IF NOT ROAD_ARCADE_NET_IS_MULTIPLAYER_AVAILABLE()
		RETURN FALSE
	ENDIF
	
	IF ROAD_ARCADE_NET_GET_SERVER_STATE() = MULTI_SERVER_INIT
		RETURN FALSE
	ENDIF
	
	IF ROAD_ARCADE_NET_GET_SERVER_STATE() = MULTI_SERVER_SOLO
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[0].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[1].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// Retrieve Participant data
FUNC GAME_STATES ROAD_ARCADE_NET_GET_PARTICIPANT_GAME_STATE(INT iParticipant)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		RETURN roadArcadePlayerBd[iParticipant].activeState
	ENDIF

	RETURN GS_CLEANUP_TO_TITLE
ENDFUNC

FUNC FLOAT ROAD_ARCADE_NET_GET_PARTICIPANT_PROGRESS(INT iParticipant)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		RETURN roadArcadePlayerBd[iParticipant].fPlayerProgress
	ENDIF

	RETURN 0.0
ENDFUNC

FUNC INT ROAD_ARCADE_NET_GET_PARTICIPANT_FINAL_SCORE(INT iParticipant)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		RETURN roadArcadePlayerBd[iParticipant].iScore
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_GET_PARTICIPANT_FINISHED(INT iParticipant)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		RETURN roadArcadePlayerBd[iParticipant].bFinished
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(INT iParticipantNo)

	IF iParticipantNo = 0
		RETURN 1
	ELIF iParticipantNo = 1
		RETURN 0
	ENDIF

	RETURN -1

ENDFUNC

PROC ROAD_ARCADE_NET_RESET_LOCAL_PLAYER_RACE_VARIABLES()
	
	roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iScore = -1 // Set final score invalid
	roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].fPlayerProgress = 0.0
	roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].bFinished = FALSE
	roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet = 0
	roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].sCurrentStage = STAGE_INVALID
	roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].sPlayerID = ""
	
	CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_TIME_OVER)
ENDPROC

// Set the Participant information
PROC ROAD_ARCADE_NET_SET_PARTICIPANT_GAME_STATE(INT playerNo, GAME_STATES newState)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(playerNo))
	AND roadArcadePlayerBd[playerNo].activeState != newState
		roadArcadePlayerBd[playerNo].activeState = newState
	ENDIF
ENDPROC

PROC ROAD_ARCADE_NET_SET_PARTICIPANT_FINISHED(INT playerNo)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(playerNo))
	AND !roadArcadePlayerBd[playerNo].bFinished
		roadArcadePlayerBd[playerNo].bFinished = TRUE
	ENDIF
ENDPROC

PROC ROAD_ARCADE_NET_SET_PARTICIPANT_TAG(INT iParticipantNo, STRING sTagToAdd)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantNo))
	AND NOT ARE_STRINGS_EQUAL(roadArcadePlayerBd[iParticipantNo].sPlayerID, sTagToAdd)
		roadArcadePlayerBd[iParticipantNo].sPlayerID = sTagToAdd
	ENDIF
ENDPROC

// Retrieve player data
PROC ROAD_ARCADE_NET_SET_PARTICIPANT_PROGRESS(PLAYER_VEHICLE &player, INT iParticipantNo)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantNo))
	AND roadArcadePlayerBd[iParticipantNo].fPlayerProgress != player.fZValue
	AND iBroadcastFrameCounter >= 10 // Update every 10 frames to avoid updating too frequently.
		roadArcadePlayerBd[iParticipantNo].fPlayerProgress = player.fZValue
		iBroadcastFrameCounter = 0
	ENDIF
	
	iBroadcastFrameCounter++
ENDPROC

PROC ROAD_ARCADE_NET_SET_PARTICIPANT_STAGE(INT iParticipantNo, STAGES_SETS stgCurrent)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantNo))
	AND roadArcadePlayerBd[iParticipantNo].sCurrentStage != stgCurrent
		roadArcadePlayerBd[iParticipantNo].sCurrentStage = stgCurrent
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_ROAD_ARCADE_ANIM_STATE_NAME(RA_PLAYER_PED_ANIM_STATE eState)
	SWITCH eState 
		CASE RA_PLAYER_PED_ANIM_IDLE				RETURN "RA_PLAYER_PED_ANIM_IDLE"
		CASE RA_PLAYER_PED_ANIM_START_GAME			RETURN "RA_PLAYER_PED_ANIM_START_GAME"
		CASE RA_PLAYER_PED_ANIM_IN_GAME				RETURN "RA_PLAYER_PED_ANIM_IN_GAME"
		CASE RA_PLAYER_PED_ANIM_TAKE_LEAD			RETURN "RA_PLAYER_PED_ANIM_TAKE_LEAD"
		CASE RA_PLAYER_PED_ANIM_HIT_CHECKPOINT		RETURN "RA_PLAYER_PED_ANIM_HIT_CHECKPOINT"
		CASE RA_PLAYER_PED_ANIM_LOSE_LEAD			RETURN "RA_PLAYER_PED_ANIM_LOSE_LEAD"
		CASE RA_PLAYER_PED_ANIM_CRASH_VEH			RETURN "RA_PLAYER_PED_ANIM_CRASH_VEH"
		CASE RA_PLAYER_PED_ANIM_WIN					RETURN "RA_PLAYER_PED_ANIM_WIN"
		CASE RA_PLAYER_PED_ANIM_LOSE				RETURN "RA_PLAYER_PED_ANIM_LOSE"
		CASE RA_PLAYER_PED_ANIM_BACK_TO_IDLE		RETURN "RA_PLAYER_PED_ANIM_BACK_TO_IDLE"
	ENDSWITCH 
	RETURN "GET_ROAD_ARCADE_ANIM_STATE_NAME INVALID"
ENDFUNC
#ENDIF


PROC SET_ROAD_ARCADE_ANIM_STATE(PLAYER_VEHICLE &ePlayerData, RA_PLAYER_PED_ANIM_STATE eState)
	IF ePlayerData.ePlayerPedAnimState != eState
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[road_arcade] - SET_ROAD_ARCADE_ANIM_STATE From: ", GET_ROAD_ARCADE_ANIM_STATE_NAME(ePlayerData.ePlayerPedAnimState), " To: ", GET_ROAD_ARCADE_ANIM_STATE_NAME(eState))
		ePlayerData.ePlayerPedAnimState = eState
	ENDIF
ENDPROC

PROC ROAD_ARCADE_NET_SET_PARTICIPANT_FINAL_SCORE(PLAYER_VEHICLE &player, INT iParticipantNo)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantNo))
	AND roadArcadePlayerBd[iParticipantNo].iScore != player.iPlayerScore
		roadArcadePlayerBd[iParticipantNo].iScore = player.iPlayerScore
	ENDIF
ENDPROC

PROC ROAD_ARCADE_NET_SET_RIVAL_PARTICIPANT_PROGRESS(RIVAL_PLAYER_VEHICLE &rival, INT rivalId)
	rival.fZDrawAt = roadArcadePlayerBd[rivalId].fPlayerProgress
ENDPROC

FUNC BOOL ROAD_ARCADE_NET_ARE_PLAYERS_IN_WAIT_STATE()

	Bool bReturn = TRUE
	
	IF (ROAD_ARCADE_NET_GET_PARTICIPANT_GAME_STATE(0) != GS_MULTIPLAYER_WAIT_FOR_NETWORK)
		bReturn = false
	ENDIF

	IF (ROAD_ARCADE_NET_GET_PARTICIPANT_GAME_STATE(1) != GS_MULTIPLAYER_WAIT_FOR_NETWORK)
		bReturn = false
	ENDIF
	
	RETURN bReturn

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_ARE_PLAYERS_IN_GAME_STATE()

	Bool bReturn = TRUE
	
	IF (ROAD_ARCADE_NET_GET_PARTICIPANT_GAME_STATE(0) != GS_MAIN_GAME_UPDATE)
		bReturn = false
	ENDIF

	IF (ROAD_ARCADE_NET_GET_PARTICIPANT_GAME_STATE(1) != GS_MAIN_GAME_UPDATE)
		bReturn = false
	ENDIF
	
	RETURN bReturn

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_HAVE_PLAYERS_CONFIRMED_REMATCH()

	Bool bReturn = TRUE
		
	INT i = 0
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i
		
		IF NOT IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[i].iNetBitSet, RA_NET_PLAYER_REMATCH_AGREED)
			bReturn = FALSE
		ENDIF
			
	ENDREPEAT
	
	RETURN bReturn

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_REMATCH_DECLINED()

	Bool bReturn = FALSE
		
	INT i = 0
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i
		
		IF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[i].iNetBitSet, RA_NET_PLAYER_REMATCH_DECLINED)
			bReturn = TRUE
		ENDIF
			
	ENDREPEAT
	
	RETURN bReturn

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_DID_PLAYER_DROP_OUT()

	INT i = 0
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i
	
		IF ROAD_ARCADE_NET_GET_PARTICIPANT_GAME_STATE(i) = GS_CLEANUP_TO_TITLE
			RETURN TRUE
		ENDIF
	
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			RETURN TRUE
		ENDIF
		
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_DID_PLAYER_TIME_OVER()

	INT i = 0
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i
	
		IF roadArcadePlayerBd[i].activeState != GS_MAIN_GAME_UPDATE
			RETURN FALSE
		ENDIF
	
		IF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[i].iNetBitSet, RA_NET_PLAYER_TIME_OVER)
			RETURN TRUE
		ENDIF
			
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_DID_LOCAL_PLAYER_WIN(INT playerNo)

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(playerNo))
		RETURN (roadArcadePlayerBd[playerNo].iScore > roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(playerNo)].iScore)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_IS_MULTIPLAYER_RACE_OVER()

	INT i = 0
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i

		IF ROAD_ARCADE_NET_DID_PLAYER_TIME_OVER()
			RETURN TRUE
		ENDIF
					
		IF roadArcadePlayerBd[i].bFinished
			RETURN TRUE
		ENDIF
			
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

PROC ROAD_ARCADE_NET_SET_SERVER_STATE(ROAD_ARCADE_MULTI_GS iNewState)

	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF

#IF IS_DEBUG_BUILD
		STRING sCurrent = ROAD_ARCADE_GET_SERVER_STATE_AS_STRING(ENUM_TO_INT(roadArcadeServerBd.serverGameState))
		STRING sNew 	= ROAD_ARCADE_GET_SERVER_STATE_AS_STRING(ENUM_TO_INT(iNewState))
		CDEBUG1LN(DEBUG_MINIGAME, "[ROAD_ARCADE_ARCADE] {DSW} [ROAD_ARCADE_SET_SERVER_STATE] ", sCurrent, " -> ", sNew)
#ENDIF
		
	IF roadArcadeServerBd.serverGameState != iNewState
		roadArcadeServerBd.serverGameState = iNewState
	ENDIF
ENDPROC

PROC ROAD_ARCADE_NET_PROCESS_PLAYER_IN_GAME_PROGRESS(PLAYER_VEHICLE &player)
	INT i

	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ROAD_ARCADE_CABINET) i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			ROAD_ARCADE_NET_SET_PARTICIPANT_PROGRESS(player)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT ROAD_ARCADE_GET_RIVAL_PROGRESS_FROM_HOST(INT iParticipantNo)
	RETURN roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(iParticipantNo)].fPlayerProgress
ENDFUNC

FUNC INT ROAD_ARCADE_GET_RIVAL_FINAL_SCORE_FROM_HOST(INT iParticipantNo)
	RETURN roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(iParticipantNo)].iScore
ENDFUNC

//---------------------- NETWORK MENUS ----------------------

PROC ROAD_ARCADE_NET_DRAW_REMATCH_SCREEN(INT iParticipantNo, BOOL bWaiting = false)

	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundSCALE = INIT_VECTOR_2D(1280, 1112)
	
	STRING sBackground = ""
	
	IF iPlayerSeatLocalID = 0
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
		OR eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_FRAME_02"
		ENDIF
		
	ELSE
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			sBackground = "HUD_LEADERBOARD_TORERO_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_OPPONENT_FRAME_02"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_MESA_FRAME_03"
		ENDIF
		
	ENDIF
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackground, vBackgroundPos, vBackgroundScale, 0.0, rgbaOriginal)
	
	IF bWaiting	
	
		TEXT_LABEL_63 stringToDisplay = "WAITING FOR PLAYER "
		stringToDisplay += roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(iParticipantNo)].sPlayerID
		stringToDisplay += " TO RESPOND"
	
		RA_WRITE_STRING_USING_TEXT_SPRITES(TEXT_LABEL_TO_STRING(stringToDisplay), INIT_VECTOR_2D(432, 420))
		EXIT
	ENDIF
		
	VECTOR_2D vStartPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0+156)
	VECTOR_2D vStartScale = INIT_VECTOR_2D(520, 100)
		
	IF TIMERA() >= 1000
		ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), "HUD_BUTTON_START", vStartPos, vStartScale, 0.0, rgbaOriginal)
	ENDIF

	IF TIMERA() >= 2000
		SETTIMERA(0)
	ENDIF
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("WANNA REMATCH?", INIT_VECTOR_2D(704, 444), 1, TRUE, 0, 2)
		
ENDPROC

FUNC BOOL ROAD_ARCADE_NET_DRAW_REMATCH_DENIED_SCREEN(Bool bDidPlayerWin = false)
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundSCALE = INIT_VECTOR_2D(1280, 1112)
	
	RGBA_COLOUR_STRUCT rgbaDarker = rgbaOriginal
	
	rgbaDarker.iR = ROUND(rgbaOriginal.iR/2.0)
	rgbaDarker.iG = ROUND(rgbaOriginal.iG/2.0)
	rgbaDarker.iB = ROUND(rgbaOriginal.iB/2.0)
	
	STRING sBackground = ""
	
	IF iPlayerSeatLocalID = 0
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
		OR eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_FRAME_02"
		ENDIF
		
	ELSE
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			sBackground = "HUD_LEADERBOARD_TORERO_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_OPPONENT_FRAME_02"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_MESA_FRAME_03"
		ENDIF
		
	ENDIF
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackground, vBackgroundPos, vBackgroundScale, 0.0, rgbaDarker)
	
	// Display a special message based on results of last race
	IF bDidPlayerWin
		RA_WRITE_STRING_USING_TEXT_SPRITES("THANK YOU FOR PLAYING!", INIT_VECTOR_2D(584, 420), 1, TRUE, 1)
	ELSE // Different message.
		RA_WRITE_STRING_USING_TEXT_SPRITES("KEEP PRACTICING!", INIT_VECTOR_2D(584, 420), 1, TRUE, 1)
	ENDIF
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("MAYBE NEXT TIME...", INIT_VECTOR_2D(744, 620), 1, TRUE, 0, 1)	
	
	IF TIMERA() >= 3000
	OR RA_INPUT_IS_ACCEL_DOWN(TRUE)
//		IF ROAD_ARCADE_NET_GET_SERVER_STATE() != MULTI_SERVER_SOLO
//			RETURN FALSE
//		ENDIF
	
		SETTIMERA(0)
		RETURN TRUE
	ENDIF

	RETURN FALSE
		
ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_LOST_CONNECTION_SCREEN()
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundSCALE = INIT_VECTOR_2D(1280, 1112)
	
	RGBA_COLOUR_STRUCT rgbaDarker = rgbaOriginal
	
	rgbaDarker.iR = ROUND(rgbaOriginal.iR/2.0)
	rgbaDarker.iG = ROUND(rgbaOriginal.iG/2.0)
	rgbaDarker.iB = ROUND(rgbaOriginal.iB/2.0)
	
	STRING sBackground = ""
	
	IF iPlayerSeatLocalID = 0
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
		OR eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_FRAME_02"
		ENDIF
		
	ELSE
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			sBackground = "HUD_LEADERBOARD_TORERO_FRAME_03"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackground = "HUD_LEADERBOARD_OPPONENT_FRAME_02"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackground = "HUD_LEADERBOARD_MESA_FRAME_03"
		ENDIF
		
	ENDIF
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackground, vBackgroundPos, vBackgroundScale, 0.0, rgbaDarker)
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("CONNECTION LOST", INIT_VECTOR_2D(884, 420), 1, TRUE)	
	
	RA_WRITE_STRING_USING_TEXT_SPRITES("RETURNING TO 1P MODE", INIT_VECTOR_2D(784, 620), 1, TRUE, 0, 1)	
	
	IF TIMERA() >= 3000
	OR RA_INPUT_IS_ACCEL_DOWN(TRUE)
		SETTIMERA(0)
		RETURN TRUE
	ENDIF

	RETURN FALSE
		
ENDFUNC

RA_TAG_DATA lbCurrentEntry

FUNC BOOL ROAD_ARCADE_NET_TAG_ENTRY(RA_TAG_DATA &curretEntry, INT iParticipantNo)

	ROAD_ARCADE_CHECK_LEFT_STICK_NEUTRAL()

	// Process inputs
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
		curretEntry.iCurrentSelection--
	ENDIF

	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
		curretEntry.iCurrentSelection++
	ENDIF
	
	// Don't go beyond limits
	IF curretEntry.iCurrentSelection < 0
		curretEntry.iCurrentSelection = 40
	ENDIF

	IF curretEntry.iCurrentSelection > 40
		curretEntry.iCurrentSelection = 0
	ENDIF

	FLOAT xSTARTPos = (cfBASE_SCREEN_WIDTH/2.0) - 56
	FLOAT ySTARTPos = 648

	SWITCH eCurrentEntryStage
	
		CASE NE_FIRST_LETTER
						
			curretEntry.sFirstLetter = RA_GET_LETTER_FROM_SELECTION(curretEntry.iCurrentSelection)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sFirstLetter, INIT_VECTOR_2D(xSTARTPos, ySTARTPos), 2)
			
			IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
				eCurrentEntryStage = NE_SECOND_LETTER
			ENDIF
			
		BREAK
	
		CASE NE_SECOND_LETTER
		
			curretEntry.sSecondLetter = RA_GET_LETTER_FROM_SELECTION(curretEntry.iCurrentSelection)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sFirstLetter, INIT_VECTOR_2D(xSTARTPos, ySTARTPos), 2)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sSecondLetter, INIT_VECTOR_2D(xSTARTPos + 68, ySTARTPos), 2)
		
			IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
				eCurrentEntryStage = NE_THIRD_LETTER
			ENDIF
		
		BREAK
		
		CASE NE_THIRD_LETTER
		
			curretEntry.sThirdLetter = RA_GET_LETTER_FROM_SELECTION(curretEntry.iCurrentSelection)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sFirstLetter, INIT_VECTOR_2D(xSTARTPos, ySTARTPos), 2)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sSecondLetter, INIT_VECTOR_2D(xSTARTPos + 68, ySTARTPos), 2)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sThirdLetter, INIT_VECTOR_2D(xSTARTPos + 136, ySTARTPos), 2)
		
			IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
				eCurrentEntryStage = NE_CONFIRM
			ENDIF
		
		BREAK
		
		CASE NE_CONFIRM
		
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sFirstLetter, INIT_VECTOR_2D(xSTARTPos, ySTARTPos), 2)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sSecondLetter, INIT_VECTOR_2D(xSTARTPos + 68, ySTARTPos), 2)
			RA_WRITE_STRING_USING_TEXT_SPRITES(curretEntry.sThirdLetter, INIT_VECTOR_2D(xSTARTPos + 136, ySTARTPos), 2)
		
			IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
			
				curretEntry.sCompleteEntry += curretEntry.sFirstLetter 
				curretEntry.sCompleteEntry += curretEntry.sSecondLetter 
				curretEntry.sCompleteEntry += curretEntry.sThirdLetter

				ROAD_ARCADE_NET_SET_PARTICIPANT_TAG(iParticipantNo, TEXT_LABEL_TO_STRING(curretEntry.sCompleteEntry))
				
				RETURN TRUE
			ENDIF
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL ROAD_ARCADE_NET_DRAW_MULTIPLAYER_RESULTS()

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
		RETURN FALSE
	ENDIF

	RGBA_COLOUR_STRUCT rgbaDarker = rgbaOriginal
	
	rgbaDarker.iR = ROUND(rgbaOriginal.iR/2.0)
	rgbaDarker.iG = ROUND(rgbaOriginal.iG/2.0)
	rgbaDarker.iB = ROUND(rgbaOriginal.iB/2.0)

	STRING sBackground = "HUD_WIN_BACKGROUND_01"
	
	IF iPlayerSeatLocalID = 1
		sBackground = "HUD_WIN_BACKGROUND_02"
	ENDIF
	
	VECTOR_2D vBackgroundPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vBackgroundSCALE = INIT_VECTOR_2D(1264, 932)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackground, vBackgroundPos, vBackgroundScale, 0.0, rgbaDarker)
	
	IF iPlayerSeatLocalID = 0
		TEXT_LABEL_63 sBackgroundTail1 = "HUD_WIN_DOG_TAIL_FRAME_01"
		TEXT_LABEL_63 sBackgroundTail2 = "HUD_WIN_DOG_TAIL_FRAME_02"
		
		VECTOR_2D vBackgroundTailPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-372, (cfBASE_SCREEN_HEIGHT/2.0)+36)
		VECTOR_2D vBackgroundTailSCALE = INIT_VECTOR_2D(316, 400)
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sBackgroundTail1 = "HUD_WIN_CAT_TAIL_FRAME_01"
			sBackgroundTail2 = "HUD_WIN_CAT_TAIL_FRAME_02"
			
			vBackgroundTailPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-504, (cfBASE_SCREEN_HEIGHT/2.0)+62)
			
			vBackgroundTailSCALE = INIT_VECTOR_2D(236, 224)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sBackgroundTail1 = "HUD_WIN_DOG_TONGUE_FRAME_01"
			sBackgroundTail2 = "HUD_WIN_DOG_TONGUE_FRAME_02"
			
			vBackgroundTailSCALE = INIT_VECTOR_2D(136, 152)
			
			vBackgroundTailPos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-152, (cfBASE_SCREEN_HEIGHT/2.0)-152)
		ENDIF

			
		IF TIMERA() < 400		
			ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackgroundTail1, vBackgroundTailPos, vBackgroundTailScale, 0.0, rgbaDarker)
		ELSE
			ARCADE_DRAW_PIXELSPACE_SPRITE(ROAD_ARCADE_GET_LEADERBOARD_DICT(), sBackgroundTail2, vBackgroundTailPos, vBackgroundTailScale, 0.0, rgbaDarker)
		ENDIF

		IF TIMERA() >= 800
			SETTIMERA(0)
		ENDIF
	ENDIF
		
	RA_WRITE_STRING_USING_TEXT_SPRITES("FINAL RESULTS", INIT_VECTOR_2D(744,240), 1, TRUE)	
	
	IF NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(roadArcadePlayerBd[0].sPlayerID))
		RA_WRITE_STRING_USING_TEXT_SPRITES(TEXT_LABEL_TO_STRING(roadArcadePlayerBd[0].sPlayerID), INIT_VECTOR_2D(692,420))
	ENDIF
		
	IF NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(roadArcadePlayerBd[1].sPlayerID))
		RA_WRITE_STRING_USING_TEXT_SPRITES(TEXT_LABEL_TO_STRING(roadArcadePlayerBd[1].sPlayerID), INIT_VECTOR_2D(1212,420))
	ENDIF
	
	IF roadArcadePlayerBd[0].iScore >= 0
		ROAD_ARCADE_DRAW_SCORE(roadArcadePlayerBd[0].iScore, INIT_VECTOR_2D(508,492))
	ENDIF
	
	IF roadArcadePlayerBd[1].iScore >= 0
		ROAD_ARCADE_DRAW_SCORE(roadArcadePlayerBd[1].iScore, INIT_VECTOR_2D(1050,492))
	ENDIF

	// Print name of winner
	IF roadArcadePlayerBd[0].iScore != roadArcadePlayerBd[1].iScore
		
		INT iWinningId = 0
		
		IF ROAD_ARCADE_NET_DID_LOCAL_PLAYER_WIN(PARTICIPANT_ID_TO_INT())
			iWinningId = PARTICIPANT_ID_TO_INT()
		ELSE
			iWinningId = ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(PARTICIPANT_ID_TO_INT())
		ENDIF
		
		IF iWinningId != -1
			IF NOT IS_STRING_NULL_OR_EMPTY(roadArcadePlayerBd[iWinningId].sPlayerID)
				TEXT_LABEL_63 sWinner = roadArcadePlayerBd[iWinningId].sPlayerID
				sWINNER += "  WINS!"
				RA_WRITE_STRING_USING_TEXT_SPRITES(TEXT_LABEL_TO_STRING(sWINNER), INIT_VECTOR_2D(688,640), 2, TRUE, 1, 1)
			ENDIF
		ENDIF
	
		// local player won!
		IF PARTICIPANT_ID_TO_INT() = iWinningId
			RA_WRITE_STRING_USING_TEXT_SPRITES("CONGRATS!", INIT_VECTOR_2D(858,880), 1, TRUE, 1)	
		ELSE 
			RA_WRITE_STRING_USING_TEXT_SPRITES("HARD LUCK", INIT_VECTOR_2D(838,880), 1, TRUE)		
		ENDIF
	
	ELSE // Players tied!
		RA_WRITE_STRING_USING_TEXT_SPRITES("PLAYERS TIED!", INIT_VECTOR_2D(688,640), 2, TRUE, 1)
		
		RA_WRITE_STRING_USING_TEXT_SPRITES("NO CONTEST", INIT_VECTOR_2D(858,880), 1, TRUE)
	ENDIF

	
	
	IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


//---------STREAMING---------
FUNC STRING ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(STAGES_SETS stage)

	IF stage = STAGE_VICE_CITY
	
		RETURN sDICT_TrackVice
	
	ENDIF

	IF stage = STAGE_SAN_FIERRO
	
		RETURN sDICT_TrackFeirro
	
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
	
		RETURN sDICT_TrackLiberty
	
	ENDIF

	IF stage = STAGE_SAN_ANDREAS
	
		RETURN sDICT_TrackAndreas
	
	ENDIF

	IF stage = STAGE_LAS_VENTURAS
		RETURN sDICT_TrackVenturas
	ENDIF
	
	RETURN sDICT_TrackVice

ENDFUNC

FUNC BOOL HAVE_MENU_ASSETS_LOADED()

	UNUSED_PARAMETER(sPrefixDebugRoadArcade)
	
	bool bLoaded = true
	
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericAssets)
	
	STRING sMenuAssetsToLoad
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		sMenuAssetsToLoad = sDICT_SelectMenu
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		sMenuAssetsToLoad = sDICT_SelectMenu_Bike
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sMenuAssetsToLoad = sDICT_SelectMenu_Truck
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT(sMenuAssetsToLoad)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_SelectMenuRadio)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_HUDGeneric)
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_GenericAssets)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - Road assets are loading")
		bLoaded = false	
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sMenuAssetsToLoad)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - SELECT menu is loading")
		bLoaded = false
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_SelectMenuRadio)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - SELECT menu radio is loading")
		bLoaded = false
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_HUDGeneric)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - HUD elements are loading")
		bLoaded = false
	ENDIF
	
	IF bLoaded 
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - All textures have loaded!")
		ARCADE_CABINET_COMMON_INITIALIZATION()
	ENDIF
	
	RETURN bLoaded

ENDFUNC

PROC RELEASE_MENU_ASSETS()

	STRING sMenuAssetsToLoad = sDICT_SelectMenu

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		//REQUEST_STREAMED_TEXTURE_DICT(sDICT_SelectMenu)
		sMenuAssetsToLoad = sDICT_SelectMenu
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		//REQUEST_STREAMED_TEXTURE_DICT(sDICT_SelectMenu_Bike)
		sMenuAssetsToLoad = sDICT_SelectMenu_Bike
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sMenuAssetsToLoad = sDICT_SelectMenu_Truck
	ENDIF
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sMenuAssetsToLoad)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_SelectMenuRadio)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
	
	ARCADE_GAMES_SOUND_STOP(roadArcade_Radio) // Stop the radio sounds safely

ENDPROC

FUNC BOOL ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED(STAGES_SETS stageToLoad, STRING playerDict, bool bMpVersion = FALSE)

	UNUSED_PARAMETER(sPrefixDebugRoadArcade)
	
	bool bLoaded = true
	
	STRING stageDict = ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stageToLoad)
	
	REQUEST_STREAMED_TEXTURE_DICT(stageDict)
	
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - Player Dictionary ", playerDict)
	
	REQUEST_STREAMED_TEXTURE_DICT(playerDict)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_EffectsAssets)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericAssets)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_TitleAssets)
		
	IF bMpVersion
		REQUEST_STREAMED_TEXTURE_DICT(sDICT_RivalVehDistant)		
		REQUEST_STREAMED_TEXTURE_DICT(sDICT_HUDGeneric)
	ENDIF
		
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_GenericAssets)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - GENERIC assets are loading")
		bLoaded = false	
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(stageDict)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - STAGE assets are loading")
		bLoaded = false
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_EffectsAssets)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - EFFECTS assets are loading")
		bLoaded = false
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_TitleAssets)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - TITLE elements are loading")
		bLoaded = false
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(playerDict)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - PLAYER elements are loading , Dict: ", playerDict)
		bLoaded = false
	ENDIF
	
	IF bMpVersion
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_RivalVehDistant)
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - MP - Rival elements are loading")
			bLoaded = false
		ENDIF
		
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_HUDGeneric)
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - MP - Generic HUD elements are loading")
			bLoaded = false
		ENDIF
	ENDIF
	
	IF bLoaded 
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED - All textures have loaded!")
		ARCADE_CABINET_COMMON_INITIALIZATION()
	ENDIF
	
	RETURN bLoaded

ENDFUNC

PROC RELEASE_TITLE_ASSETS(STRING activeStageDict)

	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerAssets)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(activeStageDict)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_EffectsAssets)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_TitleAssets)
	
ENDPROC

FUNC BOOL HAVE_INGAME_ASSETS_LOADED(STAGES_SETS stageToLoad, PLAYER_VEHICLE& pvPlayer)

	UNUSED_PARAMETER(sPrefixDebugRoadArcade)
	
	// Override stage to load with server choice!
	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
		stageToLoad = roadArcadeServerBd.stageToStart
	ENDIF
	
	STRING strTrackDict =  sDICT_TrackVice
		
	SWITCH stageToLoad
	
		CASE STAGE_VICE_CITY
			strTrackDict =  sDICT_TrackVice
		BREAK
	
		CASE STAGE_SAN_FIERRO
			strTrackDict =  sDICT_TrackFeirro
		BREAK
	
		CASE STAGE_SAN_ANDREAS
			strTrackDict =  sDICT_TrackAndreas
		BREAK
		
		CASE STAGE_LAS_VENTURAS
			strTrackDict =  sDICT_TrackVenturas
		BREAK
	
		CASE STAGE_LIBERTY_CITY
			strTrackDict =  sDICT_TrackLiberty
		BREAK
	
	ENDSWITCH

	bool bLoaded = true
	
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericAssets)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_HUDGeneric)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_EffectsAssets)
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets
			pvPlayer.sSpriteFrame = "VEHICLE_CAR_01_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets
			pvPlayer.sSpriteFrame = "VEHICLE_CAR_02_FRAME_01"
		ENDIF
		
		// Same width and heigh across both sprites
		pvPlayer.iWidth = 676
		pvPlayer.iHeight = 260
	ENDIF
		
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets_Bike
			pvPlayer.sSpriteFrame = "VEHICLE_BIKE_01_FRAME_01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets_Bike
			pvPlayer.sSpriteFrame = "VEHICLE_BIKE_02_FRAME_01"
		ENDIF
		
		// Same width and heigh across both sprites
		pvPlayer.iWidth = 288
		pvPlayer.iHeight = 288
		
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			pvPlayer.sSpriteDict = sDICT_PlayerAssets_Truck
			pvPlayer.sSpriteFrame = "VEHICLE_TRUCK_01_FRAME_01"
			pvPlayer.sDriverFrameNo = "01"
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			pvPlayer.sSpriteDict = sDICT_PlayerTwoAssets_Truck
			pvPlayer.sSpriteFrame = "VEHICLE_TRUCK_02_FRAME_01"
		ENDIF
		
		pvPlayer.iWidth = 752
		pvPlayer.iHeight = 288
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT(pvPlayer.sSpriteDict)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_RivalVehDistant)
	REQUEST_STREAMED_TEXTURE_DICT(strTrackDict)
	
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericBike)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericCar)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericTruck)
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_GenericPickUp)
		
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_GenericAssets)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - Road assets are loading")
		bLoaded = false	
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(pvPlayer.sSpriteDict)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - Player assets are loading")
		bLoaded = false	
	ENDIF

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_HUDGeneric)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - HUD elements are loading")
		bLoaded = false
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_EffectsAssets)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - HUD elements are loading")
		bLoaded = false
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(strTrackDict)
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - STAGE elements are loading: ", strTrackDict)
		bLoaded = false
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - STAGE elements have loaded: ", strTrackDict)
	ENDIF
	
	IF bLoaded 
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "HAVE_MENU_ASSETS_LOADED - All textures have loaded!")
		ARCADE_CABINET_COMMON_INITIALIZATION()
	ENDIF
	
	RETURN bLoaded

ENDFUNC

PROC RELEASE_INGAME_ASSETS()

	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_GenericAssets)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_EffectsAssets)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_FinishLine)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_FinishLine_Bike)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_FinishLine_Truck)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_FinishCrowd)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_FinishGroups)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerAssets)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerCrash)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerTwoAssets)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerTwoCrash)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerAssets_Bike)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerCrash_Bike)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerTwoAssets_Bike)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerTwoCrash_Bike)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerAssets_Truck)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerCrash_Truck)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerTwoAssets_Truck)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_PlayerTwoCrash_Truck)
		
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_GenericBike)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_GenericCar)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_GenericTruck)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_GenericPickup)

	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_RivalVehDistant)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_TrackVice)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_TrackFeirro)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_TrackAndreas)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_TrackVenturas)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_TrackLiberty)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_EasterEggs)

ENDPROC

//--------------ROAD CREATION HELPERS ---------------

TWEAK_INT ciROAD_DEFAULT_SPRITE_WIDTH	2052
TWEAK_INT ciROAD_DEFAULT_SPRITE_HEIGHT	4
TWEAK_INT ciROAD_DEFAULT_TUNNEL_SIZE	32

TWEAK_FLOAT cfROAD_PatternScaleMod	0.6
TWEAK_FLOAT cfROAD_OVERScaleMod 0.6
TWEAK_FLOAT cfROAD_CROSSScaleMod 0.6
TWEAK_FLOAT cfROAD_VEHScaleMod 0.6

TWEAK_FLOAT cfROAD_SEGMENT_LENGTH 6.75

CONST_INT ciROAD_HORIZON_POINT_BASE 650

INT ciROAD_HORIZON_POINT = 650
FLOAT cfROAD_HillScale = 0.0
FLOAT cfROAD_HorizonScale = 0.99
FLOAT cfROAD_SideScaleMod =	0.6
FLOAT fROAD_RoadWidthMultiplier = 2.0

STRING sROAD_LineSegment = "RoadSectionA_W"
STRING sROAD_NoLineSegment = "RoadSectionB_W"
STRING sROAD_PATTERN = sROAD_LineSegment
STRING sGROUND_PATTERN = sROAD_LineSegment

PROC RETRIEVE_TRACK_CURVE_DATA(CURVE_DATA& cdToReturn, STAGES_SETS currentStage, FLOAT fCurrentZValue)

	cdToReturn.fCurvePixels = 0
	cdToReturn.fCurveInStart = 0.0
	cdToReturn.fCurveInEnd = 0.0
	cdToReturn.fCurveOutStart = 0.0
	cdToReturn.fCurveOutEnd = 0.0
	
	// Stage: Vice City
	IF currentStage = STAGE_VICE_CITY
		// Street Legal - Level data
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 200
			AND fCurrentZValue <= 600
			
				cdToReturn.fCurvePixels = 100
				cdToReturn.fCurveInStart = 200
				cdToReturn.fCurveInEnd = 325
				cdToReturn.fCurveOutStart = 375
				cdToReturn.fCurveOutEnd = 600
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 600
			AND fCurrentZValue <= 1200
			
				cdToReturn.fCurvePixels = -300
				cdToReturn.fCurveInStart = 600
				cdToReturn.fCurveInEnd = 800
				cdToReturn.fCurveOutStart = 1000
				cdToReturn.fCurveOutEnd = 1200
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 1300
			AND fCurrentZValue <= 2250
			
				cdToReturn.fCurvePixels = 200
				cdToReturn.fCurveInStart = 1300
				cdToReturn.fCurveInEnd = 1600
				cdToReturn.fCurveOutStart = 1950
				cdToReturn.fCurveOutEnd = 2250
			
			ENDIF
			
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 2400
			AND fCurrentZValue <= 2800
			
				cdToReturn.fCurvePixels = -300
				cdToReturn.fCurveInStart = 2400
				cdToReturn.fCurveInEnd = 2600
				cdToReturn.fCurveOutStart = 2600
				cdToReturn.fCurveOutEnd = 2800
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 2900
			AND fCurrentZValue <= 3700
			
				cdToReturn.fCurvePixels = 300
				cdToReturn.fCurveInStart = 2900
				cdToReturn.fCurveInEnd = 3150
				cdToReturn.fCurveOutStart = 3400
				cdToReturn.fCurveOutEnd = 3700
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 3900
			AND fCurrentZValue <= 4600
			
				cdToReturn.fCurvePixels = -350
				cdToReturn.fCurveInStart = 3900
				cdToReturn.fCurveInEnd = 4100
				cdToReturn.fCurveOutStart = 4250
				cdToReturn.fCurveOutEnd = 4600
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 5000
			AND fCurrentZValue <= 5700
			
				cdToReturn.fCurvePixels = 400
				cdToReturn.fCurveInStart = 5000
				cdToReturn.fCurveInEnd = 5350
				cdToReturn.fCurveOutStart = 5350
				cdToReturn.fCurveOutEnd = 5700
			
			ENDIF
			
		ENDIF
		
		// Crotch Rockets - Level design
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 365
			AND fCurrentZValue <= 930
			
				cdToReturn.fCurvePixels = -200
				cdToReturn.fCurveInStart = 365
				cdToReturn.fCurveInEnd = 525
				cdToReturn.fCurveOutStart = 665
				cdToReturn.fCurveOutEnd = 930
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 1200
			AND fCurrentZValue <= 1750
			
				cdToReturn.fCurvePixels = 400
				cdToReturn.fCurveInStart = 1200
				cdToReturn.fCurveInEnd = 1400
				cdToReturn.fCurveOutStart = 1550
				cdToReturn.fCurveOutEnd = 1750
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 1850
			AND fCurrentZValue <= 2950
			
				cdToReturn.fCurvePixels = -550
				cdToReturn.fCurveInStart = 1850
				cdToReturn.fCurveInEnd = 2200
				cdToReturn.fCurveOutStart = 2600
				cdToReturn.fCurveOutEnd = 2950
			
			ENDIF
			
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 3750
			AND fCurrentZValue <= 4950
			
				cdToReturn.fCurvePixels = 450
				cdToReturn.fCurveInStart = 3750
				cdToReturn.fCurveInEnd = 4000
				cdToReturn.fCurveOutStart = 4650
				cdToReturn.fCurveOutEnd = 4950
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 4950
			AND fCurrentZValue <= 6100
			
				cdToReturn.fCurvePixels = 400
				cdToReturn.fCurveInStart = 4950
				cdToReturn.fCurveInEnd = 5350
				cdToReturn.fCurveOutStart = 5750
				cdToReturn.fCurveOutEnd = 6100
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 6100
			AND fCurrentZValue <= 6500
			
				cdToReturn.fCurvePixels = -450
				cdToReturn.fCurveInStart = 6100
				cdToReturn.fCurveInEnd = 6300
				cdToReturn.fCurveOutStart = 6300
				cdToReturn.fCurveOutEnd = 6500
				
			ENDIF
		ENDIF

	ENDIF
	
	
	// Stage: san Feirro
	IF currentStage = STAGE_SAN_FIERRO
		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			IF fCurrentZValue > 2050
			AND fCurrentZValue <= 2850
			
				cdToReturn.fCurvePixels = 200
				cdToReturn.fCurveInStart = 2050
				cdToReturn.fCurveInEnd = 2300
				cdToReturn.fCurveOutStart = 2400
				cdToReturn.fCurveOutEnd = 2850
				
			ENDIF
			
			
			IF fCurrentZValue > 2850
			AND fCurrentZValue <= 3650
			
				cdToReturn.fCurvePixels = -200
				cdToReturn.fCurveInStart = 2850
				cdToReturn.fCurveInEnd = 3100
				cdToReturn.fCurveOutStart = 3200
				cdToReturn.fCurveOutEnd = 3650
				
			ENDIF
			
			
			IF fCurrentZValue > 4150
			AND fCurrentZValue <= 4950
			
				cdToReturn.fCurvePixels = -350
				cdToReturn.fCurveInStart = 4150
				cdToReturn.fCurveInEnd = 4425
				cdToReturn.fCurveOutStart = 4650
				cdToReturn.fCurveOutEnd = 4950
				
			ENDIF
			
			
			IF fCurrentZValue > 5100
			AND fCurrentZValue <= 5750
			
				cdToReturn.fCurvePixels = 375
				cdToReturn.fCurveInStart = 5100
				cdToReturn.fCurveInEnd = 5340
				cdToReturn.fCurveOutStart = 5500
				cdToReturn.fCurveOutEnd = 5750
				
			ENDIF
			
		ENDIF
		
		// Bike is a straight hill
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			IF fCurrentZValue > 730
			AND fCurrentZValue <= 2950
			
				cdToReturn.fCurvePixels = -100
				cdToReturn.fCurveInStart = 730
				cdToReturn.fCurveInEnd = 1500
				cdToReturn.fCurveOutStart = 2600
				cdToReturn.fCurveOutEnd = 2950
				
			ENDIF
			
			
			IF fCurrentZValue > 3500
			AND fCurrentZValue <= 5200
			
				cdToReturn.fCurvePixels = 100
				cdToReturn.fCurveInStart = 3500
				cdToReturn.fCurveInEnd = 3800
				cdToReturn.fCurveOutStart = 4150
				cdToReturn.fCurveOutEnd = 5200
				
			ENDIF
		ENDIF
		
		// Crotch Rockets - Level design
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			IF fCurrentZValue > 5700
			AND fCurrentZValue <= 6100
				cdToReturn.fCurvePixels = -480
				cdToReturn.fCurveInStart = 5700
				cdToReturn.fCurveInEnd = 5950
				cdToReturn.fCurveOutStart = 5950
				cdToReturn.fCurveOutEnd = 6100
			ENDIF
		
			IF fCurrentZValue > 6100
			AND fCurrentZValue <= 6550
				cdToReturn.fCurvePixels = 480
				cdToReturn.fCurveInStart = 6100
				cdToReturn.fCurveInEnd = 6300
				cdToReturn.fCurveOutStart = 6300
				cdToReturn.fCurveOutEnd = 6550
			ENDIF
		ENDIF
		
	ENDIF
	
	
	
	// Stage: San Andreas
	IF currentStage = STAGE_SAN_ANDREAS
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 540
			AND fCurrentZValue <= 2130
			
				cdToReturn.fCurvePixels = -350
				cdToReturn.fCurveInStart = 540
				cdToReturn.fCurveInEnd = 840
				cdToReturn.fCurveOutStart = 1400
				cdToReturn.fCurveOutEnd = 2130
			
			ENDIF

			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 2530
			AND fCurrentZValue <= 4125
			
				cdToReturn.fCurvePixels = 350
				cdToReturn.fCurveInStart = 2530
				cdToReturn.fCurveInEnd = 3000
				cdToReturn.fCurveOutStart = 3850
				cdToReturn.fCurveOutEnd = 4125
			
			ENDIF
			
			IF fCurrentZValue >= 5200
			AND fCurrentZValue <= 5725
			
				cdToReturn.fCurvePixels = 350
				cdToReturn.fCurveInStart = 5200
				cdToReturn.fCurveInEnd = 5360
				cdToReturn.fCurveOutStart = 5525
				cdToReturn.fCurveOutEnd = 5725
			
			ENDIF
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 340
			AND fCurrentZValue <= 2150
			
				cdToReturn.fCurvePixels = 400
				cdToReturn.fCurveInStart = 340
				cdToReturn.fCurveInEnd = 800
				cdToReturn.fCurveOutStart = 1500
				cdToReturn.fCurveOutEnd = 2150
			
			ENDIF

			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 2100
			AND fCurrentZValue <= 3600
			
				cdToReturn.fCurvePixels = -400
				cdToReturn.fCurveInStart = 2100
				cdToReturn.fCurveInEnd = 2400
				cdToReturn.fCurveOutStart = 2875
				cdToReturn.fCurveOutEnd = 3600
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 4400
			AND fCurrentZValue <= 5700
			
				cdToReturn.fCurvePixels = 350
				cdToReturn.fCurveInStart = 4400
				cdToReturn.fCurveInEnd = 5000
				cdToReturn.fCurveOutStart = 5350
				cdToReturn.fCurveOutEnd = 5700
			
			ENDIF
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 400
			AND fCurrentZValue <= 1500
			
				cdToReturn.fCurvePixels = 150
				cdToReturn.fCurveInStart = 400
				cdToReturn.fCurveInEnd = 800
				cdToReturn.fCurveOutStart = 1200
				cdToReturn.fCurveOutEnd = 1500
			
			ENDIF

			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 2400
			AND fCurrentZValue <= 3450
			
				cdToReturn.fCurvePixels = -200
				cdToReturn.fCurveInStart = 2400
				cdToReturn.fCurveInEnd = 3050
				cdToReturn.fCurveOutStart = 3050
				cdToReturn.fCurveOutEnd = 3450
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 3750
			AND fCurrentZValue <= 4470
			
				cdToReturn.fCurvePixels = 225
				cdToReturn.fCurveInStart = 3750
				cdToReturn.fCurveInEnd = 4050
				cdToReturn.fCurveOutStart = 4200
				cdToReturn.fCurveOutEnd = 4470
			
			ENDIF
		ENDIF
		
		// Crotch Rockets - Level design
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			IF fCurrentZValue > 4000
			AND fCurrentZValue <= 4500
				cdToReturn.fCurvePixels = -510
				cdToReturn.fCurveInStart = 4000
				cdToReturn.fCurveInEnd = 4250
				cdToReturn.fCurveOutStart = 4250
				cdToReturn.fCurveOutEnd = 4500
			ENDIF
		
			IF fCurrentZValue > 6500
			AND fCurrentZValue <= 7000
				cdToReturn.fCurvePixels = 510
				cdToReturn.fCurveInStart = 6500
				cdToReturn.fCurveInEnd = 6700
				cdToReturn.fCurveOutStart = 6800
				cdToReturn.fCurveOutEnd = 7000
			ENDIF
		ENDIF
		
	ENDIF
	
	
	
	// Stage: Las Venturas
	IF currentStage = STAGE_LAS_VENTURAS
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 400
			AND fCurrentZValue <= 2300
			
				cdToReturn.fCurvePixels = -300
				cdToReturn.fCurveInStart = 400
				cdToReturn.fCurveInEnd = 1300
				cdToReturn.fCurveOutStart = 2000
				cdToReturn.fCurveOutEnd = 2300
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 2400
			AND fCurrentZValue <= 4000
			
				cdToReturn.fCurvePixels = 300
				cdToReturn.fCurveInStart = 2400
				cdToReturn.fCurveInEnd = 3300
				cdToReturn.fCurveOutStart = 3450
				cdToReturn.fCurveOutEnd = 4000
				
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 4300
			AND fCurrentZValue <= 4700
			
				cdToReturn.fCurvePixels = -450
				cdToReturn.fCurveInStart = 4300
				cdToReturn.fCurveInEnd = 4500
				cdToReturn.fCurveOutStart = 4500
				cdToReturn.fCurveOutEnd = 4700
				
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 4700
			AND fCurrentZValue <= 5100
			
				cdToReturn.fCurvePixels = 450
				cdToReturn.fCurveInStart = 4700
				cdToReturn.fCurveInEnd = 4950
				cdToReturn.fCurveOutStart = 4950
				cdToReturn.fCurveOutEnd = 5100
				
			ENDIF
			
		ENDIF
		
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 275
			AND fCurrentZValue <= 1465
			
				cdToReturn.fCurvePixels = 400
				cdToReturn.fCurveInStart = 275
				cdToReturn.fCurveInEnd = 1000
				cdToReturn.fCurveOutStart = 1000
				cdToReturn.fCurveOutEnd = 1465
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 2400
			AND fCurrentZValue <= 4000
			
				cdToReturn.fCurvePixels = -500
				cdToReturn.fCurveInStart = 2400
				cdToReturn.fCurveInEnd = 3100
				cdToReturn.fCurveOutStart = 3400
				cdToReturn.fCurveOutEnd = 4000
				
			ENDIF
		
			IF fCurrentZValue > 4250
			AND fCurrentZValue <= 4450
			
				cdToReturn.fCurvePixels = -375
				cdToReturn.fCurveInStart = 4250
				cdToReturn.fCurveInEnd = 4350
				cdToReturn.fCurveOutStart = 4350
				cdToReturn.fCurveOutEnd = 4450
			
			ENDIF
			
			IF fCurrentZValue > 4450
			AND fCurrentZValue <= 4650
			
				cdToReturn.fCurvePixels = 375
				cdToReturn.fCurveInStart = 4450
				cdToReturn.fCurveInEnd = 4550
				cdToReturn.fCurveOutStart = 4550
				cdToReturn.fCurveOutEnd = 4650
				
			ENDIF
		
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 5200
			AND fCurrentZValue <= 6150
			
				cdToReturn.fCurvePixels = 285
				cdToReturn.fCurveInStart = 5200
				cdToReturn.fCurveInEnd = 5550
				cdToReturn.fCurveOutStart = 5850
				cdToReturn.fCurveOutEnd = 6150
				
			ENDIF
		
		
		
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 800
			AND fCurrentZValue <= 1500
			
				cdToReturn.fCurvePixels = 200
				cdToReturn.fCurveInStart = 800
				cdToReturn.fCurveInEnd = 1200
				cdToReturn.fCurveOutStart = 1300
				cdToReturn.fCurveOutEnd = 1500
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 2100
			AND fCurrentZValue <= 3900
			
				cdToReturn.fCurvePixels = 200
				cdToReturn.fCurveInStart = 2100
				cdToReturn.fCurveInEnd = 2750
				cdToReturn.fCurveOutStart = 3400
				cdToReturn.fCurveOutEnd = 3900
				
			ENDIF
			
			
			IF fCurrentZValue > 4450
			AND fCurrentZValue <= 4850
			
				cdToReturn.fCurvePixels = -435
				cdToReturn.fCurveInStart = 4450
				cdToReturn.fCurveInEnd = 4650
				cdToReturn.fCurveOutStart = 4650
				cdToReturn.fCurveOutEnd = 4850
				
			ENDIF
			
			IF fCurrentZValue > 4850
			AND fCurrentZValue <= 5300
			
				cdToReturn.fCurvePixels = 435
				cdToReturn.fCurveInStart = 4850
				cdToReturn.fCurveInEnd = 5050
				cdToReturn.fCurveOutStart = 5050
				cdToReturn.fCurveOutEnd = 5300
				
			ENDIF
			
		ENDIF
		
	ENDIF

	// Stage: San Andreas
	IF currentStage = STAGE_LIBERTY_CITY
	
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 540
			AND fCurrentZValue <= 1150
			
				cdToReturn.fCurvePixels = -250
				cdToReturn.fCurveInStart = 540
				cdToReturn.fCurveInEnd = 800
				cdToReturn.fCurveOutStart = 900
				cdToReturn.fCurveOutEnd = 1150
			
			ENDIF
			
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 2300
			AND fCurrentZValue <= 2950
			
				cdToReturn.fCurvePixels = -250
				cdToReturn.fCurveInStart = 2300
				cdToReturn.fCurveInEnd = 2650
				cdToReturn.fCurveOutStart = 2650
				cdToReturn.fCurveOutEnd = 2950
			
			ENDIF
			
			IF fCurrentZValue >= 3950
			AND fCurrentZValue <= 5525
			
				cdToReturn.fCurvePixels = 250
				cdToReturn.fCurveInStart = 3950
				cdToReturn.fCurveInEnd = 4150
				cdToReturn.fCurveOutStart = 5100
				cdToReturn.fCurveOutEnd = 5525
			
			ENDIF
		ENDIF
	
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 400
			AND fCurrentZValue <= 1850
			
				cdToReturn.fCurvePixels = 375
				cdToReturn.fCurveInStart = 400
				cdToReturn.fCurveInEnd = 875
				cdToReturn.fCurveOutStart = 1600
				cdToReturn.fCurveOutEnd = 1850
			
			ENDIF

			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue >= 1860
			AND fCurrentZValue <= 2400
			
				cdToReturn.fCurvePixels = -300
				cdToReturn.fCurveInStart = 1860
				cdToReturn.fCurveInEnd = 2100
				cdToReturn.fCurveOutStart = 2100
				cdToReturn.fCurveOutEnd = 2400
			
			ENDIF
				
			// Define any curves within ranges below
			// This will determine the overall shape of the track.
			IF fCurrentZValue > 3800
			AND fCurrentZValue <= 5150
			
				cdToReturn.fCurvePixels = -350
				cdToReturn.fCurveInStart = 3800
				cdToReturn.fCurveInEnd = 4100
				cdToReturn.fCurveOutStart = 4700
				cdToReturn.fCurveOutEnd = 5150
				
			ENDIF
			
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				IF fCurrentZValue > 6400
				AND fCurrentZValue <= 6700
				
					cdToReturn.fCurvePixels = -480
					cdToReturn.fCurveInStart = 6400
					cdToReturn.fCurveInEnd = 6550
					cdToReturn.fCurveOutStart = 6550
					cdToReturn.fCurveOutEnd = 6700
					
				ENDIF
				
				IF fCurrentZValue > 6700
				AND fCurrentZValue <= 7000
				
					cdToReturn.fCurvePixels = 500
					cdToReturn.fCurveInStart = 6700
					cdToReturn.fCurveInEnd = 6850
					cdToReturn.fCurveOutStart = 6850
					cdToReturn.fCurveOutEnd = 7000
					
				ENDIF
			ENDIF
			
		ENDIF
		
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK

			IF fCurrentZValue >= 1050
			AND fCurrentZValue <= 1500
			
				cdToReturn.fCurvePixels = -175
				cdToReturn.fCurveInStart = 1050
				cdToReturn.fCurveInEnd = 1300
				cdToReturn.fCurveOutStart = 1300
				cdToReturn.fCurveOutEnd = 1500
			
			ENDIF

				
			IF fCurrentZValue > 2000
			AND fCurrentZValue <= 2950
			
				cdToReturn.fCurvePixels = 150
				cdToReturn.fCurveInStart = 2000
				cdToReturn.fCurveInEnd = 2350
				cdToReturn.fCurveOutStart = 2550
				cdToReturn.fCurveOutEnd = 2950
				
			ENDIF
			
			IF fCurrentZValue >= 3450
			AND fCurrentZValue <= 4400
			
				cdToReturn.fCurvePixels = -200
				cdToReturn.fCurveInStart = 3450
				cdToReturn.fCurveInEnd = 3800
				cdToReturn.fCurveOutStart = 4000
				cdToReturn.fCurveOutEnd = 4400
			
			ENDIF
			
		ENDIF
			
	ENDIF
	
ENDPROC


// Curving logic 
FUNC float SCALING_ROAD_CURVATURE(STAGES_SETS currentStage, FLOAT fCurrentZValue, BOOL bInverse = FALSE)
	
	CURVE_DATA crvData
	RETRIEVE_TRACK_CURVE_DATA(crvData, currentStage, fCurrentZValue)
	
	// Store the curvature multiplier in this local variable
	float fCurveMultiplier

	IF fJOURNEY_DistanceTravelled <= 0
		RETURN 0.0
	ENDIF

	// Use an eased Cosine interpolation to control the curvature of the road.
	IF fCurrentZValue >= crvData.fCurveInStart
	AND fCurrentZValue <= crvData.fCurveInEnd
	
		float min = fCurrentZValue - crvData.fCurveInStart
		float max = crvData.fCurveInEnd - crvData.fCurveInStart
	
		fCurveMultiplier = COSINE_INTERP_FLOAT(0, 1, COSINE_INTERP_FLOAT(0, 1, min/max))
		
	ENDIF

	// Set this to 1 if we are in between values
	IF fCurrentZValue > crvData.fCurveInEnd
	AND fCurrentZValue < crvData.fCurveOutStart
	
		fCurveMultiplier = 1.0
		
	ENDIF

	// Don't reset the offset if the value is 0. Allows us to stack curves.
	if crvData.fCurveOutStart != 0.0

		// Use an eased Cosine interpolation to control the curvature of the road.
		if fCurrentZValue >= crvData.fCurveOutStart
		and fCurrentZValue <= crvData.fCurveOutEnd
		
			float min = fCurrentZValue - crvData.fCurveOutStart
			float max = crvData.fCurveOutEnd - crvData.fCurveOutStart
		
			fCurveMultiplier = 1 - COSINE_INTERP_FLOAT(0, 1, COSINE_INTERP_FLOAT(0, 1, min/max))
			
			IF bInverse
				fCurveMultiplier = COSINE_INTERP_FLOAT(0, 1, COSINE_INTERP_FLOAT(0, 1, min/max))
			ENDIF
						
		ENDIF
	
	ENDIF

		
	
	// Transition version
	IF transitionData.transition_ActiveState != T_INACTIVE
	
		IF transitionData.bSwitchCurve
			crvData.fCurvePixels = 225
		ELSE
			crvData.fCurvePixels = -225		
		ENDIF
	
		// Store the curvature multiplier in this local variable
		fCurveMultiplier = COSINE_INTERP_FLOAT(0, 1, COSINE_INTERP_FLOAT(0, 1, transitionData.fTransitionProgress))
		
	ENDIF

	// Fix interp juddering.
	fCurveMultiplier *= 100
	fCurveMultiplier = TO_FLOAT(FLOOR(fCurveMultiplier))
	fCurveMultiplier /= 100

	return fCurveMultiplier*crvData.fCurvePixels

ENDFUNC


FUNC FLOAT ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(STAGES_SETS currentStage, FLOAT fROAD_SpriteZValue, INT iROAD_RoadSpriteInstanceCounter, FLOAT fROAD_SpriteDistanceScale)
	RETURN SCALING_ROAD_CURVATURE(currentStage, fROAD_SpriteZValue)*((iROAD_RoadSpriteInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(1.0-(fROAD_SpriteDistanceScale)))
ENDFUNC

// Hill scale constants to describe track configuration 
CONST_INT TALL_HILL_HORIZON 386
CONST_FLOAT TALL_HILL_ROAD_WIDTH_MULTI 2.46
CONST_FLOAT TALL_HILL_SCALE 0.128
CONST_FLOAT TALL_HILL_SIDE_OBJ_SCALE 0.7

CONST_INT BLIND_HILL_HORIZON 688
CONST_FLOAT BLIND_HILL_TOP_WIDTH_SCALE 1.5
CONST_FLOAT BLIND_HILL_SCALE -0.5
CONST_FLOAT BLIND_HILL_SIDE_OBJ_SCALE 0.575

//CONST_INT MEDIUM_HILL_HORIZON 560
CONST_FLOAT MEDIUM_HILL_ROAD_WIDTH_MULTI 0.75
CONST_FLOAT MEDIUM_HILL_SCALE 0.005
CONST_FLOAT MEDIUM_HILL_SIDE_OBJ_SCALE 0.6

PROC RETRIEVE_TRACK_HILL_DATA(HILL_DATA& hdToReturn, STAGES_SETS currentStage, FLOAT fCurrentZValue)

	hdToReturn.iHorizonLine = 0
	hdToReturn.fRoadWidth = 0.0
	hdToReturn.fHillScale = 0.0
	hdToReturn.fHillInStart = 0.0
	hdToReturn.fHillInEnd = 0.0
	hdToReturn.fHillOutStart = 0.0
	hdToReturn.fHillOutEnd = 0.0

	IF currentStage = STAGE_VICE_CITY
	
		IF fCurrentZValue > 400
		AND fCurrentZValue < 600
		
			hdToReturn.iHorizonLine = 700
			hdToReturn.fRoadWidth = 1.25
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 400
			hdToReturn.fHillInEnd = 500
			hdToReturn.fHillOutStart = 500.0
			hdToReturn.fHillOutEnd = 600.0
		
		ENDIF
	
		IF fCurrentZValue > 1200
		AND fCurrentZValue < 1800
		
			hdToReturn.iHorizonLine = 690
			hdToReturn.fRoadWidth = 4.75
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 1200
			hdToReturn.fHillInEnd = 1400
			hdToReturn.fHillOutStart = 1550.0
			hdToReturn.fHillOutEnd = 1800.0
		
		ENDIF
	
		IF fCurrentZValue > 4500
		AND fCurrentZValue < 5600
		
			hdToReturn.iHorizonLine = 700
			hdToReturn.fRoadWidth = 1.25
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 4500
			hdToReturn.fHillInEnd = 5000
			hdToReturn.fHillOutStart = 5250.0
			hdToReturn.fHillOutEnd = 5600.0
		
		ENDIF
	
	ENDIF

	IF currentStage = STAGE_SAN_FIERRO
	
		// Blind hill in San Feirro
		IF fCurrentZValue > 150
		AND fCurrentZValue < 1000
		
			hdToReturn.iHorizonLine = BLIND_HILL_HORIZON
			hdToReturn.fRoadWidth = BLIND_HILL_TOP_WIDTH_SCALE
			hdToReturn.fHillScale = BLIND_HILL_SCALE
			hdToReturn.fSideScale = BLIND_HILL_SIDE_OBJ_SCALE
			hdToReturn.fHillInStart = 150
			hdToReturn.fHillInEnd = 350
			hdToReturn.fHillOutStart = 700.0
			hdToReturn.fHillOutEnd = 1000.0
		
		ENDIF
		
		// Tall hill in San Feirro
		IF fCurrentZValue > 1500
		AND fCurrentZValue < 2350
		
			hdToReturn.iHorizonLine = TALL_HILL_HORIZON
			hdToReturn.fRoadWidth = TALL_HILL_ROAD_WIDTH_MULTI
			hdToReturn.fHillScale = TALL_HILL_SCALE
			hdToReturn.fSideScale = TALL_HILL_SIDE_OBJ_SCALE
			
			hdToReturn.fHillInStart = 1500
			hdToReturn.fHillInEnd = 1800
			hdToReturn.fHillOutStart = 1900.0
			hdToReturn.fHillOutEnd = 2350.0
		
		ENDIF
	
		// Blind hill in San Feirro
		IF fCurrentZValue > 3050
		AND fCurrentZValue < 3750
		
			hdToReturn.iHorizonLine = BLIND_HILL_HORIZON
			hdToReturn.fRoadWidth = BLIND_HILL_TOP_WIDTH_SCALE
			hdToReturn.fHillScale = BLIND_HILL_SCALE
			hdToReturn.fSideScale = BLIND_HILL_SIDE_OBJ_SCALE
			hdToReturn.fHillInStart = 3050
			hdToReturn.fHillInEnd = 3300.0
			hdToReturn.fHillOutStart = 3425.0
			hdToReturn.fHillOutEnd = 3750.0
		
		ENDIF
	
		// Tall hill in San Feirro
		IF fCurrentZValue > 4050
		AND fCurrentZValue < 4800
		
			hdToReturn.iHorizonLine = TALL_HILL_HORIZON
			hdToReturn.fRoadWidth = TALL_HILL_ROAD_WIDTH_MULTI
			hdToReturn.fHillScale = TALL_HILL_SCALE
			hdToReturn.fSideScale = TALL_HILL_SIDE_OBJ_SCALE
			
			hdToReturn.fHillInStart = 4050.0
			hdToReturn.fHillInEnd = 4300.0
			hdToReturn.fHillOutStart = 4550.0
			hdToReturn.fHillOutEnd = 4800.0
		
		ENDIF
	
	ENDIF
	
	IF currentStage = STAGE_SAN_ANDREAS
	
		IF fCurrentZValue > 250
		AND fCurrentZValue < 700
		
			hdToReturn.iHorizonLine = 670
			hdToReturn.fRoadWidth = 4.75
			hdToReturn.fHillScale = 0.0125
			hdToReturn.fSideScale = 0.6
			hdToReturn.fHillInStart = 250
			hdToReturn.fHillInEnd = 550
			hdToReturn.fHillOutStart = 550.0
			hdToReturn.fHillOutEnd = 700.0
		
		ENDIF
	
		IF fCurrentZValue > 1750
		AND fCurrentZValue < 2300
		
			hdToReturn.iHorizonLine = 680
			hdToReturn.fRoadWidth = 1.75
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 1750
			hdToReturn.fHillInEnd = 1925
			hdToReturn.fHillOutStart = 2050.0
			hdToReturn.fHillOutEnd = 2300.0
		
		ENDIF
	
		IF fCurrentZValue > 4050
		AND fCurrentZValue < 4250
		
			hdToReturn.iHorizonLine = 660
			hdToReturn.fRoadWidth = MEDIUM_HILL_ROAD_WIDTH_MULTI
			hdToReturn.fHillScale = MEDIUM_HILL_SCALE
			hdToReturn.fSideScale = MEDIUM_HILL_SIDE_OBJ_SCALE
		
			hdToReturn.fHillInStart = 4050.0
			hdToReturn.fHillInEnd = 4145.0
			hdToReturn.fHillOutStart = 4175.0
			hdToReturn.fHillOutEnd = 4250.0
		
		ENDIF
	
	ENDIF

	IF currentStage = STAGE_LAS_VENTURAS
	
		IF fCurrentZValue > 250
		AND fCurrentZValue < 700
		
			hdToReturn.iHorizonLine = 675
			hdToReturn.fRoadWidth = 4.75
			hdToReturn.fHillScale = 0.0125
			hdToReturn.fSideScale = 0.6
			hdToReturn.fHillInStart = 250
			hdToReturn.fHillInEnd = 550
			hdToReturn.fHillOutStart = 550.0
			hdToReturn.fHillOutEnd = 700.0
		
		ENDIF
	
		IF fCurrentZValue > 1150
		AND fCurrentZValue < 1700
		
			hdToReturn.iHorizonLine = 665
			hdToReturn.fRoadWidth = 1.75
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 1150
			hdToReturn.fHillInEnd = 1425
			hdToReturn.fHillOutStart = 1550.0
			hdToReturn.fHillOutEnd = 1700.0
		
		ENDIF
	
		IF fCurrentZValue > 1850
		AND fCurrentZValue < 2050
		
			hdToReturn.iHorizonLine = 655
			hdToReturn.fRoadWidth = 3.55
			hdToReturn.fHillScale = -0.0015
			hdToReturn.fSideScale = 0.6
			hdToReturn.fHillInStart = 1850.0
			hdToReturn.fHillInEnd = 1945.0
			hdToReturn.fHillOutStart = 1945.0
			hdToReturn.fHillOutEnd = 2050.0
		
		ENDIF
//	
//		IF fCurrentZValue > 2600
//		AND fCurrentZValue < 3350
//		
//			hdToReturn.iHorizonLine = 580
//			hdToReturn.fRoadWidth = 3.9
//			hdToReturn.fHillScale = -0.0025
//			hdToReturn.fSideScale = 0.6
//			hdToReturn.fHillInStart = 2600.0
//			hdToReturn.fHillInEnd = 2945.0
//			hdToReturn.fHillOutStart = 3045.0
//			hdToReturn.fHillOutEnd = 3350.0
//		
//		ENDIF
	
		IF fCurrentZValue > 3550
		AND fCurrentZValue < 4600
		
			hdToReturn.iHorizonLine = 675
			hdToReturn.fRoadWidth = 1.55
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 3550
			hdToReturn.fHillInEnd = 4000.0
			hdToReturn.fHillOutStart = 4150.0
			hdToReturn.fHillOutEnd = 4600.0
		
		ENDIF
	
	
	ENDIF

	IF currentStage = STAGE_LIBERTY_CITY
	
		IF fCurrentZValue > 350
		AND fCurrentZValue < 800
		
			hdToReturn.iHorizonLine = 670
			hdToReturn.fRoadWidth = 4.75
			hdToReturn.fHillScale = 0.0125
			hdToReturn.fSideScale = 0.6
			hdToReturn.fHillInStart = 350
			hdToReturn.fHillInEnd = 550
			hdToReturn.fHillOutStart = 550.0
			hdToReturn.fHillOutEnd = 800.0
		
		ENDIF
	
		IF fCurrentZValue > 1050
		AND fCurrentZValue < 1700
		
			hdToReturn.iHorizonLine = 660
			hdToReturn.fRoadWidth = 1.75
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 1050
			hdToReturn.fHillInEnd = 1425
			hdToReturn.fHillOutStart = 1550.0
			hdToReturn.fHillOutEnd = 1700.0
		
		ENDIF
		
		IF fCurrentZValue > 3550
		AND fCurrentZValue < 4600
		
			hdToReturn.iHorizonLine = 655
			hdToReturn.fRoadWidth = 1.55
			hdToReturn.fHillScale = 0.0025
			hdToReturn.fSideScale = 0.65
			hdToReturn.fHillInStart = 3550
			hdToReturn.fHillInEnd = 4000.0
			hdToReturn.fHillOutStart = 4150.0
			hdToReturn.fHillOutEnd = 4600.0
		
		ENDIF
		
	
		IF fCurrentZValue > 4850
		AND fCurrentZValue < 5050
		
			hdToReturn.iHorizonLine = 652
			hdToReturn.fRoadWidth = 3.55
			hdToReturn.fHillScale = -0.0055
			hdToReturn.fSideScale = 0.47
			hdToReturn.fHillInStart = 1850.0
			hdToReturn.fHillInEnd = 1945.0
			hdToReturn.fHillOutStart = 1945.0
			hdToReturn.fHillOutEnd = 2050.0
		
		ENDIF
	
	ENDIF

ENDPROC

PROC SCALING_ROAD_HILL_MOD(STAGES_SETS currentStage, FLOAT fCurrentZValue, BOOL bInverse = FALSE)
	
	HILL_DATA hillData
	RETRIEVE_TRACK_HILL_DATA(hillData, currentStage, fCurrentZValue)
	
	// Store the curvature multiplier in this local variable
	float fHillMultiplier

	// Use an eased Cosine interpolation to control the curvature of the road.
	IF fCurrentZValue >= hillData.fHillInStart
	AND fCurrentZValue <= hillData.fHillInEnd
	
		float min = fCurrentZValue - hillData.fHillInStart
		float max = hillData.fHillInEnd - hillData.fHillInStart
	
		fHillMultiplier = COSINE_INTERP_FLOAT(0, 1, min/max)
		
	ENDIF

	// Set this to 1 if we are in between values
	IF fCurrentZValue > hillData.fHillInEnd
	AND fCurrentZValue < hillData.fHillOutStart
	
		fHillMultiplier = 1.0
		
	ENDIF

	if hillData.fHillOutStart != 0.0

		// Use an eased Cosine interpolation to control the curvature of the road.
		if fCurrentZValue >= hillData.fHillOutStart
		and fCurrentZValue <= hillData.fHillOutEnd
		
			float min = fCurrentZValue - hillData.fHillOutStart
			float max = hillData.fHillOutEnd - hillData.fHillOutStart
		
			fHillMultiplier = 1 - COSINE_INTERP_FLOAT(0, 1, min/max)
			
			IF bInverse
				fHillMultiplier = COSINE_INTERP_FLOAT(0, 1, min/max)
			ENDIF
			
		ENDIF
	
	ENDIF
	
	// Transition version
	IF transitionData.transition_ActiveState != T_INACTIVE
	
		// Blind hill from Fierro
		hillData.iHorizonLine = BLIND_HILL_HORIZON
		hillData.fRoadWidth = BLIND_HILL_TOP_WIDTH_SCALE
		hillData.fHillScale = BLIND_HILL_SCALE
		hillData.fSideScale = BLIND_HILL_SIDE_OBJ_SCALE

		// Store the curvature multiplier in this local variable
		fHillMultiplier = COSINE_INTERP_FLOAT(0, 1, transitionData.fTransitionProgress)
				
	ENDIF

	fHillMultiplier *= 100
	fHillMultiplier = TO_FLOAT(ROUND(fHillMultiplier))
	fHillMultiplier /= 100
		
	// Don't update the value it it's not a value of 4. Anti judder.
	IF ABSF(TO_FLOAT(ciROAD_HORIZON_POINT - (ciROAD_HORIZON_POINT_BASE + FLOOR((fHillMultiplier)*(hillData.iHorizonLine - ciROAD_HORIZON_POINT_BASE))))) > 4
		ciROAD_HORIZON_POINT = ciROAD_HORIZON_POINT_BASE + FLOOR((fHillMultiplier)*(hillData.iHorizonLine - ciROAD_HORIZON_POINT_BASE))
	ENDIF
		
	fROAD_RoadWidthMultiplier = 2.0 + ((fHillMultiplier)*(hillData.fRoadWidth - 2.0))
	cfROAD_HillScale = ((fHillMultiplier)*(hillData.fHillScale))
	cfROAD_SideScaleMod = 0.6 + ((fHillMultiplier)*(hillData.fSideScale - 0.6))

ENDPROC

FUNC FLOAT ROAD_ARCADE_GET_INVERTED_SCALE(float fDistanceScale)

	FLOAT fROAD_DistanceScale_Inverse = 1.0 - (fDistanceScale/2.0)
			
	fROAD_DistanceScale_Inverse = POW(fROAD_DistanceScale_Inverse, 2.0)
	fROAD_DistanceScale_Inverse *= 1.0-((cfROAD_HillScale/2.0))//*(fROAD_DistanceScale_Inverse))
	fROAD_DistanceScale_Inverse *= 2.0 // Side Objects are slightly smaller to save on texture space.
	
	IF (fROAD_DistanceScale_Inverse < 0.05)
		fROAD_DistanceScale_Inverse = 0.05
	ENDIF

	IF (fROAD_DistanceScale_Inverse > 2.0)
		fROAD_DistanceScale_Inverse = 2.0
	ENDIF

	RETURN fROAD_DistanceScale_Inverse
ENDFUNC

FUNC VECTOR_2D ROAD_ARCADE_GET_FINAL_SCALE(float fDistanceScale, VECTOR_2D vObjectScale)
	FLOAT fROAD_DistanceScale_Inverse = ROAD_ARCADE_GET_INVERTED_SCALE(fDistanceScale)
	
	vObjectScale = MULTIPLY_VECTOR_2D(vObjectScale, fROAD_DistanceScale_Inverse)
	
	vObjectScale.x += (vObjectScale.x%4.0)
	vObjectScale.y += (vObjectScale.y%4.0)
	
	return vObjectScale
ENDFUNC

FUNC FLOAT ROAD_ARCADE_GET_END_OF_STAGE(STAGES_SETS currentStage)

	IF currentStage = STAGE_VICE_CITY
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN 6175.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN 7225.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN 5805.0
		ENDIF
	ENDIF

	IF currentStage = STAGE_SAN_FIERRO
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN 6000.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN 7155.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN 5625.0
		ENDIF
	ENDIF

	IF currentStage = STAGE_SAN_ANDREAS
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN 6100.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN 7205.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN 5655.0
		ENDIF
	ENDIF

	IF currentStage = STAGE_LAS_VENTURAS
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN 6575.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN 6925.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN 5855.0
		ENDIF
	ENDIF

	IF currentStage = STAGE_LIBERTY_CITY
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			RETURN 6200.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			RETURN 7205.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			RETURN 5605.0
		ENDIF
	ENDIF

	RETURN -1.0

ENDFUNC

PROC CHANGE_GEAR(PLAYER_VEHICLE &player)

	ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_SHIFT_GEAR)

	IF player.pgPlayerGear = pgLow
		player.pgPlayerGear = pgHigh
		EXIT
	ENDIF
	
	IF player.pgPlayerGear = pgHigh
		player.pgPlayerGear = pgLow
		EXIT
	ENDIF

ENDPROC

PROC PLAYER_VEHICLE_CONTROLS(PLAYER_VEHICLE &player)

	// Gear transition
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RB)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CHANGE_GEAR(player)
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_RELOAD)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
			CHANGE_GEAR(player)
		ENDIF
	ENDIF
	
	// Handle player perfect peel out.
	// Min speed is always 1/2 max speed
	IF player.bPerfectStart
	AND fCountDownTimer >= 3000
		player.fPlayerSpeed = player.fMaxSpeed/2.0
		
		IF fCountDownTimer >= 5000 
			player.bPerfectStart = FALSE
		ENDIF
	ENDIF
	
	// Reset the angle
	FLOAT fTurningAngleMax = 0.0
	
	IF player.fPlayerLeftStickX != 0.0
				
		IF player.fPlayerLeftStickX < 0.0
		AND player.fPlayerSpeed > 0
			
			IF player.fPlayerLeftStickX <= -0.05
				fTurningAngleMax = 2.5
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.1
				fTurningAngleMax = 5
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.15
				fTurningAngleMax = 10
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.2
				fTurningAngleMax = 12.5
			ENDIF
						
			IF player.fPlayerLeftStickX <= -0.25
				fTurningAngleMax = 15
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.3
				fTurningAngleMax = 17.5
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.35
				fTurningAngleMax = 20
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.4
				fTurningAngleMax = 22.5
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.5
				fTurningAngleMax = 25
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.55
				fTurningAngleMax = 27.5
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.6
				fTurningAngleMax = 30
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.7
				fTurningAngleMax = 35
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.8
				fTurningAngleMax = 40
			ENDIF
			
			IF player.fPlayerLeftStickX <= -0.9
				fTurningAngleMax = 45
			ENDIF
			
			IF player.fPlayerLeftStickX <= -1.0
				fTurningAngleMax = 50
			ENDIF
			
		ENDIF
		
		IF player.fPlayerLeftStickX > 0.0
		AND player.fPlayerSpeed > 0	
			
			IF player.fPlayerLeftStickX >= 0.05
				fTurningAngleMax = -2.5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.1
				fTurningAngleMax = -5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.125
				fTurningAngleMax = -7.5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.15
				fTurningAngleMax = -10
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.2
				fTurningAngleMax = -12.5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.25
				fTurningAngleMax = -15
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.3
				fTurningAngleMax = -17.5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.35
				fTurningAngleMax = -20
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.4
				fTurningAngleMax = -22.5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.5
				fTurningAngleMax = -25
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.55
				fTurningAngleMax = -27.5
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.6
				fTurningAngleMax = -30
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.7
				fTurningAngleMax = -35
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.8
				fTurningAngleMax = -40
			ENDIF
			
			IF player.fPlayerLeftStickX >= 0.9
				fTurningAngleMax = -45
			ENDIF
				
			IF player.fPlayerLeftStickX >= 1.0
				fTurningAngleMax = -50
			ENDIF
																			
		ENDIF
			
	ENDIF
	
//	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "LLeft stick value - ", player.fPlayerLeftStickX )
	
	// Stick is at rest. 0.1 value considers damaged stick drifting.
	IF player.fPlayerLeftStickX <= 0.01
	AND player.fPlayerLeftStickX >= -0.01
		fTurningAngleMax = 0
	ENDIF
	
	IF fTurningAngleMax > 0
		player.fTurningAngle += (0+@(player.fHandling * 10))
		
		// Cap off the turning speed
		IF player.fTurningAngle > fTurningAngleMax
			player.fTurningAngle = fTurningAngleMax
		ENDIF
		
	ELIF fTurningAngleMax < 0
		player.fTurningAngle -= (0+@(player.fHandling * 10))
		
		// Cap off the turning speed		
		IF player.fTurningAngle < fTurningAngleMax
			player.fTurningAngle = fTurningAngleMax
		ENDIF
				
	ENDIF
	
	IF fTurningAngleMax = 0.0

		player.fTurningAngle = LERP_FLOAT(player.fTurningAngle, 0, player.fDrag/200)

		IF player.fTurningAngle < 5
		AND player.fTurningAngle > -5
			player.fTurningAngle = 0
		ENDIF
	
	ENDIF
	
	// Turning sounds
	IF player.fTurningAngle != 0 
	AND player.fPlayerSpeed > 40.0
		IF ABSF(player.fTurningAngle) >= 30 
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
			ENDIF
		ENDIF
			
		IF ABSF(player.fTurningAngle) >= 50 
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
			ENDIF
		ENDIF		
	ELSE
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
		ENDIF

		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
		ENDIF
		
	ENDIF
	
	// Acceleration
	player.fAccelerationNormal = 0
	player.fBrakingNormal = 0
	
	IF GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT) > 0.1
		player.fAccelerationNormal = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
	ENDIF
	
	IF GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LT) > 0.1
		player.fBrakingNormal = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LT)
	ENDIF
	
	IF RA_INPUT_IS_ACCEL_DOWN(FALSE)
		player.fAccelerationNormal = 1.0
	ENDIF
	
	// Keep normal within limits
	IF player.fAccelerationNormal > 1.0
		player.fAccelerationNormal = 1.0
	ENDIF
	
	IF player.fAccelerationNormal < 0
		player.fAccelerationNormal = 0
	ENDIF
	
	FLOAT fTargetSpeed = LERP_FLOAT(0, (player.fMaxSpeed*player.fMaxSpeedMod), player.fAccelerationNormal)
				
	// Accel and drag may need to be modded
	FLOAT fFinalAccel = player.fAcceleration*player.fAccelerationMod
	FLOAT fFinalDrag = player.fDrag*(player.fPlayerSpeed/player.fMaxSpeed)

	// Cap max speed in a low gear
	IF player.pgPlayerGear = pgLow
		fTargetSpeed *= (2.0/3.0)
		fFinalAccel *= 1.0-(SQRT(player.fPlayerSpeed/fTargetSpeed))		
	ENDIF
	
	// Higher speeds but slower acceleration
	IF player.pgPlayerGear = pgHigh
		fFinalAccel *= 1.0-POW(player.fPlayerSpeed/fTargetSpeed, player.fPlayerSpeed/(fTargetSpeed/2.0))
	ENDIF

	IF fFinalAccel < 0.1
		fFinalAccel = 0.1
	ENDIF
		
	// Mod Accel based on Turning Angle
	FLOAT fTurnAngleAbs = ABSF(player.fTurningAngle)
			
	IF fTurnAngleAbs != 0
		fFinalAccel *= 1-(fTurnAngleAbs/200)
	ENDIF

	// Get the current road width for determining if the player is off road.
	FLOAT fRoadWidth = (ciROAD_DEFAULT_SPRITE_WIDTH*(1-(35/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*cfROAD_HorizonScale))
	FLOAT fROAD_SpriteDistanceScale = ((player.fZvalue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
	fRoadWidth *= (POW(1.0-cfROAD_HillScale, 1.5))-(fROAD_SpriteDistanceScale*(cfROAD_HillScale*fROAD_RoadWidthMultiplier))
	fRoadWidth /= ciROAD_DEFAULT_SPRITE_WIDTH
	
	// Off road
	IF player.iPlayerPosX < player.fRoadCenter - (((ciROAD_DEFAULT_SPRITE_WIDTH*(fRoadWidth))*0.25))
	OR player.iPlayerPosX > player.fRoadCenter + (((ciROAD_DEFAULT_SPRITE_WIDTH*(fRoadWidth))*0.25))
		fFinalAccel *= 0.2
				
		IF !player.bOffRoad
			player.bOffRoad = TRUE
		ENDIF
		
	ELSE
		player.bOffRoad = FALSE
	ENDIF
	
	// Went too far off the road
	IF player.psActiveState != ps_Crashed
		IF player.bOffRoad
		
			IF player.fPlayerSpeed >= 7.5
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)
				SET_CONTROL_SHAKE(FRONTEND_CONTROL, 100, 100)
			ELSE
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)	
			ENDIF
		
			IF fXCarOffset >= (ciROAD_DEFAULT_SPRITE_WIDTH * 1.5)
			OR fXCarOffset <= -(ciROAD_DEFAULT_SPRITE_WIDTH * 1.5)
				player.psActiveState = ps_Crashed
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)
			ENDIF
						
		ELSE // Stop the sound if we are back on the road.
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)
		ENDIF
	ENDIF
	
	IF !bFinishedRace
	
		IF player.fPlayerSpeed < fTargetSpeed
			player.fPlayerSpeed += (0+@fFinalAccel)
		ELIF player.fPlayerSpeed > fTargetSpeed
			player.fPlayerSpeed -= (0+@fFinalDrag)
		ENDIF
					
		IF player.bOffRoad
			IF player.fPlayerSpeed > 15
				player.fPlayerSpeed -= (0+@fFinalDrag*2)
			ENDIF	
		ENDIF
				
		IF RA_INPUT_IS_LEFT_DOWN()
			player.fPlayerLeftStickX -= (0+@1000)
		ENDIF
	
		IF RA_INPUT_IS_RIGHT_DOWN()
			player.fPlayerLeftStickX += (0+@1000)
		ENDIF
		
		IF ABSF(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)) > 0.1
			player.fPlayerLeftStickX = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		ELSE // Check if no inputs are held
		
			// Movement	
			IF NOT RA_INPUT_IS_LEFT_DOWN()
			AND NOT RA_INPUT_IS_RIGHT_DOWN()
				player.fPlayerLeftStickX = 0.0
			ENDIF
		
		ENDIF
		
		
	ENDIF
	
	
	// Turning
	IF player.fTurningAngle != 0
	
		fXCarOffset = fXCarOffset +@(player.fPlayerSpeed*(player.fTurningAngle))

	ENDIF
		
	// Braking logic.			
	IF RA_INPUT_IS_BRAKE_DOWN()
		IF player.fPlayerSpeed > 0

			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
			ENDIF
			
		ELSE
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
			ENDIF
		ENDIF

		player.fAccelerationNormal = 0.0 // Zero accel normal.
		
		IF RA_INPUT_IS_BRAKE_DOWN(FALSE)
			player.fBrakingNormal = 1.0
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "APPLYING BRAKES THIS FRAME")
		ENDIF
		
		IF player.fPlayerSpeed > 0.0
			player.fPlayerSpeed -=  (0+@(player.fDrag*player.fBrakingNormal))
		ENDIF
	ELSE
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
		ENDIF
	ENDIF
	
	// Handle engine sound
	IF pfasCurrentFinishState < FS3_Dift_To_Stop
		
		IF player.psActiveState = ps_Drive
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
				ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR, "Speed", player.fPlayerSpeed)
				ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR, "Gear", TO_FLOAT(ENUM_TO_INT(player.pgPlayerGear)))
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
				ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE, "Speed", player.fPlayerSpeed)
				ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE, "Gear", TO_FLOAT(ENUM_TO_INT(player.pgPlayerGear)))
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
				ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK, "Speed", player.fPlayerSpeed)
				ARCADE_GAMES_SOUND_SET_VARIABLE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK, "Gear", TO_FLOAT(ENUM_TO_INT(player.pgPlayerGear)))
			ENDIF
		ENDIF
		
	ENDIF
	
	// Stop all the engine souds if we are offroad
	IF player.psActiveState = ps_Crashed
	OR pfasCurrentFinishState >= FS3_Dift_To_Stop
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
		ENDIF
	ENDIF

	
ENDPROC

PROC ROAD_ARCADE_SLIP_STREAM(PLAYER_VEHICLE &player, BOOL isSlipstreaming)

//	IF isSlipstreaming
//		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_SLIP_STREAM - Slipstreaming. Slip timer is: ", player.fSlipTime)
//	ENDIF

	// A slip state is active
	IF player.slipState != slipINACTIVE
		IF player.fAccelerationMod < 1.5
			player.fAccelerationMod += (0.0+@150)
		ELSE
			player.fAccelerationMod = 1.5
		ENDIF
	
		IF player.fPlayerSpeed >= 10.0
		
			IF player.fMaxSpeedMod < 1.25
				player.fMaxSpeedMod += (0.0+@75)
			ELSE
				player.fMaxSpeedMod = 1.25
			ENDIF
		
		ENDIF
	ENDIF

	SWITCH(player.slipState)
	
		CASE slipINACTIVE
			IF isSlipstreaming
				player.slipState = slipACTIVE
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_CAR)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_BIKE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_TRUCK)
				ENDIF
			ELSE
				IF player.fMaxSpeedMod > 1.0	
					player.fMaxSpeedMod -= (0.0+@50)
				ELSE
					player.fMaxSpeedMod = 1.0
				ENDIF
				
				IF player.fAccelerationMod > 1.0
					player.fAccelerationMod -= (0.0+@75)
				ELSE
					player.fAccelerationMod = 1.0
				ENDIF
			
			ENDIF
		BREAK
	
		CASE slipACTIVE

			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_TRUCK)
			ENDIF

			// Slip state inactive
			IF !isSlipstreaming
				player.slipState = slipINACTIVE
				
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_CAR)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_BIKE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_TRUCK)
				ENDIF
			ENDIF
			
			// 3 seconds in the slipstream
			IF player.fMaxSpeedMod >= 1.25
			AND player.fAccelerationMod >= 2.0
				player.slipState = slipMAXED
			ENDIF
			
		BREAK
		
		CASE slipMAXED
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_CAR)
			ENDIF
				
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_TRUCK)
			ENDIF
		
			// 3 seconds in the slipstream
			IF !isSlipstreaming
				player.fSlipTime = player.fOverdriveTime
				SET_CONTROL_SHAKE(FRONTEND_CONTROL, CEIL(player.fOverdriveTime), 90)
				player.slipState = slipOVERDRIVE
			ENDIF
		
		BREAK
		
		CASE slipOVERDRIVE
		
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_TRUCK)
			ENDIF

			player.fSlipTime -= (0.0+@1000) //
		
			IF player.fSlipTime <= 0.0
				player.fSlipTime = 0.0
				player.slipState = slipINACTIVE
				
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_CAR)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_BIKE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OVERDRIVE_LOOP_TRUCK)
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH 

ENDPROC

PROC PLAYER_ANIMATION(PLAYER_VEHICLE &player)
	
	string sNeutral = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
	string sSlightT = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
	string sMidT = "VEHICLE_CAR_01_FRAME_03_UP_HILL"

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			sNeutral = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
			sSlightT = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
		ELIF iPlayerSeatLocalID = 1
			sNeutral = "VEHICLE_CAR_02_FRAME_01_UP_HILL"
			sSlightT = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
		ENDIF
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		IF iPlayerSeatLocalID = 0
			sNeutral = "VEHICLE_BIKE_01_FRAME_01_UP_HILL"
			sSlightT = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
		ELIF iPlayerSeatLocalID = 1
			sNeutral = "VEHICLE_BIKE_02_FRAME_01_UP_HILL"
			sSlightT = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
		ENDIF
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			sNeutral = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01"
			sSlightT = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
			sMidT = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
		ELIF iPlayerSeatLocalID = 1
			sNeutral = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01"
			sSlightT = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
			sMidT = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
		ENDIF
	ENDIF

	player.sSpriteFrame = sNeutral
	
	FLOAT fTurningSpeed = player.fTurningAngle*player.fPlayerSpeed
	
	IF fTurningSpeed <= -350
		player.sSpriteFrame = sSlightT
	ENDIF

	IF fTurningSpeed <= -4000
		player.sSpriteFrame = sMidT
	ENDIF
	
	IF fTurningSpeed >= 350
		player.sSpriteFrame = sSlightT
	ENDIF

	IF fTurningSpeed >= 4000
		player.sSpriteFrame = sMidT
	ENDIF
	
ENDPROC

PROC GET_FX_TURN_OFFSETS(PLAYER_VEHICLE &player, VECTOR_2D &fxLeft, VECTOR_2D &fxRight)

	FLOAT fTurningSpeed = player.fTurningAngle*player.fPlayerSpeed
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			IF fTurningSpeed >= 350
				fxLeft.X -= 40
				fxRight.X -= 32
				fxLeft.Y += 12
				//fxRight.Y -= 4
				
			ENDIF

			IF fTurningSpeed >= 4000

				fxLeft.X -= 36
				fxRight.X -= 32
				
				fxLeft.Y += 12
				//fxRight.Y -= 4
				
			ENDIF
		
			IF fTurningSpeed <= -350
				fxLeft.X += 40
				fxRight.X += 32
				
				//fxLeft.Y -= 4
				fxright.Y += 12
				
			ENDIF

			IF fTurningSpeed <= -4000
			
				fxLeft.X += 32
				fxRight.X += 36
				
				fxRight.Y += 12
				//fxLeft.Y -= 4
						
			ENDIF
				
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
	
		fxLeft.X += 24
		fxRight.X -= 36
		
		IF fTurningSpeed >= -350
		AND fTurningSpeed <= 350
			fxLeft.Y -= 2
			fxRight.Y -= 2
		ENDIF
		
		IF fTurningSpeed <= -350
			fxLeft.X += 12
			fxRight.X -= 12
			fxRight.Y -= 9
			fxLeft.Y -= 13
		ENDIF
	
		IF fTurningSpeed >= 350
			fxRight.X -= 12
			fxLeft.X += 12
			fxLeft.Y -= 9
			fxRight.Y -= 13
		ENDIF
	
		fxLeft.Y += 2
		fxRight.Y += 2
	
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
	
		// Neutral Effect correction
		fxLeft.X = -164
		fxRight.X = 156
	
		IF fTurningSpeed >= 350
			fxLeft.X += 12
			fxRight.X += 12
		ENDIF

		IF fTurningSpeed >= 4000

			fxLeft.X += 16
			fxRight.X += 20
			
		ENDIF
	
		IF fTurningSpeed <= -350
			fxLeft.X -= 12
			fxRight.X -= 12
		ENDIF

		IF fTurningSpeed <= -4000
		
			fxLeft.X -= 16
			fxRight.X -= 20

		ENDIF
	ENDIF

ENDPROC

PROC PLAYER_VEHICLE_DRAW(PLAYER_VEHICLE &player)
	
	int xScale = player.iWidth
	
	// Flip the sprite if we are travelling left.
	IF (player.fTurningAngle*player.fPlayerSpeed) > 350 // Do not flip the first frame
		xScale = -xScale
	ENDIF
		
	VECTOR_2D vSpriteScale = INIT_VECTOR_2D(TO_FLOAT(xScale), TO_FLOAT(player.iHeight))
	
	VECTOR_2D vSpritePos = INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY))

	PLAYER_ANIMATION(player)
	
	player.fAnimationCounter += (0+@10) // Iterate the anim counter
	
	// Veh shadow - flash alternative frames! Emulates transprancy.
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_CAR_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ELIF iPlayerSeatLocalID = 1
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_CAR_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_BIKE_SHADOW", INIT_VECTOR_2D(vSpritePos.x + 8, (vSpritePos.y + (vSpriteScale.y/2.0))-20), INIT_VECTOR_2D(72, 52), 0.0, rgbaOriginal)	
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_TRUCK_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ELIF iPlayerSeatLocalID = 1
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_TRUCK_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
	ENDIF
	
	// Draw the car sprite over the shadow
	ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, player.sSpriteFrame, vSpritePos, vSpriteScale, 0.0, rgbaOriginal)

	// Driver Sprite - Draw over the top of the car
	// Will only appear under these these conditions
	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
	AND iPlayerSeatLocalID = 0
		if player.fAnimationCounter > player.fAnimationSpeed
		
			if ARE_STRINGS_EQUAL(player.sDriverFrameNo, "01")
				player.sDriverFrameNo = "02"
			ELSE
				player.sDriverFrameNo = "01"
			ENDIF

		ENDIF
		
		// Combine the strings to form the frame name of the driver sprite.
		TEXT_LABEL_63 driverFrame = player.sSpriteFrame 
		driverFrame += "_DRIVER_FRAME_"	
		driverFrame += player.sDriverFrameNo
		
		VECTOR_2D vDriverScale = INIT_VECTOR_2D(236.0, vSpriteScale.y)
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			vDriverScale = INIT_VECTOR_2D(288.0, 288)
		ENDIF
		
		IF (player.fTurningAngle*player.fPlayerSpeed) > 350
			driverFrame += "_LEFT"
			vDriverScale.x = ABSF(vDriverScale.x) // We don't flip the Driver & Pup!
		ENDIF
				
		ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, TEXT_LABEL_TO_STRING(driverFrame), 
			vSpritePos,
			vDriverScale, 0.0, rgbaOriginal)
	ENDIF
		
	particleFxAnim += (0.0+@1000)
		
	IF particleFxAnim >= 500
		particleFxAnim = 0.0
	ENDIF
		
	// Draw burnout effects by the wheels.
	IF player.fPlayerSpeed > 0
	AND	player.fPlayerSpeed < 20
	AND player.fAccelerationNormal >= 0.8
	
		String sSmokeFrame = "VEHICLE_ASSET_SMOKE_01_FRAME_01"
		

		IF particleFxAnim <= 250
			sSmokeFrame = "VEHICLE_ASSET_SMOKE_01_FRAME_01"
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				sSmokeFrame = "VEHICLE_BIKE_ASSET_SMOKE_01_FRAME_01"
			ENDIF
			
		ELSE
			sSmokeFrame = "VEHICLE_ASSET_SMOKE_01_FRAME_02"
			
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				sSmokeFrame = "VEHICLE_BIKE_ASSET_SMOKE_01_FRAME_02"
			ENDIF
			
		ENDIF
		
		// Shake if we are in burnout
		SET_CONTROL_SHAKE(FRONTEND_CONTROL, 50, 200)
		
		VECTOR_2D vSmokeSpriteScaleRIGHT = INIT_VECTOR_2D(-292.0,80.0)
		VECTOR_2D vSmokeSpriteScaleLEFT = INIT_VECTOR_2D(292.0,80.0)
		
		VECTOR_2D vSmokePosLEFT = INIT_VECTOR_2D(0.0,0.0)
		VECTOR_2D vSmokePosRIGHT = INIT_VECTOR_2D(0.0,0.0)
			
		vSmokePosLEFT.x -=  ((ABSF(vSpriteScale.x/2.0) - (vSmokeSpriteScaleLEFT.x/2.0))+32.0)
		vSmokePosRIGHT.x += ((ABSF(vSpriteScale.x/2.0) - (vSmokeSpriteScaleLEFT.x/2.0))+32.0)
		
		GET_FX_TURN_OFFSETS(player, vSmokePosLEFT, vSmokePosRIGHT)
				
		FLOAT vSmokePosY = (-vSpriteScale.y/2.0) 
		vSmokePosY += (vSmokeSpriteScaleLEFT.y/2.0)
		
		vSmokePosLEFT.y = vSmokePosY
		vSmokePosRIGHT.y = vSmokePosY
	
		VECTOR_2D vFINALleft = SUBTRACT_VECTOR_2D(vSpritePos, vSmokePosLEFT)
		VECTOR_2D vFINALright = SUBTRACT_VECTOR_2D(vSpritePos, vSmokePosRIGHT)
		
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sSmokeFrame, vFINALleft, vSmokeSpriteScaleLEFT, 0.0, rgbaOriginal)
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sSmokeFrame, vFINALright, vSmokeSpriteScaleRIGHT, 0.0, rgbaOriginal)
	ENDIF

	// Draw dust effects by the wheels.
	IF player.fPlayerSpeed >= 20
	
		String sDustFrame = "VEHICLE_ASSET_DUST_01_FRAME_01"
		

		IF particleFxAnim <= 250
			sDustFrame = "VEHICLE_ASSET_DUST_01_FRAME_01"
		ELSE
			sDustFrame = "VEHICLE_ASSET_DUST_01_FRAME_02"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			IF particleFxAnim <= 250
				sDustFrame = "VEHICLE_BIKE_ASSET_DUST_01_FRAME_01"
			ELSE
				sDustFrame = "VEHICLE_BIKE_ASSET_DUST_01_FRAME_02"
			ENDIF
		ENDIF
		
		VECTOR_2D vDustSpriteScaleRIGHT = INIT_VECTOR_2D(-292.0,80.0)
		VECTOR_2D vDustSpriteScaleLEFT = INIT_VECTOR_2D(292.0,80.0)
				
		VECTOR_2D vDustPosLEFT = INIT_VECTOR_2D(0.0,0.0)
		VECTOR_2D vDustPosRIGHT = INIT_VECTOR_2D(0.0,0.0)
			
		vDustPosLEFT.x -=  ((ABSF(vSpriteScale.x/2.0) - (vDustSpriteScaleLEFT.x/2.0))+40.0)
		vDustPosRIGHT.x += ((ABSF(vSpriteScale.x/2.0) - (vDustSpriteScaleLEFT.x/2.0))+40.0)
		FLOAT vDustPosY = (-vSpriteScale.y/2.0) 
		vDustPosY += (vDustSpriteScaleLEFT.y/2.0)
		vDustPosY += 8.0 // Micro adjustment
		
		vDustPosLEFT.y = vDustPosY
		vDustPosRIGHT.y = vDustPosY
		
		GET_FX_TURN_OFFSETS(player, vDustPosLEFT, vDustPosRIGHT)
		
		VECTOR_2D vFINALleft = SUBTRACT_VECTOR_2D(vSpritePos, vDustPosLEFT)
		VECTOR_2D vFINALright = SUBTRACT_VECTOR_2D(vSpritePos, vDustPosRIGHT)
	
		// Offset system is unresponsive in the case of the Rival Car.
		// Due to time and risks I'm placing sprite offset logic here.		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			IF iPlayerSeatLocalID = 0
				vFINALleft.y += 4.0 // Micro adjustment
				vFINALright.y += 4.0
			ENDIF
			
			IF iPlayerSeatLocalID = 1
				vFINALleft.x -= 8.0
				vFINALleft.y += 2.0
				vFINALright.y += 2.0
						
				FLOAT fTurningSpeed = player.fTurningAngle*player.fPlayerSpeed

				IF fTurningSpeed <= -350
					
					vFINALleft.Y += 8
					vFINALleft.X -= 12
					vFINALright.X -= 4

					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
		
				IF fTurningSpeed <= -4000
					
					vFINALleft.Y += 8
					vFINALleft.X -= 12
					vFINALright.X -= 4

					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
		
				IF fTurningSpeed >= 350
					
					vFINALright.Y += 8
					vFINALright.X += 4
					vFINALleft.X += 8
										
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
				
				IF fTurningSpeed >= 4000
					
					vFINALright.Y += 8
					vFINALright.X += 4
					vFINALleft.X += 8
										
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
				
			ENDIF
			
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			IF iPlayerSeatLocalID = 1
				vFINALleft.Y -= 8
				vFINALright.Y -= 8
				
				vFINALleft.X -= 32
				vFINALright.X += 12
				
			ENDIF
		ENDIF
			
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sDustFrame, vFINALleft, vDustSpriteScaleLEFT, 0.0, rgbaOriginal)
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sDustFrame, vFINALright, vDustSpriteScaleRIGHT, 0.0, rgbaOriginal)
		
	ENDIF
	
	// Draw flames by the wheels
	IF player.slipState != slipINACTIVE
	OR player.bPerfectStart
		STRING sFlameFrame = "VEHICLE_ASSET_FIRE_01_FRAME_01"
		

		
		if particleFxAnim <= 250.0
			sFlameFrame = "VEHICLE_ASSET_FIRE_01_FRAME_01"
		ELSE
			sFlameFrame = "VEHICLE_ASSET_FIRE_01_FRAME_02"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			IF particleFxAnim <= 250
				sFlameFrame = "VEHICLE_BIKE_ASSET_FIRE_01_FRAME_01"
			ELSE
				sFlameFrame = "VEHICLE_BIKE_ASSET_FIRE_01_FRAME_02"
			ENDIF
		ENDIF
		
		VECTOR_2D vSmokeSpriteScaleRIGHT = INIT_VECTOR_2D(-292.0,80.0)
		VECTOR_2D vSmokeSpriteScaleLEFT = INIT_VECTOR_2D(292.0,80.0)
		
		VECTOR_2D vSmokePosLEFT = INIT_VECTOR_2D(0.0,0.0)
		VECTOR_2D vSmokePosRIGHT = INIT_VECTOR_2D(0.0,0.0)
			
		vSmokePosLEFT.x -=  ((ABSF(vSpriteScale.x/2.0) - (vSmokeSpriteScaleLEFT.x/2.0))+32.0)
		vSmokePosRIGHT.x += ((ABSF(vSpriteScale.x/2.0) - (vSmokeSpriteScaleLEFT.x/2.0))+32.0)
						
		FLOAT vSmokePosY = (-vSpriteScale.y/2.0) 
		vSmokePosY += (vSmokeSpriteScaleLEFT.y/2.0)
		vSmokePosY += 8
						
		vSmokePosLEFT.y = vSmokePosY
		vSmokePosRIGHT.y = vSmokePosY
	
		GET_FX_TURN_OFFSETS(player, vSmokePosLEFT, vSmokePosRIGHT)
						
		VECTOR_2D vFINALleft = SUBTRACT_VECTOR_2D(vSpritePos, vSmokePosLEFT)
		VECTOR_2D vFINALright = SUBTRACT_VECTOR_2D(vSpritePos, vSmokePosRIGHT)

		// Offset system is unresponsive in the case of the Rival Car.
		// Due to time and risks I'm placing sprite offset logic here.		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			IF iPlayerSeatLocalID = 0
				vFINALleft.y += 4.0 // Micro adjustment
				vFINALright.y += 4.0
			ENDIF
			
			IF iPlayerSeatLocalID = 1
				vFINALleft.x -= 8.0
				vFINALleft.y += 2.0
				vFINALright.y += 2.0
						
				FLOAT fTurningSpeed = player.fTurningAngle*player.fPlayerSpeed

				IF fTurningSpeed <= -350
					
					vFINALleft.Y += 8
					vFINALleft.X -= 8
					vFINALright.X -= 4

					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
		
				IF fTurningSpeed <= -4000
					
					vFINALleft.Y += 8
					vFINALleft.X -= 12
					vFINALright.X -= 4

					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
		
				IF fTurningSpeed >= 350
					
					vFINALright.Y += 8
					vFINALright.X += 4
					vFINALleft.X += 8
										
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
				
				IF fTurningSpeed >= 4000
					
					vFINALright.Y += 8
					vFINALright.X += 4
					vFINALleft.X += 8
										
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
				
			ENDIF
			
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			IF iPlayerSeatLocalID = 1
//				vFINALleft.Y -= 8
//				vFINALright.Y -= 8
				
				vFINALleft.X -= 48
				vFINALright.X += 20
				
			ENDIF
		ENDIF

//		// Bring flames up a bit.
//		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
//			vFINALleft.y -= 12
//			vFINALRIGHT.y -= 12
//		ENDIF
//			
//		IF eRoadArcade_ActiveGameType = RA_GT_CAR
//			vFINALleft.y -= 8
//			vFINALRIGHT.y -= 8
//		ENDIF

		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			vFINALleft.y += 12
			vFINALRIGHT.y += 12
		ENDIF
						
		IF player.slipState = slipOVERDRIVE
		OR player.bPerfectStart
			ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sFlameFrame, vFINALleft, vSmokeSpriteScaleLEFT, 0.0, rgbaOriginal)
			ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sFlameFrame, vFINALRight, vSmokeSpriteScaleRIGHT, 0.0, rgbaOriginal)
		ENDIF
			
		slipStreamFxAnim += (0.0+@1000.0)
			
		IF slipStreamFxAnim >= 250
			slipStreamFxAnim = 0.0
		ENDIF
			
		if player.fPlayerSpeed > 20.0
		AND slipStreamFxAnim <= 125
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_EffectsAssets, "VEHICLE_ASSET_SLIP_STREAM_01_FRAME_01", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0) - 200.0), INIT_VECTOR_2D(1264,564), 0.0, rgbaOriginal)
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_SLIPSTREAMING)
		ENDIF
		
	ENDIF
	
	IF player.bOffRoad
	AND player.fPlayerSpeed > 10.0
		String sSmokeFrame = "VEHICLE_ASSET_SMOKE_01_FRAME_01"
		
		IF particleFxAnim < 250
			sSmokeFrame = "VEHICLE_ASSET_SMOKE_01_FRAME_01"
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				sSmokeFrame = "VEHICLE_BIKE_ASSET_SMOKE_01_FRAME_01"
			ENDIF
			
		ELSE
			sSmokeFrame = "VEHICLE_ASSET_SMOKE_01_FRAME_02"
			
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				sSmokeFrame = "VEHICLE_BIKE_ASSET_SMOKE_01_FRAME_02"
			ENDIF
			
		ENDIF
		
		VECTOR_2D vSmokeSpriteScaleRIGHT = INIT_VECTOR_2D(-292.0,80.0)
		VECTOR_2D vSmokeSpriteScaleLEFT = INIT_VECTOR_2D(292.0,80.0)
		
		VECTOR_2D vSmokePosLEFT = INIT_VECTOR_2D(0.0,0.0)
		VECTOR_2D vSmokePosRIGHT = INIT_VECTOR_2D(0.0,0.0)
			
		vSmokePosLEFT.x -=  ((ABSF(vSpriteScale.x/2.0) - (vSmokeSpriteScaleLEFT.x/2.0))+32.0)
		vSmokePosRIGHT.x += ((ABSF(vSpriteScale.x/2.0) - (vSmokeSpriteScaleLEFT.x/2.0))+32.0)
		
		GET_FX_TURN_OFFSETS(player, vSmokePosLEFT, vSmokePosRIGHT)
				
		FLOAT vSmokePosY = (-vSpriteScale.y/2.0) 
		vSmokePosY += (vSmokeSpriteScaleLEFT.y/2.0)
		vSmokePosY += 2
		
		vSmokePosLEFT.y = vSmokePosY
		vSmokePosRIGHT.y = vSmokePosY
	
		VECTOR_2D vFINALleft = SUBTRACT_VECTOR_2D(vSpritePos, vSmokePosLEFT)
		VECTOR_2D vFINALright = SUBTRACT_VECTOR_2D(vSpritePos, vSmokePosRIGHT)
		
		
		// Offset system is unresponsive in the case of the Rival Car.
		// Due to time and risks I'm placing sprite offset logic here.		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			IF iPlayerSeatLocalID = 0
				vFINALleft.y += 4.0 // Micro adjustment
				vFINALright.y += 4.0
			ENDIF
			
			IF iPlayerSeatLocalID = 1
				vFINALleft.x -= 8.0
				vFINALleft.y += 2.0
				vFINALright.y += 2.0
						
				FLOAT fTurningSpeed = player.fTurningAngle*player.fPlayerSpeed

				IF fTurningSpeed <= -350
					
					vFINALleft.Y += 8
					vFINALleft.X -= 12
					vFINALright.X -= 4

					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
		
				IF fTurningSpeed <= -4000
					
					vFINALleft.Y += 8
					vFINALleft.X -= 12
					vFINALright.X -= 4

					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
		
				IF fTurningSpeed >= 350
					
					vFINALright.Y += 8
					vFINALright.X += 4
					vFINALleft.X += 8
										
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
				
				IF fTurningSpeed >= 4000
					
					vFINALright.Y += 8
					vFINALright.X += 4
					vFINALleft.X += 8
										
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "GET_FX_TURN_OFFSETS - fTurningSpeed <= -350")
				ENDIF
				
			ENDIF
			
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			IF iPlayerSeatLocalID = 1
				vFINALleft.Y -= 8
				vFINALright.Y -= 8
				
				vFINALleft.X -= 48
				vFINALright.X += 20
				
			ENDIF
		ENDIF
		
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sSmokeFrame, vFINALleft, vSmokeSpriteScaleLEFT, 0.0, rgbaOriginal)
		ARCADE_CABINET_DRAW_PIXELSPACE_SPRITE_CORRECTED(sDICT_EffectsAssets, sSmokeFrame, vFINALright, vSmokeSpriteScaleRIGHT, 0.0, rgbaOriginal)
	ENDIF
	
	// Reset the counter
	if player.fAnimationCounter > player.fAnimationSpeed
		player.fAnimationCounter = 0.0 // reset the counter
	ENDIF

ENDPROC

// Anim States - Skid

ENUM PLAYER_SWERVE_STATE

	SA1_Spawn_EFFECT,
	SA2_SWERVE

ENDENUM

PLAYER_SWERVE_STATE pssSkidState

PROC RA_TRIGGER_PLAYER_SWERVE(PLAYER_VEHICLE &player)
	IF player.psActiveState != ps_Swerving
	
		IF !player.bCrashedOnce
			player.bCrashedOnce = TRUE
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "RA_TRIGGER_PLAYER_SWERVE - Setting bCrashedOnce to TRUE")
		ENDIF
		sRoadArcadeTelemetry.kills++
				
		pssSkidState = SA1_Spawn_EFFECT // Reset the animation state
		player.psActiveState = ps_Swerving
	ENDIF
ENDPROC

FUNC BOOL ROAD_ARCADE_HANDLE_PLAYER_SWERVE(PLAYER_VEHICLE &player)

	VECTOR_2D vSpritePos = INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY))
	VECTOR_2D vSpriteScale = INIT_VECTOR_2D(TO_FLOAT(player.iWidth), TO_FLOAT(player.iHeight))
	VECTOR_2D vDriverScale = INIT_VECTOR_2D(236, 288)
		
	// Car shadow - flash alternative frames! Emulates transprancy.
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_CAR_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_CAR_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
	ENDIF
	
	// Bike shadow
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_BIKE_SHADOW", INIT_VECTOR_2D(vSpritePos.x + 8, (vSpritePos.y + (vSpriteScale.y/2.0))-20), INIT_VECTOR_2D(72, 52), 0.0, rgbaOriginal)	
	ENDIF
	
	// Truck
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
	
		vDriverScale = INIT_VECTOR_2D(288, 288)
	
		IF iPlayerSeatLocalID = 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_TRUCK_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_TRUCK_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
	ENDIF
	
	STRING sPlayerTexture
	STRING sPlayerDriver
	
	// We'll play a SFX if the player is swerving out of control.
	IF player.fPlayerSpeed > 15
		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
		ENDIF

		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
		ENDIF
		
	ELSE
		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
		ENDIF

		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
		ENDIF
		
	ENDIF
	
	SWITCH(pssSkidState)
	
		CASE SA1_Spawn_EFFECT
		
			// Flashing effect
			ARCADE_DRAW_PIXELSPACE_RECT(
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0),
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH,cfBASE_SCREEN_HEIGHT),
				rgbaVicePurple)
				
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_EffectsAssets, 
				"VEHICLE_ASSET_IMPACT_01_FRAME_01",
				INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY) - 100),
				vSpriteScale,
				0.0,
				rgbaOriginal)
				
			// Standard sound to play upon impact.
			ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_COLLISION_GEN)
			
			sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
			sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
			
			IF iPlayerSeatLocalID = 0
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
					sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					sPlayerTexture = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
					sPlayerDriver = "VEHICLE_BIKE_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
					sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01"
				ENDIF
			ELSE
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					sPlayerTexture = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
					sPlayerDriver = "VEHICLE_CAR_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					sPlayerTexture = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
					sPlayerDriver = "VEHICLE_BIKE_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
					sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02_DRIVER_FRAME_01"
				ENDIF
			ENDIF
			
				
			SETTIMERA(0)

			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 500, 125)
							
			pssSkidState = SA2_SWERVE
		
		BREAK
	
		CASE SA2_SWERVE
		
			IF player.fPlayerSpeed > 0	
				player.fPlayerSpeed = player.fPlayerSpeed -@(player.fDrag*player.fPenaltyMod)
			ELSE
				player.fPlayerSpeed = 0
			ENDIF
		
			IF (TIMERA() < 100)
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF
		
		
			IF (TIMERA() >= 100 AND TIMERA() < 200)
			
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF
		
			IF (TIMERA() >= 200 AND TIMERA() < 300)
						
				
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03_DRIVER_FRAME_01"
					ENDIF
				ENDIF
								
			ENDIF
		
			IF (TIMERA() >= 300 AND TIMERA() < 400)

				sPlayerTexture = "VEHICLE_CAR_01_FRAME_04_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_04_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_04_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_04_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_04"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_04_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_04_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_04_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_04"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_04_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF
		
			IF (TIMERA() >= 400 AND TIMERA() < 500)
				
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF
		
			IF (TIMERA() >= 500 AND TIMERA() < 600)
					
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF

			IF (TIMERA() >= 600 AND TIMERA() < 700)
					
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF

			IF (TIMERA() >= 700)
			AND (TIMERA() < 1400)
				vSpriteScale.x = -ABSF(vSpriteScale.x)
			ENDIF

			IF (TIMERA() >= 700 AND TIMERA() < 800)

				sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01_LEFT"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_02_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02_DRIVER_FRAME_01_LEFT"
					ENDIF
				ENDIF
				
			ENDIF
			
			
			
			IF (TIMERA() >= 800 AND TIMERA() < 900)

				sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
				
								
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03_DRIVER_FRAME_01_LEFT"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_03_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03_DRIVER_FRAME_01_LEFT"
					ENDIF
				ENDIF
				
			ENDIF
		
			IF (TIMERA() >= 900 AND TIMERA() < 1050)
				
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_04_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_04_UP_HILL_DRIVER_FRAME_01_LEFT"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_04_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_04_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_04"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_04_DRIVER_FRAME_01_LEFT"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_04_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_04_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_04_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_04"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_04_DRIVER_FRAME_01_LEFT"
					ENDIF
				ENDIF
				
			ENDIF
					
			IF (TIMERA() >= 1050 AND TIMERA() < 1200)
							
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01_LEFT"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03_DRIVER_FRAME_01_LEFT"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_03_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_03_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03_DRIVER_FRAME_01_LEFT"
					ENDIF
				ENDIF
				
			ENDIF
		
			IF (TIMERA() >= 1200 AND TIMERA() < 1300)
			
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01_LEFT"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01_LEFT"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_02_UP_HILL_DRIVER_FRAME_01_LEFT"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02_DRIVER_FRAME_01_LEFT"
					ENDIF
				ENDIF
				
			ENDIF
			
			IF (TIMERA() >= 1300 AND TIMERA() < 1400)
							
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_01_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_01_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF
		
			IF (TIMERA() >= 1400)
					
				sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
				sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_01_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01"
					ENDIF
				ELSE
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						sPlayerTexture = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_CAR_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						sPlayerTexture = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
						sPlayerDriver = "VEHICLE_BIKE_02_FRAME_02_UP_HILL_DRIVER_FRAME_01"
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						sPlayerTexture = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
						sPlayerDriver = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02_DRIVER_FRAME_01"
					ENDIF
				ENDIF
				
			ENDIF
			
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, 
					sPlayerTexture,
					vSpritePos,
					vSpriteScale,
					0.0,
					rgbaOriginal)
					
			// Add driver frame
			IF iPlayerSeatLocalID = 0
			AND eRoadArcade_ActiveGameType != RA_GT_BIKE
							
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
					sPlayerDriver,
					vSpritePos,
					vDriverScale,
					0.0,
					rgbaOriginal)
			ENDIF
		
			IF (TIMERA() >= 1500)
			OR player.fPlayerSpeed <= 0	
				RETURN TRUE
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

// Anim states - Crash
ENUM PLAYER_CRASH_ANIM_STATE

	CA1_Spawn_EFFECT,
	CA3_FLIP_ANIM,
	CA4_Crash_LOOP,
	CA5_RESET_POSITION,
	CA6_RELEASE_AND_CLEAN_UP
		
ENDENUM

PLAYER_CRASH_ANIM_STATE pcasCrashState

PROC RA_TRIGGER_PLAYER_CRASH(PLAYER_VEHICLE &player)
	IF player.psActiveState != ps_Crashed
	
		IF !player.bCrashedOnce
			player.bCrashedOnce = TRUE
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "RA_TRIGGER_PLAYER_SWERVE - Setting bCrashedOnce to TRUE")
		ENDIF
		sRoadArcadeTelemetry.kills++
		
		SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_LOSE_LEAD)
		pcasCrashState = CA1_Spawn_EFFECT // Reset the animation state
		player.psActiveState = ps_Crashed
	ENDIF
ENDPROC

PROC RA_TRIGGER_PLAYER_CRASH_RESET(PLAYER_VEHICLE &player)
	IF player.psActiveState != ps_Crashed
		fJOURNEY_DistanceTravelled -= 35.0
		pcasCrashState = CA5_RESET_POSITION // Reset the animation state
		player.psActiveState = ps_Crashed
	ENDIF
ENDPROC

ENUM CRASH_ANIM_FRAMES

	// Starting from 2 as frames
	CRASH_FRAME_01,
	CRASH_FRAME_02,
	CRASH_FRAME_03,
	CRASH_FRAME_04,
	CRASH_FRAME_05,
	CRASH_FRAME_06,
	CRASH_FRAME_07
	
ENDENUM

CRASH_ANIM_FRAMES crashFrames

ENUM CRASH_ANIM_FRAMES_DOG

	// Starting from 2 as frames
	DOG_CRASH_FRAME_01,
	DOG_CRASH_FRAME_02
	
ENDENUM

CRASH_ANIM_FRAMES_DOG crashDogFrames
FUNC BOOL ROAD_ARCADE_HANDLE_PLAYER_CRASH_ANIM(PLAYER_VEHICLE &player, Bool bJustReset = FALSE)

	VECTOR_2D vSpritePos = INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY))
	VECTOR_2D vSpriteScale = INIT_VECTOR_2D(TO_FLOAT(player.iWidth), TO_FLOAT(player.iHeight)) // Base value

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		vSpritePos.y += 48
	ENDIF
		
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		vSpritePos.y -= 48
	ENDIF
		
	FLOAT fCarDistanceFromCenterOfRoad = 0
	
	VECTOR_2D vShadowPos = INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY))
	
	// Car shadow
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_CAR_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vShadowPos.x, vShadowPos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_CAR_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vShadowPos.x, vShadowPos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_BIKE_SHADOW", INIT_VECTOR_2D(vShadowPos.x + 8, (vShadowPos.y + (vSpriteScale.y/2.0))-20), INIT_VECTOR_2D(72, 52), 0.0, rgbaOriginal)	
	ENDIF
	
	// Truck
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_TRUCK_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vShadowPos.x, vShadowPos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_TRUCK_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vShadowPos.x, vShadowPos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
		ENDIF
	ENDIF
	
	STRING sCurrentCrashFrame = ""
	STRING sCurrentCrashDict = sDICT_PlayerCrash
	
	FLOAT fRoadPieceY = TO_FLOAT(player.iPlayerPosY)
	
	IF bJustReset
		IF pcasCrashState != CA5_RESET_POSITION

			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
			ENDIF
			
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)
		
			pcasCrashState = CA5_RESET_POSITION
		ENDIF
	ENDIF
	
	STRING sStartingFrame = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			sStartingFrame = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
			sCurrentCrashDict = sDICT_PlayerCrash
		ELSE
			sStartingFrame = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
			sCurrentCrashDict = sDICT_PlayerTwoCrash
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		IF iPlayerSeatLocalID = 0
			sStartingFrame = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
			sCurrentCrashDict = sDICT_PlayerCrash_Bike
		ELSE
			sStartingFrame = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
			sCurrentCrashDict = sDICT_PlayerTwoCrash_Bike
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			sStartingFrame = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
			sCurrentCrashDict = sDICT_PlayerCrash_Truck
		ELSE
			sStartingFrame = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
			sCurrentCrashDict = sDICT_PlayerTwoCrash_Truck
		ENDIF
	ENDIF
	
	// All truck crash sprites are same scale
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		vSpriteScale = INIT_VECTOR_2D(604,472)
	ENDIF
	
	SWITCH(pcasCrashState)
	
		CASE CA1_Spawn_EFFECT
				
			// Stop the player dead
			player.fPlayerSpeed = 0
			player.fTurningAngle = 0
						
			// Flashing effect
			IF player.bDrawShadow			
				ARCADE_DRAW_PIXELSPACE_RECT(
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0,cfBASE_SCREEN_HEIGHT/2.0),
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH,cfBASE_SCREEN_HEIGHT),
				rgbaVicePurple)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
			ENDIF

			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
			ENDIF
				
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_EffectsAssets, 
				"VEHICLE_ASSET_IMPACT_01_FRAME_01",
				INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY) - 100),
				INIT_VECTOR_2D(308, 260),
				0.0,
				rgbaOriginal)
							
			ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, 
				sStartingFrame,
				vSpritePos,
				INIT_VECTOR_2D(TO_FLOAT(player.iWidth), TO_FLOAT(player.iHeight)),
				0.0,
				rgbaOriginal)
				
			// Standard sound to play upon impact.
			ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_COLLISION_GEN)
			
			
			ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)
				
				
			REQUEST_STREAMED_TEXTURE_DICT(sCurrentCrashDict)
	
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(sCurrentCrashDict)
				SETTIMERA(0)
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_CRASH_START)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_OFF_ROAD)

				SET_CONTROL_SHAKE(FRONTEND_CONTROL, 500, 150)
				crashFrames = CRASH_FRAME_01
				pcasCrashState = CA3_FLIP_ANIM
			ENDIF
	
		BREAK
			
		CASE CA3_FLIP_ANIM
			
			player.fPlayerSpeed = 30.0
						
			// Run through the Crash animation
			SWITCH(crashFrames)
						
				CASE CRASH_FRAME_01
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_01_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_01_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(752,288)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_01_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_01_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(260,256)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_01_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_01_CRASH"
						ENDIF
						
						fRoadPieceY -= 10
					ENDIF
					
					fRoadPieceY -= 10
				BREAK
			
				CASE CRASH_FRAME_02
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_02_CRASH"
										
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_02_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_02_CRASH"
						ENDIF
						vSpriteScale = INIT_VECTOR_2D(752,512)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_02_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_02_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(260,360)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_02_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_02_CRASH"
						ENDIF
						
						fRoadPieceY -= 20 // Doubling for truck!
					ENDIF
					
					fRoadPieceY -= 20
				BREAK
			
				CASE CRASH_FRAME_03
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_03_CRASH"
										
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_03_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_03_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(752, 604)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_03_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_03_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(260,360)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_03_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_03_CRASH"
						ENDIF
						
						fRoadPieceY -= 40
					ENDIF
					
					fRoadPieceY -= 40
				BREAK
			
				CASE CRASH_FRAME_04
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_04_CRASH"
					
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_04_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_04_CRASH"
						ENDIF
					
						vSpriteScale = INIT_VECTOR_2D(752, 604)
					
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_04_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_04_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(260,360)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_04_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_04_CRASH"
						ENDIF
						
						fRoadPieceY -= 28
					ENDIF
					
					fRoadPieceY -= 28
				BREAK
				
				CASE CRASH_FRAME_05
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_05_CRASH"
					
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_05_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_05_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(752,512)
					
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_05_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_05_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(260,360)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_05_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_05_CRASH"
						ENDIF
						
						fRoadPieceY -= 16
					ENDIF
					
					fRoadPieceY -= 4
				BREAK
				
				CASE CRASH_FRAME_06
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_06_CRASH"
					
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_06_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_06_CRASH"
						ENDIF
					
						vSpriteScale = INIT_VECTOR_2D(752,512)
					
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_06_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_06_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(260,200)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						IF iPlayerSeatLocalID = 0
							sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_06_CRASH"
						ELSE
							sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_06_CRASH"
						ENDIF
						
						vSpriteScale = INIT_VECTOR_2D(604,472)
						fRoadPieceY += 12
			
					ENDIF
					
					fRoadPieceY += 8
				BREAK
														
			ENDSWITCH
			
			// Advance the frame every 0.5secs
			IF TIMERA() > 100
				SETTIMERA(0)
				// Iterate Frame Enum by one
				crashFrames = INT_TO_ENUM(CRASH_ANIM_FRAMES, ENUM_TO_INT(crashFrames) + 1 )
			ENDIF
			
			fRoadPieceY -= (vSpriteScale.y/2)
			
			ARCADE_DRAW_PIXELSPACE_SPRITE(sCurrentCrashDict, sCurrentCrashFrame,
				INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), fRoadPieceY),
				vSpriteScale,
				0.0,
				rgbaOriginal)
					
			IF crashFrames = CRASH_FRAME_07
				pcasCrashState = CA4_Crash_LOOP
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_CRASH_FINISH)
				
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
				ENDIF
				
				IF iPlayerSeatLocalID = 0
					IF eRoadArcade_ActiveGameType != RA_GT_BIKE
						ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_CRASH_DOG)
					ELSE
						ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_CRASH_CAT)
					ENDIF
				ENDIF
				
				SETTIMERA(0)
				SETTIMERB(0)
			ENDIF
			
		BREAK
		
		CASE CA4_Crash_LOOP
		
			player.fPlayerSpeed = 0.0
			vSpriteScale = INIT_VECTOR_2D(752,256)
			
			// Truck sprite scale override
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				vSpriteScale = INIT_VECTOR_2D(604,472)
				fRoadPieceY -= 112
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				fRoadPieceY += 64
			ENDIF
			
			sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_07_CRASH"
			
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				IF iPlayerSeatLocalID = 0
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_07_CRASH"
				ELSE
					sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_07_CRASH"
				ENDIF
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				IF iPlayerSeatLocalID = 0
					sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_07_CRASH"
				ELSE
					sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_07_CRASH"
				ENDIF
				
				vSpriteScale = INIT_VECTOR_2D(284,200)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				IF iPlayerSeatLocalID = 0
					sCurrentCrashFrame = "VEHICLE_TRUCK_01_FRAME_07_CRASH"
				ELSE
					sCurrentCrashFrame = "VEHICLE_TRUCK_02_FRAME_07_CRASH"
				ENDIF
			ENDIF
			
		
			ARCADE_DRAW_PIXELSPACE_SPRITE(sCurrentCrashDict,
				sCurrentCrashFrame,
				INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), fRoadPieceY),
				vSpriteScale,
				0.0,
				rgbaOriginal)
			
			// Only first player has driver sprites
			IF iPlayerSeatLocalID = 0
				SWITCH(crashDogFrames)
				
					CASE DOG_CRASH_FRAME_01
					
						IF eRoadArcade_ActiveGameType = RA_GT_CAR
							ARCADE_DRAW_PIXELSPACE_SPRITE(sCurrentCrashDict,
								"VEHICLE_CAR_01_FRAME_07_CRASH_DOG_FRAME_01",
								INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), fRoadPieceY), 
								INIT_VECTOR_2D(92.0,256.0),
								0.0,
								rgbaOriginal)
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
							
							ARCADE_DRAW_PIXELSPACE_SPRITE(sCurrentCrashDict,
								"VEHICLE_TRUCK_01_FRAME_07_CRASH_DOG_FRAME_01",
								INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), fRoadPieceY), 
								INIT_VECTOR_2D(288.0,472.0),
								0.0,
								rgbaOriginal)
						
						ENDIF
									
					BREAK
				
					CASE DOG_CRASH_FRAME_02
					
						IF eRoadArcade_ActiveGameType = RA_GT_CAR
						
							ARCADE_DRAW_PIXELSPACE_SPRITE(sCurrentCrashDict,
								"VEHICLE_CAR_01_FRAME_07_CRASH_DOG_FRAME_02",
								INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), fRoadPieceY), 
								INIT_VECTOR_2D(92.0,256.0),
								0.0,
								rgbaOriginal)
								
						ENDIF
							
						IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
							
							ARCADE_DRAW_PIXELSPACE_SPRITE(sCurrentCrashDict,
								"VEHICLE_TRUCK_01_FRAME_07_CRASH_DOG_FRAME_02",
								INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), fRoadPieceY), 
								INIT_VECTOR_2D(288.0,472.0),
								0.0,
								rgbaOriginal)
						
						ENDIF
										
					BREAK
				
				
				ENDSWITCH
				
				IF TIMERA() >= 250
					
					IF crashDogFrames = DOG_CRASH_FRAME_01
						crashDogFrames = DOG_CRASH_FRAME_02
					ELSE
						crashDogFrames = DOG_CRASH_FRAME_01
					ENDIF
					
					SETTIMERA(0)
				ENDIF
			ENDIF
			
			IF TIMERB() >= 1500
				pcasCrashState = CA5_RESET_POSITION
				player.fTurningAngle = 0.0 // Neutral position
				SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sCurrentCrashDict)
				SETTIMERB(0)
			ENDIF
		
		BREAK
		
		CASE CA5_RESET_POSITION
					
			player.fPlayerSpeed = 0.0
					
			fCarDistanceFromCenterOfRoad = player.fRoadCenter - (cfBASE_SCREEN_WIDTH/2.0)
			
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "CA5_RESET_POSITION - Distance from center: ", fCarDistanceFromCenterOfRoad)
			
			IF ABSF(fCarDistanceFromCenterOfRoad) < 30.0
			OR TIMERB() >= 2500
				fXCarOffset += fCarDistanceFromCenterOfRoad
				player.pgPlayerGear = pgLow
				RETURN TRUE
			ENDIF
			
			IF fCarDistanceFromCenterOfRoad < 0
				fXCarOffset += (20*(0+@100))
			ENDIF
			
			IF fCarDistanceFromCenterOfRoad > 0
				fXCarOffset -= (20*(0+@100))
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				IF iPlayerSeatLocalID = 0
					sCurrentCrashFrame = "VEHICLE_CAR_01_FRAME_01_UP_HILL"
				ELSE
					sCurrentCrashFrame = "VEHICLE_CAR_02_FRAME_01_UP_HILL"
				ENDIF
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				IF iPlayerSeatLocalID = 0
					sCurrentCrashFrame = "VEHICLE_BIKE_01_FRAME_01_UP_HILL"
				ELSE
					sCurrentCrashFrame = "VEHICLE_BIKE_02_FRAME_01_UP_HILL"
				ENDIF
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				IF iPlayerSeatLocalID = 0
					sCurrentCrashFrame = "VEHICLE_TRUCK_01_UP_HILL_FRAME_01"
				ELSE
					sCurrentCrashFrame = "VEHICLE_TRUCK_02_UP_HILL_FRAME_01"
				ENDIF
			ENDIF
			
			
			if player.bDrawShadow
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, 
				sCurrentCrashFrame,
				INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), TO_FLOAT(player.iPlayerPosY)),
				INIT_VECTOR_2D(TO_FLOAT(player.iWidth), TO_FLOAT(player.iHeight)),
				0.0, rgbaOriginal)
				
				// If in valid vehicle types draw the player
				IF iPlayerSeatLocalID = 0
				
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, 
						"VEHICLE_CAR_01_FRAME_01_UP_HILL_DRIVER_FRAME_01",
						INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), TO_FLOAT(player.iPlayerPosY-5)), 
						INIT_VECTOR_2D(236.0, TO_FLOAT(player.iHeight)), 
						0.0, rgbaOriginal)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, 
						"VEHICLE_TRUCK_01_UP_HILL_FRAME_01_DRIVER_FRAME_01",
						INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX), TO_FLOAT(player.iPlayerPosY-5)), 
						INIT_VECTOR_2D(288.0, TO_FLOAT(player.iHeight)), 
						0.0, rgbaOriginal)
					ENDIF
					
				ENDIF
			ENDIF
						
		BREAK
	
	ENDSWITCH
	
	// Ocilate shadow display
	player.bDrawShadow = !player.bDrawShadow
	
	RETURN FALSE

ENDFUNC

// --------------- FLAGGER AT START --------------

STRUCT RA_FLAGGER

	STRING sDict 
	STRING sTexture
	STRING sActiveFrame
		
	VECTOR_2D vScale
	VECTOR_2D vPos
	
	FLOAT fAnimSpeed = 500.0
	FLOAT fAnimTimer = 0.0

	FLOAT fXOffset = -500.0
	FLOAT fYOffset = 104.0
	
ENDSTRUCT

RA_FLAGGER flagStart

PROC RA_INIT_FLAGGER(STAGES_SETS startingStage)

	flagStart.sDict = sDICT_GenericAssets

	IF startingStage = STAGE_VICE_CITY
		flagStart.sTexture = "GENERIC_FLAG_GUY_01_"
	ENDIF
	
	IF startingStage = STAGE_SAN_ANDREAS
		flagStart.sTexture = "GENERIC_FLAG_GIRL_01_"
	ENDIF
	
	IF startingStage = STAGE_SAN_FIERRO
		flagStart.sTexture = "GENERIC_FLAG_GIRL_02_"
	ENDIF
	
	IF startingStage = STAGE_LAS_VENTURAS
		flagStart.sTexture = "GENERIC_FLAG_GIRL_01_"
	ENDIF
	
	IF startingStage = STAGE_LIBERTY_CITY
		flagStart.sTexture = "GENERIC_FLAG_GUY_01_"
	ENDIF
	
	flagStart.sActiveFrame = "FRAME_01"
	flagStart.fAnimTimer = 0
	flagStart.fAnimSpeed = 1000.0
	
	flagStart.fXOffset = -500.0
	flagStart.fYOffset = 104.0
	
	flagStart.vScale = INIT_VECTOR_2D(240.0, 376.0)
ENDPROC

PROC RA_FLAGGER_UPDATE(ROAD_ARCADE_Z_BUFFER &zBuffer, VECTOR_2D startPostPos, FLOAT postScale, FLOAT zValue)

	flagStart.fAnimTimer += 0.0+@(1000)
	
	flagStart.vPos = startPostPos
	flagStart.vPos.x += (flagStart.fXOffset * postScale)
	flagStart.vPos.y += (flagStart.fYOffset * postScale)
		
	// Animate the flagger according to the in game timer.
	IF fCountDownTimer > 2000.0
		IF ARE_STRINGS_EQUAL(flagStart.sActiveFrame, "FRAME_01")
			flagStart.sActiveFrame = "FRAME_02"
		ENDIF		
	ENDIF
	
	IF fCountDownTimer > 3000.0
		flagStart.fAnimSpeed = 500.0
		
		IF ARE_STRINGS_EQUAL(flagStart.sActiveFrame, "FRAME_02")
			flagStart.sActiveFrame = "FRAME_03"
			flagStart.fAnimTimer = 0.0
		ENDIF
		
	ENDIF
	
	IF flagStart.fAnimTimer >= flagStart.fAnimSpeed
	
		flagStart.fAnimTimer = 0.0
			
		// Light is green
		IF ARE_STRINGS_EQUAL(flagStart.sActiveFrame, "FRAME_03")
			flagStart.sActiveFrame = "FRAME_04"
		ELIF ARE_STRINGS_EQUAL(flagStart.sActiveFrame, "FRAME_04")
			flagStart.sActiveFrame = "FRAME_03"
		ENDIF
				
	ENDIF
	
	TEXT_LABEL_63 fFinalTexture = flagStart.sTexture
	fFinalTexture += flagStart.sActiveFrame

	VECTOR_2D vFinalScale = MULTIPLY_VECTOR_2D(flagStart.vScale, postScale)
	
	ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(zBuffer, 
		flagStart.sDict,
		fFinalTexture,
		flagStart.vPos,
		vFinalScale,
		rgbaOriginal,
		0.0,
		zValue)

ENDPROC

STRUCT SIDELINE_OBJECT

	INT objID 
	STRING sObjDict
	STRING sObjTexture
	FLOAT fObjectFrequencyLeft // Set to 0 to have it not appear on the given side
	FLOAT fObjectFrequencyRight // Set to 0 to have it not appear on the given side
	FLOAT fOffsetFromCenter
	VECTOR_2D vSpriteScale
	
	// Positions from the start of the current stage that the object appears.
	// If left at 0 this will tile the whole way!
	// Setting a limit will caus ethe objects to only appear in between the Z Values 
	FLOAT fZMin = 0.0
	FLOAT fZMax = 0.0
	
	FLOAT fYOffset = 0.0
	
	BOOL bFlipLeft = TRUE

ENDSTRUCT

CONST_INT iROAD_ARCADE_MaxSideLineObjects 8
SIDELINE_OBJECT stageSideLineObjects[iROAD_ARCADE_MaxSideLineObjects]

// VICE CITY - Side objects
ENUM SIDE_OBJECTS_VC_ID

	// Vice city objects
	V_PALM_FAR,
	V_PALM_NEAR,
	V_SHOP_02,
	V_SHOP_01,
	V_PALM_FAR_2,
	V_SHOP_01_2,
	V_PALM_NEAR_2,
	V_PALM_AVENUE
	
ENDENUM

PROC VICE_CITY_SO_INIT()

	// TREE 01 - Palm Tree 01 - Cloaser to road
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].objID = ENUM_TO_INT(V_PALM_NEAR)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].fObjectFrequencyLeft = 55
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].fObjectFrequencyRight = 80
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].fOffsetFromCenter = 1700
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].vSpriteScale = INIT_VECTOR_2D(184.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].fZMin = 1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR)].fZMax = 500

	// TREE 01 - Palm Tree 02 - Further from
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].objID = ENUM_TO_INT(V_PALM_FAR)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].fObjectFrequencyLeft = 90
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].fObjectFrequencyRight = 170
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].fOffsetFromCenter = 2700
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].vSpriteScale = INIT_VECTOR_2D(184.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].fZMin = 1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR)].fZMax = 500

	// SHOP 01 - Green Shop
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].objID = ENUM_TO_INT(V_SHOP_01)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].sObjTexture = "SHOP_01"
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].fObjectFrequencyLeft = 120
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].fObjectFrequencyRight = 110
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].fOffsetFromCenter = 3500
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].vSpriteScale = INIT_VECTOR_2D(-468.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].fZMin = 500
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].fZMax = 800
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01)].fYOffset = 0.175

	// SHOP 02 - Pink Shop
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].objID = ENUM_TO_INT(V_SHOP_02)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].sObjTexture = "SHOP_02"
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fObjectFrequencyLeft = 90
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fObjectFrequencyRight = 80
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fOffsetFromCenter = 3200
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].vSpriteScale = INIT_VECTOR_2D(-468.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fZMin = 800
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fZMax = 1450
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fYOffset = 0.175
	
	
	// TREE 01 - Palm Tree 01 - Closer to road
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].objID = ENUM_TO_INT(V_PALM_NEAR_2)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].fObjectFrequencyLeft = 70
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].fObjectFrequencyRight = 80
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].fOffsetFromCenter = 1900
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].vSpriteScale = INIT_VECTOR_2D(184.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].fZMin = 1500
	stageSideLineObjects[ENUM_TO_INT(V_PALM_NEAR_2)].fZMax = 3000

	// TREE 01 - Palm Tree 02 - Further from
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].objID = ENUM_TO_INT(V_PALM_FAR_2)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].fObjectFrequencyLeft = 50
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].fObjectFrequencyRight = 60
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].fOffsetFromCenter = 5200
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].vSpriteScale = INIT_VECTOR_2D(184.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].fZMin = 1800
	stageSideLineObjects[ENUM_TO_INT(V_PALM_FAR_2)].fZMax = 3000
	
	// Shop 01 - Second round!
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].objID = ENUM_TO_INT(V_SHOP_01_2)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].sObjTexture = "SHOP_01"
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].fObjectFrequencyLeft = 40
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].fObjectFrequencyRight = 55
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].fOffsetFromCenter = 4200
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].vSpriteScale = INIT_VECTOR_2D(-468.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].fZMin = 2500
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].fZMax = 4200
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_01_2)].fYOffset = 0.175
	
	// TREE 01 - Rendered as a long Avenue
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].objID = ENUM_TO_INT(V_PALM_AVENUE)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fObjectFrequencyLeft = 21
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fObjectFrequencyRight = 21
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fOffsetFromCenter = 1600
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].vSpriteScale = INIT_VECTOR_2D(184.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fZMin = 4350
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_VICE_CITY) - 200
	

ENDPROC

PROC VICE_CITY_SO_INIT_TITLE()

	// SHOP 02 - Pink Shop
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].objID = ENUM_TO_INT(V_SHOP_02)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].sObjTexture = "SHOP_02"
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fObjectFrequencyLeft = 90
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fObjectFrequencyRight = 80
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fOffsetFromCenter = 3200
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].vSpriteScale = INIT_VECTOR_2D(-468.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fZMax = -1
	stageSideLineObjects[ENUM_TO_INT(V_SHOP_02)].fYOffset = 0.175
	
	// TREE 01 - Rendered as a long Avenue
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].objID = ENUM_TO_INT(V_PALM_AVENUE)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fObjectFrequencyLeft = 18
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fObjectFrequencyRight = 18
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fOffsetFromCenter = 1600
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].vSpriteScale = INIT_VECTOR_2D(184.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(V_PALM_AVENUE)].fZMax = -1
	

ENDPROC

// SAN FIERRO - Side objects
ENUM SIDE_OBJECTS_FIERRO_ID

	SF_SHOP02_09,
	SF_SHOP01_09,
	SF_SHOP02_08,
	SF_SHOP01_08,
	SF_LAMPS_WHOLE
	
ENDENUM

PROC FIERRO_SO_INIT()

	VECTOR_2D shopScale = INIT_VECTOR_2D(-468.0, 712.0)
	FLOAT fShopOffset = 3500
	FLOAT fShopYOffset = 0.13

	// LAMP_WHOLE - Lamps drawn along the duration of the entire track.
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].objID = ENUM_TO_INT(SF_LAMPS_WHOLE)
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].sObjTexture = "LAMPPOST_01"
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fObjectFrequencyLeft = 25
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fObjectFrequencyRight = 25
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fOffsetFromCenter = 1500
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].vSpriteScale = INIT_VECTOR_2D(240.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fZMin = 100
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_SAN_FIERRO) - 150
	
	// SHOP 01 - Red
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].objID = ENUM_TO_INT(SF_SHOP01_08)
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].sObjTexture = "SHOP_01"
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].fObjectFrequencyLeft = 30
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].fObjectFrequencyRight = 30
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_08)].fZMax = 1200
	
	// SHOP 02 - White
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].objID = ENUM_TO_INT(SF_SHOP02_08)
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].sObjTexture = "SHOP_02"
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fObjectFrequencyLeft = 28
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fObjectFrequencyRight = 28
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fZMin = 1400
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fZMax = 2600
	
	
	// SHOP 01 - Red
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].objID = ENUM_TO_INT(SF_SHOP01_09)
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].sObjTexture = "SHOP_01"
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fObjectFrequencyLeft = 31
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fObjectFrequencyRight = 31
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fZMin = 2800
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fZMax = 4000
	
	// SHOP 02 - White
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].objID = ENUM_TO_INT(SF_SHOP02_09)
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].sObjTexture = "SHOP_02"
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].fObjectFrequencyLeft = 31
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].fObjectFrequencyRight = 31
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].fZMin = 4300
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_09)].fZMax = 5500
	

ENDPROC

PROC FIERRO_SO_INIT_TITLE()

	VECTOR_2D shopScale = INIT_VECTOR_2D(-468.0, 712.0)
	FLOAT fShopOffset = 4000
	FLOAT fShopYOffset = 0.03

	// LAMP_WHOLE - Lamps drawn along the duration of the entire track.
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].objID = ENUM_TO_INT(SF_LAMPS_WHOLE)
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].sObjTexture = "LAMPPOST_01"
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fObjectFrequencyLeft = 25
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fObjectFrequencyRight = 25
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fOffsetFromCenter = 1500
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].vSpriteScale = INIT_VECTOR_2D(240.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SF_LAMPS_WHOLE)].fZMax = -1

	// SHOP 02 - White
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].objID = ENUM_TO_INT(SF_SHOP02_08)
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].sObjTexture = "SHOP_02"
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fObjectFrequencyLeft = 40
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fObjectFrequencyRight = 40
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fZMin = 20
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP02_08)].fZMax = -1

	// SHOP 01 - Red
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].objID = ENUM_TO_INT(SF_SHOP01_09)
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].sObjTexture = "SHOP_01"
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fObjectFrequencyLeft = 40
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fObjectFrequencyRight = 40
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SF_SHOP01_09)].fZMax = -1

ENDPROC

// SAN ANDREAS - Side objects
ENUM SIDE_OBJECTS_ANDREAS_ID

	SA_TREE_NEAR,
	SA_TREE_FAR,
	SA_LAMP
	
ENDENUM

PROC ANDREAS_SO_INIT()

	// LAMP - Lamps drawn along the duration of the entire track.
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].objID = ENUM_TO_INT(SA_LAMP)
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].sObjTexture = "LAMPPOST_01"
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fObjectFrequencyLeft = 30
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fObjectFrequencyRight = 30
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fOffsetFromCenter = 1400
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].vSpriteScale = INIT_VECTOR_2D(60.0, 384.0)
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_SAN_ANDREAS) - 200
	
	// TREE 01 - Palm Tree 01 - Cloaser to road
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].objID = ENUM_TO_INT(SA_TREE_NEAR)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fObjectFrequencyLeft = 65
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fObjectFrequencyRight = 90
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fOffsetFromCenter = 3200
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].vSpriteScale = INIT_VECTOR_2D(308.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fYOffset = 0.2
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_SAN_ANDREAS) - 200

	// TREE 01 - Palm Tree 02 - Further from
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].objID = ENUM_TO_INT(SA_TREE_FAR)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fObjectFrequencyRight = 110
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fOffsetFromCenter = 4200
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].vSpriteScale = INIT_VECTOR_2D(308.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fYOffset = 0.2
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_SAN_ANDREAS) - 200

ENDPROC

PROC ANDREAS_SO_INIT_TITLE()

	// LAMP - Lamps drawn along the duration of the entire track.
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].objID = ENUM_TO_INT(SA_LAMP)
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].sObjTexture = "LAMPPOST_01"
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fObjectFrequencyLeft = 30
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fObjectFrequencyRight = 30
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fOffsetFromCenter = 1400
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].vSpriteScale = INIT_VECTOR_2D(60.0, 384.0)
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SA_LAMP)].fZMax = -1
	
	// TREE 01 - Palm Tree 01 - Cloaser to road
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].objID = ENUM_TO_INT(SA_TREE_NEAR)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fObjectFrequencyLeft = 65
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fObjectFrequencyRight = 90
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fOffsetFromCenter = 3200
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].vSpriteScale = INIT_VECTOR_2D(308.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fYOffset = 0.2
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_NEAR)].fZMax = -1

	// TREE 01 - Palm Tree 02 - Further from
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].objID = ENUM_TO_INT(SA_TREE_FAR)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].sObjTexture = "TREE_01"
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fObjectFrequencyRight = 110
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fOffsetFromCenter = 4200
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].vSpriteScale = INIT_VECTOR_2D(308.0, 308.0)
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fYOffset = 0.2
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(SA_TREE_FAR)].fZMax = -1

ENDPROC

// LAS VENTURAS - Side objects
ENUM SIDE_OBJECTS_VENTURAS_ID

	LV_CACT_FAR,
	LV_AKEDO_SIGN_1,
	LV_AKEDO_SIGN_2,
	LV_CACT_NEAR
	
ENDENUM

PROC VENTURAS_SO_INIT()

	// AKEDO SIGN
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].objID = ENUM_TO_INT(LV_AKEDO_SIGN_1)
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].sObjTexture = "BILLBOARD_01"
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fObjectFrequencyLeft = 90
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fObjectFrequencyRight = 90
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fOffsetFromCenter = 1900
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].vSpriteScale = INIT_VECTOR_2D(196.0, 144.0)
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fZMax = 1400
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].bFlipLeft = FALSE // Don't flip this one

	// AKEDO SIGN 2
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].objID = ENUM_TO_INT(LV_AKEDO_SIGN_2)
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].sObjTexture = "BILLBOARD_01"
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].fObjectFrequencyLeft = 70
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].fObjectFrequencyRight = 70
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].fOffsetFromCenter = 1900
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].vSpriteScale = INIT_VECTOR_2D(196.0, 144.0)
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].fZMin = 4000
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_LAS_VENTURAS)-170
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_2)].bFlipLeft = FALSE // Don't flip this one
	
	// Cactus - Palm Tree 01 - Cloaser to road
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].objID = ENUM_TO_INT(LV_CACT_NEAR)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].sObjTexture = "CACTUS_01"
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fObjectFrequencyLeft = 55
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fObjectFrequencyRight = 95
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fOffsetFromCenter = 1700
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].vSpriteScale = INIT_VECTOR_2D(100.0, 200.0)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fZMin = 150
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_LAS_VENTURAS)-170

	// Cactus - Cact 02 - Further from
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].objID = ENUM_TO_INT(LV_CACT_FAR)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].sObjTexture = "CACTUS_01"
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fObjectFrequencyLeft = 60
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fObjectFrequencyRight = 90
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fOffsetFromCenter = 2700
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].vSpriteScale = INIT_VECTOR_2D(100.0, 200.0)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fZMin = 150
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_LAS_VENTURAS)-170

ENDPROC

PROC VENTURAS_SO_INIT_TITLE()

	// AKEDO SIGN
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].objID = ENUM_TO_INT(LV_AKEDO_SIGN_1)
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].sObjTexture = "BILLBOARD_01"
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fObjectFrequencyLeft = 130
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fObjectFrequencyRight = 130
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fOffsetFromCenter = 1900
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].vSpriteScale = INIT_VECTOR_2D(196.0, 144.0)
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].fZMax = -1
	stageSideLineObjects[ENUM_TO_INT(LV_AKEDO_SIGN_1)].bFlipLeft = FALSE // Don't flip this one
	
	// Cactus - Palm Tree 01 - Cloaser to road
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].objID = ENUM_TO_INT(LV_CACT_NEAR)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].sObjTexture = "CACTUS_01"
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fObjectFrequencyLeft = 55
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fObjectFrequencyRight = 95
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fOffsetFromCenter = 1700
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].vSpriteScale = INIT_VECTOR_2D(100.0, 200.0)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fZMin = 150
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_NEAR)].fZMax = -1

	// Cactus - Cact 02 - Further from
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].objID = ENUM_TO_INT(LV_CACT_FAR)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].sObjTexture = "CACTUS_01"
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fObjectFrequencyLeft = 60
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fObjectFrequencyRight = 90
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fOffsetFromCenter = 2700
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].vSpriteScale = INIT_VECTOR_2D(100.0, 200.0)
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fYOffset = 0.1
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fZMin = 150
	stageSideLineObjects[ENUM_TO_INT(LV_CACT_FAR)].fZMax = -1

ENDPROC

// LIBERTY CITY - Side objects
ENUM SIDE_OBJECTS_LIBERTY_ID

	LB_BODEGA01_07,
	LB_BODEGA02_07,
	LB_BODEGA01_08,
	LB_BODEGA02_08,
	LB_BODEGA01_09,
	LB_BODEGA02_09,
	LB_LAMPS_WHOLE
	
ENDENUM

PROC LIBERTY_SO_INIT()

	VECTOR_2D shopScale = INIT_VECTOR_2D(-468.0, 352.0)
	FLOAT fShopOffset = 3500
	FLOAT fShopYOffset = 0.05

	// LAMP_WHOLE - Lamps drawn along the duration of the entire track.
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].objID = ENUM_TO_INT(LB_LAMPS_WHOLE)
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].sObjTexture = "LAMPPOST_01"
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fObjectFrequencyLeft = 25
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fObjectFrequencyRight = 25
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fOffsetFromCenter = 1500
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].vSpriteScale = INIT_VECTOR_2D(240.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fZMin = 100
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_LIBERTY_CITY)-170
	
	// SHOP 01 - Red
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].objID = ENUM_TO_INT(LB_BODEGA01_07)
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].sObjTexture = "BODEGA_01"
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].fObjectFrequencyRight = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].fZMin = 200
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_07)].fZMax = 1700
	
	// SHOP 02 - White
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].objID = ENUM_TO_INT(LB_BODEGA02_07)
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].sObjTexture = "GARAGE_01"
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].fObjectFrequencyRight = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].fZMin = 1915
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_07)].fZMax = 2900
	
	// SHOP 01 - Red
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].objID = ENUM_TO_INT(LB_BODEGA01_08)
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].sObjTexture = "BODEGA_01"
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fObjectFrequencyRight = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fZMin = 3115
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fZMax = 3900
	
	// SHOP 02 - White
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].objID = ENUM_TO_INT(LB_BODEGA02_08)
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].sObjTexture = "GARAGE_01"
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].fObjectFrequencyRight = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].fZMin = 4015
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA02_08)].fZMax = ROAD_ARCADE_GET_END_OF_STAGE(STAGE_LIBERTY_CITY) - 150

ENDPROC

PROC LIBERTY_SO_INIT_TITLE()

	VECTOR_2D shopScale = INIT_VECTOR_2D(-468.0, 352.0)
	FLOAT fShopOffset = 3500
	FLOAT fShopYOffset = 0.05

	// LAMP_WHOLE - Lamps drawn along the duration of the entire track.
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].objID = ENUM_TO_INT(LB_LAMPS_WHOLE)
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].sObjTexture = "LAMPPOST_01"
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fObjectFrequencyLeft = 25
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fObjectFrequencyRight = 25
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fOffsetFromCenter = 1500
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].vSpriteScale = INIT_VECTOR_2D(240.0, 324.0)
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fZMin = 100
	stageSideLineObjects[ENUM_TO_INT(LB_LAMPS_WHOLE)].fZMax = -1
	
	// SHOP 02 - White
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].objID = ENUM_TO_INT(LB_BODEGA01_08)
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].sObjTexture = "BODEGA_01"
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fObjectFrequencyLeft = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fObjectFrequencyRight = 45
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fOffsetFromCenter = fShopOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].vSpriteScale = shopScale
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fYOffset = fShopYOffset
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fZMin = 0
	stageSideLineObjects[ENUM_TO_INT(LB_BODEGA01_08)].fZMax = -1
	
ENDPROC

//------------------------------------------------------------------------

// ------------ Scaling Sideline objects --------------
FUNC INT ROAD_ARCADE_GET_SIDELINE_OBJ_COUNT(STAGES_SETS stage)


	IF stage = STAGE_VICE_CITY
		RETURN COUNT_OF(SIDE_OBJECTS_VC_ID)-1
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		RETURN COUNT_OF(SIDE_OBJECTS_FIERRO_ID)-1
	ENDIF
	
	IF stage = STAGE_SAN_ANDREAS
		RETURN COUNT_OF(SIDE_OBJECTS_ANDREAS_ID)-1
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		RETURN COUNT_OF(SIDE_OBJECTS_VENTURAS_ID)-1
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		RETURN COUNT_OF(SIDE_OBJECTS_LIBERTY_ID)-1
	ENDIF
	
	RETURN 0 // Adjusted by one for arrays

ENDFUNC

PROC ROAD_ARCADE_INIT_STAGE_SIDE_OBJ(STAGES_SETS stage)

	SIDELINE_OBJECT empty[iROAD_ARCADE_MaxSideLineObjects]
	int i
	FOR i = 0 TO iROAD_ARCADE_MaxSideLineObjects-1
		stageSideLineObjects[i] = empty[i]
	ENDFOR

	IF stage = STAGE_VICE_CITY
		VICE_CITY_SO_INIT()
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		FIERRO_SO_INIT()
	ENDIF

	IF stage = STAGE_SAN_ANDREAS
		ANDREAS_SO_INIT()
	ENDIF

	IF stage = STAGE_LAS_VENTURAS
		VENTURAS_SO_INIT()
	ENDIF

	IF stage = STAGE_LIBERTY_CITY
		LIBERTY_SO_INIT()
	ENDIF


ENDPROC

PROC ROAD_ARCADE_INIT_STAGE_SIDE_OBJ_TITLE(STAGES_SETS stage)

	SIDELINE_OBJECT empty[iROAD_ARCADE_MaxSideLineObjects]
	int i
	FOR i = 0 TO iROAD_ARCADE_MaxSideLineObjects-1
		stageSideLineObjects[i] = empty[i]
	ENDFOR

	IF stage = STAGE_VICE_CITY
		VICE_CITY_SO_INIT_TITLE()
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		FIERRO_SO_INIT_TITLE()
	ENDIF

	IF stage = STAGE_SAN_ANDREAS
		ANDREAS_SO_INIT_TITLE()
	ENDIF

	IF stage = STAGE_LAS_VENTURAS
		VENTURAS_SO_INIT_TITLE()
	ENDIF

	IF stage = STAGE_LIBERTY_CITY
		LIBERTY_SO_INIT_TITLE()
	ENDIF


ENDPROC

PROC ROAD_ARCADE_DRAW_SIDELINE_OBJECT_SINGLE(PLAYER_VEHICLE &player, SIDELINE_OBJECT sideObject, SIDELINE_SIDES sideToDraw, STAGES_SETS currentStage, BOOL isTitleVersion = FALSE)

	// Don't process if the Z value is out of range
	IF sideObject.fZMin > fFinalZValue
		EXIT
	ENDIF
	
	IF !isTitleVersion
		IF transitionData.transition_ActiveState != T_DRAW_FINISH
			IF transitionData.transition_ActiveState != T_INACTIVE
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	// Init the counters to the updated starting Z value
	FLOAT fROAD_SideThreshold = fJOURNEY_DistanceTravelled
	FLOAT fROAD_SideNextZPosition = fJOURNEY_DistanceTravelled
	
	INT iSideLineHorizon = round(ciROAD_HORIZON_POINT * 1.0)
	INT iROAD_SideInstanceCounter = 0
	
	FLOAT fLengthBetween = 0
		
	IF sideToDraw = SIDE_LEFT
		fLengthBetween = sideObject.fObjectFrequencyLeft
	ENDIF
	
	IF sideToDraw = SIDE_RIGHT
		fLengthBetween = sideObject.fObjectFrequencyRight
	ENDIF
	
	// Stop the Draw function if the Frequency is 0
	IF fLengthBetween <= 0	
		EXIT
	ENDIF
	
	ROAD_ARCADE_Z_BUFFER roadArcade_ZSideBuffer
	ROAD_ARCADE_Z_BUFFER_INIT(roadArcade_ZSideBuffer)
	
	
	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
	
		FLOAT fROAD_SideZValue =  fROAD_SideNextZPosition
		
		FLOAT fROAD_SideDistanceScale = ((fROAD_SideZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
		FLOAT fROAD_SideThresholdRemainder = fLengthBetween-(fROAD_SideZValue%fLengthBetween)
		FLOAT fROAD_SideNextThreshold = fROAD_SideZValue + fROAD_SideThresholdRemainder
					
		VECTOR_2D vSpritePos = INIT_VECTOR_2D(((cfBASE_SCREEN_WIDTH/2.0)),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_SideInstanceCounter)									
		
		iROAD_SideInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
							
		// Stop the loop if we hit the limits of the screen
		IF cfBASE_SCREEN_HEIGHT - iROAD_SideInstanceCounter < iSideLineHorizon							
			BREAKLOOP
		ENDIF
													
		// Does the sprites position into the screen exceed the pattern transition threshold?
		IF fROAD_SideZValue > fROAD_SideThreshold
		AND fROAD_SideZValue > transitionData.fZTransitionCutOff // Don't draw anything behind the transition line
		
			FLOAT fSideLine1 = 0.0
			VECTOR_2D vObjectScale = sideObject.vSpriteScale		
				
			fSideLine1 = vSpritePos.x
			
			IF !isTitleVersion
				fSideLine1 += (fXCarOffset*(1.0-(iROAD_SideInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))))

				//fSideLine1 += (SCALING_ROAD_CURVATURE(currentStage, fROAD_SideZValue)*(2.0-fROAD_SideDistanceScale))
				fSideLine1 += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_SideZValue, iROAD_SideInstanceCounter, fROAD_SideDistanceScale)
			ENDIF

			FLOAT fScaleOffset = (POW(1.0-cfROAD_HillScale, 1.5))-(fROAD_SideDistanceScale*(cfROAD_HillScale*fROAD_RoadWidthMultiplier))
				
			FLOAT yRelativeScaling = ((vSpritePos.Y - ciROAD_HORIZON_POINT)/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))
			
			FLOAT fHillScaleForOffset = 1.0-(cfROAD_HillScale*yRelativeScaling)
						
			IF sideToDraw = SIDE_RIGHT
				fSideLine1 += ((sideObject.fOffsetFromCenter*((vSpritePos.Y - iSideLineHorizon)/(cfBASE_SCREEN_HEIGHT-iSideLineHorizon)))*(fHillScaleForOffset))*fScaleOffset
			ENDIF
			
			IF sideToDraw = SIDE_LEFT	
				fSideLine1 -= ((sideObject.fOffsetFromCenter*((vSpritePos.Y - iSideLineHorizon)/(cfBASE_SCREEN_HEIGHT-iSideLineHorizon)))*(fHillScaleForOffset))*fScaleOffset
				// Flip the animation
				IF sideObject.bFlipLeft
					vObjectScale.x = -(vObjectScale.x)
				ENDIF
			ENDIF
			
			// Object drawing positions
			VECTOR_2D vObjectPos = INIT_VECTOR_2D(fSideLine1, vSpritePos.y)
			
			// Adjust the next expected threshold.			
			fROAD_SideThreshold = fROAD_SideNextThreshold
							
			// Reloop if we are outside an objects range.
			IF sideObject.fZMin != 0	
				IF fROAD_SideZValue < sideObject.fZMin
					fROAD_SideNextZPosition += (iROAD_SideInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_SideScaleMod+(fROAD_SideDistanceScale*2))
					RELOOP
				ENDIF			
			ENDIF
						
			IF sideObject.fZMax != -1	
				IF fROAD_SideZValue > sideObject.fZMax
					fROAD_SideNextZPosition += (iROAD_SideInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_SideScaleMod+(fROAD_SideDistanceScale*2))
					RELOOP
				ENDIF			
			ENDIF
			
			// Don't draw any side objects past the Final crowd.
			IF genericObjects[ENUM_TO_INT(G_CROWD)].fZDrawAt != -1
				IF fROAD_SideZValue > genericObjects[ENUM_TO_INT(G_CROWD)].fZDrawAt
					fROAD_SideNextZPosition += (iROAD_SideInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_SideScaleMod+(fROAD_SideDistanceScale*2))
					RELOOP
				ENDIF			
			ENDIF
												
			// Filter by stage
			STRING sStageDict = ""
			TEXT_LABEL_63 sSpriteTexture = ""
						
			IF currentStage = STAGE_VICE_CITY
				sStageDict = sDICT_TrackVice
				sSpriteTexture = "TRACK_01_SIDE"
			ENDIF

			IF currentStage = STAGE_SAN_FIERRO
				sStageDict = sDICT_TrackFeirro
				sSpriteTexture = "TRACK_03_SIDE"
			ENDIF

			IF currentStage = STAGE_SAN_ANDREAS
				sStageDict = sDICT_TrackAndreas
				sSpriteTexture = "TRACK_02_SIDE"
			ENDIF
			
			IF currentStage = STAGE_LAS_VENTURAS
				sStageDict = sDICT_TrackVenturas
				sSpriteTexture = "TRACK_05_SIDE"
			ENDIF
			
			IF currentStage = STAGE_LIBERTY_CITY
				sStageDict = sDICT_TrackLiberty
				sSpriteTexture = "TRACK_04_SIDE"
			ENDIF
			
			// Non stage sideline objects. For example the finish line.
			IF NOT IS_STRING_NULL_OR_EMPTY(sideObject.sObjDict)
				
				IF ARE_STRINGS_EQUAL(sideObject.sObjDict, sDICT_FinishGroups)
					sStageDict = sDICT_FinishGroups
					sSpriteTexture = "GENERIC"
				ENDIF
				
			ENDIF
			
			vObjectScale = ROAD_ARCADE_GET_FINAL_SCALE(fROAD_SideDistanceScale, vObjectScale)
			
			// Origin offset
			vObjectPos.y  -= (vObjectScale.y/2.0)*(1.0-sideObject.fYOffset)
			
			// Complete the object string
			sSpriteTexture += "_FRAME_01_"
			sSpriteTexture += sideObject.sObjTexture

			// Convert text label to String
														
//			VECTOR_2D vObjectPos_SCREEN = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vObjectPos)
//			VECTOR_2D vObjectScale_SCREEN = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vObjectScale)						

			IF NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(sSpriteTexture))
			AND i > 2 // 2nd count will always produce a sprite regardless of progress
									 
				ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(roadArcade_ZSideBuffer, 
					sStageDict,
					sSpriteTexture,
					vObjectPos,
					vObjectScale,
					rgbaOriginal,
					0.0,
					fROAD_SideZValue)
			
				IF iROAD_SideInstanceCounter <= 150
					
					// The lamposts have wide textures so the collision doesn't make sense.
					// Creating special case.
					IF NOT ARE_STRINGS_EQUAL(TEXT_LABEL_TO_STRING(sSpriteTexture), "TRACK_03_SIDE_FRAME_01_LAMPPOST_01")
					AND NOT ARE_STRINGS_EQUAL(TEXT_LABEL_TO_STRING(sSpriteTexture), "TRACK_04_SIDE_FRAME_01_LAMPPOST_01")
						IF vObjectPos.x <= ((cfBASE_SCREEN_WIDTH/2.0) + ABSF(vObjectScale.x/2.0)) // Player would have knocked the objects.
						AND vObjectPos.x >= ((cfBASE_SCREEN_WIDTH/2.0) - ABSF(vObjectScale.x/2.0))
							// Trigger car swerve / crash
							RA_TRIGGER_PLAYER_CRASH(player)

						ENDIF
					ELSE
					
						IF sideToDraw = SIDE_LEFT

							IF vObjectPos.x - ABSF(vObjectScale.x/2.0) >= (cfBASE_SCREEN_WIDTH/2.0)-120 // Player would have knocked the objects.
								RA_TRIGGER_PLAYER_CRASH(player)
							ENDIF
						ENDIF
					
						IF sideToDraw = SIDE_RIGHT
						
							IF vObjectPos.x + ABSF(vObjectScale.x/2.0) <= (cfBASE_SCREEN_WIDTH/2.0)+120 // Player would have knocked the objects.
								RA_TRIGGER_PLAYER_CRASH(player)
							ENDIF
						ENDIF
					
					ENDIF
					
				ENDIF

			ENDIF
					
		ENDIF
		
		fROAD_SideNextZPosition += (iROAD_SideInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_SideScaleMod+(fROAD_SideDistanceScale*2))	
	ENDFOR
	
	ROAD_ARCADE_Z_BUFFER_RENDER(roadArcade_ZSideBuffer)

ENDPROC


PROC ROAD_ARCADE_DRAW_SIDELINE_OBJECTS(PLAYER_VEHICLE &player, SIDELINE_SIDES sideToDraw, STAGES_SETS currentStage, INT iObjectCount, BOOL bIsTitleVersion = FALSE)

	SIDELINE_OBJECT sideObject = stageSideLineObjects[iObjectCount]
	ROAD_ARCADE_DRAW_SIDELINE_OBJECT_SINGLE(player, sideObject, sideToDraw, currentStage, bIsTitleVersion)
		
ENDPROC


// ----------- CROSSING OBJECTS ------
// Struct detailing info on an overroad object.
STRUCT CROSSING_ROAD_OBJECT

	INT objID = -1
	STRING sObjTexture
		
	SIDELINE_SIDES sideStart
	
	// Position along route to draw the object
	FLOAT fZDrawAt
	FLOAT fZWasDrawnAt = 0.0 // Write to value 
	FLOAT fYOffset = 0.0
	
	FLOAT fXCrossingProgress = 0.0
	FLOAT fCrossingSpeed = 25.0
	
	FLOAT fAnimTimer = 0.0	
	BOOL bIsEasterEgg = FALSE
	
	BOOL bBeenHit = FALSE
	
	BOOL bFlipAnim = FALSE // Reverse the orientation of the sprite.

ENDSTRUCT

// Vice City Stage Crossers
ENUM CROSSING_OBJECTS_ID_VICE

	CROSS_VC_SURF_2,
	CROSS_VC_BIKE,
	CROSS_VC_SURF,
	CROSS_VC_BIGFOOT
	
ENDENUM

CONST_INT iROAD_ARCADE_MaxCrossers 10
CROSSING_ROAD_OBJECT crossingObject[iROAD_ARCADE_MaxCrossers]

PROC CROSS_INIT_VICE()

	crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].objID = ENUM_TO_INT(CROSS_VC_SURF)
	crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].sObjTexture = "CROSSING_SURFER_01"

	crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].fYOffset = 0.1
	
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].fZDrawAt = 380	
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].fZDrawAt = 1100
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].fZDrawAt = 3100
	ENDIF

	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].objID = ENUM_TO_INT(CROSS_VC_BIKE)
	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].sObjTexture = "CROSSING_CYCLIST_01"

	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fXCrossingProgress = 420
	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fYOffset = 0.1
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fZDrawAt = 1080.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fZDrawAt = 1920.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fZDrawAt = 1390.0
	ENDIF
				
	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].sideStart = SIDE_RIGHT
	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fCrossingSpeed = 35.0
	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].fXCrossingProgress = -150.0
	crossingObject[ENUM_TO_INT(CROSS_VC_BIKE)].bBeenHit = FALSE
	
	crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].sideStart = SIDE_LEFT

	crossingObject[ENUM_TO_INT(CROSS_VC_SURF)].bBeenHit = FALSE
	
	crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].objID = ENUM_TO_INT(CROSS_VC_BIGFOOT)
	crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].sObjTexture = "CROSSING_BIGFOOT_01"

	crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fYOffset = 0.1
		
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fZDrawAt = 5605
		crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fXCrossingProgress = 250
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fXCrossingProgress = 275
		crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fZDrawAt = 6420
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fXCrossingProgress = 225
		crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].fZDrawAt = 4600
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].sideStart = SIDE_RIGHT
	crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].bBeenHit = FALSE
	crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].bIsEasterEgg = TRUE
	
	// Unique to truck stage
	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].objID = ENUM_TO_INT(CROSS_VC_SURF_2)
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].sObjTexture = "CROSSING_SURFER_01"

		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].fXCrossingProgress = 125
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].fZDrawAt = 4865
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].bFlipAnim = TRUE
		crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].sideStart = SIDE_RIGHT
		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			crossingObject[ENUM_TO_INT(CROSS_VC_SURF_2)].fZDrawAt = 5195
		ENDIF
	ENDIF

	
ENDPROC

// San Feirro Stage Crossers
ENUM CROSSING_OBJECTS_ID_FIERRO

	CROSS_SF_HIPPIE,
	CROSS_SF_BOARDER,
	CROSS_SF_BOARDER_02,
	CROSS_SF_HIPPIE_02,
	CROSS_SF_WEREWOLF
	
ENDENUM

PROC CROSS_INIT_FIERRO()

	crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].objID = ENUM_TO_INT(CROSS_SF_HIPPIE)
	crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].sObjTexture = "CROSSING_HIPPIE_01"

	crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].fXCrossingProgress = -150
	crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].fYOffset = 0.1
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].fZDrawAt = 440.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].fZDrawAt = 2475.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].fZDrawAt = 4495.5
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].sideStart = SIDE_LEFT

	crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE)].bBeenHit = FALSE

	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].objID = ENUM_TO_INT(CROSS_SF_BOARDER)
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].sObjTexture = "CROSSING_SKATEBOARD_01"

	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].fXCrossingProgress = 150
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].fYOffset = 0.1
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].fZDrawAt = 2275.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].fZDrawAt = 595.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].fZDrawAt = 665.0
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].sideStart = SIDE_RIGHT

	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].fCrossingSpeed = 30.0
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER)].bBeenHit = FALSE
	
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].objID = ENUM_TO_INT(CROSS_SF_BOARDER_02)
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].sObjTexture = "CROSSING_SKATEBOARD_01"

	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].fXCrossingProgress = 150
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].sideStart = SIDE_LEFT

	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].fCrossingSpeed = 30.0
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].bFlipAnim = TRUE
	crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].bBeenHit = FALSE

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].fZDrawAt = 2700.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].fZDrawAt = 1565.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SF_BOARDER_02)].fZDrawAt = 730.0
	ENDIF
	
	// 2nd hippie
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
	OR eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE_02)].objID = ENUM_TO_INT(CROSS_SF_HIPPIE_02)
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE_02)].sObjTexture = "CROSSING_HIPPIE_01"

		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE_02)].fXCrossingProgress = -150
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE_02)].fZDrawAt = 2700.0
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			crossingObject[ENUM_TO_INT(CROSS_SF_HIPPIE_02)].fZDrawAt = 6900.0
		ENDIF
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].objID = ENUM_TO_INT(CROSS_SF_WEREWOLF)
	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].sObjTexture = "CROSSING_WEREWOLF_01"

	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].fXCrossingProgress = 250
	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].fYOffset = 0.1
		
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].fZDrawAt = 4275.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].fZDrawAt = 5600.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].fZDrawAt = 4385.0
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].sideStart = SIDE_RIGHT

	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].fCrossingSpeed = 40.0
	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].bBeenHit = FALSE
	crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].bIsEasterEgg = TRUE
	
ENDPROC

// SAN ANDREAS Stage Crossers
ENUM CROSSING_OBJECTS_ID_ANDREAS

	CROSS_SA_SHOPPER_01,
	CROSS_SA_SHOPPER_02,
	CROSS_SA_SHOPPER_03,
	CROSS_SA_STREAKER,
	CROSS_SA_SHOPPER_04,
	CROSS_SA_GHOST
	
ENDENUM

PROC CROSS_INIT_ANDREAS()

	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].objID = ENUM_TO_INT(CROSS_SA_SHOPPER_01)
	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].sObjTexture = "CROSSING_SHOPPER_01"

	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fYOffset = 0.1
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fZDrawAt = 200.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fXCrossingProgress = 450
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].bFlipAnim = TRUE
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fZDrawAt = 605.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fXCrossingProgress = 100
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].sideStart = SIDE_RIGHT
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].bFlipAnim = FALSE
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fZDrawAt = 240.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].fXCrossingProgress = -50
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].bFlipAnim = TRUE
	ENDIF
	

	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_01)].bBeenHit = FALSE

	// Get Truckin only
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].objID = ENUM_TO_INT(CROSS_SA_SHOPPER_02)
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].sObjTexture = "CROSSING_SHOPPER_01"

		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].fXCrossingProgress = 250
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].fZDrawAt = 1650
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].fCrossingSpeed = 30.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_02)].bBeenHit = FALSE
		
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].objID = ENUM_TO_INT(CROSS_SA_SHOPPER_03)
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].sObjTexture = "CROSSING_SHOPPER_01"

		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].fXCrossingProgress = -300
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].fZDrawAt = 5425
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].fCrossingSpeed = 30.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_03)].bFlipAnim = TRUE
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].objID = ENUM_TO_INT(CROSS_SA_STREAKER)
	crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].sObjTexture = "CROSSING_STREAKER_01"

	crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fYOffset = 0.1
		
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fZDrawAt = 2970.0
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fXCrossingProgress = 150
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].bFlipAnim = FALSE
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fZDrawAt = 4255.0
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fXCrossingProgress = -400
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fCrossingSpeed = -15
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].sideStart = SIDE_RIGHT
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].bFlipAnim = TRUE
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fZDrawAt = 4495.0
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fXCrossingProgress = 200
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].bFlipAnim = FALSE
	ENDIF
	

	crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].fCrossingSpeed = 30.0
	crossingObject[ENUM_TO_INT(CROSS_SA_STREAKER)].bBeenHit = FALSE
	
	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].objID = ENUM_TO_INT(CROSS_SA_SHOPPER_04)
	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].sObjTexture = "CROSSING_SHOPPER_01"

	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fYOffset = 0.1
		
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fZDrawAt = 3200.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fXCrossingProgress = -200
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].bFlipAnim = FALSE
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].sideStart = SIDE_LEFT
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fZDrawAt = 1035.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fXCrossingProgress = -125
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].sideStart = SIDE_RIGHT
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].bFlipAnim = TRUE
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fZDrawAt = 3030.0
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fXCrossingProgress = -75
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].bFlipAnim = FALSE
	ENDIF
	

	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].fCrossingSpeed = 30.0
	crossingObject[ENUM_TO_INT(CROSS_SA_SHOPPER_04)].bBeenHit = FALSE
	
	crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].objID = ENUM_TO_INT(CROSS_SA_GHOST)
	crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].sObjTexture = "CROSSING_GHOST_01"

	crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fYOffset = 0.1
		
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fZDrawAt = 4770
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fXCrossingProgress = 600
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].bFlipAnim = FALSE
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].sideStart = SIDE_RIGHT
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fZDrawAt = 4800.0
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fXCrossingProgress = 500
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].bFlipAnim = FALSE
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].sideStart = SIDE_RIGHT
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fZDrawAt = 4475.0
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fXCrossingProgress = -400
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].bFlipAnim = TRUE
	ENDIF
	

	crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].fCrossingSpeed = 125.0
	crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].bBeenHit = FALSE
	crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].bIsEasterEgg = TRUE
	
ENDPROC

ENUM CROSSING_OBJECTS_ID_VENTURAS

	CROSS_LV_CRAZY_01,
	CROSS_LV_CRAZY_02,
	CROSS_LV_CRAZY_03,
	CROSS_LV_CENT_01,
	CROSS_LV_CRAZY_04,
	CROSS_LV_CRAZY_05,
	CROSS_LV_CRAZY_06,
	CROSS_LV_CENT_02,
	CROSS_LV_ALIEN
	
ENDENUM

PROC CROSS_INIT_VENTURAS()

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].objID = ENUM_TO_INT(CROSS_LV_CRAZY_01)
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].sObjTexture = "CROSSING_CRAZY_01"

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].fXCrossingProgress = 0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].fZDrawAt = 400.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].sideStart = SIDE_LEFT

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].fCrossingSpeed = 0.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_01)].bBeenHit = FALSE

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].objID = ENUM_TO_INT(CROSS_LV_CRAZY_02)
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].sObjTexture = "CROSSING_CRAZY_01"

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].fXCrossingProgress = 77
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].fZDrawAt = 410.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].sideStart = SIDE_LEFT

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].fCrossingSpeed = 0.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_02)].bBeenHit = FALSE
	
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].objID = ENUM_TO_INT(CROSS_LV_CRAZY_03)
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].sObjTexture = "CROSSING_CRAZY_01"

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].fXCrossingProgress = -80
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].fZDrawAt = 420.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].sideStart = SIDE_LEFT

	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].fCrossingSpeed = 0.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_03)].bBeenHit = FALSE

	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].objID = ENUM_TO_INT(CROSS_LV_CENT_01)
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].sObjTexture = "CROSSING_CENTURIAN_01"

		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].fXCrossingProgress = -220
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].fZDrawAt = 890.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].fCrossingSpeed = 23.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_01)].bBeenHit = FALSE

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].objID = ENUM_TO_INT(CROSS_LV_CRAZY_04)
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].sObjTexture = "CROSSING_CRAZY_01"

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].fXCrossingProgress = 0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].fZDrawAt = 1545.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].fCrossingSpeed = 0.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_04)].bBeenHit = FALSE

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].objID = ENUM_TO_INT(CROSS_LV_CRAZY_05)
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].sObjTexture = "CROSSING_CRAZY_01"

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].fXCrossingProgress = -54
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].fZDrawAt = 1560.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].fCrossingSpeed = 0.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_05)].bBeenHit = FALSE
		
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].objID = ENUM_TO_INT(CROSS_LV_CRAZY_06)
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].sObjTexture = "CROSSING_CRAZY_01"

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].fXCrossingProgress = 70
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].fZDrawAt = 1575.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].fCrossingSpeed = 0.0
		crossingObject[ENUM_TO_INT(CROSS_LV_CRAZY_06)].bBeenHit = FALSE
	ENDIF
		
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].objID = ENUM_TO_INT(CROSS_LV_CENT_02)
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].sObjTexture = "CROSSING_CENTURIAN_01"

	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].fXCrossingProgress = -260
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].fZDrawAt = 3700.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].sideStart = SIDE_RIGHT

	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].fCrossingSpeed = 21.0
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].bBeenHit = FALSE
	crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].bFlipAnim = TRUE

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_LV_CENT_02)].fZDrawAt = 4180.0
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].objID = ENUM_TO_INT(CROSS_LV_ALIEN)
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].sObjTexture = "CROSSING_ALIEN_01"

	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].fXCrossingProgress = 275
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].fZDrawAt = 5035
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].fZDrawAt = 5235
	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].sideStart = SIDE_RIGHT

	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].fCrossingSpeed = 40.0
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].bBeenHit = FALSE
	crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].bIsEasterEgg = TRUE
	
ENDPROC


ENUM CROSSING_OBJECTS_ID_LIBERTY

	CROSS_LB_RAT_02,
	CROSS_LB_RAT_04,
	CROSS_LB_RAT_05,
	CROSS_LB_RAT_07,
	CROSS_LB_RAT_08,
	CROSS_LB_RAT_10,
	CROSS_LB_DUMPSTER_01,
	CROSS_LB_DUMPSTER_02,
	CROSS_LB_DUMPSTER_03,
	CROSS_LB_ZOMBIE
	
ENDENUM

PROC CROSS_INIT_LIBERTY()

	// Car version
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].objID = ENUM_TO_INT(CROSS_LB_RAT_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fXCrossingProgress = -320
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fZDrawAt = 340.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fCrossingSpeed = 33.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].bFlipAnim = TRUE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].objID = ENUM_TO_INT(CROSS_LB_RAT_05)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fXCrossingProgress = -260
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fZDrawAt = 1090.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fCrossingSpeed = 36.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].bFlipAnim = TRUE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_01)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fXCrossingProgress = 870

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fZDrawAt = 1680.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].sideStart = SIDE_RIGHT
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fCrossingSpeed = 0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].bBeenHit = FALSE

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fXCrossingProgress = -870

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fZDrawAt = 3310.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fCrossingSpeed = 20
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].bBeenHit = FALSE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].objID = ENUM_TO_INT(CROSS_LB_RAT_10)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fXCrossingProgress = 280
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fZDrawAt = 5175.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fCrossingSpeed = 34.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].bBeenHit = FALSE
		
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].objID = ENUM_TO_INT(CROSS_LB_RAT_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fXCrossingProgress = -320
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fZDrawAt = 1430.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fCrossingSpeed = 33.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].bFlipAnim = TRUE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].objID = ENUM_TO_INT(CROSS_LB_RAT_05)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fXCrossingProgress = -260
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fZDrawAt = 1720.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fCrossingSpeed = 36.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].bFlipAnim = TRUE
		
		// Pass between the dumpsters
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_01)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fXCrossingProgress = 920

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fZDrawAt = 4032.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fCrossingSpeed = 0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].bBeenHit = FALSE

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fXCrossingProgress = -920

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fZDrawAt = 3625.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fCrossingSpeed = 0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].bBeenHit = FALSE
				
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].objID = ENUM_TO_INT(CROSS_LB_RAT_10)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fXCrossingProgress = 280
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fZDrawAt = 5375.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fCrossingSpeed = 34.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].bBeenHit = FALSE
		
	ENDIF


	// Truck exclusive layout
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].objID = ENUM_TO_INT(CROSS_LB_RAT_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fXCrossingProgress = -320
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fZDrawAt = 1480.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].fCrossingSpeed = 33.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_02)].bFlipAnim = TRUE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].objID = ENUM_TO_INT(CROSS_LB_RAT_04)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].fXCrossingProgress = 230
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].fZDrawAt = 1540.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].sideStart = SIDE_RIGHT
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].fCrossingSpeed = 35.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_04)].bBeenHit = FALSE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].objID = ENUM_TO_INT(CROSS_LB_RAT_05)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fXCrossingProgress = -260
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fZDrawAt = 1560.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].fCrossingSpeed = 36.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_05)].bFlipAnim = TRUE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].objID = ENUM_TO_INT(CROSS_LB_RAT_07)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].fXCrossingProgress = 320
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].fZDrawAt = 2675.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].sideStart = SIDE_RIGHT
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].fCrossingSpeed = 33.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_07)].bBeenHit = FALSE

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].objID = ENUM_TO_INT(CROSS_LB_RAT_08)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].fXCrossingProgress = -320
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].fZDrawAt = 2775.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].sideStart = SIDE_LEFT
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].fCrossingSpeed = 38.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].bBeenHit = FALSE
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_08)].bFlipAnim = TRUE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].objID = ENUM_TO_INT(CROSS_LB_RAT_10)
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].sObjTexture = "CROSSING_RAT_01"

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fXCrossingProgress = 280
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fZDrawAt = 2875.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].fCrossingSpeed = 34.0
		crossingObject[ENUM_TO_INT(CROSS_LB_RAT_10)].bBeenHit = FALSE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_01)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fXCrossingProgress = 895

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fZDrawAt = 3770.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].fCrossingSpeed = 0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_01)].bBeenHit = FALSE

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fXCrossingProgress = -895

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fZDrawAt = 3900.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].sideStart = SIDE_LEFT

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].fCrossingSpeed = 0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_02)].bBeenHit = FALSE
		
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].objID = ENUM_TO_INT(CROSS_LB_DUMPSTER_02)
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].sObjTexture = "CROSSING_DUMPSTER_01"
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].fXCrossingProgress = 895

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].fYOffset = 0.1
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].fZDrawAt = 4045.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].sideStart = SIDE_RIGHT

		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].fCrossingSpeed = 0.0
		crossingObject[ENUM_TO_INT(CROSS_LB_DUMPSTER_03)].bBeenHit = FALSE

	ENDIF
	
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].objID = ENUM_TO_INT(CROSS_LB_ZOMBIE)
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].sObjTexture = "CROSSING_ZOMBIE_01"
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].fXCrossingProgress = 400

	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].fYOffset = 0.1
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].fZDrawAt = 4770.0
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].sideStart = SIDE_RIGHT

	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].fCrossingSpeed = 15.0
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].bBeenHit = FALSE
	crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].bIsEasterEgg = TRUE
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].fZDrawAt = 4570.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].fZDrawAt = 5300.0
	ENDIF 
	
ENDPROC

FUNC INT ROAD_ARCADE_GET_CROSSING_OBJ_COUNT(STAGES_SETS stage)

	INT count = 0

	IF stage = STAGE_VICE_CITY
		count = COUNT_OF(CROSSING_OBJECTS_ID_VICE)
	ENDIF
	
	IF stage = STAGE_SAN_FIERRO
		count = COUNT_OF(CROSSING_OBJECTS_ID_FIERRO)
	ENDIF

	IF stage = STAGE_SAN_ANDREAS
		count = COUNT_OF(CROSSING_OBJECTS_ID_ANDREAS)
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		count = COUNT_OF(CROSSING_OBJECTS_ID_VENTURAS)
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		count = COUNT_OF(CROSSING_OBJECTS_ID_LIBERTY)
	ENDIF
	
	RETURN count - 1 // Adjusted by one for arrays

ENDFUNC

PROC ROAD_ARCADE_INIT_STAGE_CROSSING_OBJ(STAGES_SETS stage)

	CROSSING_ROAD_OBJECT empty[iROAD_ARCADE_MaxCrossers]
	int i
	FOR i = 0 TO iROAD_ARCADE_MaxCrossers-1
		crossingObject[i] = empty[i]
	ENDFOR

	IF stage = STAGE_VICE_CITY
		CROSS_INIT_VICE()
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		CROSS_INIT_FIERRO()
	ENDIF

	IF stage = STAGE_SAN_ANDREAS
		CROSS_INIT_ANDREAS()
	ENDIF

	IF stage = STAGE_LAS_VENTURAS
		CROSS_INIT_VENTURAS()
	ENDIF

	IF stage = STAGE_LIBERTY_CITY
		CROSS_INIT_LIBERTY()
	ENDIF

ENDPROC

PROC ROAD_ARCADE_DRAW_CROSSING_OBJECTS(PLAYER_VEHICLE &player, ROAD_ARCADE_Z_BUFFER &zBuffer, STAGES_SETS currentStage, INT iObjectCount)
	
	// Don't process non initialised objects.
	IF crossingObject[iObjectCount].objID = -1
		EXIT
	ENDIF
	
	// Don't process if the Z value is out of range
	IF crossingObject[iObjectCount].fZDrawAt > fFinalZValue
		EXIT
	ENDIF
	
	FLOAT fROAD_CROSSNextZPosition = fJOURNEY_DistanceTravelled
	
	UNUSED_PARAMETER(player)
	
	INT iCROSSLineHorizon = round(ciROAD_HORIZON_POINT * 1.0)
	INT iROAD_CROSSInstanceCounter = 0
	
	// Count is out of limits
	IF iObjectCount > ROAD_ARCADE_GET_CROSSING_OBJ_COUNT(currentStage)
		EXIT
	ENDIF
			
	// Don't continue further is the object has already been drawn
	IF crossingObject[iObjectCount].fZDrawAt != 0
	AND fJOURNEY_DistanceTravelled >= crossingObject[iObjectCount].fZDrawAt
		EXIT
	ENDIF
	
	// Don't draw if they are taken out
	IF crossingObject[iObjectCount].bBeenHit
		EXIT
	ENDIF
		
	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
		
		FLOAT fROAD_CROSSZValue =  fROAD_CROSSNextZPosition
				
		FLOAT fROAD_CROSSDistanceScale = ((fROAD_CROSSZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-iCROSSLineHorizon)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
					
		VECTOR_2D vSpritePos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_CROSSInstanceCounter)									
				
		iROAD_CROSSInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
							
		// Stop the loop if we hit the limits of the screen
		IF cfBASE_SCREEN_HEIGHT - iROAD_CROSSInstanceCounter < iCROSSLineHorizon							
			BREAKLOOP
		ENDIF
													
		// Does the sprites position into the screen exceed the pattern transition threshold?
					
		IF fROAD_CROSSZValue >= crossingObject[iObjectCount].fZDrawAt
											
			// Object drawing positions
			VECTOR_2D vObjectScale = INIT_VECTOR_2D(160.0,160.0)
			
			IF ARE_STRINGS_EQUAL(crossingObject[iObjectCount].sObjTexture, "CROSSING_CENTURIAN_01")
				vObjectScale = INIT_VECTOR_2D(240.0,160.0)
			ENDIF
																		
			// Filter by stage
			STRING sStageDict = sDICT_TrackVice
			TEXT_LABEL_63 sSpriteTexture = "TRACK_01_"
			
			// Filter by stage
			IF currentStage = STAGE_VICE_CITY
				sStageDict = sDICT_TrackVice
				sSpriteTexture = "TRACK_01_"
			ENDIF

			IF currentStage = STAGE_SAN_FIERRO
				sStageDict = sDICT_TrackFeirro
				sSpriteTexture = "TRACK_03_"
			ENDIF
			
			IF currentStage = STAGE_SAN_ANDREAS
				sStageDict = sDICT_TrackAndreas
				sSpriteTexture = "TRACK_02_"
			ENDIF
			
			IF currentStage = STAGE_LAS_VENTURAS
				sStageDict = sDICT_TrackVenturas
				sSpriteTexture = "TRACK_05_"
			ENDIF
			
			IF currentStage = STAGE_LIBERTY_CITY
				sStageDict = sDICT_TrackLiberty
				sSpriteTexture = "TRACK_04_"
			ENDIF
			
			// Character may be an easter egg
			IF crossingObject[iObjectCount].bIsEasterEgg
				sStageDict = sDICT_EasterEggs
				
				REQUEST_STREAMED_TEXTURE_DICT(sStageDict)
				
				IF !HAS_STREAMED_TEXTURE_DICT_LOADED(sStageDict)
					fROAD_CROSSNextZPosition += (iROAD_CROSSInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_CROSSScaleMod+(fROAD_CROSSDistanceScale*2))	
					crossingObject[iObjectCount].fZDrawAt = fFinalZValue
					BREAKLOOP
				ENDIF
				
			ENDIF
			
			// Complete the object string
			sSpriteTexture += crossingObject[iObjectCount].sObjTexture
						
			vObjectScale = ROAD_ARCADE_GET_FINAL_SCALE(fROAD_CROSSDistanceScale, vObjectScale)
			vObjectScale = MULTIPLY_VECTOR_2D(vObjectScale, 0.75) // Double scaling defintiely doesn't work in this case!!
			
			crossingObject[iObjectCount].fAnimTimer += 0+@200
			
			IF crossingObject[iObjectCount].fAnimTimer < 100
				sSpriteTexture += "_FRAME_02"
			ELSE
				sSpriteTexture += "_FRAME_01"
			ENDIF
			
			// Reset anim timer
			IF crossingObject[iObjectCount].fAnimTimer >= 200
				crossingObject[iObjectCount].fAnimTimer = 0
			ENDIF
				
			IF crossingObject[iObjectCount].sideStart = SIDE_RIGHT
				crossingObject[iObjectCount].fXCrossingProgress -= 0+@(crossingObject[iObjectCount].fCrossingSpeed)
			ENDIF
			
			IF crossingObject[iObjectCount].sideStart = SIDE_LEFT	
				crossingObject[iObjectCount].fXCrossingProgress += 0+@(crossingObject[iObjectCount].fCrossingSpeed)
			ENDIF
			
			FLOAT yRelativeScaling = ((vSpritePos.Y - ciROAD_HORIZON_POINT)/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))
			FLOAT fSideLine1 = (fXCarOffset*(yRelativeScaling))
			//fSideLine1 += SCALING_ROAD_CURVATURE(currentStage, fROAD_CROSSZValue)*(2.0-fROAD_CROSSDistanceScale)
			fSideLine1 += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_CROSSZValue, iROAD_CROSSInstanceCounter, fROAD_CROSSDistanceScale)
			fSideLine1 += (crossingObject[iObjectCount].fXCrossingProgress*(yRelativeScaling))
			fSideLine1 *= (1.0-cfROAD_HillScale)	
			fSideLine1 += vSpritePos.X
									
			// Object drawing positions
			VECTOR_2D vObjectPos = INIT_VECTOR_2D(fSideLine1, vSpritePos.y)
			
			// Origin offset
			vObjectPos.y  -= (vObjectScale.y/2.0)*(1.0-crossingObject[iObjectCount].fYOffset)

			// Reverse the sprite
			IF crossingObject[iObjectCount].bFlipAnim
				vObjectScale.x = -ABSF(vObjectScale.x)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(sSpriteTexture))
			AND i > 2 // 2nd count will always produce a sprite regardless of progress
				
				ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(zBuffer, 
					sStageDict,
					sSpriteTexture,
					vObjectPos,
					vObjectScale,
					rgbaOriginal,
					0.0,
					fROAD_CROSSZValue)
				
				IF crossingObject[iObjectCount].fZWasDrawnAt = 0
					ROAD_ARCADE_INIT_WARNING_OBJ(sDICT_HUDGeneric, "HUD_BUTTON_WATCH_OUT", INIT_VECTOR_2D(288, 64))
				ENDIF
				
				fROAD_CROSSNextZPosition += (iROAD_CROSSInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_CROSSScaleMod+(fROAD_CROSSDistanceScale*2))
				crossingObject[iObjectCount].fZWasDrawnAt = fROAD_CROSSZValue
								
				IF iROAD_CROSSInstanceCounter <= 150
				
//					IF vObjectPos.x <= ((cfBASE_SCREEN_WIDTH/2.0) + ABSF(vObjectScale.x/2.0)) // Player would have knocked the objects.
//					AND vObjectPos.x >= ((cfBASE_SCREEN_WIDTH/2.0) - ABSF(vObjectScale.x/2.0))
					IF ABSF(vObjectPos.x - (cfBASE_SCREEN_WIDTH/2.0)) < ABSF(vObjectScale.x)
						// Trigger car swerve / crash
						IF eRoadArcade_ActiveGameType != RA_GT_BIKE
							RA_TRIGGER_PLAYER_SWERVE(player)
						ELSE
							RA_TRIGGER_PLAYER_CRASH(player)
						ENDIF
						
						IF NOT crossingObject[iObjectCount].bIsEasterEgg
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_COLLISION_PED)
						ELSE
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_COLLISION_SPECIAL)
						ENDIF
												
						crossingObject[iObjectCount].bBeenHit = TRUE
					ENDIF
					
					// Player is skimming the Pedestrian this frame
					IF ABSF(vObjectPos.x - (cfBASE_SCREEN_WIDTH/2.0)) < ABSF(vObjectScale.x*2.0)

						player.iPlayerScore += ROUND(0+@(500 * player.iSpeedMultiplier))
						
						// Special sound effects for one timers
						IF currentStage = STAGE_VICE_CITY
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_VC_BIGFOOT)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_BIGFOOT)
						ENDIF
						
						IF currentStage = STAGE_SAN_FIERRO
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_SF_WEREWOLF)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_BIGFOOT)
						ENDIF
												
						IF currentStage = STAGE_LAS_VENTURAS
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_LV_CENT_01)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_CENTURION)
						ENDIF
							
						IF currentStage = STAGE_SAN_ANDREAS
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_SA_GHOST)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_GHOST)
						ENDIF
						
						IF currentStage = STAGE_LAS_VENTURAS
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_LV_ALIEN)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_GHOST)
						ENDIF
						
						IF currentStage = STAGE_VICE_CITY
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_VC_BIKE)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_CYCLIST)
						ENDIF
						
						IF currentStage = STAGE_LIBERTY_CITY
						AND crossingObject[iObjectCount].objID = ENUM_TO_INT(CROSS_LB_ZOMBIE)
							ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PASSING_BIGFOOT)
						ENDIF
													
						// Double points if it's a rare crosser!
						IF crossingObject[iObjectCount].bIsEasterEgg
							player.iPlayerScore += ROUND(0+@(500 * player.iSpeedMultiplier))
						ENDIF
					ENDIF
					
				ENDIF
											
				BREAKLOOP

			ENDIF
			
		ENDIF
				
		fROAD_CROSSNextZPosition += (iROAD_CROSSInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_CROSSScaleMod+(fROAD_CROSSDistanceScale*2))	
	ENDFOR
	
ENDPROC


//-------------------------- ROAD OBJECT CONTROL -------------------------
PROC GENERIC_OV_INIT(STAGES_SETS sCurrentStage, STAGES_SETS sFirstStage)

	IF sCurrentStage = sFirstStage
		genericObjects[ENUM_TO_INT(G_START)].objID = ENUM_TO_INT(G_START)
		genericObjects[ENUM_TO_INT(G_START)].sObjTexture = "START_BANNER_01"
		genericObjects[ENUM_TO_INT(G_START)].vSpriteScale = INIT_VECTOR_2D(1264.0, 540.0)
		genericObjects[ENUM_TO_INT(G_START)].fYOffset = 0.25
		genericObjects[ENUM_TO_INT(G_START)].fZDrawAt = 30.0
		genericObjects[ENUM_TO_INT(G_START)].bIsStart = TRUE
	ENDIF
		
	genericObjects[ENUM_TO_INT(G_FINISH)].objID = -1
	genericObjects[ENUM_TO_INT(G_FINISH)].fZWasDrawnAt = 0
	
	// Reset the one off variables
	genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].objID = -1
	genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZDrawAt = 0
	genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZWasDrawnAt = 0

	genericObjects[ENUM_TO_INT(G_CROWD)].objID = -1
	genericObjects[ENUM_TO_INT(G_CROWD)].fZWasDrawnAt = 0
	
ENDPROC

PROC STAGE_OV_INIT(STAGES_SETS currentStage)

	// Control where the midstage checkpoints appear per stage
	FLOAT drawAtOne = ROAD_ARCADE_GET_END_OF_STAGE(currentStage)
	FLOAT drawAtTwo = ROAD_ARCADE_GET_END_OF_STAGE(currentStage)

	drawAtOne *= (1.0/3.0)
	drawAtTwo *= (2.0/3.0)

//	FLOAT drawAtOne
//	FLOAT drawAtTwo
	
//	IF currentStage = STAGE_VICE_CITY
//		drawAtOne = 1491
//		drawAtTwo = 2983
//	ENDIF
//	
//	IF currentStage = STAGE_SAN_FIERRO
//		drawAtOne = 1350
//		drawAtTwo = 2700
//	ENDIF
//	
//	IF currentStage = STAGE_SAN_ANDREAS
//		drawAtOne = 1383
//		drawAtTwo = 2766
//	ENDIF
//	
//	IF currentStage = STAGE_LAS_VENTURAS
//		drawAtOne = 1375
//		drawAtTwo = 2750
//	ENDIF
//	
//	IF currentStage = STAGE_LIBERTY_CITY
//		drawAtOne = 1483
//		drawAtTwo = 2967
//	ENDIF
	
	IF drawAtOne <= 0
		CDEBUG1LN(DEBUG_MINIGAME, "STAGE_OV_INIT - Invalid checkpoint 1 value")
	ENDIF
	
	IF drawAtTwo <= 0
		CDEBUG1LN(DEBUG_MINIGAME, "STAGE_OV_INIT - Invalid checkpoint 2 value")
	ENDIF
	
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_01)].objID = ENUM_TO_INT(G_CHECKPOINT_01)
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_01)].sObjTexture = "CHECKPOINT_01"
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_01)].vSpriteScale = INIT_VECTOR_2D(1264.0, 540.0)
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_01)].fYOffset = 0.0
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_01)].fZDrawAt = drawAtOne
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_01)].bIsCheckpoint = TRUE

	genericObjects[ENUM_TO_INT(G_CHECKPOINT_02)].objID = ENUM_TO_INT(G_CHECKPOINT_02)
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_02)].sObjTexture = "CHECKPOINT_01"
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_02)].vSpriteScale = INIT_VECTOR_2D(1264.0, 540.0)
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_02)].fYOffset = 0.0
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_02)].fZDrawAt = drawAtTwo
	genericObjects[ENUM_TO_INT(G_CHECKPOINT_02)].bIsCheckpoint = TRUE
	
	genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].objID = -1
	genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZDrawAt = 0
	genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZWasDrawnAt = 0
	
ENDPROC

PROC ROAD_ARCADE_DRAW_GENERIC_OVERROAD_OBJECTS(PLAYER_VEHICLE &player, ROAD_ARCADE_Z_BUFFER &zBuffer, STAGES_SETS currentStage, INT iObjectCount)

	// Don't proceed if the ID is invalid
	IF genericObjects[iObjectCount].objID = -1
		EXIT
	ENDIF
	
	// Don't process if the Z value is out of range
	IF genericObjects[iObjectCount].fZDrawAt > fFinalZValue
		EXIT
	ENDIF
	
	IF transitionData.transition_ActiveState != T_DRAW_FINISH
		IF transitionData.transition_ActiveState != T_INACTIVE
			EXIT
		ENDIF
	ENDIF

	// Init the counters to the updated starting Z value
	FLOAT fROAD_OVERNextZPosition = fJOURNEY_DistanceTravelled
	
	INT iOverLineHorizon = round(ciROAD_HORIZON_POINT * 1.0)
	INT iROAD_OVERInstanceCounter = 0
	
	IF fJOURNEY_DistanceTravelled > genericObjects[iObjectCount].fZDrawAt
	
		IF genericObjects[iObjectCount].bIsStart
			genericObjects[iObjectCount].objID = -1
			genericObjects[iObjectCount].bIsStart = FALSE
		ENDIF
	
		// Passed the checkpoint!
		IF genericObjects[iObjectCount].bIsCheckpoint
			
			// Remove the Easter egg character if the player was too slow!
			IF genericObjects[iObjectCount].objID = ENUM_TO_INT(G_CHECKPOINT_02)		
				IF fGameTimerElapsed > 30000
									
					IF currentStage = STAGE_VICE_CITY
						crossingObject[ENUM_TO_INT(CROSS_VC_BIGFOOT)].objID = -1
					ENDIF
				
					IF currentStage = STAGE_SAN_FIERRO
						crossingObject[ENUM_TO_INT(CROSS_SF_WEREWOLF)].objID = -1
					ENDIF
					
					IF currentStage = STAGE_SAN_ANDREAS
						crossingObject[ENUM_TO_INT(CROSS_SA_GHOST)].objID = -1
					ENDIF
					
					IF currentStage = STAGE_LAS_VENTURAS
						crossingObject[ENUM_TO_INT(CROSS_LV_ALIEN)].objID = -1
					ENDIF
					
					IF currentStage = STAGE_LIBERTY_CITY
						crossingObject[ENUM_TO_INT(CROSS_LB_ZOMBIE)].objID = -1
					ENDIF
				ENDIF
			ENDIF
			
			fGameTimerElapsed -= 25000
			genericObjects[iObjectCount].objID = -1
			player.ePlayerPedAnimState = RA_PLAYER_PED_ANIM_HIT_CHECKPOINT
			ROAD_ARCADE_INIT_TICKER_OBJ(sDICT_HUDGeneric, "HUD_BUTTON_EXTENDED", INIT_VECTOR_2D(260, 64))
			ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_CHECKPOINT_HIT)
						
			genericObjects[iObjectCount].bIsCheckpoint = FALSE
		ENDIF
	
		// Passed the finish
		IF genericObjects[iObjectCount].bIsFinish
			SETTIMERB(0)
			//fGameTimerElapsed = 0 // Bug: 6177887
			
			IF !bFinishedRace
				CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_GENERIC_OVERROAD_OBJECTS - Passed finish post")
				player.iPlayerScore += 50000 // Score bonus for finishing!
				
				IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
					ROAD_ARCADE_NET_SET_PARTICIPANT_FINAL_SCORE(player, PARTICIPANT_ID_TO_INT())
				ENDIF
								
				IF !player.bCrashedOnce
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_GENERIC_OVERROAD_OBJECTS - Completed MP_AWARD_TRAFFICAVOI challenge")
				
					IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TRAFFICAVOI)
						CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_GENERIC_OVERROAD_OBJECTS - Setting MP_AWARD_TRAFFICAVOI to completed")
						SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TRAFFICAVOI, TRUE)
						ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_ROAD_ARCADE_TRAFFICAVOI)
						SET_BITMASK_AS_ENUM(sRoadArcadeTelemetry.challenges, RA_CHALLENGE_TRAFFICAVOI)
					ENDIF
					
				ENDIF
				
				ROAD_ARCADE_NET_SET_PARTICIPANT_FINISHED(PARTICIPANT_ID_TO_INT())
				
				// url:bugstar:6207614 - Should have been awarded on finish instead of start.
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
				AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDCAR)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDCAR, TRUE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDBIKE)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDBIKE, TRUE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDTRUCK)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDTRUCK, TRUE)
				ENDIF
								
				IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDCAR)
				AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDBIKE)
				AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_RACECHASESTARTEDTRUCK)
				
					IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_CANTCATCHBRA)
						CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - GET_MP_BOOL_CHARACTER_AWARD - MP_AWARD_CANTCATCHBRA")
						SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_CANTCATCHBRA, TRUE)
						ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_ROAD_ARCADE_CANTCATCHBRA)
						SET_BITMASK_AS_ENUM(sRoadArcadeTelemetry.challenges, RA_CHALLENGE_CANTCATCHBRA)
					ENDIF
					
				ENDIF
				
				
				bFinishedRace = TRUE
			ENDIF
			
			SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_WIN)
			genericObjects[iObjectCount].bIsFinish = FALSE
		ENDIF
	
		EXIT
	ENDIF

	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
		
		FLOAT fROAD_OVERZValue =  fROAD_OVERNextZPosition
				
		FLOAT fROAD_OVERDistanceScale = ((fROAD_OVERZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-iOverLineHorizon)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
			
		VECTOR_2D vSpritePos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_OVERInstanceCounter)									
				
		iROAD_OVERInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
							
		// Stop the loop if we hit the limits of the screen
		IF cfBASE_SCREEN_HEIGHT - iROAD_OVERInstanceCounter < iOverLineHorizon							
			BREAKLOOP
		ENDIF
													
		// Does the sprites position into the screen exceed the pattern transition threshold?
		
		// Load the special assets ahead
		IF !IS_STRING_NULL_OR_EMPTY(genericObjects[iObjectCount].sObjDict)
			IF (fROAD_OVERZValue >= genericObjects[iObjectCount].fZDrawAt - 600)
				REQUEST_STREAMED_TEXTURE_DICT(genericObjects[iObjectCount].sObjDict)
			ENDIF
			
			IF !HAS_STREAMED_TEXTURE_DICT_LOADED(genericObjects[iObjectCount].sObjDict)
				fROAD_OVERNextZPosition += (iROAD_OVERInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_OVERScaleMod+(fROAD_OVERDistanceScale*2))	
				genericObjects[iObjectCount].fZDrawAt = fFinalZValue

				BREAKLOOP
			ENDIF
		ENDIF
				
		IF fROAD_OVERZValue >= genericObjects[iObjectCount].fZDrawAt

			VECTOR_2D vObjectScale = genericObjects[iObjectCount].vSpriteScale	
					
			// Adjust the X value according to the Curve
			FLOAT yRelativeScaling = ((vSpritePos.Y - ciROAD_HORIZON_POINT)/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))
			FLOAT fSideLine1 = (fXCarOffset*(yRelativeScaling))
			fSideLine1 += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_OVERZValue, iROAD_OVERInstanceCounter, fROAD_OVERDistanceScale)
			fSideLine1 *= (1.0-cfROAD_HillScale)	
			fSideLine1 += (cfBASE_SCREEN_WIDTH/2.0)
																
			// Object drawing positions
			VECTOR_2D vObjectPos = INIT_VECTOR_2D(fSideLine1, vSpritePos.y)
							
			STRING sStageDict = sDICT_GenericAssets
			
			IF !IS_STRING_NULL_OR_EMPTY(genericObjects[iObjectCount].sObjDict)
				sStageDict = genericObjects[iObjectCount].sObjDict
			ENDIF
			
			TEXT_LABEL_63 sSpriteTexture = "GENERIC"
									
			// Correct to 0 to 1 scale.
			vObjectScale = ROAD_ARCADE_GET_FINAL_SCALE(fROAD_OVERDistanceScale, vObjectScale)
			FLOAT fROAD_OverDistanceScaleInverse =  ROAD_ARCADE_GET_INVERTED_SCALE(fROAD_OVERDistanceScale)
				
			sSpriteTexture += "_FRAME_01_"
							
			// Origin offset
			vObjectPos.y  -= (vObjectScale.y/2.0)*(1.0-genericObjects[iObjectCount].fYOffset)
			
			// Complete the object string
			sSpriteTexture += genericObjects[iObjectCount].sObjTexture
												
			
			IF NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(sSpriteTexture))
			AND i > 2 // 2nd count will always produce a sprite regardless of progress
																	
				//ARCADE_CABINET_DRAW_SPRITE(sStageDict, sSpriteTexture, vObjectPos_SCREEN.x, vObjectPos_SCREEN.y, vObjectScale_SCREEN.x, vObjectScale_SCREEN.y, 0.0, rgbaOriginal)
				ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(zBuffer, 
					sStageDict, 
					sSpriteTexture, 
					vObjectPos,  
					vObjectScale, 
					rgbaOriginal,
					0.0,
					fROAD_OVERZValue)
					
				// Draw the flag man with the starting banner
				IF iObjectCount = ENUM_TO_INT(G_START)
					RA_FLAGGER_UPDATE(zBuffer, vObjectPos, fROAD_OverDistanceScaleInverse, fROAD_OVERZValue-1)
				ENDIF
				
				fROAD_OVERNextZPosition += (iROAD_OVERInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_OVERScaleMod+(fROAD_OVERDistanceScale*2))
				genericObjects[iObjectCount].fZWasDrawnAt = fROAD_OVERZValue
				
				IF iROAD_OVERInstanceCounter <= 175
					
					IF player.bOffRoad
						// Trigger car swerve / crash
						RA_TRIGGER_PLAYER_CRASH(player)
					ENDIF
					
				ENDIF
				
				BREAKLOOP

			ENDIF
		
		ENDIF
		
		fROAD_OVERNextZPosition += (iROAD_OVERInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_OVERScaleMod+(fROAD_OVERDistanceScale*2))	
	ENDFOR
	
ENDPROC

// --------------------- GENERIC VEHICLES --------------------
// Struct detailing info on an overroad object.
STRUCT AMBIENT_VEHICLE_OBJECT

	INT objID = -1
	STRING sObjDict
	STRING sObjTexture
	VECTOR_2D vSpriteScale
		
	// Position along route to draw the object
	FLOAT fZDrawAt
	FLOAT fZWasDrawnAt = 0.0 // Write to value 
	BOOL bWasDrawn = FALSE // Has the object been created?
	FLOAT fYOffset = 0.0
	
	FLOAT fVehicleSpeed = 50.0	
	INT iXOffsetFromCenter = 0
	BOOL bWillChange = FALSE
	BOOL bBeenHit = FALSE
	BOOL bBeenPassed = FALSE // If the vehicle has been passed we won't draw another one.

ENDSTRUCT

FUNC INT GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDELINE_SIDES side, STRING objDict)

	IF ARE_STRINGS_EQUAL(objDict, sDICT_GenericCar)
	OR ARE_STRINGS_EQUAL(objDict, sDICT_GenericBike)
		IF side = SIDE_LEFT
			RETURN -450
		ENDIF

		IF side = SIDE_RIGHT
			RETURN 450
		ENDIF
	ENDIF

	IF ARE_STRINGS_EQUAL(objDict, sDICT_GenericTruck)
	OR ARE_STRINGS_EQUAL(objDict, sDICT_GenericPickUp)
		IF side = SIDE_LEFT
			RETURN -550
		ENDIF

		IF side = SIDE_RIGHT
			RETURN 550
		ENDIF
	ENDIF

	RETURN 0

ENDFUNC

CONST_INT iROAD_ARCADE_MaxAmbientVehicles 15
AMBIENT_VEHICLE_OBJECT ambientVehicles[iROAD_ARCADE_MaxAmbientVehicles]

// Vice City Vehicles
ENUM GENERIC_VEHICLE_ID_VICE

	V_CAR_01,
	V_BIKE_01,
	V_PICK_UP_01,
	V_TRUCK_01,
	V_BIKE_02,
	V_CAR_02
	
ENDENUM

PROC VICE_CITY_VEH_INIT()

	// Car 01 - Vice City car
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].objID = ENUM_TO_INT(V_CAR_01)
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].fZDrawAt = 530
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(V_CAR_01)].fVehicleSpeed = 60.0
		
	// Bike 01 - Vice City bike
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].objID = ENUM_TO_INT(V_BIKE_01)
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].fZDrawAt = 1325
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(V_BIKE_01)].fVehicleSpeed = 55.0
	
	// Truck 01 - Vice City truck
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].objID = ENUM_TO_INT(V_TRUCK_01)
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].sObjDict = sDICT_GenericTruck
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericTruck)
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].fYOffset = 0.2
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].fZDrawAt = 2700
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(V_TRUCK_01)].fVehicleSpeed = 55.0
	
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].objID = ENUM_TO_INT(V_PICK_UP_01)
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].sObjDict = sDICT_GenericPickUp
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].fZDrawAt = 3125
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(V_PICK_UP_01)].fVehicleSpeed = 65.0
		
	// Bike 02 - Vice City bike
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].objID = ENUM_TO_INT(V_BIKE_02)
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericBike)
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].fZDrawAt = 3730
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(V_BIKE_02)].fVehicleSpeed = 80.0
		
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(V_BIKE_02)].fVehicleSpeed = 52.5
	ENDIF
		
	// Car 02 - Vice City car
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].objID = ENUM_TO_INT(V_CAR_02)
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].fZDrawAt = 4930
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(V_CAR_02)].fVehicleSpeed = 55.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(V_CAR_02)].fZDrawAt = 4650
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(V_CAR_02)].fZDrawAt = 5800
		ambientVehicles[ENUM_TO_INT(V_CAR_02)].fVehicleSpeed = 80.0
	ENDIF
		
ENDPROC

// San Feirro vehicles
ENUM GENERIC_VEHICLE_ID_FIERRO
	SF_CAR_01,
	SF_TRUCK_03,
	SF_TRUCK_02,
	SF_TRUCK_01,
	SF_BIKE_03,
	SF_BIKE_02,
	SF_BIKE_01
ENDENUM


PROC SAN_FIERRO_VEH_INIT()

	// BIKE 01 - San Feirro bike
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].objID = ENUM_TO_INT(SF_BIKE_01)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].bWasDrawn = FALSE
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].fVehicleSpeed = 50.0
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].fZDrawAt = 5600
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].fVehicleSpeed = 40.0
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].fZDrawAt = 730
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].iXOffsetFromCenter = -150
	ENDIF
	
	// Don't draw for bike
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SF_BIKE_01)].objID = -1
	ENDIF
	
	
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].objID = ENUM_TO_INT(SF_BIKE_02)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].bWasDrawn = FALSE
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fVehicleSpeed = 40.0
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fZDrawAt = 1070
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].iXOffsetFromCenter = 100 + GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fVehicleSpeed = 30.0
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fZDrawAt = 1175.0
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].iXOffsetFromCenter = 250
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fVehicleSpeed = 60.0
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].fZDrawAt = 4700
		ambientVehicles[ENUM_TO_INT(SF_BIKE_02)].iXOffsetFromCenter = -250
	ENDIF

				
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].objID = ENUM_TO_INT(SF_BIKE_03)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].iXOffsetFromCenter = (-100) + GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericBike)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].fZDrawAt = 1555
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].bWasDrawn = FALSE
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].fVehicleSpeed = 60.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].fVehicleSpeed = 70.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(SF_BIKE_03)].fVehicleSpeed = 45.0
	ENDIF
		
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
	OR eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].objID = ENUM_TO_INT(SF_TRUCK_01)
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].sObjDict = sDICT_GenericTruck
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericTruck)-320
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].fYOffset = 0.2
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].fZDrawAt = 2540
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].fVehicleSpeed = 40.0
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ambientVehicles[ENUM_TO_INT(SF_TRUCK_01)].fZDrawAt = 2800
		ENDIF

		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].objID = ENUM_TO_INT(SF_TRUCK_02)
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].sObjDict = sDICT_GenericTruck
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericTruck)+320
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].fYOffset = 0.2
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].fZDrawAt = 2800
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].fVehicleSpeed = 40.0
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ambientVehicles[ENUM_TO_INT(SF_TRUCK_02)].fZDrawAt = 4200
		ENDIF
	ENDIF
	
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].objID = ENUM_TO_INT(SF_TRUCK_03)
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].sObjDict = sDICT_GenericTruck
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericTruck)-320
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].fYOffset = 0.2
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].fZDrawAt = 4550
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].fVehicleSpeed = 40.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SF_TRUCK_03)].fZDrawAt = 4325
	ENDIF

	// Car only
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].objID = ENUM_TO_INT(SF_CAR_01)
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)+100
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].fZDrawAt = 5000
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(SF_CAR_01)].fVehicleSpeed = 62.5
	ENDIF

		
ENDPROC

// San Andreas vehicles
ENUM GENERIC_VEHICLE_ID_ANDREAS
	
	SA_CAR_02,
	SA_BIKE_02,
	SA_BIKE_01,
	SA_PICKUP_03,
	SA_PICKUP_02,
	SA_PICKUP_01,
	SA_CAR_01
		
ENDENUM

PROC SAN_ANDREAS_VEH_INIT()

	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].objID = ENUM_TO_INT(SA_CAR_01)
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].fZDrawAt = 367.5
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(SA_CAR_01)].fVehicleSpeed = 45.0
	
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].objID = ENUM_TO_INT(SA_PICKUP_01)
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].sObjDict = sDICT_GenericPickUp
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].fZDrawAt = 1200
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_01)].fVehicleSpeed = 50.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].objID = ENUM_TO_INT(SA_PICKUP_02)
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].sObjDict = sDICT_GenericPickUp
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].fYOffset = 0.4
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].fZDrawAt = 1875
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_02)].fVehicleSpeed = 50.0

	ENDIF

	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].objID = ENUM_TO_INT(SA_PICKUP_03)
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].sObjDict = sDICT_GenericPickUp
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].fZDrawAt = 2375
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].fVehicleSpeed = 55.0

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].fZDrawAt = 4925
		ambientVehicles[ENUM_TO_INT(SA_PICKUP_03)].fVehicleSpeed = 38.0
	ENDIF
			
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].objID = ENUM_TO_INT(SA_BIKE_01)
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].fZDrawAt = 3430
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].bWasDrawn = FALSE
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].fVehicleSpeed = 50.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].fVehicleSpeed = 75.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(SA_BIKE_01)].fVehicleSpeed = 55.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].objID = ENUM_TO_INT(SA_BIKE_02)
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].sObjDict = sDICT_GenericBike
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].bWasDrawn = FALSE		
		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericBike)
			ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].fZDrawAt = 5505
			ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].fVehicleSpeed = 60.0
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
			ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].fZDrawAt = 3625
			ambientVehicles[ENUM_TO_INT(SA_BIKE_02)].fVehicleSpeed = 40.0
		ENDIF

	ENDIF
	
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].objID = ENUM_TO_INT(SA_CAR_02)
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(SA_CAR_02)].bWasDrawn = FALSE
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fZDrawAt = 4995
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fVehicleSpeed = 60.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fZDrawAt = 5125
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fVehicleSpeed = 55.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fZDrawAt = 3750
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(SA_CAR_02)].fVehicleSpeed = 45.0
	ENDIF
	
ENDPROC

// San Andreas vehicles
ENUM GENERIC_VEHICLE_ID_VENTURAS
	LV_SLOW_CAR_03,
	LV_SLOW_CAR_02,
	LV_SLOW_CAR_01,
	LV_BIKE_01,
	LV_PICKUP_01,
	
	LV_CAR_CONVOY2_03,
	LV_CAR_CONVOY2_01,
	LV_CAR_CONVOY1_03,
	LV_CAR_CONVOY1_01
	
ENDENUM

PROC LAS_VENTURAS_VEH_INIT()

	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].objID = ENUM_TO_INT(LV_CAR_CONVOY1_01)
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fZDrawAt = 495
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fVehicleSpeed = 35.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fVehicleSpeed = 45.0
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fZDrawAt = 685
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fVehicleSpeed = 35.0
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_01)].fZDrawAt = 2370.0
	ENDIF

	
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].objID = ENUM_TO_INT(LV_CAR_CONVOY2_01)
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fZDrawAt = 3130
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fVehicleSpeed = 35.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fZDrawAt = 540
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fVehicleSpeed = 45.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fZDrawAt = 555
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_01)].fVehicleSpeed = 25.0
	ENDIF
		
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].objID = ENUM_TO_INT(LV_CAR_CONVOY2_03)
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].fZDrawAt = 2875
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY2_03)].fVehicleSpeed = 25.0
	ENDIF
	
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].objID = ENUM_TO_INT(LV_PICKUP_01)
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].sObjDict = sDICT_GenericPickUp
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericPickUp)
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fZDrawAt = 1900
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fVehicleSpeed = 50.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fZDrawAt = 1075
		ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fVehicleSpeed = 65.0
		ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fZDrawAt = 1000
		ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].fVehicleSpeed = 40.0
		ambientVehicles[ENUM_TO_INT(LV_PICKUP_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericPickUp)
	ENDIF
	
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].objID = ENUM_TO_INT(LV_BIKE_01)
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericBike)+50
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fZDrawAt = 2450
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fVehicleSpeed = 55.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fZDrawAt = 2185
		ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fVehicleSpeed = 70.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fZDrawAt = 1920
		ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].fVehicleSpeed = 40.0
		ambientVehicles[ENUM_TO_INT(LV_BIKE_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike) + 136
	ENDIF
	
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].objID = ENUM_TO_INT(LV_SLOW_CAR_01)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fZDrawAt = 3625
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fVehicleSpeed = 30.0
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fZDrawAt = 4275
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fVehicleSpeed = 47.5
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
	ENDIF
	
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].objID = ENUM_TO_INT(LV_SLOW_CAR_02)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)+70
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fZDrawAt = 4485
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fVehicleSpeed = 40.0
	
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].objID = ENUM_TO_INT(LV_SLOW_CAR_02)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)-70
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fZDrawAt = 5225
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fVehicleSpeed = 45.0
	
	// Avoid Alien overlap.
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fZDrawAt = 5335
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)+80
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
	
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_02)].fZDrawAt = 4230.0
	
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fZDrawAt = 5420
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].fVehicleSpeed = 32.5
		ambientVehicles[ENUM_TO_INT(LV_SLOW_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)

		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].objID = ENUM_TO_INT(LV_CAR_CONVOY1_03)
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].fZDrawAt = 5510
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LV_CAR_CONVOY1_03)].fVehicleSpeed = 37.5
	ENDIF
			
ENDPROC

// San Andreas vehicles
ENUM GENERIC_VEHICLE_ID_LIBERTY

	LB_JAM03_PICKUP_02,
	LB_JAM03_BIKE_03,
	LB_JAM03_CAR_02,
	LB_JAM03_TRUCK_01,
	LB_JAM03_CAR_01,
	
	LB_JAM02_PICKUP_02,
	LB_JAM02_BIKE_01,
	LB_JAM02_CAR_01,
	LB_JAM02_TRUCK_03,
	LB_JAM02_TRUCK_01,
	
	LB_JAM01_CAR_04,
	LB_JAM01_PICKUP_01,
	LB_JAM01_BIKE_02,
	LB_JAM01_TRUCK_01,
	LB_JAM01_CAR_01
		
ENDENUM

PROC LIBERTY_CITY_VEH_INIT()
	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].objID = ENUM_TO_INT(LB_JAM01_CAR_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].fZDrawAt = 1200
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].fVehicleSpeed = 0.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].fZDrawAt = 1025.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].objID = ENUM_TO_INT(LB_JAM01_TRUCK_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].sObjDict = sDICT_GenericTruck
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericTruck)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].fYOffset = 0.2
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].fZDrawAt = 1400.0
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].fVehicleSpeed = 0.0
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ambientVehicles[ENUM_TO_INT(LB_JAM01_CAR_01)].fZDrawAt = 3095.0
		ambientVehicles[ENUM_TO_INT(LB_JAM01_TRUCK_01)].fVehicleSpeed = 20.0
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].objID = ENUM_TO_INT(LB_JAM01_PICKUP_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].sObjDict = sDICT_GenericPickUp
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericPickUp)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].fYOffset = 0.4
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].fZDrawAt = 730
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM01_PICKUP_01)].fVehicleSpeed = 0.0
	ENDIF

	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].objID = ENUM_TO_INT(LB_JAM02_TRUCK_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].sObjDict = sDICT_GenericTruck
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericTruck)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].fYOffset = 0.2
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].fZDrawAt = 2000
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_01)].fVehicleSpeed = 0.0
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].objID = ENUM_TO_INT(LB_JAM02_TRUCK_03)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].sObjDict = sDICT_GenericTruck
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericTruck)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].fYOffset = 0.2
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].fZDrawAt = 2200
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_TRUCK_03)].fVehicleSpeed = 0.0
	ENDIF

	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].objID = ENUM_TO_INT(LB_JAM01_BIKE_02)
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].sObjDict = sDICT_GenericBike
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericBike)+200
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].fZDrawAt = 1600
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM01_BIKE_02)].fVehicleSpeed = 0.0

	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].objID = ENUM_TO_INT(LB_JAM02_CAR_01)
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].sObjDict = sDICT_GenericCar
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericCar)-200
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].fYOffset = 0.5
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].fZDrawAt = 1600
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM02_CAR_01)].fVehicleSpeed = 0.0
	
	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].objID = ENUM_TO_INT(LB_JAM02_BIKE_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].sObjDict = sDICT_GenericBike
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].fZDrawAt = 2550
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM02_BIKE_01)].fVehicleSpeed = 0.0
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].objID = ENUM_TO_INT(LB_JAM03_CAR_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fZDrawAt = 2875.0
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fVehicleSpeed = 0.0
	ENDIF

	IF eRoadArcade_ActiveGameType != RA_GT_BIKE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].objID = ENUM_TO_INT(LB_JAM03_CAR_01)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fZDrawAt = 3275
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_01)].fVehicleSpeed = 0.0
	ENDIF

	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].objID = ENUM_TO_INT(LB_JAM03_TRUCK_01)
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].sObjDict = sDICT_GenericTruck
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].sObjTexture = "VEHICLE_AMBIENT_TRUCK"
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericTruck)
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].vSpriteScale = INIT_VECTOR_2D(512.0, 512.0)
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].fYOffset = 0.2
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].fZDrawAt = 4075
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM03_TRUCK_01)].fVehicleSpeed = 0.0
		
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].objID = ENUM_TO_INT(LB_JAM03_CAR_02)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].sObjDict = sDICT_GenericCar
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].sObjTexture = "VEHICLE_AMBIENT_CAR"
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericCar)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].fZDrawAt = 4420
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_CAR_02)].fVehicleSpeed = 0.0

		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].objID = ENUM_TO_INT(LB_JAM03_BIKE_03)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].sObjDict = sDICT_GenericBike
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].sObjTexture = "VEHICLE_AMBIENT_BIKE"
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_RIGHT, sDICT_GenericBike)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].fYOffset = 0.5
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].fZDrawAt = 4560
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].fZWasDrawnAt = 0.0
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].bBeenHit = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].bBeenPassed = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].bWasDrawn = FALSE
		ambientVehicles[ENUM_TO_INT(LB_JAM03_BIKE_03)].fVehicleSpeed = 0.0
	ENDIF
	
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].objID = ENUM_TO_INT(LB_JAM03_PICKUP_02)
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].sObjDict = sDICT_GenericPickUp
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].sObjTexture = "VEHICLE_AMBIENT_PICK_UP"
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)+200
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].vSpriteScale = INIT_VECTOR_2D(752.0, 288.0)
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].fYOffset = 0.4
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].fZDrawAt = 5080
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].fZWasDrawnAt = 0.0
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].bBeenHit = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].bBeenPassed = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].bWasDrawn = FALSE
	ambientVehicles[ENUM_TO_INT(LB_JAM03_PICKUP_02)].fVehicleSpeed = 0.0
		
ENDPROC


PROC ROAD_ARCADE_INIT_GENERIC_VEH(STAGES_SETS stage)

	AMBIENT_VEHICLE_OBJECT empty[iROAD_ARCADE_MaxAmbientVehicles]
	int i
	FOR i = 0 TO iROAD_ARCADE_MaxAmbientVehicles-1
		ambientVehicles[i] = empty[i]
	ENDFOR

	IF stage = STAGE_VICE_CITY
		VICE_CITY_VEH_INIT()
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		SAN_FIERRO_VEH_INIT()
	ENDIF
	
	IF stage = STAGE_SAN_ANDREAS
		SAN_ANDREAS_VEH_INIT()
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		LAS_VENTURAS_VEH_INIT()
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		LIBERTY_CITY_VEH_INIT()
	ENDIF

ENDPROC

FUNC INT ROAD_ARCADE_GET_VEH_COUNT(STAGES_SETS stage)

	INT count = 0

	IF stage = STAGE_VICE_CITY
		count = COUNT_OF(GENERIC_VEHICLE_ID_VICE)
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		count = COUNT_OF(GENERIC_VEHICLE_ID_FIERRO)
	ENDIF
	
	IF stage = STAGE_SAN_ANDREAS
		count = COUNT_OF(GENERIC_VEHICLE_ID_ANDREAS)
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		count = COUNT_OF(GENERIC_VEHICLE_ID_VENTURAS)
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		count = COUNT_OF(GENERIC_VEHICLE_ID_LIBERTY)
	ENDIF
	
	RETURN count - 1 // Adjusted by one for arrays

ENDFUNC

PROC ROAD_ARCADE_DRAW_GENERIC_VEH(PLAYER_VEHICLE &player, ROAD_ARCADE_Z_BUFFER &zBuffer, STAGES_SETS currentStage, INT iObjectCount)

	// Don't process if the Z value is out of range
	IF ambientVehicles[iObjectCount].fZDrawAt > fFinalZValue
		EXIT
	ENDIF
	
	// Do not draw the overroad vehicles in this state
	IF transitionData.transition_ActiveState = T_LOAD
	OR transitionData.transition_ActiveState = T_WAIT_FOR_RETURN
		EXIT
	ENDIF
	
	// Init the counters to the updated starting Z value
	FLOAT fROAD_VEHNextZPosition = fJOURNEY_DistanceTravelled
	
	UNUSED_PARAMETER(player)
	
	INT iVEHLineHorizon = round(ciROAD_HORIZON_POINT * 1.0)
	INT iROAD_VEHInstanceCounter = 0
	
	IF ambientVehicles[iObjectCount].objID = -1
		EXIT
	ENDIF
	
	// Don't draw if they are taken out
	IF ambientVehicles[iObjectCount].bBeenHit
		EXIT
	ENDIF
	
	// Don't draw if they are taken out
	IF ambientVehicles[iObjectCount].bBeenPassed
		EXIT
	ENDIF
		
	// Was the vehicle drawn this frame?
	BOOL bVehDrawnThisFrame = FALSE
	
	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
		
		FLOAT fROAD_VEHZValue =  fROAD_VEHNextZPosition
				
		FLOAT fROAD_VEHDistanceScale = ((fROAD_VEHZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-iVEHLineHorizon)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
			
		VECTOR_2D vSpritePos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_VEHInstanceCounter)									
				
		iROAD_VEHInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
							
		// Stop the loop if we hit the limits of the screen
		IF cfBASE_SCREEN_HEIGHT - iROAD_VEHInstanceCounter < iVEHLineHorizon							
			BREAKLOOP
		ENDIF
															
		// Does the sprites position into the screen exceed the pattern transition threshold?		
		IF fROAD_VEHZValue >= ambientVehicles[iObjectCount].fZDrawAt
											
			// Object drawing positions
			VECTOR_2D vObjectScale = ambientVehicles[iObjectCount].vSpriteScale
			
			// Complete the object string
			TEXT_LABEL_63 sSpriteTexture = ambientVehicles[iObjectCount].sObjTexture
			
						
			vObjectScale = ROAD_ARCADE_GET_FINAL_SCALE(fROAD_VEHDistanceScale, vObjectScale)
			vObjectScale = DIVIDE_VECTOR_2D(vObjectScale, 2.0) // Double scaling defintiely doesn't work in this case!!
									
			sSpriteTexture += "_FRAME_01"
	
			// Convert text label to String
			STRING sSpriteTextureF = TEXT_LABEL_TO_STRING(sSpriteTexture)
				
			FLOAT yRelativeScaling = ((vSpritePos.Y - ciROAD_HORIZON_POINT)/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))
			
			FLOAT fSideLine1 = (fXCarOffset*(yRelativeScaling))
			//fSideLine1 += SCALING_ROAD_CURVATURE(currentStage, fROAD_VEHZValue)*(2.0-fROAD_VEHDistanceScale)
			fSideLine1 += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_VEHZValue, iROAD_VEHInstanceCounter, fROAD_VEHDistanceScale)
			fSideLine1 += (ambientVehicles[iObjectCount].iXOffsetFromCenter*(yRelativeScaling))
			fSideLine1 *= (1.0-cfROAD_HillScale)	
			fSideLine1 += vSpritePos.X
				
			// Object drawing positions
			VECTOR_2D vObjectPos = INIT_VECTOR_2D(fSideLine1, vSpritePos.y)
			
			// Origin offset
			vObjectPos.y -= (vObjectScale.y/2.0)*(1.0-ambientVehicles[iObjectCount].fYOffset)

			
			IF NOT IS_STRING_NULL_OR_EMPTY(sSpriteTextureF)
			AND i > 2 // 2nd count will always produce a sprite regardless of progress
					
				ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(zBuffer, 
					ambientVehicles[iObjectCount].sObjDict,
					sSpriteTexture,
					vObjectPos,
					vObjectScale,
					rgbaOriginal,
					0.0,
					fROAD_VEHZValue)
					
				IF !ambientVehicles[iObjectCount].bWasDrawn
					ambientVehicles[iObjectCount].bWasDrawn = TRUE
				ENDIF
				
				bVehDrawnThisFrame = TRUE
								
				fROAD_VEHNextZPosition += (iROAD_VEHInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_VEHScaleMod+(fROAD_VEHDistanceScale*2))
				ambientVehicles[iObjectCount].fZWasDrawnAt = fROAD_VEHZValue
												
				IF ambientVehicles[iObjectCount].fZDrawAt <= player.fZvalue + 1.5
					
					IF vObjectPos.x <= ((cfBASE_SCREEN_WIDTH/2.0) + ABSF(vObjectScale.x/4.0)) // Player would have knocked the objects.
					AND vObjectPos.x >= ((cfBASE_SCREEN_WIDTH/2.0) - ABSF(vObjectScale.x/4.0))
				
						// Trigger car swerve / crash
						IF eRoadArcade_ActiveGameType != RA_GT_BIKE
							IF NOT ARE_STRINGS_EQUAL(ambientVehicles[iObjectCount].sObjDict, sDICT_GenericBike)
								CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_GENERIC_VEH - Crashed into Object with Dictionary: ", ambientVehicles[iObjectCount].sObjDict)
								CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_GENERIC_VEH - Crashed into Object with ID no: ", iObjectCount)
								RA_TRIGGER_PLAYER_CRASH(player)
							ELSE // Just swerve if we hit the bike
								// Trigger car swerve / crash
								RA_TRIGGER_PLAYER_SWERVE(player)
							ENDIF
						ELSE
							RA_TRIGGER_PLAYER_CRASH(player)
						ENDIF
						
						ambientVehicles[iObjectCount].bBeenHit = TRUE
					ELSE
						//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(ambientVehicles[iObjectCount].sObjDict)
						IF !ambientVehicles[iObjectCount].bBeenPassed
							player.iPlayerScore += (500*player.iSpeedMultiplier)
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_WORLD_PASS)
							ambientVehicles[iObjectCount].bBeenPassed = TRUE
						ENDIF
					ENDIF
										
				ENDIF
				
				// Player is behind the car. Active slipstreaming.
				IF ambientVehicles[iObjectCount].fZDrawAt <= player.fZvalue + 70
					IF vObjectPos.x <= ((cfBASE_SCREEN_WIDTH/2.0) + ABSF(vObjectScale.x/2.5)) // Player would have knocked the objects.
					AND vObjectPos.x >= ((cfBASE_SCREEN_WIDTH/2.0) - ABSF(vObjectScale.x/2.5))
						// Slipstream bonus
						player.iPlayerScore += ROUND(0+@(500 * player.iSpeedMultiplier))
						player.bWillSlipStream = TRUE
					ENDIF
				ENDIF
				
												
				BREAKLOOP

			ENDIF
			
		ENDIF
						
		fROAD_VEHNextZPosition += (iROAD_VEHInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_VEHScaleMod+(fROAD_VEHDistanceScale*2))	
	ENDFOR
	
	// Add to the Vehicles Z Value according to speed		
	IF bVehDrawnThisFrame
	AND transitionData.transition_ActiveState = T_INACTIVE
		ambientVehicles[iObjectCount].fZDrawAt += 0+@(ambientVehicles[iObjectCount].fVehicleSpeed)		
	ENDIF
		
		
ENDPROC

//------------------ RIVAL VEHICLES --------------------
STRUCT RIVAL_VEHICLE_OBJECT

	INT objID = -1
	VECTOR_2D vSpriteScale
		
	// Position along route to draw the object
	FLOAT fZDrawAt
	FLOAT fZWasDrawnAt = 0.0 // Write to value 
	BOOL bWasDrawn = FALSE // Has the object been created?
	FLOAT fYOffset = 0.5
	
	FLOAT fVehicleSpeed = 70.0	
	INT iXOffsetFromCenter = 0
	BOOL bBeenHit = FALSE
	BOOL bCrashed = FALSE
	BOOL bBeenPassed = FALSE // If the vehicle has been passed we won't draw another one.

	FLOAT fSwerveSpeed = 400.0
	SIDELINE_SIDES currentSide
	
	FLOAT fSideChange = 100.0
	FLOAT fZSinceChange = 0.0
	
	BOOL bIsMoving = FALSE
	
	// Hard values for the Rival offset from the center.
	INT iRightOffset = 385
	INT iLeftOffset = -385
	
	BOOL bDrawShadow = FALSE

ENDSTRUCT

CONST_INT iROAD_ARCADE_MaxRivalVehicles 2
RIVAL_VEHICLE_OBJECT rivalVehicles[iROAD_ARCADE_MaxRivalVehicles]

FUNC TEXT_LABEL_63 ROAD_ARCADE_GET_RIVAL_TEXTURE_FROM_PLAYER_ID(INT playerID)

	TEXT_LABEL_63 returnLabel = ""

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF playerID = 0
			returnLabel = "VEHICLE_CAR_02_FRAME_01_UP_HILL_DISTANT_FRAME_01"
		ENDIF

		IF playerID = 1
			returnLabel = "VEHICLE_CAR_01_FRAME_01_UP_HILL_DISTANCE_FRAME_01"
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		IF playerID = 0
			returnLabel = "VEHICLE_BIKE_02_FRAME_01_UP_HILL_DISTANT_FRAME_01"
		ENDIF

		IF playerID = 1
			returnLabel = "VEHICLE_BIKE_01_FRAME_01_UP_HILL_DISTANT_FRAME_01"
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF playerID = 0
			returnLabel = "VEHICLE_TRUCK_02_FRAME_01_UP_HILL_DISTANT_FRAME_01"
		ENDIF

		IF playerID = 1
			returnLabel = "VEHICLE_TRUCK_01_FRAME_01_UP_HILL_DISTANT_FRAME_01"
		ENDIF
	ENDIF
		
	RETURN returnLabel
	
ENDFUNC

// Vice City Rivals
ENUM RIVAL_VEHICLE_ID_VICE

	V_RIVAL_01
	
ENDENUM

PROC VICE_CITY_RIVAL_INIT()

	INT leftOffset = rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].iLeftOffset
	INT rightOffset = rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].iRightOffset

	UNUSED_PARAMETER(rightOffset)

	// Car 01 - Vice City car
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].objID = ENUM_TO_INT(V_RIVAL_01)
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].iXOffsetFromCenter = leftOffset
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)

	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fZDrawAt = 1650.0
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fSwerveSpeed = 800.0
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].currentSide = SIDE_LEFT
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fVehicleSpeed = 65.0
	rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fZDrawAt = 2325.0
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fVehicleSpeed = 27.5
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].iXOffsetFromCenter = rightOffset + 100
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].currentSide = SIDE_RIGHT
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fZDrawAt = 6450.0
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].fVehicleSpeed = 90.0
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericBike)
		rivalVehicles[ENUM_TO_INT(V_RIVAL_01)].currentSide = SIDE_LEFT
	ENDIF
		
ENDPROC

// San Feirro rivals
ENUM RIVAL_VEHICLE_ID_FIERRO

	SF_RIVAL_01
	
ENDENUM

PROC SAN_FIERRO_RIVAL_INIT()

	INT leftOffset = rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].iLeftOffset
	INT rightOffset = rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].iRightOffset

	UNUSED_PARAMETER(leftOffset)

	// Car 02 - San Feirro rival 1
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].objID = ENUM_TO_INT(SF_RIVAL_01)
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].iXOffsetFromCenter = rightOffset
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)

	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fZDrawAt = 828.5
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fSwerveSpeed = 600.0
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].currentSide = SIDE_RIGHT
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fVehicleSpeed = 55.0
	rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fZDrawAt = 3468.5
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fZDrawAt = 4960.0
		rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].fVehicleSpeed = 70.5
		rivalVehicles[ENUM_TO_INT(SF_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF
		
ENDPROC

// San Andreas rivals
ENUM RIVAL_VEHICLE_ID_ANDREAS

	SA_RIVAL_02,
	SA_RIVAL_01
	
	
ENDENUM

PROC SAN_ANDREAS_RIVAL_INIT()

	INT leftOffset = rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].iLeftOffset
	INT rightOffset = rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].iRightOffset

	// Rival 01 - San Andreas rival 1
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].objID = ENUM_TO_INT(SA_RIVAL_01)
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].iXOffsetFromCenter = rightOffset
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fZDrawAt = 1550.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fSwerveSpeed = 300.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].currentSide = SIDE_RIGHT
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fVehicleSpeed = 55.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fZDrawAt = 600.0
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].fVehicleSpeed = 40.0
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF
	
	// Rival 02 - San Andreas rival 2
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].objID = ENUM_TO_INT(SA_RIVAL_02)
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].iXOffsetFromCenter = leftOffset
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fZDrawAt = 5332.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fSwerveSpeed = 800.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].currentSide = SIDE_LEFT
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fVehicleSpeed = 55.0
	rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fZDrawAt = 4950.0
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fVehicleSpeed = 67.5
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF
		
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fZDrawAt = 5598.0
		rivalVehicles[ENUM_TO_INT(SA_RIVAL_02)].fVehicleSpeed = 77.5
	ENDIF
		
ENDPROC

// Las Venturas rivals
ENUM RIVAL_VEHICLE_ID_VENTURAS

	LV_RIVAL_02,
	LV_RIVAL_01
	
ENDENUM

PROC LAS_VENTURAS_RIVAL_INIT()

	INT leftOffset = rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].iLeftOffset
	INT rightOffset = rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].iRightOffset

	UNUSED_PARAMETER(leftOffset)

	// Rival 01 - San Andreas rival 1
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].objID = ENUM_TO_INT(LV_RIVAL_01)
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].iXOffsetFromCenter = rightOffset
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)

	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fZDrawAt = 1285.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fSwerveSpeed = 400.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].currentSide = SIDE_RIGHT
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fVehicleSpeed = 50.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fZSinceChange = 0

	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fZDrawAt = 1385.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fVehicleSpeed = 75.5
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fSwerveSpeed = 800.0
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fZDrawAt = 1150.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].currentSide = SIDE_LEFT
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].fVehicleSpeed = 45.5
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].iXOffsetFromCenter = GENERIC_VEH_GET_OFFSET_FROM_SIDE(SIDE_LEFT, sDICT_GenericPickUp)
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF

	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].objID = ENUM_TO_INT(LV_RIVAL_02)
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].iXOffsetFromCenter = (rightOffset-150) // PLaying around with offsets
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)

	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZDrawAt = 5615.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fSwerveSpeed = 900.0
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].currentSide = SIDE_RIGHT
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fVehicleSpeed = 82.5
	rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZDrawAt = 3260.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fVehicleSpeed = 45.5
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF

	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].objID = ENUM_TO_INT(LV_RIVAL_02)
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].iXOffsetFromCenter = (leftOffset-250) // PLaying around with offsets
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZDrawAt = 5785.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZWasDrawnAt = 0.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bBeenHit = FALSE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bBeenPassed = FALSE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bWasDrawn = FALSE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bIsMoving = FALSE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fSwerveSpeed = 1100.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].bCrashed = FALSE
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].currentSide = SIDE_LEFT
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fVehicleSpeed = 90.0
		rivalVehicles[ENUM_TO_INT(LV_RIVAL_02)].fZSinceChange = 0
	ENDIF
		
ENDPROC

// Las Venturas rivals
ENUM RIVAL_VEHICLE_ID_LIBERTY

	LB_RIVAL_02,
	LB_RIVAL_01
	
ENDENUM

PROC LIBERTY_CITY_RIVAL_INIT()

	INT leftOffset = rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].iLeftOffset
	INT rightOffset = rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].iRightOffset

	UNUSED_PARAMETER(rightOffset)

	// Rival 01 - Liberty rival 1
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].objID = ENUM_TO_INT(LB_RIVAL_01)
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].iXOffsetFromCenter = leftOffset
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)

	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fZDrawAt = 3450.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fSwerveSpeed = 500.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].currentSide = SIDE_LEFT
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fVehicleSpeed = 55.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fZDrawAt = 3800.0
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fVehicleSpeed = 35.5
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fZDrawAt = 3060.0
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_01)].fVehicleSpeed = 70.0
	ENDIF
	
	
	// Rival 02 - LIBERTY rival 2
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].objID = ENUM_TO_INT(LB_RIVAL_02)
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].iXOffsetFromCenter = leftOffset
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].vSpriteScale = INIT_VECTOR_2D(352.0, 256.0)

	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fZDrawAt = 5200.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fZWasDrawnAt = 0.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].bBeenHit = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].bBeenPassed = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].bWasDrawn = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].bIsMoving = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fSwerveSpeed = 1200.0
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].bCrashed = FALSE
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].currentSide = SIDE_RIGHT
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fVehicleSpeed = 81.5
	rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fZSinceChange = 0
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fZDrawAt = 4800.0
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fVehicleSpeed = 40.5
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].vSpriteScale = INIT_VECTOR_2D(384.0, 280.0)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fZDrawAt = 5550.0
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fVehicleSpeed = 100.0
		rivalVehicles[ENUM_TO_INT(LB_RIVAL_02)].fSwerveSpeed = 1500.0
	ENDIF
		
ENDPROC

FUNC INT ROAD_ARCADE_GET_RIVAL_COUNT(STAGES_SETS stage)

	IF stage = STAGE_VICE_CITY
		return (COUNT_OF(RIVAL_VEHICLE_ID_VICE)-1)
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		return (COUNT_OF(RIVAL_VEHICLE_ID_FIERRO)-1)
	ENDIF
	
	IF stage = STAGE_SAN_ANDREAS
		return (COUNT_OF(RIVAL_VEHICLE_ID_ANDREAS)-1)
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		return (COUNT_OF(RIVAL_VEHICLE_ID_VENTURAS)-1)
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		return (COUNT_OF(RIVAL_VEHICLE_ID_LIBERTY)-1)
	ENDIF
	
	RETURN 0 // Adjusted by one for arrays

ENDFUNC

PROC ROAD_ARCADE_INIT_RIVAL_VEH(STAGES_SETS stage)

	RIVAL_VEHICLE_OBJECT empty[iROAD_ARCADE_MaxRivalVehicles]
	int i
	FOR i = 0 TO iROAD_ARCADE_MaxRivalVehicles-1
		rivalVehicles[i] = empty[i]
	ENDFOR

	IF stage = STAGE_VICE_CITY
		VICE_CITY_RIVAL_INIT()
	ENDIF

	IF stage = STAGE_SAN_FIERRO
		SAN_FIERRO_RIVAL_INIT()
	ENDIF
	
	IF stage = STAGE_SAN_ANDREAS
		SAN_ANDREAS_RIVAL_INIT()
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		LAS_VENTURAS_RIVAL_INIT()
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		LIBERTY_CITY_RIVAL_INIT()
	ENDIF
	
ENDPROC

FUNC STRING RA_RIVAL_ANIMATION_UPDATE(RIVAL_VEHICLE_OBJECT &rival, FLOAT fScreenOffset)

	string sNeutral = "VEHICLE_CAR_02_FRAME_01_UP_HILL"
	string sSlightT = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
	string sMidT = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
	string sFarT = "VEHICLE_CAR_02_FRAME_04_UP_HILL"

	STRING returnStr = sNeutral
	
	INT iFinalOffset = ROUND(ABSF(fScreenOffset))
	
	IF iFinalOffset > 250
		returnStr = sSlightT
	ENDIF

	IF iFinalOffset > 500
		returnStr = sMidT
	ENDIF
	
	IF iFinalOffset > 700
		returnStr = sFarT
	ENDIF
	
	IF rival.bIsMoving
	
		IF ARE_STRINGS_EQUAL(returnStr, sSlightT)
			RETURN sMidT
		ENDIF
	
		IF ARE_STRINGS_EQUAL(returnStr, sMidT)
			RETURN sFarT
		ENDIF
			
	ENDIF
	
	RETURN returnStr

ENDFUNC

PROC ROAD_ARCADE_UPDATE_RIVAL_MOVEMENT_SIDE(RIVAL_VEHICLE_OBJECT &rival)

	FLOAT fCurrentZValue = rival.fZDrawAt

	IF rival.fZSinceChange = 0
		rival.fZSinceChange = fCurrentZValue
	ENDIF

	// Rival is moving
	IF rival.bIsMoving
		rival.fZSinceChange = fCurrentZValue
	ENDIF

	IF ABSF(rival.fZSinceChange - fCurrentZValue) >= rival.fSideChange
	
		SIDELINE_SIDES side = rival.currentSide
		
		SWITCH (side)
	
			CASE SIDE_LEFT
				rival.currentSide = SIDE_RIGHT
			BREAK
		
			CASE SIDE_RIGHT
				rival.currentSide = SIDE_LEFT
			BREAK
			
		ENDSWITCH
		
		rival.fZSinceChange = fCurrentZValue
			
	ENDIF

ENDPROC

PROC ROAD_ARCADE_UPDATE_RIVAL_MOVEMENT(RIVAL_VEHICLE_OBJECT &rival, FLOAT fCurrentDistanceScale)

	ROAD_ARCADE_UPDATE_RIVAL_MOVEMENT_SIDE(rival)

	SIDELINE_SIDES side = rival.currentSide
	
	rival.bIsMoving = FALSE
	
	SWITCH (side)
	
		CASE SIDE_LEFT
		
			IF (rival.iXOffsetFromCenter > rival.iLeftOffset*fCurrentDistanceScale)
			
				rival.iXOffsetFromCenter -= CEIL(0.0+@(rival.fSwerveSpeed*fCurrentDistanceScale))
				rival.bIsMoving = TRUE
				
			ENDIF
		
		BREAK
		
		CASE SIDE_RIGHT
		
			IF (rival.iXOffsetFromCenter < rival.iRightOffset*fCurrentDistanceScale)
			
				rival.iXOffsetFromCenter += CEIL(0.0+@(rival.fSwerveSpeed*fCurrentDistanceScale))
				rival.bIsMoving = TRUE
			
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC ROAD_ARCADE_DRAW_RIVAL_VEH(PLAYER_VEHICLE &player, ROAD_ARCADE_Z_BUFFER &zBuffer, STAGES_SETS currentStage, INT iObjectCount)
		
	// Don't process if the Z value is out of range
	IF rivalVehicles[iObjectCount].fZDrawAt > fFinalZValue
		EXIT
	ENDIF
		
	// Do not draw the overroad vehicles in this state
	IF transitionData.transition_ActiveState = T_LOAD
	OR transitionData.transition_ActiveState = T_WAIT_FOR_RETURN
		EXIT
	ENDIF
	
	// Init the counters to the updated starting Z value
	FLOAT fROAD_VEHNextZPosition = fJOURNEY_DistanceTravelled
		
	UNUSED_PARAMETER(player)
	
	INT iVEHLineHorizon = round(ciROAD_HORIZON_POINT * 1.0)
	INT iROAD_VEHInstanceCounter = 0
	
	IF rivalVehicles[iObjectCount].objID = -1
		EXIT
	ENDIF
	
	// Don't draw if they are taken out
	IF rivalVehicles[iObjectCount].bBeenHit
		EXIT
	ENDIF
	
	// Don't draw if they are taken out
	IF rivalVehicles[iObjectCount].bBeenPassed
		EXIT
	ENDIF
		
	IF rivalVehicles[iObjectCount].fZDrawAt != 0
	AND (player.fZvalue-10.0) >= rivalVehicles[iObjectCount].fZDrawAt
		IF !rivalVehicles[iObjectCount].bBeenPassed
			rivalVehicles[iObjectCount].bBeenPassed = TRUE
			
			// Only reward if you are in teh main game state!
			IF transitionData.transition_ActiveState = T_INACTIVE
				player.iPlayerScore += (1000*player.iSpeedMultiplier)
				ROAD_ARCADE_INIT_TICKER_OBJ(sDICT_HUDGeneric, "HUD_BUTTON_RIVAL_PASSED", INIT_VECTOR_2D(356, 64))
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TOOK_LEAD)
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	// Was the vehicle drawn this frame?
	BOOL bVehDrawnThisFrame = FALSE
		
	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
		
		FLOAT fROAD_VEHZValue =  fROAD_VEHNextZPosition
				
		FLOAT fROAD_VEHDistanceScale = ((fROAD_VEHZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-iVEHLineHorizon)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
			
		VECTOR_2D vSpritePos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_VEHInstanceCounter)									
				
		iROAD_VEHInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
							
		// Stop the loop if we hit the limits of the screen
		IF cfBASE_SCREEN_HEIGHT - iROAD_VEHInstanceCounter < iVEHLineHorizon							
			BREAKLOOP
		ENDIF
		
		// Local offset mod
		FLOAT yOffset = rivalVehicles[iObjectCount].fYOffset
															
		// Does the sprites position into the screen exceed the pattern transition threshold?		
		IF fROAD_VEHZValue >= rivalVehicles[iObjectCount].fZDrawAt
											
			// Object drawing positions
			VECTOR_2D vObjectScale = rivalVehicles[iObjectCount].vSpriteScale
			
			vObjectScale = ROAD_ARCADE_GET_FINAL_SCALE(fROAD_VEHDistanceScale, vObjectScale)
			vObjectScale = DIVIDE_VECTOR_2D(vObjectScale, 2.0) // Double scaling defintiely doesn't work in this case!!
			FLOAT fROAD_VEHDistanceScaleInverse = ROAD_ARCADE_GET_INVERTED_SCALE(fROAD_VEHDistanceScale)
			
			// Change what side the Vehicle should go to.
			ROAD_ARCADE_UPDATE_RIVAL_MOVEMENT(rivalVehicles[iObjectCount], fROAD_VEHDistanceScaleInverse)
						
			FLOAT yRelativeScaling = ((vSpritePos.Y - ciROAD_HORIZON_POINT)/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))
			
			FLOAT fSideLine1 = (fXCarOffset*(yRelativeScaling))
			fSideLine1 += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_VEHZValue, iROAD_VEHInstanceCounter, fROAD_VEHDistanceScale)
			fSideLine1 += (rivalVehicles[iObjectCount].iXOffsetFromCenter*(yRelativeScaling))
			fSideLine1 *= (1.0-cfROAD_HillScale)	
			fSideLine1 += vSpritePos.X
			
			// Object drawing positions
			VECTOR_2D vObjectPos = INIT_VECTOR_2D(fSideLine1, vSpritePos.y)
			
			// Origin offset
			vObjectPos.y  -= (vObjectScale.y/2.0)*(1.0-yOffset)
						
						
			IF i > 2 // 2nd count will always produce a sprite regardless of progress
	
				ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(zBuffer, 
					sDICT_RivalVehDistant,
					ROAD_ARCADE_GET_RIVAL_TEXTURE_FROM_PLAYER_ID(iPlayerSeatLocalID),
					vObjectPos,
					vObjectScale,
					rgbaOriginal,
					0.0,
					fROAD_VEHZValue)
					
				bVehDrawnThisFrame = TRUE
					
				IF !rivalVehicles[iObjectCount].bWasDrawn
					rivalVehicles[iObjectCount].bWasDrawn = TRUE
				ENDIF
				
				bVehDrawnThisFrame = TRUE
								
				IF rivalVehicles[iObjectCount].fZWasDrawnAt = 0
					ROAD_ARCADE_INIT_WARNING_OBJ(sDICT_HUDGeneric, "HUD_BUTTON_RIVAL_AHEAD", INIT_VECTOR_2D(328, 64))
				ENDIF
							
				fROAD_VEHNextZPosition += (iROAD_VEHInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_VEHScaleMod+(fROAD_VEHDistanceScale*2))
				rivalVehicles[iObjectCount].fZWasDrawnAt = fROAD_VEHZValue
												
				IF rivalVehicles[iObjectCount].fZDrawAt <= player.fZvalue + 1.5
					
					IF vObjectPos.x <= ((cfBASE_SCREEN_WIDTH/2.0) + ABSF(vObjectScale.x/4.0)) // Player would have knocked the objects.
					AND vObjectPos.x >= ((cfBASE_SCREEN_WIDTH/2.0) - ABSF(vObjectScale.x/4.0))
				
						// Trigger car swerve / crash
						RA_TRIGGER_PLAYER_CRASH(player)
						
						CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_RIVAL_VEH - Crashed into Object with ID no: ", iObjectCount)
								
						rivalVehicles[iObjectCount].bBeenHit = TRUE
						rivalVehicles[iObjectCount].bCrashed = TRUE
					ENDIF
					
					// Player is skimming the vehicle this frame
					IF ABSF(vObjectPos.x - (cfBASE_SCREEN_WIDTH/2.0)) < ABSF(vObjectScale.x/3.0)
						CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_GENERIC_VEH - Skimming a vehicle this frame.")
						player.iPlayerScore += ROUND(0+@(500 * player.iSpeedMultiplier))
					ENDIF
					
				ENDIF
				
				// Player is behind the car. Active slipstreaming.
				IF rivalVehicles[iObjectCount].fZDrawAt <= player.fZvalue + 70
					IF vObjectPos.x <= ((cfBASE_SCREEN_WIDTH/2.0) + ABSF(vObjectScale.x/2.5)) // Player would have knocked the objects.
					AND vObjectPos.x >= ((cfBASE_SCREEN_WIDTH/2.0) - ABSF(vObjectScale.x/2.5))
						player.bWillSlipStream = TRUE
					ENDIF
				ENDIF
												
				BREAKLOOP

			ENDIF
			
		ENDIF
						
		fROAD_VEHNextZPosition += (iROAD_VEHInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_VEHScaleMod+(fROAD_VEHDistanceScale*2))	
	ENDFOR
	
	// Add to the Vehicles Z Value according to speed		
	
	IF bVehDrawnThisFrame
	AND transitionData.transition_ActiveState = T_INACTIVE
		rivalVehicles[iObjectCount].fZDrawAt += 0+@(rivalVehicles[iObjectCount].fVehicleSpeed)		
	ENDIF
		
ENDPROC

PROC ROAD_ARCADE_DRAW_RIVAL_NET_VEH(RIVAL_PLAYER_VEHICLE &rival, ROAD_ARCADE_Z_BUFFER &zBuffer, STAGES_SETS currentStage)
	
	IF roadArcadePlayerBd[0].sCurrentStage != roadArcadePlayerBd[1].sCurrentStage
		EXIT
	ENDIF
	
	// Don't process if the Z value is out of range
	IF fFinalZValue > 0
		IF rival.fZDrawAt > fFinalZValue
			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_RIVAL_NET_VEH - Z value off horizon!")
			EXIT
		ENDIF
	ENDIF
		
	IF rival.fZDrawAt < fJOURNEY_DistanceTravelled + 5
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_RIVAL_NET_VEH - Z value behind player")
		EXIT
	ENDIF
	
	// Init the counters to the updated starting Z value
	FLOAT fROAD_VEHNextZPosition = fJOURNEY_DistanceTravelled
		
	INT iVEHLineHorizon = round(ciROAD_HORIZON_POINT * 1.0)
	INT iROAD_VEHInstanceCounter = 0

	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
		
		FLOAT fROAD_VEHZValue =  fROAD_VEHNextZPosition
				
		FLOAT fROAD_VEHDistanceScale = ((fROAD_VEHZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-iVEHLineHorizon)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
			
		VECTOR_2D vSpritePos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_VEHInstanceCounter)									
				
		iROAD_VEHInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
							
		// Stop the loop if we hit the limits of the screen
		IF cfBASE_SCREEN_HEIGHT - iROAD_VEHInstanceCounter < iVEHLineHorizon							
			BREAKLOOP
		ENDIF
								
		// Does the sprites position into the screen exceed the pattern transition threshold?		
		IF fROAD_VEHZValue >= rival.fZDrawAt
											
			// Object drawing positions
			VECTOR_2D vObjectScale = INIT_VECTOR_2D(352, 256)
			
			// Size up if a Truck
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				vObjectScale = INIT_VECTOR_2D(384.0, 280.0)
			ENDIF
			
			vObjectScale = ROAD_ARCADE_GET_FINAL_SCALE(fROAD_VEHDistanceScale, vObjectScale)
			vObjectScale = DIVIDE_VECTOR_2D(vObjectScale, 2.0) // Double scaling defintiely doesn't work in this case!!
//			FLOAT fROAD_VEHDistanceScaleInverse = ROAD_ARCADE_GET_INVERTED_SCALE(fROAD_VEHDistanceScale)
			
			FLOAT yRelativeScaling = ((vSpritePos.Y - ciROAD_HORIZON_POINT)/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))
			
			FLOAT fSideLine1 = (fXCarOffset*(yRelativeScaling))
			//fSideLine1 += SCALING_ROAD_CURVATURE(currentStage, fROAD_VEHZValue)*(2.0-fROAD_VEHDistanceScale)
			fSideLine1 += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_VEHZValue, iROAD_VEHInstanceCounter, fROAD_VEHDistanceScale)
			fSideLine1 *= (1.0-cfROAD_HillScale)	
			fSideLine1 += vSpritePos.X
			
			// Object drawing positions
			VECTOR_2D vObjectPos = INIT_VECTOR_2D(fSideLine1, vSpritePos.y)
			
			// Origin offset
			vObjectPos.y  -= (vObjectScale.y/2.0)*(1.0-rival.fYOffset)
		
			IF i > 2 // 2nd count will always produce a sprite regardless of progress
	
				IF rival.bDrawThisFrame
					ROAD_ARCADE_Z_BUFFER_ADD_SPRITE_TO_ARRAY(zBuffer, 
						sDICT_RivalVehDistant,
						ROAD_ARCADE_GET_RIVAL_TEXTURE_FROM_PLAYER_ID(iPlayerSeatLocalID),
						vObjectPos,
						vObjectScale,
						rgbaOriginal,
						0.0,
						fROAD_VEHZValue)
						
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_RIVAL_NET_VEH - Veh X: ", vObjectPos.X, " Veh y: ", vObjectPos.Y)
					CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_RIVAL_NET_VEH - Scale X: ", vObjectScale.X, " Scale y: ", vObjectScale.Y)
				ENDIF
				
				// Reverse the bool
				rival.bDrawThisFrame = !rival.bDrawThisFrame
									
				BREAKLOOP

			ENDIF
			
		ENDIF
						
		fROAD_VEHNextZPosition += (iROAD_VEHInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_VEHScaleMod+(fROAD_VEHDistanceScale*2))	
	ENDFOR
				
ENDPROC

// Draw any overroad objects in Z Order
PROC ROAD_ARCADE_DRAW_OVERROAD_OBJS(PLAYER_VEHICLE &pvPlayer, RIVAL_PLAYER_VEHICLE &rival, STAGES_SETS stgCurrentStage)
	
	ROAD_ARCADE_Z_BUFFER roadArcade_ZOverBuffer
	ROAD_ARCADE_Z_BUFFER_INIT(roadArcade_ZOverBuffer)
			
	// Draw Crossing objects on the road
	INT objectCount = ROAD_ARCADE_GET_CROSSING_OBJ_COUNT(stgCurrentStage)
	INT i = 0
	FOR i = 0 TO objectCount
		ROAD_ARCADE_DRAW_CROSSING_OBJECTS(pvPlayer, roadArcade_ZOverBuffer, stgCurrentStage, i)
	ENDFOR
	
	// Draw generic Vehicles
	objectCount = ROAD_ARCADE_GET_VEH_COUNT(stgCurrentStage)
	i = 0
	FOR i = 0 TO objectCount
		ROAD_ARCADE_DRAW_GENERIC_VEH(pvPlayer, roadArcade_ZOverBuffer, stgCurrentStage, i)
	ENDFOR
	
	// Draw rival Vehicles
	objectCount = ROAD_ARCADE_GET_RIVAL_COUNT(stgCurrentStage)
	i = 0
	FOR i = 0 TO objectCount
		ROAD_ARCADE_DRAW_RIVAL_VEH(pvPlayer, roadArcade_ZOverBuffer, stgCurrentStage, i)
	ENDFOR
	
	// Draw objects over the road
	objectCount = COUNT_OF(OVER_OBJECTS_GEN_ID) - 1
	
	i = 0
	FOR i = 0 TO objectCount
		ROAD_ARCADE_DRAW_GENERIC_OVERROAD_OBJECTS(pvPlayer, roadArcade_ZOverBuffer, stgCurrentStage, i)			
	ENDFOR

	// Only display other player progress in MP mode naturally.
	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
		ROAD_ARCADE_DRAW_RIVAL_NET_VEH(rival, roadArcade_ZOverBuffer, stgCurrentStage)
	ENDIF
	
	ROAD_ARCADE_Z_BUFFER_RENDER(roadArcade_ZOverBuffer)

ENDPROC

// ---------------------- ROAD RENDERING ---------------------
FUNC RGBA_COLOUR_STRUCT ROAD_ARCADE_GET_ROADSIDE_COLOUR_BY_STAGE(STAGES_SETS currentStage)

	IF currentStage = STAGE_VICE_CITY
		RETURN rgbaVicePurple
	ENDIF

	IF currentStage = STAGE_SAN_FIERRO
		RETURN rgbaFeirroRed
	ENDIF

	IF currentStage = STAGE_SAN_ANDREAS
		RETURN rgbaAndreasOrange
	ENDIF

	IF currentStage = STAGE_LAS_VENTURAS
		RETURN rgbaVenturasBlue
	ENDIF

	IF currentStage = STAGE_LIBERTY_CITY
		RETURN rgbaLibertyTaxi
	ENDIF

	RETURN rgbaOriginal

ENDFUNC

FUNC RGBA_COLOUR_STRUCT ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(RGBA_COLOUR_STRUCT rgbaFrom, RGBA_COLOUR_STRUCT rgbaTo)
	
	RGBA_COLOUR_STRUCT interpColour

	interpColour.iR = LERP_INT(rgbaFrom.iR, rgbaTo.iR, transitionData.fTransitionProgress)
	interpColour.iG = LERP_INT(rgbaFrom.iG, rgbaTo.iG, transitionData.fTransitionProgress)
	interpColour.iB = LERP_INT(rgbaFrom.iB, rgbaTo.iB, transitionData.fTransitionProgress)
	interpColour.iA = 255

	RETURN interpColour

ENDFUNC

FUNC RGBA_COLOUR_STRUCT ROAD_ARCADE_HANDLE_ROADSIDE_SWITCH(STAGES_SETS fromStage, STAGES_SETS toStage)

	RGBA_COLOUR_STRUCT firstRoadSide = ROAD_ARCADE_GET_ROADSIDE_COLOUR_BY_STAGE(fromStage)
	RGBA_COLOUR_STRUCT toRoadSide = ROAD_ARCADE_GET_ROADSIDE_COLOUR_BY_STAGE(toStage)
	
	RETURN ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(firstRoadSide, toRoadSide)
ENDFUNC

// CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "SCALING_ROAD_DRAW -
PROC ROAD_ARCADE_SCALING_ROAD_DRAW(PLAYER_VEHICLE &player, STAGES_SETS currentStage, STAGES_SETS nextStage, BOOL isTitleVersion = FALSE) 
//	UNUSED_PARAMETER(cfJOURNEY_FinishLine)
//	UNUSED_PARAMETER(fFinalZValue)
		
	RGBA_COLOUR_STRUCT rgbaGround
	INIT_RGBA_STRUCT(rgbaGround, 219, 202, 186, 255)
		
	INT iROAD_RoadSpriteInstanceCounter = 0

	// Init the counters to the updated starting Z value
	FLOAT fROAD_PatternThreshold = fJOURNEY_DistanceTravelled
	FLOAT fROAD_RoadSpriteNextZPosition = fJOURNEY_DistanceTravelled
	
	IF fJOURNEY_DistanceTravelled%(cfROAD_SEGMENT_LENGTH*2) > cfROAD_SEGMENT_LENGTH
		sROAD_PATTERN = sROAD_NoLineSegment
	ELSE
		sROAD_PATTERN = sROAD_LineSegment
	ENDIF
	
	// Resetting the Player's stored ZValue
	player.fZValue = 0
	player.iSpriteInstance = 0
	
	RGBA_COLOUR_STRUCT colourStruct
		
	IF transitionData.transition_ActiveState != T_WAIT_FOR_BACKGROUND
	AND transitionData.transition_ActiveState != T_LOAD
		colourStruct = ROAD_ARCADE_GET_ROADSIDE_COLOUR_BY_STAGE(currentStage)
	ELSE
		colourStruct = ROAD_ARCADE_HANDLE_ROADSIDE_SWITCH(currentStage, nextStage)
	ENDIF	
		
	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
	
		FLOAT fROAD_SpriteZValue =  fROAD_RoadSpriteNextZPosition
		FLOAT fROAD_SpriteDistanceScale = ((fROAD_SpriteZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
		FLOAT fROAD_SpriteThresholdRemainder = cfROAD_SEGMENT_LENGTH-(fROAD_SpriteZValue%cfROAD_SEGMENT_LENGTH)
		FLOAT fROAD_SpriteNextThreshold = fROAD_SpriteZValue + fROAD_SpriteThresholdRemainder
	
		VECTOR_2D vSpritePos = INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0),(cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_RoadSpriteInstanceCounter)									
		VECTOR_2D vSpriteScale = INIT_VECTOR_2D(ciROAD_DEFAULT_SPRITE_WIDTH*(1-(iROAD_RoadSpriteInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*cfROAD_HorizonScale),TO_FLOAT(ciROAD_DEFAULT_SPRITE_HEIGHT))
		
		IF !isTitleVersion
			// Set the Hill modifier
			vSpriteScale.x *= (POW(1.0-cfROAD_HillScale, 1.5))-(fROAD_SpriteDistanceScale*(cfROAD_HillScale*fROAD_RoadWidthMultiplier))
			vSpritePos.x += (fXCarOffset*(1.0-(iROAD_RoadSpriteInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))))
			
			// Adjust the X value according to the Curve
			//vSpritePos.x += SCALING_ROAD_CURVATURE(currentStage, fROAD_SpriteZValue)*((iROAD_RoadSpriteInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(1.0-(fROAD_SpriteDistanceScale)))
			vSpritePos.x += ROAD_ARCADE_GET_ROAD_CURVE_OFFSET(currentStage, fROAD_SpriteZValue, iROAD_RoadSpriteInstanceCounter, fROAD_SpriteDistanceScale)
		ENDIF
					
		// Store the Z value and Instance count that the player is drawn around.
		IF vSpritePos.y <= player.iPlayerPosY
			IF player.fZValue = 0 // Only at 0
				player.fZValue = fROAD_SpriteZValue
				player.iSpriteInstance = iROAD_RoadSpriteInstanceCounter
				
				player.fRoadCenter = vSpritePos.x
			ENDIF
		ENDIF
						
		VECTOR_2D vSpritePosSCREEN = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vSpritePos)
		VECTOR_2D vSpriteScaleSCREEN = ARCADE_CABINET_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(vSpriteScale)						
							
		// Alternate the patterns
		IF ARE_STRINGS_EQUAL(sROAD_PATTERN, sROAD_NoLineSegment)
			INIT_RGBA_STRUCT(rgbaGround, 235, 222, 206, 255)
		ELIF ARE_STRINGS_EQUAL(sROAD_PATTERN, sROAD_LineSegment)
			INIT_RGBA_STRUCT(rgbaGround, 219, 202, 186, 255)
		ENDIF
		
		IF vSpritePos.y < 1008.0
			ARCADE_CABINET_DRAW_SPRITE(sDICT_GenericAssets, sROAD_PATTERN,
				vSpritePosSCREEN.X,
				vSpritePosSCREEN.Y,
				vSpriteScaleSCREEN.X,
				vSpriteScaleSCREEN.Y, 0.0, rgbaOriginal)
						
			// Side of road colours
			IF ciROAD_HORIZON_POINT >= ciROAD_HORIZON_POINT_BASE // Don't draw an overlay if the road is too tall.
				ARCADE_CABINET_DRAW_SPRITE(sDICT_GenericAssets, "RoadSection_ColorOver",
					vSpritePosSCREEN.X,
					vSpritePosSCREEN.Y,
					vSpriteScaleSCREEN.X,
					vSpriteScaleSCREEN.Y, 0.0, colourStruct)
			ENDIF
		ENDIF
				
		iROAD_RoadSpriteInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
			
		fROAD_RoadSpriteNextZPosition += (iROAD_RoadSpriteInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_PatternScaleMod+(fROAD_SpriteDistanceScale*2))
	
		// Does the sprites position into the screen exceed the pattern transition threshold?
		IF fROAD_SpriteZValue > fROAD_PatternThreshold
			
			IF ARE_STRINGS_EQUAL(sROAD_PATTERN,sROAD_NoLineSegment)
				sROAD_PATTERN = sROAD_LineSegment
			ELIF ARE_STRINGS_EQUAL(sROAD_PATTERN, sROAD_LineSegment)
				sROAD_PATTERN = sROAD_NoLineSegment
			ENDIF
			
			fROAD_PatternThreshold = fROAD_SpriteNextThreshold
		
		ENDIF
						
		// Stop the loop if we hit the limits of the screen
		IF ciROAD_HORIZON_POINT >= cfBASE_SCREEN_HEIGHT - iROAD_RoadSpriteInstanceCounter
			fFinalZValue = fROAD_SpriteZValue
			BREAKLOOP
		ENDIF
		
	ENDFOR

ENDPROC

PROC ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(STAGES_SETS currentStage, RGBA_COLOUR_STRUCT &colourOne, RGBA_COLOUR_STRUCT &colourTwo)

	IF currentStage = STAGE_VICE_CITY
		
		INIT_RGBA_STRUCT(colourOne, 191, 255, 183, 255)
		INIT_RGBA_STRUCT(colourTwo, 133, 183, 124, 255)
		
	ENDIF

	IF currentStage = STAGE_SAN_FIERRO
		
		INIT_RGBA_STRUCT(colourOne, 235, 222, 206, 255)
		INIT_RGBA_STRUCT(colourTwo, 219, 202, 186, 255)
		
	ENDIF

	IF currentStage = STAGE_SAN_ANDREAS
		
		INIT_RGBA_STRUCT(colourOne, 165, 137, 111, 255)
		INIT_RGBA_STRUCT(colourTwo, 158, 101, 45, 255)
		
	ENDIF

	IF currentStage = STAGE_LAS_VENTURAS
		
		INIT_RGBA_STRUCT(colourOne, 129, 102, 95, 255)
		INIT_RGBA_STRUCT(colourTwo, 92, 92, 110, 255)
		
	ENDIF

	IF currentStage = STAGE_LIBERTY_CITY
		
		INIT_RGBA_STRUCT(colourOne, 211, 188, 124, 255)
		INIT_RGBA_STRUCT(colourTwo, 143, 134, 108, 255)
		
	ENDIF

ENDPROC

// CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "SCALING_ROAD_DRAW -
PROC ROAD_ARCADE_SCALING_GROUND_DRAW(STAGES_SETS currentStage, BOOL bTitleVersion = FALSE) 
//	UNUSED_PARAMETER(cfJOURNEY_FinishLine)
//	UNUSED_PARAMETER(fFinalZValue)
	
	IF !bTitleVersion
		IF ciROAD_HORIZON_POINT < ciROAD_HORIZON_POINT_BASE
			RGBA_COLOUR_STRUCT fierroColour
			INIT_RGBA_STRUCT(fierroColour, 219, 202, 186, 255)
			ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT-((cfBASE_SCREEN_HEIGHT - TO_FLOAT(ciROAD_HORIZON_POINT))/2.0)), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT), fierroColour)
			EXIT
		ENDIF
	ENDIF
	
	INT iROAD_RoadSpriteInstanceCounter = 0
	
	// Basic colour start 
	RGBA_COLOUR_STRUCT groundColourOne
	RGBA_COLOUR_STRUCT groundColourTwo
	
	IF transitionData.transition_ActiveState != T_WAIT_FOR_BACKGROUND
	AND transitionData.transition_ActiveState != T_LOAD
		ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(currentStage, groundColourOne, groundColourTwo)
	
	ELSE // We are transitioning between stages
		
		// Switching colours
		RGBA_COLOUR_STRUCT fromColourOne
		RGBA_COLOUR_STRUCT toColourOne
		
		RGBA_COLOUR_STRUCT fromColourTwo
		RGBA_COLOUR_STRUCT toColourTwo
		
		IF currentStage = STAGE_VICE_CITY
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(currentStage, fromColourOne, fromColourTwo)
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(STAGE_SAN_FIERRO, toColourOne, toColourTwo)
		ENDIF
		
		IF currentStage = STAGE_SAN_FIERRO
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(currentStage, fromColourOne, fromColourTwo)
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(STAGE_SAN_ANDREAS, toColourOne, toColourTwo)
		ENDIF
		
		IF currentStage = STAGE_SAN_ANDREAS
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(currentStage, fromColourOne, fromColourTwo)
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(STAGE_LAS_VENTURAS, toColourOne, toColourTwo)
		ENDIF
		
		IF currentStage = STAGE_LAS_VENTURAS
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(currentStage, fromColourOne, fromColourTwo)
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(STAGE_LIBERTY_CITY, toColourOne, toColourTwo)
		ENDIF
		
		IF currentStage = STAGE_LIBERTY_CITY
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(currentStage, fromColourOne, fromColourTwo)
			ROAD_ARCADE_GET_GROUND_COLOUR_BY_STAGE(STAGE_VICE_CITY, toColourOne, toColourTwo)
		ENDIF
		
		groundColourOne = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(fromColourOne, toColourOne)
		groundColourTwo = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(fromColourTwo, toColourTwo)
		
	ENDIF	
	
	// Init the counters to the updated starting Z value
	FLOAT fROAD_PatternThreshold = fJOURNEY_DistanceTravelled
	FLOAT fROAD_RoadSpriteNextZPosition = fJOURNEY_DistanceTravelled
		
	IF fJOURNEY_DistanceTravelled%(cfROAD_SEGMENT_LENGTH*2) > cfROAD_SEGMENT_LENGTH
		sGROUND_PATTERN = sROAD_NoLineSegment
	ELSE
		sGROUND_PATTERN = sROAD_LineSegment
	ENDIF
	
	INT i
	FOR i = 1 TO (ROUND(cfBASE_SCREEN_HEIGHT)-ciROAD_HORIZON_POINT)
	
		FLOAT fROAD_SpriteZValue =  fROAD_RoadSpriteNextZPosition
		FLOAT fROAD_SpriteDistanceScale = ((fROAD_SpriteZValue-fJOURNEY_DistanceTravelled)/((cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT)/(2*ciROAD_DEFAULT_SPRITE_HEIGHT)))
		//FLOAT fROAD_SpriteDistanceScaleFixed = fROAD_SpriteDistanceScale/2
		FLOAT fROAD_SpriteThresholdRemainder = cfROAD_SEGMENT_LENGTH-(fROAD_SpriteZValue%cfROAD_SEGMENT_LENGTH)
		FLOAT fROAD_SpriteNextThreshold = fROAD_SpriteZValue + fROAD_SpriteThresholdRemainder
	
		FLOAT fSpritePosY = (cfBASE_SCREEN_HEIGHT+ciROAD_DEFAULT_SPRITE_HEIGHT)-iROAD_RoadSpriteInstanceCounter
		
			
		// Alternate the patterns
		IF fSpritePosY < 1008.0
			IF ARE_STRINGS_EQUAL(sGROUND_PATTERN, sROAD_NoLineSegment)
				ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, fSpritePosY), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, TO_FLOAT(ciROAD_DEFAULT_SPRITE_HEIGHT)), groundColourOne)
			ELIF ARE_STRINGS_EQUAL(sGROUND_PATTERN, sROAD_LineSegment)
				ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, fSpritePosY), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, TO_FLOAT(ciROAD_DEFAULT_SPRITE_HEIGHT)), groundColourTwo)
			ENDIF
		ENDIF

			
		iROAD_RoadSpriteInstanceCounter += ciROAD_DEFAULT_SPRITE_HEIGHT
				
		fROAD_RoadSpriteNextZPosition += (iROAD_RoadSpriteInstanceCounter/(cfBASE_SCREEN_HEIGHT-ciROAD_HORIZON_POINT))*(cfROAD_PatternScaleMod+(fROAD_SpriteDistanceScale*2))
	
		// Does the sprites position into the screen exceed the pattern transition threshold?
		IF fROAD_SpriteZValue > fROAD_PatternThreshold
			
			IF ARE_STRINGS_EQUAL(sGROUND_PATTERN,sROAD_NoLineSegment)
				sGROUND_PATTERN = sROAD_LineSegment
			ELIF ARE_STRINGS_EQUAL(sGROUND_PATTERN, sROAD_LineSegment)
				sGROUND_PATTERN = sROAD_NoLineSegment
			ENDIF
			
			fROAD_PatternThreshold = fROAD_SpriteNextThreshold
		
		ENDIF
						
		// Stop the loop if we hit the limits of the screen
		IF ciROAD_HORIZON_POINT >= cfBASE_SCREEN_HEIGHT - iROAD_RoadSpriteInstanceCounter
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
ENDPROC

//---------BACKGROUNDS-------
// Logic borrowed and modified from Last Gunslinger - Guile Diaz
STRUCT ROAD_ARCADE_BACKGROUND_SPRITE
		
	FLOAT fYposition
	VECTOR_2D vSpriteSize
	
	STRING sDict
	STRING sSprite

	RGBA_COLOUR_STRUCT rgba
	
	FLOAT fScrollOffsetScale
	FLOAT fOffsetXPos = 0.0
	
	// Store total length of background and tiles here.
	FLOAT fBackgroundLength 
	INT iNumberOfTiles
	
	FLOAT fAnimTimer = 0.0
	FLOAT fAnimSpeed = 100.0
	
ENDSTRUCT

CONST_INT ciBackGroundSpriteLayersMax 5

// Background tweaks
TWEAK_FLOAT cfSkyXpos  0.0
TWEAK_FLOAT cfSkyYpos 0.0

TWEAK_FLOAT cfTerrainXpos 0.0
TWEAK_FLOAT cfTerrainYpos 88.0

TWEAK_FLOAT cfCityXpos 0.0
TWEAK_FLOAT cfCityYpos 167.0


ROAD_ARCADE_BACKGROUND_SPRITE rabsActiveBackground[ciBackGroundSpriteLayersMax]

// Enum descriptions of each background layer in each stage
ENUM VICE_BG_LAYERS
	VC_SKY,
	VC_SUN,
	VC_CITY_FAR,
	VC_BLIMP
	//GROUND,
ENDENUM

// Enum descriptions of each background layer in each stage
ENUM FIERRO_BG_LAYERS
	SF_SKY,
	SF_SUN,
	SF_CITY_FAR
	//GROUND,
ENDENUM

// Enum descriptions of each background layer in each stage
ENUM ANDREAS_BG_LAYERS
	SA_SKY,
	SA_SUN,	
	SA_CITY_FAR,
	SA_CITY_NEAR
	//GROUND,
ENDENUM

// Enum descriptions of each background layer in each stage
ENUM VENTURAS_BG_LAYERS
	LV_SKY,
	LV_SUN,
	LV_CITY_FAR,
	LV_UFO
	//GROUND,
ENDENUM

// Enum descriptions of each background layer in each stage
ENUM LIBERTY_BG_LAYERS
	LB_SKY,
	LB_SUN,
	LB_STATUE,
	LB_CITY_FAR
	//GROUND,
ENDENUM

PROC ROAD_ARCADE_INIT_BACKGROUND_DATA(STAGES_SETS stage)
	
	int iSKY = ENUM_TO_INT(VC_SKY)
	int iSUN = ENUM_TO_INT(VC_SUN)
	int iCityFar = ENUM_TO_INT(VC_CITY_FAR)
	
	// Vice City
	IF stage = STAGE_VICE_CITY
		iSKY = ENUM_TO_INT(VC_SKY)
		rabsActiveBackground[iSKY].fYposition = cfSkyYpos
		rabsActiveBackground[iSKY].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSKY].sSprite = "TRACK_01_BACKGROUND_SKY"
		rabsActiveBackground[iSKY].vSpriteSize =  INIT_VECTOR_2D(632.0, 532.0)
		rabsActiveBackground[iSKY].rgba = rgbaOriginal
		rabsActiveBackground[iSKY].fScrollOffsetScale = 0.2
		
		iSUN = ENUM_TO_INT(VC_SUN)
		rabsActiveBackground[iSUN].fYposition = 172.0
		rabsActiveBackground[iSUN].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSUN].sSprite = "TRACK_01_BACKGROUND_SUN"
		rabsActiveBackground[iSUN].vSpriteSize =  INIT_VECTOR_2D(264.0, 264.0)
		rabsActiveBackground[iSUN].rgba = rgbaOriginal
		rabsActiveBackground[iSUN].fScrollOffsetScale = 0.1
		rabsActiveBackground[iSUN].fOffsetXPos = 400
		
		iCityFar = ENUM_TO_INT(VC_CITY_FAR)
		rabsActiveBackground[iCityFar].fYposition = cfCityYpos
		rabsActiveBackground[iCityFar].sDict = sDICT_TrackVice
		rabsActiveBackground[iCityFar].sSprite = "TRACK_01_BACKGROUND_SKYLINE"
		rabsActiveBackground[iCityFar].vSpriteSize = INIT_VECTOR_2D(1024.0, 516.0)
		rabsActiveBackground[iCityFar].rgba = rgbaOriginal
		rabsActiveBackground[iCityFar].fScrollOffsetScale = 0.8
		
		int iBlimp = ENUM_TO_INT(VC_BLIMP)
		rabsActiveBackground[iBlimp].fYposition = 400
		rabsActiveBackground[iBlimp].sDict = sDICT_TrackVice
		rabsActiveBackground[iBlimp].sSprite = "TRACK_01_CROSSING_BLIMP_01"
		rabsActiveBackground[iBlimp].vSpriteSize = INIT_VECTOR_2D(200.0, 64.0)
		rabsActiveBackground[iBlimp].rgba = rgbaOriginal
		rabsActiveBackground[iBlimp].fScrollOffsetScale = 3.0
		rabsActiveBackground[iBlimp].fAnimSpeed = 10.0
		rabsActiveBackground[iBlimp].fAnimTimer = 0.0
		rabsActiveBackground[iBlimp].fOffsetXPos = 500
					
	ENDIF
					
	// San Feirro
	IF stage = STAGE_SAN_FIERRO
		iSKY = ENUM_TO_INT(SF_SKY)
		rabsActiveBackground[iSKY].fYposition = cfSkyYpos
		rabsActiveBackground[iSKY].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSKY].sSprite = "TRACK_01_BACKGROUND_SKY"
		rabsActiveBackground[iSKY].vSpriteSize =  INIT_VECTOR_2D(632.0, 532.0)
		rabsActiveBackground[iSKY].rgba = rgbaOriginal
		rabsActiveBackground[iSKY].fScrollOffsetScale = 0.0

		iSUN = ENUM_TO_INT(SF_SUN)
		rabsActiveBackground[iSUN].fYposition = 264.0
		rabsActiveBackground[iSUN].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSUN].sSprite = "TRACK_01_BACKGROUND_SUN"
		rabsActiveBackground[iSUN].vSpriteSize =  INIT_VECTOR_2D(264.0, 264.0)
		rabsActiveBackground[iSUN].rgba = rgbaOriginal
		rabsActiveBackground[iSUN].fScrollOffsetScale = 0.0
		rabsActiveBackground[iSUN].fOffsetXPos = 500
		
		iCityFar = ENUM_TO_INT(SF_CITY_FAR)
		rabsActiveBackground[iCityFar].fYposition = cfCityYpos+80
		rabsActiveBackground[iCityFar].sDict = sDICT_TrackFeirro
		rabsActiveBackground[iCityFar].sSprite = "TRACK_03_BACKGROUND_SKYLINE"
		rabsActiveBackground[iCityFar].vSpriteSize = INIT_VECTOR_2D(1264.0, 568.0)
		rabsActiveBackground[iCityFar].rgba = rgbaOriginal
		rabsActiveBackground[iCityFar].fScrollOffsetScale = 0.0
		rabsActiveBackground[iCityFar].fOffsetXPos = -303
	ENDIF
		
	// San Andreas
	IF stage = STAGE_SAN_ANDREAS
		iSKY = ENUM_TO_INT(SA_SKY)
		rabsActiveBackground[iSKY].fYposition = cfSkyYpos
		rabsActiveBackground[iSKY].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSKY].sSprite = "TRACK_01_BACKGROUND_SKY"
		rabsActiveBackground[iSKY].vSpriteSize =  INIT_VECTOR_2D(632.0, 532.0)
		rabsActiveBackground[iSKY].rgba = rgbaSanAndreasEvening
		rabsActiveBackground[iSKY].fScrollOffsetScale = 0.2

		iSUN = ENUM_TO_INT(SA_SUN)
		rabsActiveBackground[iSUN].fYposition = 380.0
		rabsActiveBackground[iSUN].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSUN].sSprite = "TRACK_01_BACKGROUND_SUN"
		rabsActiveBackground[iSUN].vSpriteSize =  INIT_VECTOR_2D(264.0, 264.0)
		rabsActiveBackground[iSUN].rgba = rgbaSanAndreasEvening
		rabsActiveBackground[iSUN].fScrollOffsetScale = 0.1
		rabsActiveBackground[iSUN].fOffsetXPos = 600
		
		iCityFar = ENUM_TO_INT(SA_CITY_FAR)
		rabsActiveBackground[iCityFar].fYposition = cfCityYpos
		rabsActiveBackground[iCityFar].sDict = sDICT_TrackAndreas
		rabsActiveBackground[iCityFar].sSprite = "TRACK_02_BACKGROUND_VINEWOOD_SIGN"
		rabsActiveBackground[iCityFar].vSpriteSize = INIT_VECTOR_2D(1264.0, 516.0)
		rabsActiveBackground[iCityFar].rgba = rgbaOriginal
		rabsActiveBackground[iCityFar].fScrollOffsetScale = 0.8
		
		INT iCityNear = ENUM_TO_INT(SA_CITY_NEAR)
		rabsActiveBackground[iCityNear].fYposition = cfCityYpos
		rabsActiveBackground[iCityNear].sDict = sDICT_TrackAndreas
		rabsActiveBackground[iCityNear].sSprite = "TRACK_02_BACKGROUND_SKYLINE"
		rabsActiveBackground[iCityNear].vSpriteSize = INIT_VECTOR_2D(1264.0, 516.0)
		rabsActiveBackground[iCityNear].rgba = rgbaOriginal
		rabsActiveBackground[iCityNear].fScrollOffsetScale = 1.3
		rabsActiveBackground[iCityNear].fOffsetXPos = -208.0
	ENDIF
		
	// Las Venturas
	IF stage = STAGE_LAS_VENTURAS
		iSKY = ENUM_TO_INT(LV_SKY)
		rabsActiveBackground[iSKY].fYposition = cfSkyYpos
		rabsActiveBackground[iSKY].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSKY].sSprite = "TRACK_01_BACKGROUND_SKY"
		rabsActiveBackground[iSKY].vSpriteSize =  INIT_VECTOR_2D(632.0, 532.0)
		rabsActiveBackground[iSKY].rgba = rgbaNightContrast
		rabsActiveBackground[iSKY].fScrollOffsetScale = 0.2

		iSUN = ENUM_TO_INT(LV_SUN)
		rabsActiveBackground[iSUN].fYposition = 412.0
		rabsActiveBackground[iSUN].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSUN].sSprite = "TRACK_01_BACKGROUND_SUN"
		rabsActiveBackground[iSUN].vSpriteSize =  INIT_VECTOR_2D(264.0, 264.0)
		rabsActiveBackground[iSUN].rgba = rgbaLibertyCityEvening
		rabsActiveBackground[iSUN].fScrollOffsetScale = 0.1
		rabsActiveBackground[iSUN].fOffsetXPos = 800
		
		iCityFar = ENUM_TO_INT(LV_CITY_FAR)
		rabsActiveBackground[iCityFar].fYposition = cfCityYpos + 296.0
		rabsActiveBackground[iCityFar].sDict = sDICT_TrackVenturas
		rabsActiveBackground[iCityFar].sSprite = "TRACK_05_BACKGROUND_SKYLINE"
		rabsActiveBackground[iCityFar].vSpriteSize = INIT_VECTOR_2D(1264.0, 300.0)
		rabsActiveBackground[iCityFar].rgba = rgbaOriginal
		rabsActiveBackground[iCityFar].fScrollOffsetScale = 0.25
		
		int iUFO = ENUM_TO_INT(LV_UFO)
		rabsActiveBackground[iUFO].fYposition = 330
		rabsActiveBackground[iUFO].sDict = sDICT_TrackVenturas
		rabsActiveBackground[iUFO].sSprite = "TRACK_05_CROSSING_UFO_01_FRAME_01"
		rabsActiveBackground[iUFO].vSpriteSize = INIT_VECTOR_2D(200.0, 64.0)
		rabsActiveBackground[iUFO].rgba = rgbaOriginal
		rabsActiveBackground[iUFO].fScrollOffsetScale = 2.0
		rabsActiveBackground[iUFO].fAnimSpeed = 100.0
		rabsActiveBackground[iUFO].fAnimTimer = 0.0
		rabsActiveBackground[iUFO].fOffsetXPos = -2500
	ENDIF
		
	// Liberty City
	IF stage = STAGE_LIBERTY_CITY
		iSKY = ENUM_TO_INT(LB_SKY)
		rabsActiveBackground[iSKY].fYposition = cfSkyYpos
		rabsActiveBackground[iSKY].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSKY].sSprite = "TRACK_01_BACKGROUND_SKY"
		rabsActiveBackground[iSKY].vSpriteSize =  INIT_VECTOR_2D(632.0, 532.0)
		rabsActiveBackground[iSKY].rgba = rgbaLibertyCityEvening
		rabsActiveBackground[iSKY].fScrollOffsetScale = 0.2

		iSUN = ENUM_TO_INT(LB_SUN)
		rabsActiveBackground[iSUN].fYposition = 492.0
		rabsActiveBackground[iSUN].sDict = sDICT_GenericAssets
		rabsActiveBackground[iSUN].sSprite = "TRACK_01_BACKGROUND_SUN"
		rabsActiveBackground[iSUN].vSpriteSize =  INIT_VECTOR_2D(264.0, 264.0)
		rabsActiveBackground[iSUN].rgba = rgbaLibertyCityEvening
		rabsActiveBackground[iSUN].fScrollOffsetScale = 0.1
		rabsActiveBackground[iSUN].fOffsetXPos = 1100
		
		INT iStatue = ENUM_TO_INT(LB_STATUE)
		rabsActiveBackground[iStatue].fYposition = cfCityYpos
		rabsActiveBackground[iStatue].sDict = sDICT_TrackLiberty
		rabsActiveBackground[iStatue].sSprite = "TRACK_04_BACKGROUND_STATUE_OF_HAPPINESS"
		rabsActiveBackground[iStatue].vSpriteSize = INIT_VECTOR_2D(1264.0, 516.0)
		rabsActiveBackground[iStatue].rgba = rgbaOriginal
		rabsActiveBackground[iStatue].fOffsetXPos = 700
		rabsActiveBackground[iStatue].fScrollOffsetScale = 0.5
		
		iCityFar = ENUM_TO_INT(LB_CITY_FAR)
		rabsActiveBackground[iCityFar].fYposition = cfCityYpos
		rabsActiveBackground[iCityFar].sDict = sDICT_TrackLiberty
		rabsActiveBackground[iCityFar].sSprite = "TRACK_04_BACKGROUND_SKYLINE"
		rabsActiveBackground[iCityFar].vSpriteSize = INIT_VECTOR_2D(1264.0, 516.0)
		rabsActiveBackground[iCityFar].rgba = rgbaOriginal
		rabsActiveBackground[iCityFar].fScrollOffsetScale = 0.8
	ENDIF
			
ENDPROC

PROC ROAD_ARCADE_DRAW_BACKGROUND(STAGES_SETS stage, PLAYER_VEHICLE player, BOOL bIsTitleVersion = FALSE)

	// Draw a backdrop to the sky
	VECTOR_2D vSkyBackPos = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0)
	VECTOR_2D vSkyBackSCALE = INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT)
	ARCADE_DRAW_PIXELSPACE_RECT(vSkyBackPos, vSkyBackSCALE, rabsActiveBackground[0].rgba)
	
	INT i
		
	INT iLayerCount = 0
	FLOAT fCurveOffset
	FLOAT fSpeedDiv = 250
		
	// Filter the count by active stage
	IF stage = STAGE_VICE_CITY
		iLayerCount = COUNT_OF(VICE_BG_LAYERS)
	ENDIF
	
	IF stage = STAGE_SAN_FIERRO
		iLayerCount = COUNT_OF(FIERRO_BG_LAYERS)
	ENDIF

	IF stage = STAGE_SAN_ANDREAS
		iLayerCount = COUNT_OF(ANDREAS_BG_LAYERS)
	ENDIF
	
	IF stage = STAGE_LAS_VENTURAS
		iLayerCount = COUNT_OF(VENTURAS_BG_LAYERS)
	ENDIF
	
	IF stage = STAGE_LIBERTY_CITY
		iLayerCount = COUNT_OF(LIBERTY_BG_LAYERS)
	ENDIF
			
	FOR i = 0 TO iLayerCount - 1
	
	
		// Stage specific objects
		IF stage = STAGE_VICE_CITY
			
		
			IF i = ENUM_TO_INT(VC_BLIMP)
				rabsActiveBackground[i].fAnimTimer += (0.0+@1000)
			
				Float fOffsetYPos = rabsActiveBackground[i].fYposition + (rabsActiveBackground[i].vSpriteSize.y/2)
				fOffsetYPos += (ciROAD_HORIZON_POINT - 666)// Add the current offset.
				fOffsetYPos += transitionData.fTransitionOffset_Final
				// Adjust position for scale offset
				FLOAT fCurve = 0
				
				IF (!bIsTitleVersion)
					fCurve = SCALING_ROAD_CURVATURE(stage, player.fZvalue, true)
				ENDIF
				
				fCurveOffset = (0+@((player.fplayerSpeed/fSpeedDiv)*(fCurve*rabsActiveBackground[i].fScrollOffsetScale)))
				rabsActiveBackground[i].fOffsetXPos += 0+@(rabsActiveBackground[i].fAnimSpeed)
			
				// Round to a multiple of 4
				FLOAT fFinalX = (rabsActiveBackground[i].fOffsetXPos + fCurveOffset)
				fFinalX -= (fFinalX%4)
			
				
				IF fFinalX > 50
				AND fFinalX < 1930
					ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(rabsActiveBackground[i].sDict, rabsActiveBackground[i].sSprite, 
							INIT_VECTOR_2D(fFinalX, fOffsetYPos), 
							INIT_VECTOR_2D(rabsActiveBackground[i].vSpriteSize.x, rabsActiveBackground[i].vSpriteSize.y)
							, 0, rabsActiveBackground[i].rgba)
				ENDIF
							
				RELOOP
			ENDIF
			
		ENDIF
		
		IF stage = STAGE_LIBERTY_CITY
						
			// Draw a single sun sprite.
			INT iSTATUE = 2 // Sun is set up to be consitently '1' in all BG Data structs.
			if i = iSTATUE 
			
				Float fOffsetYPos = rabsActiveBackground[i].fYposition + (rabsActiveBackground[i].vSpriteSize.y/2)
				fOffsetYPos += transitionData.fTransitionOffset_Final
				//fOffsetYPos += (ciROAD_HORIZON_POINT - 666)// Add the current offset.
					
				rabsActiveBackground[i].fOffsetXPos += fCurveOffset
					
				ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(rabsActiveBackground[i].sDict, rabsActiveBackground[i].sSprite, 
						INIT_VECTOR_2D(rabsActiveBackground[i].fOffsetXPos, fOffsetYPos), 
						INIT_VECTOR_2D(160, 516),
						0, rabsActiveBackground[i].rgba)
						
				RELOOP
			ENDIF
			
		ENDIF
		
		
		// Get Venturas stage data
		IF stage = STAGE_LAS_VENTURAS
		
			IF i = ENUM_TO_INT(LV_UFO)
				rabsActiveBackground[i].fAnimTimer += (0.0+@1000)
				
				Float fOffsetYPos = rabsActiveBackground[i].fYposition + (rabsActiveBackground[i].vSpriteSize.y/2)
				fOffsetYPos += (ciROAD_HORIZON_POINT - 666)// Add the current offset.
				fOffsetYPos += transitionData.fTransitionOffset_Final
				
				// Adjust position for scale offset
				FLOAT fCurve = 0
				
				IF (!bIsTitleVersion)
					fCurve = SCALING_ROAD_CURVATURE(stage, player.fZvalue, true)
				ENDIF
				
				fCurveOffset = (0+@((player.fplayerSpeed/fSpeedDiv)*(fCurve*rabsActiveBackground[i].fScrollOffsetScale)))
				rabsActiveBackground[i].fOffsetXPos += 0+@(rabsActiveBackground[i].fAnimSpeed)
			
					// Round to a multiple of 4
				FLOAT fFinalX = (rabsActiveBackground[i].fOffsetXPos + fCurveOffset)
				fFinalX -= (fFinalX%4)
					
				IF fFinalX > 50
				AND fFinalX < 1930
					IF ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(rabsActiveBackground[i].sDict, rabsActiveBackground[i].sSprite, 
							INIT_VECTOR_2D(fFinalX, fOffsetYPos), 
							INIT_VECTOR_2D(rabsActiveBackground[i].vSpriteSize.x, rabsActiveBackground[i].vSpriteSize.y)
							, 0, rabsActiveBackground[i].rgba)
							
						ARCADE_GAMES_SOUND_PLAY_FROM_POSITION_LOOPING(ROAD_ARCADE_AUDIO_EFFECT_WORLD_UFO, INIT_VECTOR_2D(fFinalX, fOffsetYPos))
					ELSE
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_WORLD_UFO)
					ENDIF
					
				ENDIF
						
				RELOOP
			ENDIF
		
		ELSE
			
			// Safely stop the UFO sound
			IF ARCADE_GAMES_SOUND_HAS_FINISHED(ROAD_ARCADE_AUDIO_EFFECT_WORLD_UFO)
				ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_WORLD_UFO)
			ENDIF
		
		ENDIF
		
		// Get Liberty stage data
			
		FLOAT fCurve = 0
				
		IF (!bIsTitleVersion)
			fCurve = SCALING_ROAD_CURVATURE(stage, player.fZvalue, true)
		ENDIF
		
		
		fCurveOffset = (0+@((player.fplayerSpeed/fSpeedDiv)*(fCurve*rabsActiveBackground[i].fScrollOffsetScale)))
			
		// Draw a single sun sprite.
		INT iSUN = 1 // Sun is set up to be consitently '1' in all BG Data structs.
		if i = iSUN 
								
			IF transitionData.transition_ActiveState = T_WAIT_FOR_BACKGROUND
			OR transitionData.transition_ActiveState = T_LOAD
				IF stage = STAGE_VICE_CITY
					rabsActiveBackground[i].fYposition = COSINE_INTERP_FLOAT(172.0, 264.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].fOffsetXPos = COSINE_INTERP_FLOAT(400.0, 500.0, transitionData.fTransitionProgress)
				ENDIF
			
				IF stage = STAGE_SAN_FIERRO
					rabsActiveBackground[i].fYposition = COSINE_INTERP_FLOAT(264.0, 380.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].fOffsetXPos = COSINE_INTERP_FLOAT(500.0, 600.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaOriginal, rgbaSanAndreasEvening)
				ENDIF
				
				IF stage = STAGE_SAN_ANDREAS
					rabsActiveBackground[i].fYposition = COSINE_INTERP_FLOAT(380.0, 412.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].fOffsetXPos = COSINE_INTERP_FLOAT(600.0, 800.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaSanAndreasEvening, rgbaNightContrast)
				ENDIF
				
				IF stage = STAGE_LAS_VENTURAS
					rabsActiveBackground[i].fYposition = COSINE_INTERP_FLOAT(412.0, 492.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].fOffsetXPos = COSINE_INTERP_FLOAT(800.0, 1100.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaNightContrast, rgbaLibertyCityEvening)
				ENDIF
				
				IF stage = STAGE_LIBERTY_CITY
					rabsActiveBackground[i].fYposition = COSINE_INTERP_FLOAT(492.0, 172.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].fOffsetXPos = COSINE_INTERP_FLOAT(1100.0, 400.0, transitionData.fTransitionProgress)
					rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaLibertyCityEvening, rgbaOriginal)
				ENDIF
			ELSE
			
				// Bug: Quick fix. Unlear why transition snaps
				IF stage = STAGE_LAS_VENTURAS
					rabsActiveBackground[i].rgba = rgbaNightContrast
				ENDIF
			
			ENDIF
			
			rabsActiveBackground[i].fOffsetXPos += fCurveOffset
							
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(rabsActiveBackground[i].sDict, rabsActiveBackground[i].sSprite, 
					INIT_VECTOR_2D(rabsActiveBackground[i].fOffsetXPos, rabsActiveBackground[i].fYposition), 
					INIT_VECTOR_2D(rabsActiveBackground[i].vSpriteSize.x, rabsActiveBackground[i].vSpriteSize.y)
					, 0, rabsActiveBackground[i].rgba)
					
			RELOOP
		ENDIF
		
//		IF i = 2
//			CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_DRAW_BACKGROUND - Layer 2 X offset: ", rabsActiveBackground[i].fOffsetXPos)
//		ENDIF
		
		// Start from sprite edge. Will correct to halfway as part of loop.
		// Adjust position for scale offset
		IF rabsActiveBackground[i].fOffsetXPos <= -rabsActiveBackground[i].vSpriteSize.x
			rabsActiveBackground[i].fOffsetXPos += rabsActiveBackground[i].vSpriteSize.x
		ENDIF
			
		IF rabsActiveBackground[i].fOffsetXPos >= rabsActiveBackground[i].vSpriteSize.x*2.0
			rabsActiveBackground[i].fOffsetXPos -= rabsActiveBackground[i].vSpriteSize.x
		ENDIF
			
		fCurveOffset += (fCurveOffset%4) // Round to a whole pixel
		rabsActiveBackground[i].fOffsetXPos += fCurveOffset
		
		FLOAT fxpos = rabsActiveBackground[i].fOffsetXPos
		
		INT j	
		FOR j = 0 TO 10 // Repeat for max number of sprite instances.
												
			//rabsActiveBackground[i].fOffsetXPos += (rabsActiveBackground[i].vSpriteSize.x/2)
			Float fOffsetYPos = rabsActiveBackground[i].fYposition + (rabsActiveBackground[i].vSpriteSize.y/2)
			
			// Testing: May need to filter by city if we add additional layers.
			fOffsetYPos += (ciROAD_HORIZON_POINT - ciROAD_HORIZON_POINT_BASE)// Add the current offset.
										
			// Break the loop if we have filled the screen.
			IF fxpos > (cfBASE_SCREEN_WIDTH + rabsActiveBackground[i].vSpriteSize.x)
				rabsActiveBackground[i].fBackgroundLength = fxpos
				rabsActiveBackground[i].iNumberOfTiles = j
				BREAKLOOP
			ENDIF
			
			// Draw the first one off screen if we are over the screen limit.
			if j = 0
			and fxpos > 0
				fxpos -= rabsActiveBackground[i].vSpriteSize.x
			ENDIF
			
			fOffsetYPos -= (fOffsetYPos%4)
			
			IF i != 0 // SKY stays put
				fOffsetYPos += transitionData.fTransitionOffset_Final
			ELSE
				
				// Colour interp between backgrounds
				IF transitionData.transition_ActiveState = T_WAIT_FOR_BACKGROUND
				OR transitionData.transition_ActiveState = T_LOAD
					IF stage = STAGE_SAN_FIERRO
						rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaOriginal, rgbaSanAndreasEvening)
					ENDIF
					
					IF stage = STAGE_SAN_ANDREAS
						rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaSanAndreasEvening, rgbaNightContrast)
					ENDIF
					
					IF stage = STAGE_LAS_VENTURAS
						rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaNightContrast, rgbaLibertyCityEvening)
					ENDIF
					
					IF stage = STAGE_LIBERTY_CITY
						rabsActiveBackground[i].rgba = ROAD_ARCADE_HANDLE_INTERP_BETWEEN_COLOURS(rgbaLibertyCityEvening, rgbaOriginal)
					ENDIF
				ENDIF
			
			ENDIF
					
			ARCADE_DRAW_PIXELSPACE_SPRITE(rabsActiveBackground[i].sDict, rabsActiveBackground[i].sSprite, 
				INIT_VECTOR_2D(fxpos, fOffsetYPos), 
				INIT_VECTOR_2D(rabsActiveBackground[i].vSpriteSize.x, rabsActiveBackground[i].vSpriteSize.y)
				, 0, rabsActiveBackground[i].rgba)
			
			fxpos += rabsActiveBackground[i].vSpriteSize.x
					
								
		ENDFOR
									
	ENDFOR
		
ENDPROC

//--------------- HUD Control ------------
PROC ROAD_ARCADE_HUD_GET_SPEED(FLOAT speed, TEXT_LABEL_63 &sSpeed[])
	
	FLOAT fInputSpeed = speed
		
	IF fInputSpeed < 0
		fInputSpeed = 0
	ENDIF 
		
	sSpeed[0] += FLOOR(fInputSpeed/100)
	fInputSpeed -= (FLOOR(fInputSpeed/100))*100 // Remove the hundreds
	sSpeed[1] += FLOOR(fInputSpeed/10)
	fInputSpeed -= (FLOOR(fInputSpeed/10))*10 // Remove the hundreds	
	sSpeed[2] += FLOOR(fInputSpeed)

ENDPROC

PROC ROAD_ARCADE_HUD_DRAW_STARTING_LIGHTS(PLAYER_VEHICLE &player)

	STRING sRedLight = "HUD_BUTTON_LIGHT_RED"
	STRING sYellowLight = "HUD_BUTTON_LIGHT_AMBER"
	STRING sGreenLight = "HUD_BUTTON_LIGHT_GREEN"
	
	STRING sCurrentLight = ""
	
	// If 3 seconds have not passed keep TIMERB at 0
		
	IF fCountDownTimer >= 3000
	AND player.psActiveState = ps_countdown
		player.psActiveState = ps_Drive
		
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_START_CAR)
		ENDIF

		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_START_BIKE)
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_START_TRUCK)
		ENDIF
	ENDIF
		
	IF fCountDownTimer != 0
		IF fCountDownTimer < 4000
			sCurrentLight = sGreenLight
		ENDIF
		
		IF fCountDownTimer < 3000
			sCurrentLight = sYellowLight
		ENDIF
		
		IF fCountDownTimer < 2000
			sCurrentLight = sRedLight
		ENDIF
		
		IF fCountDownTimer < 1000
			sCurrentLight = ""
		ENDIF
	ENDIF
		
	IF !IS_STRING_NULL_OR_EMPTY(sCurrentLight)
		
		IF ARE_STRINGS_EQUAL(sCurrentLight, sRedLight)
			ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_COUNTDOWN_RED)
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCurrentLight, sYellowLight)
			ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_COUNTDOWN_YELLOW)
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCurrentLight, sGreenLight)
			ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_GREEN_LIGHT_GO)
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCurrentLight, sGreenLight)
			ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_GREEN_LIGHT_GO)
		ENDIF
	
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sCurrentLight, 
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), 
				INIT_VECTOR_2D(64.0*2.0, 148.0*2.0)
				, 0, rgbaOriginal)
		
	ENDIF
	
	IF fCountDownTimer < 6000
		fCountDownTimer = fCountDownTimer +@1000	
	ENDIF
ENDPROC

PROC ROAD_ARCADE_HUD_GET_TIMER(TEXT_LABEL_63 &sTimer[])

	FLOAT fTimeRemaining = fGameTIMER
	
	sTimer[0] += FLOOR((fTimeRemaining - (fTimeRemaining%60))/60) // Calculates minutes
	fTimeRemaining -= (fTimeRemaining - (fTimeRemaining%60)) // Remove the minutes
	sTimer[1] += FLOOR(fTimeRemaining/10)
	fTimeRemaining -= (FLOOR(fTimeRemaining/10))*10 // Remove the tens
	sTimer[2] += FLOOR(fTimeRemaining)

ENDPROC

PROC ROAD_ARCADE_HUD_DRAW(PLAYER_VEHICLE &player)

	// Draw speedo
	STRING sSpeedIcon = "HUD_BUTTON_SPEED"
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSpeedIcon, 
				INIT_VECTOR_2D(436.0, 864.0), 
				INIT_VECTOR_2D(128.0, 64.0)
				, 0, rgbaOriginal)
	
	TEXT_LABEL_63 sSpeed[3]
	sSpeed[0] = "HUD_TIMER_NUMBER_" // Hundreds
	sSpeed[1] = "HUD_TIMER_NUMBER_" // Tens
	sSpeed[2] = "HUD_TIMER_NUMBER_" // Singles
	
	ROAD_ARCADE_HUD_GET_SPEED(player.fPlayerSpeed, sSpeed) // Translate numbers into text
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSpeed[0], 
				INIT_VECTOR_2D(388.0, 932.0), 
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSpeed[1], 
				INIT_VECTOR_2D(420, 932.0), 
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSpeed[2], 
				INIT_VECTOR_2D(452.0, 932.0),
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
	
	// Draw Drift Multiplier
	IF player.iSpeedMultiplier > 1
		TEXT_LABEL_63 sSpeedMult = "HUD_BUTTON_X"
		sSpeedMult += player.iSpeedMultiplier
		
		ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sSpeedMult, 
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH - 404, 762.0),
				INIT_VECTOR_2D(64.0, 64.0)
				, 0, rgbaOriginal)
	ENDIF
	
	// Draw Gears
	STRING sGearTexture = ""
	IF player.pgPlayerGear = pgLow
		sGearTexture = "HUD_BUTTON_GEAR_LOW"
	ENDIF
	
	IF player.pgPlayerGear = pgHigh
		sGearTexture = "HUD_BUTTON_GEAR_HIGH"
	ENDIF
	
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sGearTexture, 
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH - 404, 888.0),
				INIT_VECTOR_2D(64.0, 148.0)
				, 0, rgbaOriginal)
	
	// Draw current score
	FLOAT fScoreNumStartX = ((cfBASE_SCREEN_WIDTH/2.0)+224)
	
	FLOAT fStartY = 152.0
	ROAD_ARCADE_DRAW_SCORE(player.iPlayerScore, INIT_VECTOR_2D(fScoreNumStartX, fStartY))
	
	// Draw timer
	STRING sTimerIcon = "HUD_BUTTON_TIMER"
	FLOAT fTimerStartPosX = 436
		
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sTimerIcon, 
				INIT_VECTOR_2D(fTimerStartPosX, fStartY), 
				INIT_VECTOR_2D(128.0, 64.0)
				, 0, rgbaOriginal)
	
	// Creates a flashing effect on the time objects.
	IF fGameTIMER < 10
	AND fGameTIMER > 5
	
		IF (fGameTIMER%1) < 0.5
			EXIT
		ENDIF
	
	ENDIF
	
	IF fGameTIMER < 5
	AND fGameTIMER > 0.0
	
		IF (fGameTIMER%0.5) < 0.25
			EXIT
		ENDIF
	
	ENDIF
	
	TEXT_LABEL_63 sTime[3]
	sTime[0] = "HUD_TIMER_NUMBER_"
	sTime[1] = "HUD_TIMER_NUMBER_"
	sTime[2] = "HUD_TIMER_NUMBER_"
	
	ROAD_ARCADE_HUD_GET_TIMER(sTime)
		
	// Draw minutes
	fTimerStartPosX += 96 // Offset
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sTime[0], 
				INIT_VECTOR_2D(fTimerStartPosX, fStartY), 
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
				
	// Draw colon
	STRING sTimerColon = "HUD_TIMER_COLON"
	fTimerStartPosX += 48 // Offset
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sTimerColon, 
				INIT_VECTOR_2D(fTimerStartPosX, fStartY), 
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
	
	// Draw tens of seconds
	fTimerStartPosX += 48
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sTime[1], 
				INIT_VECTOR_2D(fTimerStartPosX, fStartY), 
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
	
	// Draw seconds
	fTimerStartPosX += 48
	ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_HUDGeneric, sTime[2],
				INIT_VECTOR_2D(fTimerStartPosX, fStartY), 
				INIT_VECTOR_2D(32.0, 64.0)
				, 0, rgbaOriginal)
	
	
ENDPROC

//---------DEBUG---------
#IF IS_DEBUG_BUILD

//-------------- DEBUG CONTROLS-----------------------
//Debug Variables
WIDGET_GROUP_ID widgetRoadPrototype
BOOL bDebugCreatedWidgets = FALSE

WIDGET_GROUP_ID widgetBackgroundPrototype
BOOL bDebugCreatedBackgroundWidgets = FALSE

WIDGET_GROUP_ID widgetArcadeCabinetSupport
BOOL bDebugCreatedArcadeCabWidgets = FALSE

//Debug Functions
PROC ROAD_DEBUG_SET_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)
	widgetRoadPrototype = parentWidgetGroup
	widgetBackgroundPrototype = parentWidgetGroup
	widgetArcadeCabinetSupport = parentWidgetGroup
ENDPROC

//---------------------------------------------------

PROC ROAD_ARCADE_RESET_CHALLENGES()

	SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TRAFFICAVOI, FALSE)
	SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_CANTCATCHBRA, FALSE)

ENDPROC

PROC BACKGROUND_DEBUG_CREATE_WIDGETS()

	IF bDebugCreatedBackgroundWidgets
		EXIT
	ENDIF
	
	IF widgetBackgroundPrototype = NULL
		EXIT
	ENDIF

	SET_CURRENT_WIDGET_GROUP(widgetBackgroundPrototype)
		
	START_WIDGET_GROUP("Arcade Road Prototype - Background")
		
		ADD_WIDGET_FLOAT_SLIDER("SKY_X_POS: ", cfSkyXpos, -1920.0, 1920.0, 4.0)
		ADD_WIDGET_FLOAT_SLIDER("SKY_Y_POS: ", cfSkyYpos, -1080.0, 1080.0, 4.0)
		
		ADD_WIDGET_STRING(" ")
		
		ADD_WIDGET_FLOAT_SLIDER("TERRAIN_X_POS: ", cfTerrainXpos, -1920.0, 1920.0, 4.0)
		ADD_WIDGET_FLOAT_SLIDER("TERRAIN_Y_POS: ", cfTerrainYpos, -1080.0, 1080.0, 4.0)
		
		ADD_WIDGET_STRING(" ")
		
		ADD_WIDGET_FLOAT_SLIDER("CITY_X_POS: ", cfCityXpos, -1920.0, 1920.0, 4.0)
		ADD_WIDGET_FLOAT_SLIDER("CITY_Y_POS: ", cfCityYpos, -1080.0, 1080.0, 4.0)
					
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(widgetBackgroundPrototype)
		
	bDebugCreatedBackgroundWidgets = TRUE
	
ENDPROC

PROC CABINET_DEBUG_CREATE_WIDGETS()

	IF bDebugCreatedArcadeCabWidgets
		EXIT
	ENDIF
	
	IF widgetArcadeCabinetSupport = NULL
		EXIT
	ENDIF

	SET_CURRENT_WIDGET_GROUP(widgetArcadeCabinetSupport)
		
	START_WIDGET_GROUP("Arcade Road Prototype - Arcade Cabinet Support")
		ARCADE_GAMES_POSTFX_WIDGET_CREATE()
		ARCADE_GAMES_SOUND_WIDGET_CREATE()
		ARCADE_GAMES_LEADERBOARD_WIDGET_CREATE()
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(widgetArcadeCabinetSupport)
	
	bDebugCreatedArcadeCabWidgets = TRUE
	
ENDPROC

PROC ROAD_DEBUG_CREATE_WIDGETS()

	INT iTIMER_A = TIMERA()
	INT iTIMER_B = TIMERB()

	IF bDebugCreatedWidgets
		EXIT
	ENDIF
	
	IF widgetRoadPrototype = NULL
		EXIT
	ENDIF

	SET_CURRENT_WIDGET_GROUP(widgetRoadPrototype)
		
	START_WIDGET_GROUP("Arcade Road Prototype - Road Debug")

		ADD_WIDGET_BOOL("g_bMinimizeArcadeCabinetDrawing", g_bMinimizeArcadeCabinetDrawing)
		
		ADD_WIDGET_STRING(" ")

		ADD_WIDGET_FLOAT_READ_ONLY("JOURNEY_PROGRESS: ", fJOURNEY_DistanceTravelled)
		ADD_WIDGET_INT_READ_ONLY("SPRITES DRAWN: ", iSpritesDrawnThisFrame)
		ADD_WIDGET_INT_READ_ONLY("RECTS DRAWN: ", iRectsDrawnThisFrame)
		ADD_WIDGET_FLOAT_READ_ONLY("CAR_X_POS: ", fXCarOffset)
		ADD_WIDGET_INT_READ_ONLY("TIMER_A: ", iTIMER_A)
		ADD_WIDGET_INT_READ_ONLY("TIMER_B: ", iTIMER_B)
		ADD_WIDGET_STRING(" ")
		//ADD_WIDGET_FLOAT_SLIDER("VEHICLE_SPEED", player.fPlayerSpeed, 0.0, 180.0, 1.0)
//		ADD_WIDGET_INT_SLIDER("ROAD_HORIZON", ciROAD_HORIZON_POINT, 222, ROUND(cfBASE_SCREEN_HEIGHT), 1)
		ADD_WIDGET_FLOAT_SLIDER("SEGMENT_LENGTH",cfROAD_SEGMENT_LENGTH, 1.0, 100.0, 0.1)
		ADD_WIDGET_INT_SLIDER("ACTIVE_SPRITE_HEIGHT",ciROAD_DEFAULT_SPRITE_HEIGHT, 1, 24, 1)
		ADD_WIDGET_FLOAT_SLIDER("cfROAD_HillScaleMod",cfROAD_HillScaleMod, -0.5, 0.15, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("cfROAD_WidthMultiplierMod",cfROAD_WidthMultiplierMod, -10.0, 10.0, 0.01)
		ADD_WIDGET_INT_SLIDER("ciROAD_HorizonDebug",ciROAD_HorizonDebug, -1000, 1000, 1)
		//ADD_WIDGET_FLOAT_SLIDER("SIDE_SCALE_MOD",cfROAD_SideScaleMod, 0.1, 10, 0.1)
		ADD_WIDGET_INT_SLIDER("TUNNEL PIECE SIZE MOD", ciROAD_DEFAULT_TUNNEL_SIZE, 2, 32, 1)
									
	STOP_WIDGET_GROUP()
		
	CLEAR_CURRENT_WIDGET_GROUP(widgetRoadPrototype)
	
	bDebugCreatedWidgets = TRUE
	
ENDPROC
#ENDIF


// Final update function to run at the end of the frame
PROC ROAD_ARCADE_JOURNEY_UPDATE(PLAYER_VEHICLE &player)

	IF !bFinishedRace
		ROAD_ARCADE_SLIP_STREAM(player, player.bWillSlipStream)
	 	player.bWillSlipStream = FALSE
		
		IF player.fPlayerSpeed < 0.0
			player.fPlayerSpeed = 0.0
		ENDIF
		
		// Cap out the speed!
		IF player.fPlayerSpeed > (player.fMaxSpeed * 2.0)
		AND player.slipState != slipOVERDRIVE // Slingshot temporarily exceeds max speed
			player.fPlayerSpeed = player.fMaxSpeed * 2.0
		ENDIF
	ENDIF

	fJOURNEY_DistanceTravelled = fJOURNEY_DistanceTravelled +@(player.fPlayerSpeed)
	fZTotalLengthOfRoad = fFinalZValue-fJOURNEY_DistanceTravelled // Z length of the on-screen road.
	fZTotalLengthOfRoad = fZTotalLengthOfRoad // Self asigning until needed
	
	// Calculate speed multiplier
	// Multiplier always stays at 1.
	player.iSpeedMultiplier = 1
	
	// For each Multiple of 10 above 100 that player is travelling at add 1 to the multiplier.
	IF player.fPlayerSpeed >= 60
		player.iSpeedMultiplier = CEIL((player.fPlayerSpeed - 60)/10)
	ENDIF
	
	IF player.iSpeedMultiplier < 1
		player.iSpeedMultiplier = 1
	ENDIF
	
	IF player.iSpeedMultiplier > 5
		player.iSpeedMultiplier = 5
	ENDIF
	
	// Add to player score
	IF !bFinishedRace
		player.iPlayerScore	= ROUND((player.iPlayerScore+@(player.fPlayerSpeed*player.iSpeedMultiplier)))
	ENDIF
		
ENDPROC

FUNC ARCADE_GAMES_HELP_TEXT_ENUM RA_GET_RANDOM_HINT()

	INT random = 0
	ARCADE_GAMES_HELP_TEXT_ENUM toReturn = ARCADE_GAMES_HELP_TEXT_ENUM_RNC_HINT_SPEED
	
	random = GET_RANDOM_INT_IN_RANGE(0, 4)
	
	SWITCH random
	
		case 1
			toReturn = ARCADE_GAMES_HELP_TEXT_ENUM_RNC_HINT_SKIMMING
		break
	
		case 2
			toReturn = ARCADE_GAMES_HELP_TEXT_ENUM_RNC_HINT_SLIPSTREAM
		break
		
		case 3
			toReturn = ARCADE_GAMES_HELP_TEXT_ENUM_RNC_HINT_CROSSERS
		break
		
	ENDSWITCH

	RETURN toReturn

ENDFUNC

PROC ROAD_ARCADE_GO_TO_LEADERBOARD(PLAYER_VEHICLE &player)
	UNUSED_PARAMETER(player)
	
	SETTIMERA(0)
	SETTIMERB(0)

	ARCADE_GAMES_HELP_TEXT_PRINT(RA_GET_RANDOM_HINT())
	eCurrentEntryStage = NE_FIRST_LETTER // Reset tag entry
	ROAD_ARCADE_RELEASE_ALL_SOUNDS() // Safely stop he crowd sound
	ROAD_ARCADE_AUDIO_START_AUDIO_SCENE(ROAD_ARCADE_AUDIO_SCENE_MENU)
	gameCurrentState = GS_CLEANUP_GAME_ASSETS
ENDPROC

PROC ROAD_ARCADE_LOAD_LEADERBOARDS()

	IF eRoadArcade_ActiveGameType = RA_GT_INVALID
		EXIT
	ENDIF

	// Load the game's leaderboard by game type
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		ARCADE_GAMES_LEADERBOARD_LOAD(CASINO_ARCADE_GAME_RNC_STREET_LEGAL)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		ARCADE_GAMES_LEADERBOARD_LOAD(CASINO_ARCADE_GAME_RNC_CROTCH_ROCKETS)
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		ARCADE_GAMES_LEADERBOARD_LOAD(CASINO_ARCADE_GAME_RNC_GET_TRUCKIN)
	ENDIF

ENDPROC

SIDELINE_OBJECT soFinishGroup_1
SIDELINE_OBJECT soFinishGroup_2

FLOAT fTrophyGiverX = 0.0

PROC ROAD_ARCADE_HANDLE_PLAYER_FINISH_ANIM(PLAYER_VEHICLE &player, STAGES_SETS currentStage)

	string sSlightT = "VEHICLE_CAR_01_FRAME_02_UP_HILL"
	string sMidT = "VEHICLE_CAR_01_FRAME_03_UP_HILL"
	string sFarT = "VEHICLE_CAR_01_FRAME_04_UP_HILL"
	
	string sSlightT_Driver = "VEHICLE_CAR_01_FRAME_02_UP_HILL_DRIVER_FRAME_01_LEFT"
	string sMidT_Driver = "VEHICLE_CAR_01_FRAME_03_UP_HILL_DRIVER_FRAME_01_LEFT"
	string sFarT_Driver = "VEHICLE_CAR_01_FRAME_04_UP_HILL_DRIVER_FRAME_01_LEFT"

	string sFinishSlide1 = "VEHICLE_CAR_01_FRAME_01_FINISH"
	string sFinishSlide2 = "VEHICLE_CAR_01_FRAME_02_FINISH"
	string sFinishSlide3 = "VEHICLE_CAR_01_FRAME_03_FINISH"

	string sFinishSlide1_Driver = "VEHICLE_CAR_01_FRAME_01_FINISH_DRIVER_FRAME_01"
	string sFinishSlide2_Driver = "VEHICLE_CAR_01_FRAME_02_FINISH_DRIVER_FRAME_01"
	string sFinishSlide3_Driver = "VEHICLE_CAR_01_FRAME_03_FINISH_DRIVER_FRAME_01"

	IF iPlayerSeatLocalID = 0
	
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sSlightT = "VEHICLE_BIKE_01_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_BIKE_01_FRAME_03_UP_HILL"
			sFarT = "VEHICLE_BIKE_01_FRAME_04_UP_HILL"
			
			sFinishSlide1 = "VEHICLE_BIKE_01_FRAME_01_FINISH"
			sFinishSlide2 = "VEHICLE_BIKE_01_FRAME_02_FINISH"
			sFinishSlide3 = "VEHICLE_BIKE_01_FRAME_03_FINISH"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sSlightT = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02"
			sMidT = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03"
			sFarT = "VEHICLE_TRUCK_01_UP_HILL_FRAME_04"
			
			sFinishSlide1 = "VEHICLE_TRUCK_01_FRAME_01_FINISH"
			sFinishSlide2 = "VEHICLE_TRUCK_01_FRAME_02_FINISH"
			sFinishSlide3 = "VEHICLE_TRUCK_01_FRAME_03_FINISH"
		ENDIF
	
	ENDIF

	IF iPlayerSeatLocalID = 1
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			sSlightT = "VEHICLE_CAR_02_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_CAR_02_FRAME_03_UP_HILL"
			sFarT = "VEHICLE_CAR_02_FRAME_04_UP_HILL"
			
			sFinishSlide1 = "VEHICLE_CAR_02_FRAME_01_FINISH"
			sFinishSlide2 = "VEHICLE_CAR_02_FRAME_02_FINISH"
			sFinishSlide3 = "VEHICLE_CAR_02_FRAME_03_FINISH"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			sSlightT = "VEHICLE_BIKE_02_FRAME_02_UP_HILL"
			sMidT = "VEHICLE_BIKE_02_FRAME_03_UP_HILL"
			sFarT = "VEHICLE_BIKE_02_FRAME_04_UP_HILL"
			
			sFinishSlide1 = "VEHICLE_BIKE_02_FRAME_01_FINISH"
			sFinishSlide2 = "VEHICLE_BIKE_02_FRAME_02_FINISH"
			sFinishSlide3 = "VEHICLE_BIKE_02_FRAME_03_FINISH"
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			sSlightT = "VEHICLE_TRUCK_02_UP_HILL_FRAME_02"
			sMidT = "VEHICLE_TRUCK_02_UP_HILL_FRAME_03"
			sFarT = "VEHICLE_TRUCK_02_UP_HILL_FRAME_04"
			
			sFinishSlide1 = "VEHICLE_TRUCK_02_FRAME_01_FINISH"
			sFinishSlide2 = "VEHICLE_TRUCK_02_FRAME_02_FINISH"
			sFinishSlide3 = "VEHICLE_TRUCK_02_FRAME_03_FINISH"
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sSlightT_Driver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_02_DRIVER_FRAME_01_LEFT"
		sMidT_Driver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_03_DRIVER_FRAME_01_LEFT"
		sFarT_Driver = "VEHICLE_TRUCK_01_UP_HILL_FRAME_04_DRIVER_FRAME_01_LEFT"

		sFinishSlide1_Driver = "VEHICLE_TRUCK_01_FRAME_01_FINISH_DRIVER_FRAME_01"
		sFinishSlide2_Driver = "VEHICLE_TRUCK_01_FRAME_02_FINISH_DRIVER_FRAME_02"
		sFinishSlide3_Driver = "VEHICLE_TRUCK_01_FRAME_03_FINISH_DRIVER_FRAME_01"
	ENDIF
	
	VECTOR_2D vSpritePos = INIT_VECTOR_2D(TO_FLOAT(player.iPlayerPosX),TO_FLOAT(player.iPlayerPosY))
	VECTOR_2D vSpriteScale = INIT_VECTOR_2D(TO_FLOAT(player.iWidth), TO_FLOAT(player.iHeight))
	VECTOR_2D vDriverScale = INIT_VECTOR_2D(236, 288)
	VECTOR_2D vSpriteScaleRevX = INIT_VECTOR_2D(TO_FLOAT(player.iWidth), TO_FLOAT(player.iHeight))
	vSpriteScaleRevX.x = -ABSF(vSpriteScaleRevX.x)
		
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		vDriverScale = INIT_VECTOR_2D(288, 288)
	ENDIF
		
	// Dog sits up
	string sDogAnim2 = "VEHICLE_CAR_01_FRAME_03_FINISH_DRIVER_FRAME_02"
	string sDogAnim3 = "VEHICLE_CAR_01_FRAME_03_FINISH_DRIVER_FRAME_03"
	string sDogAnim4_Frame1 = "VEHICLE_CAR_01_FRAME_03_FINISH_DRIVER_FRAME_04"
	string sDogAnim4_Frame2 = "VEHICLE_CAR_01_FRAME_03_FINISH_DRIVER_FRAME_05"
	
	// Override for the Truck version
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sDogAnim2 = "VEHICLE_TRUCK_01_FRAME_03_FINISH_DRIVER_FRAME_01"
		sDogAnim3 = "VEHICLE_TRUCK_01_FRAME_03_FINISH_DRIVER_FRAME_03"
		sDogAnim4_Frame1 = "VEHICLE_TRUCK_01_FRAME_03_FINISH_DRIVER_FRAME_02"
		sDogAnim4_Frame2 = "VEHICLE_TRUCK_01_FRAME_03_FINISH_DRIVER_FRAME_03"
	ENDIF
			
	IF pfasCurrentFinishState >= FS3_Dift_To_Stop
		
		// Car shadow
		IF eRoadArcade_ActiveGameType = RA_GT_CAR
			IF iPlayerSeatLocalID = 0
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_CAR_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
			ENDIF
			
			IF iPlayerSeatLocalID = 1
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_CAR_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
			ENDIF
		ENDIF
		
		IF eRoadArcade_ActiveGameType = RA_GT_BIKE
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(player.sSpriteDict, "VEHICLE_BIKE_SHADOW", INIT_VECTOR_2D(vSpritePos.x + 8, (vSpritePos.y + (vSpriteScale.y/2.0))-20), INIT_VECTOR_2D(72, 52), 0.0, rgbaOriginal)	
		ENDIF
		
		// Truck
		IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
			IF iPlayerSeatLocalID = 0
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_TRUCK_01_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
			ENDIF
			
			IF iPlayerSeatLocalID = 1
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict, "VEHICLE_TRUCK_02_SHADOW_FRAME_01", INIT_VECTOR_2D(vSpritePos.x, vSpritePos.y), INIT_VECTOR_2D(ABSF(vSpriteScale.x), vSpriteScale.y), 0.0, rgbaOriginal)
			ENDIF
		ENDIF
		
	ENDIF
	
	FLOAT fMinSpeed = 60
	
	IF pfasCurrentFinishState > FS0_INIT
		ROAD_ARCADE_DRAW_SIDELINE_OBJECT_SINGLE(player, soFinishGroup_1, SIDE_LEFT, currentStage)
		ROAD_ARCADE_DRAW_SIDELINE_OBJECT_SINGLE(player, soFinishGroup_1, SIDE_RIGHT, currentStage)
		ROAD_ARCADE_DRAW_SIDELINE_OBJECT_SINGLE(player, soFinishGroup_2, SIDE_LEFT, currentStage)
		ROAD_ARCADE_DRAW_SIDELINE_OBJECT_SINGLE(player, soFinishGroup_2, SIDE_RIGHT, currentStage)
	ENDIF

	// Temp string to hold the final finish line anim structs.
	STRING sDICT_FinishLine_Final = sDICT_FinishLine

	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		sDICT_FinishLine_Final = sDICT_FinishLine
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		sDICT_FinishLine_Final = sDICT_FinishLine_Bike
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sDICT_FinishLine_Final = sDICT_FinishLine_Truck
	ENDIF
		
	SWITCH(pfasCurrentFinishState)
	
		CASE FS0_INIT
		
			REQUEST_STREAMED_TEXTURE_DICT(sDICT_FinishGroups)
		
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_FinishGroups)
				soFinishGroup_1.objID = 0
				soFinishGroup_1.sObjDict = sDICT_FinishGroups
				soFinishGroup_1.sObjTexture = "FINISH_CROWD_01"
				soFinishGroup_1.vSpriteScale = INIT_VECTOR_2D(208.0, 240.0)
				soFinishGroup_1.fObjectFrequencyLeft = 15
				soFinishGroup_1.fObjectFrequencyRight = 22
				soFinishGroup_1.fOffsetFromCenter = 1700
				soFinishGroup_1.fYOffset = 0.2
				soFinishGroup_1.fZMin = fFinalZValue
				soFinishGroup_1.fZMax = -1
								
				soFinishGroup_2.objID = 0
				soFinishGroup_2.sObjDict = sDICT_FinishGroups
				soFinishGroup_2.sObjTexture = "FINISH_CROWD_02"
				soFinishGroup_2.vSpriteScale = INIT_VECTOR_2D(208.0, 240.0)
				soFinishGroup_2.fObjectFrequencyLeft = 22
				soFinishGroup_2.fObjectFrequencyRight = 15
				soFinishGroup_2.fOffsetFromCenter = 1710
				soFinishGroup_2.fYOffset = 0.2
				soFinishGroup_2.fZMin = fFinalZValue
				soFinishGroup_2.fZMax = -1
			
				pfasCurrentFinishState = FS1_CORRECT_X
			ENDIF
		
		BREAK
	
		CASE FS1_CORRECT_X 
				
			IF player.fPlayerSpeed < fMinSpeed
				player.fPlayerSpeed = player.fPlayerSpeed +@40
			ENDIF
			
			IF player.fPlayerSpeed > fMinSpeed
				player.fPlayerSpeed = player.fPlayerSpeed -@40
			ENDIF
			
			// Off to left
			IF fXCarOffset < -30
				 player.fPlayerLeftStickX = -0.5
			ENDIF
			
			// Off to right
			IF fXCarOffset > 30
				player.fPlayerLeftStickX = 0.5
			ENDIF
			
				
			IF fXCarOffset >= -30.0
			AND fXCarOffset <= 30.0
			AND player.fPlayerSpeed >= fMinSpeed - 10
			AND player.fPlayerSpeed <= fMinSpeed + 10
				player.fPlayerLeftStickX = 0.0
				fXCarOffset = 0.0
				
				player.fPlayerSpeed = fMinSpeed
				
				REQUEST_STREAMED_TEXTURE_DICT(sDICT_FinishLine_Final)
				REQUEST_STREAMED_TEXTURE_DICT(sDICT_FinishCrowd)
					
				pfasCurrentFinishState = FS2_BEGIN_BRAKE
			ENDIF
	
		BREAK
	
		CASE FS2_BEGIN_BRAKE
		
			player.fPlayerSpeed = player.fPlayerSpeed -@(25*1.25)
			
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
			ENDIF
			
			// Wait for crowd to be loaded
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_FinishCrowd)
				IF genericObjects[ENUM_TO_INT(G_CROWD)].objID = -1
				
					IF player.fPlayerSpeed <= fMinSpeed
						genericObjects[ENUM_TO_INT(G_CROWD)].objID = ENUM_TO_INT(G_CROWD)
						genericObjects[ENUM_TO_INT(G_CROWD)].sObjTexture = "POLICE_01"
						genericObjects[ENUM_TO_INT(G_CROWD)].sObjDict = "MPRoadArcade_FinishCrowd"
						genericObjects[ENUM_TO_INT(G_CROWD)].vSpriteScale = INIT_VECTOR_2D(1056.0, 340.0)
						genericObjects[ENUM_TO_INT(G_CROWD)].fYOffset = 0.2
						genericObjects[ENUM_TO_INT(G_CROWD)].fZDrawAt = fFinalZValue
						genericObjects[ENUM_TO_INT(G_CROWD)].fZWasDrawnAt = 0
					ENDIF
				
				ENDIF
			ENDIF
		
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_FinishLine_Final)			
				
				IF player.fPlayerSpeed <= 30
					player.psActiveState = ps_Finish // Set player into finish state
					pfasCurrentFinishState = FS3_Dift_To_Stop
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_MINIGAME, "ROAD_ARCADE_HANDLE_PLAYER_FINISH_ANIM - Loading finish line animations")
			ENDIF
			
		BREAK
		
		CASE FS3_Dift_To_Stop
			
			IF eRoadArcade_ActiveGameType = RA_GT_CAR
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_BIKE
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
			ENDIF
			
			IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
				ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
			ENDIF

			player.fPlayerSpeed = player.fPlayerSpeed -@(player.fDrag*2)
			
			IF player.fPlayerSpeed <= 30
			AND player.fPlayerSpeed > 25
						
				// Car frame
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
						sSlightT, 
						vSpritePos, 
						vSpriteScaleRevX, 
						0.0,
						rgbaOriginal)
					
				// Driver frame
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType != RA_GT_BIKE
					ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
							sSlightT_Driver, 
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)
				ENDIF	
			ENDIF
				
			IF player.fPlayerSpeed <= 25
			AND player.fPlayerSpeed > 20
			
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
						sMidT,
						vSpritePos, 
						vSpriteScaleRevX, 
						0.0,
						rgbaOriginal)
						
				// Driver frame
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType != RA_GT_BIKE
					ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
							sMidT_Driver, 
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)
				ENDIF
						
			ENDIF
			
			IF player.fPlayerSpeed <= 20
			AND player.fPlayerSpeed > 10
				ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
						sFarT,
						vSpritePos, 
						vSpriteScaleRevX, 
						0.0,
						rgbaOriginal)
						
				// Driver frame
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType != RA_GT_BIKE
					ARCADE_DRAW_PIXELSPACE_SPRITE(player.sSpriteDict,
							sFarT_Driver, 
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)
				ENDIF
			ENDIF
			
			IF player.fPlayerSpeed <= 10
			AND player.fPlayerSpeed > 5
			
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType = RA_GT_BIKE
					vSpriteScale = INIT_VECTOR_2D(752, 288)
				ENDIF
			
				ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
					sFinishSlide1,
					vSpritePos, 
					vSpriteScale, 
					0.0,
					rgbaOriginal)
					
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType != RA_GT_BIKE
					ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
						sFinishSlide1_Driver,
						vSpritePos, 
						vDriverScale, 
						0.0,
						rgbaOriginal)
				ENDIF
			ENDIF
			
			IF player.fPlayerSpeed  <= 5
			AND player.fPlayerSpeed > 2.5
			
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType = RA_GT_BIKE
					vSpriteScale = INIT_VECTOR_2D(752, 288)
				ENDIF
			
				ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
					sFinishSlide2,
					vSpritePos, 
					vSpriteScale, 
					0.0,
					rgbaOriginal)
					
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType != RA_GT_BIKE
					ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
						sFinishSlide2_Driver,
						vSpritePos, 
						vDriverScale, 
						0.0,
						rgbaOriginal)
				ENDIF
				
			ENDIF
				
			IF player.fPlayerSpeed < 2.5
			
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType = RA_GT_BIKE
					vSpriteScale = INIT_VECTOR_2D(752, 288)
				ENDIF
			
				ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
					sFinishSlide3,
					vSpritePos, 
					vSpriteScale,
					0.0,
					rgbaOriginal)
					
				IF iPlayerSeatLocalID =  0
				AND eRoadArcade_ActiveGameType != RA_GT_BIKE	
					ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
						sFinishSlide3_Driver,
						vSpritePos, 
						vDriverScale, 
						0.0,
						rgbaOriginal)
				ENDIF
			ENDIF
				
			IF player.fPlayerSpeed <= 1.0
			
				SETTIMERA(0)
				SETTIMERB(0)
				
				dafCurrentDogFrame = DOG_FRAME_02
			
				player.fPlayerSpeed = 0.0
				
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_BIKE
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
				ENDIF
				
				IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
					ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
				ENDIF
				
				pfasCurrentFinishState = FS4_Dog_Loop
									
			ENDIF
					
		BREAK
		
		CASE FS4_Dog_Loop
		CASE FS5_Trophy
			vSpriteScale.x = ABSF(vSpriteScale.x) // Correct finish anim
		
			ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_WORLD_CROWD_LOOP)
			
			IF iPlayerSeatLocalID =  0
			AND eRoadArcade_ActiveGameType = RA_GT_BIKE
				vSpriteScale = INIT_VECTOR_2D(752, 288)
				vSpritePos.y += 24 // Offset by 24 pixels
			ENDIF
					
			
			ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
					sFinishSlide3,
					vSpritePos, 
					vSpriteScale, 
					0.0,
					rgbaOriginal)
			
			// Only display dog sprites under these conditions
			IF iPlayerSeatLocalID =  0
			AND eRoadArcade_ActiveGameType != RA_GT_BIKE
	
				IF eRoadArcade_ActiveGameType = RA_GT_CAR
					vSpritePos.y += 6
					vSpritePos.x += 16
				ENDIF
	
				SWITCH(dafCurrentDogFrame)
				
					CASE DOG_FRAME_02
					
						ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							sDogAnim2,
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)
					
						IF TIMERA() >= 500
							dafCurrentDogFrame = DOG_FRAME_03	
							SETTIMERA(0)
						ENDIF
					
					BREAK
				
					CASE DOG_FRAME_03
					
						ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							sDogAnim3,
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)	
							
						IF TIMERA() >= 500
							fTrophyGiverX = 2000.0
							trophyGive = TROPHY_WALK
							pfasCurrentFinishState = FS5_Trophy
							dafCurrentDogFrame = DOG_FRAME_04	
							SETTIMERA(0)
						ENDIF
					
					BREAK
					
					CASE DOG_FRAME_04
					
						ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							sDogAnim4_Frame1,
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)	
					
						IF TIMERA() >= 500
							dafCurrentDogFrame = DOG_FRAME_05
							CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "DOG FRAME 02: ", sDogAnim4_Frame2)
							SETTIMERA(0)
						ENDIF
					
					BREAK
				
					CASE DOG_FRAME_05
					
						ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							sDogAnim4_Frame2,
							vSpritePos, 
							vDriverScale, 
							0.0,
							rgbaOriginal)	
					
						IF TIMERA() >= 500
							dafCurrentDogFrame = DOG_FRAME_04	
							CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "DOG FRAME 01: ", sDogAnim4_Frame1)
							SETTIMERA(0)
						ENDIF
					
					BREAK
				
				ENDSWITCH
			
			ELSE
				IF TIMERA() >= 500
				AND dafCurrentDogFrame != DOG_FRAME_04
					fTrophyGiverX = 2000.0
					trophyGive = TROPHY_WALK
					pfasCurrentFinishState = FS5_Trophy
					dafCurrentDogFrame = DOG_FRAME_04	
					SETTIMERA(0)
				ENDIF
			ENDIF
			
			// Trophy Girl goes infront of the car
			IF pfasCurrentFinishState = FS5_Trophy
				SWITCH (trophyGive)
					
					CASE TROPHY_WALK
						
						trophyGirlAnim += (0.0+@1000.0)
						
						IF trophyGirlAnim < 250
							
							ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							"GENERIC_TROPHY_GIRL_01_FRAME_01",
							INIT_VECTOR_2D(fTrophyGiverX, TO_FLOAT(player.iPlayerPosY + 24)), 
							INIT_VECTOR_2D(260,260), 
							0.0,
							rgbaOriginal)
							
						ELSE
							
							ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							"GENERIC_TROPHY_GIRL_01_FRAME_02",
							INIT_VECTOR_2D(fTrophyGiverX, TO_FLOAT(player.iPlayerPosY + 24)), 
							INIT_VECTOR_2D(260,260), 
							0.0,
							rgbaOriginal)
							
						ENDIF
						
						IF trophyGirlAnim >= 500.0
							trophyGirlAnim = 0.0
						ENDIF
						
						IF fTrophyGiverX > (cfBASE_SCREEN_WIDTH*0.6)
							fTrophyGiverX -= 0+@(600)
						ELSE
							trophyGive = TROPHY_STOP
						ENDIF
						
					BREAK
					
					CASE TROPHY_STOP
					
						// Solid frame
						ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_FinishLine_Final,
							"GENERIC_TROPHY_GIRL_01_FRAME_03",
							INIT_VECTOR_2D(fTrophyGiverX, TO_FLOAT(player.iPlayerPosY + 24)), 
							INIT_VECTOR_2D(260,260),
							0.0,
							rgbaOriginal)
					
					BREAK
				
				ENDSWITCH
				
				IF TIMERB() >= 5000
					
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_CAR)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_CAR)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_CAR)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_CAR)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_BIKE)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_BIKE)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_BIKE)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_BIKE)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_ENGINE_TRUCK)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_BRAKE_TRUCK)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPF_TRUCK)
						ARCADE_GAMES_SOUND_STOP(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_TURN_LOOPS_TRUCK)
					ENDIF
					
					// Unload finish anims
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDICT_FinishLine_Final)
					
					ROAD_ARCADE_GO_TO_LEADERBOARD(player)
				ENDIF
			ENDIF
		

					
		BREAK
			
	ENDSWITCH

ENDPROC

FUNC BOOL ROAD_ARCADE_ARE_NON_LOOPING_ANIMS_FINISHED()

	BOOL bToReturn = TRUE
	
	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD, TRUE)
		bToReturn = FALSE
	ENDIF
	
	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_ENTER_B, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_ENTRY, TRUE)
		bToReturn = FALSE
	ENDIF
	
	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_EXIT, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_IDLE_TO_RACE, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_TO_RACE_V2, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_WIN, TRUE)
		bToReturn = FALSE
	ENDIF

	IF NOT HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_LOSE, TRUE)
		bToReturn = FALSE
	ENDIF
	
	return bToReturn

ENDFUNC

PROC ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS(PLAYER_VEHICLE &player)
	BOOL bBike = FALSE
	INT iRandomAnimClip 
	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
		bBike = TRUE
	ENDIF
	
	SWITCH player.ePlayerPedAnimState
		CASE RA_PLAYER_PED_ANIM_IDLE
			IF (!IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay))
			OR (bBike AND IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
			AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay))
				player.ae_PedAnimData.bLoopAnim = TRUE
				player.ae_PedAnimData.bHoldLastFrame = FALSE
				IF !bBike
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
				ELSE
					iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 4)
					SWITCH iRandomAnimClip
						CASE 0
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
						BREAK
						CASE 1
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V2	
						BREAK
						CASE 2
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V3	
						BREAK
						CASE 3
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V4	
						BREAK
					ENDSWITCH	
					
					IF player.previousIdleAnim != player.animationToPlay 
						PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
					ENDIF	
				ENDIF
			ENDIF
		BREAK

		CASE RA_PLAYER_PED_ANIM_START_GAME
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
			//AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
				player.ae_PedAnimData.bLoopAnim = FALSE
				player.ae_PedAnimData.bHoldLastFrame = TRUE
				
				IF !bBike
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_IDLE_TO_RACE
				ELSE
					iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRandomAnimClip
						CASE 0
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_IDLE_TO_RACE
						BREAK
						CASE 1
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_TO_RACE_V2
						BREAK
					ENDSWITCH
				ENDIF	
				SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IN_GAME)
				RESET_NET_TIMER(player.sIdleAnimTimer)
				PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)	
			ENDIF	
		BREAK
		
		CASE RA_PLAYER_PED_ANIM_IN_GAME

			player.ae_PedAnimData.bLoopAnim = TRUE
			player.ae_PedAnimData.bHoldLastFrame = FALSE
			
			IF NOT IS_BIT_SET(player.iAnimBitSet, ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM)
				IF !bBike
					iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 8)
					SWITCH iRandomAnimClip
						CASE 0
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_A 
						BREAK
						CASE 1
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_B 
						BREAK
						CASE 2
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_C 
						BREAK	
						CASE 3
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_D
						BREAK
						CASE 4
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_E 
						BREAK
						CASE 5	
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_F
						BREAK
						CASE 6
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_G
						BREAK
						CASE 7
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_H
						BREAK
					ENDSWITCH
					
					IF HAS_NET_TIMER_STARTED(player.sIdleAnimTimer)
						IF HAS_NET_TIMER_EXPIRED(player.sIdleAnimTimer, 20000)
							IF !bBike
								iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 2)
								SWITCH iRandomAnimClip
									CASE 0
										player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
									BREAK
									CASE 1	
										player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
									BREAK
								ENDSWITCH	
							ENDIF
						ENDIF
					ENDIF	
				ELSE
					iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRandomAnimClip
						CASE 0
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING 
						BREAK
						CASE 1
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_RACING_V2	 
						BREAK
						CASE 2
							player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_RACING_V3 
						BREAK	
					ENDSWITCH
					
					IF HAS_NET_TIMER_STARTED(player.sIdleAnimTimer)
						IF HAS_NET_TIMER_EXPIRED(player.sIdleAnimTimer, 20000)
							iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 4)
							SWITCH iRandomAnimClip
								CASE 0
									player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
								BREAK
								CASE 1
									player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_TAKE_LEAD_V2	
								BREAK
								CASE 2
									player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
								BREAK
								CASE 3
									player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD_V2	
								BREAK
							ENDSWITCH	
						ENDIF
					ENDIF	
				ENDIF
				
				IF player.previousIdleAnim != player.animationToPlay 
					IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.ae_PedAnimData.ePreviousAnimClip)
					AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.ae_PedAnimData.ePreviousAnimClip)
						player.previousIdleAnim = player.animationToPlay
						PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
						
						IF NOT HAS_NET_TIMER_STARTED(player.sIdleAnimTimer)
							START_NET_TIMER(player.sIdleAnimTimer)
							PRINTLN("[road_arcade] ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS start sIdleAnimTimer timer")
						ELSE
							IF player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
							OR player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
							OR player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
							OR player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_TAKE_LEAD_V2	
							OR player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
							OR player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD_V2	
								RESET_NET_TIMER(player.sIdleAnimTimer)
								PRINTLN("[road_arcade] ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS reset sIdleAnimTimer timer")
							ENDIF
						ENDIF
						
						SET_BIT(player.iAnimBitSet, ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM)
						PRINTLN("[road_arcade] ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM TRUE")
					ENDIF	
				ENDIF	
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					CLEAR_BIT(player.iAnimBitSet, ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM)
					PRINTLN("[road_arcade] ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM FALSE")
				ENDIF	
			ENDIF
		BREAK
		
		CASE RA_PLAYER_PED_ANIM_TAKE_LEAD
		CASE RA_PLAYER_PED_ANIM_HIT_CHECKPOINT
			IF player.animationToPlay != ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
			AND player.ae_PedAnimData.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD	
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = FALSE
					player.ae_PedAnimData.bHoldLastFrame = TRUE
					IF !bBike
						player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
					ELSE
						iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 2)
						SWITCH iRandomAnimClip
							CASE 0
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD
							BREAK
							CASE 1
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_TAKE_LEAD_V2	
							BREAK
						ENDSWITCH	
					ENDIF	
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
					RESET_NET_TIMER(player.sIdleAnimTimer)
				ENDIF	
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					CLEAR_BIT(player.iAnimBitSet, ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM)
					PRINTLN("[road_arcade] ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM FALSE played take_lead move back to racing FALSE")
					SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IN_GAME)
				ENDIF	
			ENDIF
		BREAK
	
		CASE RA_PLAYER_PED_ANIM_LOSE_LEAD
		CASE RA_PLAYER_PED_ANIM_CRASH_VEH
			IF player.animationToPlay != ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
			AND player.ae_PedAnimData.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD	
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = FALSE
					player.ae_PedAnimData.bHoldLastFrame = TRUE
					IF !bBike
						player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
					ELSE
						iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 2)
						SWITCH iRandomAnimClip
							CASE 0
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD
							BREAK
							CASE 1
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD_V2	
							BREAK
						ENDSWITCH	
					ENDIF	
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
					RESET_NET_TIMER(player.sIdleAnimTimer)
				ENDIF	
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					CLEAR_BIT(player.iAnimBitSet, ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM)
					PRINTLN("[road_arcade] ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS ARCADE_ROAD_LOCAL_ANIM_BS_PICK_RANDOM_IDLE_ANIM FALSE played lose lead move back to racing FALSE")
					SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IN_GAME)
				ENDIF	
			ENDIF
		BREAK
	
		CASE RA_PLAYER_PED_ANIM_WIN
			IF player.animationToPlay != ARCADE_CABINET_ANIM_CLIP_WIN	
			AND player.ae_PedAnimData.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_WIN	
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = FALSE
					player.ae_PedAnimData.bHoldLastFrame = TRUE
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_WIN	
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
					RESET_NET_TIMER(player.sIdleAnimTimer)
				ENDIF	
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = TRUE
					player.ae_PedAnimData.bHoldLastFrame = FALSE
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					IF !bBike
						SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IDLE)
					ELSE
						iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 4)
						SWITCH iRandomAnimClip
							CASE 0
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
							BREAK
							CASE 1
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V2	
							BREAK
							CASE 2
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V3	
							BREAK
							CASE 3
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V4	
							BREAK
						ENDSWITCH	
						SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IDLE)
					ENDIF	
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
				ENDIF	
			ENDIF
		BREAK
	
		CASE RA_PLAYER_PED_ANIM_LOSE
			IF player.animationToPlay != ARCADE_CABINET_ANIM_CLIP_LOSE	
			AND player.ae_PedAnimData.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_LOSE	
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = FALSE
					player.ae_PedAnimData.bHoldLastFrame = TRUE
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_LOSE				
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
					RESET_NET_TIMER(player.sIdleAnimTimer)
				ENDIF	
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = TRUE
					player.ae_PedAnimData.bHoldLastFrame = FALSE
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					IF bBike
						iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 4)
						SWITCH iRandomAnimClip
							CASE 0
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
							BREAK
							CASE 1
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V2	
							BREAK
							CASE 2
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V3	
							BREAK
							CASE 3
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V4	
							BREAK
						ENDSWITCH	
					ENDIF	
					SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IDLE)
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
				ENDIF	
			ENDIF	
		BREAK
		
		CASE RA_PLAYER_PED_ANIM_BACK_TO_IDLE
			IF player.animationToPlay != ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = FALSE
					player.ae_PedAnimData.bHoldLastFrame = TRUE
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
					RESET_NET_TIMER(player.sIdleAnimTimer)
				ENDIF	
			ELSE
				IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(player.animationToPlay)
				AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(player.animationToPlay)
					player.ae_PedAnimData.bLoopAnim = TRUE
					player.ae_PedAnimData.bHoldLastFrame = FALSE
					player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
					IF !bBike
						SET_ROAD_ARCADE_ANIM_STATE(player, RA_PLAYER_PED_ANIM_IDLE)
					ELSE
						iRandomAnimClip = GET_RANDOM_INT_IN_RANGE(0, 4)
						SWITCH iRandomAnimClip
							CASE 0
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_IDLE
							BREAK
							CASE 1
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V2	
							BREAK
							CASE 2
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V3	
							BREAK
							CASE 3
								player.animationToPlay = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V4	
							BREAK
						ENDSWITCH	
					ENDIF	
					PLAY_ARCADE_CABINET_ANIMATION(player.ae_PedAnimData, player.animationToPlay)
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL ROAD_ARCADE_STREAM_AUDIO()

	Bool bReady = TRUE

	IF NOT ARCADE_GAMES_SOUND_REQUESTING_AUDIO_BANK("DLC_HEIST3/H3_RaceNChase")
		bReady = FALSE
	ENDIF
	
	IF NOT ARCADE_GAMES_SOUND_REQUESTING_AUDIO_BANK("DLC_HEIST3/H3_RaceNChase_Music_Previews")
		bReady = FALSE
	ENDIF
		
	RETURN bReady
ENDFUNC


// ---------------- TRANSITION EFFECT ---------------

FUNC STAGES_SETS ROAD_ARCADE_GET_NEXT_STAGE(STAGES_SETS currentStage, BOOL bReverse)
	
	IF currentStage = STAGE_VICE_CITY
		
		IF !bReverse
			RETURN STAGE_SAN_FIERRO
		ELSE
			RETURN STAGE_LIBERTY_CITY
		ENDIF
		
	ENDIF

	IF currentStage = STAGE_SAN_FIERRO
	
		IF !bReverse
			RETURN STAGE_SAN_ANDREAS
		ELSE
			RETURN STAGE_VICE_CITY
		ENDIF
		
	ENDIF

	IF currentStage = STAGE_SAN_ANDREAS
	
		IF !bReverse
			RETURN STAGE_LAS_VENTURAS
		ELSE
			RETURN STAGE_SAN_FIERRO
		ENDIF
		
	ENDIF

	IF currentStage = STAGE_LAS_VENTURAS
	
		IF !bReverse
			RETURN STAGE_LIBERTY_CITY
		ELSE
			RETURN STAGE_SAN_ANDREAS
		ENDIF
		
	ENDIF

	IF currentStage = STAGE_LIBERTY_CITY
	
		IF !bReverse
			RETURN STAGE_VICE_CITY
		ELSE
			RETURN STAGE_LAS_VENTURAS
		ENDIF
		
	ENDIF

	RETURN currentStage

ENDFUNC


PROC ROAD_ARCADE_HANDLE_STAGE_TRANSITION(PLAYER_VEHICLE &player, STAGES_SETS &currentStage, STAGES_SETS &stageToTransitionTo, STAGES_SETS firstStage, STAGES_SETS finalStage)
	
	//Float ticker = 0 // Value to add to the offsets.
	
	SWITCH(transitionData.transition_ActiveState)
	
		CASE T_INACTIVE

			IF fFinalZValue < ROAD_ARCADE_GET_END_OF_STAGE(currentStage)
				EXIT
			ENDIF
			
			IF currentStage != finalStage

				IF genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].objID = -1
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].objID = ENUM_TO_INT(G_TRANSITION_CHECK)
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].sObjTexture = "CHECKPOINT_01"
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].sObjDict = sDICT_GenericAssets
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].vSpriteScale = INIT_VECTOR_2D(1264.0, 620.0)
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fYOffset = 0
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZDrawAt = fFinalZValue
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZWasDrawnAt = 0
				ENDIF
							
				IF genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZWasDrawnAt != 0
				AND player.fZValue >= genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZDrawAt
					fGameTimerElapsed -= 25000
					genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].objID = -1
					player.ePlayerPedAnimState = RA_PLAYER_PED_ANIM_HIT_CHECKPOINT
					ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_CHECKPOINT_HIT)
					
					ROAD_ARCADE_TRANSITION_DATA empty
					empty.transition_ActiveState = T_WAIT_FOR_BACKGROUND
					transitionData = empty
					
					ROAD_ARCADE_INIT_TICKER_OBJ(sDICT_HUDGeneric, "HUD_BUTTON_EXTENDED", INIT_VECTOR_2D(260, 64))
					
				ENDIF
					
			ENDIF
			
			IF currentStage = finalStage
				
				genericObjects[ENUM_TO_INT(G_TRANSITION_CHECK)].fZDrawAt = -1
				
				genericObjects[ENUM_TO_INT(G_FINISH)].objID = ENUM_TO_INT(G_FINISH)
				genericObjects[ENUM_TO_INT(G_FINISH)].sObjTexture = "FINISH_BANNER_01"	
				genericObjects[ENUM_TO_INT(G_FINISH)].vSpriteScale = INIT_VECTOR_2D(1264.0, 540.0)
				genericObjects[ENUM_TO_INT(G_FINISH)].fYOffset = 0
				genericObjects[ENUM_TO_INT(G_FINISH)].fZDrawAt = fFinalZValue
				genericObjects[ENUM_TO_INT(G_FINISH)].bIsFinish = TRUE
				
				ROAD_ARCADE_TRANSITION_DATA empty
				empty.transition_ActiveState = T_DRAW_FINISH
				transitionData = empty
								
				EXIT
			ENDIF
			
			
		BREAK
		
		CASE T_WAIT_FOR_BACKGROUND
				
				
			transitionData.fTransitionOffset += ((0.0+@player.fPlayerSpeed)/2.0)
			transitionData.fTransitionProgress = (transitionData.fTransitionOffset/(cfBASE_SCREEN_HEIGHT/2))
			transitionData.fTransitionProgress = SQRT(transitionData.fTransitionProgress)
			//transitionData.fTransitionProgress += (0.0+@150) // Start curvature early
						
			IF transitionData.fTransitionProgress > 1.0
				transitionData.fTransitionProgress = 1.0
			ENDIF
						
			IF transitionData.fTransitionOffset >= (cfBASE_SCREEN_HEIGHT/2)
				
				transitionData.fTransitionOffset = (cfBASE_SCREEN_HEIGHT/2)
									
				REQUEST_STREAMED_TEXTURE_DICT(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stageToTransitionTo))
									
				transitionData.transition_ActiveState = T_LOAD
				
			ENDIF
			
								
		BREAK
		
		CASE T_LOAD
					
			transitionData.fTransitionProgress = 1.0
					
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stageToTransitionTo))			
				SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(currentStage))
				
				// Update progress and stage
				currentStage = stageToTransitionTo
				stageToTransitionTo = ROAD_ARCADE_GET_NEXT_STAGE(currentStage, FALSE)
				ROAD_ARCADE_INIT_STAGE_SIDE_OBJ(currentStage)
				ROAD_ARCADE_INIT_BACKGROUND_DATA(currentStage)
				ROAD_ARCADE_INIT_GENERIC_VEH(currentStage)
				ROAD_ARCADE_INIT_STAGE_CROSSING_OBJ(currentStage)
				ROAD_ARCADE_INIT_RIVAL_VEH(currentStage)
				STAGE_OV_INIT(currentStage)
				GENERIC_OV_INIT(currentStage, firstStage) // Reinit
				
				transitionData.fZTransitionCutOff = 150 // We wait a screen before drawing objects again
				
				// Passed a level.
				sRoadArcadeTelemetry.level++
								
				ROAD_ARCADE_INIT_TICKER_OBJ(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(currentStage), "GENERIC_FRAME_01_ROAD_SIGN", INIT_VECTOR_2D(512, 188))
				transitionData.transition_ActiveState = T_WAIT_FOR_RETURN
			ENDIF
			
		BREAK
		
		CASE T_WAIT_FOR_RETURN
					
			transitionData.fTransitionOffset -= (0.0+@player.fPlayerSpeed)
						
			transitionData.fTransitionProgress = (transitionData.fTransitionOffset/(cfBASE_SCREEN_HEIGHT/2))
			transitionData.fTransitionProgress = SQRT(transitionData.fTransitionProgress)
						
			IF transitionData.fTransitionProgress < 0
				transitionData.fTransitionProgress = 0
			ENDIF
			
			IF transitionData.fTransitionOffset <= 0
			
				transitionData.fTransitionOffset = 0
							
				// Clean up
				player.psActiveState = ps_Drive // Return control to the player
			
				fJOURNEY_DistanceTravelled = 0.1 // Reset the journey counter
				transitionData.bSwitchCurve = !transitionData.bSwitchCurve // Switch transtion side
				transitionData.transition_ActiveState = T_INACTIVE
			
			ENDIF
			
		BREAK

	
	ENDSWITCH

	
	transitionData.fTransitionOffset_Final = transitionData.fTransitionOffset-(transitionData.fTransitionOffset%4)
ENDPROC

PROC ROAD_ARCADE_NET_DRAW_CHALLENGER_ICON()

	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_HUDGeneric)
		EXIT
	ENDIF
	
	BOOL bChallengeMade = FALSE
				
	IF !bChallengeMade
		bChallengeMade = IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[0].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
	ENDIF
	
	IF !bChallengeMade
		bChallengeMade = IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[1].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
	ENDIF
	
	IF !bChallengeMade 
	
		ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_HUDGeneric, 
			"HUD_BUTTON_CHALLENGER", INIT_VECTOR_2D(916, 152.0), INIT_VECTOR_2D(232, 64), 0.0, rgbaOriginal)
				
	ELSE
		
		IF (challengerIconFlash) > 2000
			challengerIconFlash = 0
		ELSE
			challengerIconFlash += (0.0+@1000.0)
		ENDIF
		
		IF (challengerIconFlash) > 1000
			ROAD_ARCADE_DRAW_FORMATTED_SPRITE_NON_STRUCT(sDICT_HUDGeneric, 
				"HUD_BUTTON_CHALLENGER", INIT_VECTOR_2D(916, 152.0), INIT_VECTOR_2D(232, 64), 0.0, rgbaOriginal)
		ENDIF
				
	ENDIF

ENDPROC

// Telemetry data
PROC ROAD_ARCADE_ASIGN_FINAL_TELEMETRY_DATA(PLAYER_VEHICLE &player)
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		sRoadArcadeTelemetry.gameType =  HASH("ARCADE_CABINET_CH_NIGHT_DRIVE_CAR")
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		sRoadArcadeTelemetry.gameType =  HASH("ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE")
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		sRoadArcadeTelemetry.gameType =  HASH("ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK")
	ENDIF
		
	sRoadArcadeTelemetry.matchId = -1
	
	IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
		sRoadArcadeTelemetry.numPlayers = 2
	ELSE
		sRoadArcadeTelemetry.numPlayers = 1
	ENDIF
	
	sRoadArcadeTelemetry.score = player.iPlayerScore
	sRoadArcadeTelemetry.powerUps = -1
				
	// Player won the trophy this session.
	IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TRAFFICAVOI)
	AND GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_CANTCATCHBRA)
	AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACECHAMP) = 40
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_RACE_CHASE, DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID()))
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_ROAD_ARCADE_COMPLETE)
		sRoadArcadeTelemetry.reward = ARCADE_GAME_GET_TROPHY_ID_FOR_TELEMETRY(ARCADE_GAME_TROPHY_RACE_CHASE)
	ELSE
		sRoadArcadeTelemetry.reward = 0
	ENDIF
	
	sRoadArcadeTelemetry.timePlayed = ROUND(fRunTimer)
	sRoadArcadeTelemetry.matchId = roadArcadeServerBd.iMatchId
	CDEBUG3LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "challenges final value", sRoadArcadeTelemetry.challenges)
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			sRoadArcadeTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_AUTO_SHOP")
		ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			sRoadArcadeTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_ARCADE")
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_ROAD_HELPERS] [ROAD_ARCADE_ASIGN_FINAL_TELEMETRY_DATA] not in auto shop or arcade, setting sRoadArcadeTelemetry.location = 0")
			sRoadArcadeTelemetry.location = 0
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[ARCADE_ROAD_HELPERS] [ROAD_ARCADE_ASIGN_FINAL_TELEMETRY_DATA] Invalid Player ID, setting sRoadArcadeTelemetry.location = 0")
		sRoadArcadeTelemetry.location = 0
	ENDIF
	
	PLAYSTATS_ARCADE_CABINET(sRoadArcadeTelemetry)

	STRUCT_ARCADE_CABINET empty
	sRoadArcadeTelemetry = empty

ENDPROC

// url:bugstar:6195081 - Ignorable Assert Chain x2 - [script] Error: Assertf(0) FAILED: CDoubleBufferedIntroRects::GetNextFree - have already drawn 500 release rectangles this frame  ...from Updating script road_arcade
PROC RESET_lbCurrentEntry()
	RA_TAG_DATA empty
	lbCurrentEntry = empty
ENDPROC

STRING sPlayerOneTag
STRING sPlayerTwoTag

PROC ROAD_ARCADE_SET_PLAYER_RANK_TAGS()

	IF IS_STRING_NULL_OR_EMPTY(sPlayerOneTag)
		IF NOT IS_STRING_NULL_OR_EMPTY(roadArcadePlayerBd[0].sPlayerID)
			sPlayerOneTag = TEXT_LABEL_TO_STRING(roadArcadePlayerBd[0].sPlayerID)
		ENDIF
	ENDIF

	IF IS_STRING_NULL_OR_EMPTY(sPlayerTwoTag)
		IF NOT IS_STRING_NULL_OR_EMPTY(roadArcadePlayerBd[1].sPlayerID)
			sPlayerTwoTag = TEXT_LABEL_TO_STRING(roadArcadePlayerBd[1].sPlayerID)
		ENDIF
	ENDIF

ENDPROC

// Function to display the Rank of the player
PROC ROAD_ARCADE_NET_RENDER_PLAYER_RANK()

	ROAD_ARCADE_SET_PLAYER_RANK_TAGS()
	
	IF IS_STRING_NULL_OR_EMPTY(sPlayerOneTag)
		EXIT
	ENDIF

	IF IS_STRING_NULL_OR_EMPTY(sPlayerTwoTag)
		EXIT
	ENDIF

	RA_WRITE_STRING_USING_TEXT_SPRITES("1ST" , INIT_VECTOR_2D(392, 424), DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3)
	RA_WRITE_STRING_USING_TEXT_SPRITES("2ND" , INIT_VECTOR_2D(392, 506), DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3)

	// PIck the string below

	IF roadArcadePlayerBd[0].fPlayerProgress >= roadArcadePlayerBd[1].fPlayerProgress
		// First player is ahead
		RA_WRITE_STRING_USING_TEXT_SPRITES(sPlayerOneTag, INIT_VECTOR_2D(534, 424), DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3)
		RA_WRITE_STRING_USING_TEXT_SPRITES(sPlayerTwoTag, INIT_VECTOR_2D(534, 506), DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3)
	ELSE // 2nd player is ahead
		RA_WRITE_STRING_USING_TEXT_SPRITES(sPlayerTwoTag, INIT_VECTOR_2D(534, 424), DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3)
		RA_WRITE_STRING_USING_TEXT_SPRITES(sPlayerOneTag, INIT_VECTOR_2D(534, 506), DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3)
	ENDIF
	
	
ENDPROC

// New scaleform buttons for Standards
FUNC SPRITE_PLACEMENT ROAD_ARCADE_GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()

	SPRITE_PLACEMENT aSprite
	
	//Scaleform movie Bottom
	aSprite.x = 0.5 //389 // Move to edge of safezone (910150)
	aSprite.y = 0.5
	aSprite.w = 1.0
	aSprite.h = 1.0
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 200
	aSprite.fRotation = 0
	
	RETURN aSprite

ENDFUNC

PROC ROAD_ARCADE_HANDLE_SCALEFORM_BUTTONS()

	SCALEFORM_INDEX rncScaleformInstructionalButtonsIndex
	SCALEFORM_INSTRUCTIONAL_BUTTONS rncScaleformInstructionalButtons
	SPRITE_PLACEMENT thisSpritePlacement = ROAD_ARCADE_GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()

	IF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON("","RNC_BUTTON_WAIT",rncScaleformInstructionalButtons)
	ELIF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(PARTICIPANT_ID_TO_INT())].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE),"RNC_BUTTON_ACCE",rncScaleformInstructionalButtons)
	ELSE
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE),"RNC_BUTTON_SEND",rncScaleformInstructionalButtons)
	ENDIF
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(rncScaleformInstructionalButtonsIndex, thisSpritePlacement, rncScaleformInstructionalButtons)

ENDPROC

