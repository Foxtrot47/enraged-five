USING "degenatron_games_using.sch"
USING "degenatron_games_common.sch"
USING "degenatron_games_animation.sch"

CONST_FLOAT cfDG_MONKEY_GROUND_BASE_HEIGHT 		100.0
CONST_FLOAT cfDG_MONKEY_SKY_HEIGHT 				180.0

CONST_FLOAT cfDG_MONKEY_TREE_HEIGHT				78.86968085
CONST_FLOAT cfDG_MONKEY_TREE_WIDHT 				100.0
CONST_INT 	ciDG_MONKEY_TREE_AMOUNT 			5

CONST_FLOAT cfDG_MONKEY_PLAYER_HEIGHT			94.64361702
CONST_FLOAT cfDG_MONKEY_PLAYER_WIDHT 			120.0
CONST_INT	ciDG_MONKEY_PLAYER_SWING_TIME		500
CONST_FLOAT	cfDG_MONKEY_PLAYER_TRAVEL_SPEED		1.0

CONST_INT 	ciDG_MONKEY_TREE_SCORE 				10

CONST_INT 	ciDG_MONKEY_UNDERWATER_LEVEL		20


ENUM DG_MONKEY_STATE_LEVEL
	DG_MONKEY_STATE_LEVEL_INIT,
	DG_MONKEY_STATE_LEVEL_UPDATE,
	DG_MONKEY_STATE_LEVEL_DEAD,
	DG_MONKEY_STATE_LEVEL_WAIT_TIME,
	DG_MONKEY_STATE_LEVEL_END
ENDENUM

FUNC STRING DG_MONKEY_STATE_LEVEL_TO_STRING(DG_MONKEY_STATE_LEVEL eEnum)
	SWITCH eEnum
		CASE DG_MONKEY_STATE_LEVEL_INIT 			RETURN "LEVEL_INIT"
		CASE DG_MONKEY_STATE_LEVEL_UPDATE 			RETURN "LEVEL_UPDATE"
		CASE DG_MONKEY_STATE_LEVEL_DEAD 			RETURN "LEVEL_DEAD"
		CASE DG_MONKEY_STATE_LEVEL_WAIT_TIME		RETURN "LEVEL_WAIT_TIME"
		CASE DG_MONKEY_STATE_LEVEL_END 				RETURN "LEVEL_END"
	ENDSWITCH
	RETURN "***INVALID***"
ENDFUNC

STRUCT DG_MONKEY_LEVEL_DATA
	DG_MONKEY_STATE_LEVEL 	eLevelState
	FLOAT					fTravelRate
	INT						iCurrentTree
	INT						iDesiredTree
	INT						iTimeSwing
	INT						iTimeTree = -HIGHEST_INT
	BOOL					bTravelling
	BOOL					bFail
	BOOL					bWaterLevelReached
	BOOL					bWaterLevelPassed
ENDSTRUCT

STRUCT DG_MONKEY_DATA
	BOOL							bAquaApeAward = FALSE
	FLOAT							fTreeHeightPos[ciDG_MONKEY_TREE_AMOUNT]
	DG_MONKEY_LEVEL_DATA			sLevelData
	
ENDSTRUCT

DG_MONKEY_DATA sDGMonkeyData

//**************************************************//
//*            DATA ACCESSORS - MONKEY             *//
//**************************************************//

// SKY //
FUNC FLOAT DG_MONKEY_GET_SKY_HEIGHT
	RETURN cfDG_MONKEY_SKY_HEIGHT
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_SKY_POSITION
	VECTOR_2D screenPosition = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	screenPosition.y -= DEGENATRON_GAMES_GET_SCREEN_HEIGHT()/2 - DG_MONKEY_GET_SKY_HEIGHT()/2 - DEGENATRON_GAMES_GET_SCORE_HEIGHT()
	RETURN screenPosition
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_SKY_SIZE
	RETURN INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH*2,DG_MONKEY_GET_SKY_HEIGHT())
ENDFUNC

// TREES //
FUNC VECTOR_2D DG_MONKEY_TREE_POSITION(INT iTree, INT iOrder)
	VECTOR_2D treePosition
	IF NOT sDGMonkeyData.sLevelData.bTravelling AND sDGMonkeyData.sLevelData.fTravelRate > 0.0
		treePosition.x = -DEGENATRON_GAMES_GET_SCREEN_WIDTH()/7 + DEGENATRON_GAMES_GET_SCREEN_WIDTH()/7*(iOrder)*2 - DEGENATRON_GAMES_GET_SCREEN_WIDTH()/7*2*(1-sDGMonkeyData.sLevelData.fTravelRate)
	ELSE
		treePosition.x = -DEGENATRON_GAMES_GET_SCREEN_WIDTH()/7 + DEGENATRON_GAMES_GET_SCREEN_WIDTH()/7*iOrder*2
	ENDIF
	treePosition.y = sDGMonkeyData.fTreeHeightPos[iTree%ciDG_MONKEY_TREE_AMOUNT]
	RETURN treePosition
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_TREE_SIZE
	RETURN INIT_VECTOR_2D(cfDG_MONKEY_TREE_WIDHT,cfDG_MONKEY_TREE_HEIGHT)
ENDFUNC

// GROUND //
FUNC FLOAT DG_MONKEY_GET_GROUND_HEIGHT
	RETURN cfDG_MONKEY_GROUND_BASE_HEIGHT
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_GROUND_POSITION
	VECTOR_2D groundPosition = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	IF sDGMonkeyData.sLevelData.iCurrentTree >= 18 AND sDGMonkeyData.sLevelData.iCurrentTree <= 20
		VECTOR_2D treePosition = DG_MONKEY_TREE_POSITION(20,20-sDGMonkeyData.sLevelData.iCurrentTree+2)
		groundPosition.x -= DEGENATRON_GAMES_GET_SCREEN_WIDTH() - treePosition.x + DEGENATRON_GAMES_GET_SCREEN_WIDTH()/4
	ENDIF
	groundPosition.y += DEGENATRON_GAMES_GET_SCREEN_HEIGHT()/2 - DG_MONKEY_GET_GROUND_HEIGHT()/2
	RETURN groundPosition
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_GROUND_SIZE
	RETURN INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH*2,DG_MONKEY_GET_GROUND_HEIGHT())
ENDFUNC

// PLAYER //
FUNC FLOAT DG_MONKEY_GET_PLAYER_TRAVEL_SPEED
	RETURN cfDG_MONKEY_PLAYER_TRAVEL_SPEED/1000
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_PLAYER_SIZE
	RETURN INIT_VECTOR_2D(cfDG_MONKEY_PLAYER_WIDHT,cfDG_MONKEY_PLAYER_HEIGHT)
ENDFUNC
FUNC VECTOR_2D DG_MONKEY_PLAYER_POSITION_ON_TREE(INT iTree, INT iOrder)
	VECTOR_2D currentTreePosition = DG_MONKEY_TREE_POSITION(iTree,iOrder)
	VECTOR_2D treeSize = DG_MONKEY_TREE_SIZE()
	VECTOR_2D playerSize = DG_MONKEY_PLAYER_SIZE()
	VECTOR_2D playerPosition
	playerPosition.x = currentTreePosition.x 
	playerPosition.y = currentTreePosition.y + treeSize.y/2 + playerSize.y/2 
	
	RETURN playerPosition
ENDFUNC

FUNC VECTOR_2D DG_MONKEY_PLAYER_POSITION()
	VECTOR_2D currentTreePosition = DG_MONKEY_PLAYER_POSITION_ON_TREE(sDGMonkeyData.sLevelData.iCurrentTree,2)
	VECTOR_2D desiredTreePosition = DG_MONKEY_PLAYER_POSITION_ON_TREE(sDGMonkeyData.sLevelData.iDesiredTree,3)
	VECTOR_2D groundSize = DG_MONKEY_GROUND_SIZE()
	VECTOR_2D groundPosition = DG_MONKEY_GROUND_POSITION()
	VECTOR_2D treeSize = DG_MONKEY_TREE_SIZE()
	VECTOR_2D playerSize = DG_MONKEY_PLAYER_SIZE()
	VECTOR_2D playerPos
	
	IF sDGMonkeyData.sLevelData.bFail OR sDGMonkeyData.sLevelData.iCurrentTree = 20
		desiredTreePosition.y = groundPosition.y - groundSize.y/2 - playerSize.y/2
	ENDIF
	IF NOT sDGMonkeyData.sLevelData.bTravelling AND sDGMonkeyData.sLevelData.fTravelRate > 0.0
		playerPos = desiredTreePosition
	ELSE
		playerPos.x = currentTreePosition.x + (desiredTreePosition.x - currentTreePosition.x) * sDGMonkeyData.sLevelData.fTravelRate
		playerPos.y = currentTreePosition.y + (desiredTreePosition.y - currentTreePosition.y) * sDGMonkeyData.sLevelData.fTravelRate
		IF NOT sDGMonkeyData.sLevelData.bTravelling
			playerPos.x += TO_FLOAT((sDGMonkeyData.sLevelData.iTimeSwing/(ciDG_MONKEY_PLAYER_SWING_TIME/2)*2)-1)*treeSize.x/2.0
		ENDIF
	ENDIF
	RETURN playerPos
ENDFUNC

//**************************************************//
//*                 INIT - MONKEY                  *//
//**************************************************//
PROC DG_MONKEY_INIT_TREES
	sDGMonkeyData.fTreeHeightPos[0] = 570.0
	sDGMonkeyData.fTreeHeightPos[1] = 420.0
	sDGMonkeyData.fTreeHeightPos[2] = 380.0
	sDGMonkeyData.fTreeHeightPos[3] = 450.0
	sDGMonkeyData.fTreeHeightPos[4] = 400.0
ENDPROC

PROC DG_MONKEY_LEVEL_INIT
	sDGMonkeyData.sLevelData.iCurrentTree = 0
	sDGMonkeyData.sLevelData.iTimeSwing = 0
	sDGMonkeyData.sLevelData.iDesiredTree = 0
	sDGMonkeyData.sLevelData.fTravelRate = 0.0
	sDGMonkeyData.sLevelData.bTravelling = FALSE
	sDegenatronGamesData.sGameData.iFlashTime = 0
	sDegenatronGamesData.sGameData.iWaitTime = 0
	sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
	sDegenatronGamesData.sGameData.bFlashedTwice = FALSE
	sDegenatronGamesData.sGameData.bPlayerIsDead = FALSE
	
	DG_MONKEY_INIT_TREES()
ENDPROC

//**************************************************//
//*                UPDATE - MONKEY                 *//
//**************************************************//
PROC DG_MONKEY_UPDATE
	IF NOT sDGMonkeyData.sLevelData.bTravelling
		IF sDGMonkeyData.sLevelData.fTravelRate = 0.0
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
			OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT))
				ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_JUMP)
				sDGMonkeyData.sLevelData.iDesiredTree = sDGMonkeyData.sLevelData.iCurrentTree + 1
				sDGMonkeyData.sLevelData.bTravelling = TRUE
				sDGMonkeyData.sLevelData.bFail = sDGMonkeyData.sLevelData.iTimeSwing < ciDG_MONKEY_PLAYER_SWING_TIME/2.0
				IF sDGMonkeyData.sLevelData.iCurrentTree = 20
				OR sDGMonkeyData.sLevelData.iCurrentTree = 40
					sDGMonkeyData.sLevelData.bFail = FALSE
				ENDIF
				sDegenatronGamesData.bShouldDoMidShake = NOT sDGMonkeyData.sLevelData.bFail AND sDegenatronGamesData.iMachineBashed = 0
			ELSE
//				IF sDGMonkeyData.sLevelData.iTimeSwing < ciDG_MONKEY_PLAYER_SWING_TIME/2.0
					ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_SWING)
//				ELSE
//					ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_SWING_B)
//				ENDIF
				sDGMonkeyData.sLevelData.iTimeSwing = (sDGMonkeyData.sLevelData.iTimeSwing + DEGENATRON_GAMES_FRAME_TIME())%ciDG_MONKEY_PLAYER_SWING_TIME
			ENDIF
		ELSE
			sDGMonkeyData.sLevelData.fTravelRate = CLAMP(sDGMonkeyData.sLevelData.fTravelRate - cfDG_MONKEY_PLAYER_TRAVEL_SPEED*DEGENATRON_GAMES_FRAME_TIME()/1000.0,0.0,1.0)
			IF sDGMonkeyData.sLevelData.fTravelRate = 0.0
				sDGMonkeyData.sLevelData.iCurrentTree = sDGMonkeyData.sLevelData.iDesiredTree
				sDGMonkeyData.sLevelData.iTimeSwing = 0
				IF sDGMonkeyData.sLevelData.iCurrentTree = 41
					sDGMonkeyData.bAquaApeAward = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		sDGMonkeyData.sLevelData.fTravelRate = CLAMP(sDGMonkeyData.sLevelData.fTravelRate + cfDG_MONKEY_PLAYER_TRAVEL_SPEED*DEGENATRON_GAMES_FRAME_TIME()/1000.0,0.0,1.0)
		IF sDGMonkeyData.sLevelData.fTravelRate = 1.0
			sDGMonkeyData.sLevelData.bTravelling = FALSE
			IF sDGMonkeyData.sLevelData.bFail
				ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_EXPLODE)
				sDegenatronGamesData.bShouldDoHeavyShake = sDGMonkeyData.sLevelData.bFail AND sDegenatronGamesData.iMachineBashed = 0
				sDegenatronGamesData.sGameData.bPlayerIsDead = TRUE
				sDegenatronGamesData.sGameData.iFlashTime = 0
				sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
				sDegenatronGamesData.sGameData.bFlashedTwice = FALSE
			ELSE
				sDegenatronGamesData.sGameData.iScore += ciDG_MONKEY_TREE_SCORE
				sDegenatronGamesData.sTelemetry.kills++
			ENDIF
		ENDIF
	ENDIF
	sDGMonkeyData.sLevelData.bWaterLevelReached = sDGMonkeyData.sLevelData.iCurrentTree > 20
	sDGMonkeyData.sLevelData.bWaterLevelPassed = sDGMonkeyData.sLevelData.iCurrentTree > 40
	sDegenatronGamesData.bShouldDoSoftShake = NOT sDGMonkeyData.sLevelData.bTravelling AND sDGMonkeyData.sLevelData.fTravelRate = 0.0 AND sDGMonkeyData.sLevelData.iTimeSwing > ciDG_MONKEY_PLAYER_SWING_TIME/2.0 AND sDegenatronGamesData.iMachineBashed = 0
ENDPROC

PROC DG_MONKEY_UPDATE_LEVEL_STATE()
	SWITCH sDGMonkeyData.sLevelData.eLevelState
		CASE DG_MONKEY_STATE_LEVEL_INIT
			sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_UPDATE
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_UPDATE
			IF sDegenatronGamesData.sGameData.bPlayerIsDead
				sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_DEAD
			ENDIF
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_DEAD
			sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_WAIT_TIME
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_WAIT_TIME
			INT waitTime
			IF sDegenatronGamesData.sGameData.iLifes = 0
				waitTime = ciDEGENATRON_GAMES_WAIT_TIME_GAME_OVER
			ELSE
				waitTime = ciDEGENATRON_GAMES_WAIT_TIME_DEAD
			ENDIF
			IF sDegenatronGamesData.sGameData.iWaitTime > waitTime
				IF sDegenatronGamesData.sGameData.bPlayerIsDead
					IF sDegenatronGamesData.sGameData.bFlashedOnce AND sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME
						IF sDegenatronGamesData.sGameData.iLifes = 0
							sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_END
						ELSE
							sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_INIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_END
			sDGMonkeyData.sLevelData.eLevelState = DG_MONKEY_STATE_LEVEL_INIT
		BREAK
	ENDSWITCH
ENDPROC

//**************************************************//
//*              DEBUG INFO - MONKEY               *//
//**************************************************//
#IF IS_DEBUG_BUILD

PROC __DG_MONKEY_PRINT_DEBUG_LEVEL_INFO
	INT iLine = 0
	DEBUG_TEXT_INT_IN_SCREEN("iCurrentTree ",sDGMonkeyData.sLevelData.iCurrentTree,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_INT_IN_SCREEN("iDesiredTree ",sDGMonkeyData.sLevelData.iDesiredTree,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_INT_IN_SCREEN("bTravelling ",BOOL_TO_INT(sDGMonkeyData.sLevelData.bTravelling),0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_FLOAT_IN_SCREEN("fTravelRate ",sDGMonkeyData.sLevelData.fTravelRate,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_INT_IN_SCREEN("iTimeSwing ",sDGMonkeyData.sLevelData.iTimeSwing,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_INT_IN_SCREEN("bFail ",BOOL_TO_INT(sDGMonkeyData.sLevelData.bFail),0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_STRING_IN_SCREEN("eLevelState ",DG_MONKEY_STATE_LEVEL_TO_STRING(sDGMonkeyData.sLevelData.eLevelState),0.16,0.05+0.01*iLine)
	iLine++
ENDPROC

#ENDIF

PROC DG_MONKEY_CLIENT_STATE_UPDATE()
	SWITCH sDGMonkeyData.sLevelData.eLevelState
		CASE DG_MONKEY_STATE_LEVEL_INIT
			DG_MONKEY_LEVEL_INIT()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_UPDATE
			DG_MONKEY_UPDATE()
			IF sDGMonkeyData.sLevelData.iCurrentTree >= 21 AND sDGMonkeyData.sLevelData.iCurrentTree <= 40
				ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
				ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP_UW)
			ELSE
				IF ARCADE_GAMES_SOUND_HAS_FINISHED(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
					ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP_UW)
					ARCADE_GAMES_SOUND_PLAY_LOOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
				ENDIF
			ENDIF
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
			IF sDGMonkeyData.bAquaApeAward
				DEGENATRON_GAMES_SET_CHALLENGE_COMPLETED(DEGENATRON_GAMES_CHALLENGE_BIT_AQUAAPE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_DEGENATRON_AQUATIC_APE)
				sDGMonkeyData.bAquaApeAward = FALSE
			ENDIF
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_DEAD
			sDegenatronGamesData.sGameData.iLifes--
			IF sDegenatronGamesData.sGameData.iLifes > 0
				DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_DEAD)
			ELSE
				ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
				ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP_UW)
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_FAIL)
				DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_GAMEOVER)
			ENDIF
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_WAIT_TIME
			sDegenatronGamesData.sGameData.iWaitTime += DEGENATRON_GAMES_FRAME_TIME()
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_END
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
	ENDSWITCH

	IF sDegenatronGamesData.sTelemetry.level < sDGMonkeyData.sLevelData.iCurrentTree
		sDegenatronGamesData.sTelemetry.level = sDGMonkeyData.sLevelData.iCurrentTree
	ENDIF
	sDegenatronGamesData.sTelemetry.score = sDegenatronGamesData.sGameData.iScore
	IF sDegenatronGamesData.sGameData.iScore >= g_sMPTunables.iCH_ARCADE_GAMES_DG_MONKEY_GLITCH_SCORE AND sDegenatronGamesData.iMachineBashed != 0
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_DEGENATRON_PSYCHONAUT)
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_GLITCHED_DEGENATRON_TSHIRT, TRUE)
	ENDIF

	DG_MONKEY_UPDATE_LEVEL_STATE()
	
	#IF IS_DEBUG_BUILD
	IF sDegenatronGamesData.bShowHelp
		__DG_MONKEY_PRINT_DEBUG_LEVEL_INFO()
	ENDIF
	#ENDIF

ENDPROC

//**************************************************//
//*                 DRAW - MONKEY                  *//
//**************************************************//

PROC DG_MONKEY_DRAW_BACKGROUND()

	IF NOT sDGMonkeyData.sLevelData.bWaterLevelReached OR sDGMonkeyData.sLevelData.bWaterLevelReached AND sDGMonkeyData.sLevelData.bWaterLevelPassed
		DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaBlack)
	ENDIF
	IF sDGMonkeyData.sLevelData.bWaterLevelReached AND NOT sDGMonkeyData.sLevelData.bWaterLevelPassed
		DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaBlue)
	ENDIF

	
ENDPROC

PROC DG_MONKEY_DRAW_SKY()
	IF NOT sDGMonkeyData.sLevelData.bWaterLevelReached OR sDGMonkeyData.sLevelData.bWaterLevelReached AND sDGMonkeyData.sLevelData.bWaterLevelPassed
		DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_SKY_POSITION(), DG_MONKEY_SKY_SIZE(),0.0, sDegenatronGamesData.rgbaBlue)
	ENDIF
	IF sDGMonkeyData.sLevelData.bWaterLevelReached AND NOT sDGMonkeyData.sLevelData.bWaterLevelPassed
		DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_SKY_POSITION(), DG_MONKEY_SKY_SIZE(),0.0, sDegenatronGamesData.rgbaBlack)
	ENDIF
ENDPROC

PROC DG_MONKEY_DRAW_GROUND()
	DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_GROUND_POSITION(), DG_MONKEY_GROUND_SIZE(),0.0, sDegenatronGamesData.rgbaBrown)
//	DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DG_MONKEY_GROUND_POSITION(), DG_MONKEY_GROUND_SIZE(), sDegenatronGamesData.rgbaBrown)

	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		sDegenatronGamesData.sGameData.iFlashTime += DEGENATRON_GAMES_FRAME_TIME()
		IF NOT sDegenatronGamesData.sGameData.bFlashedOnce
			DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_GROUND_POSITION(), DG_MONKEY_GROUND_SIZE(),0.0, sDegenatronGamesData.rgbaRed)
//			DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DG_MONKEY_GROUND_POSITION(), DG_MONKEY_GROUND_SIZE(), sDegenatronGamesData.rgbaRed)
			sDegenatronGamesData.sGameData.bFlashedOnce = TRUE
		ELIF NOT sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME/2
			DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_GROUND_POSITION(), DG_MONKEY_GROUND_SIZE(),0.0, sDegenatronGamesData.rgbaWhite)
//			DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DG_MONKEY_GROUND_POSITION(), DG_MONKEY_GROUND_SIZE(), sDegenatronGamesData.rgbaWhite)
			sDegenatronGamesData.sGameData.bFlashedTwice = TRUE
		ENDIF
	ENDIF

ENDPROC

PROC DG_MONKEY_DRAW_PLAYER()
	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		EXIT
	ENDIF

	IF NOT sDGMonkeyData.sLevelData.bWaterLevelReached OR sDGMonkeyData.sLevelData.bWaterLevelReached AND sDGMonkeyData.sLevelData.bWaterLevelPassed
		DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_PLAYER_POSITION(), DG_MONKEY_PLAYER_SIZE(),0.0, sDegenatronGamesData.rgbaRed)
	ENDIF
	IF sDGMonkeyData.sLevelData.bWaterLevelReached AND NOT sDGMonkeyData.sLevelData.bWaterLevelPassed
		DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_MONKEY_PLAYER_POSITION(), DG_MONKEY_PLAYER_SIZE(),0.0, sDegenatronGamesData.rgbaMagenta)
	ENDIF
	
ENDPROC

PROC DG_MONKEY_DRAW_TREES()
	INT iTree = 0
	REPEAT ciDG_MONKEY_TREE_AMOUNT iTree
		INT iCurrentTree = iTree + sDGMonkeyData.sLevelData.iCurrentTree - 2
		IF NOT sDGMonkeyData.sLevelData.bWaterLevelReached
			IF iCurrentTree >= 0 AND iCurrentTree <= 20
				DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,DG_MONKEY_TREE_POSITION(iCurrentTree, iTree), DG_MONKEY_TREE_SIZE(),0.0, sDegenatronGamesData.rgbaGreen)
			ENDIF
		ENDIF
		IF sDGMonkeyData.sLevelData.bWaterLevelReached AND NOT sDGMonkeyData.sLevelData.bWaterLevelPassed
			IF iCurrentTree >= 21 AND iCurrentTree <= 40
				DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,DG_MONKEY_TREE_POSITION(iCurrentTree, iTree), DG_MONKEY_TREE_SIZE(),0.0, sDegenatronGamesData.rgbaCyan)
			ENDIF
		ENDIF
		IF sDGMonkeyData.sLevelData.bWaterLevelReached AND sDGMonkeyData.sLevelData.bWaterLevelPassed
			IF iCurrentTree >= 41
				DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,DG_MONKEY_TREE_POSITION(iCurrentTree, iTree), DG_MONKEY_TREE_SIZE(),0.0, sDegenatronGamesData.rgbaGreen)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DG_MONKEY_CLIENT_STATE_DRAW
	DG_MONKEY_DRAW_BACKGROUND()
	DEGENATRON_GAMES_DRAW_SCORE_BAR(FALSE,FALSE)
	SWITCH sDGMonkeyData.sLevelData.eLevelState
		CASE DG_MONKEY_STATE_LEVEL_INIT
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_UPDATE
		CASE DG_MONKEY_STATE_LEVEL_DEAD
		CASE DG_MONKEY_STATE_LEVEL_WAIT_TIME
			DG_MONKEY_DRAW_SKY()
			DG_MONKEY_DRAW_GROUND()
			DG_MONKEY_DRAW_TREES()
			DG_MONKEY_DRAW_PLAYER()
			DEGENATRON_GAMES_DRAW_FAIL_SUCCESS_TEXT()
		BREAK
		CASE DG_MONKEY_STATE_LEVEL_END
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD

PROC DG_MONKEY_DEBUG_CREATE_WIDGETS
	START_WIDGET_GROUP("Monkey's Paradise")
		ADD_WIDGET_INT_SLIDER("TREE",sDGMonkeyData.sLevelData.iCurrentTree,0,100,1)
	STOP_WIDGET_GROUP()
ENDPROC

PROC DG_MONKEY_DEBUG_UPDATE_WIDGETS
ENDPROC

#ENDIF

