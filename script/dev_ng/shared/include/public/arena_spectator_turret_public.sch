//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        arena_spectator_turret_public.sch															//
// Description: This header contains wrappers for the turret_manager system & turret_cam_script 			//
//				specifically for the arena spectator turrets.												//
//																											//
//				Sections:																					//
//				* DEFINITIONS																				//
//				* HELPER				- Helper functions for this file									//
//				* SPIN-THE-WHEEL		- API targeting the spin-the-wheel minigame							//
//				* SPECTATOR-SEATING		- API targeting the seats in the specator box						//
//				* MISSION-CONTROLLER	- API targeting the mission controller 								//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		12/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "turret_manager_public.sch"
USING "turret_cam_public.sch"
USING "website_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DEFINITIONS																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////  

/// PURPOSE: "Weapon Type" for arena turrets.
CONST_INT ci_ARENA_TURRET_TYPE_MG 0 	// Machine gun
CONST_INT ci_ARENA_TURRET_TYPE_HM 1 	// Homing missile
CONST_INT ci_ARENA_TURRET_TYPE_PM 2 	// Piloted missile
CONST_INT ci_ARENA_TURRET_TYPE_COUNT 3 	// Number of weapon types

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HELPER																									//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////  

/// PURPOSE:
///    Returns the time that the missile for turretId comes off cooldown
FUNC TIME_DATATYPE ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_COMPLETE_TIME(INT iTurretId)
	IF iTurretId = g_arenaSpectatorTurret.iMissileCdTurretId
		RETURN g_arenaSpectatorTurret.tMissileCdComplete
	ELSE
		RETURN GlobalServerBD_BlockB.arenaSpectatorTurretServer.tMissileCooldown[iTurretId]
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns true if the missile at turretId is ready
FUNC BOOL ARENA_SPECTATOR_TURRET_IS_MISSILE_COOLDOWN_COMPLETE(INT iTurretId)
	TIME_DATATYPE tCooldownTime = ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_COMPLETE_TIME(iTurretId)
	RETURN NATIVE_TO_INT(tCooldownTime) = 0
		OR IS_TIME_MORE_THAN(GET_NETWORK_TIME(), tCooldownTime)
ENDFUNC

/// PURPOSE:
///    Returns the time left before the missile for iTurretId is ready in milliseconds.
FUNC INT ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_REMAINING(INT iTurretId)
	TIME_DATATYPE tCooldownTime = ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_COMPLETE_TIME(iTurretId)
	IF NATIVE_TO_INT(tCooldownTime) = 0
		RETURN 0
	ENDIF
	RETURN GET_TIME_DIFFERENCE(tCooldownTime , GET_NETWORK_TIME())
ENDFUNC

/// PURPOSE:
///    Calculates and returns a time that a missile will come off cooldown.
FUNC TIME_DATATYPE ARENA_SPECTATOR_TURRET_CALC_COOLDOWN()
	TIME_DATATYPE time = GET_TIME_OFFSET(GET_NETWORK_TIME(), 1000 * g_FMMC_STRUCT.iTurretMissileCD)
	IF NATIVE_TO_INT(time) = 0 // 0 is special value and 1 ms shift isn't going to make a difference.
		time = GET_TIME_OFFSET(time, 1)
	ENDIF
	RETURN time
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the serverBd is up to date for this turret according to our local data.
FUNC BOOL ARENA_SPECTATOR_IS_SERVER_CD_CORRECT(INT iTurretId)
	IF iTurretId = -1
	OR g_FMMC_STRUCT.iTurretMissileCD = -1 // No cooldown - no need to verify server data is up to date.
		RETURN TRUE
	ENDIF
	// We can only verify against our own local data 
	IF iTurretId = g_arenaSpectatorTurret.iMissileCdTurretId
	AND (GlobalServerBD_BlockB.arenaSpectatorTurretServer.tMissileCooldown[iTurretId] = INT_TO_NATIVE(TIME_DATATYPE, 0)
	OR IS_TIME_MORE_THAN(g_arenaSpectatorTurret.tMissileCdComplete, GlobalServerBD_BlockB.arenaSpectatorTurretServer.tMissileCooldown[iTurretId]))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC ARENA_SPECTATOR_TURRET_CONFIG_MG()
	INT iAmmoDurationMs = ROUND(g_FMMC_STRUCT.fTurretAmmoInSeconds * 1000)
	INT iReloadDurationMs = 3000
	TURRET_CAM_CONFIG_WEAPON_FULL_AUTO(WEAPONTYPE_DLC_ARENA_MACHINE_GUN, 20, 100, DEFAULT, 0.6, iAmmoDurationMs, iReloadDurationMs)
	TURRET_CAM_CONFIG_WEAPON_SOUNDS(TCWS_ARENA_TURRET_SOUNDS)
	
	ARENA_HUD_WEAPON_ICON eIconPm = AHWI_TRANSPARENT
	IF g_FMMC_STRUCT.iTurretMissileCD = -1	
		eIconPm = AHWI_GREY
	ENDIF
	
	TURRET_CAM_CONFIG_ARENA_HUD(TRUE, AHWI_OPAQUE, AHWI_GREY, eIconPm)
ENDPROC

PROC ARENA_SPECTATOR_TURRET_CONFIG_PM(INT iCooldown)
	TURRET_CAM_CONFIG_WEAPON_PILOTED_MISSILE(iCooldown) 
	TURRET_CAM_CONFIG_WEAPON_SOUNDS(TCWS_ARENA_CONTESTANT_PM_SOUNDS)
	TURRET_CAM_CONFIG_ARENA_HUD(TRUE, AHWI_TRANSPARENT, AHWI_GREY, AHWI_OPAQUE)
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SPIN-THE-WHEEL																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Call this proc once as early as possible when player 
///    starts the spin-the-wheel minigame to attempt to reserve
///    an arena spectator turret.
///    
///    Note:
///    * You can check whether you've already called this function because
///      ref_eRequestId will no longer be TLRI_NONE
///    
/// PARAMS:
///    ref_eRequestId - Just store a TURRET_LOCK_REQUEST_ID locally and pass it in here.
///    
PROC ARENA_SPECTATOR_LOCK_TURRET(TURRET_LOCK_REQUEST_ID& ref_eRequestId)
	ref_eRequestId = TURRET_MANAGER_CREATE_LOCK_REQUEST(TGT_ARENA_SPECTATOR, 0, 0, TLST_ANY_TURRET)
ENDPROC

/// PURPOSE:
///    Call this to check whether a spectator turret is a valid result or not.
///    You must have called ARENA_SPECTATOR_LOCK_TURRET else this will always return false.
///    
///    Do not call continuously. Only call when you need to check the validity of the
///    lock request (once).
/// PARAMS:
///    ref_eRequestId - Pass in the same Id used for ARENA_SPECTATOR_LOCK_TURRET. 
/// RETURNS:
///    
FUNC BOOL ARENA_SPECTATOR_TURRET_IS_LOCKED(TURRET_LOCK_REQUEST_ID& ref_eRequestId)
	TURRET_LOCK_RESPONSE result = TURRET_MANAGER_GET_LOCK_STATUS(ref_eRequestId)
	IF result = TLR_SUCCESS
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- locked")
		ref_eRequestId = TLRI_NONE
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- failed")
		ref_eRequestId = TLRI_NONE
		TURRET_MANAGER_UNLOCK()
		RETURN FALSE
	ENDIF
ENDFUNC


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// SPECTATOR-SEATING																		   			      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Call this function to trigger a server request for an arena turret seat.
///    
/// PARAMS:
///    ref_eRequestId - This needs to be persistent (i.e. in script local data).
///    
/// RETURNS:
///    TURRET_LOCK_RESPONSE_STATUS:
///    * TLR_SUCCESS : You can set the global bit for tools to launch the cam.
///    * TLR_PENDING : Still waiting on server response. It has a long timeout so 
///    					you may wish to add your own. Call TURRET_MANAGER_UNLOCK() 
///						if you wish to cancel this request.
///    * TLR_FAILED  : No free turrets or the request has been overwritten.
FUNC TURRET_LOCK_RESPONSE ARENA_SPECTATOR_TURRET_LOCK(TURRET_LOCK_REQUEST_ID& ref_eRequestId)
	
	// This function is just a wrapper around TURRET_MANAGER_CREATE_LOCK_REQUEST for
	// TGT_ARENA_SPECTATOR turrets with specific args and sets/clears the requestId.
	
	IF ref_eRequestId = TLRI_NONE
		ref_eRequestId = TURRET_MANAGER_CREATE_LOCK_REQUEST(TGT_ARENA_SPECTATOR, 0, 0, TLST_ANY_TURRET)
	ENDIF
	
	TURRET_LOCK_RESPONSE result = TURRET_MANAGER_GET_LOCK_STATUS(ref_eRequestId)
	IF result = TLR_SUCCESS
		ref_eRequestId = TLRI_NONE
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC SEAT ----- locked")
	ELIF result <> TLR_PENDING
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC SEAT ----- failed")
		ref_eRequestId = TLRI_NONE
		TURRET_MANAGER_UNLOCK()
	ENDIF
	
	RETURN result
ENDFUNC

/// PURPOSE:
///    Apply the TURRET_CAM_CONFIG_HUD settings for this turret.
PROC ARENA_SPECTATOR_TURRET_CONFIG_HUD()
	TURRET_CAM_HUD_TYPE eHud = TCHT_NONE
	SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
		CASE ARENA_THEME_DYSTOPIAN				eHud = TCHT_ARENA_APOC	BREAK
		CASE ARENA_THEME_SCIFI					eHud = TCHT_ARENA_SCIFI	BREAK
		CASE ARENA_THEME_CONSUMERWASTELAND		eHud = TCHT_ARENA_CONS	BREAK
	ENDSWITCH
	TURRET_CAM_CONFIG_HUD(eHud)
ENDPROC

/// PURPOSE:
///    Apply all the necessary TURRET_CAM_CONFIG_* settings and fill "ref_args" for this turret.
PROC ARENA_SPECATOR_SET_CONFIG(TURRET_CAM_CONFIG_ID& ref_eCamConfigId, TURRET_CAM_ARGS& ref_args)
	INT iTurretId = TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID())
	SWITCH iTurretId
		CASE 0 ref_args.vPos = <<2716.6184, -3706.9858, 139.0007>>	ref_args.vRot.z = 0.0    BREAK
		CASE 1 ref_args.vPos = <<2617.0869, -3800.0027, 139.0007>> 	ref_args.vRot.z = 90.0   BREAK
		CASE 2 ref_args.vPos = <<2716.4685, -3892.9023, 139.0007>>	ref_args.vRot.z = 180.0  BREAK
		CASE 3 ref_args.vPos = <<2883.4773, -3892.9023, 139.0007>> 	ref_args.vRot.z = 180.0  BREAK
		CASE 4 ref_args.vPos = <<2982.8750, -3800.0027, 139.0007>>	ref_args.vRot.z = -90.0  BREAK
		CASE 5 ref_args.vPos = <<2883.4773, -3706.9858, 139.0007>>	ref_args.vRot.z = 0.0    BREAK
	ENDSWITCH	
	
	// Consumer wasteland east/west turrets need to be hoisted up
	IF g_FMMC_STRUCT.sArenaInfo.iArena_Theme = ARENA_THEME_CONSUMERWASTELAND
	AND (iTurretId = 1
	OR iTurretId = 4)
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Bumping turret height for iTurretId = ", iTurretId)
		ref_args.vPos.z += 18.2
	ENDIF
	
	ref_args.vRot.z = NORMALIZE_ANGLE(ref_args.vRot.z + 180.0)
	ref_args.vPos += ROTATE_VECTOR_ABOUT_Z(<<0.0, 7.0, 15.00>>, ref_args.vRot.z)

	/* Add '/' to the start of this line to spawn the turret near the player coords for debugging. Don't forget to romve the '/' before submitting.
	PED_INDEX pedLocal = PLAYER_PED_ID()
	ref_args.vPos += - <<2716.6184, -3706.9858, 139.0007>> + GET_ENTITY_COORDS(pedLocal) + <<2.0 * TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID()), 0, 2.0>>
	//*/
	
	// Make sure our turret config is always up to date
	CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- config start")
	TURRET_CAM_CONFIG_START()
		TURRET_CAM_CONFIG_CAM_SPEEDS(0.8, 40.0)	
		TURRET_CAM_CONFIG_CAM_LIMITS(-90.0, 90.0, -85.0, 10.0, 30.0, 60.0)	
		
		ARENA_SPECTATOR_TURRET_CONFIG_MG()
		ARENA_SPECTATOR_TURRET_CONFIG_HUD()
		TURRET_CAM_CONFIG_HUD_SOUNDS(TCHS_ARENA_CAM)
		TURRET_CAM_CONFIG_SET_LOAD_ARGS(TRUE, TRUE, TRUE, 500)
		
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RUP, "ATB_EXIT")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RB, "ATB_CYCLE_R")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_LB, "ATB_CYCLE_L")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_GROUP(INPUTGROUP_LOOK, "ATB_LOOK")
		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SNIPER_ZOOM, "ATB_ZOOM")

		IF g_FMMC_STRUCT.fTurretAmmoInSeconds <> 0
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- config : ammo restriction")
			TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RRIGHT, "ATB_RELOAD")
		ENDIF

		IF g_FMMC_STRUCT.iTurretMissileCD <> -1
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- config : missiles")
			TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RLEFT, "ACT_CYCLE")
		ENDIF

		TURRET_CAM_CONFIG_ADD_INSTRUCTIONAL_BUTTON(INPUT_SCRIPT_RT, "ATB_FIRE")
		TURRET_CAM_CONFIG_SET_INPUT_MAP("Arena_Turrets")
	TURRET_CAM_CONFIG_STOP(ref_eCamConfigId)
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MISSION-CONTROLLER																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////    

/// PURPOSE:
///    Reset missile cooldowns. Needs to be called by
///    every player locally.
PROC ARENA_SPECTATOR_TURRET_RESET_MISSILE_COOLDOWNS(ARENA_SPECATOR_TURRET_CONTEXT& ref_data)
	CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Reset turret cooldowns")

	// Check if local player owns the serverBd "GlobalServerBD_BlockB"
	IF (NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) = PLAYER_ID())
		// Reset all missile cooldowns
		INT iTurretId = 0 
		REPEAT COUNT_OF(GlobalServerBD_BlockB.arenaSpectatorTurretServer.tMissileCooldown) iTurretId
			GlobalServerBD_BlockB.arenaSpectatorTurretServer.tMissileCooldown[iTurretId] = INT_TO_NATIVE(TIME_DATATYPE, 0)
		ENDREPEAT
	ENDIF

	// Reset local data
	g_arenaSpectatorTurret.iMissileCdTurretId = -1
	g_arenaSpectatorTurret.tMissileCdComplete = INT_TO_NATIVE(TIME_DATATYPE, 0)
	RESET_NET_TIMER(g_arenaSpectatorTurret.resendTimer)
	
	// Remove turret cam cooldown
	IF ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_PM
		TURRET_CAM_CONFIG_UPDATE_START()	
			ARENA_SPECTATOR_TURRET_CONFIG_PM(0)
		TURRET_CAM_CONFIG_UPDATE_STOP()		
	ENDIF
ENDPROC

/// PURPOSE:
///    Call continuously to launch a specator turret cam.
///  
/// RETURNS:
///  	TRUE when the turret is launched & faded in.  
FUNC BOOL ARENA_SPECTATOR_TURRET_LAUNCH(ARENA_SPECATOR_TURRET_CONTEXT& ref_data)
	IF TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY()
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Ready to launch cam...")
		TURRET_CAM_ARGS args
		ARENA_SPECATOR_SET_CONFIG(ref_data.eCamConfigId, args)
		TURRET_MANAGER_LAUNCH_CAM_NOW(args)
		
		ref_data.eTurretState = ASTS_DEFAULT		
		ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_MG
		g_arenaSpectatorTurret.iMissileCdTurretId = -1	// Reset local missile cooldown data
	ELIF TURRET_MANAGER_CURRENT_CAM_MATCHES_LOCK()
		CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Waiting for screen fade in")
		RETURN IS_SCREEN_FADED_IN()
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This func will manage launching launching a turret
///    camera after a spectator specator wins/selects it.
///    
///    Requires ARENA_SPECTATOR_TURRET_LAUNCH() has returned 
///    TRUE.
///    
///    You may kick the player out early by calling 
///    TURRET_MANAGER_UNLOCK().
/// RETURNS:
///    FALSE if the player quits out of the turret.
FUNC BOOL ARENA_SPECTATOR_TURRET_UPDATE(ARENA_SPECATOR_TURRET_CONTEXT& ref_data)
	
	INT iTurretId = TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID())

	// We need to make sure the serverBd is up to date with our last known cooldown.
	// Player can't move from this turret until this is up to date so the only way a cooldown is missed
	// is if the ronud ends (in which case it doesn't matter) or the player pulls the plug after firing.
	IF NOT ARENA_SPECTATOR_IS_SERVER_CD_CORRECT(iTurretId)
		// Re-send event every 1000 ms while the serverBd is out of date...
		IF HAS_NET_TIMER_EXPIRED(g_arenaSpectatorTurret.resendTimer, 1000)
			REINIT_NET_TIMER(g_arenaSpectatorTurret.resendTimer)
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Sending CD event again...")
			ARENA_SPECTATOR_TURRET_BROADCAST_MISSILE_FIRED(g_arenaSpectatorTurret.iMissileCdTurretId, g_arenaSpectatorTurret.tMissileCdComplete)		
		ENDIF
	ENDIF
	
	// B* url:bugstar:5483468 : god text is getting in the way of the instructional buttons
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)

	// Set Radar Map to use Arena map instead of arena_box map.
	VECTOR vInteriorPos = <<2800.0, -3800.0, 100.0>>
	SET_RADAR_AS_INTERIOR_THIS_FRAME(HASH("xs_x18_int_01"), vInteriorPos.x, vInteriorPos.y, 0, GET_ARENA_ENTITY_SET_LEVEL_NUMBER(g_FMMC_STRUCT.sArenaInfo.iArena_Theme, g_FMMC_STRUCT.sArenaInfo.iArena_Variation))
	HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)		
			
	IF ref_data.bCyclingForwards
	OR ref_data.bCyclingBackwards
		IF ref_Data.eRequestId = TLRI_NONE
			TURRET_LOCK_SEARCH_TYPE eSearchType 
			IF ref_data.bCyclingBackwards
				eSearchType = TLST_ANY_TURRET_BACKWARDS
			ELSE
				eSearchType = TLST_ANY_TURRET_FORWARDS
			ENDIF
			ref_data.iTurretIdBeforeCycle = TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID())
			// If this cycle fails because all turrets are full bKickOnFail = FALSE means we'll stay in our current turret.
			ref_data.eRequestId = TURRET_MANAGER_CREATE_LOCK_REQUEST(TGT_ARENA_SPECTATOR, 0, ref_data.iTurretIdBeforeCycle, eSearchType, FALSE)
		ENDIF
		
		TURRET_LOCK_RESPONSE result = TURRET_MANAGER_GET_LOCK_STATUS(ref_data.eRequestId)
		IF result = TLR_SUCCESS
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC -----cycle locked")
			ref_data.bCyclingForwards = FALSE
			ref_data.bCyclingBackwards = FALSE
			ref_data.eRequestId = TLRI_NONE
			
			// If we tried to cycle but the server said there are no other free turrets
			IF ref_data.iTurretIdBeforeCycle = TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID())
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ELSE
				ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_MG
			ENDIF
		ELIF result <> TLR_PENDING
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC -----cycle failed")
			ref_data.eRequestId = TLRI_NONE
			ref_data.bCyclingForwards = FALSE
			ref_data.bCyclingBackwards = FALSE
		ENDIF
	ELIF NOT IS_SCREEN_FADING_OUT()  // don't load until faded out (or, continue while faded in).
	AND TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_SPECTATOR // We've got a locked turret?
		IF TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY()
			CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Launching cycle cam")
			TURRET_CAM_ARGS args
			ARENA_SPECATOR_SET_CONFIG(ref_data.eCamConfigId, args)
			TURRET_MANAGER_LAUNCH_CAM_NOW(args)
		
		ELIF TURRET_MANAGER_CURRENT_CAM_MATCHES_LOCK()	
		AND TURRET_CAM_IS_READY()
		AND NOT IS_PAUSE_MENU_ACTIVE()
		AND IS_SCREEN_FADED_IN()
			// **Turret running** Check controls...	
			PLAYER_INDEX piLocal = PLAYER_ID()
			IF GET_PLAYER_WANTED_LEVEL(piLocal) > 0
				SET_PLAYER_WANTED_LEVEL(piLocal, 0)
				SET_PLAYER_WANTED_LEVEL_NOW(piLocal)	
			ENDIF
						
			SWITCH ref_data.eTurretState
				CASE ASTS_DEFAULT
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_BROWSER_OPEN()
						IF IS_PLAYER_USING_DRONE(PLAYER_ID())
							ref_data.eTurretState = ASTS_USING_MISSILE
						ELSE
							// Draw a "MISSILE COOLDOWN" meter if this mode uses missiles (with cooldowns)
							IF ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_PM
								DRAW_GENERIC_METER(ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_REMAINING(iTurretId), 1000 * g_FMMC_STRUCT.iTurretMissileCD, "AST_CD", HUD_COLOUR_WHITE, DEFAULT, HUDORDER_NINETHBOTTOM)
							ENDIF
							
							IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
								CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Input: Kill")
								IF ARENA_SPECTATOR_IS_SERVER_CD_CORRECT(iTurretId)
									TURRET_MANAGER_UNLOCK()
									RETURN FALSE
								ELSE 
									CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Input: Kill : cooldown not updated yet...")
								ENDIF
							ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
								CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Input: Cycle forwards ")
								IF ARENA_SPECTATOR_IS_SERVER_CD_CORRECT(iTurretId)
									g_arenaSpectatorTurret.iMissileCdTurretId = -1
									ref_data.bCyclingForwards = TRUE
									DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
								ELSE
									CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Input: Cycle forwards failed : cooldown not updated yet...")
								ENDIF
							ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
								CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Input: Cycle backwards")
								IF ARENA_SPECTATOR_IS_SERVER_CD_CORRECT(iTurretId)
									g_arenaSpectatorTurret.iMissileCdTurretId = -1
									ref_data.bCyclingBackwards = TRUE
									DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
								ELSE
									CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Input: Cycle forwards failed : cooldown not updated yet...")
								ENDIF
							ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
							AND g_FMMC_STRUCT.iTurretMissileCD <> -1
								// Toggle between Mg and Hm
								IF ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_MG
									CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT -----Input: Switch weapon from MG to PM")
									ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_PM
									TURRET_CAM_CONFIG_UPDATE_START()	
										ARENA_SPECTATOR_TURRET_CONFIG_PM(ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_REMAINING(iTurretId))
									TURRET_CAM_CONFIG_UPDATE_STOP()								
								ELSE
									CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA CONTESTANT -----Input: Switch weapon from PM to MG")
									ref_data.iCurrentWeaponType = ci_ARENA_TURRET_TYPE_MG
									TURRET_CAM_CONFIG_UPDATE_START()	
										ARENA_SPECTATOR_TURRET_CONFIG_MG()
									TURRET_CAM_CONFIG_UPDATE_STOP()										
								ENDIF 
								PLAY_SOUND_FRONTEND(-1, "DPAD_WEAPON_SCROLL", "HUD_FRONTEND_DEFAULT_SOUNDSET") 
							ENDIF	
						ENDIF
					ENDIF
					BREAK
								
				CASE ASTS_USING_MISSILE
					IF NOT IS_PLAYER_USING_DRONE(PLAYER_ID())
						CDEBUG1LN(DEBUG_NET_TURRET, "----- ARENA SPEC ----- Drone dead")
						ref_data.eTurretState = ASTS_DEFAULT
	
						// Save local data so we can validate server cd values
						g_arenaSpectatorTurret.iMissileCdTurretId = iTurretId
						g_arenaSpectatorTurret.tMissileCdComplete = ARENA_SPECTATOR_TURRET_CALC_COOLDOWN()
						REINIT_NET_TIMER(g_arenaSpectatorTurret.resendTimer)
						
						// Tell server to update missile cd
						ARENA_SPECTATOR_TURRET_BROADCAST_MISSILE_FIRED(g_arenaSpectatorTurret.iMissileCdTurretId, g_arenaSpectatorTurret.tMissileCdComplete)		
						
						// Force cam script to reload instructional buttons
						SET_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_FORCE_INSTR_BTN_RELOAD)
						
						// Force hud to update & update cooldown timer
						TURRET_CAM_CONFIG_UPDATE_START()	
							ARENA_SPECTATOR_TURRET_CONFIG_PM(ARENA_SPECTATOR_TURRET_GET_MISSILE_COOLDOWN_REMAINING(iTurretId))
						TURRET_CAM_CONFIG_UPDATE_STOP()
					ENDIF
					BREAK
			ENDSWITCH
		ENDIF
	ENDIF

	RETURN TRUE	
ENDFUNC
