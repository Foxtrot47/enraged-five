USING "commands_cutscene.sch"
USING "commands_camera.sch"

PROC  RESET_SEAMLESS_CUTS(CAMERA_INDEX &seamlessCam, bool &tracker)
	if DOES_CAM_EXIST(seamlessCam)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(seamlessCam)
		tracker=FALSE
	ENDIF
ENDPROC

PROC  SET_SEAMLESS_CUTS_ACTIVE(bool &tracker)
	tracker = true
ENDPROC

PROC RUN_SEAMLESS_CUTSCENE(CAMERA_INDEX &seamlessCam, bool &tracker)
	if tracker = true
		if not HAS_CUTSCENE_FINISHED()
			if not DOES_CAM_EXIST(seamlessCam)
				seamlessCam = CREATE_CAMERA(CAMTYPE_SCRIPTED,TRUE)
				SET_CAM_COORD(seamlessCam,<<10,10,10>>) //initiialise its coords as cams don't like being created at <<0,0,0>>
				RENDER_SCRIPT_CAMS(true,FALSE)
			ENDIF
			
			SET_CAM_COORD(seamlessCam,GET_FINAL_RENDERED_CAM_COORD())
			SET_CAM_FAR_CLIP(seamlessCam,GET_FINAL_RENDERED_CAM_FAR_CLIP())
			SET_CAM_FAR_DOF(seamlessCam,GET_FINAL_RENDERED_CAM_FAR_DOF())
			SET_CAM_FOV(seamlessCam,GET_FINAL_RENDERED_CAM_FOV())
			SET_CAM_MOTION_BLUR_STRENGTH(seamlessCam,GET_FINAL_RENDERED_CAM_MOTION_BLUR_STRENGTH())
			SET_CAM_NEAR_CLIP(seamlessCam,GET_FINAL_RENDERED_CAM_NEAR_CLIP())
			SET_CAM_NEAR_DOF(seamlessCam,GET_FINAL_RENDERED_CAM_NEAR_DOF())
			SET_CAM_ROT(seamlessCam,GET_FINAL_RENDERED_CAM_ROT())
		ELSE
			if DOES_CAM_EXIST(seamlessCam)
				if WAS_CUTSCENE_SKIPPED()
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
				ELSE
					RENDER_SCRIPT_CAMS(FALSE,true)
				ENDIF	
				tracker = FALSE
			ENDIF
		ENDIF
	ELSE
		if DOES_CAM_EXIST(seamlessCam)
			if not IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				DESTROY_CAM(seamlessCam)
			ENDIF
		ENDIF
	ENDIF

	
ENDPROC
