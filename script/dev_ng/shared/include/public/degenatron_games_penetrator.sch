USING "degenatron_games_using.sch"
USING "degenatron_games_common.sch"
USING "degenatron_games_animation.sch"

CONST_FLOAT cfDG_PENETRATOR_ENEMY_HEIGHT			78.86968085
CONST_FLOAT cfDG_PENETRATOR_ENEMY_WIDHT 			100.0
CONST_INT 	ciDG_PENETRATOR_ENEMY_MAX_AMOUNT 		12
CONST_INT 	ciDG_PENETRATOR_ENEMY_MAX_TIME_ALIVE	6000
CONST_INT	ciDG_PENETRATOR_ENEMY_STEPS_TO_EDGE		5

CONST_INT	ciDG_PENETRATOR_ENEMY_SCORE				20
CONST_INT	ciDG_PENETRATOR_ENEMY_SPAWN_TIME		1000

CONST_FLOAT cfDG_PENETRATOR_PLAYER_HEIGHT			94.64361702
CONST_FLOAT cfDG_PENETRATOR_PLAYER_WIDHT 			120.0

CONST_FLOAT cfDG_PENETRATOR_HOLE_HEIGHT				757.1489362
CONST_FLOAT cfDG_PENETRATOR_HOLE_WIDHT 				960.0

CONST_FLOAT cfDG_PENETRATOR_BULLET_HEIGHT			39.43484043
CONST_FLOAT cfDG_PENETRATOR_BULLET_WIDHT 			50.0
CONST_INT 	ciDG_PENETRATOR_BULLET_MAX_AMOUNT 		5
CONST_INT 	ciDG_PENETRATOR_BULLET_MAX_TIME_ALIVE	2000
CONST_INT	ciDG_PENETRATOR_BULLET_STEPS_TO_EDGE	10

CONST_INT	ciDG_PENETRATOR_HOLE_POSITIONS			4*4
CONST_FLOAT	cfDG_PENETRATOR_HOLE_RATIO				0.85


ENUM DG_PENETRATOR_STATE_LEVEL
	DG_PENETRATOR_STATE_LEVEL_INIT,
	DG_PENETRATOR_STATE_LEVEL_UPDATE,
	DG_PENETRATOR_STATE_LEVEL_DEAD,
	DG_PENETRATOR_STATE_LEVEL_WAIT_TIME,
	DG_PENETRATOR_STATE_LEVEL_END
ENDENUM

FUNC STRING DG_PENETRATOR_STATE_LEVEL_TO_STRING(DG_PENETRATOR_STATE_LEVEL eEnum)
	SWITCH eEnum
		CASE DG_PENETRATOR_STATE_LEVEL_INIT 			RETURN "LEVEL_INIT"
		CASE DG_PENETRATOR_STATE_LEVEL_UPDATE 			RETURN "LEVEL_UPDATE"
		CASE DG_PENETRATOR_STATE_LEVEL_DEAD 			RETURN "LEVEL_DEAD"
		CASE DG_PENETRATOR_STATE_LEVEL_WAIT_TIME 		RETURN "LEVEL_WAIT_TIME"
		CASE DG_PENETRATOR_STATE_LEVEL_END 				RETURN "LEVEL_END"
	ENDSWITCH
	RETURN "***INVALID***"
ENDFUNC

STRUCT DG_PENETRATOR_LEVEL_DATA
	DG_PENETRATOR_STATE_LEVEL 	eLevelState
	INT							iPlayerHolePosition
	INT							iTimeSinceLastEnemy = 0
	INT							iBulletIndex = 0
	BOOL						bObjectiveJustPicked
ENDSTRUCT
STRUCT DG_PENETRATOR_ENEMY_DATA
	INT							iEnemyHolePosition = -1
	INT							iEnemyTimeAlive = 0
	FLOAT						fEnemyHoleDeep = 0.0
	BOOL						bEnemyOnEdge = FALSE
	BOOL						bEnemyIsDead = TRUE
ENDSTRUCT

STRUCT DG_PENETRATOR_BULLET_DATA
	INT							iBulletHolePosition = -1
	INT							iBulletTimeAlive = 0
	FLOAT						fBulletHoleDeep = 0.0
ENDSTRUCT

STRUCT DG_PENETRATOR_DATA
	DG_PENETRATOR_LEVEL_DATA			sLevelData
	DG_PENETRATOR_ENEMY_DATA			sEnemiesData[ciDG_PENETRATOR_ENEMY_MAX_AMOUNT]
	DG_PENETRATOR_BULLET_DATA			sBulletsData[ciDG_PENETRATOR_BULLET_MAX_AMOUNT]
	
ENDSTRUCT

DG_PENETRATOR_DATA sDGPenetratorData

//**************************************************//
//*          DATA ACCESSORS - PENETRATOR           *//
//**************************************************//

// HOLE //
FUNC VECTOR_2D DG_PENETRATOR_HOLE_SIZE
	RETURN INIT_VECTOR_2D(cfDG_PENETRATOR_HOLE_WIDHT,cfDG_PENETRATOR_HOLE_HEIGHT)
ENDFUNC

FUNC VECTOR_2D DG_PENETRATOR_HOLE_POSITION()
	VECTOR_2D holePos = DEGENATRON_GAMES_GET_SCREEN_CENTER()
	holePos.y += ciDEGENATRON_GAMES_SCORE_HEIGHT/2
	RETURN holePos
ENDFUNC

// PLAYER //
FUNC VECTOR_2D DG_PENETRATOR_PLAYER_SIZE
	RETURN INIT_VECTOR_2D(cfDG_PENETRATOR_PLAYER_WIDHT,cfDG_PENETRATOR_PLAYER_HEIGHT)
ENDFUNC

FUNC VECTOR_2D DG_PENETRATOR_PLAYER_POSITION()
	VECTOR_2D holePos = DG_PENETRATOR_HOLE_POSITION()
	VECTOR_2D holeSize = DG_PENETRATOR_HOLE_SIZE()
	VECTOR_2D playerPos
	IF   sDGPenetratorData.sLevelData.iPlayerHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 0 // up
		playerPos.x = holePos.x - holeSize.x/2 + holeSize.x/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sLevelData.iPlayerHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)
		playerPos.y = holePos.y - holeSize.y/2
	ELIF sDGPenetratorData.sLevelData.iPlayerHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 1 // right
		playerPos.x = holePos.x + holeSize.x/2
		playerPos.y = holePos.y - holeSize.y/2 + holeSize.y/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sLevelData.iPlayerHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)
	ELIF sDGPenetratorData.sLevelData.iPlayerHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 2 // down
		playerPos.x = holePos.x + holeSize.x/2 + holeSize.x/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sLevelData.iPlayerHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)*-1
		playerPos.y = holePos.y + holeSize.y/2
	ELIF sDGPenetratorData.sLevelData.iPlayerHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 3 // left
		playerPos.x = holePos.x - holeSize.x/2
		playerPos.y = holePos.y + holeSize.y/2 + holeSize.y/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sLevelData.iPlayerHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)*-1
	ENDIF

	RETURN playerPos
ENDFUNC

// BULLET //
FUNC VECTOR_2D DG_PENETRATOR_BULLET_SIZE(INT iBullet, BOOL bTail = FALSE)
	RETURN INIT_VECTOR_2D(cfDG_PENETRATOR_BULLET_WIDHT*(0.265 + 0.735*((1.0-sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep)-0.5*BOOL_TO_INT(bTail))),cfDG_PENETRATOR_BULLET_HEIGHT*(0.265 + 0.735*((1.0-sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep)-0.5*BOOL_TO_INT(bTail))))
ENDFUNC

FUNC VECTOR_2D DG_PENETRATOR_BULLET_POSITION(INT iBullet, BOOL bTail = FALSE)
	VECTOR_2D holePos = DG_PENETRATOR_HOLE_POSITION()
	VECTOR_2D holeSize = DG_PENETRATOR_HOLE_SIZE()
	VECTOR_2D bulletPos
	FLOAT deepRate = 0.265 + 0.735*((1.0-sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep)+(1.0/ciDG_PENETRATOR_BULLET_STEPS_TO_EDGE)*BOOL_TO_INT(bTail))
	IF   sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 0 // up
		bulletPos.x = holePos.x - holeSize.x/2*deepRate + holeSize.x*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)
		bulletPos.y = holePos.y - holeSize.y/2*deepRate
	ELIF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 1 // right
		bulletPos.x = holePos.x + holeSize.x/2*deepRate
		bulletPos.y = holePos.y - holeSize.y/2*deepRate + holeSize.y*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)
	ELIF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 2 // down
		bulletPos.x = holePos.x + holeSize.x/2*deepRate + holeSize.x*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)*-1
		bulletPos.y = holePos.y + holeSize.y/2*deepRate
	ELIF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 3 // left
		bulletPos.x = holePos.x - holeSize.x/2*deepRate
		bulletPos.y = holePos.y + holeSize.y/2*deepRate + holeSize.y*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)*-1
	ENDIF

	RETURN bulletPos
ENDFUNC


// ENEMY //
FUNC VECTOR_2D DG_PENETRATOR_ENEMY_SIZE(INT iEnemy)
	RETURN INIT_VECTOR_2D(cfDG_PENETRATOR_ENEMY_WIDHT*(0.265 + 0.735*sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep),cfDG_PENETRATOR_ENEMY_HEIGHT*(0.265 + 0.735*sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep))
ENDFUNC

FUNC VECTOR_2D DG_PENETRATOR_ENEMY_POSITION(INT iEnemy)
	VECTOR_2D holePos = DG_PENETRATOR_HOLE_POSITION()
	VECTOR_2D holeSize = DG_PENETRATOR_HOLE_SIZE()
	VECTOR_2D enemyPos
	FLOAT deepRate = (0.265 + 0.735*sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep)
	IF   sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 0 // up
		enemyPos.x = holePos.x - holeSize.x/2*deepRate + holeSize.x*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)
		enemyPos.y = holePos.y - holeSize.y/2*deepRate
	ELIF sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 1 // right
		enemyPos.x = holePos.x + holeSize.x/2*deepRate
		enemyPos.y = holePos.y - holeSize.y/2*deepRate + holeSize.y*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)
	ELIF sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 2 // down
		enemyPos.x = holePos.x + holeSize.x/2*deepRate + holeSize.x*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)*-1
		enemyPos.y = holePos.y + holeSize.y/2*deepRate
	ELIF sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition/(ciDG_PENETRATOR_HOLE_POSITIONS/4) = 3 // left
		enemyPos.x = holePos.x - holeSize.x/2*deepRate
		enemyPos.y = holePos.y + holeSize.y/2*deepRate + holeSize.y*deepRate/(ciDG_PENETRATOR_HOLE_POSITIONS/4+1) * (sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition % (ciDG_PENETRATOR_HOLE_POSITIONS/4)+1)*-1
	ENDIF

	RETURN enemyPos
ENDFUNC


//**************************************************//
//*               INIT - PENETRATOR                *//
//**************************************************//

PROC DG_PENETRATOR_ENEMIES_INIT()
	INT iEnemy
	REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
		sDGPenetratorData.sEnemiesData[iEnemy].bEnemyOnEdge = FALSE
		sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead = TRUE
		sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep = 0.0
		sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition = iEnemy
		sDGPenetratorData.sEnemiesData[iEnemy].iEnemyTimeAlive = 0
	ENDREPEAT
ENDPROC

PROC DG_PENETRATOR_BULLET_INIT()
	INT iBullet
	REPEAT ciDG_PENETRATOR_BULLET_MAX_AMOUNT iBullet
		sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep = 0.0
		sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition = -1
		sDGPenetratorData.sBulletsData[iBullet].iBulletTimeAlive = 0
	ENDREPEAT
ENDPROC

PROC DG_PENETRATOR_LEVEL_INIT
	sDGPenetratorData.sLevelData.iPlayerHolePosition = 0
	sDGPenetratorData.sLevelData.iBulletIndex = 0
	sDGPenetratorData.sLevelData.iTimeSinceLastEnemy = 0
	sDegenatronGamesData.sGameData.bPlayerIsDead = FALSE
	sDGPenetratorData.sLevelData.bObjectiveJustPicked = FALSE
	sDegenatronGamesData.sGameData.iWaitTime = 0
	sDegenatronGamesData.sGameData.iFlashTime = 0
	sDegenatronGamesData.sGameData.bFlashedOnce = FALSE
	sDegenatronGamesData.sGameData.bFlashedTwice = FALSE

	DG_PENETRATOR_ENEMIES_INIT()
	DG_PENETRATOR_BULLET_INIT()
ENDPROC


//**************************************************//
//*            DEBUG INFO - PENETRATOR             *//
//**************************************************//
#IF IS_DEBUG_BUILD

PROC __DG_PENETRATOR_PRINT_DEBUG_LEVEL_INFO
	INT iLine = 0
	DEBUG_TEXT_INT_IN_SCREEN("iPlayerHolePosition ",sDGPenetratorData.sLevelData.iPlayerHolePosition,0.16,0.05+0.01*iLine)
	iLine++
	DEBUG_TEXT_STRING_IN_SCREEN("eLevelState ",DG_PENETRATOR_STATE_LEVEL_TO_STRING(sDGPenetratorData.sLevelData.eLevelState),0.16,0.05+0.01*iLine)
	iLine++
ENDPROC

PROC __DG_PENETRATOR_PRINT_DEBUG_ENEMIES_INFO
	INT iLine = 0
	INT iEnemy = 0
	REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
		TEXT_LABEL_15 output
		output = "Enemy "
		output += iEnemy
		DEBUG_TEXT_FLOAT_IN_SCREEN(output,sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep,0.31,0.05+0.01*iLine)
		iLine++
		DEBUG_TEXT_INT_IN_SCREEN( "iEnemyTimeAlive",sDGPenetratorData.sEnemiesData[iEnemy].iEnemyTimeAlive,0.31,0.05+0.01*iLine)
		iLine++
		DEBUG_TEXT_VECTOR_2D_IN_SCREEN("position",DG_PENETRATOR_ENEMY_POSITION(iEnemy),0.31,0.05+0.01*iLine)
		iLine++
	ENDREPEAT
ENDPROC

PROC __DG_PENETRATOR_PRINT_DEBUG_BULLETS_INFO
	INT iLine = 0
	INT iBullet = 0
	REPEAT ciDG_PENETRATOR_BULLET_MAX_AMOUNT iBullet
		TEXT_LABEL_15 output
		output = "Bullet "
		output += iBullet
		DEBUG_TEXT_FLOAT_IN_SCREEN(output,sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep,0.46,0.05+0.01*iLine)
		iLine++
		DEBUG_TEXT_INT_IN_SCREEN( "iBulletTimeAlive",sDGPenetratorData.sBulletsData[iBullet].iBulletTimeAlive,0.46,0.05+0.01*iLine)
		iLine++
		DEBUG_TEXT_VECTOR_2D_IN_SCREEN("position",DG_PENETRATOR_BULLET_POSITION(iBullet),0.46,0.05+0.01*iLine)
		iLine++
	ENDREPEAT
ENDPROC

#ENDIF

//**************************************************//
//*               DRAW - PENETRATOR                *//
//**************************************************//

PROC DG_PENETRATOR_DRAW_BACKGROUND()
	DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DEGENATRON_GAMES_GET_SCREEN_CENTER(), DEGENATRON_GAMES_GET_SCREEN_SIZE(), sDegenatronGamesData.rgbaBlack)
ENDPROC

PROC DG_PENETRATOR_DRAW_HOLE()
	DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_PENETRATOR_SCENE_FRAME,DG_PENETRATOR_HOLE_POSITION(), DG_PENETRATOR_HOLE_SIZE(),0.0,sDegenatronGamesData.rgbaRed)
	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		sDegenatronGamesData.sGameData.iFlashTime += DEGENATRON_GAMES_FRAME_TIME()
		IF NOT sDegenatronGamesData.sGameData.bFlashedOnce
			DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DG_PENETRATOR_HOLE_POSITION(), DG_PENETRATOR_HOLE_SIZE(), sDegenatronGamesData.rgbaDarkGreen)
			sDegenatronGamesData.sGameData.bFlashedOnce = TRUE
		ELIF NOT sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME/2
			DEGENATRON_GAMES_DRAW_PIXELSPACE_RECT(DG_PENETRATOR_HOLE_POSITION(), DG_PENETRATOR_HOLE_SIZE(), sDegenatronGamesData.rgbaDarkGreen)
			sDegenatronGamesData.sGameData.bFlashedTwice = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC DG_PENETRATOR_DRAW_PLAYER()
	IF sDegenatronGamesData.sGameData.bPlayerIsDead
		EXIT
	ENDIF	
	DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_01_SOLID,DG_PENETRATOR_PLAYER_POSITION(), DG_PENETRATOR_PLAYER_SIZE(),0.0, sDegenatronGamesData.rgbaRed)

ENDPROC

PROC DG_PENETRATOR_DRAW_BULLETS()
	INT iBullet
	REPEAT ciDG_PENETRATOR_BULLET_MAX_AMOUNT iBullet
		IF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition != -1
			DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_03_SOLID,DG_PENETRATOR_BULLET_POSITION(iBullet), DG_PENETRATOR_BULLET_SIZE(iBullet),0.0, sDegenatronGamesData.rgbaRed)
			DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_03_SOLID,DG_PENETRATOR_BULLET_POSITION(iBullet,TRUE), DG_PENETRATOR_BULLET_SIZE(iBullet,TRUE),0.0, sDegenatronGamesData.rgbaRed)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DG_PENETRATOR_DRAW_ENEMIES()
	INT iEnemy
	REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
		IF NOT sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead
			DEGENATRON_GAMES_DRAW_PIXELSPACE_SPRITE(DEGENATRON_GAMES_TEXT_ITEM_SQUARE_02_SOLID,DG_PENETRATOR_ENEMY_POSITION(iEnemy), DG_PENETRATOR_ENEMY_SIZE(iEnemy),0.0, sDegenatronGamesData.rgbaGreen)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DG_PENETRATOR_CLIENT_STATE_DRAW
	DG_PENETRATOR_DRAW_BACKGROUND()
	DEGENATRON_GAMES_DRAW_SCORE_BAR(FALSE,FALSE)
	SWITCH sDGPenetratorData.sLevelData.eLevelState
		CASE DG_PENETRATOR_STATE_LEVEL_INIT
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_UPDATE
		CASE DG_PENETRATOR_STATE_LEVEL_DEAD
		CASE DG_PENETRATOR_STATE_LEVEL_WAIT_TIME
			DG_PENETRATOR_DRAW_HOLE()
			DG_PENETRATOR_DRAW_PLAYER()
			DG_PENETRATOR_DRAW_ENEMIES()
			DG_PENETRATOR_DRAW_BULLETS()
			DEGENATRON_GAMES_DRAW_FAIL_SUCCESS_TEXT()
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_END
		BREAK
	ENDSWITCH
ENDPROC



//**************************************************//
//*              UPDATE - PENETRATOR               *//
//**************************************************//
PROC DG_PENETRATOR_UPDATE_PLAYER
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RLEFT))
		IF sDGPenetratorData.sBulletsData[sDGPenetratorData.sLevelData.iBulletIndex].iBulletHolePosition = -1
			ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_BUZZER)
			sDGPenetratorData.sBulletsData[sDGPenetratorData.sLevelData.iBulletIndex].iBulletHolePosition = sDGPenetratorData.sLevelData.iPlayerHolePosition
			sDGPenetratorData.sBulletsData[sDGPenetratorData.sLevelData.iBulletIndex].fBulletHoleDeep = 0.0
			sDGPenetratorData.sBulletsData[sDGPenetratorData.sLevelData.iBulletIndex].iBulletTimeAlive = 0
			sDGPenetratorData.sLevelData.iBulletIndex = (sDGPenetratorData.sLevelData.iBulletIndex + 1)%ciDG_PENETRATOR_BULLET_MAX_AMOUNT
		ENDIF
	ENDIF
	IF sDegenatronGamesData.sControlsInput.iLeftAxisXTick > 0
		ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_1POINT)
		sDGPenetratorData.sLevelData.iPlayerHolePosition = sDGPenetratorData.sLevelData.iPlayerHolePosition - 1
		IF sDGPenetratorData.sLevelData.iPlayerHolePosition < 0
			sDGPenetratorData.sLevelData.iPlayerHolePosition = ciDG_PENETRATOR_HOLE_POSITIONS - 1
		ENDIF
	ENDIF
	IF sDegenatronGamesData.sControlsInput.iLeftAxisXTick < 0
		ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_1POINT)
		sDGPenetratorData.sLevelData.iPlayerHolePosition = (sDGPenetratorData.sLevelData.iPlayerHolePosition + 1)%ciDG_PENETRATOR_HOLE_POSITIONS
	ENDIF
ENDPROC

PROC DG_PENETRATOR_UPDATE_ENEMIES
	INT iEnemy
	REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
		IF sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep = 1.0
			sDGPenetratorData.sEnemiesData[iEnemy].bEnemyOnEdge = TRUE
		ENDIF
		IF NOT sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead
			sDGPenetratorData.sEnemiesData[iEnemy].iEnemyTimeAlive += DEGENATRON_GAMES_FRAME_TIME()
			FLOAT fEnemyHoldDeepPrevious = sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep
			sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep = TO_FLOAT((sDGPenetratorData.sEnemiesData[iEnemy].iEnemyTimeAlive/(ciDG_PENETRATOR_ENEMY_MAX_TIME_ALIVE/ciDG_PENETRATOR_ENEMY_STEPS_TO_EDGE)))/TO_FLOAT(ciDG_PENETRATOR_ENEMY_STEPS_TO_EDGE)
			IF fEnemyHoldDeepPrevious != sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_ARP1)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DG_PENETRATOR_UPDATE_BULLETS
	INT iBullet
	REPEAT ciDG_PENETRATOR_BULLET_MAX_AMOUNT iBullet
		IF sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep = 1.0
			sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition = -1
		ENDIF
		IF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition != -1
			sDGPenetratorData.sBulletsData[iBullet].iBulletTimeAlive += DEGENATRON_GAMES_FRAME_TIME()
			sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep = TO_FLOAT((sDGPenetratorData.sBulletsData[iBullet].iBulletTimeAlive/(ciDG_PENETRATOR_BULLET_MAX_TIME_ALIVE/ciDG_PENETRATOR_BULLET_STEPS_TO_EDGE)))/TO_FLOAT(ciDG_PENETRATOR_BULLET_STEPS_TO_EDGE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DG_PENETRATOR_UPDATE_LEVEL
	INT iEnemy
	REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
		IF sDGPenetratorData.sEnemiesData[iEnemy].bEnemyOnEdge
			sDegenatronGamesData.sGameData.bPlayerIsDead = TRUE
			sDegenatronGamesData.bShouldDoHeavyShake = TRUE AND sDegenatronGamesData.iMachineBashed = 0
			EXIT
		ENDIF
	ENDREPEAT

	INT iBullet
	REPEAT ciDG_PENETRATOR_BULLET_MAX_AMOUNT iBullet
		IF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition != -1
			REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
				IF NOT sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead
					IF sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition = sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition
						IF 1-sDGPenetratorData.sBulletsData[iBullet].fBulletHoleDeep <= sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep
							ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_EXPLODE)
							sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead = TRUE
							sDegenatronGamesData.bShouldDoMidShake = TRUE AND sDegenatronGamesData.iMachineBashed = 0
							sDGPenetratorData.sBulletsData[iBullet].iBulletHolePosition = -1
							sDegenatronGamesData.sGameData.iScore += ciDG_PENETRATOR_ENEMY_SCORE
							sDegenatronGamesData.sTelemetry.kills++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	sDGPenetratorData.sLevelData.iTimeSinceLastEnemy += DEGENATRON_GAMES_FRAME_TIME()
	IF sDGPenetratorData.sLevelData.iTimeSinceLastEnemy > ciDG_PENETRATOR_ENEMY_SPAWN_TIME*2
	OR (sDGPenetratorData.sLevelData.iTimeSinceLastEnemy > ciDG_PENETRATOR_ENEMY_SPAWN_TIME AND GET_RANDOM_INT_IN_RANGE(0,10000) < sDegenatronGamesData.sGameData.iScore)
		REPEAT ciDG_PENETRATOR_ENEMY_MAX_AMOUNT iEnemy
			IF sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead
				sDGPenetratorData.sEnemiesData[iEnemy].bEnemyIsDead = FALSE
				sDGPenetratorData.sEnemiesData[iEnemy].fEnemyHoleDeep = 0.0
				sDGPenetratorData.sEnemiesData[iEnemy].bEnemyOnEdge = FALSE
				sDGPenetratorData.sEnemiesData[iEnemy].iEnemyTimeAlive = 0
				sDGPenetratorData.sEnemiesData[iEnemy].iEnemyHolePosition = GET_RANDOM_INT_IN_RANGE(0,ciDG_PENETRATOR_HOLE_POSITIONS)
				
				sDGPenetratorData.sLevelData.iTimeSinceLastEnemy = 0
				
				sDegenatronGamesData.sTelemetry.level++
				EXIT
			ENDIF
		ENDREPEAT		
	ENDIF
ENDPROC

PROC DG_PENETRATOR_UPDATE
	DG_PENETRATOR_UPDATE_PLAYER()
	DG_PENETRATOR_UPDATE_ENEMIES()
	DG_PENETRATOR_UPDATE_BULLETS()
	DG_PENETRATOR_UPDATE_LEVEL()
ENDPROC

PROC DG_PENETRATOR_UPDATE_LEVEL_STATE()
	SWITCH sDGPenetratorData.sLevelData.eLevelState
		CASE DG_PENETRATOR_STATE_LEVEL_INIT
			sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_UPDATE
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_UPDATE
			IF sDegenatronGamesData.sGameData.bPlayerIsDead
				sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_DEAD
			ENDIF
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_DEAD
			sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_WAIT_TIME
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_WAIT_TIME
			IF sDegenatronGamesData.sGameData.iWaitTime > ciDEGENATRON_GAMES_WAIT_TIME_DEAD
				IF sDegenatronGamesData.sGameData.bPlayerIsDead
					IF sDegenatronGamesData.sGameData.bFlashedOnce AND sDegenatronGamesData.sGameData.bFlashedTwice AND sDegenatronGamesData.sGameData.iFlashTime > ciDEGENATRON_GAMES_PLAYER_FLASH_TIME
						IF sDegenatronGamesData.sGameData.iLifes = 0
							sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_END
						ELSE
							sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_INIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_END
			sDGPenetratorData.sLevelData.eLevelState = DG_PENETRATOR_STATE_LEVEL_INIT
		BREAK
	ENDSWITCH
ENDPROC

PROC DG_PENETRATOR_CLIENT_STATE_UPDATE()
	
	SWITCH sDGPenetratorData.sLevelData.eLevelState
		CASE DG_PENETRATOR_STATE_LEVEL_INIT
			DG_PENETRATOR_LEVEL_INIT()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_UPDATE
			DG_PENETRATOR_UPDATE()
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_DEAD
			sDegenatronGamesData.sGameData.iLifes--
			IF sDegenatronGamesData.sGameData.iLifes > 0
				DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_DEAD)
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_WARN)
			ELSE
				ARCADE_GAMES_SOUND_STOP(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_MAIN_LOOP)
				ARCADE_GAMES_SOUND_PLAY(ARCADE_GAMES_SOUND_DEGENATRON_MUSIC_FAIL)
				DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_GAMEOVER)
			ENDIF
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_WAIT_TIME
			sDegenatronGamesData.sGameData.iWaitTime += DEGENATRON_GAMES_FRAME_TIME()
		BREAK
		CASE DG_PENETRATOR_STATE_LEVEL_END
			DEGENATRON_GAMES_ANIMATION_SET_STATE(DEGENATRON_GAMES_ANIMATION_STATE_PLAYING)
		BREAK
	ENDSWITCH

	sDegenatronGamesData.sTelemetry.score = sDegenatronGamesData.sGameData.iScore
	DEGENATRON_GAMES_SET_CHALLENGE_VALUE(DEGENATRON_GAMES_CHALLENGE_BIT_MASTERFUL_BRONZE,sDegenatronGamesData.sGameData.iScore)
	IF sDegenatronGamesData.sGameData.iScore >= 40000
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_DEGENATRON_RIMMING_MASTER)
	ENDIF
	IF sDegenatronGamesData.sGameData.iScore >= g_sMPTunables.iCH_ARCADE_GAMES_DG_PENETRATOR_GLITCH_SCORE AND sDegenatronGamesData.iMachineBashed != 0
		ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_DEGENATRON_PSYCHONAUT)
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_GLITCHED_DEGENATRON_TSHIRT, TRUE)
	ENDIF
	
	DG_PENETRATOR_UPDATE_LEVEL_STATE()
	
	#IF IS_DEBUG_BUILD
	IF sDegenatronGamesData.bShowHelp
		__DG_PENETRATOR_PRINT_DEBUG_LEVEL_INFO()
		__DG_PENETRATOR_PRINT_DEBUG_ENEMIES_INFO()
		__DG_PENETRATOR_PRINT_DEBUG_BULLETS_INFO()
	ENDIF
	#ENDIF

ENDPROC

#IF IS_DEBUG_BUILD

PROC DG_PENETRATOR_DEBUG_CREATE_WIDGETS
ENDPROC

PROC DG_PENETRATOR_DEBUG_UPDATE_WIDGETS
ENDPROC

#ENDIF

