///    HUD_TEXT.sch
///    Used for displaying text onto the hud and scrolling it while fading out
///    AUTHOR : John R. Diaz

USING "minigames_helpers.sch"

CONST_INT NUM_TEXT_REPS		5

CONST_FLOAT		TAXI_HUD_TEXT_POSX		0.469
CONST_FLOAT		TAXI_HUD_TEXT_POSY		0.387
CONST_FLOAT		TAXI_HUD_TEXT_POSY_END	0.273		// DESTROY HEIGHT
CONST_INT		TAXI_HUD_TEXT_ALPHA		255
CONST_FLOAT		CONST_HUD_TEXT_XSCALE	0.9
CONST_FLOAT		CONST_HUD_TEXT_YSCALE	0.9

//STRUCT HUD_2D_TEXT
//	
//	STRING			sign
//	
//	FLOAT 			posX
//	FLOAT 			posY
//	
//	FLOAT			frameTime
//	
//	INT				alpha
//	
//	INT				ptValue
//	
//	BOOL			bActive
//	
//	HUD_COLOURS		color
//ENDSTRUCT
//
//STRUCT HUD_2D_STRING
//	FLOAT posX
//	FLOAT posY
//
//	INT r
//	INT g
//	INT b
//	INT a
//
//	STRING sValue
//	FLOAT fScale
//ENDSTRUCT

STRUCT HUD_2D_INT
	FLOAT posX
	FLOAT posY

	INT r
	INT g
	INT b
	INT a

	INT iValue
	FLOAT fScale
ENDSTRUCT

//INT			m_iActiveSPT
/// PURPOSE:Gets a string from an int
///    
/// PARAMS:	
///    i - The int to convert
/// RETURNS: The int value as a string
///    
#IF IS_DEBUG_BUILD
FUNC STRING TAXI_UTILS_GET_STRING_FROM_INT_SP(INT i)
	TEXT_LABEL_31 tl23
	tl23 += i
	RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tl23, GET_LENGTH_OF_LITERAL_STRING(tl23))
ENDFUNC

FUNC STRING TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(FLOAT i)
	TEXT_LABEL_31 tl23
	tl23 += ROUND(i)
	RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tl23, GET_LENGTH_OF_LITERAL_STRING(tl23))
ENDFUNC
#ENDIF	//	IS_DEBUG_BUILD

PROC SET_HUD_INT_VALUE(HUD_2D_INT &thisNum, INT iData, FLOAT fPosX, FLOAT fPosY, FLOAT fScale = 1.0, INT iR = 255, INT iG = 255, INT iB= 255, INT iA = 255 )
	
	thisNum.posX = fPosX
	thisNum.posY = fPosY
	
	thisNum.fScale = fScale
	thisNum.r = iR
	thisNum.g = iG
	thisNum.b = iB
	thisNum.a = iA
	thisNum.iValue = iData
ENDPROC

PROC HUD_2D_NUM_DRAW(HUD_2D_INT &thisNum)
	
	IF thisNum.iValue < 0
		thisNum.iValue = 0
	ENDIF

	SET_TEXT_SCALE(thisNum.fScale, thisNum.fScale)
	SET_TEXT_COLOUR(thisNum.r, thisNum.g, thisNum.b, thisNum.a)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	DISPLAY_TEXT_WITH_NUMBER(thisNum.posX,thisNum.posY, "NUMBER", thisNum.iValue)
			
ENDPROC

//PROC SET_HUD_STRING_VALUE(HUD_2D_STRING &thisString, STRING sData, FLOAT fPosX, FLOAT fPosY, FLOAT fScale = 1.0, INT iR = 255, INT iG = 255, INT iB= 255, INT iA = 255 )
//	
//	thisString.posX = fPosX
//	thisString.posY = fPosY
//	
//	thisString.fScale = fScale
//	thisString.r = iR
//	thisString.g = iG
//	thisString.b = iB
//	thisString.a = iA
//	thisString.sValue = sData
//ENDPROC

/*
PROC HUD_2D_STRING_DRAW_LITERAL(HUD_2D_STRING &thisString)
	IF NOT IS_STRING_EMPTY(thisString.sValue)
		SET_TEXT_SCALE(thisString.fScale, thisString.fScale)
		SET_TEXT_COLOUR(thisString.r, thisString.g, thisString.b, thisString.a)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(thisString.posX,thisString.posY, "STRING", thisString.sValue)
	ENDIF
			
ENDPROC


PROC HUD_2D_STRING_DRAW(HUD_2D_STRING &thisString)
	IF NOT IS_STRING_EMPTY(thisString.sValue)
		SET_TEXT_SCALE(thisString.fScale, thisString.fScale)
		SET_TEXT_COLOUR(thisString.r, thisString.g, thisString.b, thisString.a)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
		DISPLAY_TEXT(thisString.posX,thisString.posY, thisString.sValue)
	ENDIF
			
ENDPROC

PROC HUD_2D_AddToQueue(HUD_2D_TEXT &hudTextQueue[], INT iValue,HUD_COLOURS txtColor, STRING whatSign, FLOAT posX = TAXI_HUD_TEXT_POSX, FLOAT posY = TAXI_HUD_TEXT_POSY )
	
	// Make sure we don't array overrun
	IF m_iActiveSPT >= NUM_TEXT_REPS
		// TODO: Check if 0 is still active before we do this..
		m_iActiveSPT = 0
	ENDIF
	
	// Create the new object
	hudTextQueue[m_iActiveSPT].sign = whatSign
	hudTextQueue[m_iActiveSPT].posx = posX
	hudTextQueue[m_iActiveSPT].posY = posY
	hudTextQueue[m_iActiveSPT].alpha = TAXI_HUD_TEXT_ALPHA
	hudTextQueue[m_iActiveSPT].ptValue = iValue
	hudTextQueue[m_iActiveSPT].bActive = TRUE
	hudTextQueue[m_iActiveSPT].frameTime = 0
	hudTextQueue[m_iActiveSPT].color = txtColor
#IF IS_DEBUG_BUILD
	DEBUG_MESSAGE("AddToSPTQueue -- Adding new SPT to queue with value of: ",TAXI_UTILS_GET_STRING_FROM_INT_SP(hudTextQueue[m_iActiveSPT].ptValue))
#ENDIF
	m_iActiveSPT++
	
ENDPROC

/// PURPOSE: Pass in the HUD object to make render with the scroll/fade effect
///    NOTES: Helper function for TAXI_HUD_UpdateText
///   
/// PARAMS:
///    sptObj - The HUD text object
/// RETURNS: TRUE when it's done displaying
///    
FUNC BOOL HUD_2D_Text_Scroll(HUD_2D_TEXT &sptObj, FLOAT fStartPosY = TAXI_HUD_TEXT_POSY, FLOAT fEndPosY = TAXI_HUD_TEXT_POSY_END)
	
	FLOAT fStart,fEnd
	FLOAT fInterpTime
	
	// Increment our frame count
	sptObj.frameTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = sptObj.frameTime / CONST_SPT_LENGTH

	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		
		// Lerp the position
		fStart = fStartPosY
		fEnd = fEndPosY
		sptObj.posY = fStart + (fEnd - fStart) * fInterpTime
		
		// Lerp the alpha
		fStart = TAXI_HUD_TEXT_ALPHA
		fEnd = 0
		
		sptObj.alpha = ROUND(fStart + (fEnd - fStart) * fInterpTime)		
		RETURN FALSE
	ELSE			
		// Dissapear!
		sptObj.alpha = 0
		
		DEBUG_MESSAGE("SPT_Scroll -- Done Scrolling!!")		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC
/// PURPOSE:
///    Updates our Scrolling Point Text.
///    To be Used in TAXI_HUD_Draw2D_Text as a helper.
PROC HUD_2D_UpdateText(HUD_2D_TEXT &hudTextQueue[])
	INT iIter
	
	// Scroll the text up after its been displayed
	REPEAT NUM_TEXT_REPS iIter
		// Is this active
		IF hudTextQueue[iIter].bActive
			IF HUD_2D_Text_Scroll(hudTextQueue[iIter])		
				hudTextQueue[iIter].bActive = FALSE
			ENDIF
		ENDIF		
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Draws the players points when he gains/loses driving points
PROC HUD_2D_Draw_Text(HUD_2D_TEXT &hudTextQueue[])
	INT iIter

	// Update all of the text
	HUD_2D_UpdateText(hudTextQueue)
	
	REPEAT NUM_TEXT_REPS iIter
		IF hudTextQueue[iIter].bActive			
			IF (hudTextQueue[iIter].ptValue <> 0)
				DISPLAY_TEXT_LABEL_WITH_NUMBER(hudTextQueue[iIter].sign, hudTextQueue[iIter].posX, hudTextQueue[iIter].posY,
						CONST_HUD_TEXT_XSCALE, CONST_HUD_TEXT_YSCALE, 1.0, 1.0, GetTextColor(hudTextQueue[iIter].color, hudTextQueue[iIter].alpha), 
						hudTextQueue[iIter].ptValue)
			ELSE
				DISPLAY_TEXT_LABEL(hudTextQueue[iIter].sign, hudTextQueue[iIter].posX, hudTextQueue[iIter].posY,
						CONST_HUD_TEXT_XSCALE, CONST_HUD_TEXT_YSCALE, 1.0, 1.0, GetTextColor(hudTextQueue[iIter].color, hudTextQueue[iIter].alpha))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
*/
//EOF
