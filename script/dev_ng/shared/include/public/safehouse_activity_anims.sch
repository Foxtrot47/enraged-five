
USING "commands_debug.sch"
USING "commands_path.sch"
USING "commands_camera.sch"
USING "commands_brains.sch"

USING "brains.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "drunk_public.sch"
USING "friends_public.sch"
USING "flow_mission_data_public.sch"
USING "flow_public_core.sch"
USING "model_enums.sch"
USING "player_ped_public.sch"
USING "script_player.sch"
USING "types.sch"
USING "selector_public.sch"

ENUM ACTIVITY_ANIM_STATE
	AAS_WAITING,
	AAS_GET_TO_START,
	AAS_START, 
	AAS_ENTRY_ANIM, 
	AAS_ENTRY_TO_RUN_ANIM, 
	AAS_RUN_ANIM,
	AAS_CHOOSE_RANDOM_IDLE,
	AAS_ENTRY_TO_EXIT_ANIM, 
	AAS_RUN_EXIT_ANIM
ENDENUM

STRUCT LOCAL_ACTIVITY_STRUCT
	PED_INDEX piClone
	OBJECT_INDEX oiTrigger
	OBJECT_INDEX oiSecondaryItem
	INT iSynchedScene
	VECTOR vScPos
	VECTOR vScRot
	VECTOR vTrigPos
	FLOAT fTrigRot
	ACTIVITY_ANIM_STATE AnimState
	BOOL bLighterFlame
	BOOL bLighterSparks
	BOOL bBongSmoke
	PTFX_ID ptfxLighter
	PLAYER_INDEX playerIndex
	PED_INDEX pedIndex
	MODEL_NAMES mBathroomDoorModel
	VECTOR vBathroomDoorPos
	INT iLocalDoorHash
ENDSTRUCT

FUNC INT GET_ACTIVITY_SCRIPT_STRING(INT i)
	SWITCH i
		CASE ci_APT_ACT_BONG				RETURN HASH("ob_bong") 				break	
		CASE ci_APT_ACT_BEER				RETURN HASH("ob_franklin_beer")		break
		CASE ci_APT_ACT_SHOTS				RETURN HASH("ob_drinking_shots") 	break	
		CASE ci_APT_ACT_WHEATGRASS			RETURN HASH("ob_wheatgrass") 		break	
		CASE ci_APT_ACT_SHOWER				RETURN HASH("ob_mp_shower_med") 	break	
		CASE ci_APT_ACT_BED_LOW				RETURN HASH("ob_mp_bed_low") 		break
		CASE ci_APT_ACT_BED_MED				RETURN HASH("ob_mp_bed_med") 		break	
		CASE ci_APT_ACT_BED_HI				RETURN HASH("ob_mp_bed_high") 		break	
		CASE ci_APT_ACT_WINE				RETURN HASH("ob_franklin_wine") 	break
		CASE ci_APT_ACT_SHOWER_2			RETURN HASH("ob_mp_shower_med") 	break
		CASE ci_APT_ACT_SHOWER_3			RETURN HASH("ob_mp_shower_med") 	break
		CASE ci_APT_ACT_BED_HI_YACHT_2		RETURN HASH("ob_mp_bed_high") 		break
		CASE ci_APT_ACT_BED_HI_YACHT_3		RETURN HASH("ob_mp_bed_high") 		break
	ENDSWITCH
	
	RETURN HASH("")
ENDFUNC

FUNC STRING GET_ACTIVITY_ANIM_DICT(INT i, PED_INDEX user)
	SWITCH i
		CASE ci_APT_ACT_BONG				RETURN "mp_safehousebong@" 			break	
		CASE ci_APT_ACT_BEER				RETURN "MP_SAFEHOUSEBEER@" 			break	
		CASE ci_APT_ACT_SHOTS				RETURN "MP_SAFEHOUSEWHISKEY@" 		break	
		CASE ci_APT_ACT_WHEATGRASS			RETURN "mp_safehousewheatgrass@" 	break	
		CASE ci_APT_ACT_BED_LOW				RETURN "mp_bedmid" 					break
		CASE ci_APT_ACT_BED_MED				RETURN "mp_bedmid" 					break	
		CASE ci_APT_ACT_BED_HI				RETURN "mp_bedmid" 					break	
		CASE ci_APT_ACT_WINE				RETURN "MP_SAFEHOUSEWINE@" 			break	
		
		CASE ci_APT_ACT_SHOWER				
			IF GET_ENTITY_MODEL(user) = MP_M_FREEMODE_01
				RETURN "mp_safehouseshower@male@" 
			ELSE
				RETURN "mp_safehouseshower@female@"
			ENDIF
		break		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_BEER_BOTTLE_EXIT_ANIM(PED_INDEX mPed)
	
	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(mPed)
	
	SWITCH eDrunkLevel 
		CASE DL_NO_LEVEL		sAnim = "exit_1_bottle"		BREAK
		CASE DL_moderatedrunk	sAnim = "exit_2_bottle"		BREAK
		CASE DL_slightlydrunk	sAnim = "exit_3_bottle"		BREAK
		CASE DL_verydrunk		sAnim = "exit_4_bottle"		BREAK
	ENDSWITCH	
	
	RETURN sAnim
	
ENDFUNC

FUNC STRING GET_BEER_PED_EXIT_ANIM(PED_INDEX mPed)
	
	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(mPed)
	
	SWITCH eDrunkLevel 
		CASE DL_NO_LEVEL		sAnim = "exit_1"	BREAK
		CASE DL_moderatedrunk	sAnim = "exit_2"	BREAK
		CASE DL_slightlydrunk	sAnim = "exit_3"	BREAK
		CASE DL_verydrunk		sAnim = "exit_4"	BREAK
	ENDSWITCH
	
	RETURN sAnim
	
ENDFUNC

FUNC OBJECT_INDEX GET_TRIGGER_OBJECT(INT i, PED_INDEX user)

	VECTOR vpos = GET_ENTITY_COORDS(user)
	
	SWITCH i
		CASE ci_APT_ACT_BONG				
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, PROP_BONG_01, FALSE) 			
		break	
		CASE ci_APT_ACT_BEER				
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, PROP_CS_BEER_BOT_01, FALSE) 			
		break
		CASE ci_APT_ACT_SHOTS				
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, p_tumbler_cs2_s, FALSE) 	
		break	
		CASE ci_APT_ACT_WHEATGRASS			
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, P_W_GRASS_GLS_S, FALSE)  		
		break	
		CASE ci_APT_ACT_SHOWER				
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, P_MP_SHOWERDOOR_S, FALSE)	
		break	
		CASE ci_APT_ACT_WINE				
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, P_WINE_GLASS_S, FALSE)
		break		
	ENDSWITCH
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_SECONDARY_OBJECT(INT i, PED_INDEX user)

	VECTOR vpos = GET_ENTITY_COORDS(user)
	
	SWITCH i
		CASE ci_APT_ACT_BONG			
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, P_CS_LIGHTER_01, FALSE) 	 			
		break	
		CASE ci_APT_ACT_SHOTS			
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, p_whiskey_bottle_s, FALSE)		
		break	
		CASE ci_APT_ACT_WINE			
			RETURN GET_CLOSEST_OBJECT_OF_TYPE(vpos, 5, PROP_WINE_BOT_01, FALSE)	
		break		
	ENDSWITCH
	
	RETURN NULL
ENDFUNC

FUNC INT CONVERT_NEILB_ACTIVITY_TO_ROBS(INT i)
	SWITCH i
		CASE ci_APT_ACT_BONG				RETURN SAFEACT_BONG		break
		CASE ci_APT_ACT_BEER				RETURN SAFEACT_BEER		break	
		CASE ci_APT_ACT_SHOTS				RETURN SAFEACT_WHISKEY 	break	
		CASE ci_APT_ACT_WHEATGRASS			RETURN SAFEACT_WHEATGRS	break	
		CASE ci_APT_ACT_SHOWER				RETURN SAFEACT_SHOWER 	break	
		CASE ci_APT_ACT_BED_LOW				RETURN SAFEACT_BED		break
		CASE ci_APT_ACT_BED_MED				RETURN SAFEACT_BED		break	
		CASE ci_APT_ACT_BED_HI				RETURN SAFEACT_BED		break	
		CASE ci_APT_ACT_WINE				RETURN SAFEACT_WINE 	break		
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL DOES_THIS_ACTIVITY_NEED_SYNCING_LOCALLY(INT iActivity)

	IF iActivity = ci_APT_ACT_WINE	
	OR iActivity = ci_APT_ACT_BEER
	OR iActivity = ci_APT_ACT_WHEATGRASS
	OR iActivity = ci_APT_ACT_BEER
	OR iActivity = ci_APT_ACT_BONG
	OR iActivity = ci_APT_ACT_SHOWER	
	OR iActivity = ci_APT_ACT_SHOTS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_MASK(PED_INDEX mPed, PED_COMP_NAME_ENUM &eReturnItem)	
	// Remove any mask
	SET_PED_COMPONENT_VARIATION(mPed, PED_COMP_BERD, 0, 0)
	
	// After removing mask, restore the hair...
	IF IS_SAFE_TO_RESTORE_SAVED_HAIR_MP(mPed, eReturnItem) 
		SET_PED_COMP_ITEM_CURRENT_MP(mPed, COMP_TYPE_HAIR, eReturnItem, FALSE) 
	ENDIF
	
	IF HAS_PED_HEAD_BLEND_FINISHED(mPed)
	AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(mPed)
		FINALIZE_HEAD_BLEND(mPed) 
	ENDIF	
ENDPROC

PROC RETURN_MASK(PED_INDEX mPed, PED_VARIATION_STRUCT &sVariationStruct)
	
	SET_PED_VARIATIONS(mPed, sVariationStruct)
	
	IF HAS_PED_HEAD_BLEND_FINISHED(mPed)
	AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(mPed)
		FINALIZE_HEAD_BLEND(mPed) 
	ENDIF
					
ENDPROC

PROC MP_COMMON_DO_WINE_ACTIVITY_ANIMS(BOOL bIsActivePlayer, STRING sAnimDict, LOCAL_ACTIVITY_STRUCT &activity, PED_VARIATION_STRUCT &sVariationStruct, PED_COMP_NAME_ENUM &eReturnItem)
		
	STRING sPedEnterAnim, sGlassEnterAnim, sBottleEnterAnim, sGlassExitAnim, sBottleExitAnim
	
	sPedEnterAnim 		= "drinking_wine"
	sGlassEnterAnim 	= "drinking_wine_glass"
	sBottleEnterAnim 	= "drinking_wine_bottle"
	sGlassExitAnim		= "drinking_wine_exit_glass"
	sBottleExitAnim		= "drinking_wine_exit_bottle"
	
	IF (DOES_ENTITY_EXIST(activity.piClone) and not IS_ENTITY_DEAD(activity.piClone))
	AND (DOES_ENTITY_EXIST(activity.oiTrigger) and not IS_ENTITY_DEAD(activity.oiTrigger))
	AND (DOES_ENTITY_EXIST(activity.oiSecondaryItem) and not IS_ENTITY_DEAD(activity.oiSecondaryItem))
	
		SWITCH activity.AnimState
			
			CASE AAS_GET_TO_START
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				// Remember what clothes ped is wearing
				GET_PED_VARIATIONS(activity.piClone, sVariationStruct)
					
				TASK_FOLLOW_NAV_MESH_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, activity.fTrigRot)
				activity.AnimState = AAS_START
				
			BREAK
			
			
			CASE AAS_START
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
					
					REMOVE_MASK(activity.piClone, eReturnItem)
										
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sPedEnterAnim, NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sGlassEnterAnim, sAnimDict, NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, sBottleEnterAnim, sAnimDict, NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
					// Play scene
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wine: TS_RUN_ANIM")
					activity.AnimState = AAS_RUN_ANIM
				
				ENDIF
				
			BREAK
			
			CASE AAS_RUN_ANIM
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					// Exit anim depends on peds drunk level...
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, "drinking_wine_exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sGlassExitAnim, sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)	
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, sBottleExitAnim, sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)		
										
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: Enter anim finished - run exit anim")
					activity.AnimState  = AAS_RUN_EXIT_ANIM
													
				ENDIF
			BREAK 
			
			CASE AAS_RUN_EXIT_ANIM
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					RETURN_MASK(activity.piClone, sVariationStruct)
	
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, "drinking_wine_exit_glass", sAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene, 1.0)
										
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: TS_RUN_EXIT_ANIM -> TS_RESET")
					activity.AnimState = AAS_WAITING
					
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC MP_COMMON_DO_BEER_ACTIVITY_ANIMS(BOOL bIsActivePlayer, STRING sAnimDict, LOCAL_ACTIVITY_STRUCT &activity, PED_VARIATION_STRUCT &sVariationStruct, PED_COMP_NAME_ENUM &eReturnItem)
		
	STRING sPedEnterAnim
	STRING sBottleEnterAnim
	
	sPedEnterAnim 		= "enter"
	sBottleEnterAnim 	= "enter_bottle"
	
	IF (DOES_ENTITY_EXIST(activity.piClone) and not IS_ENTITY_DEAD(activity.piClone))
	AND (DOES_ENTITY_EXIST(activity.oiTrigger) and not IS_ENTITY_DEAD(activity.oiTrigger))
	
		SWITCH activity.AnimState
			
			CASE AAS_GET_TO_START
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				// Remember what clothes ped is wearing
				GET_PED_VARIATIONS(activity.piClone, sVariationStruct)
					
				TASK_FOLLOW_NAV_MESH_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, activity.fTrigRot)
				activity.AnimState = AAS_START
				
			BREAK
			
			
			CASE AAS_START
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
					
					REMOVE_MASK(activity.piClone, eReturnItem)
										
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sPedEnterAnim, NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sBottleEnterAnim, sAnimDict, NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					
					// Play scene
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Wine: TS_RUN_ANIM")
					activity.AnimState = AAS_RUN_ANIM
				
				ENDIF
				
			BREAK
			
			CASE AAS_RUN_ANIM
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					// Exit anim depends on peds drunk level...
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, GET_BEER_PED_EXIT_ANIM(activity.piClone), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, GET_BEER_BOTTLE_EXIT_ANIM(activity.piClone), sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)	
										
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: Enter anim finished - run exit anim")
					activity.AnimState  = AAS_RUN_EXIT_ANIM
													
				ENDIF
			BREAK 
			
			CASE AAS_RUN_EXIT_ANIM
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					RETURN_MASK(activity.piClone, sVariationStruct)
											
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Drinking: TS_RUN_EXIT_ANIM -> TS_RESET")
					activity.AnimState = AAS_WAITING
					
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC MP_COMMON_DO_WHEATGRASS_ACTIVITY_ANIMS(BOOL bIsActivePlayer, STRING sAnimDict, LOCAL_ACTIVITY_STRUCT &activity, PED_VARIATION_STRUCT &sVariationStruct, PED_COMP_NAME_ENUM &eReturnItem)
	
	STRING sPedEnterAnim, sGlassEnterAnim
	
	sPedEnterAnim 		= "ig_2_wheatgrassdrink_michael"
	sGlassEnterAnim 	= "ig_2_wheatgrassdrink_glass"
	
	IF (DOES_ENTITY_EXIST(activity.piClone) and not IS_ENTITY_DEAD(activity.piClone))
	AND (DOES_ENTITY_EXIST(activity.oiTrigger) and not IS_ENTITY_DEAD(activity.oiTrigger))
	
		SWITCH activity.AnimState
		
			CASE AAS_GET_TO_START
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				// Remember what clothes ped is wearing.
				GET_PED_VARIATIONS(activity.piClone, sVariationStruct)
					
				//TASK_FOLLOW_NAV_MESH_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, activity.fTrigRot)
				TASK_GO_STRAIGHT_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, activity.vScRot.z+90, 0.1)
				//TASK_PED_SLIDE_TO_COORD_HDG_RATE(activity.piClone, activity.vTrigPos, activity.vScRot.z,  0.6, 270.0)
				activity.AnimState = AAS_START
			BREAK
						
			CASE AAS_START
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					
					REMOVE_MASK(activity.piClone, eReturnItem)
					
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sPedEnterAnim, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sGlassEnterAnim, sAnimDict, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
										
					// Play scene
					activity.AnimState = AAS_RUN_ANIM
					CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass TS_GRAB_PLAYER -> TS_RUN_ANIM")
				
				ENDIF
				
			BREAK
			
			CASE AAS_RUN_ANIM
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					RETURN_MASK(activity.piClone, sVariationStruct)
					
					CPRINTLN(DEBUG_AMBIENT, "[SH] Wheatgrass TS_RUN_ANIM -> TS_RESET")

					activity.AnimState  = AAS_WAITING
									
				ENDIF
			BREAK 
					
		ENDSWITCH
	ENDIF
ENDPROC

PROC MP_COMMON_DO_BONG_ACTIVITY(BOOL bIsActivePlayer, STRING sAnimDict, LOCAL_ACTIVITY_STRUCT &activity, PED_VARIATION_STRUCT &sVariationStruct, PED_COMP_NAME_ENUM &eReturnItem)
		
	STRING sPedAnim, sBongAnim, sLighterAnim
	
	sPedAnim 		= "bong_FRA"
	sBongAnim 		= "bong_bong"
	sLighterAnim 	= "bong_lighter"
	
	IF (DOES_ENTITY_EXIST(activity.piClone) and not IS_ENTITY_DEAD(activity.piClone))
	AND (DOES_ENTITY_EXIST(activity.oiTrigger) and not IS_ENTITY_DEAD(activity.oiTrigger))
	AND (DOES_ENTITY_EXIST(activity.oiSecondaryItem) and not IS_ENTITY_DEAD(activity.oiSecondaryItem))
		SWITCH activity.AnimState
			
			CASE AAS_GET_TO_START
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				REQUEST_PTFX_ASSET()
				
				//SET_ENTITY_COLLISION(activity.piClone, FALSE)
				
				// Remember what clothes ped is wearing.
				GET_PED_VARIATIONS(activity.piClone, sVariationStruct)
				
				//TASK_FOLLOW_NAV_MESH_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, activity.fTrigRot)
				TASK_GO_STRAIGHT_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVEBLENDRATIO_WALK, 5000, DEFAULT_NAVMESH_RADIUS, activity.fTrigRot)
				
				activity.AnimState = AAS_START
				
			BREAK
			
			
			CASE AAS_START
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					
					//SET_ENTITY_COLLISION(activity.piClone, TRUE)
					
					REMOVE_MASK(activity.piClone, eReturnItem)
					
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sPedAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sBongAnim, sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, sLighterAnim, sAnimDict, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)	
							
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_CHECK_SMOKE_ANIM")
					activity.AnimState = AAS_RUN_ANIM
				
				ENDIF
				
			BREAK
			
			CASE AAS_RUN_ANIM
					
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
						
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) < 0.95 AND HAS_PTFX_ASSET_LOADED()
						
					// Lighter flame
					IF activity.bLighterFlame = FALSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.194
							activity.ptfxLighter = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_lighter_flame", activity.oiSecondaryItem, <<-0.005,0.0,0.05>>, <<0.0,0.0,0.0>>)
							activity.bLighterFlame = TRUE
						ENDIF
					ELSE
						IF DOES_PARTICLE_FX_LOOPED_EXIST(activity.ptfxLighter)
							IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.438
								STOP_PARTICLE_FX_LOOPED(activity.ptfxLighter)
							ENDIF
						ENDIF
					ENDIF
			
//					// Lighter sparks
					IF activity.bLighterSparks = FALSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.185
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sh_lighter_sparks", activity.oiSecondaryItem, << 0.0, 0.0, 0.06 >>, << 0.0, 0.0, 0.0 >>)
							activity.bLighterSparks = TRUE
						ENDIF
					ENDIF
//					
//					// Bong smoke fx
					IF activity.bBongSmoke = FALSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.440
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", activity.piClone, << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
							activity.bBongSmoke = TRUE
						ENDIF
					ENDIF

				// Synchronised scene has finished
				ELSE			
					CPRINTLN(DEBUG_AMBIENT, "[Safehouse] Bong: MP_TS_RESET")
					RETURN_MASK(activity.piClone, sVariationStruct)
					activity.AnimState  = AAS_WAITING
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC MP_COMMON_DO_SHOWER_ACTIVITY(BOOL bIsActivePlayer, STRING sAnimDict, LOCAL_ACTIVITY_STRUCT &activity, INT iActivitySubStage, PED_VARIATION_STRUCT &sVariationStruct, PED_COMP_NAME_ENUM &eReturnItem)
	
	STRING sEnterAnim, sEnterToIdle, sIdleToExit, sIdleAnimA, sIdleAnimB, sIdleAnimC	
	STRING sEnterAnimDoor, sExitAnimDoor
	FLOAT fNakedPhase, fNonNakedPhase
	
	IF ARE_STRINGS_EQUAL(sAnimDict, "mp_safehouseshower@male@")
		sEnterAnim			= "male_shower_undress_&_turn_on_water"
		sEnterToIdle		= "male_shower_enter_into_idle"
		sIdleAnimA			= "male_shower_idle_a"
		sIdleAnimB			= "male_shower_idle_b"
		sIdleAnimC			= "male_shower_idle_c"
		sIdleToExit			= "Male_Shower_Exit_To_Idle"
		
		sEnterAnimDoor		= "male_shower_undress_&_turn_on_water_door"
		sExitAnimDoor		= "Male_Shower_Exit_To_Idle_Door"
		
		fNakedPhase			= 0.5
		fNonNakedPhase		= 0.55
	ELSE
		sEnterAnim 			= "shower_undress_&_turn_on_water"
		sEnterToIdle		= "shower_enter_into_idle"
		sIdleToExit			= "shower_Exit_To_Idle"
		sIdleAnimA			= "shower_idle_a"
		sIdleAnimB			= "shower_idle_b"
		sIdleAnimC			= "shower_idle_a"
		
		sEnterAnimDoor 		= "shower_undress_&_turn_on_water_door"
		sExitAnimDoor 		= "shower_Exit_To_Idle_Door"
		
		fNakedPhase			= 0.5
		fNonNakedPhase		= 0.5
	ENDIF
	
	IF (DOES_ENTITY_EXIST(activity.piClone) and not IS_ENTITY_DEAD(activity.piClone))
	AND (DOES_ENTITY_EXIST(activity.oiTrigger) and not IS_ENTITY_DEAD(activity.oiTrigger))	
		
		SWITCH activity.AnimState
			
			CASE AAS_GET_TO_START
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				// Close the bathroom door
				DOOR_SYSTEM_SET_OPEN_RATIO(activity.iLocalDoorHash, 0.0, FALSE, FALSE)
				
				// Lock the bathroom door
				DOOR_SYSTEM_SET_DOOR_STATE(activity.iLocalDoorHash, DOORSTATE_LOCKED, FALSE, TRUE)
				
				// Remember what clothes ped is wearing.
				GET_PED_VARIATIONS(activity.piClone, sVariationStruct)
				
				activity.AnimState = AAS_START
			BREAK
			
			
			CASE AAS_START
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				//IF GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
				//AND GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
														
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sEnterAnim, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sEnterAnimDoor, sAnimDict, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
					
					CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_CHECK_ENTRY_ANIM")
					activity.AnimState = AAS_ENTRY_ANIM
					
				//ENDIF
				
			BREAK
			
			CASE AAS_ENTRY_ANIM
				                  
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
				
					CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_RUN_ENTER_TO_IDLE_ANIM")
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sEnterToIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									
					CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_CHECK_ENTER_TO_IDLE_ANIM")
					activity.AnimState = AAS_ENTRY_TO_RUN_ANIM
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > fNakedPhase
					
						IF GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_JBIB) != 15 
						OR GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_TORSO) != 15 
						OR GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_SPECIAL) != 15 
						OR GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_SPECIAL2) != 0 
						OR GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_TEETH) != 0 
						OR GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_HAND) != 0 
						OR GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_FEET) != 5 
						OR (GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_LEG) != 14 AND GET_ENTITY_MODEL(activity.piClone) = MP_M_FREEMODE_01)
						OR (GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_LEG) != 15 AND GET_ENTITY_MODEL(activity.piClone) != MP_M_FREEMODE_01)
												
							CPRINTLN(DEBUG_AMBIENT, "[SH] shower - taking clothes off")
							
							SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_JBIB, 15, 0)
							
							IF GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_TORSO) != 15
								SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_TORSO, 15, 0)
							ENDIF
							
							SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_SPECIAL, 15, 0)
							SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_SPECIAL2, 0, 0)
							SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_TEETH, 0, 0)
							SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_HAND, 0, 0)
							
							IF GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_FEET) != 5
								SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_FEET, 5, 0)
							ENDIF
							
							SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_BERD, 0, 0)		// Store the mask
				
							IF IS_SAFE_TO_RESTORE_SAVED_HAIR_MP(activity.piClone, eReturnItem) 
								SET_PED_COMP_ITEM_CURRENT_MP(activity.piClone, COMP_TYPE_HAIR, eReturnItem, FALSE) 
							ENDIF
	
							CLEAR_ALL_PED_PROPS(activity.piClone)
														 
							IF GET_ENTITY_MODEL(activity.piClone) = MP_M_FREEMODE_01
											
								IF GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_LEG) != 14	
									SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_LEG, 14, 0)
								ENDIF						

							ELSE
								
								IF GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_LEG) != 15
									SET_PED_COMPONENT_VARIATION(activity.piClone, PED_COMP_LEG, 15, 0)
								ENDIF
								
							ENDIF
							
							IF HAS_PED_HEAD_BLEND_FINISHED(activity.piClone)
							AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(activity.piClone)
								FINALIZE_HEAD_BLEND(activity.piClone) 
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			
			BREAK
			
			CASE AAS_ENTRY_TO_RUN_ANIM
							
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					INT iChosenIdle
					STRING sChosenIdle
					
					iChosenIdle = GET_RANDOM_INT_IN_RANGE(0, 3)				
					IF iChosenIdle = 0
						sChosenIdle = sIdleAnimA
					ELIF iChosenIdle = 1
						sChosenIdle = sIdleAnimB
					ELSE
						sChosenIdle = sIdleAnimC
					ENDIF
				
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sChosenIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
													
					CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_CHECK_IDLE_ANIM")
					activity.AnimState = AAS_RUN_ANIM
				
				ELSE	
				
					IF iActivitySubStage = ci_APT_ACT_STATE_FINISHING	
						CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_RUN_EXIT_SHOWER_ANIM")
						
						activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
						TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sIdleToExit, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sExitAnimDoor, sAnimDict, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
						
						activity.AnimState = AAS_RUN_EXIT_ANIM
					ENDIF				
				ENDIF
				
			BREAK
					
			CASE AAS_RUN_ANIM
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_CHOOSE_RANDOM_IDLE")
					activity.AnimState = AAS_ENTRY_TO_RUN_ANIM
				ENDIF	
						
				IF iActivitySubStage = ci_APT_ACT_STATE_FINISHING	
										
					CPRINTLN(DEBUG_AMBIENT, "[SH SHOWER] - TS_RUN_EXIT_SHOWER_ANIM")
					
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sIdleToExit, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sExitAnimDoor, sAnimDict, REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
						
					activity.AnimState = AAS_RUN_EXIT_ANIM
				ENDIF

			BREAK
			
			CASE AAS_RUN_EXIT_ANIM
							                  
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					// Unlock the bathroom door
					DOOR_SYSTEM_SET_DOOR_STATE(activity.iLocalDoorHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
					
					activity.AnimState = AAS_WAITING
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > fNonNakedPhase
						// Put clothes back on
						IF GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_JBIB) = 15 
						AND GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_TORSO) = 15 
						AND GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_SPECIAL) = 15 
						AND GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_SPECIAL2) = 0 
						AND GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_TEETH) = 0 
						AND GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_HAND) = 0 
						AND GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_FEET) = 5 
						AND ((GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_LEG) = 14 AND GET_ENTITY_MODEL(activity.piClone) = MP_M_FREEMODE_01)
						OR (GET_PED_DRAWABLE_VARIATION(activity.piClone, PED_COMP_LEG) = 15 AND GET_ENTITY_MODEL(activity.piClone) != MP_M_FREEMODE_01))
							
							SET_PED_VARIATIONS(activity.piClone, sVariationStruct)
							
							IF HAS_PED_HEAD_BLEND_FINISHED(activity.piClone)
							AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(activity.piClone)
								FINALIZE_HEAD_BLEND(activity.piClone) 
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
										
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Get the correct idle anim based on how many shots the player has had.
FUNC STRING GET_MP_IDLE_ANIM_FOR_THIS_SHOT(INT iTimesUsed)

	STRING sAnim
	
	SWITCH iTimesUsed		
		CASE 0  sAnim = "first_shot_base"	BREAK
		CASE 1  sAnim = "first_shot_base" 	BREAK
		CASE 2  sAnim = "second_shot_base"  BREAK 
		CASE 3  sAnim = "third_shot_base" 	BREAK 
		CASE 4  sAnim = "fourth_shot_base" 	BREAK 
		DEFAULT sAnim = "fifth_shot_base"  	BREAK
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_SHOT_ANIM_FOR_THIS_SHOT(INT iTimesUsed)

	STRING sAnim
	
	SWITCH iTimesUsed 
		CASE 0	sAnim = "first_shot"	BREAK
		CASE 1	sAnim = "first_shot"	BREAK
		CASE 2	sAnim = "second_shot"	BREAK
		CASE 3	sAnim = "third_shot"	BREAK
		CASE 4	sAnim = "fourth_shot"	BREAK
		CASE 5	sAnim = "fifth_shot"	BREAK
	ENDSWITCH
					
	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_EXIT_ANIM_FOR_THIS_SHOT(PED_INDEX ped)

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(ped)
	
	SWITCH eDrunkLevel 
		CASE DL_NO_LEVEL		sAnim = "exit_sober"					BREAK
		CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk"			BREAK
		CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk"			BREAK
		CASE DL_verydrunk		sAnim = "exit_drunk"					BREAK
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_SHOT_ANIM_FOR_GLASS(INT iTimesUsed)
	
	STRING sAnim
	
	SWITCH iTimesUsed 
		CASE 0	sAnim = "first_shot_glass"	BREAK
		CASE 1	sAnim = "first_shot_glass"	BREAK
		CASE 2	sAnim = "second_shot_glass"	BREAK
		CASE 3	sAnim = "third_shot_glass"	BREAK
		CASE 4	sAnim = "fourth_shot_glass"	BREAK
		CASE 5	sAnim = "fifth_shot_glass"	BREAK
	ENDSWITCH				

	RETURN sAnim
	
ENDFUNC

FUNC STRING GET_MP_SHOT_ANIM_FOR_BOTTLE(INT iTimesUsed)

	STRING sAnim
	
	SWITCH iTimesUsed
		CASE 0	sAnim = "first_shot_bot"	BREAK
		CASE 1	sAnim = "first_shot_bot"	BREAK
		CASE 2	sAnim = "second_shot_bot"	BREAK
		CASE 3	sAnim = "third_shot_bot"	BREAK
		CASE 4	sAnim = "fourth_shot_bot"	BREAK
		CASE 5	sAnim = "fifth_shot_bot"	BREAK
	ENDSWITCH	

	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_SHOT_EXIT_ANIM_FOR_GLASS(PED_INDEX ped)

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(ped)
	
	SWITCH eDrunkLevel 
		CASE DL_NO_LEVEL		sAnim = "exit_sober_glass"	BREAK
		CASE DL_moderatedrunk	sAnim = "exit_sober_glass"	BREAK
		CASE DL_slightlydrunk	sAnim = "exit_drunk_glass"	BREAK
		CASE DL_verydrunk		sAnim = "exit_drunk_glass"	BREAK
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_SHOT_EXIT_ANIM_FOR_BOTTLE(PED_INDEX ped)

	STRING sAnim
	g_eDrunkLevel eDrunkLevel = Get_Peds_Drunk_Level(ped)
	
	SWITCH eDrunkLevel 
		CASE DL_NO_LEVEL		sAnim = "exit_sober_bot"			BREAK
		CASE DL_moderatedrunk	sAnim = "exit_moderately_drunk_bot"	BREAK
		CASE DL_slightlydrunk	sAnim = "exit_slightly_drunk_bot"	BREAK
		CASE DL_verydrunk		sAnim = "exit_drunk_bot"			BREAK
	ENDSWITCH					
	
	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_SHOT_IDLE_ANIM_FOR_BOTTLE(INT iTimesUsed)

	STRING sAnim

	SWITCH (iTimesUsed)		
		CASE 0  sAnim = "first_shot_base_bot"	BREAK
		CASE 1  sAnim = "second_shot_base_bot" 	BREAK
		CASE 2  sAnim = "third_shot_base_bot"  	BREAK 
		CASE 3  sAnim = "fourth_shot_base_bot" 	BREAK 
		DEFAULT sAnim = "fifth_shot_base_bot"	BREAK
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

FUNC STRING GET_MP_SHOT_IDLE_ANIM_FOR_GLASS(INT iTimesUsed)

	STRING sAnim		
	
	SWITCH (iTimesUsed)		
		CASE 0  sAnim = "first_shot_base_glass"  	BREAK
		CASE 1  sAnim = "second_shot_base_glass" 	BREAK
		CASE 2  sAnim = "third_shot_base_glass"  	BREAK 
		CASE 3  sAnim = "fourth_shot_base_glass" 	BREAK 
		DEFAULT sAnim = "fifth_shot_base_glass"  	BREAK
	ENDSWITCH
	
	RETURN sAnim
ENDFUNC

PROC MP_COMMON_DO_SHOTS_ACTIVITY(BOOL bIsActivePlayer, STRING sAnimDict, LOCAL_ACTIVITY_STRUCT &activity, INT iActivitySubStage, PED_VARIATION_STRUCT &sVariationStruct, INT &iCloneShotNum)
		
	INT iTimesUsed = iActivitySubStage - 3
		
	IF iTimesUsed < 0
		iTimesUsed = 0
	ENDIF
	
//	CPRINTLN(DEBUG_AMBIENT, "[CLONE] iActivitySubStage = ", iActivitySubStage)
//	CPRINTLN(DEBUG_AMBIENT, "[CLONE]        iTimesUsed = ", iTimesUsed)
	
	STRING sPedEnterAnim
	STRING sGlassEnterAnim
	STRING sBottleEnterAnim
	
	sPedEnterAnim 		= "enter"	
	sGlassEnterAnim		= "enter_glass"	
	sBottleEnterAnim 	= "enter_bot"
	
	IF (DOES_ENTITY_EXIST(activity.piClone) and not IS_ENTITY_DEAD(activity.piClone))
	AND (DOES_ENTITY_EXIST(activity.oiTrigger) and not IS_ENTITY_DEAD(activity.oiTrigger))
	AND (DOES_ENTITY_EXIST(activity.oiSecondaryItem) and not IS_ENTITY_DEAD(activity.oiSecondaryItem))	
		SWITCH activity.AnimState		
			
			CASE AAS_GET_TO_START
				
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				// Remember what clothes ped is wearing.
				GET_PED_VARIATIONS(activity.piClone, sVariationStruct)
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(activity.piClone, activity.vTrigPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, activity.fTrigRot)
				activity.AnimState = AAS_START
				
			BREAK
						
			CASE AAS_START
			
				IF bIsActivePlayer
					DISABLE_SELECTOR_THIS_FRAME()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_ALL_MP_HUD_THIS_FRAME()
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(activity.piClone, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
							
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					
					// Apply drinking anims to player and glass
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, sPedEnterAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, sGlassEnterAnim, sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, sBottleEnterAnim, sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
								
					CPRINTLN(DEBUG_AMBIENT, "[CLONE] TS_WAIT_FOR_MORE_INPUT")		
					activity.AnimState = AAS_ENTRY_ANIM
				ENDIF
				
			BREAK
					
			CASE AAS_ENTRY_ANIM
				
				// Apply the idle if we're not already playing it...
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
					
					CPRINTLN(DEBUG_AMBIENT, "[CLONE] ANIM FINISHED, APPLY THE IDLE AND WAIT FOR MORE INPUT")				
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
						
					// Apply looped idle anims to player and props
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, GET_MP_IDLE_ANIM_FOR_THIS_SHOT(iTimesUsed), SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, GET_MP_SHOT_IDLE_ANIM_FOR_GLASS(iTimesUsed), sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, GET_MP_SHOT_IDLE_ANIM_FOR_BOTTLE(iTimesUsed), sAnimDict, SLOW_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE))
					iCloneShotNum = iTimesUsed
					
					CPRINTLN(DEBUG_AMBIENT, "[CLONE] PLAY IDLE NUMBER ", iTimesUsed)
					activity.AnimState = AAS_CHOOSE_RANDOM_IDLE
				ENDIF	
			BREAK
			
			CASE AAS_CHOOSE_RANDOM_IDLE
			
//				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99
//					activity.AnimState = AAS_ENTRY_ANIM
//				ENDIF
				
				IF iActivitySubStage = ci_APT_ACT_STATE_FINISHING
					activity.AnimState = AAS_ENTRY_TO_EXIT_ANIM
					
				ELIF iCloneShotNum != iTimesUsed
					activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
					TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, GET_MP_SHOT_ANIM_FOR_THIS_SHOT(iTimesUsed), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, GET_MP_SHOT_ANIM_FOR_GLASS(iTimesUsed), sAnimDict, SLOW_BLEND_IN, SLOW_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, GET_MP_SHOT_ANIM_FOR_BOTTLE(iTimesUsed), sAnimDict, SLOW_BLEND_IN, SLOW_BLEND_OUT)
					
					CPRINTLN(DEBUG_AMBIENT, "[CLONE] PLAY SHOT NUMBER ", iTimesUsed)
					iCloneShotNum = iTimesUsed
					activity.AnimState = AAS_ENTRY_ANIM
				ENDIF
			BREAK
			
			CASE AAS_ENTRY_TO_EXIT_ANIM
										
				activity.iSynchedScene = CREATE_SYNCHRONIZED_SCENE(activity.vScPos, activity.vScRot)
				TASK_SYNCHRONIZED_SCENE(activity.piClone, activity.iSynchedScene, sAnimDict, GET_MP_EXIT_ANIM_FOR_THIS_SHOT(activity.pedIndex), SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiTrigger, activity.iSynchedScene, GET_MP_SHOT_EXIT_ANIM_FOR_GLASS(activity.pedIndex), sAnimDict, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(activity.oiSecondaryItem, activity.iSynchedScene, GET_MP_SHOT_EXIT_ANIM_FOR_BOTTLE(activity.pedIndex), sAnimDict, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
												
				activity.AnimState = AAS_RUN_EXIT_ANIM
				
			BREAK 
									
			CASE AAS_RUN_EXIT_ANIM
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(activity.iSynchedScene) > 0.99			
					
					activity.AnimState = AAS_WAITING
				
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC ANIMATE_LOCAL_SYNC_SCENE(BOOL bIsActivePlayer, INT iActivity, LOCAL_ACTIVITY_STRUCT &activity, INT iActivitySubStage, PED_VARIATION_STRUCT &sVariationStruct, INT &iCloneShotNum, PED_COMP_NAME_ENUM &eReturnItem)
	SWITCH iActivity
		CASE ci_APT_ACT_WINE	
			MP_COMMON_DO_WINE_ACTIVITY_ANIMS(bIsActivePlayer, "MP_SAFEHOUSEWINE@", activity, sVariationStruct, eReturnItem)	
		BREAK
		CASE ci_APT_ACT_WHEATGRASS	
			MP_COMMON_DO_WHEATGRASS_ACTIVITY_ANIMS(bIsActivePlayer, "mp_safehousewheatgrass@", activity, sVariationStruct, eReturnItem)	
		BREAK	
		CASE ci_APT_ACT_BONG
			MP_COMMON_DO_BONG_ACTIVITY(bIsActivePlayer, "mp_safehousebong@", activity, sVariationStruct, eReturnItem)
		BREAK
		CASE ci_APT_ACT_BEER
			MP_COMMON_DO_BEER_ACTIVITY_ANIMS(bIsActivePlayer, "MP_SAFEHOUSEBEER@", activity, sVariationStruct, eReturnItem)
		BREAK
		
		// COMPLICATED ONES....
		CASE ci_APT_ACT_SHOWER	
			IF GET_ENTITY_MODEL(activity.piClone) = MP_M_FREEMODE_01
				MP_COMMON_DO_SHOWER_ACTIVITY(bIsActivePlayer, "mp_safehouseshower@male@", activity, iActivitySubStage, sVariationStruct, eReturnItem)
			ELSE
				MP_COMMON_DO_SHOWER_ACTIVITY(bIsActivePlayer, "mp_safehouseshower@female@", activity, iActivitySubStage, sVariationStruct, eReturnItem)
			ENDIF			
		BREAK	
		CASE ci_APT_ACT_SHOTS
			MP_COMMON_DO_SHOTS_ACTIVITY(bIsActivePlayer, "MP_SAFEHOUSEWHISKEY@", activity, iActivitySubStage, sVariationStruct, iCloneShotNum)
		BREAK		
	ENDSWITCH
	
ENDPROC

