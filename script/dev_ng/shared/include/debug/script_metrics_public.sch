
USING "rage_builtins.sch"
USING "globals.sch"
USING "net_events.sch"

#IF IS_DEBUG_BUILD


STRING DebugFilePath = "X:/gta5/build/dev"
STRING DebugFileName = "script_metrics.log"

PROC KEEP_ANY_SCRIPT_METRIC_RECORDING()
	IF (b_ScriptMetricsAreRecording)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				GlobalServerBD.g_bRecordScriptMetrics = TRUE
			ENDIF
		ELSE
			GlobalServerBD.g_bRecordScriptMetrics = TRUE
		ENDIF
	ENDIF
ENDPROC


PROC INIT_THREAD_METRIC_RECORDINGS()
	INT i
	REPEAT MAX_SCRIPT_METRIC_THREADS i
		RecordingThreads[i].Thread_ID = INT_TO_NATIVE(THREADID, -1)	
		RecordingThreads[i].iStartTime = 0
	ENDREPEAT
ENDPROC

FUNC INT GET_THREAD_DURATION(THREADID Thread_ID)
	INT i
	INT iFreeSlot = -1
	REPEAT MAX_SCRIPT_METRIC_THREADS i
		IF IS_THREAD_ACTIVE(RecordingThreads[i].Thread_ID)
			IF (RecordingThreads[i].Thread_ID = Thread_ID)
				IF NOT (RecordingThreads[i].iStartTime = 0)
					RETURN(GET_GAME_TIMER() - RecordingThreads[i].iStartTime)
				ELSE
					RecordingThreads[i].iStartTime = GET_GAME_TIMER()
					RETURN(0)
				ENDIF
			ENDIF
		ELSE
			IF NOT (RecordingThreads[i].Thread_ID = INT_TO_NATIVE(THREADID, -1))
				RecordingThreads[i].Thread_ID = INT_TO_NATIVE(THREADID, -1)	
				RecordingThreads[i].iStartTime = 0
			ENDIF
			IF (iFreeSlot = -1)
				iFreeSlot = i
			ENDIF				
		ENDIF
	ENDREPEAT
	
	// this thread is not being recorded yet, start now.
	IF NOT (iFreeSlot = -1)
		RecordingThreads[iFreeSlot].Thread_ID	= Thread_ID
		RecordingThreads[iFreeSlot].iStartTime = GET_GAME_TIMER()
		RETURN(0)
	ENDIF
	
	RETURN(-1)
	
ENDFUNC

FUNC BOOL AM_I_HOST_OF_THREAD(THREADID Thread_ID)	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_THREAD_A_NETWORK_SCRIPT(Thread_ID)
			PLAYER_INDEX HostPlayer
			HostPlayer = NETWORK_GET_HOST_OF_THREAD(Thread_ID)
			IF (PLAYER_ID() = HostPlayer)
				RETURN(TRUE)
			ENDIF
			RETURN(FALSE)	
		ELSE
			PRINTLN("NETWORK_IS_THREAD_A_NETWORK_SCRIPT = FALSE - called by ", GET_THIS_SCRIPT_NAME()) 
			RETURN(FALSE)
		ENDIF
	ELSE
		RETURN(FALSE)
	ENDIF
ENDFUNC

PROC ADD_SCRIPT_METRIC_MARKER(STRING sMarkerMessage)

	IF (b_ScriptMetricsAreRecording)

		TEXT_LABEL_63 strNetworkTime
		strNetworkTime = "NetworkTime:"
		strNetworkTime += NATIVE_TO_INT(GET_NETWORK_TIME())
		strNetworkTime += ";"

		TEXT_LABEL_63 strPlayerInfo
		
		strPlayerInfo = "PID:"
		strPlayerInfo += NATIVE_TO_INT(GET_PLAYER_INDEX())
		strPlayerInfo += ";"
		
		strPlayerInfo += "Team:"
		IF NETWORK_IS_GAME_IN_PROGRESS()
			strPlayerInfo += GET_PLAYER_TEAM(GET_PLAYER_INDEX())
		ELSE
			strPlayerInfo += -1
		ENDIF
		strPlayerInfo += ";"
		
		IF AM_I_HOST_OF_THREAD(GET_ID_OF_THIS_THREAD())
			strPlayerInfo += "Host:1;"	
		ELSE
			strPlayerInfo += "Host:0;"
		ENDIF		

//		CPRINTLN(DEBUG_METRIC_DATA, 
//			strPlayerInfo,
//			"Script:",
//			GET_THIS_SCRIPT_NAME(),
//			";Marker:",
//			sMarkerMessage,
//			";"
//			// times
//			//";Current:",	METRICS_GET_UPDATE_TIME_FOR_THREAD(GET_ID_OF_THIS_THREAD()),
//			//";Average:", METRICS_GET_AVERAGE_UPDATE_TIME_FOR_THREAD(GET_ID_OF_THIS_THREAD()),
//			//";Peak:", METRICS_GET_PEAK_UPDATE_TIME_FOR_THREAD(GET_ID_OF_THIS_THREAD()), ";"
//			)
			
		// write to external file
		SAVE_STRING_TO_NAMED_DEBUG_FILE(strNetworkTime, DebugFilePath, DebugFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(strPlayerInfo, DebugFilePath, DebugFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("Script:", DebugFilePath, DebugFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_THIS_SCRIPT_NAME(), DebugFilePath, DebugFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(";Marker:", DebugFilePath, DebugFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(sMarkerMessage, DebugFilePath, DebugFileName)	
		SAVE_STRING_TO_NAMED_DEBUG_FILE(";", DebugFilePath, DebugFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(DebugFilePath, DebugFileName)

	ENDIF

ENDPROC



#ENDIF

// eof
