// name:        net_hud_debug_CnC.sch
// description: Hud Creation Tool
// written by:  Andy Duthie

USING "globals.sch"
USING "net_include.sch"

USING "script_player.sch"
USING "commands_graphics.sch"



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES FOR MISSION /////////////////////////////////////////////////////////////////////////
//Increment this if you need more pieces
CONST_INT MAX_NUM_PIECES 	30

CONST_INT HUD_STATE_INI 		0
CONST_INT HUD_STATE_WAIT 		1
CONST_INT HUD_STATE_RUNNING		2

CONST_INT DEBUG_PIECES_INI		0
CONST_INT DEBUG_PIECES_ADD		1

INT HudState = 0
INT DebugState = 0
//BOOL DebugMode = FALSE
BOOL bEditingBool = FALSE
BOOL bHasEditWidgetBeenCreated = FALSE

INT RectPieceNum = 0
INT TextPieceNum = 0
INT SpritePieceNum = 0

BOOL AddRectPiece = FALSE
BOOL AddTextPiece = FALSE
BOOL AddSpritePiece = FALSE

BOOL FinishAddingRectPiece[MAX_NUM_PIECES] 
BOOL FinishAddingRectPieceHUD[MAX_NUM_PIECES] 
BOOL FinishAddingTextPiece[MAX_NUM_PIECES]
BOOL FinishAddingTextPieceHUD[MAX_NUM_PIECES]
BOOL FinishAddingSpritePiece[MAX_NUM_PIECES]
BOOL FinishAddingSpritePieceHUD[MAX_NUM_PIECES]
BOOL FinishAddingSpriteTexData[MAX_NUM_PIECES]
BOOL bTextEntered[MAX_NUM_PIECES]
BOOL bSpriteLoaded[MAX_NUM_PIECES]

//	TEXTURE_ID SpriteTextureID[MAX_NUM_PIECES]

WIDGET_GROUP_ID RootWidget
WIDGET_GROUP_ID RectWidget[MAX_NUM_PIECES]
WIDGET_GROUP_ID TextWidget[MAX_NUM_PIECES]
TEXT_WIDGET_ID SpriteTextureInputWidget[MAX_NUM_PIECES]
TEXT_WIDGET_ID SpriteDictInputWidget[MAX_NUM_PIECES]
WIDGET_GROUP_ID SpriteWidget[MAX_NUM_PIECES]

WIDGET_GROUP_ID WG_AddNewPiece
TEXT_WIDGET_ID TxtInputWidget[MAX_NUM_PIECES]
WIDGET_GROUP_ID WG_Editing

//draw rect struct
STRUCT DrawRectStruct
	FLOAT CentreX				
	FLOAT CentreY
	FLOAT Width
	FLOAT Height
	INT R
	INT G
	INT B
	INT A
ENDSTRUCT
DrawRectStruct DrawRect[MAX_NUM_PIECES]

//draw sprite struct
STRUCT DrawSpriteStruct
	FLOAT CentreX				
	FLOAT CentreY
	STRING sSpriteLabel
	STRING sSpriteDictLabel
	FLOAT Width
	FLOAT Height
	FLOAT Rotation
	INT R
	INT G
	INT B
	INT A
ENDSTRUCT
DrawSPRITEStruct DrawSprite[MAX_NUM_PIECES]

//draw text struct
STRUCT DisplayTextStruct
	FLOAT DisplayAtX				
	FLOAT DisplayAtY
	STRING sTxtLabel 
	FLOAT TextXScale
	FLOAT TextYScale
	INT R
	INT G
	INT B
	INT A
	BOOL JustifyCentre 
	INT DropshadowAmount
	INT DropShadowAlpha
	INT TextEdgeAmount
	INT TextEdgeAlpha
	BOOL bUseStringFromScript
ENDSTRUCT
DisplayTextStruct DisplayTxt[MAX_NUM_PIECES]



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC LOAD_SPRITE(INT i)

	IF bTextEntered[i]
		DrawSprite[i].sSpriteLabel = GET_CONTENTS_OF_TEXT_WIDGET(SpriteTextureInputWidget[i])
		DrawSprite[i].sSpriteDictLabel = GET_CONTENTS_OF_TEXT_WIDGET(SpriteDictInputWidget[i])
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(DrawSprite[i].sSpriteDictLabel)
			REQUEST_STREAMED_TEXTURE_DICT(DrawSprite[i].sSpriteDictLabel)
		ELSE
			//	SpriteTextureID[i] = GET_TEXTURE_FROM_STREAMED_TEXTURE_DICT(DrawSprite[i].sSpriteDictLabel,DrawSprite[i].sSpriteLabel)
			bTextEntered[i] = FALSE
			bSpriteLoaded[i] = TRUE
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Update Current Hud Pieces.  Each "Piece" is either a Disply_Text or a Draw_Rect
PROC DRAW_CURRENT_PIECES()
	INT i
	
	REPEAT MAX_NUM_PIECES i   
		//Draw the specific Piece
		IF DrawSprite[i].A > 0
			LOAD_SPRITE(i)
			IF bSpriteLoaded[i]
				DRAW_SPRITE(DrawSprite[i].sSpriteDictLabel,DrawSprite[i].sSpriteLabel,DrawSprite[i].CentreX, DrawSprite[i].CentreY, 
						DrawSprite[i].Width, DrawSprite[i].Height, DrawSprite[i].Rotation,
						DrawSprite[i].R, DrawSprite[i].G, DrawSprite[i].B, DrawSprite[i].A)	
			ENDIF
		ENDIF
	ENDREPEAT

	REPEAT MAX_NUM_PIECES i   
		//Draw the specific Piece
		IF DrawRect[i].A > 0
			DRAW_RECT(DrawRect[i].CentreX, DrawRect[i].CentreY, 
							DrawRect[i].Width, DrawRect[i].Height, 
							DrawRect[i].R, DrawRect[i].G, DrawRect[i].B, DrawRect[i].A)	
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_PIECES i   
		//Display the text
		IF DisplayTxt[i].A > 0	
			IF DisplayTxt[i].bUseStringFromScript = FALSE
				DisplayTxt[i].sTxtLabel = GET_CONTENTS_OF_TEXT_WIDGET(TxtInputWidget[i])
			ENDIF
			SET_TEXT_SCALE(DisplayTxt[i].TextXScale, DisplayTxt[i].TextYScale)
			SET_TEXT_COLOUR(DisplayTxt[i].R, DisplayTxt[i].G, DisplayTxt[i].B, DisplayTxt[i].A)
			IF DisplayTxt[i].JustifyCentre = TRUE
				SET_TEXT_CENTRE(TRUE)
			ENDIF
			IF DisplayTxt[i].DropShadowAlpha > 0 
				SET_TEXT_DROPSHADOW(DisplayTxt[i].DropshadowAmount, 0, 0, 0, DisplayTxt[i].DropShadowAlpha)
			ENDIF
			IF DisplayTxt[i].TextEdgeAlpha > 0 
				SET_TEXT_EDGE(DisplayTxt[i].TextEdgeAmount, 0, 0, 0, DisplayTxt[i].TextEdgeAlpha)
			ENDIF
			DISPLAY_TEXT_WITH_LITERAL_STRING(DisplayTxt[i].DisplayAtX, DisplayTxt[i].DisplayAtY, "STRING", DisplayTxt[i].sTxtLabel)
		ENDIF
	ENDREPEAT
ENDPROC

////PURPOSE: Adding a Rect Widget
PROC ADD_RECT_WIDGET(WIDGET_GROUP_ID &RctWidgetID)
	TEXT_LABEL_63 tl63                              
	tl63 = "Rect Piece: "
	tl63 += RectPieceNum                                
	STRING PieceName = GET_STRING_FROM_STRING(tl63, 0 , GET_LENGTH_OF_LITERAL_STRING(tl63))

	SET_CURRENT_WIDGET_GROUP(RootWidget)																			  
																													  
		RctWidgetID = START_WIDGET_GROUP(PieceName)																	  
		ADD_WIDGET_FLOAT_SLIDER("Centre X", DrawRect[RectPieceNum].CentreX, 0.0, 1.0, 0.001)					  
		ADD_WIDGET_FLOAT_SLIDER("Centre Y", DrawRect[RectPieceNum].CentreY, 0.0, 1.0, 0.001)					  
		ADD_WIDGET_FLOAT_SLIDER("Width", DrawRect[RectPieceNum].Width, 0.0, 1.0, 0.01)							  
		ADD_WIDGET_FLOAT_SLIDER("Height", DrawRect[RectPieceNum].Height, 0.0, 1.0, 0.01)						  
		ADD_WIDGET_INT_SLIDER("Red", DrawRect[RectPieceNum].R, 0, 255, 1)										  
		ADD_WIDGET_INT_SLIDER("Green", DrawRect[RectPieceNum].G, 0, 255, 1)										  
		ADD_WIDGET_INT_SLIDER("Blue", DrawRect[RectPieceNum].B, 0, 255, 1)										  
		ADD_WIDGET_INT_SLIDER("Alpha", DrawRect[RectPieceNum].A, 0, 255, 1)			
		ADD_WIDGET_BOOL("Output Info", FinishAddingRectPiece[RectPieceNum])
		ADD_WIDGET_BOOL("Output HUD Tool Info", FinishAddingRectPieceHUD[RectPieceNum])
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

	IF DOES_WIDGET_GROUP_EXIST(RctWidgetID) 
	ENDIF
ENDPROC
////PURPOSE: Adding a Rect Widget
PROC ADD_SPRITE_WIDGET(WIDGET_GROUP_ID &SptWidgetID)				
	TEXT_LABEL_63 tl63                              
	tl63 = "Sprite Piece: "
	tl63 += SpritePieceNum                                
	STRING PieceName = GET_STRING_FROM_STRING(tl63, 0 , GET_LENGTH_OF_LITERAL_STRING(tl63))

	DrawSprite[SpritePieceNum].sSpriteDictLabel = "Temp Label"
	DrawSprite[SpritePieceNum].sSpriteLabel = "Temp Label"
	
	SET_CURRENT_WIDGET_GROUP(RootWidget)																			  
																													  
		SptWidgetID = START_WIDGET_GROUP(PieceName)		
			SpriteDictInputWidget[SpritePieceNum] = ADD_TEXT_WIDGET("TextureDictName")
			SET_CONTENTS_OF_TEXT_WIDGET(SpriteDictInputWidget[SpritePieceNum], DrawSprite[SpritePieceNum].sSpriteDictLabel)
			SpriteTextureInputWidget[SpritePieceNum] = ADD_TEXT_WIDGET("TextureName")
			SET_CONTENTS_OF_TEXT_WIDGET(SpriteTextureInputWidget[SpritePieceNum], DrawSprite[SpritePieceNum].sSpriteLabel)
			ADD_WIDGET_BOOL("Text Labels Entered", bTextEntered[SpritePieceNum])	
			ADD_WIDGET_FLOAT_SLIDER("Centre X", DrawSprite[SpritePieceNum].CentreX, 0.0, 1.0, 0.001)					  
			ADD_WIDGET_FLOAT_SLIDER("Centre Y", DrawSprite[SpritePieceNum].CentreY, 0.0, 1.0, 0.001)					  
			ADD_WIDGET_FLOAT_SLIDER("Width", DrawSprite[SpritePieceNum].Width, 0.0, 2.0, 0.01)							  
			ADD_WIDGET_FLOAT_SLIDER("Height", DrawSprite[SpritePieceNum].Height, 0.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Rotation", DrawSprite[SpritePieceNum].Rotation, 0.0, 360.0, 1.0)
			ADD_WIDGET_INT_SLIDER("Red", DrawSprite[SpritePieceNum].R, 0, 255, 1)										  
			ADD_WIDGET_INT_SLIDER("Green", DrawSprite[SpritePieceNum].G, 0, 255, 1)										  
			ADD_WIDGET_INT_SLIDER("Blue", DrawSprite[SpritePieceNum].B, 0, 255, 1)										  
			ADD_WIDGET_INT_SLIDER("Alpha", DrawSprite[SpritePieceNum].A, 0, 255, 1)			
			ADD_WIDGET_BOOL("Output Info", FinishAddingSpritePiece[SpritePieceNum])	
			ADD_WIDGET_BOOL("Output HUD Tool Info", FinishAddingSpritePieceHUD[TextPieceNum])	
			ADD_WIDGET_BOOL("Output Sprite Texture Ditionary Script", FinishAddingSpriteTexData[TextPieceNum])	
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

	IF DOES_WIDGET_GROUP_EXIST(SptWidgetID) 
	ENDIF
ENDPROC

//PURPOSE: Adding a Text Widget
PROC ADD_TXT_WIDGET(WIDGET_GROUP_ID &txtWidgetID)				
	
	TEXT_LABEL_63 tl63                              
	tl63 = "Text Piece: "
	tl63 += TextPieceNum                                
	STRING PieceName = GET_STRING_FROM_STRING(tl63, 0 , GET_LENGTH_OF_LITERAL_STRING(tl63))
	
	IF DisplayTxt[TextPieceNum].bUseStringFromScript = FALSE
		DisplayTxt[TextPieceNum].sTxtLabel = "Temp Label"
	ENDIF
	SET_CURRENT_WIDGET_GROUP(RootWidget)
		
		txtWidgetID = START_WIDGET_GROUP(PieceName)
			TxtInputWidget[TextPieceNum] = ADD_TEXT_WIDGET("Text")
			SET_CONTENTS_OF_TEXT_WIDGET(TxtInputWidget[TextPieceNum], DisplayTxt[TextPieceNum].sTxtLabel)
			ADD_WIDGET_FLOAT_SLIDER("Centre X", DisplayTxt[TextPieceNum].DisplayAtX, 0.0, 1.0, 0.001)			
			ADD_WIDGET_FLOAT_SLIDER("Centre Y", DisplayTxt[TextPieceNum].DisplayAtY, 0.0, 1.0, 0.001)			
			ADD_WIDGET_FLOAT_SLIDER("X Scale", DisplayTxt[TextPieceNum].TextXScale, 0.0, 5.0, 0.01)			
			ADD_WIDGET_FLOAT_SLIDER("Y Scale", DisplayTxt[TextPieceNum].TextYScale, 0.0, 5.0, 0.01)			
			ADD_WIDGET_INT_SLIDER("Red", DisplayTxt[TextPieceNum].R, 0, 255, 1)			
			ADD_WIDGET_INT_SLIDER("Green", DisplayTxt[TextPieceNum].G, 0, 255, 1)			
			ADD_WIDGET_INT_SLIDER("Blue", DisplayTxt[TextPieceNum].B, 0, 255, 1)			
			ADD_WIDGET_INT_SLIDER("Alpha", DisplayTxt[TextPieceNum].A, 0, 255, 1)			
			ADD_WIDGET_BOOL("Justify Centre", DisplayTxt[TextPieceNum].JustifyCentre)
			ADD_WIDGET_INT_SLIDER("Dropshadow Amount", DisplayTxt[TextPieceNum].DropshadowAmount, 0, 10, 1)			
			ADD_WIDGET_INT_SLIDER("DropShadow Alpha", DisplayTxt[TextPieceNum].DropShadowAlpha, 0, 255, 1)			
			ADD_WIDGET_INT_SLIDER("TextEdge Amount", DisplayTxt[TextPieceNum].TextEdgeAmount, 0, 10, 1)			
			ADD_WIDGET_INT_SLIDER("TextEdge Alpha", DisplayTxt[TextPieceNum].TextEdgeAlpha, 0, 255, 1)			
			ADD_WIDGET_BOOL("Use String from Script", DisplayTxt[TextPieceNum].bUseStringFromScript)			
			ADD_WIDGET_BOOL("Output Info", FinishAddingTextPiece[TextPieceNum])
			ADD_WIDGET_BOOL("Output HUD Tool Info", FinishAddingTextPieceHUD[TextPieceNum])	
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

	IF DOES_WIDGET_GROUP_EXIST(txtWidgetID) 
	ENDIF
ENDPROC

//PURPOSE: Output info on a Rect Widget
PROC OUTPUT_RECT(INT i)
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//RectPieceNum: ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DRAW_RECT(")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].CentreX)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].CentreY)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].Width)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].Height)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].R)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].G)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].B)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].A)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()	
	CLOSE_DEBUG_FILE()
ENDPROC

PROC OUTPUT_HUD_RECT(INT i)
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//Hud Creator Output: ")
		SAVE_STRING_TO_DEBUG_FILE("//RectPieceNum: ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].CentreX =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].CentreX)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].CentreY =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].CentreY)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].Width =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].Width)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].Height =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawRect[i].Height)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].R =")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].R)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].G =")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].G)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].B =")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].B)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawRect[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].A =")
		SAVE_INT_TO_DEBUG_FILE(DrawRect[i].A)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
ENDPROC
//PURPOSE: Output info on a Sprite Widget
PROC OUTPUT_SPRITE(INT i)
	
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//SpritePieceNum: ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("TEXTURE_ID ")
		SAVE_STRING_TO_DEBUG_FILE("tx")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteLabel)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("tx")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteLabel)
		SAVE_STRING_TO_DEBUG_FILE(" = GET_TEXTURE(txd")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteDictLabel)
		SAVE_STRING_TO_DEBUG_FILE(",\"")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteLabel)
		SAVE_STRING_TO_DEBUG_FILE("\")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DRAW_SPRITE(")
		SAVE_STRING_TO_DEBUG_FILE("tx")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteLabel)
		//SAVE_INT_TO_DEBUG_FILE(i)
		//SAVE_STRING_TO_DEBUG_FILE("])")
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].CentreX)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].CentreY)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].Width)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].Height)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].Rotation)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].R)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].G)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].B)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].A)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
ENDPROC

PROC OUTPUT_TEXTURE_DICTIONARY_DATA(INT i)
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("TXD_ID ")
		SAVE_STRING_TO_DEBUG_FILE("txd")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteDictLabel)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("txd")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteDictLabel)
		SAVE_STRING_TO_DEBUG_FILE("= LOAD_TEXTURE_DICT(\"")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteDictLabel)
		SAVE_STRING_TO_DEBUG_FILE("\")")
	CLOSE_DEBUG_FILE()
ENDPROC

PROC OUTPUT_HUD_SPIRTE(INT i)
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//Hud Creator Output: ")
		SAVE_STRING_TO_DEBUG_FILE("//SpritePieceNum: ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].sSpriteDictLabel =")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteDictLabel)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].sSpriteLabel =")
		SAVE_STRING_TO_DEBUG_FILE(DrawSprite[i].sSpriteLabel)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].CentreX =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].CentreX)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].CentreY =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].CentreY)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].Width =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].Width)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].Height =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].Height)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].Rotation =")
		SAVE_FLOAT_TO_DEBUG_FILE(DrawSprite[i].Rotation)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].R =")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].R)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].G =")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].G)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].B =")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].B)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DrawSprite[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].A =")
		SAVE_INT_TO_DEBUG_FILE(DrawSprite[i].A)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
ENDPROC

//PURPOSE: Output info on a Text Widget
PROC OUTPUT_TEXT(INT i)
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//TextPieceNum: ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_TEXT_SCALE(")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].TextXScale)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].TextYScale)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_TEXT_COLOUR(")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].R)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].G)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].B)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].A)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_TEXT_CENTRE(")
		SAVE_BOOL_TO_DEBUG_FILE(DisplayTxt[i].JustifyCentre)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_TEXT_DROPSHADOW(")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].DropshadowAmount)
		SAVE_STRING_TO_DEBUG_FILE(", 0, 0, 0, ")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].DropShadowAlpha)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_TEXT_EDGE(")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].TextEdgeAmount)
		SAVE_STRING_TO_DEBUG_FILE(", 0, 0, 0, ")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].TextEdgeAlpha)
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DISPLAY_TEXT_WITH_LITERAL_STRING(")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].DisplayAtX)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].DisplayAtY)
		SAVE_STRING_TO_DEBUG_FILE(", \"STRING\", \"")
		SAVE_STRING_TO_DEBUG_FILE(DisplayTxt[i].sTxtLabel)
		SAVE_STRING_TO_DEBUG_FILE("\")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
ENDPROC

PROC OUTPUT_HUD_TEXT(INT i)

	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//Hud Creator Output: ")
		SAVE_STRING_TO_DEBUG_FILE("//TextPieceNum: ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].sTxtLabel =")
		SAVE_STRING_TO_DEBUG_FILE(DisplayTxt[i].sTxtLabel)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].DisplayAtX =")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].DisplayAtX)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].DisplayAtY =")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].DisplayAtY)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].TextXScale =")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].TextXScale)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].TextYScale =")
		SAVE_FLOAT_TO_DEBUG_FILE(DisplayTxt[i].TextYScale)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].R =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].R)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].G =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].G)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].B =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].B)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].A =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].A)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].JustifyCentre =")
		SAVE_BOOL_TO_DEBUG_FILE(DisplayTxt[i].JustifyCentre)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].DropshadowAmount =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].DropshadowAmount)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].DropShadowAlpha  =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].DropShadowAlpha)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].TextEdgeAmount  =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].TextEdgeAmount)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].TextEdgeAlpha  =")
		SAVE_INT_TO_DEBUG_FILE(DisplayTxt[i].TextEdgeAlpha)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("DisplayTxt[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].bUseStringFromScript  =")
		SAVE_BOOL_TO_DEBUG_FILE(DisplayTxt[i].bUseStringFromScript)
		SAVE_NEWLINE_TO_DEBUG_FILE()		
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()

ENDPROC

//PURPOSE: Do all the editing of the mission select screen 	   
PROC DO_EDITING()
	INT i
	SWITCH DebugState

		//Initialising Widgets
		CASE DEBUG_PIECES_INI
			//creating a list of all current Rect widgets
			REPEAT MAX_NUM_PIECES i
				//initialise data
				FinishAddingRectPiece[i] = FALSE
				
				//check if alpha is greater than zero on the current piece and draw it
				IF DrawRect[i].A > 0			
					ADD_RECT_WIDGET(RectWidget[i])
					RectPieceNum ++	
				ENDIF
			ENDREPEAT
			
			//creating a list of all current Sprite widgets
			REPEAT MAX_NUM_PIECES i
				//initialise data
				FinishAddingSpritePiece[i] = FALSE
				
				//check if alpha is greater than zero on the current piece and draw it
				IF DrawSprite[i].A > 0			
					ADD_SPRITE_WIDGET(SpriteWidget[i])
					SpritePieceNum ++	
				ENDIF
			ENDREPEAT	
			
			//creating a list of all current Text widgets
			REPEAT MAX_NUM_PIECES i
				//initialise data
				FinishAddingTextPiece[i] = FALSE
				
				//check if alpha is greater than zero on the current piece and draw it
				IF DisplayTxt[i].A > 0			
					ADD_TXT_WIDGET(TextWidget[i])
					TextPieceNum ++	
				ENDIF
			ENDREPEAT	
		
			//add checks
			SET_CURRENT_WIDGET_GROUP(RootWidget)
				WG_AddNewPiece = START_WIDGET_GROUP("Add New Piece")
					ADD_WIDGET_BOOL("Add New Rect Piece", AddRectPiece)
					ADD_WIDGET_BOOL("Add New Sprite Piece", AddSpritePiece)
					ADD_WIDGET_BOOL("Add New Text Piece", AddTextPiece)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

			DebugState = DEBUG_PIECES_ADD
			NET_PRINT("HudState = DEBUG_PIECES_ADD")
			NET_NL()
		BREAK

		//Waiting to add a piece
		CASE DEBUG_PIECES_ADD
			
			//Adding a new widget to define a new piece
			IF AddRectPiece = TRUE
				//Remove add checks
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					DELETE_WIDGET_GROUP(WG_AddNewPiece)
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

				//Add the new widget
				ADD_RECT_WIDGET(RectWidget[RectPieceNum])
				RectPieceNum ++	
				
				//put back in add checks
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					WG_AddNewPiece = START_WIDGET_GROUP("Add New Piece")
						ADD_WIDGET_BOOL("Add New Rect Piece", AddRectPiece)
						ADD_WIDGET_BOOL("Add New Sprite Piece", AddSpritePiece)
						ADD_WIDGET_BOOL("Add New Text Piece", AddTextPiece)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

				AddRectPiece = FALSE
			ENDIF
			
			//Adding a new widget to define a new piece
			IF AddSpritePiece = TRUE
				//Remove add checks
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					DELETE_WIDGET_GROUP(WG_AddNewPiece)
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

				//Add the new widget
				ADD_SPRITE_WIDGET(SpriteWidget[SpritePieceNum])
				SpritePieceNum ++	
				
				//put back in add checks
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					WG_AddNewPiece = START_WIDGET_GROUP("Add New Piece")
						ADD_WIDGET_BOOL("Add New Rect Piece", AddRectPiece)
						ADD_WIDGET_BOOL("Add New Sprite Piece", AddSpritePiece)
						ADD_WIDGET_BOOL("Add New Text Piece", AddTextPiece)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

				AddSpritePiece = FALSE
			ENDIF
			
			//Adding a new widget to define a new piece
			IF AddTextPiece = TRUE
				//Remove add checks
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					DELETE_WIDGET_GROUP(WG_AddNewPiece)
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

				//Add the new widget
				ADD_TXT_WIDGET(TextWidget[TextPieceNum])
				TextPieceNum ++	
				
				//put back in add checks
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					WG_AddNewPiece = START_WIDGET_GROUP("Add New Piece")
						ADD_WIDGET_BOOL("Add New Rect Piece", AddRectPiece)
						ADD_WIDGET_BOOL("Add New Sprite Piece", AddSpritePiece)
						ADD_WIDGET_BOOL("Add New Text Piece", AddTextPiece)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)

				AddTextPiece = FALSE
			ENDIF

			//Outputting Rect info
			REPEAT MAX_NUM_PIECES i
				IF FinishAddingRectPiece[i] = TRUE
					OUTPUT_RECT(i)
					FinishAddingRectPiece[i] = FALSE
				ENDIF
				IF FinishAddingRectPieceHUD[i] = TRUE
					OUTPUT_HUD_RECT(i)
					FinishAddingRectPieceHUD[i] = FALSE
				ENDIF
			ENDREPEAT
			
			//Outputting sprite info
			REPEAT MAX_NUM_PIECES i
				IF FinishAddingSpritePiece[i] = TRUE
					OUTPUT_SPRITE(i)
					FinishAddingSpritePiece[i] = FALSE
				ENDIF
				IF FinishAddingSpritePieceHUD[i] = TRUE
					OUTPUT_HUD_SPIRTE(i)
					FinishAddingSpritePieceHUD[i] = FALSE
				ENDIF
				IF FinishAddingSpriteTexData[i] = TRUE
					OUTPUT_TEXTURE_DICTIONARY_DATA(i)
					FinishAddingSpriteTexData[i] = FALSE
				ENDIF
			ENDREPEAT
			
			//Outputting Text info
			REPEAT MAX_NUM_PIECES i
				IF FinishAddingTextPiece[i] = TRUE
					OUTPUT_TEXT(i)
					FinishAddingTextPiece[i] = FALSE
				ENDIF
				IF FinishAddingTextPieceHUD[i] = TRUE
					OUTPUT_HUD_TEXT(i)
					FinishAddingTextPieceHUD[i] = FALSE
				ENDIF
			ENDREPEAT

		BREAK	

	ENDSWITCH
ENDPROC

//PURPOSE: Creat Widgets
PROC CREATE_HUD_WIDGETS(WIDGET_GROUP_ID ParentWidgetGroup = NULL)
	IF NOT (ParentWidgetGroup = NULL)
		SET_CURRENT_WIDGET_GROUP(ParentWidgetGroup)
	ENDIF
	RootWidget = START_WIDGET_GROUP("Hud Creator Tool")  
		// Which case are we in
		START_WIDGET_GROUP("Hud State") 
				ADD_WIDGET_INT_SLIDER("Hud state", HudState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	IF NOT (ParentWidgetGroup = NULL)
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidgetGroup)
	ENDIF
ENDPROC		

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
PROC Maintain_Hud_Creator_Tool(WIDGET_GROUP_ID ParentWidgetGroup = NULL)
#ENDIF
#IF IS_FINAL_BUILD
PROC Maintain_Hud_Creator_Tool()
#ENDIF
	SWITCH HudState
		
		//Initialisation
		CASE HUD_STATE_INI
			#IF IS_DEBUG_BUILD
			CREATE_HUD_WIDGETS(ParentWidgetGroup)
			#ENDIF

			HudState = HUD_STATE_WAIT
			NET_PRINT("HudState = HUD_STATE_WAIT")
			NET_NL()	
		BREAK 
		
		//Waiting for player to trigger mission select screen 
		CASE HUD_STATE_WAIT
			
			IF bHasEditWidgetBeenCreated = FALSE
				//Remove Start editing button
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					WG_Editing = START_WIDGET_GROUP("Start/Stop Editing Hud")
						ADD_WIDGET_BOOL("Start Editing Hud", bEditingBool)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)
				bHasEditWidgetBeenCreated = TRUE
			ENDIF
			
			//Bringing Up the keyboard
			IF bEditingBool = TRUE
				
				//resetting the widget
				bHasEditWidgetBeenCreated = FALSE
				bEditingBool = FALSE

				HudState = HUD_STATE_RUNNING
				NET_PRINT("HudState = HUD_STATE_RUNNING")
				NET_NL()	
			ENDIF	
		BREAK 

		//Dealing with Mission select screen 
		CASE HUD_STATE_RUNNING
			
			//This updates the current hud pieces (i.e. each piece of text or rect is a "piece")
			DRAW_CURRENT_PIECES()

			/*
			#IF IS_DEBUG_BUILD
			//Start Debug
			IF DebugMode = FALSE
				IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_D) 
					
					PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "HUD MISSION SELECT DEBUG ON.", 7500, 1)
					DebugMode = TRUE	
				ENDIF
			ELSE
				
				//Allow editing of the screen 
				DO_EDITING()
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_D) 
					PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "HUD MISSION SELECT DEBUG OFF.", 7500, 1)
					DebugMode = FALSE	
				ENDIF
			ENDIF
			#ENDIF
			*/
			
			DO_EDITING()

			//Swopping the widget over to say stop creating widget
			IF bHasEditWidgetBeenCreated = FALSE
				//Remove Start editing button
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					DELETE_WIDGET_GROUP(WG_Editing)
					WG_Editing = START_WIDGET_GROUP("Start/Stop Editing Hud")
						ADD_WIDGET_BOOL("Stop Editing Hud", bEditingBool)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)
				bHasEditWidgetBeenCreated = TRUE
			ENDIF

			//Removing the hud mission select screen
			IF bEditingBool = TRUE
				SET_CURRENT_WIDGET_GROUP(RootWidget)
					DELETE_WIDGET_GROUP(WG_Editing)
				CLEAR_CURRENT_WIDGET_GROUP(RootWidget)
				
				//resetting the widget
				bHasEditWidgetBeenCreated = FALSE
				bEditingBool = FALSE
				
				HudState = HUD_STATE_WAIT
				NET_PRINT("HudState = HUD_STATE_WAIT")
				NET_NL()	
			ENDIF	
		BREAK 

	ENDSWITCH	
ENDPROC


//------------------------------------------------
