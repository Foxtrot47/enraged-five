USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_xml.sch"
USING "script_player.sch"
USING "commands_brains.sch"
USING "net_script_timers.sch"
USING "charsheet_public.sch"
USING "exclusive_vehicle_mod_helper.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	shared_debug.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	Contains any debug keypresses and widgets common to both
//								singleplayer and multiplayer.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// ...Shared Debug Bit Flag Definitions
CONST_INT		CLEAR_SHARED_DEBUG_FLAGS			0
CONST_INT		DBG_LOCATES_ON						1		// TRUE if the debug outlines are switched ON, FALSE for OFF
CONST_INT		DBG_OVERLAYS_ON						2		// TRUE if the screen HUD overlays are ON, FALSE if OFF
CONST_INT		DBG_F9_SCREEN_ON					3		// TRUE if the mission flow F7 screen is ON, FALSE if OFF
CONST_INT		DBG_F8_SCREEN_ON					4		// TRUE if the mission flow F9 screen is ON, FALSE if OFF
CONST_INT		DBG_F10_SCREEN_ON					5		// TRUE if the family F8 screen is ON, FALSE if OFF
CONST_INT		DBG_F11_SCREEN_ON					6		// TRUE if the friends F10 screen is ON, FALSE if OFF
CONST_INT		DBG_RADAR_AREA_COLOURS_ON			7		// TRUE if the radar is displaying area colours, FALSE if not
CONST_INT		DBG_SPACE_F9						8		// TRUE if space turned on the F9

// I've broken the rules with this INT because it contains bit flags for many different debug functions
INT				m_sharedDebugFlags					= CLEAR_SHARED_DEBUG_FLAGS


// NOTE: These variables should get initialised in Initialise_Shared_Debug_Variables() below
STRUCT structSharedDebugVars
	// ...vehicle recording variables
	BOOL			vehicleRecordingRequested
	BOOL			vehicleRecordingBeingRecorded
	BOOL			moveVehicleBackToStartPosition
	BOOL			overwriteRecording
	BOOL			keepOverwriteRecordingTicked
	BOOL			allowSquareButtonToStartStopVehicleRecording
	BOOL 			bAllowSimultaneousTrailerRecording
	TEXT_WIDGET_ID	vehicleRecordingFilenameTextWidgetID
	INT				vehicleRecordingID
	INT 			iTrailerRecordingID
	VEHICLE_INDEX	indexVehicleRecordingVehicle
	VEHICLE_INDEX 	vehTrailerToRecord
	VECTOR 			xyzVehicleRecordingStartPos
	VECTOR 			xyzVehicleRecordingStartRot
	FLOAT			quatVehicleRecordingStartX
	FLOAT			quatVehicleRecordingStartY
	FLOAT			quatVehicleRecordingStartZ
	FLOAT			quatVehicleRecordingStartW
	
	// ...vehicle colour variables
	INT 			vehicleColour1
	INT 			vehicleColour2
	INT 			vehicleColour3
	INT 			vehicleColour4
	INT 			vehicleColour5
	INT 			vehicleColour6
	FLOAT 			vehicleEnvEff
	BOOL 			vehicleColourActivate
	BOOL 			vehicleColourClean
	BOOL 			vehicleColourOutput
	BOOL 			vehicleColourInitialise
	TEXT_LABEL_63 	vehicleColourPathStr
	TEXT_LABEL_23 	vehicleColourModelStr
	TEXT_WIDGET_ID	vehicleColourPath
	TEXT_WIDGET_ID	vehicleColourModel
	
	// ...vehicle mods
	INT vehicleLivery, vehicleLiveryTemp
	INT vehicleLivery2, vehicleLivery2Temp
	INT vehicleModTyreSmokeR, vehicleModTyreSmokeRTemp
	INT vehicleModTyreSmokeG, vehicleModTyreSmokeGTemp
	INT vehicleModTyreSmokeB, vehicleModTyreSmokeBTemp
	INT vehicleTyreType, vehicleTyreTypeTemp
	INT vehicleModWindowTintColour, vehicleModWindowTintColourTemp
	INT vehicleModSlotIndex[MAX_VEHICLE_MOD_SLOTS]
	BOOL bMaxSpeedOverride
	BOOL bTopSpeedOverride
	FLOAT vehicleMaxSpeedOverride
	FLOAT vehicleTopSpeedOverride
	INT vehicleModWheelType
	BOOL vehicleModEnableBasic
	BOOL vehicleModEnableAdvanced
	BOOL vehicleModDataSetBasic
	BOOL vehicleModDataSetAdvanced
	BOOL vehicleModExtraOn[16]
	BOOL vehicleModExtraOnTemp[16]
	BOOL vehicleModDisplayCurrentMods
	BOOL selectedVehicleModDisplayCurrentMods
	BOOL bTireDesign 
	
	// ...hask key generation variables
	BOOL			generateHashKey
	INT				hashKey
	TEXT_WIDGET_ID	hashKeyGeneratorTextWidgetID
	
	// ...scripted cam editor variables
	BOOL 			launchCamEditor
	
	// ...generic debug tick
	BOOL			bShutdownLoadingScreens
	BOOL			forcefadein
	BOOL			forcefadeout
	BOOL			killplayer
	BOOL			forceplayercontrolon
	BOOL			forceplayercontroloff
	BOOL			forceplayerfrozen
	BOOL			forceplayernotfrozen
	BOOL			forceplayercollision
	BOOL			forceplayernocollision
	BOOL			reactivateWorldBrains
	BOOL			clearareaaroundplayer
	
	// ...save game test widgets
	BOOL			doAutosave
	BOOL			doNormalSave
	
	BOOL			returnToGameplayCam
	
	// ...ptfx tester widgets
	TEXT_WIDGET_ID ptfxDictionary
	TEXT_WIDGET_ID ptfxName
	PTFX_ID ptfxID
	VECTOR ptfxPos
	VECTOR ptfxRot
	FLOAT ptfxScale
	BOOl playPtfxLooped
	BOOL playPtfxNonLooped
	BOOL ptfxIsRelativeToEntity
	BOOL ptfxTesterEnabled
	
	// ...object positioning widgets
	ENTITY_INDEX objFocusedEntity
	VECTOR objPos
	VECTOR objRot
	FLOAT objQuatX
	FLOAT objQuatY
	FLOAT objQuatZ
	FLOAT objQuatW
	BOOL objPositioningEnabled
	BOOL objPositioningOutput
	BOOL objPositioningFreeze
	BOOL objPositioningAxisSelected
	BOOL objPositioningUseLocalAxis
	INT iObjPositioningCurrentAxis
	
	// ...min/max locate widgets
	BOOL bMinMaxWidgetEnabled
	BOOL bMinMaxWidgetOutput
	BOOL bMinMaxGetMin
	BOOL bMinMaxGetMax
	VECTOR vMinMaxCornerMin
	VECTOR vMinMaxCornerMax
	INT iMinMaxCurrentSelection
	
	// ...door opening widget
	ENTITY_INDEX objFocusedDoor
	MODEL_NAMES	mnDoorModel
	VECTOR	vDoorPosition
	FLOAT	fOpenRatio
	BOOL	bDoorOpeningEnabled
	BOOL	bDoorOpeningOutputLog
	BOOL	bDoorOpeningOutputFile
	BOOL	bDoorsLocked
	
	// ...car tyre bursts and damage
	VEHICLE_INDEX	vehFocusedCar
	MODEL_NAMES		mnCarModel
	VECTOR 			vFrontLeftWheelOffset		
	VECTOR 			vFrontRightWheelOffset		
	VECTOR 			vRearLeftWheelOffset		
	VECTOR 			vRearRightWheelOffset		
	VECTOR 			vFrontLeftDamageOffset		
	VECTOR 			vFrontRightDamageOffset		
	VECTOR 			vRearLeftDamageOffset		
	VECTOR 			vRearRightDamageOffset
	FLOAT			fSphereRadius
	BOOL			bCarTyreBurstsAndDamageEnabled
	BOOL			bCarTyreBurstsAndDamageOutput
	BOOL			bRetoreCarHealth
	BOOL			bDrawDebugSpheres
	FLOAT			fDamage
	FLOAT			fDeformation
	BOOL			bApplyFrontLeftDamage
	BOOL			bApplyFrontRightDamage
	BOOL			bApplyRearLeftDamage
	BOOL			bApplyRearRightDamage
	
	// ...vehicle extras
	BOOL bVehicleExtraEnabled
	VEHICLE_INDEX vehVehicleExtraFocus
	INT iVehicleExtra, iVehicleExtraLast
	BOOL bVehicleExtra
	BOOL bVehicleExtraDebug
	BOOL bVehicleExtraOutput
	
	// ...Vehicle force widgets
	ENTITY_INDEX entityToApplyTo
	BOOL bEnableForceWidget
	INT iForceApplyType
	VECTOR vForceDirection
	VECTOR vForceOffset
	INT iForceComponent
	BOOL bIsLocalForce
	BOOL bIsLocalOffset
	BOOL bScaleForceByMass
	
	// ...radar area colours
	BOOL	toggleRadarAreaColours

	// ...Cam offset widget
	BOOL bCamOffsetEnabled
	BOOL bCamOffsetActivate
	BOOL bCamOffsetRelativePosition
	BOOL bCamOffsetRelativeLookAt
	
	// ...native trace
	TEXT_WIDGET_ID	nativeTraceTextWidgetID
	BOOL bNativeTraceInput
	BOOL bNativeTraceBreakInput
	BOOL bNativeTraceBreakFirstInput
	BOOL bNativeTraceRunning	
	BOOL bNativeTraceBreakRunning	
	BOOL bNativeTraceBreakFirstRunning
	
	// ...game time date
	BOOL bDrawGameTimeDate
	FLOAT fCurrentTimeDateX
	FLOAT fCurrentTimeDateY
	FLOAT fDebugTimeDateX
	FLOAT fDebugTimeDateY
	STRUCT_STAT_DATE fDebugDate1, fDebugDate2, fDebugDateRes
	BOOL bAddTODs
	BOOL bSetTOD
	
	// ...set gameplay cam relative heading and pitch
	BOOL bSetGameplayCamRelativePitchAndHeading
	FLOAT fCamRelativePitch
	FLOAT fCamRelativeHeading
	
	// Neon colours
	INT XenonColorLightIndex = 255
	INT XenonColorLightIndexTemp	
ENDSTRUCT



// *****************************************************************************************
//	INITIALISATION FUNCTIONS
// *****************************************************************************************

/// PURPOSE:		Initialises any variables required for debug
/// INPUT PARAMS:	structSharedDebugVars		The struct of variables to be initialised
PROC Initialise_Shared_Debug_Variables(structSharedDebugVars &paramVars)
	
	// ...vehicle recording variables
	paramVars.vehicleRecordingRequested						= FALSE
	paramVars.vehicleRecordingBeingRecorded					= FALSE
	paramVars.moveVehicleBackToStartPosition				= FALSE
	paramVars.overwriteRecording							= FALSE
	paramVars.keepOverwriteRecordingTicked					= FALSE
	paramVars.allowSquareButtonToStartStopVehicleRecording	= FALSE
	paramVars.bAllowSimultaneousTrailerRecording			= FALSE
	paramVars.vehicleRecordingFilenameTextWidgetID			= NULL
	paramVars.vehicleRecordingID							= 0
	paramVars.iTrailerRecordingID							= 0
	paramVars.indexVehicleRecordingVehicle					= NULL
	paramVars.vehTrailerToRecord							= NULL
	paramVars.xyzVehicleRecordingStartPos					= << 0.0, 0.0, 0.0 >>
	paramVars.xyzVehicleRecordingStartRot					= << 0.0, 0.0, 0.0 >>
	paramVars.quatVehicleRecordingStartX					= 0.0
	paramVars.quatVehicleRecordingStartY					= 0.0
	paramVars.quatVehicleRecordingStartZ					= 0.0
	paramVars.quatVehicleRecordingStartW					= 0.0
	
	// ...vehicle colour variables
	paramVars.vehicleColour1 								= 0
	paramVars.vehicleColour2 								= 0
	paramVars.vehicleColour3 								= 0
	paramVars.vehicleColour4 								= 0
	paramVars.vehicleColourActivate 						= FALSE
	paramVars.vehicleColourInitialise 						= FALSE
	paramVars.vehicleColourOutput 							= FALSE
	paramVars.vehicleColourClean 							= FALSE
	paramVars.vehicleColourPath 							= NULL
	paramVars.vehicleColourModel 							= NULL
	paramVars.vehicleColourPathStr 							= ""
	paramVars.vehicleColourModelStr 						= ""
	
	// ...hask key generation variables
	paramVars.generateHashKey								= FALSE
	paramVars.hashKey										= 0
	paramVars.hashKeyGeneratorTextWidgetID					= NULL

	// ...generic debug tick
	paramVars.bShutdownLoadingScreens						= FALSE
	paramVars.forcefadein									= FALSE
	paramVars.forcefadeout									= FALSE
	paramVars.killplayer									= FALSE
	paramVars.forceplayercontrolon							= FALSE
	paramVars.forceplayercontroloff							= FALSE
	paramVars.forceplayerfrozen								= FALSE
	paramVars.forceplayernotfrozen							= FALSE
	paramVars.forceplayercollision							= FALSE
	paramVars.forceplayernocollision						= FALSE
	paramVars.reactivateWorldBrains							= FALSE
	paramVars.clearareaaroundplayer							= FALSE
	
	// ...scripted cam editor widgets
	paramVars.launchCamEditor								= FALSE
	
	// ...save game test widgets
	paramVars.doAutosave									= FALSE
	paramVars.doNormalSave									= FALSE
	
	paramVars.returnToGameplayCam							= FALSE
	
	// ...ptfx test widgets
	paramVars.ptfxDictionary								= NULL
	paramVars.ptfxName										= NULL
	paramVars.ptfxID										= NULL
	paramVars.ptfxPos 										= <<0.0, 0.0, 0.0>>
	paramVars.ptfxRot 										= <<0.0, 0.0, 0.0>>
	paramVars.ptfxScale 									= 1.0
	paramVars.playPtfxLooped 								= FALSE
	paramVars.playPtfxNonLooped 							= FALSE
	paramVars.ptfxIsRelativeToEntity 						= FALSE
	
	// ...object positioning widgets
	paramVars.objFocusedEntity								= NULL
	paramVars.objPos										= <<0.0, 0.0, 0.0>>
	paramVars.objRot										= <<0.0, 0.0, 0.0>>
	paramVars.objQuatX										= 0.0
	paramVars.objQuatY										= 0.0
	paramVars.objQuatZ										= 0.0
	paramVars.objQuatW										= 0.0
	paramVars.objPositioningEnabled							= FALSE
	paramVars.objPositioningOutput							= FALSE
	paramVars.objPositioningFreeze							= TRUE
	paramVars.objPositioningAxisSelected					= FALSE
	paramVars.objPositioningUseLocalAxis					= TRUE
	paramVars.iObjPositioningCurrentAxis					= 0
	
	// ...min/max locate widget
	paramVars.bMinMaxWidgetEnabled							= FALSE
	paramVars.bMinMaxWidgetOutput							= FALSE
	paramVars.bMinMaxGetMin									= FALSE
	paramVars.bMinMaxGetMax									= FALSE
	paramVars.iMinMaxCurrentSelection						= -1
	paramVars.vMinMaxCornerMin								= <<0.0, 0.0, 0.0>>
	paramVars.vMinMaxCornerMax 								= <<0.0, 0.0, 0.0>>
	
	// ...door opening widget
	paramVars.objFocusedDoor								= NULL
	paramVars.mnDoorModel									= DUMMY_MODEL_FOR_SCRIPT
	paramVars.vDoorPosition									= <<0.0, 0.0, 0.0>>
	paramVars.fOpenRatio									= 0.0
	paramVars.bDoorOpeningEnabled							= FALSE
	paramVars.bDoorOpeningOutputLog							= FALSE
	paramVars.bDoorOpeningOutputFile						= FALSE
	paramVars.bDoorsLocked									= FALSE
	
	// ...radar area colours
	paramVars.toggleRadarAreaColours						= FALSE
	
	// ...cam offset widget
	paramVars.bCamOffsetEnabled								= FALSE
	paramVars.bCamOffsetActivate							= FALSE
	paramVars.bCamOffsetRelativePosition					= TRUE
	paramVars.bCamOffsetRelativeLookAt						= TRUE
	
	// ...vehicle force widget
	paramVars.entityToApplyTo								= NULL
	paramVars.bEnableForceWidget							= FALSE
	paramVars.iForceApplyType								= 0
	paramVars.vForceDirection								= <<0.0, 0.0, 0.0>>
	paramVars.vForceOffset									= <<0.0, 0.0, 0.0>>
	paramVars.iForceComponent								= 0
	paramVars.bIsLocalForce									= FALSE
	paramVars.bIsLocalOffset								= FALSE
	paramVars.bScaleForceByMass								= FALSE
	
	
	// ...native trace
	paramVars.nativeTraceTextWidgetID						= NULL
	
	// ...gameplay cam relative heading and pitch
	paramVars.bSetGameplayCamRelativePitchAndHeading		= FALSE
	paramVars.fCamRelativePitch								= 0.0
	paramVars.fCamRelativeHeading							= 0.0
	
	// Any initially 'on' flags
	SET_BIT (m_sharedDebugFlags, DBG_OVERLAYS_ON)

ENDPROC



// *****************************************************************************************
//	GENERAL FUNCTIONS
// *****************************************************************************************

// -----------------------------------------------------------------------------------------------------------



// *****************************************************************************************
//	Mission Flow Debug Screen - toggle on/off
// *****************************************************************************************

/// PURPOSE:
///    KEY F9: Toggles Mission Flow debug screen on/off
PROC Maintain_KeyF9_Toggle_Mission_Flow_Debug_Screen(SCRIPT_TIMER &stTimer)
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_NONE, "Normal bug") AND GET_COMMANDLINE_PARAM_EXISTS("displayF9OnBugCreation")
	AND NOT IS_DEBUG_KEY_JUST_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_CTRL, "Normal bug")
	AND NOT IS_DEBUG_KEY_JUST_RELEASED(KEY_SPACE, KEYBOARD_MODIFIER_CTRL, "Normal bug")
	AND NOT IS_DEBUG_KEY_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_CTRL, "Normal bug")
		IF !IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
		AND NETWORK_IS_GAME_IN_PROGRESS()
			RESET_NET_TIMER(stTimer)
			START_NET_TIMER(stTimer)
			SET_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
			//SET_BIT(m_sharedDebugFlags, DBG_SPACE_F9)
			PRINTLN("...F9 SCREEN ON - SPACE")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
		IF HAS_NET_TIMER_STARTED(stTimer)
			IF HAS_NET_TIMER_EXPIRED(stTimer, 500)
				CLEAR_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
			//	CLEAR_BIT(m_sharedDebugFlags, DBG_SPACE_F9)
				PRINTLN("...F9 SCREEN OFF - SPACE")
				RESET_NET_TIMER(stTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F9) OR IS_DEBUG_KEY_JUST_PRESSED(KEY_F9, KEYBOARD_MODIFIER_CTRL_SHIFT, "Mission Flow Screenshot") )
		WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_F9) OR IS_DEBUG_KEY_PRESSED(KEY_F9, KEYBOARD_MODIFIER_CTRL_SHIFT, "Mission Flow Screenshot") )
			WAIT (0)
		ENDWHILE
		
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON))
			CLEAR_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
			RESET_NET_TIMER(stTimer)
			WAIT(0)
			
			PRINTSTRING("...F9 SCREEN OFF")
			PRINTNL()
		ELSE
			SET_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
			RESET_NET_TIMER(stTimer)
			PRINTSTRING("...F9 SCREEN ON")
			PRINTNL()
		ENDIF
	ENDIF


ENDPROC


// *****************************************************************************************
//	Force Fade In
// *****************************************************************************************

/// PURPOSE:
///    KEY LSHIFT+*: Forces the screen to fade in
PROC Maintain_KeyLShiftMultiply_Force_Fade_In()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_MULTIPLY, KEYBOARD_MODIFIER_SHIFT, "Force Fade In")
		IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_IN(1000)
			PRINTSTRING("...FORCING SCREEN FADE IN")
			PRINTNL()
		ENDIF
	ENDIF
ENDPROC


// *******************************************************************************************
//  WIDGET FUNCTIONS AND PROCEDURES
// *******************************************************************************************
/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC ADD_WIDGET_VECTOR_SLIDER(STRING name, VECTOR &pos_to_change, FLOAT min, FLOAT max, FLOAT slider_step)
	TEXT_LABEL_63 slider_name = name
	slider_name += " x"
	IF (slider_step != 0.0)
		ADD_WIDGET_FLOAT_SLIDER(slider_name, pos_to_change.x, min, max, slider_step)
	ELSE
		ADD_WIDGET_FLOAT_READ_ONLY(slider_name, pos_to_change.x)
	ENDIF
	slider_name = name
	slider_name += " y"
	IF (slider_step != 0.0)
		ADD_WIDGET_FLOAT_SLIDER(slider_name, pos_to_change.y, min, max, slider_step)
	ELSE
		ADD_WIDGET_FLOAT_READ_ONLY(slider_name, pos_to_change.y)
	ENDIF
	slider_name = name
	slider_name += " z"
	IF (slider_step != 0.0)
		ADD_WIDGET_FLOAT_SLIDER(slider_name, pos_to_change.z, min, max, slider_step)
	ELSE
		ADD_WIDGET_FLOAT_READ_ONLY(slider_name, pos_to_change.z)
	ENDIF
ENDPROC

/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:    neil f
/// COMMENTS:   
PROC ADD_WIDGET_VECTOR_READ_ONLY(STRING name, VECTOR &pos_to_change)
	ADD_WIDGET_VECTOR_SLIDER(name, pos_to_change, 0.0,0.0, 0.0)
ENDPROC

/// PURPOSE:    saves the vector to the given file at the given path.
/// PARAMS:     
///    vVector_to_save - 
///    FilePath - 
///    FileName - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC SAVE_VECTOR_TO_NAMED_DEBUG_FILE(VECTOR vVector_to_save, string FilePath, string FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("<<", FilePath, FileName)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.x, FilePath, FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", FilePath, FileName)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.y, FilePath, FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", FilePath, FileName)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.z, FilePath, FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE(">>", FilePath, FileName)
ENDPROC

/// PURPOSE:    saves the vector to temp_debug.txt
/// PARAMS:     
///    vVector_to_save - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC SAVE_VECTOR_TO_DEBUG_FILE(VECTOR vVector_to_save)
    SAVE_STRING_TO_DEBUG_FILE("<<")
    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.x)
    SAVE_STRING_TO_DEBUG_FILE(", ")
    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.y)
    SAVE_STRING_TO_DEBUG_FILE(", ")
    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.z)
    SAVE_STRING_TO_DEBUG_FILE(">>")
ENDPROC

/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC ADD_WIDGET_DATE_READ_ONLY(STRING name, STRUCT_STAT_DATE &date_to_change)
    TEXT_LABEL_63 slider_name = name
    slider_name += " year"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.year)
    slider_name = name
    slider_name += " month"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.month)
    slider_name = name
    slider_name += " day"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.day)
	slider_name = name
    slider_name += " hour"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.hour)
	slider_name = name
    slider_name += " minute"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.minute)
	slider_name = name
    slider_name += " seconds"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.seconds)
	slider_name = name
    slider_name += " milliseconds"
    ADD_WIDGET_INT_READ_ONLY(slider_name, date_to_change.Milliseconds)
    
ENDPROC


/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC ADD_WIDGET_DATE_SLIDER(STRING name, STRUCT_STAT_DATE &date_to_change, INT Min, INT Max, INT Slider )
    TEXT_LABEL_63 slider_name = name
    slider_name += " year"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.year, Min, Max, Slider)
    slider_name = name
    slider_name += " month"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.month, Min, Max, Slider)
    slider_name = name
    slider_name += " day"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.day, Min, Max, Slider)
	slider_name = name
    slider_name += " hour"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.hour, Min, Max, Slider)
	slider_name = name
    slider_name += " minute"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.minute, Min, Max, Slider)
	slider_name = name
    slider_name += " seconds"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.seconds, Min, Max, Slider)
	slider_name = name
    slider_name += " milliseconds"
    ADD_WIDGET_INT_SLIDER(slider_name, date_to_change.Milliseconds, Min, Max, Slider)
    
ENDPROC


/// PURPOSE:    procedure that prints a bool TRUE/FALSE to the console (NOT the game screen)
/// PARAMS:     
///    B - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC PRINTBOOL(BOOL B)
	IF B
		PRINTSTRING("TRUE")
	ELSE
		PRINTSTRING("FALSE")
	ENDIF
ENDPROC

//PURPOSE: Save a bool to the given file at the given path.
PROC SAVE_BOOL_TO_NAMED_DEBUG_FILE(BOOL bBool, string FilePath, string FileName)
	IF bBool = TRUE
		SAVE_STRING_TO_NAMED_DEBUG_FILE("TRUE", FilePath, FileName)
	ELSE
		SAVE_STRING_TO_NAMED_DEBUG_FILE("FALSE", FilePath, FileName)
	ENDIF
ENDPROC

//PURPOSE: Save a bool to the debug file
PROC SAVE_BOOL_TO_DEBUG_FILE(BOOL bBool)
	IF bBool = TRUE
		SAVE_STRING_TO_DEBUG_FILE("TRUE")
	ELSE
		SAVE_STRING_TO_DEBUG_FILE("FALSE")
	ENDIF
ENDPROC

// *****************************************************************************************
//	Player Coordinates - Write to Debug File
// *****************************************************************************************

/// PURPOSE:		Writes coordinates and heading to the debug file
/// INPUT PARAMS:	paramCoords		The coordinates to be output
///					paramHeading	The heading to be output
/// NOTES:			Assumes the debug file has already been opened and this routine leaves it open
PROC Write_Coordinates_And_Heading_To_File(VECTOR paramCoords, FLOAT paramHeading)
	
	SAVE_VECTOR_TO_DEBUG_FILE(paramCoords)
	SAVE_STRING_TO_DEBUG_FILE(", ")
	SAVE_FLOAT_TO_DEBUG_FILE(paramHeading)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:		Writes coordinates and heading to the debug file in the debug menu format
/// INPUT PARAMS:	paramCoords		The coordinates to be output
///					paramHeading	The heading to be output
/// NOTES:			Assumes the debug file has already been opened and this routine leaves it open
PROC Write_Debug_Menu_Coordinates_And_Heading_To_File(VECTOR paramCoords, FLOAT paramHeading)
	
	SAVE_STRING_TO_DEBUG_FILE("x=\"")
	SAVE_FLOAT_TO_DEBUG_FILE(paramCoords.x)
	SAVE_STRING_TO_DEBUG_FILE("\" y=\"")
	SAVE_FLOAT_TO_DEBUG_FILE(paramCoords.y)
	SAVE_STRING_TO_DEBUG_FILE("\" z=\"")
	SAVE_FLOAT_TO_DEBUG_FILE(paramCoords.z)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	
	SAVE_STRING_TO_DEBUG_FILE(" heading=\"")
	SAVE_FLOAT_TO_DEBUG_FILE(paramHeading)
	SAVE_STRING_TO_DEBUG_FILE("\"")

ENDPROC

/// PURPOSE:
///    KEY C: Writes player coordinates, heading, and roomname to the debug file
/// NOTES:
///    Location Information Includes: coordinates, heading, roomname
PROC Maintain_KeyC_Write_Player_Location_Details_To_File()

	IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
		EXIT
	ENDIF
	
	IF NOT (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C))
		EXIT
	ENDIF
	
	// Coordinates and heading
	VECTOR	xyzP		= GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT	actualZ		= xyzP.z
	FLOAT	heading		= GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	FLOAT	groundZ		= 0.0
	IF (GET_GROUND_Z_FOR_3D_COORD(xyzP, groundZ))
		xyzP.z = groundZ
	ENDIF

	//Camera Coords
	//CAMERA_INDEX vCam	= GET_DEBUG_CAM()
	VECTOR vCamPos  	= GET_FINAL_RENDERED_CAM_COORD() //GET_CAM_COORD(vCam)
	VECTOR vCamRot		= GET_FINAL_RENDERED_CAM_ROT()   //GET_CAM_ROT(vCam)
	FLOAT fFov			= GET_FINAL_RENDERED_CAM_FOV()   //GET_CAM_FOV(vCam)
	FLOAT fGCamHeading	= GET_GAMEPLAY_CAM_RELATIVE_HEADING()
	FLOAT fGCamPitch	= GET_GAMEPLAY_CAM_RELATIVE_PITCH()


	// Dump the Coordinates to the debug file
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Player Position = ")
		Write_Coordinates_And_Heading_To_File(xyzP, heading)
		
		// Dump the player's current room name to the debug file
		STRING theRoom = GET_ROOM_NAME_FROM_PED_DEBUG(PLAYER_PED_ID())
	
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Room Name: ")
	
		IF (ARE_STRINGS_EQUAL(theRoom, ""))
			SAVE_STRING_TO_DEBUG_FILE("NONE")
		ELSE
			SAVE_STRING_TO_DEBUG_FILE(theRoom)
		ENDIF
		
		INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
		
		SAVE_STRING_TO_DEBUG_FILE("   Room Key: ")
		SAVE_INT_TO_DEBUG_FILE(iRoomKey)		
		
		// Actual 'Z' on the next file
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Actual Z coords = ")
		SAVE_FLOAT_TO_DEBUG_FILE(actualZ)
		
		// Dump the player's coordinates and heading in the debug menu format
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Debug Menu Warp = ")
		Write_Debug_Menu_Coordinates_And_Heading_To_File(xyzP, heading)

		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera Position = ")
		SAVE_VECTOR_TO_DEBUG_FILE(vCamPos)

		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_VECTOR_TO_DEBUG_FILE(vCamRot)

		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera FOV = ")
		SAVE_FLOAT_TO_DEBUG_FILE(fFov)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Gameplay Cam Relative Heading = ")
		SAVE_FLOAT_TO_DEBUG_FILE(fGCamHeading)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Gameplay Cam Relative Pitch = ")
		SAVE_FLOAT_TO_DEBUG_FILE(fGCamPitch)
		
		//Output the coords and heading in script command format.
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(entity, ")
		SAVE_VECTOR_TO_DEBUG_FILE(xyzP)
		SAVE_STRING_TO_DEBUG_FILE(")")
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_HEADING(entity, ")
		SAVE_FLOAT_TO_DEBUG_FILE(heading)
		SAVE_STRING_TO_DEBUG_FILE(")")

		// End output to the debug file
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
	
	PRINT_NOW("DUMPCOORDS", 1500, 1)
	PRINTSTRING("WRITING PLAYER AND CAMERA COORDS TO FILE...")
	PRINTNL()

ENDPROC



// *****************************************************************************************
//	Locate Outlines - toggle on/off
// *****************************************************************************************

/// PURPOSE:
///    KEY L: Toggles Locate outlines on/off
PROC Maintain_KeyL_Toggle_Locate_Outlines()

	// Activate?
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_L)
		// ...yes
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_LOCATES_ON))
			CLEAR_BIT(m_sharedDebugFlags, DBG_LOCATES_ON)
			SET_DEBUG_ACTIVE(FALSE)
			PRINTSTRING("Locate Outlines OFF")PRINTNL()
		ELSE
			SET_BIT(m_sharedDebugFlags, DBG_LOCATES_ON)
			SET_DEBUG_ACTIVE(TRUE)
			PRINTSTRING("Locate Outlines ON")PRINTNL()
		ENDIF
	ENDIF

ENDPROC

// *****************************************************************************************
//	Max special - trigger to set
// *****************************************************************************************

/// PURPOSE:
///    
PROC Maintain_AltKeyK_Max_Special_Ability()

	// Activate?
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K,KEYBOARD_MODIFIER_ALT,"Enable and max special ability for all chars") 
	

			PLAYER_INDEX p = GET_PLAYER_INDEX()
			IF NOT IS_PLAYER_DEAD(p)
				//PRINTLN("attempting unlocking and maxing special ability for current char")
				
				
				
				SPECIAL_ABILITY_UNLOCK(GLOBAL_CHARACTER_SHEET_GET_GAME_MODEL(CHAR_FRANKLIN))
				SPECIAL_ABILITY_UNLOCK(GLOBAL_CHARACTER_SHEET_GET_GAME_MODEL(CHAR_TREVOR))
				SPECIAL_ABILITY_UNLOCK(GLOBAL_CHARACTER_SHEET_GET_GAME_MODEL(CHAR_MICHAEL))
				ENABLE_SPECIAL_ABILITY(p,TRUE)
				SPECIAL_ABILITY_FILL_METER(p, TRUE)
				
			ENDIF

	ENDIF

ENDPROC

// *****************************************************************************************
//	Screen Overlays - toggle on/off
// *****************************************************************************************

/// PURPOSE:
///    KEY O: Toggles screen overlays on/off
PROC Maintain_KeyO_Toggle_Screen_Overlays()

	// Activate?
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_O)
		// ...yes
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_OVERLAYS_ON))
			CLEAR_BIT(m_sharedDebugFlags, DBG_OVERLAYS_ON)
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			SET_DEBUG_TEXT_VISIBLE(FALSE)
			PRINTSTRING("Screen Overlays OFF")PRINTNL()
		ELSE
			SET_BIT(m_sharedDebugFlags, DBG_OVERLAYS_ON)
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			PRINTSTRING("Screen Overlays ON")PRINTNL()
		ENDIF
	ENDIF

ENDPROC



// *****************************************************************************************
//	Car Flp - Dpad Down
// *****************************************************************************************

/// PURPOSE:
///    If dpad down is held, right the player vehicle if it is not reasonably upright. 
PROC Maintain_DpadDown_Check_For_Player_Car_Not_Upright()

	CONST_INT	HOLD_BUTTON_msec	2000
	CONST_FLOAT	UPRIGHT_THRESHOLD 	0.125
	
	VECTOR coordsVeh
	FLOAT headingVeh
						
	IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehTemp
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
		IF NOT IS_ENTITY_DEAD(vehTemp)
		
			IF (GET_ENTITY_UPRIGHT_VALUE(vehTemp) < UPRIGHT_THRESHOLD)
					
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
				
					SETTIMERA(0)
					
					WHILE (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
						
						// early-out to avoid the reset check if the HUD is faded
						IF NOT IS_SCREEN_FADED_IN()
							EXIT
						ENDIF
						
						IF IS_ENTITY_DEAD(PLAYER_PED_ID())
							EXIT
						ENDIF
						
						IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
							EXIT
						ENDIF
						
						// check to see if player has control before reset
						IF NOT (IS_PLAYER_CONTROL_ON(PLAYER_ID()))
							EXIT
						ENDIF
						
						IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							EXIT
						ENDIF
						
						BOOL bInBoat = FALSE
						IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
							bInBoat = TRUE
						ENDIF
						
						vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						
						IF NOT bInBoat AND IS_ENTITY_IN_AIR(vehTemp)
							EXIT
						ENDIF
						
						// Has the player waited the desired length of time?
						IF (TIMERA() > HOLD_BUTTON_msec)							
							// ...yes, so fix the car
							
							PRINTSTRING("DEBUG Fixing player car.")PRINTNL()
							
							SET_VEHICLE_FIXED(vehTemp)
							SET_ENTITY_HEALTH(vehTemp, 1000)
							
							coordsVeh = GET_ENTITY_COORDS(vehTemp)
							headingVeh = GET_ENTITY_HEADING(vehTemp)

							IF NOT bInBoat
								GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(coordsVeh,coordsVeh,headingVeh)
								SET_ENTITY_HEADING(vehTemp, headingVeh)
								SET_ENTITY_COORDS(vehTemp,coordsVeh)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehTemp)
							ELSE
								SET_ENTITY_ROTATION(vehTemp, <<0,0,0>>)
								SET_ENTITY_HEADING(vehTemp, headingVeh)
							ENDIF
														
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							
							WHILE (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
								WAIT(0)
							ENDWHILE
							
							EXIT
							
						ENDIF
						
						// Keep the car alive if the button is pressed but timer hasn't elapsed
						IF (IS_VEHICLE_DRIVEABLE(vehTemp))
							IF GET_ENTITY_HEALTH(vehTemp) < 300
								SET_ENTITY_HEALTH(vehTemp, 400)
							ENDIF							
						ELSE						
							EXIT
						ENDIF
						
						WAIT(0)
						
					ENDWHILE
					
					// Button has been released, but too early, so do nothing
				ENDIF
				
			ENDIF
		
		ENDIF
		
	ENDIF

ENDPROC


// *****************************************************************************************
//	HASH KEY GENERATOR
// *****************************************************************************************

// PURPOSE: Creates the widgets that generates a hash key value from a string
PROC Create_Hash_Key_Generator_Widgets(structSharedDebugVars &paramVars)

	START_WIDGET_GROUP("Hash Key Generator")
		paramVars.hashKeyGeneratorTextWidgetID = ADD_TEXT_WIDGET("Hash Key String")
		ADD_WIDGET_INT_READ_ONLY("Hash Value", paramVars.hashKey)
		ADD_WIDGET_BOOL("Generate Hash Key (accurate in temp_debug.txt)", paramVars.generateHashKey)
	STOP_WIDGET_GROUP()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Reposition the vehicle back at the start of the recording ready to try again
PROC Maintain_Hash_Key_Generator_Widgets(structSharedDebugVars &paramVars)

	IF NOT (paramVars.generateHashKey)
		EXIT
	ENDIF
	
	paramVars.generateHashKey = FALSE
	
	STRING theString = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.hashKeyGeneratorTextWidgetID)
	IF NOT (IS_STRING_NULL(theString))
		IF NOT (ARE_STRINGS_EQUAL(theString, ""))
			paramVars.hashKey = GET_HASH_KEY(theString)
		ELSE
			EXIT
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("HASH KEY GENERATION: ")
		SAVE_STRING_TO_DEBUG_FILE(theString)
		SAVE_STRING_TO_DEBUG_FILE(":  ")
		SAVE_INT_TO_DEBUG_FILE(paramVars.hashKey)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
		
ENDPROC


// *****************************************************************************************
//	GENERIC DEBUG WIDGETS
// *****************************************************************************************

// PURPOSE: Create widgets for generic debug
PROC Create_Generic_Debug_Widgets(structSharedDebugVars &paramVars)

	START_WIDGET_GROUP("General Debug")
		ADD_WIDGET_BOOL("shutdown loading screens", paramVars.bShutdownLoadingScreens)
		ADD_WIDGET_BOOL("force fade in", paramVars.forcefadein)
		ADD_WIDGET_BOOL("force fade out", paramVars.forcefadeout)
		ADD_WIDGET_BOOL("force kill player", paramVars.killplayer)
		ADD_WIDGET_BOOL("force player control on", paramVars.forceplayercontrolon)
		ADD_WIDGET_BOOL("force player control off", paramVars.forceplayercontroloff)
		ADD_WIDGET_BOOL("force player position freeze", paramVars.forceplayerfrozen)
		ADD_WIDGET_BOOL("force player position unfreeze", paramVars.forceplayernotfrozen)
		ADD_WIDGET_BOOL("force player collision on", paramVars.forceplayercollision)
		ADD_WIDGET_BOOL("force player collision off", paramVars.forceplayernocollision)
		ADD_WIDGET_BOOL("return to gameplay camera", paramVars.returnToGameplayCam)
		ADD_WIDGET_BOOL("reactivate world brains", paramVars.reactivateWorldBrains)
		ADD_WIDGET_BOOL("clear area around player (100m radius)", paramVars.clearareaaroundplayer)
	STOP_WIDGET_GROUP()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Handles generic debug 
PROC Maintain_Generic_Debug_Widgets(structSharedDebugVars &paramVars)

	IF paramVars.bShutdownLoadingScreens
		SHUTDOWN_LOADING_SCREEN()
		paramVars.bShutdownLoadingScreens = FALSE
	ENDIF

	IF paramVars.forcefadein
		
		IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_IN(1000)
		ENDIF
		
		paramVars.forcefadein = FALSE
	ENDIF
	
	IF paramVars.forcefadeout
		IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_OUT(1000)
		ENDIF
		paramVars.forcefadeout = FALSE
	ENDIF
	
	IF paramVars.killplayer
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			SET_ENTITY_HEALTH(GET_PLAYER_PED(GET_PLAYER_INDEX()), 1)
		ENDIF
		
		paramVars.killplayer = FALSE
	ENDIF
	
	IF paramVars.forceplayercontrolon
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE,SPC_AMBIENT_SCRIPT)
		ENDIF
		
		paramVars.forceplayercontrolon = FALSE
	ENDIF
	
	IF paramVars.forceplayercontroloff
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE,SPC_AMBIENT_SCRIPT)
		ENDIF
		paramVars.forceplayercontroloff = FALSE
	ENDIF
	
	IF paramVars.forceplayerfrozen
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			FREEZE_ENTITY_POSITION(GET_PLAYER_PED(GET_PLAYER_INDEX()), TRUE)
		ENDIF
		paramVars.forceplayerfrozen = FALSE
	ENDIF
	
	IF paramVars.forceplayernotfrozen
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			FREEZE_ENTITY_POSITION(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE)
		ENDIF
		paramVars.forceplayernotfrozen = FALSE
	ENDIF
	
	IF paramVars.forceplayercollision
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			SET_ENTITY_COLLISION(GET_PLAYER_PED(GET_PLAYER_INDEX()), TRUE)
		ENDIF
		paramVars.forceplayercollision = FALSE
	ENDIF
	
	IF paramVars.forceplayernocollision
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			SET_ENTITY_COLLISION(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE)
		ENDIF
		paramVars.forceplayernocollision = FALSE
	ENDIF
	
	IF paramVars.reactivateWorldBrains
		REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
		paramVars.reactivateWorldBrains = FALSE
	ENDIF
	
	IF paramVars.returnToGameplayCam
		DESTROY_ALL_CAMS()
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		paramVars.returnToGameplayCam = FALSE
	ENDIF
	
	IF paramVars.clearareaaroundplayer
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			CLEAR_AREA(GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX())), 100.0, TRUE)
		ENDIF
		
		paramVars.clearareaaroundplayer = FALSE
	ENDIF
	
ENDPROC


// *****************************************************************************************
//	TEST SAVEGAME WIDGETS
// *****************************************************************************************

// PURPOSE: Create widgets for new savegame method test functions
PROC Create_Test_SaveGame_Widgets(structSharedDebugVars &paramVars)

	START_WIDGET_GROUP("Save Game")
		ADD_WIDGET_BOOL("AutoSave", paramVars.doAutosave)
		ADD_WIDGET_BOOL("Normal Save", paramVars.doNormalSave)
	STOP_WIDGET_GROUP()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Maintain_Test_Autosave(structSharedDebugVars &paramVars)

	IF NOT (paramVars.doAutosave)
		EXIT
	ENDIF
	
	paramVars.doAutosave = FALSE

	PRINTSTRING("--------------------------------------")
	PRINTNL()
	PRINTSTRING("KGM......DEBUG: About to perform autosave")
	PRINTNL()	
	PRINTSTRING("--------------------------------------")
	PRINTNL()
	PRINTNL()

	DO_AUTO_SAVE()
	
	WHILE (IS_AUTO_SAVE_IN_PROGRESS())
		WAIT(0)
		PRINTSTRING("...KGM...DEBUG: doing autosave")
		PRINTNL()
	ENDWHILE

	PRINTSTRING("--------------------------------------")
	PRINTNL()
	PRINTSTRING("KGM......DEBUG: Autosave Complete")
	PRINTNL()	
	PRINTSTRING("--------------------------------------")
	PRINTNL()
	PRINTNL()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Maintain_Test_Normal_Save(structSharedDebugVars &paramVars)

	IF NOT (paramVars.doNormalSave)
		EXIT
	ENDIF
	
	paramVars.doNormalSave = FALSE

	PRINTSTRING("--------------------------------------")
	PRINTNL()
	PRINTSTRING("KGM......DEBUG: About to perform normal save")
	PRINTNL()	
	PRINTSTRING("--------------------------------------")
	PRINTNL()
	PRINTNL()

	SET_SAVE_MENU_ACTIVE()
			
	IF (DID_SAVE_COMPLETE_SUCCESSFULLY())
		PRINTSTRING("--------------------------------------")
		PRINTNL()
		PRINTSTRING("KGM......DEBUG: Normal Save Completed Successfully")
		PRINTNL()	
		PRINTSTRING("--------------------------------------")
		PRINTNL()
		PRINTNL()
	ELSE
		PRINTSTRING("--------------------------------------")
		PRINTNL()
		PRINTSTRING("KGM......DEBUG: Normal Save FAILED")
		PRINTNL()	
		PRINTSTRING("--------------------------------------")
		PRINTNL()
		PRINTNL()
	ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Handles generic debug 
PROC Maintain_Test_SaveGame_Widgets(structSharedDebugVars &paramVars)

	Maintain_Test_Autosave(paramVars)
	Maintain_Test_Normal_Save(paramVars)

ENDPROC


// *****************************************************************************************
//	VEHICLE RECORDINGS
// *****************************************************************************************

/// PURPOSE: Creates the widgets that allow a vehicle recording to start and stop
PROC Create_Vehicle_Recording_Widgets(structSharedDebugVars &paramVars)

	START_WIDGET_GROUP("Vehicle Recording")
		paramVars.vehicleRecordingFilenameTextWidgetID = ADD_TEXT_WIDGET("Car Recording Filename")
		ADD_WIDGET_INT_SLIDER("Car Recording ID", paramVars.vehicleRecordingID, 0, 999, 1)
		ADD_WIDGET_BOOL("Start (ticked) / Stop (unticked)", paramVars.vehicleRecordingRequested)
		ADD_WIDGET_BOOL("Reset Vehicle to Start Position?", paramVars.moveVehicleBackToStartPosition)
		ADD_WIDGET_BOOL("Overwrite Duplicate Save Name?", paramVars.overwriteRecording)
		ADD_WIDGET_BOOL("Overwrite Duplicate Save Name - DON'T AUTO-UNTICK?", paramVars.keepOverwriteRecordingTicked)
		ADD_WIDGET_BOOL("Use SQUARE button as Start / Stop?", paramVars.allowSquareButtonToStartStopVehicleRecording)
		ADD_WIDGET_BOOL("Record attached trailer at the same time", paramVars.bAllowSimultaneousTrailerRecording)
		ADD_WIDGET_INT_SLIDER("Trailer Recording ID", paramVars.iTrailerRecordingID, 0, 999, 1)
	STOP_WIDGET_GROUP()

ENDPROC


// *****************************************************************************************
//	VEHICLE COLOURS
// *****************************************************************************************

/// PURPOSE: Creates the widgets that allow a vehicle recording to start and stop
PROC Create_Vehicle_Colour_Widgets(structSharedDebugVars &paramVars)

	// Reading in the carcol.xml file so we can find out the total car colour count
	INT iColourCount
	/*IF LOAD_XML_FILE("../../carcols.xml")
 		IF GET_NUMBER_OF_XML_NODES() <> 0
			INT eachNode
			FOR eachNode = 0 TO (GET_NUMBER_OF_XML_NODES()-1)
				IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), "colorName")
					iColourCount++
				ENDIF
				GET_NEXT_XML_NODE()
			ENDFOR
		ENDIF
		DELETE_XML_FILE()
	ENDIF*/
	IF iColourCount = 0
		iColourCount = 250
	ENDIF
	
	START_WIDGET_GROUP("Vehicle Colours")
		ADD_WIDGET_BOOL("Activate", paramVars.vehicleColourActivate)
		ADD_WIDGET_BOOL("Clean vehicle", paramVars.vehicleColourClean)
		
		ADD_WIDGET_INT_SLIDER("Colour 1", paramVars.vehicleColour1, 0, iColourCount, 1)
		ADD_WIDGET_INT_SLIDER("Colour 2", paramVars.vehicleColour2, 0, iColourCount, 1)
		ADD_WIDGET_INT_SLIDER("Colour 3", paramVars.vehicleColour3, 0, iColourCount, 1)
		ADD_WIDGET_INT_SLIDER("Colour 4", paramVars.vehicleColour4, 0, iColourCount, 1)
		ADD_WIDGET_INT_SLIDER("Colour 5", paramVars.vehicleColour5, 0, iColourCount, 1)
		ADD_WIDGET_INT_SLIDER("Colour 6", paramVars.vehicleColour6, 0, iColourCount, 1)
		ADD_WIDGET_FLOAT_SLIDER("Env Eff", paramVars.vehicleEnvEff, 0, 1.0, 0.01)
		
		
		paramVars.vehicleColourPath  = ADD_TEXT_WIDGET("Save path")
		paramVars.vehicleColourModel = ADD_TEXT_WIDGET("Vehicle model")
		SET_CONTENTS_OF_TEXT_WIDGET(paramVars.vehicleColourPath, "X:\\")
		SET_CONTENTS_OF_TEXT_WIDGET(paramVars.vehicleColourModel, "model")
		
		ADD_WIDGET_BOOL("Output data", paramVars.vehicleColourOutput)
	STOP_WIDGET_GROUP()

ENDPROC

/// PURPOSE: Creates the widgets that allow vehicle mods to be set
PROC Create_Vehicle_Mod_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Vehicle Mods")
	
		ADD_WIDGET_BOOL("Display current mods", paramVars.vehicleModDisplayCurrentMods)
		ADD_WIDGET_BOOL("Display selected vehicle current mods", paramVars.selectedVehicleModDisplayCurrentMods)
		
		START_WIDGET_GROUP("Basic Mods")
			ADD_WIDGET_BOOL("Enable", paramVars.vehicleModEnableBasic)
			
			INT i
			REPEAT COUNT_OF(paramVars.vehicleModExtraOn) i
				TEXT_LABEL str = "Extra["
				str += (i+1)
				str += "]"
				ADD_WIDGET_BOOL(str, paramVars.vehicleModExtraOn[i])
			ENDREPEAT
			
			ADD_WIDGET_INT_SLIDER("Livery", paramVars.vehicleLivery, 0, 10, 1)
			ADD_WIDGET_INT_SLIDER("Livery2", paramVars.vehicleLivery2, 0, 10, 1)
			ADD_WIDGET_INT_SLIDER("Window Tint Colour", paramVars.vehicleModWindowTintColour, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("Tyre Smoke Colour R", paramVars.vehicleModTyreSmokeR, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Tyre Smoke Colour G", paramVars.vehicleModTyreSmokeG, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Tyre Smoke Colour B", paramVars.vehicleModTyreSmokeB, 0, 255, 1)
			ADD_WIDGET_STRING("Tyre Type 0: Standard, 1: LGT, 2: BPT")
			ADD_WIDGET_INT_SLIDER("Tyre Type", paramVars.vehicleTyreType, -1, 2, 1)
			ADD_WIDGET_INT_SLIDER("Xenon Color Index", paramVars.XenonColorLightIndex, 0, 255, 1)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Advanced Mods")
			ADD_WIDGET_BOOL("Enable", paramVars.vehicleModEnableAdvanced)
			ADD_WIDGET_INT_SLIDER("Wheel Type", paramVars.vehicleModWheelType, 0, 13, 1)
			ADD_WIDGET_INT_SLIDER("MOD_SPOILER", paramVars.vehicleModSlotIndex[MOD_SPOILER], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_BUMPER_F", paramVars.vehicleModSlotIndex[MOD_BUMPER_F], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_BUMPER_R", paramVars.vehicleModSlotIndex[MOD_BUMPER_R], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_SKIRT", paramVars.vehicleModSlotIndex[MOD_SKIRT], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_EXHAUST", paramVars.vehicleModSlotIndex[MOD_EXHAUST], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_CHASSIS", paramVars.vehicleModSlotIndex[MOD_CHASSIS], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_GRILL", paramVars.vehicleModSlotIndex[MOD_GRILL], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_BONNET", paramVars.vehicleModSlotIndex[MOD_BONNET], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_WING_L", paramVars.vehicleModSlotIndex[MOD_WING_L], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_WING_R", paramVars.vehicleModSlotIndex[MOD_WING_R], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_ROOF", paramVars.vehicleModSlotIndex[MOD_ROOF], -1, 250, 1)
			
			ADD_WIDGET_INT_SLIDER("MOD_ENGINE", paramVars.vehicleModSlotIndex[MOD_ENGINE], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_BRAKES", paramVars.vehicleModSlotIndex[MOD_BRAKES], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_GEARBOX", paramVars.vehicleModSlotIndex[MOD_GEARBOX], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_HORN", paramVars.vehicleModSlotIndex[MOD_HORN], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_SUSPENSION", paramVars.vehicleModSlotIndex[MOD_SUSPENSION], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_ARMOUR", paramVars.vehicleModSlotIndex[MOD_ARMOUR], -1, 250, 1)
			
			ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_NITROUS", paramVars.vehicleModSlotIndex[MOD_TOGGLE_NITROUS], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_TURBO", paramVars.vehicleModSlotIndex[MOD_TOGGLE_TURBO], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_SUBWOOFER", paramVars.vehicleModSlotIndex[MOD_TOGGLE_SUBWOOFER], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_TYRE_SMOKE", paramVars.vehicleModSlotIndex[MOD_TOGGLE_TYRE_SMOKE], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_HYDRAULICS", paramVars.vehicleModSlotIndex[MOD_TOGGLE_HYDRAULICS], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_XENON_LIGHTS", paramVars.vehicleModSlotIndex[MOD_TOGGLE_XENON_LIGHTS], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_WHEELS", paramVars.vehicleModSlotIndex[MOD_WHEELS], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_REAR_WHEELS", paramVars.vehicleModSlotIndex[MOD_REAR_WHEELS], -1, 250, 1)
			ADD_WIDGET_BOOL("Tire Design", paramVars.bTireDesign)
			
			ADD_WIDGET_INT_SLIDER("MOD_PLTHOLDER", paramVars.vehicleModSlotIndex[MOD_PLTHOLDER], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_PLTVANITY", paramVars.vehicleModSlotIndex[MOD_PLTVANITY], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_INTERIOR1", paramVars.vehicleModSlotIndex[MOD_INTERIOR1], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_INTERIOR2", paramVars.vehicleModSlotIndex[MOD_INTERIOR2], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_INTERIOR3", paramVars.vehicleModSlotIndex[MOD_INTERIOR3], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_INTERIOR4", paramVars.vehicleModSlotIndex[MOD_INTERIOR4], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_INTERIOR5", paramVars.vehicleModSlotIndex[MOD_INTERIOR5], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_SEATS", paramVars.vehicleModSlotIndex[MOD_SEATS], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_STEERING", paramVars.vehicleModSlotIndex[MOD_STEERING], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_KNOB", paramVars.vehicleModSlotIndex[MOD_KNOB], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_PLAQUE", paramVars.vehicleModSlotIndex[MOD_PLAQUE], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_ICE", paramVars.vehicleModSlotIndex[MOD_ICE], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_TRUNK", paramVars.vehicleModSlotIndex[MOD_TRUNK], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_HYDRO", paramVars.vehicleModSlotIndex[MOD_HYDRO], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_ENGINEBAY1", paramVars.vehicleModSlotIndex[MOD_ENGINEBAY1], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_ENGINEBAY2", paramVars.vehicleModSlotIndex[MOD_ENGINEBAY2], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_ENGINEBAY3", paramVars.vehicleModSlotIndex[MOD_ENGINEBAY3], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_CHASSIS2", paramVars.vehicleModSlotIndex[MOD_CHASSIS2], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_CHASSIS3", paramVars.vehicleModSlotIndex[MOD_CHASSIS3], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_CHASSIS4", paramVars.vehicleModSlotIndex[MOD_CHASSIS4], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_CHASSIS5", paramVars.vehicleModSlotIndex[MOD_CHASSIS5], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_DOOR_L", paramVars.vehicleModSlotIndex[MOD_DOOR_L], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_DOOR_R", paramVars.vehicleModSlotIndex[MOD_DOOR_R], -1, 250, 1)
			ADD_WIDGET_INT_SLIDER("MOD_LIVERY", paramVars.vehicleModSlotIndex[MOD_LIVERY], -1, 250, 1)
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC Create_Vehicle_Performance_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Vehicle Performance")
	
		ADD_WIDGET_BOOL("Enable max speed override", paramVars.bMaxSpeedOverride)
		ADD_WIDGET_FLOAT_SLIDER("SET_VEHICLE_MAX_SPEED", paramVars.vehicleMaxSpeedOverride, -100.0, 100.0, 1.0)
		
		ADD_WIDGET_BOOL("Enable top speed override", paramVars.bTopSpeedOverride)
		ADD_WIDGET_FLOAT_SLIDER("MODIFY_VEHICLE_TOP_SPEED", paramVars.vehicleTopSpeedOverride, -100.0, 100.0, 1.0)
		
	STOP_WIDGET_GROUP()
ENDPROC





// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Reposition the vehicle back at the start of the recording ready to try again
PROC Move_Vehicle_Back_To_Start_Of_Recording(structSharedDebugVars &paramVars)

	IF (paramVars.vehicleRecordingBeingRecorded) 
		PRINT_NOW("CRECSTOP5", 8000, 1)
	ENDIF
	
	paramVars.vehicleRecordingRequested		= FALSE
	paramVars.vehicleRecordingBeingRecorded	= FALSE
	
	STOP_RECORDING_ALL_VEHICLES()
	
	IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
		EXIT
	ENDIF
	
	IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
		EXIT
	ENDIF
	
	paramVars.indexVehicleRecordingVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	SET_ENTITY_COORDS(paramVars.indexVehicleRecordingVehicle, paramVars.xyzVehicleRecordingStartPos)
	SET_ENTITY_QUATERNION(paramVars.indexVehicleRecordingVehicle, paramVars.quatVehicleRecordingStartX, paramVars.quatVehicleRecordingStartY, paramVars.quatVehicleRecordingStartZ, paramVars.quatVehicleRecordingStartW)
	
	IF paramVars.bAllowSimultaneousTrailerRecording
		IF NOT IS_ENTITY_DEAD(paramVars.vehTrailerToRecord)
			SET_ENTITY_COORDS_NO_OFFSET(paramVars.vehTrailerToRecord, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.indexVehicleRecordingVehicle, <<0.0, -10.0, 0.0>>))
			SET_ENTITY_ROTATION(paramVars.vehTrailerToRecord, GET_ENTITY_ROTATION(paramVars.indexVehicleRecordingVehicle))
			ATTACH_VEHICLE_TO_TRAILER(paramVars.indexVehicleRecordingVehicle, paramVars.vehTrailerToRecord)
		ENDIF		
	ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: MAke sure the conditions are right to continue with the vehicle recording
PROC Maintain_Active_Vehicle_Recording(structSharedDebugVars &paramVars)

	IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
		PRINT_NOW("CRECENOP5", 8000, 1)
		paramVars.vehicleRecordingRequested = FALSE
		paramVars.vehicleRecordingBeingRecorded = FALSE
		STOP_RECORDING_ALL_VEHICLES()
		EXIT
	ENDIF
	
	IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
		PRINT_NOW("CRECENOC5", 8000, 1)
		paramVars.vehicleRecordingRequested = FALSE
		paramVars.vehicleRecordingBeingRecorded = FALSE
		STOP_RECORDING_ALL_VEHICLES()
		EXIT
	ENDIF
	
	IF NOT (IS_VEHICLE_DRIVEABLE(paramVars.indexVehicleRecordingVehicle))
		PRINT_NOW("CRECEDED5", 8000, 1)
		paramVars.vehicleRecordingRequested = FALSE
		paramVars.vehicleRecordingBeingRecorded = FALSE
		STOP_RECORDING_ALL_VEHICLES()
		EXIT
	ENDIF
	
	IF NOT (IS_RECORDING_GOING_ON_FOR_VEHICLE(paramVars.indexVehicleRecordingVehicle))
		PRINT_NOW("CRECENOR5", 8000, 1)
		paramVars.vehicleRecordingRequested = FALSE
		paramVars.vehicleRecordingBeingRecorded = FALSE
		STOP_RECORDING_ALL_VEHICLES()
		EXIT
	ENDIF
	
	// Everything fine, so let it continue
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Write the position and quaternian details to the debug file
PROC Write_Vehicle_Recording_Start_Position_Details_To_Debug_File(structSharedDebugVars &paramVars)

	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		STRING theFilename = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.vehicleRecordingFilenameTextWidgetID)
		IF NOT (IS_STRING_NULL(theFilename))
			IF NOT (ARE_STRINGS_EQUAL(theFilename, ""))
				SAVE_STRING_TO_DEBUG_FILE(theFilename)
				SAVE_STRING_TO_DEBUG_FILE(":  ")
			ENDIF
		ENDIF
		SAVE_INT_TO_DEBUG_FILE(paramVars.vehicleRecordingID)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE("Vehicle Rec Start = ")
		SAVE_VECTOR_TO_DEBUG_FILE(paramVars.xyzVehicleRecordingStartPos)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Vehicle Rec Rot = ")
		SAVE_VECTOR_TO_DEBUG_FILE(paramVars.xyzVehicleRecordingStartRot)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Quat = ")
		SAVE_FLOAT_TO_DEBUG_FILE(paramVars.quatVehicleRecordingStartX)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(paramVars.quatVehicleRecordingStartY)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(paramVars.quatVehicleRecordingStartZ)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(paramVars.quatVehicleRecordingStartW)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Maintain_Vehicle_Recording_Widgets(structSharedDebugVars &paramVars)

	// reset to start position
	IF (paramVars.moveVehicleBackToStartPosition)
		Move_Vehicle_Back_To_Start_Of_Recording(paramVars)
		paramVars.moveVehicleBackToStartPosition = FALSE
	ENDIF
	
	// Check if the PS3 Pad SQUARE button should start/stop a recording
	IF (paramVars.allowSquareButtonToStartStopVehicleRecording)
		IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X))
			// Change the state of the vehicleRecordingRequested flag
			IF (paramVars.vehicleRecordingRequested)
				paramVars.vehicleRecordingRequested = FALSE
			ELSE
				paramVars.vehicleRecordingRequested = TRUE
			ENDIF
		ENDIF
	ENDIF

	IF (paramVars.vehicleRecordingRequested)
		IF (paramVars.vehicleRecordingBeingRecorded)
			Maintain_Active_Vehicle_Recording(paramVars)
			EXIT
		ENDIF
		
		// Error Checking Before Starting Recording
		// ...player playing?
		IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
			PRINT_NOW("CRECSNOP5", 8000, 1)
			paramVars.vehicleRecordingRequested = FALSE
			EXIT
		ENDIF
		
		// ...player in vehicle?
		IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
			PRINT_NOW("CRECSNOC5", 8000, 1)
			paramVars.vehicleRecordingRequested = FALSE
			EXIT
		ENDIF

		paramVars.indexVehicleRecordingVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

		IF paramVars.bAllowSimultaneousTrailerRecording
			IF IS_VEHICLE_ATTACHED_TO_TRAILER(paramVars.indexVehicleRecordingVehicle)
				GET_VEHICLE_TRAILER_VEHICLE(paramVars.indexVehicleRecordingVehicle, paramVars.vehTrailerToRecord)
			ENDIF
		ENDIF

		// ...vehicle dead?
		IF NOT (IS_VEHICLE_DRIVEABLE(paramVars.indexVehicleRecordingVehicle))
			PRINT_NOW("CRECSDED5", 8000, 1)
			paramVars.vehicleRecordingRequested = FALSE
			EXIT
		ENDIF
		
		// ...is recording already going on for this vehicle?
		IF (IS_RECORDING_GOING_ON_FOR_VEHICLE(paramVars.indexVehicleRecordingVehicle))
			PRINT_NOW("CRECSREC5", 8000, 1)
			paramVars.vehicleRecordingRequested = FALSE
			STOP_RECORDING_ALL_VEHICLES()
			EXIT
		ENDIF
		
		STRING theFilename = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.vehicleRecordingFilenameTextWidgetID)
		IF (IS_STRING_NULL(theFilename))
			PRINT_NOW("CRECSNFN5", 8000, 1)
			paramVars.vehicleRecordingRequested = FALSE
			STOP_RECORDING_ALL_VEHICLES()
			EXIT
		ELSE
			IF (ARE_STRINGS_EQUAL(theFilename, ""))
			OR (ARE_STRINGS_EQUAL(theFilename, "New text widget"))
				PRINT_NOW("CRECSNFN5", 8000, 1)
				paramVars.vehicleRecordingRequested = FALSE
				STOP_RECORDING_ALL_VEHICLES()
				EXIT
			ENDIF
		ENDIF
		
		IF NOT (START_RECORDING_VEHICLE(paramVars.indexVehicleRecordingVehicle, paramVars.vehicleRecordingID, theFilename, paramVars.overwriteRecording))
			PRINT_NOW("CRECSRTN5", 8000, 1)
			paramVars.vehicleRecordingRequested = FALSE
			STOP_RECORDING_ALL_VEHICLES()
			EXIT
		ENDIF
		
		//Start recording the trailer if the front vehicle successfully started recording.
		IF paramVars.bAllowSimultaneousTrailerRecording
			IF NOT IS_ENTITY_DEAD(paramVars.vehTrailerToRecord)
				START_RECORDING_VEHICLE(paramVars.vehTrailerToRecord, paramVars.iTrailerRecordingID, theFilename, paramVars.overwriteRecording)
			ENDIF
		ENDIF
		
		// Everything okay, so start recording
		PRINT_NOW("CRECSTART5", 300000, 1)
		
		// Clear the 'overwrite' flag (unless it has been requested to not auto-untick)
		IF NOT (paramVars.keepOverwriteRecordingTicked)
			paramVars.overwriteRecording = FALSE
		ENDIF
		
		// grab the start position and quaternion
		paramVars.xyzVehicleRecordingStartPos = GET_ENTITY_COORDS(paramVars.indexVehicleRecordingVehicle)
		paramVars.xyzVehicleRecordingStartRot = GET_ENTITY_ROTATION(paramVars.indexVehicleRecordingVehicle)
		GET_ENTITY_QUATERNION(paramVars.indexVehicleRecordingVehicle, paramVars.quatVehicleRecordingStartX, paramVars.quatVehicleRecordingStartY, paramVars.quatVehicleRecordingStartZ, paramVars.quatVehicleRecordingStartW)
		Write_Vehicle_Recording_Start_Position_Details_To_Debug_File(paramVars)
		paramVars.vehicleRecordingBeingRecorded = TRUE
		EXIT
	ENDIF
	
	// Recording not activated, so stop one if one is currently being done
	IF (paramVars.vehicleRecordingBeingRecorded)
		// ...currently doing a vehicle recording, so stop it
		PRINT_NOW("CRECSTOP5", 8000, 1)
		STOP_RECORDING_ALL_VEHICLES()
		paramVars.vehicleRecordingBeingRecorded = FALSE
		EXIT
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allows vehicle artists to quickly switch vehicle colours when setting up the carcols.dat file
PROC Maintain_Vehicle_Colour_Widgets(structSharedDebugVars &paramVars)
	
	// 	Only update when activated
	IF paramVars.vehicleColourActivate
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
				
					INT iCol1, iCol2, iCol3, iCol4
					INT iCol5, iCol6
					
					// Set the default colours if we have just got in the vehicle
					IF NOT paramVars.vehicleColourInitialise
						GET_VEHICLE_COLOURS(vehTemp, paramVars.vehicleColour1, paramVars.vehicleColour2)
						GET_VEHICLE_EXTRA_COLOURS(vehTemp, paramVars.vehicleColour3, paramVars.vehicleColour4)
						GET_VEHICLE_EXTRA_COLOUR_5(vehTemp, paramVars.vehicleColour5)
						paramVars.vehicleColourInitialise = TRUE
					ENDIF
					
					// Grab the current vehicle colours
					GET_VEHICLE_COLOURS(vehTemp, iCol1,iCol2)
					GET_VEHICLE_EXTRA_COLOURS(vehTemp, iCol3,iCol4)
					GET_VEHICLE_EXTRA_COLOUR_5(vehTemp, iCol5)
					GET_VEHICLE_EXTRA_COLOUR_5(vehTemp, iCol6)
					
					// Update the vehicle colours
					IF iCol1 <> paramVars.vehicleColour1 OR iCol2 <> paramVars.vehicleColour2
						IF IS_VEHICLE_COLOUR_INDEX_LOCKED(paramVars.vehicleColour1)
						OR IS_VEHICLE_COLOUR_INDEX_LOCKED(paramVars.vehicleColour2)
							// skip
						ELSE
							SET_VEHICLE_COLOURS(vehTemp, paramVars.vehicleColour1, paramVars.vehicleColour2)
						ENDIF	
					ENDIF
					
					// Update the vehicle extra colours
					IF iCol3 <> paramVars.vehicleColour3 OR iCol4 <> paramVars.vehicleColour4
						SET_VEHICLE_EXTRA_COLOURS(vehTemp, paramVars.vehicleColour3, paramVars.vehicleColour4)
					ENDIF
					
					// Update the vehicle extra colour 5 & 6
					IF iCol6 <> paramVars.vehicleColour5
						SET_VEHICLE_EXTRA_COLOUR_5(vehTemp, paramVars.vehicleColour5)
					ENDIF
					IF iCol6 <> paramVars.vehicleColour6
						SET_VEHICLE_EXTRA_COLOUR_6(vehTemp, paramVars.vehicleColour6)
					ENDIF
					
					FLOAT fEnvEff = GET_VEHICLE_ENVEFF_SCALE(vehTemp)
					IF fEnvEff != paramVars.vehicleEnvEff
						SET_VEHICLE_ENVEFF_SCALE(vehTemp, paramVars.vehicleEnvEff)
					ENDIF
					
					// Clean the vehicle
					IF paramVars.vehicleColourClean
						SET_VEHICLE_DIRT_LEVEL(vehTemp, 0.0)
						paramVars.vehicleColourClean = FALSE
					ENDIF
				ENDIF
			ELSE
				paramVars.vehicleColourInitialise = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Output the colours in number form
	IF paramVars.vehicleColourOutput
	
		BOOL bNewData = FALSE
		
		TEXT_LABEL_23 sModel = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.vehicleColourModel)
		TEXT_LABEL_63 sPath  = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.vehicleColourPath)
		TEXT_LABEL_23 sFile = sModel
		sFile += ".txt"
		
		// Compare the strings so we know if we should add the initial data
		IF NOT ARE_STRINGS_EQUAL(paramVars.vehicleColourPathStr, sPath)
		OR NOT ARE_STRINGS_EQUAL(paramVars.vehicleColourModelStr, sModel)
			bNewData = TRUE
			paramVars.vehicleColourModelStr = sModel
			paramVars.vehicleColourPathStr = sPath
		ENDIF
		
		OPEN_NAMED_DEBUG_FILE(sPath, sFile)
		
		IF bNewData
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sModel, sPath, sFile)
		
			
			IF GET_LENGTH_OF_LITERAL_STRING(sModel) < 3
				SAVE_STRING_TO_NAMED_DEBUG_FILE(",			", sPath, sFile)
			ELIF GET_LENGTH_OF_LITERAL_STRING(sModel) < 8
				SAVE_STRING_TO_NAMED_DEBUG_FILE(",		", sPath, sFile)
			ELSE
				SAVE_STRING_TO_NAMED_DEBUG_FILE(",	", sPath, sFile)
			ENDIF
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", sPath, sFile)
		ENDIF
		
		SAVE_INT_TO_NAMED_DEBUG_FILE(paramVars.vehicleColour1, sPath, sFile)SAVE_STRING_TO_NAMED_DEBUG_FILE(",", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(paramVars.vehicleColour2, sPath, sFile)SAVE_STRING_TO_NAMED_DEBUG_FILE(",", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(paramVars.vehicleColour3, sPath, sFile)SAVE_STRING_TO_NAMED_DEBUG_FILE(",", sPath, sFile)
		
		IF paramVars.vehicleColour4 <> 0
			SAVE_INT_TO_NAMED_DEBUG_FILE(paramVars.vehicleColour4, sPath, sFile)SAVE_STRING_TO_NAMED_DEBUG_FILE(",", sPath, sFile)
		ENDIF
		
		CLOSE_DEBUG_FILE()
		
		PRINTSTRING("*** Vehicle colour data output to '")PRINTSTRING(sPath)PRINTSTRING(sFile)PRINTNL()
		paramVars.vehicleColourOutput = FALSE
	ENDIF
	
ENDPROC

PROC Display_Vehicle_Mod_Debug(VEHICLE_INDEX &vehID, MOD_TYPE eMod, BOOL bToggleMod, STRING sTitle, FLOAT fTextX, FLOAT fTextY)
	
	TEXT_LABEL_63 tlText = sTitle
	tlText += ": "
	
	IF NOT bToggleMod
		IF (GET_VEHICLE_MOD(vehID, eMod) = -1)
			tlText += "None"
		ELSE
			tlText += (GET_VEHICLE_MOD(vehID, eMod)+1)
			tlText += " of "
			tlText += (GET_NUM_VEHICLE_MODS(vehID, eMod))
		ENDIF
	ELSE
		IF IS_TOGGLE_MOD_ON(vehID, eMod)
			tlText += "On"
		ELSE
			tlText += "Off"
		ENDIF
	ENDIF
	
	SET_TEXT_SCALE(0.4000, 0.4000)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fTextX, fTextY, "STRING", tlText)
ENDPROC

PROC Maintain_Vehicle_Mod_Widgets(structSharedDebugVars &paramVars)

	IF IS_PED_INJURED(PLAYER_PED_ID())
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableBasic = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetBasic = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		paramVars.selectedVehicleModDisplayCurrentMods = FALSE
		EXIT
	ENDIF
	VEHICLE_INDEX vehID 
	
	IF !paramVars.selectedVehicleModDisplayCurrentMods
		vehID = GET_PLAYERS_LAST_VEHICLE()
	ELSE
		IF DOES_ENTITY_EXIST(GET_FOCUS_ENTITY_INDEX())
			vehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX())
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehID)
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableBasic = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetBasic = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehID)
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableBasic = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetBasic = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		EXIT
	ENDIF
	
	IF GET_NUM_MOD_KITS(vehID) = 0
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		//EXIT
	ENDIF
	
	
	// DISPLAY MODS
	IF paramVars.vehicleModDisplayCurrentMods
	
		IF GET_VEHICLE_MOD_KIT(vehID) != 0
			SET_VEHICLE_MOD_KIT(vehID, 0)
		ENDIF
		
		FLOAT fLineOffset = GET_RENDERED_CHARACTER_HEIGHT(0.4)
		FLOAT fTextX = 0.825+0.005
		FLOAT fTextY = 0.200+0.005
		
		Display_Vehicle_Mod_Debug(vehID, MOD_SPOILER, FALSE, "Spoiler", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BUMPER_F, FALSE, "Front Bumper", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BUMPER_R, FALSE, "Rear Bumper", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_SKIRT, FALSE, "Skirt", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_EXHAUST, FALSE, "Exhaust", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_CHASSIS, FALSE, "Chassis", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_GRILL, FALSE, "Grill", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BONNET, FALSE, "Bonnet", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_WING_L, FALSE, "Left Wing", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_WING_R, FALSE, "Right Wing", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_ROOF, FALSE, "Roof", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_ENGINE, FALSE, "Engine", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BRAKES, FALSE, "Brakes", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_GEARBOX, FALSE, "Gearbox", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_HORN, FALSE, "Horn", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_SUSPENSION, FALSE, "Suspension", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_ARMOUR, FALSE, "Armour", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_WHEELS, FALSE, "Wheels", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_REAR_WHEELS, FALSE, "Rear Wheel", fTextX, fTextY) fTextY += fLineOffset+fLineOffset
		
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_NITROUS, TRUE, "Nitrous", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_TURBO, TRUE, "Turbo", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_SUBWOOFER, TRUE, "Sub Woofer", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_HYDRAULICS, TRUE, "Hydraulics", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_XENON_LIGHTS, TRUE, "Xenon Lights", fTextX, fTextY) fTextY += fLineOffset
		
		DRAW_RECT_FROM_CORNER(0.825, 0.200, 0.175, fTextY-0.200+0.005, 0, 0, 0, 255)
	ENDIF
	
	// UPDATE BASIC MODS
	IF paramVars.vehicleModEnableBasic
		INT i
		
		IF NOT paramVars.vehicleModDataSetBasic
		
			GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, paramVars.vehicleModTyreSmokeR, paramVars.vehicleModTyreSmokeG, paramVars.vehicleModTyreSmokeB)
			GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, paramVars.vehicleModTyreSmokeRTemp, paramVars.vehicleModTyreSmokeGTemp, paramVars.vehicleModTyreSmokeBTemp)
			
			IF NOT GET_VEHICLE_TYRES_CAN_BURST(vehID)
				paramVars.vehicleTyreTypeTemp = 2
				paramVars.vehicleTyreType = 2
			ELIF GET_DRIFT_TYRES_SET(vehID)
				paramVars.vehicleTyreTypeTemp = 1
				paramVars.vehicleTyreType = 1
			ELSE
				paramVars.vehicleTyreTypeTemp = 0
				paramVars.vehicleTyreType = 0
			ENDIF
			
			paramVars.vehicleLivery = GET_VEHICLE_LIVERY(vehID)
			paramVars.vehicleLiveryTemp = GET_VEHICLE_LIVERY(vehID)
			
			paramVars.vehicleLivery2 = GET_VEHICLE_LIVERY2(vehID)
			paramVars.vehicleLivery2Temp = GET_VEHICLE_LIVERY2(vehID)
			
			paramVars.vehicleModWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
			paramVars.vehicleModWindowTintColourTemp = GET_VEHICLE_WINDOW_TINT(vehID)
			
			REPEAT COUNT_OF(paramVars.vehicleModExtraOn) i
				paramVars.vehicleModExtraOn[i] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, i+1)
				paramVars.vehicleModExtraOnTemp[i] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, i+1)
			ENDREPEAT
			
			paramVars.XenonColorLightIndexTemp = GET_VEHICLE_XENON_LIGHT_COLOR_INDEX(vehID)
			
			paramVars.vehicleModDataSetBasic = TRUE
		ENDIF
		
		// Vehicle neon light
		IF paramVars.XenonColorLightIndexTemp != paramVars.XenonColorLightIndex
			SET_VEHICLE_XENON_LIGHT_COLOR_INDEX(vehID, paramVars.XenonColorLightIndex)
		ENDIF
		
		// Tyre smoke
		IF paramVars.vehicleModTyreSmokeRTemp != paramVars.vehicleModTyreSmokeR
		OR paramVars.vehicleModTyreSmokeGTemp != paramVars.vehicleModTyreSmokeG
		OR paramVars.vehicleModTyreSmokeBTemp != paramVars.vehicleModTyreSmokeB
			SET_VEHICLE_TYRE_SMOKE_COLOR(vehID, paramVars.vehicleModTyreSmokeR, paramVars.vehicleModTyreSmokeG, paramVars.vehicleModTyreSmokeB)
			paramVars.vehicleModTyreSmokeRTemp = paramVars.vehicleModTyreSmokeR
			paramVars.vehicleModTyreSmokeGTemp = paramVars.vehicleModTyreSmokeG
			paramVars.vehicleModTyreSmokeBTemp = paramVars.vehicleModTyreSmokeB
		ENDIF
		
		// Tyre type
		IF paramVars.vehicleTyreType != paramVars.vehicleTyreTypeTemp
			SWITCH paramVars.vehicleTyreType
				CASE 0
					SET_VEHICLE_TYRES_CAN_BURST(vehID, TRUE)
					SET_DRIFT_TYRES(vehID, FALSE)
				BREAK
				CASE 1
					SET_VEHICLE_TYRES_CAN_BURST(vehID, TRUE)
					SET_DRIFT_TYRES(vehID, TRUE)
				BREAK
				CASE 2
					SET_VEHICLE_TYRES_CAN_BURST(vehID, FALSE)
					SET_DRIFT_TYRES(vehID, FALSE)
				BREAK
			ENDSWITCH
			
			paramVars.vehicleTyreTypeTemp = paramVars.vehicleTyreType
		ENDIF
		
		// Window tint
		IF paramVars.vehicleModWindowTintColourTemp != paramVars.vehicleModWindowTintColour
			SET_VEHICLE_WINDOW_TINT(vehID, paramVars.vehicleModWindowTintColour)
			paramVars.vehicleModWindowTintColourTemp = paramVars.vehicleModWindowTintColour
		ENDIF
		
		// Livery
		IF paramVars.vehicleLiveryTemp != paramVars.vehicleLivery
			SET_VEHICLE_LIVERY(vehID, paramVars.vehicleLivery)
			paramVars.vehicleLiveryTemp = paramVars.vehicleLivery
		ENDIF
		
		IF paramVars.vehicleLivery2Temp != paramVars.vehicleLivery2
			SET_VEHICLE_LIVERY2(vehID, paramVars.vehicleLivery2)
			paramVars.vehicleLivery2Temp = paramVars.vehicleLivery2
		ENDIF
		
		// Extras
		REPEAT COUNT_OF(paramVars.vehicleModExtraOn) i
			IF paramVars.vehicleModExtraOnTemp[i] != paramVars.vehicleModExtraOn[i]
				SET_VEHICLE_EXTRA(vehID, i+1, !paramVars.vehicleModExtraOn[i])
				paramVars.vehicleModExtraOnTemp[i] = paramVars.vehicleModExtraOn[i]
			ENDIF
		ENDREPEAT
	ELSE
		paramVars.vehicleModDataSetBasic = FALSE
	ENDIF
	
	// UPDATE ADVANCED MODS
	IF paramVars.vehicleModEnableAdvanced
	
		IF GET_VEHICLE_MOD_KIT(vehID) != 0
			SET_VEHICLE_MOD_KIT(vehID, 0)
		ENDIF
		
		INT i, iMaxMods
		MOD_TYPE eMod
		BOOL bToggleMod
		
		IF NOT paramVars.vehicleModDataSetAdvanced
		
			REPEAT MAX_VEHICLE_MOD_SLOTS i
				eMod = INT_TO_ENUM(MOD_TYPE, i)
				IF eMod = MOD_TOGGLE_NITROUS
				OR eMod = MOD_TOGGLE_TURBO
				OR eMod = MOD_TOGGLE_SUBWOOFER
				OR eMod = MOD_TOGGLE_TYRE_SMOKE
				OR eMod = MOD_TOGGLE_HYDRAULICS
				OR eMod = MOD_TOGGLE_XENON_LIGHTS
					bToggleMod = TRUE
				ELSE
					bToggleMod = FALSE
				ENDIF
				
				IF bToggleMod
					IF IS_TOGGLE_MOD_ON(vehID, eMod)
						paramVars.vehicleModSlotIndex[eMod] = 1
					ELSE
						paramVars.vehicleModSlotIndex[eMod] = 0
					ENDIF
				ELSE
					paramVars.vehicleModSlotIndex[eMod] = GET_VEHICLE_MOD(vehID, eMod)
				ENDIF
			ENDREPEAT
			
			paramVars.vehicleModWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))
			
			paramVars.vehicleModDataSetAdvanced = TRUE
		ENDIF
		
		BOOL bVariation
		
		// Mods
		REPEAT MAX_VEHICLE_MOD_SLOTS i
			 bVariation = FALSE
			
			eMod = INT_TO_ENUM(MOD_TYPE, i)
			IF eMod = MOD_TOGGLE_NITROUS
			OR eMod = MOD_TOGGLE_TURBO
			OR eMod = MOD_TOGGLE_SUBWOOFER
			OR eMod = MOD_TOGGLE_TYRE_SMOKE
			OR eMod = MOD_TOGGLE_HYDRAULICS
			OR eMod = MOD_TOGGLE_XENON_LIGHTS
				bToggleMod = TRUE
			ELSE
				bToggleMod = FALSE
			ENDIF
			
			IF bToggleMod
				// Cap
				IF paramVars.vehicleModSlotIndex[eMod] < 0
					paramVars.vehicleModSlotIndex[eMod] = 0
				ELIF paramVars.vehicleModSlotIndex[eMod] > 1
					paramVars.vehicleModSlotIndex[eMod] = 1
				ENDIF
				
				// Turn off
				IF IS_TOGGLE_MOD_ON(vehID, eMod)
					IF paramVars.vehicleModSlotIndex[eMod] = 0
						PRINTLN("Toggling vehicle mod off")
						TOGGLE_VEHICLE_MOD(vehID, eMod, FALSE)
					ENDIF
				
				// Turn on
				ELSE
					IF paramVars.vehicleModSlotIndex[eMod] = 1
						PRINTLN("Toggling vehicle mod on")
						TOGGLE_VEHICLE_MOD(vehID, eMod, TRUE)
					ENDIF
				ENDIF
			ELSE
				// Cap
				iMaxMods = GET_NUM_VEHICLE_MODS(vehID, eMod)
				IF paramVars.vehicleModSlotIndex[eMod] > iMaxMods-1
					paramVars.vehicleModSlotIndex[eMod] = iMaxMods-1
				ENDIF
				
				// Update
				IF GET_VEHICLE_MOD(vehID, eMod) <> paramVars.vehicleModSlotIndex[eMod]
					IF paramVars.vehicleModSlotIndex[eMod] = -1
						PRINTLN("Removing vehicle mod")
						REMOVE_VEHICLE_MOD(vehID, eMod)
					ELSE
						IF GET_NUM_VEHICLE_MODS(vehID, eMod) > paramVars.vehicleModSlotIndex[eMod]
							PRINTLN("Setting vehicle mod")
							IF eMod = MOD_WHEELS
								IF paramVars.bTireDesign
									bVariation = TRUE
								ENDIF	
							ENDIF	
							
							IF IS_VEHICLE_MOD_LOCKED(vehID, eMod, paramVars.vehicleModSlotIndex[eMod])
								// skip
							ELSE
								SET_VEHICLE_MOD(vehID, eMod, paramVars.vehicleModSlotIndex[eMod], bVariation)
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		IF GET_VEHICLE_WHEEL_TYPE(vehID) != INT_TO_ENUM(MOD_WHEEL_TYPE, paramVars.vehicleModWheelType)
			PRINTLN("Setting wheel type")
			SET_VEHICLE_WHEEL_TYPE(vehID, INT_TO_ENUM(MOD_WHEEL_TYPE, paramVars.vehicleModWheelType))
		ENDIF
	ELSE
		paramVars.vehicleModDataSetAdvanced = FALSE
	ENDIF
	
ENDPROC

PROC Maintain_Vehicle_Performance_Widgets(structSharedDebugVars &paramVars)
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF NOT paramVars.bMaxSpeedOverride
	AND NOT paramVars.bTopSpeedOverride
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehID = GET_PLAYERS_LAST_VEHICLE()
	
	IF NOT DOES_ENTITY_EXIST(vehID)
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehID)
		EXIT
	ENDIF
	
	IF paramVars.bTopSpeedOverride
		MODIFY_VEHICLE_TOP_SPEED(vehID, paramVars.vehicleTopSpeedOverride)
	ENDIF
	
	IF paramVars.bMaxSpeedOverride
		SET_VEHICLE_MAX_SPEED(vehID, paramVars.vehicleMaxSpeedOverride)
	ENDIF
	
ENDPROC

// *****************************************************************************************
//	SCRIPTED CAM EDITOR
// *****************************************************************************************

/// PURPOSE:
///    Creates a widget to launch the cam editor script.
PROC Create_Scripted_Cam_Editor_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Scripted Cam Editor")
		ADD_WIDGET_BOOL("Launch", paramVars.launchCamEditor)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Scripted_Cam_Editor_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.launchCamEditor
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("scripted_cam_editor")) = 0
			REQUEST_SCRIPT("scripted_cam_editor")
			
			IF HAS_SCRIPT_LOADED("scripted_cam_editor")
				START_NEW_SCRIPT("scripted_cam_editor", FRIEND_STACK_SIZE)
				paramVars.launchCamEditor = FALSE
			ENDIF
		ELSE
			paramVars.launchCamEditor = FALSE
		ENDIF
	ENDIF 
ENDPROC


// *****************************************************************************************
//	PTFX TESTER
// *****************************************************************************************
PROC Create_Ptfx_Tester_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Ptfx Tester")
		ADD_WIDGET_BOOL("Enable", paramVars.ptfxTesterEnabled)

		paramVars.ptfxDictionary = ADD_TEXT_WIDGET("Ptfx dictionary")
		paramVars.ptfxName = ADD_TEXT_WIDGET("Ptfx name")
		
		ADD_WIDGET_BOOL("Attach ptfx to focus entity", paramVars.ptfxIsRelativeToEntity)
		ADD_WIDGET_VECTOR_SLIDER("Attach offset", paramVars.ptfxPos, -9999.0, 9999.0, 0.001)
		ADD_WIDGET_VECTOR_SLIDER("Rotation", paramVars.ptfxRot, -180.0, 180.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Scale", paramVars.ptfxScale, 0.001, 100.0, 0.01)
		ADD_WIDGET_BOOL("Play/stop looped", paramVars.playPtfxLooped)
		ADD_WIDGET_BOOL("Play non-looped", paramVars.playPtfxNonLooped)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Ptfx_Tester_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.ptfxTesterEnabled
		IF paramVars.playPtfxNonLooped
		OR paramVars.playPtfxLooped
			BOOL bAssetsLoaded = FALSE
			
			STRING strPtfxDict = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.ptfxDictionary)
			STRING strPtfxName = GET_CONTENTS_OF_TEXT_WIDGET(paramVars.ptfxName)
			
			IF ARE_STRINGS_EQUAL(strPtfxDict, "script")
				REQUEST_PTFX_ASSET()
				
				IF HAS_PTFX_ASSET_LOADED()
					bAssetsLoaded = TRUE
				ENDIF
			ELSE
				bAssetsLoaded = TRUE
			ENDIF
			
			IF bAssetsLoaded
				IF paramVars.ptfxIsRelativeToEntity
					ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
				
					IF NOT IS_ENTITY_DEAD(entityFocus)
						IF paramVars.playPtfxNonLooped
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(strPtfxName, entityFocus, paramVars.ptfxPos, paramVars.ptfxRot, paramVars.ptfxScale)
						ENDIF
						
						IF paramVars.playPtfxLooped
							IF paramVars.ptfxID = NULL
								paramVars.ptfxID = START_PARTICLE_FX_LOOPED_ON_ENTITY(strPtfxName, entityFocus, paramVars.ptfxPos, paramVars.ptfxRot, paramVars.ptfxScale)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF paramVars.playPtfxNonLooped
						START_PARTICLE_FX_NON_LOOPED_AT_COORD(strPtfxName, paramVars.ptfxPos, paramVars.ptfxRot, paramVars.ptfxScale)
					ENDIF
					
					IF paramVars.playPtfxLooped
						IF paramVars.ptfxID = NULL
							paramVars.ptfxID = START_PARTICLE_FX_LOOPED_AT_COORD(strPtfxName, paramVars.ptfxPos, paramVars.ptfxRot, paramVars.ptfxScale)
						ENDIF
					ENDIF
				ENDIF
				
				paramVars.playPtfxNonLooped = FALSE
			ENDIF
		ENDIF
		
		IF paramVars.ptfxID != NULL
			IF paramVars.playPtfxLooped
				//NOTE: Real-time updates to looping particle effects could go here.
			ELSE
				STOP_PARTICLE_FX_LOOPED(paramVars.ptfxID)
				paramVars.ptfxID = NULL
			ENDIF
		ENDIF
	ENDIF 
ENDPROC

// *****************************************************************************************
//	OBJECT POSITIONING
// *****************************************************************************************

/// PURPOSE:
///    Creates a widget to launch the cam editor script.
PROC Create_Object_Positioning_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Entity Positioning")
		ADD_WIDGET_STRING("When enabled, click on an entity to start positioning. Drag the entity using the axis markers.")
		ADD_WIDGET_BOOL("Enable widgets", paramVars.objPositioningEnabled)

		ADD_WIDGET_VECTOR_SLIDER("Position", paramVars.objPos, -10000.0, 10000.0, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("Rotation", paramVars.objRot, -180.0, 180.0, 0.1)
		ADD_WIDGET_BOOL("Show local position axis", paramVars.objPositioningUseLocalAxis)
		ADD_WIDGET_BOOL("Output to debug", paramVars.objPositioningOutput)
		ADD_WIDGET_BOOL("Freeze entity when repositioning", paramVars.objPositioningFreeze)
		
		ADD_WIDGET_FLOAT_READ_ONLY("Quat X", paramVars.objQuatX)
		ADD_WIDGET_FLOAT_READ_ONLY("Quat Y", paramVars.objQuatY)
		ADD_WIDGET_FLOAT_READ_ONLY("Quat Z", paramVars.objQuatZ)
		ADD_WIDGET_FLOAT_READ_ONLY("Quat W", paramVars.objQuatW)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Object_Positioning_Widgets(structSharedDebugVars &paramVars)
	BOOL bJustDeselectedAxis = FALSE

	IF paramVars.objPositioningEnabled
		IF NOT IS_ENTITY_DEAD(paramVars.objFocusedEntity)
			//Draw the axes.
			VECTOR vXOffset, vYOffset, vZOffset
			VECTOR vCentre = GET_ENTITY_COORDS(paramVars.objFocusedEntity)
			VECTOR vMouse2DPos, vXOffset2D, vYOffset2D, vZOffset2D, vCentre2D
			FLOAT fDistToXOffset, fDistToYOffset, fDistToZOffset, fClosestDist

			IF paramVars.objPositioningUseLocalAxis
				vXOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.objFocusedEntity, <<-1.0, 0.0, 0.0>>)
				vYOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.objFocusedEntity, <<0.0, -1.0, 0.0>>)
				vZOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.objFocusedEntity, <<0.0, 0.0, 1.0>>)
			ELSE
				vXOffset = vCentre + <<-1.0, 0.0, 0.0>>
				vYOffset = vCentre + <<0.0, -1.0, 0.0>>
				vZOffset = vCentre + <<0.0, 0.0, 1.0>>
			ENDIF
			
			GET_MOUSE_POSITION(vMouse2DPos.x, vMouse2DPos.y)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vXOffset, vXOffset2D.x, vXOffset2D.y)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vYOffset, vYOffset2D.x, vYOffset2D.y)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vZOffset, vZOffset2D.x, vZOffset2D.y)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vCentre, vCentre2D.x, vCentre2D.y)
			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			IF paramVars.objPositioningAxisSelected AND paramVars.iObjPositioningCurrentAxis = 0
				DRAW_DEBUG_LINE_2D(vCentre2D, vXOffset2D, 255, 255, 255, 200)
				DRAW_RECT(vXOffset2D.x, vXOffset2D.y, 0.0125, 0.02, 255, 255, 255, 200)
			ELSE
				DRAW_DEBUG_LINE_2D(vCentre2D, vXOffset2D, 255, 0, 0, 200)
				DRAW_RECT(vXOffset2D.x, vXOffset2D.y, 0.0125, 0.02, 255, 0, 0, 200)
			ENDIF
			
			IF paramVars.objPositioningAxisSelected AND paramVars.iObjPositioningCurrentAxis = 1
				DRAW_DEBUG_LINE_2D(vCentre2D, vYOffset2D, 255, 255, 255, 200)
				DRAW_RECT(vYOffset2D.x, vYOffset2D.y, 0.0125, 0.02, 255, 255, 255, 200)
			ELSE
				DRAW_DEBUG_LINE_2D(vCentre2D, vYOffset2D, 0, 255, 0, 200)
				DRAW_RECT(vYOffset2D.x, vYOffset2D.y, 0.0125, 0.02, 0, 255, 0, 200)
			ENDIF
			
			IF paramVars.objPositioningAxisSelected AND paramVars.iObjPositioningCurrentAxis = 2
				DRAW_DEBUG_LINE_2D(vCentre2D, vZOffset2D, 255, 255, 255, 200)
				DRAW_RECT(vZOffset2D.x, vZOffset2D.y, 0.0125, 0.02, 255, 255, 255, 200)
			ELSE
				DRAW_DEBUG_LINE_2D(vCentre2D, vZOffset2D, 0, 0, 255, 200)
				DRAW_RECT(vZOffset2D.x, vZOffset2D.y, 0.0125, 0.02, 0, 0, 255, 200)
			ENDIF
			
			//Update widget values if the player selects an axis and drags.
			IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
				fDistToXOffset = VDIST(vMouse2DPos, vXOffset2D)
				fDistToYOffset = VDIST(vMouse2DPos, vYOffset2D)
				fDistToZOffset = VDIST(vMouse2DPos, vZOffset2D)
			
				IF NOT paramVars.objPositioningAxisSelected
					//Get the closest axis to the mouse, if it's close enough then select it.
					IF fDistToXOffset < fDistToYOffset
						IF fDistToXOffset < fDistToZOffset
							paramVars.iObjPositioningCurrentAxis = 0
							fClosestDist = fDistToXOffset
						ELSE
							paramVars.iObjPositioningCurrentAxis = 2
							fClosestDist = fDistToZOffset
						ENDIF
					ELIF fDistToYOffset < fDistToZOffset
						paramVars.iObjPositioningCurrentAxis = 1
						fClosestDist = fDistToYOffset
					ELSE
						paramVars.iObjPositioningCurrentAxis = 2
						fClosestDist = fDistToZOffset
					ENDIF
					
					IF fClosestDist < 0.02
						paramVars.objPositioningAxisSelected = TRUE
					ENDIF
				ELSE
					FLOAT fMoveSpeed = 0.0
					FLOAT fDistFromXToCentre = VDIST(vXOffset2D, vCentre2D)
					FLOAT fDistFromYToCentre = VDIST(vYOffset2D, vCentre2D)
					FLOAT fDistFromZToCentre = VDIST(vZOffset2D, vCentre2D)
					
					//Compare the difference between the mouse position and the axis.
					IF paramVars.iObjPositioningCurrentAxis = 0
						//Using the length of the axis line in 2D space we can work out how much the object needs to move in 3D space to match the new mouse position.
						VECTOR vDesiredXOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vCentre2D, vXOffset2D, FALSE)
						FLOAT fDistFromDesiredXOffsetToXOffset = VDIST(vDesiredXOffset, vXOffset2D)
						fMoveSpeed = fDistFromDesiredXOffsetToXOffset / fDistFromXToCentre
						
						IF DOT_PRODUCT(vDesiredXOffset - vXOffset2D, vXOffset2D - vCentre2D) > 0.0
							fMoveSpeed = -fMoveSpeed
						ENDIF
						
						IF paramVars.objPositioningUseLocalAxis
							paramVars.objPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.objFocusedEntity, <<fMoveSpeed, 0.0, 0.0>>)
						ELSE
							paramVars.objPos.x += fMoveSpeed
						ENDIF
					ELIF paramVars.iObjPositioningCurrentAxis = 1
						VECTOR vDesiredYOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vCentre2D, vYOffset2D, FALSE)
						FLOAT fDistFromDesiredYOffsetToYOffset = VDIST(vDesiredYOffset, vYOffset2D)
						fMoveSpeed = fDistFromDesiredYOffsetToYOffset / fDistFromYToCentre
						
						IF DOT_PRODUCT(vDesiredYOffset - vYOffset2D, vYOffset2D - vCentre2D) > 0.0
							fMoveSpeed = -fMoveSpeed
						ENDIF
						
						IF paramVars.objPositioningUseLocalAxis
							paramVars.objPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.objFocusedEntity, <<0.0, fMoveSpeed, 0.0>>)
						ELSE
							paramVars.objPos.y += fMoveSpeed
						ENDIF
					ELSE
						VECTOR vDesiredZOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vCentre2D, vZOffset2D, FALSE)
						FLOAT fDistFromDesiredZOffsetToZOffset = VDIST(vDesiredZOffset, vZOffset2D)
						fMoveSpeed = fDistFromDesiredZOffsetToZOffset / fDistFromZToCentre
						
						IF DOT_PRODUCT(vDesiredZOffset - vZOffset2D, vZOffset2D - vCentre2D) < 0.0
							fMoveSpeed = -fMoveSpeed
						ENDIF
						
						IF paramVars.objPositioningUseLocalAxis
							paramVars.objPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.objFocusedEntity, <<0.0, 0.0, fMoveSpeed>>)
						ELSE
							paramVars.objPos.z += fMoveSpeed
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF paramVars.objPositioningAxisSelected
					bJustDeselectedAxis = TRUE
				ENDIF
			
				paramVars.objPositioningAxisSelected = FALSE
				paramVars.iObjPositioningCurrentAxis = 0
			ENDIF
			
			//Update position/rotation based on widget values.
			FREEZE_ENTITY_POSITION(paramVars.objFocusedEntity, paramVars.objPositioningFreeze)
			SET_ENTITY_COORDS_NO_OFFSET(paramVars.objFocusedEntity, paramVars.objPos)
			SET_ENTITY_ROTATION(paramVars.objFocusedEntity, paramVars.objRot)
			
			GET_ENTITY_QUATERNION(paramVars.objFocusedEntity, paramVars.objQuatX, paramVars.objQuatY, paramVars.objQuatZ, paramVars.objQuatW)
		ENDIF
		
		//If we're not dragging an object around then allow selection of other objects.
		//NOTE: this is done after the object positioning section so that axis selection takes priority over object selection.
		IF NOT paramVars.objPositioningAxisSelected
		AND NOT bJustDeselectedAxis
			IF IS_MOUSE_BUTTON_JUST_RELEASED(MB_LEFT_BTN)
				ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
				
				IF NOT IS_ENTITY_DEAD(entityFocus)
					IF entityFocus != paramVars.objFocusedEntity
						paramVars.objFocusedEntity = entityFocus
						
						paramVars.objPos = GET_ENTITY_COORDS(entityFocus)
						paramVars.objRot = GET_ENTITY_ROTATION(entityFocus)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF paramVars.objPositioningOutput
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS_NO_OFFSET(entity, ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.objPos)
			SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_ROTATION(entity, ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.objRot)
			SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(entity, ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.objQuatX)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.objQuatY)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.objQuatZ)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.objQuatW)
			SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
						
			paramVars.objPositioningOutput = FALSE
		ENDIF
	ENDIF 
ENDPROC


// *****************************************************************************************
//	MIN/MAX LOCATE
// *****************************************************************************************

PROC Create_MinMax_Locate_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Min/Max Locates")
		ADD_WIDGET_STRING("When enabled, drag the min/max corners to change the locate size.")
		ADD_WIDGET_BOOL("Enable widget", paramVars.bMinMaxWidgetEnabled)
		ADD_WIDGET_BOOL("Output to debug", paramVars.bMinMaxWidgetOutput)

		ADD_WIDGET_BOOL("Grab min pos from camera", paramVars.bMinMaxGetMin)
		ADD_WIDGET_VECTOR_SLIDER("Min", paramVars.vMinMaxCornerMin, -10000.0, 10000.0, 0.01)
		ADD_WIDGET_BOOL("Grab max pos from camera", paramVars.bMinMaxGetMax)
		ADD_WIDGET_VECTOR_SLIDER("Max", paramVars.vMinMaxCornerMax, -10000.0, 10000.0, 0.01)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_MinMax_Locate_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.bMinMaxWidgetEnabled
		//Stick some initial values in around the player.
		IF paramVars.vMinMaxCornerMin.x = 0.0
		AND paramVars.vMinMaxCornerMin.y = 0.0
		AND paramVars.vMinMaxCornerMin.z = 0.0
		AND paramVars.vMinMaxCornerMax.x = 0.0
		AND paramVars.vMinMaxCornerMax.y = 0.0
		AND paramVars.vMinMaxCornerMax.z = 0.0
			paramVars.vMinMaxCornerMin = GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-2.0, -2.0, 0.0>>
			paramVars.vMinMaxCornerMax = GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<2.0, 2.0, 2.0>>
		ENDIF
	
		IF paramVars.bMinMaxGetMin
			paramVars.vMinMaxCornerMin = GET_FINAL_RENDERED_CAM_COORD()
			paramVars.bMinMaxGetMin = FALSE
		ENDIF
		
		IF paramVars.bMinMaxGetMax
			paramVars.vMinMaxCornerMax = GET_FINAL_RENDERED_CAM_COORD()
			paramVars.bMinMaxGetMax = FALSE
		ENDIF
		
		//allow dragging the min and max nodes.
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
			VECTOR vMouse2DPos, vMin2DPos, vMax2DPos, vXoffset2D, vYOffset2D
			FLOAT fDistToMin, fDistToMax, fXMoveSpeed, fYMoveSpeed
		
			GET_MOUSE_POSITION(vMouse2DPos.x, vMouse2DPos.y)
			GET_SCREEN_COORD_FROM_WORLD_COORD(paramVars.vMinMaxCornerMin, vMin2DPos.x, vMin2DPos.y)
			GET_SCREEN_COORD_FROM_WORLD_COORD(paramVars.vMinMaxCornerMax, vMax2DPos.x, vMax2DPos.y)
				
			IF paramVars.iMinMaxCurrentSelection = -1
				fDistToMin = VDIST(vMouse2DPos, vMin2DPos)
				fDistToMax = VDIST(vMouse2DPos, vMax2DPos)
				
				IF fDistToMin < fDistToMax
					IF fDistToMin < 0.02
						paramVars.iMinMaxCurrentSelection = 0
					ENDIF
				ELSE
					IF fDistToMax < 0.02
						paramVars.iMinMaxCurrentSelection = 1
					ENDIF
				ENDIF
			ELIF paramVars.iMinMaxCurrentSelection = 0
				VECTOR vXOffset = paramVars.vMinMaxCornerMin + <<1.0, 0.0, 0.0>>
				VECTOR vYOffset = paramVars.vMinMaxCornerMin + <<0.0, 1.0, 0.0>>
				GET_SCREEN_COORD_FROM_WORLD_COORD(vXOffset, vXoffset2D.x, vXoffset2D.y)
				GET_SCREEN_COORD_FROM_WORLD_COORD(vYOffset, vYoffset2D.x, vYoffset2D.y)
			
				VECTOR vDesiredXOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vMin2DPos, vXOffset2D, FALSE)
				FLOAT fDistFromDesiredXOffsetToCentre = VDIST(vDesiredXOffset, vMin2DPos)
				FLOAT fDistFromMaxXToCentre = VDIST(vXOffset2D, vMin2DPos)
				fXMoveSpeed = fDistFromDesiredXOffsetToCentre / fDistFromMaxXToCentre
				
				IF DOT_PRODUCT(vDesiredXOffset - vMin2DPos, vXOffset2D - vMin2DPos) < 0.0
					fXMoveSpeed = -fXMoveSpeed
				ENDIF
				
				VECTOR vDesiredYOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vMin2DPos, vYOffset2D, FALSE)
				FLOAT fDistFromDesiredYOffsetToCentre = VDIST(vDesiredYOffset, vMin2DPos)
				FLOAT fDistFromMaxYToCentre = VDIST(vYOffset2D, vMin2DPos)
				fYMoveSpeed = fDistFromDesiredYOffsetToCentre / fDistFromMaxYToCentre
				
				IF DOT_PRODUCT(vDesiredYOffset - vMin2DPos, vYOffset2D - vMin2DPos) < 0.0
					fYMoveSpeed = -fYMoveSpeed
				ENDIF

				paramVars.vMinMaxCornerMin.x += fXMoveSpeed
				paramVars.vMinMaxCornerMin.y += fYMoveSpeed
			ELSE
				VECTOR vXOffset = paramVars.vMinMaxCornerMax + <<1.0, 0.0, 0.0>>
				VECTOR vYOffset = paramVars.vMinMaxCornerMax + <<0.0, 1.0, 0.0>>
				GET_SCREEN_COORD_FROM_WORLD_COORD(vXOffset, vXoffset2D.x, vXoffset2D.y)
				GET_SCREEN_COORD_FROM_WORLD_COORD(vYOffset, vYoffset2D.x, vYoffset2D.y)
			
				VECTOR vDesiredXOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vMax2DPos, vXOffset2D, FALSE)
				FLOAT fDistFromDesiredXOffsetToCentre = VDIST(vDesiredXOffset, vMax2DPos)
				FLOAT fDistFromMaxXToCentre = VDIST(vXOffset2D, vMax2DPos)
				fXMoveSpeed = fDistFromDesiredXOffsetToCentre / fDistFromMaxXToCentre
				
				IF DOT_PRODUCT(vDesiredXOffset - vMax2DPos, vXOffset2D - vMax2DPos) < 0.0
					fXMoveSpeed = -fXMoveSpeed
				ENDIF
				
				VECTOR vDesiredYOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vMax2DPos, vYOffset2D, FALSE)
				FLOAT fDistFromDesiredYOffsetToCentre = VDIST(vDesiredYOffset, vMax2DPos)
				FLOAT fDistFromMaxYToCentre = VDIST(vYOffset2D, vMax2DPos)
				fYMoveSpeed = fDistFromDesiredYOffsetToCentre / fDistFromMaxYToCentre
				
				IF DOT_PRODUCT(vDesiredYOffset - vMax2DPos, vYOffset2D - vMax2DPos) < 0.0
					fYMoveSpeed = -fYMoveSpeed
				ENDIF

				paramVars.vMinMaxCornerMax.x += fXMoveSpeed
				paramVars.vMinMaxCornerMax.y += fYMoveSpeed
			ENDIF
		ELSE
			paramVars.iMinMaxCurrentSelection = -1
		ENDIF
		
		IF paramVars.bMinMaxWidgetOutput
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("vMin = ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vMinMaxCornerMin)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("vMax = ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vMinMaxCornerMax)
			SAVE_NEWLINE_TO_DEBUG_FILE()
						
			paramVars.bMinMaxWidgetOutput = FALSE
		ENDIF
		
		//Draw a rectangle to visualise the locate.
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		IF paramVars.iMinMaxCurrentSelection = 0
			DRAW_DEBUG_SPHERE(paramVars.vMinMaxCornerMin, 0.2, 255, 255, 255)
		ELSE
			DRAW_DEBUG_SPHERE(paramVars.vMinMaxCornerMin, 0.2, 255, 0, 0)
		ENDIF
		
		IF paramVars.iMinMaxCurrentSelection = 1
			DRAW_DEBUG_SPHERE(paramVars.vMinMaxCornerMax, 0.2, 255, 255, 255)
		ELSE
			DRAW_DEBUG_SPHERE(paramVars.vMinMaxCornerMax, 0.2, 0, 255, 0)
		ENDIF
		
		DRAW_DEBUG_LINE(paramVars.vMinMaxCornerMin, <<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMin.z>>)
		DRAW_DEBUG_LINE(paramVars.vMinMaxCornerMin, <<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMin.z>>)
		DRAW_DEBUG_LINE(paramVars.vMinMaxCornerMin, <<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMax.z>>)
		
		DRAW_DEBUG_LINE(paramVars.vMinMaxCornerMax, <<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMin.z>>)
		DRAW_DEBUG_LINE(paramVars.vMinMaxCornerMax, <<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMax.z>>)
		DRAW_DEBUG_LINE(paramVars.vMinMaxCornerMax, <<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMax.z>>)
		
		DRAW_DEBUG_LINE(<<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMin.z>>, 
					    <<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMax.z>>)
		DRAW_DEBUG_LINE(<<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMin.z>>, 
					    <<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMax.z>>)
						
		DRAW_DEBUG_LINE(<<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMax.z>>, 
					    <<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMax.z>>)
		DRAW_DEBUG_LINE(<<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMax.z>>, 
					    <<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMax.z>>)
						
		DRAW_DEBUG_LINE(<<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMin.z>>, 
					    <<paramVars.vMinMaxCornerMin.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMin.z>>)
		DRAW_DEBUG_LINE(<<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMax.y, paramVars.vMinMaxCornerMin.z>>, 
					    <<paramVars.vMinMaxCornerMax.x, paramVars.vMinMaxCornerMin.y, paramVars.vMinMaxCornerMin.z>>)
	ENDIF 
ENDPROC

// *****************************************************************************************
//	DOOR OPENING ROTATION
// *****************************************************************************************

PROC Create_Door_Opening_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Door Opening")
		ADD_WIDGET_BOOL("Enable widget", paramVars.bDoorOpeningEnabled)
		ADD_WIDGET_STRING("Click on a door to change its open ratio.")
		ADD_WIDGET_FLOAT_SLIDER("Door open ratio: ", paramVars.fOpenRatio, -2.5, 2.5, 0.01)
		ADD_WIDGET_BOOL("Doors locked", paramVars.bDoorsLocked)
		ADD_WIDGET_BOOL("Output to RAG output log", paramVars.bDoorOpeningOutputLog)
		ADD_WIDGET_BOOL("Output to debug file", paramVars.bDoorOpeningOutputFile)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Door_Opening_Widgets(structSharedDebugVars &paramVars)

	IF paramVars.bDoorOpeningEnabled

		ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
		
		IF NOT IS_ENTITY_DEAD(entityFocus)
			//If the current focused entity differs from the one stored, update the widgets to match the new entity.
			IF entityFocus != paramVars.objFocusedDoor
				paramVars.objFocusedDoor = entityFocus
				
				paramVars.vDoorPosition = GET_ENTITY_COORDS(entityFocus)
				paramVars.mnDoorModel = GET_ENTITY_MODEL(entityFocus)
			ENDIF
			
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(paramVars.mnDoorModel, paramVars.vDoorPosition, paramVars.bDoorsLocked, paramVars.fOpenRatio)
			
		ENDIF

		IF paramVars.bDoorOpeningOutputFile
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(")
			SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(paramVars.mnDoorModel))
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vDoorPosition)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_BOOL_TO_DEBUG_FILE(paramVars.bDoorsLocked)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.fOpenRatio)
			SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			paramVars.bDoorOpeningOutputFile = FALSE
		ENDIF
		
		IF paramVars.bDoorOpeningOutputLog
			
			PRINTSTRING("SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(")
			PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(paramVars.mnDoorModel))
			PRINTSTRING(", ")
			PRINTVECTOR(paramVars.vDoorPosition)
			PRINTSTRING(", ")
			PRINTBOOL(paramVars.bDoorsLocked)
			PRINTSTRING(", ")
			PRINTFLOAT(paramVars.fOpenRatio)
			PRINTSTRING(")")
			PRINTNL()
			
			paramVars.bDoorOpeningOutputLog = FALSE
		ENDIF
		
	ENDIF
ENDPROC


// *****************************************************************************************
//	RADAR MAP AREA COLOURS
// *****************************************************************************************

// PURPOSE:	Add a widget that allows the area colours to be displayed on the Radar
PROC Create_Radar_Map_Colours_Widgets(structSharedDebugVars &paramVars)
	ADD_WIDGET_BOOL("Toggle Radar Area Colours (tick to display)", paramVars.toggleRadarAreaColours)
ENDPROC


// *****************************************************************************************
//	GET CAM OFFSET FROM ENTITY
// *****************************************************************************************

/// PURPOSE:
///    Creates a widget to launch the cam editor script.
PROC Create_Cam_Offset_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Get Cam Offset From Entity")
		ADD_WIDGET_STRING("When enabled, click on an entity, then move the game cam into position.")
		
		ADD_WIDGET_BOOL("Enable widgets", paramVars.bCamOffsetEnabled)
		ADD_WIDGET_BOOL("Offset relative to entity", paramVars.bCamOffsetRelativePosition)
		ADD_WIDGET_BOOL("Lookat relative to entity", paramVars.bCamOffsetRelativeLookAt)
		ADD_WIDGET_BOOL("Output to temp_debug", paramVars.bCamOffsetActivate)
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Cam_Offset_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.bCamOffsetEnabled
		ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
			
		IF NOT IS_ENTITY_DEAD(entityFocus)
			//If the current focused entity differs from the one stored, update the widgets to match the new entity.
			IF entityFocus != paramVars.objFocusedEntity
				paramVars.objFocusedEntity = entityFocus
			ENDIF
		ENDIF
		
		IF paramVars.bCamOffsetActivate
			IF NOT IS_ENTITY_DEAD(paramVars.objFocusedEntity)
	            VECTOR v_cam_pos = GET_FINAL_RENDERED_CAM_COORD()
	            VECTOR v_cam_rot = GET_FINAL_RENDERED_CAM_ROT()             
	            VECTOR v_entity_rot = GET_ENTITY_ROTATION(paramVars.objFocusedEntity)
	            IF v_entity_rot.z > 180.0
	                v_entity_rot.z = -180.0 -(180.0 - v_entity_rot.z)
	            ENDIF
	            
	            VECTOR v_cam_dir = <<-SIN(v_cam_rot.z) * COS(v_cam_rot.x), COS(v_cam_rot.z) * COS(v_cam_rot.x), SIN(v_cam_rot.x)>>
	            VECTOR v_cam_front_pos = v_cam_pos + (v_cam_dir * 3.0)
	         
	            SAVE_NEWLINE_TO_DEBUG_FILE()
	            
	            SAVE_STRING_TO_DEBUG_FILE("ATTACH_CAM_TO_ENTITY(cam, entity, ")
				IF paramVars.bCamOffsetRelativePosition
	            	SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(paramVars.objFocusedEntity, v_cam_pos))
					SAVE_STRING_TO_DEBUG_FILE(", TRUE)")
				ELSE
					SAVE_VECTOR_TO_DEBUG_FILE( v_cam_pos - GET_ENTITY_COORDS(paramVars.objFocusedEntity) )
					SAVE_STRING_TO_DEBUG_FILE(", FALSE)")
				ENDIF

	            SAVE_NEWLINE_TO_DEBUG_FILE()
	            
	            SAVE_STRING_TO_DEBUG_FILE("POINT_CAM_AT_ENTITY(cam, entity, ")
				IF paramVars.bCamOffsetRelativeLookAt
	            	SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(paramVars.objFocusedEntity, v_cam_front_pos))
	            	SAVE_STRING_TO_DEBUG_FILE(", TRUE)")
				ELSE
					SAVE_VECTOR_TO_DEBUG_FILE( v_cam_front_pos - GET_ENTITY_COORDS(paramVars.objFocusedEntity) )
	            	SAVE_STRING_TO_DEBUG_FILE(", FALSE)")
				ENDIF
	            
	            SAVE_NEWLINE_TO_DEBUG_FILE()
	            
	            SAVE_STRING_TO_DEBUG_FILE("SET_CAM_FOV(cam, ")
	            SAVE_FLOAT_TO_DEBUG_FILE(GET_FINAL_RENDERED_CAM_FOV())
	            SAVE_STRING_TO_DEBUG_FILE(")")
	            
	            SAVE_NEWLINE_TO_DEBUG_FILE()
	            
	            //Source data
	            VECTOR v_entity_pos = GET_ENTITY_COORDS(paramVars.objFocusedEntity)
	            
	            SAVE_STRING_TO_DEBUG_FILE("Camera world coords: ")
	            SAVE_VECTOR_TO_DEBUG_FILE(v_cam_pos)
	            SAVE_STRING_TO_DEBUG_FILE("  Camera rotation: ")
	            SAVE_VECTOR_TO_DEBUG_FILE(v_cam_rot)
	            
	            SAVE_NEWLINE_TO_DEBUG_FILE()
	            
	            SAVE_STRING_TO_DEBUG_FILE("Entity world coords: ")
	            SAVE_VECTOR_TO_DEBUG_FILE(v_entity_pos)
	            SAVE_STRING_TO_DEBUG_FILE("  Entity rotation: ")
	            SAVE_VECTOR_TO_DEBUG_FILE(v_entity_rot)
	    
	            SAVE_NEWLINE_TO_DEBUG_FILE()
	         ENDIF   
			
			paramVars.bCamOffsetActivate = FALSE
		ENDIF
	ENDIF 
ENDPROC


// *****************************************************************************************
//	VEHICLE FORCES
// *****************************************************************************************
PROC Create_Vehicle_Force_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Entity forces")
		ADD_WIDGET_STRING("When enabled click on an entity, adjust the settings, then hold Square/X to apply the force.")
	
		ADD_WIDGET_BOOL("Enable widgets", paramVars.bEnableForceWidget)
		ADD_WIDGET_INT_SLIDER("Force type", paramVars.iForceApplyType, 0, 5, 1)
		ADD_WIDGET_VECTOR_SLIDER("Force direction", paramVars.vForceDirection, -50.0, 50.0, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("Force offset", paramVars.vForceOffset, -30.0, 30.0, 0.1)
		ADD_WIDGET_INT_SLIDER("Component", paramVars.iForceComponent, 0, 50, 1)
		ADD_WIDGET_BOOL("Local force", paramVars.bIsLocalForce)
		ADD_WIDGET_BOOL("Local offset", paramVars.bIsLocalOffset)
		ADD_WIDGET_BOOL("Scale by mass", paramVars.bScaleForceByMass)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Vehicle_Force_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.bEnableForceWidget
		ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
			
		IF NOT IS_ENTITY_DEAD(entityFocus)
			//If the current focused entity differs from the one stored, update the widgets to match the new entity.
			IF entityFocus != paramVars.entityToApplyTo
				paramVars.entityToApplyTo = entityFocus
			ENDIF
		ENDIF
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			APPLY_FORCE_TYPE eForceType
			
			IF paramVars.iForceApplyType = 0
				eForceType = APPLY_TYPE_FORCE
			ELIF  paramVars.iForceApplyType = 1
				eForceType = APPLY_TYPE_IMPULSE
			ELIF  paramVars.iForceApplyType = 2
				eForceType = APPLY_TYPE_EXTERNAL_FORCE
			ELIF  paramVars.iForceApplyType = 3
				eForceType = APPLY_TYPE_EXTERNAL_IMPULSE
			ELIF  paramVars.iForceApplyType = 4
				eForceType = APPLY_TYPE_TORQUE
			ELIF  paramVars.iForceApplyType = 5
				eForceType = APPLY_TYPE_ANGULAR_IMPULSE
			ENDIF
			
			APPLY_FORCE_TO_ENTITY(entityFocus, eForceType, paramVars.vForceDirection, paramVars.vForceOffset, paramVars.iForceComponent, 
								  paramVars.bIsLocalForce, paramVars.bIsLocalOffset, paramVars.bScaleForceByMass)
		ENDIF
	ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the widget tickbox has changed that allows the area colours to be displayed on the Radar
PROC Maintain_Radar_Map_Colours_Widgets(structSharedDebugVars &paramVars)

	// Check if the Radar Area Colours tickbox is ticked
	IF NOT (paramVars.toggleRadarAreaColours)
		EXIT
	ENDIF
	
	// Untick the toggle
	paramVars.toggleRadarAreaColours = FALSE
	
	PRINTSTRING("------> Radar Area Colours Toggle Detected") PRINTNL()
	
	// Tickbox is ticked, so switch it on or off
	IF (IS_BIT_SET(m_sharedDebugFlags, DBG_RADAR_AREA_COLOURS_ON))
		PRINTSTRING("           Radar Area Colours already on - about to clear the minimap component") PRINTNL()
	
		// It's already on, so switch it off
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_DEBUG_MAP_AREAS, FALSE)
		CLEAR_BIT(m_sharedDebugFlags, DBG_RADAR_AREA_COLOURS_ON)

		PRINTSTRING("           ...cleared") PRINTNL()
		EXIT
	ENDIF
	
	PRINTSTRING("           Radar Area Colours not on - about to set the minimap component") PRINTNL()
	
	// Area Colours are off, so switch them on
	IF NOT (SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_DEBUG_MAP_AREAS, TRUE))
		// ...component not valid
		EXIT
	ENDIF
	
	PRINTSTRING("           ...set") PRINTNL()
	
	// Set the 'colours on' bitflag
	SET_BIT(m_sharedDebugFlags, DBG_RADAR_AREA_COLOURS_ON)

ENDPROC

// *****************************************************************************************
//	CAR DAMAGE
// *****************************************************************************************

/// PURPOSE:
///    Add a widget that allows to mark areas on cars for tyre burst and damage
PROC Create_Car_Damage_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Car Damage")
		ADD_WIDGET_BOOL("Enable widget", paramVars.bCarTyreBurstsAndDamageEnabled)
		ADD_WIDGET_STRING("Click on a car to start editing tyre burst areas and damage.")
		ADD_WIDGET_BOOL("Draw debug spheres", paramVars.bDrawDebugSpheres)
		ADD_WIDGET_BOOL("Restore car's health", paramVars.bRetoreCarHealth)
		ADD_WIDGET_FLOAT_SLIDER("Damage", paramVars.fDamage, 0.0, 500.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Deformation", paramVars.fDeformation, 0.0, 500.0, 1.0)
		ADD_WIDGET_BOOL("Apply front left damage", paramVars.bApplyFrontLeftDamage)
		ADD_WIDGET_BOOL("Apply front right damage", paramVars.bApplyFrontRightDamage)
		ADD_WIDGET_BOOL("Apply rear left damage", paramVars.bApplyRearLeftDamage)
		ADD_WIDGET_BOOL("Apply rear right damage", paramVars.bApplyRearRightDamage)
		ADD_WIDGET_FLOAT_SLIDER("Sphere radius", paramVars.fSphereRadius, 0.1, 2.0, 0.1)
		START_WIDGET_GROUP("Front Left Wheel offset")
			ADD_WIDGET_VECTOR_SLIDER("Front Left wheel offset", paramVars.vFrontLeftWheelOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Front Right Wheel offset")
			ADD_WIDGET_VECTOR_SLIDER("Front Right wheel offset", paramVars.vFrontRightWheelOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Rear Left Wheel offset")
			ADD_WIDGET_VECTOR_SLIDER("Rear Left wheel offset", paramVars.vRearLeftWheelOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Rear Right Wheel offset")
			ADD_WIDGET_VECTOR_SLIDER("Rear Right wheel offset", paramVars.vRearRightWheelOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Front Left Damage offset")
			ADD_WIDGET_VECTOR_SLIDER("Front Left damage offset", paramVars.vFrontLeftDamageOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Front Right Damage offset")
			ADD_WIDGET_VECTOR_SLIDER("Front Right damage offset", paramVars.vFrontRightDamageOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Rear Left Damage offset")
			ADD_WIDGET_VECTOR_SLIDER("Rear Left damage offset", paramVars.vRearLeftDamageOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Rear Right Damage offset")
			ADD_WIDGET_VECTOR_SLIDER("Rear Right damage offset", paramVars.vRearRightDamageOffset, -5.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("Output to debug", paramVars.bCarTyreBurstsAndDamageOutput)
		
		paramVars.fSphereRadius 			= 0.2
		
		paramVars.vFrontLeftWheelOffset		= << -1.0, 1.5, -0.5 >>
		paramVars.vFrontRightWheelOffset	= << 1.0, 1.5, -0.5 >>
		paramVars.vRearLeftWheelOffset		= << -1.0, -1.5, -0.5 >>
		paramVars.vRearRightWheelOffset		= << 1.0, -1.5, -0.5 >>

		paramVars.vFrontLeftDamageOffset	= << -1.0, 1.0, 0.2 >>
		paramVars.vFrontRightDamageOffset	= << 1.0, 1.0, 0.2 >>
		paramVars.vRearLeftDamageOffset		= << -1.0, -1.0, 0.2 >>
		paramVars.vRearRightDamageOffset	= << 1.0, -1.0, 0.2 >>
		
		paramVars.fDamage					= 100.0
		paramVars.fDeformation				= 100.0
		
		paramVars.bDrawDebugSpheres			= TRUE
		
	STOP_WIDGET_GROUP()
ENDPROC


// ════════════════════════════════════╡ VEHICLE EXTRAS ╞══════════════════════════════════

/// PURPOSE:
///    Add a widget to easily modify a vehicles extras
PROC Create_Vehicle_Extra_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Vehicle Extras")
		ADD_WIDGET_BOOL("Enable widget", paramVars.bVehicleExtraEnabled)
		ADD_WIDGET_STRING("Click on a vehicle to start editing the vehicle extras.")
		ADD_WIDGET_INT_SLIDER("Extra Index", paramVars.iVehicleExtra, 0, 32, 1)
		ADD_WIDGET_BOOL("Extra Toggle", paramVars.bVehicleExtra)
		ADD_WIDGET_STRING("")
		ADD_WIDGET_BOOL("Debug Info", paramVars.bVehicleExtraDebug)
		ADD_WIDGET_BOOL("Output Info", paramVars.bVehicleExtraOutput)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Car_Damage_Widgets(structSharedDebugVars &paramVars)

	IF paramVars.bCarTyreBurstsAndDamageEnabled

		ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
		
		IF NOT IS_ENTITY_DEAD(entityFocus)
		AND IS_ENTITY_A_VEHICLE(entityFocus)
		
			VEHICLE_INDEX vehicleFocus = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityFocus)
			
			IF IS_VEHICLE_DRIVEABLE(vehicleFocus)
			
				//If the current focused entity differs from the one stored, update the widgets to match the new entity.
				IF vehicleFocus != paramVars.vehFocusedCar
					paramVars.vehFocusedCar = vehicleFocus
					paramVars.mnCarModel = GET_ENTITY_MODEL(entityFocus)
				ENDIF
				
				IF ( paramVars.bRetoreCarHealth = TRUE )
				
					SET_VEHICLE_ENGINE_HEALTH(paramVars.vehFocusedCar, 1000)
					SET_ENTITY_HEALTH(paramVars.vehFocusedCar, 1000)
					SET_VEHICLE_FIXED(paramVars.vehFocusedCar)
					
					paramVars.bRetoreCarHealth = FALSE
				
				ENDIF
				
				IF ( paramVars.bDrawDebugSpheres = TRUE )
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vFrontLeftWheelOffset), paramVars.fSphereRadius, 0, 0, 255, 128)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vFrontRightWheelOffset), paramVars.fSphereRadius, 0, 0, 255, 128)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vRearLeftWheelOffset), paramVars.fSphereRadius, 0, 0, 255, 128)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vRearRightWheelOffset), paramVars.fSphereRadius, 0, 0, 255, 128)
					
					DRAW_DEBUG_TEXT("FRONT LEFT WHEEL", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vFrontLeftWheelOffset))
					DRAW_DEBUG_TEXT("FRONT RIGHT WHEEL", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vFrontRightWheelOffset))
					DRAW_DEBUG_TEXT("REAR LEFT WHEEL", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vRearLeftWheelOffset))
					DRAW_DEBUG_TEXT("REAR RIGHT WHEEL", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vRearRightWheelOffset))
					
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar,  paramVars.vFrontLeftDamageOffset), 0.1, 255, 0, 0)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar,  paramVars.vFrontRightDamageOffset), 0.1, 255, 0, 0)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar,  paramVars.vRearLeftDamageOffset), 0.1, 255, 0, 0)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar,  paramVars.vRearRightDamageOffset), 0.1, 255, 0, 0)
					
					DRAW_DEBUG_TEXT("FRONT LEFT DAMAGE", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vFrontLeftDamageOffset), 255, 0, 0)
					DRAW_DEBUG_TEXT("FRONT RIGHT DAMAGE", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vFrontRightDamageOffset), 255, 0, 0)
					DRAW_DEBUG_TEXT("REAR LEFT DAMAGE", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vRearLeftDamageOffset), 255, 0, 0)
					DRAW_DEBUG_TEXT("REAR RIGHT DAMAGE", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehFocusedCar, paramVars.vRearRightDamageOffset), 255, 0, 0)
					
				ENDIF
				
				IF ( paramVars.bApplyFrontLeftDamage = TRUE )
					SET_VEHICLE_DAMAGE(paramVars.vehFocusedCar, paramVars.vFrontLeftDamageOffset, paramVars.fDamage, paramVars.fDeformation, TRUE)
					paramVars.bApplyFrontLeftDamage = FALSE
				ENDIF
				
				IF ( paramVars.bApplyFrontRightDamage = TRUE )
					SET_VEHICLE_DAMAGE(paramVars.vehFocusedCar, paramVars.vFrontRightDamageOffset, paramVars.fDamage, paramVars.fDeformation, TRUE)
					paramVars.bApplyFrontRightDamage = FALSE
				ENDIF
				
				IF ( paramVars.bApplyRearLeftDamage = TRUE )
					SET_VEHICLE_DAMAGE(paramVars.vehFocusedCar, paramVars.vRearLeftDamageOffset, paramVars.fDamage, paramVars.fDeformation, TRUE)
					paramVars.bApplyRearLeftDamage = FALSE
				ENDIF
				
				IF ( paramVars.bApplyRearRightDamage = TRUE )
					SET_VEHICLE_DAMAGE(paramVars.vehFocusedCar, paramVars.vRearRightDamageOffset, paramVars.fDamage, paramVars.fDeformation, TRUE)
					paramVars.bApplyRearRightDamage = FALSE
				ENDIF
			
			ELSE
			
				paramVars.bApplyFrontLeftDamage = FALSE
				paramVars.bApplyFrontRightDamage = FALSE
				paramVars.bApplyRearLeftDamage = FALSE
				paramVars.bApplyRearRightDamage = FALSE
			
			ENDIF
			
		ENDIF

		IF ( paramVars.bCarTyreBurstsAndDamageOutput = TRUE )
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("CAR MODEL: ")
			SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(paramVars.mnCarModel))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("FRONT LEFT WHEEL OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vFrontLeftWheelOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("FRONT RIGHT WHEEL OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vFrontRightWheelOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("REAR LEFT WHEEL OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vRearLeftWheelOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("REAR RIGHT WHEEL OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vRearRightWheelOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("FRONT LEFT DAMAGE OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vFrontLeftDamageOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("FRONT RIGHT DAMAGE OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vFrontRightDamageOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("REAR LEFT DAMAGE OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vRearLeftDamageOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("REAR RIGHT DAMAGE OFFSET: ")
			SAVE_VECTOR_TO_DEBUG_FILE(paramVars.vRearRightDamageOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("DAMAGE: ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.fDamage)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("DEFORMATION: ")
			SAVE_FLOAT_TO_DEBUG_FILE(paramVars.fDeformation)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			paramVars.bCarTyreBurstsAndDamageOutput = FALSE
			
		ENDIF
		
	ENDIF
ENDPROC


PROC Maintain_Vehicle_Extra_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.bVehicleExtraEnabled
		ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
		
		IF NOT IS_ENTITY_DEAD(entityFocus)
		AND IS_ENTITY_A_VEHICLE(entityFocus)
			VEHICLE_INDEX vehicleFocus = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityFocus)
			IF IS_VEHICLE_DRIVEABLE(vehicleFocus)
				//If the current focused entity differs from the one stored, update the widgets to match the new entity.
				IF vehicleFocus != paramVars.vehVehicleExtraFocus
					paramVars.vehVehicleExtraFocus = vehicleFocus
				ENDIF
				
				IF paramVars.iVehicleExtra != paramVars.iVehicleExtraLast
					paramVars.iVehicleExtraLast = paramVars.iVehicleExtra
					
					IF IS_VEHICLE_EXTRA_TURNED_ON(paramVars.vehVehicleExtraFocus, paramVars.iVehicleExtra)
						paramVars.bVehicleExtra = TRUE
					ELSE
						paramVars.bVehicleExtra = FALSE
					ENDIF
				ELSE
					IF paramVars.bVehicleExtra != IS_VEHICLE_EXTRA_TURNED_ON(paramVars.vehVehicleExtraFocus, paramVars.iVehicleExtra)
						SET_VEHICLE_EXTRA(paramVars.vehVehicleExtraFocus, paramVars.iVehicleExtra, paramVars.bVehicleExtra)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF paramVars.bVehicleExtraDebug = TRUE
			//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			TEXT_LABEL txtLabel
			
			txtLabel = "iVehicleExtra = "
			txtLabel += paramVars.iVehicleExtra
			
			DRAW_DEBUG_TEXT(txtLabel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehVehicleExtraFocus, <<0.0, 0.0, 0.5>>))
			
			txtLabel = "bVehicleExtra = "
			IF paramVars.bVehicleExtra
				txtLabel += "TRUE"
			ELSE
				txtLabel += "FALSE"
			ENDIF
			
			DRAW_DEBUG_TEXT(txtLabel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(paramVars.vehVehicleExtraFocus, <<0.0, 0.0, 0.25>>))
		ENDIF
		
		IF paramVars.bVehicleExtraOutput = TRUE
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("iVehicleExtra = ")
			SAVE_INT_TO_DEBUG_FILE(paramVars.iVehicleExtra)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("bVehicleExtra = ")
			SAVE_BOOL_TO_DEBUG_FILE(paramVars.bVehicleExtra)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			paramVars.bCarTyreBurstsAndDamageOutput = FALSE
		ENDIF
	ENDIF
ENDPROC


// *****************************************************************************************
//	SCRIPTED CAM EDITOR
// *****************************************************************************************

/// PURPOSE:
///    Creates a widget to launch the cam editor script.
PROC Create_Native_Trace_Widget(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Native Trace")
		paramVars.nativeTraceTextWidgetID = ADD_TEXT_WIDGET("Native Command")
		ADD_WIDGET_BOOL("Trace", paramVars.bNativeTraceInput)
		ADD_WIDGET_BOOL("Break on call", paramVars.bNativeTraceBreakInput)
		ADD_WIDGET_BOOL("Break on first call only", paramVars.bNativeTraceBreakFirstInput)
		
//		ADD_WIDGET_BOOL("bNativeTraceRunning", paramVars.bNativeTraceRunning)
//		ADD_WIDGET_BOOL("bNativeTraceBreakRunning", paramVars.bNativeTraceBreakRunning)
//		ADD_WIDGET_BOOL("bNativeTraceBreakFirstRunning", paramVars.bNativeTraceBreakFirstRunning)
		
	STOP_WIDGET_GROUP()
ENDPROC


PROC Create_Game_Time_Date_Widget(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Time/Date")
		ADD_WIDGET_BOOL("Draw Time/Date", paramVars.bDrawGameTimeDate)
		ADD_WIDGET_FLOAT_SLIDER("Current Time/Date X", paramVars.fCurrentTimeDateX, -1.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Current Time/Date Y", paramVars.fCurrentTimeDateY, -1.0, 1.0, 0.01)
		
		ADD_WIDGET_STRING("Set g_debugTOD variable to draw custom TIMEOFDAY value.")
		ADD_WIDGET_FLOAT_SLIDER("Debug Time/Date X", paramVars.fDebugTimeDateX, -1.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Debug Time/Date Y", paramVars.fDebugTimeDateY, -1.0, 1.0, 0.01)
		
		ADD_WIDGET_STRING(" ")
		ADD_WIDGET_STRING("Debug TOD calculations")
		ADD_WIDGET_DATE_SLIDER("TOD1",paramVars.fDebugDate1,0,3000,1)
		ADD_WIDGET_STRING(" ")
		
		ADD_WIDGET_DATE_SLIDER("TOD2",paramVars.fDebugDate2,0,3000,1)
		ADD_WIDGET_BOOL("Add TODs",paramVars.bAddTODs)
		ADD_WIDGET_BOOL("Set current TOD - uses TOD1",paramVars.bSetTOD)
		
		
		ADD_WIDGET_DATE_READ_ONLY("Test Result",paramVars.fDebugDateRes)


	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Native_Trace_Widgets(structSharedDebugVars &paramVars)
	IF paramVars.bNativeTraceInput
		
		// if the TRACE_NATIVE_COMMAND is running, the BREAK_ON_NATIVE_COMMAND can't be.
		IF paramVars.bNativeTraceBreakInput
			paramVars.bNativeTraceBreakInput = FALSE	
		ENDIF
		IF paramVars.bNativeTraceBreakRunning
			paramVars.bNativeTraceBreakRunning = FALSE	
		ENDIF
		IF paramVars.bNativeTraceBreakFirstInput
			paramVars.bNativeTraceBreakFirstInput = FALSE
		ENDIF
		IF paramVars.bNativeTraceBreakFirstRunning
			paramVars.bNativeTraceBreakFirstRunning = FALSE
		ENDIF
		
		// start the trace
		IF NOT paramVars.bNativeTraceRunning
			TRACE_NATIVE_COMMAND(GET_CONTENTS_OF_TEXT_WIDGET(paramVars.nativeTraceTextWidgetID))
			paramVars.bNativeTraceRunning = TRUE
		ENDIF
		
	ELSE
		IF paramVars.bNativeTraceRunning
			paramVars.bNativeTraceRunning = FALSE
		ENDIF	
	ENDIF
	
	IF paramVars.bNativeTraceBreakInput
	
		// if the BREAK_ON_NATIVE_COMMAND is running, the TRACE_NATIVE_COMMAND can't be.
		IF paramVars.bNativeTraceInput
			paramVars.bNativeTraceInput = FALSE
		ENDIF
		IF paramVars.bNativeTraceRunning
			paramVars.bNativeTraceRunning = FALSE
		ENDIF
		
		// start the break
		IF NOT paramVars.bNativeTraceBreakRunning
		
			IF NOT paramVars.bNativeTraceBreakFirstInput
				BREAK_ON_NATIVE_COMMAND(GET_CONTENTS_OF_TEXT_WIDGET(paramVars.nativeTraceTextWidgetID), FALSE)
			ELSE
				BREAK_ON_NATIVE_COMMAND(GET_CONTENTS_OF_TEXT_WIDGET(paramVars.nativeTraceTextWidgetID), TRUE)
				paramVars.bNativeTraceBreakFirstRunning = TRUE
			ENDIF
			paramVars.bNativeTraceBreakRunning = TRUE
			
		ELSE			
			// has the user change the option for breaking on first call
			IF paramVars.bNativeTraceBreakFirstInput	
				IF NOT paramVars.bNativeTraceBreakFirstRunning
					paramVars.bNativeTraceBreakRunning = FALSE	
				ENDIF
			ELSE
				IF paramVars.bNativeTraceBreakFirstRunning	
					paramVars.bNativeTraceBreakRunning = FALSE
					paramVars.bNativeTraceBreakFirstRunning = FALSE
				ENDIF
			ENDIF			
		ENDIF
		
	ELSE
		IF paramVars.bNativeTraceBreakRunning
			paramVars.bNativeTraceBreakRunning = FALSE		
		ENDIF		
		IF paramVars.bNativeTraceBreakFirstInput 
		
			IF paramVars.bNativeTraceBreakFirstRunning 
				paramVars.bNativeTraceBreakFirstRunning = FALSE
				paramVars.bNativeTraceBreakFirstInput = FALSE
			ELSE
				paramVars.bNativeTraceBreakInput = TRUE
			ENDIF
		ELSE		
			IF paramVars.bNativeTraceBreakFirstRunning 
				paramVars.bNativeTraceBreakFirstRunning = FALSE
			ENDIF		
		ENDIF		
	ENDIF 
ENDPROC


// *****************************************************************************************
//	GAMEPLAY CAM RELATIVE PITCH AND HEADING
// *****************************************************************************************

PROC Create_Gameplay_Cam_Relative_Pitch_And_Heading_Widgets(structSharedDebugVars &paramVars)
	START_WIDGET_GROUP("Gameplay Cam Relative Pitch And Heading")
		ADD_WIDGET_BOOL("Set Cam Heading And Pitch", paramVars.bSetGameplayCamRelativePitchAndHeading)
		ADD_WIDGET_FLOAT_SLIDER("Relative Pitch: ", paramVars.fCamRelativePitch, -360.0, 360.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Relative Heading: ", paramVars.fCamRelativeHeading, -360.0, 360.0, 1.0)		
	STOP_WIDGET_GROUP()
ENDPROC

PROC Maintain_Gameplay_Cam_Relative_Pitch_And_Heading_Widgets(structSharedDebugVars &paramVars)

	IF paramVars.bSetGameplayCamRelativePitchAndHeading

		SET_GAMEPLAY_CAM_RELATIVE_PITCH(paramVars.fCamRelativePitch)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(paramVars.fCamRelativeHeading)
		
		paramVars.bSetGameplayCamRelativePitchAndHeading = FALSE
		
	ENDIF
ENDPROC

// ===========================================================================================================

/// PURPOSE:
///    Checks for any keyboard-activated debug commands common to both singleplayer and multiplayer.
PROC Maintain_Shared_Debug_Keypresses(SCRIPT_TIMER &stTimer)
	
	Maintain_AltKeyK_Max_Special_Ability()
	Maintain_KeyC_Write_Player_Location_Details_To_File()
	Maintain_KeyL_Toggle_Locate_Outlines()
	Maintain_KeyO_Toggle_Screen_Overlays()
	Maintain_DpadDown_Check_For_Player_Car_Not_Upright()
	Maintain_KeyF9_Toggle_Mission_Flow_Debug_Screen(stTimer)
	Maintain_KeyLShiftMultiply_Force_Fade_In()
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Creates for any widget-activated debug commands common to both singleplayer and multiplayer.
/// NOTES:
///    Also initialises the shared debug variables
PROC Create_Shared_Debug_Widgets(structSharedDebugVars &paramVars)

	Initialise_Shared_Debug_Variables(paramVars)
	
	Create_Hash_Key_Generator_Widgets(paramVars)
	Create_Vehicle_Recording_Widgets(paramVars)
	Create_Vehicle_Colour_Widgets(paramVars)
	Create_Vehicle_Mod_Widgets(paramVars)
	Create_Vehicle_Performance_Widgets(paramVars)
	Create_Generic_Debug_Widgets(paramVars)
	Create_Test_SaveGame_Widgets(paramVars)
	Create_Scripted_Cam_Editor_Widgets(paramVars)
	Create_Ptfx_Tester_Widgets(paramVars)
	Create_Object_Positioning_Widgets(paramVars)
	Create_MinMax_Locate_Widgets(paramVars)
	Create_Door_Opening_Widgets(paramVars)
	Create_Radar_Map_Colours_Widgets(paramVars)
	Create_Car_Damage_Widgets(paramVars)
	Create_Vehicle_Extra_Widgets(paramVars)
	Create_Cam_Offset_widgets(paramVars)
	Create_Vehicle_Force_Widgets(paramVars)
	Create_Native_Trace_Widget(paramVars)
	Create_Game_Time_Date_Widget(paramVars)
	Create_Gameplay_Cam_Relative_Pitch_And_Heading_Widgets(paramVars)
	
	ADD_WIDGET_BOOL("Switch on script debug info", displayDebugDataInCommandWindow)
	
	//ADD_WIDGET_BOOL("Use new code AI blip system", g_bUseCodeAIBlipSystem)


ENDPROC



// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Checks for any widget-activated debug commands common to both singleplayer and multiplayer.
PROC Maintain_Shared_Debug_Widgets(structSharedDebugVars &paramVars)

	Maintain_Hash_Key_Generator_Widgets(paramVars)
	Maintain_Vehicle_Recording_Widgets(paramVars)
	Maintain_Vehicle_Colour_Widgets(paramVars)
	Maintain_Vehicle_Mod_Widgets(paramVars)
	Maintain_Vehicle_Performance_Widgets(paramVars)
	Maintain_Generic_Debug_Widgets(paramVars)
	Maintain_Test_SaveGame_Widgets(paramVars)
	Maintain_Scripted_Cam_Editor_Widgets(paramVars)
	Maintain_Ptfx_Tester_Widgets(paramVars)
	Maintain_Object_Positioning_Widgets(paramVars)
	Maintain_MinMax_Locate_Widgets(paramVars)
	Maintain_Door_Opening_Widgets(paramVars)
	Maintain_Radar_Map_Colours_Widgets(paramVars)
	Maintain_Car_Damage_Widgets(paramVars)
	Maintain_Vehicle_Extra_Widgets(paramVars)
	Maintain_Cam_Offset_widgets(paramVars)
	Maintain_Vehicle_Force_Widgets(paramVars)
	Maintain_Native_Trace_Widgets(paramVars)
	Maintain_Gameplay_Cam_Relative_Pitch_And_Heading_Widgets(paramVars)
		
ENDPROC




// ===========================================================================================================

/// PURPOSE:
///    Checks for any keyboard-activated debug commands common to both singleplayer and multiplayer.
PROC UPDATE_DEBUG_TIMESTAMP()
	SET_BIT(g_iDebugTimestampBitset, 0)
ENDPROC

FUNC STRING GET_DEBUG_TIMESTAMP()
	RETURN GET_STRING_FROM_STRING(g_tlDebugTimestamp, 0, GET_LENGTH_OF_LITERAL_STRING(g_tlDebugTimestamp))
ENDFUNC

PROC PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL(STRING strName)
	INT iThisNumCommandsExecuted = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
	IF (iThisNumCommandsExecuted < g_iCommandExecutedLastCall)
		g_iCommandExecutedLastCall = 0
	ENDIF
	PRINTLN("COMMANDS_EXECUTED_SINCE_LAST_CALL - ", strName, " = ", iThisNumCommandsExecuted - g_iCommandExecutedLastCall, " total commands this frame = ", iThisNumCommandsExecuted)
	g_iCommandExecutedLastCall = iThisNumCommandsExecuted
ENDPROC

PROC UPDATE_HIGHEST_NUMBER_OF_COMMANDS_EXECUTED_THIS_FRAME(INT &iCurrentHighest)
	INT iThisNumCommandsExecuted = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
	IF (iThisNumCommandsExecuted > 1000000)
		PRINTLN("over 1 million commands for ", GET_THIS_SCRIPT_NAME(), ".sc, total = ", iThisNumCommandsExecuted)
	ENDIF
	IF (iThisNumCommandsExecuted > iCurrentHighest)
		PRINTLN("NEW max no of commands for ", GET_THIS_SCRIPT_NAME(), ".sc is ", iThisNumCommandsExecuted)
		iCurrentHighest = iThisNumCommandsExecuted
	ENDIF
ENDPROC


FUNC STRING DEBUG_GET_LEVEL_NAME(eLevelIndex level)
	SWITCH level
		CASE LEVEL_GTA5			RETURN "LEVEL_GTA5"			BREAK
	    CASE LEVEL_TESTBED		RETURN "LEVEL_TESTBED"		BREAK
	    CASE LEVEL_VFX_TEST		RETURN "LEVEL_VFX_TEST"		BREAK
	    CASE LEVEL_NM_TEST		RETURN "LEVEL_NM_TEST"		BREAK
	    CASE LEVEL_NET_TEST		RETURN "LEVEL_NET_TEST"		BREAK
	    CASE LEVEL_ANIM_TEST	RETURN "LEVEL_ANIM_TEST"	BREAK
	    CASE LEVEL_CP_TESTBED	RETURN "LEVEL_CP_TESTBED"	BREAK
	    CASE LEVEL_TOOLS_TEST	RETURN "LEVEL_TOOLS_TEST"	BREAK
	   	CASE LEVEL_WATERBED		RETURN "LEVEL_WATERBED"		BREAK
	    CASE LEVEL_GFX_TEST		RETURN "LEVEL_GFX_TEST"		BREAK
	ENDSWITCH
	
	RETURN "UNKNOWN LEVEL"
ENDFUNC

FUNC STRING GET_CAMERA_GRAPH_TYPE_NAME(CAMERA_GRAPH_TYPE eGraphType)
	SWITCH eGraphType
		CASE GRAPH_TYPE_LINEAR 						RETURN "GRAPH_TYPE_LINEAR" 
		CASE GRAPH_TYPE_SIN_ACCEL_DECEL				RETURN "GRAPH_TYPE_SIN_ACCEL_DECEL" 
		CASE GRAPH_TYPE_ACCEL						RETURN "GRAPH_TYPE_ACCEL" 
		CASE GRAPH_TYPE_DECEL						RETURN "GRAPH_TYPE_DECEL" 
		CASE GRAPH_TYPE_SLOW_IN						RETURN "GRAPH_TYPE_SLOW_IN" 
		CASE GRAPH_TYPE_SLOW_OUT					RETURN "GRAPH_TYPE_SLOW_OUT" 
		CASE GRAPH_TYPE_SLOW_IN_OUT					RETURN "GRAPH_TYPE_SLOW_IN_OUT" 
		CASE GRAPH_TYPE_VERY_SLOW_IN				RETURN "GRAPH_TYPE_VERY_SLOW_IN" 
		CASE GRAPH_TYPE_VERY_SLOW_OUT				RETURN "GRAPH_TYPE_VERY_SLOW_OUT" 
		CASE GRAPH_TYPE_VERY_SLOW_IN_SLOW_OUT		RETURN "GRAPH_TYPE_VERY_SLOW_IN_SLOW_OUT" 
		CASE GRAPH_TYPE_SLOW_IN_VERY_SLOW_OUT		RETURN "GRAPH_TYPE_SLOW_IN_VERY_SLOW_OUT" 
		CASE GRAPH_TYPE_VERY_SLOW_IN_VERY_SLOW_OUT	RETURN "GRAPH_TYPE_V_SLOW_IN_V_SLOW_OUT" 
		CASE GRAPH_TYPE_EASE_IN						RETURN "GRAPH_TYPE_EASE_IN" 
		CASE GRAPH_TYPE_EASE_OUT					RETURN "GRAPH_TYPE_EASE_OUT" 
		CASE GRAPH_TYPE_QUADRATIC_EASE_IN			RETURN "GRAPH_TYPE_QUADRATIC_EASE_IN"
		CASE GRAPH_TYPE_QUADRATIC_EASE_OUT			RETURN "GRAPH_TYPE_QUADRATIC_EASE_OUT"
		CASE GRAPH_TYPE_QUADRATIC_EASE_IN_OUT		RETURN "GRAPH_TYPE_QUADRATIC_EASE_IN_OUT"
		CASE GRAPH_TYPE_CUBIC_EASE_IN				RETURN "GRAPH_TYPE_CUBIC_EASE_IN"
		CASE GRAPH_TYPE_CUBIC_EASE_OUT				RETURN "GRAPH_TYPE_CUBIC_EASE_OUT"
		CASE GRAPH_TYPE_CUBIC_EASE_IN_OUT			RETURN "GRAPH_TYPE_CUBIC_EASE_IN_OUT"
		CASE GRAPH_TYPE_QUARTIC_EASE_IN				RETURN "GRAPH_TYPE_QUARTIC_EASE_IN"
		CASE GRAPH_TYPE_QUARTIC_EASE_OUT			RETURN "GRAPH_TYPE_QUARTIC_EASE_OUT"
		CASE GRAPH_TYPE_QUARTIC_EASE_IN_OUT			RETURN "GRAPH_TYPE_QUARTIC_EASE_IN_OUT"
		CASE GRAPH_TYPE_QUINTIC_EASE_IN				RETURN "GRAPH_TYPE_QUINTIC_EASE_IN"
		CASE GRAPH_TYPE_QUINTIC_EASE_OUT			RETURN "GRAPH_TYPE_QUINTIC_EASE_OUT"
		CASE GRAPH_TYPE_QUINTIC_EASE_IN_OUT  		RETURN "GRAPH_TYPE_QUINTIC_EASE_IN_OUT"
		CASE GRAPH_TYPE_CIRCULAR_EASE_IN			RETURN "GRAPH_TYPE_CIRC_EASE_INTYPE_LINEAR"
		CASE GRAPH_TYPE_CIRCULAR_EASE_OUT			RETURN "GRAPH_TYPE_CIRC_EASE_OUT"
		CASE GRAPH_TYPE_CIRCULAR_EASE_IN_OUT		RETURN "GRAPH_TYPE_CIRC_EASE_IN_OUT"
	ENDSWITCH
	RETURN ""
ENDFUNC


// ##### --------------------------------------------------------------------------------------------------
// ##### Section Name: Debug Movers -----------------------------------------------------------------------
// ##### Description: Helper Functions for adding movers to debug so it can be dragged around the screen --
// ##### --------------------------------------------------------------------------------------------------
// ##### --------------------------------------------------------------------------------------------------

// Reuse this one, as we don't care.
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_REUSABLE					0 // If you don't care that other debug might also use this.	

// Debug.sc
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_CONVERSATION 			1

// MC DEBUG (start at 1
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_SPAWN_GROUPS 			1
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_AGGRO_INDEXES 			2
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_CUSTOM_SPAWN_POINTS 		3
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_ENTITY_STATE 			4

// MAX
CONST_INT ciDEBUG_ON_SCREEN_MOVERS_MAX 						5

STRUCT DEBUG_MOVERS_STRUCT
	FLOAT fDebugMoverMouseX[ciDEBUG_ON_SCREEN_MOVERS_MAX]
	FLOAT fDebugMoverMouseY[ciDEBUG_ON_SCREEN_MOVERS_MAX]
	INT iDebugMoverExtend = -1
ENDSTRUCT

FUNC BOOL IS_DEBUG_MOVERS_MOUSE_CLICK_PRESSED(MOUSE_BUTTON eMB)
	
	// TODO - This is unreliable for some reason. Sometimes not detecting the press, including IS_MOUSE_BUTTON_PRESSED. Investigate a better way. Note: INPUT_CURSOR_ACCEPT does not work while in Debug Keyboard.
	IF IS_MOUSE_BUTTON_PRESSED(eMB)		
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_DEBUG_MOVER_BEING_HOVERED_OVER(DEBUG_MOVERS_STRUCT &sDebugMoversStruct, INT iDebugMoverType, FLOAT fPosX, FLOAT fPosY, FLOAT fSizeX, FLOAT fSizeY, FLOAT &fXReturn, FLOAT &fYReturn)
	
	FLOAT fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	FLOAT fMouseY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	FLOAT fRectX_Max = fPosX + PICK_FLOAT(iDebugMoverType = sDebugMoversStruct.iDebugMoverExtend, 1.0, fSizeX/2)
	FLOAT fRectY_Max = fPosY + PICK_FLOAT(iDebugMoverType = sDebugMoversStruct.iDebugMoverExtend, 1.0, fSizeY/2)
	FLOAT fRectX_Min = fPosX - PICK_FLOAT(iDebugMoverType = sDebugMoversStruct.iDebugMoverExtend, 1.0, fSizeX/2)
	FLOAT fRectY_Min = fPosY - PICK_FLOAT(iDebugMoverType = sDebugMoversStruct.iDebugMoverExtend, 1.0, fSizeY/2)
	
	IF sDebugMoversStruct.iDebugMoverExtend = iDebugMoverType
		sDebugMoversStruct.iDebugMoverExtend = -1
	ENDIF
	
	IF fMouseX >= fRectX_Min AND fMouseX <= fRectX_Max
	AND fMouseY >= fRectY_Min AND fMouseY <= fRectY_Max
		fXReturn = fMouseX
		fYReturn = fMouseY
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_DEBUG_MOVER_FOR_TYPE(DEBUG_MOVERS_STRUCT &sDebugMoversStruct, INT iDebugMoverType, FLOAT &fPosX, FLOAT &fPosY, FLOAT fSizeX, FLOAT fSizeY, BOOL bDrawWhenNotHovering = FALSE, BOOL bRectFromCorner = FALSE,  BOOL bRectFromLeftCenter = FALSE)
		
	IF iDebugMoverType < 0
	OR iDebugMoverType > ciDEBUG_ON_SCREEN_MOVERS_MAX		
		EXIT
	ENDIF
	
	IF sDebugMoversStruct.fDebugMoverMouseX[iDebugMoverType] != 0.0
	AND sDebugMoversStruct.fDebugMoverMouseY[iDebugMoverType] != 0.0
		fPosX = sDebugMoversStruct.fDebugMoverMouseX[iDebugMoverType]
		fPosY = sDebugMoversStruct.fDebugMoverMouseY[iDebugMoverType]		
	ENDIF
	
	FLOAT fRectX, fRectY
	IF bRectFromLeftCenter
		fRectX = fPosX + fSizeX/2
		fRectY = fPosY
	ELIF bRectFromCorner
		fRectX = fPosX + fSizeX/2
		fRectY = fPosY + fSizeY/2
	ELSE
		fRectX = fPosX
		fRectY = fPosY
	ENDIF
	
	FLOAT fMouseX, fMouseY
	
	IF NOT IS_DEBUG_MOVER_BEING_HOVERED_OVER(sDebugMoversStruct, iDebugMoverType, fRectX, fRectY, fSizeX, fSizeY, fMouseX, fMouseY)
		// Not Hovered
		IF bDrawWhenNotHovering
			DRAW_RECT(fRectX, fRectY, fSizeX, fSizeY, 0, 0, 0, 50)	
		ENDIF
		IF iDebugMoverType = sDebugMoversStruct.iDebugMoverExtend
			sDebugMoversStruct.iDebugMoverExtend = -1
		ENDIF
		
		EXIT
	ENDIF
			
	IF IS_DEBUG_MOVERS_MOUSE_CLICK_PRESSED(MB_LEFT_BTN)
		// Pressed
		DRAW_RECT(fRectX, fRectY, fSizeX, fSizeY, 0, 0, 0, 150)
		
		// Mouse Cache	
		IF sDebugMoversStruct.iDebugMoverExtend = -1
			sDebugMoversStruct.fDebugMoverMouseX[iDebugMoverType] = fMouseX - PICK_FLOAT(bRectFromCorner, fSizeX/2, 0.0)
			sDebugMoversStruct.fDebugMoverMouseY[iDebugMoverType] = fMouseY - PICK_FLOAT(bRectFromCorner, fSizeY/2, 0.0)		
			sDebugMoversStruct.iDebugMoverExtend = iDebugMoverType
		ENDIF
	ELSE
		// Hovered
		DRAW_RECT(fRectX, fRectY, fSizeX, fSizeY, 0, 0, 0, 100)
		IF iDebugMoverType = sDebugMoversStruct.iDebugMoverExtend
			sDebugMoversStruct.iDebugMoverExtend = -1
		ENDIF
	ENDIF	
	
ENDPROC 


