#IF IS_DEBUG_BUILD
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "commands_graphics.sch"
USING "transition_joining.sch"

STRUCT DEBUG_DRAW_DATA

	// debug drawing stuff
	BOOL bRenderDebugRect
	FLOAT dd_rect_x = 0.5
	FLOAT dd_rect_y = 0.5
	FLOAT dd_rect_width = 0.25
	FLOAT dd_rect_height = 0.25
	INT dd_rect_r
	INT dd_rect_g 
	INT dd_rect_b
	INT dd_rect_a = 128
	BOOL bFromCorner

	// text
	BOOL bRenderDebugText
	FLOAT dd_text_x = 0.5 
	FLOAT dd_text_y = 0.5
	FLOAT dd_text_scale = 1.0
	INT dd_text_r = 255
	INT dd_text_g = 255
	INT dd_text_b = 255
	INT dd_text_a = 255
	TEXT_WIDGET_ID dd_text_widget
	
	INT iDrawOrder

	BOOL bTestNative


ENDSTRUCT


PROC UPDATE_DEBUG_DRAW(DEBUG_DRAW_DATA &data)

	SET_SCRIPT_GFX_DRAW_ORDER(INT_TO_ENUM(GFX_DRAW_ORDER, data.iDrawOrder))
	
	// debug draw
	IF (data.bRenderDebugRect)
		IF (data.bFromCorner)
			DRAW_RECT_FROM_CORNER(data.dd_rect_x, data.dd_rect_y, data.dd_rect_width,  data.dd_rect_height, data.dd_rect_r, data.dd_rect_g, data.dd_rect_b, data.dd_rect_a)
		ELSE
			DRAW_RECT(data.dd_rect_x, data.dd_rect_y, data.dd_rect_width,  data.dd_rect_height, data.dd_rect_r, data.dd_rect_g, data.dd_rect_b, data.dd_rect_a)
		ENDIF
	ENDIF
	
	IF (data.bRenderDebugText)
		SET_TEXT_SCALE(0.0000, data.dd_text_scale)
		SET_TEXT_COLOUR(data.dd_text_r, data.dd_text_g, data.dd_text_b, data.dd_text_a)
		IF DOES_TEXT_WIDGET_EXIST(data.dd_text_widget)
			DISPLAY_TEXT_WITH_LITERAL_STRING(data.dd_text_x, data.dd_text_y, "STRING", GET_CONTENTS_OF_TEXT_WIDGET(data.dd_text_widget))
		ELSE
			DISPLAY_TEXT_WITH_LITERAL_STRING(data.dd_text_x, data.dd_text_y, "STRING", "test text")
		ENDIF
	ENDIF
	
	IF (data.bTestNative)
		SET_FRONTEND_ACTIVE(FALSE)	
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
		RESET_PAUSED_RENDERPHASES()
		data.bTestNative = FALSE
	ENDIF


ENDPROC


PROC CREATE_DEBUG_DRAW_WIDGET(DEBUG_DRAW_DATA &data)

	PRINTLN("called CREATE_DEBUG_DRAW_WIDGET")
	
	START_WIDGET_GROUP("Debug Draw")
	
		ADD_WIDGET_INT_SLIDER("iDrawOrder", data.iDrawOrder, 0, 99, 1)
		
		START_WIDGET_GROUP("Rect")
			ADD_WIDGET_BOOL("bRender", data.bRenderDebugRect)
			ADD_WIDGET_BOOL("bFromCorner", data.bFromCorner)
			ADD_WIDGET_FLOAT_SLIDER("x",data.dd_rect_x, -1.0, 1.0, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("y",data.dd_rect_y, -1.0, 1.0, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("width",data.dd_rect_width, 0.0, 2.0, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("height",data.dd_rect_height, 0.0, 2.0, 0.0001)
			ADD_WIDGET_INT_SLIDER("r",data.dd_rect_r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g",data.dd_rect_g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b",data.dd_rect_b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a",data.dd_rect_a, 0, 255, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Text")
			ADD_WIDGET_BOOL("bRender", data.bRenderDebugText)
			data.dd_text_widget = ADD_TEXT_WIDGET("Test String")
			ADD_WIDGET_FLOAT_SLIDER("x",data.dd_text_x, 0.0, 1.0, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("y",data.dd_text_y, 0.0, 1.0, 0.0001)
			ADD_WIDGET_FLOAT_SLIDER("scale",data.dd_text_scale, 0.0, 1.0, 0.0001)
			ADD_WIDGET_INT_SLIDER("r",data.dd_text_r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g",data.dd_text_g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b",data.dd_text_b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a",data.dd_text_a, 0, 255, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Call Natives")
			ADD_WIDGET_BOOL("bTestNative", data.bTestNative)				
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
			
			

ENDPROC

#ENDIF


