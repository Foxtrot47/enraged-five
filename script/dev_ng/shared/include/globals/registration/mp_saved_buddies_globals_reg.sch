//USING "rage_builtins.sch"
//USING "globals.sch"
//USING "commands_misc.sch"
//
//
//// *****************************************************************************************
//// *****************************************************************************************
//// *****************************************************************************************
////
////      MISSION NAME    :   mp_saved_buddies_globals_reg.sch
////      AUTHOR          :   Ryan B
////      DESCRIPTION     :   Contains the registrations with code for all MP buddies
////							globals that need to be saved.
////
//// *****************************************************************************************
//// *****************************************************************************************
//// *****************************************************************************************
//
///// PURPOSE:
/////    Register all the variables in the struct containing saved Gamer Handle variables for a single saved MP Buddy
//PROC Register_Struct_MP_Saved_Buddies_GH(GAMER_HANDLE &savedBuddiesGamerHandle, STRING paramNameOfStruct)
//
//	START_SAVE_STRUCT_WITH_SIZE(savedBuddiesGamerHandle, SIZE_OF(savedBuddiesGamerHandle), paramNameOfStruct)	
//
//		START_SAVE_ARRAY_WITH_SIZE(savedBuddiesGamerHandle.Data, SIZE_OF(savedBuddiesGamerHandle.Data), "DATA")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[0], "Data0")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[1], "Data1")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[2], "Data2")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[3], "Data3")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[4], "Data4")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[5], "Data5")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[6], "Data6")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[7], "Data7")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[8], "Data8")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[9], "Data9")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[10], "Data10")
//			REGISTER_INT_TO_SAVE(savedBuddiesGamerHandle.Data[11], "Data11")
//		STOP_SAVE_ARRAY()
//		
//	STOP_SAVE_STRUCT()
//	
//ENDPROC
//
///// PURPOSE:
/////    Register all the variables in the struct containing saved Gamer Info variables for a single saved MP Buddy
//PROC Register_Struct_MP_Saved_Buddies_GI(GAMER_INFO &savedBuddiesGamerInfo, STRING paramNameOfStruct)
//
//	START_SAVE_STRUCT_WITH_SIZE(savedBuddiesGamerInfo, SIZE_OF(savedBuddiesGamerInfo), paramNameOfStruct)
//		
//		Register_Struct_MP_Saved_Buddies_GH(savedBuddiesGamerInfo.Handle, "GAMER_HANDLE_STRUCT")
//		REGISTER_TEXT_LABEL_23_TO_SAVE(savedBuddiesGamerInfo.Name, "NAME")
//	STOP_SAVE_STRUCT()
//
//ENDPROC
//
///// PURPOSE:
/////    Register all the variables in the struct containing saved Gamer Info for all Buddies
//PROC Register_Struct_MP_Saved_Buddies(GAMER_INFO &BuddyGamerInfoArray[MAX_MP_SAVED_BUDDIES], STRING paramNameOfStruct)
//	INT tempLoop = 0
//
//	START_SAVE_ARRAY_WITH_SIZE(BuddyGamerInfoArray, SIZE_OF(BuddyGamerInfoArray), paramNameOfStruct)
//		TEXT_LABEL_31 mpBuddyID
//		REPEAT MAX_MP_SAVED_BUDDIES tempLoop
//			mpBuddyID = "GAMER_INFO_STRUCT_"
//			mpBuddyID += tempLoop
//			
//			Register_Struct_MP_Saved_Buddies_GI(BuddyGamerInfoArray[tempLoop], mpBuddyID)
//		ENDREPEAT
//		
//		
//	STOP_SAVE_STRUCT()
//
//ENDPROC
//
///// PURPOSE:
/////    Register the array containing the saved variables for each buddy
//PROC Register_Array_Of_Saved_MP_BUDDIES(MP_SAVED_BUDDIES_STRUCT &savedBuddies, STRING paramNameOfArray)
//
//	
//	
//	START_SAVE_STRUCT_WITH_SIZE(savedBuddies, SIZE_OF(savedBuddies), paramNameOfArray)
//		Register_Struct_MP_Saved_Buddies(savedBuddies.buddygamerinfo, "MP_BUDDY")
//		REGISTER_INT_TO_SAVE(savedBuddies.iNumBuddies, "NUMBER_OF_BUDDIES")
//	STOP_SAVE_ARRAY()
//
//ENDPROC
//
//// -----------------------------------------------------------------------------------------------------------
//
///// PURPOSE:
/////    Sets up the save structure for registering all the buddies globals that need to be saved
//PROC REGISTER_MP_BUDDIES_SAVED_GLOBALS()
//
//	Register_Array_Of_Saved_MP_BUDDIES(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies, "MP_SAVED_BUDDIES")
//
//ENDPROC
