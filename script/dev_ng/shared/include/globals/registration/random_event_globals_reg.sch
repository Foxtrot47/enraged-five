USING "rage_builtins.sch"
USING "globals.sch"

PROC REGISTER_ARRAY_OF_RANDOMEVENTVARIATION(INT &paramPropertiesArray[MAX_RANDOM_EVENTS], STRING paramNameOfArray)

	INT tempLoop = 0
	START_SAVE_ARRAY_WITH_SIZE(paramPropertiesArray, SIZE_OF(paramPropertiesArray), paramNameOfArray)
		TEXT_LABEL_31 PropertiesName
		REPEAT MAX_RANDOM_EVENTS tempLoop
			PropertiesName = "RAND_EVENT_Var"
			PropertiesName += tempLoop
			REGISTER_INT_TO_SAVE(paramPropertiesArray[tempLoop], PropertiesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
ENDPROC

PROC REGISTER_ARRAY_OF_RANDOMEVENTTIME(TIMEOFDAY &paramPropertiesArray[MAX_RANDOM_EVENTS], STRING paramNameOfArray)

	INT tempLoop = 0
	START_SAVE_ARRAY_WITH_SIZE(paramPropertiesArray, SIZE_OF(paramPropertiesArray), paramNameOfArray)
		TEXT_LABEL_31 PropertiesName
		REPEAT MAX_RANDOM_EVENTS tempLoop
			PropertiesName = "RAND_EVENT_Time"
			PropertiesName += tempLoop
			REGISTER_ENUM_TO_SAVE(paramPropertiesArray[tempLoop], PropertiesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
ENDPROC

PROC REGISTER_RANDOM_EVENTS_SAVED_GLOBALS()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sRandomEventData, SIZE_OF(g_savedGlobals.sRandomEventData), "RANDOM_EVENTS_SAVED")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iBitsetReUnlockA, "RE_FLAGS")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iBitsetReUnlockB, "RE_TIME_REQ_SET")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iHelpCount, "RE_HELP_COUNT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iBlipFlashHelpCount, "RE_BLIP_HELP_COUNT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iItemReturnHelpCount, "RE_ITEM_HELP_COUNT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds, "RE_NUMBER_OF_PEDS_DELIVERED")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomEventData.iSecurityVanHelps, "RE_SECVANHELP")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomEventData.bHasPlayerAttackedDrugFarm, "RE_DRUGFARMATTACKED")
		REGISTER_ARRAY_OF_RANDOMEVENTVARIATION(g_savedGlobals.sRandomEventData.iREVariationComplete, "RE_VAR_COMPLETE") 
		REGISTER_ARRAY_OF_RANDOMEVENTTIME(g_savedGlobals.sRandomEventData.eTimeBlockUntil, "RE_BLOCK_TIME")
	STOP_SAVE_STRUCT()
	
ENDPROC

