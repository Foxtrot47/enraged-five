USING "rage_builtins.sch"
USING "globals.sch"

PROC Register_Array_Of_Golf_Holes_Saved(GolfHoleDataSaved & sHoleArray[9], STRING szNameOfArray)
	INT iHoleIndex
	TEXT_LABEL_63 holeName
	TEXT_LABEL_63 holeVarLabel

	START_SAVE_ARRAY_WITH_SIZE(sHoleArray, SIZE_OF(sHoleArray), szNameOfArray)
	
		REPEAT COUNT_OF(sHoleArray) iHoleIndex
			holeName = szNameOfArray
			holeName += iHoleIndex
			holeName += "_"

			holeVarLabel = holeName
			holeVarLabel += "fLongestDriveHole"
			REGISTER_FLOAT_TO_SAVE(sHoleArray[iHoleIndex].fLongestDriveHole, holeVarLabel)
			
			holeVarLabel = holeName
			holeVarLabel += "fLongestPuttHole"
			REGISTER_FLOAT_TO_SAVE(sHoleArray[iHoleIndex].fLongestPuttHole, holeVarLabel)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC


PROC Register_Array_Of_Golf_Course_Saved(GolfCourseDataSaved & sCourseArray[1], STRING szNameOfArray)
	INT iCourseIndex
	TEXT_LABEL_63 courseName

	START_SAVE_ARRAY_WITH_SIZE(sCourseArray, SIZE_OF(sCourseArray), szNameOfArray)

		REPEAT COUNT_OF(sCourseArray) iCourseIndex
			courseName = szNameOfArray
			courseName += iCourseIndex
			courseName += "_"
			Register_Array_Of_Golf_Holes_Saved(sCourseArray[iCourseIndex].sHole, courseName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

PROC Register_Array_Of_Num_Games_Played_Saved(INT &NumGamesArray[3], STRING szNameOfArray)
	INT index
	TEXT_LABEL_63 NumGamesName

	START_SAVE_ARRAY_WITH_SIZE(NumGamesArray, SIZE_OF(NumGamesArray), szNameOfArray)

		REPEAT COUNT_OF(NumGamesArray) index
			NumGamesName = szNameOfArray
			NumGamesName += index
			REGISTER_INT_TO_SAVE(NumGamesArray[index], NumGamesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC


PROC Register_Golf_Saved_Globals()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sGolfData, SIZE_OF(g_savedGlobals.sGolfData), "GOLF_SAVED_STRUCT")
		Register_Array_Of_Golf_Course_Saved(g_savedGlobals.sGolfData.sCourse, "GOLF_COURSE_")
		Register_Array_Of_Num_Games_Played_Saved(g_savedGlobals.sGolfData.iNumRoundsWithCharacter, "GOLF_iNumRoundsWithCharacter_")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iNumHolesPlayed, "GOLF_iNumHolesPlayed")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iNumPuttsTotal, "GOLF_iNumPuttsTotal")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iNumFairwayDrivesTotal, "GOLF_iNumFairwayDrivesTotal")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sGolfData.unlockedBuddies, "GOLF_eUnlockedBuddies")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sGolfData.golfSavedFlags, "GOLF_eGolfSavedFlags")
		
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iBestRound, "GOLF_iBestRound")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iNumRoundsPlayedSP, "GOLF_iNumRoundsPlayedSP")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iNumRoundsPlayedMP, "GOLF_iNumRoundsPlayedMP")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sGolfData.iNumGolfWins, "GOLF_iNumGolfWins")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sGolfData.fLongestDrive, "GOLF_fLongestDrive")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sGolfData.fLongestPutt, "GOLF_fLongestPutt")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sGolfData.fAveragePutts, "GOLF_fAveragePutts")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sGolfData.fAverageFairwayDrives, "GOLF_fAverageFairwayDrives")

	STOP_SAVE_STRUCT()
ENDPROC

