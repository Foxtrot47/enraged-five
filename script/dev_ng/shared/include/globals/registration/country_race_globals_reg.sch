// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   country_race_globals_reg.sch
//      AUTHOR          :   Joe Elvin
//      DESCRIPTION     :   Contains the registrations with code for all country race
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the country race globals that need to be saved
PROC Register_Country_Race_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sCountryRaceData, SIZE_OF(g_savedGlobals.sCountryRaceData), "COUNTRY_RACE_SAVED_STRUCT")
		
		REGISTER_INT_TO_SAVE(g_savedGlobals.sCountryRaceData.iCurrentRace, "COUNTRY_RACE_iCurrentRace")
		REGISTER_INT_TO_SAVE(g_savedGLobals.sCountryRaceData.iBestTime, "COUNTRY_RACE_iBestTime")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sCountryRaceData.iSlipStreamHelpCount, "COUNTRY_RACE_iSlipStreamHelpCount")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bStallionUnlocked, "COUNTRY_RACE_bStallionUnlocked")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bGauntletUnlocked, "COUNTRY_RACE_bGauntletUnlocked")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bDominatorUnlocked, "COUNTRY_RACE_bDominatorUnlocked")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bBuffaloUnlocked, "COUNTRY_RACE_bBuffaloUnlocked")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bMarshallUnlocked, "COUNTRY_RACE_bMarshallUnlocked")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bRaceJustCompleted, "COUNTRY_RACE_bRaceJustCompleted")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bDisrupted, "COUNTRY_RACE_bDisrupted")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCountryRaceData.bTextRegistered, "COUNTRY_RACE_bTextRegistered")
	STOP_SAVE_STRUCT()
ENDPROC
