USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   mp_saved_big_ass_vehicles_globals_reg.sch
//      AUTHOR          :   David G
//      DESCRIPTION     :   Contains the registrations with code for big ass vehicle
//							globals (pegasus) that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register the array containing the Big Ass Vehicles saved variables
PROC Register_Array_Of_Saved_MP_BIG_ASS_VEHICLES(MP_SAVED_BIG_ASS_VEHICLES_STRUCT &savedBigAssVehicles, STRING paramNameOfArray)

	START_SAVE_STRUCT_WITH_SIZE(savedBigAssVehicles, SIZE_OF(savedBigAssVehicles), paramNameOfArray)		
		INT tempLoop
		TEXT_LABEL_15 tempLabel
		REGISTER_INT_TO_SAVE(savedBigAssVehicles.iBAV_timestamp, "BAV_Timestamp")
		START_SAVE_ARRAY_WITH_SIZE(savedBigAssVehicles.iBigAssVehiclesBS, SIZE_OF(savedBigAssVehicles.iBigAssVehiclesBS), "B_A_V_BS_ID")
			REPEAT NUM_BIG_ASS_VEHICLE_BITSETS tempLoop
				tempLabel = "B_A_V_BS_ID" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(savedBigAssVehicles.iBigAssVehiclesBS[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
	STOP_SAVE_ARRAY()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the Big Ass Vehicle globals that need to be saved
PROC REGISTER_MP_BIG_ASS_VEHICLES_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
	TEXT_LABEL_23 tlSlot = 	"MP_BIG_ASS_VEHICLES"
	tlSlot += iIndex
	Register_Array_Of_Saved_MP_BIG_ASS_VEHICLES(globalData.MpSavedBigAssVehicles, tlSlot)

ENDPROC

