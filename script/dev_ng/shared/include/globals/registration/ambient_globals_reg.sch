USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   ambient_globals_reg.sch
//      AUTHOR          :   Craig
//      DESCRIPTION     :   Contains the registrations with code for all ambient
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

PROC Register_Hunting_Challenge_Array(INT &iArray[MAX_HUNT_CHALLENGE], STRING tArrayName)
	TEXT_LABEL_31 tLabel
	INT iCounter
	START_SAVE_ARRAY_WITH_SIZE(iArray, SIZE_OF(iArray), tArrayName)
		REPEAT COUNT_OF(iArray) iCounter
			tLabel = tArrayName
			tLabel += iCounter
			REGISTER_INT_TO_SAVE(iArray[iCounter], tLabel)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC


/// PURPOSE:
///    Register all the variables in the struct containing saved variables for ambient events
/// PARAMS:
///    paramambient		This instance of the Ambient event to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_STUNT_JUMPS(STUNT_JUMPS &paramambient, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramambient, SIZE_OF(paramambient), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(paramambient.iStuntJumpsCompleted, "STUNT_JUMPS_iStuntJumpsCompleted")
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register all the variables in the ProstituteSaved struct containing saved variables for unique prostitutes B*1276913
/// PARAMS:
///    sSavedProstituteData - g_savedGlobals.sAmbient.sProstituteData[]
///    sNameOfStruct - Unique name - Currently suggest the unique prostitutes ID ex: "PUN_BABS"
PROC Register_Struct_PROSTITUTION(ProstituteSaved &sSavedProstituteData, STRING sNameOfStruct)
	
	TEXT_LABEL_63 siSleptTimesArrayName
	siSleptTimesArrayName = sNameOfStruct
	siSleptTimesArrayName += "_iNumFdAry"
		
	START_SAVE_STRUCT_WITH_SIZE(sSavedProstituteData, SIZE_OF(sSavedProstituteData), sNameOfStruct)
		
		START_SAVE_ARRAY_WITH_SIZE(sSavedProstituteData.iNumTimesEncounteredByPlayer, SIZE_OF(sSavedProstituteData.iNumTimesEncounteredByPlayer), siSleptTimesArrayName)
		
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iNumTimesEncounteredByPlayer[CHAR_MICHAEL], "iMetMichael")
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iNumTimesEncounteredByPlayer[CHAR_FRANKLIN], "iMetFranklin")
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iNumTimesEncounteredByPlayer[CHAR_TREVOR], "iMetTrevor")
		
		STOP_SAVE_ARRAY()
		
		TEXT_LABEL_63 sComponentArrayName
		sComponentArrayName = sNameOfStruct
		sComponentArrayName += "_CmpDrawAry"
		
		START_SAVE_ARRAY_WITH_SIZE(sSavedProstituteData.iComponentDrawableVariation, SIZE_OF(sSavedProstituteData.iComponentDrawableVariation), sComponentArrayName)
		
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iComponentDrawableVariation[PRO_COMP_HEAD], "iUPCmpDrawHEAD")
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iComponentDrawableVariation[PRO_COMP_HAIR], "iUPCmpDrawHAIR")
		//	REGISTER_INT_TO_SAVE(sSavedProstituteData.iComponentDrawableVariation[PRO_COMP_TORSO], "iUPCmpDrawTORSO")
		//	REGISTER_INT_TO_SAVE(sSavedProstituteData.iComponentDrawableVariation[PRO_COMP_LEG], "iUPCmpDrawLEG")
			
		STOP_SAVE_ARRAY()
		
		sComponentArrayName = sNameOfStruct
		sComponentArrayName += "_CmpTextAry"
		
		START_SAVE_ARRAY_WITH_SIZE(sSavedProstituteData.iComponentTextureVariation, SIZE_OF(sSavedProstituteData.iComponentTextureVariation), sComponentArrayName)
		
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iComponentTextureVariation[PRO_TEXT_VAR_HEAD], "iUPCmpTextHEAD")
			REGISTER_INT_TO_SAVE(sSavedProstituteData.iComponentTextureVariation[PRO_TEXT_VAR_HAIR], "iUPCmpTextHAIR")
			
		STOP_SAVE_ARRAY()
		
		//Model Type
		sComponentArrayName = sNameOfStruct
		sComponentArrayName += "_ModelType"
		
		REGISTER_INT_TO_SAVE(sSavedProstituteData.iModelType, sComponentArrayName)
		
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register all the instances of the ProstituteSaved struct in the array containing saved variables for unique prostitutes B*1276913
/// PARAMS:
///    sSavedProstituteDataArray - g_savedGlobals.sAmbient.sProstituteData
///    sNameOfSavedProstituteDataArray - unique ID
PROC Register_Array_Of_Unique_Prostitutes(ProstituteSaved &sSavedProstituteDataArray[MAX_UNIQUE_PROSTITUTES], STRING sNameOfSavedProstituteDataArray) 
	
	INT iIterator = 0
	
	START_SAVE_ARRAY_WITH_SIZE(sSavedProstituteDataArray, SIZE_OF(sSavedProstituteDataArray), sNameOfSavedProstituteDataArray)
		
		TEXT_LABEL_31 sProstituteID
		
		REPEAT MAX_UNIQUE_PROSTITUTES iIterator
			
			SWITCH INT_TO_ENUM(PROSTITUTE_UNIQUE_ENUM, iIterator)
				
				CASE PUN_BABS
					sProstituteID = "U_PRO_BABS"
				BREAK

				CASE PUN_DANA		
					sProstituteID = "U_PRO_DANA"
				BREAK

				CASE PUN_LIZZIE		
					sProstituteID = "U_PRO_LIZZIE"
				BREAK

				CASE PUN_AMANDA		
					sProstituteID = "U_PRO_AMANDA"
				BREAK

				CASE PUN_ASHLEY		
					sProstituteID = "U_PRO_ASHLEY"
				BREAK
			
				CASE PUN_KRISTEN	
					sProstituteID = "U_PRO_KRISTEN"
				BREAK
				
				CASE PUN_SASHA		
					sProstituteID = "U_PRO_SASHA"
				BREAK
				
				CASE PUN_JACQUELINE
					sProstituteID = "U_PRO_JACQUELINE"
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Unique_Prostitutes - Missing Prostitute")
					sProstituteID = "Prostitute_UDF"
					sProstituteID += iIterator
				#if not USE_SP_DLC	//Added this in to allow intellesence to see funcs but without causing binary dif - CV
				BREAK
				#ENDIF
			ENDSWITCH

			Register_Struct_PROSTITUTION( sSavedProstituteDataArray[iIterator], sProstituteID)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
ENDPROC


PROC Register_Array_Of_Num_Times_Player_Paid_For_Prostitutes(INT &iNumTimesPaidForProstitutes[3], STRING sNameOfArray) 

	START_SAVE_ARRAY_WITH_SIZE(iNumTimesPaidForProstitutes, SIZE_OF(iNumTimesPaidForProstitutes), sNameOfArray)
		REGISTER_INT_TO_SAVE(iNumTimesPaidForProstitutes[CHAR_MICHAEL], "PROSTITUTE_SERVICES_MICHAEL")
		REGISTER_INT_TO_SAVE(iNumTimesPaidForProstitutes[CHAR_FRANKLIN], "PROSTITUTE_SERVICES_FRANKLIN")
		REGISTER_INT_TO_SAVE(iNumTimesPaidForProstitutes[CHAR_TREVOR], "PROSTITUTE_SERVICES_TREVOR")
	STOP_SAVE_ARRAY()
	
ENDPROC


//PROC Register_Struct_re_arrest(RE_ARREST &paramambient, STRING paramNameOfStruct)
//
//	START_SAVE_STRUCT(paramambient, paramNameOfStruct)
//		REGISTER_BOOL_TO_SAVE(paramambient.g_cop_killer, "re_arrest_bool")
//	STOP_SAVE_STRUCT()
//
//ENDPROC


/// PURPOSE:
///    Registers variables for the hidden package globals needed by the chop script
PROC Register_Struct_GLOBAL_SCRAP_DATA(GLOBAL_SCRAP_DATA &paramAmbient, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramAmbient, SIZE_OF(paramAmbient), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(paramAmbient.iScrap0to31, "GLOBAL_SCRAP_DATA_iScrap0to31")
		REGISTER_INT_TO_SAVE(paramAmbient.iScrap32to63, "GLOBAL_SCRAP_DATA_iScrap32to63")
		REGISTER_BOOL_TO_SAVE(paramAmbient.bMissionActive, "GLOBAL_SCRAP_DATA_bMissionActive")
	STOP_SAVE_STRUCT()
	
ENDPROC

PROC Register_Website_Subscription_Array(BOOL &bWebsiteSubscription[3], STRING sNameOfArray) 

	START_SAVE_ARRAY_WITH_SIZE(bWebsiteSubscription, SIZE_OF(bWebsiteSubscription), sNameOfArray)
		REGISTER_BOOL_TO_SAVE(bWebsiteSubscription[CHAR_MICHAEL], "REALITY_MILL_MICHAEL")
		REGISTER_BOOL_TO_SAVE(bWebsiteSubscription[CHAR_FRANKLIN], "REALITY_MILL_FRANKLIN")
		REGISTER_BOOL_TO_SAVE(bWebsiteSubscription[CHAR_TREVOR], "REALITY_MILL_TREVOR")
	STOP_SAVE_ARRAY()
ENDPROC

PROC Register_Array_Of_Photography_Monkey_Flags(INT &iPhotographyMonkeyFlags[2], STRING sNameOfArray) 

	START_SAVE_ARRAY_WITH_SIZE(iPhotographyMonkeyFlags, SIZE_OF(iPhotographyMonkeyFlags), sNameOfArray)
		REGISTER_INT_TO_SAVE(iPhotographyMonkeyFlags[0], "PHOTOGRAPHY_MONKEY_FLAGS_1")
		REGISTER_INT_TO_SAVE(iPhotographyMonkeyFlags[1], "PHOTOGRAPHY_MONKEY_FLAGS_2")
	STOP_SAVE_ARRAY()
	
ENDPROC

PROC Register_Array_Of_Peyote_Pickup_Found_Bitsets(INT &iPeyotePickupFoundBitsets[APT_COUNT], STRING sNameOfArray) 

	START_SAVE_ARRAY_WITH_SIZE(iPeyotePickupFoundBitsets, SIZE_OF(iPeyotePickupFoundBitsets), sNameOfArray)
		INT iBitsetIndex
		REPEAT APT_COUNT iBitsetIndex
			TEXT_LABEL_63 tlName = sNameOfArray
			tlName += "_"
			tlName += iBitsetIndex
			REGISTER_INT_TO_SAVE(iPeyotePickupFoundBitsets[iBitsetIndex], tlName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

#if USE_CLF_DLC
/// PURPOSE:
///    Sets up the save structure for registering all the Ambient globals that need to be saved
PROC Register_ambient_Saved_Globals_CLF()

//	Register_Struct_re_arrest(g_savedGlobals.sAmbient.ReArrest, "RE_ARREST_SAVED_STRUCT")
	Register_Struct_STUNT_JUMPS(g_savedGlobalsClifford.sAmbient.stuntJumps, "STUNT_JUMPS_SAVED_STRUCT")
	Register_Array_Of_Unique_Prostitutes(g_savedGlobalsClifford.sAmbient.sProstituteData, "PROSTITUTE_SAVED_DATA")
	
	Register_Array_Of_Num_Times_Player_Paid_For_Prostitutes(g_savedGlobalsClifford.sAmbient.iNumTimesPlayerPaidForProstituteServices, "PROSTITUTE_SERVICES_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iChopHintsDisplayed, "CHOP_HINTS_DISPLAYED")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iWardrobeHintsDisplayed, "WARDROBE_HINTS_DISPLAYED")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iBridgesFlownUnderFlags, "BRIDGES_FLOWN_UNDER_FLAGS")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iBridgesFlownUnderFlags2, "BRIDGES_FLOWN_UNDER_FLAGS_2")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iBridgesFlownUnderFlags3, "BRIDGES_FLOWN_UNDER_FLAGS_3")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iWildlifePhotographsFlags, "WILDLIFE_PHOTOGRAPHY_FLAGS")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_mission, "CHASE_HELP_TEXT_DISPLAYED_MI")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_RC, "CHASE_HELP_TEXT_DISPLAYED_RC")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_RE, "CHASE_HELP_TEXT_DISPLAYED_RE")
	
	// Random Characters
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iEpsilonCarStage, "EPSCAR_STAGE")								// Epsilon 3
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iEpsilonLastEmailSentDay, "EPSCAR_LAST_EMAIL")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iEpsilonRobesHours, "EPSROBES_ROBES_TIME")						// Epsilon 5
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iEpsilonRobesDeliveryTime, "EPSROBES_ROBES_DELIVERY_TIME")		// Epsilon 5 (Expected Robes Delivery Time)
	REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sAmbient.fDistRunInDesert, "EPSDESERT_DISTANCE_RUN")					// Epsilon 7
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iForSaleSignDestroyedFlags, "FOR_SALE_SIGN_DESTROYED_FLAGS")	// Josh 1

	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iHighScoreFreeMode, "HUNTING_HISCORE_FREE_MODE")				// Ambient Hunting
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iLowestGoldTime, "HUNTING_LOWEST_TIME_GOLD")					// Ambient Hunting
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sAmbient.bChallengeHelpDisplayed, "HUNTING_CHALLENGE_DISPLAYED")		// Ambient Hunting
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sAmbient.todHuntedWeekExp, "HUNTING_WEEK_TIMER")						// Ambient Hunting
	Register_Hunting_Challenge_Array(g_savedGlobalsClifford.sAmbient.iChallengeRankFreeMode, "CHALLENGE_FREE_MODE")		// Ambient Hunting
	
//	tLabel = "HIGH_SCORE_STEALTH_MODE"
//	Register_Hunting_Location_Array(g_SavedGlobals.sAmbient.iHighScoreStealthMode, tLabel)		// Ambient Hunting
//	
//	tLabel = "HIGH_SCORE_TIME_CHALLENGE"
//	Register_Hunting_Location_Array(g_SavedGlobals.sAmbient.iHighScoreTimeChallenge,tLabel )	// Ambient Hunting
//	
//	tLabel = "HIGH_SCORE_KILL_CHALLENGE"
//	Register_Hunting_Location_Array(g_SavedGlobals.sAmbient.iHighScoreKillChallenge, tLabel)	// Ambient Hunting
	
	// Vehicle Repair
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iVehiclesRepairedByFranklin,"VEHICLE_REPAIR_FRANKLIN")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iVehiclesRepairedByMichael,"VEHICLE_REPAIR_MICHAEL")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iVehiclesRepairedByTrevor,"VEHICLE_REPAIR_TREVOR")
	
	// Taxi
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sAmbient.bTaxiHailingHelpDisplayed, "TAXI_HAIL_HELP_PROMPT")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iTaxiEnterPromptDisplayed, "TAXI_ENTER_HELP_PROMPT")
	
	// Letter scraps and spaceship parts
	Register_Struct_GLOBAL_SCRAP_DATA(g_savedGlobalsClifford.sAmbient.sLetterScrapData, "LETTER_SCRAPS_SAVED_STRUCT")
	Register_Struct_GLOBAL_SCRAP_DATA(g_savedGlobalsClifford.sAmbient.sSpaceshipPartData, "SPACESHIP_PARTS_SAVED_STRUCT")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iGetawayState, "FIB4_GETAWAY_STATE")
	
	Register_Website_Subscription_Array(g_savedGlobalsClifford.sAmbient.bWebsiteSubscription, "WEBSITE_SUBSCRIPTION_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iMurderMysteryFlags, "MURDER_MYSTERY_PROGRESS")	// Murder mystery easter egg
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sAmbient.eNoirEffectState, "NOIR_EFFECTS_STATUS")	// Used to hold noir effects status
	Register_Array_Of_Photography_Monkey_Flags(g_savedGlobalsClifford.sAmbient.iPhotographyMonkeyFlags, "PHOTOGRAPHY_MONKEY_FLAGS") // Photography monkey flags

	Register_Array_Of_Peyote_Pickup_Found_Bitsets(g_savedGlobalsClifford.sAmbient.iPeyotePickupOfTypeFound, "PEYOTE_PICKUP_OF_TYPE_FOUND")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sAmbient.iPeyoteAnimalSeen, "PEYOTE_ANIMAL_SEEN")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sAmbient.bPeyoteProgressionComplete, "PEYOTE_PROGRESSION_COMPLETE")
ENDPROC
#ENDIF

#if USE_NRM_DLC
PROC Register_ambient_Saved_Globals_NRM()

//	Register_Struct_re_arrest(g_savedGlobals.sAmbient.ReArrest, "RE_ARREST_SAVED_STRUCT")
	Register_Struct_STUNT_JUMPS(g_savedGlobalsnorman.sAmbient.stuntJumps, "STUNT_JUMPS_SAVED_STRUCT")
	Register_Array_Of_Unique_Prostitutes(g_savedGlobalsnorman.sAmbient.sProstituteData, "PROSTITUTE_SAVED_DATA")
	
	Register_Array_Of_Num_Times_Player_Paid_For_Prostitutes(g_savedGlobalsnorman.sAmbient.iNumTimesPlayerPaidForProstituteServices, "PROSTITUTE_SERVICES_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iChopHintsDisplayed, "CHOP_HINTS_DISPLAYED")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iWardrobeHintsDisplayed, "WARDROBE_HINTS_DISPLAYED")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iBridgesFlownUnderFlags, "BRIDGES_FLOWN_UNDER_FLAGS")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iBridgesFlownUnderFlags2, "BRIDGES_FLOWN_UNDER_FLAGS_2")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iBridgesFlownUnderFlags3, "BRIDGES_FLOWN_UNDER_FLAGS_3")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iWildlifePhotographsFlags, "WILDLIFE_PHOTOGRAPHY_FLAGS")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_mission, "CHASE_HELP_TEXT_DISPLAYED_MI")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_RC, "CHASE_HELP_TEXT_DISPLAYED_RC")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_RE, "CHASE_HELP_TEXT_DISPLAYED_RE")
	
	// Random Characters
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iEpsilonCarStage, "EPSCAR_STAGE")								// Epsilon 3
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iEpsilonLastEmailSentDay, "EPSCAR_LAST_EMAIL")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iEpsilonRobesHours, "EPSROBES_ROBES_TIME")						// Epsilon 5
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iEpsilonRobesDeliveryTime, "EPSROBES_ROBES_DELIVERY_TIME")		// Epsilon 5 (Expected Robes Delivery Time)
	REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sAmbient.fDistRunInDesert, "EPSDESERT_DISTANCE_RUN")					// Epsilon 7
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iForSaleSignDestroyedFlags, "FOR_SALE_SIGN_DESTROYED_FLAGS")	// Josh 1

	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iHighScoreFreeMode, "HUNTING_HISCORE_FREE_MODE")				// Ambient Hunting
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iLowestGoldTime, "HUNTING_LOWEST_TIME_GOLD")					// Ambient Hunting
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sAmbient.bChallengeHelpDisplayed, "HUNTING_CHALLENGE_DISPLAYED")		// Ambient Hunting
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sAmbient.todHuntedWeekExp, "HUNTING_WEEK_TIMER")						// Ambient Hunting
	Register_Hunting_Challenge_Array(g_savedGlobalsnorman.sAmbient.iChallengeRankFreeMode, "CHALLENGE_FREE_MODE")		// Ambient Hunting

	// Vehicle Repair
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iVehiclesRepairedByFranklin,"VEHICLE_REPAIR_FRANKLIN")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iVehiclesRepairedByMichael,"VEHICLE_REPAIR_MICHAEL")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iVehiclesRepairedByTrevor,"VEHICLE_REPAIR_TREVOR")
	
	// Taxi
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sAmbient.bTaxiHailingHelpDisplayed, "TAXI_HAIL_HELP_PROMPT")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iTaxiEnterPromptDisplayed, "TAXI_ENTER_HELP_PROMPT")
	
	// Letter scraps and spaceship parts
	Register_Struct_GLOBAL_SCRAP_DATA(g_savedGlobalsnorman.sAmbient.sLetterScrapData, "LETTER_SCRAPS_SAVED_STRUCT")
	Register_Struct_GLOBAL_SCRAP_DATA(g_savedGlobalsnorman.sAmbient.sSpaceshipPartData, "SPACESHIP_PARTS_SAVED_STRUCT")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iGetawayState, "FIB4_GETAWAY_STATE")
	
	Register_Website_Subscription_Array(g_savedGlobalsnorman.sAmbient.bWebsiteSubscription, "WEBSITE_SUBSCRIPTION_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iMurderMysteryFlags, "MURDER_MYSTERY_PROGRESS")	// Murder mystery easter egg
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sAmbient.eNoirEffectState, "NOIR_EFFECTS_STATUS")	// Used to hold noir effects status
	Register_Array_Of_Photography_Monkey_Flags(g_savedGlobalsnorman.sAmbient.iPhotographyMonkeyFlags, "PHOTOGRAPHY_MONKEY_FLAGS") // Photography monkey flags
	
	Register_Array_Of_Peyote_Pickup_Found_Bitsets(g_savedGlobalsnorman.sAmbient.iPeyotePickupOfTypeFound, "PEYOTE_PICKUP_OF_TYPE_FOUND")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sAmbient.iPeyoteAnimalSeen, "PEYOTE_ANIMAL_SEEN")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sAmbient.bPeyoteProgressionComplete, "PEYOTE_PROGRESSION_COMPLETE")
ENDPROC   
#ENDIF

PROC Register_ambient_Saved_Globals()
#if USE_CLF_DLC
	Register_ambient_Saved_Globals_CLF()
#ENDIF
#if USE_NRM_DLC
	Register_ambient_Saved_Globals_NRM()
#ENDIF
#if not USE_SP_DLC
//	Register_Struct_re_arrest(g_savedGlobals.sAmbient.ReArrest, "RE_ARREST_SAVED_STRUCT")
	Register_Struct_STUNT_JUMPS(g_savedGlobals.sAmbient.stuntJumps, "STUNT_JUMPS_SAVED_STRUCT")
	Register_Array_Of_Unique_Prostitutes(g_savedGlobals.sAmbient.sProstituteData, "PROSTITUTE_SAVED_DATA")
	
	Register_Array_Of_Num_Times_Player_Paid_For_Prostitutes(g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices, "PROSTITUTE_SERVICES_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iChopHintsDisplayed, "CHOP_HINTS_DISPLAYED")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iWardrobeHintsDisplayed, "WARDROBE_HINTS_DISPLAYED")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, "BRIDGES_FLOWN_UNDER_FLAGS")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, "BRIDGES_FLOWN_UNDER_FLAGS_2")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, "BRIDGES_FLOWN_UNDER_FLAGS_3")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iWildlifePhotographsFlags, "WILDLIFE_PHOTOGRAPHY_FLAGS")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_mission, "CHASE_HELP_TEXT_DISPLAYED_MI")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_RC, "CHASE_HELP_TEXT_DISPLAYED_RC")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_RE, "CHASE_HELP_TEXT_DISPLAYED_RE")
	
	// Random Characters
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iEpsilonCarStage, "EPSCAR_STAGE")								// Epsilon 3
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iEpsilonLastEmailSentDay, "EPSCAR_LAST_EMAIL")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iEpsilonRobesHours, "EPSROBES_ROBES_TIME")						// Epsilon 5
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iEpsilonRobesDeliveryTime, "EPSROBES_ROBES_DELIVERY_TIME")		// Epsilon 5 (Expected Robes Delivery Time)
	REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sAmbient.fDistRunInDesert, "EPSDESERT_DISTANCE_RUN")					// Epsilon 7
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iForSaleSignDestroyedFlags, "FOR_SALE_SIGN_DESTROYED_FLAGS")	// Josh 1

	REGISTER_INT_TO_SAVE(g_SavedGlobals.sAmbient.iHighScoreFreeMode, "HUNTING_HISCORE_FREE_MODE")				// Ambient Hunting
	REGISTER_INT_TO_SAVE(g_SavedGlobals.sAmbient.iLowestGoldTime, "HUNTING_LOWEST_TIME_GOLD")					// Ambient Hunting
	REGISTER_BOOL_TO_SAVE(g_SavedGlobals.sAmbient.bChallengeHelpDisplayed, "HUNTING_CHALLENGE_DISPLAYED")		// Ambient Hunting
	REGISTER_ENUM_TO_SAVE(g_SavedGlobals.sAmbient.todHuntedWeekExp, "HUNTING_WEEK_TIMER")						// Ambient Hunting
	Register_Hunting_Challenge_Array(g_SavedGlobals.sAmbient.iChallengeRankFreeMode, "CHALLENGE_FREE_MODE")		// Ambient Hunting
	
//	tLabel = "HIGH_SCORE_STEALTH_MODE"
//	Register_Hunting_Location_Array(g_SavedGlobals.sAmbient.iHighScoreStealthMode, tLabel)		// Ambient Hunting
//	
//	tLabel = "HIGH_SCORE_TIME_CHALLENGE"
//	Register_Hunting_Location_Array(g_SavedGlobals.sAmbient.iHighScoreTimeChallenge,tLabel )	// Ambient Hunting
//	
//	tLabel = "HIGH_SCORE_KILL_CHALLENGE"
//	Register_Hunting_Location_Array(g_SavedGlobals.sAmbient.iHighScoreKillChallenge, tLabel)	// Ambient Hunting
	
	// Vehicle Repair
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iVehiclesRepairedByFranklin,"VEHICLE_REPAIR_FRANKLIN")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iVehiclesRepairedByMichael,"VEHICLE_REPAIR_MICHAEL")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iVehiclesRepairedByTrevor,"VEHICLE_REPAIR_TREVOR")
	
	// Taxi
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sAmbient.bTaxiHailingHelpDisplayed, "TAXI_HAIL_HELP_PROMPT")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iTaxiEnterPromptDisplayed, "TAXI_ENTER_HELP_PROMPT")
	
	// Letter scraps and spaceship parts
	Register_Struct_GLOBAL_SCRAP_DATA(g_savedGlobals.sAmbient.sLetterScrapData, "LETTER_SCRAPS_SAVED_STRUCT")
	Register_Struct_GLOBAL_SCRAP_DATA(g_savedGlobals.sAmbient.sSpaceshipPartData, "SPACESHIP_PARTS_SAVED_STRUCT")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iGetawayState, "FIB4_GETAWAY_STATE")
	
	Register_Website_Subscription_Array(g_savedGlobals.sAmbient.bWebsiteSubscription, "WEBSITE_SUBSCRIPTION_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_SavedGlobals.sAmbient.iMurderMysteryFlags, "MURDER_MYSTERY_PROGRESS")	// Murder mystery easter egg
	REGISTER_ENUM_TO_SAVE(g_SavedGlobals.sAmbient.eNoirEffectState, "NOIR_EFFECTS_STATUS")	// Used to hold noir effects status
	Register_Array_Of_Photography_Monkey_Flags(g_SavedGlobals.sAmbient.iPhotographyMonkeyFlags, "PHOTOGRAPHY_MONKEY_FLAGS") // Photography monkey flags
	
	Register_Array_Of_Peyote_Pickup_Found_Bitsets(g_SavedGlobals.sAmbient.iPeyotePickupOfTypeFound, "PEYOTE_PICKUP_OF_TYPE_FOUND")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sAmbient.iPeyoteAnimalSeen, "PEYOTE_ANIMAL_SEEN")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sAmbient.bPeyoteProgressionComplete, "PEYOTE_PROGRESSION_COMPLETE")
#ENDIF	
ENDPROC
