USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   heist_globals_reg.sch
//      AUTHOR          :   BenR
//      DESCRIPTION     :   Contains the registrations with code for all heist
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


/// PURPOSE:
///    Register the array containing the visited status of each of the heist planning locations.
/// INPUT PARAMS:
///    paramPlanningLocationVisitedArray[]		This instance of the planning location visited status array
///    paramNameOfArray							The name of this instance of the array
PROC Register_Array_PlanningLocationVisited(BOOL &paramPlanningLocationVisitedArray[PLN_MAX_PLANNING_LOCATIONS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramPlanningLocationVisitedArray, SIZE_OF(paramPlanningLocationVisitedArray), paramNameOfArray)

		INT index
		REPEAT PLN_MAX_PLANNING_LOCATIONS index
			SWITCH INT_TO_ENUM(PlanningLocationName, index)
				CASE PLN_STRIPCLUB
					REGISTER_BOOL_TO_SAVE(paramPlanningLocationVisitedArray[index], "Stripclub_Visited")
				BREAK
				
				CASE PLN_SWEATSHOP
					REGISTER_BOOL_TO_SAVE(paramPlanningLocationVisitedArray[index], "Sweatshop_Visited")
				BREAK
				
				CASE PLN_METH_LAB
					REGISTER_BOOL_TO_SAVE(paramPlanningLocationVisitedArray[index], "Meth_Lab_Visited")
				BREAK
				
				CASE PLN_TREV_CITY
					REGISTER_BOOL_TO_SAVE(paramPlanningLocationVisitedArray[index], "Trev_City_Visited")
				BREAK
			ENDSWITCH
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the gameplay selection choice displayed flags.
/// INPUT PARAMS:
///    paramChoiceHelpDisplayedArray[]			This instance of the choice help displayed array
///    paramNameOfArray							The name of this instance of the array
PROC Register_Array_ChoiceHelpDisplayed(BOOL &paramChoiceHelpDisplayedArray[NO_HEISTS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramChoiceHelpDisplayedArray, SIZE_OF(paramChoiceHelpDisplayedArray), paramNameOfArray)

		INT index
		REPEAT NO_HEISTS index
			SWITCH index
				CASE HEIST_JEWEL
					REGISTER_BOOL_TO_SAVE(paramChoiceHelpDisplayedArray[index], "Jewel_Choice_Help_Displayed")
				BREAK
				
				CASE HEIST_DOCKS
					REGISTER_BOOL_TO_SAVE(paramChoiceHelpDisplayedArray[index], "Docks_Choice_Help_Displayed")
				BREAK
				
				CASE HEIST_RURAL_BANK
					REGISTER_BOOL_TO_SAVE(paramChoiceHelpDisplayedArray[index], "Rural_Choice_Help_Displayed")
				BREAK
				
				CASE HEIST_AGENCY
					REGISTER_BOOL_TO_SAVE(paramChoiceHelpDisplayedArray[index], "Agency_Choice_Help_Displayed")
				BREAK
				
				CASE HEIST_FINALE
					REGISTER_BOOL_TO_SAVE(paramChoiceHelpDisplayedArray[index], "Finale_Choice_Help_Displayed")
				BREAK
			ENDSWITCH
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the crew selection choice displayed flags.
/// INPUT PARAMS:
///    paramCrewHelpDisplayedArray[]			This instance of the choice help displayed array
///    paramNameOfArray							The name of this instance of the array
PROC Register_Array_CrewHelpDisplayed(BOOL &paramCrewHelpDisplayedArray[NO_HEISTS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramCrewHelpDisplayedArray, SIZE_OF(paramCrewHelpDisplayedArray), paramNameOfArray)

		INT index
		REPEAT NO_HEISTS index
			SWITCH index
				CASE HEIST_JEWEL
					REGISTER_BOOL_TO_SAVE(paramCrewHelpDisplayedArray[index], "Jewel_Crew_Help_Displayed")
				BREAK
				
				CASE HEIST_DOCKS
					REGISTER_BOOL_TO_SAVE(paramCrewHelpDisplayedArray[index], "Docks_Crew_Help_Displayed")
				BREAK
				
				CASE HEIST_RURAL_BANK
					REGISTER_BOOL_TO_SAVE(paramCrewHelpDisplayedArray[index], "Rural_Crew_Help_Displayed")
				BREAK
				
				CASE HEIST_AGENCY
					REGISTER_BOOL_TO_SAVE(paramCrewHelpDisplayedArray[index], "Agency_Crew_Help_Displayed")
				BREAK
				
				CASE HEIST_FINALE
					REGISTER_BOOL_TO_SAVE(paramCrewHelpDisplayedArray[index], "Finale_Crew_Help_Displayed")
				BREAK
			ENDSWITCH
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the display group visible bitsets.
/// INPUT PARAMS:
///    paramDisplayGroupVisibleBitsetArray[]	This instance of the display group visible bitset array
///    paramNameOfArray							The name of this instance of the array
PROC Register_Array_DisplayGroupVisibleBitset(INT &paramDisplayGroupVisibleBitsetArray[NO_HEISTS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramDisplayGroupVisibleBitsetArray, SIZE_OF(paramDisplayGroupVisibleBitsetArray), paramNameOfArray)

		INT index
		REPEAT NO_HEISTS index
			SWITCH index
				CASE HEIST_JEWEL
					REGISTER_INT_TO_SAVE(paramDisplayGroupVisibleBitsetArray[index], "Jewel_Display_Group_Bitset")
				BREAK
				
				CASE HEIST_DOCKS
					REGISTER_INT_TO_SAVE(paramDisplayGroupVisibleBitsetArray[index], "Docks_Display_Group_Bitset")
				BREAK

				CASE HEIST_RURAL_BANK
					REGISTER_INT_TO_SAVE(paramDisplayGroupVisibleBitsetArray[index], "Rural_Display_Group_Bitset")
				BREAK
				
				CASE HEIST_AGENCY
					REGISTER_INT_TO_SAVE(paramDisplayGroupVisibleBitsetArray[index], "Agency_Display_Group_Bitset")
				BREAK
				
				CASE HEIST_FINALE
					REGISTER_INT_TO_SAVE(paramDisplayGroupVisibleBitsetArray[index], "Finale_Display_Group_Bitset")
				BREAK
			ENDSWITCH
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the selected crew memebers for one heist.
/// INPUT PARAMS:
///    paramSelectedCrew[]			This instance of the selected crew array to be saved
///    paramNameOfArray				The name of this instance of the array
PROC Register_Array_SelectedCrew(CrewMember &paramSelectedCrew[MAX_CREW_SIZE], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramSelectedCrew, SIZE_OF(paramSelectedCrew), paramNameOfArray)

		INT index
		REPEAT MAX_CREW_SIZE index
				TEXT_LABEL_15 tCrewSlotName = "Crew_Slot_"
				tCrewSlotName += index
				REGISTER_ENUM_TO_SAVE(paramSelectedCrew[index], tCrewSlotName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing arrays of selected crew members for each heist.
/// INPUT PARAMS:
///    paramSelectedCrew[][]		This instance of the array of selected crew arrays to be saved
///    paramNameOfArray				The name of this instance of the 2D array
PROC Register_Array_SelectedCrewArrays(CrewMember &paramSelectedCrew[MAX_HEIST_CHOICES][MAX_CREW_SIZE], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramSelectedCrew, SIZE_OF(paramSelectedCrew), paramNameOfArray)

	INT index
	REPEAT MAX_HEIST_CHOICES index
		SWITCH index
			
			CASE HEIST_CHOICE_EMPTY
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_EMPTY")
			BREAK
			
			CASE HEIST_CHOICE_JEWEL_STEALTH
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_JEWEL_STEALTH")
			BREAK
			CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_JEWEL_HIGH_IMPACT")
			BREAK
			
			CASE HEIST_CHOICE_DOCKS_DEEP_SEA
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_DOCKS_DEEP_SEA")
			BREAK
			CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_DOCKS_BLOW_UP_BOAT")
			BREAK
			
			CASE HEIST_CHOICE_RURAL_NO_TANK
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_RURAL_NO_TANK")
			BREAK
			
			CASE HEIST_CHOICE_AGENCY_FIRETRUCK
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_AGENCY_FIRETRUCK")
			BREAK
			CASE HEIST_CHOICE_AGENCY_HELICOPTER
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_AGENCY_HELICOPTER")
			BREAK
			
			CASE HEIST_CHOICE_FINALE_TRAFFCONT
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_FINALE_TRAFFCONT")
			BREAK
			CASE HEIST_CHOICE_FINALE_HELI
				Register_Array_SelectedCrew(paramSelectedCrew[index], "CREW_ARRAY_FINALE_HELI")
			BREAK
			
			DEFAULT
				PRINTLN("Register_Array_SelectedCrewArrays - unhandled case - index = ", index)
				SCRIPT_ASSERT("Register_Array_SelectedCrewArrays - unhandled case - check TTY")
			BREAK
			
		ENDSWITCH
	ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC



/// PURPOSE:
///    Register the struct containing active data for each crew member in the game.
/// INPUT PARAMS:
///    paramCrewActiveData			This instance of the crew active data struct to be saved.
///    paramNameOfStruct			The name of this instance of the struct.
PROC Register_Struct_CrewActiveData(CrewMemberActiveData &paramCrewActiveData, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramCrewActiveData, SIZE_OF(paramCrewActiveData), paramNameOfStruct)
		REGISTER_ENUM_TO_SAVE(paramCrewActiveData.skill, "Skill")
		REGISTER_INT_TO_SAVE(paramCrewActiveData.statsA, "StatsA")
		REGISTER_INT_TO_SAVE(paramCrewActiveData.statsB, "StatsB")
	STOP_SAVE_STRUCT()
	
ENDPROC


/// PURPOSE:
///    Register the array containing structs of active data for each crew member.
/// INPUT PARAMS:
///    paramCrewActiveData[]		This instance of the array of crew active data structs to be saved.
///    paramNameOfArray				The name of this instance of the 2D array
PROC Register_Array_CrewActiveDataStructs(CrewMemberActiveData &paramCrewActiveData[CM_MAX_CREW_MEMBERS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramCrewActiveData, SIZE_OF(paramCrewActiveData), paramNameOfArray)
		INT index
		REPEAT CM_MAX_CREW_MEMBERS index
			TEXT_LABEL_31 tStructName = "CREW_ACTIVE_DATA_STRUCT_"
			tStructName += index
			Register_Struct_CrewActiveData(paramCrewActiveData[index], tStructName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the expense descriptions (TEXT_LABEL_63s) for a specific heist.
/// INPUT PARAMS:
///    paramExpenseDescriptionArray[]		This instance of the expense descriptions array to be saved
///    paramExpenseDescriptionArrayName		The name of this instance of the array
PROC Register_Array_ExpenseDescriptions(TEXT_LABEL_63 &paramExpenseDescriptionArray[MAX_END_SCREEN_EXPENSES], STRING paramExpenseDescriptionArrayName)

	START_SAVE_ARRAY_WITH_SIZE(paramExpenseDescriptionArray, SIZE_OF(paramExpenseDescriptionArray), paramExpenseDescriptionArrayName)

		INT index
		REPEAT MAX_END_SCREEN_EXPENSES index
			TEXT_LABEL_15 tTextLabelName = "Description_"
			tTextLabelName += index
			REGISTER_TEXT_LABEL_63_TO_SAVE(paramExpenseDescriptionArray[index], tTextLabelName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
ENDPROC


/// PURPOSE:
///    Register the array containing the expense values for a specific heist.
/// INPUT PARAMS:
///    paramExpenseValueArray[]			This instance of the expense value array to be saved
///    paramExpenseValueArrayName		The name of this instance of the array
PROC Register_Array_ExpenseValues(INT &paramExpenseValueArray[MAX_END_SCREEN_EXPENSES], STRING paramExpenseValueArrayName)
	
	START_SAVE_ARRAY_WITH_SIZE(paramExpenseValueArray, SIZE_OF(paramExpenseValueArray), paramExpenseValueArrayName)

		INT index
		REPEAT MAX_END_SCREEN_EXPENSES index
			TEXT_LABEL_15 tIntName = "Value_"
			tIntName += index
			REGISTER_INT_TO_SAVE(paramExpenseValueArray[index], tIntName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the player take values for each of the three player 
///    characters.
/// INPUT PARAMS:
///    paramPlayerTakeArray[]		This instance of the player take array to be saved
///    paramPlayerTakeArrayName		The name of this instance of the array
PROC Register_Array_PlayerTake(INT &paramPlayerTakeArray[3], STRING paramPlayerTakeArrayName)

	START_SAVE_ARRAY_WITH_SIZE(paramPlayerTakeArray, SIZE_OF(paramPlayerTakeArray), paramPlayerTakeArrayName)
		INT index
		REPEAT 3 index
			TEXT_LABEL_15 tIntName = "Plyr_Take_"
			tIntName += index
			REGISTER_INT_TO_SAVE(paramPlayerTakeArray[index], tIntName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the player percentage values for each of the three player 
///    characters.
/// INPUT PARAMS:
///    paramPlayerPercentageArray[]		This instance of the player percentage array to be saved
///    paramPlayerPercentageArrayName		The name of this instance of the array
PROC Register_Array_PlayerPercentage(FLOAT &paramPlayerPercentageArray[3], STRING paramPlayerPercentageArrayName)

	START_SAVE_ARRAY_WITH_SIZE(paramPlayerPercentageArray, SIZE_OF(paramPlayerPercentageArray), paramPlayerPercentageArrayName)
		INT index
		REPEAT 3 index
			TEXT_LABEL_15 tIntName = "Plyr_Perc_"
			tIntName += index
			REGISTER_FLOAT_TO_SAVE(paramPlayerPercentageArray[index], tIntName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the ending status of all crew members who accompanied
///    the player on a specific heist.
/// INPUT PARAMS:
///    paramCrewStatusArray[]		This instance of the crew status array to be saved
///    paramNameOfArray				The name of this instance of the array
PROC Register_Array_CrewStatus(CrewMemberStatus &paramCrewStatusArray[MAX_CREW_SIZE], STRING paramCrewStatusArrayName)

	START_SAVE_ARRAY_WITH_SIZE(paramCrewStatusArray, SIZE_OF(paramCrewStatusArray), paramCrewStatusArrayName)

		INT index
		REPEAT MAX_CREW_SIZE index
				TEXT_LABEL_15 tEnumName = "Crew_Status_"
				tEnumName += index
				REGISTER_ENUM_TO_SAVE(paramCrewStatusArray[index], tEnumName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the amount of money each crew member takes.
/// INPUT PARAMS:
///    paramCrewTakesArray[]		This instance of the crew takes array to be saved
///    paramNameOfArray				The name of this instance of the array
PROC Register_Array_CrewTakes(INT &paramCrewTakesArray[MAX_CREW_SIZE], STRING paramCrewTakesArrayName)

	START_SAVE_ARRAY_WITH_SIZE(paramCrewTakesArray, SIZE_OF(paramCrewTakesArray), paramCrewTakesArrayName)

		INT index
		REPEAT MAX_CREW_SIZE index
				TEXT_LABEL_15 tIntName = "Crew_Take_"
				tIntName += index
				REGISTER_INT_TO_SAVE(paramCrewTakesArray[index], tIntName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the struct containing end screen data for a single heist. End screen data
///    is saved so that there is a permanent record of how well a player did on a specific heist.
/// INPUT PARAMS:
///    paramEndScreenData			This instance of the end screen data struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_EndScreenData(HeistEndScreenData &paramEndScreenData, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramEndScreenData, SIZE_OF(paramEndScreenData), paramNameOfStruct)

	REGISTER_INT_TO_SAVE(paramEndScreenData.iPotentialTake, "Potential_Take")
	REGISTER_INT_TO_SAVE(paramEndScreenData.iActualTake, "Actual_Take")
	REGISTER_INT_TO_SAVE(paramEndScreenData.iTimeTaken, "Time_Taken")
	Register_Array_PlayerTake(paramEndScreenData.iPlayerTake, "HEIST_END_SCREEN_PLAYER_TAKE")
	Register_Array_PlayerPercentage(paramEndScreenData.fPlayerPercentage, "HEIST_END_SCREEN_PLAYER_PERC")

	Register_Array_CrewStatus(paramEndScreenData.eCrewStatus, "HEIST_END_SCREEN_CREW_STATUS")
	Register_Array_CrewTakes(paramEndScreenData.iCrewMemberTake, "HEIST_END_SCREEN_CREW_TAKE")

	//REGISTER_INT_TO_SAVE(paramEndScreenData.iMaxItems, "Max_Item_Count")
	//REGISTER_INT_TO_SAVE(paramEndScreenData.iItemCount, "Item_Count")
	//Register_Array_ItemDescriptions(paramEndScreenData.tItemDescription, "HEIST_END_SCREEN_ITEM_DESC")
	//Register_Array_ItemValues(paramEndScreenData.iItemValue, "HEIST_END_SCREEN_ITEM_VALUE")
	
	STOP_SAVE_STRUCT()
	
ENDPROC


/// PURPOSE:
///    Register the array containing the heist end screen data structs for each heist. End screen data
///    is saved so that there is a permanent record of how well a player did on a specific heist.
/// INPUT PARAMS:
///    paramEndScreenData[]			This instance of the end screen data array to be saved
///    paramNameOfArray				The name of this instance of the array
PROC Register_Array_EndScreenStructs(HeistEndScreenData &paramEndScreenData[NO_HEISTS], STRING paramNameOfArray)
	
	START_SAVE_ARRAY_WITH_SIZE(paramEndScreenData, SIZE_OF(paramEndScreenData), paramNameOfArray)

	INT index
	REPEAT NO_HEISTS index
		SWITCH index
			CASE HEIST_JEWEL
				Register_Struct_EndScreenData(paramEndScreenData[index], "HEIST_END_SCREEN_JEWEL")
			BREAK
			
			CASE HEIST_DOCKS
				Register_Struct_EndScreenData(paramEndScreenData[index], "HEIST_END_SCREEN_DOCKS")
			BREAK
			
			CASE HEIST_RURAL_BANK
				Register_Struct_EndScreenData(paramEndScreenData[index], "HEIST_END_SCREEN_RURAL_BANK")
			BREAK
			
			CASE HEIST_AGENCY
				Register_Struct_EndScreenData(paramEndScreenData[index], "HEIST_END_SCREEN_AGENCY")
			BREAK
			
			CASE HEIST_FINALE
				Register_Struct_EndScreenData(paramEndScreenData[index], "HEIST_END_SCREEN_FINALE")
			BREAK
		ENDSWITCH
	ENDREPEAT
	STOP_SAVE_ARRAY()
	
ENDPROC


/// PURPOSE:
///    Sets up the save structure for registering all the heist globals that need to be saved.
PROC Register_Heist_Saved_Globals()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sHeistData, SIZE_OF(g_savedGlobals.sHeistData), "HEIST_DATA_STRUCT")
	
	Register_Array_ChoiceHelpDisplayed(g_savedGlobals.sHeistData.bChoiceHelpDisplayed, "HEIST_CHOICE_HELP_DISPLAYED_ARRAY")
	Register_Array_CrewHelpDisplayed(g_savedGlobals.sHeistData.bCrewHelpDisplayed, "HEIST_CREW_HELP_DISPLAYED_ARRAY")
	Register_Array_SelectedCrewArrays(g_savedGlobals.sHeistData.eSelectedCrew, "HEIST_SELECTED_CREW_ARRAY")
	Register_Array_CrewActiveDataStructs(g_savedGlobals.sHeistData.sCrewActiveData, "HEIST_CREW_ACTIVE_DATA_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sHeistData.iCrewUnlockedBitset, "Crew_Unlocked_Bitset")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sHeistData.iCrewUsedBitset, "Crew_Used_Bitset")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sHeistData.iCrewDeadBitset, "Crew_Dead_Bitset")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sHeistData.iCrewDialogueBitset, "Crew_Dialogue_Bitset")
	Register_Array_DisplayGroupVisibleBitset(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset, "HEIST_DISPLAY_GROUP_VISIBLE_ARRAY")
	Register_Array_EndScreenStructs(g_savedGlobals.sHeistData.sEndScreenData, "HEIST_END_SCREEN_DATA_STRUCT")
	
	STOP_SAVE_STRUCT()

ENDPROC


