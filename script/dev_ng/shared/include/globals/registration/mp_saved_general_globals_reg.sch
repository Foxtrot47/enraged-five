USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   mp_saved_buddies_globals_reg.sch
//      AUTHOR          :   Ryan B
//      DESCRIPTION     :   Contains the registrations with code for all MP general
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register the array containing the General saved variables
PROC Register_Array_Of_Saved_MP_GENERAL(MP_SAVED_GENERAL_STRUCT &savedGeneral, STRING paramNameOfArray)

	INT i
	TEXT_LABEL_15 tlLabel
	//INT iNonSaveDataSize = SIZE_OF(savedGeneral.iMultiPropertyValueArray)
	START_SAVE_STRUCT_WITH_SIZE(savedGeneral, SIZE_OF(savedGeneral), paramNameOfArray)
		REGISTER_INT_TO_SAVE(savedGeneral.iCashGivenTotal, "CASH_GIVEN_TOTAL")
		REGISTER_INT_TO_SAVE(savedGeneral.iCashGivenTime, "CASH_GIVEN_TIME")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastSavedCarUsed, "LAST_SAVED_CAR")
		//REGISTER_INT_TO_SAVE(savedGeneral.iLastFreemodeDataLBMaintenance, "LAST_LB_MAINTAIN")
		REGISTER_INT_TO_SAVE(savedGeneral.iCurrentPropertyValue, "CURRENT_PROP_VALUE")
		REGISTER_INT_TO_SAVE(savedGeneral.iNewVehPurchased, "iNewVehPurchased")
		REGISTER_INT_TO_SAVE(savedGeneral.iWheelieDayTimer, "WHEELIE_TIMER")
		REGISTER_INT_TO_SAVE(savedGeneral.iWheelieUpdatesThisDay, "WHEELIE_UPDATES")
		REGISTER_INT_TO_SAVE(savedGeneral.iWheelieTimeAtStartOfDay, "WHEELIE_TIME")
		REGISTER_BOOL_TO_SAVE(savedGeneral.bGrabCurrentWheelieTime, "GRAB_TIME")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastSoldVehicleTime, "iLastSoldVehicleTime")
		REGISTER_INT_TO_SAVE(savedGeneral.ilasttimeplayed, "ilasttimeplayed")
		REGISTER_INT_TO_SAVE(savedGeneral.iSaveCoupons, "iSaveCoupons")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iLastBruciePillReminder, "iLastBruciePillReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastSecVanReminder, "iLastSecVanReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastBountyReminder, "iLastBountyReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastParaReminder, "iLastParaReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastCrateDropReminder, "iLastCrateDropReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastGangAttackReminder, "iLastGangAttackReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastImpExpReminder, "iLastImpExpReminder") 
		REGISTER_INT_TO_SAVE(savedGeneral.iLastInsuranceReminder, "iLastInsuranceReminder") 
		REGISTER_INT_TO_SAVE(savedGeneral.iLastSurvivalReminder, "iLastSurvivalReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastBikerBackupReminder, "iLastBikerBackupReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastVagosBackupReminder, "iLastVagosBackupReminder") 
		REGISTER_INT_TO_SAVE(savedGeneral.iLastLesterVehReminder, "iLastLesterVehReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastPersonalVehDeliveryReminder, "iLastPersonalVehDeliveryReminder") 
		REGISTER_INT_TO_SAVE(savedGeneral.iLastPegasusVehicleReminder, "iLastPegasusVehicleReminder") 
		REGISTER_INT_TO_SAVE(savedGeneral.iLastMerryweatherReminder, "iLastMerryweatherReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastLesterHelpReminder, "iLastLesterHelpReminder")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iLastLesterReqJobReminder, "iLastLesterReqJobReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastGeraldReqJobReminder, "iLastGeraldReqJobReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastSimeonReqJobReminder, "iLastSimeonReqJobReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastMartinReqJobReminder, "iLastMartinReqJobReminder")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastRonReqJobReminder, "iLastRonReqJobReminder")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iRecentlyPassedMissionBitset, "iRecentlyPassedMissionBitset")
		REGISTER_INT_TO_SAVE(savedGeneral.iRecentlyPassedMissionTime, "iRecentlyPassedMissionTime")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iLastImportExportDelTime, "iLastImportExportDelTime")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastImportExportListTime, "iLastImportExportListTime") 
		REGISTER_INT_TO_SAVE(savedGeneral.iMyLastImportExportListDay, "iMyLastImportExportListDay")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iVehicleWebsiteReminderTime, "iVehicleWebsiteReminderTime")
		
		REGISTER_BOOL_TO_SAVE(savedGeneral.bDefaultClothesSet, "bDefaultClothesSet")
				
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iClothesViewedBitset, SIZE_OF(savedGeneral.iClothesViewedBitset), "CLOTHES")
			REPEAT COUNT_OF(savedGeneral.iClothesViewedBitset) i
				tlLabel = "CLOTHES"
				tlLabel += i
				REGISTER_INT_TO_SAVE(savedGeneral.iClothesViewedBitset[i], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iCarmodsViewedBitset, SIZE_OF(savedGeneral.iCarmodsViewedBitset), "CARMODS")
			REPEAT COUNT_OF(savedGeneral.iCarmodsViewedBitset) i
				tlLabel = "CARMODS"
				tlLabel += i
				REGISTER_INT_TO_SAVE(savedGeneral.iCarmodsViewedBitset[i], tlLabel)	
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_INT_TO_SAVE(savedGeneral.iLastCrewCharWasIn,"iLastCrewCharWasIn")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iStripperUnlockedBS,"iStripperUnlockedBS")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iGeneralBS,"iGeneralBS")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iCarsModifiedTimeStamp,"iCarsModifiedTimeStamp")
	
		
		INT iIndex
		TEXT_LABEL_63 tempLabel
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.fLongestDriveHole, SIZE_OF(savedGeneral.fLongestDriveHole), "GOLF_fLongestDriveHole")
			REPEAT COUNT_OF(savedGeneral.fLongestDriveHole) iIndex
				tempLabel = "fLongestDriveHole_"
				tempLabel += iIndex
				REGISTER_FLOAT_TO_SAVE(savedGeneral.fLongestDriveHole[iIndex], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.fLongestPuttHole, SIZE_OF(savedGeneral.fLongestPuttHole), "GOLF_fLongestPuttHole")
			REPEAT COUNT_OF(savedGeneral.fLongestPuttHole) iIndex
				tempLabel = "fLongestPuttHole_"
				tempLabel += iIndex
				REGISTER_FLOAT_TO_SAVE(savedGeneral.fLongestPuttHole[iIndex], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()

		REGISTER_INT_TO_SAVE(savedGeneral.iBestRound, "GOLF_iBestRound")
		REGISTER_FLOAT_TO_SAVE(savedGeneral.fLongestDrive, "GOLF_fLongestDrive")
		REGISTER_FLOAT_TO_SAVE(savedGeneral.fLongestPutt, "GOLF_fLongestPutt")
		
		REGISTER_INT_TO_SAVE(savedGeneral.iShareLJCashTotal, "iShareLJCashTotal")
		REGISTER_INT_TO_SAVE(savedGeneral.iShareLJCashTime, "iShareLJCashTime")
		REGISTER_INT_TO_SAVE(savedGeneral.iReceiveLJCashTotal, "iReceiveLJCashTotal")
		REGISTER_INT_TO_SAVE(savedGeneral.iReceiveLJCashTime, "iReceiveLJCashTime")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastAccessedPropertyID,"LAST_ACC_PROP")
		REGISTER_INT_TO_SAVE(savedGeneral.iMultiProperty1Value,"MULTI1_PROP_VAL")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastAccessedSimpleInteriorID,"LAST_ACC_SMPLINT")
		
		INT iSlot
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iMultiPropertyValueArray, SIZE_OF(savedGeneral.iMultiPropertyValueArray), "PROP_ARY_VAL")
			REPEAT COUNT_OF(savedGeneral.iMultiPropertyValueArray) iSlot
				tlLabel = "PROP_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.iMultiPropertyValueArray[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_INT_TO_SAVE(savedGeneral.iPatchUpdatesForKenneth,"KR_PATCH_UPDATE")
		
		iSlot = 0
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iDLCCarmodItemsViewed, SIZE_OF(savedGeneral.iDLCCarmodItemsViewed), "DLC_MODS")
			REPEAT COUNT_OF(savedGeneral.iDLCCarmodItemsViewed) iSlot
				tlLabel = "DLC_MODS_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.iDLCCarmodItemsViewed[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		iSlot = 0
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iDLCSupermodItemsViewed, SIZE_OF(savedGeneral.iDLCSupermodItemsViewed), "DLC_SUPERMODS")
			REPEAT COUNT_OF(savedGeneral.iDLCSupermodItemsViewed) iSlot
				tlLabel = "DLC_SMODS_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.iDLCSupermodItemsViewed[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		INT iComp
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_CompDraw, SIZE_OF(savedGeneral.iSavedOutfit_CompDraw), "OUTFIT_CompDraw")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CompDraw"
				tlLabel += "_"
				tlLabel += iSlot
				START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_CompDraw[iSlot], SIZE_OF(savedGeneral.iSavedOutfit_CompDraw[iSlot]), tlLabel)
					REPEAT 12 iComp
						tlLabel = "CompDraw"
						tlLabel += "_"
						tlLabel += iSlot
						tlLabel += "_"
						tlLabel += iComp
						REGISTER_INT_TO_SAVE(savedGeneral.iSavedOutfit_CompDraw[iSlot][iComp], tlLabel)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_CompTex, SIZE_OF(savedGeneral.iSavedOutfit_CompTex), "OUTFIT_CompTex")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CompTex"
				tlLabel += "_"
				tlLabel += iSlot
				START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_CompTex[iSlot], SIZE_OF(savedGeneral.iSavedOutfit_CompTex[iSlot]), tlLabel)
					REPEAT 12 iComp
						tlLabel = "CompTex"
						tlLabel += "_"
						tlLabel += iSlot
						tlLabel += "_"
						tlLabel += iComp
						REGISTER_INT_TO_SAVE(savedGeneral.iSavedOutfit_CompTex[iSlot][iComp], tlLabel)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_PropID, SIZE_OF(savedGeneral.iSavedOutfit_PropID), "OUTFIT_PropID")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "PropID"
				tlLabel += "_"
				tlLabel += iSlot
				START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_PropID[iSlot], SIZE_OF(savedGeneral.iSavedOutfit_PropID[iSlot]), tlLabel)
					REPEAT 9 iComp
						tlLabel = "PropID"
						tlLabel += "_"
						tlLabel += iSlot
						tlLabel += "_"
						tlLabel += iComp
						REGISTER_INT_TO_SAVE(savedGeneral.iSavedOutfit_PropID[iSlot][iComp], tlLabel)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_PropTex, SIZE_OF(savedGeneral.iSavedOutfit_PropTex), "OUTFIT_PropTex")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "PropTex"
				tlLabel += "_"
				tlLabel += iSlot
				START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_PropTex[iSlot], SIZE_OF(savedGeneral.iSavedOutfit_PropTex[iSlot]), tlLabel)
					REPEAT 9 iComp
						tlLabel = "PropTex"
						tlLabel += "_"
						tlLabel += iSlot
						tlLabel += "_"
						tlLabel += iComp
						REGISTER_INT_TO_SAVE(savedGeneral.iSavedOutfit_PropTex[iSlot][iComp], tlLabel)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_Stored, SIZE_OF(savedGeneral.bSaveOutfit_Stored), "OUTFIT_Stored")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "Stored"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_Stored[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_TatCrewA, SIZE_OF(savedGeneral.bSaveOutfit_TatCrewA), "OUTFIT_CrewTatA")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CrewDecalA"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_TatCrewA[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_TatCrewB, SIZE_OF(savedGeneral.bSaveOutfit_TatCrewB), "OUTFIT_CrewTatB")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CrewDecalB"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_TatCrewB[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_TatCrewC, SIZE_OF(savedGeneral.bSaveOutfit_TatCrewC), "OUTFIT_CrewTatC")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CrewDecalC"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_TatCrewC[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_TatCrewD, SIZE_OF(savedGeneral.bSaveOutfit_TatCrewD), "OUTFIT_CrewTatD")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CrewDecalD"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_TatCrewD[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_TatCrewE, SIZE_OF(savedGeneral.bSaveOutfit_TatCrewE), "OUTFIT_CrewTatE")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CrewDecalE"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_TatCrewE[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.bSaveOutfit_TatCrewF, SIZE_OF(savedGeneral.bSaveOutfit_TatCrewF), "OUTFIT_CrewTatF")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "CrewDecalF"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.bSaveOutfit_TatCrewF[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iSavedOutfit_ShirtDecal, SIZE_OF(savedGeneral.iSavedOutfit_ShirtDecal), "OUTFIT_Shirt")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "ShirtDecal"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.iSavedOutfit_ShirtDecal[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.tlSavedOutfit_Name, SIZE_OF(savedGeneral.tlSavedOutfit_Name), "OUTFIT_Name")
			REPEAT MAX_CUSTOM_OUTFIT_SLOTS iSlot
				tlLabel = "Name"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_TEXT_LABEL_31_TO_SAVE(savedGeneral.tlSavedOutfit_Name[iSlot], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		TEXT_LABEL_15 arrayName
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.gamerFromLastJob, SIZE_OF(savedGeneral.gamerFromLastJob), "LAST_JobGamer")
			REPEAT MAX_LAST_JOB_GAMERS iSlot
				tlLabel = "LastJobG64_"
				tlLabel += iSlot
				
				arrayName = tlLabel
				arrayName += "_1"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data1, arrayName)
				arrayName = tlLabel
				arrayName += "_2"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data2, arrayName)
				arrayName = tlLabel
				arrayName += "_3"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data3, arrayName)
				arrayName = tlLabel
				arrayName += "_4"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data4, arrayName)
				arrayName = tlLabel
				arrayName += "_5"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data5, arrayName)
				arrayName = tlLabel
				arrayName += "_6"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data6, arrayName)
				arrayName = tlLabel
				arrayName += "_7"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data7, arrayName)
				arrayName = tlLabel
				arrayName += "_8"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data8, arrayName)
				arrayName = tlLabel
				arrayName += "_9"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data9, arrayName)
				arrayName = tlLabel
				arrayName += "_10"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data10, arrayName)
				arrayName = tlLabel
				arrayName += "_11"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data11, arrayName)
				arrayName = tlLabel
				arrayName += "_12"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data12, arrayName)
				arrayName = tlLabel
				arrayName += "_13"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastJob[iSlot].Data13, arrayName)

			ENDREPEAT
		STOP_SAVE_ARRAY()
			
		//Daily Objectives
		REGISTER_BOOL_TO_SAVE(savedGeneral.bCompletedDailyObjectives,"DO_CompletedObjectives")
		REGISTER_BOOL_TO_SAVE(savedGeneral.bLoggedInToday,"DO_LoggedInToday")
		REGISTER_INT_TO_SAVE(savedGeneral.iDailyObjectiveXValue,"DO_xValue")
		REGISTER_INT_TO_SAVE(savedGeneral.iLastHistoryLength,"DO_LastHistoryLength")
		REGISTER_INT_TO_SAVE(savedGeneral.LastResetTime,"DO_LastResetTime")
	
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.Current_Daily_Objectives,	SIZE_OF(savedGeneral.Current_Daily_Objectives),"DO_Objectives")
			REPEAT NUM_MP_DAILY_OBJECTIVES iSlot
				tlLabel = "Objective"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.Current_Daily_Objectives[iSlot].iObjective,tlLabel)
				tlLabel = "Completed"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_BOOL_TO_SAVE(savedGeneral.Current_Daily_Objectives[iSlot].bCompleted,tlLabel)
				tlLabel = "Initial"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.Current_Daily_Objectives[iSlot].iDailyObjectiveInitialValue,tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
			
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iObjectiveHistory,SIZE_OF(savedGeneral.iObjectiveHistory),"DO_History")
			REPEAT MAX_DO_HISTORY_SIZE iSlot
				tlLabel = "History"
				tlLabel += "_"
				tlLabel += iSlot
				REGISTER_INT_TO_SAVE(savedGeneral.iObjectiveHistory[iSlot],tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		// Textlabels for the last job names of players
		TEXT_LABEL_15 arrayNameTL
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.gamerFromLastJobTL, SIZE_OF(savedGeneral.gamerFromLastJobTL), "LAST_JobGamer_TL")
			REPEAT MAX_LAST_JOB_GAMERS iSlot
				tlLabel = "LastJobTL_"
				tlLabel += iSlot
				
				arrayNameTL = tlLabel
				arrayNameTL += "_1"
				REGISTER_TEXT_LABEL_63_TO_SAVE(savedGeneral.gamerFromLastJobTL[iSlot], arrayNameTL)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iExtraFlagsForBgScripts, SIZE_OF(savedGeneral.iExtraFlagsForBgScripts), "BGSAVEINT")
			REPEAT 10 i
				tlLabel = "BGSAVEINT"
				tlLabel += i
				REGISTER_INT_TO_SAVE(savedGeneral.iExtraFlagsForBgScripts[i], tlLabel)	
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.iPropLibraryViewed, SIZE_OF(savedGeneral.iPropLibraryViewed), "PROPLIB")
			REPEAT COUNT_OF(savedGeneral.iPropLibraryViewed)  i
				tlLabel = "PROPLIB"
				tlLabel += i
				REGISTER_INT_TO_SAVE(savedGeneral.iPropLibraryViewed[i], tlLabel)	
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		TEXT_LABEL_15 arrayNameGO
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.gamerFromLastGangopsMission, SIZE_OF(savedGeneral.gamerFromLastGangopsMission), "LAST_GOGamer")
			REPEAT MAX_LAST_GANGOPS_GAMERS iSlot
				tlLabel = "LastGOG64_"
				tlLabel += iSlot
				
				arrayNameGO = tlLabel
				arrayNameGO += "_1"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data1, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_2"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data2, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_3"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data3, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_4"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data4, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_5"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data5, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_6"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data6, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_7"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data7, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_8"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data8, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_9"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data9, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_10"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data10, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_11"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data11, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_12"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data12, arrayNameGO)
				arrayNameGO = tlLabel
				arrayNameGO += "_13"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangopsMission[iSlot].Data13, arrayNameGO)

			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		TEXT_LABEL_15 arrayNameGJ
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.gamerFromLastGangOpsJob, SIZE_OF(savedGeneral.gamerFromLastGangOpsJob), "LAST_GOJobGamer")
			REPEAT MAX_LAST_GANGOPS_GAMERS iSlot
				tlLabel = "LastGJG64_"
				tlLabel += iSlot
				
				arrayNameGJ = tlLabel
				arrayNameGJ += "_1"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data1, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_2"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data2, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_3"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data3, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_4"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data4, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_5"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data5, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_6"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data6, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_7"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data7, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_8"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data8, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_9"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data9, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_10"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data10, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_11"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data11, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_12"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data12, arrayNameGJ)
				arrayNameGJ = tlLabel
				arrayNameGJ += "_13"
				REGISTER_INT64_TO_SAVE(savedGeneral.gamerFromLastGangOpsJob[iSlot].Data13, arrayNameGJ)

			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		TEXT_LABEL_15 arrayNameGJTL
		START_SAVE_ARRAY_WITH_SIZE(savedGeneral.gamerFromLastGangOpsJobTL, SIZE_OF(savedGeneral.gamerFromLastGangOpsJobTL), "LAST_GOJobGamer_TL")
			REPEAT MAX_LAST_JOB_GAMERS iSlot
				tlLabel = "LastGJTL_"
				tlLabel += iSlot
				
				arrayNameGJTL = tlLabel
				arrayNameGJTL += "_1"
				REGISTER_TEXT_LABEL_63_TO_SAVE(savedGeneral.gamerFromLastGangOpsJobTL[iSlot], arrayNameGJTL)
			ENDREPEAT
		STOP_SAVE_ARRAY()
			
	STOP_SAVE_ARRAY()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the General globals that need to be saved
PROC REGISTER_MP_GENERAL_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
	TEXT_LABEL_23 tlSlot = 	"MP_GENERAL_SAVED"
	tlSlot += iIndex
	Register_Array_Of_Saved_MP_GENERAL(globalData.MpSavedGeneral, tlSlot)

ENDPROC
