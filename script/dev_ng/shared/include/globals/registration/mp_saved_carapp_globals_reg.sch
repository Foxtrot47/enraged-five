USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   mp_saved_carapp_globals.reg.sch
//      AUTHOR          :   Kenneth Ross
//      DESCRIPTION     :   Contains the registrations with code for all social globals
//							that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

PROC Register_Struct_CarAppOrder_Slot(SOCIAL_CAR_APP_ORDER_DATA &paramCarAppSavedVars, STRING paramNameOfStruct)	

	START_SAVE_STRUCT_WITH_SIZE(paramCarAppSavedVars, SIZE_OF(paramCarAppSavedVars), paramNameOfStruct)
	
		// Vehicle setup
		REGISTER_ENUM_TO_SAVE(paramCarAppSavedVars.eModel, "Model")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColourID1, "iColourID1")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColourID2, "iColourID2")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColour1Group, "iColour1Group")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColour2Group, "iColour2Group")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWindowTint, "iWindowTint")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeR, "iTyreSmokeR")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeG, "iTyreSmokeG")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeB, "iTyreSmokeB")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iEngine, "iEngine")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iBrakes, "iBrakes")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWheels, "iWheels")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWheelType, "iWheelType")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iExhaust, "iExhaust")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iSuspension, "iSuspension")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iArmour, "iArmour")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iHorn, "iHorn")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iLights, "iLights")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bBulletProofTyres, "bBulletProofTyres")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTurbo, "iTurbo")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmoke, "iTyreSmoke")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramCarAppSavedVars.tlPlateText, "tlPlateText")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iPlateBack, "iPlateBack")
		
		// Cloud data
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iUID, "UID")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iCost, "Cost")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramCarAppSavedVars.tlPlateText_pending, "tlPlateText_pending")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iPlateBack_pending, "iPlateBack_pending")
		
		// Game data
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bProcessOrder, "bProcessOrder")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderPending, "bOrderPending")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderReceivedOnBoot, "bOrderReceivedOnBoot")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderForPlayerVehicle, "bOrderForPlayerVehicle")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bCheckPlateProfanity, "bCheckPlateProfanity")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderPaidFor, "bOrderPaidFor")
		
		
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bSCProfanityFailed, "bSCProfanityFailed")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderFailedFunds, "bOrderFailedFunds")
	
	STOP_SAVE_STRUCT()
ENDPROC

/// PURPOSE:
///    Register the struct containing the carapp data
/// INPUT PARAMS:
///    paramCarAppSavedVars			This instance of the MP Saved CarApp Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_MP_CarAppData(MP_SAVED_CARAPP_STRUCT &paramCarAppSavedVars, INT iIndex)

	TEXT_LABEL_63 tlSlot
	INT iSlot
	
	START_SAVE_ARRAY_WITH_SIZE(paramCarAppSavedVars.bCarHiddenInApp, SIZE_OF(paramCarAppSavedVars.bCarHiddenInApp), "CAR_HIDDEN")
		REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iSlot
			tlSlot = "CAR_HIDDEN"
			tlSlot += iSlot
			REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bCarHiddenInApp[iSlot], tlSlot)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
	Register_Struct_CarAppOrder_Slot(paramCarAppSavedVars.sCarAppOrder, "CAR_APP_ORDER")
	
	tlSlot = "bUpdateMods"
	tlSlot += iIndex
	REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bUpdateMods, tlSlot)
	tlSlot = "bMultiplayerDataWiped"
	tlSlot += iIndex
	REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bMultiplayerDataWiped, tlSlot)
	tlSlot = "bCarAppPlateSet"
	tlSlot += iIndex
	REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bCarAppPlateSet, tlSlot)
	tlSlot = "bDeleteCarData"
	tlSlot += iIndex
	REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bDeleteCarData, tlSlot)
	tlSlot = "iCarAppPlateBack"
	tlSlot += iIndex
	REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iCarAppPlateBack, tlSlot)
	tlSlot = "tlCarAppPlateText"
	tlSlot += iIndex
	REGISTER_TEXT_LABEL_15_TO_SAVE(paramCarAppSavedVars.tlCarAppPlateText,tlSlot )
	tlSlot = "iOrderCount"
	tlSlot += iIndex
	REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iOrderCount,tlSlot )	
	tlSlot = "iOrderVehicle"
	tlSlot += iIndex
	REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iOrderVehicle,tlSlot )	
	
	START_SAVE_ARRAY_WITH_SIZE(paramCarAppSavedVars.iProcessSlot, SIZE_OF(paramCarAppSavedVars.iProcessSlot), "SAVED_VEH_SLOT")
		REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iSlot
			tlSlot = "VEH_SLOT"
			tlSlot += iSlot
			REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iProcessSlot[iSlot], tlSlot)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	START_SAVE_ARRAY_WITH_SIZE(paramCarAppSavedVars.iSlotPriority, SIZE_OF(paramCarAppSavedVars.iSlotPriority), "SAVED_VEH_PRIO")
		REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iSlot
			tlSlot = "VEH_PRIO"
			tlSlot += iSlot
			REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iSlotPriority[iSlot], tlSlot)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iLastSavedVehUsed, "LAST_USED")
	REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iNewSavedVehToProcess, "NEW_SAVED")
	REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bNewSetupInitialised, "SETUP_INIT")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the saved carapp globals that need to be saved
PROC REGISTER_MP_CARAPP_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
		Register_Struct_MP_CarAppData(globalData.MpSavedCarApp, iIndex)
ENDPROC


