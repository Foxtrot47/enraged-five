USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   shop_globals_reg.sch
//      AUTHOR          :   Kenneth Ross
//      DESCRIPTION     :   Contains the registrations with code for all shop globals
//							that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Save the Properties array
/// INPUT PARAMS:
///    paramIntIDsArray			The instance of the Array of Properties to be saved
///    paramNameOfArray			The name of this instance of the Properties Array
PROC Register_Array_Of_ShopProperties(INT &paramPropertiesArray[NUMBER_OF_SHOPS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramPropertiesArray, SIZE_OF(paramPropertiesArray), paramNameOfArray)

		TEXT_LABEL_31 PropertiesName
		
		REPEAT NUMBER_OF_SHOPS tempLoop
			PropertiesName = "SHOP_"
			PropertiesName += tempLoop
			REGISTER_INT_TO_SAVE(paramPropertiesArray[tempLoop], PropertiesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the struct containing the shop carmod data
/// INPUT PARAMS:
///    paramShopSavedVars			This instance of the Carmod Repair Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_CarmodData(VehicleService &paramCarmodVars, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramCarmodVars, SIZE_OF(paramCarmodVars), paramNameOfStruct)
		REGISTER_BOOL_TO_SAVE(paramCarmodVars.bProcessing, "PROCESSING")
		REGISTER_BOOL_TO_SAVE(paramCarmodVars.bReadyForCollection, "READY")
		REGISTER_BOOL_TO_SAVE(paramCarmodVars.bMessageSent, "MESSAGE_SENT")
		REGISTER_INT_TO_SAVE(paramCarmodVars.iHoursToComplete, "HOURS_TO_COMPLETE")
		REGISTER_INT_TO_SAVE(paramCarmodVars.iType, "TYPE")
	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Register the struct containing the shop properties INT and setup BOOL
/// INPUT PARAMS:
///    paramShopSavedVars			This instance of the Shop Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_ShopData(ShopDataSaved &paramShopSavedVars, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramShopSavedVars, SIZE_OF(paramShopSavedVars), paramNameOfStruct)
		Register_Array_Of_ShopProperties(paramShopSavedVars.iProperties, 	"SHOP_PROPERTIES")
		REGISTER_BOOL_TO_SAVE(paramShopSavedVars.bDefaultDataSet, 	"SHOP_DEFAULT_DATA_SET")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iShopTypesVisited, 	"SHOP_TYPES_VISITED")
		
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iHairdoShopVisits, 	"iHairdoShopVisits")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iClothesShopVisits, "iClothesShopVisits")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iTattooShopVisits, "iTattooShopVisits")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iCarmodShopVisits, "iCarmodShopVisits")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iGunShopVisits, "iGunShopVisits")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iGunShopHelpCount, "iGunShopHelpCount")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iGunShopPostTrev2Dialogue, "iGunShopPostTrev2Dialogue")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iGunShopPostLamar1Dialogue, "iGunShopPostLamar1Dialogue")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iClothesShopPostLester1Dialogue, "iClothesShopPostLester1Dialogue")
		
		START_SAVE_ARRAY_WITH_SIZE(paramShopSavedVars.sVehicleService, SIZE_OF(paramShopSavedVars.sVehicleService), "CARMOD_DATA")
			Register_Struct_CarmodData(paramShopSavedVars.sVehicleService[CHAR_MICHAEL], "P0_CARMOD_DATA")
			Register_Struct_CarmodData(paramShopSavedVars.sVehicleService[CHAR_FRANKLIN], "P1_CARMOD_DATA")
			Register_Struct_CarmodData(paramShopSavedVars.sVehicleService[CHAR_TREVOR], "P2_CARMOD_DATA")
		STOP_SAVE_ARRAY()
		
		REGISTER_BOOL_TO_SAVE(paramShopSavedVars.bFirstModShopRepairComplete, "FIRST_MODSHOP_REPAIR")
		
		REGISTER_BOOL_TO_SAVE(paramShopSavedVars.bBarberBlipHelpShown, "SHOP_BARBER_BLIP_HELP_SHOWN")
		REGISTER_BOOL_TO_SAVE(paramShopSavedVars.bArmourHelpShown, "SHOP_ARMOUR_HELP_SHOWN")
		
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iContentChecks_Vehicles, 	"CONTENT_VEHICLES")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iContentChecks_Weapons, 	"CONTENT_WEAPONS")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iContentChecks_Clothes, 	"CONTENT_CLOTHES")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iContentChecks_Hairdos, 	"CONTENT_HAIRDOS")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iContentChecks_Tattoos, 	"CONTENT_TATTOOS")
		REGISTER_INT_TO_SAVE(paramShopSavedVars.iContentChecks_Game, 		"CONTENT_GAME")
		
		START_SAVE_ARRAY_WITH_SIZE(paramShopSavedVars.iCarmodsViewedBitset, SIZE_OF(paramShopSavedVars.iCarmodsViewedBitset), "VIEWED_MODS")
			INT i
			TEXT_LABEL_15 tlLabel
			REPEAT COUNT_OF(paramShopSavedVars.iCarmodsViewedBitset) i
				tlLabel = "VIEWED_MODS"
				tlLabel += i
				REGISTER_INT_TO_SAVE(paramShopSavedVars.iCarmodsViewedBitset[i], tlLabel)	
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		
	STOP_SAVE_STRUCT()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the shop globals that need to be saved
PROC Register_Shop_Saved_Globals()
	
	Register_Struct_ShopData(g_savedGlobals.sShopData, "SHOP_SAVED_DATA_STRUCT")
	
ENDPROC
PROC Register_Shop_Saved_Globals_CLF()
	
	Register_Struct_ShopData(g_savedGlobalsClifford.sShopData, "SHOP_SAVED_DATA_STRUCT")
	
ENDPROC


