USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   charsheet_globals_reg.sch
//      AUTHOR          :   Steve Taylor
//      DESCRIPTION     :   Contains the registrations with code for all charsheet globals
//                          that require or may require to be saved
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





/// PURPOSE:
///    Register all the variables in the struct containing saved variables for each index of the character sheet struct array.
/// PARAMS:
///    IndividualStructEntry        This instance of the character sheet struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
PROC Register_Individual_Struct_CharSheet(structCharacterSheet &IndividualStructEntry, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(IndividualStructEntry, SIZE_OF(IndividualStructEntry), paramNameOfStruct)
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.game_model, "game_model")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - game model: ", IndividualStructEntry.game_model)

        REGISTER_INT_TO_SAVE (IndividualStructEntry.alphaint, "alpha_int")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - alpha_int: ", IndividualStructEntry.alphaint)
		
        REGISTER_INT_TO_SAVE (IndividualStructEntry.original_alphaint, "orig_alpha_int")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - original_alphaint: ", IndividualStructEntry.original_alphaint)
		
        REGISTER_TEXT_LABEL_TO_SAVE (IndividualStructEntry.label, "char_label")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - label: ", IndividualStructEntry.label)
        
        //Removed in a effort to reduce global usage.
        //REGISTER_TEXT_LABEL_TO_SAVE (IndividualStructEntry.role, "char_role")

        REGISTER_TEXT_LABEL_TO_SAVE (IndividualStructEntry.picture, "char_picture")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - picture: ", IndividualStructEntry.picture)

        //Removed - does not need to be a saved global. Steve T - 06.05.13
        //REGISTER_ENUM_TO_SAVE (IndividualStructEntry.phone, "phone_contact")
        
        //Removed - does not need to be a saved global. Steve T - 06.05.13
        //REGISTER_ENUM_TO_SAVE (IndividualStructEntry.email, "email_contact")

        //Removed - does not need to be a saved global. Steve T - 06.05.13
        //REGISTER_TEXT_LABEL_TO_SAVE (IndividualStructEntry.ansphone_labelroot, "ansphone_label")

        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.friend, "is_friend")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - friend: ", IndividualStructEntry.friend)
        
        //Removed in a effort to reduce global usage.
        //REGISTER_ENUM_TO_SAVE (IndividualStructEntry.familyMember, "is_family")




        START_SAVE_ARRAY_WITH_SIZE(IndividualStructEntry.PhoneBookState, SIZE_OF(IndividualStructEntry.PhoneBookState), "PhoneBookStateArray")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.PhoneBookState[0], "PhoneBook_State_ToMichael")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.PhoneBookState[1], "PhoneBook_State_ToFranklin")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.PhoneBookState[2], "PhoneBook_State_ToTrevor")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.PhoneBookState[3], "PhoneBook_State_ToMP")

        STOP_SAVE_ARRAY()
		
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - PhoneBookState[0]: ", IndividualStructEntry.PhoneBookState[0])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - PhoneBookState[1]: ", IndividualStructEntry.PhoneBookState[1])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - PhoneBookState[2]: ", IndividualStructEntry.PhoneBookState[2])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - PhoneBookState[3]: ", IndividualStructEntry.PhoneBookState[3])


        //Removed - does not need to be a saved global. Steve T - 06.05.13
        //REGISTER_TEXT_LABEL_TO_SAVE (IndividualStructEntry.phonebookNumberLabel, "phonebook_label")
        
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.bank_account, "bank_acc")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - bank_account: ", IndividualStructEntry.bank_account)
		
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.picmsgStatus, "Picmsg_Status")
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - picmsgStatus: ", IndividualStructEntry.picmsgStatus)

       
        START_SAVE_ARRAY_WITH_SIZE(IndividualStructEntry.missedCallStatus, SIZE_OF(IndividualStructEntry.missedCallStatus), "MissedCallStatusArray")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.missedCallStatus[0], "MissedCallStatus_ToMichael")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.missedCallStatus[1], "MissedCallStatus_ToFranklin")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.missedCallStatus[2], "MissedCallStatus_ToTrevor")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.missedCallStatus[3], "MissedCallStatus_ToMP")

        STOP_SAVE_ARRAY()
		
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - missedCallStatus[0]: ", IndividualStructEntry.missedCallStatus[0])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - missedCallStatus[1]: ", IndividualStructEntry.missedCallStatus[1])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - missedCallStatus[2]: ", IndividualStructEntry.missedCallStatus[2])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - missedCallStatus[3]: ", IndividualStructEntry.missedCallStatus[3])


        START_SAVE_ARRAY_WITH_SIZE(IndividualStructEntry.statusAsCaller, SIZE_OF(IndividualStructEntry.statusAsCaller), "StatusAsCallerArray")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.statusAsCaller[0], "StatusAsCaller_ToMichael")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.statusAsCaller[1], "StatusAsCaller_ToFranklin")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.statusAsCaller[2], "StatusAsCaller_ToTrevor")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.statusAsCaller[3], "StatusAsCaller_ToMP")

        STOP_SAVE_ARRAY()
		
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - statusAsCaller[0]: ", IndividualStructEntry.statusAsCaller[0])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - statusAsCaller[1]: ", IndividualStructEntry.statusAsCaller[1])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - statusAsCaller[2]: ", IndividualStructEntry.statusAsCaller[2])
		PRINTLN("[CHAR_SHEET_SAVING] Register_Individual_Struct_CharSheet - statusAsCaller[3]: ", IndividualStructEntry.statusAsCaller[3])


        /* Removed by Steve T. 12/11/12
        START_SAVE_ARRAY_WITH_SIZE(IndividualStructEntry.ActivityStatus, SIZE_OF(IndividualStructEntry.activityStatus), "ActivityStatus")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.ActivityStatus[0], "ActivityStatus_ToMichael")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.ActivityStatus[1], "ActivityStatus_ToFranklin")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.ActivityStatus[2], "ActivityStatus_ToTrevor")
            REGISTER_ENUM_TO_SAVE (IndividualStructEntry.ActivityStatus[3], "ActivityStatus_ToMP")

        STOP_SAVE_ARRAY()
        */





    STOP_SAVE_STRUCT()

ENDPROC





/// PURPOSE:
///    Register the array containing the saved variables for each index of the character sheet struct array 
/// INPUT PARAMS
///    paramCharSheetArray      The instance of the Array of Char Street Structs to be saved
///    paramNameOfArray         The name of this instance of the Char Sheet Array

PROC Register_Array_Of_CharSheetsCLF(structCharacterSheet &paramCharSheetArray[MAX_CHARACTERS_SP_SAVE], STRING paramNameOfArray)

    INT i_arrayIndex= 0   
    START_SAVE_ARRAY_WITH_SIZE(paramCharSheetArray, SIZE_OF(paramCharSheetArray), paramNameOfArray)
        TEXT_LABEL_31 CharSheetName  
        WHILE i_arrayIndex < ENUM_TO_INT(MAX_CHARACTERS_SP_SAVE)  
            CharSheetName = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[i_arrayIndex].label 
            //the charsheet label should point to a unique text label within american_cellphone.txt. We can use this as the root of a unique identifier.

            CharSheetName += "_SavedCLF"  
			CharSheetName += i_arrayIndex
			
            //Register details for this index of the array of char sheet structs
            Register_Individual_Struct_CharSheet(paramCharSheetArray[i_arrayIndex], CharSheetName)
            i_arrayIndex ++
        ENDWHILE      
    STOP_SAVE_ARRAY()


ENDPROC
PROC Register_Array_Of_CharSheetsNRM(structCharacterSheet &paramCharSheetArray[MAX_CHARACTERS_SP_SAVE], STRING paramNameOfArray)
   
    INT i_arrayIndex= 0    
    START_SAVE_ARRAY_WITH_SIZE(paramCharSheetArray, SIZE_OF(paramCharSheetArray), paramNameOfArray)
        TEXT_LABEL_31 CharSheetName       
        
        WHILE i_arrayIndex < ENUM_TO_INT(MAX_CHARACTERS_SP_SAVE)  
            CharSheetName = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[i_arrayIndex].label 
            //the charsheet label should point to a unique text label within american_cellphone.txt. We can use this as the root of a unique identifier. 
            CharSheetName += "_SavedNRM"
			CharSheetName += i_arrayIndex
            //Register details for this index of the array of char sheet structs
            Register_Individual_Struct_CharSheet(paramCharSheetArray[i_arrayIndex], CharSheetName)
            i_arrayIndex ++
        ENDWHILE       
    STOP_SAVE_ARRAY()
ENDPROC
PROC Register_Array_Of_CharSheets(structCharacterSheet &paramCharSheetArray[MAX_CHARACTERS_SP_SAVE], STRING paramNameOfArray)

   
    INT i_arrayIndex= 0

    
    START_SAVE_ARRAY_WITH_SIZE(paramCharSheetArray, SIZE_OF(paramCharSheetArray), paramNameOfArray)

        TEXT_LABEL_31 CharSheetName
        
        
        WHILE i_arrayIndex < ENUM_TO_INT(MAX_CHARACTERS_SP_SAVE)
            
      
            CharSheetName = g_SavedGlobals.sCharSheetData.g_CharacterSheet[i_arrayIndex].label 
            //the charsheet label should point to a unique text label within american_cellphone.txt. We can use this as the root of a unique identifier. 
			PRINTLN("[CHAR_SHEET_SAVING] Register_Array_Of_CharSheets - saving: ", g_SavedGlobals.sCharSheetData.g_CharacterSheet[i_arrayIndex].label )
            CharSheetName += "_Saved"


    
            //Register details for this index of the array of char sheet structs
            Register_Individual_Struct_CharSheet(paramCharSheetArray[i_arrayIndex], CharSheetName)

            i_arrayIndex ++

        ENDWHILE
        

    STOP_SAVE_ARRAY()


ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved char sheet variables for a character
/// PARAMS:
///    paramCharSheetSaved       The root structs of the data to be saved
///    paramNameOfStruct            The name of this instance of the struct
PROC Register_Struct_CharSheetSavedCLF(g_CharSheetSavedData &paramCharSheetSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCharSheetSaved, SIZE_OF(paramCharSheetSaved), paramNameOfStruct)
        Register_Array_Of_CharSheetsCLF(paramCharSheetSaved.g_CharacterSheet, "struct_g_CharacterSheet")
    STOP_SAVE_STRUCT()

ENDPROC
PROC Register_Struct_CharSheetSavedNRM(g_CharSheetSavedData &paramCharSheetSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCharSheetSaved, SIZE_OF(paramCharSheetSaved), paramNameOfStruct)
        Register_Array_Of_CharSheetsNRM(paramCharSheetSaved.g_CharacterSheet, "struct_g_CharacterSheet")
    STOP_SAVE_STRUCT()

ENDPROC
PROC Register_Struct_CharSheetSaved(g_CharSheetSavedData &paramCharSheetSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCharSheetSaved, SIZE_OF(paramCharSheetSaved), paramNameOfStruct)
        Register_Array_Of_CharSheets(paramCharSheetSaved.g_CharacterSheet, "struct_g_CharacterSheet")
    STOP_SAVE_STRUCT()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering the specific char sheet globals that need to be saved

/// PURPOSE:
///    Sets up the save structure for registering the specific char sheet globals that need to be saved
PROC Register_CharSheet_Saved_Globals_CLF()
    Register_Struct_CharSheetSavedCLF(g_savedGlobalsClifford.sCharSheetData, "CHARSHEET_SAVED_ARRAY")
ENDPROC
PROC Register_CharSheet_Saved_Globals_NRM()
    Register_Struct_CharSheetSavedNRM(g_savedGlobalsnorman.sCharSheetData, "CHARSHEET_SAVED_ARRAY")
ENDPROC
PROC Register_CharSheet_Saved_Globals()
    Register_Struct_CharSheetSaved(g_savedGlobals.sCharSheetData, "CHARSHEET_SAVED_ARRAY")
ENDPROC

