// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   darts_globals_reg.sch
//      AUTHOR          :   Lino Manansala
//      DESCRIPTION     :   Contains the registrations with code for all darts
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the darts globals that need to be saved
PROC Register_Darts_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sDartsData, SIZE_OF(g_savedGlobals.sDartsData), "DARTS_SAVED_STRUCT")
		
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iNumBullseyes, "DARTS_iNumBullseyes")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iNumOneEighties, "DARTS_iNumOneEighties")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iDartGamesWon, "DARTS_iDartGamesWon")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iDartGamesLoss, "DARTS_iDartGamesLoss")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iDartsThrown, "DARTS_iDartsThrown")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iTotalDartGames, "DARTS_iTotalDartGames")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sDartsData.iDartTimePlayed, "DARTS_iDartTimePlayed")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDartsData.fWinPct, "DARTS_fWinPct")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDartsData.fAvgDartsPerMatch, "DARTS_fAvgDartsPerMatch")
		
	STOP_SAVE_STRUCT()
ENDPROC

