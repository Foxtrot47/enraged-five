// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   range_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all shooting range
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Save the actual rounds for the range.
PROC Register_Array_Of_SRange_Rounds(RangeRoundDataSaved & sRounds[RT_MAX_ROUND_TYPES], STRING szNameOfArray)
	INT iTemp = 0

	START_SAVE_ARRAY_WITH_SIZE(sRounds, SIZE_OF(sRounds), szNameOfArray)

		REPEAT RT_MAX_ROUND_TYPES iTemp
			TEXT_LABEL_31 szRoundSavedName = "SRANGE_ROUND"
			szRoundSavedName += iTemp
			TEXT_LABEL_31 szTemp = szRoundSavedName
			
			szTemp += "_Z1HITS"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iZone1Hit, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_Z2HITS"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iZone2Hit, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_Z3HITS"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iZone3Hit, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_Z4HITS"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iZone4Hit, szTemp)
			szTemp = szRoundSavedName
		
			
			szTemp += "_LASTSCORE"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iLastScore, szTemp)
			szTemp = szRoundSavedName
			
//			szTemp += "_HIGHSCORE"
//			REGISTER_INT_TO_SAVE(sRounds[iTemp].iHighScore, szTemp)
//			szTemp = szRoundSavedName
			
			szTemp += "_UNLOCKED"
			REGISTER_BOOL_TO_SAVE(sRounds[iTemp].bUnlocked, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_PASSED"
			REGISTER_BOOL_TO_SAVE(sRounds[iTemp].bPassed, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_MEDAL"
			REGISTER_ENUM_TO_SAVE(sRounds[iTemp].eTopMedal, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_WEAPTYPE"
			REGISTER_ENUM_TO_SAVE(sRounds[iTemp].eWeaponType, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_FIRED"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iShotsFired, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_HITS"
			REGISTER_INT_TO_SAVE(sRounds[iTemp].iShotsHit, szTemp)
			szTemp = szRoundSavedName
			
			szTemp += "_ACCURACY"
			REGISTER_FLOAT_TO_SAVE(sRounds[iTemp].fAccuracy, szTemp)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC


/// PURPOSE:
///    Sets up the save structure for registering all the shooting range globals that need to be saved
PROC Register_ShootingRange_Saved_Globals()
	INT iLoopCounter = 0
	
	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sRangeData, SIZE_OF(g_savedGlobals.sRangeData), "SHOOTING_RANGE_SAVED_ARRAY")
		TEXT_LABEL_31 sRangeSavedName
		
		REPEAT 3 iLoopCounter
			IF iLoopCounter = 0
				sRangeSavedName = "RANGE_SAVED_MICHAEL"
			ELIF iLoopCounter = 1
				sRangeSavedName = "RANGE_SAVED_FRANKLIN"
			ELIF iLoopCounter = 2
				sRangeSavedName = "RANGE_SAVED_TREVOR"
			ENDIF
			
			START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sRangeData[iLoopCounter], SIZE_OF(g_savedGlobals.sRangeData[iLoopCounter]), sRangeSavedName)
				REGISTER_INT_TO_SAVE(g_savedGlobals.sRangeData[iLoopCounter].iCatChalLockStatus, "SRANGE_iCategoryChallengeLockStatus")
				REGISTER_INT_TO_SAVE(g_savedGlobals.sRangeData[iLoopCounter].iWeaponLockStatus, "SRANGE_iWeaponsLockStatus")
				REGISTER_INT_TO_SAVE(g_savedGlobals.sRangeData[iLoopCounter].iWeaponUsedStatus, "SRANGE_iWeaponUsedStatus")
				
				Register_Array_Of_SRange_Rounds(g_savedGlobals.sRangeData[iLoopCounter].sRounds, "SRANGE_ROUND_ARRAY")
				
				REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRangeData[iLoopCounter].m_bSeenTutorial, "SRANGE_bHasSeenTutorial")
				REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRangeData[iLoopCounter].bClearedAll, "SRANGE_bClearedAll")
				REGISTER_INT_TO_SAVE(g_savedGlobals.sRangeData[iLoopCounter].iBools, "SRANGE_iBoolBits")
			STOP_SAVE_STRUCT()
			
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

