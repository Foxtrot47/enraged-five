USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "player_ped_public.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   respawn_location_globals_reg.sch
//      AUTHOR          :   Kenneth
//      DESCRIPTION     :   Contains the registrations with code for all respawn location  
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Save the array of items
/// INPUT PARAMS:
///    paramIntArray		The instance of the array of ints to be saved
///    paramNameOfArray		The name of this instance of the int array

PROC Register_Array_Of_Savehouse_Properties(INT &paramIntArray[NUMBER_OF_SAVEHOUSE_LOCATIONS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramIntArray, SIZE_OF(paramIntArray), paramNameOfArray)

		TEXT_LABEL_31 TempName
		
		REPEAT COUNT_OF(paramIntArray) tempLoop
			TempName = paramNameOfArray
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramIntArray[tempLoop], TempName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Save the array of items
/// INPUT PARAMS:
///    paramIntArray		The instance of the array of ints to be saved
///    paramNameOfArray		The name of this instance of the int array
PROC Register_Array_Of_Police_Station_Properties(INT &paramIntArray[NUMBER_OF_POLICE_STATION_LOCATIONS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramIntArray, SIZE_OF(paramIntArray), paramNameOfArray)

		TEXT_LABEL_31 TempName
		
		REPEAT COUNT_OF(paramIntArray) tempLoop
			TempName = paramNameOfArray
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramIntArray[tempLoop], TempName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Save the array of items
/// INPUT PARAMS:
///    paramIntArray		The instance of the array of ints to be saved
///    paramNameOfArray		The name of this instance of the int array
PROC Register_Array_Of_Hospital_Properties(INT &paramIntArray[NUMBER_OF_HOSPITAL_LOCATIONS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramIntArray, SIZE_OF(paramIntArray), paramNameOfArray)

		TEXT_LABEL_31 TempName
		
		REPEAT COUNT_OF(paramIntArray) tempLoop
			TempName = paramNameOfArray
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramIntArray[tempLoop], TempName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC




/// PURPOSE:
///    Register the struct containing the respawn location data
/// INPUT PARAMS:
///    paramRespawnDataSaved		This instance of the Control Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct

PROC Register_Struct_RespawnDataSaved(RespawnDataSaved &paramRespawnDataSaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramRespawnDataSaved, SIZE_OF(paramRespawnDataSaved), paramNameOfStruct)
		
		Register_Array_Of_Savehouse_Properties(paramRespawnDataSaved.iSavehouseProperties, "SAVEHOUSE")
		Register_Array_Of_Police_Station_Properties(paramRespawnDataSaved.iPoliceStationProperties, "POLICE_STATION")
		Register_Array_Of_Hospital_Properties(paramRespawnDataSaved.iHospitalProperties, "HOSPITAL")
		
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bDefaultSavehouseDataSet, "SAVEHOUSE_DATA_SET")
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bDefaultHospitalDataSet, "HOSPITAL_DATA_SET")
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bDefaultPoliceStationDataSet, "POLICE_DATA_SET")
		
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bSeenFirstTimeWasted, "bSeenFirstTimeWasted")
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bSeenFirstTimeDrowned, "bSeenFirstTimeDrowned")
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bSeenFirstTimeBusted, "bSeenFirstTimeBusted")
		
		REGISTER_BOOL_TO_SAVE(paramRespawnDataSaved.bNewGameStarted, "bNewGameStarted")
	STOP_SAVE_STRUCT()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the respawn location globals that need to be saved

/// PURPOSE:
///    Sets up the save structure for registering all the respawn location globals that need to be saved
#if USE_CLF_DLC
PROC Register_Respawn_Location_Saved_Globals_CLF()
	Register_Struct_RespawnDataSaved(g_savedGlobalsClifford.sRespawnData, "RESPAWN_LOCATION_DATA")
ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Respawn_Location_Saved_Globals_NRM()
	Register_Struct_RespawnDataSaved(g_savedGlobalsnorman.sRespawnData, "RESPAWN_LOCATION_DATA")
ENDPROC
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Respawn_Location_Saved_Globals()
	Register_Struct_RespawnDataSaved(g_savedGlobals.sRespawnData, "RESPAWN_LOCATION_DATA")
ENDPROC
#endif
#endif

