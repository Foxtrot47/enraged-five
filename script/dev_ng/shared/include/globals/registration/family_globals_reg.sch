USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   family_globals_reg.sch
//      AUTHOR          :   Alwyn R
//      DESCRIPTION     :   Contains the registrations with code for family
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


/// PURPOSE:
///    Register the array containing the saved variables for each family
/// INPUT PARAMS
///    paramFamilyArray		The instance of the Array of family Structs to be saved
///    paramNameOfArray			The name of this instance of the family Array
PROC Register_Array_Of_FamilyEvents(enumFamilyEvents &paramFamilyEventsArray[MAX_FAMILY_MEMBER], STRING paramNameOfArray)

	enumFamilyMember eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramFamilyEventsArray, SIZE_OF(paramFamilyEventsArray), paramNameOfArray)

		TEXT_LABEL_31 FamilyName
		
		REPEAT MAX_FAMILY_MEMBER eLoop
			SWITCH (eLoop)
				CASE FM_MICHAEL_SON
					FamilyName = "FM_MICHAEL_SON"
					BREAK

				CASE FM_MICHAEL_DAUGHTER
					FamilyName = "FM_MICHAEL_DAUGHTER"
					BREAK

				CASE FM_MICHAEL_WIFE
					FamilyName = "FM_MICHAEL_WIFE"
					BREAK

				CASE FM_MICHAEL_MEXMAID
					FamilyName = "FM_MICHAEL_MEXMAID"
					BREAK

				CASE FM_MICHAEL_GARDENER
					FamilyName = "FM_MICHAEL_GARDENER"
					BREAK


				CASE FM_FRANKLIN_AUNT
					FamilyName = "FM_FRANKLIN_AUNT"
					BREAK

				CASE FM_FRANKLIN_LAMAR
					FamilyName = "FM_FRANKLIN_LAMAR"
					BREAK

				CASE FM_FRANKLIN_STRETCH
					FamilyName = "FM_FRANKLIN_STRETCH"
					BREAK

//				CASE FM_FRANKLIN_DOG
//					FamilyName = "FM_FRANKLIN_DOG"
//					BREAK


				CASE FM_TREVOR_0_RON
					FamilyName = "FM_TREVOR_0_RON"
					BREAK

				CASE FM_TREVOR_0_MICHAEL
					FamilyName = "FM_TREVOR_0_MICHAEL"
					BREAK

				CASE FM_TREVOR_0_TREVOR
					FamilyName = "FM_TREVOR_0_TREVOR"
					BREAK

				CASE FM_TREVOR_0_WIFE
					FamilyName = "FM_TREVOR_0_WIFE"
					BREAK

				CASE FM_TREVOR_0_MOTHER
					FamilyName = "FM_TREVOR_0_MOTHER"
					BREAK


				CASE FM_TREVOR_1_FLOYD
					FamilyName = "FM_TREVOR_1_FLOYD"
					BREAK

				CASE FM_TREVOR_1_WADE
					FamilyName = "FM_TREVOR_1_WADE"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_FamilyEvents - case missing for a family")
					FamilyName = "Family_Sheet_"
					FamilyName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the familys Array
			REGISTER_ENUM_TO_SAVE(paramFamilyEventsArray[eLoop], FamilyName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved family variables for a character
/// PARAMS:
///    paramFamilySaved			This instance of the family data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_FamilySaved(g_FamilySavedData &paramFamilySaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramFamilySaved, SIZE_OF(paramFamilySaved), paramNameOfStruct)

		Register_Array_Of_FamilyEvents(paramFamilySaved.ePreviousFamilyEvent, "ePreviousFamilyEvent")
		REGISTER_BOOL_TO_SAVE(paramFamilySaved.bInitialisedPreviousEvents, "bInitialisedPreviousEvents")
		REGISTER_BOOL_TO_SAVE(paramFamilySaved.bSeenFamWeaponDisplay, "bSeenFamWeaponDisplay")
		REGISTER_BOOL_TO_SAVE(paramFamilySaved.bHeardTrevorCountry, "bHeardTrevorCountry")
	STOP_SAVE_STRUCT()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the family globals that need to be saved
PROC Register_Family_Saved_Globals()

	Register_Struct_FamilySaved(g_savedGlobals.sFamilyData, "FAMILY_SAVED_DATA")

ENDPROC


