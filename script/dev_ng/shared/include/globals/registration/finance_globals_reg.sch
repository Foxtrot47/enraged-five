USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   finance_globals_reg.sch
//      AUTHOR          :   Andrew K
//      DESCRIPTION     :   Contains the registrations for finance system globals that are
//							saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



PROC RegisterIndexedInt(INT &value, INT index, STRING base)
	
	TEXT_LABEL t = base
	
	t += index
	
	REGISTER_INT_TO_SAVE(value, t)
	
ENDPROC

PROC RegisterIndexedFloat(FLOAT &value, INT index, STRING base)
	
	TEXT_LABEL t = base
	
	t += index
	
	REGISTER_FLOAT_TO_SAVE(value, t)
	
ENDPROC
PROC Register_Struct_FinanceSaved(FinanceDataSaved &paramFinanceSaved, STRING paramNameOfStruct)
/*
	START_SAVE_STRUCT(paramFriendsSaved, paramNameOfStruct)
		Register_Array_Of_FriendSheets(paramFriendsSaved.g_FriendSheet, "g_FriendSheet")
		Register_Array_Of_FriendConnections(paramFriendsSaved.g_FriendConnectData, "g_FriendConnectData")
		Register_Struct_Timer(paramFriendsSaved.g_nextContact, "g_nextContact")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneCanCancel, "g_bHelpDoneCanCancel")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bCalledToCancelOnce, "g_bCalledToCancelOnce")
	STOP_SAVE_STRUCT()
*/

	START_SAVE_STRUCT_WITH_SIZE(paramFinanceSaved, SIZE_OF(paramFinanceSaved), paramNameOfStruct)
	
		//registration of owned stock entries for each char
		INT i
		//mike
		//INT MIKE_TOTAL_OWNED
		//INT MIKE_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		//FLOAT MIKE_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		//INT MIKE_SHARES_NUMBER_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		
		
		//REGISTER_INT_TO_SAVE(paramFinanceSaved.MIKE_TOTAL_OWNED, "MIKE_TOTAL_OWNED_SHARES")
		
		i = 0
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.MIKE_SHARES_COMPANY_INDEX, SIZE_OF(paramFinanceSaved.MIKE_SHARES_COMPANY_INDEX), "M_S_CI_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedInt(paramFinanceSaved.MIKE_SHARES_COMPANY_INDEX[i],i,"M_CI_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		i = 0
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.MIKE_SHARES_INVESTED, SIZE_OF(paramFinanceSaved.MIKE_SHARES_INVESTED), "M_S_I_AR")
		
		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedFloat(paramFinanceSaved.MIKE_SHARES_INVESTED[i],i,"M_I_")
		ENDREPEAT
		STOP_SAVE_ARRAY()

		i = 0

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.MIKE_SHARES_OWNED, SIZE_OF(paramFinanceSaved.MIKE_SHARES_OWNED), "M_S_OOT_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedInt(paramFinanceSaved.MIKE_SHARES_OWNED[i],i,"M_OOT_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		//franklin
		//INT FRANKLIN_TOTAL_OWNED
		//INT FRANKLIN_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		//FLOAT FRANKLIN_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		//INT FRANKLIN_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		
		//REGISTER_INT_TO_SAVE(paramFinanceSaved.FRANKLIN_TOTAL_OWNED, "FRANKLIN_TOTAL_OWNED_SHARES")
		
		i = 0

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.FRANKLIN_SHARES_COMPANY_INDEX, SIZE_OF(paramFinanceSaved.FRANKLIN_SHARES_COMPANY_INDEX), "F_S_CI_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedInt(paramFinanceSaved.FRANKLIN_SHARES_COMPANY_INDEX[i],i,"F_CI_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		i = 0
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.FRANKLIN_SHARES_INVESTED, SIZE_OF(paramFinanceSaved.FRANKLIN_SHARES_INVESTED), "F_S_I_AR")
		
		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedFloat(paramFinanceSaved.FRANKLIN_SHARES_INVESTED[i],i,"F_I_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		i = 0

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.FRANKLIN_SHARES_OWNED, SIZE_OF(paramFinanceSaved.FRANKLIN_SHARES_OWNED), "F_S_OOT_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedInt(paramFinanceSaved.FRANKLIN_SHARES_OWNED[i],i,"F_OOT_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		//trevor
		//INT TREVOR_TOTAL_OWNED
		//INT TREVOR_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		//FLOAT TREVOR_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		//INT TREVOR_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
		
		//REGISTER_INT_TO_SAVE(paramFinanceSaved.TREVOR_TOTAL_OWNED, "TREVOR_TOTAL_OWNED_SHARES")
		
		i = 0

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.TREVOR_SHARES_COMPANY_INDEX, SIZE_OF(paramFinanceSaved.TREVOR_SHARES_COMPANY_INDEX), "T_S_CI_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedInt(paramFinanceSaved.TREVOR_SHARES_COMPANY_INDEX[i],i,"T_CI_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		i = 0

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.TREVOR_SHARES_INVESTED, SIZE_OF(paramFinanceSaved.TREVOR_SHARES_INVESTED), "T_S_I_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedFloat(paramFinanceSaved.TREVOR_SHARES_INVESTED[i],i,"T_I_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		i = 0

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.TREVOR_SHARES_OWNED, SIZE_OF(paramFinanceSaved.TREVOR_SHARES_OWNED), "T_S_OOT_AR")

		REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
			RegisterIndexedInt(paramFinanceSaved.TREVOR_SHARES_OWNED[i],i,"T_OOT_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		
		
		//bFirstTimeTutorialSeen
		REGISTER_BOOL_TO_SAVE(paramFinanceSaved.bFirstTimeTutorialSeen,"STOCK_TUT")
		REGISTER_BOOL_TO_SAVE(paramFinanceSaved.bAtmFirstTimeFlowHelpShown,"ATM_TUT")
		REGISTER_BOOL_TO_SAVE(paramFinanceSaved.bInitialPriceGenerationPerformed,"STK_PRC_GEN")
		REGISTER_BOOL_TO_SAVE(paramFinanceSaved.bShitSkipBool,"STK_PRC_SSK")
	 
		/*
		//Stored single player prices
		FLOAT SINGLE_PLAYER_SHARE_PRICE_DUMP[MAX_SP_FINANCE_VALUE_STORAGE]
		FLOAT SINGLE_PLAYER_SHARE_MAX_DUMP[MAX_SP_FINANCE_VALUE_STORAGE]
		FLOAT SINGLE_PLAYER_SHARE_MIN_DUMP[MAX_SP_FINANCE_VALUE_STORAGE]
		*/
		
		
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.SINGLE_PLAYER_SHARE_PRICE_DUMP, SIZE_OF(paramFinanceSaved.SINGLE_PLAYER_SHARE_PRICE_DUMP), "SPSPD_AR")
		REPEAT MAX_SP_FINANCE_VALUE_STORAGE i
			RegisterIndexedFloat(paramFinanceSaved.SINGLE_PLAYER_SHARE_PRICE_DUMP[i],i,"SPSPD_")
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.SINGLE_PLAYER_SHARE_MAX_DUMP, SIZE_OF(paramFinanceSaved.SINGLE_PLAYER_SHARE_MAX_DUMP), "SPSMAD_AR")
		REPEAT MAX_SP_FINANCE_VALUE_STORAGE i
			RegisterIndexedFloat(paramFinanceSaved.SINGLE_PLAYER_SHARE_MAX_DUMP[i],i,"SPSMAD_")
		ENDREPEAT
		STOP_SAVE_ARRAY()

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.SINGLE_PLAYER_SHARE_MIN_DUMP, SIZE_OF(paramFinanceSaved.SINGLE_PLAYER_SHARE_MIN_DUMP), "SPSMID_AR")
		REPEAT MAX_SP_FINANCE_VALUE_STORAGE i
			RegisterIndexedFloat(paramFinanceSaved.SINGLE_PLAYER_SHARE_MIN_DUMP[i],i,"SPSMID_")
		ENDREPEAT
		STOP_SAVE_ARRAY()


		REGISTER_BOOL_TO_SAVE(paramFinanceSaved.bFirstTimeBankInit,"SCBNK_INI")
		

		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS, SIZE_OF(paramFinanceSaved.PLAYER_ACCOUNT_LOGS), "PLR_ACNT_LGS")

		REPEAT MAX_BANK_ACCOUNTS i
			TEXT_LABEL t = "BNKST_"
			t += i
			START_SAVE_STRUCT_WITH_SIZE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i],SIZE_OF(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i]), t)
			
				TEXT_LABEL y = t
				y += "ac"
				
				REGISTER_INT_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].iLogActions,y)
				
				y = t
				y += "lp"
					
				REGISTER_INT_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint,y)
			
				y = t
				y += "l"
			
			
				START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries,
										   SIZE_OF(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries),
										   y)
										   
				y += "_"
				INT j = 0
				REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
				
					TEXT_LABEL u = y
					u += j
					
					START_SAVE_STRUCT_WITH_SIZE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j],
												SIZE_OF(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j]),
												u)
							u = y					
							u += "a"
							REGISTER_ENUM_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType,u)
							u = y
							u += "b"
							REGISTER_ENUM_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource,u)
							u = y
							u += "c"
							REGISTER_INT_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree,u)
							u = y
							u += "d"
							REGISTER_ENUM_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint,u)
							u = y
							u += "e"
							REGISTER_ENUM_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint,u)
							u = y
							u += "f"
							REGISTER_INT_TO_SAVE(paramFinanceSaved.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint,u)
						
					STOP_SAVE_STRUCT()
				ENDREPEAT
				STOP_SAVE_ARRAY()
			
			
			STOP_SAVE_STRUCT()
			
			
			
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		
		REGISTER_INT_TO_SAVE(paramFinanceSaved.iProfitLoss, "Profloss")

		//INT iFiltersRegistered
		REGISTER_INT_TO_SAVE(paramFinanceSaved.iFiltersRegistered, "FILTR_TOT")
		TEXT_LABEL t = "FILTR_"	
		
		
		
		//BAWSAQ_COMPANIES FilteredBind[MAX_SP_FINANCE_FILTERS]
		TEXT_LABEL y = t
		y += "ff"
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.FilteredBind,
								   SIZE_OF(paramFinanceSaved.FilteredBind),
								   y)
		REPEAT MAX_SP_FINANCE_FILTERS i
			TEXT_LABEL u = y
			u += i
			REGISTER_ENUM_TO_SAVE(paramFinanceSaved.FilteredBind[i],u)
		ENDREPEAT
		STOP_SAVE_ARRAY()
		//INT iFilterDurationRemaining[MAX_SP_FINANCE_FILTERS]
		y = t
		y += "fdr"
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.iFilterDurationRemaining,
								   SIZE_OF(paramFinanceSaved.iFilterDurationRemaining),
								   y)
		REPEAT MAX_SP_FINANCE_FILTERS i
			TEXT_LABEL u = y
			u += i
			REGISTER_INT_TO_SAVE(paramFinanceSaved.iFilterDurationRemaining[i],u)
		ENDREPEAT
		STOP_SAVE_ARRAY()

		//INT iFilterFlags[MAX_SP_FINANCE_FILTERS]
		y = t
		y += "ffl"
		START_SAVE_ARRAY_WITH_SIZE(paramFinanceSaved.iFilterFlags,
								   SIZE_OF(paramFinanceSaved.iFilterFlags),
								   y)
		REPEAT MAX_SP_FINANCE_FILTERS i
			TEXT_LABEL u = y
			u += i
			REGISTER_INT_TO_SAVE(paramFinanceSaved.iFilterFlags[i],u)
		ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_BOOL_TO_SAVE(paramFinanceSaved.bBrowserTutorialSeen,"SCBNK_WEBTU")
		
		REGISTER_INT_TO_SAVE(paramFinanceSaved.iSaveCoupons,"FIN_COUPONS")
		
		REGISTER_ENUM_TO_SAVE(paramFinanceSaved.eCurrentEyeFindNewsStoryState,"FIN_NEWSENM")
		
		
	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the friends globals that need to be saved
PROC Register_Finance_Saved_Globals()
	//SCRIPT_ASSERT("REGISTERING FINANCE SAVED GLOBALS")
	Register_Struct_FinanceSaved(g_savedGlobals.sFinanceData, "FINANCE_SAVED_ARRAY")
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the friends globals that need to be saved
PROC Register_Finance_Saved_Globals_CLF()
	//SCRIPT_ASSERT("REGISTERING FINANCE SAVED GLOBALS")
	Register_Struct_FinanceSaved(g_savedGlobalsClifford.sFinanceData, "FINANCE_SAVED_ARRAY")
ENDPROC
PROC Register_Finance_Saved_Globals_NRM()
	//SCRIPT_ASSERT("REGISTERING FINANCE SAVED GLOBALS")
	Register_Struct_FinanceSaved(g_savedGlobalsnorman.sFinanceData, "FINANCE_SAVED_ARRAY")
ENDPROC

























































