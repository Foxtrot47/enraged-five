USING "rage_builtins.sch"
USING "globals.sch"




//RegisterIndexedInt
//RegisterIndexedFloat

PROC RegisterIndexedBool(BOOL &value, INT index, STRING base)
	
	TEXT_LABEL t = base
	
	t += index
	
	REGISTER_BOOL_TO_SAVE(value, t)
	
ENDPROC


PROC RegisterIndexedPartcipantEnum(EMAILER_ENUMS &value, INT index, STRING base)
	
	TEXT_LABEL t = base
	
	t += index
	
	REGISTER_ENUM_TO_SAVE(value, t)
	
ENDPROC

PROC REGISTER_EMAIL_INBOX_STRUCT(EmailSavedInbox &estr,INT index,STRING name)

	/*
	STRUCT EmailSavedInbox
			INT InboxTotalMailsIn
			
			INT InboxEmailLogIndices[MAX_INBOX_LOGGED_MAILS_THREADS]
			INT InboxThreadIndex[MAX_INBOX_LOGGED_MAILS_THREADS]
			BOOL InboxHasFiredResponses[MAX_INBOX_LOGGED_MAILS_THREADS]
			INT InboxPickedResponseIndices[MAX_INBOX_LOGGED_MAILS_THREADS]
			BOOL InboxHasBeenViewed[MAX_INBOX_LOGGED_MAILS_THREADS]
			BOOL InboxIsDynamic[MAX_INBOX_LOGGED_MAILS_THREADS]
	ENDSTRUCT
	*/
		TEXT_LABEL t = name
		t += index
		t += "_"
		
		START_SAVE_STRUCT_WITH_SIZE(estr, SIZE_OF(estr), t)
		
			INT i = 0
			TEXT_LABEL b
			TEXT_LABEL z
			
			z = t
			z += "A"
			REGISTER_INT_TO_SAVE(estr.InboxTotalMailsIn,z)
			

			z = t
			z += "B"
			START_SAVE_ARRAY_WITH_SIZE(estr.InboxEmailLogIndices, SIZE_OF(estr.InboxEmailLogIndices), z)
			REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
					b = z
					b += i
					b += "_"
					RegisterIndexedInt(estr.InboxEmailLogIndices[i],i,b)
			ENDREPEAT
			STOP_SAVE_ARRAY()

			i = 0
			z = t
			z += "C"
			START_SAVE_ARRAY_WITH_SIZE(estr.InboxThreadIndex, SIZE_OF(estr.InboxThreadIndex), z)
			REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
					b = z
					b += i
					b += "_"
					RegisterIndexedInt(estr.InboxThreadIndex[i],i,b)
			ENDREPEAT
			STOP_SAVE_ARRAY()
			i = 0
			z = t
			z += "D"
			START_SAVE_ARRAY_WITH_SIZE(estr.InboxHasFiredResponses, SIZE_OF(estr.InboxHasFiredResponses), z)
			z += "_"
			REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
					b = z
					b += i
					b += "_"
					RegisterIndexedBool(estr.InboxHasFiredResponses[i],i,b)
			ENDREPEAT
			STOP_SAVE_ARRAY()
			i = 0
			z = t
			z += "E"
			START_SAVE_ARRAY_WITH_SIZE(estr.InboxPickedResponseIndices, SIZE_OF(estr.InboxPickedResponseIndices), z)
			REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
					b = z
					b += i
					b += "_"
					RegisterIndexedInt(estr.InboxPickedResponseIndices[i],i,b)
			ENDREPEAT
			STOP_SAVE_ARRAY()
			i = 0
			z = t
			z += "F"
			START_SAVE_ARRAY_WITH_SIZE(estr.InboxHasBeenViewed, SIZE_OF(estr.InboxHasBeenViewed), z)
			REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
					b = z
					b += i
					b += "_"
					RegisterIndexedBool(estr.InboxHasBeenViewed[i],i,b)
			ENDREPEAT
			STOP_SAVE_ARRAY()
			i = 0
			z = t
			z += "G"
			START_SAVE_ARRAY_WITH_SIZE(estr.InboxIsDynamic, SIZE_OF(estr.InboxIsDynamic), z)
			REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
					b = z
					b += i
					b += "_"
					RegisterIndexedBool(estr.InboxIsDynamic[i],i,b)
			ENDREPEAT
			STOP_SAVE_ARRAY()
			
		STOP_SAVE_STRUCT()
		
ENDPROC


PROC REGISTER_EMAIL_DYNAMIC_EMAIL_STRUCT(DYNAMIC_EMAIL_INSTANCE &estr,INT index,STRING name)
	/*
	STRUCT DYNAMIC_EMAIL_INSTANCE//this struct is saved so keep data and changes to a minimum
		//content
		//uses instance of email?
		EMAIL_MESSAGE_ENUMS sourceEmail//take content and responses from this email
		
		//
		BOOL bOverrideContent
		TEXT_LABEL_15 content
		INT iOverrideAdditional
		TEXT_LABEL_15 additional[5]
		

	ENDSTRUCT
	*/
	
	TEXT_LABEL t = name
	t += index
	START_SAVE_STRUCT_WITH_SIZE(estr, SIZE_OF(estr), t)
	t += "_"
	TEXT_LABEL b
		
		//BOOL bOverrideContent
		b = t
		b += "A"
		REGISTER_ENUM_TO_SAVE(estr.sourceEmail, b)
		
		//BOOL bOverrideContent
		b = t
		b += "B"
		REGISTER_BOOL_TO_SAVE(estr.bOverrideContent, b)
		
		
		//TEXT_LABEL_15 content
		b = t
		b += "C"
	    REGISTER_TEXT_LABEL_15_TO_SAVE(estr.content, b)
		
		//INT iOverrideAdditional
		b = t
		b += "D"
		REGISTER_INT_TO_SAVE(estr.iOverrideAdditional, b)
		
		//TEXT_LABEL_15 additional[5]
		INT i = 0
		b = t
		b += "E"
		TEXT_LABEL z
		START_SAVE_ARRAY_WITH_SIZE(estr.additional,SIZE_OF(estr.additional),b)
			REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS i // previous was 5 increased to 10 by CC
				z = b
				z += i
				REGISTER_TEXT_LABEL_15_TO_SAVE(estr.additional[i], z)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
	
	STOP_SAVE_STRUCT()
	
ENDPROC


PROC REGISTER_EMAIL_DYNAMIC_THREAD_STRUCT(EmailSavedDynamicThread &estr,INT index,STRING name)



/*
STRUCT EmailSavedDynamicThread
			
		//enum//
		BelongsTo
		
		BOOL isCritical
		
		INT iParticipants
		INT participantIDs[MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS]
		
		
		DYNAMIC_EMAIL_INSTANCE DynamicEmailData[DYNAMIC_EMAIL_THREAD_MAX_LENGTH]		
				
ENDSTRUCT
*/

		TEXT_LABEL t = name
		t += index
		t += "_"
		TEXT_LABEL b = t

		START_SAVE_STRUCT_WITH_SIZE(estr, SIZE_OF(estr), t)
		
		
			
			
			b = t
			b += "A"
			//INT ThreadNameID
			REGISTER_ENUM_TO_SAVE(estr.belongsTo, b)
			
			b = t
			b += "B"
			//INT AssignedID
			REGISTER_ENUM_TO_SAVE(estr.AssignedID, b)
			
			
			b = t
			b+= "C"
			//BOOL isCritical
			REGISTER_BOOL_TO_SAVE(estr.isCritical, b)
			
			b = t
			b+= "D"
			//INT iParticipants
			REGISTER_INT_TO_SAVE(estr.iParticipants, b)
			
			
			b = t
			b+= "E"
			START_SAVE_ARRAY_WITH_SIZE(estr.participantIDs, SIZE_OF(estr.participantIDs), b)
				INT i = 0
				//INT participantIDs[MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS]
				REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS i
					RegisterIndexedPartcipantEnum(estr.participantIDs[i],i,b)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			//Finally dynamic instance
			//DYNAMIC_EMAIL_INSTANCE DynamicEmailData[DYNAMIC_EMAIL_THREAD_MAX_LENGTH]	
			
			b = t
			b+= "F"
			REGISTER_INT_TO_SAVE(estr.iProgress, b)
			
			b = t
			b+= "G"
			START_SAVE_ARRAY_WITH_SIZE(estr.DynamicEmailData, SIZE_OF(estr.DynamicEmailData), b)
				i = 0
				REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH i
					REGISTER_EMAIL_DYNAMIC_EMAIL_STRUCT(estr.DynamicEmailData[i],i,b)	
				ENDREPEAT
			STOP_SAVE_ARRAY()
		
		STOP_SAVE_STRUCT()
ENDPROC

PROC REGISTER_EMAIL_STATIC_THREAD_STRUCT(EmailSavedStaticThread &estr,INT i,STRING name)

/*
STRUCT EmailSavedStaticThread

		BOOL active
		BOOL ended
		BOOL blockedForResponse
		INT iCurrentlyAt
		INT iDelayPassed
ENDSTRUCT
*/

		TEXT_LABEL t = name
		t += i
		
		
		TEXT_LABEL b
		

		START_SAVE_STRUCT_WITH_SIZE(estr, SIZE_OF(estr), t)
			t += "_"
			
			
			//BOOL active
			b = t
			b += "A"
			REGISTER_BOOL_TO_SAVE(estr.active, b)
			
			//BOOL ended
			b = t
			b += "E"
			REGISTER_BOOL_TO_SAVE(estr.ended, b)
			
			//BOOL blockedForResponse
			b = t
			b += "B"
			REGISTER_BOOL_TO_SAVE(estr.blockedForResponse, b)
			
			//INT iCurrentlyAt
			b = t
			b += "C"
			REGISTER_INT_TO_SAVE(estr.iCurrentlyAt, b)		
			
			INT j
			TEXT_LABEL c 
			//INT iDelayPassed
			//b = t
			//b += "D"
			//REGISTER_INT_TO_SAVE(estr.iDelayPassed, b)	
/*
			b = t
			b+= "X"
			START_SAVE_ARRAY_WITH_SIZE(estr.ThreadMails, SIZE_OF(estr.ThreadMails), b)
				i = 0
				
				
				REPEAT MAX_THREAD_LENGTH j
					c = b
					c += j
					REGISTER_INT_TO_SAVE(estr.ThreadMails[j], c)	
				ENDREPEAT
			STOP_SAVE_ARRAY()
*/			
			b = t
			b+= "Z"
			START_SAVE_ARRAY_WITH_SIZE(estr.ThreadEmailLog, SIZE_OF(estr.ThreadEmailLog), b)
				i = 0
				REPEAT MAX_THREAD_LOG_LENGTH j
					c = b
					c += j
					REGISTER_INT_TO_SAVE(estr.ThreadEmailLog[j], c)	
				ENDREPEAT
			STOP_SAVE_ARRAY()
		
		
		
		STOP_SAVE_STRUCT()
ENDPROC



PROC REGISTER_EMAIL_DYNAMIC_BUFFERS_STRUCT(DYNAMIC_THREAD_EMAIL_PENDING &estr,INT index,STRING name)

	
		TEXT_LABEL t = name
		t += index
		t += "_"
		TEXT_LABEL b = t

		START_SAVE_STRUCT_WITH_SIZE(estr, SIZE_OF(estr), t)
		

			b = t
			b += "A"
			//DYNAMIC_EMAIL_THREAD_NAMES eTargetThread
			REGISTER_ENUM_TO_SAVE(estr.eTargetThread, b)
			
			b = t
			b += "B"
			//EMAIL_MESSAGE_ENUMS eEmailID
			REGISTER_ENUM_TO_SAVE(estr.eEmailID, b)
			
			b = t
			b += "C"
			//INT iInGameHoursBeforeTrigger
			REGISTER_INT_TO_SAVE(estr.iInGameHoursBeforeTrigger, b)
			
			b = t
			b += "D"
			//INT iPendingIndex
			REGISTER_INT_TO_SAVE(estr.iPendingIndex, b)
			
			b = t
			b += "E"
			//INT bNotSent
			REGISTER_BOOL_TO_SAVE(estr.bNotSent, b)

			b = t
			b += "F"
			//BOOL bOverrideContent
			REGISTER_BOOL_TO_SAVE(estr.bOverrideContent, b)

			b = t
			b += "G"
			//TEXT_LABEL_15 content
			REGISTER_TEXT_LABEL_15_TO_SAVE(estr.content, b)
			
			b = t
			b += "H"
			//INT iOverrideAdditional
			REGISTER_INT_TO_SAVE(estr.iOverrideAdditional, b)
			
			b = t
			b += "J"
			START_SAVE_ARRAY_WITH_SIZE(estr.additional, SIZE_OF(estr.additional), b)
				INT i = 0
				REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS i // previous was 5 increased to 10 by CC 
				
					TEXT_LABEL nm = b
					nm += "_"
					nm += i
					REGISTER_TEXT_LABEL_15_TO_SAVE(estr.additional[i], nm)	
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			
			b = t
			b += "RI"
			//INT RegIDOfTarget
			REGISTER_INT_TO_SAVE(estr.RegIDOfTarget, b)
			
		STOP_SAVE_STRUCT()

ENDPROC


/*

EmailDataSaved
	EmailSavedInbox
	EmailSavedDynamicThread
	EmailSavedStaticThread

*/

PROC REGISTER_EMAIL_DATA_STRUCT(EmailDataSaved &estr)


	//START_SAVE_STRUCT_WITH_SIZE(estr, SIZE_OF(estr), name)
	//REGISTER_EMAIL_INBOX_STRUCT(estr.InboxSnapshots)
	//TOTAL_INBOXES
	
	INT i = 0
	START_SAVE_ARRAY_WITH_SIZE(estr.InboxSnapshots, SIZE_OF(estr.InboxSnapshots), "EMI_AR")
	REPEAT TOTAL_INBOXES i
		REGISTER_EMAIL_INBOX_STRUCT(estr.InboxSnapshots[i],i,"EMI_")
	ENDREPEAT
	STOP_SAVE_ARRAY()
	
	
	
	
	REGISTER_INT_TO_SAVE(estr.g_PendingEmailIDGenerator, "EMIDGENRT")
	//REGISTER_EMAIL_DYNAMIC_THREAD_STRUCT(estr.DynamicThreadBufferSnapshots)
	//DYNAMIC_EMAIL_THREAD_BUFFERS
	i = 0
	START_SAVE_ARRAY_WITH_SIZE(estr.DynamicThreadBufferSnapshots, SIZE_OF(estr.DynamicThreadBufferSnapshots), "EMDT_AR")
	REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
		REGISTER_EMAIL_DYNAMIC_THREAD_STRUCT(estr.DynamicThreadBufferSnapshots[i],i,"EMD_")
	ENDREPEAT
	STOP_SAVE_ARRAY()
	
	
	//REGISTER_EMAIL_STATIC_THREAD_STRUCT(estr.StaticThreadSnapshots)
	//TOTAL_EMAIL_THREADS_IN_GAME
	
	i = 0
	START_SAVE_ARRAY_WITH_SIZE(estr.StaticThreadSnapshots, SIZE_OF(estr.StaticThreadSnapshots), "EMS_AR")
	REPEAT TOTAL_EMAIL_THREADS_IN_GAME i
		REGISTER_EMAIL_STATIC_THREAD_STRUCT(estr.StaticThreadSnapshots[i],i,"EMS_")
	ENDREPEAT
	STOP_SAVE_ARRAY()
	
	
	
	i = 0
	START_SAVE_ARRAY_WITH_SIZE(estr.DynamicPendingMails, SIZE_OF(estr.DynamicPendingMails), "EMB_AR")
	REPEAT MAX_DYNAMIC_EMAILS_PENDING i
		REGISTER_EMAIL_DYNAMIC_BUFFERS_STRUCT(estr.DynamicPendingMails[i],i,"EMB_")
	ENDREPEAT
	STOP_SAVE_ARRAY()
	
	
	//STOP_SAVE_STRUCT()
ENDPROC



PROC Register_Email_Saved_Globals()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sEmailData, SIZE_OF(g_savedGlobals.sEmailData), "EMAIL_DATA")
	REGISTER_EMAIL_DATA_STRUCT(g_savedGlobals.sEmailData)
	STOP_SAVE_STRUCT()
ENDPROC

PROC Register_Email_Saved_Globals_CLF()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sEmailData, SIZE_OF(g_savedGlobalsClifford.sEmailData), "EMAIL_DATA")
	REGISTER_EMAIL_DATA_STRUCT(g_savedGlobalsClifford.sEmailData)
	STOP_SAVE_STRUCT()
ENDPROC
PROC Register_Email_Saved_Globals_NRM()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sEmailData, SIZE_OF(g_savedGlobalsnorman.sEmailData), "EMAIL_DATA")
	REGISTER_EMAIL_DATA_STRUCT(g_savedGlobalsnorman.sEmailData)
	STOP_SAVE_STRUCT()
ENDPROC



















































































