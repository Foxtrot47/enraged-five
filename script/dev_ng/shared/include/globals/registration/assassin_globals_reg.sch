// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   assassin_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all assassination
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the assassination globals that need to be saved
PROC Register_Assassin_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sAssassinData, SIZE_OF(g_savedGlobals.sAssassinData), "ASSASSIN_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sAssassinData.iCurrentAssassinRank, "ASSASSIN_iCurRank")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sAssassinData.iGenericData, "ASSASSIN_iGenData")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sAssassinData.iBools, "ASSASSIN_iBools")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sAssassinData.iAttemptsOnCur, "ASSASSIN_iAttemptsOnCur")
		
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sAssassinData.TODForNextMission, "ASSASSIN_TOD_iTime")

		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sAssassinData.fHotelMissionTime, "ASSASSIN_HotelMissionTime")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sAssassinData.fMultiMissionTime, "ASSASSIN_MultiMissionTime")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sAssassinData.fViceMissionTime, "ASSASSIN_ViceMissionTime")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sAssassinData.fBusMissionTime, "ASSASSIN_BusMissionTime")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sAssassinData.fConstructionMissionTime, "ASSASSIN_ConstructionMissionTime")
	STOP_SAVE_STRUCT()
ENDPROC

