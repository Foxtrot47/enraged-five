USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the bail bond globals that need to be saved
PROC Register_Bailbond_Saved_Globals()
	INT iCounter
	TEXT_LABEL_31 tLabel
	TEXT_LABEL_31 tArrayName = "BB_FailsNP"
	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sBailBondData, SIZE_OF(g_savedGlobals.sBailBondData), "BAIL_BOND_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sBailBondData.iLauncherBitFlags, "Launcher_Bit_Flags")		
		REGISTER_INT_TO_SAVE(g_savedGlobals.sBailBondData.iCurrentMission, "Current_Mission")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sBailBondData.iLauncherState, "Launcher_State")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sBailBondData.timeMissionStamp, "Stored_Time_Stamp")
		
		START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sBailBondData.iFailsNoProgress, SIZE_OF(g_savedGlobals.sBailBondData.iFailsNoProgress), tArrayName)
			REPEAT COUNT_OF(g_savedGlobals.sBailBondData.iFailsNoProgress) iCounter
				tLabel = tArrayName
				tLabel += iCounter
				REGISTER_INT_TO_SAVE(g_savedGlobals.sBailBondData.iFailsNoProgress[iCounter], tLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
	STOP_SAVE_STRUCT()
ENDPROC





















































