// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   specialPed_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all special ped
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
USING "globals.sch"
USING "commands_misc.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the taxi globals that need to be saved
PROC Register_SpecialPed_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sSpecialPedData, SIZE_OF(g_savedGlobals.sSpecialPedData), "SPECIALPED_SAVED_STRUCT")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sSpecialPedData.ePedsKilled, "SPEC_PedsKilled")
	STOP_SAVE_STRUCT()
ENDPROC
