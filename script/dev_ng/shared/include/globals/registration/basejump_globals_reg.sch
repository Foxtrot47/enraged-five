// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   basejump_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all basejump
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************


/// PURPOSE:
///    Sets up the save structure for registering all the taxi globals that need to be saved
PROC Register_Basejump_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sBasejumpData, SIZE_OF(g_savedGlobals.sBasejumpData), "BASEJUMP_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sBasejumpData.iLaunchRank, "BJ_iLaunchRank")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sBasejumpData.iCompletedFlags, "BJ_iCompletedFlags")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sBasejumpData.fLongestSkydive, "BJ_fLongestSkydive")
	STOP_SAVE_STRUCT()
ENDPROC

