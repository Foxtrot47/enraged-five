USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "player_ped_public.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   player_ped_globals_reg.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   Contains the registrations with code for all player ped 
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



/// PURPOSE: Helper function which inserts the player peds name to the front of the string
FUNC TEXT_LABEL_63 ADD_PED_NAME_TO_STRING(INT iPed, STRING tlString)
	TEXT_LABEL_63 sRetStr = "SP"
	sRetStr += iPed
	sRetStr += "_"
	sRetStr += tlString
	RETURN sRetStr
ENDFUNC


/// PURPOSE:
///    Save the Items array
/// INPUT PARAMS:
///    paramItemsArray			The instance of the Array of Items to be saved
///    paramNameOfArray			The name of this instance of the IntIDs Array
PROC Register_Array_Of_Items(INT &paramItemsArray[NUMBER_OF_PED_COMP_TYPES], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramItemsArray, SIZE_OF(paramItemsArray), paramNameOfArray)

		TEXT_LABEL_31 ItemName
		
		REPEAT NUMBER_OF_PED_COMP_TYPES tempLoop
			PED_COMP_TYPE_ENUM loopAsEnum = INT_TO_ENUM(PED_COMP_TYPE_ENUM, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE COMP_TYPE_HEAD
					ItemName = "COMP_TYPE_HEAD"
					BREAK
					
				CASE COMP_TYPE_HAIR
					ItemName = "COMP_TYPE_HAIR"
					BREAK
					
				CASE COMP_TYPE_TORSO
					ItemName = "COMP_TYPE_TORSO"
					BREAK
					
				CASE COMP_TYPE_LEGS
					ItemName = "COMP_TYPE_LEGS"
					BREAK
					
				CASE COMP_TYPE_FEET
					ItemName = "COMP_TYPE_FEET"
					BREAK
					
				CASE COMP_TYPE_HAND
					ItemName = "COMP_TYPE_HAND"
					BREAK
					
				CASE COMP_TYPE_SPECIAL
					ItemName = "COMP_TYPE_SPECIAL"
					BREAK
					
				CASE COMP_TYPE_SPECIAL2
					ItemName = "COMP_TYPE_SPECIAL2"
					BREAK
					
				CASE COMP_TYPE_OUTFIT
					ItemName = "COMP_TYPE_OUTFIT"
					BREAK
					
				CASE COMP_TYPE_PROPGROUP
					ItemName = "COMP_TYPE_PROPGROUP"
					BREAK
					
				CASE COMP_TYPE_PROPS
					ItemName = "COMP_TYPE_PROPS"
					BREAK
					
				CASE COMP_TYPE_DECL
					ItemName = "COMP_TYPE_DECL"
					BREAK
					
				CASE COMP_TYPE_BERD
					ItemName = "COMP_TYPE_BERD"
					BREAK
					
				CASE COMP_TYPE_TEETH
					ItemName = "COMP_TYPE_TEETH"
					BREAK
					
				CASE COMP_TYPE_JBIB
					ItemName = "COMP_TYPE_JBIB"
					BREAK
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Items - case missing for an Item")
					ItemName = "ItemDetails"
					ItemName += tempLoop
					BREAK
			ENDSWITCH
			
			REGISTER_INT_TO_SAVE(paramItemsArray[tempLoop], ItemName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the struct containing the player ped component data that state what clothes/props the player has
/// INPUT PARAMS:
///    paramComponentsSaved			This instance of the Control Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_PlayerPedComponentsSaved(PED_COMPONENTS_STRUCT &paramComponentsSaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramComponentsSaved, SIZE_OF(paramComponentsSaved), paramNameOfStruct)	
		
		// head
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iHeadBitset0, SIZE_OF(paramComponentsSaved.iHeadBitset0), "HEAD0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHeadBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHeadBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHeadBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// beard
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iBeardBitset0, SIZE_OF(paramComponentsSaved.iBeardBitset0), "BEARD0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iBeardBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iBeardBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iBeardBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// hair
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iHairBitset0, SIZE_OF(paramComponentsSaved.iHairBitset0), "HAIR0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHairBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHairBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHairBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// torso
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset0, SIZE_OF(paramComponentsSaved.iTorsoBitset0), "TORSO0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset1, SIZE_OF(paramComponentsSaved.iTorsoBitset1), "TORSO1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset2, SIZE_OF(paramComponentsSaved.iTorsoBitset2), "TORSO2")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset2[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset2[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset2[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset3, SIZE_OF(paramComponentsSaved.iTorsoBitset3), "TORSO3")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset3[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset3[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset3[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset4, SIZE_OF(paramComponentsSaved.iTorsoBitset4), "TORSO4")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset4[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset4[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset4[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset5, SIZE_OF(paramComponentsSaved.iTorsoBitset5), "TORSO5")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset5[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset5[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset5[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset6, SIZE_OF(paramComponentsSaved.iTorsoBitset6), "TORSO6")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset6[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset6[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset6[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset7, SIZE_OF(paramComponentsSaved.iTorsoBitset7), "TORSO7")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset7[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset7[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset7[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset8, SIZE_OF(paramComponentsSaved.iTorsoBitset8), "TORSO8")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset8[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset8[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset8[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTorsoBitset9, SIZE_OF(paramComponentsSaved.iTorsoBitset9), "TORSO9")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset9[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset9[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTorsoBitset9[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// legs
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iLegsBitset0, SIZE_OF(paramComponentsSaved.iLegsBitset0), "LEGS0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iLegsBitset1, SIZE_OF(paramComponentsSaved.iLegsBitset1), "LEGS1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iLegsBitset2, SIZE_OF(paramComponentsSaved.iLegsBitset2), "LEGS2")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset2[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset2[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset2[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iLegsBitset3, SIZE_OF(paramComponentsSaved.iLegsBitset3), "LEGS3")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset3[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset3[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iLegsBitset3[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// hand
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iHandBitset0, SIZE_OF(paramComponentsSaved.iHandBitset0), "HAND0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHandBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHandBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iHandBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// Feet
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iFeetBitset0, SIZE_OF(paramComponentsSaved.iFeetBitset0), "FEET0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iFeetBitset1, SIZE_OF(paramComponentsSaved.iFeetBitset1), "FEET1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iFeetBitset2, SIZE_OF(paramComponentsSaved.iFeetBitset2), "FEET2")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset2[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset2[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset2[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iFeetBitset3, SIZE_OF(paramComponentsSaved.iFeetBitset3), "FEET3")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset3[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset3[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset3[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iFeetBitset4, SIZE_OF(paramComponentsSaved.iFeetBitset4), "FEET4")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset4[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset4[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iFeetBitset4[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Teeth
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iTeethBitset0, SIZE_OF(paramComponentsSaved.iTeethBitset0), "TEETH0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTeethBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTeethBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iTeethBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Special
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iSpecialBitset0, SIZE_OF(paramComponentsSaved.iSpecialBitset0), "SPEC0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iSpecialBitset1, SIZE_OF(paramComponentsSaved.iSpecialBitset1), "SPEC1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iSpecialBitset2, SIZE_OF(paramComponentsSaved.iSpecialBitset2), "SPEC2")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset2[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset2[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecialBitset2[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		// Special 2
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iSpecial2Bitset0, SIZE_OF(paramComponentsSaved.iSpecial2Bitset0), "SPEC2_0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecial2Bitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecial2Bitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iSpecial2Bitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Decal
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iDeclBitset0, SIZE_OF(paramComponentsSaved.iDeclBitset0), "DECL0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iDeclBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iDeclBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iDeclBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iDeclBitset1, SIZE_OF(paramComponentsSaved.iDeclBitset1), "DECL1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iDeclBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iDeclBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iDeclBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Jbib
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iJbibBitset0, SIZE_OF(paramComponentsSaved.iJbibBitset0), "JBIB0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iJbibBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iJbibBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iJbibBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iJbibBitset1, SIZE_OF(paramComponentsSaved.iJbibBitset1), "JBIB1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iJbibBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iJbibBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iJbibBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Outfit
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iOutfitBitset0, SIZE_OF(paramComponentsSaved.iOutfitBitset0), "OUTF0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iOutfitBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iOutfitBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iOutfitBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iOutfitBitset1, SIZE_OF(paramComponentsSaved.iOutfitBitset1), "OUTF1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iOutfitBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iOutfitBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iOutfitBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Propgroup
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropGroupBitset0, SIZE_OF(paramComponentsSaved.iPropGroupBitset0), "PROPG0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropGroupBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropGroupBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropGroupBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()

		// Props
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropsBitset0, SIZE_OF(paramComponentsSaved.iPropsBitset0), "PROP0")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset0[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset0[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset0[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropsBitset1, SIZE_OF(paramComponentsSaved.iPropsBitset1), "PROP1")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset1[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset1[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset1[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropsBitset2, SIZE_OF(paramComponentsSaved.iPropsBitset2), "PROP2")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset2[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset2[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset2[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropsBitset3, SIZE_OF(paramComponentsSaved.iPropsBitset3), "PROP3")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset3[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset3[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset3[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropsBitset4, SIZE_OF(paramComponentsSaved.iPropsBitset4), "PROP4")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset4[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset4[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset4[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramComponentsSaved.iPropsBitset5, SIZE_OF(paramComponentsSaved.iPropsBitset5), "PROP5")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset5[PED_COMPONENT_AVAILABLE_SLOT], "AVAILABLE")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset5[PED_COMPONENT_ACQUIRED_SLOT], "ACQUIRED")
			REGISTER_INT_TO_SAVE(paramComponentsSaved.iPropsBitset5[PED_COMPONENT_USED_SLOT], "NEW")
		STOP_SAVE_ARRAY()
	STOP_SAVE_STRUCT()
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Save the TextureIDs array
/// INPUT PARAMS:
///    paramTextureIDArray		The instance of the Array of TextureIDs to be saved
///    paramNameOfArray			The name of this instance of the TextureIDs Array
PROC Register_Array_Of_TextureIDs(INT &paramTextureIDArray[NUM_PED_COMPONENTS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramTextureIDArray, SIZE_OF(paramTextureIDArray), paramNameOfArray)

		TEXT_LABEL_31 TextureIDName
		
		REPEAT NUM_PED_COMPONENTS tempLoop
			PED_COMPONENT loopAsEnum = INT_TO_ENUM(PED_COMPONENT, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE PED_COMP_HEAD
					TextureIDName = "PED_COMP_HEAD"
					BREAK
					
				CASE PED_COMP_BERD
					TextureIDName = "PED_COMP_BERD"
					BREAK
					
				CASE PED_COMP_HAIR
					TextureIDName = "PED_COMP_HAIR"
					BREAK
					
				CASE PED_COMP_TORSO
					TextureIDName = "PED_COMP_TORSO"
					BREAK
					
				CASE PED_COMP_LEG
					TextureIDName = "PED_COMP_LEG"
					BREAK
					
				CASE PED_COMP_HAND
					TextureIDName = "PED_COMP_HAND"
					BREAK
					
				CASE PED_COMP_FEET
					TextureIDName = "PED_COMP_FEET"
					BREAK
					
				CASE PED_COMP_TEETH
					TextureIDName = "PED_COMP_TEETH"
					BREAK
					
				CASE PED_COMP_SPECIAL
					TextureIDName = "PED_COMP_SPECIAL"
					BREAK
					
				CASE PED_COMP_SPECIAL2
					TextureIDName = "PED_COMP_SPECIAL2"
					BREAK
					
				CASE PED_COMP_DECL
					TextureIDName = "PED_COMP_DECL"
					BREAK
					
				CASE PED_COMP_JBIB
					TextureIDName = "PED_COMP_JBIB"
					BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_TextureIDs - case missing for a TextureID")
					TextureIDName = "TextureIDDetails"
					TextureIDName += tempLoop
					BREAK
			ENDSWITCH
			
			REGISTER_INT_TO_SAVE(paramTextureIDArray[tempLoop], TextureIDName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Save the DrawableIDs array
/// INPUT PARAMS:
///    paramDrawableIDArray		The instance of the Array of DrawableIDs to be saved
///    paramNameOfArray			The name of this instance of the DrawableIDs Array
PROC Register_Array_Of_DrawableIDs(INT &paramDrawableIDArray[NUM_PED_COMPONENTS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramDrawableIDArray, SIZE_OF(paramDrawableIDArray), paramNameOfArray)

		TEXT_LABEL_31 DrawableIDName
		
		REPEAT NUM_PED_COMPONENTS tempLoop
			PED_COMPONENT loopAsEnum = INT_TO_ENUM(PED_COMPONENT, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE PED_COMP_HEAD
					DrawableIDName = "PED_COMP_HEAD"
					BREAK
					
				CASE PED_COMP_BERD
					DrawableIDName = "PED_COMP_BERD"
					BREAK
					
				CASE PED_COMP_HAIR
					DrawableIDName = "PED_COMP_HAIR"
					BREAK
					
				CASE PED_COMP_TORSO
					DrawableIDName = "PED_COMP_TORSO"
					BREAK
					
				CASE PED_COMP_LEG
					DrawableIDName = "PED_COMP_LEG"
					BREAK
					
				CASE PED_COMP_HAND
					DrawableIDName = "PED_COMP_HAND"
					BREAK
					
				CASE PED_COMP_FEET
					DrawableIDName = "PED_COMP_FEET"
					BREAK
					
				CASE PED_COMP_TEETH
					DrawableIDName = "PED_COMP_TEETH"
					BREAK
					
				CASE PED_COMP_SPECIAL
					DrawableIDName = "PED_COMP_SPECIAL"
					BREAK
					
				CASE PED_COMP_SPECIAL2
					DrawableIDName = "PED_COMP_SPECIAL2"
					BREAK
					
				CASE PED_COMP_DECL
					DrawableIDName = "PED_COMP_DECL"
					BREAK
					
				CASE PED_COMP_JBIB
					DrawableIDName = "PED_COMP_JBIB"
					BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_DrawableIDs - case missing for a DrawableID")
					DrawableIDName = "DrawableIDDetails"
					DrawableIDName += tempLoop
					BREAK
			ENDSWITCH
			
			REGISTER_INT_TO_SAVE(paramDrawableIDArray[tempLoop], DrawableIDName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Save the PaletteIDs array
/// INPUT PARAMS:
///    paramDrawableIDArray		The instance of the Array of PaletteIDs to be saved
///    paramNameOfArray			The name of this instance of the PaletteIDs Array
PROC Register_Array_Of_PaletteIDs(INT &paramPaletteIDArray[NUM_PED_COMPONENTS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramPaletteIDArray, SIZE_OF(paramPaletteIDArray), paramNameOfArray)

		TEXT_LABEL_31 PaletteIDName
		
		REPEAT NUM_PED_COMPONENTS tempLoop
			PED_COMPONENT loopAsEnum = INT_TO_ENUM(PED_COMPONENT, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE PED_COMP_HEAD
					PaletteIDName = "PED_COMP_HEAD"
					BREAK
					
				CASE PED_COMP_BERD
					PaletteIDName = "PED_COMP_BERD"
					BREAK
					
				CASE PED_COMP_HAIR
					PaletteIDName = "PED_COMP_HAIR"
					BREAK
					
				CASE PED_COMP_TORSO
					PaletteIDName = "PED_COMP_TORSO"
					BREAK
					
				CASE PED_COMP_LEG
					PaletteIDName = "PED_COMP_LEG"
					BREAK
					
				CASE PED_COMP_HAND
					PaletteIDName = "PED_COMP_HAND"
					BREAK
					
				CASE PED_COMP_FEET
					PaletteIDName = "PED_COMP_FEET"
					BREAK
					
				CASE PED_COMP_TEETH
					PaletteIDName = "PED_COMP_TEETH"
					BREAK
					
				CASE PED_COMP_SPECIAL
					PaletteIDName = "PED_COMP_SPECIAL"
					BREAK
					
				CASE PED_COMP_SPECIAL2
					PaletteIDName = "PED_COMP_SPECIAL2"
					BREAK
					
				CASE PED_COMP_DECL
					PaletteIDName = "PED_COMP_DECL"
					BREAK
					
				CASE PED_COMP_JBIB
					PaletteIDName = "PED_COMP_JBIB"
					BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_PaletteIDs - case missing for a PaletteID")
					PaletteIDName = "PaletteIDDetails"
					PaletteIDName += tempLoop
					BREAK
			ENDSWITCH
			
			REGISTER_INT_TO_SAVE(paramPaletteIDArray[tempLoop], PaletteIDName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Save the Props array
/// INPUT PARAMS:
///    paramPropsArray			The instance of the Array of Props to be saved
///    paramNameOfArray			The name of this instance of the Props Array
PROC Register_Array_Of_Props(INT &paramPropsArray[NUM_PLAYER_PED_PROPS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramPropsArray, SIZE_OF(paramPropsArray), paramNameOfArray)

		TEXT_LABEL_31 PropName
		
		REPEAT NUM_PLAYER_PED_PROPS tempLoop
			PED_PROP_POSITION loopAsEnum = INT_TO_ENUM(PED_PROP_POSITION, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE ANCHOR_HEAD
					PropName = "ANCHOR_HEAD"
					BREAK
					
				CASE ANCHOR_EYES
					PropName = "ANCHOR_EYES"
					BREAK
					
				CASE ANCHOR_EARS
					PropName = "ANCHOR_EARS"
					BREAK
					
				CASE ANCHOR_MOUTH
					PropName = "ANCHOR_MOUTH"
					BREAK
					
				CASE ANCHOR_LEFT_HAND
					PropName = "ANCHOR_LEFT_HAND"
					BREAK
					
				CASE ANCHOR_RIGHT_HAND
					PropName = "ANCHOR_RIGHT_HAND"
					BREAK
					
				CASE ANCHOR_LEFT_WRIST
					PropName = "ANCHOR_LEFT_WRIST"
					BREAK
					
				CASE ANCHOR_RIGHT_WRIST
					PropName = "ANCHOR_RIGHT_WRIST"
					BREAK
					
				CASE ANCHOR_HIP
					PropName = "ANCHOR_HIP"
					BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Props - case missing for a Prop")
					PropName = "PropDetails"
					PropName += tempLoop
					BREAK
			ENDSWITCH
			
			REGISTER_INT_TO_SAVE(paramPropsArray[tempLoop], PropName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the struct containing the players variations
/// INPUT PARAMS:
///    paramVariationsSaved			This instance of the Ped Variations to be saved
///    paramNameOfStruct			The name of this instance of the Ped Variations struct
PROC Register_Struct_PlayerPedVariations(PED_VARIATION_STRUCT &paramVariationsSaved, STRING paramNameOfStruct)
	
	START_SAVE_STRUCT_WITH_SIZE(paramVariationsSaved, SIZE_OF(paramVariationsSaved), paramNameOfStruct)
		Register_Array_Of_TextureIDs(paramVariationsSaved.iDrawableVariation, "DRAWABLE_VARIATION")
		Register_Array_Of_DrawableIDs(paramVariationsSaved.iTextureVariation, "TEXTURE_VARIATION")
		Register_Array_Of_PaletteIDs(paramVariationsSaved.iPaletteVariation, "PALETTE_VARIATION")
		Register_Array_Of_Props(paramVariationsSaved.iPropIndex, "PROP_INDEX")
		Register_Array_Of_Props(paramVariationsSaved.iPropTexture, "PROP_TEXTURE")
		
		// hair / masks stuff
		REGISTER_ENUM_TO_SAVE(paramVariationsSaved.eStoredHairstyle, "STORED_HAIR")
		REGISTER_ENUM_TO_SAVE(paramVariationsSaved.eItemThatForcedHairChange, "HAIR_CHANGE_ITEM")
		REGISTER_ENUM_TO_SAVE(paramVariationsSaved.eTypeThatForcedHairChange, "HAIR_CHANGE_TYPE")
		// Beard / masks stuff
		REGISTER_ENUM_TO_SAVE(paramVariationsSaved.eStoredBeard, "STORED_BEARD")
		REGISTER_ENUM_TO_SAVE(paramVariationsSaved.eItemThatForcedBeardChange, "BEARD_CHANGE_ITEM")
		REGISTER_ENUM_TO_SAVE(paramVariationsSaved.eTypeThatForcedBeardChange, "BEARD_CHANGE_TYPE")
	STOP_SAVE_STRUCT()
ENDPROC

/// PURPOSE:
///    Save the Unlocked Weapons array
/// INPUT PARAMS:
///    paramUnlockedWeaponsArray	The instance of the Array of Unlocked Weapons to be saved
///    paramNameOfArray				The name of this instance of the Weapons Array
PROC Register_Array_Of_Weapon_Unlocks(INT &paramUnlockedWeaponsArray[NUM_PLAYER_PED_WEAPON_BIT_SETS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramUnlockedWeaponsArray, SIZE_OF(paramUnlockedWeaponsArray), paramNameOfArray)

		TEXT_LABEL_31 UnlockedBitSet
		
		REPEAT NUM_PLAYER_PED_WEAPON_BIT_SETS tempLoop
			UnlockedBitSet = "BITSET_"
			UnlockedBitSet += tempLoop
			REGISTER_ENUM_TO_SAVE(paramUnlockedWeaponsArray[tempLoop], UnlockedBitSet)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Save the Unlocked Weapon Mods array
/// INPUT PARAMS:
///    paramUnlockedWeaponModsArray	The instance of the Array of Unlocked Weapon Mods to be saved
///    paramNameOfArray				The name of this instance of the Weapon Mods Array
PROC Register_Array_Of_WeaponMod_Unlocks(INT &paramUnlockedWeaponModsArray[NUM_PLAYER_PED_WEAPON_MOD_BIT_SETS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramUnlockedWeaponModsArray, SIZE_OF(paramUnlockedWeaponModsArray), paramNameOfArray)

		TEXT_LABEL_31 UnlockedBitSet
		
		REPEAT NUM_PLAYER_PED_WEAPON_MOD_BIT_SETS tempLoop
			UnlockedBitSet = "BITSET_"
			UnlockedBitSet += tempLoop
			REGISTER_ENUM_TO_SAVE(paramUnlockedWeaponModsArray[tempLoop], UnlockedBitSet)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

PROC Register_Array_Of_WeaponInfos(WEAPON_INFO& paramWeaponInfos[NUM_PLAYER_PED_WEAPON_SLOTS], STRING paramNameOfArray)
	INT tempLoop = 0
	TEXT_LABEL_31 TempName
	
	START_SAVE_ARRAY_WITH_SIZE(paramWeaponInfos, SIZE_OF(paramWeaponInfos), paramNameOfArray)
		
		REPEAT NUM_PLAYER_PED_WEAPON_SLOTS tempLoop
			TempName = "WEAPON_IN_SLOT_"
			TempName += tempLoop
			REGISTER_ENUM_TO_SAVE(paramWeaponInfos[tempLoop].eWeaponType, TempName)
			
			TempName = "AMMO_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iAmmoCount, TempName)
			
			TempName = "MOD_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iModsAsBitfield, TempName)
			
			TempName = "TINT_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iTint, TempName)
			
			TempName = "CAMO_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iCamo, TempName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

PROC Register_Array_Of_DLCWeaponInfos(WEAPON_INFO& paramWeaponInfos[NUMBER_OF_DLC_WEAPONS], STRING paramNameOfArray)
	INT tempLoop = 0
	TEXT_LABEL_31 TempName
	
	START_SAVE_ARRAY_WITH_SIZE(paramWeaponInfos, SIZE_OF(paramWeaponInfos), paramNameOfArray)
		
		REPEAT NUMBER_OF_DLC_WEAPONS tempLoop
			TempName = "WEAPON_IN_SLOT_"
			TempName += tempLoop
			REGISTER_ENUM_TO_SAVE(paramWeaponInfos[tempLoop].eWeaponType, TempName)
			
			TempName = "AMMO_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iAmmoCount, TempName)
			
			TempName = "MOD_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iModsAsBitfield, TempName)
			
			TempName = "TINT_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iTint, TempName)
			
			TempName = "CAMO_IN_SLOT_"
			TempName += tempLoop
			REGISTER_INT_TO_SAVE(paramWeaponInfos[tempLoop].iCamo, TempName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Register the struct containing the players weapons
/// INPUT PARAMS:
///    paramVariationsSaved			This instance of the Ped Weapons to be saved
///    paramNameOfStruct			The name of this instance of the Ped Weapons struct
PROC Register_Struct_PlayerWeapons(PED_WEAPONS_STRUCT &paramWeaponsSaved, STRING paramNameOfStruct)
	
	START_SAVE_STRUCT_WITH_SIZE(paramWeaponsSaved, SIZE_OF(paramWeaponsSaved), paramNameOfStruct)
		Register_Array_Of_WeaponInfos(paramWeaponsSaved.sWeaponInfo, "WEAPON_INFO")
		Register_Array_Of_DLCWeaponInfos(paramWeaponsSaved.sDLCWeaponInfo, "DLC_INFO")
	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Save the Mods array
/// INPUT PARAMS:
///    paramModsArray			The instance of the Array of Mods to be saved
///    paramNameOfArray			The name of this instance of the Mods Array
PROC Register_Array_Of_Mods(INT &paramModsArray[NUM_PLAYER_PED_MOD_WEAPONS], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramModsArray, SIZE_OF(paramModsArray), paramNameOfArray)

		TEXT_LABEL_31 ModWeapon
		
		REPEAT NUM_PLAYER_PED_MOD_WEAPONS tempLoop
			ModWeapon = "MOD_WEAPON_"
			ModWeapon += tempLoop
			REGISTER_INT_TO_SAVE(paramModsArray[tempLoop], ModWeapon)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the struct containing the players LastActiveTime
/// INPUT PARAMS:
///    paramLastActiveTimeSaved		This instance of the LastActiveTime to be saved
///    paramNameOfStruct			The name of this instance of the LastActiveTime struct
PROC Register_Struct_LastTimeActive(TIMEOFDAY &paramLastActiveTimeSaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramLastActiveTimeSaved, SIZE_OF(paramLastActiveTimeSaved), paramNameOfStruct)

		/*
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iSeconds, "SECONDS")
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iMinutes, "MINUTES")
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iHours, "HOURS")
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iDayOfWeek, "DAY_OF_WEEK")
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iDayOfMonth, "DAY_OF_MONTH")
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iMonth, "MONTH")
		REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iYear, "YEAR")
		*/
		
		REGISTER_ENUM_TO_SAVE(paramLastActiveTimeSaved, "iStoredTime")
		//REGISTER_INT_TO_SAVE(paramLastActiveTimeSaved.iYears, "iYear")
		
	STOP_SAVE_STRUCT()
	
ENDPROC


/// PURPOSE:
///    Register the struct containing the players SavedVehicleData
/// INPUT PARAMS:
///    paramLastActiveTimeSaved		This instance of the SavedVehicleData to be saved
///    paramNameOfStruct			The name of this instance of the SavedVehicleData struct
PROC Register_Struct_SavedVehData(PED_VEH_DATA_STRUCT &paramSavedVehSaved, STRING paramNameOfStruct)

	INT tempLoop
	TEXT_LABEL_15 tempLabel
	
	START_SAVE_STRUCT_WITH_SIZE(paramSavedVehSaved, SIZE_OF(paramSavedVehSaved), paramNameOfStruct)		

		REGISTER_ENUM_TO_SAVE(paramSavedVehSaved.model, "model")
		REGISTER_ENUM_TO_SAVE(paramSavedVehSaved.modelTrailer, "modelTrailer")
		REGISTER_FLOAT_TO_SAVE(paramSavedVehSaved.fDirtLevel, "fDirtLevel")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.fHealth, "fHealth")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iColourCombo, "iColourCombo")
		
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iColour1, "iColour1")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iColour2, "iColour2")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iColourExtra1, "iColourExtra1")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iColourExtra2, "iColourExtra2")
		
		REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bColourCombo, "bColourCombo")
		REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bColourExtra, "bColourExtra")
		
		START_SAVE_ARRAY_WITH_SIZE(paramSavedVehSaved.bExtraOn, SIZE_OF(paramSavedVehSaved.bExtraOn), "EXTRAS")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[0], "Extra0")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[1], "Extra1")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[2], "Extra2")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[3], "Extra3")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[4], "Extra4")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[5], "Extra5")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[6], "Extra6")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[7], "Extra7")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[8], "Extra8")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[9], "Extra9")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[10], "Extra10")
			REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bExtraOn[11], "Extra11")
		STOP_SAVE_ARRAY()
		REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bConvertible, "bConvertible")
		
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iRadioIndex, "iRadioIndex")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iPlateBack, "iPlateBack")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramSavedVehSaved.tlNumberPlate, "tlNumberPlate")
		
		START_SAVE_ARRAY_WITH_SIZE(paramSavedVehSaved.iModIndex, SIZE_OF(paramSavedVehSaved.iModIndex), "VEH_MOD_ID")
			REPEAT MAX_VEHICLE_MOD_SLOTS tempLoop
				tempLabel = "MOD_ID" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(paramSavedVehSaved.iModIndex[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramSavedVehSaved.iModVariation, SIZE_OF(paramSavedVehSaved.iModVariation), "VEH_MOD_VAR")
			REPEAT MAX_VEHICLE_MOD_VAR_SLOTS tempLoop
				tempLabel = "MOD_VAR" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(paramSavedVehSaved.iModVariation[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iTyreR, "iTyreR")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iTyreG, "iTyreG")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iTyreB, "iTyreB")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iWindowTintColour, "iWindowTintColour")
		REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bTyresCanBurst, "bTyresCanBurst")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iLivery, "iLivery")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iWheelType, "iWheelType")
		
		REGISTER_FLOAT_TO_SAVE(paramSavedVehSaved.fEnvEff, "fEnvEff")
		REGISTER_BOOL_TO_SAVE(paramSavedVehSaved.bIsPlayerVehicle, "bIsPlayerVehicle")
		
		REGISTER_ENUM_TO_SAVE(paramSavedVehSaved.eType, "eType")
		
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iNeonR, "iNeonR")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iNeonG, "iNeonG")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iNeonB, "iNeonB")
		REGISTER_INT_TO_SAVE(paramSavedVehSaved.iFlags, "iFlags")
	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Register the struct containing the limited wardrobe items
/// INPUT PARAMS:
///    paramWardrobeItems			This instance of the Limited Wardrobe Items to be saved
///    paramNameOfStruct			The name of this instance of the Limited Wardrobe Items struct
PROC Register_Struct_LimitedWardrobeItems(LIMITED_WARDROBE_ITEMS &paramWardrobeItems, STRING paramNameOfStruct)
	
	START_SAVE_STRUCT_WITH_SIZE(paramWardrobeItems, SIZE_OF(paramWardrobeItems), paramNameOfStruct)
		START_SAVE_ARRAY_WITH_SIZE(paramWardrobeItems.iItemBitset, SIZE_OF(paramWardrobeItems.iItemBitset), "ITEM_BITSET")
			INT iComp, iBitset
			TEXT_LABEL_15 RegistrationName
			
			REPEAT NUMBER_OF_PED_COMP_TYPES iComp
				RegistrationName = "COMP" RegistrationName += iComp
				START_SAVE_ARRAY_WITH_SIZE(paramWardrobeItems.iItemBitset[iComp], SIZE_OF(paramWardrobeItems.iItemBitset[iComp]), RegistrationName)
					REPEAT PED_COMPONENT_BITSETS iBitset
						RegistrationName = "BITSET" RegistrationName += iBitset
						REGISTER_INT_TO_SAVE(paramWardrobeItems.iItemBitset[iComp][iBitset], RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
	STOP_SAVE_STRUCT()
	
ENDPROC


/// PURPOSE:
///    Register the struct containing the player ped info
/// INPUT PARAMS:
///    paramComponentsSaved			This instance of the Control Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_PlayerPedInfoSaved(PED_INFO_STRUCT &paramPedInfoSaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramPedInfoSaved, SIZE_OF(paramPedInfoSaved), paramNameOfStruct)

		INT iPed
		TEXT_LABEL_63 RegistrationName
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sVariations, SIZE_OF(paramPedInfoSaved.sVariations), "VARIATIONS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "VARIATIONS")
				Register_Struct_PlayerPedVariations(paramPedInfoSaved.sVariations[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eGolfTop, SIZE_OF(paramPedInfoSaved.eGolfTop), "GOLF_TOPS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "GOLF_TOPS")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eGolfTop[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eGolfPants, SIZE_OF(paramPedInfoSaved.eGolfPants), "GOLF_PANTS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "GOLF_PANTS")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eGolfPants[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sWeapons, SIZE_OF(paramPedInfoSaved.sWeapons), "WEAPONS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "WEAPONS")
				Register_Struct_PlayerWeapons(paramPedInfoSaved.sWeapons[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eWeaponToShow, SIZE_OF(paramPedInfoSaved.eWeaponToShow), "WEAPON_TO_SHOW")
			INT iWheelSlot
			REPEAT MAX_WHEEL_SLOTS iWheelSlot
				RegistrationName = "WHEEL_SLOT_" RegistrationName += iWheelSlot
				START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eWeaponToShow[iWheelSlot], SIZE_OF(paramPedInfoSaved.eWeaponToShow[iWheelSlot]), RegistrationName)
					REPEAT NUM_OF_PLAYABLE_PEDS iPed
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "WEAPON")
						REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eWeaponToShow[iWheelSlot][iPed], RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_INT_TO_SAVE(paramPedInfoSaved.equippedWeaponSlot, "EQUIPPED_WEAPON_SLOT")
		
		Register_Struct_PlayerWeapons(paramPedInfoSaved.sMissionSnapshotWeapons, "SNAP_WEAP")
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.fHealthPerc, SIZE_OF(paramPedInfoSaved.fHealthPerc), "HEALTH_PERCENTAGE")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "HEALTH_PERCENTAGE")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.fHealthPerc[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iArmour, SIZE_OF(paramPedInfoSaved.iArmour), "ARMOUR")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "ARMOUR")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iArmour[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
				
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.bPedAvailable, SIZE_OF(paramPedInfoSaved.bPedAvailable), "AVAILABLE")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "AVAILABLE")
				REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bPedAvailable[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iUnlockedCarmods, SIZE_OF(paramPedInfoSaved.iUnlockedCarmods), "CARMODS")
			INT iModBitset
			REPEAT 5 iModBitset
				RegistrationName = ADD_PED_NAME_TO_STRING(iModBitset, "_CARMODS")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iUnlockedCarmods[iModBitset], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iPlayerStatScript, SIZE_OF(paramPedInfoSaved.iPlayerStatScript), "STAT_UPDATES")
			INT iStat
			REPEAT NUMBER_OF_PLAYER_STATS iStat
				RegistrationName = "STATS" RegistrationName += iStat
				START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iPlayerStatScript[iStat], SIZE_OF(paramPedInfoSaved.iPlayerStatScript[iStat]), RegistrationName)
					REPEAT NUM_OF_PLAYABLE_PEDS iPed
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_STATS")
						REGISTER_INT_TO_SAVE(paramPedInfoSaved.iPlayerStatScript[iStat][iPed], RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eTrackedPedComp, SIZE_OF(paramPedInfoSaved.eTrackedPedComp), "TRACKED_COMPS")
			INT iComp
			REPEAT NUM_PED_COMPONENTS iComp
				RegistrationName = "COMP" RegistrationName += iComp
				START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eTrackedPedComp[iComp], SIZE_OF(paramPedInfoSaved.eTrackedPedComp[iComp]), RegistrationName)
					REPEAT NUM_OF_PLAYABLE_PEDS iPed
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_COMP")
						REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eTrackedPedComp[iComp][iPed], RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eTrackedPedProps, SIZE_OF(paramPedInfoSaved.eTrackedPedProps), "TRACKED_PROPS")
			INT iProp
			REPEAT NUM_PLAYER_PED_PROPS iProp
				RegistrationName = "PROP" RegistrationName += iProp
				START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eTrackedPedProps[iProp], SIZE_OF(paramPedInfoSaved.eTrackedPedProps[iProp]), RegistrationName)
					REPEAT NUM_OF_PLAYABLE_PEDS iPed
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_PROPS")
						REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eTrackedPedProps[iProp][iPed], RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iWheelieDayTimer, SIZE_OF(paramPedInfoSaved.iWheelieDayTimer), "WH_TIMER")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_WHTIMER")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iWheelieDayTimer[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iWheelieUpdatesThisDay, SIZE_OF(paramPedInfoSaved.iWheelieUpdatesThisDay), "WH_UPDATE")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_WHUPDATE")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iWheelieUpdatesThisDay[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iWheelieTimeAtStartOfDay, SIZE_OF(paramPedInfoSaved.iWheelieTimeAtStartOfDay), "WH_TIME")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_WHTIME")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iWheelieTimeAtStartOfDay[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.bGrabCurrentWheelieTime, SIZE_OF(paramPedInfoSaved.bGrabCurrentWheelieTime), "GRAB_TIME")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "_GRABTIME")
				REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bGrabCurrentWheelieTime[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		INT iSlot
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.vPlayerVehicleCoords, SIZE_OF(paramPedInfoSaved.vPlayerVehicleCoords), "VEH_POS")
			REPEAT 2 iSlot
				RegistrationName = "VEH_POS_" RegistrationName += iSlot
				START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.vPlayerVehicleCoords[iSlot], SIZE_OF(paramPedInfoSaved.vPlayerVehicleCoords[iSlot]), RegistrationName)
					REPEAT NUM_OF_PLAYABLE_PEDS iPed
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "VEH_POS_X")
						REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vPlayerVehicleCoords[iSlot][iPed].x, RegistrationName)
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "VEH_POS_Y")
						REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vPlayerVehicleCoords[iSlot][iPed].y, RegistrationName)
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "VEH_POS_Z")
						REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vPlayerVehicleCoords[iSlot][iPed].z, RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.fPlayerVehicleHeading, SIZE_OF(paramPedInfoSaved.fPlayerVehicleHeading), "VEH_HEAD")
			REPEAT 2 iSlot
				RegistrationName = "VEH_HEAD_" RegistrationName += iSlot
				START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.fPlayerVehicleHeading[iSlot], SIZE_OF(paramPedInfoSaved.fPlayerVehicleHeading[iSlot]), RegistrationName)
					REPEAT NUM_OF_PLAYABLE_PEDS iPed
						RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "VEH_HEAD")
						REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.fPlayerVehicleHeading[iSlot][iPed], RegistrationName)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.fLastKnownHead, SIZE_OF(paramPedInfoSaved.fLastKnownHead), "LAST_KNOWN_HEADING")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_HEADING")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.fLastKnownHead[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iLastKnownRoomKey, SIZE_OF(paramPedInfoSaved.iLastKnownRoomKey), "LAST_KNOWN_ROOM_KEY")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_ROOM_KEY")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iLastKnownRoomKey[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.vLastKnownVelocity, SIZE_OF(paramPedInfoSaved.vLastKnownVelocity), "LAST_KNOWN_VELOCITY")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_VELOCITY_X")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vLastKnownVelocity[iPed].x, RegistrationName)
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_VELOCITY_Y")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vLastKnownVelocity[iPed].y, RegistrationName)
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_VELOCITY_Z")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vLastKnownVelocity[iPed].z, RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iLastKnownWantedLevel, SIZE_OF(paramPedInfoSaved.iLastKnownWantedLevel), "LAST_KNOWN_WANTED")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_WANTED")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iLastKnownWantedLevel[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.bChangedClothesOnMission, SIZE_OF(paramPedInfoSaved.bChangedClothesOnMission), "CHANGE_ON_MISS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "CHANGE_ON_MISS")
				REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bChangedClothesOnMission[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sLastTimeWeChangedClothes, SIZE_OF(paramPedInfoSaved.sLastTimeWeChangedClothes), "CHANGED_CLOTHES")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "CHANGED_CLOTHES")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.sLastTimeWeChangedClothes[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iLastTimeWeChangedHairdo, SIZE_OF(paramPedInfoSaved.iLastTimeWeChangedHairdo), "CHANGED_HAIR")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "CHANGED_HAIR")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iLastTimeWeChangedHairdo[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iLastTimeWeGotTattoo, SIZE_OF(paramPedInfoSaved.iLastTimeWeGotTattoo), "CHANGED_TATTS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "CHANGED_TATTS")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iLastTimeWeGotTattoo[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.vLastKnownCoords, SIZE_OF(paramPedInfoSaved.vLastKnownCoords), "LAST_KNOWN_XCOORD")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_XCOORD")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vLastKnownCoords[iPed].x, RegistrationName)
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_YCOORD")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vLastKnownCoords[iPed].y, RegistrationName)
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_KNOWN_ZCOORD")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.vLastKnownCoords[iPed].z, RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sLastTimeActive, SIZE_OF(paramPedInfoSaved.sLastTimeActive), "LAST_TIME_ACTIVE")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "LAST_TIME_ACTIVE")
				Register_Struct_LastTimeActive(paramPedInfoSaved.sLastTimeActive[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sPlayerVehicle, SIZE_OF(paramPedInfoSaved.sPlayerVehicle), "STORED_VEH_DATA")
			START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR], SIZE_OF(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR]), "STORED_CAR_DATA")
				REPEAT NUM_OF_PLAYABLE_PEDS iPed
					RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STORED_CAR_DATA")
					Register_Struct_SavedVehData(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][iPed], RegistrationName)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE], SIZE_OF(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE]), "STORED_BIK_DATA")
				REPEAT NUM_OF_PLAYABLE_PEDS iPed
					RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STORED_BIK_DATA")
					Register_Struct_SavedVehData(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][iPed], RegistrationName)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_GARAGE], SIZE_OF(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_GARAGE]), "STORED_GAR_DATA")
				REPEAT NUM_OF_PLAYABLE_PEDS iPed
					RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STORED_GAR_DATA")
					Register_Struct_SavedVehData(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_GARAGE][iPed], RegistrationName)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_MODDED], SIZE_OF(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_MODDED]), "STORED_MOD_DATA")
				REPEAT NUM_OF_PLAYABLE_PEDS iPed
					RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STORED_MOD_DATA")
					Register_Struct_SavedVehData(paramPedInfoSaved.sPlayerVehicle[SAVED_VEHICLE_SLOT_MODDED][iPed], RegistrationName)
				ENDREPEAT
			STOP_SAVE_ARRAY()
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sNPCVehicle, SIZE_OF(paramPedInfoSaved.sNPCVehicle), "NPC_VEH_DATA")
			START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR], SIZE_OF(paramPedInfoSaved.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR]), "NPC_CAR_DATA")
				// Currently only used for Amanda.
				Register_Struct_SavedVehData(paramPedInfoSaved.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][0], "AMANDAS_CAR")
				Register_Struct_SavedVehData(paramPedInfoSaved.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][1], "TRACEYS_CAR")
			STOP_SAVE_ARRAY()
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.bSpecialAbilityUnlocked, SIZE_OF(paramPedInfoSaved.bSpecialAbilityUnlocked), "SPECIAL_ABILITY")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "SPECIAL_ABILITY")
				REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bSpecialAbilityUnlocked[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
				
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.fStatOffset_DistRunning, SIZE_OF(paramPedInfoSaved.fStatOffset_DistRunning), "STAT_OFFSET_1")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_1")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.fStatOffset_DistRunning[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iStatOffset_HitsUnarmed, SIZE_OF(paramPedInfoSaved.iStatOffset_HitsUnarmed), "STAT_OFFSET_2")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_2")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iStatOffset_HitsUnarmed[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iStatOffset_NumberNearMisses, SIZE_OF(paramPedInfoSaved.iStatOffset_NumberNearMisses), "STAT_OFFSET_3")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_3")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iStatOffset_NumberNearMisses[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.fStatOffset_DistStealth, SIZE_OF(paramPedInfoSaved.fStatOffset_DistStealth), "STAT_OFFSET_4")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_4")
				REGISTER_FLOAT_TO_SAVE(paramPedInfoSaved.fStatOffset_DistStealth[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iStatOffset_StealtKills, SIZE_OF(paramPedInfoSaved.iStatOffset_StealtKills), "STAT_OFFSET_5")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_5")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iStatOffset_StealtKills[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iStatOffset_HitsMission, SIZE_OF(paramPedInfoSaved.iStatOffset_HitsMission), "STAT_OFFSET_6")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_6")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iStatOffset_HitsMission[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iStatOffset_HitsOffMission, SIZE_OF(paramPedInfoSaved.iStatOffset_HitsOffMission), "STAT_OFFSET_7")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "STAT_OFFSET_7")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iStatOffset_HitsOffMission[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
			
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iTrackedSpecialAbilityUnlock, SIZE_OF(paramPedInfoSaved.iTrackedSpecialAbilityUnlock), "SPEC_AB_UNLOCK")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "SPEC_AB_UNLOCK")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iTrackedSpecialAbilityUnlock[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.iTrackedPedComps, SIZE_OF(paramPedInfoSaved.iTrackedPedComps), "PED_COMPS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "PED_COMPS")
				REGISTER_INT_TO_SAVE(paramPedInfoSaved.iTrackedPedComps[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eFBI4MaskName, SIZE_OF(paramPedInfoSaved.eFBI4MaskName), "FBI4MASKSNAME")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "FBI4MASKNAME")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eFBI4MaskName[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()

		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eFBI4MaskType, SIZE_OF(paramPedInfoSaved.eFBI4MaskType), "FBI4MASKSTYPE")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "FBI4MASKTYPE")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eFBI4MaskType[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()

		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eFBI4BoilerSuit, SIZE_OF(paramPedInfoSaved.eFBI4BoilerSuit), "FBI4SUITS")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "FBI4SUIT")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eFBI4BoilerSuit[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.sLimitedWardrobeItems, SIZE_OF(paramPedInfoSaved.sLimitedWardrobeItems), "WARDROBE")
			INT iWardrobe
			REPEAT NUM_PLAYER_PED_LIMITED_WARDROBES iWardrobe
				RegistrationName = "WARDROBE"
				RegistrationName += iWardrobe
				Register_Struct_LimitedWardrobeItems(paramPedInfoSaved.sLimitedWardrobeItems[iWardrobe], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramPedInfoSaved.eLastVehicleSentToCloudSavehouse, SIZE_OF(paramPedInfoSaved.eLastVehicleSentToCloudSavehouse), "CLOUD_VEH")
			REPEAT NUM_OF_PLAYABLE_PEDS iPed
				RegistrationName = ADD_PED_NAME_TO_STRING(iPed, "CLOUD_VEH")
				REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eLastVehicleSentToCloudSavehouse[iPed], RegistrationName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bDefaultInfoSet, "PP_DEFAULT_INFO_SET")
		REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bDefaultClothesInfoSet, "PP_DEFAULT_CLOTHES_INFO_SET")
		REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bDefaultStatsSet, "PP_DEFAULT_STATS_SET")
		
		REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bPlayerVariationsStoredToStats, "PP_VARS_IN_STATS")
		
		REGISTER_BOOL_TO_SAVE(paramPedInfoSaved.bJimmyModsSetOnMichaelsCar, "PP_JIMMYMODS")
		
		REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eCurrentPed, "PP_CURRENT_PED")
		REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.ePreviousPed, "PP_PREVIOUS_PED")
		REGISTER_ENUM_TO_SAVE(paramPedInfoSaved.eLastKnownPed, "PP_LAST_KNOWN_PED")
	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Register the struct containing the player ped tattoo data
/// INPUT PARAMS:
///    paramTattoosSaved		This instance of the Control Struct to be saved
///    paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_PlayerPedTattooSaved(PED_TATTOO_STRUCT &paramTattoosSaved, STRING paramNameOfStruct)

	INT tempLoop
	TEXT_LABEL_31 ItemName
	
	START_SAVE_STRUCT_WITH_SIZE(paramTattoosSaved, SIZE_OF(paramTattoosSaved), paramNameOfStruct)	
		START_SAVE_ARRAY_WITH_SIZE(paramTattoosSaved.iUnlockedTattoos, SIZE_OF(paramTattoosSaved.iUnlockedTattoos), "UNLOCKED")	
			REPEAT MAX_NUMBER_OF_TATTOO_BITSETS tempLoop
				ItemName = "unlockedBitset"
				ItemName += tempLoop
				REGISTER_INT_TO_SAVE(paramTattoosSaved.iUnlockedTattoos[tempLoop], ItemName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramTattoosSaved.iViewedTattoos, SIZE_OF(paramTattoosSaved.iViewedTattoos), "VIEWED")	
			REPEAT MAX_NUMBER_OF_TATTOO_BITSETS tempLoop
				ItemName = "viewedBitset"
				ItemName += tempLoop
				REGISTER_INT_TO_SAVE(paramTattoosSaved.iViewedTattoos[tempLoop], ItemName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramTattoosSaved.iCurrentTattoos, SIZE_OF(paramTattoosSaved.iCurrentTattoos), "CURRENT")	
			REPEAT MAX_NUMBER_OF_TATTOO_BITSETS tempLoop
				ItemName = "currentBitset"
				ItemName += tempLoop
				REGISTER_INT_TO_SAVE(paramTattoosSaved.iCurrentTattoos[tempLoop], ItemName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
	STOP_SAVE_STRUCT()
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the player ped globals that need to be saved
PROC Register_Player_Ped_Saved_Globals()

	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sPlayerData.sComponents, SIZE_OF(g_savedGlobals.sPlayerData.sComponents), "COMPONENTS_ARRAY")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobals.sPlayerData.sComponents[CHAR_MICHAEL], "SP0")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobals.sPlayerData.sComponents[CHAR_FRANKLIN], "SP1")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobals.sPlayerData.sComponents[CHAR_TREVOR], "SP2")
	STOP_SAVE_ARRAY()
	
	Register_Struct_PlayerPedInfoSaved(g_savedGlobals.sPlayerData.sInfo, "PP_INFO_STRUCT")
	
	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sPlayerData.sTattoos, SIZE_OF(g_savedGlobals.sPlayerData.sTattoos), "TATTOOS_ARRAY")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobals.sPlayerData.sTattoos[CHAR_MICHAEL], "SP0")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobals.sPlayerData.sTattoos[CHAR_FRANKLIN], "SP1")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobals.sPlayerData.sTattoos[CHAR_TREVOR], "SP2")
	STOP_SAVE_ARRAY()
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID, "FRANKLIN_ORIGINAL_OUTFIT_ID")
	REGISTER_ENUM_TO_SAVE(g_savedGlobals.sPlayerData.eOverridePed, "OVERRIDE_PED")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sPlayerData.iDLCPatchBitset, "PLAYER_PED_DATA_PATCH_BITSET")
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the player ped globals that need to be saved
PROC Register_Player_Ped_Saved_Globals_CLF()

	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobalsClifford.sPlayerData.sComponents, SIZE_OF(g_savedGlobalsClifford.sPlayerData.sComponents), "COMPONENTS_ARRAY")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobalsClifford.sPlayerData.sComponents[CHAR_MICHAEL], "SP0")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobalsClifford.sPlayerData.sComponents[CHAR_FRANKLIN], "SP1")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobalsClifford.sPlayerData.sComponents[CHAR_TREVOR], "SP2")
	STOP_SAVE_ARRAY()
	
	Register_Struct_PlayerPedInfoSaved(g_savedGlobalsClifford.sPlayerData.sInfo, "PP_INFO_STRUCT")
	
	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobalsClifford.sPlayerData.sTattoos, SIZE_OF(g_savedGlobalsClifford.sPlayerData.sTattoos), "TATTOOS_ARRAY")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobalsClifford.sPlayerData.sTattoos[CHAR_MICHAEL], "SP0")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobalsClifford.sPlayerData.sTattoos[CHAR_FRANKLIN], "SP1")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobalsClifford.sPlayerData.sTattoos[CHAR_TREVOR], "SP2")
	STOP_SAVE_ARRAY()
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sPlayerData.iFranklinOriginalOutfitID, "FRANKLIN_ORIGINAL_OUTFIT_ID")
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sPlayerData.eOverridePed, "OVERRIDE_PED")
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the player ped globals that need to be saved
PROC Register_Player_Ped_Saved_Globals_NRM()

	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobalsnorman.sPlayerData.sComponents, SIZE_OF(g_savedGlobalsnorman.sPlayerData.sComponents), "COMPONENTS_ARRAY")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobalsnorman.sPlayerData.sComponents[CHAR_MICHAEL], "SP0")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobalsnorman.sPlayerData.sComponents[CHAR_FRANKLIN], "SP1")
		Register_Struct_PlayerPedComponentsSaved(g_savedGlobalsnorman.sPlayerData.sComponents[CHAR_TREVOR], "SP2")
	STOP_SAVE_ARRAY()
	
	Register_Struct_PlayerPedInfoSaved(g_savedGlobalsnorman.sPlayerData.sInfo, "PP_INFO_STRUCT")
	
	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobalsnorman.sPlayerData.sTattoos, SIZE_OF(g_savedGlobalsnorman.sPlayerData.sTattoos), "TATTOOS_ARRAY")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobalsnorman.sPlayerData.sTattoos[CHAR_MICHAEL], "SP0")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobalsnorman.sPlayerData.sTattoos[CHAR_FRANKLIN], "SP1")
		Register_Struct_PlayerPedTattooSaved(g_savedGlobalsnorman.sPlayerData.sTattoos[CHAR_TREVOR], "SP2")
	STOP_SAVE_ARRAY()
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sPlayerData.iFranklinOriginalOutfitID, "FRANKLIN_ORIGINAL_OUTFIT_ID")
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sPlayerData.eOverridePed, "OVERRIDE_PED")
ENDPROC

