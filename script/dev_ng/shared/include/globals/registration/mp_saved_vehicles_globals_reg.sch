USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   mp_saved_vehicles_globals_reg.sch
//      AUTHOR          :   Conor M
//      DESCRIPTION     :   Contains the registrations with code for all MP vehicle
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Save the Dyanmic Veh Gen Data array
/// INPUT PARAMS:
///    paramDynamicDataArray	The instance of the Array of VEHICLE_SETUP_STRUCT to be saved
///    paramNameOfArray			The name of this instance of the VEHICLE_SETUP_STRUCT Array
PROC Register_Contents_Of_GamerHandle(GAMER_HANDLE &gamerHandle, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(gamerHandle, SIZE_OF(gamerHandle), paramNameOfStruct)	

		REGISTER_INT64_TO_SAVE(gamerHandle.Data1, "Data64_1")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data2, "Data64_2")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data3, "Data64_3")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data4, "Data64_4")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data5, "Data64_5")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data6, "Data64_6")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data7, "Data64_7")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data8, "Data64_8")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data9, "Data64_9")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data10, "Data64_10")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data11, "Data64_11")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data12, "Data64_12")
		REGISTER_INT64_TO_SAVE(gamerHandle.Data13, "Data64_13")

	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Save the Dyanmic Veh Gen Data array
/// INPUT PARAMS:
///    paramDynamicDataArray	The instance of the Array of VEHICLE_SETUP_STRUCT to be saved
///    paramNameOfArray			The name of this instance of the VEHICLE_SETUP_STRUCT Array
PROC Register_Contents_Of_DynamicVehGenData_SetupStruct_MP(VEHICLE_SETUP_STRUCT &paramDynamicDataArray, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramDynamicDataArray, SIZE_OF(paramDynamicDataArray), paramNameOfStruct)	

		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iPlateIndex, "iPlateIndex")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramDynamicDataArray.tlPlateText, "tlPlateText")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColour1, "iColour1")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColour2, "iColour2")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColourExtra1, "iColourExtra1")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColourExtra2, "iColourExtra2")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreR, "iTyreR")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreG, "iTyreG")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreB, "iTyreB")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iWindowTintColour, "iWindowTintColour")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iLivery, "iLivery")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iWheelType, "iWheelType")
		REGISTER_ENUM_TO_SAVE(paramDynamicDataArray.eRoofState, "eRoofState")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iFlags, "iFlags")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iCustomR, "iCustomR")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iCustomG, "iCustomG")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iCustomB, "iCustomB")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iNeonR, "iNeonR")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iNeonG, "iNeonG")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iNeonB, "iNeonB")
		
		REGISTER_ENUM_TO_SAVE(paramDynamicDataArray.eModel, "eModel")
		
		INT tempLoop
		TEXT_LABEL_15 tempLabel
		START_SAVE_ARRAY_WITH_SIZE(paramDynamicDataArray.iModIndex, SIZE_OF(paramDynamicDataArray.iModIndex), "MODS_ID")
			REPEAT MAX_VEHICLE_MOD_SLOTS tempLoop
				tempLabel = "MOD_ID" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(paramDynamicDataArray.iModIndex[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramDynamicDataArray.iModVariation, SIZE_OF(paramDynamicDataArray.iModVariation), "MODS_VAR")
			REPEAT MAX_VEHICLE_MOD_VAR_SLOTS tempLoop
				tempLabel = "MOD_VAR" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(paramDynamicDataArray.iModVariation[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_ENUM_TO_SAVE(paramDynamicDataArray.eLockState,"eLockState")

	STOP_SAVE_STRUCT()
	
ENDPROC



//	INT iCustomR, iCustomG, iCustomB
//	FLOAT fEnveffScale
//	BOOL bHasCrewEmblem
//	CONVERTIBLE_ROOF_STATE ConvertibleRoofState
//	
//	GAMER_HANDLE GamerHandleOfCarOwner

/// PURPOSE:
///    Save the Dyanmic Veh Gen Data array
/// INPUT PARAMS:
///    paramDynamicDataArray	The instance of the Array of VEHICLE_SETUP_STRUCT to be saved
///    paramNameOfArray			The name of this instance of the VEHICLE_SETUP_STRUCT Array
PROC Register_Contents_Of_DynamicVehGenData_MP(VEHICLE_SETUP_STRUCT_MP &paramDynamicDataArray, STRING paramNameOfStruct)

	Register_Contents_Of_DynamicVehGenData_SetupStruct_MP(paramDynamicDataArray.VehicleSetup, "VEHICLE_SETUP_STRUCT")
	
	START_SAVE_STRUCT_WITH_SIZE(paramDynamicDataArray, SIZE_OF(paramDynamicDataArray), paramNameOfStruct)	
	
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iSpawnVehicleHorn, "iSpawnVehicleHorn")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iHornID, "iHornID")
		REGISTER_FLOAT_TO_SAVE(paramDynamicDataArray.fEnveffScale, "fEnveffScale")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColour5, "iColour5") 
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iLivery2, "iLivery2")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColour6, "iColour6")
		
		Register_Contents_Of_GamerHandle(paramDynamicDataArray.GamerHandleOfCarOwner, "GamerHandleOfCarOwner")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iOwnerStatus, "OwnerStatus")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iFlags, "iFlags")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iPVSlot, "iPVSlot")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iPlayerIndex, "iPlayerIndex")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iIEPlayerVehicle, "iIEPlayerVehicle")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreType, "iTyreType")

	STOP_SAVE_STRUCT()
	
ENDPROC


///// PURPOSE:
/////    Save the MP_SAVED_VEHICLE_DETAILS_STRUCT
///// INPUT PARAMS:
/////    paramDynamicDataArray	The instance of the Array of VEHICLE_SETUP_STRUCT to be saved
/////    paramNameOfArray			The name of this instance of the VEHICLE_SETUP_STRUCT Array
//PROC Register_Contents_Of_MP_Saved_Vehicle_details_Struct(MP_SAVED_VEHICLE_DETAILS_STRUCT &paramDynamicDataArray, STRING paramNameOfStruct)
//
//	START_SAVE_STRUCT_WITH_SIZE(paramDynamicDataArray, SIZE_OF(paramDynamicDataArray), paramNameOfStruct)	
//
//		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iInsuranceYear, "iInsuranceYear")
//		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iInsuranceMonth, "iInsuranceMonth")
//		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iInsuranceDay, "iInsuranceDay")
//		
//	STOP_SAVE_STRUCT()
//	
//ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for a single saved MP vehicle
PROC Register_Struct_MP_Saved_Vehicles(MP_SAVED_VEHICLE_STRUCT &savedVehicleArray, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(savedVehicleArray, SIZE_OF(savedVehicleArray), paramNameOfStruct)
		
		Register_Contents_Of_DynamicVehGenData_MP(savedVehicleArray.vehicleSetupMP, "VEHICLE_SETUP_STRUCT_MP")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iVehicleBS,"VEHICLE_BS")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iPremiumPaidByDestroyer,"PAID_PREMIUM")
		REGISTER_TEXT_LABEL_63_TO_SAVE(savedVehicleArray.tl31Destroyer,"PAID_PLAYER")
		REGISTER_TEXT_LABEL_63_TO_SAVE(savedVehicleArray.tlRadioStationName,"RADIO_STATION")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iImpoundTime,"IMPOUND_TIME")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iPrimaryColourGroup,"COLOUR_GROUP1")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iSecondaryColourGroup,"COLOUR_GROUP2")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iPricePaid,"PRICE_PAID")
		REGISTER_INT_TO_SAVE(savedVehicleArray.iObtainedTime,"OBTAIN_TIME")
		//REGISTER_INT_TO_SAVE(savedVehicleArray.iModifiedTimeStamp,"MODIFIED_TIME")
		//REGISTER_TEXT_LABEL_23_TO_SAVE(savedVehicleArray.tl23DestroyerUserID,"PAID_USERID")
		//Register_Contents_Of_MP_Saved_Vehicle_details_Struct(savedVehicleArray.vehicleDetails, "MP_SAVED_VEHICLE_DETAILS_STRUCT")
	STOP_SAVE_STRUCT()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each vehicle
PROC Register_Array_Of_Saved_MP_VEHICLES(MP_SAVED_VEHICLE_STRUCT &savedVehicleArray[MAX_MP_SAVED_VEHICLES_SG], STRING paramNameOfArray)

	INT tempLoop = 0
	START_SAVE_ARRAY_WITH_SIZE(savedVehicleArray, SIZE_OF(savedVehicleArray), paramNameOfArray)
		TEXT_LABEL_31 mpVehicleID
		REPEAT MAX_MP_SAVED_VEHICLES_SG tempLoop
			mpVehicleID = "MP_VEHICLE"
			mpVehicleID += tempLoop
			
			// Register details for one element of the Properties Array
			Register_Struct_MP_Saved_Vehicles(savedVehicleArray[tempLoop], mpVehicleID)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the strip club globals that need to be saved
//PROC REGISTER_MP_VEHICLES_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
//	TEXT_LABEL_23 tlSlot = 	"MP_SAVED_VEHICLES"
//	tlSlot += iIndex
//	Register_Array_Of_Saved_MP_VEHICLES(globalData.MpSavedVehicles, tlSlot )
//
//ENDPROC


