// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   offroad_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all offroad
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

PROC ORR_Register_One_Race_BestRanks_Array_Saved(INT & sRaceBestRanks[NUM_OFFROAD_RACES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(sRaceBestRanks, SIZE_OF(sRaceBestRanks), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForVar
		INT iRaceIndex	
		REPEAT COUNT_OF(sRaceBestRanks) iRaceIndex
			saveLabelForVar = saveLabelForArray
			saveLabelForVar += "_BestRank_"
			saveLabelForVar += iRaceIndex			
			REGISTER_INT_TO_SAVE(sRaceBestRanks[iRaceIndex], saveLabelForVar)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC
		
PROC ORR_Register_One_Race_BestTimes_Array_Saved(FLOAT & sRaceBestTimes[NUM_OFFROAD_RACES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(sRaceBestTimes, SIZE_OF(sRaceBestTimes), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForVar
		INT iRaceIndex	
		REPEAT COUNT_OF(sRaceBestTimes) iRaceIndex
			saveLabelForVar = saveLabelForArray
			saveLabelForVar += "_BestTime_"
			saveLabelForVar += iRaceIndex			
			REGISTER_FLOAT_TO_SAVE(sRaceBestTimes[iRaceIndex], saveLabelForVar)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC


/// PURPOSE:
///    Sets up the save structure for registering all the offroad globals that need to be saved
PROC Register_Offroad_Saved_Globals()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sOffroadData, SIZE_OF(g_savedGlobals.sOffroadData), "OFFROAD_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iBitFlags, "OFFROAD_iBitflags")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sOffroadData.eCurrentRace, "OFFROAD_eCurrentRace")
		ORR_Register_One_Race_BestTimes_Array_Saved(g_savedGlobals.sOffroadData.fBestTime, "OFFROAD_TIMES")
		ORR_Register_One_Race_BestRanks_Array_Saved(g_savedGlobals.sOffroadData.iBestRank, "OFFROAD_RANKS")
		
		
		REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRacePlaceEarned, "OFFROAD_iRacePlaceEarned")
		START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sOffroadData.iRaceBestPlaces, SIZE_OF(g_savedGlobals.sOffroadData.iRaceBestPlaces), "OFFROAD_iBestPlaces")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE_CANYON_CLIFFS], "OFFROAD_Race5Best")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE_RIDGE_RUN], "OFFROAD_Race8Best")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE_VALLEY_TRAIL], "OFFROAD_Race9Best")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE_LAKESIDE_SPLASH], "OFFROAD_Race10Best")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE_ECO_FRIENDLY], "OFFROAD_Race11Best")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE_MINEWARD_SPIRAL], "OFFROAD_Race12Best")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sOffroadData.iRaceBestPlaces[OFFROAD_RACE13], "OFFROAD_Race13Best")
		STOP_SAVE_ARRAY()
		
		
	STOP_SAVE_STRUCT()
ENDPROC

