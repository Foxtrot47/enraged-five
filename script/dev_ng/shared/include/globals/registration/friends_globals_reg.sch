USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   friends_globals_reg.sch
//      AUTHOR          :   Alwyn R
//      DESCRIPTION     :   Contains the registrations with code for friends
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

PROC REGISTER_VECTOR_TO_SAVE(VECTOR &VectorToSave, STRING pLabel)
	
	TEXT_LABEL_31 str = pLabel
	str += "x"
	REGISTER_FLOAT_TO_SAVE(VectorToSave.x, str)
	str = pLabel
	str += "y"
	REGISTER_FLOAT_TO_SAVE(VectorToSave.y, str)
	str = pLabel
	str += "z"
	REGISTER_FLOAT_TO_SAVE(VectorToSave.z, str)
	
ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for friends
/// PARAMS:
///    sTimer					This instance of the friends to be saved
///    paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_Timer(structTimer &sTimer, STRING paramNameOfStruct)
	START_SAVE_STRUCT_WITH_SIZE(sTimer, SIZE_OF(sTimer), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(sTimer.TimerBits, "TimerBits")
		REGISTER_FLOAT_TO_SAVE(sTimer.StartTime, "StartTime")
		REGISTER_FLOAT_TO_SAVE(sTimer.PauseTime, "PauseTime")
	STOP_SAVE_STRUCT()
ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for friends
/// PARAMS:
///    friendSheet				This instance of the friends to be saved
///    paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_FriendSheet(structFriendData &friendSheet, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(friendSheet, SIZE_OF(friendSheet), paramNameOfStruct)
		Register_Individual_Struct_CharSheet(friendSheet.charSheet, "charSheet")
//		REGISTER_ENUM_TO_SAVE(friendSheet.defaultPickupLoc, "defaultPickupLoc")
	STOP_SAVE_STRUCT()

ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for friends
/// PARAMS:
///    friendConv				This instance of the friends to be saved
///    paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_FriendChatData(structFriendChatData &friendChatData, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(friendChatData, SIZE_OF(friendChatData), paramNameOfStruct)
		START_SAVE_ARRAY_WITH_SIZE(friendChatData.banks, SIZE_OF(friendChatData.banks), "banks")

			TEXT_LABEL_31 tBankName
			INT i
			REPEAT CONST_iFriendChatMaxBanks i
				tBankName = "bank"
				tBankName += i
				REGISTER_INT_TO_SAVE(friendChatData.banks[i], tBankName)
			ENDREPEAT
			
		STOP_SAVE_ARRAY()
	STOP_SAVE_STRUCT()

ENDPROC



/// PURPOSE:
///    Register all the variables in the struct containing saved variables for friends
/// PARAMS:
///    friendSheet				This instance of the friends to be saved
///    paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_FriendConnection(structFriendConnectData &friendSheet, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(friendSheet, SIZE_OF(friendSheet), paramNameOfStruct)
		REGISTER_ENUM_TO_SAVE(friendSheet.friendA, "friendA")
		REGISTER_ENUM_TO_SAVE(friendSheet.friendB, "friendB")
		
//		REGISTER_ENUM_TO_SAVE(friendSheet.state, "state")
//		REGISTER_ENUM_TO_SAVE(friendSheet.mode, "mode")
		REGISTER_INT_TO_SAVE(friendSheet.blockBits, "blockBits")
		REGISTER_ENUM_TO_SAVE(friendSheet.blockMissionID, "blockMissionID")
		REGISTER_ENUM_TO_SAVE(friendSheet.commID, "commID")
		
		Register_Struct_Timer(friendSheet.lastContactTimer, "lastContactTimer")
		REGISTER_ENUM_TO_SAVE(friendSheet.lastContactType, "lastContactType")
		Register_Struct_FriendChatData(friendSheet.chatData, "chatData")
		
//		REGISTER_INT_TO_SAVE(friendSheet.playerTurnedDown, "playerTurnedDown")
		REGISTER_ENUM_TO_SAVE(friendSheet.wanted, "wanted")
		REGISTER_INT_TO_SAVE(friendSheet.likes, "likes")
		REGISTER_INT_TO_SAVE(friendSheet.flags, "flags")
	STOP_SAVE_STRUCT()

ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for friends
/// PARAMS:
///    friendGroup				This instance of the group to be saved
///    paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_FriendGroup(structFriendGroupData &friendGroup, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(friendGroup, SIZE_OF(friendGroup), paramNameOfStruct)
		Register_Struct_FriendChatData(friendGroup.chatData, "chatData")
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register the array containing the saved variables for each friends
/// INPUT PARAMS
///    paramFriendsArray		The instance of the Array of friends Structs to be saved
///    paramNameOfArray			The name of this instance of the friends Array
PROC Register_Array_Of_FriendSheets(structFriendData &paramFriendsArray[MAX_FRIENDS], STRING paramNameOfArray)

	enumFriend eLoop
	
	START_SAVE_ARRAY_WITH_SIZE(paramFriendsArray, SIZE_OF(paramFriendsArray), paramNameOfArray)

		TEXT_LABEL_31 FriendsName
		
		REPEAT MAX_FRIENDS eLoop
			SWITCH (eLoop)
				CASE FR_MICHAEL
					FriendsName = "FR_MICHAEL"
					BREAK

				CASE FR_FRANKLIN
					FriendsName = "FR_FRANKLIN"
					BREAK

				CASE FR_TREVOR
					FriendsName = "FR_TREVOR"
					BREAK

				CASE FR_LAMAR
					FriendsName = "FR_LAMAR"
					BREAK

				CASE FR_JIMMY
					FriendsName = "FR_JIMMY"
					BREAK

				CASE FR_AMANDA
					FriendsName = "FR_AMANDA"
					BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_FriendSheets - case missing for a friend")
					FriendsName = "Friend Sheet"
					FriendsName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the friendss Array
			Register_Struct_FriendSheet(paramFriendsArray[eLoop], FriendsName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each friends
/// INPUT PARAMS
///    paramFriendsArray		The instance of the Array of friends Structs to be saved
///    paramNameOfArray			The name of this instance of the friends Array
PROC Register_Array_Of_FriendConnections(structFriendConnectData &paramFriendConnectionsArray[MAX_FRIEND_CONNECTIONS], STRING paramNameOfArray)

	enumFriendConnection eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramFriendConnectionsArray, SIZE_OF(paramFriendConnectionsArray), paramNameOfArray)

		TEXT_LABEL_31 FriendsName
		
		REPEAT MAX_FRIEND_CONNECTIONS eLoop
			SWITCH (eLoop)
				CASE FC_MICHAEL_FRANKLIN
					FriendsName = "FC_MICHAEL_FRANKLIN"
					BREAK

				CASE FC_FRANKLIN_TREVOR
					FriendsName = "FC_FRANKLIN_TREVOR"
					BREAK

				CASE FC_TREVOR_MICHAEL
					FriendsName = "FC_TREVOR_MICHAEL"
					BREAK

				CASE FC_FRANKLIN_LAMAR
					FriendsName = "FC_FRANKLIN_LAMAR"
					BREAK

				CASE FC_TREVOR_LAMAR
					FriendsName = "FC_TREVOR_LAMAR"
					BREAK

				CASE FC_MICHAEL_JIMMY
					FriendsName = "FC_MICHAEL_JIMMY"
					BREAK

				CASE FC_FRANKLIN_JIMMY
					FriendsName = "FC_FRANKLIN_JIMMY"
					BREAK

				CASE FC_TREVOR_JIMMY
					FriendsName = "FC_TREVOR_JIMMY"
					BREAK

				CASE FC_MICHAEL_AMANDA
					FriendsName = "FC_MICHAEL_AMANDA"
					BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_FriendConnections - case missing for a friend")
					FriendsName = "Friend Sheet"
					FriendsName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the friendss Array
			Register_Struct_FriendConnection(paramFriendConnectionsArray[eLoop], FriendsName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each friends
/// INPUT PARAMS
///    paramFriendsArray		The instance of the Array of friends Structs to be saved
///    paramNameOfArray			The name of this instance of the friends Array
PROC Register_Array_Of_FriendGroups(structFriendGroupData &paramFriendGroupsArray[MAX_FRIEND_GROUPS], STRING paramNameOfArray)

	enumFriendGroup eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramFriendGroupsArray, SIZE_OF(paramFriendGroupsArray), paramNameOfArray)

		TEXT_LABEL_31 tGroupName
		
		REPEAT MAX_FRIEND_GROUPS eLoop
			SWITCH (eLoop)
				CASE FG_MICHAEL_FRANKLIN_TREVOR
					tGroupName = "FG_MICHAEL_FRANKLIN_TREVOR"
					BREAK

				CASE FG_FRANKLIN_TREVOR_LAMAR
					tGroupName = "FG_FRANKLIN_TREVOR_LAMAR"
					BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_FriendGroups - case missing for a friend group")
					tGroupName = "Friend group"
					tGroupName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the friendss Array
			Register_Struct_FriendGroup(paramFriendGroupsArray[eLoop], tGroupName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each friends
/// INPUT PARAMS
///    paramFriendsArray		The instance of the Array of friends Structs to be saved
///    paramNameOfArray			The name of this instance of the friends Array
PROC Register_Array_Of_Timers(structTimer &paramTimerArray[MAX_FRIENDS], STRING paramNameOfArray)

	enumFriend eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramTimerArray, SIZE_OF(paramTimerArray), paramNameOfArray)

		TEXT_LABEL_31 tTimerName
		
		REPEAT MAX_FRIENDS eLoop
			SWITCH (eLoop)
				CASE FR_MICHAEL
					tTimerName = "tMICHAEL"
					BREAK
					
				CASE FR_FRANKLIN
					tTimerName = "tFRANKLIN"
					BREAK

				CASE FR_TREVOR
					tTimerName = "tTREVOR"
					BREAK

				CASE FR_LAMAR
					tTimerName = "tLAMAR"
					BREAK

				CASE FR_JIMMY
					tTimerName = "tJIMMY"
					BREAK

				CASE FR_AMANDA
					tTimerName = "tAMANDA"
					BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Timers - case missing for a timer name")
					tTimerName = "Friend"
					tTimerName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the friendss Array
			Register_Struct_Timer(paramTimerArray[eLoop], tTimerName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each friends
/// INPUT PARAMS
///    paramFriendsArray		The instance of the Array of friends Structs to be saved
///    paramNameOfArray			The name of this instance of the friends Array
PROC Register_Array_Of_CommIDs(CC_CommID &paramCommIDArray[MAX_FRIENDS], STRING paramNameOfArray)

	enumFriend eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramCommIDArray, SIZE_OF(paramCommIDArray), paramNameOfArray)

		TEXT_LABEL_31 tElementName
		
		REPEAT MAX_FRIENDS eLoop
			SWITCH (eLoop)
				CASE FR_MICHAEL
					tElementName = "tMICHAEL"
					BREAK
					
				CASE FR_FRANKLIN
					tElementName = "tFRANKLIN"
					BREAK

				CASE FR_TREVOR
					tElementName = "tTREVOR"
					BREAK

				CASE FR_LAMAR
					tElementName = "tLAMAR"
					BREAK

				CASE FR_JIMMY
					tElementName = "tJIMMY"
					BREAK

				CASE FR_AMANDA
					tElementName = "tAMANDA"
					BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_CommIDs - missing an element name")
					tElementName = "Friend"
					tElementName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the friendss Array
			REGISTER_ENUM_TO_SAVE(paramCommIDArray[eLoop], tElementName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each friends
/// INPUT PARAMS
///    paramFriendBoolArray		The instance of the Array of friend bools to be saved
///    paramNameOfArray			The name of this instance of the friends Array
PROC Register_Array_Of_FriendBools(BOOL &paramFriendBoolArray[MAX_FRIENDS], STRING paramNameOfArray)

	enumFriend eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramFriendBoolArray, SIZE_OF(paramFriendBoolArray), paramNameOfArray)

		TEXT_LABEL_31 FriendsName
		
		REPEAT MAX_FRIENDS eLoop
			SWITCH (eLoop)
				CASE FR_MICHAEL
					FriendsName = "FR_MICHAEL"
					BREAK

				CASE FR_FRANKLIN
					FriendsName = "FR_FRANKLIN"
					BREAK

				CASE FR_TREVOR
					FriendsName = "FR_TREVOR"
					BREAK

				CASE FR_LAMAR
					FriendsName = "FR_LAMAR"
					BREAK

				CASE FR_JIMMY
					FriendsName = "FR_JIMMY"
					BREAK

				CASE FR_AMANDA
					FriendsName = "FR_AMANDA"
					BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_FriendBools - case missing for a friend")
					FriendsName = "Friend "
					FriendsName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the friendss Array
			REGISTER_BOOL_TO_SAVE(paramFriendBoolArray[eLoop], FriendsName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved friends variables for a character
/// PARAMS:
///    paramFriendsSaved			This instance of the friends data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_FriendsSaved(g_FriendsSavedData &paramFriendsSaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramFriendsSaved, SIZE_OF(paramFriendsSaved), paramNameOfStruct)
		Register_Array_Of_FriendSheets(paramFriendsSaved.g_FriendData,				"g_FriendData")
		Register_Array_Of_FriendConnections(paramFriendsSaved.g_FriendConnectData,	"g_FriendConnectData")
		Register_Array_Of_FriendGroups(paramFriendsSaved.g_FriendGroupData,			"g_FriendGroupData")
		Register_Array_Of_Timers(paramFriendsSaved.g_FriendFailTimers,				"g_FriendFailTimers")
		Register_Array_Of_CommIDs(paramFriendsSaved.g_FriendFailMessages,			"g_FriendFailMessages")
		Register_Struct_FriendChatData(paramFriendsSaved.g_FranklinLamarEndChat,	"g_FranklinLamarEndChat")
		
		REGISTER_INT_TO_SAVE(paramFriendsSaved.g_FriendScriptThread,				"g_FriendScriptThread")
		
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneCanPhoneFriend,			"g_bHelpDoneCanPhoneFriend")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneCanPhoneBBuddy,			"g_bHelpDoneCanPhoneBBuddy")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneCanPhoneDecline,			"g_bHelpDoneCanPhoneDecline")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDonePickupDest,				"g_bHelpDonePickupDest")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDonePickupWait,				"g_bHelpDonePickupWait")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneActivityBlips,			"g_bHelpDoneActivityBlips")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneOpenMap,					"g_bHelpDoneOpenMap")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneDropoff,					"g_bHelpDoneDropoff")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneCanCancel,				"g_bHelpDoneCanCancel")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bCalledToCancelOnce,				"g_bCalledToCancelOnce")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHasPlayerBeenTurnedDown,			"g_bHasPlayerBeenTurnedDown")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bExplainedDeadFriend,				"g_bExplainedDeadFriend")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneBBuddyArrival,			"g_bHelpDoneBBuddyArrival")
		REGISTER_BOOL_TO_SAVE(paramFriendsSaved.g_bHelpDoneBBuddySwitch,			"g_bHelpDoneBBuddySwitch")
		REGISTER_INT_TO_SAVE(paramFriendsSaved.g_iAmbChatBitfield,					"g_iAmbChatBitfield")
	STOP_SAVE_STRUCT()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the friends globals that need to be saved
PROC Register_Friends_Saved_Globals()

	Register_Struct_FriendsSaved(g_savedGlobals.sFriendsData, "FRIENDS_SAVED_ARRAY")

ENDPROC


