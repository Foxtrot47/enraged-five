USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

PROC SPTT_Register_One_Race_BestRanks_Array_Saved(INT & sRaceBestRanks[NUMBER_OF_SPTT_COURSES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(sRaceBestRanks, SIZE_OF(sRaceBestRanks), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForVar
		INT iRaceIndex	
		REPEAT COUNT_OF(sRaceBestRanks) iRaceIndex
			saveLabelForVar = saveLabelForArray
			saveLabelForVar += "_BestRank_"
			saveLabelForVar += iRaceIndex			
			REGISTER_INT_TO_SAVE(sRaceBestRanks[iRaceIndex], saveLabelForVar)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC
		
PROC SPTT_Register_One_Race_BestTimes_Array_Saved(FLOAT & sRaceBestTimes[NUMBER_OF_SPTT_COURSES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(sRaceBestTimes, SIZE_OF(sRaceBestTimes), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForVar
		INT iRaceIndex	
		REPEAT COUNT_OF(sRaceBestTimes) iRaceIndex
			saveLabelForVar = saveLabelForArray
			saveLabelForVar += "_BestTime_"
			saveLabelForVar += iRaceIndex			
			REGISTER_FLOAT_TO_SAVE(sRaceBestTimes[iRaceIndex], saveLabelForVar)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

PROC Register_One_Race_SPTT_Struct_Saved(SPTT_STRUCT & sRaceSpr, STRING szNameOfStruct)	
	
	START_SAVE_STRUCT_WITH_SIZE(sRaceSpr, SIZE_OF(sRaceSpr), szNameOfStruct)
		TEXT_LABEL_63 saveLabelForElement = szNameOfStruct
		saveLabelForElement += "_Name"
		REGISTER_TEXT_LABEL_TO_SAVE(sRaceSpr.name, saveLabelForElement)		
		saveLabelForElement = szNameOfStruct
		saveLabelForElement += "_Status"
		REGISTER_ENUM_TO_SAVE(sRaceSpr.status, saveLabelForElement)		
		saveLabelForElement = szNameOfStruct
		saveLabelForElement += "_Goals"
		REGISTER_INT_TO_SAVE(sRaceSpr.goals, saveLabelForElement)		
		saveLabelForElement = szNameOfStruct
		saveLabelForElement += "_PreReq"
		REGISTER_INT_TO_SAVE(sRaceSpr.preReq, saveLabelForElement)		
		saveLabelForElement = szNameOfStruct
		saveLabelForElement += "_MedalScore"
		REGISTER_INT_TO_SAVE(sRaceSpr.medalscore, saveLabelForElement)
	STOP_SAVE_STRUCT()
ENDPROC

PROC Register_One_Race_SPTT_Struct_Array_Saved(SPTT_STRUCT &structSPTT[NUMBER_OF_SPTT_COURSES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(structSPTT, SIZE_OF(structSPTT), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForStruct
		INT iRaceIndex	
		REPEAT COUNT_OF(structSPTT) iRaceIndex
			saveLabelForStruct = saveLabelForArray
			saveLabelForStruct += "_SPTTSTRUCT_"
			saveLabelForStruct += iRaceIndex
			Register_One_Race_SPTT_Struct_Saved(structSPTT[iRaceIndex], saveLabelForStruct)
		ENDREPEAT
	STOP_SAVE_ARRAY()	
ENDPROC


PROC Register_Struct_SPTT_Saved_Globals(SPTTDataSaved& sSPTTData, STRING paramNameOfStruct)
	START_SAVE_STRUCT_WITH_SIZE(sSPTTData, SIZE_OF(sSPTTData), paramNameOfStruct)
		SPTT_Register_One_Race_BestTimes_Array_Saved(sSPTTData.fBestTime, "SPTT_TIMES")
		SPTT_Register_One_Race_BestRanks_Array_Saved(sSPTTData.iBestRank, "SPTT_RANKS")
		Register_One_Race_SPTT_Struct_Array_Saved(sSPTTData.CourseData, "SPTT_COURSE_DATA")
		REGISTER_INT_TO_SAVE(sSPTTData.iFlags, "SPTT_iGeneralFlags")
	STOP_SAVE_STRUCT()
ENDPROC

PROC Register_SPTT_Saved_Globals()	
   	Register_Struct_SPTT_Saved_Globals(g_savedGlobals.sSPTTData, "SPTT_DATA_SAVED_STRUCT")		
ENDPROC


