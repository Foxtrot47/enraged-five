USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Registers an array of ints to save.
///    Must be of length MAX_VEHICLE_MOD_SLOTS
/// PARAMS:
///    iArray - the array
///    tArrayName - name of the array
PROC Register_Snapshot_Array_Int_For_Vehicle_Mods(INT &iArray[MAX_VEHICLE_MOD_SLOTS], TEXT_LABEL_31 tArrayName)
	TEXT_LABEL_31 tLabel
	INT iCounter
	START_SAVE_ARRAY_WITH_SIZE(iArray, SIZE_OF(iArray), tArrayName)
		REPEAT COUNT_OF(iArray) iCounter
			tLabel = tArrayName
			tLabel += iCounter
			REGISTER_INT_TO_SAVE(iArray[iCounter], tLabel)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Registers an array of ints to save.
///    Must be of length MAX_VEHICLE_MOD_VAR_SLOTS
/// PARAMS:
///    iArray - the array
///    tArrayName - name of the array
PROC Register_Snapshot_Array_Int_For_Vehicle_Mods_Var(INT &iArray[MAX_VEHICLE_MOD_VAR_SLOTS], TEXT_LABEL_31 tArrayName)
	TEXT_LABEL_31 tLabel
	INT iCounter
	START_SAVE_ARRAY_WITH_SIZE(iArray, SIZE_OF(iArray), tArrayName)
		REPEAT COUNT_OF(iArray) iCounter
			tLabel = tArrayName
			tLabel += iCounter
			REGISTER_INT_TO_SAVE(iArray[iCounter], tLabel)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Registers the repeat play / save anywhere snapshot
PROC Register_Snapshot_Saved_Globals()
	TEXT_LABEL_31 tLabel
	TEXT_LABEL_31 tStructName
	TEXT_LABEL_31 tStruct2Name
	
	// player struct
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sRepeatPlayData.mPlayerStruct, SIZE_OF(g_savedGlobals.sRepeatPlayData.mPlayerStruct), "RP_PLAYER")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos.x, "RP_PLAYER_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos.y, "RP_PLAYER_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos.z, "RP_PLAYER_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mPlayerStruct.fHeading, "RP_PLAYER_heading")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mPlayerStruct.iWantedLevel, "RP_PLAYER_Wanted")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.mPlayerStruct.eParachuteState, "RP_PLAYER_Parachute")
	STOP_SAVE_STRUCT()
	
	// vehicle struct
	tStructName = "RP_Vehicle"
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sRepeatPlayData.mVehicleStruct, SIZE_OF(g_savedGlobals.sRepeatPlayData.mVehicleStruct), tStructName)

		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.vVehiclePos.x, "RP_Vehicle_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.vVehiclePos.y, "RP_Vehicle_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.vVehiclePos.z, "RP_Vehicle_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.x, "RP_Velocity_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.y, "RP_Velocity_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.z, "RP_Velocity_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.fVehicleHeading, "RP_Vehicle_heading")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.bPersonalVehicle, "RP_Vehicle_personal")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.bVehGen, "RP_Vehicle_bVehGen")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.bInVehicle, "RP_Vehicle_bInVehicle")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.bTrackedForImpound, "RP_Vehicle_bTrackedForImpound")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.ePlayerCharacter, "RP_Vehicle_character")
	
		tStruct2Name = tStructName
		tStruct2Name += "_Data_"
		START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle, SIZE_OF(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle), tStruct2Name)
		
			tLabel = tStruct2Name
			tLabel += "PlateI"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iPlateIndex, tLabel)

			tLabel = tStruct2Name
			tLabel += "PlateT"
			REGISTER_TEXT_LABEL_15_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.tlPlateText, tLabel)

			tLabel = tStruct2Name
			tLabel += "Colour1"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iColour1, tLabel)

			tLabel = tStruct2Name
			tLabel += "Colour2"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iColour2, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ColourEx1"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iColourExtra1, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ColourEx2"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iColourExtra2, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreR"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreG"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreB"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "WindowTint"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iWindowTintColour, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Flags"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iFlags, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomR"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomG"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomB"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonR"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonG"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonB"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "LockState"
			REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.eLockState, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Model"
			REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.eModel, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Livery"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iLivery, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "WheelType"
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iWheelType, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Roof"
			REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.eRoofState, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ModIndex"
			Register_Snapshot_Array_Int_For_Vehicle_Mods(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iModIndex, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ModVariations"
			Register_Snapshot_Array_Int_For_Vehicle_Mods_Var(g_savedGlobals.sRepeatPlayData.mVehicleStruct.mVehicle.iModVariation, tLabel)
		STOP_SAVE_STRUCT()
	STOP_SAVE_STRUCT()
	
	REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.eMission, "Mission")
	REGISTER_ENUM_TO_SAVE(g_savedGlobals.sRepeatPlayData.eRCMissionToBlock, "eRCMissionToBlock")
ENDPROC

/// PURPOSE:
///    Registers the repeat play / save anywhere snapshot
PROC Register_Snapshot_Saved_Globals_CLF()
	TEXT_LABEL_31 tLabel
	TEXT_LABEL_31 tStructName
	TEXT_LABEL_31 tStruct2Name
	
	// player struct
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct, SIZE_OF(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct), "RP_PLAYER")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.vPos.x, "RP_PLAYER_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.vPos.y, "RP_PLAYER_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.vPos.z, "RP_PLAYER_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.fHeading, "RP_PLAYER_heading")
		REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.iWantedLevel, "RP_PLAYER_Wanted")
		REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.eParachuteState, "RP_PLAYER_Parachute")
	STOP_SAVE_STRUCT()
	
	// vehicle struct
	tStructName = "RP_Vehicle"
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct, SIZE_OF(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct), tStructName)

		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.vVehiclePos.x, "RP_Vehicle_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.vVehiclePos.y, "RP_Vehicle_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.vVehiclePos.z, "RP_Vehicle_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.x, "RP_Velocity_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.y, "RP_Velocity_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.z, "RP_Velocity_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.fVehicleHeading, "RP_Vehicle_heading")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.bPersonalVehicle, "RP_Vehicle_personal")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.bVehGen, "RP_Vehicle_bVehGen")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.bInVehicle, "RP_Vehicle_bInVehicle")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.bTrackedForImpound, "RP_Vehicle_bTrackedForImpound")
		REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.ePlayerCharacter, "RP_Vehicle_character")
	
		tStruct2Name = tStructName
		tStruct2Name += "_Data_"
		START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle, SIZE_OF(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle), tStruct2Name)
		
			tLabel = tStruct2Name
			tLabel += "PlateI"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iPlateIndex, tLabel)

			tLabel = tStruct2Name
			tLabel += "PlateT"
			REGISTER_TEXT_LABEL_15_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.tlPlateText, tLabel)

			tLabel = tStruct2Name
			tLabel += "Colour1"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iColour1, tLabel)

			tLabel = tStruct2Name
			tLabel += "Colour2"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iColour2, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ColourEx1"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iColourExtra1, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ColourEx2"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iColourExtra2, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreR"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreG"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreB"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "WindowTint"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iWindowTintColour, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Flags"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iFlags, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomR"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomG"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomB"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonR"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonG"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonB"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "LockState"
			REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.eLockState, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Model"
			REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.eModel, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Livery"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iLivery, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "WheelType"
			REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iWheelType, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Roof"
			REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.eRoofState, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ModIndex"
			Register_Snapshot_Array_Int_For_Vehicle_Mods(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iModIndex, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ModVariations"
			Register_Snapshot_Array_Int_For_Vehicle_Mods_Var(g_savedGlobalsClifford.sRepeatPlayData.mVehicleStruct.mVehicle.iModVariation, tLabel)
		STOP_SAVE_STRUCT()
	STOP_SAVE_STRUCT()
	
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.eMission, "Mission")
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sRepeatPlayData.eRCMissionToBlock, "eRCMissionToBlock")
ENDPROC
PROC Register_Snapshot_Saved_Globals_NRM()
	TEXT_LABEL_31 tLabel
	TEXT_LABEL_31 tStructName
	TEXT_LABEL_31 tStruct2Name
	
	// player struct
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct, SIZE_OF(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct), "RP_PLAYER")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.vPos.x, "RP_PLAYER_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.vPos.y, "RP_PLAYER_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.vPos.z, "RP_PLAYER_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.fHeading, "RP_PLAYER_heading")
		REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.iWantedLevel, "RP_PLAYER_Wanted")
		REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.eParachuteState, "RP_PLAYER_Parachute")
	STOP_SAVE_STRUCT()
	
	// vehicle struct
	tStructName = "RP_Vehicle"
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct, SIZE_OF(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct), tStructName)

		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.vVehiclePos.x, "RP_Vehicle_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.vVehiclePos.y, "RP_Vehicle_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.vVehiclePos.z, "RP_Vehicle_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.x, "RP_Velocity_x")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.y, "RP_Velocity_y")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.vVehicleVelocity.z, "RP_Velocity_z")
		REGISTER_FLOAT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.fVehicleHeading, "RP_Vehicle_heading")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.bPersonalVehicle, "RP_Vehicle_personal")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.bVehGen, "RP_Vehicle_bVehGen")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.bInVehicle, "RP_Vehicle_bInVehicle")
		REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.bTrackedForImpound, "RP_Vehicle_bTrackedForImpound")
		REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.ePlayerCharacter, "RP_Vehicle_character")
	
		tStruct2Name = tStructName
		tStruct2Name += "_Data_"
		START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle, SIZE_OF(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle), tStruct2Name)
		
			tLabel = tStruct2Name
			tLabel += "PlateI"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iPlateIndex, tLabel)

			tLabel = tStruct2Name
			tLabel += "PlateT"
			REGISTER_TEXT_LABEL_15_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.tlPlateText, tLabel)

			tLabel = tStruct2Name
			tLabel += "Colour1"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iColour1, tLabel)

			tLabel = tStruct2Name
			tLabel += "Colour2"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iColour2, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ColourEx1"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iColourExtra1, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ColourEx2"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iColourExtra2, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreR"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreG"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "TyreB"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iTyreB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "WindowTint"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iWindowTintColour, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Flags"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iFlags, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomR"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomG"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "CustomB"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iCustomB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonR"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonR, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonG"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonG, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "NeonB"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iNeonB, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "LockState"
			REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.eLockState, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Model"
			REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.eModel, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Livery"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iLivery, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "WheelType"
			REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iWheelType, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "Roof"
			REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.eRoofState, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ModIndex"
			Register_Snapshot_Array_Int_For_Vehicle_Mods(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iModIndex, tLabel)
			
			tLabel = tStruct2Name
			tLabel += "ModVariations"
			Register_Snapshot_Array_Int_For_Vehicle_Mods_Var(g_savedGlobalsnorman.sRepeatPlayData.mVehicleStruct.mVehicle.iModVariation, tLabel)
		STOP_SAVE_STRUCT()
	STOP_SAVE_STRUCT()
	
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.eMission, "Mission")
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sRepeatPlayData.eRCMissionToBlock, "eRCMissionToBlock")
ENDPROC
