USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   player_scene_globals_reg.sch
//      AUTHOR          :   Alwyn R
//      DESCRIPTION     :   Contains the registrations with code for Player Switch Scene
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


/// PURPOSE:
///    Register the array containing the saved variables for each Player_Scene
/// INPUT PARAMS
///    paramPlayer_SceneArray		The instance of the Array of Player_Scene Structs to be saved
///    paramNameOfArray			The name of this instance of the Player_Scene Array
PROC Register_Array_Of_Player_Ped_Request_Scene_Enum_CLF(PED_REQUEST_SCENE_ENUM &paramPlayerPedRequestSceneArray[NUM_OF_PLAYABLE_PEDS], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerPedRequestSceneArray, SIZE_OF(paramPlayerPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL
					PlayerSceneName = "CHAR_MICHAEL"
					BREAK

				CASE CHAR_FRANKLIN
					PlayerSceneName = "CHAR_FRANKLIN"
					BREAK

				CASE CHAR_TREVOR
					PlayerSceneName = "CHAR_TREVOR"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Player_Ped_Request_Scene_Enum - case missing for a player scene")
					PlayerSceneName = "Player_Scene_Sheet_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			REGISTER_ENUM_TO_SAVE(paramPlayerPedRequestSceneArray[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
PROC Register_Array_Of_Player_Ped_Request_Scene_Enum_NRM(PED_REQUEST_SCENE_ENUM &paramPlayerPedRequestSceneArray[NUM_OF_PLAYABLE_PEDS], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerPedRequestSceneArray, SIZE_OF(paramPlayerPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL 	 PlayerSceneName = "CHAR_MICHAEL"		BREAK
				CASE CHAR_NRM_JIMMY	 PlayerSceneName = "CHAR_NRM_JIMMY"		BREAK
				CASE CHAR_NRM_TRACEY PlayerSceneName = "CHAR_NRM_TRACEY"	BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Player_Ped_Request_Scene_Enum NRM - case missing for a player scene")
					PlayerSceneName = "Player_Scene_Sheet_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			REGISTER_ENUM_TO_SAVE(paramPlayerPedRequestSceneArray[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
PROC Register_Array_Of_Player_Ped_Request_Scene_Enum(PED_REQUEST_SCENE_ENUM &paramPlayerPedRequestSceneArray[NUM_OF_PLAYABLE_PEDS], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerPedRequestSceneArray, SIZE_OF(paramPlayerPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL
					PlayerSceneName = "CHAR_MICHAEL"
					BREAK

				CASE CHAR_FRANKLIN
					PlayerSceneName = "CHAR_FRANKLIN"
					BREAK

				CASE CHAR_TREVOR
					PlayerSceneName = "CHAR_TREVOR"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Player_Ped_Request_Scene_Enum - case missing for a player scene")
					PlayerSceneName = "Player_Scene_Sheet_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			REGISTER_ENUM_TO_SAVE(paramPlayerPedRequestSceneArray[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each Player_Scene
/// INPUT PARAMS
///    paramPlayer_SceneArray		The instance of the Array of Player_Scene Structs to be saved
///    paramNameOfArray			The name of this instance of the Player_Scene Array
PROC Register_Array_Of_Ped_Request_Scene_Enum(PED_REQUEST_SCENE_ENUM &paramPedRequestSceneArray[5], STRING paramNameOfArray)
	
	INT iLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPedRequestSceneArray, SIZE_OF(paramPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT COUNT_OF(paramPedRequestSceneArray) iLoop
			PlayerSceneName = "_"
			PlayerSceneName += iLoop
			
			// Register details for one element of the Player_Scenes Array
			REGISTER_ENUM_TO_SAVE(paramPedRequestSceneArray[iLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each Player_Scene
/// INPUT PARAMS
///    paramPlayerTriggeredPrioritySwitch		The instance of the Array of Player_Scene Structs to be saved
///    paramNameOfArray			The name of this instance of the Player_Scene Array
PROC Register_2D_Array_Of_Ped_Request_Scene_Enum_CLF(PED_REQUEST_SCENE_ENUM &paramPedRequestSceneArray[NUM_OF_PLAYABLE_PEDS][5], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPedRequestSceneArray, SIZE_OF(paramPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL
					PlayerSceneName = "CHAR_MICHAEL"
					BREAK

				CASE CHAR_FRANKLIN
					PlayerSceneName = "CHAR_FRANKLIN"
					BREAK

				CASE CHAR_TREVOR
					PlayerSceneName = "CHAR_TREVOR"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_2D_Array_Of_Ped_Request_Scene_Enum - case missing for a player scene")
					PlayerSceneName = "char_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			Register_Array_Of_Ped_Request_Scene_Enum(paramPedRequestSceneArray[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
PROC Register_2D_Array_Of_Ped_Request_Scene_Enum_NRM(PED_REQUEST_SCENE_ENUM &paramPedRequestSceneArray[NUM_OF_PLAYABLE_PEDS][5], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPedRequestSceneArray, SIZE_OF(paramPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL	 PlayerSceneName = "CHAR_MICHAEL"	 BREAK
				CASE CHAR_NRM_JIMMY	 PlayerSceneName = "CHAR_NRM_JIMMY"	 BREAK
				CASE CHAR_NRM_TRACEY PlayerSceneName = "CHAR_NRM_TRACEY" BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Register_2D_Array_Of_Ped_Request_Scene_Enum - case missing for a player scene")
					PlayerSceneName = "char_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			Register_Array_Of_Ped_Request_Scene_Enum(paramPedRequestSceneArray[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
PROC Register_2D_Array_Of_Ped_Request_Scene_Enum(PED_REQUEST_SCENE_ENUM &paramPedRequestSceneArray[NUM_OF_PLAYABLE_PEDS][5], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPedRequestSceneArray, SIZE_OF(paramPedRequestSceneArray), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL
					PlayerSceneName = "CHAR_MICHAEL"
					BREAK

				CASE CHAR_FRANKLIN
					PlayerSceneName = "CHAR_FRANKLIN"
					BREAK

				CASE CHAR_TREVOR
					PlayerSceneName = "CHAR_TREVOR"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_2D_Array_Of_Ped_Request_Scene_Enum - case missing for a player scene")
					PlayerSceneName = "char_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			Register_Array_Of_Ped_Request_Scene_Enum(paramPedRequestSceneArray[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each Player_Scene
/// INPUT PARAMS
///    paramPlayer_SceneArray		The instance of the Array of Player_Scene Structs to be saved
///    paramNameOfArray			The name of this instance of the Player_Scene Array
PROC Register_Array_Of_Priotity_Scene_Checks(BOOL &paramPlayerTriggeredPrioritySwitch[4], STRING paramNameOfArray)

	INT iLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerTriggeredPrioritySwitch, SIZE_OF(paramPlayerTriggeredPrioritySwitch), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT COUNT_OF(paramPlayerTriggeredPrioritySwitch) iLoop
			SWITCH (iLoop)
				CASE 0
					PlayerSceneName = "_0"
					BREAK

				CASE 1
					PlayerSceneName = "_1"
					BREAK

				CASE 2
					PlayerSceneName = "_2"
					BREAK

				CASE 3
					PlayerSceneName = "_3"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Priotity_Scene_Checks - case missing for a player scene")
					PlayerSceneName += iLoop
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			REGISTER_BOOL_TO_SAVE(paramPlayerTriggeredPrioritySwitch[iLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each Player_Scene
/// INPUT PARAMS
///    paramPlayerTriggeredPrioritySwitch		The instance of the Array of Player_Scene Structs to be saved
///    paramNameOfArray			The name of this instance of the Player_Scene Array
PROC Register_2D_Array_Of_Priotity_Scene_Checks_CLF(BOOL &paramPlayerTriggeredPrioritySwitch[NUM_OF_PLAYABLE_PEDS][4], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerTriggeredPrioritySwitch, SIZE_OF(paramPlayerTriggeredPrioritySwitch), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL
					PlayerSceneName = "CHAR_MICHAEL"
					BREAK

				CASE CHAR_FRANKLIN
					PlayerSceneName = "CHAR_FRANKLIN"
					BREAK

				CASE CHAR_TREVOR
					PlayerSceneName = "CHAR_TREVOR"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_2D_Array_Of_Priotity_Scene_Checks - case missing for a player scene")
					PlayerSceneName = "char_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			Register_Array_Of_Priotity_Scene_Checks(paramPlayerTriggeredPrioritySwitch[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
PROC Register_2D_Array_Of_Priotity_Scene_Checks_NRM(BOOL &paramPlayerTriggeredPrioritySwitch[NUM_OF_PLAYABLE_PEDS][4], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerTriggeredPrioritySwitch, SIZE_OF(paramPlayerTriggeredPrioritySwitch), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL		PlayerSceneName = "CHAR_MICHAEL"		BREAK
				CASE CHAR_NRM_JIMMY		PlayerSceneName = "CHAR_NRM_JIMMY"		BREAK
				CASE CHAR_NRM_TRACEY	PlayerSceneName = "CHAR_NRM_TRACEY"		BREAK

				DEFAULT
					SCRIPT_ASSERT("Register_2D_Array_Of_Priotity_Scene_Checks - case missing for a player scene")
					PlayerSceneName = "char_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			Register_Array_Of_Priotity_Scene_Checks(paramPlayerTriggeredPrioritySwitch[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC
PROC Register_2D_Array_Of_Priotity_Scene_Checks(BOOL &paramPlayerTriggeredPrioritySwitch[NUM_OF_PLAYABLE_PEDS][4], STRING paramNameOfArray)

	enumCharacterList eLoop
	START_SAVE_ARRAY_WITH_SIZE(paramPlayerTriggeredPrioritySwitch, SIZE_OF(paramPlayerTriggeredPrioritySwitch), paramNameOfArray)

		TEXT_LABEL_31 PlayerSceneName
		
		REPEAT NUM_OF_PLAYABLE_PEDS eLoop
			SWITCH (eLoop)
				CASE CHAR_MICHAEL
					PlayerSceneName = "CHAR_MICHAEL"
					BREAK

				CASE CHAR_FRANKLIN
					PlayerSceneName = "CHAR_FRANKLIN"
					BREAK

				CASE CHAR_TREVOR
					PlayerSceneName = "CHAR_TREVOR"
					BREAK


				DEFAULT
					SCRIPT_ASSERT("Register_2D_Array_Of_Priotity_Scene_Checks - case missing for a player scene")
					PlayerSceneName = "char_"
					PlayerSceneName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Player_Scenes Array
			Register_Array_Of_Priotity_Scene_Checks(paramPlayerTriggeredPrioritySwitch[eLoop], PlayerSceneName)
			
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved Player_Scene variables for a character
/// PARAMS:
///    paramPlayerSceneSaved			This instance of the Player_Scene data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_Player_Scene_Saved_CLF(g_PlayerSceneSavedData &paramPlayerSceneSaved, STRING paramNameOfStruct)
	
	START_SAVE_STRUCT_WITH_SIZE(paramPlayerSceneSaved, SIZE_OF(paramPlayerSceneSaved), paramNameOfStruct)
		Register_Array_Of_Player_Ped_Request_Scene_Enum_CLF(paramPlayerSceneSaved.g_ePlayerLastScene, "g_ePlayerLastScene")
		Register_2D_Array_Of_Ped_Request_Scene_Enum_CLF(paramPlayerSceneSaved.g_eLastSceneQueue, "g_eLastSceneQueue")
		Register_2D_Array_Of_Priotity_Scene_Checks_CLF(paramPlayerSceneSaved.g_bPlayerTriggeredPrioritySwitch, "g_bPlayerTriggeredPrioritySwitch")
		REGISTER_INT_TO_SAVE(paramPlayerSceneSaved.g_iSeenOneOffSceneBit, "g_iSeenOneOffSceneBit")
		REGISTER_BOOL_TO_SAVE(paramPlayerSceneSaved.g_bSeenTrevorsPrettyDress, "g_bSeenTrevorsPrettyDress")
	STOP_SAVE_STRUCT()

ENDPROC
PROC Register_Struct_Player_Scene_Saved_NRM(g_PlayerSceneSavedData &paramPlayerSceneSaved, STRING paramNameOfStruct)
	
	START_SAVE_STRUCT_WITH_SIZE(paramPlayerSceneSaved, SIZE_OF(paramPlayerSceneSaved), paramNameOfStruct)
		Register_Array_Of_Player_Ped_Request_Scene_Enum_NRM(paramPlayerSceneSaved.g_ePlayerLastScene, "g_ePlayerLastScene")
		Register_2D_Array_Of_Ped_Request_Scene_Enum_NRM(paramPlayerSceneSaved.g_eLastSceneQueue, "g_eLastSceneQueue")
		Register_2D_Array_Of_Priotity_Scene_Checks_NRM(paramPlayerSceneSaved.g_bPlayerTriggeredPrioritySwitch, "g_bPlayerTriggeredPrioritySwitch")
		REGISTER_INT_TO_SAVE(paramPlayerSceneSaved.g_iSeenOneOffSceneBit, "g_iSeenOneOffSceneBit")
		REGISTER_BOOL_TO_SAVE(paramPlayerSceneSaved.g_bSeenTrevorsPrettyDress, "g_bSeenTrevorsPrettyDress")
	STOP_SAVE_STRUCT()

ENDPROC
PROC Register_Struct_Player_Scene_Saved(g_PlayerSceneSavedData &paramPlayerSceneSaved, STRING paramNameOfStruct)
	
	START_SAVE_STRUCT_WITH_SIZE(paramPlayerSceneSaved, SIZE_OF(paramPlayerSceneSaved), paramNameOfStruct)
		Register_Array_Of_Player_Ped_Request_Scene_Enum(paramPlayerSceneSaved.g_ePlayerLastScene, "g_ePlayerLastScene")
		Register_2D_Array_Of_Ped_Request_Scene_Enum(paramPlayerSceneSaved.g_eLastSceneQueue, "g_eLastSceneQueue")
		Register_2D_Array_Of_Priotity_Scene_Checks(paramPlayerSceneSaved.g_bPlayerTriggeredPrioritySwitch, "g_bPlayerTriggeredPrioritySwitch")
		REGISTER_INT_TO_SAVE(paramPlayerSceneSaved.g_iSeenOneOffSceneBit, "g_iSeenOneOffSceneBit")
		REGISTER_BOOL_TO_SAVE(paramPlayerSceneSaved.g_bSeenTrevorsPrettyDress, "g_bSeenTrevorsPrettyDress")
	STOP_SAVE_STRUCT()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the Player_Scene globals that need to be saved
PROC Register_Player_Scene_Saved_Globals_CLF()

	Register_Struct_Player_Scene_Saved_CLF(g_savedGlobalsClifford.sPlayerSceneData, "PLAYER_SCENE_SAVED_DATA")

ENDPROC
PROC Register_Player_Scene_Saved_Globals_NRM()

	Register_Struct_Player_Scene_Saved_NRM(g_savedGlobalsnorman.sPlayerSceneData, "PLAYER_SCENE_SAVED_DATA")

ENDPROC
PROC Register_Player_Scene_Saved_Globals()

	Register_Struct_Player_Scene_Saved(g_savedGlobals.sPlayerSceneData, "PLAYER_SCENE_SAVED_DATA")

ENDPROC


