USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   mp_saved_bounty_reg.sch
//      AUTHOR          :   Conor McGuire
//      DESCRIPTION     :   Contains the registrations with code for all MP bounty 
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Sets up the save structure for registering all the General globals that need to be saved
PROC REGISTER_MP_BOUNTY_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
	TEXT_LABEL_23 tlSlot = 	"MP_BOUNTY_SAVED"
	tlSlot += iIndex
	TEXT_LABEL_31 arrayName
	INT i
	START_SAVE_STRUCT_WITH_SIZE(globalData.MpSavedBounty, SIZE_OF(globalData.MpSavedBounty), tlSlot)
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data1, "BOUNTY_GAMERH64_1")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data2, "BOUNTY_GAMERH64_2")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data3, "BOUNTY_GAMERH64_3")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data4, "BOUNTY_GAMERH64_4")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data5, "BOUNTY_GAMERH64_5")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data6, "BOUNTY_GAMERH64_6")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data7, "BOUNTY_GAMERH64_7")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data8, "BOUNTY_GAMERH64_8")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data9, "BOUNTY_GAMERH64_9")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data10, "BOUNTY_GAMERH64_10")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data11, "BOUNTY_GAMERH64_11")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data12, "BOUNTY_GAMERH64_12")
		REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountyPlacer.Data13, "BOUNTY_GAMERH64_13")
		REGISTER_INT_TO_SAVE(globalData.MpSavedBounty.iTimePassedSoFar, "BOUNTY_TIME")
		REGISTER_INT_TO_SAVE(globalData.MpSavedBounty.iBountyValue, "BOUNTY_VALUE")
		tlSlot += "_GH"
		START_SAVE_ARRAY_WITH_SIZE(globalData.MpSavedBounty.gh_BountySet, SIZE_OF(globalData.MpSavedBounty.gh_BountySet), tlSlot)
			REPEAT MAX_BOUNTIES_PER_DAY i
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_1"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data1, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_2"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data2, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_3"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data3, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_4"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data4, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_5"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data5, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_6"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data6, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_7"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data7, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_8"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data8, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_9"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data9, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_10"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data10, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_11"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data11, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_12"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data12, arrayName)
				arrayName = tlSlot
				arrayName += "64_"
				arrayName += i
				arrayName += "_13"
				REGISTER_INT64_TO_SAVE(globalData.MpSavedBounty.gh_BountySet[i].Data13, arrayName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		tlSlot = "MP_BOUNTY_SAVED"
		tlSlot += iIndex
		tlSlot += "_TIM"
		START_SAVE_ARRAY_WITH_SIZE(globalData.MpSavedBounty.iPosXTimeSet, SIZE_OF(globalData.MpSavedBounty.iPosXTimeSet), tlSlot)
			REPEAT MAX_BOUNTIES_PER_DAY i
				arrayName = tlSlot
				arrayName += i
				REGISTER_INT_TO_SAVE(globalData.MpSavedBounty.iPosXTimeSet[i], arrayName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
	STOP_SAVE_STRUCT()
ENDPROC


PROC REGISTER_MP_ATM_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
	TEXT_LABEL_23 tlSlot = 	"MP_ATM_SAVED"
	tlSlot += iIndex
	START_SAVE_STRUCT_WITH_SIZE(
	globalData.MpSavedATM, 
	SIZE_OF(globalData.MpSavedATM), 
	tlSlot)
	
	
		/*
			//log data
			INT iLogCaret
			INT LogValues[MP_TOTAL_ATM_LOG_ENTRIES]
			TEXT_LABEL_23 LogSourceNames[MP_TOTAL_ATM_LOG_ENTRIES]
			MP_ATM_LOG_ACTION_TYPE LogActionType[MP_TOTAL_ATM_LOG_ENTRIES]
		*/
			
		REGISTER_INT_TO_SAVE(globalData.MpSavedATM.iLogCaret, "MPATM_CARET_")
		
		INT i = 0
		TEXT_LABEL base = "MPATMLOGVAL"
		START_SAVE_ARRAY_WITH_SIZE(globalData.MpSavedATM.LogValues, SIZE_OF(globalData.MpSavedATM.LogValues), base)
		REPEAT MP_TOTAL_ATM_LOG_ENTRIES i
			TEXT_LABEL t = base
			t += i
			REGISTER_INT_TO_SAVE(globalData.MpSavedATM.LogValues[i], t)
		ENDREPEAT
		STOP_SAVE_ARRAY()

		base = "MPATMLOGSCRS"
		START_SAVE_ARRAY_WITH_SIZE(globalData.MpSavedATM.LogSourceNames, SIZE_OF(globalData.MpSavedATM.LogSourceNames), base)
		REPEAT MP_TOTAL_ATM_LOG_ENTRIES i
			TEXT_LABEL t = base
			t += i
			REGISTER_TEXT_LABEL_23_TO_SAVE(globalData.MpSavedATM.LogSourceNames[i], t)
		ENDREPEAT
		STOP_SAVE_ARRAY()

		base = "MPATMLOGACT"
		START_SAVE_ARRAY_WITH_SIZE(globalData.MpSavedATM.LogActionType, SIZE_OF(globalData.MpSavedATM.LogActionType), base)
		REPEAT MP_TOTAL_ATM_LOG_ENTRIES i
			TEXT_LABEL t = base
			t += i
			REGISTER_ENUM_TO_SAVE(globalData.MpSavedATM.LogActionType[i], t)
		ENDREPEAT
		STOP_SAVE_ARRAY()
	
	
		base = "MPATMLOGDAT"
		START_SAVE_ARRAY_WITH_SIZE(globalData.MpSavedATM.LogData, SIZE_OF(globalData.MpSavedATM.LogData), base)
		REPEAT MP_TOTAL_ATM_LOG_ENTRIES i
			TEXT_LABEL t = base
			t += i
			REGISTER_INT_TO_SAVE(globalData.MpSavedATM.LogData[i], t)
		ENDREPEAT
		STOP_SAVE_ARRAY()
	
	
	
		REGISTER_BOOL_TO_SAVE(globalData.MpSavedATM.bAnyMpVehicleBought,"mpAnyVecBought")
	
	STOP_SAVE_STRUCT()
ENDPROC


