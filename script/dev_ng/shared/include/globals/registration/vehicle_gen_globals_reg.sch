USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   vehicle_gen_globals_reg.sch
//      AUTHOR          :   Kenneth Ross
//      DESCRIPTION     :   Contains the registrations with code for the vehicle generation 
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Save the Properties array
/// INPUT PARAMS:
///    paramIntIDsArray			The instance of the Array of Properties to be saved
///    paramNameOfArray			The name of this instance of the Properties Array
PROC Register_Array_Of_VehicleGenProperties(INT &paramPropertiesArray[NUMBER_OF_VEHICLES_TO_GEN], STRING paramNameOfArray)

	INT tempLoop = 0

	START_SAVE_ARRAY_WITH_SIZE(paramPropertiesArray, SIZE_OF(paramPropertiesArray), paramNameOfArray)
		TEXT_LABEL_31 PropertiesName
		
		REPEAT NUMBER_OF_VEHICLES_TO_GEN tempLoop
			PropertiesName = "VEHGEN_"
			PropertiesName += tempLoop
			REGISTER_INT_TO_SAVE(paramPropertiesArray[tempLoop], PropertiesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Save the Dyanmic Veh Gen Data array
/// INPUT PARAMS:
///    paramDynamicDataArray	The instance of the Array of VEHICLE_SETUP_STRUCT to be saved
///    paramNameOfArray			The name of this instance of the VEHICLE_SETUP_STRUCT Array
PROC Register_Contents_Of_DynamicVehGenData(VEHICLE_SETUP_STRUCT &paramDynamicDataArray, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramDynamicDataArray, SIZE_OF(paramDynamicDataArray), paramNameOfStruct)	

		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iPlateIndex, "iPlateIndex")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramDynamicDataArray.tlPlateText, "tlPlateText")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColour1, "iColour1")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColour2, "iColour2")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColourExtra1, "iColourExtra1")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iColourExtra2, "iColourExtra2")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreR, "iTyreR")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreG, "iTyreG")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iTyreB, "iTyreB")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iWindowTintColour, "iWindowTintColour")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iLivery, "iLivery")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iWheelType, "iWheelType")
		REGISTER_ENUM_TO_SAVE(paramDynamicDataArray.eRoofState, "eRoofState")
		
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iFlags, "iFlags")
		REGISTER_ENUM_TO_SAVE(paramDynamicDataArray.eLockState, "eLockState")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iCustomR, "iCustomR")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iCustomG, "iCustomG")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iCustomB, "iCustomB")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iNeonR, "iNeonR")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iNeonG, "iNeonG")
		REGISTER_INT_TO_SAVE(paramDynamicDataArray.iNeonB, "iNeonB")
		REGISTER_ENUM_TO_SAVE(paramDynamicDataArray.eModel, "eModel")
		
		INT tempLoop
		TEXT_LABEL_15 tempLabel
		START_SAVE_ARRAY_WITH_SIZE(paramDynamicDataArray.iModIndex, SIZE_OF(paramDynamicDataArray.iModIndex), "VEH_MOD_ID")
			REPEAT MAX_VEHICLE_MOD_SLOTS tempLoop
				tempLabel = "MOD_ID" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(paramDynamicDataArray.iModIndex[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramDynamicDataArray.iModVariation, SIZE_OF(paramDynamicDataArray.iModVariation), "VEH_MOD_VAR")
			REPEAT MAX_VEHICLE_MOD_VAR_SLOTS tempLoop
				tempLabel = "MOD_VAR" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(paramDynamicDataArray.iModVariation[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		

	STOP_SAVE_STRUCT()
	
ENDPROC

/// PURPOSE:
///    Register the struct containing the vehicle gen properties INT
/// INPUT PARAMS:
///    paramVehicleGenSavedVars		This instance of the Vehicle Gen Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_VehicleGenData(VehicleGenDataSaved &paramVehicleGenSavedVars, STRING paramNameOfStruct)

	INT iTempLoop
	
	TEXT_LABEL_23 tlLabel
	
	START_SAVE_STRUCT_WITH_SIZE(paramVehicleGenSavedVars, SIZE_OF(paramVehicleGenSavedVars), paramNameOfStruct)
		Register_Array_Of_VehicleGenProperties(paramVehicleGenSavedVars.iProperties, "VEHICLE_GEN_PROPERTIES")
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sDynamicData, SIZE_OF(paramVehicleGenSavedVars.sDynamicData), "DYANMIC_DATA")
			REPEAT NUMBER_OF_DYNAMIC_VEHICLE_GENS iTempLoop
				tlLabel = "DYANMIC_DATA"
				tlLabel += iTempLoop
				Register_Contents_Of_DynamicVehGenData(paramVehicleGenSavedVars.sDynamicData[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sHeistPrepVehicles, SIZE_OF(paramVehicleGenSavedVars.sHeistPrepVehicles), "HEIST_VEHS")
			REPEAT COUNT_OF(paramVehicleGenSavedVars.sHeistPrepVehicles) iTempLoop
				tlLabel = "HEIST_VEHS"
				tlLabel += iTempLoop
				Register_Contents_Of_DynamicVehGenData(paramVehicleGenSavedVars.sHeistPrepVehicles[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sImpoundVehicles, SIZE_OF(paramVehicleGenSavedVars.sImpoundVehicles), "IMPOUND_VEHS")
			INT iCharLoop
			REPEAT NUM_OF_PLAYABLE_PEDS iCharLoop
				tlLabel = "IMPOUND_VEHS"
				tlLabel += "_"
				tlLabel += iCharLoop
				START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sImpoundVehicles[iCharLoop], SIZE_OF(paramVehicleGenSavedVars.sImpoundVehicles[iCharLoop]), tlLabel)
					REPEAT get_vehicle_setup_array_size(paramVehicleGenSavedVars.sImpoundVehicles[iCharLoop]) iTempLoop
						tlLabel = "IMPOUND_VEHS"
						tlLabel += "_"
						tlLabel += iCharLoop
						tlLabel += "_"
						tlLabel += iTempLoop
						Register_Contents_Of_DynamicVehGenData(paramVehicleGenSavedVars.sImpoundVehicles[iCharLoop][iTempLoop], tlLabel)
					ENDREPEAT
				STOP_SAVE_ARRAY()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		Register_Contents_Of_DynamicVehGenData(paramVehicleGenSavedVars.sCarForImpound, "NEXT_IMPOUND")
		
		REGISTER_BOOL_TO_SAVE(paramVehicleGenSavedVars.bTrackingImpoundVehicle, "IMPOUND_TRACK")
		REGISTER_BOOL_TO_SAVE(paramVehicleGenSavedVars.bImpoundVehicleClearedBySwitch, "IMPOUND_SWITCH")
		
		REGISTER_ENUM_TO_SAVE(paramVehicleGenSavedVars.eCurrentTrackedChar, "IMPOUND_CHAR")
		REGISTER_ENUM_TO_SAVE(paramVehicleGenSavedVars.eLastCharToUseMissionVehGen, "VEHGEN_CHAR")
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.iImpoundVehicleSlot, SIZE_OF(paramVehicleGenSavedVars.iImpoundVehicleSlot), "IMPOUND_SLOTS")
			REPEAT COUNT_OF(paramVehicleGenSavedVars.iImpoundVehicleSlot) iTempLoop
				tlLabel = "IMPOUND_SLOTS"
				tlLabel += iTempLoop
				REGISTER_INT_TO_SAVE(paramVehicleGenSavedVars.iImpoundVehicleSlot[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.bImpoundVehicleHelp, SIZE_OF(paramVehicleGenSavedVars.bImpoundVehicleHelp), "IMPOUND_HELP")
			REPEAT COUNT_OF(paramVehicleGenSavedVars.bImpoundVehicleHelp) iTempLoop
				tlLabel = "IMPOUND_HELP"
				tlLabel += iTempLoop
				REGISTER_BOOL_TO_SAVE(paramVehicleGenSavedVars.bImpoundVehicleHelp[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.vDynamicCoords, SIZE_OF(paramVehicleGenSavedVars.vDynamicCoords), "DYNAMIC_COORDS")
			REPEAT NUMBER_OF_DYNAMIC_VEHICLE_GENS iTempLoop
				tlLabel = "DYNAMIC_COORDS"
				tlLabel += iTempLoop
				tlLabel += "_X"
				REGISTER_FLOAT_TO_SAVE(paramVehicleGenSavedVars.vDynamicCoords[iTempLoop].x, tlLabel)
				
				tlLabel = "DYNAMIC_COORDS"
				tlLabel += iTempLoop
				tlLabel += "_Y"
				REGISTER_FLOAT_TO_SAVE(paramVehicleGenSavedVars.vDynamicCoords[iTempLoop].y, tlLabel)
				
				tlLabel = "DYNAMIC_COORDS"
				tlLabel += iTempLoop
				tlLabel += "_Z"
				REGISTER_FLOAT_TO_SAVE(paramVehicleGenSavedVars.vDynamicCoords[iTempLoop].z, tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.fDynamicHeading, SIZE_OF(paramVehicleGenSavedVars.fDynamicHeading), "DYANMIC_HEAD")
			REPEAT NUMBER_OF_DYNAMIC_VEHICLE_GENS iTempLoop
				tlLabel = "DYNAMIC_HEAD"
				tlLabel += iTempLoop
				REGISTER_FLOAT_TO_SAVE(paramVehicleGenSavedVars.fDynamicHeading[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.iPlayerVehicle, SIZE_OF(paramVehicleGenSavedVars.iPlayerVehicle), "PLAYER_VEH")
			REPEAT NUMBER_OF_DYNAMIC_VEHICLE_GENS iTempLoop
				tlLabel = "PLAYER_VEH"
				tlLabel += iTempLoop
				REGISTER_INT_TO_SAVE(paramVehicleGenSavedVars.iPlayerVehicle[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sWebVehicles, SIZE_OF(paramVehicleGenSavedVars.sWebVehicles), "WEB_VEHICLES")
			INT iPed
			REPEAT COUNT_OF(paramVehicleGenSavedVars.sWebVehicles) iPed
				
				tlLabel = "WEB_VEH_"
				tlLabel += iPed
				
				START_SAVE_STRUCT_WITH_SIZE(paramVehicleGenSavedVars.sWebVehicles[iPed], SIZE_OF(paramVehicleGenSavedVars.sWebVehicles[iPed]), tlLabel)
					START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sWebVehicles[iPed].iSiteID, SIZE_OF(paramVehicleGenSavedVars.sWebVehicles[iPed].iSiteID), "SITE_ID")
						REPEAT NUMBER_OF_BUYABLE_VEHICLES_SP iTempLoop
							tlLabel = "SITE_ID_"
							tlLabel += iTempLoop
							REGISTER_INT_TO_SAVE(paramVehicleGenSavedVars.sWebVehicles[iPed].iSiteID[iTempLoop], tlLabel)
						ENDREPEAT
					STOP_SAVE_ARRAY()
					
					START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sWebVehicles[iPed].eVehGen, SIZE_OF(paramVehicleGenSavedVars.sWebVehicles[iPed].eVehGen), "VEH_GEN")
						REPEAT NUMBER_OF_BUYABLE_VEHICLES_SP iTempLoop
							tlLabel = "VEH_GEN_"
							tlLabel += iTempLoop
							REGISTER_ENUM_TO_SAVE(paramVehicleGenSavedVars.sWebVehicles[iPed].eVehGen[iTempLoop], tlLabel)
						ENDREPEAT
					STOP_SAVE_ARRAY()
					
					START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sWebVehicles[iPed].todEmailDate, SIZE_OF(paramVehicleGenSavedVars.sWebVehicles[iPed].todEmailDate), "EMAIL_DATE")
						REPEAT NUMBER_OF_BUYABLE_VEHICLES_SP iTempLoop
							tlLabel = "EMAIL_DATE_"
							tlLabel += iTempLoop
							REGISTER_ENUM_TO_SAVE(paramVehicleGenSavedVars.sWebVehicles[iPed].todEmailDate[iTempLoop], tlLabel)
						ENDREPEAT
					STOP_SAVE_ARRAY()
				STOP_SAVE_STRUCT()
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramVehicleGenSavedVars.sImpoundSwitchVehicles, SIZE_OF(paramVehicleGenSavedVars.sImpoundSwitchVehicles), "SWITCH_IMP")
			REPEAT COUNT_OF(paramVehicleGenSavedVars.sImpoundSwitchVehicles) iTempLoop
				tlLabel = "SWITCH_IMP"
				tlLabel += iTempLoop
				Register_Contents_Of_DynamicVehGenData(paramVehicleGenSavedVars.sImpoundSwitchVehicles[iTempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_BOOL_TO_SAVE(paramVehicleGenSavedVars.bInitialDataSetup, "VEHDATA_SETUP")
		REGISTER_ENUM_TO_SAVE(paramVehicleGenSavedVars.eMissionVehTimeStamp, "MISSVEH_TIME")
		REGISTER_BOOL_TO_SAVE(paramVehicleGenSavedVars.bGarageIntroRun, "GARAGE_INTRO")
		
	STOP_SAVE_STRUCT()
ENDPROC
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the vehicle gen globals that need to be saved
PROC Register_Vehicle_Gen_Saved_Globals()	
	Register_Struct_VehicleGenData(g_savedGlobals.sVehicleGenData, "VEHICLE_GEN_SAVED_DATA_STRUCT")	
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the vehicle gen globals that need to be saved
PROC Register_Vehicle_Gen_Saved_Globals_CLF()	
	Register_Struct_VehicleGenData(g_savedGlobalsClifford.sVehicleGenData, "VEHICLE_GEN_SAVED_DATA_STRUCT")	
ENDPROC
PROC Register_Vehicle_Gen_Saved_Globals_NRM()	
	Register_Struct_VehicleGenData(g_savedGlobalsnorman.sVehicleGenData, "VEHICLE_GEN_SAVED_DATA_STRUCT")	
ENDPROC
