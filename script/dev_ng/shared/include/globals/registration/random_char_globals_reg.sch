USING "rage_builtins.sch"
USING "globals.sch"
//	USING "commands_misc.sch"



PROC Register_One_Random_Char(g_structSavedMissionsRC &RandomCharStruct, STRING NameOfRandomCharStruct)

	START_SAVE_STRUCT_WITH_SIZE(RandomCharStruct, SIZE_OF(RandomCharStruct), NameOfRandomCharStruct)
		REGISTER_INT_TO_SAVE(RandomCharStruct.rcFlags, "RC_FLAGS")
		REGISTER_BOOL_TO_SAVE(RandomCharStruct.rcTimeReqSet, "RC_TIME_REQ_SET")
		REGISTER_ENUM_TO_SAVE(RandomCharStruct.rcTimeReq, "RC_STORED_TIME")
		REGISTER_INT_TO_SAVE(RandomCharStruct.iCompletionOrder, "RC_iCompletionOrder")
		REGISTER_INT_TO_SAVE(RandomCharStruct.iFailsNoProgress, "RC_iFailsNoProgress")
		REGISTER_FLOAT_TO_SAVE(RandomCharStruct.fStatCompletion, "fStatCompletion")
	STOP_SAVE_STRUCT()
ENDPROC



// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the random character globals that need to be saved
PROC Register_Random_Char_Saved_Globals()

	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sRandomChars.savedRC, SIZE_OF(g_savedGlobals.sRandomChars.savedRC), "SAVED_RANDOM_CHARACTERS")

		g_eRC_MissionIDs RandomCharLoop
		
		REPEAT MAX_RC_MISSIONS RandomCharLoop
		
			STRING RandomCharName
			
			SWITCH RandomCharLoop
			
				CASE RC_ABIGAIL_1
					RandomCharName = "RC_ABIGAIL_1"
					BREAK
				
				CASE RC_ABIGAIL_2
					RandomCharName = "RC_ABIGAIL_2"
					BREAK
					
				CASE RC_BARRY_1
					RandomCharName = "RC_BARRY_1"
					BREAK
					
				CASE RC_BARRY_2
					RandomCharName = "RC_BARRY_2"
					BREAK
				
				CASE RC_BARRY_3
					RandomCharName = "RC_BARRY_3"
					BREAK
					
				CASE RC_BARRY_3A
					RandomCharName = "RC_BARRY_3A"
					BREAK
					
				CASE RC_BARRY_3C
					RandomCharName = "RC_BARRY_3C"
					BREAK
					
				CASE RC_BARRY_4
					RandomCharName = "RC_BARRY_4"
					BREAK

				CASE RC_DREYFUSS_1
					RandomCharName = "RC_DREYFUSS_1"
					BREAK

				CASE RC_EPSILON_1
					RandomCharName = "RC_EPSILON_1"
					BREAK
					
				CASE RC_EPSILON_2
					RandomCharName = "RC_EPSILON_2"
					BREAK
					
				CASE RC_EPSILON_3
					RandomCharName = "RC_EPSILON_3"
					BREAK
					
				CASE RC_EPSILON_4
					RandomCharName = "RC_EPSILON_4"
					BREAK
					
				CASE RC_EPSILON_5
					RandomCharName = "RC_EPSILON_5"
					BREAK
					
				CASE RC_EPSILON_6
					RandomCharName = "RC_EPSILON_6"
					BREAK
					
				CASE RC_EPSILON_7
					RandomCharName = "RC_EPSILON_7"
					BREAK
					
				CASE RC_EPSILON_8
					RandomCharName = "RC_EPSILON_8"
					BREAK

				CASE RC_EXTREME_1
					RandomCharName = "RC_EXTREME_1"
					BREAK
					
				CASE RC_EXTREME_2
					RandomCharName = "RC_EXTREME_2"
					BREAK
					
				CASE RC_EXTREME_3
					RandomCharName = "RC_EXTREME_3"
					BREAK
					
				CASE RC_EXTREME_4
					RandomCharName = "RC_EXTREME_4"
					BREAK

				CASE RC_FANATIC_1
					RandomCharName = "RC_FANATIC_1"
					BREAK
					
				CASE RC_FANATIC_2
					RandomCharName = "RC_FANATIC_2"
					BREAK
					
				CASE RC_FANATIC_3
					RandomCharName = "RC_FANATIC_3"
					BREAK
				
				CASE RC_HAO_1
					RandomCharName = "RC_HAO_1"
					BREAK
					
				CASE RC_HUNTING_1
					RandomCharName = "RC_HUNTING_1"
					BREAK
					
				CASE RC_HUNTING_2
					RandomCharName = "RC_HUNTING_2"
					BREAK

				CASE RC_JOSH_1
					RandomCharName = "RC_JOSH_1"
					BREAK
					
				CASE RC_JOSH_2
					RandomCharName = "RC_JOSH_2"
					BREAK
					
				CASE RC_JOSH_3
					RandomCharName = "RC_JOSH_3"
					BREAK
					
				CASE RC_JOSH_4
					RandomCharName = "RC_JOSH_4"
					BREAK

				CASE RC_MAUDE_1
					RandomCharName = "RC_MAUDE_1"
					BREAK

				CASE RC_MINUTE_1
					RandomCharName = "RC_MINUTE_1"
					BREAK
					
				CASE RC_MINUTE_2
					RandomCharName = "RC_MINUTE_2"
					BREAK
					
				CASE RC_MINUTE_3
					RandomCharName = "RC_MINUTE_3"
					BREAK
					
				CASE RC_MRS_PHILIPS_1
					RandomCharName = "RC_MRS_PHILIPS_1"
					BREAK
					
				CASE RC_MRS_PHILIPS_2
					RandomCharName = "RC_MRS_PHILIPS_2"
					BREAK
					
				CASE RC_NIGEL_1
					RandomCharName = "RC_NIGEL_1"
					BREAK
					
				CASE RC_NIGEL_1A
					RandomCharName = "RC_NIGEL_1A"
					BREAK
					
				CASE RC_NIGEL_1B
					RandomCharName = "RC_NIGEL_1B"
					BREAK
					
				CASE RC_NIGEL_1C
					RandomCharName = "RC_NIGEL_1C"
					BREAK
					
				CASE RC_NIGEL_1D
					RandomCharName = "RC_NIGEL_1D"
					BREAK
					
				CASE RC_NIGEL_2
					RandomCharName = "RC_NIGEL_2"
					BREAK
					
				CASE RC_NIGEL_3
					RandomCharName = "RC_NIGEL_3"
					BREAK
				
				CASE RC_OMEGA_1
					RandomCharName = "RC_OMEGA_1"
					BREAK
				
				CASE RC_OMEGA_2
					RandomCharName = "RC_OMEGA_2"
					BREAK
				
				CASE RC_PAPARAZZO_1
					RandomCharName = "RC_PAPARAZZO_1"
					BREAK
					
				CASE RC_PAPARAZZO_2
					RandomCharName = "RC_PAPARAZZO_2"
					BREAK
					
				CASE RC_PAPARAZZO_3
					RandomCharName = "RC_PAPARAZZO_3"
					BREAK
					
				CASE RC_PAPARAZZO_3A
					RandomCharName = "RC_PAPARAZZO_3A"
					BREAK
					
				CASE RC_PAPARAZZO_3B
					RandomCharName = "RC_PAPARAZZO_3B"
					BREAK
					
				CASE RC_PAPARAZZO_4
					RandomCharName = "RC_PAPARAZZO_4"
					BREAK
				
				CASE RC_RAMPAGE_1
					RandomCharName = "RC_RAMPAGE_1"
					BREAK
					
				CASE RC_RAMPAGE_2
					RandomCharName = "RC_RAMPAGE_2"
					BREAK
					
				CASE RC_RAMPAGE_3
					RandomCharName = "RC_RAMPAGE_3"
					BREAK
					
				CASE RC_RAMPAGE_4
					RandomCharName = "RC_RAMPAGE_4"
					BREAK
					
				CASE RC_RAMPAGE_5
					RandomCharName = "RC_RAMPAGE_5"
					BREAK
					
				CASE RC_THELASTONE
					RandomCharName = "RC_THELASTONE"
					BREAK
					
				CASE RC_TONYA_1
					RandomCharName = "RC_TONYA_1"
					BREAK
				
				CASE RC_TONYA_2
					RandomCharName = "RC_TONYA_2"
					BREAK
				
				CASE RC_TONYA_3
					RandomCharName = "RC_TONYA_3"
					BREAK	
				
				CASE RC_TONYA_4
					RandomCharName = "RC_TONYA_4"
					BREAK	

				CASE RC_TONYA_5
					RandomCharName = "RC_TONYA_5"
					BREAK

				DEFAULT
					RandomCharName = "UNKNOWN"
					PRINTLN("Random character ", RandomCharLoop, " is not handled by the switch inside Register_Random_Char_Saved_Globals")
					SCRIPT_ASSERT("Register_Random_Char_Saved_Globals - this random character is not handled in this SWITCH statement")
					BREAK
					
			ENDSWITCH
	
			Register_One_Random_Char(g_savedGlobals.sRandomChars.savedRC[RandomCharLoop], RandomCharName)
	
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomChars.savedRCEvents, "RC_EVENTS")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomChars.iRCMissionsCompleted, "iRCMissionsCompleted")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomChars.g_iCurrentEpsilonPayment, "g_iCurrentEpsilonPayment")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, "g_iWebsiteQueryBit")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomChars.g_iREDomesticCompOrder, "g_iREDomesticCompOrder")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomChars.g_bFanaticHelp, "g_bFanaticHelp")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomChars.g_bFanaticStamina, "g_bFanaticStamina")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomChars.g_bFanaticCheated, "g_bFanaticCheated")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomChars.g_bFinalEpsilonPayment, "g_bFinalEpsilonPayment")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomChars.g_bStoleEpsilonCash, "g_bStoleEpsilonCash")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sRandomChars.g_bTriggeredHao1, "g_bTriggeredHao1")
	
	START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sRandomChars.g_iVisibleInFOWBitset, SIZE_OF(g_savedGlobals.sRandomChars.g_iVisibleInFOWBitset), "VISIBLE_IN_FOW")
		INT iVisibleBitset
		REPEAT RC_MAX_FOW_VISIBLE_BITSETS iVisibleBitset
			TEXT_LABEL_63 tIntName = "VISIBLE_IN_FOW_BITSET_"
			tIntName += iVisibleBitset
			REGISTER_INT_TO_SAVE(g_savedGlobals.sRandomChars.g_iVisibleInFOWBitset[iVisibleBitset], tIntName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
ENDPROC

