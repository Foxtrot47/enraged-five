// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :  triathlon_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all triathlon
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

PROC TRI_Register_One_Race_BestRanks_Array_Saved(INT & sRaceBestRanks[NUM_TRIATHLON_RACES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(sRaceBestRanks, SIZE_OF(sRaceBestRanks), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForVar
		INT iRaceIndex	
		REPEAT COUNT_OF(sRaceBestRanks) iRaceIndex
			saveLabelForVar = saveLabelForArray
			saveLabelForVar += "_BestRank_"
			saveLabelForVar += iRaceIndex			
			REGISTER_INT_TO_SAVE(sRaceBestRanks[iRaceIndex], saveLabelForVar)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC
		
PROC TRI_Register_One_Race_BestTimes_Array_Saved(FLOAT & sRaceBestTimes[NUM_TRIATHLON_RACES], STRING saveLabelForArray)	

	START_SAVE_ARRAY_WITH_SIZE(sRaceBestTimes, SIZE_OF(sRaceBestTimes), saveLabelForArray)
		TEXT_LABEL_63 saveLabelForVar
		INT iRaceIndex	
		REPEAT COUNT_OF(sRaceBestTimes) iRaceIndex
			saveLabelForVar = saveLabelForArray
			saveLabelForVar += "_BestTime_"
			saveLabelForVar += iRaceIndex			
			REGISTER_FLOAT_TO_SAVE(sRaceBestTimes[iRaceIndex], saveLabelForVar)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the offroad globals that need to be saved
PROC Register_Triathlon_Saved_Globals()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sTriathlonData, SIZE_OF(g_savedGlobals.sTriathlonData), "TRIATHLON_SAVED_STRUCT")
		TRI_Register_One_Race_BestTimes_Array_Saved(g_savedGlobals.sTriathlonData.fBestTime, "TRI_TIMES")
		TRI_Register_One_Race_BestRanks_Array_Saved(g_savedGlobals.sTriathlonData.iBestRank, "TRI_RANKS")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sTriathlonData.eCurrentRaceUnlocked, "TRIATHLON_eCurrentUnlock")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTriathlonData.iBitFlags, "TRIATHLON_iBitFlags")
	STOP_SAVE_STRUCT()
ENDPROC

