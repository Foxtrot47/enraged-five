USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   social_globals_reg.sch
//      AUTHOR          :   Kenneth Ross
//      DESCRIPTION     :   Contains the registrations with code for all social globals
//							that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register the struct containing the car app data
/// INPUT PARAMS:
///    paramCarAppSavedVars			This instance of the Car App Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_CarAppData(SOCIAL_CAR_APP_DATA &paramCarAppSavedVars, STRING paramNameOfStruct)	

	INT tempLoop
	TEXT_LABEL_15 tlLabel
	
	START_SAVE_STRUCT_WITH_SIZE(paramCarAppSavedVars, SIZE_OF(paramCarAppSavedVars), paramNameOfStruct)
	
		// Vehicle setup
		REGISTER_ENUM_TO_SAVE(paramCarAppSavedVars.eModel, "Model")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColourID1, "iColourID1")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColourID2, "iColourID2")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWindowTint, "iWindowTint")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeR, "iTyreSmokeR")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeG, "iTyreSmokeG")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeB, "iTyreSmokeB")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iEngine, "iEngine")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iBrakes, "iBrakes")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWheels, "iWheels")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWheelType, "iWheelType")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iExhaust, "iExhaust")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iSuspension, "iSuspension")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iArmour, "iArmour")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iHorn, "iHorn")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iLights, "iLights")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bBulletProofTyres, "bBulletProofTyres")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTurbo, "iTurbo")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmoke, "iTyreSmoke")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramCarAppSavedVars.tlPlateText, "tlPlateText")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iPlateBack, "iPlateBack")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountEngine, "iModCountEngine")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountBrakes, "iModCountBrakes")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountExhaust, "iModCountExhaust")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountWheels, "iModCountWheels")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountHorn, "iModCountHorn")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountArmour, "iModCountArmour")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModCountSuspension, "iModCountSuspension")
		REGISTER_FLOAT_TO_SAVE(paramCarAppSavedVars.fModPriceModifier, "fModPriceModifier")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iModColoursThatCanBeSet, "iModColoursThatCanBeSet")
		
		START_SAVE_ARRAY_WITH_SIZE(paramCarAppSavedVars.iHornHash, SIZE_OF(paramCarAppSavedVars.iHornHash), "iHornHash")
			REPEAT 5 tempLoop
				tlLabel = "iHornHash" tlLabel+=tempLoop
				REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iHornHash[tempLoop], tlLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_ENUM_TO_SAVE(paramCarAppSavedVars.eModKitType, "eModKitType")
				
		// Game data
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bSendDataToCloud, "bSendDataToCloud")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bDeleteData, "bDeleteData")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bUpdateMods, "bUpdateMods")
	
	STOP_SAVE_STRUCT()
ENDPROC

PROC Register_Struct_CarAppOrder(SOCIAL_CAR_APP_ORDER_DATA &paramCarAppSavedVars, STRING paramNameOfStruct)	

	START_SAVE_STRUCT_WITH_SIZE(paramCarAppSavedVars, SIZE_OF(paramCarAppSavedVars), paramNameOfStruct)
	
		// Vehicle setup
		REGISTER_ENUM_TO_SAVE(paramCarAppSavedVars.eModel, "Model")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColourID1, "iColourID1")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColourID2, "iColourID2")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColour1Group, "iColour1Group")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iColour2Group, "iColour2Group")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWindowTint, "iWindowTint")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeR, "iTyreSmokeR")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeG, "iTyreSmokeG")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmokeB, "iTyreSmokeB")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iEngine, "iEngine")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iBrakes, "iBrakes")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWheels, "iWheels")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iWheelType, "iWheelType")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iExhaust, "iExhaust")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iSuspension, "iSuspension")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iArmour, "iArmour")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iHorn, "iHorn")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iLights, "iLights")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bBulletProofTyres, "bBulletProofTyres")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTurbo, "iTurbo")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iTyreSmoke, "iTyreSmoke")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramCarAppSavedVars.tlPlateText, "tlPlateText")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iPlateBack, "iPlateBack")
		
		// Cloud data
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iUID, "UID")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iCost, "Cost")
		REGISTER_TEXT_LABEL_15_TO_SAVE(paramCarAppSavedVars.tlPlateText_pending, "tlPlateText_pending")
		REGISTER_INT_TO_SAVE(paramCarAppSavedVars.iPlateBack_pending, "iPlateBack_pending")
		
		// Game data
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bProcessOrder, "bProcessOrder")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderPending, "bOrderPending")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderReceivedOnBoot, "bOrderReceivedOnBoot")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderForPlayerVehicle, "bOrderForPlayerVehicle")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bCheckPlateProfanity, "bCheckPlateProfanity")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderPaidFor, "bOrderPaidFor")
		
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bSCProfanityFailed, "bSCProfanityFailed")
		REGISTER_BOOL_TO_SAVE(paramCarAppSavedVars.bOrderFailedFunds, "bOrderFailedFunds")
	
	STOP_SAVE_STRUCT()
ENDPROC


/// PURPOSE:
///    Register the struct containing the dog app data
/// INPUT PARAMS:
///    paramDogAppSavedVars			This instance of the Dog App Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_DogAppData(SOCIAL_DOG_APP_DATA &paramDogAppSavedVars, STRING paramNameOfStruct)	

	START_SAVE_STRUCT_WITH_SIZE(paramDogAppSavedVars, SIZE_OF(paramDogAppSavedVars), paramNameOfStruct)
		REGISTER_FLOAT_TO_SAVE(paramDogAppSavedVars.fHappiness, "fHappiness")
		REGISTER_FLOAT_TO_SAVE(paramDogAppSavedVars.fCleanliness, "fCleanliness")
		REGISTER_FLOAT_TO_SAVE(paramDogAppSavedVars.fHunger, "fHunger")
		REGISTER_INT_TO_SAVE(paramDogAppSavedVars.iTrainingLevel, "iTrainingLevel")
		REGISTER_INT_TO_SAVE(paramDogAppSavedVars.iCollar, "iCollar")
		REGISTER_BOOL_TO_SAVE(paramDogAppSavedVars.bAppDataReceived, "bAppDataReceived")
	STOP_SAVE_STRUCT()
ENDPROC


/// PURPOSE:
///    Register the struct containing the social data
/// INPUT PARAMS:
///    paramSocialSavedVars			This instance of the Social Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_SocialData(SocialDataSaved &paramSocialSavedVars, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramSocialSavedVars, SIZE_OF(paramSocialSavedVars), paramNameOfStruct)
		START_SAVE_ARRAY_WITH_SIZE(paramSocialSavedVars.sCarAppData, SIZE_OF(paramSocialSavedVars.sCarAppData), "CAR_APP_DATA")
			Register_Struct_CarAppData(paramSocialSavedVars.sCarAppData[0], "CAR_APP_DATA_M")
			Register_Struct_CarAppData(paramSocialSavedVars.sCarAppData[1], "CAR_APP_DATA_F")
			Register_Struct_CarAppData(paramSocialSavedVars.sCarAppData[2], "CAR_APP_DATA_T")
		STOP_SAVE_ARRAY()
		
		START_SAVE_ARRAY_WITH_SIZE(paramSocialSavedVars.sCarAppOrder, SIZE_OF(paramSocialSavedVars.sCarAppOrder), "CAR_APP_ORDER")
			Register_Struct_CarAppOrder(paramSocialSavedVars.sCarAppOrder[0], "CAR_APP_ORDER_M")
			Register_Struct_CarAppOrder(paramSocialSavedVars.sCarAppOrder[1], "CAR_APP_ORDER_F")
			Register_Struct_CarAppOrder(paramSocialSavedVars.sCarAppOrder[2], "CAR_APP_ORDER_T")
		STOP_SAVE_ARRAY()
		
		Register_Struct_DogAppData(paramSocialSavedVars.sDogAppData, "DOG_APP_DATA")
	STOP_SAVE_STRUCT()
	
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bSingleplayeDataWiped, "bSingleplayeDataWiped")
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bCarAppPlateSet, "bCarAppPlateSet")
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bCarAppUsed, "bCarAppUsed")
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bDogAppUsed, "bDogAppUsed")
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bUpdateDogLocation, "bUpdateDogLocation")
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bDeleteCarData, "bDeleteCarData")
	REGISTER_INT_TO_SAVE(paramSocialSavedVars.iOrderToDelete, "iOrderToDelete")
	
	REGISTER_INT_TO_SAVE(paramSocialSavedVars.iCarAppPlateBack, "iCarAppPlateBack")
	REGISTER_TEXT_LABEL_15_TO_SAVE(paramSocialSavedVars.tlCarAppPlateText, "tlCarAppPlateText")
	
	REGISTER_INT_TO_SAVE(paramSocialSavedVars.iGameUID, "iGameUID")
	
	START_SAVE_ARRAY_WITH_SIZE(paramSocialSavedVars.bPlayerUnlockedInApp, SIZE_OF(paramSocialSavedVars.bPlayerUnlockedInApp), "PED_UNLOCK")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bPlayerUnlockedInApp[0], "0")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bPlayerUnlockedInApp[1], "1")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bPlayerUnlockedInApp[2], "2")
	STOP_SAVE_ARRAY()
	
	START_SAVE_ARRAY_WITH_SIZE(paramSocialSavedVars.bFirstVehicleSentToCloud, SIZE_OF(paramSocialSavedVars.bFirstVehicleSentToCloud), "VEH_SENT")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bFirstVehicleSentToCloud[0], "0")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bFirstVehicleSentToCloud[1], "1")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bFirstVehicleSentToCloud[2], "2")
	STOP_SAVE_ARRAY()
	
	START_SAVE_ARRAY_WITH_SIZE(paramSocialSavedVars.bFirstOrderProcessed, SIZE_OF(paramSocialSavedVars.bFirstOrderProcessed), "FIRST_ORDER")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bFirstOrderProcessed[0], "0")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bFirstOrderProcessed[1], "1")
		REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bFirstOrderProcessed[2], "2")
	STOP_SAVE_ARRAY()
	
	START_SAVE_ARRAY_WITH_SIZE(paramSocialSavedVars.iOrderCount, SIZE_OF(paramSocialSavedVars.iOrderCount), "ORDER_COUNT")
		REGISTER_INT_TO_SAVE(paramSocialSavedVars.iOrderCount[0], "0")
		REGISTER_INT_TO_SAVE(paramSocialSavedVars.iOrderCount[1], "1")
		REGISTER_INT_TO_SAVE(paramSocialSavedVars.iOrderCount[2], "2")
	STOP_SAVE_ARRAY()
	
	REGISTER_BOOL_TO_SAVE(paramSocialSavedVars.bCarAppHelpTextTriggered, "HelpTextTriggered")
	
	REGISTER_INT_TO_SAVE(paramSocialSavedVars.iFacebookPostsMadeBitset, "FacebookPostsMadeBitset")
	REGISTER_INT_TO_SAVE(paramSocialSavedVars.iActivityFeedPostsMadeBitset, "ActivityFeedPostsMadeBitset")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the social globals that need to be saved
PROC Register_Social_Saved_Globals()
	
	Register_Struct_SocialData(g_savedGlobals.sSocialData, "SOCIAL_SAVED_DATA_STRUCT")
	
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the social globals that need to be saved
//PROC Register_Social_Saved_Globals_CLF()
//	
//	Register_Struct_SocialData(g_savedGlobalsClifford.sSocialData, "SOCIAL_SAVED_DATA_STRUCT")
//	
//ENDPROC

