USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   tennis_globals_reg.sch
//      AUTHOR          :   Rob Pearsall
//      DESCRIPTION     :   Contains the registrations with code for Tennis globals that
//							need to be saved
//
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register the struct for Tennis Saved Data
/// PARAMS:
///    tennisData - the tennis global data struct
///    paramNameOfStruct - an unique name for the struct
PROC Register_Struct_Tennis(TennisDataSaved &tennisData, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(tennisData, SIZE_OF(tennisData), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(tennisData.iTotalAces, "TENNIS_iTotalAces")
		REGISTER_INT_TO_SAVE(tennisData.iTotalAced, "TENNIS_iTotalAced")
		REGISTER_INT_TO_SAVE(tennisData.iTotalPoints, "TENNIS_iTotalPoints")
		REGISTER_INT_TO_SAVE(tennisData.iHighestDeuce, "TENNIS_iHighestDeuce")
		REGISTER_INT_TO_SAVE(tennisData.iGamesWon, "TENNIS_iGamesWon")
		REGISTER_INT_TO_SAVE(tennisData.iGamesLost, "TENNIS_iGamesLost")
		REGISTER_INT_TO_SAVE(tennisData.iSetsWon, "TENNIS_iSetsWon")
		REGISTER_INT_TO_SAVE(tennisData.iSetsLost, "TENNIS_iSetsLost")
		REGISTER_INT_TO_SAVE(tennisData.iMatchesWon, "TENNIS_iMatchesWon")
		REGISTER_INT_TO_SAVE(tennisData.iMatchesLost, "TENNIS_iMatchesLost")
		REGISTER_INT_TO_SAVE(tennisData.iTotalOOB, "TENNIS_iTotalOOB")
		REGISTER_INT_TO_SAVE(tennisData.iTotalFaults, "TENNIS_iTotalFaults")
		REGISTER_INT_TO_SAVE(tennisData.iSwingsForStrength, "TENNIS_iSwingsForStrength")
		REGISTER_ENUM_TO_SAVE(tennisData.iSavedFlags, "TENNIS_iSavedFlags")
	STOP_SAVE_STRUCT()

ENDPROC

/// PURPOSE:
///    Registers the tennis saved data struct
PROC Register_Tennis_Saved_Globals()

	Register_Struct_Tennis(g_savedGlobals.sTennisData, "TENNIS_SAVE_DATA")

ENDPROC
