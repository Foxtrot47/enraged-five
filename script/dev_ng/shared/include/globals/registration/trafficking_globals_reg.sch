// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   trafficking_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all trafficking
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

PROC Register_Array_Of_Trafficking_Saved(INT & iItemArray[NUM_WEAPON_CARGO_PKGS], STRING szNameOfArray)
	INT iTemp = 0

	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT NUM_WEAPON_CARGO_PKGS iTemp
			TEXT_LABEL_31 szTraffickingSavedName = "TRAFFICKING_iTimePkgCollected_"
			szTraffickingSavedName += iTemp
			REGISTER_INT_TO_SAVE(iItemArray[iTemp], szTraffickingSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the trafficking globals that need to be saved
PROC Register_Trafficking_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sTraffickingData, SIZE_OF(g_savedGlobals.sTraffickingData), "TRAFFICKING_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iBools, "TRAFFICKING_iBools")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iGroundRank, "TRAFFICKING_iGndRank")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iAirRank, "TRAFFICKING_SAVED_iAirRank")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iLastPairing, "TRAFFICKING_iLastPairing")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iNumRepeatComplete, "TRAFFICKING_iNumRepeatComplete")
		
		Register_Array_Of_Trafficking_Saved(g_savedGlobals.sTraffickingData.iTimePackageCollected, "TRAFFICKING_TIME_COLLECTED_ARRAY")
		
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sTraffickingData.todNextPlayable_Air, "TRAFFICKING_todNextAir")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sTraffickingData.todNextPlayable_Gnd, "TRAFFICKING_todNextGnd")
		
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bGndBlipActive, "TRAFFICKING_GndBlipOn")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bAirBlipActive, "TRAFFICKING_AirBlipOn")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bRanIntroduction, "TRAFFICKING_RanIntro")
		
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bBomb, "TRAFFICKING_bBomb")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bTrain, "TRAFFICKING_bTrain")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bConvoy, "TRAFFICKING_bConvoy")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bTimed, "TRAFFICKING_bTimed")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sTraffickingData.bLowAlt, "TRAFFICKING_bLowAlt")
		
		START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sTraffickingData.iDropLocations, SIZE_OF(g_savedGlobals.sTraffickingData.iDropLocations), "TRAFFICKING_iDropLocations")
			INT iTemp
			REPEAT COUNT_OF(g_savedGlobals.sTraffickingData.iDropLocations) iTemp
				TEXT_LABEL_31 szTraffickingSavedName = "TRAFFICKING_iDropLocations"
				szTraffickingSavedName += iTemp
				REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iDropLocations[iTemp], szTraffickingSavedName)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTraffickingData.iNumDropLocations, "TRAFFICKING_iNumDropLocations")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sTraffickingData.gangTypes, "TRAFFICKING_gangTypes")
	STOP_SAVE_STRUCT()
ENDPROC

