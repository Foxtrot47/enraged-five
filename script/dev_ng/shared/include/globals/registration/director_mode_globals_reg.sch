//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author: Ben Rollinson				Date: 12/12/14				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				  	  Director Mode Saved Global Registration					│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

#IF FEATURE_SP_DLC_DIRECTOR_MODE

	/// PURPOSE:
	///    Sets up the save structure for registering all the director mode globals that need to be saved.
	PROC Register_Director_Mode_Saved_Globals()	

		INT iCounter
		TEXT_LABEL_31 tLabel
		TEXT_LABEL_31 tArrayName

		START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sDirectorModeData, SIZE_OF(g_savedGlobals.sDirectorModeData), "DIRECTOR_SAVED_STRUCT")
		
			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock, "BitsetCharacterStoryUnlock")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock, "BitsetCharacterHeistUnlock")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock, "BitsetCharacterSpecialUnlock")
			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock, "BitsetCharacterAnimalUnlock")

			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, "BitsetTravelLocationRevealed")

			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iExitHelpShownCount, "ExitHelpShownCount")
			
			tArrayName = "DirMode_bookmark"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.iShortlistData, SIZE_OF(g_savedGlobals.sDirectorModeData.iShortlistData), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.iShortlistData) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.iShortlistData[iCounter], tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			tArrayName = "DirMode_bookmarkcat"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.shortlistCategories, SIZE_OF(g_savedGlobals.sDirectorModeData.shortlistCategories), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.shortlistCategories) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.shortlistCategories[iCounter], tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			tArrayName = "DirMode_bookmarkmodel"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.shortlistModels, SIZE_OF(g_savedGlobals.sDirectorModeData.shortlistModels), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.shortlistModels) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.shortlistModels[iCounter], tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.shortlistSex, "BitsetshortlistSex")
			
			tArrayName = "DirMode_bookmarkvoice"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.shortlistVoice, SIZE_OF(g_savedGlobals.sDirectorModeData.shortlistVoice), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.shortlistVoice) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.shortlistVoice[iCounter], tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedMapCoord.x, "Player_headingX")
			REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedMapCoord.y, "Player_headingY")
			REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedMapCoord.z, "Player_headingZ")
			REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.fSavedPlayerHeading, "Player_heading")
				
			tArrayName = "DirMode_userloc"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation, SIZE_OF(g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation) iCounter
					tLabel = tArrayName
					tLabel += "X"
					tLabel += iCounter
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[iCounter].x, tLabel)

					tLabel = tArrayName
					tLabel += "Y"
					tLabel += iCounter
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[iCounter].y, tLabel)
					
					tLabel = tArrayName
					tLabel += "Z"
					tLabel += iCounter
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedUserDefinedLocation[iCounter].z, tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()

			tArrayName = "DirMode_userhead"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading, SIZE_OF(g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.fSavedUserDefinedHeading[iCounter], tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			tArrayName = "DirMode_userInterior"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior, SIZE_OF(g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior), tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_BOOL_TO_SAVE(g_savedGlobals.sDirectorModeData.bSavedUserDefinedInterior[iCounter], tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			#IF FEATURE_SP_DLC_DM_PROP_EDITOR
			tArrayName = "DirMode_PropEditScenePos"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.vSavedPropScenesPos,SIZE_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesPos),tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesPos) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesPos[iCounter].x,tLabel)
					
					tLabel += "y"
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesPos[iCounter].y,tLabel)
					
					tLabel += "z"
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesPos[iCounter].z,tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			tArrayName = "DirMode_PropEditSceneRot"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.vSavedPropScenesRot,SIZE_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesRot),tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesRot) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesRot[iCounter].x,tLabel)
					
					tLabel += "y"
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesRot[iCounter].y,tLabel)
					
					tLabel += "z"
					REGISTER_FLOAT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesRot[iCounter].z,tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			tArrayName = "DirMode_PropEditSceneModel"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.vSavedPropScenesModel,SIZE_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesModel),tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesModel) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_INT_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesModel[iCounter],tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			
			tArrayName = "DirMode_PropEditSceneNames"
			START_SAVE_ARRAY_WITH_SIZE(g_savedGlobals.sDirectorModeData.vSavedPropScenesNames,SIZE_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesNames),tArrayName)
				REPEAT COUNT_OF(g_savedGlobals.sDirectorModeData.vSavedPropScenesNames) iCounter
					tLabel = tArrayName
					tLabel += iCounter
					REGISTER_TEXT_LABEL_23_TO_SAVE(g_savedGlobals.sDirectorModeData.vSavedPropScenesNames[iCounter],tLabel)
				ENDREPEAT
			STOP_SAVE_ARRAY()
			#ENDIF

		STOP_SAVE_STRUCT()
	ENDPROC

#ENDIF

