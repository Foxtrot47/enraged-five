USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   comms_control_globals_reg.sch
//      AUTHOR          :   BenR
//      DESCRIPTION     :   Contains the registrations with code for all the communication
//							controller globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


/// PURPOSE:
///   	Register a struct containing generic communication data. This data is common to all
///    	communications tracked in the communications queues.
/// INPUT PARAMS:
///		paramCommunicationDataStruct		An instance of a generic communication data struct.
///    	paramNameOfStruct					The name of this instance of the struct
PROC Register_Struct_CommunicationData(CC_CommData &paramCommunicationDataStruct, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramCommunicationDataStruct, SIZE_OF(paramCommunicationDataStruct), paramNameOfStruct)
	
	REGISTER_ENUM_TO_SAVE(paramCommunicationDataStruct.eID, "ID")
	REGISTER_INT_TO_SAVE(paramCommunicationDataStruct.iSettings, "Settings")
	REGISTER_INT_TO_SAVE(paramCommunicationDataStruct.iPlayerCharBitset, "Player_Char_Bitset")
	REGISTER_ENUM_TO_SAVE(paramCommunicationDataStruct.ePriority, "Priority")
	REGISTER_INT_TO_SAVE(paramCommunicationDataStruct.iQueueTime, "Queue_Time")
	REGISTER_INT_TO_SAVE(paramCommunicationDataStruct.iRequeueTime, "Requeue_Time")
	REGISTER_ENUM_TO_SAVE(paramCommunicationDataStruct.eNPCCharacter, "NPC_Character")
	REGISTER_ENUM_TO_SAVE(paramCommunicationDataStruct.eRestrictedAreaID, "Restricted_Area_ID")
	REGISTER_ENUM_TO_SAVE(paramCommunicationDataStruct.eExecuteOnCompleteID, "Execute_On_Complete_ID")
	REGISTER_ENUM_TO_SAVE(paramCommunicationDataStruct.eSendCheck, "Send_Check")

	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///   	Register a struct containing phone call communication data.
/// INPUT PARAMS:
///		paramCallDataStruct		An instance of a phone call communication data struct.
///    	paramNameOfStruct		The name of this instance of the struct
PROC Register_Struct_CallData(CC_CallData &paramCallDataStruct, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramCallDataStruct, SIZE_OF(paramCallDataStruct), paramNameOfStruct)

	Register_Struct_CommunicationData( paramCallDataStruct.sCommData, "COMMUNICATION_DATA_STRUCT")
	REGISTER_ENUM_TO_SAVE(paramCallDataStruct.eCommExtra, "eCommExtra")
	REGISTER_ENUM_TO_SAVE(paramCallDataStruct.eCommExtra2, "eCommExtra2")
	REGISTER_ENUM_TO_SAVE(paramCallDataStruct.eYesResponse, "eYesResponse")
	REGISTER_ENUM_TO_SAVE(paramCallDataStruct.eNoResponse, "eNoResponse")
	REGISTER_INT_TO_SAVE(paramCallDataStruct.iSpeakerID, "Speaker_ID")

	STOP_SAVE_STRUCT()
	
ENDPROC


/// PURPOSE:
///   	Register a struct containing text message communication data.
/// INPUT PARAMS:
///		paramTextMessageDataStruct		An instance of a text message communication data struct.
///    	paramNameOfStruct				The name of this instance of the struct
PROC Register_Struct_TextMessageData(CC_TextMessageData &paramTextMessageDataStruct, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramTextMessageDataStruct, SIZE_OF(paramTextMessageDataStruct), paramNameOfStruct)

	Register_Struct_CommunicationData(paramTextMessageDataStruct.sCommData, "COMMUNICATION_DATA_STRUCT")
	REGISTER_ENUM_TO_SAVE(paramTextMessageDataStruct.ePart1, "ePart1")
	REGISTER_ENUM_TO_SAVE(paramTextMessageDataStruct.ePart2, "ePart2")
	REGISTER_INT_TO_SAVE(paramTextMessageDataStruct.iFailCount, "Fail_Count")
	REGISTER_ENUM_TO_SAVE(paramTextMessageDataStruct.WhichCanCallSenderStatus, "WhichCanCallSenderStatus")
	
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///   	Register a struct containing email communication data.
/// INPUT PARAMS:
///		paramEmailDataStruct			An instance of an email communication data struct.
///    	paramNameOfStruct				The name of this instance of the struct
PROC Register_Struct_EmailData(CC_EmailData &paramEmailDataStruct, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramEmailDataStruct, SIZE_OF(paramEmailDataStruct), paramNameOfStruct)

	Register_Struct_CommunicationData( paramEmailDataStruct.sCommData, "COMMUNICATION_DATA_STRUCT")

	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///   	Register the array containing the phone call data structs for the phone call communication queue.
/// INPUT PARAMS:
///		paramQueuedCallStructs[]		An instance of a phone call communication queue array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_QueuedCallStructs(CC_CallData &paramQueuedCallStructs[CC_MAX_QUEUED_CALLS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramQueuedCallStructs, SIZE_OF(paramQueuedCallStructs), paramNameOfArray)
	
	INT index
	REPEAT CC_MAX_QUEUED_CALLS index
		TEXT_LABEL_63 tStructName = "CALL_DATA_STRUCT_"
		tStructName += index
		Register_Struct_CallData(paramQueuedCallStructs[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing the missed phone call data structs.
/// INPUT PARAMS:
///		paramMissedCallStructs[]		An instance of a missed call queue array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_MissedCallStructs(CC_CallData &paramMissedCallStructs[CC_MAX_MISSED_CALLS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramMissedCallStructs, SIZE_OF(paramMissedCallStructs), paramNameOfArray)
	
	INT index
	REPEAT CC_MAX_MISSED_CALLS index
		TEXT_LABEL_63 tStructName = "MISSED_CALL_DATA_STRUCT_"
		tStructName += index
		Register_Struct_CallData(paramMissedCallStructs[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing the chat call data structs.
/// INPUT PARAMS:
///		paramChatCallStructs[]			An instance of a chat call queue array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_ChatCallStructs(CC_CallData &paramChatCallStructs[CC_MAX_CHAT_CALLS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramChatCallStructs, SIZE_OF(paramChatCallStructs), paramNameOfArray)
	
	INT index
	REPEAT CC_MAX_CHAT_CALLS index
		TEXT_LABEL_63 tStructName = "CHAT_CALL_DATA_STRUCT_"
		tStructName += index
		Register_Struct_CallData(paramChatCallStructs[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing the text message data structs for the text message communication queue.
/// INPUT PARAMS:
///		paramQueuedTextStructs[]		An instance of a text message communication queue array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_QueuedTextStructs(CC_TextMessageData &paramQueuedTextStructs[CC_MAX_QUEUED_TEXTS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramQueuedTextStructs, SIZE_OF(paramQueuedTextStructs), paramNameOfArray)
	
	INT index
	REPEAT CC_MAX_QUEUED_TEXTS index
		TEXT_LABEL_63 tStructName = "TEXT_MESSAGE_DATA_STRUCT_"
		tStructName += index
		Register_Struct_TextMessageData(paramQueuedTextStructs[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()
	
ENDPROC


/// PURPOSE:
///   	Register the array containing the sent text message data structs for the communication queue.
/// INPUT PARAMS:
///		paramSentTextStructs[]		An instance of a sent text message communication queue array.
///    	paramNameOfArray			The name of this instance of the array
PROC Register_Array_SentTextStructs(CC_TextMessageData &paramSentTextStructs[CC_MAX_SENT_TEXTS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramSentTextStructs, SIZE_OF(paramSentTextStructs), paramNameOfArray)
	
	INT index
	REPEAT CC_MAX_SENT_TEXTS index
		TEXT_LABEL_63 tStructName = "SENT_TEXT_DATA_STRUCT_"
		tStructName += index
		Register_Struct_TextMessageData(paramSentTextStructs[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()
	
ENDPROC


/// PURPOSE:
///   	Register the array containing the email data structs for the email communication queue.
/// INPUT PARAMS:
///		paramQueuedEmailStructs[]		An instance of a email communication queue array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_QueuedEmailStructs(CC_EmailData &paramQueuedEmailStructs[CC_MAX_QUEUED_EMAILS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramQueuedEmailStructs, SIZE_OF(paramQueuedEmailStructs), paramNameOfArray)
	
	INT index
	REPEAT CC_MAX_QUEUED_EMAILS index
		TEXT_LABEL_63 tStructName = "EMAIL_DATA_STRUCT_"
		tStructName += index
		Register_Struct_EmailData(paramQueuedEmailStructs[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()
	
ENDPROC


/// PURPOSE:
///   	Register the array containing the character priority enums for the communication queues.
/// INPUT PARAMS:
///		paramCharPriorityEnums[]		An instance of a text message communication queue array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_CharacterPriorityEnums(CC_CommunicationPriority &paramCharPriorityEnums[3], STRING paramNameOfArray)
	INT index
	TEXT_LABEL_63 tEnumSaveName

	START_SAVE_ARRAY_WITH_SIZE(paramCharPriorityEnums, SIZE_OF(paramCharPriorityEnums), paramNameOfArray)
	
	REPEAT 3 index
		enumCharacterList eCharacter = INT_TO_ENUM(enumCharacterList, index)
		tEnumSaveName = "Character_Priority_"
		
		SWITCH (eCharacter)
			CASE CHAR_MICHAEL
				tEnumSaveName += "Michael"
			BREAK
			CASE CHAR_FRANKLIN
				tEnumSaveName += "Franklin"
			BREAK
			CASE CHAR_TREVOR
				tEnumSaveName += "Trevor"
			BREAK
		ENDSWITCH

		REGISTER_ENUM_TO_SAVE(paramCharPriorityEnums[index], tEnumSaveName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()
	
ENDPROC


/// PURPOSE:
///    Sets up the save structure for registering all the communication controller globals that need to be saved.
PROC Register_Comms_Control_Globals()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sCommsControlData, SIZE_OF(g_savedGlobals.sCommsControlData), "COMM_CONTROL_DATA_STRUCT")

	//Save the phonecall queue state.
	Register_Array_QueuedCallStructs(g_savedGlobals.sCommsControlData.sQueuedCalls, "COMM_CONTROL_QUEUED_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iNoQueuedCalls, "No_Queued_Calls")
	
	//Save the missed phonecall queue state.
	Register_Array_MissedCallStructs(g_savedGlobals.sCommsControlData.sMissedCalls, "COMM_CONTROL_MISSED_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iNoMissedCalls, "No_Missed_Calls")
	
	//Save the chat phonecall queue state.
	Register_Array_ChatCallStructs(g_savedGlobals.sCommsControlData.sChatCalls, "COMM_CONTROL_CHAT_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iNoChatCalls, "No_Chat_Calls")
	
	//Save the text message communication queue state.
	Register_Array_QueuedTextStructs(g_savedGlobals.sCommsControlData.sQueuedTexts, "COMM_CONTROL_QUEUED_TEXTS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iNoQueuedTexts, "No_Queued_Texts")
	
	//Save the sent text message communication queue state.
	Register_Array_SentTextStructs(g_savedGlobals.sCommsControlData.sSentTexts, "COMM_CONTROL_SENT_TEXTS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iNoSentTexts, "No_Sent_Texts")
	
	//Save the email communication queue state.
	Register_Array_QueuedEmailStructs(g_savedGlobals.sCommsControlData.sQueuedEmails, "COMM_CONTROL_QUEUED_EMAILS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iNoQueuedEmails, "No_Queued_Emails")
	
	//Save "last call" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobals.sCommsControlData.eLastCompletedCall, "Last_Completed_Call")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCommsControlData.bLastCallAnswered, "Last_Call_Answered")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCommsControlData.bLastCallHadResponse, "Last_Call_Had_Response")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCommsControlData.bLastCallResponse, "Last_Call_Response")
	
	//Save "last text" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobals.sCommsControlData.eLastCompletedText, "Last_Completed_Text")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCommsControlData.bLastTextHadResponse, "Last_Text_Had_Response")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sCommsControlData.bLastTextResponse, "Last_Text_Response")
	
	//Save "last email" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobals.sCommsControlData.eLastCompletedEmail, "Last_Completed_Email")
	
	//Save state of character priority levels.
	Register_Array_CharacterPriorityEnums(g_savedGlobals.sCommsControlData.eCharacterPriorityLevel, "COMM_CONTROL_CHAR_PRIORITY_ARRAY")
	
	//Save bit set state of exil warning calls
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iExileWarningBitset, "Exile_Warning_Bitset")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sCommsControlData.iCommsGameTime, "Comms_Game_time")
	
	STOP_SAVE_STRUCT()
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the communication controller globals that need to be saved.
PROC Register_Comms_Control_Globals_CLF()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sCommsControlData, SIZE_OF(g_savedGlobalsClifford.sCommsControlData), "COMM_CONTROL_DATA_STRUCT")

	//Save the phonecall queue state.
	Register_Array_QueuedCallStructs(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls, "COMM_CONTROL_QUEUED_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls, "No_Queued_Calls")
	
	//Save the missed phonecall queue state.
	Register_Array_MissedCallStructs(g_savedGlobalsClifford.sCommsControlData.sMissedCalls, "COMM_CONTROL_MISSED_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls, "No_Missed_Calls")
	
	//Save the chat phonecall queue state.
	Register_Array_ChatCallStructs(g_savedGlobalsClifford.sCommsControlData.sChatCalls, "COMM_CONTROL_CHAT_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iNoChatCalls, "No_Chat_Calls")
	
	//Save the text message communication queue state.
	Register_Array_QueuedTextStructs(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts, "COMM_CONTROL_QUEUED_TEXTS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts, "No_Queued_Texts")
	
	//Save the sent text message communication queue state.
	Register_Array_SentTextStructs(g_savedGlobalsClifford.sCommsControlData.sSentTexts, "COMM_CONTROL_SENT_TEXTS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iNoSentTexts, "No_Sent_Texts")
	
	//Save the email communication queue state.
	Register_Array_QueuedEmailStructs(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails, "COMM_CONTROL_QUEUED_EMAILS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails, "No_Queued_Emails")
	
	//Save "last call" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall, "Last_Completed_Call")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.bLastCallAnswered, "Last_Call_Answered")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.bLastCallHadResponse, "Last_Call_Had_Response")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.bLastCallResponse, "Last_Call_Response")
	
	//Save "last text" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.eLastCompletedText, "Last_Completed_Text")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.bLastTextHadResponse, "Last_Text_Had_Response")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.bLastTextResponse, "Last_Text_Response")
	
	//Save "last email" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.eLastCompletedEmail, "Last_Completed_Email")
	
	//Save state of character priority levels.
	Register_Array_CharacterPriorityEnums(g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel, "COMM_CONTROL_CHAR_PRIORITY_ARRAY")
	
	//Save bit set state of exil warning calls
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iExileWarningBitset, "Exile_Warning_Bitset")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sCommsControlData.iCommsGameTime, "Comms_Game_time")
	
	STOP_SAVE_STRUCT()
ENDPROC
PROC Register_Comms_Control_Globals_NRM()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sCommsControlData, SIZE_OF(g_savedGlobalsnorman.sCommsControlData), "COMM_CONTROL_DATA_STRUCT")
	//Save the phonecall queue state.
	Register_Array_QueuedCallStructs(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls, "COMM_CONTROL_QUEUED_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls, "No_Queued_Calls")	
	//Save the missed phonecall queue state.
	Register_Array_MissedCallStructs(g_savedGlobalsnorman.sCommsControlData.sMissedCalls, "COMM_CONTROL_MISSED_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls, "No_Missed_Calls")	
	//Save the chat phonecall queue state.
	Register_Array_ChatCallStructs(g_savedGlobalsnorman.sCommsControlData.sChatCalls, "COMM_CONTROL_CHAT_CALLS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iNoChatCalls, "No_Chat_Calls")	
	//Save the text message communication queue state.
	Register_Array_QueuedTextStructs(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts, "COMM_CONTROL_QUEUED_TEXTS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts, "No_Queued_Texts")	
	//Save the sent text message communication queue state.
	Register_Array_SentTextStructs(g_savedGlobalsnorman.sCommsControlData.sSentTexts, "COMM_CONTROL_SENT_TEXTS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iNoSentTexts, "No_Sent_Texts")	
	//Save the email communication queue state.
	Register_Array_QueuedEmailStructs(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails, "COMM_CONTROL_QUEUED_EMAILS_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails, "No_Queued_Emails")	
	//Save "last call" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall, "Last_Completed_Call")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.bLastCallAnswered, "Last_Call_Answered")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.bLastCallHadResponse, "Last_Call_Had_Response")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.bLastCallResponse, "Last_Call_Response")	
	//Save "last text" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.eLastCompletedText, "Last_Completed_Text")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.bLastTextHadResponse, "Last_Text_Had_Response")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.bLastTextResponse, "Last_Text_Response")	
	//Save "last email" state.
	REGISTER_ENUM_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.eLastCompletedEmail, "Last_Completed_Email")	
	//Save state of character priority levels.
	Register_Array_CharacterPriorityEnums(g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel, "COMM_CONTROL_CHAR_PRIORITY_ARRAY")	
	//Save bit set state of exil warning calls
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iExileWarningBitset, "Exile_Warning_Bitset")	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sCommsControlData.iCommsGameTime, "Comms_Game_time")
	
	STOP_SAVE_STRUCT()
ENDPROC
