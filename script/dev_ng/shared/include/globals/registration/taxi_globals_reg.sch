// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   taxi_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all taxi
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

PROC Register_Array_Of_Taxi_Missions(TAXI_MISSION_DATA & eItemArray[TXM_NUM_TYPES], STRING szNameOfArray)
	INT iTemp = 0

	START_SAVE_ARRAY_WITH_SIZE(eItemArray, SIZE_OF(eItemArray), szNameOfArray)
		REPEAT TXM_NUM_TYPES iTemp
			TEXT_LABEL_31 szTaxiSavedName = "TAXI_MISSION_PASSED"
			szTaxiSavedName += iTemp
			REGISTER_BOOL_TO_SAVE(eItemArray[iTemp].bIsComplete, szTaxiSavedName)
			
			szTaxiSavedName = "TAXI_MISSION_FAILED"
			szTaxiSavedName += iTemp
			REGISTER_BOOL_TO_SAVE(eItemArray[iTemp].bMissionFailed, szTaxiSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

PROC Register_Array_Of_Taxi_Stats(INT & iItemArray[TAXI_STATS_NUM_STATS], STRING szNameOfArray)
	INT iTemp = 0

	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT TAXI_STATS_NUM_STATS iTemp
			TEXT_LABEL_31 szTaxiSavedName = "TAXI_STAT"
			szTaxiSavedName += iTemp
			REGISTER_INT_TO_SAVE(iItemArray[iTemp], szTaxiSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

PROC Register_Array_Of_Taxi_Runs(INT & iItemArray[TXM_NUM_TYPES], STRING szNameOfArray)
	INT iTemp = 0

	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT TXM_NUM_TYPES iTemp
			TEXT_LABEL_31 szTaxiSavedName = "TAXI_RUNS"
			szTaxiSavedName += iTemp
			REGISTER_INT_TO_SAVE(iItemArray[iTemp], szTaxiSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the taxi globals that need to be saved
PROC Register_Taxi_Saved_Globals()	

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sTaxiData, SIZE_OF(g_savedGlobals.sTaxiData), "TAXI_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTaxiData.iGenericTaxiData, "TAXI_iGenData")
		
		Register_Array_Of_Taxi_Missions(g_savedGlobals.sTaxiData.missions, "TAXI_MISSION_ARRAY")
		Register_Array_Of_Taxi_Stats(g_savedGlobals.sTaxiData.iTaxiStats, "TAXI_STAT_ARRAY")
		Register_Array_Of_Taxi_Runs(g_savedGlobals.sTaxiData.iTaxiOJ_NumRuns, "TAXI_RUNS_ARRAY")
	STOP_SAVE_STRUCT()
ENDPROC

