USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   mp_saved_property_globals_reg.sch
//      AUTHOR          :   David G
//      DESCRIPTION     :   Contains the registrations with code for MP property
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register the array containing the MP Property saved variables
PROC Register_Array_Of_Saved_MP_PROPERTY(MP_SAVED_PROPERTY_STRUCT &savedProperty, STRING paramNameOfArray)

	START_SAVE_STRUCT_WITH_SIZE(savedProperty, SIZE_OF(savedProperty), paramNameOfArray)
		REGISTER_BOOL_TO_SAVE(savedProperty.bTVOn, "TV_ON")
		REGISTER_INT_TO_SAVE(savedProperty.iTVChannelID, "TV_CHANNEL_ID")
		
		REGISTER_BOOL_TO_SAVE(savedProperty.bPenthouseTVOn, "PENTHOUSE_TV_ON")
		REGISTER_INT_TO_SAVE(savedProperty.iPenthouseTVChannelID, "PENTHOUSE_TV_CHANNEL_ID")
		
		REGISTER_BOOL_TO_SAVE(savedProperty.bMediaRoomTVOn, "MEDIA_ROOM_TV_ON")
		REGISTER_INT_TO_SAVE(savedProperty.iMediaRoomTVChannelID, "MEDIA_ROOM_TV_CHANNEL_ID")
		
		INT tempLoop
		TEXT_LABEL_15 tempLabel
		START_SAVE_ARRAY_WITH_SIZE(savedProperty.bRadioOn, SIZE_OF(savedProperty.bRadioOn), "RADIO_ON")
			REPEAT 2 tempLoop
				tempLabel = "RADIO_ON_ID" tempLabel+= tempLoop
				REGISTER_BOOL_TO_SAVE(savedProperty.bRadioOn[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(savedProperty.iRadioStationID, SIZE_OF(savedProperty.iRadioStationID), "RADIO_STATION")
			REPEAT 2 tempLoop
				tempLabel = "RDO_STA_ID" tempLabel+= tempLoop
				REGISTER_INT_TO_SAVE(savedProperty.iRadioStationID[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		
		REGISTER_BOOL_TO_SAVE(savedProperty.bGarageTVOn, "GARAGE_TV_ON")
		REGISTER_INT_TO_SAVE(savedProperty.iGarageTVChannelID, "GARAGE_TV_CHANNEL_ID")
		REGISTER_BOOL_TO_SAVE(savedProperty.bGarageRadioOn, "GARAGE_RADIO_ON")
		REGISTER_INT_TO_SAVE(savedProperty.iGarageRadioStationID, "GARAGE_RADIO_STATION_ID")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesSmoked, "NUMBERS_TIMES_SMOKED")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesDrank, "NUMBER_TIMES_DRANK")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesStrippers, "NUMBER_TIMES_STRIPPERS")
		
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesSmokedSecond, "NUMBERS_TIMES_SMOKED_SECOND")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesDrankSecond, "NUMBER_TIMES_DRANK_SECOND")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesStrippersSecond, "NUMBER_TIMES_STRIPPERS_SECOND")
		
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesSmoked3, "NUMBERS_TIMES_SMOKED3")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesDrank3, "NUMBER_TIMES_DRANK3")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesStrippers3, "NUMBER_TIMES_STRIPPERS3")
		
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesSmoked4, "NUMBERS_TIMES_SMOKED4")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesDrank4, "NUMBER_TIMES_DRANK4")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesStrippers4, "NUMBER_TIMES_STRIPPERS4")
		
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesSmoked5, "NUMBERS_TIMES_SMOKED5")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesDrank5, "NUMBER_TIMES_DRANK5")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesStrippers5, "NUMBER_TIMES_STRIPPERS5")
		
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesSmokedYacht, "NUMBERS_TIMES_SMOKEDYacht")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesDrankYacht, "NUMBER_TIMES_DRANKYacht")
		REGISTER_INT_TO_SAVE(savedProperty.iNumOfTimesStrippersYacht, "NUMBER_TIMES_STRIPPERSYacht")
		
		REGISTER_ENUM_TO_SAVE(savedProperty.TODLastCleaned, "DATE_LAST_CLEANED_APARTMENT")
		REGISTER_ENUM_TO_SAVE(savedProperty.iShowersTakenWithHeadset, "SHOWERS_TAKEN")
		START_SAVE_ARRAY_WITH_SIZE(savedProperty.iArrayNumTimesDrank, SIZE_OF(savedProperty.iArrayNumTimesDrank), "PROP_DRINKS")
			tempLabel = "DRANK_"
			REPEAT 10-PROPERTY_OWNED_SLOT_OFFICE_0 tempLoop  //MAX_OWNED_PROPERTIES too annoyig doing these individually going to move to an array going forward. 
				tempLabel += (tempLoop+5)
				REGISTER_INT_TO_SAVE(savedProperty.iArrayNumTimesDrank[tempLoop], tempLabel)
			ENDREPEAT 
			REPEAT 17 tempLoop  //OLD PROPERTIES
				tempLabel = "DRANK_8"
				tempLabel += tempLoop
				REGISTER_INT_TO_SAVE(savedProperty.iArrayNumTimesDrank[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(savedProperty.iArrayNumTimesSmoked, SIZE_OF(savedProperty.iArrayNumTimesSmoked), "PROP_SMOKES")
			tempLabel = "SMOKED_"
			REPEAT 10-PROPERTY_OWNED_SLOT_OFFICE_0 tempLoop 
				tempLabel += (tempLoop+5)
				REGISTER_INT_TO_SAVE(savedProperty.iArrayNumTimesSmoked[tempLoop], tempLabel)
			ENDREPEAT
			REPEAT 17 tempLoop //OLD PROPERTIES
				tempLabel = "SMOKED_8"
				tempLabel += tempLoop
				REGISTER_INT_TO_SAVE(savedProperty.iArrayNumTimesSmoked[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(savedProperty.iArrayNumTimesStripper, SIZE_OF(savedProperty.iArrayNumTimesStripper), "PROP_STRIPPERS")
			tempLabel = "STRIPPERS_"
			REPEAT 10-PROPERTY_OWNED_SLOT_OFFICE_0 tempLoop 
				tempLabel += (tempLoop+5)
				REGISTER_INT_TO_SAVE(savedProperty.iArrayNumTimesStripper[tempLoop], tempLabel)
			ENDREPEAT
			REPEAT 17 tempLoop //OLD PROPERTIES
				tempLabel = "STRIPPERS_8"
				tempLabel += tempLoop
				REGISTER_INT_TO_SAVE(savedProperty.iArrayNumTimesStripper[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
		START_SAVE_ARRAY_WITH_SIZE(savedProperty.ArenaWarsNamedVehicles, SIZE_OF(savedProperty.ArenaWarsNamedVehicles), "PROP_NAMEDVEHS")
			REPEAT 30 tempLoop 
				tempLabel = "NAMEDVEH_"
				tempLabel += (tempLoop)
				REGISTER_TEXT_LABEL_63_TO_SAVE(savedProperty.ArenaWarsNamedVehicles[tempLoop], tempLabel)
			ENDREPEAT
		STOP_SAVE_ARRAY()
	STOP_SAVE_ARRAY()
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the MP Property globals that need to be saved
PROC REGISTER_MP_PROPERTY_SAVED_GLOBALS(g_structSavedMPGlobals &globalData,INT iIndex)
	TEXT_LABEL_23 tlSlot = 	"MP_PROPERTY_SAVED"
	tlSlot += iIndex
	Register_Array_Of_Saved_MP_PROPERTY(globalData.MpSavedProperty, tlSlot)

ENDPROC
