USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      HEADER NAME    :   CompletionPercentage_globals_reg.sch
//      AUTHOR          :   Steve Taylor
//      DESCRIPTION     :   Contains the registrations with code for Comp Percentage globals
//                          that require or may require to be saved
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





/// PURPOSE:
///    Register all the variables in the struct containing saved variables for each index of the completion percentage struct array.
/// PARAMS:
///    IndividualStructEntry        This instance of the completion percentage struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
PROC Register_Individual_Struct_CompletionPercentage(structCompletionPercentageList &IndividualStructEntry, STRING paramNameOfStruct)  

    START_SAVE_STRUCT_WITH_SIZE(IndividualStructEntry, SIZE_OF(IndividualStructEntry), paramNameOfStruct)

        REGISTER_BOOL_TO_SAVE (IndividualStructEntry.CP_Marked_As_Complete, "Marked_as_Completed")    //Verified as working
        //REGISTER_BOOL_TO_SAVE (IndividualStructEntry.CP_Assigned_as_included_by_Script, "Was_Assigned_as_Completed_by_Script")       //Verified as working
        REGISTER_TEXT_LABEL_TO_SAVE (IndividualStructEntry.CP_Label, "CompPercentage_Label")
        REGISTER_FLOAT_TO_SAVE (IndividualStructEntry.CP_Weighting, "CompPercentage_Weighting")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.CP_Grouping, "CompPercentage_Grouping")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.CP_ChoiceMission, "CompPercentage_ChoiceMission")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.CP_EnumAndVariationData, "CompPercentage_EnumAndVarData")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.CP_SocialClubClusterDisplay, "CompPercentage_SC_Cluster")

        REGISTER_FLOAT_TO_SAVE (IndividualStructEntry.CP_DefinedLocationX, "Runtime_defined_location_X")
        REGISTER_FLOAT_TO_SAVE (IndividualStructEntry.CP_DefinedLocationY, "Runtime_defined_location_Y")

    STOP_SAVE_STRUCT()

ENDPROC





/// PURPOSE:
///    Register the array containing the saved variables for each index of the completion percentage struct array 
/// INPUT PARAMS
///    paramCompletionPercentageArray       The instance of the Array of completion percentage structs to be saved
///    paramNameOfArray                     The name of this instance of the completion percentage array
PROC Register_Array_Of_CompletionPercentage(structCompletionPercentageList &paramCompletionPercentageArray[MAX_COMP_PERCENTAGE_ENTRIES], STRING paramNameOfArray)

   
    INT i_arrayIndex= 0

    START_SAVE_ARRAY_WITH_SIZE(paramCompletionPercentageArray, SIZE_OF(paramCompletionPercentageArray), paramNameOfArray)
        
        TEXT_LABEL_31 CompletionPercentageName
        
        
        WHILE i_arrayIndex < ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES)
            
      
            CompletionPercentageName = g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[i_arrayIndex].CP_label 
           

            CompletionPercentageName += "_Saved"


    
            //Register details for this index of the array of completion percentage structs
            Register_Individual_Struct_CompletionPercentage(paramCompletionPercentageArray[i_arrayIndex], CompletionPercentageName)

            i_arrayIndex ++

        ENDWHILE
        

    STOP_SAVE_ARRAY()


ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each index of the completion percentage struct array 
/// INPUT PARAMS
///    paramCompletionPercentageArray       The instance of the Array of completion percentage structs to be saved
///    paramNameOfArray                     The name of this instance of the completion percentage array
PROC Register_Array_Of_CompletionPercentage_CLF(structCompletionPercentageList &paramCompletionPercentageArray[MAX_COMP_PERCENTAGE_ENTRIES], STRING paramNameOfArray)

    INT i_arrayIndex= 0

    START_SAVE_ARRAY_WITH_SIZE(paramCompletionPercentageArray, SIZE_OF(paramCompletionPercentageArray), paramNameOfArray)        
        TEXT_LABEL_31 CompletionPercentageName   
        WHILE i_arrayIndex < ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES)           
            CompletionPercentageName = g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[i_arrayIndex].CP_label   
            CompletionPercentageName += "_SavedCLF"
			CompletionPercentageName += i_arrayIndex			
//			CPRINTLN(DEBUG_INIT, "<AGENT T>Register_Array_Of_CompletionPercentage_CLF : i_arrayindex:",i_arrayIndex," CompletionPercentageName: ",CompletionPercentageName)

            //Register details for this index of the array of completion percentage structs
            Register_Individual_Struct_CompletionPercentage(paramCompletionPercentageArray[i_arrayIndex], CompletionPercentageName)
            i_arrayIndex ++
        ENDWHILE
    STOP_SAVE_ARRAY()

ENDPROC
PROC Register_Array_Of_CompletionPercentage_NRM(structCompletionPercentageList &paramCompletionPercentageArray[MAX_COMP_PERCENTAGE_ENTRIES], STRING paramNameOfArray)

    INT i_arrayIndex= 0

    START_SAVE_ARRAY_WITH_SIZE(paramCompletionPercentageArray, SIZE_OF(paramCompletionPercentageArray), paramNameOfArray)        
        TEXT_LABEL_31 CompletionPercentageName   
        WHILE i_arrayIndex < ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES)           
            CompletionPercentageName = g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[i_arrayIndex].CP_label   
            CompletionPercentageName += "_SavedNRM"
			CompletionPercentageName += i_arrayIndex			
//			CPRINTLN(DEBUG_INIT, "<AGENT T>Register_Array_Of_CompletionPercentage_CLF : i_arrayindex:",i_arrayIndex," CompletionPercentageName: ",CompletionPercentageName)

            //Register details for this index of the array of completion percentage structs
            Register_Individual_Struct_CompletionPercentage(paramCompletionPercentageArray[i_arrayIndex], CompletionPercentageName)
            i_arrayIndex ++
        ENDWHILE
    STOP_SAVE_ARRAY()

ENDPROC



/// PURPOSE:
///    Register all the variables in the struct containing saved completion percentage variables for a mission, task or oddjob
/// PARAMS:
///    paramCompletionPercentageSaved       The root structs of the data to be saved
///    paramNameOfStruct                    The name of this instance of the struct
PROC Register_Struct_CompletionPercentageSaved(g_CompletionPercentageSavedData &paramCompletionPercentageSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCompletionPercentageSaved, SIZE_OF(paramCompletionPercentageSaved), paramNameOfStruct)

        Register_Array_Of_CompletionPercentage(paramCompletionPercentageSaved.g_CompletionPercentageList, "struct_g_CompletionPercentage")
    
    
        REGISTER_FLOAT_TO_SAVE (paramCompletionPercentageSaved.g_Resultant_CompletionPercentage, "Game_Complete_Percentage")     //Verified as working.

        REGISTER_BOOL_TO_SAVE (paramCompletionPercentageSaved.b_g_OneHundredPercentReached, "Has_100_Percent_Been_Reached")    //Verified as working.

        REGISTER_INT_TO_SAVE (paramCompletionPercentageSaved.g_BitSet_SP_EventFeedEntryTracker, "SP_Event_F_EntryTrackerBitset")

        REGISTER_INT_TO_SAVE (paramCompletionPercentageSaved.g_BitSet_SP_EventFeedEntryTracker_2, "SP_Event_F_EntryTrackerBS_2")

                                                                                                                

    STOP_SAVE_STRUCT()




ENDPROC
/// PURPOSE:
///    Register all the variables in the struct containing saved completion percentage variables for a mission, task or oddjob
/// PARAMS:
///    paramCompletionPercentageSaved       The root structs of the data to be saved
///    paramNameOfStruct                    The name of this instance of the struct
PROC Register_Struct_CompletionPercentageSaved_CLF(g_CompletionPercentageSavedData &paramCompletionPercentageSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCompletionPercentageSaved, SIZE_OF(paramCompletionPercentageSaved), paramNameOfStruct)
        Register_Array_Of_CompletionPercentage_CLF(paramCompletionPercentageSaved.g_CompletionPercentageList, "struct_g_CompletionPercentage")   
        REGISTER_FLOAT_TO_SAVE (paramCompletionPercentageSaved.g_Resultant_CompletionPercentage, "Game_Complete_Percentage")     //Verified as working.
        REGISTER_BOOL_TO_SAVE (paramCompletionPercentageSaved.b_g_OneHundredPercentReached, "Has_100_Percent_Been_Reached")    //Verified as working.
        REGISTER_INT_TO_SAVE (paramCompletionPercentageSaved.g_BitSet_SP_EventFeedEntryTracker, "SP_Event_F_EntryTrackerBitset")
        REGISTER_INT_TO_SAVE (paramCompletionPercentageSaved.g_BitSet_SP_EventFeedEntryTracker_2, "SP_Event_F_EntryTrackerBS_2")
    STOP_SAVE_STRUCT()

ENDPROC
PROC Register_Struct_CompletionPercentageSaved_NRM(g_CompletionPercentageSavedData &paramCompletionPercentageSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCompletionPercentageSaved, SIZE_OF(paramCompletionPercentageSaved), paramNameOfStruct)
        Register_Array_Of_CompletionPercentage_NRM(paramCompletionPercentageSaved.g_CompletionPercentageList, "struct_g_CompletionPercentage")   
        REGISTER_FLOAT_TO_SAVE (paramCompletionPercentageSaved.g_Resultant_CompletionPercentage, "Game_Complete_Percentage")     //Verified as working.
        REGISTER_BOOL_TO_SAVE (paramCompletionPercentageSaved.b_g_OneHundredPercentReached, "Has_100_Percent_Been_Reached")    //Verified as working.
        REGISTER_INT_TO_SAVE (paramCompletionPercentageSaved.g_BitSet_SP_EventFeedEntryTracker, "SP_Event_F_EntryTrackerBitset")
        REGISTER_INT_TO_SAVE (paramCompletionPercentageSaved.g_BitSet_SP_EventFeedEntryTracker_2, "SP_Event_F_EntryTrackerBS_2")
    STOP_SAVE_STRUCT()

ENDPROC
// -----------------------------------------------------------------------------------------------------------
                                                                                 
/// PURPOSE:
///    Sets up the save structure for registering the specific completion percentage globals that need to be saved  - called from startup.sc
PROC Register_CompletionPercentage_Saved_Globals()

   

    Register_Struct_CompletionPercentageSaved(g_savedGlobals.sCompletionPercentageData, "COMP_PERCENT_SAVED_ARRAY")

  
    

    //REGISTER_FLOAT_TO_SAVE (g_Resultant_CompletionPercentage, "Game_Complete_Percentage")     //Verified as working.

    //REGISTER_BOOL_TO_SAVE (b_g_OneHundredPercentReached, "Has_100_Percent_Been_Reached")    //Verified as working.


ENDPROC
                                                                                 
/// PURPOSE:
///    Sets up the save structure for registering the specific completion percentage globals that need to be saved  - called from startup.sc
PROC Register_CompletionPercentage_Saved_Globals_CLF()  
    Register_Struct_CompletionPercentageSaved_CLF(g_savedGlobalsClifford.sCompletionPercentageData, "COMP_PERCENT_SAVED_ARRAY")
ENDPROC
PROC Register_CompletionPercentage_Saved_Globals_NRM()  
    Register_Struct_CompletionPercentageSaved_NRM(g_savedGlobalsnorman.sCompletionPercentageData, "COMP_PERCENT_SAVED_ARRAY")
ENDPROC

