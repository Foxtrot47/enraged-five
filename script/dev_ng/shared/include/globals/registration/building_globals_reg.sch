USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   building_globals_reg.sch
//      AUTHOR          :   Kenneth Ross
//      DESCRIPTION     :   Contains the registrations with code for all building globals
//							that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Save the Door Properties array
/// INPUT PARAMS:
///    paramIntIDsArray			The instance of the Array of Properties to be saved
///    paramNameOfArray			The name of this instance of the Properties Array
PROC Register_Array_Of_DoorProperties(DOOR_STATE_ENUM &paramPropertiesArray[NUMBER_OF_DOORS], STRING paramNameOfArray)

	INT tempLoop = 0

	START_SAVE_ARRAY_WITH_SIZE(paramPropertiesArray, SIZE_OF(paramPropertiesArray), paramNameOfArray)
	
		TEXT_LABEL_31 PropertiesName
		
		REPEAT NUMBER_OF_DOORS tempLoop
			PropertiesName = "DOOR_"
			PropertiesName += tempLoop
			REGISTER_ENUM_TO_SAVE(paramPropertiesArray[tempLoop], PropertiesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Save the Building States array
/// INPUT PARAMS:
///    paramIntIDsArray			The instance of the Array of Properties to be saved
///    paramNameOfArray			The name of this instance of the Properties Array
PROC Register_Array_Of_BuildingStates(BUILDING_STATE_ENUM &paramPropertiesArray[NUMBER_OF_BUILDINGS], STRING paramNameOfArray)

	INT tempLoop = 0

	START_SAVE_ARRAY_WITH_SIZE(paramPropertiesArray, SIZE_OF(paramPropertiesArray), paramNameOfArray)
	
		TEXT_LABEL_31 PropertiesName
		
		REPEAT NUMBER_OF_BUILDINGS tempLoop
			PropertiesName = "BUILDING_"
			PropertiesName += tempLoop
			REGISTER_ENUM_TO_SAVE(paramPropertiesArray[tempLoop], PropertiesName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the struct containing the building properties INT and setup BOOL
/// INPUT PARAMS:
///    paramBuildingSavedVars		This instance of the Shop Data Struct to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_BuildingData(buildingDataSaved &paramBuildingSavedVars, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramBuildingSavedVars, SIZE_OF(paramBuildingSavedVars), paramNameOfStruct)
		Register_Array_Of_DoorProperties(paramBuildingSavedVars.eDoorState, "DOOR_STATES")
		Register_Array_Of_BuildingStates(paramBuildingSavedVars.eBuildingState, "BUILDING_STATES")
		REGISTER_BOOL_TO_SAVE(paramBuildingSavedVars.bDefaultDataSet, 	"BUILDING_DEFAULT_DATA_SET")
	STOP_SAVE_STRUCT()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the shop globals that need to be saved
PROC Register_Building_Saved_Globals()	
	Register_Struct_BuildingData(g_savedGlobals.sBuildingData, "BUILDING_SAVED_DATA_STRUCT")	
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the shop globals that need to be saved
PROC Register_Building_Saved_Globals_CLF()	
	Register_Struct_BuildingData(g_savedGlobalsClifford.sBuildingData, "BUILDING_SAVED_DATA_STRUCT")	
ENDPROC
PROC Register_Building_Saved_Globals_NRM()	
	Register_Struct_BuildingData(g_savedGlobalsnorman.sBuildingData, "BUILDING_SAVED_DATA_STRUCT")	
ENDPROC

