USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   pilotschool_globals_reg.sch
//      AUTHOR          :   Alwyn R
//      DESCRIPTION     :   Contains the registrations with code for all pilot school
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//STRUCT PS_PLAYER_DATA_STRUCT
//	FLOAT						ElapsedTime
//	FLOAT						LandingDistance
//	INT							CheckpointCount
/// PURPOSE:
///    Register all the variables in the struct containing saved variables for pilot school
/// PARAMS:
///    paramPilotSchoolStruct			This instance of the pilot school to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_PilotSchool(PS_PLAYER_DATA_STRUCT &paramPilotSchoolStruct, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramPilotSchoolStruct, SIZE_OF(paramPilotSchoolStruct), paramNameOfStruct)
		REGISTER_FLOAT_TO_SAVE(paramPilotSchoolStruct.ElapsedTime, "ElapsedTime")
		REGISTER_FLOAT_TO_SAVE(paramPilotSchoolStruct.LastElapsedTime, "LastElapsedTime")
		REGISTER_FLOAT_TO_SAVE(paramPilotSchoolStruct.LandingDistance, "LandingDistance")
		REGISTER_FLOAT_TO_SAVE(paramPilotSchoolStruct.LastLandingDistance, "LastLandingDistance")
		REGISTER_FLOAT_TO_SAVE(paramPilotSchoolStruct.Multiplier, "Multiplier")
		REGISTER_INT_TO_SAVE(paramPilotSchoolStruct.CheckpointCount, "CheckpointCount")
		REGISTER_FLOAT_TO_SAVE(paramPilotSchoolStruct.FormationTimer, "FormationTimer")
		REGISTER_BOOL_TO_SAVE(paramPilotSchoolStruct.HasPassedLesson, "HasPassedLesson")
		REGISTER_ENUM_TO_SAVE(paramPilotSchoolStruct.eMedal, "eMedal")
		REGISTER_ENUM_TO_SAVE(paramPilotSchoolStruct.eLastMedal, "eLastMedal")
	STOP_SAVE_STRUCT()

ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each pilot school
/// INPUT PARAMS
///    paramPilotSchoolsArray		The instance of the Array of Pilot School Structs to be saved
///    paramNameOfArray			The name of this instance of the Pilot School Array
PROC Register_Array_Of_PilotSchools(PS_PLAYER_DATA_STRUCT &paramPilotSchoolsArray[NUMBER_OF_PILOT_SCHOOL_CLASSES], STRING paramNameOfArray)

	PILOT_SCHOOL_CLASSES_ENUM eLoop
	
	START_SAVE_ARRAY_WITH_SIZE(paramPilotSchoolsArray, SIZE_OF(paramPilotSchoolsArray), paramNameOfArray)

		TEXT_LABEL_31 pilotSchoolName
		
		REPEAT NUMBER_OF_PILOT_SCHOOL_CLASSES eLoop
			SWITCH (eLoop)
				CASE PSC_Takeoff
					pilotSchoolName = "PSC_Takeoff"
					BREAK
				CASE PSC_Landing
					pilotSchoolName = "PSC_Landing"
					BREAK
				CASE PSC_Inverted
					pilotSchoolName = "PSC_Inverted"
					BREAK
				CASE PSC_Knifing
					pilotSchoolName = "PSC_Knifing"
					BREAK
				CASE PSC_loopTheLoop
					pilotSchoolName = "PSC_loopTheLoop"
					BREAK
				CASE PSC_FlyLow
					pilotSchoolName = "PSC_FlyLow"
					BREAK
				CASE PSC_DaringLanding
					pilotSchoolName = "PSC_DaringLanding"
					BREAK
				CASE PSC_planeCourse
					pilotSchoolName = "PSC_planeCourse"
					BREAK
				CASE PSC_heliCourse
					pilotSchoolName = "PSC_heliCourse"
					BREAK
				CASE PSC_heliSpeedRun
					pilotSchoolName = "PSC_heliSpeedRun"
					BREAK
				CASE PSC_parachuteOntoTarget
					pilotSchoolName = "PSC_parachuteOntoTarget"
					BREAK
				CASE PSC_chuteOntoMovingTarg
					pilotSchoolName = "PSC_chuteOntoMovingTarg"
					BREAK
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_PilotSchools - case missing for a pilot school")
					pilotSchoolName = "Pilot School"
					pilotSchoolName += ENUM_TO_INT(eLoop)
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Pilot Schools Array
			Register_Struct_PilotSchool(paramPilotSchoolsArray[eLoop], pilotSchoolName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register all the variables in the struct containing saved pilot school variables for a character
/// PARAMS:
///    paramFlightSchoolSaved			This instance of the pilot school data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Array_FlightSchoolSaved(FlightSchoolSaved &paramFlightSchoolSaved[NUM_OF_PLAYABLE_PEDS], STRING paramNameOfArray)
	START_SAVE_ARRAY_WITH_SIZE(paramFlightSchoolSaved, SIZE_OF(paramFlightSchoolSaved), paramNameOfArray)
		Register_Array_Of_PilotSchools(paramFlightSchoolSaved[0].PlayerData, "structPilotSchool_p0")
		Register_Array_Of_PilotSchools(paramFlightSchoolSaved[1].PlayerData, "structPilotSchool_p1")
		Register_Array_Of_PilotSchools(paramFlightSchoolSaved[2].PlayerData, "structPilotSchool_p2")
	STOP_SAVE_ARRAY()

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the pilot school globals that need to be saved
PROC Register_PilotSchool_Saved_Globals()

	Register_Array_FlightSchoolSaved(g_savedGlobals.sFlightSchoolData, "PILOT_SCHOOL_SAVED_ARRAY")

ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the pilot school globals that need to be saved
PROC Register_DLCPilotSchool_Saved_Globals()

	REGISTER_INT_TO_SAVE(g_savedGlobalsPilotSchool.iDummySavedVar , "PILOT_SCHOOL_DUMMY_DATA")

ENDPROC
