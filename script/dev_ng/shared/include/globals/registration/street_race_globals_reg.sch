// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   street_Race_globals_reg.sch
//      AUTHOR          :   Ben Rollinson
//      DESCRIPTION     :   Contains the registrations with code for all Street Race
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the Street Race globals that need to be saved.
PROC Register_Street_Race_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sStreetRaceData, SIZE_OF(g_savedGlobals.sStreetRaceData), "STREET_RACE_SAVED_STRUCT")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sStreetRaceData.eRaceToUnlock, "eStreetRaceToUnlock")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sStreetRaceData.iRaceUnlocked,  "iStreetRaceUnlocked")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sStreetRaceData.iRaceWon,       "iStreetRaceWon")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, "iStreetRaceLeaveArea")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sStreetRaceData.bPhonecallDone, "bPhonecallDone")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sStreetRaceData.iSlipstreamHelpCount, "iSlipstreamHelpCount")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sStreetRaceData.bSpecialAbilityHelp, "bSpecialAbilityHelp")
	STOP_SAVE_STRUCT()
ENDPROC
