USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   properties_globals_reg.sch
//      AUTHOR          :   Rob B
//      DESCRIPTION     :   Contains the registrations with code for all property
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for property
/// PARAMS:
///    paramStripClub		This instance of the property ownership data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_Property_Ownership_Data(PROPERTY_OWNERSHIP_DATA &paramPropertyOwnershipData, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramPropertyOwnershipData, SIZE_OF(paramPropertyOwnershipData), paramNameOfStruct)
		REGISTER_ENUM_TO_SAVE(paramPropertyOwnershipData.charOwner, "propertyOwner")
		REGISTER_ENUM_TO_SAVE(paramPropertyOwnershipData.nextManagementEventTime, "propertyNextTime")
		REGISTER_INT_TO_SAVE(paramPropertyOwnershipData.iBit, "bit")
		REGISTER_INT_TO_SAVE(paramPropertyOwnershipData.iLastIncome, "lastincome")
	STOP_SAVE_STRUCT()

ENDPROC



/// PURPOSE:
///    Register the array containing the saved variables for each strip club
/// INPUT PARAMS
///    paramStripClubsArray		The instance of the Array of Property Ownership Structs to be saved
///    paramNameOfArray			The name of this instance of the Property Ownership Array
PROC Register_Array_Of_Property_Ownership_Data(PROPERTY_OWNERSHIP_DATA &paramPropertyOwnershipDataArray[COUNT_OF(PROPERTY_ENUM)], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramPropertyOwnershipDataArray, SIZE_OF(paramPropertyOwnershipDataArray), paramNameOfArray)
		TEXT_LABEL_31 PropertyName
		
		REPEAT COUNT_OF(PROPERTY_ENUM) tempLoop
			PROPERTY_ENUM loopAsEnum = INT_TO_ENUM(PROPERTY_ENUM, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE PROPERTY_TOWING_IMPOUND
					PropertyName = "PROPERTY_TOWING_IMPOUND"
					BREAK
				CASE PROPERTY_TAXI_LOT
					PropertyName = "PROPERTY_TAXI_LOT"
					BREAK
				CASE PROPERTY_ARMS_TRAFFICKING
					PropertyName = "PROPERTY_ARMS_TRAFFICKING"
					BREAK
				CASE PROPERTY_SONAR_COLLECTIONS
					PropertyName = "PROPERTY_SONAR_COLLECTIONS"
					BREAK
				CASE PROPERTY_CAR_MOD_SHOP
					PropertyName = "PROPERTY_CAR_MOD_SHOP"
					BREAK
				CASE PROPERTY_CINEMA_VINEWOOD
					PropertyName = "PROPERTY_CINEMA_VINEWOOD"
					BREAK
				CASE PROPERTY_CINEMA_DOWNTOWN
					PropertyName = "PROPERTY_CINEMA_DOWNTOWN"
					BREAK
				CASE PROPERTY_CINEMA_MORNINGWOOD
					PropertyName = "PROPERTY_CINEMA_MORNINGWOOD"
					BREAK
				CASE PROPERTY_GOLF_CLUB
					PropertyName = "PROPERTY_GOLF_CLUB"
					BREAK
				CASE PROPERTY_CAR_SCRAP_YARD
					PropertyName = "PROPERTY_CAR_SCRAP_YARD"
					BREAK
				CASE PROPERTY_WEED_SHOP
					PropertyName = "PROPERTY_WEED_SHOP"
					BREAK
				CASE PROPERTY_BAR_TEQUILALA
					PropertyName = "PROPERTY_BAR_TEQUILALA"
					BREAK
				CASE PROPERTY_BAR_PITCHERS
					PropertyName = "PROPERTY_BAR_PITCHERS"
					BREAK
				CASE PROPERTY_BAR_HEN_HOUSE
					PropertyName = "PROPERTY_BAR_HEN_HOUSE"
					BREAK
				CASE PROPERTY_BAR_HOOKIES
					PropertyName = "PROPERTY_BAR_HOOKIES"
					BREAK
				CASE PROPERTY_STRIP_CLUB
					PropertyName = "PROPERTY_BAR_STRIP_CLUB"
					BREAK
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Property_Ownership_Data - case missing for a property")
					PropertyName = "Property"
					PropertyName += tempLoop
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Properties Array
			Register_Struct_Property_Ownership_Data(paramPropertyOwnershipDataArray[tempLoop], PropertyName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC

PROC Register_Struct_Property_Data(PropertySaved &paramPropertyData, STRING paramNameOfStruct)
	START_SAVE_STRUCT_WITH_SIZE(paramPropertyData, SIZE_OF(paramPropertyData), paramNameOfStruct)
		Register_Array_Of_Property_Ownership_Data(paramPropertyData.propertyOwnershipData, "OwnershipData")
		REGISTER_INT_TO_SAVE(paramPropertyData.iBit, "bit")
		REGISTER_INT_TO_SAVE(paramPropertyData.iSubVarBit, "subvarbit")
		REGISTER_ENUM_TO_SAVE(paramPropertyData.nextCheckManagementEventTime, "propertyNextTime")
		REGISTER_INT_TO_SAVE(paramPropertyData.iTotalCarsDestroyed, "cardest")
		REGISTER_INT_TO_SAVE(paramPropertyData.iCashFromSonarThisWeek, "soncash")
		REGISTER_INT_TO_SAVE(paramPropertyData.iCashFromTraffickingThisWeek, "trafcash")
		REGISTER_INT_TO_SAVE(paramPropertyData.iCashFromTowingThisWeek, "towcash")
	STOP_SAVE_STRUCT()
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the strip club globals that need to be saved
PROC Register_Properties_Saved_Globals()

	Register_Struct_Property_Data(g_savedGlobals.sPropertyData, "PROPERTIES_SAVED_DATA")

	//Register_Array_Of_Property_Ownership_Data(g_savedGlobals.sStripClubData, "PROPERTIES_SAVED_ARRAY")

ENDPROC


