USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   stripclub_globals_reg.sch
//      AUTHOR          :   Rob B
//      DESCRIPTION     :   Contains the registrations with code for all strip club
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for strip club
/// PARAMS:
///    paramStripClub		This instance of the strip club to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_StripClub(STRIP_CLUB &paramStripClub, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramStripClub, SIZE_OF(paramStripClub), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(paramStripClub.iReputation, "stripClubReputation")
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register all the variables in the struct containing saved variables for stripper
/// PARAMS:
///    paramStripper		This instance of the stripper to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_Stripper(BOOTY_CALL_CONTACT &paramStripper, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramStripper, SIZE_OF(paramStripper), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(paramStripper.iLike, "bootyCallLike")
		REGISTER_INT_TO_SAVE(paramStripper.iNumPlayerDenials, "bootyCallPlayerDenials")
		REGISTER_INT_TO_SAVE(paramStripper.iSextsSent, "bootyCallSextsSent")
		REGISTER_INT_TO_SAVE(paramStripper.iBCCBits,"bootyCallBCCBits")
		REGISTER_BOOL_TO_SAVE(paramStripper.bActivated, "bootyCallActivated")
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register the array containing the saved variables for each strip club
/// INPUT PARAMS
///    paramStripClubsArray		The instance of the Array of Strip Club Structs to be saved
///    paramNameOfArray			The name of this instance of the Strip Club Array
PROC Register_Array_Of_StripClubs(STRIP_CLUB &paramStripClubsArray[COUNT_OF(STRIP_CLUB_ENUM)], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramStripClubsArray, SIZE_OF(paramStripClubsArray), paramNameOfArray)
		TEXT_LABEL_31 StripClubName
		
		REPEAT COUNT_OF(STRIP_CLUB_ENUM) tempLoop
			STRIP_CLUB_ENUM loopAsEnum = INT_TO_ENUM(STRIP_CLUB_ENUM, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE STRIP_CLUB_LOW
					StripClubName = "STRIP_CLUB_LOW"
					BREAK
				/*
				CASE STRIP_CLUB_MED
					StripClubName = "STRIP_CLUB_MED"
					BREAK

				CASE STRIP_CLUB_HIGH
					StripClubName = "STRIP_CLUB_HIGH"
					BREAK
				*/	
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_StripClubs - case missing for a strip club")
					StripClubName = "Strip Club"
					StripClubName += tempLoop
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Strip Clubs Array
			Register_Struct_StripClub(paramStripClubsArray[tempLoop], StripClubName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register the array containing the saved variables for each stripper
/// INPUT PARAMS
///    paramStripperArray		The instance of the Array of Stripper Structs to be saved
///    paramNameOfArray			The name of this instance of the Stripper Array
PROC Register_Array_Of_Booty_Calls(BOOTY_CALL_CONTACT &paramStrippersArray[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramStrippersArray, SIZE_OF(paramStrippersArray), paramNameOfArray)
		TEXT_LABEL_31 StripperName
		
		REPEAT COUNT_OF(BOOTY_CALL_CONTACT_ENUM) tempLoop
			BOOTY_CALL_CONTACT_ENUM loopAsEnum = INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE BC_STRIPPER_JULIET
					StripperName = "BC_STRIPPER_JULIET"
				BREAK

				CASE BC_STRIPPER_NIKKI
					StripperName = "BC_STRIPPER_NIKKI"
				BREAK

				CASE BC_STRIPPER_CHASTITY
					StripperName = "BC_STRIPPER_CHASTITY"
				BREAK
					
				CASE BC_STRIPPER_CHEETAH
					StripperName = "BC_STRIPPER_CHEETAH"
				BREAK

				CASE BC_STRIPPER_SAPPHIRE
					StripperName = "BC_STRIPPER_SAPPHIRE"
				BREAK

				CASE BC_STRIPPER_INFERNUS
					StripperName = "BC_STRIPPER_INFERNUS"
				BREAK
					
				CASE BC_STRIPPER_FUFU
					StripperName = "BC_STRIPPER_FUFU"
				BREAK

				CASE BC_STRIPPER_PEACH
					StripperName = "BC_STRIPPER_PEACH"
				BREAK
					
//				CASE BC_FIXED_CAR
//					StripperName = "BC_FIXED_CAR"
//				BREAK
				
				CASE BC_TAXI_LIZ
					StripperName = "BC_TAXI_LIZ"
				BREAK
				
				CASE BC_HITCHER_GIRL
					StripperName = "BC_HITCHER_GIRL"
				BREAK
					
				
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_Booty_Calls - case missing for a booty call")
					StripperName = "BootyCall"
					StripperName += tempLoop
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Strippers Array
			Register_Struct_Stripper(paramStrippersArray[tempLoop], StripperName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Register all the variables in the struct containing saved strip club variables for a character
/// PARAMS:
///    paramStripClubSaved			This instance of the strip club data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_StripClubSaved(StripClubSaved &paramStripClubSaved, STRING paramNameOfStruct)

	START_SAVE_STRUCT_WITH_SIZE(paramStripClubSaved, SIZE_OF(paramStripClubSaved), paramNameOfStruct)
		Register_Array_Of_StripClubs(paramStripClubSaved.stripClub, "STRIP_CLUBS")
		Register_Array_Of_Booty_Calls(paramStripClubSaved.bootyCall, "BOOTY_CALLS")
	STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register the array containing the saved variables for each strip club saved data
/// INPUT PARAMS
///    paramStripClubSavedArray		The instance of the Array of Stripper Structs to be saved
///    paramNameOfArray			The name of this instance of the Stripper Array
PROC Register_Array_Of_StripClubSaved(StripClubSaved &paramStripClubSavedArray[COUNT_OF(STRIP_CLUB_PLAYER_CHAR_ENUM)], STRING paramNameOfArray)

	INT tempLoop = 0
	
	START_SAVE_ARRAY_WITH_SIZE(paramStripClubSavedArray, SIZE_OF(paramStripClubSavedArray), paramNameOfArray)
		TEXT_LABEL_63 stripClubSavedName
		
		REPEAT COUNT_OF(STRIP_CLUB_PLAYER_CHAR_ENUM) tempLoop
			STRIP_CLUB_PLAYER_CHAR_ENUM loopAsEnum = INT_TO_ENUM(STRIP_CLUB_PLAYER_CHAR_ENUM, tempLoop)
			
			SWITCH (loopAsEnum)
				CASE STRIP_CLUB_PLAYER_CHAR_MICHAEL
					stripClubSavedName = "SAVED_STRIP_CLUB_MICHAEL"
					BREAK

				CASE STRIP_CLUB_PLAYER_CHAR_FRANKLIN
					stripClubSavedName = "SAVED_STRIP_CLUB_FRANKLIN"
					BREAK

				CASE STRIP_CLUB_PLAYER_CHAR_TREVOR
					stripClubSavedName = "SAVED_STRIP_CLUB_TREVOR"
					BREAK
					
				CASE STRIP_CLUB_PLAYER_CHAR_MULTI_0
					stripClubSavedName = "STRIP_CLUB_PLAYER_CHAR_MULTI_0"
					BREAK
				CASE STRIP_CLUB_PLAYER_CHAR_MULTI_1
					stripClubSavedName = "STRIP_CLUB_PLAYER_CHAR_MULTI_1"
					BREAK
				CASE STRIP_CLUB_PLAYER_CHAR_MULTI_2
					stripClubSavedName = "STRIP_CLUB_PLAYER_CHAR_MULTI_2"
					BREAK
				CASE STRIP_CLUB_PLAYER_CHAR_MULTI_3
					stripClubSavedName = "STRIP_CLUB_PLAYER_CHAR_MULTI_3"
					BREAK
				CASE STRIP_CLUB_PLAYER_CHAR_MULTI_4
					stripClubSavedName = "STRIP_CLUB_PLAYER_CHAR_MULTI_4"
					BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Register_Array_Of_StripClubSaved - case missing for a saved strip club data")
					stripClubSavedName = "Strip Club Saved"
					stripClubSavedName += tempLoop
					BREAK
			ENDSWITCH
	
			// Register details for one element of the Strip Club Saved Array
			Register_Struct_StripClubSaved(paramStripClubSavedArray[tempLoop], stripClubSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC



// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the strip club globals that need to be saved
PROC Register_StripClub_Saved_Globals()

	Register_Array_Of_StripClubSaved(g_savedGlobals.sStripClubData, "STRIP_CLUB_SAVED_ARRAY")

ENDPROC


