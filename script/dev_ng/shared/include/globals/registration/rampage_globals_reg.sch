// *****************************************************************************************
// *****************************************************************************************
//
//		FILE NAME		:	rampage_globals_reg.sch
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Contains the registrations with code for all rampage
//							globals that need to be saved.
//
//		NOTES			:	These need to be registered				
//							STRUCT RAMPAGE_PLAYER_DATA
//								INT iMedalIndex	= 0				
//								INT iHighScore = 0
//							ENDSTRUCT
//
//							STRUCT RampageDataSaved
//								RAMPAGE_PLAYER_DATA playerData[NUM_OF_RAMPAGES]
//							ENDSTRUCT
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for Rampages
/// PARAMS:
///    paramPilotSchoolStruct			This instance of the Rampages to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_RampagePlayerData(RAMPAGE_PLAYER_DATA &paramDataStruct, STRING paramNameOfStruct)
	START_SAVE_STRUCT_WITH_SIZE(paramDataStruct, SIZE_OF(paramDataStruct), paramNameOfStruct)
		REGISTER_INT_TO_SAVE(paramDataStruct.iMedalIndex, "MedalIndex")
		REGISTER_INT_TO_SAVE(paramDataStruct.iHighScore, "HighScore")
	STOP_SAVE_STRUCT()
ENDPROC

/// PURPOSE:
///    Register the array containing the saved variables for each Rampage
/// INPUT PARAMS
///    paramDataArray		The instance of the Array of Rampages to be saved
///    paramNameOfArray			The name of this instance of the Rampages Array
PROC Register_Array_Of_Rampages(RAMPAGE_PLAYER_DATA &paramDataArray[NUM_OF_RAMPAGES], STRING paramNameOfArray)
	INT i 
	TEXT_LABEL_31 rampageName
	START_SAVE_ARRAY_WITH_SIZE(paramDataArray, SIZE_OF(paramDataArray), paramNameOfArray)
	
	REPEAT COUNT_OF(paramDataArray) i
		rampageName = "Rampage"
		rampageName += (i + 1)
		Register_Struct_RampagePlayerData(paramDataArray[i], rampageName)
	ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Register all the variables in the struct containing saved Rampage variables 
/// PARAMS:
///    paramDataSaved			This instance of the rampage data to be saved
///    paramNameOfStruct			The name of this instance of the struct
PROC Register_Struct_RampageDataSaved(RampageDataSaved &paramDataSaved, STRING paramNameOfStruct)
	START_SAVE_STRUCT_WITH_SIZE(paramDataSaved, SIZE_OF(paramDataSaved), paramNameOfStruct)
		Register_Array_Of_Rampages(paramDataSaved.playerData, "structRampagePlayerData")
	STOP_SAVE_STRUCT()
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the pilot school globals that need to be saved
PROC Register_Rampage_Saved_Globals()
	Register_Struct_RampageDataSaved(g_savedGlobals.sRampageData, "RAMPAGE_SAVED_ARRAY")
ENDPROC



