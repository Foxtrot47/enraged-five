// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   sea_Race_globals_reg.sch
//      AUTHOR          :   Dave Roberts
//      DESCRIPTION     :   Contains the registrations with code for all Sea Race
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the Sea Race globals that need to be saved.
PROC Register_Sea_Race_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sSeaRaceData, SIZE_OF(g_savedGlobals.sSeaRaceData), "SEA_RACE_SAVED_STRUCT")
		REGISTER_BOOL_TO_SAVE(g_savedGlobals.sSeaRaceData.bFirstBlip, "bSeaRaceFirstBlip")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sSeaRaceData.iRaceWon, "iSeaRaceWon")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sSeaRaceData.iRaceLeaveArea, "iSeaRaceLeaveArea")
	STOP_SAVE_STRUCT()
ENDPROC
