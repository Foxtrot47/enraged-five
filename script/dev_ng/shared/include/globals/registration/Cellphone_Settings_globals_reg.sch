USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      HEADER NAME     :   Cellphone_Settings_globals_reg.sch
//      AUTHOR          :   Steve Taylor
//      DESCRIPTION     :   Contains the registrations with code for cellphone settings
//                          that require or may require to be saved
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


                                                                                                  

/// PURPOSE:
///    Register all the variables in the struct containing saved variables for each index of the cellphone settings struct array.
/// PARAMS:
///    IndividualStructEntry        This instance of the cellphone settings struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
PROC Register_Individual_Struct_CellphoneSettings(structThis_Cellphone_Owner_Settings &IndividualStructEntry, STRING paramNameOfStruct)  

    START_SAVE_STRUCT_WITH_SIZE(IndividualStructEntry, SIZE_OF(IndividualStructEntry), paramNameOfStruct)
        REGISTER_TEXT_LABEL_23_TO_SAVE (IndividualStructEntry.ScaleformOS_Movie_Name, "Movie_Name_For_This_Player")

        REGISTER_INT_TO_SAVE (IndividualStructEntry.OSTypeForThisPlayer, "OS_For_This_Player")

        REGISTER_INT_TO_SAVE (IndividualStructEntry.ThemeForThisPlayer, "Theme_For_This_Player")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.VibrateForThisPlayer, "Vibrate_For_This_Player")  
        
        REGISTER_INT_TO_SAVE (IndividualStructEntry.ProviderForThisPlayer, "Provider_For_This_Player")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.WallpaperForThisPlayer, "Wallpaper_For_This_Player")
 
        
        REGISTER_TEXT_LABEL_23_TO_SAVE (IndividualStructEntry.RingtoneForThisPlayer, "Ringtone_For_This_Player")

        REGISTER_BOOL_TO_SAVE (IndividualStructEntry.g_LastMessageSentMustBeRead, "LastMessageNeedsRead")
        REGISTER_BOOL_TO_SAVE (IndividualStructEntry.g_LaunchToTextMessageScreen, "LaunchToTextMessageScreen")

        

        REGISTER_BOOL_TO_SAVE (IndividualStructEntry.Is_This_Player_On_Scheduled_Activity, "ThisPlayerOnScheduledActivity")

    STOP_SAVE_STRUCT()

ENDPROC





/// PURPOSE:
///    Register all the variables in the struct containing saved variables for each index of the text message struct array.
/// PARAMS:
///    IndividualStructEntry        This instance of the text message struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
PROC Register_Individual_Struct_TextMessage(structTextMessage &IndividualStructEntry, STRING paramNameOfStruct)  

    START_SAVE_STRUCT_WITH_SIZE(IndividualStructEntry, SIZE_OF(IndividualStructEntry), paramNameOfStruct)

        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.TxtMsgLabel, "GXTlabel")

        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgFeedEntryId, "FeedId_Int")
       
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgSender, "Sender")


        //Could condense time by combining time into one int as used in my time comparison routines. Optimise later.
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgTimeSent.TxtMsgSecs, "SentSecs")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgTimeSent.TxtMsgMins, "SentMins")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgTimeSent.TxtMsgHours, "SentHours")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgTimeSent.TxtMsgDay, "SentDay")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgTimeSent.TxtMsgMonth, "SentMonth")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgTimeSent.TxtMsgYear, "SentYear")



        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgLockStatus, "LockStatus")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgCritical, "IsCritical")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgAutoUnlockStatus, "AutoUnlock")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgDeletionMode, "DelMode")
        
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgReadStatus, "ReadStatus")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgReplyStatus, "ReplyStatus")

        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgCanCallSenderStatus, "CallSenderStatus")
        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgBarterStatus, "BarterStatus")

        REGISTER_ENUM_TO_SAVE (IndividualStructEntry.TxtMsgSpecialComponents, "SpecialComps")
        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.TxtMsgStringComponent, "StringComp")
        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgNumberComponent, "NumberComp")


        REGISTER_INT_TO_SAVE (IndividualStructEntry.TxtMsgNumberOfAdditionalStrings, "NumberofAdditionalStrings")

        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.TxtMsgSecondStringComponent, "SecondStringComp")
        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.TxtMsgThirdStringComponent, "ThirdStringComp")

        //Upped this to a TL_63 for #2024559
        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.TxtMsgSenderStringComponent, "SenderStringComp")

   



        START_SAVE_ARRAY_WITH_SIZE(IndividualStructEntry.PhonePresence, SIZE_OF(IndividualStructEntry.PhonePresence), "PhonePresenceArray")
            REGISTER_BOOL_TO_SAVE (IndividualStructEntry.PhonePresence[0], "MichaelPres")
            REGISTER_BOOL_TO_SAVE (IndividualStructEntry.PhonePresence[1], "FrankPres")
            REGISTER_BOOL_TO_SAVE (IndividualStructEntry.PhonePresence[2], "TrevPres")
            REGISTER_BOOL_TO_SAVE (IndividualStructEntry.PhonePresence[3], "MP_Pres")
        STOP_SAVE_ARRAY()

      

    STOP_SAVE_STRUCT()

ENDPROC





PROC Register_Individual_Struct_GalleryImage(structGalleryImage &IndividualStructEntry, STRING paramNameOfStruct)  

    START_SAVE_STRUCT_WITH_SIZE(IndividualStructEntry, SIZE_OF(IndividualStructEntry), paramNameOfStruct)

        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.GalleryImage_ThumbLabel, "Thumb_label")
       
        REGISTER_TEXT_LABEL_63_TO_SAVE (IndividualStructEntry.GalleryImage_PhotoLabel, "Photo_label")


    STOP_SAVE_STRUCT()

ENDPROC
















/// PURPOSE:
///    Register the array containing the saved variables for each index of the cellphone settings struct array 
/// INPUT PARAMS
///    paramCellphoneSettingsArray       The instance of the Array of cellphone settings structs to be saved
///    paramNameOfArray                     The name of this instance of the cellphone settings array
PROC Register_Array_Of_CellphoneSettings(structThis_Cellphone_Owner_Settings &paramCellphoneSettingsArray[Max_Setting_Profiles], STRING paramNameOfArray)

   
    INT i_arrayIndex= 0


    START_SAVE_ARRAY_WITH_SIZE(paramCellphoneSettingsArray, SIZE_OF(paramCellphoneSettingsArray), paramNameOfArray)

        TEXT_LABEL_23 CellphoneSettingsName
        
        
        WHILE i_arrayIndex < ENUM_TO_INT(Max_Setting_Profiles)
            
      
            SWITCH i_arrayIndex
            
                CASE 0
                
                    CellphoneSettingsName = "Michael_Char_0_CS"

                BREAK 


                CASE 1
                
                    CellphoneSettingsName = "Franklin_Char_1_CS"

                BREAK 


                CASE 2
                
                    CellphoneSettingsName = "Trevor_Char_2_CS"

                BREAK 


                CASE 3
                
                    CellphoneSettingsName = "MP_Char_3_CS"

                BREAK 


            ENDSWITCH


    
            //Register details for this index of the array of cellphone settings structs
            Register_Individual_Struct_CellphoneSettings(paramCellphoneSettingsArray[i_arrayIndex], CellphoneSettingsName)

            i_arrayIndex ++

        ENDWHILE
        

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CELLPHONE_SETTINGS_GLOBALS_REG : Registering array of owner settings.")
            PRINTNL()

        #endif

    STOP_SAVE_ARRAY()


ENDPROC






/// PURPOSE:
///    Register the array containing the saved variables for each index of the text message struct array 
/// INPUT PARAMS
///    paramTextMessageArray       The instance of the Array of text message structs to be saved
///    paramNameOfArray                     The name of this instance of the text message array
PROC Register_Array_Of_TextMessages(structTextMessage &paramTextMessageArray[MAX_TEXT_MESSAGES], STRING paramNameOfArray)

   
    INT i_arrayIndex= 0

    START_SAVE_ARRAY_WITH_SIZE(paramTextMessageArray, SIZE_OF(paramTextMessageArray), paramNameOfArray)

        TEXT_LABEL_23 TextMessageName
        
        
        WHILE i_arrayIndex < MAX_TEXT_MESSAGES
            
      
            TextMessageName = "TextMsgIndex"
            TextMessageName += "_"
            TextMessageName += i_arrayIndex


    
         
            Register_Individual_Struct_TextMessage(paramTextMessageArray[i_arrayIndex], TextMessageName)

            i_arrayIndex ++

        ENDWHILE
        

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CELLPHONE_SETTINGS_GLOBALS_REG : Registering array of text messages.")
            PRINTNL()

        #endif

    STOP_SAVE_ARRAY()


ENDPROC






PROC Register_Array_Of_GalleryImages(structGalleryImage &paramGalleryImageArray[MAX_GALLERY_IMAGES], STRING paramNameOfArray)

   
    INT i_arrayIndex= 0

    START_SAVE_ARRAY_WITH_SIZE(paramGalleryImageArray, SIZE_OF(paramGalleryImageArray), paramNameOfArray)

        TEXT_LABEL_23 GalleryImageName
        
        
        WHILE i_arrayIndex < MAX_GALLERY_IMAGES
            
      
            GalleryImageName = "GalleryImgIndex"
            GalleryImageName += "_"
            GalleryImageName += i_arrayIndex


    
            Register_Individual_Struct_GalleryImage(paramGalleryImageArray[i_arrayIndex], GalleryImageName)

            i_arrayIndex ++

        ENDWHILE
        

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CELLPHONE_SETTINGS_GLOBALS_REG : Registering array of gallery images.")
            PRINTNL()

        #endif

    STOP_SAVE_ARRAY()


ENDPROC


















/// PURPOSE:
///    Register all the variables in the struct containing saved cellphone settings variables for a mission, task or oddjob
/// PARAMS:
///    paramCellphoneSettingsSaved          The root structs of the data to be saved
///    paramNameOfStruct                    The name of this instance of the struct
PROC Register_Struct_CellphoneSettingsSaved(g_CellphoneSettingsSavedData &paramCellphoneSettingsSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCellphoneSettingsSaved, SIZE_OF(paramCellphoneSettingsSaved), paramNameOfStruct)
        Register_Array_Of_CellphoneSettings(paramCellphoneSettingsSaved.This_Cellphone_Owner_Settings, "struct_g_CellphoneSettings")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed, "Focus_Lock_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed, "Sleep_Warning_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed, "SleepIcon_Help_Been_Displayed")  
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed, "Sleep_Reminder_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_HasQuickSaveHelpBeenDisplayed, "QuickSave_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_HasTranslucentIconHelpBeenDisplayed, "TranslucentIcon_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_MP_HasTranslucentIconHelpBeenDisplayed, "MPTranslucentIcon_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable, "Is_Sniper_App_Available")
        REGISTER_BOOL_TO_SAVE (g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable, "Is_Trackify_App_Available")
    STOP_SAVE_STRUCT()

ENDPROC
/// PURPOSE:
///    Register all the variables in the struct containing saved cellphone settings variables for a mission, task or oddjob
/// PARAMS:
///    paramCellphoneSettingsSaved          The root structs of the data to be saved
///    paramNameOfStruct                    The name of this instance of the struct
PROC Register_Struct_CellphoneSettingsSaved_CLF(g_CellphoneSettingsSavedData &paramCellphoneSettingsSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCellphoneSettingsSaved, SIZE_OF(paramCellphoneSettingsSaved), paramNameOfStruct)
        Register_Array_Of_CellphoneSettings(paramCellphoneSettingsSaved.This_Cellphone_Owner_Settings, "struct_g_CellphoneSettings")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed, "Focus_Lock_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed, "Sleep_Warning_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed, "SleepIcon_Help_Been_Displayed")  
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed, "Sleep_Reminder_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_HasQuickSaveHelpBeenDisplayed, "QuickSave_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_HasTranslucentIconHelpBeenDisplayed, "TranslucentIcon_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_MP_HasTranslucentIconHelpBeenDisplayed, "MPTranslucentIcon_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_IsSniperAppAvailable, "Is_Sniper_App_Available")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsClifford.sCellphoneSettingsData.b_IsTrackifyAppAvailable, "Is_Trackify_App_Available")
    STOP_SAVE_STRUCT()

ENDPROC
PROC Register_Struct_CellphoneSettingsSaved_NRM(g_CellphoneSettingsSavedData &paramCellphoneSettingsSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramCellphoneSettingsSaved, SIZE_OF(paramCellphoneSettingsSaved), paramNameOfStruct)
        Register_Array_Of_CellphoneSettings(paramCellphoneSettingsSaved.This_Cellphone_Owner_Settings, "struct_g_CellphoneSettings")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_HasSettingsHelpBeenDisplayed, "Focus_Lock_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_HasSleepWarningBeenDisplayed, "Sleep_Warning_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_HasSleepIconHelpBeenDisplayed, "SleepIcon_Help_Been_Displayed")  
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_HasSleepReminderBeenDisplayed, "Sleep_Reminder_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_HasQuickSaveHelpBeenDisplayed, "QuickSave_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_HasTranslucentIconHelpBeenDisplayed, "TranslucentIcon_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_MP_HasTranslucentIconHelpBeenDisplayed, "MPTranslucentIcon_Help_Been_Displayed")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_IsSniperAppAvailable, "Is_Sniper_App_Available")
        REGISTER_BOOL_TO_SAVE (g_savedGlobalsnorman.sCellphoneSettingsData.b_IsTrackifyAppAvailable, "Is_Trackify_App_Available")
    STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register all the variables in the struct containing saved text message variables for a mission, task or oddjob
/// PARAMS:
///    paramTextMessageSaved                The root structs of the data to be saved
///    paramNameOfStruct                    The name of this instance of the struct
PROC Register_Struct_TextMessagesSaved(g_TextMessageSavedData &paramTextMessageSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramTextMessageSaved, SIZE_OF(paramTextMessageSaved), paramNameOfStruct)
        Register_Array_Of_TextMessages(paramTextMessageSaved.g_TextMessage, "struct_g_TextMessage")
    STOP_SAVE_STRUCT()

ENDPROC





PROC Register_Struct_GalleryImagesSaved(g_GalleryImageSavedData &paramGalleryImageSaved, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramGalleryImageSaved, SIZE_OF(paramGalleryImageSaved), paramNameOfStruct)
        Register_Array_Of_GalleryImages(paramGalleryImageSaved.g_GalleryImage, "struct_g_GalleryImage")
    STOP_SAVE_STRUCT()

ENDPROC
















// -----------------------------------------------------------------------------------------------------------
                                                                                 
/// PURPOSE:
///    Sets up the save structure for registering the specific cellphone settings globals that need to be saved  - called from startup.sc
PROC Register_CellphoneSettings_Saved_Globals()

   

    Register_Struct_CellphoneSettingsSaved(g_savedGlobals.sCellphoneSettingsData, "CELLPHONE_SETTINGS_SAVED_ARRAY")

    Register_Struct_TextMessagesSaved(g_savedGlobals.sTextMessageSavedData, "TEXT_MESSAGES_SAVED_ARRAY")

    Register_Struct_GalleryImagesSaved(g_savedGlobals.sGalleryImageSavedData, "GALLERY_IMAGES_SAVED_ARRAY")


      
ENDPROC
// -----------------------------------------------------------------------------------------------------------
                                                                                 
/// PURPOSE:
///    Sets up the save structure for registering the specific cellphone settings globals that need to be saved  - called from startup.sc
PROC Register_CellphoneSettings_Saved_Globals_CLF()
    Register_Struct_CellphoneSettingsSaved_CLF(g_savedGlobalsClifford.sCellphoneSettingsData, "CELLPHONE_SETTINGS_SAVED_ARRAY")
    Register_Struct_TextMessagesSaved(g_savedGlobalsClifford.sTextMessageSavedData, "TEXT_MESSAGES_SAVED_ARRAY")
    Register_Struct_GalleryImagesSaved(g_savedGlobalsClifford.sGalleryImageSavedData, "GALLERY_IMAGES_SAVED_ARRAY")
ENDPROC
PROC Register_CellphoneSettings_Saved_Globals_NRM()
    Register_Struct_CellphoneSettingsSaved_NRM(g_savedGlobalsnorman.sCellphoneSettingsData, "CELLPHONE_SETTINGS_SAVED_ARRAY")
    Register_Struct_TextMessagesSaved(g_savedGlobalsnorman.sTextMessageSavedData, "TEXT_MESSAGES_SAVED_ARRAY")
    Register_Struct_GalleryImagesSaved(g_savedGlobalsnorman.sGalleryImageSavedData, "GALLERY_IMAGES_SAVED_ARRAY")
ENDPROC


