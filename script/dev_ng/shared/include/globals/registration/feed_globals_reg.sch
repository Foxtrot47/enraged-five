
//registration for feed buffer data

USING "rage_builtins.sch"
USING "globals.sch"




/*
STRUCT FEED_DATA_ENTRY
	
	ENUM_FEED_DATA_TYPE myType//dicates what scaleform queue it goes in
	ENUM_FEED_ICONS eIcon//sets which icon it uses
	
	//message header - a simple text label, not used for all types
		TEXT_LABEL mheader
	
	//message core - a wide awake nightmare of substrings
	//has to be like this to support all the systems that can pass data into this
		//core
		TEXT_LABEL mcore	
		//format //remember to count previous when adding
		ENUM_FEED_SUB_TYPES coreComponents[MAX_FEED_COMPONENTS] //-1 unused, otherwise substring pass will be attempted
		
		//subs//totals		
		TEXT_LABEL subtext[MAX_FEED_TEXT_LABEL_COMPONENTS]
		
		TEXT_LABEL_31 subnames[MAX_FEED_COMPONENTS]
		
		FLOAT subfloats[MAX_FEED_FLOAT_COMPONENTS]
		
		INT subints[MAX_FEED_INT_COMPONENTS]
	
	
	//NOTES
	//supplemental data fields //substrings etc etc
	//structTextMessage -> TxtMsgSpecialComponents //msgdata
	//multiplayer names
	//TEXT_LABEL_31 //player names are 32 chars max // max of 4 in mp message
	
ENDSTRUCT

*/



PROC Register_Struct_Feed_DataEntry(FEED_DATA_ENTRY &tosave,INT index)

	TEXT_LABEL t = "feed"
	
	t += index
	TEXT_LABEL b = t
	TEXT_LABEL f = b
	INT i = 0
	
	
	
	//ENUM_FEED_DATA_TYPE myType
	b += "ty"
	REGISTER_ENUM_TO_SAVE(tosave.myType,b)
	
	
	b = t
	b += "ic"
	//ENUM_FEED_ICONS eIcon
	REGISTER_ENUM_TO_SAVE(tosave.eIcon,b)
	
	//TEXT_LABEL mheader
	b = t
	b += "mh"
	REGISTER_TEXT_LABEL_TO_SAVE(tosave.mheader,b)
	//TEXT_LABEL mcore	
	b = t
	b += "mc"
	REGISTER_TEXT_LABEL_TO_SAVE(tosave.mcore,b)
	
	
	
	
	
	//ENUM_FEED_SUB_TYPES coreComponents[MAX_FEED_COMPONENTS] 
	b = t
	b += "cc"
	START_SAVE_ARRAY_WITH_SIZE(tosave.coreComponents, SIZE_OF(tosave.coreComponents), b)
		i = 0
		REPEAT MAX_FEED_COMPONENTS i
			f = b
			f += i
			REGISTER_ENUM_TO_SAVE(tosave.coreComponents[i],f)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
	
	//TEXT_LABEL subtext[MAX_FEED_TEXT_LABEL_COMPONENTS]
	b = t
	b += "st"
	START_SAVE_ARRAY_WITH_SIZE(tosave.subtext, SIZE_OF(tosave.subtext), b)
		i = 0
		REPEAT MAX_FEED_TEXT_LABEL_COMPONENTS i
			f = b
			f += i
			REGISTER_TEXT_LABEL_TO_SAVE(tosave.subtext[i],f)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
	//TEXT_LABEL_31 subnames[MAX_FEED_PLAYER_NAME_COMPONENTS]
	b = t
	b += "sn"
	START_SAVE_ARRAY_WITH_SIZE(tosave.subnames, SIZE_OF(tosave.subnames), b)
		i = 0
		REPEAT MAX_FEED_PLAYER_NAME_COMPONENTS i
			f = b
			f += i
			REGISTER_TEXT_LABEL_31_TO_SAVE(tosave.subnames[i],f)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
	//FLOAT subfloats[MAX_FEED_FLOAT_COMPONENTS]
	b = t
	b += "sf"
	START_SAVE_ARRAY_WITH_SIZE(tosave.subfloats, SIZE_OF(tosave.subfloats), b)
		i = 0
		REPEAT MAX_FEED_FLOAT_COMPONENTS i
			f = b
			f += i
			REGISTER_FLOAT_TO_SAVE(tosave.subfloats[i],f)
		ENDREPEAT
	STOP_SAVE_ARRAY()
	
	//INT subints[MAX_FEED_INT_COMPONENTS]
	b = t
	b += "si"
	START_SAVE_ARRAY_WITH_SIZE(tosave.subints, SIZE_OF(tosave.subints), b)
		i = 0
		REPEAT MAX_FEED_INT_COMPONENTS i
			f = b
			f += i
			REGISTER_INT_TO_SAVE(tosave.subints[i],f)
		ENDREPEAT
	STOP_SAVE_ARRAY()

	
ENDPROC



















































