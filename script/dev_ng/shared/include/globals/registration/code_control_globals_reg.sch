USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 08/05/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  Code Controller Saved Globals Registration				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///   	Register the array containing the run flags for all codeIDs.
/// INPUT PARAMS:
///		paramCodeIDRunFlags[]			An instance of a CodeID run flag array.
///    	paramNameOfArray				The name of this instance of the array.
PROC Register_Array_CodeIDRunFlags(BOOL &paramCodeIDRunFlags[CID_MAX], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramCodeIDRunFlags, SIZE_OF(paramCodeIDRunFlags), paramNameOfArray)
	
	INT index
	REPEAT CID_MAX index
		TEXT_LABEL_63 tFlagName = "CODE_ID_RUNFLAG_"
		tFlagName += index
		REGISTER_BOOL_TO_SAVE(paramCodeIDRunFlags[index], tFlagName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///   	Register the array containing the run flags for all codeIDs.
/// INPUT PARAMS:
///		paramCodeIDExecuteTimers[]		An instance of a CodeID execute timers array.
///    	paramNameOfArray				The name of this instance of the array.
PROC Register_Array_CodeIDExecuteTimers(INT &paramCodeIDExecuteTimers[CID_MAX], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramCodeIDExecuteTimers, SIZE_OF(paramCodeIDExecuteTimers), paramNameOfArray)
	
	INT index
	REPEAT CID_MAX index
		TEXT_LABEL_63 tTimerName = "CODE_ID_EXECUTE_TIMER_"
		tTimerName += index
		REGISTER_INT_TO_SAVE(paramCodeIDExecuteTimers[index], tTimerName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the code controller globals that need to be saved.
PROC Register_Code_Control_Globals()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sCodeControlData, SIZE_OF(g_savedGlobals.sCodeControlData), "CODE_CONTROL_DATA_STRUCT")
		Register_Array_CodeIDRunFlags(g_savedGlobals.sCodeControlData.bRunCodeID, "CODE_CONTROL_RUNFLAG_ARRAY")
		Register_Array_CodeIDExecuteTimers(g_savedGlobals.sCodeControlData.iExecuteTimeForCodeID, "CODE_CONTROL_EXECUTE_TIMER_ARRAY")
	STOP_SAVE_STRUCT()	
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the code controller globals that need to be saved.
PROC Register_Code_Control_Globals_CLF()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sCodeControlData, SIZE_OF(g_savedGlobalsClifford.sCodeControlData), "CODE_CONTROL_DATA_STRUCT")
		Register_Array_CodeIDRunFlags(g_savedGlobalsClifford.sCodeControlData.bRunCodeID, "CODE_CONTROL_RUNFLAG_ARRAY")
		Register_Array_CodeIDExecuteTimers(g_savedGlobalsClifford.sCodeControlData.iExecuteTimeForCodeID, "CODE_CONTROL_EXECUTE_TIMER_ARRAY")
	STOP_SAVE_STRUCT()	
ENDPROC
PROC Register_Code_Control_Globals_NRM()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sCodeControlData, SIZE_OF(g_savedGlobalsnorman.sCodeControlData), "CODE_CONTROL_DATA_STRUCT")
		Register_Array_CodeIDRunFlags(g_savedGlobalsnorman.sCodeControlData.bRunCodeID, "CODE_CONTROL_RUNFLAG_ARRAY")
		Register_Array_CodeIDExecuteTimers(g_savedGlobalsnorman.sCodeControlData.iExecuteTimeForCodeID, "CODE_CONTROL_EXECUTE_TIMER_ARRAY")
	STOP_SAVE_STRUCT()
	
ENDPROC
