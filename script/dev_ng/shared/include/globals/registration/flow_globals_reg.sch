USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   flow_globals_reg.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   Contains the registrations with code for all mission flow
//                          globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Register all the variables in the struct containing a single flow command.
/// PARAMS:
///    paramStructFlowCommands      This instance of the Flow Commands Struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
PROC Register_Struct_FlowCommand(FLOW_COMMANDS &paramStructFlowCommand, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramStructFlowCommand, SIZE_OF(paramStructFlowCommand), paramNameOfStruct)

        REGISTER_ENUM_TO_SAVE(paramStructFlowCommand.command, "flowCommandID")
        REGISTER_ENUM_TO_SAVE(paramStructFlowCommand.storageType, "flowCommandStorageType")
        REGISTER_INT_TO_SAVE(paramStructFlowCommand.index, "flowCommandIndex")
    STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register all the variables in the struct containing saved variables for strands
/// PARAMS:
///    paramStrandSavedVars     This instance of the Strands Struct to be saved
///    paramNameOfStruct        The name of this instance of the struct
PROC Register_Struct_StrandSavedVars(STRAND_SAVED_VARS &paramStrandSavedVars, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramStrandSavedVars, SIZE_OF(paramStrandSavedVars), paramNameOfStruct)
        REGISTER_INT_TO_SAVE(paramStrandSavedVars.savedBitflags, "flowStrandBitFlags")
        REGISTER_INT_TO_SAVE(paramStrandSavedVars.thisCommandPos, "flowStrandCommandPos")
        REGISTER_INT_TO_SAVE(paramStrandSavedVars.thisCommandHashID, "flowStrandCommandHashID")
    STOP_SAVE_STRUCT()

ENDPROC


/// PURPOSE:
///    Register the array containing the saved variables for each strand
/// INPUT PARAMS
///    paramStrandsArray        The instance of the Array of Strands Structs to be saved
///    paramNameOfArray         The name of this instance of the Strands Array
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Array_Of_Strands(STRAND_SAVED_VARS &paramStrandsArray[MAX_STRANDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramStrandsArray, SIZE_OF(paramStrandsArray), paramNameOfArray)

        TEXT_LABEL_31 StrandName
        
        REPEAT MAX_STRANDS tempLoop
            STRANDS loopAsEnum = INT_TO_ENUM(STRANDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE STRAND_AGENCY_HEIST
                    StrandName = "STRAND_AGENCY_HEIST"
                    BREAK
					
				CASE STRAND_ASSASSINATIONS
                    StrandName = "STRAND_ASSASSINATIONS"
                    BREAK
			
                CASE STRAND_ARMENIAN
                    StrandName = "STRAND_ARMENIAN"
                    BREAK

                CASE STRAND_CAR_STEAL
                    StrandName = "STRAND_CAR_STEAL"
                    BREAK

                CASE STRAND_CHINESE
                    StrandName = "STRAND_CHINESE"
                    BREAK

                CASE STRAND_DOCKS_HEIST
                    StrandName = "STRAND_DOCKS_HEIST"
                    BREAK
					
				CASE STRAND_DOCKS_HEIST_2
                    StrandName = "STRAND_DOCKS_HEIST_2"
                    BREAK
                    
                CASE STRAND_EXILE
                    StrandName = "STRAND_EXILE"
                    BREAK

                CASE STRAND_FAMILY
                    StrandName = "STRAND_FAMILY"
                    BREAK

                CASE STRAND_FBI_OFFICERS
                    StrandName = "STRAND_FBI_OFFICERS"
                    BREAK
					
				CASE STRAND_FBI_OFFICERS_2
                    StrandName = "STRAND_FBI_OFFICERS_2"
                    BREAK
					
				CASE STRAND_FBI_OFFICERS_3
                    StrandName = "STRAND_FBI_OFFICERS_3"
                    BREAK
					
				CASE STRAND_FBI_OFFICERS_4
                    StrandName = "STRAND_FBI_OFFICERS_4"
                    BREAK
					
				CASE STRAND_FBI_OFFICERS_5
                    StrandName = "STRAND_FBI_OFFICERS_5"
                    BREAK
					
				CASE STRAND_FINALE
                    StrandName = "STRAND_FINALE"
                    BREAK

                CASE STRAND_FINALE_HEIST
                    StrandName = "STRAND_FINALE_HEIST"
                    BREAK
					
				CASE STRAND_FINALE_HEIST_2
                    StrandName = "STRAND_FINALE_HEIST_2"
                    BREAK
					
				CASE STRAND_FINALE_HEIST_3
                    StrandName = "STRAND_FINALE_HEIST_3"
                    BREAK
					
				CASE STRAND_FINALE_HEIST_4
                    StrandName = "STRAND_FINALE_HEIST_4"
                    BREAK
                    
                CASE STRAND_FRANKLIN
                    StrandName = "STRAND_FRANKLIN"
                    BREAK

                CASE STRAND_JEWEL_HEIST
                    StrandName = "STRAND_JEWEL_HEIST"
                    BREAK
					
				CASE STRAND_JEWEL_HEIST_2
                    StrandName = "STRAND_JEWEL_HEIST_2"
                    BREAK
                    
                CASE STRAND_LAMAR
                    StrandName = "STRAND_LAMAR"
                    BREAK

                CASE STRAND_LESTER
                    StrandName = "STRAND_LESTER"
                    BREAK
					
				CASE STRAND_MARTIN
                    StrandName = "STRAND_MARTIN"
                    BREAK
                    
                CASE STRAND_MICHAEL
                    StrandName = "STRAND_MICHAEL"
                    BREAK
					
				CASE STRAND_MICHAEL_EVENTS
                    StrandName = "STRAND_MICHAEL_EVENTS"
                    BREAK

                CASE STRAND_PROLOGUE
                    StrandName = "STRAND_PROLOGUE"
                    BREAK

                CASE STRAND_RURAL_BANK_HEIST
                    StrandName = "STRAND_RURAL_BANK_HEIST"
                    BREAK
					
				CASE STRAND_SHRINK
                    StrandName = "STRAND_SHRINK"
                    BREAK
                    
                CASE STRAND_SOLOMON
                    StrandName = "STRAND_SOLOMON"
                    BREAK

                CASE STRAND_TREVOR
                    StrandName = "STRAND_TREVOR"
                    BREAK
                    
                DEFAULT
                    SCRIPT_ASSERT("Register_Array_Of_Strands_Globals - case missing for a strand")
                    StrandName = "StrandDetails"
                    StrandName += tempLoop
                    BREAK
            ENDSWITCH
    
            // Register details for one element of the Strands Array
            Register_Struct_StrandSavedVars(paramStrandsArray[tempLoop], StrandName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#endif

/// PURPOSE:
///    Register the array containing the saved variables for each strand
/// INPUT PARAMS
///    paramStrandsArray        The instance of the Array of Strands Structs to be saved
///    paramNameOfArray         The name of this instance of the Strands Array
#if USE_CLF_DLC
PROC Register_Array_Of_Strands_CLF(STRAND_SAVED_VARS &paramStrandsArray[MAX_STRANDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramStrandsArray, SIZE_OF(paramStrandsArray), paramNameOfArray)

        TEXT_LABEL_31 StrandName
        
        REPEAT MAX_STRANDS tempLoop
            STRANDS loopAsEnum = INT_TO_ENUM(STRANDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE STRAND_CLF
                    StrandName = "STRAND_CLF"
                    BREAK
					
				CASE STRAND_CLF_IAA
                    StrandName = "STRAND_CLF_IAA"
                    BREAK
			
                CASE STRAND_CLF_KOR
                    StrandName = "STRAND_CLF_KOR"
                    BREAK

                CASE STRAND_CLF_RUS
                    StrandName = "STRAND_CLF_RUS"
                    BREAK

                CASE STRAND_CLF_ARA
                    StrandName = "STRAND_CLF_ARA"
                    BREAK

                CASE STRAND_CLF_CAS1
                    StrandName = "STRAND_CLF_CAS1"
                    BREAK
					
				CASE STRAND_CLF_CAS2
                    StrandName = "STRAND_CLF_CAS2"
                    BREAK
					
                CASE STRAND_CLF_ASS_PAP
                    StrandName = "STRAND_CLF_ASS_PAP"
                    BREAK			 
					
				 CASE STRAND_CLF_ASS_A14
                    StrandName = "STRAND_CLF_ASS_A14"
                    BREAK			
					
				 CASE STRAND_CLF_JET
                    StrandName = "STRAND_CLF_JET"
                    BREAK
					
				 CASE STRAND_CLF_RC_ALEX
					StrandName = "STRAND_CLF_RC_ALE"
					BREAK
					
				CASE STRAND_CLF_RC_MELODY
					StrandName = "STRAND_CLF_RC_MEL"
					BREAK
					
				CASE STRAND_CLF_RC_AGNES
					StrandName = "STRAND_CLF_RC_AGN"
					BREAK
					
				CASE STRAND_CLF_RC_CLAIRE
					StrandName = "STRAND_CLF_RC_CLA"
					BREAK			
				  
                DEFAULT
                    StrandName = "StrandDetails"
                    StrandName += tempLoop
                    BREAK
            ENDSWITCH
    
            // Register details for one element of the Strands Array
            Register_Struct_StrandSavedVars(paramStrandsArray[tempLoop], StrandName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Array_Of_Strands_NRM(STRAND_SAVED_VARS &paramStrandsArray[MAX_STRANDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramStrandsArray, SIZE_OF(paramStrandsArray), paramNameOfArray)

        TEXT_LABEL_31 StrandName
        
        REPEAT MAX_STRANDS tempLoop
            STRANDS loopAsEnum = INT_TO_ENUM(STRANDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE STRAND_NRM_SURVIVE
                    StrandName = "STRAND_NRM_SURVIVE"
                    BREAK
				CASE STRAND_NRM_RESCUE_ENG
				    StrandName = "STRAND_NRM_RESCUE_ENG"
                    BREAK
				CASE STRAND_NRM_RESCUE_MED
				    StrandName = "STRAND_NRM_RESCUE_MED"
                    BREAK
				CASE STRAND_NRM_RESCUE_GUN
				    StrandName = "STRAND_NRM_RESCUE_GUN"
                    BREAK
				CASE STRAND_NRM_SUPPLY_FUEL
				    StrandName = "STRAND_NRM_SUPPLY_FUEL"
                    BREAK		
				CASE STRAND_NRM_SUPPLY_AMMO
				    StrandName = "STRAND_NRM_SUPPLY_AMMO"
                    BREAK		
				CASE STRAND_NRM_SUPPLY_MED
				    StrandName = "STRAND_NRM_SUPPLY_MED"
                    BREAK
				CASE STRAND_NRM_SUPPLY_FOOD
				    StrandName = "STRAND_NRM_SUPPLY_FOOD"
                    BREAK	
				CASE STRAND_NRM_RADIO
				    StrandName = "STRAND_NRM_RADIO"
                    BREAK	
					
                DEFAULT
                    StrandName = "StrandDetails"
                    StrandName += tempLoop
                    BREAK
            ENDSWITCH
    
            // Register details for one element of the Strands Array
            Register_Struct_StrandSavedVars(paramStrandsArray[tempLoop], StrandName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Save the Synchronisation IDs array
/// INPUT PARAMS:
///    paramSyncIDsArray        The instance of the Array of SyncIDs to be saved
///    paramNameOfArray         The name of this instance of the SyncIDs Array
#if not USE_CLF_DLC
#if not USE_NRM_DLC	
PROC Register_Array_Of_SyncIDs(BOOL &paramSyncIDsArray[MAX_SYNCHRONISATION_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramSyncIDsArray, SIZE_OF(paramSyncIDsArray), paramNameOfArray)

        TEXT_LABEL_31 SyncIDName
        
        REPEAT MAX_SYNCHRONISATION_IDS tempLoop
            SYNCHRONIZATION_IDS loopAsEnum = INT_TO_ENUM(SYNCHRONIZATION_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE SYNC_ASSA3_TO_CARSTEAL1
                    SyncIDName = "SYNC_ASSA3_TO_CARSTEAL1"
                    BREAK
					
				CASE SYNC_ASSA5_TO_EXILE1
					SyncIDName = "SYNC_ASSA5_TO_EXILE1"
                    BREAK
			
                CASE SYNC_CAR_STEAL_TO_MICHAEL
                    SyncIDName = "SYNC_CAR_STEAL_TO_MICHAEL"
                    BREAK
					
				CASE SYNC_CAR_STEAL3_TO_MARTIN1
                    SyncIDName = "SYNC_CAR_STEAL3_TO_MARTIN1"
                    BREAK
                    
                CASE SYNC_CHINESE_TO_TREVOR
                    SyncIDName = "SYNC_CHINESE_TO_TREVOR"
                    BREAK
					
				CASE SYNC_DOCKS_HEIST_P_TO_FBI_OFF_3
                    SyncIDName = "SYNC_DOCKS_HEISTP_TO_FBIO3"
                    BREAK
					
				CASE SYNC_DOCKS_HEIST_TO_DOCKS_HEIST_2
                    SyncIDName = "SYNC_DOCKSH_TO_DOCKSH_2"
                    BREAK
					
				CASE SYNC_EXILE2_TO_RURAL_HEIST1
                    SyncIDName = "SYNC_EXILE2_TO_RURAL_H1"
                    BREAK
					
				CASE SYNC_EXILE3_TO_RURAL_HEIST2
                    SyncIDName = "SYNC_EXILE3_TO_RURAL_H2"
                    BREAK
					
				CASE SYNC_FAMILY3_TO_TONYA1
					SyncIDName = "SYNC_FAMILY3_TO_TONYA1"
                    BREAK
					
				CASE SYNC_FAMILY2_TO_FRANKLIN0
					SyncIDName = "SYNC_FAMILY2_TO_FRANKLIN0"
                    BREAK
					
				CASE SYNC_FAMILY_TO_FBI_OFF
					SyncIDName = "SYNC_FAMILY_TO_FBI_OFF"
					BREAK 
   
                CASE SYNC_FAMILY_TO_TREVOR
                    SyncIDName = "SYNC_FAMILY_TO_TREVOR"
                    BREAK
					
				CASE SYNC_FAMILY6_TO_AGENCY1
                    SyncIDName = "SYNC_FAMILY6_TO_AGENCY1"
                    BREAK
					
				CASE SYNC_FBI_OFF4_TO_ASS1
					SyncIDName = "SYNC_FBI_OFF4_TO_ASS1"
					BREAK
					
				CASE SYNC_FBI_OFF4_TO_DOCKSH2
					SyncIDName = "SYNC_FBI_OFF4_TO_DOCKSH2"
					BREAK
					
				CASE SYNC_FBI_OFF4_TO_FRANKLIN1
					SyncIDName = "SYNC_FBI_OFF4_TO_FRANKLIN1"
					BREAK
					
				CASE SYNC_FBI_OFF_TO_FBI_OFF2
                    SyncIDName = "SYNC_FBI_OFF_TO_FBI_OFF2"
                    BREAK
					
				CASE SYNC_FBI_OFF_TO_FBI_OFF3
                    SyncIDName = "SYNC_FBI_OFF_TO_FBI_OFF3"
                    BREAK
					
				CASE SYNC_FBI_OFF_TO_FBI_OFF4
                    SyncIDName = "SYNC_FBI_OFF_TO_FBI_OFF4"
                    BREAK
					
				CASE SYNC_FBI_OFF_TO_FBI_OFF5
                    SyncIDName = "SYNC_FBI_OFF_TO_FBI_OFF5"
                    BREAK
					
                CASE SYNC_FBI_OFF4_TO_DOCKSH_PREP
                    SyncIDName = "SYNC_FBI_OFF4_TO_DOCKS_H_P"
                    BREAK
                    
                CASE SYNC_FBI_OFF_TO_EXILE
                    SyncIDName = "SYNC_FBI_OFF_TO_EXILE"
                    BREAK
                    
                CASE SYNC_FBI_OFF_TO_FAMILY
                    SyncIDName = "SYNC_FBI_OFF_TO_FAMILY"
                    BREAK
					
				CASE SYNC_FINALE_HEIST1_TO_SOLOMON1B
                    SyncIDName = "SYNC_FINALEH1_TO_SOL1B"
                    BREAK
					
				CASE SYNC_FINALE_HEIST_TO_MICHAEL4
                    SyncIDName = "SYNC_FINALEH_TO_MICHAEL4"
                    BREAK
					
				CASE SYNC_FINALE_HEIST_TO_FINALE_HEIST2
                    SyncIDName = "SYNC_FINALEH_TO_FINALEH2"
                    BREAK
					
				CASE SYNC_FINALE_HEIST_TO_FINALE_HEIST3
                    SyncIDName = "SYNC_FINALEH_TO_FINALEH3"
                    BREAK
					
				CASE SYNC_FINALE_HEIST_TO_FINALE_HEIST4
                    SyncIDName = "SYNC_FINALEH_TO_FINALEH4"
                    BREAK
					
				CASE SYNC_FRANKLIN1_TO_LAMAR
                    SyncIDName = "SYNC_FRANKLIN1_TO_LAMAR"
                    BREAK
   
				CASE SYNC_FRANKLIN1_TO_FBI_OFF2
                    SyncIDName = "SYNC_FRANKLIN1_TO_FBI_OFF2"
                    BREAK
					
				CASE SYNC_FRANKLIN2_TO_FAMILY6
                    SyncIDName = "SYNC_FRANKLIN2_TO_FAMILY6"
                    BREAK
					
				CASE SYNC_FRANKLIN2_TO_MICHAEL3
                    SyncIDName = "SYNC_FRANKLIN2_TO_MICHAEL3"
                    BREAK
					
                CASE SYNC_JEWEL_HEIST_PREP_TO_FAMILY3
                    SyncIDName = "SYNC_JEWELHP_TO_FAMILY3"
                    BREAK
					
				CASE SYNC_JEWEL_HEIST_TO_JEWEL_HEIST_2
                    SyncIDName = "SYNC_JEWELH_TO_JEWELH2"
                    BREAK
					
				CASE SYNC_LAMAR1_TO_FRANKLIN0
					SyncIDName = "SYNC_LAMAR1_TO_FRANKLIN0"
					BREAK
					
				CASE SYNC_MARTIN1_TO_CARSTEAL2
                    SyncIDName = "SYNC_MARTIN1_TO_CAR_STEAL2"
                    BREAK
					
				CASE SYNC_MICHAEL1_TO_CARS3
                    SyncIDName = "SYNC_MICHAEL1_TO_CARS3"
                    BREAK
					
                CASE SYNC_MICHAEL2_TO_CAR_STEAL4
                    SyncIDName = "SYNC_MICHAEL2_TO_CAR_STEAL4"
                    BREAK
                    
                CASE SYNC_MICHAEL3_TO_AGENCY_HEIST
                    SyncIDName = "SYNC_MICHAEL3_TO_AGENCYH"
                    BREAK
					
				CASE SYNC_MICHAEL4_TO_FRANKLIN2
					SyncIDName = "SYNC_MICHAEL4_TO_FRANKLIN2"
                    BREAK
					
				CASE SYNC_MICHAEL4_TO_SOLOMON3
                    SyncIDName = "SYNC_MICH4_TO_SOLOMON3"
                    BREAK
					
				CASE SYNC_MIKE_EV_T_TO_FAMILY6
                    SyncIDName = "SYNC_MIKE_EV_J_TO_FAM6"
                    BREAK
					
				CASE SYNC_MIKE_EV_J_TO_MICHAEL4
                    SyncIDName = "SYNC_MIKE_EV_J_TO_MIC4"
                    BREAK

				CASE SYNC_RURAL_HEIST2_TO_EXILE2
                    SyncIDName = "SYNC_RURALH2_TO_EXILE2"
                    BREAK
					
				CASE SYNC_SHRINK4_TO_EXILE1
                    SyncIDName = "SYNC_SHRINK4_TO_EXILE1"
                    BREAK
					
				CASE SYNC_SHRINK2_TO_FAMILY4
                    SyncIDName = "SYNC_SHRINK2_TO_FAMILY4"
                    BREAK
					
				CASE SYNC_SHRINK5_TO_FAMILY6
                    SyncIDName = "SYNC_SHRINK5_TO_FAMILY6"
                    BREAK
					
				CASE SYNC_SHRINK3_TO_SOLOMON1
                    SyncIDName = "SYNC_SHRINK3_TO_SOLOMON1"
                    BREAK
					
				CASE SYNC_SHRINK5_TO_SOLOMON3
                    SyncIDName = "SYNC_SHRINK5_TO_SOLOMON3"
                    BREAK
                    
				CASE SYNC_SHRINK5_TO_MICHAEL3
                    SyncIDName = "SYNC_SHRINK5_TO_MICHAEL3"
                    BREAK
                    
                CASE SYNC_SOLOMON2_TO_MICHAEL2
                    SyncIDName = "SYNC_SOLOMON2_TO_MICHAEL2"
                    BREAK
					
				CASE SYNC_SOLOMON3_TO_AGENCYH2
					SyncIDName = "SYNC_SOLOMON3_TO_AGENCYH2"
					BREAK
					
				CASE SYNC_SOLOMON3_TO_FAMILY6
					SyncIDName = "SYNC_SOLOMON3_TO_FAMILY6"
					BREAK
					
				CASE SYNC_TREVOR2_TO_CHINESE1_CALL
                    SyncIDName = "SYNC_TREVOR2_TO_CHINESE1C"
                    BREAK
                                   
                CASE SYNC_TREVOR3_TO_CHINESE2
                    SyncIDName = "SYNC_TREVOR3_TO_CHINESE2"
                    BREAK
					
				CASE SYNC_TREVOR4_TO_FBI_OFF5
                    SyncIDName = "SYNC_TREVOR4_TO_FBIO5"
                    BREAK
					
                DEFAULT
                    SCRIPT_ASSERT("Register_Array_Of_SyncIDs - case missing for a syncID")
                    SyncIDName = "SyncIDDetails"
                    SyncIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_BOOL_TO_SAVE(paramSyncIDsArray[tempLoop], SyncIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#endif
/// PURPOSE:
///    Save the Synchronisation IDs array
/// INPUT PARAMS:
///    paramSyncIDsArray        The instance of the Array of SyncIDs to be saved
///    paramNameOfArray         The name of this instance of the SyncIDs Array
#if USE_CLF_DLC
PROC Register_Array_Of_SyncIDs_CLF(BOOL &paramSyncIDsArray[MAX_SYNCHRONISATION_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramSyncIDsArray, SIZE_OF(paramSyncIDsArray), paramNameOfArray)

        TEXT_LABEL_31 SyncIDName
        
        REPEAT MAX_SYNCHRONISATION_IDS tempLoop
            SYNCHRONIZATION_IDS loopAsEnum = INT_TO_ENUM(SYNCHRONIZATION_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
			
				CASE SYNC_IAA_FINALE
		            SyncIDName = "SYNC_IAA_FINALE"
					BREAK	
					
				CASE SYNC_KOR_FINALE
		            SyncIDName = "SYNC_KOR_FINALE"
					BREAK	
					
				CASE SYNC_ARA_FINALE
		            SyncIDName = "SYNC_ARA_FINALE"
					BREAK	
					
				CASE SYNC_A14_FINALE
		            SyncIDName = "SYNC_A14_FINALE"
					BREAK	
					
				CASE SYNC_KOR_PROTECT
		            SyncIDName = "SYNC_KOR_PROTECT"
					BREAK	
					
				CASE SYNC_KOR_SUBBASE
		            SyncIDName = "SYNC_KOR_SUBBASE"
					BREAK		
					
				CASE SYNC_RUS_JETPACK
		            SyncIDName = "SYNC_RUS_JETPACK"
					BREAK	
					
				CASE SYNC_RUS_END
		            SyncIDName = "SYNC_RUS_END"
					BREAK	
					
				CASE SYNC_PAPERMAN
					SyncIDName = "SYNC_PAPERMAN"
					BREAK
					
				CASE SYNC_CAS_PREP2
					SyncIDName = "SYNC_CAS_PREP2"
					BREAK
					
				CASE SYNC_CAS_PREP3
					SyncIDName = "SYNC_CAS_PREP3"
					BREAK
				
				CASE SYNC_CAS_HEIST
					SyncIDName = "SYNC_CAS_HEIST"
					BREAK
					
				CASE SYNC_MIL_DAM
					SyncIDName = "SYNC_MIL_DAM"
					BREAK
					
				CASE SYNC_MIL_PLA
					SyncIDName = "SYNC_MIL_PLA"
					BREAK
					
				CASE SYNC_IAA_RTS
					SyncIDName = "SYNC_IAA_RTS"
					BREAK
					
			    DEFAULT
                    SyncIDName = "SyncIDDetails"
                    SyncIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_BOOL_TO_SAVE(paramSyncIDsArray[tempLoop], SyncIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Array_Of_SyncIDs_NRM(BOOL &paramSyncIDsArray[MAX_SYNCHRONISATION_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramSyncIDsArray, SIZE_OF(paramSyncIDsArray), paramNameOfArray)

        TEXT_LABEL_31 SyncIDName
        
        REPEAT MAX_SYNCHRONISATION_IDS tempLoop
            SYNCHRONIZATION_IDS loopAsEnum = INT_TO_ENUM(SYNCHRONIZATION_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
				
				CASE SYNC_SURJIMMY_WAITFOR_RADIOA	
					SyncIDName = "SYNC_SURJIMMY_WAITFOR_RADIOA"
                    BREAK	
				CASE SYNC_RADIOB_WAITFOR_SUR_JIMMY	
					SyncIDName = "SYNC_RADIOB_WAITFOR_SUR_JIMMY"
                    BREAK	
			    DEFAULT
                    SyncIDName = "SyncIDDetails"
                    SyncIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_BOOL_TO_SAVE(paramSyncIDsArray[tempLoop], SyncIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
/// PURPOSE:
///    Save the Flag IDs array
/// INPUT PARAMS:
///    paramFlagIDsArray        The instance of the Array of FlagIDs to be saved
///    paramNameOfArray         The name of this instance of the FlagIDs Array
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Array_Of_FlagIDs(BOOL &paramFlagIDsArray[MAX_FLOW_FLAG_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramFlagIDsArray, SIZE_OF(paramFlagIDsArray), paramNameOfArray)

        TEXT_LABEL_31 FlagIDName
        
        REPEAT MAX_FLOW_FLAG_IDS tempLoop
            FLOW_FLAG_IDS loopAsEnum = INT_TO_ENUM(FLOW_FLAG_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE FLOWFLAG_HEIST_BOARD_UNDO
					FlagIDName = "FLAG_H_BOARD_UNDO"
					BREAK
			
                //Heist completion flow flags.      
                CASE FLOWFLAG_HEIST_FINISHED_DOCKS
                    FlagIDName = "FLAG_H_FIN_DOCKS"
                    BREAK
                    
                CASE FLOWFLAG_HEIST_FINISHED_AGENCY
                    FlagIDName = "FLAG_H_FIN_AGENCY"
                    BREAK
                    
                CASE FLOWFLAG_HEIST_FINISHED_JEWEL
                    FlagIDName = "FLAG_H_FIN_JEWEL"
                    BREAK
                    
                CASE FLOWFLAG_HEIST_FINISHED_RURAL_BANK
                    FlagIDName = "FLAG_H_FIN_RURALBANK"
                    BREAK

                CASE FLOWFLAG_HEIST_FINISHED_FINALE
                    FlagIDName = "FLAG_H_FIN_FINALE"
                    BREAK
					
                    
                //Agency Heist.
				CASE FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION	
					FlagIDName = "FLAG_H_AGENCY_PRIME_BOARD"
                    BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE
                    FlagIDName = "FLAG_H_AGENCY_DO_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE
                    FlagIDName = "FLAG_H_AGENCY_LOAD_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_CALLS_COMPLETE
					FlagIDName = "FLAG_H_AGENCY_CALLS_COMPLETE"
					BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_2_UNLOCKED
					FlagIDName = "FLAG_H_AGENCY_2_UNLOCKED"
					BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_2_AUTOTRIGGERED
					FlagIDName = "FLAG_H_AGENCY_2_AUTOTRIG"
					BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_FIRETRUCK_GOT_WANTED
					FlagIDName = "FLAG_H_AGENCY_FT_GOT_WANTED"
					BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_PREP_2_DONE
					FlagIDName = "FLAG_H_AGENCY_P_2_DONE"
					BREAK
					
				CASE FLOWFLAG_HEIST_AGENCY_PREP_CALLS_DONE
					FlagIDName = "FLAG_H_AGENCY_P_CALLS_DONE"
					BREAK
					

				//Docks Heist.
				CASE FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION
                    FlagIDName = "FLAG_H_DOCKS_PRIME_BOARD"
                    BREAK
                    
                CASE FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE
                    FlagIDName = "FLAG_H_DOCKS_DO_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE
                    FlagIDName = "FLAG_H_DOCKS_LOAD_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_DOCKS_PRE_HEIST_TEXTS_DONE
                    FlagIDName = "FLAG_DOCKS_PRE_H_TEXTS_DONE"
                    BREAK
					
					
				//Finale Heist.
				CASE FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION
					FlagIDName = "FLAG_H_FINALE_PRIME_BOARD"
                    BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE
					FlagIDName = "FLAG_H_FINALE_LOAD_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE
					FlagIDName = "FLAG_H_FINALE_DO_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_PREP_GOT_WANTED
					FlagIDName = "FLAG_H_FINALE_P_GOT_WANTED"
					BREAK

				CASE FLOWFLAG_HEIST_FINALE_PREP_A_CALLS_DONE
					FlagIDName = "FLAG_H_FINALE_PA_CALLS_DONE"
					BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_PREP_B_CALLS_DONE
					FlagIDName = "FLAG_H_FINALE_PB_CALLS_DONE"
					BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_PREPC_EMAIL_DONE
					FlagIDName = "FLAG_H_FINALE_PC_EMAIL_DONE"
					BREAK

				CASE FLOWFLAG_HEIST_FINALE_PREPC_CAR_1_STOLEN_MAP
					FlagIDName = "FLAG_H_FINALE_PC_1_STOLEN"
					BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_PREPC_CAR_2_STOLEN_MAP
					FlagIDName = "FLAG_H_FINALE_PC_2_STOLEN"
					BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_PREPC_CAR_3_STOLEN_MAP
					FlagIDName = "FLAG_H_FINALE_PC_3_STOLEN"
					BREAK
					
				CASE FLOWFLAG_HEIST_FINALE_PREPE_DONE
					FlagIDName = "FLAG_H_FINALE_P_E_DONE"
					BREAK

				CASE FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR
					FlagIDName = "FLAG_H_FINALE_P_E_PLACE"
					BREAK

				CASE FLOWFLAG_HEIST_FINALE_2_READY
					FlagIDName = "FLAG_H_FINALE_2_READY"
					BREAK
					
                //Jewelery Heist.
				CASE FLOWFLAG_HEIST_JEWEL_PREP2A_READY
					FlagIDName = "FLAG_H_JEWEL_P2A_R"
                    BREAK
					
				CASE FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION
                    FlagIDName = "FLAG_H_JEWEL_PRIME_BOARD"
                    BREAK
                    
                CASE FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE
                    FlagIDName = "FLAG_H_JEWEL_DO_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE
                    FlagIDName = "FLAG_H_JEWEL_LOAD_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_JEWEL_RUN
                    FlagIDName = "FLAG_H_JEWEL_RUN"
                    BREAK
										
				//Rural Bank Heist.
				CASE FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING
					FlagIDName = "FLAG_H_RURAL_LOAD_MIKE_WIN"
                    BREAK
					
				CASE FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING
					FlagIDName = "FLAG_H_RURAL_LOAD_TREV_WIN"
                    BREAK
					
				CASE FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE
					FlagIDName = "FLAG_H_RURAL_LOAD_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE
					FlagIDName = "FLAG_H_RURAL_DO_B_EXIT"
                    BREAK
					
				CASE FLOWFLAG_HEIST_RURAL_PREP_CALL_DONE
					FlagIDName = "FLAG_H_RURAL_PREP_CALL_DONE"
					BREAK
					
                //Random Events.
                CASE FLOWFLAG_ALLOW_RANDOM_EVENTS
                    FlagIDName = "FLAG_ALLOW_RANDOM_EVENTS"
                    BREAK

				//Activity unlocking.
				CASE FLOWFLAG_ALLOW_CINEMA_ACTIVITY
                    FlagIDName = "FLAG_ALLOW_CINEMA"
                    BREAK
					
				CASE FLOWFLAG_ALLOW_COMEDYCLUB_ACTIVITY
                    FlagIDName = "FLAG_ALLOW_COMEDYCLUB"
                    BREAK
					
				CASE FLOWFLAG_ALLOW_LIVEMUSIC_ACTIVITY
                    FlagIDName = "FLAG_ALLOW_LIVEMUSIC"
                    BREAK
					
				CASE FLOWFLAG_ALLOW_SHOP_ROBBERIES
                    FlagIDName = "FLAG_ALLOW_SHOP_ROBBERIES"
                    BREAK
                    
                //Player timetables.
                CASE FLOWFLAG_BLOCK_SAVEHOUSE_FOR_AUTOSAVE
                    FlagIDName = "FLOW_BLOCK_SH_FOR_SAVE"
                    BREAK
                    
                CASE FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY
                    FlagIDName = "FLAG_T_RESTRICT_COUNTRY"
                    BREAK
                    
                CASE FLOWFLAG_TREVOR_RESTRICTED_TO_CITY
                    FlagIDName = "FLAG_T_RESTRICT_CITY"
                    BREAK
                    
                
                // Miscellaneous	
				CASE FLOWFLAG_FRANKLIN_FROZEN_POST_PROLOGUE
					FlagIDName = "FLOWFLAG_F_FROZEN_POST_PRO"
					BREAK
				
				CASE FLOWFLAG_CHOP_THE_DOG_UNLOCKED
                    FlagIDName = "FLAG_CHOP_DOG_UNLOCKED"
                    BREAK
					
				CASE FLOWFLAG_CARSTEAL3_INITIALISED
                    FlagIDName = "FLAG_CAR3_INITIALISED"
                    BREAK
					
				CASE FLOWFLAG_MOVIE_STUDIO_OPEN
                    FlagIDName = "FLAG_MOVIE_STUDIO_OPEN"
                    BREAK
					
				CASE FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN
                    FlagIDName = "FLAG_MOVIE_STUDIO_OPEN_FRAN"
                    BREAK
					
				CASE FLOWFLAG_AIR_VEHICLE_PARACHUTE_UNLOCKED
					FlagIDName = "FLAG_AIR_VEH_PARA_UNLOCKED"
					BREAK
					
				CASE FLOWFLAG_WATER_VEHICLE_SCUBA_GEAR_UNLOCKED
					FlagIDName = "FLAG_WATER_VEH_SCUBA_UNLOCKED"
					BREAK
					
				CASE FLOWFLAG_OFFROAD_RACING_UNLOCKED_FOR_ALL
					FlagIDName = "FLAG_OFFR_RACE_UNLOCK_ALL"
					BREAK
					
				CASE FLOWFLAG_RUN_BUILDINGSITE_AMBIENCE_AUDIO
					FlagIDName = "FLAG_RUN_BUILDSITE_AMB_AUD"
					BREAK
					
				CASE FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS
					FlagIDName = "FLAG_MICHAEL_D_FLYING_LES"
					BREAK
				
				CASE FLOWFLAG_ARM1_CAR_DAMAGED
					FlagIDName = "FLOWFLAG_ARM1_CAR_DAMAGED"
					BREAK
					
				CASE FLOWFLAG_STRETCH_TEXT_SENT
					FlagIDName = "FLAG_STRETCH_TEXT_SENT"
					BREAK
					
				CASE FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE
					FlagIDName = "FLAG_ARM3_STRIPSWITCH_AVAIL"
					BREAK
					
				CASE FLOWFLAG_DARTS_YELLOW_JACK_AVAILABLE
					FlagIDName = "FLAG_DARTS_YELLOW_JACK_AV"
					BREAK
					
				CASE FLOWFLAG_RES_AND_RCS_UNLOCKED
					FlagIDName = "FLAG_RES_AND_RCS_UNLOCKED"
					BREAK
					
				CASE FLOWFLAG_EXILE1_PICKUPS_UNLOCKED
					FlagIDName = "FLAG_EXILE1_PICKUPS_UNLOCKED"
					BREAK
					
				CASE FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN
					FlagIDName = "FLAG_CAR2_KILLED_MULLIGAN"
					BREAK
					
				CASE FLOWFLAG_FRAN_DONE_ACTIVITY_WITH_LAMAR
					FlagIDName = "FLAG_FRAN_DONE_ACT_WITH_LAM"
					BREAK
					
				CASE FLOWFLAG_TREV_DONE_ACTIVITY_WITH_LAMAR
					FlagIDName = "FLAG_TREV_DONE_ACT_WITH_LAM"
					BREAK
					
				CASE FLOWFLAG_AMANDA_MICHAEL_EVENT_SKIPPED
					FlagIDName = "FLOWFLAG_AMANDA_M_EV_SKIPPED"
					BREAK
				
				CASE FLOWFLAG_SHRINK_KILLED
					FlagIDName = "FLOWFLAG_SHRINK_KILLED"
					BREAK
					
				CASE FLOWFLAG_BLOCK_FRAN_MISSIONS_FOR_TREV
					FlagIDName = "FLOWFLAG_BLOCK_F_MISSIONS_FOR_T"
					BREAK
				
				CASE FLOWFLAG_GUNRANGE_TUTORIAL_COMPLETED
					FlagIDName = "FLAG_GUNRANGE_TUTORIAL_DONE"
					BREAK
					
				CASE FLOWFLAG_WILDLIFE_PHOTOGRAPHY_UNLOCKED
					FlagIDName = "FLAG_WILD_PHOTO_UNLOCKED"
					BREAK
					
				// RCM
				CASE FLOWFLAG_BARRY3_TEXT_RECEIVED
                    FlagIDName = "FLAG_BARRY3_TEXT_RECEIVED"
                    BREAK
				
				CASE FLOWFLAG_BARRY4_TEXT_RECEIVED
                    FlagIDName = "FLAG_BARRY4_TEXT_RECEIVED"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE
					FlagIDName = "FLAG_EPSILON_QUESTIONNAIRE_DONE"
                    BREAK

				CASE FLOWFLAG_EPSILON_DONATED_500
					FlagIDName = "FLAG_EPSILON_DONATED_500"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_DONATED_5000
					FlagIDName = "FLAG_EPSILON_DONATED_5000"
                    BREAK
				
				CASE FLOWFLAG_EPSILON_DONATED_10000
					FlagIDName = "FLAG_EPSILON_DONATED_10000"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_CARS_DONE
					FlagIDName = "FLAG_EPSILON_CARS_DONE"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_ROBES_BOUGHT
					FlagIDName = "FLAG_EPSILON_ROBES_BOUGHT"
                    BREAK

				CASE FLOWFLAG_EPSILON_ROBES_DONE
					FlagIDName = "FLAG_EPSILON_ROBES_DONE"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_6_TEXT_RECEIVED
					FlagIDName = "FLAG_EPSILON_6_TEXT_RECEIVED"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_DESERT_DONE
					FlagIDName = "FLAG_EPSILON_DESERT_DONE"
                    BREAK
					
				CASE FLOWFLAG_EPSILON_UNLOCKED_TRACT
					FlagIDName = "FLAG_EPSILON_UNLOCKED_TRACT"
                    BREAK
				
				CASE FLOWFLAG_EXTREME2_TEXT_RECEIVED
					FlagIDName = "FLAG_EXTREME2_TEXT_RECEIVED"
                    BREAK
					
				CASE FLOWFLAG_EXTREME3_TEXT_RECEIVED
					FlagIDName = "FLAG_EXTREME3_TEXT_RECEIVED"
                    BREAK
					
				CASE FLOWFLAG_EXTREME4_BJUMPS_FINISHED
					FlagIDName = "FLAG_EXTREME4_BJUMPS_FINISHED"
                    BREAK
				
				CASE FLOWFLAG_HUNTING1_TEXT_RECEIVED
                    FlagIDName = "FLAG_HUNTINTG1_TEXT_RECEIVED"
                    BREAK
					
				CASE FLOWFLAG_NIGEL1_EMAIL_RECEIVED
                    FlagIDName = "FLAG_NIGEL1_EMAIL_RECEIVED"
                    BREAK
				
				CASE FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED
					FlagIDName = "FLAG_NIGEL3_NAPOLI_KILLED"
					BREAK
					
				CASE FLOWFLAG_PAPARAZZO3_TEXT_RECEIVED
                    FlagIDName = "FLAG_PAPARAZZO3_TEXT_RECEIVED"
					BREAK
				
				CASE FLOWFLAG_TONYA3_TEXT_RECEIVED
                    FlagIDName = "FLAG_TONYA3_TEXT_RECEIVED"
                    BREAK

				CASE FLOWFLAG_TONYA4_TEXT_RECEIVED
                    FlagIDName = "FLAG_TONYA4_TEXT_RECEIVED"
                    BREAK
                
				CASE FLOWFLAG_FOR_SALE_SIGNS_DESTROYED
                    FlagIDName = "FLAG_FOR_SALE_SIGNS_DESTROYED"
                    BREAK
					
				CASE FLOWFLAG_LETTER_SCRAPS_DONE
					FlagIDName = "FLAG_LETTER_SCRAPS_DONE"
					BREAK
				CASE FLOWFLAG_SPACESHIP_PARTS_DONE
					FlagIDName = "FLAG_SPACESHIP_PARTS_DONE"
					BREAK
				
				CASE FLOWFLAG_ALL_RAMPAGES_UNLOCKED
					FlagIDName = "FLAG_ALL_RAMPAGES_UNLOCKED"
					BREAK
					
				CASE FLOWFLAG_PURCHASED_MARINA_PROPERTY
					FlagIDName = "FLAG_PURCHASED_MARINA_PROPERTY"
					BREAK
					
                // Player Controls
                CASE FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE
                    FlagIDName = "FLAG_PLAYER_VEH_F_UNLOCK_BIKE"
                    BREAK
                    
                CASE FLOWFLAG_PLAYER_VEH_T_UNLOCK_RASP_JAM
                    FlagIDName = "FLAG_PLAYER_VEH_T_UNLOCK_RJAM"
                    BREAK
					
				CASE FLOWFLAG_PLAYER_PED_INTRODUCED_M
                    FlagIDName = "FLAG_PLAYER_PED_INTRODUCED_M"
                    BREAK
                    
                CASE FLOWFLAG_PLAYER_PED_INTRODUCED_F
                    FlagIDName = "FLAG_PLAYER_PED_INTRODUCED_F"
                    BREAK
                    
                CASE FLOWFLAG_PLAYER_PED_INTRODUCED_T
                    FlagIDName = "FLAG_PLAYER_PED_INTRODUCED_T"
                    BREAK
					
				CASE FLOWFLAG_MIC_HAS_HAGGARD_SUIT
                    FlagIDName = "FLAG_MIC_HAS_HAGGARD_SUIT"
                    BREAK
					
				CASE FLOWFLAG_MIC_SET_HAGGARD_SUIT
                    FlagIDName = "FLAG_MIC_SET_HAGGARD_SUIT"
					BREAK
					
				CASE FLOWFLAG_MIC_REM_HAGGARD_SUIT
                    FlagIDName = "FLAG_MIC_REM_HAGGARD_SUIT"
					BREAK
				
					
				CASE FLOWFLAG_MIC_HIDE_BARE_CHEST
					FlagIDName = "FLOW_MIC_HIDE_BARE_CHEST"
                    BREAK
					
				CASE FLOWFLAG_MIC_PRO_MASK_REMOVED
					FlagIDName = "FLOW_MIC_PRO_MASK_REMOVED"
                    BREAK
					
				CASE FLOWFLAG_TRV_PRO_MASK_REMOVED
					FlagIDName = "FLOW_TRV_PRO_MASK_REMOVED"
                    BREAK
					
					
				//Lamar Strand
				CASE FLOWFLAG_UNLOCK_LAMAR_1
					FlagIDName = "FLAG_UNLOCK_LAMAR_1"
					BREAK
				//Franklin	
				CASE FLOWFLAG_MISSION_FRANK1_MC_CLIPPED	
					FlagIDName = "FLAG_FRANK1_MC_CLIPPED"
					BREAK
					
				//FBI Officers Strand
				CASE FLOWFLAG_MISSION_FBI_3_CALLS_DONE
					FlagIDName = "FLAG_FBI_3_CALLS_DONE"
					BREAK

				CASE FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED
					FlagIDName = "FLAG_FBI_4_PREP_3_COMPLETED"
					BREAK
					
				CASE FLOWFLAG_MISSION_FBI_4_UNLOCKED_FROM_PREP
					FlagIDName = "FLAG_FBI_4_UNLOCKED_FROM_P"
					BREAK
					
				CASE FLOWFLAG_MISSION_FBI_4_CALLS_DONE
					FlagIDName = "FLAG_FBI_4_CALLS_DONE"
					BREAK
					
				CASE FLOWFLAG_ASS1_UNLOCKED	
					FlagIDName = "FLAG_ASS_1_UNLOCKED"
					BREAK
					
				//finale strand
				CASE FLOWFLAG_ASS_COMPLETED
					FlagIDName = "FLAG_ASS_COMPLETED"
					BREAK
					
				//Michael Strand.
				CASE FLOWFLAG_MISSION_MICHAEL_4_TEXTS_DONE
					FlagIDName = "FLAG_MICHAEL_4_TEXTS_DONE"
					BREAK
					
					
				//Martin Strand.
				CASE FLOWFLAG_MISSION_MARTIN_1_CALLS_DONE
					FlagIDName = "FLAG_MARTIN_1_CALLS_DONE"
					BREAK


				//Key flow stages.
				CASE FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT
					FlagIDName = "FLAG_F_MOVED_TO_HILLS_APART"
					BREAK
				
				CASE FLOWFLAG_MICHAEL_FRANKLIN_ARE_FRIENDS
					FlagIDName = "FLAG_M_F_ARE_FRIENDS"
					BREAK
				
				CASE FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT
					FlagIDName = "FLAG_M_A_HAVE_SPLIT"
					BREAK
				
				CASE FLOWFLAG_MICHAEL_TREVOR_HAVE_FALLEN_OUT
					FlagIDName = "FLAG_M_A_HAVE_FALLEN_OUT"
					BREAK
				
				CASE FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED
					FlagIDName = "FLAG_M_T_EXILE_STARTED"
					BREAK
				
				CASE FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED
					FlagIDName = "FLAG_M_T_EXILE_FINISHED"
					BREAK
					
				CASE FLOWFLAG_ORTEGA_KILLED
					FlagIDName = "FLAG_ORTEGA_KILLED"
					BREAK
					
				CASE FLOWFLAG_GAME_100_PERCENT_COMPLETE
					FlagIDName = "FLAG_GAME_100_PERC_COMP"
					BREAK
					
				CASE FLOWFLAG_FINAL_CHOICE_MADE
					FlagIDName = "FLAG_FINAL_CHOICE_MADE"
					BREAK
					
				CASE FLOWFLAG_MICHAEL_KILLED
					FlagIDName = "FLAG_MICHAEL_KILLED"
					BREAK
					
				CASE FLOWFLAG_TREVOR_KILLED
					FlagIDName = "FLAG_TREVOR_KILLED"
					BREAK
					
				CASE FLOWFLAG_RESPAWNED_AFTER_FINALE
					FlagIDName = "FLAG_RESPAWNED_AFTER_FIN"
					BREAK
					
				CASE FLOWFLAG_DIVING_SCRAPS_DONE
					FlagIDName = "FLAG_DIVING_SCRAPS_DONE"
					BREAK
					
				CASE FLOWFLAG_PLAYER_HAS_USED_FP_VIEW
					FlagIDName = "FLAG_PLAYER_HAS_USED_FP_VIEW"
					BREAK
					
				CASE FLOWFLAG_MIKE_CON_BOUGHT
					FlagIDName = "FLAG_MIKE_CON_BOUGHT"
					BREAK
					
				CASE FLOWFLAG_FRANK_CON_BOUGHT
					FlagIDName = "FLAG_FRANK_CON_BOUGHT"
					BREAK
					
				CASE FLOWFLAG_TREV_CON_BOUGHT
					FlagIDName = "FLAG_TREV_CON_BOUGHT"
					BREAK
					
				CASE FLOWFLAG_BEVERLY_SENT_WILDLIFE_TEXT
					FlagIDName = "FLAG_BEV_SENT_WILD_TXT"
					BREAK
					
				CASE FLOWFLAG_SENT_PROXIMITY_MINE_EMAIL
					FlagIDName = "FLAG_SENT_PROX_MINE_EMAIL"
					BREAK
					
				CASE FLOWFLAG_SENT_HOMING_MISSILE_EMAIL
					FlagIDName = "FLAG_SENT_MISSILE_EMAIL"
					BREAK
					
				CASE FLOWFLAG_SENT_COMBAT_PDW_EMAIL
					FlagIDName = "FLAG_SENT_DLC_WEAPON_EMAIL_1"
					BREAK
					
				CASE FLOWFLAG_SENT_KNUCKLE_DUSTER_EMAIL
					FlagIDName = "FLAG_SENT_DLC_WEAPON_EMAIL_2"
					BREAK

				CASE FLOWFLAG_SENT_MARKSMAN_PISTOL_EMAIL
					FlagIDName = "FLAG_SENT_DLC_WEAPON_EMAIL_3"
					BREAK
					
				CASE FLOWFLAG_SENT_MACHETE_EMAIL
					FlagIDName = "FLAG_SENT_DLC_WEAPON_EMAIL_4"
					BREAK
					
				CASE FLOWFLAG_SENT_MACHINE_PISTOL_EMAIL
					FlagIDName = "FLAG_SENT_DLC_WEAPON_EMAIL_5"
					BREAK
					
                DEFAULT
                    SCRIPT_ASSERT("Register_Array_Of_FlagIDs - case missing for a flagID")
                    FlagIDName = "FlagIDDetails"
                    FlagIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_BOOL_TO_SAVE(paramFlagIDsArray[tempLoop], FlagIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY() 
ENDPROC
#endif
#endif
/// PURPOSE:
///    Save the Flag IDs array
/// INPUT PARAMS:
///    paramFlagIDsArray        The instance of the Array of FlagIDs to be saved
///    paramNameOfArray         The name of this instance of the FlagIDs Array
#if USE_CLF_DLC
PROC Register_Array_Of_FlagIDs_CLF(BOOL &paramFlagIDsArray[MAX_FLOW_FLAG_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramFlagIDsArray, SIZE_OF(paramFlagIDsArray), paramNameOfArray)

        TEXT_LABEL_31 FlagIDName
        
        REPEAT MAX_FLOW_FLAG_IDS tempLoop
            FLOW_FLAG_IDS loopAsEnum = INT_TO_ENUM(FLOW_FLAG_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE FLOWFLAG_NONE
					FlagIDName = "FLOWFLAG_NONE"
					BREAK
			
                //Heist completion flow flags.      
                CASE FLOWFLAG_CLF01
                    FlagIDName = "FLOWFLAG_CLF01"
                    BREAK
                 	
                DEFAULT
                    FlagIDName = "FlagIDDetails"
                    FlagIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_BOOL_TO_SAVE(paramFlagIDsArray[tempLoop], FlagIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY() 
ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Array_Of_FlagIDs_NRM(BOOL &paramFlagIDsArray[MAX_FLOW_FLAG_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramFlagIDsArray, SIZE_OF(paramFlagIDsArray), paramNameOfArray)

        TEXT_LABEL_31 FlagIDName
        
        REPEAT MAX_FLOW_FLAG_IDS tempLoop
            FLOW_FLAG_IDS loopAsEnum = INT_TO_ENUM(FLOW_FLAG_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
				CASE FLOWFLAG_NONE
					FlagIDName = "FLOWFLAG_NONE"
					BREAK
			      
                CASE FLOWFLAG_NRM_START
                    FlagIDName = "FLOWFLAG_NRM_START"
                    BREAK
                 	
                DEFAULT
                    FlagIDName = "FlagIDDetails"
                    FlagIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_BOOL_TO_SAVE(paramFlagIDsArray[tempLoop], FlagIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY() 
ENDPROC
#endif
/// PURPOSE:
///    Save the Int IDs array
/// INPUT PARAMS:
///    paramIntIDsArray         The instance of the Array of IntIDs to be saved
///    paramNameOfArray         The name of this instance of the IntIDs Array
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Array_Of_IntIDs(INT &paramIntIDsArray[MAX_FLOW_INT_IDS], STRING paramNameOfArray)

    INT tempLoop = 0

    START_SAVE_ARRAY_WITH_SIZE(paramIntIDsArray, SIZE_OF(paramIntIDsArray), paramNameOfArray)

        TEXT_LABEL_31 IntIDName
        
        REPEAT MAX_FLOW_INT_IDS tempLoop
            FLOW_INT_IDS loopAsEnum = INT_TO_ENUM(FLOW_INT_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
                CASE FLOWINT_HEIST_MISSION_TRIGGER_HEIST
                    IntIDName = "INT_H_TRIGGER_HEIST"
                    BREAK
                    
                CASE FLOWINT_HEIST_MISSION_TRIGGER_MISSION
                    IntIDName = "INT_H_TRIGGER_MISSION"
                    BREAK
                    
                CASE FLOWINT_HEIST_BOARD_MODE_JEWEL
                    IntIDName = "INT_H_BOARD_M_JEWEL"
                    BREAK
                    
                CASE FLOWINT_HEIST_BOARD_MODE_DOCKS
                    IntIDName = "INT_H_BOARD_M_DOCKS"
                    BREAK
                    
                CASE FLOWINT_HEIST_BOARD_MODE_RURAL
                    IntIDName = "INT_H_BOARD_M_RURAL"
                    BREAK
                    
                CASE FLOWINT_HEIST_BOARD_MODE_AGENCY
                    IntIDName = "INT_H_BOARD_M_AGENCY"
                    BREAK
                    
                CASE FLOWINT_HEIST_BOARD_MODE_FINALE
                    IntIDName = "INT_H_BOARD_M_FINALE"
                    BREAK
                    
                CASE FLOWINT_HEIST_CHOICE_JEWEL
                    IntIDName = "INT_HEIST_CHOICE_JEWEL"
                    BREAK
                    
                CASE FLOWINT_HEIST_CHOICE_DOCKS
                    IntIDName = "INT_HEIST_CHOICE_DOCKS"
                    BREAK
                    
                CASE FLOWINT_HEIST_CHOICE_RURAL
                    IntIDName = "INT_HEIST_CHOICE_RURAL"
                    BREAK
                    
                CASE FLOWINT_HEIST_CHOICE_AGENCY
                    IntIDName = "INT_HEIST_CHOICE_AGENCY"
                    BREAK
                    
                CASE FLOWINT_HEIST_CHOICE_FINALE
                    IntIDName = "INT_HEIST_CHOICE_FINALE"
                    BREAK
					
				CASE FLOWINT_MISSION_CHOICE_FINALE
                    IntIDName = "INT_MISS_CHOICE_FINALE"
                    BREAK

                DEFAULT
                    SCRIPT_ASSERT("Register_Array_Of_IntIDs - case missing for a IntID")
                    IntIDName = "IntIDDetails"
                    IntIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_INT_TO_SAVE(paramIntIDsArray[tempLoop], IntIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#endif
/// PURPOSE:
///    Save the Int IDs array
/// INPUT PARAMS:
///    paramIntIDsArray         The instance of the Array of IntIDs to be saved
///    paramNameOfArray         The name of this instance of the IntIDs Array
#if USE_CLF_DLC
PROC Register_Array_Of_IntIDs_CLF(INT &paramIntIDsArray[MAX_FLOW_INT_IDS], STRING paramNameOfArray)

    INT tempLoop = 0

    START_SAVE_ARRAY_WITH_SIZE(paramIntIDsArray, SIZE_OF(paramIntIDsArray), paramNameOfArray)

        TEXT_LABEL_31 IntIDName
        
        REPEAT MAX_FLOW_INT_IDS tempLoop
            FLOW_INT_IDS loopAsEnum = INT_TO_ENUM(FLOW_INT_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
                CASE FLOWINT_NONE
                    IntIDName = "FLOWINT_NONE"
                    BREAK
					
                CASE FLOWINT_CLF_JETPACK_LEVEL
                    IntIDName = "FLOWINT_CLF_JETPACK_LEVEL"
                    BREAK
					
				CASE MAX_FLOW_INT_IDS_CLF
                    IntIDName = "MAX_FLOW_INT_IDS_CLF"
                    BREAK
					
                DEFAULT
                    IntIDName = "IntIDDetails"
                    IntIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_INT_TO_SAVE(paramIntIDsArray[tempLoop], IntIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Array_Of_IntIDs_NRM(INT &paramIntIDsArray[MAX_FLOW_INT_IDS], STRING paramNameOfArray)

    INT tempLoop = 0

    START_SAVE_ARRAY_WITH_SIZE(paramIntIDsArray, SIZE_OF(paramIntIDsArray), paramNameOfArray)

        TEXT_LABEL_31 IntIDName
        
        REPEAT MAX_FLOW_INT_IDS tempLoop
            FLOW_INT_IDS loopAsEnum = INT_TO_ENUM(FLOW_INT_IDS, tempLoop)
            
            SWITCH (loopAsEnum)
                CASE FLOWINT_NONE
                    IntIDName = "FLOWINT_NRMTEST"
                    BREAK
					
                CASE MAX_FLOW_INT_IDS_NRM
                    IntIDName = "MAX_FLOW_INT_IDS_NRM"
                    BREAK
					
                DEFAULT
                    IntIDName = "IntIDDetails"
                    IntIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_INT_TO_SAVE(paramIntIDsArray[tempLoop], IntIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
/// PURPOSE:
///    Save the Bitset IDs array
/// INPUT PARAMS:
///    paramBitsetIDsArray      The instance of the Array of BitsetIDs to be saved
///    paramNameOfArray         The name of this instance of the BitsetIDs Array
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Array_Of_BitsetIDs(INT &paramBitsetIDsArray[MAX_FLOW_BITSET_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramBitsetIDsArray, SIZE_OF(paramBitsetIDsArray), paramNameOfArray)

        TEXT_LABEL_31 BitsetIDName
        
        REPEAT MAX_FLOW_BITSET_IDS tempLoop
            FLOW_BITSET_IDS loopAsEnum = INT_TO_ENUM(FLOW_BITSET_IDS, tempLoop)
            
            SWITCH (loopAsEnum) 	
				CASE FLOWBITSET_MINIGAME_ACTIVE
					BitSetIDName = "BITS_MINIGAME_ACTIVE"
					BREAK
			
                CASE FLOWBITSET_HEIST_DISABLE_BOARD_EXIT
                    BitsetIDName = "BITS_H_DISABLE_BOARD_EXIT"
                    BREAK
                    
                CASE FLOWBITSET_HEIST_TOGGLE_BOARD
                    BitsetIDName = "BITS_H_TOGGLE_BOARD"
                    BREAK
					
				CASE FLOWBITSET_HEIST_POI_OVERVIEW_DONE
                    BitsetIDName = "BITS_H_POI_OVERVIEW_DONE"
                    BREAK
                                            
                CASE FLOWBITSET_HEIST_CREW_SELECTED
                    BitsetIDName = "BITS_H_CREW_SELECTED"
                    BREAK
					
				CASE FLOWBITSET_HEIST_CHOICE_SELECTED
                    BitsetIDName = "BITS_H_CHOICE_SELECTED"
                    BREAK
                
                CASE FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT
                    BitsetIDName = "BITS_FR_CHANGED_HAIRCUT"
                    BREAK
                
                CASE FLOWBITSET_FRANKLIN_CHANGED_OUTFIT
                    BitsetIDName = "BITS_FR_CHANGED_OUTFIT"
                    BREAK
                
                CASE FLOWBITSET_MICHAEL_VISITED_GYM
                    BitsetIDName = "BITS_MI_VISITED_GYM"
                    BREAK
                    
                CASE FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1
                    BitsetIDName = "BITS_RST_FLOWLAUNCH_SCS1"
                    BREAK

				CASE FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2
                    BitsetIDName = "BITS_RST_FLOWLAUNCH_SCS2"
                    BREAK

                DEFAULT
                    SCRIPT_ASSERT("Register_Array_Of_BitsetIDs - case missing for a BitsetID")
                    BitsetIDName = "BitsetIDDetails"
                    BitsetIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_INT_TO_SAVE(paramBitsetIDsArray[tempLoop], BitsetIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#endif
/// PURPOSE:
///    Save the Bitset IDs array
/// INPUT PARAMS:
///    paramBitsetIDsArray      The instance of the Array of BitsetIDs to be saved
///    paramNameOfArray         The name of this instance of the BitsetIDs Array
#if USE_CLF_DLC
PROC Register_Array_Of_BitsetIDs_CLF(INT &paramBitsetIDsArray[MAX_FLOW_BITSET_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramBitsetIDsArray, SIZE_OF(paramBitsetIDsArray), paramNameOfArray)

        TEXT_LABEL_31 BitsetIDName
        
        REPEAT MAX_FLOW_BITSET_IDS tempLoop
            FLOW_BITSET_IDS loopAsEnum = INT_TO_ENUM(FLOW_BITSET_IDS, tempLoop)
            
            SWITCH (loopAsEnum) 	
				CASE FLOWBITSET_CLF_MINIGAME_ACTIVE
					BitSetIDName = "FLOWBITSET_CLF_MINIGAME_ACTIVE"
					BREAK
					
                DEFAULT
                    BitsetIDName = "BitsetIDDetails"
                    BitsetIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_INT_TO_SAVE(paramBitsetIDsArray[tempLoop], BitsetIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Array_Of_BitsetIDs_NRM(INT &paramBitsetIDsArray[MAX_FLOW_BITSET_IDS], STRING paramNameOfArray)

    INT tempLoop = 0
    
    START_SAVE_ARRAY_WITH_SIZE(paramBitsetIDsArray, SIZE_OF(paramBitsetIDsArray), paramNameOfArray)

        TEXT_LABEL_31 BitsetIDName
        
        REPEAT MAX_FLOW_BITSET_IDS tempLoop
            FLOW_BITSET_IDS loopAsEnum = INT_TO_ENUM(FLOW_BITSET_IDS, tempLoop)
            
            SWITCH (loopAsEnum) 	
				CASE FLOWBITSET_NRM_MINIGAME_ACTIVE
					BitSetIDName = "FLOWBITSET_NRM_MINIGAME_ACTIVE"
					BREAK
					
                DEFAULT
                    BitsetIDName = "BitsetIDDetails"
                    BitsetIDName += tempLoop
                    BREAK
            ENDSWITCH
            
            REGISTER_INT_TO_SAVE(paramBitsetIDsArray[tempLoop], BitsetIDName)
        ENDREPEAT
    STOP_SAVE_ARRAY()

ENDPROC
#endif
/// PURPOSE:
///    Register the struct containing the mission flow INTS, BITFLAGS, BOOLS, etc required to control
///         the mission flow.
/// INPUT PARAMS:
///    paramControlSavedVars        This instance of the Control Struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Struct_MissionFlowControlSavedVars(FLOW_CONTROL_SAVED_VARS &paramControlSavedVars, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramControlSavedVars, SIZE_OF(paramControlSavedVars), paramNameOfStruct)
        Register_Array_Of_SyncIDs(paramControlSavedVars.syncIDs,        "MF_CONTROL_SYNCIDS")
        Register_Array_Of_FlagIDs(paramControlSavedVars.flagIDs,        "MF_CONTROL_FLAGIDS")
        Register_Array_Of_IntIDs(paramControlSavedVars.intIDs,          "MF_CONTROL_INTIDS")
        Register_Array_Of_BitsetIDs(paramControlSavedVars.bitsetIDs,    "MF_CONTROL_BITSETIDS")
    STOP_SAVE_STRUCT()
    
ENDPROC
#endif
#endif
/// PURPOSE:
///    Register the struct containing the mission flow INTS, BITFLAGS, BOOLS, etc required to control
///         the mission flow.
/// INPUT PARAMS:
///    paramControlSavedVars        This instance of the Control Struct to be saved
///    paramNameOfStruct            The name of this instance of the struct
#if USE_CLF_DLC
PROC Register_Struct_MissionFlowControlSavedVars_CLF(FLOW_CONTROL_SAVED_VARS &paramControlSavedVars, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramControlSavedVars, SIZE_OF(paramControlSavedVars), paramNameOfStruct)
        Register_Array_Of_SyncIDs_CLF(paramControlSavedVars.syncIDs,        "MF_CONTROL_SYNCIDS")
        Register_Array_Of_FlagIDs_CLF(paramControlSavedVars.flagIDs,        "MF_CONTROL_FLAGIDS")
        Register_Array_Of_IntIDs_CLF(paramControlSavedVars.intIDs,          "MF_CONTROL_INTIDS")
        Register_Array_Of_BitsetIDs_CLF(paramControlSavedVars.bitsetIDs,    "MF_CONTROL_BITSETIDS")
    STOP_SAVE_STRUCT()
    
ENDPROC
#endif
#if USE_NRM_DLC
PROC Register_Struct_MissionFlowControlSavedVars_NRM(FLOW_CONTROL_SAVED_VARS &paramControlSavedVars, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramControlSavedVars, SIZE_OF(paramControlSavedVars), paramNameOfStruct)
        Register_Array_Of_SyncIDs_NRM(paramControlSavedVars.syncIDs,        "MF_CONTROL_SYNCIDS")
        Register_Array_Of_FlagIDs_NRM(paramControlSavedVars.flagIDs,        "MF_CONTROL_FLAGIDS")
        Register_Array_Of_IntIDs_NRM(paramControlSavedVars.intIDs,          "MF_CONTROL_INTIDS")
        Register_Array_Of_BitsetIDs_NRM(paramControlSavedVars.bitsetIDs,    "MF_CONTROL_BITSETIDS")
    STOP_SAVE_STRUCT()
    
ENDPROC
#endif
/// PURPOSE:
///    Register the struct containing the mission flow active data elements such as completion state, no of
///    times failed, and leave area flags.
/// INPUT PARAMS:
///    paramMissionActiveDataSavedStruct 	This instance of the Mission Active Data Struct to be saved
///    paramNameOfStruct            		The name of this instance of the struct
PROC Register_Struct_MissionActiveDataSaved(MISSION_SAVED_DATA &paramMissionActiveDataSavedStruct, STRING paramNameOfStruct)

    START_SAVE_STRUCT_WITH_SIZE(paramMissionActiveDataSavedStruct, SIZE_OF(paramMissionActiveDataSavedStruct), paramNameOfStruct)
		REGISTER_BOOL_TO_SAVE(paramMissionActiveDataSavedStruct.completed, "completed")
		REGISTER_INT_TO_SAVE(paramMissionActiveDataSavedStruct.missionFailsNoProgress, "missionFailsNoProgress")
		REGISTER_INT_TO_SAVE(paramMissionActiveDataSavedStruct.missionFailsTotal, "missionFailsTotal")
		REGISTER_INT_TO_SAVE(paramMissionActiveDataSavedStruct.iCompletionOrder, "iCompletionOrder")
		REGISTER_INT_TO_SAVE(paramMissionActiveDataSavedStruct.iScore, "iScore")
		REGISTER_FLOAT_TO_SAVE(paramMissionActiveDataSavedStruct.fStatCompletion, "fStatCompletion")
    STOP_SAVE_STRUCT()
    
ENDPROC


/// PURPOSE:
///    Register the array containing the active mission data structs for each mission.
/// INPUT PARAMS
///    paramActiveMissionDataSavedStructArray   The instance of the Array of Active Mission Data Structs to be saved
///    paramNameOfArray         				The name of this instance of the Strands Array
PROC Register_Array_Of_MissionActiveDataSavedStructs(MISSION_SAVED_DATA &paramActiveMissionDataSavedStructArray[MAX_MISSION_DATA_SLOTS], STRING paramNameOfArray)

    START_SAVE_ARRAY_WITH_SIZE(paramActiveMissionDataSavedStructArray, SIZE_OF(paramActiveMissionDataSavedStructArray), paramNameOfArray)

	INT index
	REPEAT MAX_MISSION_DATA_SLOTS index
		TEXT_LABEL_63 tStructName = "MF_MISSION_STRUCT_"
		tStructName += index
		Register_Struct_MissionActiveDataSaved(paramActiveMissionDataSavedStructArray[index], tStructName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up the save structure for registering all the mission flow globals that need to be saved
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Register_Flow_Saved_Globals()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sFlow, SIZE_OF(g_savedGlobals.sFlow), "FLOW_STRUCT")		
	
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sFlow.isGameflowActive, "isGameflowActive")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sFlow.flowCompleted, "flowCompleted")
    Register_Array_Of_Strands(g_savedGlobals.sFlow.strandSavedVars, "MF_STRANDS_ARRAY")
    Register_Struct_MissionFlowControlSavedVars(g_savedGlobals.sFlow.controls, "MF_CONTROLS_STRUCT")
	Register_Array_Of_MissionActiveDataSavedStructs(g_savedGlobals.sFlow.missionSavedData, "MF_MISSION_ARRAY")		
	
	STOP_SAVE_STRUCT()

ENDPROC
#endif
#endif
#if USE_CLF_DLC
PROC Register_flow_saved_globals_CLF()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sFlow, SIZE_OF(g_savedGlobalsClifford.sFlow), "FLOW_STRUCT")
	
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sFlow.isGameflowActive, "isGameflowActive")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sFlow.flowCompleted, "flowCompleted")
    Register_Array_Of_Strands_CLF(g_savedGlobalsClifford.sFlow.strandSavedVars, "MF_STRANDS_ARRAY")
    Register_Struct_MissionFlowControlSavedVars_CLF(g_savedGlobalsClifford.sFlow.controls, "MF_CONTROLS_STRUCT")
	Register_Array_Of_MissionActiveDataSavedStructs(g_savedGlobalsClifford.sFlow.missionSavedData, "MF_MISSION_ARRAY")
	
	STOP_SAVE_STRUCT()
ENDPROC
#endif
#if USE_NRM_DLC	
PROC Register_flow_saved_globals_NRM()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sFlow, SIZE_OF(g_savedGlobalsnorman.sFlow), "FLOW_STRUCT")
	
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sFlow.isGameflowActive, "isGameflowActive")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sFlow.flowCompleted, "flowCompleted")
    Register_Array_Of_Strands_NRM(g_savedGlobalsnorman.sFlow.strandSavedVars, "MF_STRANDS_ARRAY")
    Register_Struct_MissionFlowControlSavedVars_NRM(g_savedGlobalsnorman.sFlow.controls, "MF_CONTROLS_STRUCT")
	Register_Array_Of_MissionActiveDataSavedStructs(g_savedGlobalsnorman.sFlow.missionSavedData, "MF_MISSION_ARRAY")
	
	STOP_SAVE_STRUCT()
ENDPROC
#endif

/// PURPOSE:
///   	Register the array containing the strands that have command pointer overrides saved for them.
/// INPUT PARAMS:
///		paramStrandToOverride[]			An instance of a strand to override array.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_Of_StrandToOverrides(STRANDS &paramStrandToOverride[MAX_STRAND_POINTER_OVERRIDES], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramStrandToOverride, SIZE_OF(paramStrandToOverride), paramNameOfArray)
	
	INT index
	REPEAT MAX_STRAND_POINTER_OVERRIDES index
		TEXT_LABEL_63 tEnumName = "STRAND_TO_OVERRIDE_"
		tEnumName += index
		REGISTER_ENUM_TO_SAVE(paramStrandToOverride[index], tEnumName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing the strands command pointer overrides values.
/// INPUT PARAMS:
///		paramCommandPointerOverride[]	An instance of a command pointer INT for a strand.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_Of_CommandPointerOverrides(INT &paramCommandPointerOverride[MAX_STRAND_POINTER_OVERRIDES], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramCommandPointerOverride, SIZE_OF(paramCommandPointerOverride), paramNameOfArray)
	
	INT index
	REPEAT MAX_STRAND_POINTER_OVERRIDES index
		TEXT_LABEL_63 tIntName = "COMMAND_POINTER_OVERRIDE_"
		tIntName += index
		REGISTER_INT_TO_SAVE(paramCommandPointerOverride[index], tIntName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing the strands command pointer hash ID values.
/// INPUT PARAMS:
///		paramCommandPointerHashID[]		An instance of a command hash ID INT for a strand.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_Of_CommandPointerHashIDs(INT &paramCommandPointerHashID[MAX_STRAND_POINTER_OVERRIDES], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramCommandPointerHashID, SIZE_OF(paramCommandPointerHashID), paramNameOfArray)
	
	INT index
	REPEAT MAX_STRAND_POINTER_OVERRIDES index
		TEXT_LABEL_63 tIntName = "COMMAND_POINTER_HASH_ID_"
		tIntName += index
		REGISTER_INT_TO_SAVE(paramCommandPointerHashID[index], tIntName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing any missions that should be flagged as uncompleted when restoring the save override.
/// INPUT PARAMS:
///		paramMissionToUncomplete[]		An instance of a SP_MISSIONS enum pointing to a story mission that should be uncompleted.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_Of_MissionsToUncomplete(SP_MISSIONS &paramMissionToUncomplete[MAX_STRAND_POINTER_OVERRIDES], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramMissionToUncomplete, SIZE_OF(paramMissionToUncomplete), paramNameOfArray)
	
	INT index
	REPEAT MAX_STRAND_POINTER_OVERRIDES index
		TEXT_LABEL_63 tIntName = "MISSION_TO_UNCOMPLETE_"
		tIntName += index
		REGISTER_ENUM_TO_SAVE(paramMissionToUncomplete[index], tIntName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing flags for which command pointer overrides should only apply on save loads and not MP/SP switches.
/// INPUT PARAMS:
///		paramApplyOnMPSwitchOnly[]		An instance of a BOOL array containing which strand overrides should be applied on MP switches only.
///    	paramNameOfArray				The name of this instance of the array
PROC Register_Array_Of_ApplyOnMPSwitchOnlyFlags(BOOL &paramApplyOnMPSwitchOnly[MAX_STRAND_POINTER_OVERRIDES], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramApplyOnMPSwitchOnly, SIZE_OF(paramApplyOnMPSwitchOnly), paramNameOfArray)
	
	INT index
	REPEAT MAX_STRAND_POINTER_OVERRIDES index
		TEXT_LABEL_63 tBoolName = "APPLY_ON_MP_SWITCH_ONLY_"
		tBoolName += index
		REGISTER_BOOL_TO_SAVE(paramApplyOnMPSwitchOnly[index], tBoolName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///   	Register the array containing bitsets which missions haven't had their first activation run yet.
/// INPUT PARAMS:
///		paramMissionFirstActivateBitset[]		An instance of a INT array containing bitsets of mission first activation bits.
///    	paramNameOfArray						The name of this instance of the array
PROC Register_Array_Of_MissionFirstActivateBitsets(INT &paramMissionFirstActivateBitset[NO_MISSION_ACTIVATE_BITSETS], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramMissionFirstActivateBitset, SIZE_OF(paramMissionFirstActivateBitset), paramNameOfArray)
	
	INT index
	REPEAT NO_MISSION_ACTIVATE_BITSETS index
		TEXT_LABEL_63 tIntName = "FIRST_ACTIVATION_BITSET_"
		tIntName += index
		REGISTER_INT_TO_SAVE(paramMissionFirstActivateBitset[index], tIntName)
	ENDREPEAT
	
	STOP_SAVE_ARRAY()

ENDPROC


/// PURPOSE:
///    Sets up the save structure for registering all the custom mission flow globals that need to be saved
PROC Register_Flow_Custom_Saved_Globals()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sFlowCustom, SIZE_OF(g_savedGlobals.sFlowCustom), "FLOW_CUSTOM_STRUCT")
	
	Register_Array_Of_StrandToOverrides(g_savedGlobals.sFlowCustom.strandToOverride, "MF_STRANDS_TO_OVERRIDE_ARRAY")
	Register_Array_Of_CommandPointerOverrides(g_savedGlobals.sFlowCustom.commandPointerOverride, "MF_COMMAND_POINTER_OVERRIDE_ARRAY")
	Register_Array_Of_CommandPointerHashIDs(g_savedGlobals.sFlowCustom.commandPointerHashID, "MF_COMMAND_POINTER_HASH_ID_ARRAY")
	Register_Array_Of_MissionsToUncomplete(g_savedGlobals.sFlowCustom.missionToUncomplete, "MF_MISSION_TO_UNCOMPLETE")
	Register_Array_Of_ApplyOnMPSwitchOnlyFlags(g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly, "MF_APPLY_ON_MP_SWITCH_ONLY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.numberStoredOverrides, "numberStoredOverrides")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.iMissionsCompleted, "iMissionsCompleted")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.iMissionGolds, "iMissionGolds")
	
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sFlowCustom.wasFadedOut, "wasFadedOut")
	REGISTER_BOOL_TO_SAVE(g_savedGlobals.sFlowCustom.wasFadedOut_switch, "wasFadedOut_switch")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.spInitBitset, "spInitBitset")
	Register_Array_Of_MissionFirstActivateBitsets(g_savedGlobals.sFlowCustom.missionFirstActivateBitset, "MF_MISS_FIRST_ACTIVATE_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.iFirstPersonCoverHelpCountMission, "iFirstPersonCoverHelpCountMission")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.iFirstPersonCoverHelpCountFlow, "iFirstPersonCoverHelpCountFlow")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowCustom.iVehDuckHelpCount, "iVehDuckHelpCount")
	
	STOP_SAVE_STRUCT()
	
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the custom mission flow globals that need to be saved
PROC Register_Flow_Custom_Saved_Globals_CLF()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sFlowCustom, SIZE_OF(g_savedGlobalsClifford.sFlowCustom), "FLOW_CUSTOM_STRUCT")
	
	Register_Array_Of_StrandToOverrides(g_savedGlobalsClifford.sFlowCustom.strandToOverride, "MF_STRANDS_TO_OVERRIDE_ARRAY")
	Register_Array_Of_CommandPointerOverrides(g_savedGlobalsClifford.sFlowCustom.commandPointerOverride, "MF_COMMAND_POINTER_OVERRIDE_ARRAY")
	Register_Array_Of_CommandPointerHashIDs(g_savedGlobalsClifford.sFlowCustom.commandPointerHashID, "MF_COMMAND_POINTER_HASH_ID_ARRAY")
	Register_Array_Of_MissionsToUncomplete(g_savedGlobalsClifford.sFlowCustom.missionToUncomplete, "MF_MISSION_TO_UNCOMPLETE")
	Register_Array_Of_ApplyOnMPSwitchOnlyFlags(g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly, "MF_APPLY_ON_MP_SWITCH_ONLY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides, "numberStoredOverrides")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted, "iMissionsCompleted")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.iMissionGolds, "iMissionGolds")
	
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.wasFadedOut, "wasFadedOut")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.wasFadedOut_switch, "wasFadedOut_switch")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.spInitBitset, "spInitBitset")
	Register_Array_Of_MissionFirstActivateBitsets(g_savedGlobalsClifford.sFlowCustom.missionFirstActivateBitset, "MF_MISS_FIRST_ACTIVATE_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.iFirstPersonCoverHelpCountMission, "iFirstPersonCoverHelpCountMission")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.iFirstPersonCoverHelpCountFlow, "iFirstPersonCoverHelpCountFlow")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowCustom.iVehDuckHelpCount, "iVehDuckHelpCount")
	
	STOP_SAVE_STRUCT()
	
ENDPROC
/// PURPOSE:
///    Sets up the save structure for registering all the custom mission flow globals that need to be saved
PROC Register_Flow_Custom_Saved_Globals_NRM()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sFlowCustom, SIZE_OF(g_savedGlobalsnorman.sFlowCustom), "FLOW_CUSTOM_STRUCT")
	
	Register_Array_Of_StrandToOverrides(g_savedGlobalsnorman.sFlowCustom.strandToOverride, "MF_STRANDS_TO_OVERRIDE_ARRAY")
	Register_Array_Of_CommandPointerOverrides(g_savedGlobalsnorman.sFlowCustom.commandPointerOverride, "MF_COMMAND_POINTER_OVERRIDE_ARRAY")
	Register_Array_Of_CommandPointerHashIDs(g_savedGlobalsnorman.sFlowCustom.commandPointerHashID, "MF_COMMAND_POINTER_HASH_ID_ARRAY")
	Register_Array_Of_MissionsToUncomplete(g_savedGlobalsnorman.sFlowCustom.missionToUncomplete, "MF_MISSION_TO_UNCOMPLETE")
	Register_Array_Of_ApplyOnMPSwitchOnlyFlags(g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly, "MF_APPLY_ON_MP_SWITCH_ONLY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides, "numberStoredOverrides")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.iMissionsCompleted, "iMissionsCompleted")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.iMissionGolds, "iMissionGolds")
	
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.wasFadedOut, "wasFadedOut")
	REGISTER_BOOL_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.wasFadedOut_switch, "wasFadedOut_switch")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.spInitBitset, "spInitBitset")
	Register_Array_Of_MissionFirstActivateBitsets(g_savedGlobalsnorman.sFlowCustom.missionFirstActivateBitset, "MF_MISS_FIRST_ACTIVATE_ARRAY")
	
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.iFirstPersonCoverHelpCountMission, "iFirstPersonCoverHelpCountMission")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.iFirstPersonCoverHelpCountFlow, "iFirstPersonCoverHelpCountFlow")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowCustom.iVehDuckHelpCount, "iVehDuckHelpCount")
	
	STOP_SAVE_STRUCT()
	
ENDPROC
