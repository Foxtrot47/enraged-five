// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   towing_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all towing
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE:
///    Sets up the save structure for registering all the towing globals that need to be saved
PROC Register_Towing_Saved_Globals()	

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sTowingData, SIZE_OF(g_savedGlobals.sTowingData), "TOWING_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iCurTowingRank, "TOWING_iRank")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iBools, "TOWING_iBools")
		REGISTER_ENUM_TO_SAVE(g_savedGlobals.sTowingData.lastNodeType, "TOWING_eLastNode")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iTowingJobsCompleted, "TOWING_iJobsCompleted")
		
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iAbandonCompleted, "TOWING_iAbandonCompleted")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iTrainCompleted, "TOWING_iTrainCompleted")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iHandiCompleted, "TOWING_iHandiCompleted")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iBreakDownCompleted, "TOWING_iBreakDownCompleted")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iLastNodeIndex, "TOWING_iLastNodeIndex")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sTowingData.iAccidentCompleted, "TOWING_iAccidentCompleted")
	STOP_SAVE_STRUCT()
ENDPROC

