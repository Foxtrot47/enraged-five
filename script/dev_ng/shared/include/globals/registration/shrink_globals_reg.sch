// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   shrink_globals_reg.sch
//      AUTHOR          :   Ryan Paradis
//      DESCRIPTION     :   Contains the registrations with code for all shrink
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Sets up the save structure for registering all the taxi globals that need to be saved
PROC Register_Shrink_Saved_Globals()	
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sShrinkData, SIZE_OF(g_savedGlobals.sShrinkData), "SHRINK_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sShrinkData.iByte0, "SHRINK_iByte0")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sShrinkData.iByte1, "SHRINK_iByte1")
	STOP_SAVE_STRUCT()
ENDPROC
