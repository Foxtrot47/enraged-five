// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME    	:   shoprobberies_globals_reg.sch
//      AUTHOR          :   Carlos Mijares
//      DESCRIPTION     :   Contains the registrations with code for all Shop Robberies
//							globals that need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


/// PURPOSE:
///    Registers the array tracking the last player character that robbed each shop.
PROC Register_Array_Of_Last_Player_To_Rob_Shop(INT &iItemArray[NUM_SHOP_ROBBERIES_SHOPS], STRING szNameOfArray)
	INT iItemCounter
	
	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT NUM_SHOP_ROBBERIES_SHOPS iItemCounter
			TEXT_LABEL_31 szShopRobberiesSavedName = "ShopRob_LastPlayerToRobShop"
			szShopRobberiesSavedName += iItemCounter
			REGISTER_INT_TO_SAVE(iItemArray[iItemCounter], szShopRobberiesSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Registers the array tracking the time, in milliseconds, each shop was last robbed on by the last character that robbed said shop.
PROC Register_Array_Of_Time_When_Shop_Was_Last_Robbed_By_Last_Player(INT &iItemArray[NUM_SHOP_ROBBERIES_SHOPS], STRING szNameOfArray)
	INT iItemCounter
	
	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT NUM_SHOP_ROBBERIES_SHOPS iItemCounter
			TEXT_LABEL_31 szShopRobberiesSavedName = "ShopRob_TimeLastPlayerRobbed"
			szShopRobberiesSavedName += iItemCounter
			REGISTER_INT_TO_SAVE(iItemArray[iItemCounter], szShopRobberiesSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Registers the array tracking the time, in milliseconds, each shop was last robbed on.
PROC Register_Array_Of_Time_When_Shop_Was_Last_Robbed(INT &iItemArray[NUM_SHOP_ROBBERIES_SHOPS], STRING szNameOfArray)
	INT iItemCounter
	
	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT NUM_SHOP_ROBBERIES_SHOPS iItemCounter
			TEXT_LABEL_31 szShopRobberiesSavedName = "ShopRob_TimeLastRobbed"
			szShopRobberiesSavedName += iItemCounter
			REGISTER_INT_TO_SAVE(iItemArray[iItemCounter], szShopRobberiesSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Registers the array tracking the number of times each shop has been robbed.
PROC Register_Array_Of_Num_Times_A_Shop_Was_Robbed(INT &iItemArray[NUM_SHOP_ROBBERIES_SHOPS], STRING szNameOfArray)
	INT iItemCounter
	
	START_SAVE_ARRAY_WITH_SIZE(iItemArray, SIZE_OF(iItemArray), szNameOfArray)
		REPEAT NUM_SHOP_ROBBERIES_SHOPS iItemCounter
			TEXT_LABEL_31 szShopRobberiesSavedName = "ShopRob_NumTimesRobbed"
			szShopRobberiesSavedName += iItemCounter
			REGISTER_INT_TO_SAVE(iItemArray[iItemCounter], szShopRobberiesSavedName)
		ENDREPEAT
	STOP_SAVE_ARRAY()
ENDPROC

/// PURPOSE:
///    Sets up the save structure for registering all the Shop Robberies globals that need to be saved.
PROC Register_ShopRobberies_Saved_Globals()
	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sShopRobberiesData, SIZE_OF(g_savedGlobals.sShopRobberiesData), "SHOPROBBERIES_SAVED_STRUCT")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sShopRobberiesData.iNumberOfShopRobsSinceLastCopAmbush, "ShopRob_NumOfRobsSinceCopAmbush")
		REGISTER_INT_TO_SAVE(g_savedGlobals.sShopRobberiesData.iGenericShopRobData, "ShopRob_GenericShopData")
		Register_Array_Of_Num_Times_A_Shop_Was_Robbed(g_savedGlobals.sShopRobberiesData.iNumberOfTimesShopsRobbed, "ShopRob_NumTimesRobbed_Array")
		Register_Array_Of_Time_When_Shop_Was_Last_Robbed(g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbed, "ShopRob_TimeLastRobbed_Array")
		Register_Array_Of_Time_When_Shop_Was_Last_Robbed_By_Last_Player(g_savedGlobals.sShopRobberiesData.iMilliSecsWhenShopsWereRobbedByLastPlayer, "ShopRob_TimeLastPlayerRobbed_Array")
		Register_Array_Of_Last_Player_To_Rob_Shop(g_savedGlobals.sShopRobberiesData.iLastPlayerToRobShops, "ShopRob_LastPlayerToRobShop_Array")
	STOP_SAVE_STRUCT()
ENDPROC

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
