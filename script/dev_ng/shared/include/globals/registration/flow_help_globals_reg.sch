//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 09/02/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					 		Flow Help Queue Globals								│
//│																				│
//│			Registers all globals used by the flow help queue system			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"


PROC Register_Struct_FlowHelp(FlowHelp &paramFlowHelpStruct, STRING sStructName)

	START_SAVE_STRUCT_WITH_SIZE(paramFlowHelpStruct, SIZE_OF(paramFlowHelpStruct), sStructName)
		REGISTER_TEXT_LABEL_TO_SAVE(paramFlowHelpStruct.tHelpText, 	"tHelpText")
		REGISTER_TEXT_LABEL_TO_SAVE(paramFlowHelpStruct.tAddText, 	"tAddText")
		REGISTER_INT_TO_SAVE(paramFlowHelpStruct.iStartTime, 		"iStartTime")
		REGISTER_INT_TO_SAVE(paramFlowHelpStruct.iDuration, 		"iDuration")
		REGISTER_INT_TO_SAVE(paramFlowHelpStruct.iExpirationTime, 	"iExpirationTime")
		REGISTER_INT_TO_SAVE(paramFlowHelpStruct.iCharBitset,		"iCharBitset")
		REGISTER_ENUM_TO_SAVE(paramFlowHelpStruct.ePriority,		"ePriority")
		REGISTER_ENUM_TO_SAVE(paramFlowHelpStruct.eCodeIDStart,		"eCodeIDStart")
		REGISTER_ENUM_TO_SAVE(paramFlowHelpStruct.eCodeIDDisplayed,	"eCodeIDDisplayed")
		REGISTER_INT_TO_SAVE(paramFlowHelpStruct.iSettingsFlags,	"iSettingsFlags")
	STOP_SAVE_STRUCT()

ENDPROC


PROC Register_Array_FlowHelpStructs(FlowHelp &paramFlowHelpStructArray[FLOW_HELP_QUEUE_SIZE], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramFlowHelpStructArray, SIZE_OF(paramFlowHelpStructArray), paramNameOfArray)

		TEXT_LABEL_31 txtFlowHelpStructName
		INT i
		REPEAT COUNT_OF(paramFlowHelpStructArray) i
			txtFlowHelpStructName = "FLOW_HELP_STRUCT_"
			txtFlowHelpStructName += i
			
			// Register details for one element of the Pilot Schools Array
			Register_Struct_FlowHelp(paramFlowHelpStructArray[i], txtFlowHelpStructName)
		ENDREPEAT
		
	STOP_SAVE_ARRAY()

ENDPROC


PROC Register_Array_FlowHelpPriority(FlowHelpPriority &paramFlowHelpPriorityArray[3], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramFlowHelpPriorityArray, SIZE_OF(paramFlowHelpPriorityArray), paramNameOfArray)
		TEXT_LABEL_31 txtFlowHelpPriorityName
		INT i
		REPEAT COUNT_OF(paramFlowHelpPriorityArray) i
			txtFlowHelpPriorityName = "FLOW_HELP_PRIORITY_"
			txtFlowHelpPriorityName += i
			REGISTER_ENUM_TO_SAVE(paramFlowHelpPriorityArray[i], txtFlowHelpPriorityName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


PROC Register_Array_HelpDisplayed_Bitsets(INT &paramHelpDisplayedBitset[FLOW_HELP_BITSET_COUNT], STRING paramNameOfArray)

	START_SAVE_ARRAY_WITH_SIZE(paramHelpDisplayedBitset, SIZE_OF(paramHelpDisplayedBitset), paramNameOfArray)
		TEXT_LABEL_31 txtHelpDisplayedBitsetName
		INT i
		REPEAT COUNT_OF(paramHelpDisplayedBitset) i
			txtHelpDisplayedBitsetName = "HELP_DISPLAYED_BITSET_"
			txtHelpDisplayedBitsetName += i
			REGISTER_ENUM_TO_SAVE(paramHelpDisplayedBitset[i], txtHelpDisplayedBitsetName)
		ENDREPEAT
	STOP_SAVE_ARRAY()

ENDPROC


PROC Register_FlowHelp_Saved_Globals()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobals.sFlowHelp, SIZE_OF(g_savedGlobals.sFlowHelp), "FLOW_HELP_STRUCT")
		
	Register_Array_FlowHelpStructs(g_savedGlobals.sFlowHelp.sHelpQueue, "FLOW_HELP_STRUCT_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobals.sFlowHelp.iFlowHelpCount, "iFlowHelpCount")
	Register_Array_FlowHelpPriority(g_savedGlobals.sFlowHelp.eFlowHelpPriority, "FLOW_HELP_PRIORITY_ARRAY")
	Register_Array_HelpDisplayed_Bitsets(g_savedGlobals.sFlowHelp.iHelpDisplayedBitset, "HELP_DISPLAYED_BITSET_ARRAY")

	STOP_SAVE_STRUCT()

ENDPROC

PROC Register_FlowHelp_Saved_Globals_CLF()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsClifford.sFlowHelp, SIZE_OF(g_savedGlobalsClifford.sFlowHelp), "FLOW_HELP_STRUCT")
		
	Register_Array_FlowHelpStructs(g_savedGlobalsClifford.sFlowHelp.sHelpQueue, "FLOW_HELP_STRUCT_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount, "iFlowHelpCount")
	Register_Array_FlowHelpPriority(g_savedGlobalsClifford.sFlowHelp.eFlowHelpPriority, "FLOW_HELP_PRIORITY_ARRAY")
	Register_Array_HelpDisplayed_Bitsets(g_savedGlobalsClifford.sFlowHelp.iHelpDisplayedBitset, "HELP_DISPLAYED_BITSET_ARRAY")

	STOP_SAVE_STRUCT()

ENDPROC
PROC Register_FlowHelp_Saved_Globals_NRM()

	START_SAVE_STRUCT_WITH_SIZE(g_savedGlobalsnorman.sFlowHelp, SIZE_OF(g_savedGlobalsnorman.sFlowHelp), "FLOW_HELP_STRUCT")
		
	Register_Array_FlowHelpStructs(g_savedGlobalsnorman.sFlowHelp.sHelpQueue, "FLOW_HELP_STRUCT_ARRAY")
	REGISTER_INT_TO_SAVE(g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount, "iFlowHelpCount")
	Register_Array_FlowHelpPriority(g_savedGlobalsnorman.sFlowHelp.eFlowHelpPriority, "FLOW_HELP_PRIORITY_ARRAY")
	Register_Array_HelpDisplayed_Bitsets(g_savedGlobalsnorman.sFlowHelp.iHelpDisplayedBitset, "HELP_DISPLAYED_BITSET_ARRAY")

	STOP_SAVE_STRUCT()

ENDPROC

