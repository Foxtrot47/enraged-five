USING "rage_builtins.sch"
USING "types.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   end_screen_global_definitions.sch
//      DESCRIPTION     :   Extracts the END_SCREEN_DATASET struct into its own file so it can be included where needed, including in
//							various global files (which is the main reason why this data had to be extracted). Note: This header
//							must never contain instantiated global variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      CONSTS
// ===========================================================================================================

CONST_INT END_SCREEN_MAX_ELEMENTS 13


// ===========================================================================================================
//      ENUMS
// ===========================================================================================================

ENUM END_SCREEN_ELEMENT_FORMATTING
	ESEF_NAME_ONLY,
	ESEF_RAW_INTEGER,
	ESEF_FRACTION,
	ESEF_DOLLAR_VALUE,
	ESEF_DOLLAR_VALUE_REDBACK,
	ESEF_DOLLAR_VALUE_BLUEBACK,
	ESEF_RAW_STRING,
	ESEF_RAW_USERNAME,
	ESEF_TIME_H_M_S,
	ESEF_TIME_M_S,
	ESEF_TIME_M_S_MS,
	ESEF_RAW_PERCENT,
	ESEF_DISTANCE_VALUE_METERS,
	ESEF_SINGLE_NUMBER_SUB,
	ESEF_DOUBLE_NUMBER_SUB,
	ESEF_DOUBLE_NUMBER_SUB_LEFT,
	ESEF_LEFT_AND_RIGHT_SUBSTRINGS,
	ESEF_LEFT_SIDE_INT_SUBSTRING,
	ESEF_TIME_M_S_MS_WITH_PERIOD, //1506472
	ESEF_RAW_USERNAME_WITH_RANK,
	ESEF_TWO_STRINGS
ENDENUM

ENUM END_SCREEN_CHECK_MARK_STATUS
	ESCM_NO_MARK,
	ESCM_UNCHECKED,
	ESCM_CHECKED,
	ESCM_INVALIDATED
ENDENUM

ENUM END_SCREEN_COMPLETION_TYPE 
	ESC_PERCENTAGE_COMPLETION,
	ESC_FRACTION_COMPLETION,
	ESC_XP_COMPLETION,
	ESC_STRING_COMPLETION,
	ESC_MULTI_COMPLETION,
	ESC_CASH_COMPLETION
ENDENUM

ENUM END_SCREEN_MEDAL_STATUS
	ESMS_NO_MEDAL,
	ESMS_BRONZE,
	ESMS_SILVER,
	ESMS_GOLD
ENDENUM

ENUM END_SCREEN_SOCIAL_RETURN_VALUE
	ESSRV_UNSET,
	ESSRV_TIMEOUT,
	ESSRV_ABSTAIN,
	ESSRV_THUMBS_UP,
	ESSRV_THUMBS_DOWN
ENDENUM
ENUM END_SCREEN_MP_GAME_TYPE
	ESMGT_RACE,
	ESMGT_DEATHMATCH,
	ESMGT_MISSION
ENDENUM
CONST_INT MAX_END_SCREEN_TITLE_SUB_ELEMENTS 3
CONST_INT MAX_END_SCREEN_TITLE_SUBSTRINGS 1
CONST_INT MAX_END_SCREEN_TITLE_SUBINTEGERS 2

ENUM END_SCREEN_TITLE_SUB_ELEMENT_TYPE
	ESTSET_INT,
	ESTSET_STRING,
	ESTSET_LITERAL_STRING
ENDENUM

ENUM END_SCREEN_SPLASH_TYPE

	ESST_REGULAR,
	ESST_ONE_SUB,
	ESST_TWO_SUB

ENDENUM


// ===========================================================================================================
//      The End Screen mission box struct
//		Contains specifics about the end of mission stat box.
// ===========================================================================================================


STRUCT END_SCREEN_DATASET
	FLOAT fTrueHalfWidth

	SCALEFORM_INDEX splash
	BOOL bStartedFailformAnim
	BOOL bStartedMoveUp
	SCALEFORM_INDEX button

	//header
	TEXT_LABEL passed_splash_message
	INT iSplashSubA
	INT iSplashSubB
	END_SCREEN_SPLASH_TYPE eSplashType
	
	BOOL bCenterMessageMode
	TEXT_LABEL_63 title
	BOOL titleIsUserName
	FLOAT fTitleUpMult

	INT iTitleSubelements
	END_SCREEN_TITLE_SUB_ELEMENT_TYPE titleSubElementType[MAX_END_SCREEN_TITLE_SUB_ELEMENTS]
	TEXT_LABEL_63 titleSubstrings[MAX_END_SCREEN_TITLE_SUBSTRINGS]
	INT iTitleSubInts[MAX_END_SCREEN_TITLE_SUBINTEGERS]
	
	//element list
	INT iElements
	END_SCREEN_ELEMENT_FORMATTING ElementFormat[END_SCREEN_MAX_ELEMENTS]
	TEXT_LABEL_63 ElementName[END_SCREEN_MAX_ELEMENTS]
	TEXT_LABEL_63 ElementText[END_SCREEN_MAX_ELEMENTS]
	INT ElementValA[END_SCREEN_MAX_ELEMENTS]
	INT ElementValB[END_SCREEN_MAX_ELEMENTS]
	END_SCREEN_CHECK_MARK_STATUS ElementCheck[END_SCREEN_MAX_ELEMENTS]
	//BOOL ElementInvalidation[END_SCREEN_MAX_ELEMENTS]
	BOOL ElementIsPlayerName[END_SCREEN_MAX_ELEMENTS]

	BOOL  	bTransitionOutCalled
	INT   	iFadeOutSplashTimer	
	FLOAT 	fFadeOutMult	
	FLOAT 	fScrollOutMult
	
	//completion line
	BOOL bShowCompletion
	TEXT_LABEL CompletionResultString
	TEXT_LABEL CompletionLiteralString
	//INT iCompletionPercentage
	
	INT iCompletionValueA
	INT iCompletionValueB
	END_SCREEN_COMPLETION_TYPE CompletionType
	END_SCREEN_MEDAL_STATUS CompletionMedalState
	
	//data concerning the render in effect
	INT iEndScreenDisplayFinish
	FLOAT fBlendInProgress // zero to one
	FLOAT fBlendInTargetYSum //the precalculated target height
	
	
	BOOL bHoldOnEnd
	
	BOOL bShowSkipperPrompt 
	
	BOOL bDoneFlash
	
	BOOL bNoLoadingScreenEnabled
	
	BOOL bStatsExpanded		//Shard open to show stats
	BOOL bAlwaysShowStats
	
	
	//Settings related to vote
	BOOL bVoteModeEnabled
	FLOAT fVoteBarProg
	END_SCREEN_SOCIAL_RETURN_VALUE eVoteResult
	INT iVoteTimer
	END_SCREEN_MP_GAME_TYPE eVoteGameType
	
	INT IGameTimerReplacement
	FLOAT fCircMult				//Circular interp multiplier
	INT iLastFrameRendered		//Only render once a frame, avoids flashing
	BOOL bLastRender			//If it rendered in the same frame, what was the result?
	
	INT iMidIndex
ENDSTRUCT



STRUCT END_SCREEN_DATASET_TITLE_UPDATE
	FLOAT fTrueHalfWidth

	SCALEFORM_INDEX splash
	BOOL bStartedFailformAnim
	SCALEFORM_INDEX button

	//header
	TEXT_LABEL passed_splash_message
	INT iSplashSubA
	INT iSplashSubB
	END_SCREEN_SPLASH_TYPE eSplashType
	
	BOOL bCenterMessageMode
	TEXT_LABEL_63 title
	BOOL titleIsUserName
	FLOAT fTitleUpMult

	INT iTitleSubelements
	END_SCREEN_TITLE_SUB_ELEMENT_TYPE titleSubElementType[MAX_END_SCREEN_TITLE_SUB_ELEMENTS]
	TEXT_LABEL_63 titleSubstrings[MAX_END_SCREEN_TITLE_SUBSTRINGS]
	INT iTitleSubInts[MAX_END_SCREEN_TITLE_SUBINTEGERS]
	
	//element list
	INT iElements
	END_SCREEN_ELEMENT_FORMATTING ElementFormat[END_SCREEN_MAX_ELEMENTS]
	TEXT_LABEL_63 ElementName[END_SCREEN_MAX_ELEMENTS]
	TEXT_LABEL_63 ElementText[END_SCREEN_MAX_ELEMENTS]
	INT ElementValA[END_SCREEN_MAX_ELEMENTS]
	INT ElementValB[END_SCREEN_MAX_ELEMENTS]
	END_SCREEN_CHECK_MARK_STATUS ElementCheck[END_SCREEN_MAX_ELEMENTS]
	//BOOL ElementInvalidation[END_SCREEN_MAX_ELEMENTS]
	BOOL ElementIsPlayerName[END_SCREEN_MAX_ELEMENTS]
	PLAYER_INDEX ElementRankLookup[END_SCREEN_MAX_ELEMENTS]

	BOOL  	bTransitionOutCalled
	INT   	iFadeOutSplashTimer	
	FLOAT 	fFadeOutMult	
	FLOAT 	fScrollOutMult
	
	//completion line
	BOOL bShowCompletion
	TEXT_LABEL CompletionResultString
	//INT iCompletionPercentage
	
	INT iCompletionValueA
	INT iCompletionValueB
	END_SCREEN_COMPLETION_TYPE CompletionType
	END_SCREEN_MEDAL_STATUS CompletionMedalState
	
	//data concerning the render in effect
	INT iEndScreenDisplayFinish
	FLOAT fBlendInProgress // zero to one
	FLOAT fBlendInTargetYSum //the precalculated target height
	
	
	BOOL bHoldOnEnd
	
	BOOL bShowSkipperPrompt 
	
	BOOL bDoneFlash
	
	BOOL bNoLoadingScreenEnabled
	
	
	//Settings related to vote
	BOOL bVoteModeEnabled
	FLOAT fVoteBarProg
	END_SCREEN_SOCIAL_RETURN_VALUE eVoteResult
	INT iVoteTimer
	END_SCREEN_MP_GAME_TYPE eVoteGameType
	
	INT IGameTimerReplacement
	
ENDSTRUCT






