//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	menu_globals.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used to store menu data. 							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT MAX_MENU_ROWS 				128		//This cannot go any higher
CONST_INT MAX_MENU_COLUMNS	 			5

CONST_INT MAX_STORED_MENU_TEXT_LABELS 	(MAX_MENU_ROWS * 2) // Allow 2 text labels per row for now.
CONST_INT MAX_STORED_MENU_INTS 			(MAX_MENU_ROWS * 2)
CONST_INT MAX_STORED_MENU_FLOATS 		(MAX_MENU_ROWS * 1)
CONST_INT MAX_STORED_MENU_ICONS 		(MAX_MENU_ROWS * 2)
CONST_INT MAX_STORED_MENU_PLAYER_NAMES	40
CONST_INT MAX_STORED_DESC_INTS			3
CONST_INT MAX_STORED_DESC_FLOATS		3
CONST_INT MAX_STORED_DESC_STRINGS	 	3
CONST_INT MAX_STORED_DESC_PLAYER_NAMES 	2
CONST_INT MAX_STORED_TEXT_COMPS		 	4
CONST_INT MAX_STORED_HELP_KEYS		 	12
CONST_INT MAX_COLOURED_ITEMS			2
CONST_INT MAX_MENU_IDS					6 //5


///////////////////////////////////////////////////////////
///    MENU OFFSETS AND SCALES
///    Pixels based on 1280x720
///    1px on x-axis = 0.00078125
///    1px on y-axis = 0.00138888
//CONST_FLOAT CUSTOM_MENU_X		 					0.05
FLOAT CUSTOM_MENU_X = 		 						0.05 // Temporarily making this a float so we can change when displaying killstrip in mp.
FLOAT CUSTOM_MENU_Y	=	 							0.05
FLOAT CUSTOM_MENU_W = 			 					0.225  // 288px

CONST_FLOAT CUSTOM_MENU_PIXEL_WIDTH					0.00078125 	// 1px
CONST_FLOAT CUSTOM_MENU_PIXEL_HEIGHT				0.00138888	// 1px

TWEAK_FLOAT CUSTOM_MENU_TEXT_SCALE_Y				0.3500		// 14pt = 14/40
TWEAK_FLOAT CUSTOM_MENU_CONDENSED_TEXT_SCALE_Y		0.4250		// 17pt = 17/40
CONST_FLOAT CUSTOM_MENU_TEXT_INDENT_X				0.0046875 	// 8px-2px text padding
CONST_FLOAT CUSTOM_MENU_TEXT_INDENT_Y				0.00277776 	// 2px

CONST_FLOAT CUSTOM_MENU_TITLE_TEXT_INDENT_X			0.00390625 	// 8px-3px text padding
CONST_FLOAT CUSTOM_MENU_TITLE_TEXT_INDENT_Y			0.00416664 	// 3px

CONST_FLOAT CUSTOM_MENU_HEADER_H 					0.034722	// 25px
CONST_FLOAT CUSTOM_MENU_FOOTER_H 					0.034722	// 25px
CONST_FLOAT CUSTOM_MENU_WHITE_LINE_H				0.0// 0.00277776	// 2px
CONST_FLOAT CUSTOM_MENU_ITEM_BAR_H					0.034722	// 25px
CONST_FLOAT CUSTOM_MENU_MESSAGE_OFFSET_Y			0.00277776	// 2px

CONST_FLOAT CUSTOM_MENU_SPACER_H					0.00277776	// 2px

TWEAK_INT tiCOMMON_MENU_BG_ALPHA 					153//((255/100)*60)
TWEAK_INT tiCOMMON_MENU_MB_ALPHA 					153//((255/100)*60)
TWEAK_INT tiCOMMON_MENU_H_ALPHA 					255//204//((255/100)*80)	// [TART UP]
TWEAK_INT tiCOMMON_MENU_F_ALPHA 					204//((255/100)*80)

CONST_FLOAT CUSTOM_MENU_SIXTEEN_BY_NINE_ASPECT		1.77777778 //  16/9
CONST_FLOAT	CUSTOM_MENU_TRIPLE_HEAD_THRESHOLD_RATIO	3.5

ENUM MENU_ICON_TYPE
	MENU_ICON_DUMMY = 0,
	
	// Icons used in shops
	MENU_ICON_STAR,
	MENU_ICON_LEFT_STAR,
	MENU_ICON_CROWN,
	MENU_ICON_TICK,
	MENU_ICON_BOX_TICK,
	MENU_ICON_BOX_CROSS,
	MENU_ICON_BOX_EMPTY,
	MENU_ICON_COLOUR,
	MENU_ICON_TSHIRT,
	MENU_ICON_GUN,
	MENU_ICON_INK,
	MENU_ICON_CAR,
	MENU_ICON_BIKE,
	MENU_ICON_SCISSORS,
	MENU_ICON_LOCK,
	MENU_ICON_LSC,
	MENU_ICON_AMMO,
	MENU_ICON_ARMOUR,
	MENU_ICON_HEALTH,
	MENU_ICON_MAKEUP,
	
	MENU_ICON_BLADE,
	MENU_ICON_SYRINGE,
	MENU_ICON_WEED,
	MENU_ICON_METH,
	MENU_ICON_CASH,
	
	MENU_ICON_ARROW_L,
	MENU_ICON_ARROW_R,
	
	MENU_ICON_ALERT,
	
	MENU_ICON_HEADER,
	MENU_ICON_DLC_IMAGE,
	
	MENU_ICON_M,
	MENU_ICON_F,
	MENU_ICON_T,
	
	MENU_ICON_MISSION,
	MENU_ICON_SURVIVAL,
	MENU_ICON_GANG_ATTACK,
	MENU_ICON_RACE_LAND,
	MENU_ICON_RACE_BIKE,
	MENU_ICON_RACE_WATER,
	MENU_ICON_RACE_AIR,
	MENU_ICON_RACE_FOOT,
	MENU_ICON_BASE_JUMPING,
	MENU_ICON_RACE_OPEN_WHEEL,
	MENU_ICON_DEATHMATCH,
	MENU_ICON_TEAM_DM,
	MENU_ICON_VEH_DM,
	MENU_ICON_CAPTURE,
	MENU_ICON_LTS,
	MENU_ICON_KOTH,
	MENU_ICON_TEAM_KOTH,
	
	MENU_ICON_DISCOUNT,
	
	MENU_ICON_YACHT,
	
	MENU_ICON_RACE_STUNT,
	
	MENU_ICON_STAR_FADED,
	
	MENU_ICON_BIN_LOCK,
	
	MENU_ICON_CLUBS,
	MENU_ICON_HEARTS,
	MENU_ICON_SPADES,
	MENU_ICON_DIAMONDS,
	MENU_ICON_DECORATION,
	MENU_ICON_CHIPS,
	MENU_ICON_DECORATION_FADED,
	
	// THIS MUST BE THE LAST ONE
	MAX_ICONS
	
ENDENUM
CONST_INT CUSTOM_MENU_ICON_COUNT (MAX_ICONS)

ENUM MENU_ITEM_TYPE
	MENU_ITEM_DUMMY = 0,
	
	MENU_ITEM_TEXT,				// 'Some text'		//DISPLAY_TEXT(label)
	MENU_ITEM_INT,				// '1'				//DISPLAY_NUMBER("NUMBER", int)	
	MENU_ITEM_FLOAT,			// '1.0'			//DISPLAY_TEXT_WITH_FLOAT("NUMBER", float)
	MENU_ITEM_ICON,				// '@'				//DRAW_SPRITE()
	
	MENU_ITEM_TAB				// '	'
ENDENUM

ENUM MENU_TEXT_COMP_TYPE
	MENU_TEXT_COMP_DUMMY = 0,
	
	MENU_TEXT_COMP_TEXT,
	MENU_TEXT_COMP_INT,
	MENU_TEXT_COMP_FLOAT,
	MENU_TEXT_COMP_ICON,
	MENU_TEXT_COMP_PLAYER_NAME,
	MENU_TEXT_COMP_RADIO_STATION,
	MENU_TEXT_COMP_LITERAL,
	MENU_TEXT_COMP_VEHICLE_NAME,
	MENU_TEXT_COMP_CONDENSED_LITERAL
ENDENUM

STRUCT MenuData
	
	// Header
	BOOL bUseHeaderGraphic
	
	// Title
	TEXT_LABEL_15 tl15Title
	MENU_TEXT_COMP_TYPE eTitleComps[MAX_STORED_TEXT_COMPS]
	INT iTitleInt[MAX_STORED_DESC_INTS]
	FLOAT fTitleFloat[MAX_STORED_DESC_FLOATS]
	INT iTitleFloatDP[MAX_STORED_DESC_FLOATS]
	TEXT_LABEL_23 tl15TitleText[MAX_STORED_DESC_STRINGS]
	TEXT_LABEL_63 tlTitlePlayerName[MAX_STORED_DESC_PLAYER_NAMES]
	INT iTitleTotalParams
	INT iTitleIntParams
	INT iTitleFloatParams
	INT iTitleTextParams
	INT iTitlePlayerNameParams
	
	// Main
	TEXT_LABEL_23 tl15Item[MAX_STORED_MENU_TEXT_LABELS]
#IF IS_DEBUG_BUILD
	TEXT_LABEL_23 realNames[MAX_STORED_MENU_TEXT_LABELS]
#ENDIF
	BOOL bIsSelectable[MAX_STORED_MENU_TEXT_LABELS]
	BOOL bIsDefault[MAX_STORED_MENU_TEXT_LABELS]
	BOOL bForceCondensedFont[MAX_STORED_MENU_TEXT_LABELS]
	MENU_TEXT_COMP_TYPE eTextItemComps[MAX_STORED_MENU_TEXT_LABELS][MAX_STORED_TEXT_COMPS]
	
	TEXT_LABEL_63 tlPlayerNameItem[MAX_STORED_MENU_PLAYER_NAMES]
	INT iItem[MAX_STORED_MENU_INTS]
	FLOAT fItem[MAX_STORED_MENU_FLOATS]
	INT iFloatDP[MAX_STORED_MENU_FLOATS]
	MENU_ICON_TYPE eIconItem[MAX_STORED_MENU_ICONS]
	
	// Description
	TEXT_LABEL_23 tl23Desc
	TEXT_LABEL_23 tl23DescExtra
	MENU_TEXT_COMP_TYPE eDescComps[MAX_STORED_TEXT_COMPS]
	INT iDescInt[MAX_STORED_DESC_INTS]
	FLOAT fDescFloat[MAX_STORED_DESC_FLOATS]
	INT iDescFloatDP[MAX_STORED_DESC_FLOATS]
	TEXT_LABEL_63 tl15DescText[MAX_STORED_DESC_STRINGS]
	INT iDescTotalParams
	INT iDescIntParams
	INT iDescFloatParams
	INT iDescTextParams
	INT iDescClearTimer
	INT iDescStartTimer
	MENU_ICON_TYPE eDescIcon
	
	// Help
	INT iHelpCount
	BOOL bHelpCreated
	TEXT_LABEL_63 eHelpKey[MAX_STORED_HELP_KEYS]
	TEXT_LABEL_15 tl15HelpText[MAX_STORED_HELP_KEYS]
	INT iHelpTextINT[MAX_STORED_HELP_KEYS]
	CONTROL_ACTION caHelpTextInput[MAX_STORED_HELP_KEYS]
	CONTROL_ACTION_GROUP caHelpTextInputGroup[MAX_STORED_HELP_KEYS]
	INT iHelpKeyIsClickableBits
	FLOAT fHelpKeysX
	FLOAT fHelpKeysY
	BOOL bUseTempKeyCoords

	// Layout
	MENU_ITEM_TYPE eItemLayout[MAX_MENU_COLUMNS]
	FLOAT fColumnXOffset[MAX_MENU_COLUMNS]
	FLOAT fColumnWidth[MAX_MENU_COLUMNS]
	FLOAT fTitleWidth
	BOOL bItemToggleable[MAX_MENU_COLUMNS]
	FLOAT fHelpKeyClearSpace
	BOOL bStackedKeys
	eTextJustification eJustification[MAX_MENU_COLUMNS]
	
	// Setup track
	INT iItemBitset[MAX_MENU_ROWS]
	INT iCurrentRow, iCurrentColumn
	INT iCurrentTextItem
	INT iCurrentPlayerNameItem
	INT iCurrentIntItem
	INT iCurrentFloatItem
	INT iCurrentIconItem
	INT iMenuRows
	INT iMenuColumns
	BOOL bMenuRowDoesntAddToCount[MAX_MENU_ROWS]
	BOOL bMenuRowHasSpacer[MAX_MENU_ROWS]
	BOOL bMenuRowHasDisplayItems[MAX_MENU_ROWS]

	BOOL bMenuAssetsRequested[MAX_MENU_IDS]
	BOOL bShopDiscountAssetsRequested[MAX_MENU_IDS]
	BOOL bMenuTextRequested[MAX_MENU_IDS]
	TEXT_LABEL_15 tlTextBlockName[MAX_MENU_IDS]
	SCALEFORM_DATA_STRUCT sMenuHelp[MAX_MENU_IDS]
	INT iScriptHash[MAX_MENU_IDS]
	
	BOOL bSetupComplete
	BOOL bDisplayRowsDefined
	FLOAT fSetupFinalBodyY
	INT iSetupTotalRows
	INT iSetupTotalDisplayRows
	INT iSetupTotalSelectableRows
	INT iSetupCurrentSelectableItem
	
	BOOL bOverrideTitleRowCounts
	INT iTitleRowOverride1
	INT iTitleRowOverride2
	
	// Runtime track
	INT iTopItem
	INT iCurrentItem
	INT iLastDisplayItem
	INT iComponentItems
	INT iLastTextItem
	INT iComponentCount
	MENU_ITEM_TYPE eLastAddedItemType
	FLOAT fRowHeight[MAX_MENU_ROWS]
	BOOL bDisplayCurrentItemToggles
	BOOL bDisplayItemCount
	FLOAT fMenuFinalScreenY
	FLOAT fCurrentItemScreenY
	INT iLastDrawTimer
	FLOAT fMenuItemScreenY[MAX_MENU_ROWS]
	INT iCarColourItem[MAX_COLOURED_ITEMS]
	INT iCarColour[MAX_COLOURED_ITEMS]
	BOOL bDefaultOptionAdded
	
	INT iRowIsGrayedOutBS[4] // MAX_MENU_ROWS / 32
	
	TEXT_LABEL_63 sIconTXDOverride[CUSTOM_MENU_ICON_COUNT]
	TEXT_LABEL_63 sIconTextureOverride[CUSTOM_MENU_ICON_COUNT]
	
	
	// Cached values to help speed up menu processing
	// - Row data
	INT iDisplayRow[15]
	// - Item data
	INT iStoredTextCount[40]
	INT iStoredIntCount[40]
	INT iStoredFloatCount[40]
	INT iStoredIconCount[40]
	INT iStoredPlayerNameCount[40]
	FLOAT fStoredTempWidth[40]
	FLOAT fStoredTempTextWidth[40]
	FLOAT fStoredTempIconWidth[40]
	
	//Keep phone on screen for next menu draw call
	BOOL bKeepPhoneForNextDrawMenuCall
	
	INT iHeaderR, iHeaderG, iHeaderB, iHeaderA
	BOOL bUseCustomHeaderColour
	
	INT iHeaderTextR, iHeaderTextG, iHeaderTextB, iHeaderTextA
	BOOL bUseCustomHeaderTextColour
	
	INT iBodyR, iBodyG, iBodyB, iBodyA
	BOOL bUseCustomBodyColour
	
	INT iFooterR, iFooterG, iFooterB, iFooterA
	BOOL bUseCustomFooterColour
	
	INT iHelpR, iHelpG, iHelpB, iHelpA
	BOOL bUseCustomHelpColour
	
	HUD_COLOURS eSelectionBarColour
	BOOL bUseCustomSelectionBarColour
	
	HUD_COLOURS eRowColour[2]
	BOOL bUseCustomRowColour
	INT iCustomRowColour
	
	BOOL bUseInvertedScrollColour
	
	// Block this menu
	BOOL bDisableMenu
	
	// Subitltes moved
	BOOL bSubtitlesMoved
	
//	BOOL bForceFooter = False
	
	#IF IS_DEBUG_BUILD
		BOOL bDrawDebugSpacers
		FLOAT fSpacerX[4]
		FLOAT fSpacerY[4]
		INT iSpacerScale[4]
	#ENDIF
	
ENDSTRUCT
MenuData g_sMenuData
