// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   debug_globals.sch
//      AUTHOR          :   Kenneth
//      DESCRIPTION     :   Stores globals that are used only in debug mode.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

#IF IS_DEBUG_BUILD

	STRUCT SCRIPT_METRIC_INFO
		INT iStartTime
		THREADID Thread_ID
	ENDSTRUCT

	
	USING "flow_launcher_structs_core.sch"
	USING "flow_diagram_structs_core.sch"
	USING "bugstar_globals.sch"	
	USING "debug_menu_structs_core.sch"
	
	// OJ info
	INT g_iRepoVariationDebug = -1
	INT g_iVigilVariantDebug = -1
	
	// RPG Stats
	BOOL g_bRPGStatTest	= FALSE
	
	// Random Character debug
	BOOL g_bResetRandomCharDebugRange
	
	// Special game mode flags.
	BOOL g_bPreorderGame
	BOOL g_bPreorderGameNextGen
	BOOL g_bJapaneseSpecialEditionGame
	BOOL g_bGTAOnlineAvailable = TRUE
	BOOL g_bLastGenPlayer
	BOOL g_bLastGenSpecialEditionPlayer
	BOOL g_bLastGenCollectorEditionPlayer
	
	BOOL g_bForce_PREMIUM_true = FALSE
	BOOL g_bForce_PREMIUM_false = FALSE
	BOOL g_bForce_STARTER_true = FALSE
	BOOL g_bForce_STARTER_false = FALSE
	
	BOOL g_bForce_HACKER_TRUCK_true = FALSE
	BOOL g_bForce_HACKER_TRUCK_false = FALSE
	
	BOOL g_bForce_IS_LAST_GEN_PLAYER_true = FALSE
	BOOL g_bForce_IS_LAST_GEN_PLAYER_false = FALSE
	
	// Global BOOL to say if we should load/unload prologue for art
	BOOL g_bPrologueMapLoaded = FALSE
	
	// Global BOOL to allow shop scripts to run in debug
	BOOL g_bShopsAvailableInDebug = FALSE
	
	// Global BOOL to savegame bed to run in debug
	BOOL g_bSavegameBedAvailableInDebug = FALSE
	
	// Global BOOL to allow vehicle gen controller to run in debug
	BOOL g_bVehicleGenAvailableInDebug = FALSE
	
	// Global BOOL to allow random character controller to run in debug
	BOOL g_bRandomCharsAvailableInDebug = FALSE
	
	// Global BOOL to track if we have set a random character on startup
	BOOL g_bRandomPlayerCharacterSet = FALSE
	
	// Global BOOL to detect whether a race has been launched from the debug menu
	BOOL g_RaceLaunchedFromDebug = FALSE
	
	// Global BOOL to unlock all the Chop tricks
	BOOL g_bUnlockAllChopTricks = FALSE
	
	// Global BOOL to allow mission stage select menu to display bugs with the LBD tag
	BOOL g_bDisplayLBDBugSummary = TRUE
	
	// Global BOOL to output wardrobe fail debug text
	BOOL g_bDisplayWardrobeDebug
	
	INT           g_iDebugTimestampBitset
	TEXT_LABEL_31 g_tlDebugTimestamp
	
	INT iSelectMissionStageDrawTime
	
	// Positions for randomised player start and debug widget
	// When adding new entries to this list remember to update iTOTAL_DEBUG_START_LOCATIONS
	CONST_INT     iTOTAL_DEBUG_START_LOCATIONS 10
	VECTOR        g_vDebugStartPositions[iTOTAL_DEBUG_START_LOCATIONS]
	TEXT_LABEL_31 g_DebugStartPositionStrings[iTOTAL_DEBUG_START_LOCATIONS]
	FLOAT         g_vDebugStartOrientations[iTOTAL_DEBUG_START_LOCATIONS]
	
	// Scripted cam editor globals
	INT g_iScriptedCamEditorFlags
	
	// For the coordinate Sender
	BOOL g_bSendCoords
	BOOL g_bRead_coordinates

	// For automated transition test
	BOOL g_binitiatejoinleavetest
	
	//
	BOOL g_bBlipDebugPreventFlash
	
	FLOW_DIAGRAM_VARS	g_flowDiagram

	BUGSTAR_QUERY 		g_bugstarQuery
	
	DEBUG_MENU_CONTROL	g_debugMenuControl
		
	CONST_INT MAX_SCRIPT_METRIC_THREADS 64
	BOOL b_ScriptMetricsAreRecording
	SCRIPT_METRIC_INFO RecordingThreads[MAX_SCRIPT_METRIC_THREADS]
	
	BOOL g_bSuppressBlipDebug = TRUE
	
	TIMEOFDAY g_debugTOD = INVALID_TIMEOFDAY
	
	CONST_INT SCRIPT_PROFILER_ACTIVE 0
	
	INT g_iCommandExecutedLastCall
	
	
	// Global Bool to relaunch photographyWildlife.sc
	BOOL g_bLanchPhotographyWildlifeScript = FALSE
	
	// Global Bool to terminate photographyWildlife.sc
	BOOl g_bTerminatePhotographyWildlifeScript = FALSE
	
	// Global Bool to script launcher automatically launching photographyWildlife.sc
	BOOL g_bBlockPhotographyWildlifeRelaunch = FALSE
	
	// Global Bool to clear all photographyWildlife.sc saved data
	BOOL g_bClearAllPhotographyWildlifeSaveData = FALSE
	
	eLevelIndex g_eCachedLevel
	
	// Tells main persistent to relaunch the debug script
	BOOL g_dRestartDebugSc
	
	// RNG Cache.
	INT g_iCachedRNGValues_LuckyWheel
	INT g_iCachedRNGValues_Poker[52]
	INT g_iCachedRNGValues_Blackjack[208]
	INT g_iCachedRNGValues_BlackjackDecks[208]
	INT g_iCachedRNGValues_InsideTrackSelectionFavourite[2]
	INT g_iCachedRNGValues_InsideTrackSelectionMiddle[2]
	INT g_iCachedRNGValues_InsideTrackSelectionOutlier[2]
	INT g_iCachedRNGValues_InsideTrackFinish[6]
	INT g_iCachedRNGValues_InsideTrackOverride[6]
	

	// These are temp, please remove after finalised positions.
	VECTOR g_vTempIntroCamTuner_IntroCamPos1 = <<2.0, 5.0, 0.5>>
	VECTOR g_vTempIntroCamTuner_IntroCamPoint1 = <<-0.1, 1.0, 0.05>>
	VECTOR g_vTempIntroCamTuner_IntroCamPos2 = <<1.6, 6.5, 2.25>>
	VECTOR g_vTempIntroCamTuner_IntroCamPoint2 = <<-1.250, -1.250, -0.15>>
	VECTOR g_vTempIntroCamTuner_OutroCamPos1 = <<-3.0, -2.5, 0.5>>
	VECTOR g_vTempIntroCamTuner_OutroCamPoint1 = <<0.0, 0.0, 0.0>>
	// ---
	
	INT g_iDialogueProgressLastPlayed = -1
	BOOL g_bEnableConversationDebugCustomPlaying
	BOOL g_bEnableConversationDebugCustomPlayingLine
	BOOL g_bEnableConversationDebugCustomPlayingAmbientLinePos
	BOOL g_bEnableConversationDebugCustomKillConvo
	structPedsForConversation g_ConversationDebugCustom_speechPedStruct

	TEXT_LABEL_31 g_tl31_Block_DiagDebug
	TEXT_WIDGET_ID g_ConversationDebugCustomBlock

	TEXT_WIDGET_ID g_ConversationDebugCustomRoot
	TEXT_LABEL_31 g_tl31_Root_DiagDebug

	TEXT_WIDGET_ID g_ConversationDebugCustomLabel
	TEXT_LABEL_31 g_tl31_Label_DiagDebug

	TEXT_WIDGET_ID g_widConversationDebugVoiceNamePed1
	TEXT_LABEL_31 g_ConversationDebugVoiceNamePed1
	INT g_ConversationDebugVoiceNumberPed1 = -1
	TEXT_WIDGET_ID g_widConversationDebugVoiceNamePed2
	TEXT_LABEL_31 g_ConversationDebugVoiceNamePed2
	INT g_ConversationDebugVoiceNumberPed2 = -1
	TEXT_WIDGET_ID g_widConversationDebugVoiceNamePed3
	TEXT_LABEL_31 g_ConversationDebugVoiceNamePed3
	INT g_ConversationDebugVoiceNumberPed3 = -1

	BOOL g_bEnableMouseToWorldDebug
	SHAPETEST_INDEX stiMouseToWorldCoordTest
	VECTOR vMouseToWorldCoordResult[2]		
	ENTITY_INDEX eiMouseToWorldCoordCompareTarget	
	
#ENDIF


//ENDGLOBALS

