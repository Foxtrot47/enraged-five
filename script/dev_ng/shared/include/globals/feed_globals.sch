

//feed system enums

ENUM ENUM_FEED_DATA_TYPE
	INTERNAL_FEED_UNSET,
	INTERNAL_FEED_DLC,
	INTERNAL_FEED_MESSAGE_TEXT,
	INTERNAL_FEED_MESSAGE_PICTURE,
	INTERNAL_FEED_SOCIALCLUB,
	INTERNAL_FEED_FRIENDS,
	INTERNAL_FEED_STATS
ENDENUM


ENUM ENUM_FEED_ICONS
	NO_FEED_ICON = -1,
	PLACEHOLDER_FEED_ICON = 0,
	MAX_FEED_ICONS
ENDENUM 
//FEED ICON REGISTRY IS CONTAINED INSIDE FeedManager.sc


ENUM ENUM_FEED_SUB_TYPES
	FEED_SUB_UNSET,
	FEED_SUB_TEXT_LABEL,
	FEED_SUB_PLAYER_NAME,
	FEED_SUB_FLOAT,
	FEED_SUB_INT,
	MAX_FEED_SUB_TYPES
ENDENUM




//feed system triggers



//MP FEED MAXES
//The current number of components and types are defined by the const_ints in mp_globals_common.sch:
CONST_INT MAX_FEED_TEXT_LABEL_COMPONENTS      2
CONST_INT MAX_FEED_PLAYER_NAME_COMPONENTS     4
CONST_INT MAX_FEED_FLOAT_COMPONENTS           1
CONST_INT MAX_FEED_INT_COMPONENTS             2

CONST_INT MAX_FEED_COMPONENTS 			      6 





//stored feed system data
STRUCT FEED_DATA_ENTRY
	
	ENUM_FEED_DATA_TYPE myType//dicates what scaleform queue it goes in
	ENUM_FEED_ICONS eIcon//sets which icon it uses
	
	//message header - a simple text label, not used for all types
		TEXT_LABEL mheader
	
	//message core - a wide awake nightmare of substrings
	//has to be like this to support all the systems that can pass data into this
		//core
		TEXT_LABEL mcore	
		//format //remember to count previous when adding
		ENUM_FEED_SUB_TYPES coreComponents[MAX_FEED_COMPONENTS] //-1 unused, otherwise substring pass will be attempted
		
		//subs//totals		
		TEXT_LABEL subtext[MAX_FEED_TEXT_LABEL_COMPONENTS]
		
		TEXT_LABEL_31 subnames[MAX_FEED_PLAYER_NAME_COMPONENTS]
		
		FLOAT subfloats[MAX_FEED_FLOAT_COMPONENTS]
		
		INT subints[MAX_FEED_INT_COMPONENTS]
	
		
	
	//NOTES
	//supplemental data fields //substrings etc etc
	//structTextMessage -> TxtMsgSpecialComponents //msgdata
	//multiplayer names
	//TEXT_LABEL_31 //player names are 32 chars max // max of 4 in mp message

ENDSTRUCT



CONST_INT MAX_FEED_LOG_ENTRIES 40

STRUCT FeedDataSaved
	INT iSystemFlags
	INT iFeedCaret
	INT iProcessedCaret
	FEED_DATA_ENTRY RollingFeedLog[MAX_FEED_LOG_ENTRIES]
ENDSTRUCT
























































