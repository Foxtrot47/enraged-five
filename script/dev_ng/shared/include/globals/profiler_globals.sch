USING "rage_builtins.sch"
USING "global_block_defines.sch"
USING "types.sch"

// FOR SCRIPT PROFILING

CONST_INT MAX_SCRIPT_PROFILE_MARKERS 750
CONST_INT NUMBER_OF_ROWS_TO_RENDER 41
CONST_INT MAX_NUMBER_OF_SCRIPT_PROFILER_NESTS 20

GLOBALS GLOBALS_BLOCK_PROFILER


STRUCT SCRIPT_PROFILE_MARKER_DATA
	INT iIndex = -1
	INT iParentID = -1
	INT iHash = -1
	INT iTime
	INT iIns
	INT iPeakTime
	INT iPeakIns
	BOOL bCalledThisFrame
	TEXT_LABEL_63 strName
	BOOL bIsLoopParent
	BOOL bIsLoopChild
	BOOL bShowChildren
	BOOL bHasChildren
	BOOL bIsRendering
	BOOL bIsHidden
	INT iTimeTotal
	INT iInsTotal
	INT iTimeAvg
	INT iInsAvg
	INT iCalls
ENDSTRUCT


	//#IF SCRIPT_PROFILER_ACTIVE
	STRUCT GLOBAL_SCRIPT_PROFILER_DATA

		SCRIPT_PROFILE_MARKER_DATA MarkerData[MAX_SCRIPT_PROFILE_MARKERS]
		INT iMarkerIDs[MAX_SCRIPT_PROFILE_MARKERS]
		
		INT iLastMarkerTime
		INT iLastMarkerIns
		INT iPreviousParentID[MAX_NUMBER_OF_SCRIPT_PROFILER_NESTS]
		INT iCurrentParentID
		INT iLastMarkerID
		
		INT iSuspendTime
		INT iSuspendIns
		BOOL bCountingSuspended
		
		INT iStartRowToRender = 0
		INT iNumberOfRowsToRender = NUMBER_OF_ROWS_TO_RENDER
		INT iRowToRender
		INT iTotalRowsRendered
		INT iRowToHighlight
		INT iHighlightedMarkerID
		
		INT iStartOfFrameCommands
		INT iCommandsSinceStartOfFrame
		INT iCommandsSinceStartOfFramePeak
		BOOL bStartOfFrameCalled
		BOOL bLoopProfilerStarted	
		
		INT iMarkerCount
		INT iFailedMarkerCount
		BOOL bGoodToOutput

		INT iLastBackground
		INT iActiveThread = -1
		
		INT iTotalAccumulatedTime
		INT iTotalAccumulatedCommands
		INT iTotalAccumulatedTimePeak
		TIME_DATATYPE iClearPeakTime
		
		INT iOpenNestGroups
		
	ENDSTRUCT
	GLOBAL_SCRIPT_PROFILER_DATA GlobalScriptProfileData
	
	
	
	//#ENDIF
ENDGLOBALS


