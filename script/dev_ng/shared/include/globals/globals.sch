USING "buildtype.sch"
USING "types.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "stack_sizes.sch"
USING "global_block_defines.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   globals.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   This is the globals file that all scripts should include.
//                          It contains single player or multiplayer specific globals
//                              and any shared globals that do not need to be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

GLOBALS GLOBALS_BLOCK_STANDARD

	CONST_INT USE_FINAL_PRINTS 0

	// Save file state tracking.
	CONST_FLOAT	SAVE_GAME_VERSION			1.70			
	FLOAT g_fLoadedSaveVersion 				= 0.00
	BOOL g_bRestoredSaveThisSession 		= FALSE
	
	BOOL g_bRunMultiplayerOnStartup = FALSE
	BOOL g_bMagDemoActive = FALSE
	BOOL g_bRockstarEditorActive = FALSE
	INT g_iMagDemoVariation = 0
	BOOL g_bMagDemoKillFamilySceneM = FALSE
	BOOL g_bMagDemoFBI2Retry = FALSE
	BOOL g_bMagDemoFRA2Retry = FALSE
	BOOL g_bMagDemoFRA2Passed = FALSE
	BOOL g_bMagDemoFRA2Ready = FALSE
	BOOL g_bMagDemoBJReady = FALSE
	BOOL g_bMagDemoBJStarted = FALSE
	BOOL g_bMagDemoREPapDone = FALSE
	BOOL g_bMagdemoTakingOverSwitch = FALSE
	BOOL g_bMagdemoDoTakeOverSwitch = FALSE
	BOOL g_bSuppressLaptopContextIntention = FALSE
	BOOL g_bHasPauseMapBeenAccessed = FALSE
		
	BOOL g_bTrailer2Active = FALSE
	
	CONST_INT DEFAULT_GOD_TEXT_TIME				7500
	CONST_INT DEFAULT_HELP_TEXT_TIME			10000
	CONST_INT DEFAULT_REMINDER_TEXT_TIME 		3000
	CONST_INT DEFAULT_CAR_STOPPING_TO_CUTSCENE	1000
	
	CONST_INT DEFAULT_NETWORK_GOD_TEXT_TIME		2147483647 //Highest Integer
	CONST_INT DEFAULT_NETWORK_HELP_TEXT_TIME	10000
	
	//cutscene fade times
	CONST_INT DEFAULT_FADE_TIME 				800 	// time in ms for the fades in & out of cutscenes
	CONST_INT DEFAULT_FADE_TIME_SHORT			250 	// short time in ms for the fades in & out of cutscenes
	CONST_INT DEFAULT_FADE_TIME_LONG	 		2500 	// long time in ms for the fades in & out of cutscenes

	CONST_FLOAT LOCATE_SIZE_ANY_MEANS		6.0
	CONST_FLOAT LOCATE_SIZE_ON_FOOT_ONLY	4.0
	CONST_FLOAT	LOCATE_SIZE_HEIGHT			2.0
	CONST_FLOAT LOCATE_SIZE_MISSION_TRIGGER	2.75
	
	CONST_FLOAT DEFAULT_CUTSCENE_LOAD_DIST		100.0	// The distance at which we start preloading cutscene data as we approach a cutscene location.
	CONST_FLOAT DEFAULT_CUTSCENE_UNLOAD_DIST	120.0	// The distance at which we start unloading preloaded cutscene data as we leave a cutscene location.
	
	CONST_INT DEFAULT_LOAD_SCENE_TIMEOUT		10000	// The default time after which the game should give up loading a scene and fade in regardless of scene completeness.
	
	CONST_INT DEFAULT_ROAD_NODE_LOAD_DIST		150		// The distance at which it is safe to warp the players vehicle to after loading from save
	
	VECTOR g_vAnyMeansLocate	= << LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_HEIGHT >>
	VECTOR g_vOnFootLocate		= << LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_HEIGHT >>
	
	CONST_FLOAT	BUDDY_DROWN_TIME_secs	10.0
	
	CONST_INT	JAMMED_TIME						30000		// Car stuck
	CONST_INT	ROOF_TIME						7000		// Car on roof
	CONST_INT	SIDE_TIME						40000		// Car on side
	CONST_INT	HUNG_UP_TIME					30000		// Car hung up not on wheels
	CONST_INT	BEACHED_TIME					10000		// Boat beached on land
	CONST_INT	STUCK_FAIL_TIME					180000		// Scripted stuck fail check time
	CONST_FLOAT	STUCK_FAIL_RANGE_m				100.0		// Scripted stuck fail check range
	CONST_FLOAT	STUCK_FAIL_RANGE_small			50.0		// Scripted stuck fail check range
	CONST_FLOAT ABANDON_BUDDY_FAIL_RANGE 		100.0		// Abandon buddy fail check range
	CONST_FLOAT DEFAULT_VEH_STOPPING_DISTANCE 	10.5		// Default stopping distance for locates when using BRING_VEHICLE_TO_HALT.
	
	CONST_FLOAT VEHICLE_GEN_CLEANUP_RANGE       210.0 		// Used by the vehicle_gen_controller script.
	CONST_FLOAT VEHICLE_GEN_CREATE_RANGE        200.0
	
	CONST_FLOAT VEHICLE_GEN_CLEANUP_RANGE_SHORT  50.0
	CONST_FLOAT VEHICLE_GEN_CREATE_RANGE_SHORT   50.0
	
	CONST_FLOAT VEHICLE_GEN_CLEANUP_RANGE_HIGH_PRIORITY  	310.0
	CONST_FLOAT VEHICLE_GEN_CREATE_RANGE_HIGH_PRIORITY		300.0
	
	CONST_FLOAT VEHICLE_GEN_CLEANUP_RANGE_LONG  410.0
	CONST_FLOAT VEHICLE_GEN_CREATE_RANGE_LONG   400.0
	CONST_FLOAT VEHICLE_GEN_CLEANUP_RANGE_BLIMP 1010.0
	CONST_FLOAT VEHICLE_GEN_CREATE_RANGE_BLIMP  1000.0
	CONST_FLOAT VEHICLE_GEN_CLEANUP_RANGE_WORLD 5010.0
	CONST_FLOAT VEHICLE_GEN_CREATE_RANGE_WORLD  5000.0
	
	CONST_INT ciMAX_NUM_ALARM_PROPS	10
	CONST_INT ciMIN_DIST_BETWEEN_ALARM_PROPS 100//m
	
	CONST_INT	TIMEOFDAY_YEAR_OFFSET	        2011
	
	
	//Bit flags for g_iPlayerHasLastGenSpecialContentBitset.
	CONST_INT 	BIT_IS_LAST_GEN									0
	CONST_INT 	BIT_LAST_GEN_SPECIAL_CONTENT_PS3_COLLECTORS		1
	CONST_INT 	BIT_LAST_GEN_SPECIAL_CONTENT_PS3_SPECIAL		2
	CONST_INT 	BIT_LAST_GEN_SPECIAL_CONTENT_360_COLLECTORS		3
	CONST_INT 	BIT_LAST_GEN_SPECIAL_CONTENT_360_SPECIAL		4
	CONST_INT 	BIT_LAST_GEN_SPECIAL_CONTENT_STAT_STORED_COLLECTOR		5
	CONST_INT 	BIT_LAST_GEN_SPECIAL_CONTENT_STAT_STORED_SPECIAL		6
	
	INT g_iPlayerHasLastGenSpecialContentBitset = 0		//None saved local state for whether the player had last gen SE/CE content. 
	
	
	// BOOL for debug text toggle widget
	BOOL displayDebugDataInCommandWindow
	
	//Timestamp for keeping track of the cutscene skip delay.
	INT gCutsceneSkipStartTime = 0
	INT gCutsceneSkipLastCalledTime = 0
	
	//Timestamp for keeping track of BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS
	INT giVehicleHaltTimeOut = 0
	
	//Tunnel interior index array
	CONST_INT gTunnelCount 61
	INTERIOR_INSTANCE_INDEX g_intTunnels[gTunnelCount+5]
	
	//Tunnel interior index array
	CONST_INT gRoadTunnelCount 18
	INTERIOR_INSTANCE_INDEX g_intRoadTunnels[gRoadTunnelCount]
	
	//INT for tracking if the Replay Recording UI is displaying, and delaying input after release to avoid button conflicts
	INT iDisplayingReplayRecordUI
	
	// Temp INTs for biker warehouses until stats are added
	CONST_INT gMaxBikerWarehouses	5
	
	// Include all model name enums.
	USING "model_enums.sch"
	
	// Add any shared Globals here
    // NOTE: None of these Globals will be saved
	
	USING "charsheet_globals.sch"
    USING "cellphone_globals.sch"
	USING "selector_globals.sch"
	USING "clock_globals.sch"
	USING "scaleform_globals.sch"
	USING "menu_globals.sch"
	
  	USING "shared_globals.sch"
	USING "SP_globals.sch"
	
	// MP Globals registered in STANDARD block (accessed in startup)
	USING "MP_globals_interior_instances.sch"
	USING "MP_globals_Teams.sch"
	
	// Implemented for B*3936503
	USING "net_simple_interior_base.sch"
	
	// Title update patch fix. Padding default global block with the old MP savegame struct.
	USING "MP_globals_saved_old.sch"
	
	BOOL g_bHaveFull_NetworkPrivileges		= FALSE
	BOOL g_bHaveFriend_NetworkPrivileges	= FALSE
	BOOL g_Private_Is_Running_Boot_Invite_Supress	= FALSE
	
	ENUM LAST_GEN_STATUS
		LAST_GEN_STATUS_NONE,
		LAST_GEN_STATUS_FAILED,
		LAST_GEN_STATUS_IS_LAST_GEN,
		LAST_GEN_STATUS_IS_NOT_LAST_GEN
	ENDENUM
	LAST_GEN_STATUS g_i_Private_AreCharactersLastGen = LAST_GEN_STATUS_NONE
	LAST_GEN_STATUS g_i_Private_IsPlayerFromLastGen = LAST_GEN_STATUS_NONE
	BOOL g_bCheckAndForceLastGenPlayer = TRUE
	
	ENUM PLATFORM_UPGRADE_LB_ENUMS
		PLATFORM_UPGRADE_LB_CHECK_PS3_BLIMP = 0,	
		PLATFORM_UPGRADE_LB_CHECK_XBOX360_BLIMP,
		PLATFORM_UPGRADE_LB_HAS_BLIMP,
		
		PLATFORM_UPGRADE_LB_CHECK_PS3_COLLECT,	
		PLATFORM_UPGRADE_LB_CHECK_XBOX360_COLLECT,	
		PLATFORM_UPGRADE_LB_HAS_COLLECT,
		
		PLATFORM_UPGRADE_LB_CHECK_PS3_SPECIAL,
		PLATFORM_UPGRADE_LB_CHECK_XBOX360_SPECIAL,	
		PLATFORM_UPGRADE_LB_HAS_SPECIAL
	ENDENUM

	STRUCT PLATFORM_UPGRADE_LB_CHECKS
		INT iTotalStage
		INT iReadStage
		BOOL bSuccessful
		BOOL bComplete
		INT iFrameCheck
	ENDSTRUCT
	
	STRUCT SCRIPT_CONTROL_HOLD_TIMER
		SCRIPT_TIMER sTimer
		CONTROL_TYPE control 
		CONTROL_ACTION action
	ENDSTRUCT
	
	STRUCT RGBA_STRUCT
		FLOAT R, G, B, A = 255.0
	ENDSTRUCT
	
	PLATFORM_UPGRADE_LB_CHECKS platformUpgradeLBCheck
	PLATFORM_UPGRADE_LB_CHECKS platformUpgradeLBCheckSP //We use a separate check in SP that works on SP stats. -BenR
	
	// Multiplayer debug-only globals
	#IF IS_DEBUG_BUILD
		USING "MP_globals_debug.sch" // some les widgets are used in sp
	#ENDIF	
	
	
	// MP_Globals_TEMP.sch is still registered in the STANDARD block
	// It is a copy of MP_Globals.sch which is now registered in it's own GLOBALS_MP block
	// The Contents of MP_Globals_temp.sch will be pulled across to MP_Globals.sch so that
	//		the MP globals can become registered in their own block
    //USING "MP_globals_TEMP.sch"
	
	USING "feed_globals.sch"
	
	#IF IS_DEBUG_BUILD
		USING "debug_globals.sch"
	#ENDIF	

#IF FEATURE_GEN9_STANDALONE

	STRUCT PENDING_TRANSITION_DATA
		BOOL bHasPendingTransition = FALSE
		TRANSITION_STATE eTargetTransitionState = TRANSITION_STATE_EMPTY
		MP_GAMEMODE eTargetGameMode = GAMEMODE_EMPTY
		HUD_STATE eTargetHudState = HUD_STATE_TOTAL
		BOOL bBailHappened = FALSE
	ENDSTRUCT

	PENDING_TRANSITION_DATA g_sPendingTransitionData
#ENDIF // FEATURE_GEN9_STANDALONE

ENDGLOBALS		//	GLOBALS_BLOCK_STANDARD
USING "globals_sp_dlc.sch"
using "globals_sp_clifford.sch"
using "globals_sp_pilotschool.sch"
using "globals_sp_norman.sch"

