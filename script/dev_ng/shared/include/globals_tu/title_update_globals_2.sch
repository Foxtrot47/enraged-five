
USING "buildtype.sch"
USING "global_block_defines.sch"
using "player_ped_globals.sch"
USING "flow_commands_enums_game.sch"
USING "flow_global_definitions.sch"
USING "dlc_ped_component_names.sch"

// Title Update globals
GLOBALS GLOBALS_BLOCK_TITLE_UPDATE_2

USING "MP_globals_races_TU.sch"
USING "MP_globals_player_headshots_TU.sch"
USING "MP_globals_interactions_TU.sch"
USING "MP_globals_cash_transactions_TU.sch"			// JamesA - New cash transaction service globals
USING "MP_globals_reward_transactions_TU.sch"

USING "randomChar_globals_TU.sch"
USING "shop_globals_TU.sch"
USING "website_globals_TU.sch"
USING "menu_globals_TU.sch"
USING "cellphone_globals_TU.sch"
USING "player_scene_globals_TU.sch"

USING "achievement_queue_globals.sch"

//FBI 1 - Lawrence Kerr
PED_WEAPONS_STRUCT fbi1_player_stored_weapons

#IF IS_DEBUG_BUILD
	USING "mp_globals_teams_TU.sch"
#ENDIF

CONST_INT SP_DLC_SAVE_DATA_HAS_BEEN_REGISTERED				0
CONST_INT SP_PILOTSCHOOL_SAVE_DATA_HAS_BEEN_REGISTERED		1
CONST_INT STANDARD_GLOBAL_SAVE_DATA_HAS_BEEN_REGISTERED		2
CONST_INT SP_CLIFFORD_SAVE_DATA_HAS_BEEN_REGISTERED			3
CONST_INT SP_NORMAN_SAVE_DATA_HAS_BEEN_REGISTERED			4

INT iBitFieldOfRegisteredSaveData

AVAILABLE_MISSION_STORAGE g_availableMissionsTU[MAX_MISSIONS_AVAILABLE_TU]

TEXT_LABEL_31 g_txtSP_CurrentSelfie_anim

BOOL g_bSkipWeatherResetOnRespawn	// Used to stop the respawn controller from randomising weather after the player respawns. If we are
									// replaying a mission after death we don't want this.

ENDGLOBALS  //  GLOBALS_BLOCK_TITLE_UPDATE_2




