USING "achievements_enum.sch"
USING "timer_public.sch"

//----------------------
//	STRUCTS
//----------------------
STRUCT ACHIEVEMENT_QUEUE
	ACHIEVEMENT_ENUM mQueue[ACHIEVEMENT_COUNT]
	BOOL bIsRunning = FALSE
	FLOAT fQueueTimer = 0.0
ENDSTRUCT

//----------------------
//	CONSTS
//----------------------
FLOAT 		ACHIEVEMENT_QUEUE_TIME	=	8.0			//	Changhed to a float so it can be slowed down for Director mode achievements as many can fire at once.
CONST_FLOAT ACHIEVEMENT_QUEUE_RUNTIME	300.0		// the queue will run for 5 minutes

//----------------------
//	VARIABLES
//----------------------
ACHIEVEMENT_QUEUE q_mAchievementQueue

