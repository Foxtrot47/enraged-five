USING "commands_misc.sch"

// ===========================================================================================================
//      spectator cam globals and consts
// ===========================================================================================================

// =================
// 	Social Club Television (SCTV)
// =================

ENUM eSCTVModeList_OLD
	SCTV_INIT,
	SCTV_FOLLOW_TARGETS,
	SCTV_FREE_CAM,
	SCTV_CLEANUP
ENDENUM


//STRUCT sSCTVMissionListEntry_OLD
//	INT iPlayerIDAsINT
//	INT iPlayerGBD
//	INT iFMMCType
//	INT iInstance
//	TEXT_LABEL_31 tl31Label
//	BOOL bUsedCoords = FALSE
//	BOOL bLaunch = FALSE
//ENDSTRUCT

//STRUCT SCTV_DATA_STRUCT_OLD
//	eSCTVModeList_OLD currentMode = SCTV_INIT
//	eSCTVModeList_OLD switchToMode = SCTV_INIT
//	INT iBitSet = 0
//	INT iFreeCamTransitionCatchCounter = 0
//	sSCTVMissionListEntry_OLD sMissionList[16]
//	INT iMissionList = 0
//	INT iMissionListSelect = 0
//ENDSTRUCT
//
//
//STRUCT MPSpectatorGlobalStruct_OLD
//	INT iBitSet = 0
//	PED_INDEX pedCurrentFocus
//	PED_INDEX pedDesiredFocus
//	
//	SCTV_DATA_STRUCT_OLD SCTVData
//ENDSTRUCT
//MPSpectatorGlobalStruct_OLD MPSpecGlobals_OLD
