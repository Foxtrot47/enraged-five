
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   MP_globals_saved.sch
//      AUTHOR          :   Conor McGuire
//      DESCRIPTION     :   This script is essentially a giant struct containing other
//                          structs defined by other globals files. All variables contained
//                          within this struct will be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "MP_Globals_Saved_Vehicles.sch"
//USING "MP_Globals_Saved_Buddies.sch"
USING "MP_Globals_Saved_CarApp.sch"
USING "MP_Globals_Saved_General.sch"
USING "MP_Globals_Saved_Big_Ass_Vehicles.sch"
USING "MP_Globals_Saved_Property.sch"
USING "MP_Globals_Saved_Bounty.sch"
USING "MP_Globals_Saved_Atm.sch"
USING "MP_Globals_ScriptSaves.sch"


STRUCT MP_SAVED_BIG_ASS_VEHICLES_STRUCT_OLD
	INT 								iBigAssVehiclesBS
ENDSTRUCT


STRUCT MP_SAVED_CARAPP_STRUCT_OLD
	SOCIAL_CAR_APP_DATA sCarAppData[10]
	SOCIAL_CAR_APP_ORDER_DATA sCarAppOrder[10]
	
	BOOL bMultiplayerDataWiped
	BOOL bDeleteCarData
	BOOL bCarAppPlateSet
	
	TEXT_LABEL_15 tlCarAppPlateText
	INT iCarAppPlateBack
	
	INT iOrderCount
ENDSTRUCT



STRUCT MP_SAVED_GENERAL_STRUCT_OLD
	INT iCashGivenTotal = 0
	INT iCashGivenTime 	= -1		//GET_CLOUD_TIME_AS_INT()
	INT iLastSavedCarUsed = -1
	INT iLastFreemodeDataLBMaintenance = 0
	INT iCurrentPropertyValue = 0 //stored as tunable can mean values change
	INT iWheelieDayTimer
	INT iWheelieUpdatesThisDay
	INT iWheelieTimeAtStartOfDay
	BOOL bGrabCurrentWheelieTime
	INT iLastSoldVehicleTime
	INT ilasttimeplayed = 0 
	
	BOOL bDefaultClothesSet
	
	INT iClothesViewedBitset[3]
	INT iCarmodsViewedBitset[604]
	
	INT iLastCrewCharWasIn
ENDSTRUCT


STRUCT MP_SAVED_ATM_STRUCT_OLD
	
	//log data
	INT iLogCaret
	INT LogValues[16]
	TEXT_LABEL_23 LogSourceNames[16]
	MP_ATM_LOG_ACTION_TYPE LogActionType[16]
	
ENDSTRUCT



// All structs containing variables that should be saved should be contained within this struct
//STRUCT g_structSavedMPGlobals_OLD
//
//    // All saved data relating to MP saved vehicles
//    MP_SAVED_VEHICLE_STRUCT_OLD				MpSavedVehicles[14]
//	
//	// All saved data relating to MP saved vehicles that do not fit in the garage
//    MP_SAVED_BIG_ASS_VEHICLES_STRUCT_OLD	MpSavedBigAssVehicles
//	
//	// All saved data relating to MP property
//    MP_SAVED_PROPERTY_STRUCT_OLD			MpSavedProperty
//  
//  	// All saved data relating to MP saved buddies
//	//MP_SAVED_BUDDIES_STRUCT				MpSavedBuddies
//	
//	// All saved data relating to MP CarApp data
//	MP_SAVED_CARAPP_STRUCT_OLD			MpSavedCarApp
//
//	// All saved data relating to MP General data
//	MP_SAVED_GENERAL_STRUCT_OLD			MpSavedGeneral
//	
//	// All saved data relating to MP Bounties
//	MP_SAVED_BOUNTY_STRUCT_OLD			MpSavedBounty
//	
//	//All saved data relating to the MP ATM log
//	MP_SAVED_ATM_STRUCT_OLD				MpSavedATM
//	
//	//Save data setup to take a load off the profile stats. 
//	MP_SAVE_DATA_STRUCT					mpSaveData
//	
//ENDSTRUCT
//
//g_structSavedMPGlobals_OLD g_savedMPGlobals_OLD
