
//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   menu_globals_TU.sch                                         //
//      AUTHOR          :   Ben R, Graeme W                                             //
//      DESCRIPTION     :   Globals used to store menu data (title update extensions).  //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT MAX_STORED_DISCOUNT_INTS          3
CONST_INT MAX_STORED_DISCOUNT_FLOATS        3
CONST_INT MAX_STORED_DISCOUNT_STRINGS       3
CONST_INT MAX_STORED_DISCOUNT_PLAYER_NAMES  2


///////////////////////////////////////////////////////////
STRUCT MenuData_TU
    TEXT_LABEL_63 eHelpKey
    TEXT_LABEL_15 tl15HelpText
    INT iHelpTextINT
    
    // Discount
    TEXT_LABEL_15 tl15Discount
    MENU_TEXT_COMP_TYPE eDiscountComps[MAX_STORED_TEXT_COMPS]
    INT iDiscountInt[MAX_STORED_DISCOUNT_INTS]
    FLOAT fDiscountFloat[MAX_STORED_DISCOUNT_FLOATS]
    INT iDiscountFloatDP[MAX_STORED_DISCOUNT_FLOATS]
    TEXT_LABEL_23 tl15DiscountText[MAX_STORED_DISCOUNT_STRINGS]
    INT iDiscountTotalParams
    INT iDiscountIntParams
    INT iDiscountFloatParams
    INT iDiscountTextParams
    INT iDiscountClearTimer
    INT iDiscountStartTimer
    MENU_ICON_TYPE eDiscountIcon
ENDSTRUCT

MenuData_TU g_sMenuData_TU


INT g_i_CurrentlySelected_SnapMaticFilter = 0 //No filter by default.
INT g_i_CurrentlySelected_SnapMaticBorder = 0 //No filter by default.


// Menu cursor coords so we can keep track of how far the mouse/touchpad has traveled this frame for the menus
FLOAT g_fMenuCursorX
FLOAT g_fMenuCursorY
FLOAT g_fMenuCursorXPrev
FLOAT g_fMenuCursorYPrev
FLOAT g_fMenuCursorXMoveDistance    
FLOAT g_fMenuCursorYMoveDistance
INT g_iMenuCursorItem

// Menu cursor function return states
CONST_INT MENU_CURSOR_NO_ITEM           	-1  // Cursor is not on the menu, no item returned, but other cursor interactions (e.g. camera movement) are possible.
CONST_INT MENU_CURSOR_SCROLL_UP         	-2  // Scroll up button is highlighted
CONST_INT MENU_CURSOR_SCROLL_DOWN       	-3  // Scroll down button is highlighted
CONST_INT MENU_CURSOR_NO_CAMERA_MOVE    	-4  // Cursor is not on the menu, but is off to the left of the edge of the menu, so camera movement is disabled.
CONST_INT MENU_CURSOR_INSTRUCTIONAL_BUTTONS -5  // Cursor is not on the menu, but is near the instructional buttons. Camera movement is disabled, but cursor is solid.
CONST_INT MENU_CURSOR_DRAG_CAM				-6  // Cursor is not on the menu, but is near the instructional buttons. Camera movement is disabled, but cursor is solid.

// Menu cursor hold states
CONST_INT MENU_CURSOR_BTN_NONE				0	// Button isn't pressed
CONST_INT MENU_CURSOR_BTN_HOLD_WARM_UP		1	// Button is pressed and held for less than the warm up time
CONST_INT MENU_CURSOR_BTN_HOLD_TIMED		2	// Button is being held - here we check the hold time

CONST_INT MENU_CURSOR_ARROW_SCROLL_TIME		300

// Menu cursor button hold states
INT g_iMenuCursorHoldTime = 0
