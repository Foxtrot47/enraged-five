
USING "buildtype.sch"
USING "global_block_defines.sch"
using "player_ped_globals.sch"
USING "flow_commands_enums_game.sch"
USING "flow_global_definitions.sch"
USING "dlc_ped_component_names.sch"
USING "title_update_globals_2.sch"

// Title Update globals
GLOBALS GLOBALS_BLOCK_TITLE_UPDATE

USING "mp_globals_new_features_TU.sch"			// NF: 7/3/14 any new feature added should be wrapped in #IF with CONST_INT declared in here.

USING "mp_globals_spectator.sch"
USING "MP_Globals_Missions_At_Coords_TU.sch"        // KGM 24/8/13 - Any additional MissionAtCoords globals that need declared in the TU block
USING "MP_Globals_Mission_Control_TU.sch"           // KGM 25/8/13 - Any additional Mission Control globals that need declared in the TU block
USING "MP_Globals_Mission_Trigger_TU.sch"           // KGM 25/8/13 - Any additional Mission Trigger and Joblist globals that need declared in the TU block
USING "MP_Globals_Missions_Shared_TU.sch"           // KGM 25/8/13 - Any additional Shared Mission globals that need declared in the TU block

USING "MP_globals_activity_selector_TU.sch"
USING "MP_globals_Spawning_TU.sch"
USING "MP_globals_COMMON_TU.sch"
USING "MP_globals_ambience_TU.sch"
USING "MP_globals_comms_TU.sch"
USING "MP_globals_hud_TU.sch"
USING "MP_globals_stats_TU.sch"
USING "MP_globals_events_TU.sch"

ENDGLOBALS  //  GLOBALS_BLOCK_TITLE_UPDATE


