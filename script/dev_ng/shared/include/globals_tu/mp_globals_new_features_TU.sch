//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   mp_globals_new_features_TU.sch                             	//
//      DESCRIPTION     :   const ints used to activate / deactive new features in		//
//							different dlc packs.										//
//																						//	
//		NOTES:	Should only be edited in dev_network by developers. Dev_live version 	//
//				is locked and should only be edited by build guys. 						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//CONST_INT FEATURE_MENTAL_STATE					1		// business pack 2
//CONST_INT FEATURE_FULL_BODY_INTERACTIONS		1		// hipster - moved forward to business2
CONST_INT FEATURE_APARTMENT_FURNITURE			0		// job pack
//CONST_INT FEATURE_BIG_FEED_PLAYER_DATA			1		// CTF Creator
//CONST_INT FEATURE_NEW_DRINKING_ACTIVITIES		1		// business pack 2
CONST_INT FEATURE_NEW_GYM_ACTIVITIES     		0		// apartment pack 2
//CONST_INT FEATURE_HEIST_PLANNING				1		// Heist planning board.
//CONST_INT FEATURE_VS_MISSION_ROUNDS				1		// versus missions (LTS Creator)
//CONST_INT FEATURE_SELFIE_INTERACTIONS			1		// business pack 2
//CONST_INT FEATURE_INTERACTIONS_BUSINESS2		1		// business2
//CONST_INT FEATURE_INTERACTIONS_HIPSTER			1		// hipster
CONST_INT FEATURE_INTERACTIONS_SPORTS			0		// sports
CONST_INT FEATURE_INTERACTIONS_ARMY				0		// army
CONST_INT FEATURE_INTERACTIONS_UNKNOWN			0		// pack not yet known
//CONST_INT FEATURE_INTERACTIONS_REENTRY_IDLES	1		// day 1 patch
//CONST_INT FEATURE_SYNCED_INTERACTIONS			1		// heists
CONST_INT FEATURE_BATHROOM_ACTIVITIES			0		// apartment pack2
CONST_INT FEATURE_BENCH_RELATIONSHIPS			0		// relationship pack2
//CONST_INT FEATURE_INDEPENDENCE					1		// Independence day pack
//CONST_INT FEATURE_LASTTEAMSTANDING				1		// Last Team Standing pack
//CONST_INT FEATURE_RADIO_ANIMS					1		// Pilot School Pack
//CONST_INT FEATURE_INDEPENDANT_RADIO				1		// Heist Pack
//CONST_INT FEATURE_32_PLAYERS_NG					1		// 32 player limit in the creator
//CONST_INT FEATURE_PILOT_SCHOOL					1		// Pilot School
//CONST_INT FEATURE_INTERACTIONS_LUXE				1		// luxe pack
//CONST_INT FEATURE_INTERACTIONS_LUXE_2			1		// luxe pack 2
CONST_INT FEATURE_ARMY							0		// Army 
CONST_INT FEATURE_CLIFFORD						0		// Clifford
//CONST_INT FEATURE_XMAS_2014						1 		// Christmas 2014
//CONST_INT FEATURE_LUXE_PACK_2					1		// Luxe Pack 2
//CONST_INT FEATURE_CASH_TRANSACTIONS 			1 		// Cash Transactions - PC ONLY
//CONST_INT FEATURE_LUXE_VEH_ACTIVITIES 			1 		// luxe pack
//CONST_INT FEATURE_NEW_AMBIENT_EVENTS 			1 		// New Freemode Ambient Events
//CONST_INT FEATURE_LOWRIDER_CONTENT 				1 		// Lowrider Pack
//CONST_INT FEATURE_LOWRIDER2_CONTENT 			1 		// Lowrider2 Pack
//CONST_INT FEATURE_HALLOWEEN_CONTENT 			1 		// Halloween Pack
//CONST_INT FEATURE_THANKSGIVING_CONTENT 			1		// Thanksgiving Pack
//CONST_INT FEATURE_PIEO_NEON						1		// Neon Lights PI Menu Option for peacock system
//CONST_INT FEATURE_XMAS_2015						1 		// Christmas 2015
//CONST_INT FEATURE_JANUARY_2016 					1 		// January 2016
//CONST_INT FEATURE_VALENTINES_2016 				1 		// Valentines 2016

//CONST_INT FEATURE_APARTMENT_CONTENT 				1 		// Apartment Pack
//CONST_INT FEATURE_GANG_BOSS 						1 		// Boss/goons for Magnate/Apartment Pack

//CONST_INT FEATURE_GRIEF_PASSIVE						1		// Allows players to go passive against 
															// individual players to prevent griefing


//CONST_INT FEATURE_BG_SCRIPT_KICK					1		// lowrider

// Only ever debug only
#IF IS_DEBUG_BUILD
CONST_INT FEATURE_WANTED_SYSTEM 					0 		// GTAO Wanted System Test
#ENDIF

//CONST_INT FEATURE_EXECUTIVE							1		// Executive pack 1
//CONST_INT FEATURE_STUNT								1		//Stunt pack

#IF IS_DEBUG_BUILD
CONST_INT FEATURE_CRASH_SITE_2					0		// Crash Site 2 - Contraband version (no longer being developed)
CONST_INT FEATURE_STUNT_FM_EVENTS				0		// Stunt Freemode events and challenges 		
//CONST_INT FEATURE_GR_CRASH_SITE_2				1		// Crash Site 2 - Gunrun version
#ENDIF

//CONST_INT FEATURE_BIKER							1		//Biker Pack
//CONST_INT FEATURE_PERSONAL_CAR_MOD				1		// Personal car mod shop for biker and import export
//CONST_INT FEATURE_IMPORT_EXPORT					1		//Import Export Pack

#IF NOT IS_DEBUG_BUILD
CONST_INT FEATURE_CRASH_SITE_2					0		// Crash Site 2 - Contraband version (no longer being developed)
CONST_INT FEATURE_STUNT_FM_EVENTS				0		//Stunt Freemode events and challenges 
//CONST_INT FEATURE_GR_CRASH_SITE_2				1		// Crash Site 2 - Gunrun version
#ENDIF

//CONST_INT FEATURE_NEW_CORONAS							1		//New coronas

#IF IS_DEBUG_BUILD
CONST_INT FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER	0 // temp name for in developement pack aircraft carrier to facilitate setting up more generic property queries
#ENDIF

//CONST_INT FEATURE_APARTMENT_ACTIVITY_CREATOR 		1 // New Data Driven Activity Creator

#IF IS_DEBUG_BUILD
CONST_INT FEATURE_FREEMODE_PED_ANIM_CREATOR_PROTOTYPE  1 // protype for running worker peds from freemode
#ENDIF

#IF NOT IS_DEBUG_BUILD
CONST_INT FEATURE_FREEMODE_PED_ANIM_CREATOR_PROTOTYPE  0 // protype for running worker peds from freemode
#ENDIF

#IF IS_DEBUG_BUILD
CONST_INT FEATURE_SHORTEN_SKY_HANG			1  // stop player hanging in sky on transition
#ENDIF
#IF NOT IS_DEBUG_BUILD
CONST_INT FEATURE_SHORTEN_SKY_HANG			0  // stop player hanging in sky on transition
#ENDIF

//CONST_INT FEATURE_SPECIAL_RACES 			1 // Special Race pack Jan 2017
//CONST_INT FEATURE_GUNRUNNING				1  // Gunrunning DLC
//CONST_INT FEATURE_SMUGGLER					1  // Smuggler and Air Races DLC 
//CONST_INT FEATURE_AIR_RACES 				1 // Air Race pack Jun 2017


//CONST_INT FEATURE_SPAWN_IN_SIMPLE_INTERIOR	1


//CONST_INT FEATURE_GANG_OPS					1 // New gang ops for Xmas 2017 (heists 2)
//CONST_INT FEATURE_CHINESE_SIMPLIFIED 		1
//CONST_INT FEATURE_CRIMINAL_ENTERPRISE_STARTER_PACK			1

//CONST_INT FEATURE_BUSINESS_BATTLES			1  // Freemode Business Battles

//CONST_INT FEATURE_ARENA_WARS				1

//CONST_INT FEATURE_TARGET_RACES				1  // Target Races Jan 2018

CONST_INT FEATURE_CASINO					1	//Vinewood casino Summer 2019

CONST_INT FEATURE_CASINO_HEIST				1	//Casino Heist Xmas 2019 (heists 3)

CONST_INT FEATURE_FREEMODE_ARCADE			0
CONST_INT FEATURE_COPS_N_CROOKS				0

#IF IS_PS5_BUILD
CONST_INT FEATURE_GEN9_STANDALONE			1
CONST_INT FEATURE_GEN9_EXCLUSIVE			1
CONST_INT FEATURE_GEN9_RELEASE				1
CONST_INT FEATURE_GTAO_MEMBERSHIP			1
#ENDIF

#IF IS_XBSX_BUILD
CONST_INT FEATURE_GEN9_STANDALONE			1
CONST_INT FEATURE_GEN9_EXCLUSIVE			1
CONST_INT FEATURE_GEN9_RELEASE				1
CONST_INT FEATURE_GTAO_MEMBERSHIP			1
#ENDIF

#IF IS_PC_BUILD
CONST_INT FEATURE_GEN9_STANDALONE			0
CONST_INT FEATURE_GEN9_EXCLUSIVE			0
CONST_INT FEATURE_GEN9_RELEASE				0
CONST_INT FEATURE_GTAO_MEMBERSHIP			0
#ENDIF

#IF IS_PS4_BUILD
CONST_INT FEATURE_GEN9_STANDALONE			0
CONST_INT FEATURE_GEN9_EXCLUSIVE			0
CONST_INT FEATURE_GEN9_RELEASE				0
CONST_INT FEATURE_GTAO_MEMBERSHIP			0
#ENDIF

#IF IS_XB1_BUILD
CONST_INT FEATURE_GEN9_STANDALONE			0
CONST_INT FEATURE_GEN9_EXCLUSIVE			0
CONST_INT FEATURE_GEN9_RELEASE				0
CONST_INT FEATURE_GTAO_MEMBERSHIP			0
#ENDIF

CONST_INT FEATURE_SUMMER_2020				1

CONST_INT FEATURE_ENDLESS_WINTER			0

CONST_INT FEATURE_MUSIC_STUDIO				1

CONST_INT FEATURE_HEIST_ISLAND				1

CONST_INT FEATURE_CASINO_NIGHTCLUB			1
CONST_INT FEATURE_HEIST_ISLAND_DANCES		1

CONST_INT FEATURE_TUNER						1
CONST_INT FEATURE_HALLOWEEN_2021			1

CONST_INT FEATURE_FIXER						1

CONST_INT FEATURE_FREEMODE_CLASSIC			0

CONST_INT FEATURE_DLC_1_2022				1

#IF IS_DEBUG_BUILD
CONST_INT FEATURE_FOW_EXPANSION		0
#ENDIF
#IF NOT IS_DEBUG_BUILD
CONST_INT FEATURE_FOW_EXPANSION		0
#ENDIF

#IF IS_DEBUG_BUILD
CONST_INT FEATURE_DLC_2_2022		1
#ENDIF
#IF NOT IS_DEBUG_BUILD
CONST_INT FEATURE_DLC_2_2022		0
#ENDIF

// eof


