
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     						 							
//      SCRIPT NAME     :   cellphone_globals_TU.sch                                  							
//      AUTHOR          :   Steve Taylor                                                								
//      DESCRIPTION     :   Globals used to store new cellphone data and functionality 		
//                          		(title update extensions).                                 									 
//                                                                                   
////////////////////////////////////////////////////////////////////////////////////////////////////////////


//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                           MP Email                                                                                                               
//________________________________________________________________________________________________________________________________________________________________________________


#if IS_DEBUG_BUILD

    BOOL g_b_InstructEyefindtoSendAutoLongMessage = FALSE
    BOOL g_b_ForceRerunOfMarketingPosixGrab = FALSE

#endif


INT g_NumberOfUnreadMarketingEmailsThisSession = 0 //We determine the value of this once per session in cellphone controller. Search on GNMCurrentStage.


ENUM enumMPemailLockedStatus //will specify whether or not this message can be freely deleted from the list if the email message array nears capacity.

    email_EMPTY,
    email_UNLOCKED,
    email_LOCKED

ENDENUM



ENUM enumMPemailMissionCritical  //additional specifier for mission critical email message that could be used to display a message at the top of the display list.
                                
    EMAIL_NOT_CRITICAL,
    EMAIL_CRITICAL

ENDENUM



ENUM enumMPemailAutoUnlockAfterRead //If a message has been locked on sending, i.e it can't be deleted by the user via the phone interface, this specifies if it will auto unlock on reading.
                                
    EMAIL_AUTO_UNLOCK_AFTER_READ,
    EMAIL_DO_NOT_AUTO_UNLOCK

ENDENUM



ENUM enumMPemailIsReplyRequired //specifies whether this email message will allow the player to reply with yes / no... a "replay mission" txt for example.

    MPE_NO_REPLY_REQUIRED,
    MPE_REPLY_IS_REQUIRED, 
    MPE_REPLIED_BARTER,
    MPE_REPLIED_YES,
    MPE_REPLIED_NO

ENDENUM


ENUM enumMPemailCanCallSender

    MPE_CANNOT_CALL_SENDER,
    MPE_CAN_CALL_SENDER

ENDENUM


ENUM enumMPemailIsBarterRequired

    MPE_NO_BARTER_REQUIRED,
    MPE_BARTER_IS_REQUIRED

ENDENUM




ENUM enumMPemailDeletionMode 

    MPE_DELETE_FROM_THIS_CHAR,
    MPE_DELETE_FROM_ALL_CHARACTERS

ENDENUM



ENUM enumMPemailReadStatus

    UNREAD_MPEMAIL,
    READ_MPEMAIL

ENDENUM


/* Can reuse the SP enum here. Copy kept for reference.
ENUM enumMPemailSpecialComponents
    
    NO_SPECIAL_COMPONENTS,
    STRING_COMPONENT,
    NUMBER_COMPONENT,
    STRING_AND_NUMBER_COMPONENT,
    CAR_LIST_COMPONENT,
    SUPERAUTOS_LIST_COMPONENT,
    LEGENDARY_LIST_COMPONENT,
    PEDALMETAL_LIST_COMPONENT,
    WARSTOCK_LIST_COMPONENT,
    ELITAS_LIST_COMPONENT,
    DOCKTEASE_LIST_COMPONENT

ENDENUM
*/


STRUCT structMPemailTimeSent


    INT emailSecs
    INT emailMins
    INT emailHours

    INT emailDay
    INT emailMonth // The native command uses an enum to hold the return value from the current month - will enum-to-int for uniformity's sake.
    
    INT emailYear


ENDSTRUCT





STRUCT structEmailMessage


    //Removed on trial basis. Text messages are using global text rather than a text block in the interim.
    //TEXT_LABEL                    TxtBlockToLoad
    
    TEXT_LABEL_63                   emailLabel 

    INT                             emailFeedEntryId


    enumCharacterList               emailSender 

    structMPemailTimeSent           emailTimeSent


    enumMPemailLockedStatus          emailLockStatus 
    enumMPemailMissionCritical       emailCritical
    enumMPemailAutoUnlockAfterRead   emailAutoUnlockStatus


    enumMPemailDeletionMode          emailDeletionMode


    enumMPemailReadStatus            emailReadStatus
    enumMPemailIsReplyRequired       emailReplyStatus
    enumMPemailCanCallSender         emailCanCallSenderStatus
    enumMPemailIsBarterRequired      emailBarterStatus

    enumTxtMsgSpecialComponents     emailSpecialComponents  //reusing TxtMsg enum here.

    TEXT_LABEL_63                   emailStringComponent
    INT                             emailNumberComponent

    TEXT_LABEL_63                   emailSenderStringComponent


    INT                             emailNumberOfAdditionalStrings
    TEXT_LABEL_63                   emailSecondStringComponent
    TEXT_LABEL_63                   emailThirdStringComponent


    BOOL                            PhonePresence[4]  //specifies whether this message should be included in each player character filter.


ENDSTRUCT


//Will put in when we need to support this for email.
//When moving to single message view, if the chosen text message is found to have an associated appInternet url, then it is stored in this buffer.
//It is possible to extract the string from the text_label using GET_FILENAME_FOR_AUDIO_CONVERSATION. Andrew can then use ARE_STRINGS_EQUAL to check if
//it is "NO_HYPERLINK_EMBEDDED". If it isn't then he uses the actual contents as the starting webpage.

//TEXT_LABEL_63 g_HyperLink_Buffered_Label = "NO_HYPERLINK"








//Email message array. For simplicity, 
CONST_INT MAX_MP_EMAILS 12      //The number of email messages that we can store and display is equal to the MAX_MP_EMAILS - 2. The chronological sort needs a dummy, one 
                                //is taken up for "while" loop comparison ease. So if MAX_MP_EMAILS = 8, we can have 0,1,2,3,4 and 5 displayed.



structEmailMessage g_EmailMessage[MAX_MP_EMAILS]


INT EmailIdentifiedFreeArrayIndex 


//INT g_CurrentNumberOfUnreadEmails //Was g_CurrentNumberOfUnreadTexts - currently a local var; seems okay for this in the meantime.




 BOOL g_LastEmailSentMustBeRead = FALSE     //If this is set to true then the system will not accept any new emails until the blocking email that set this, is read in single messsage view.



//Might not need these...

// Keith 26/9/12: Contains the pre-generated headshotID when the text message is sent from an MP player.
//          If not NULL it should be valid - you can be sure by calling: IS_PEDHEADSHOT_VALID(PEDHEADSHOT_ID) and IS_PEDHEADSHOT_READY(PEDHEADSHOT_ID),
//              If either of these return FALSE then there is a problem somewhere so just use a default headshot TXD or something.
//          You can get the txdString associated with this headshot ID using: GET_PEDHEADSHOT_TXD_STRING(PEDHEADSHOT_ID),
//              sounds like you pass the returned string to the Feed function as both the TXD and the ImageName
PEDHEADSHOT_ID  g_playerMPemailHeadshotMP            = NULL





//HeadshotStrings do not require to be saved.
PEDHEADSHOT_ID g_MPemailHeadShotID[MAX_MP_EMAILS]








 //Add more to this if need be... 0, 1, 2... to 31. Linked to BitSet_CellphoneTU declaration.

CONST_INT   g_BSTU_DISPLAY_EMAIL_SIGNIFIER              0
CONST_INT   g_BSTU_AUTOLAUNCH_NEW_EMAIL_APP             1
CONST_INT   g_BSTU_REMOVE_SNAPMATIC_GRID                2
CONST_INT   g_BSTU_QUICK_LAUNCH_SNAPMATIC               3
CONST_INT   g_BSTU_ENABLE_MP_TRACKIFY                   4

CONST_INT   g_BSTU_REMOVE_MP_TRACKIFY_TARGET_0          5
CONST_INT   g_BSTU_REMOVE_MP_TRACKIFY_TARGET_1          6
CONST_INT   g_BSTU_REMOVE_MP_TRACKIFY_TARGET_2          7
CONST_INT   g_BSTU_REMOVE_MP_TRACKIFY_TARGET_3          8
CONST_INT   g_BSTU_REMOVE_MP_TRACKIFY_TARGET_4          9//Spare

CONST_INT   g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION     10 //2023094


CONST_INT   g_BSTU_ONCALL_SIGNIFIER_DISPLAYED           11
CONST_INT   g_BSTU_INVITE_SIGNIFIER_DISPLAYED           12

CONST_INT   g_BSTU_HAS_FOCUS_LOCK_HELP_DISPLAYED        13

//CONST_INT   g_BSTU_HAS_FOCUS_LOCK_HELP_DISPLAYED        13 //Used in NG only. To help auto-integrate starting new common entries at bit 14.
//MP Cellphone Application Launcher work.
CONST_INT   g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH      14
CONST_INT   g_BSTU_LAUNCH_CELLPHONE_MP_APPCAMERA        15
CONST_INT   g_BSTU_LAUNCH_CELLPHONE_MP_APPTRACKIFY      16

CONST_INT   g_BSTU_MP_HYPERLINK_EMAIL_HELP              17

CONST_INT   g_BSTU_PUSHTALK_SIGNIFIER_DISPLAYED         19 //This is deliberately bit 19. Bug 2159597

CONST_INT   g_BSTU_ENABLE_MP_SIGHTSEER_APP              20 //Add app for random hacking choices as requested by Dave W. Bug 2556852
CONST_INT   g_BSTU_MP_SIGHTSEER_APP_HAS_LAUNCHED        21 //Add this signal for random hacking choices as requested by Dave W. Bug 2556852


//2574720
CONST_INT   g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP        22 //This specifies that the Presidential Extraction app should be available in the dummy App position of the phone.
CONST_INT   g_BSTU_SET_EXTRACTION_PRES_VIEW             23 //2603763 If the player is acting as the "President" then want to display slightly different text labels. This is a rename. Was originally g_BSTU_HIDE_BODYGUARD_COLUMN but the functionality changed.


//2588287
CONST_INT   g_BSTU_REQUEST_DYNAMIC_APPLIST_UPDATE       24 //If this is set, cellphone flashhand will update the app list if the phone is in max or greater state and redraw the homescreen if in max state.

CONST_INT   g_BSTU_MARKETING_EVENT_CUED_THIS_SESSION    25 //If this is set, that means we have already received a marketing event this session. We only reset this bit when the player reads the latest email in their GTAO phone email inbox.

CONST_INT   g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP        26 //This specifies that the Securoserv app should be available in the dummy App position of the phone.
CONST_INT	g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0		27



CONST_INT   g_BSTU_BEACON_SIGNIFIER_DISPLAYED           28 //Added for 3599537 - display beacon beside radar.

CONST_INT	g_BSTU_LAUNCH_CELLPHONE_MP_APPTEXTS			29 //Added for url:bugstar:6598830 - Can we please set up the system for sending the DJ request missions for Palms Trax and Moodymann when they're playing at the Casino Nightclub?

INT BitSet_CellphoneTU = 0 //clear all bits.

//Allows the colours used for each vector component to be set by MP. url:bugstar:2596571
HUD_COLOURS g_hc_PresX, g_hc_PresY, g_hc_PresZ
HUD_COLOURS g_hc_BodygX, g_hc_BodygY, g_hc_BodygZ



STRUCT StructIndividualContentCreationPedInfo

    PED_INDEX       Index
    TEXT_LABEL_23   VoiceID //decreased from 31 as MaxNum_Conversers has just been upped to 14. If this becomes an actual issue, then enum system comes into play.
    BOOL            ActiveInConversation

    INT             AddPedNativeNumber
    
    //For reference. These don't get used in MP.
    //BOOL            PlayAmbientAnims
    //BOOL            CanUseAutoLookAt //Specifies whether the ped will use the automatic "look at" code system during scripted conversations.

ENDSTRUCT




//Content creation secondary dialog ped struct for bug
STRUCT structContentCreationExtraPedsForConversation

    StructIndividualContentCreationPedInfo PedInfo[20] //catering for 17 to 36

    //For reference...
    //INT     NullPed_Number_for_VoicePlacement
    //VECTOR  NullPed_Vector_for_VoicePlacement 

ENDSTRUCT


structContentCreationExtraPedsForConversation g_ContentCreationConversationPedsStruct


BOOL g_B_ContentCreationPeds_Should_be_added_for_Dialogue = FALSE

CONST_INT constMaxExtraContentCreationNum_Conversers 20

/* Example.
g_ContentCreationConversationPedsStruct.PedInfo[0].AddPedNativeNumber = 17
g_ContentCreationConversationPedsStruct.PedInfo[0].VoiceID = "STEVE"
g_ContentCreationConversationPedsStruct.PedInfo[0].Index = NULL
g_ContentCreationConversationPedsStruct.PedInfo[0].ActiveInConversation = "NULL"
*/


//Expanded MP Trackify work. If we need to support more targets then the 4 will need to be upped.
CONST_INT c_MaxNumberOfTrackifyTargets  21

VECTOR g_v_TrackifyMultipleTarget[c_MaxNumberOfTrackifyTargets]

ENUM eTrackifyArrowType
	Arrow_Off,
	Arrow_Neutral, 
	Arrow_Up,
	Arrow_Down
ENDENUM

eTrackifyArrowType g_v_TrackifyMPTargetArrowType[c_MaxNumberOfTrackifyTargets]

INT g_i_NumberOfTrackifyTargets //This should be set by the script using trackify

//These will be replaced by MP stats to store the MP player's cellphone theme and wallpaper choices.
INT Temp_MPcurrentTheme = 1
INT Temp_MPcurrentWallpaper = 0



//First Person Cellphone Experimental Mode
BOOL g_b_ShouldCellphoneUseFirstPersonAnims = FALSE
BOOL g_FirstPersonTransitionOngoing = FALSE


//Cannot be a saved global. If the crew emblem retrievals fails once, this is set to true in cellphone_fh so we do not attempt it again that session.
BOOL g_b_HasCrewEmblemRetrievalFailedThisSession = FALSE 




//2574720
//Presidential Extraction. Need to display two sets of co-ordinates on a Cellphone App.
VECTOR g_v_ExtractionPresidentVec
VECTOR g_v_ExtractionBodyguardVec

//SecroServ Hacking app
INT iHackPercentage = 0
INT iHackStage = 0
TEXT_LABEL_15 tl15HackComplete = ""

CONST_INT ciSECUROSERV_HACK_STAGE_NO_SIGNAL 		0
CONST_INT ciSECUROSERV_HACK_STAGE_HACKING 			1
CONST_INT ciSECUROSERV_HACK_STAGE_HACK_COMPLETE 	2
CONST_INT ciSECUROSERV_HACK_STAGE_HACK_IN_PROGRESS	3
CONST_INT ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL		4


