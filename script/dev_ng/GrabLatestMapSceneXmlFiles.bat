@echo off
p4 sync //depot/gta5/assets_ng/metadata/content/...xml

p4 sync //depot/gta5/assets_ng/export/levels/gta5/...xml

p4 sync //depot/gta5/build/dev_ng/common/data/levels/gta5/vehicles.meta
p4 sync //gta5_dlc/spPacks/*/build/dev_ng/common/data/levels/gta5/vehicles.meta
p4 sync //gta5_dlc/mpPacks/*/build/dev_ng/common/data/levels/gta5/vehicles.meta
p4 sync //gta5_dlc/patchPacks/*/build/dev_ng/common/data/levels/gta5/vehicles.meta
p4 sync //depot/gta5/titleupdate/dev_ng/common/data/levels/gta5/vehicles.meta

p4 sync //depot/gta5/assets_ng/export/data/peds.pso.meta
p4 sync //gta5_dlc/spPacks/*/build/dev_ng/common/data/peds.meta
p4 sync //gta5_dlc/mpPacks/*/build/dev_ng/common/data/peds.meta
p4 sync //gta5_dlc/patchPacks/*/build/dev_ng/common/data/peds.meta

p4 sync //gta5_dlc/mpPacks/*/assets_ng/vehicles/....xml
p4 sync //gta5_dlc/spPacks/*/assets_ng/vehicles/....xml
p4 sync //gta5_dlc/patchPacks/*/assets_ng/vehicles/....xml

p4 sync //gta5_dlc/mpPacks/*/*.xml
p4 sync //gta5_dlc/mppacks/*/assets_ng/export/levels/gta5/props/...xml

if "%1"=="-nopause" goto :END
pause
:END
